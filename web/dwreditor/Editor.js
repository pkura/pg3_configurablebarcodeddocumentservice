function Editor(options) {
	this.domInput = options.input[0];
	this.file = null;
	this.fileReader = new FileReader();
	this.jqXml = null;
	this.jqXmlFields = null;
	this.xmlString = null;
	this.xmlHeader = null;
	this.jqAvail = options.avail;
	this.jqTable = options.table;
	this.jqCells = null;
	this.defaultSize = 25;
	this.jqSave = options.save;
	this.saving = false;
	
	var _this = this;
	
	var defaultSettings = {
		autosave: false,
		url: 'https://docusafe:8081/docusafe/admin/document-kinds.action'
	}
	
	this.loadSettings = function() {
		var stgs = localStorage.dwrEditorSettings;
		
		if(stgs == null) {
			localStorage.dwrEditorSettings = JSON.stringify(defaultSettings);
			stgs = defaultSettings;
		} else {
			stgs = JSON.parse(stgs);
		}
		return stgs;
	}
	
	this.saveSettings = function() {
		localStorage.dwrEditorSettings = JSON.stringify(this.settings);
	}
	
	var createTable = function() {
		var i, j;
		var jqRow, jqTd, letter;
		_this.jqTable.html('');
		for(i = 0; i < _this.defaultSize; i ++) {
			jqRow = $('<tr/>').attr('id', 'tr-' + (i+1));
			for(j = 0; j < _this.defaultSize; j ++) {
				letter = String.fromCharCode((97 + j));				
				jqTd = $('<td/>').addClass('td-' + (j+1) + '-' + (i+1)).attr('id', 'td-' + letter + (i+1))
					.attr('colindex', j).attr('rowindex', i);
				
				jqRow.append(jqTd);
			}
			
			_this.jqTable.append(jqRow);
		}
		
		jqRow = $('<tr/>').addClass('heading');
		for(i = 0; i < _this.defaultSize; i ++) {
			letter = String.fromCharCode((97 + i));
			jqTd = $('<td/>').addClass('heading').html(letter.toUpperCase());
			jqRow.append(jqTd);
		}
		_this.jqTable.prepend(jqRow);
		i = 0;
		_this.jqTable.find('tr').each(function() {
			jqTd = $('<td/>').addClass('heading');
			if(i > 0) {
				jqTd.html(i);
			}
			$(this).prepend(jqTd);
			i ++;
		});
		
		_this.jqCells = _this.jqTable.find('td:not(.heading)');
	};
	createTable();
	this.settings = this.loadSettings();
}

Editor.prototype.updateXml = function() {
	var _this = this;
	$('.field').each(function() {
		var jqThis = $(this);
		var cn = jqThis.attr('cn');
		var coords = jqThis.attr('coords');
		
		_this.jqXmlFields.filter('[cn=' + cn + ']').attr('coords', coords);
	});
}

Editor.prototype.save = function() {
	var filename;
	try {
		filename = this.file.fileName;
	} catch(e) {
		filename = 'new file';
	}
	var _this = this;
	window.webkitRequestFileSystem(window.TEMPORARY, 1024*1024*1024, function(fs) {
        fs.root.getFile(filename + '.bin', {create: true}, function(fileEntry) {
            fileEntry.createWriter(function(fileWriter) {
				_this.updateXml();
                var builder = new WebKitBlobBuilder();
				var str = _this.xmlHeader + (new XMLSerializer()).serializeToString(_this.jqXml[0]);
                builder.append(str);
                var blob = builder.getBlob();
    
                fileWriter.onwriteend = function() {
                    // navigate to file, will download
                    location.href = fileEntry.toURL();
                };
    
                fileWriter.write(blob);
            }, function() {});
        }, function() {});
    }, function() {});
}

Editor.prototype.parseXml = function(xml) {
	var _this = this;
	$('.field').remove();
	_this.xmlString = xml;
	_this.xmlHeader = _this.xmlString.match(/<\?xml.*\?>/)[0];
	_this.jqXml = $($.parseXML(xml));
	_this.jqXmlFields = _this.jqXml.children().find('fields').children('field');
	
	
	
	var i = 0;
	_this.jqXmlFields.each(function() {
		var jqThis = $(this);
		var coords = jqThis.attr('coords') || null;
		var jqField = $('<div/>');
		var startc = _this.jqCells.filter(':eq(' + i + ')').attr('id').replace('td-', '');
		var splittedCoords, startCoords, endCoords, newWidth, newHeight, jqCell;
		
		jqField.append('<div>' + jqThis.attr('cn') + '</div>').append('<div>' + jqThis.attr('name') + '</div>')
			.append('<div>typ: ' + jqThis.find('type').attr('name') + '</div>')
			.addClass('field').attr('cn', jqThis.attr('cn')).attr('coords', startc);
		
		if(coords != null) {
			jqField.attr('coords', coords);
			splittedCoords = coords.split(':');
			startCoords = splittedCoords[0];
			endCoords = splittedCoords[1] || null;
			$('#td-' + startCoords).append(jqField);
			if(endCoords != null) {
				newWidth = (endCoords.replace(/\d/, '').charCodeAt(0) - startCoords.replace(/\d/, '').charCodeAt(0) + 1) * 100;
				newHeight = (parseInt(endCoords.replace(/[a-z]/, ''), 10) - parseInt(startCoords.replace(/[a-z]/, ''), 10) + 1) * 100;
				//console.info('nw', newWidth, newHeight);
				jqCell = $('#td-' + startCoords);
				jqField.css('position', 'absolute').css('left', jqCell.position().left).css('top', jqCell.position().top)
					.width(newWidth).height(newHeight);
			}
		} else {
			_this.jqCells.filter(':eq(' + i + ')').append(jqField);
		}
		i ++;
	});
	
	$('div.field').draggable().resizable({
		grid: 100,
		start: function(event, ui) {
			$(this).attr('starth', $(this).height()).attr('startw', $(this).width());
		},
		stop: function(event, ui) {
			var coords = $(this).attr('coords');
			var startCoords = $(this).parent('td').attr('id').replace('td-', '');
			var endCoords = null;

			// zmiana wymiar�w
			
			var dCols = $(this).width() / 100 - 1;
			var dRows = $(this).height() / 100 - 1;
			var newCol = String.fromCharCode(coords.replace(/\d/, '').charCodeAt(0) + dCols);
			var newRow = parseInt(coords.replace(/[a-z]/, ''), 10) + dRows;
			var newCoords = newCol + newRow;
			//console.info(newCol);
			if(newCoords != startCoords) {
				coords = startCoords + ':' + newCoords;
				$(this).attr('coords', coords);
				/*_this.jqXmlFields.filter('[cn=' + $(this).attr('cn') + ']').find('type').attr('coords', coords);*/
			}
			
			if($('#autosave')[0].checked)
				$('#savedb').trigger('click');
		}
	});
	$('td:not(.heading)').droppable({
		accept: '.field',
		hoverClass: 'hover-state',
		drop: function(event, ui) {
			var jqThis = $(this);
			var jqField = ui.draggable;
			var newCoords;
			var oldCoords = jqField.attr('coords');
			var oldSplittedCoords = oldCoords.split(':');
			var oldStartCoords = oldSplittedCoords[0];
			var oldEndCoords = oldSplittedCoords[1] || null;
			
			if(!oldEndCoords) {
				newCoords = jqThis.attr('id').replace('td-', '');
			} else {
				var startCoords = jqThis.attr('id').replace('td-', '');
				var dCols = Math.ceil(jqField.width() / 100 - 1);
				var dRows = Math.ceil(jqField.height() / 100 - 1);
				
				var newCol = String.fromCharCode(startCoords.replace(/\d/, '').charCodeAt(0) + dCols);
				var newRow = parseInt(startCoords.replace(/[a-z]/, ''), 10) + dRows;
				newCoords = startCoords + ':' + newCol + newRow;
				
			}
			
	
			jqThis.append(
				jqField.css('position', 'absolute').css('left', jqThis.position().left).css('top', jqThis.position().top)
			);
			jqField.attr('coords', newCoords);
			if($('#autosave')[0].checked)
				$('#savedb').trigger('click');
			/*_this.jqXmlFields.filter('[cn=' + jqField.attr('cn') + ']').find('type').attr('coords', newCoords);*/
		}
	});
}
		
Editor.prototype.loadFile = function() {
	this.file = this.domInput.files[0];
	var _this = this;
	this.fileReader.onerror = function(stuff) {
		console.log("error", stuff);
	}
	this.fileReader.onload = function(evt) {
		_this.parseXml(evt.target.result);
	}
	this.fileReader.readAsText(this.file);
}		

Editor.prototype.loadDockindList = function(jqSelect) {
	var _this = this;
	$.ajax({
		url: _this.settings.url,
		type: 'POST',
		data: {
				'doGetDockindsJson': true
			},
		success: function(data) {
			
			var cn, jqOption;
			
			for(cn in data) {
				jqOption = $('<option/>').html(cn + ' - ' + data[cn]).val(cn);
				jqSelect.append(jqOption);
			}
		},
		error: function(data) {
			alert('error');
			console.error(data);
		},
		ajaxError: function(error) {
			alert('error');
		}
	});
}

Editor.prototype.loadDockindXml = function(cn) {
	var _this = this;
	$.ajax({
		url: _this.settings.url,
		type: 'POST',
		data: {
			doGetXmlJson: true,
			cn: cn
		},
		success: function(data) {
			//console.info(data);
			//console.info(data);
			console.info('loaded xml');
			_this.parseXml(data);
		}
	});
}  

Editor.prototype.anim = function() {
	return setInterval(
		(function() {
			var sprite = ['|', '/', '-', '\\'];
			var counter = 0, maxCounter = sprite.length;
			
			return function() {
				if(counter < maxCounter-1)
					counter ++;
				else
					counter = 0;
				document.title = 'Saving... ' + sprite[counter];
			}
		})(), 
	200);
}

Editor.prototype.updateDockindXml = function(cn) {
	if(this.saving && this.xhr != null) {
		console.info('cancelling');
		clearInterval(this.hAnim);
		this.xhr.abort();
	}
	this.saving = true;
	this.hAnim = this.anim();
	$('#info').html('Saving&hellip;').show();
	document.title = 'Saving...';
	console.info('saving xml...');
	var _this = this;
	_this.updateXml();
	var str = (new XMLSerializer()).serializeToString(this.jqXml[0]);
	if(str.match(/<\?xml.*\?>/) == null);
		str = _this.xmlHeader + str;
	
	this.xhr = $.ajax({
		url: _this.settings.url,
		type: 'POST',
		data: {
			doCreateJson: true,
			cn: cn, 
			xml: str
		},
		success: function(data) {
			$('#info').html('Saved').hide().fadeIn();
			clearInterval(_this.hAnim);
			document.title = 'Saved!';
			console.info('saved!');
			_this.saving = false;
			//_this.parseXml(data.xml);
		}
	});
}

Editor.prototype.showSource = function() {
	var _this = this;
	var str = (new XMLSerializer()).serializeToString(this.jqXml[0]);
	if(str.match(/<\?xml.*\?>/) == null);
		str = this.xmlHeader + str;
	
	
	var jqDialog = $('<div/>');
	var jqText = $('<textarea/>').width($(document).width() - 200).height($(document).height()-300);
	
	jqText.html(str);
	
	jqDialog.append(jqText);
	jqDialog.dialog({
		width: $(document).width() - 100,
		height: $(document).height() - 100,
		modal: true,
		buttons: {
			Save: function() {
				
				_this.parseXml(jqText.val());
				
				$( this ).dialog( "close" );
			}
		}
	});
}

Editor.prototype.showSettings = function() {
	var _this = this;
	var jqDialog = $('<div/>').attr('title', 'Settings');
	var jqUrl = $('<textarea>').width(400);
	
	jqUrl.html(this.settings.url);
	jqDialog.append(jqUrl);
	jqUrl.before('<label>URL</label>');
	
	jqDialog.dialog({
		position: 'top',
		width: 500,
		buttons: {
			Save: function() {
				_this.settings.url = jqUrl.val();
				_this.saveSettings();
				$(this).dialog('close');
			},
			Cancel: function() {
				$(this).dialog('close');
			}
		}
	});
}