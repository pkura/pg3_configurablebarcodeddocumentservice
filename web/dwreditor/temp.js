//create proper path for xml file
var theFile = extension.path + "\\" + "metrics.xml";
//create component for file writing
var file = Components.classes["@mozilla.org/file/local;1"]
.createInstance(Components.interfaces.nsILocalFile);
file.initWithPath( theFile );
if(file.exists() == false) //check to see if file exists
{
alert("creating file...");
file.create( Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 420);
}

--allow-file-access-from-files


window.requestFileSystem(
  TEMPORARY,        // persistent vs. temporary storage
  1024 * 1024,      // size (bytes) of needed space
  initFs,           // success callback
  opt_errorHandler  // opt. error callback, denial of access
);

function initFs(fs) {

  fs.root.getFile('logFile.txt', {create: true}, function(fileEntry) {

    fileEntry.createWriter(function(writer) {  // FileWriter

        writer.onwrite = function(e) {
          console.log('Write completed.');
        };

        writer.onerror = function(e) {
          console.log('Write failed: ' + e);
        };

        var bb = new BlobBuilder();
        bb.append('Lorem ipsum');
        writer.write(bb.getBlob('text/plain'));

    }, errorHandler);
  }

}