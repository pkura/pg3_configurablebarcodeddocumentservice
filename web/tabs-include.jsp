<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N tabs-include.jsp N-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- tytu� strony i zak�adki -->

<c:if test="${!empty tabs}">

<% if (request.getParameter("notitle") == null) { %>
<h3>
    <c:forEach items="${tabs}" var="tab">
        <c:if test="${tab.selected}"><c:out value="${tab.title}"/></c:if>
    </c:forEach>
</h3>
<% } %>

<p>
    <c:forEach items="${tabs}" var="tab" varStatus="status" >
        <a <c:if test="${tab.selected}">style="color: red;"</c:if> href="<c:out value="${pageContext.request.contextPath}${tab.link}"/>"><c:out value="${tab.title}"/></a>
        <c:if test="${!status.last}"> | </c:if>
    </c:forEach>
</p>
</c:if>
