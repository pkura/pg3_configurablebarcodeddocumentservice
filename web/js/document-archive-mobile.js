(function($){
$(function(){

	// var mobile = !!window.location.href.match(/mobileView=true/);
	
	if(MOBILE_VIEW) {
		// $("#middleMenuContainer").remove();
		// $("#templateKind").remove();
		// $("input[name='doCreateDocument']").remove();
		// $("input[name='doUpdateGoToTasklist']").remove();

		$("a").removeAttr("href");

		var mobileRedirect = $("#mobileRedirectInput");

		if(mobileRedirect.length > 0) {
			var mobileRedirectValue = mobileRedirect.val();
			console.log("mobileRedirectValue -->",mobileRedirectValue);
			if(mobileRedirectValue) {
				setTimeout(function(){
					redirect(mobileRedirect.val());
				},1000);
			}
		}

		$(".mobile-return").on("click", function(){
			redirect();
		});

		function redirect(dsRedirect) {
			var mobileReferrer = $("#mobileReferrerInput");

			if(mobileReferrer.length > 0) {
				var url = decodeURIComponent(mobileReferrer.val());
				if(dsRedirect) {
					window.location.href = url+(url.match(/\?/) ? "&" : "?")+"dsRedirect="+dsRedirect
				} else {
					window.location.href = url;
				}
			} else {
				console.log("ERROR cannot redirect to mobile, no referrer");
			}
			
		}

		// var form = $("#form");

		// form.on("submit", function(event){

		// 	console.log("submit -->", event.target);

		// 	event.preventDefault();
		// 	return false;

		// });

		// window.onbeforeunload = function(){
		//     return 'Are you sure you want to leave?';
		// };
	}

});
}(jQuery));