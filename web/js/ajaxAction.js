var ajaxActionFactory = function($http) {

	var ajaxAction = {};

	(function($){

		var config = {
			"actionMessageSelector": "#messageList",
			"actionErrorSelector": "#errorList",
			"actionMessagesWrapperSelector": "#messages",
			"debug": true
		}

		var DEBUG = config["debug"];

		// later assigned
		var httpGet = function() {};

		if(! console) {
			console = {
				error: function() {},
				log: function() {}
			};
		}

		// initialize messages
		var initialized = false;
		var init = function() {
			var messagesWrapper = $(config["actionMessagesWrapperSelector"]);

			var errorList = messagesWrapper.find("#errorList");
			if(errorList.length === 0)
			{
				errorList = $('<ul id="errorList" style="color: red;" class="msgList_"/>').appendTo(messagesWrapper);
			}

			var messageList = messagesWrapper.find("#messageList");
			if(messageList.length === 0)
			{
				messageList = $('<ul id="messageList" class="msgList_"/>').appendTo(messagesWrapper);
			}

			initialized = true;
		}

		function msgContainer(callback, isError) {
			callback = callback || function() {};
			
			$(function(){

				if(! initialized)
				{
					init();
				}

				var c;
				if(isError)
				{
					c = msgContainer.errorContainer || (msgContainer.errorContainer = $(config["actionErrorSelector"]));
				}
				else
				{
					c = msgContainer.messageContainer || (msgContainer.messageContainer = $(config["actionMessageSelector"]));
				}
				callback(c);
			});
		}

		var addActionMessage = ajaxAction.addActionMessage = function(message, isError) {
			msgContainer(function(container){
				$("<li/>").text(message).appendTo(container);
			}, isError);
		}

		var addActionError = ajaxAction.addActionError = function(message) {
			if(DEBUG) message += " [js]";
			return addActionMessage(message, true);
		}



		function handleMessages(messages, isError) {
			if(_.isArray(messages)) {
				_.each(messages, function(msg){
					addActionMessage(msg, isError);
				})
			}
		}

		ajaxAction.post = function(name, queryParams, postData) {
			return ajaxAction.get(name, queryParams, postData || {});
		}

		ajaxAction.get = function(name, queryParams, postData) {

			queryParams = queryParams || {};

			var dfd = $.Deferred();

			var url = URI(window.location.href);

			var q = {};
			q[name] = "true";
			url.query(q);

			var ajax, method;

			url.addQuery(queryParams);
			if(! postData) {
				method = "GET";
				ajax = httpGet({
					params: queryParams,
					url: url.toString()
				});
			} else {
				method = "POST";
				ajax = httpGet({
					url: url.toString(),
					params: queryParams,
					data: postData,
					post: true
				});
			}

			if(DEBUG) console.log(method+" "+url.toString());

			ajax.done(function(result){

				if(DEBUG) console.log(method+" "+url.toString()+" -->", result);

				if(_.isObject(result))
				{
					handleMessages(result["errors"], true);
					handleMessages(result["messages"]);

					if(result["failed"] === true)
					{
						dfd.reject("error", result["result"]);
					}
					else
					{
						dfd.resolve(result["result"]);
					}
				}
				else
				{
					var err = "result is not an object, name = "+name+", url = "+method+" "+url.toString();
					console.error(err);
					addActionError("Nie mo�na odczyta� wyniku");

					dfd.reject(err);
				}

			}).fail(function(errorMessage){
				console.error(errorMessage);
				addActionError(errorMessage);
				dfd.reject(errorMessage);
			});

			return dfd;
		}

		// wrapper for jquery $.ajax, or angular $http, or other client-server communication library
		// httpGet must return deferred object as specified:
		//    done(result)
		//    fail(errorMessage)
		if(! $http) {
			httpGet = function(options) {
				if(! options) { throw "No options"; }

				var dfd = $.Deferred();

				var ajaxOptions = {};

				if(options.data) ajaxOptions.data = options.data;
				// if(options.params) ajaxOptions.data = options.params;
				if(options.post) ajaxOptions.type = "POST";

				var ajax = $.ajax(options.url, ajaxOptions);

				ajax.done(function(data){
					dfd.resolve(data);
				});

				ajax.fail(function(jqXHR, statusText, error){
					dfd.reject(error);
				});

				return dfd;
			}
		} else {
			httpGet = function(options) {
				if(! options) { throw "No options"; }

				var dfd = $.Deferred();

				var angularOptions = {};

				angularOptions.url = options.url;
				if(options.post) {
					angularOptions.method = "POST";

					angularOptions.headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=ISO-8859-2', 'X-Requested-With': 'XMLHttpRequest'};

					// if(options.data) angularOptions.data = options.data;

					if(options.data) {
						var data = _.map(options.data, function(v, k) {
							return k+"="+encodeURIComponent(v).replace(/%20/g,'+').replace(/%3D/g,'=');
						}).join("&");

						// data = data.replace('%20','+').replace('%3D','=');
						angularOptions.data = data;
					}
					// if(options.data) angularOptions.data = options.data;
					// if(options.params) angularOptions.params = options.params;
				} else {
					angularOptions.method = "GET";
					// if(options.params) angularOptions.params = options.params;
				}

				var http = $http(angularOptions);

				http.success(function(data, status, headers, config){
					dfd.resolve(data);
				});

				http.error(function(data, status, headers, config){
					dfd.reject(data);
				});

				return dfd;
			}
		}

	}($j));

	return ajaxAction;
};
