(function($){

	$.ajax(APPLICATION_URL+"/api/documentconverter/extensions").done(function(extensions){
		$(function(){
			$(".documentconverter").each(function(){
				var jCell = $(this);
				var jSelect = jCell.find("select");
				var revisionId = jCell.data().revisionId;
				var convertUrl = APPLICATION_URL+"/api/attachmentrevision/"+revisionId;
				jCell.data().convertUrl = convertUrl;

				$.each(extensions, function(i, ext){
					var jOption = $("<option/>").val(ext).text(ext);
					jOption.appendTo(jSelect);
				});
			})

			$(".documentconverter").on("click", "button", function(event){
				event.preventDefault();

				var jTarget = $(event.target);
				var parent = jTarget.parent(".documentconverter");
				var revisionId = parent.data().revisionId;
				var targetExt = parent.find("select").val();

				var finalUrl = parent.data().convertUrl+"/"+targetExt;
				console.log("url ->", finalUrl);

				window.location.href = finalUrl;

				return false;
			});
		});
	});

}($j));