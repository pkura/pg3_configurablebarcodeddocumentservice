<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<h1><ww:property value="actualType.name" /></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />


<ds:available test="!layout2">
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</ds:available>


<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/news/show-news.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<ww:if test="notifications != null && notifications.size() > 0">
<div class="msgContainer">
<ww:iterator value="notifications" >
	<ww:set name="msgCssClass" value="periodic ? 'msg msg-periodic' : 'msg' "/>
	
	<div class="<ww:property value="#msgCssClass"/>" 
		onclick="javascript:document.location.href='<ww:url  value="/news/show-news.action" >
				<ww:param name="'doAccept'" value="'true'" />
				<ww:param name="'notificationId'" value="id" />
				<ww:param name="'przekieruj'" value="'true'" />
			</ww:url>'" >
		<span class="content">
			<a>
				<ww:property value="content" />
			</a>
		</span>

		<span class="selectRead">
			<a title="oznacz jako przeczytane" href="<ww:url value="/news/show-news.action" >
				<ww:param name="'doAccept'" value="'true'" />
				<ww:param name="'notificationId'" value="id" />
				<ww:param name="'przekieruj'" value="'false'"  />
			</ww:url>">
				oznacz jako przeczytane
			</a>
		</span>
	</div>
</ww:iterator>
<div class="clearBoth"></div>
</div>
</ww:if>

<div class="newsContainer">
	<ww:iterator value="newsList" status="status">
	<div class="news">
		<H2><ww:property value="subject" /></H2>
		<span class="info firstInfo"><ww:property value="user.asFirstnameLastnameName()" /></span>
		<span class="info"><ww:property value="ctime" /></span>
		<ds:has-permission name="NEWS_DODAWANIE">
			<span class="info edit">
				<a href="<ww:url value="'/news/edit-news.action'" >
								<ww:param name="'id'" value="id" ></ww:param>
							</ww:url>" >
							<ds:lang text="ModifyNews" />
				</a>
			</span>
			<span class="info delete">
					<a href="<ww:url value="'/news/show-news.action'" >
						<ww:param name="'doDelete'" value="'true'" ></ww:param>
						<ww:param name="'id'" value="id" ></ww:param>
						<ww:param name="'newsType'" value="newsType.id" ></ww:param>
					</ww:url>" > <ds:lang text="DeleteNews"/> </a>
			</span>
		</ds:has-permission>

		<div class="content contentCollapsed" id="news_<ww:property value="#status.index" />" >
			<ww:property escape="false" value="description" />
		</div>
		<a href="#" class="contentExpand down">wi�cej</a>

		<script type="text/javascript">
		/*$j('#news_<ww:property value="#status.index" />').expander({
			slicePoint: 500,
			expandText: 'wi�cej',
			userCollapseText: 'mniej'
		});*/
		</script>
		<div class="clearBoth"></div>
	</div>
	</ww:iterator>
		<ww:set name="pager" scope="request" value="pager"/>
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div>
	
	<script type="text/javascript">
		$j('a.contentExpand').click(function() {
			var jqThis = $j(this);
			jqThis.prev('div.content').toggleClass('contentCollapsed');
			if(jqThis.hasClass('down')) {
				jqThis.html('mniej').removeClass('down');
			} else {
				jqThis.html('wi�cej').addClass('down');
			}
		});
	
		$j('.edit').hover(function() {
			$j(this).nextAll('div.content').addClass('contentEdit');
		}, function() {
			$j(this).nextAll('div.content').removeClass('contentEdit');
		});

		$j('.delete').hover(function() {
			$j(this).nextAll('div.content').addClass('contentDelete');
		}, function() {
			$j(this).nextAll('div.content').removeClass('contentDelete');
		});
	</script>
</div>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>