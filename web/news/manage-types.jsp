<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1> <ds:lang text="ManageTypes"/> </h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="ManageTypesNews"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="ManageTypesNews"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form id="form" action="<ww:url value="'/news/manage-types.action'"/>" enctype="application/x-www-form-urlencoded" method="post">


<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<table class="search mediumTable">
<thead>
<tr>
		<th>
			<ds:lang text="id"/>
		</th>
		<th>
			<ds:lang text="name"/>
		</th>
		<th>
			<ds:lang text="ctime"/>
		</th>
		<th>
			<ds:lang text="disabled"/>
		</th>
</tr>
</thead>
<tbody>
<ww:iterator value="types" >
	<tr>
		<td>
			<a href="<ww:url value="'/news/manage-types-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">
				<ww:property value="id"/></a>
		</td>
		<td>
			<a href="<ww:url value="'/news/manage-types-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">
				<ww:property value="name"/></a>
		</td>
		<td>
			<a href="<ww:url value="'/news/manage-types-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">
				<ww:property value="ctime"/></a>
		</td>
		<td>
			<ww:checkbox name="disabled" fieldValue="disabled" disabled="true" ></ww:checkbox>
		</td>
	</tr>
</ww:iterator>
</tbody>
</table>
<table>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><ds:lang text="NewTab"/>:</td>
		<td>
			<ww:textfield name="'newType'" />
		</td>
	<td colspan="2" >
		<ds:event name="'doAdd'" value="'Dodaj'" ></ds:event>
	</td>
	</tr>
	<tr>
		<td><ds:lang text="PostNumber"/>:</td>
		<td>
			<ww:select value="postNumber" name="'postNumber'" cssClass="'sel'" list="postNumbers" />
		</td>
	<td colspan="2" >
		<ds:event name="'doChange'" value="'Aktualizuj'" ></ds:event>
	</td>
	</tr>
</table>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>