<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<!-- CKEDITOR -->
<script type="text/javascript" src="<ww:url value="'/ckeditor/ckeditor.js'"/>"></script>

<h1><ds:lang text="EditNews"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="EditNews"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="EditNews"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/news/edit-news.action'"/>" enctype="application/x-www-form-urlencoded" method="post">
	<ww:hidden name="'id'" id="id" ></ww:hidden>


<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
	<table>
		<tr>
			<td><ds:lang text="SelectType" /></td>
			<td><ww:select id="selectedType" name="'selectedType'" cssClass="'sel'" list="types" /></td>
		</tr>
		<tr>
			<td><ds:lang text="Title" /></td>
			<td><ww:textfield name="'subject'" id="subject" cssClass="txt" size="220"  ></ww:textfield> </td>
		</tr>
		 <tr>
            <td><ds:lang text="Content"/>: </td>
            <td><ww:textarea name="'description'" id="description" cols="60" rows="20" cssClass="'txt'"/></td>
        </tr>

		<tr>
			<td><ds:event name="'doSave'" value="'Zapisz'" ></ds:event>  </td>
		</tr>
	</table>

<!-- CKEDITOR -->
<script type="text/javascript">
//<![CDATA[

	CKEDITOR.replace( 'description',
		{
			fullPage : false
		});

//]]>
</script>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>