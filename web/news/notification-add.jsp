<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>

<!-- CKEDITOR -->
<script type="text/javascript" src="<ww:url value="'/ckeditor/ckeditor.js'"/>"></script>

<h1><ds:lang text="Dodawanie powiadomie�"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="AddNews"/>
</ds:available>



<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/news/notification-add.action'"/>" enctype="application/x-www-form-urlencoded" method="post">


<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
	<table>
		<ww:if test="hasPermission">
		<tr>
			<td>
				<ds:lang text="User"/>
			</td>
			<td>
				<ww:select name="'usersIds'" cssClass="'multi_sel'" cssStyle="'width: 300px; height: 300px;'"  headerKey="''" multiple="true"   list="users" listKey="id" listValue="asLastnameFirstname()"/>
			</td>
			<td>
				<ds:lang text="Dzia�"/>
			</td>
			<td>
				<ww:select name="'divisionsGuids'" cssClass="'multi_sel'" cssStyle="'width: 300px; height: 300px;'"  headerKey="''" multiple="true"   list="divisions" listKey="guid" listValue="getName()"/>
			</td>
		</tr>
		</ww:if>
	</table>
	<table>
		<tr>
			<td>
				<ds:lang text="Data powiadomienia:" />
			</td>
			<td>
			<ww:textfield name="'stringDate'" id="stringDate" cssClass="'txt'"/>
		</td>
		</tr>
		<ww:if test="documentId != null">
			<tr>
			<td>
				<ds:lang text="Powiadomienie dotyczy dokumentu:" />
			</td>
			<td>
			    <a title="<ds:lang text="Przejd� do dokumentu"/>"
                                     				href="
                                     					<ww:url value="documentLink">
                                     					</ww:url>">
                                                                <ww:label name="'documentId'" id="documentId" value="documentId" cssClass="'txt'"/></a>
                                                                <ww:hidden name="'documentId'" value="documentId"/>
			</td>
			</tr>
		</ww:if>
	</table>
	<table>
		<tr>
            <td><ds:lang text="Content"/>: </td>
            <td><ww:textarea name="'content'" id="content" cols="60" rows="20" cssClass="'txt'"/></td>
        </tr>
		<tr>
			<td><ds:event name="'addNotification'" value="'Dodaj'" ></ds:event>  </td>
		</tr>
	</table>

<!-- CKEDITOR -->
<!-- <script type="text/javascript">
//<![CDATA[

	CKEDITOR.replace( 'content',
		{
			fullPage : false
		});

//]]>
</script> -->

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>
<script type="text/javascript">


function clearProjectEntry() {
		$j('#ProjectEntry').children('tbody').children('tr:not(.buttonsRow)').each(function() {
			$j(this).children('td').children('input, select, textarea').val('');
		});
		$j('#entryId').val('');
	}
	if($j('#stringDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "stringDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsDateTimeFormat %>",      // format of the input field
	        button         :    "notificationDateTriger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	    
	    
	}
</script>

<script type="text/javascript">

$j('#stringDate').datetimepicker();


</script>