<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>

<!-- CKEDITOR -->
<script type="text/javascript" src="<ww:url value="'/ckeditor/ckeditor.js'"/>"></script>

<h1><ds:lang text="EditNews"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="EditNews"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="EditNews"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/news/notification-edit.action'"/>" enctype="application/x-www-form-urlencoded" method="post">
	<ww:hidden name="'id'" id="id" ></ww:hidden>


<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
	<table>
	<tr>
		<tr>
			<td>
				<ds:lang text="Data powiadomienia:" />
			</td>
			<td>
			    <ww:if test="cantEdit">
				    <ww:textfield name="'stringDate'" id="stringDate"  value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)" size="16" maxlength="16" cssClass="'txt'"/>
		        </ww:if>
		        <ww:else>
		            <ww:textfield name="'stringDate'" id="stringDate"  value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)" size="16" maxlength="16" cssClass="'txt'"/>
		        </ww:else>
			</td>
		</tr>
		<ww:if test="documentId != null">
			<tr>
				<td>
					<ds:lang text="Powiadomienie dotyczy dokumentu:" />
				</td>
				<td>
					<a title="<ds:lang text="Przejd� do dokumentu"/>"
                                                         				href="
                                                         					<ww:url value="documentLink">
                                                         					</ww:url>">
                                                                                    <ww:label name="'documentId'" id="documentId" value="documentId" cssClass="'txt'"/></a>
                                                                                    <ww:hidden name="'documentId'" value="documentId"/>
				</td>
			</tr>
		</ww:if>
		<tr>
            <td>
            	<ds:lang text="Content"/>
            </td>
            <td>
            <ww:if test="cantEdit">
                <ww:textarea name="'content'" id="content" cols="60" disabled="true" rows="20" cssClass="'txt'"/>
            </ww:if>
            <ww:else>
                <ww:textarea name="'content'" id="content" cols="60" rows="20" cssClass="'txt'"/>
            </ww:else>
            </td>
        </tr>
		<tr>
			<td>
				 <ww:if test="!cantEdit">  <ds:event name="'doUpdate'" value="'Aktualizuj'" ></ds:event>
				 </ww:if>
			</td>
		</tr>
	</table>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>

<script type="text/javascript">


function clearProjectEntry() {
		$j('#ProjectEntry').children('tbody').children('tr:not(.buttonsRow)').each(function() {
			$j(this).children('td').children('input, select, textarea').val('');
		});
		$j('#entryId').val('');
	}
	if($j('#notificationDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "notificationDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsDateTimeFormat %>",      // format of the input field
	        button         :    "notificationDateTriger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	    
	    
	}
</script>

<script type="text/javascript">

$j('#notificationDate').datetimepicker();


</script>