<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1> <ds:lang text="ManageTypes"/> </h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
		<a href="/news/manage-types-edit.action" title="<ds:lang text="ManageTypes" />" class="highlightedText"><ds:lang text="ManageTypes" /></a>
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
</ds:available>


<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
		<a href="<ww:url value='/news/manage-types-edit.action'/>" title="<ds:lang text="ManageTypes" />" class="highlightedText"><ds:lang text="ManageTypes" /></a>
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/news/manage-types-edit.action'"/>" enctype="application/x-www-form-urlencoded" method="post">
	<ww:hidden name="'id'" value="id" ></ww:hidden>

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<table>
	<tr>
		<td>
			<ds:lang text="id"/>
		</td>
		<td>
			<ww:property value="id" />
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="name"/>
		</td>
		<td>
			<ww:textfield name="'name'" value="name" />
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="ctime"/>
		</td>
		<td>
			<ww:property value="newsType.ctime" />
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="disabled"/>
		</td>
		<td>
			<ww:checkbox name="'disableTab'" fieldValue="true" value="disableTab" />
		</td>
	</tr>
<tr>
	<td colspan="4" >
		<ds:event name="'doUpdate'" value="'Zapisz'" ></ds:event>
	</td>
</tr>

</table>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>