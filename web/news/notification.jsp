<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<b><ww:property value="actualType.name" /></b>

<hr size="1" align="left" class="horizontalLine" width="77%" />


<ds:available test="!layout2">
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</ds:available>


<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/news/notification.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<h2><ds:lang text="otrzymanePowiadomienia"/></h2>
<div class="newsContainer">
	<table class="search" width="100%" cellspacing="0" id="mainTable">
	<tr>
		<th>
			<ds:lang text="Lp."/>
		</th>
		<th>
			<ds:lang text="Autor"/>
		</th>
		<th>
		<a title="<ds:lang text="SortowanieMalejace"/>"
                         				href="
                         					<ww:url value="'/news/notification.action?sortField=notificationDate&ascending='+false">
                         					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Data powiadomienia"/>
			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                         					<ww:url value="'/news/notification.action?sortField=notificationDate&ascending='+true">
                         					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
		 <a title="<ds:lang text="SortowanieMalejace"/>"
                 				href="
                 					<ww:url value="'/news/notification.action?sortField=ctime&ascending='+false">
                 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Data utworzenia"/>
			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                         					<ww:url value="'/news/notification.action?sortField=ctime&ascending='+true">
                         					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
		<a title="<ds:lang text="SortowanieMalejace"/>"
                         				href="
                         					<ww:url value="'/news/notification.action?sortField=content&ascending='+false">
                         					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Tre��"/>
			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                                     					<ww:url value="'/news/notification.action?sortField=content&ascending='+true">
                                     					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<ds:lang text="Usuni�cie"/>
		</th>
	</tr>

	<ww:iterator value="recivedNotifications" status="status">
	<ww:if test="!recivedNotifications.get(#status.index).isRead()">
	<tr>
		<td>
			<span class="info">

			<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                			        <ww:param name="'doReadAndEdit'" value="'true'" />
                                                    <ww:param name="'id'" value="id" ></ww:param>
                                                    <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                    <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><b><ww:property value="#status.index + 1 + '.'" /></b></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                        			        <ww:param name="'doReadAndEdit'" value="'true'" />
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><b><ww:property value="creator.getLastname() + ' ' + creator.getFirstname()" /><b></b></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                        			        <ww:param name="'doReadAndEdit'" value="'true'" />
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><b><ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)" /></b></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                        			        <ww:param name="'doReadAndEdit'" value="'true'" />
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><b><ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(ctime)" /></b></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                        			        <ww:param name="'doReadAndEdit'" value="'true'" />
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><b><ww:property value="getContentSub10()" /></b></a>
			</span>
		</td>
		<td>
			<span class="info delete">
					<a href="<ww:url value="'/news/notification.action'" >
						<ww:param name="'doDeleteChild'" value="'true'" ></ww:param>
						<ww:param name="'id'" value="id" ></ww:param>
					</ww:url>" > <ds:lang text="Usu�"/> </a>
			</span>
		</td>
	</tr>
	</ww:if>
	<ww:else>
	<tr>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                        <ww:param name="'id'" value="id" ></ww:param>
                                        <ww:param name="'documentId'" value="documentId" ></ww:param>
                                        <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><ww:property value="#status.index + 1 + '.'" /></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><ww:property value="user.getLastname() + ' ' + user.getFirstname()" /></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)" /></a>
			</span>
		</td>
		<td>
			<span class="info">
					<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                                            <ww:param name="'id'" value="id" ></ww:param>
                                                            <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                            <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(ctime)" /></a>
			</span>
		</td>
		<td>
			<a  title="Podgl�d" href="<ww:url value="'/news/notification-edit.action'" >
                                                    <ww:param name="'id'" value="id" ></ww:param>
                                                    <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                    <ww:param name="'cantEdit'" value="cantEdit" ></ww:param>
					</ww:url>" ><ww:property value="getContentSub10()" /></a>
			</span>
		</td>
		<td>
			<span class="info delete">
					<a href="<ww:url value="'/news/notification.action'" >
						<ww:param name="'doDelete'" value="'true'" ></ww:param>
						<ww:param name="'id'" value="id" ></ww:param>
					</ww:url>" > <ds:lang text="Usu�"/> </a>
			</span>
		</td>
	</tr>
	</ww:else>
	</ww:iterator>
	</table>
		<%-- <ww:set name="pager" scope="request" value="pager"/>
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div> --%>

	<script type="text/javascript">
		$j('.edit').hover(function() {
			$j(this).nextAll('div.content').addClass('contentEdit');
		}, function() {
			$j(this).nextAll('div.content').removeClass('contentEdit');
		});

		$j('.delete').hover(function() {
			$j(this).nextAll('div.content').addClass('contentDelete');
		}, function() {
			$j(this).nextAll('div.content').removeClass('contentDelete');
		});
	</script>
</div>
<!-- -------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------  -->
<h2><ds:lang text="powiadomieniaUzytkownika"/></h2>
<div class="newsContainer">
	<table class="search" width="100%" cellspacing="0" id="mainTable">
	<tr>
    		<th>
    			<ds:lang text="Lp."/>
    		</th>
    		<th width="20%">
    			<ds:lang text="Dotyczy"/>
    		</th>
    		<th>
    		<a title="<ds:lang text="SortowanieMalejace"/>"
                             				href="
                             					<ww:url value="'/news/notification.action?sortField=notificationDate&ascending='+false">
                             					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
    			<ds:lang text="Data powiadomienia"/>
    			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                             					<ww:url value="'/news/notification.action?sortField=notificationDate&ascending='+true">
                             					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
    		</th>
    		<th>
    		 <a title="<ds:lang text="SortowanieMalejace"/>"
                     				href="
                     					<ww:url value="'/news/notification.action?sortField=ctime&ascending='+false">
                     					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
    			<ds:lang text="Data utworzenia"/>
    			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                             					<ww:url value="'/news/notification.action?sortField=ctime&ascending='+true">
                             					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
    		</th>
    		<th>
    		<a title="<ds:lang text="SortowanieMalejace"/>"
                             				href="
                             					<ww:url value="'/news/notification.action?sortField=content&ascending='+false">
                             					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
    			<ds:lang text="Tre��"/>
    			<a title="<ds:lang text="SortowanieRosnace"/>" href="
                                         					<ww:url value="'/news/notification.action?sortField=content&ascending='+true">
                                         					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="40" height="11" border="0"/></a>
    		</th>
    		<th>
    			<ds:lang text="Usuni�cie"/>
    		</th>
</tr>
	<ww:iterator value="ownNotifications" status="status">

	<tr>
		<td>
			<a title="Edytuj" href="<ww:url value="'/news/notification-edit.action'" >
                        						<ww:param name="'id'" value="id" ></ww:param>
                        						<ww:param name="'documentId'" value="documentId" ></ww:param>
                        							</ww:url>" >
<ww:property value="#status.index + 1 + '.'"/>
</a>
		</td>
        <td width="30%">
        <a title="Edytuj" href="<ww:url value="'/news/notification-edit.action'" >
                          						<ww:param name="'id'" value="id" ></ww:param>
                                                <ww:param name="'documentId'" value="documentId" ></ww:param>
                                                </ww:url>" >
                        <ww:property value="getTargetUsers()" />
						</a>
        </td>
		<td>
			<a title="Edytuj" href="<ww:url value="'/news/notification-edit.action'" >
                        						<ww:param name="'id'" value="id" ></ww:param>
                        						<ww:param name="'documentId'" value="documentId" ></ww:param>
                        							</ww:url>" >
                <ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)" />
                </a>
		</td>
		<td>
			<a title="Edytuj" href="<ww:url value="'/news/notification-edit.action'" >
                        						<ww:param name="'id'" value="id" ></ww:param>
                        						<ww:param name="'documentId'" value="documentId" ></ww:param>
                        							</ww:url>" >
               <ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(ctime)" />
               </a>
		</td>
		<td>
			<a title="Edytuj" href="<ww:url value="'/news/notification-edit.action'" >
            						<ww:param name="'id'" value="id" ></ww:param>
            						<ww:param name="'documentId'" value="documentId" ></ww:param>
            							</ww:url>" >
                <ww:property value="getContentSub10()" />
                </a>
		</td>
		<td>
			<span class="info delete">
					<a  title="Usu�" href="<ww:url value="'/news/notification.action'" >
						<ww:param name="'doDelete'" value="'true'" ></ww:param>
						<ww:param name="'id'" value="id" ></ww:param>
					</ww:url>" > <ds:lang text="Usu�"/> </a>
			</span>
		</td>
	</tr>
	</ww:iterator>
	</table>
		<%-- <ww:set name="pager" scope="request" value="pager"/>
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div> --%>


	</div>





<!-- Nieprzeczytane powiadomienia (je�eli przekrocz� dat�)-->
<%-- <ww:if test="actualNotifications != null && actualNotifications.size() > 0">
<h2><ds:lang text="Aktualne powiadomienia"/></h2>
<div class="msgContainer">

<ww:iterator value="actualNotifications" >
	<ww:set name="msgCssClass" value="'msg msg-periodic'"/>
	<div class="<ww:property value="#msgCssClass"/>"
		onclick="javascript:document.location.href='<ww:url  value="link" >
			</ww:url>'" >
		<span class="content contentCollapsed">
			<a>
				<ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)"/>
				<ww:property value="content" />
			</a>
		</span>

		<span class="selectRead">
			<a title="oznacz jako przeczytane" href="<ww:url value="/news/notification.action" >
				<ww:param name="'doRead'" value="'true'" />
				<ww:param name="'notificationId'" value="id" />
				<ww:param name="'read'" value="'true'"  />
			</ww:url>">
				oznacz jako przeczytane
			</a>
		</span>
	</div>
</ww:iterator>
<div class="clearBoth"></div>
</div>
</ww:if> --%>

<!-- Nieprzeczytane powiadomienia (je�eli przekrocz� dat�)-->
<%-- <ww:if test="newNotifications != null && newNotifications.size() > 0">
<h2><ds:lang text="Nowe powiadomienia"/></h2>
<div class="msgContainer">
<ww:iterator value="newNotifications" >
	<ww:set name="msgCssClass" value="'msg'"/>
	<div class="<ww:property value="#msgCssClass"/>"
		onclick="javascript:document.location.href='<ww:url  value="link" >
			</ww:url>'" >
		<span class="content contentCollapsed">
			<a>
				<ww:property value="@pl.compan.docusafe.core.news.Notification@formatDate(notificationDate)"/>
				<ww:property value="content" />
			</a>
		</span>
		<span class="selectRead">
			<a title="oznacz jako przeczytane" href="<ww:url value="/news/notification.action" >
				<ww:param name="'doReadNew'" value="'true'" />
				<ww:param name="'notificationId'" value="id" />
				<ww:param name="'newNotification'" value="'false'" />
			</ww:url>">
				oznacz jako przeczytane
			</a>
		</span>
	</div>
</ww:iterator>
<div class="clearBoth"></div>
</div>
</ww:if> --%>



<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>