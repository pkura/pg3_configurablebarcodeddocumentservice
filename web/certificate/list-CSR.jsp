<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N list-CSR.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1>Certyfikaty</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<b>ustawienia podpisu</b>
<a href="<ww:url value="'/certificate/admin/settings.action'"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> 
<br><br>
<table class="search" width="100%" cellspacing="0">
  <tr>
    <ww:iterator value="columns" status="status" >
        <th><nobr><ww:property value="title"/></th>
        <th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
    </ww:iterator>
    <th>&nbsp;</th>
  </tr>
  <ww:iterator value="listCSR">
    <tr>
      <ww:set name="result"/>
      <ww:iterator value="columns" status="status">
        <td><a href="<ww:url value="#result['linkinfo']"/>"><ww:property value="#result[property]"/></a></td>
        <td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
      </ww:iterator>
      <td><a href="<ww:url value="#result['link']"/>">usun</a></td>
     </tr>
  </ww:iterator>
</table>
