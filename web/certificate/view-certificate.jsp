<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N view-certificate.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Dane certyfikatu dla aliasu <ww:property value='alias'/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<center>
  <applet code="pl.compan.docusafe.applets.certificate.CertApplet" 
          archive="<ww:url value="'/certificates.jar'"/>, <ww:url value="'/bcprov-jdk12-127.jar'"/>"
          width=300
          height=10>
  </applet>
</center>

<form id="form" action="<ww:url value="'/certificate/index.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'encoded'" value="encoded"/>
<script>
  var applet = document.applets[0];
  var alias  = "<ww:property value='alias'/>";
  var started = false;
  var attempts = 5;

  while (started==false && attempts-- > 0)
  {
    try
    {
      applet.isActive();
      started =true;
    }
    catch(e)
    {
      alert("Wyswietlanie chwilowo wstrzymane - \n OK aby probowac ponownie");
    }
  }
  signed = applet.isSigned(alias);
  function print(s)
  {
    document.write(s);
  }
  function printCond(s)
  {
    if (signed)
      document.write(s);
  }

</script>
<table width="100%" border="0">
    <tr>
      <th width="30%">
        &nbsp;
      </td>
      <th width="35%" >
        w�a�ciciel
      </td>
      <th width="35%" >
        <script>printCond("podpisuj�cy");</script>
      </td>
    </tr>
    <tr>
        <td>imi� i nazwisko:</td>
        <td><script>print(applet.getSubjectValue(alias,'CN'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'CN'));</script></td>
    </tr>
    <tr>
        <td>jednostka organizacyjna:</td>
        <td><script>print(applet.getSubjectValue(alias,'OU'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'OU'));</script></td>
    </tr>
    <tr>
        <td>organizacja:</td>
        <td><script>print(applet.getSubjectValue(alias,'O'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'O'));</script></td>
    </tr>
    <tr>
        <td>lokalizacja:</td>
        <td><script>print(applet.getSubjectValue(alias,'L'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'L'));</script></td>
    </tr>
    <tr>
        <td>jednostka terytorialna:</td>
        <td><script>print(applet.getSubjectValue(alias,'ST'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'ST'));</script></td>
    </tr>
    <tr>
        <td>skr�t pa�stwa:</td>
        <td><script>print(applet.getSubjectValue(alias,'C'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'C'));</script></td>
    </tr>
    <tr>
        <td>e-mail:</td>
        <td><script>print(applet.getSubjectValue(alias,'emailaddress'));</script></td>
        <td><script>printCond(applet.getIssuerValue(alias,'emailaddress'));</script></td>
    </tr>
    <tr>
        <td colspan="3"><p align="left"><ds:submit-event value="'Powr�t'" name="'default'" /></p></td>
    </tr>
</table>

</form>
<!--
        document.write("<td><a href='#' onclick='export2pfx(\""+alias+"\")'> eksportuj</a></td>");
-->