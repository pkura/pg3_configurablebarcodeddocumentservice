<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N import-CSR.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1>Certyfikat zosta� zaakceptowany</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<center>
  <applet code="pl.compan.docusafe.applets.certificate.CertApplet" 
          archive="<ww:url value="'/certificates.jar'"/>, <ww:url value="'/bcprov-jdk12-127.jar'"/>"
          width=300
          height=10>
  </applet>
</center>
<script>
  function validated()
  {
     if (document.forms[0].password.value=='')
     {
      alert("prosz� poda� has�o");
      return false;
     }
     else
      return true;
  }
  function generateCert()
  {
    _form = document.forms[0];
    ret =document.applets[0].importSignedCSR(_form.encoded.value,_form.alias.value,_form.password.value);
    if (!isNaN(ret))
    {
      ret = "niewlasciwe alias i/lub haslo ";;
      alert("wystapil blad w trakcjie genrowania CSR - "+ret);
      return false
    }
    else
    {
       alert("zrobione");
       return true;
    }  
  }
</script>
Certfikat zosta� zaakceptowany.<br>
Prosz� poda� has�o kt�re zosta�o u�yte w trakcie generowania certfikatu. 
<form id="form" action="<ww:url value="'/certificate/index.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
   <ww:hidden name="'encoded'" value="encoded"/>
   <ww:hidden name="'alias'"  value="alias"/>

<table>
    <tr>
        <td>has�o:</td>
        <td><ww:textfield name="'password'" value="password" size="10" maxlength="10" cssClass="'txt'" /></td>
    </tr>

    <tr>
        <td>
          <input type="submit" name="doCreate" value="Wyslij" class="btn" 
                 onclick="if (validated()) return generateCert(); else return false;"/>
        </td>
    </tr>
</table></table>
</form>