<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N export-PFX.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1>Eksport do pliku PFX</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<center>
  <applet code="pl.compan.docusafe.applets.certificate.CertApplet" 
          archive="<ww:url value="'/certificates.jar'"/>, <ww:url value="'/bcprov-jdk12-127.jar'"/>"
          width=300
          height=10>
  </applet>
</center>
<script>
  function validated()
  {
     if (document.forms[0].password.value=='')
     {
      alert("prosz� poda� has�o");
      return false;
     }
     else
      return true;
  }
  function export2pfx()
  {
    pass   = document.forms[0].password.value;
    alias  = document.forms[0].alias.value;
    ret  = document.applets[0].export2pfx(alias,pass);
    if (isNaN(ret))
      alert("Zrobiono. Plik znajduje sie w \n "+ret);
    else
      if (ret==109) alert("Wygenerowanie pliku pfx jest niemozliwe. \n Zaden dostawca uslug kryptograficznych nie udostepnia tej uslugi"); 
      else   alert("Wygenerowanie nie powiodlo sie. \n Prawdopodobie haslo jest niewlasciwe ("+ret+")"); 

  }
      applet = document.applets[0];
      started =false;
      count =0;
      while (started==false)
      {
        try
        {
          applet.isActive();
          started =true;
        }
        catch(e)
        {
          count++;
          if (count==5) 
          {
            started=true;
            alert("aplet nie zostal zaladowany");
          }
          else
           alert("Wyswietlanie chwilowo wstrzymane - \n OK aby probowac ponownie");
        }
      }
</script>
Prosz� poda� has�o kt�re zosta�o u�yte w trakcie generowania certfikatu. 
<form id="form" action="<ww:url value="'/certificate/index.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
   <ww:hidden name="'alias'"  value="alias"/>

<table>
    <tr>
        <td>has�o:</td>
        <td><ww:textfield name="'password'" value="password" size="10" maxlength="10" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>plik:</td>
        <td><input type="text" name="pfxfile" readonly="true" class="txt"size="45" /></td>
    </tr>
    <script>
      filename = applet.getPFXFilename("<ww:property value='alias'/>");
      document.forms[0].pfxfile.value=filename;
    </script>

    <tr>
        <td>
          <input type="button" name="doCreate" value="Wyslij" class="btn" 
                 onclick="if (validated()) return export2pfx(); else return false;"/>
        </td>
    </tr>
</table></table>
</form>