<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N view-CSR.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1>CSR dla klienta <ww:property value='username'/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table width="40%" cellspacing="0">
  <tr>
    <td align="left">
      u�ytkownik 
    </td>
    <td align="left">
      <b><ww:property value='username'/></b>
    </td>
  </tr>
  <tr>
    <td>
      alias 
    </td>
    <td>
      <b><ww:property value='alias'/> </b>
    </td>
  </tr>
  <tr>
    <td>
      data wys�ania 
    </td>
    <td>
      <b><ww:property value='creationdate'/></b> 
    </td>
  </tr>
  <tr>
    <td>
      imi� i zawisko 
    </td>
    <td>
      <b><ww:property value='CN'/> </b>
    </td>
  </tr>
  <tr>
    <td>
      Organizacja 
    </td>
    <td>
      <b><ww:property value='O'/> </b>
    </td>
  </tr>
  <tr>
    <td>
      Jednostka Organizacyjna 
    </td>
    <td>
      <b><ww:property value='OU'/> </b>
    </td>
  </tr>
  <tr>
    <td>
      Wojew�dztwo 
    </td>
    <td>
      <b><ww:property value='ST'/> </b>
    </td>
  </tr>
  <tr>
    <td>
      Pa�swo 
    </td>
    <td>
      <b><ww:property value='C'/> </b>
    </td>
  </tr>
  <form id="form" action="<ww:url value="'/certificate/admin/view.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
  <ww:if test="signed== false">   
   <ww:hidden name="'CSRId'" value="CSRId"/>
      <tr>
        <td colspan="2">
           <ww:password name="'password'" size="20" maxlength="20" cssClass="'txt'" />
        </td>
      </tr>
  </ww:if>
    <tr>
      <td colspan="2">
  <ww:if test="signed== false">   
              <ds:submit-event value="'Podpisz'" name="'doSign'" />
  </ww:if>
              <ds:submit-event value="'Anuluj'" name="'doCancel'" />
      </td>
    </tr>
    
  </form>
</table>
