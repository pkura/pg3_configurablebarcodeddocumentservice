<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.util.prefs.*" %>
<%@ page import="pl.compan.docusafe.core.certificate.*" %>
<h1>Certyfikaty</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<center>
  <applet code="pl.compan.docusafe.applets.certificate.CertApplet" 
          archive="<ww:url value="'/certificates.jar'"/>, <ww:url value="'/bcprov-jdk12-127.jar'"/>"
          width=300 
          height=10>
  </applet>
</center>
<form id="form" action="<ww:url value="'/certificate/index.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table width="100%" class="search" id="tab1">
  <tr>
    <th>
       alias
    </th>
    <th>
      wygenerowano
    </th>
    <th>
      status
    </th>
    <th></th>
    <th></th>
  </tr>
  <script>
     function addRow(alias,created,status)
     {
       var tbody       = document.getElementById("tab1").getElementsByTagName("tbody")[0];
       var row         = document.createElement("tr");
       var cell1       = document.createElement("td");
       var cell2       = document.createElement("td");
       var cell3       = document.createElement("td");
       var cell4       = document.createElement("td");
       var cell5       = document.createElement("td");
       link1 = "<a href= '<ww:url value="'/certificate/index.action'"/>?doView=view&alias="+alias+"'>"+alias+"</a>"; 
       link2  = ""; 
       if (status=="podpisany")
       {
           link2=link2+"<a href='<ww:url value="'/certificate/export.action'"/>?alias="+alias+"'>";
           link2=link2+"eksportuj do pliku";
       }
       else
       {
           link2=link2+"<a href='<ww:url value="'/certificate/import.action'"/>?alias="+alias+"'>";
           link2= link2+"sprawd�";
       }
       link2=link2+"</a>";
       link3="<a href='#' onclick='deleteEntry(\""+alias+"\")'>";
       link3=link3+"usu�</a>";
       cell1.innerHTML = link1;
       cell2.innerHTML = created;
       cell3.innerHTML = status;
       cell4.innerHTML = link2;
       cell5.innerHTML = link3;
       row.appendChild(cell1);
       row.appendChild(cell2);
       row.appendChild(cell3);
       row.appendChild(cell4);
       row.appendChild(cell5);
       tbody.appendChild(row);
     }
     function showCerts()
     {
      number = applet.getAliasesNumber();
      for (i=0;i<number;i++)
      {
        alias   = applet.getAlias(i);
        status  = applet.getStatus(i);
        created = applet.getCreationDate(i);
        addRow(alias,created,status);
      }
     }
     function deleteEntry(alias)
     {
       conf = confirm("czy jestes pewien?");
       if (true==conf)
       {
         document.applets[0].deleteEntry(alias);
         document.forms[0].submit();
       }
     }
     function exportEntry(alias)
     {
       alert(alias);
     }

      applet = document.applets[0];
      started =false;
      count =0;
      while (started==false)
      {
        try
        {
          applet.isActive();
          started =true;
        }
        catch(e)
        {
          count++;
          if (count==5)
          {
            started=true;
            alert("aplet nie zostal zaladowany");
          }
          else
           alert("Wyswietlanie chwilowo wstrzymane - \n OK aby probowac ponownie");
        }
      }
      showCerts();
  </script>
    <tr>
        <td colspan="5">
            <p align="left"><ds:submit-event value="'Wy�lij nowe CSR'" name="'doNewCSR'" /></p>
        </td>
    </tr>
</table>
</form>
<!--
        document.write("<td><a href='#' onclick='export2pfx(\""+alias+"\")'> eksportuj</a></td>");
-->