<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N settings.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1>Ustawienia CA</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<script>
  function validated()
  {
     if (document.forms[0].password.value=='')
     {
      alert("prosz� poda� has�o");
      return false;
     }
     else
      return true;
  }
</script>
Prosz� poda� has�o kt�re zosta�o u�yte w trakcie generowania certfikatu. 
<form id="form" action="<ww:url value="'/certificate/admin/settings.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
   <ww:hidden name="'oldKeystore'" value="oldKeystore"/>

<table>
    <tr>
        <td>alias:</td>
        <td><ww:textfield name="'alias'" value="alias" size="10" maxlength="10" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>has�o :</td>
        <td><ww:password name="'password'" size="20" maxlength="20" cssClass="'txt'" />(tylko do weryfikacji)</td>
    </tr>

    <tr>
        <td>plik:</td>
        <td><ww:file name="'keystoreFilename'" value="keystoreFilename" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>aktualny :</td>
        <td><ww:property value='info'/></td>
    </tr>

    <tr>
        <td>
              <ds:submit-event value="'Ustaw'" name="'doSet'" />
        </td>
    </tr>
</table></table>
</form>