<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N create-CSR.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1>CSR</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<center>
  <applet code="pl.compan.docusafe.applets.certificate.CertApplet" 
          archive="<ww:url value="'/certificates.jar'"/>, <ww:url value="'/bcprov-jdk12-127.jar'"/>"
          width=300
          height=10>
  </applet>
</center>
<script>
  function validated()
  {
     _form   = document.forms[0];
     applet  = document.applets[0];
     pass    = _form.password.value;
     pass2   = _form.password2.value;
     if ((pass=='')||(pass2=='')) 
     {
        alert("Has�o nie zosta�o podane");
        return false;
     }
     if (pass!=pass2) 
     {
        alert("Has�a nie zgadzaj� si�");
        return false;
     }
     form.alias.value=applet.getNextAlias(form.username.value);
    
     ret =applet.getCSR(form.alias.value,form.CN.value,form.OU.value,form.O.value,form.L.value,form.ST.value,form.C.value,form.email.value,form.password.value);
     if (!isNaN(ret))
     {
       value = ret;
       if ((ret=='111')||(ret=='112')||(ret=='113')) value="�aden z dostawc�w uslug krypograficznych nie wspiera wymaganych algorytm�w ("+ret+")";
       alert("Wyst�pi� b��d w trakcie generowania CSR \n"+value);
       return false;
     }
     form.encoded.value=ret;
     return true;
  }
</script>

<form id="form" action="<ww:url value="'/certificate/createCSR.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
   <ww:hidden name="'encoded'"  value="encoded"/>
   <ww:hidden name="'username'" value="username"/>
   <ww:hidden name="'alias'" value="alias"/>
   <input type="hidden" name="doCreate" id="doCreate"/>

<table>
    <tr>
        <td>Imi� i nazwisko:</td>
        <td><ww:textfield name="'CN'" value="CN" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Miejscowo�� :</td>
        <td><ww:textfield name="'L'" value="L" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Jednostka terytorialna:</td>
        <td><ww:textfield name="'ST'" value="ST" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Kraj:</td>
        <td><ww:textfield name="'C'" value="C" size="2" maxlength="2" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Organizacja:</td>
        <td><ww:textfield name="'O'" value="O" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Jednostka organizacyjna:</td>
        <td><ww:textfield name="'OU'" value="OU" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Has�o:</td>
        <td><ww:password name="'password'" value="password" size="20" maxlength="20" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Powt�rz has�o:</td>
        <td><ww:password name="'password2'" value="password2" size="20" maxlength="20" cssClass="'txt'" /></td>
    </tr>


    <tr>
        <td>Email:</td>
        <td><ww:textfield name="'email'" value="email" size="50" maxlength="50" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>
          <input type="submit" name="doCreate" value="Wyslij" class="btn" 
                 onclick="if (!validated()) return false; document.getElementById('doCreate').value = 'true';"/>
          <ds:submit-event value="'Anuluj'" name="'doCancel'" />
        </td>
    </tr>
</table></table>
</form>