<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table class="userInfoSummary" style="width: 800px; text-align: left;">
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="DrugieImie" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.drugieimie" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="NazwiskoRodowe" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.nazwiskorodowe" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="MiejsceUrodzenia" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.nazwiskorodowe" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="NrDowodu" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.dowodosnr" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="PESEL" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.pesel" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="NIP" />
			</td>
			<td>
				<ww:if test="single != null"><ww:property value="single.nip" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="UmowaStanowisko"/>
			</td>
			<td>
				<ww:if test="workDiv != null"><ww:property value="workDiv" /></ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="DataUrodzenia"/>
			</td>
			<td>
				<ww:if test="single != null">
					<ds:format-date value="single.dataur" pattern="dd-MM-yyyy"/>
				</ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="BadaniaLekWaznosc"/>
			</td>
			<td>
				<ww:if test="single != null">
					<ds:format-date value="single.badanielekwaznosc" pattern="dd-MM-yyyy"/>
				</ww:if>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="UmowaDataKoncowa"/>
			</td>
			<td>
				<ww:if test="single != null">
					<ww:property value="single.umowadatakoncowa" />
				</ww:if>
			</td>
		</tr>
		
			<tr>
				<td class="label">
					<ds:lang text="NrEwid"/>
				</td>
				<td>
					<ww:if test="single != null"><ww:property value="single.nrewid" /></ww:if>
				</td>
			</tr>
</table>

<table class="search table100p bottomSpacer userInfoSummary" id="mainTable" cellspacing="0">
<tr>
	<th class="empty">
		<ds:lang text="KodPocztowy" />
	</th>
	<th class="empty">
		<ds:lang text="PocztaNazwa"/>
	</th>
	<th class="empty">
		<ds:lang text="Miejscowosc"/>
	</th>
	<th class="empty">
		<ds:lang text="UlicaLabel"/>
	</th>
	<th class="empty">
		<ds:lang text="NrDomuLabel"/>
	</th>
	<th class="empty">
		<ds:lang text="NrMieszkaniaLabel"/>
	</th>
	<th class="empty">
		<ds:lang text="RodzajAdresu"/>
	</th>
</tr>
<ww:if test="adresy != null">
<ww:iterator value="adresy">
<tr>
	<td>
		<ww:property value="kodpoczt"/>
	</td>
	<td>
		<ww:property value="poczta"/>
	</td>
	<td>
		<ww:property value="miejscowosc"/>
	</td>
	<td>
		<ww:property value="ulica"/>
	</td>
	<td>
		<ww:property value="nrdomu"/>
	</td>
	<td>
		<ww:property value="nrmieszkania"/>
	</td>
	<td>
		<ww:property value="rodzajadresu"/>
	</td>
</tr>
</ww:iterator>
</ww:if>
</table>
</br>
