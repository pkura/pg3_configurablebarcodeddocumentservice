<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N address-book.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KsiazkaKontaktow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form name="SearchUsers" id="SearchUsers" action="<ww:url value="'/help/address-book.action'"/>" method="post" >

	<ww:hidden name="'sortField'" id="sortField" />
	<ww:hidden name="'ascending'" id="ascending" />
	<ww:hidden name="'pageNumber'" id="pageNumber" />

	<!-- WYSZUKIWARKA OS�B -->
	<fieldset class="container">
		<p class="simpleSearch">
			<label for="user.lastname"><ds:lang text="Nazwisko" /></label>
			<ww:textfield id="user.lastname" name="'user.lastname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'"/>
		</p>
		<p>
			<label for="user.firstname"><ds:lang text="Imie" /></label>
			<ww:textfield id="user.firstname" name="'user.firstname'" maxlength="50" size="15"  cssClass="'txt dontFixWidth'"/>
		</p>
		<ds:available test="addressbook.name">
		<p>
			<label for="user.name"><ds:lang text="NazwaUzytkownika" /></label>
			<ww:textfield id="user.name" name="'user.name'" maxlength="50" size="15"  cssClass="'txt dontFixWidth'"/>
		</p>
		</ds:available>
		<p>
			<label for="division"><ds:lang text="Dzial" /></label>
			<ww:textfield id="division" name="'division'" maxlength="50" size="15"  cssClass="'txt dontFixWidth'"/>
		</p>
	</fieldset>

	<fieldset class="container">
		<p class="simpleSearch">
			<label for="maxResults"><ds:lang text="LiczbaRezultatowNaStronie" /></label>
			<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
		</p>
		<p>
			<label for="selectedOrganizationId"><ds:lang text="Organizacja" /></label>
			<ww:select name="'selectedOrganizationId'" id="selectedOrganizationId" list="organizations" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
				<script type="text/javascript">
					$j('#selectedOrganizationId').change(function() {
							$j('#organizationIdReload').attr('value', 'true');
							document.forms.SearchUsers.submit();
						});
				</script>
		</p>
		<p>
			<label for="selectedFloorId"><ds:lang text="Pietro" /></label>
			<ww:select id="selectedFloorId" name="'selectedFloorId'" list="floors" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" onchange="'document.forms.SearchUsers.submit()'" />
		</p>
	</fieldset>
	
	<fieldset class="container">
		<p>
		</p>
		<p>
			<label for="position"><ds:lang text="Stanowisko" /></label>
			<ww:textfield id="position" name="'position'" maxlength="50" size="15" cssClass="'txt dontFixWidth'"/>
		</p>
		<ds:available test="addressbook.roomNum">
		<p>
			<label for="user.roomNum"><ds:lang text="NumerPokoju" /></label>
			<ww:textfield id="user.roomNum" name="'user.roomNum'" maxlength="50" size="15" cssClass="'txt dontFixWidth'"/>
		</p>
		</ds:available>
		<p>
			<label for="user.postDescription"><ds:lang text="wOpisieStanowiska" /></label>
			<ww:textfield id="user.postDescription" name="'user.postDescription'" maxlength="50" size="15" cssClass="'txt dontFixWidth'"/>
		</p>
		<%--<p>
			<label for="user.identifier"><ds:lang text="KodUzytkownika" /></label>
			<ww:textfield id="user.identifier" name="'user.identifier'" maxlength="50" size="15" cssClass="'txt dontFixWidth'"/>
		</p>--%>
	</fieldset>
	
	<div class="fieldsetSearch">
		<ds:submit-event value="'Szukaj'" name="'doFilter'" cssClass="'btn btnBanner'"/>
		<a id="searchSwitcher" href="#" extendedSearch="false" >Wyszukiwanie zaawansowane</a>
		<a id="clearFields" href="#" >Wyczy��</a>
		<ds:available test="kontakty.wydrukKontaktow">
			<ds:submit-event value="'Wydruk'" name="'doReport'" />
		</ds:available>
	
	</div>
	
	<div class="clearBoth" style="height: 30px"></div>

	<table id="mainTable" class="search" cellspacing="0" style="text-align:left;">
		<tr>
			<th style="text-align:left;">
				<a href="#" class="sortBy" name="lastname" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
				<ds:lang text="Nazwisko"/>
				<a href="#" class="sortBy" name="lastname" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
			</th>
			<th style="text-align:left;">
				<a href="#" class="sortBy" name="firstname" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11"/>
				</a>
				<ds:lang text="Imie"/>
				<a href="#" class="sortBy" name="firstname" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11"/>
				</a>
			</th>
			<th class="empty" style="text-align:left;">
				<ds:lang text="Dzial"/>
			</th>			
			<th class="empty" style="text-align:left;">
				<ds:lang text="Stanowisko"/>
			</th>
			<!-- 
			<th>
				<a href="#" class="sortBy" name="name" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
				<ds:lang text="NazwaUzytkownika"/>
				<a href="#" class="sortBy" name="name" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11"/>
				</a>
			</th>
			 -->
			<%--
			<th class="empty">
				<ds:lang text="KodUzytkownika"/>
			</th>
			--%>
			<th class="empty" style="text-align:left;">
				<ds:lang text="Tel.Stacjonarny"/>
			</th>
			<th class="empty" style="text-align:left;">
				<ds:lang text="Nr.Wewnetrzny"/>
			</th>
			<!--  
			<th class="empty">
				<ds:lang text="Tel.Kom�rkowy"/>
			</th>
			-->
			<th class="empty" style="text-align:left;">
				<ds:lang text="Email"/>
			</th>
			<th class="empty" style="text-align:left;">
				<ds:lang text="Komunikator"/>
			</th>
		</tr>
		<ww:iterator value="usersList" status="status">
			<ww:set name="result"/>
			<tr class="<ww:if test="#status.odd">oddRows </ww:if><ww:if test="!atWork"> inactive</ww:if>"> 
				<td style="text-align:left;">
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="lastname"/>
					</a>
				</td>
				<td style="text-align:left;">
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="firstname"/>
					</a>
				</td>
				<td style="text-align:left;">
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="getDivisionString(#result)"/>
					</a>
				</td>
				<td style="text-align:left;">
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="getPositionString(#result)"/>
					</a>
				</td>
				<%-- 
				<td>
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="name"/>
					</a>
				</td>
				--%>
				<%--<td>
					<a href="<ww:url value="'user-info.action'">
								<ww:param name="'userImpl.name'" value="name"/>
								<ww:param name="'tid'" value="0" />
							</ww:url>" >
						<ww:property value="identifier"/>
					</a>
				</td> --%>
				<td style="text-align:left;">
					<ww:property value="phoneNum"/>
				</td>
				<td style="text-align:left;">
					<ww:property value="extension"/>
				</td>
				<%-- 
				<td>
					<ww:property value="mobileNum"/>
				</td>
				--%>
				<td style="text-align:left;">
					<a class="tiny" href="mailto:<ww:property value="email"/>"><ww:property value="email"/></a>
				</td>
				<td style="text-align:left;">
					<ww:property value="communicatorNum"/>
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<table class="table100p">
	<tr>
		<td align="center">
			<ww:set name="pager" scope="request" value="pager"/>
				<jsp:include page="/pager-links-include.jsp"/>
				<script type="text/javascript">
					$j('.pager a').click(function() {
						var page = $j(this).attr('href');
						if (page == '#')
							return false;
						$j('#pageNumber').attr('value', page);
						document.forms.SearchUsers.submit();
						return false;
					});
				</script>
		</td>
	</tr>
	</table>

	<!-- SORTOWANIE DANYCH i inne -->
	<script type="text/javascript">
		showSortArrow($j('#sortField').val());

		$j('.sortBy').click(function(){
				$j('#sortField').attr('value', $j(this).attr('name'));
				$j('#ascending').attr('value', $j(this).attr('ascending'));
				document.forms.SearchUsers.submit();
				return false;
			});

		bindLabelDecoration();
		initExtendedSearch();
	</script>
</form>
<!--N koniec address-book.jsp N-->