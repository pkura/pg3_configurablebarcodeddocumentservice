<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="ImportBilingow"/></h1>
<p></p>

<ds:available test="!layout2">
<ds:xmlLink path="Calendars"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="BilingAdmin"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/help/biling-import.action'"/>" method="post" enctype="multipart/form-data">
<div id="middleContainer"> <!-- BIG TABLE start -->
    <table>
        <tr>
            <td><ds:lang text="Plik"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
        <td><ds:lang text="Rodzaj"/>:</td>
	       	<td>
				<ww:select name="'type'" list="importTypes" listKey="key" 
					listValue="value" cssClass="'sel'" cssStyle="'width: 150px;'" />
			</td>
		</tr>
		<td align="right">
			<ds:lang text="NumerBilingu"/>:
		</td>
		<td>
			<ww:textfield name="'bilingNumber'" maxlength="50" size="15"/>
		</td>
        <tr>
            <td></td>
            <td><ds:event name="'doImport'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </table>
    <div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</form>