<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N index.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Pomoc"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/help/index.action'"/>" id="downloadForm" method="post">
	<input type="hidden" name="doDownload" id="doDownload"/>
	<input type="hidden" name="downloadFilename" id="downloadFilename"/>
	<input type="hidden" name="downloadType" id="downloadType"/>
</form>

<ww:if test="!general.isEmpty()">
	<p>
		<ds:lang text="PodrecznikiOgolne"/>:
	</p>		
	<ww:set name="ind" value="1"/>
	
	<ww:iterator value="general">
		<ww:set name="res"/>
		<p>
			<a href="javascript:void(downloadFile(<ww:property value='1000 + #ind'/>))">
				<ww:property value='#res.getValue()'/></a>
		</p>
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
</ww:if>

<ww:if test="!specific.isEmpty()">
	<p>
		<ds:lang text="PodrecznikiDedykowane"/>:
	</p>
	
	<ww:set name="ind" value="1"/>
	
	<ww:iterator value="specific">
		<ww:set name="res"/>
		<p>
			<a href="javascript:void(downloadFile(<ww:property value='2000 + #ind'/>))">
				<ww:property value='#res.getValue()'/></a>
		</p>
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
</ww:if>

<ww:if test="!tools.isEmpty()">
	<p>
		<ds:lang text="Narzedzia"/>:
	</p>
	
	<ww:set name="ind" value="1"/>
	
	<ww:iterator value="tools">
		<ww:set name="res"/>
		<p>
			<a href="javascript:void(downloadFile(<ww:property value='3000 + #ind'/>))">
				<ww:property value='#res.getValue()'/></a>
		</p>
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
</ww:if>
<ww:if test="htmlHelpFile != null">
<ww:property value="htmlHelpFile" escape="false"/>
</ww:if>

		

<%--
<html>
<head>
	<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>" title="main" />
</head>
<body>

<h1><ds:lang text="Pomoc"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/help/index.action'"/>" id="downloadForm" method="post">
	<input type="hidden" name="doDownload" id="doDownload"/>
	<input type="hidden" name="downloadFilename" id="downloadFilename"/>
	<input type="hidden" name="downloadType" id="downloadType"/>
</form>

<ww:if test="!general.isEmpty()">
<p><ds:lang text="PodrecznikiOgolne"/>:
<ww:set name="ind" value="1"/>
<ww:iterator value="general">
	<ww:set name="res"/>
	<p><a href="javascript:void(downloadFile(<ww:property value='1000 + #ind'/>))">
	<ww:property value='#res.getValue()'/></a></p>
	<ww:set name="ind" value="#ind + 1"/>
</ww:iterator>
</p>
</ww:if>

<ww:if test="!specific.isEmpty()">
<p><ds:lang text="PodrecznikiDedykowane"/>:</p>
<ww:set name="ind" value="1"/>
<ww:iterator value="specific">
	<ww:set name="res"/>
	<p><a href="javascript:void(downloadFile(<ww:property value='2000 + #ind'/>))">
	<ww:property value='#res.getValue()'/></a></p>
	<ww:set name="ind" value="#ind + 1"/>
</ww:iterator>
</p>
</ww:if>

<ww:if test="!tools.isEmpty()">
<p><ds:lang text="Narzedzia"/>:
<ww:set name="ind" value="1"/>
<ww:iterator value="tools">
	<ww:set name="res"/>
	<p><a href="javascript:void(downloadFile(<ww:property value='3000 + #ind'/>))">
	<ww:property value='#res.getValue()'/></a></p>
	<ww:set name="ind" value="#ind + 1"/>
</ww:iterator>
</p>
</ww:if>

<%--
Docusafe&reg; <br/>
Wersja: <%= Configuration.getVersionString() %>.<%= Configuration.getDistNumber() %> <br/>
-->

</body>
</html>
--%>
<script type="text/javascript">
	var files = new Array();
	
	<ww:set name="ind" value="1"/>
	<ww:iterator value="general">
		<ww:set name="res"/>
		files[<ww:property value="1000 + #ind"/>] = '<ww:property value='#res.getKey()'/>';
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
	
	<ww:set name="ind" value="1"/>
	<ww:iterator value="specific">
		<ww:set name="res"/>
		files[<ww:property value="2000 + #ind"/>] = '<ww:property value='#res.getKey()'/>';
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
	
	<ww:set name="ind" value="1"/>
	<ww:iterator value="tools">
		<ww:set name="res"/>
		files[<ww:property value="3000 + #ind"/>] = '<ww:property value='#res.getKey()'/>';
		<ww:set name="ind" value="#ind + 1"/>
	</ww:iterator>
	
	function downloadFile(id)
	{
		var form = document.getElementById('downloadForm');
		document.getElementById('doDownload').value = 'true';
		document.getElementById('downloadFilename').value = files[id];
		document.getElementById('downloadType').value = (id - (id % 1000)) / 1000;
		form.submit();
	}
</script>
<!--N koniec index.jsp N-->