<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/help/edit-user-location.action'"/>" enctype="application/x-www-form-urlencoded" method="post">
	<ds:available test="layout2">
		<div id="middleContainer" style="zoom: 1.0; *margin-top: -12px; *border-top: none;"> <!-- BIG TABLE start -->
	</ds:available>
	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'tid'" value="'2'" />
	<ww:hidden name="'eid'" value="'true'" />
	<ww:hidden name="'userLocation.paramX'" value="userImpl.userLocation.paramX" />
	<ww:hidden name="'userLocation.paramY'" value="userImpl.userLocation.paramY" />
	
<ww:if test="userImpl.userLocation != null">
	<ww:if test="editable">
	<p>
		<ds:submit-event value="'Przejd� do edycji lokalizacji'" name="'nope'" cssClass="'btn'"/>
	</p>
	</ww:if>
	<table class="userInfoSummary" style="width: 500px; text-align: left;">
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaLokalizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.name" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaOrganizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.organization" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaUlicyOrganizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.street" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="KodPocztowyOrganizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.zipCode" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaMiastaOrganizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.city" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaRegionuOrganizacji" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.address.region" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaPietra" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.name" />
			</td>
		</tr>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="OpisPietra" />
			</td>
			<td>
				<ww:property value="userImpl.userLocation.floor.description" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" class="btn" value="Poka� lokalizacj�" onclick="showLocation()"/>
			</td>
		</tr>
	</table>
</ww:if>
<ww:else>
	<ul id="errorList" style="color: red" class="msgList_">
		<li><ds:lang text="BrakUstawionejLokalizacjiUzytkownika"/></li>
	</ul>
	<ww:if test="editable">
		<br />
		<ds:submit-event value="'Przejd� do ustawienia lokalizacji'" name="''" cssClass="'btn'"/>
	</ww:if>
</ww:else>

	<table id="userLocation" class="floatLeft" style="width: 100%; display: none;">
		<tr>
			<td>
				<img id="locationMap" style="*position: relative" src="<ww:url value="'/admin/image-viewer.action'"/>?imgId=<ww:property value="userImpl.userLocation.floor.image.id" />" />
				<img id="locationUser" src="<ww:url value="'/img/user_location.gif'" />" />
			</td>
		</tr>
		
	</table>

	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>

</form>

<script type="text/javascript">
	var jqLocationUser = $j('#locationUser');
	var jqLocationMap = $j('#locationMap');
	var locationMapPos = getPos(jqLocationMap[0]);

	function showLocation() {
		$j('#userLocation').show();
		locationMapPos = getPos(jqLocationMap[0]);
		jqLocationUser.css('left', locationMapPos.left + parseInt($j('[name="userLocation.paramX"]').val(), 10) + 'px')
		   	.css('top', locationMapPos.top + parseInt($j('[name="userLocation.paramY"]').val(), 10) + 'px');
	}
</script>
