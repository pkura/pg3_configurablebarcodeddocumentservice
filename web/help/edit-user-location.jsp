<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form name="editLocationForm" id="edit-location-form" action="<ww:url value="'/help/edit-user-location.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'userImpl.id'" />
	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />
	<ww:hidden name="'userLocation.id'" />
	<ww:hidden name="'userLocation.paramX'" value="userLocation.paramX" />
	<ww:hidden name="'userLocation.paramY'" value="userLocation.paramY" />
	

	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	<p>
	<input type="button" class="btn"
		onclick="javascript:location.href = '<ww:url value="'/help/user-info.action'"><ww:param name="'userImpl.name'" value="userImpl.name" /></ww:url>'" 
		value="<ds:lang text="PodgladProfilu" />" /><br />
	</p>
	
<ww:if test="editable">
	
	<table class="floatLeft">
		<tr>
			<td>
				<ds:lang text="WybierzLokalizacje" />
			</td>
			<td>
				<ww:select name="'selectedOrganizationId'" list="organizations" listKey="key"
					listValue="value" cssClass="'sel'" />
			</td>
			<td>
				<ds:submit-event value="'Wybierz'" name="''" cssClass="'btn'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WybierzPietroWLokalizacji" />
			</td>
			<td>
				<ww:select name="'selectedFloorId'" list="floors" listKey="key"
					listValue="value" cssClass="'sel'" />
			</td>
			<td>
				<ds:submit-event value="'Wy�wietl pi�tro'" name="'loadFloorImage'" cssClass="'btn'"/>
			</td>
		</tr>
	</table>
	
	<ww:if test="floorImage != null">
		<table class="floatLeft" style="width: 100%; text-align: center; mergin-top: 50px;">
		<tr>
			<td><span style="font-size: 14px; font-weight: bold;"><ds:lang text="KliknijPunktNaObrazkuAbyWybracSwojaLokalizaje" /></span></td>
		</tr>
		<tr>
			<td>
				<img style="*position: relative" id="locationMap" src="<ww:url value="'/admin/image-viewer.action'" />?imgId=<ww:property value="floorImage.id" />" />
				<img id="locationUser" src="<ww:url value="'/img/user_location.gif'" />" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:submit-event value="'Ustaw lokalizacje'" name="'saveUserLocation'" cssClass="'btn btnBanner'"/>
			</td>
		</tr>
		</table>
		<script type="text/javascript">
		$j(document).ready(function() {
			var jqLocationUser = $j('#locationUser');
			var jqLocationMap = $j('#locationMap');
			var locationMapPos = getPos(jqLocationMap[0]);

			//alert(locationMapPos.left + ' x ' + locationMapPos.top);

			 jqLocationUser.css('left', locationMapPos.left + parseInt($j('[name="userLocation.paramX"]').val(), 10) + 'px')
		     	.css('top', locationMapPos.top + parseInt($j('[name="userLocation.paramY"]').val(), 10) + 'px');
			
			$j('#locationMap').click(function(evt) {
				x = evt.pageX - locationMapPos.left; 
				y = evt.pageY - locationMapPos.top;
				setPos(jqLocationUser, x + locationMapPos.left, y + locationMapPos.top);
				jqLocationUser.fadeIn('slow');
				$j('input[name="userLocation.paramX"]').val(x);
				$j('input[name="userLocation.paramY"]').val(y);

				//alert($j('input[name=userLocation.paramX]').val() + ' x ' + $j('input[name=userLocation.paramY]').val());
			});

			function setPos(jqObj, x, y) {
				jqObj.css('left', x + 'px').css('top', y + 'px');
			}
		});
		</script>
	</ww:if>
	
</ww:if>
<ww:else>
	<ul id="errorList" style="color: red" class="msgList_">
		<li><ds:lang text="BrakUprawnienDoEdycjiProfilu"/></li>
	</ul>
</ww:else>

	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>