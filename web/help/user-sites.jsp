<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="user.Firstname"/>&nbsp;<ww:property value="user.Lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<style>

textarea.editable {
	border: 1px dotted gray;
	background: transparent;
	-moz-border-radius: 0;
}

textarea.editable:hover {
	border: 1px solid #AAAAAA;
	background: rgba(0,0,0,0.05);
	*background: #F2F2F2;
}

textarea.editable:focus {
	border: 1px solid #333;
	background: -moz-linear-gradient(center top , #FAF16A, #FFFBC4) repeat scroll 0 0 transparent;
	background: -webkit-gradient(linear, left top, left bottom, from(#FAF16A), to(#FFFBC4));
	-moz-box-shadow: 0 0 3px gray;
	filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr="#FAF16A", EndColorStr="#FFFBC4");
}

textarea.empty {
	color: #666;
	font-style: italic;
}

</style>

<form id="form" action="<ww:url value="'/help/user-sites.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'userImpl.id'" />
	<ww:hidden name="'tid'" />

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<table class="mediumTable search userInfoSites">
	<tr>
		<th><ds:lang text="OMnie" /></th>
		<th><ds:lang text="UlubioneStrony" /></th>
	</tr>
	<ww:if test="editable">
	<tr>
		<td>
			<ww:textarea name="'userImpl.aboutMe'" cssClass="'editable dontFixWidth'" cssStyle="'width: 300px;'" rows="10"/>
		</td>
		<td>
			<ww:textarea name="'userImpl.sites'" cssClass="'editable dontFixWidth'" cssStyle="'width: 300px;'"  rows="10"/>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<ds:submit-event value="'Zapisz zmiany'" name="'saveUserSites'" cssClass="'btn saveBtn'"/>
		</td>
	</tr>
	</ww:if>
	<ww:else>
		<tr>
			<td>
				<ww:property value="userImpl.aboutMe" />
			</td>
			<td id="sitesTd">
				<ww:property value="userImpl.sites" />
			</td>
		</tr>
	</ww:else>
</table>

<script type="text/javascript">
	var MSG_ADD_TEXT = 'dodaj tekst';

	function createHrefs() {
		var str = $j('#sitesTd').text();
		
		var r = /www[.][\S]*[.][\S]*/g;
		var founded = str.match(r);

		if(founded) {
			var nl = '';
			for(i = 0; i < founded.length; i ++) {
				if(i == 0)	nl = '<br/>';
			    str = str.replace(founded[i], nl + '<a href="http://' + founded[i] + '" >' + founded[i] + '</a>');
			    trace(str);
			}
	
			$j('#sitesTd').html(str);
		}
	}
	createHrefs();

	$j('textarea.editable').each(function() {
		if($j(this).val() == '')
			$j(this).addClass('empty').val(MSG_ADD_TEXT);
		$j(this).bind('focus.clear', function(){
			if ($j(this).hasClass('empty')) {
				$j(this).removeClass('empty').val('');
			}
		}).bind('blur.clear', function(){
			if ($j(this).val() == '' || $j(this).val() == MSG_ADD_TEXT) {
				$j(this).addClass('empty').val(MSG_ADD_TEXT);
			}
		});
	});

</script>

	
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>