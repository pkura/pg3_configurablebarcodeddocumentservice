<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1><ds:lang text="ZarzadzanieBilingami"/></h1>
<p></p>

<ds:available test="!layout2">
<ds:xmlLink path="Calendars"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="BilingAdmin"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="BilingsForm" name="BilingsForm" action="<ww:url value="'/help/user-phone-numbers.action'"/>" method="post">
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />	
	<!-- 
	<ww:hidden name="'doConnect'" />	
	<ww:hidden name="'doReconnect'" />	
	<ww:hidden name="'doDelete'" />
	 -->
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	
	<!-- WYSZUKIWARKA -->
<table class="tableMargin">
<tr>
	<td align="right">
		<ds:lang text="Uzytkownik"/>:
	</td>
	<td>
		<ww:select name="'username'" list="users" headerKey="''" headerValue="'wszystkie'" listKey="name" listValue="asLastnameFirstname()" cssClass="'sel floatLeft'"/>
	</td>
	<td align="right">
		<ds:lang text="NumerTelefonu" />:
	</td>
	<td>
		<ww:textfield name="'phoneNumber'" maxlength="50" size="15" />
		<ds:submit-event value="getText('Wyswietl')" name="'doDefault'" cssClass="'btn'"/>
	</td>
	
</tr>
<tr>
	<td></td>
	<td colspan="6" align="right">
		<ds:submit-event value="getText('PrzypiszDoOsoby')" name="'doConnect'" cssClass="'btn'"/>
		<ds:submit-event value="getText('UsunUzytkownika')" name="'doReconnect'" cssClass="'btn'"/>
		<ds:submit-event value="getText('UsunWpis')" name="'doDelete'" cssClass="'btn'"/>
	</td>
</tr>	
	</td>
</tr>
</table>
	<table class="mediumTable search userPhone">
		<tr>
			<th></th>
			<th>
				<ds:lang text="Uzytkownik" />
			</th>
			<th>
				<ds:lang text="Numer" />
			</th>
		</tr>
		<ww:iterator value="numbers">
			<ww:set name="uname" value="username"/>
		<tr>
			<td><ww:checkbox name="'ids'" fieldValue="id" value="false" /></td>
			<td>
				<ww:property value="getUserLastnameFirstname(#uname)" />
			</td>
			<td>
				<ds:format-phone-number value="phoneNumber"/>
			</td>
		</tr>
		</ww:iterator>
	</table>
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>





	