<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/help/edit-user-info.action'"/>?tid=0&eid=true" enctype="multipart/form-data" method="post">
	
	<ww:hidden name="'userImpl.name'" />

	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	<p>
	<ww:if test="editable || userProfile">
		<ds:available test="edit_profile_button">
			<ds:submit-event value="'Przejd� do edycji profilu'" name="'nope'" cssClass="'btn'"/>
		</ds:available>
		
	</ww:if>
	<ww:if test="!atWork">
		&nbsp;&nbsp;&nbsp;<ds:lang text="PracownikJestNieobecny"/> do <ds:format-date value="whenReturn" pattern="dd-MM-yyyy"/> zast�powany przez <ww:property value="substitution"/>
	</ww:if>
	</p>
	
	<ww:if test="image != null && image.name != null">
	<!-- ZDJECIE PROFILU -->
	<img src="<ww:url value="'/admin/image-viewer.action'">
			  	<ww:param name="'imgId'" value="image.id" />
			  </ww:url>" class="userInfoPhoto" alt="image.name" />
	</ww:if>
	
	<table class="userInfoSummary" style="width: 800px; text-align: left;">
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="Stanowiska" />
			</td>
		<ww:iterator value="divisions" status="status">
			<ww:if test="#status.first">
			</ww:if>
			<ww:else><tr><td></td></ww:else>
				<td>
					<ww:property value="getPrettyPath()" />
				</td>
			</tr>
		</ww:iterator>
		<tr>
			<td class="label" style="width: 180px;">
				<ds:lang text="NazwaUzytkownika" />
			</td>
			<td>
				<ww:property value="userImpl.name" />
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="NazwaAlternatywna" />
			</td>
			<td>
				<ww:property value="userImpl.externalName" />
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="KodUzytkownika" />
			</td>
			<td>
				<ww:property value="userImpl.identifier" />
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="Zalogowany" />
			</td>
			<td>
				<ww:if test="loggedIn"><span class="loggedIn">zalogowany</span></ww:if>
				<ww:else><span class="notLoggedIn">niezalogowany</span></ww:else>
			</td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="Przelozony"/>
			</td>
			<td>
				<ww:if test="userImpl.supervisorsHierarchy != null">
					<ww:iterator value="userImpl.supervisorsHierarchy">
					<a href="<ww:url value="/help/user-info.action">
								<ww:param name="'userImpl.name'" value="name" />
								<ww:param name="'tid'" value="tid" />
							</ww:url>">
						<ww:property value="firstname"/> <ww:property value="lastname"/>
					</a> 
					</ww:iterator>
				</ww:if>
				<ww:else>
					<ds:lang text="NieWybranoPrzelozonego"/>
				</ww:else>
			</td>
		</tr>
		<tr>
			<td class="label"><ds:lang text="Email"/></td>
			<td><a href="mailto:<ww:property value="userImpl.email"/>" ><ww:property value="userImpl.Email"/></a></td>
		</tr>
		<tr>
			<td class="label"><ds:lang text="Tel.Stacjonarny"/></td>
			<td><ww:property value="userImpl.phoneNum"/></td>
		</tr>
		<ww:if test="editable">
		<ds:available test="user_limit">
			<tr>
				<td class="label"><ds:lang text="Limit"/></td>
				
				<td><ww:property value="userImpl.limitPhone"/></td>
			</tr>
		</ds:available>
		</ww:if>
		<ds:available test="user_ext_number">
			<tr>
				<td class="label"><ds:lang text="Nr.Wewnetrzny"/></td>
				<td><ww:property value="userImpl.extension" /></td>
			</tr>
		</ds:available>
		<ds:available test="user_int_number">
			<tr>
				<td class="label"><ds:lang text="Nr.Wewnetrzny"/></td>
				<td><ww:property value="userImpl.internalNumber" /></td>
			</tr>
		</ds:available>
		<tr>
			<td class="label"><ds:lang text="Tel.Komorkowy"/></td>
			<ww:if test="editable">
				<td><ww:property value="userImpl.mobileNum"/></td>
			</ww:if>
			<ww:else>
				<td>*********</td>
			</ww:else>
		</tr>
		
		<ww:if test="editable">
		<ds:available test="user_limit">
			<tr>
				<td class="label"><ds:lang text="Limit"/></td>
				<td><ww:property value="userImpl.limitMobile"/></td>
			</tr>
		</ds:available>
		</ww:if>
		
		<tr>
			<td class="label"><ds:lang text="Komunikator"/></td>
			<td><ww:property value="userImpl.communicatorNum"/></td>
		</tr>
		<tr>
			<td class="label"><ds:lang text="NumerPokoju"/></td>
			<td><ww:property value="userImpl.roomNum"/></td>
		</tr>
		<tr>
			<td class="label">
				<ds:lang text="OpisStanowiska" />
			</td>
			<td>
				<ww:property value="userImpl.postDescription" />
			</td>
		</tr>
	</table>
	
	
	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
	
</form>
