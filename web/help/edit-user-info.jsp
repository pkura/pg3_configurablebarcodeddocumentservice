<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/help/edit-user-info.action'"/>" method="post" enctype="multipart/form-data">
	
	<ww:hidden name="'userImpl.id'" />
	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />

	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	<p>
	<input type="button" class="btn"
		onclick="javascript:location.href = '<ww:url value="'/help/user-info.action'"><ww:param name="'userImpl.name'" value="userImpl.name" /></ww:url>'" 
		value="<ds:lang text="PodgladProfilu" />" />
	</p>
<ww:if test="addsEdit">
<ww:if test="editable || userProfile">

	<ww:if test="image != null && image.name != null">
	<!-- ZDJECIE PROFILU -->
	<img src="<ww:url value="'/admin/image-viewer.action'">
			  	<ww:param name="'imgId'" value="image.id" />
			  </ww:url>" class="userInfoPhoto" alt="image.name" />
	</ww:if>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="DataUtworzenia"/>:
			</td>
			<td>
				<ww:property value="userImpl.creationTime"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>:
			</td>
			<td>
				<ww:property value="userImpl.name"/>
			</td>
		</tr>
		 
		<tr>
			<td>
				<ds:lang text="NazwaAlternatywna"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.externalName'" size="50" maxlength="50" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield name="'userImpl.firstname'" size="50" maxlength="50" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield name="'userImpl.lastname'" size="50" maxlength="50" />
			</td>
		</tr>
		
		<ds:modules test="coreoffice">
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
					<ww:textfield name="'userImpl.identifier'" size="5" maxlength="20" />
				</td>
			</tr>
		</ds:modules>

		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.email'" size="50" maxlength="50" />
			</td>
		</tr>
	
		<!-- nowe pola RS -->
		<tr>
			<td>
				<ds:lang text="TelStac"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.phoneNum'" size="50" maxlength="50" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Limit"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.limitPhone'" size="50" maxlength="50" disabled="!isAdmin"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="NumWew"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.extension'" size="50" maxlength="50" />
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.mobileNum'" size="50" maxlength="50" />
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="Limit"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.limitMobile'" size="50" maxlength="50" disabled="!isAdmin"/>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.communicatorNum'" size="50" maxlength="50" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumerPokoju"/>:
			</td>
			<td>
				<ww:textfield name="'userImpl.roomNum'" size="50" maxlength="30" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="ZmianaZdjeciaProfilu" />
			</td>
			<td>
				<ww:file name="'file'" cssClass="'txt'" size="50" id="file"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="OpisStanowiska" />
			</td>
			<td>
				<ww:textarea name="'userImpl.postDescription'" cssClass="'txt'" cssStyle="'width: 300px;'" />
			</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
			<td colspan="1">
				<ds:submit-event value="'Zapisz zmiany'" name="'doEditData'" cssClass="'btn'"/>
			</td>	
		</tr>
		
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2">
				<p>
					<span class="star">*</span>
					<ds:lang text="PoleObowiazkowe"/>
				</p>
			</td>
		</tr>
	</table>

</ww:if>
</ww:if>
<ww:else>
	<ul id="errorList" style="color: red" class="msgList_">
		<li><ds:lang text="BrakUprawnienDoEdycjiProfilu"/></li>
	</ul>
</ww:else>

	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>