<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1><ds:lang text="Raport przekroczonych limit�w"/></h1>
<p></p>

<ds:available test="!layout2">
<ds:xmlLink path="Calendars"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="BilingAdmin"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="BilingsForm" name="BilingsForm" action="<ww:url value="'/help/biling-reports.action'"/>" method="post">
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />	
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	
	<!-- WYSZUKIWARKA -->
<table class="tableMargin">
<tr>
	<td align="right">
		<ds:lang text="TypTelefonu" />:
	</td>
	<td>
		<ww:select name="'type'" list="phoneTypes" listKey="key" id="type"
			listValue="value" cssClass="'sel'" cssStyle="'width: 150px;'" onchange="'changeType()'" />
	</td>
	<td align="right">
		<ds:lang text="Limit" />:
	</td>
	<td>
		<ww:textfield id="limit" name="'limit'" maxlength="50" size="15" />
	</td>
	
</tr>
<tr>
	<td align="right">
		<ds:lang text="ZakresDatRozmow" />
	</td>
	<td align="right">
		
		<ds:lang text="DataOd" />:
		<ww:textfield name="'period.startDate'"  size="10" maxlength="10" cssClass="'txt'" id="startDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "startDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
	</td>
	<td align="right">
		<ds:lang text="DataDo" />:
	</td>
	<td>
		<ww:textfield name="'period.endDate'"  size="10" maxlength="10" cssClass="'txt'" id="endDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="endDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "endDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "endDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
	</td>
</tr>
<tr>
	<td>
	<td colspan="6" align="right">
		<ds:submit-event value="'GenerujRaport'" name="'doSearch'" cssClass="'btn'"/>
	</td>
</tr>
</table>
	<table class="mediumTable search userBilling" >
		<tr>
			<th>
				<ds:lang text="Uzytkownik" />
			</th>
			<th>
				<ds:lang text="Limit" />
			</th>			
			<th>
				<ds:lang text="Suma" />
			</th>
			<th>
				<ds:lang text="Przekroczono" />
			</th>
		</tr>
		<ww:iterator value="result">
		<tr>
			<td>
				<ww:property value="user" />
			</td>
			<td>
				<ds:format-currency value="limit"/>
			</td>
			<td>
				<ds:format-currency value="netto"/>
			</td>
			<td>
				<ds:format-currency value="exceed"/>
			</td>
		</tr>
		</ww:iterator>
	</table>
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
<script type="text/javascript">

function changeType()
{
	if(document.getElementById('type').selectedIndex == 'MOBILE')
	{
		document.getElementById('limit').disabled = true;
	}
	else
	{
		document.getElementById('limit').disabled = false;
	}
}
</script>
</form>





	