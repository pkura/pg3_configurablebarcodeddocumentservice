<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="user.Firstname"/>&nbsp;<ww:property value="user.Lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<form id="form" action="<ww:url value='baseLink'/>" method="post">
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	<table class="mediumTable search">
	<tr>
		<th>Lp</th>
		<th>Dokument</th>
	</tr>
	<tr>
		<td>1</td>
		<td><a href="#">Umowa o prac�</a></td>
	</tr>
	<tr>
		<td>2</td>
		<td><a href="#">Aneks</a></td>
	</tr>
	<tr>
		<td>3</td>
		<td><a href="#">Warunki prowizji</a></td>
	</tr>
	<tr>
		<td>4</td>
		<td><a href="#">Regulamin pracy</a></td>
	</tr>
	<tr>
		<td>5</td>
		<td><a href="#">Kwestionariusz osobowy</a></td>
	</tr>
	<tr>
		<td>6</td>
		<td><a href="#">R�wne traktowanie&nbsp;(wyci�g z przepis�w)</a></td>
	</tr>
</table>
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>