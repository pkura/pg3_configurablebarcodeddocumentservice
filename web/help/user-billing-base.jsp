<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

	<ww:hidden name="'userImpl.id'" />
	<ww:if test="!adminAction"><!-- bo jesli admin to wybiera z selecta -->
		<ww:hidden name="'userImpl.name'" />
	</ww:if>
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />
	<ww:hidden name="'adminAction'" id="adminAction" />
	<ww:hidden name="'sortField'" id="sortField" />
	<ww:hidden name="'ascending'" id="ascending" />
	<ww:hidden name="'pageNumber'" id="pageNumber" />
	
	<ww:hidden name="'phoneNumber'" id="phoneNumber" />
	<ww:hidden name="'moveToPrivate'" id="moveToPrivate" />
	<ww:hidden name="'removeFromPrivate'" id="removeFromPrivate" />
	<ww:hidden name="'bilingNumber'" id="bilingNumber" />
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
<style>
	td.OFFICE {
		background-color: PaleGoldenRod;
	}
	
	td.MOBILE {
		background-color: PowderBlue;
	}
	
	td.PRIVATE {
		background-color: MistyRose;
	}
</style>
	
	<!-- WYSZUKIWARKA -->
<table class="tableMargin">
<tr>
	<td align="right">
		<ds:lang text="TypTelefonu" />:
	</td>
	<td>
		<ww:select name="'biling.type'" list="phoneTypes" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 150px;'" />
	</td>
	<td align="right">
		<ds:lang text="NumerTelefonu"/>:
	</td>
	<td>
		<ww:textfield name="'biling.phoneNumberFrom'" maxlength="50" size="15"/>
	</td>
	<td align="right">
		<ds:lang text="Numer odbiorcy"/>:
	</td>
	<td>
		<ww:textfield name="'biling.phoneNumberTo'" maxlength="50" size="15" />
	</td>
</tr>
<tr>
	<td align="right">
		<ds:lang text="ZakresDatRozmow" />
	</td>
	<td align="right">
		
		<ds:lang text="DataOd" />:
		<ww:textfield name="'period.startDate'"  size="10" maxlength="10" cssClass="'txt'" id="startDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "startDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
	</td>
	<td align="right">
		<ds:lang text="DataDo" />:
	</td>
	<td>
		<ww:textfield name="'period.endDate'"  size="10" maxlength="10" cssClass="'txt'" id="endDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="endDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "endDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "endDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
	</td>
	<td align="right">
		<ds:lang text="Kierunek" />:
	</td>
	<td>
		<ww:textfield name="'biling.direction'" maxlength="50" size="15" />
	</td>
</tr>
<tr>
	<td colspan="2" align="right">
		<ds:lang text="LiczbaRezultatowNaStronie" />:
		<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel'" />
	</td>
	<td align="right">
		<ds:lang text="NumerBilingu"/>:
	</td>
	<td>
		<ww:textfield name="'biling.bilingNumber'" maxlength="50" size="15"/>
	</td>
	<ww:if test="adminAction">
	<td align="right">
		<ds:lang text="Uzytkownik"/>:
	</td>
	<td>
		<ww:select name="'userImpl.name'" list="users" headerKey="" headerValue="'-----'" listKey="name" listValue="asLastnameFirstname()" cssClass="'sel floatLeft'"/>
	</td>
	</ww:if>
	</td>
	<td>
	<td colspan="6" align="right">
		<ds:submit-event value="'Filtruj wpisy'" name="'doFilter'" cssClass="'btn'"/>
	</td>
</tr>
</table>
	<!-- END / WYSZUKIWARKA -->
	
	<table id="mainTable" class="mediumTable search" cellspacing="0" >
		<!--  <caption>Poniedziałek 20.02.10</caption> -->
		<tr>
			<th>
				<a href="#" class="sortBy" name="dateTime" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
				<ds:lang text="DataICzas" />
				<a href="#" class="sortBy" name="dateTime" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
			</th>
			<th>
				<ds:lang text="TypTelefonu" />
			</th>
			<th>
				<ds:lang text="NumerTelefonuZ" />
			</th>
			<th>
				<ds:lang text="NumerTelefonuDo" />
			</th>
			<th>
				<a href="#" class="sortBy" name="timePeriod" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
				<ds:lang text="CzasTrwania" />
				<a href="#" class="sortBy" name="timePeriod" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
			</th>
			<th>
				<a href="#" class="sortBy" name="netto" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
				<ds:lang text="Wartosc" />
				<a href="#" class="sortBy" name="netto" ascending="false">
					<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
				</a>
			</th>
		<!--	<th>
				<ds:lang text="LiczbaImpulsow" />
			</th>-->
			<th>
				<ds:lang text="Kierunek" />
			</th>
		<!--	<th>
				<ds:lang text="GPRS" />
			</th>-->
			<th>&nbsp;</th>
		</tr>
		<ww:iterator value="bilings">
		<tr>
			<td>
				<ds:format-date pattern="dd-MM-yyyy HH:mm:ss" value="dateTime" />
			</td>
			<td class="<ww:property value="type" />" >
				<ww:if test="type == 'OFFICE'"><ds:lang text="Sluzbowy" /></ww:if>
				<ww:if test="type == 'MOBILE'"><ds:lang text="Komorkowy" /></ww:if>
				<ww:if test="type == 'PRIVATE'"><ds:lang text="Prywatny" /></ww:if>
			</td>
			<td>
				<ds:format-phone-number value="phoneNumberFrom"/>
			</td>
			<td>
				<ds:format-phone-number value="phoneNumberTo"/>
			</td>
			<td>
				<ds:format-seconds-time value="timePeriod" />
			</td>
			<td>
				<ds:format-currency value="netto"/>
			</td>
		<!--
		 	<td>
				<ww:property value="impulses" />
			</td>
		 -->
			<td>
				<ww:property value="direction" />
			</td>
		<!--
			<td>
				<ww:property value="GPRS" />
			</td>
		-->
			<td>
				<ww:if test="type != 'PRIVATE'">
					<a href="#" onclick="$j('#moveToPrivate').attr('value', 'true'); $j('#phoneNumber').attr('value', '<ww:property value="phoneNumberTo" />')
					$j('#bilingNumber').attr('value', '<ww:property value="bilingNumber" />');document.forms[0].submit()">
						<ds:lang text="PrzeniesDoPrywatnych" />
					</a>
				</ww:if>
				<ww:else>
					<a href="#" onclick="$j('#removeFromPrivate').attr('value', 'true'); $j('#phoneNumber').attr('value', '<ww:property value="phoneNumberTo" />')
					$j('#bilingNumber').attr('value', '<ww:property value="bilingNumber" />');document.forms[0].submit()">
						<ds:lang text="UsunZPrywatnych" />
					</a>
				</ww:else>
			</td>
		</tr>
		</ww:iterator>
		<tr>
			<td>Suma kosztów = <ww:property value="sum"/></td>
		</tr>
	</table>
	
	<table class="table100p">
	<tr> 
		<td align="center">
			<ww:set name="pager" scope="request" value="pager"/>
				<jsp:include page="/pager-links-include.jsp"/>
				<script type="text/javascript">
					$j('.pager a').click(function() {
						var page = $j(this).attr('href');
						if (page == '#')
							return false;
						$j('#pageNumber').attr('value', page);
						document.forms.BilingsForm.submit();
						return false;
					});
				</script>
		</td>
	</tr>
	</table>
	
	<!-- SORTOWANIE DANYCH i inne -->
	<script type="text/javascript">
		var jqImg = $j('a[name=' + $j('#sortField').val() + '][ascending=' + $j('input[name=ascending]').val() + ']').children('img');
		var newImgSrc = jqImg.attr('src');
		newImgSrc = newImgSrc.replace('.gif', '-red.gif');
		jqImg.attr('src', newImgSrc);
	
		$j('.sortBy').click(function(){
				$j('#sortField').attr('value', $j(this).attr('name'));
				$j('#ascending').attr('value', $j(this).attr('ascending'));
				document.forms.BilingsForm.submit();
				return false;
			});

		bindLabelDecoration();
	</script>
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>