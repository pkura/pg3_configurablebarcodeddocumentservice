<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>
<p></p>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/help/edit-user-supervisor.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'userImpl.id'" />
	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />
	<ww:hidden name="'dguid'" />

	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	
	<p>
	<input type="button" class="btn"
		onclick="javascript:location.href = '<ww:url value="'/help/user-info.action'"><ww:param name="'userImpl.name'" value="userImpl.name" /></ww:url>'" 
		value="<ds:lang text="PodgladProfilu" />" /><br />
	</p>
	
<ww:if test="editable">

	<h3><ds:lang text="WybierzPrzelozonego" /><br /></h3>

	<div style="float: left;">
		<ww:property value="htmlTree" escape="false" />
	<ds:submit-event value="'Dodaj przełożonych'" name="'selectSupervisors'" cssClass="'btn'"/>
	</div>
	
	<ww:if test="supervisorList != null">
	<table class="search" cellspacing="15" style="float: left; margin-left: 50px;">
		<tr>
			<th>
				<ds:lang text="Usun" />
			</th>
			<th>
				<ds:lang text="NazwaUzytkownika" />
			</th>
			<th>
				<ds:lang text="Nazwisko" />
			</th>
			<th>
				<ds:lang text="Imie" />
			</th>
		</tr>
	<ww:iterator value="supervisorList"> 
		<tr>
			<td>
			 	<ww:checkbox name="'supervisorNames'" fieldValue="name" value="false" />
			</td>
			<td>
				<ww:property value="name" />
			</td>
			<td>
				<ww:property value="lastname" />
			</td>
			<td>
				<ww:property value="firstname" />
			</td>
		</tr>
	</ww:iterator>
	
		<tr>
			<td colspan="4" style="text-align: left;">
				<ds:submit-event value="'Usuń przełożonych'" name="'deleteSupervisors'" cssClass="'btn'"/>
			</td>
		</tr>
	</table>
	</ww:if>
	
</ww:if>
<ww:else>
	<ul id="errorList" style="color: red" class="msgList_">
		<li><ds:lang text="BrakUprawnienDoEdycjiProfilu"/></li>
	</ul>
</ww:else>

	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>