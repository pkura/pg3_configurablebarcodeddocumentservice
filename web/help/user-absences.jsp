<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ww:property value="userImpl.firstname"/>&nbsp;<ww:property value="userImpl.lastname"/></h1>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/help/user-absences.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'userImpl.id'" />
	<ww:hidden name="'userImpl.name'" />
	<ww:hidden name="'tid'" />
	<ww:hidden name="'eid'" />

	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>


<ww:if test="empCard != null">
	<table class="search mediumTable" style="margin-bottom: 50px;">
	<tr>
		<th>
			<ds:lang text="WybierzRok" />
		</th>
		<th>
			<ds:lang text="WybierzKartePracownikaZListy"/>	
		</th>
		<th>&nbsp;</th>
	</tr>
	<tr>
		<td>
			<ww:select name="'year'" list="yearsMap" listKey="key" 
				listValue="value" cssClass="'sel'" cssStyle="'width: 100px;'" />
		</td>
		<td>
			<ww:select name="'empCard.id'" list="employeeCardsMap" listKey="key" 
				listValue="value" cssClass="'sel'" cssStyle="'width: 150px;'" />
		</td>
		<td>
			<ds:submit-event value="'Wy�wietl wyb�r'" name="'none'" cssClass="'btn'"/>
		</td>
	</tr>
	</table>
</ww:if>


<ww:if test="emplAbsences != null">

<!-- LISTA URLOP�W PRACOWNIKA -->
<table class="search mediumTable">

<tr>
	<th>
		<ds:lang text="Lp"/>	
	</th>
	<th>
		<ds:lang text="NazwiskoImiePracownika"/>	
	</th>
	<th>
		<ds:lang text="DataOd"/>
	</th>
	<th>
		<ds:lang text="DataDo"/>
	</th>
	<th>
		<ds:lang text="LiczbaDni"/>
	</th>
	<th>
		<ds:lang text="TypUrlopu"/>
	</th>
	<th>
		<ds:lang text="PozostaloDniUrlopu"/>
	</th>
	<th>
		<ds:lang text="DodatkoweUwagi"/>
	</th>
</tr>
<ww:iterator value="emplAbsences">
<tr>
	<td>
		<ww:property value="lp"/>
	</td>
	<td>
		<ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />
	</td>
	<td>
		<ds:format-date pattern="dd-MM-yyyy" value="absence.startDate"/>
	</td>
	<td>
		<ds:format-date pattern="dd-MM-yyyy" value="absence.endDate"/>
	</td>
	<td>
		<ww:property value="absence.daysNum"/>
	</td>
	<td>
		<ww:property value="absence.absenceType.name"/>
	</td>
	<td>
		<ww:property value="restOfDays"/>
	</td>
	<td>
		<ww:property value="absence.info"/>
	</td>
</tr>
</ww:iterator>
</table>
<!-- END / LISTA URLOP�W PRACOWNIKA -->

</ww:if>


	<div style="clear: both; height: 20px;"></div>	
	
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
	
</form>