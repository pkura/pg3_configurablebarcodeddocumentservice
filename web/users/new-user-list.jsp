<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>



<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="userList.h1_a"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/users/new-user-list.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ww:hidden name="offset"/>
    <ww:hidden name="sortField"/>
    <ww:hidden name="ascending"/>
    <table class="search tableMargin">
        <tr>
            <th>
                <nobr>
                    <a href="<ww:url value="getSortLink('name', true)"/>">
                        <img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
                    <ds:lang text="NazwaUzytkownika"/>
                    <a href="<ww:url value="getSortLink('name', false)"/>">
                        <img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
                </nobr>
            </th>
            <th>
                <nobr>
                    <a href="<ww:url value="getSortLink('firstname', true)"/>">
                        <img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
                    <ds:lang text="Imie"/>
                    <a href="<ww:url value="getSortLink('firstname', false)"/>">
                        <img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
                </nobr>
            </th>
            <th>
                <nobr>
                    <a href="<ww:url value="getSortLink('lastname', true)"/>">
                        <img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
                    <ds:lang text="Nazwisko"/>
                    <a href="<ww:url value="getSortLink('lastname', false)"/>">
                        <img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
                </nobr>
            </th>
            <th></th>
            <th></th>
        </tr>
        <ww:iterator value="results">
            <tr>
                <td>
                    <a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
                        <ww:property value="name"/></a>
                </td>
                <td>
                    <a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
                        <ww:property value="firstname"/></a>
                </td>
                <td>
                    <a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
                        <ww:property value="lastname"/></a>
                </td>
                <td>
                    <a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
                        <ds:lang text="Modyfikuj"/></a>
                    /
                    <a href="<ww:url value="'/users/delete-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
                        <ds:lang text="Usun"/></a>
                </td>
            </tr>
        </ww:iterator>
        <tr>
            <td colspan="4">
            <ww:set name="pager" scope="request" value="pager" />
            <div class="alignCenter line25p">
                <jsp:include page="/pager-links-include.jsp"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ds:lang text="FragmentNazwyUzytkownikaLubNazwiska"/>:
            </td>
            <td colspan="2">
                <ww:textfield name="'name'" value="name"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ds:lang text="LiczbaDniJakaUzytkownikSieNieLogowal"/>:
            </td>
            <td colspan="2">
                <ww:textfield name="'days'" value="days"/>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;&nbsp;
            </td>
            <td>
                <ds:submit-event value="'Szukaj'" name="Szukaj"/>
            </td>
        </tr>
        <tr>
            <td>
                <ds:button-redirect href="'/users/create-new-user.action'" value="getText('NowyUzytkownik')"/>
            </td>
        </tr>
    </table>
</form>