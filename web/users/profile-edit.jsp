<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiemh
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>
<!--N profile-edit.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><c:out value="${profilename}"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p>
	<ds:xmlLink path="ModyfikacjaProfilu"/>
</p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/profile-edit">
	<html:hidden property="profilename"/>
	<html:hidden property="profileId"/>
	
	<c:if test="${!empty profilename}">
		<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>

		<table class="tableMargin">
			<tr>
				<th>
					<ds:lang text="RoleArchiwum"/>
				</th>
				
				<c:if test="${!empty groups}">
					<th>
						<ds:lang text="Grupy"/>
					</th>
				</c:if>
				
				<ds:available test="officeRoles">
					<th>
						<ds:lang text="RoleKancelarii"/>
					</th>
				</ds:available>
			</tr>
			<tr>
				<td class="alignTop">
					<table class="tableMargin">
						<tr>
							<td>
								<ds:lang text="Administrator"/>:
							</td>
							<td>
								<html:multibox property="roles" value="admin"/>
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="AdministratorDanych"/>:
							</td>
							<td>
								<html:multibox property="roles" value="data_admin"/>
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="TylkoArchiwum"/>:
							</td>
							<td>
								<html:multibox property="roles" value="archive_only"/>
							</td>
						</tr>
					</table>
				</td>
				
				<c:if test="${!empty groups}">
					<td class="alignTop">
						<table class="tableMargin">
							<c:forEach items="${groups}" var="group">
								<tr>
									<td>
										<c:out value="${group.name}"/>:
									</td>
									<td>
										<html-el:multibox property="groups" value="${group.guid}"/>
									</td>
								</tr>
							</c:forEach>
						</table>
					</td>
				</c:if>
				
				<ds:available test="officeRoles">
					<td class="alignTop">
						<table class="tableMargin">
							<c:forEach items="${officeRoles}" var="role">
								<tr>
									<td>
										<c:out value="${role.label}"/>:
									</td>
									<td>
										<html-el:multibox property="officeRoles" value="${role.value}"/>
									</td>
								</tr>
							</c:forEach>
						</table>
					</td>
				</ds:available>
			</tr>
		</table>
		<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
	</c:if>	
</html:form>

<%--
<edm-html:select-string-manager name="jspRepositoryLocalStrings" />
<edm-html:errors />
<edm-html:messages />

<html:form action="/users/profile-edit">
<html:hidden property="profilename"/>
<h1><c:out value="${profilename}"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p><ds:xmlLink path="ModyfikacjaProfilu"/></p>

<c:if test="${!empty profilename}">
<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
<table>
<tr>
	<th><ds:lang text="RoleArchiwum"/></th>
	<th><ds:lang text="Grupy"/></th>
	<ds:modules test="coreoffice">
		<th><ds:lang text="RoleKancelarii"/></th>
	</ds:modules>
</tr>
<tr>
	<td valign="top">
		<table>
		<tr>
			<td><ds:lang text="Administrator"/>:</td>
			<td><html:multibox property="roles" value="admin"/></td>
		</tr>
		<tr>
			<td><ds:lang text="AdministratorDanych"/>:</td>
			<td><html:multibox property="roles" value="data_admin"/></td>
		</tr>
		</table>
	</td>
	<td valign="top">
		<c:if test="${!empty groups}">
			<table>
			<c:forEach items="${groups}" var="group">
				<tr>
					<td><c:out value="${group.name}"/>:</td>
					<td><html-el:multibox property="groups" value="${group.guid}"/></td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
	</td>
	<ds:modules test="coreoffice">
		<td valign="top">
			<table>
				<c:forEach items="${officeRoles}" var="role">
					<tr>
						<td><c:out value="${role.label}"/>:</td>
						<td><html-el:multibox property="officeRoles" value="${role.value}"/></td>
					</tr>
				</c:forEach>
			</table>
		</td>
	</ds:modules>
</tr>
</table>
<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
</c:if><br>
</html:form>
--%>
<!--N koniec profile-edit.jsp N-->