<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N view-users.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="Certifikat"/>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
	<ds:xmlLink path="UserEdit"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UserEdit"/>
	</div>
	
</ds:available>


<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/users/user-certificate.action'"/>" method="post"
onsubmit="disableFormSubmits(this);" enctype="multipart/form-data">

	<ww:hidden name="'username'" value="username"/>
	<input type="hidden" name="doSaveCert" id="doSaveCert"/>
	<input type="hidden" name="doDeleteCert" id="doDeleteCert"/>
	<input type="hidden" name="doUpdate" id="doUpdate"/>
		
	<ds:available test="layout2">
	</ds:available>


	<br/>

	<ww:if test="certificates != null">
		<h3><ds:lang text="ListaCertyfikatowUzytkownika"/></h3>
		
		<table class="tableMargin">
			<tr>
				<th></th>
				<th colspan="2">
					<ds:lang text="Certyfikat"/>
				</th>
			</tr>
			<ww:iterator value="certificates" status="cert">			
				<tr>
					<td class="alignTop">
						<input type="checkbox" name="deleteCerts" value="<ww:property value="encoded64"/>"/>
					</td>
					<td>
						<ww:property value="dn"/>
						<span class="italic"><ds:lang text="wydanyPrzez"/></span>
						<ww:property value="issuerDn"/>)
					</td>
					<td>
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?username=<ww:property value="username"/>&amp;signature=<ww:property value="signature64"/>">DER</a>
						/
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?pem=true&amp;username=<ww:property value="username"/>&amp;signature=<ww:property value="signature64"/>">PEM</a>
					</td>
				</tr>
			</ww:iterator>
		
		</table>
		
		<input type="submit" class="btn" name="doDeleteCert" value="<ds:lang text="Usun"/>" onclick="document.getElementById('doDeleteCert').value='true';"/>
		
	</ww:if>
	<p></p>
	
	 <table width="350" border="0" cellpadding="3" cellspacing="0">
      <tr>
        <td colspan="2">
        	</br></br>
        	<h4><ds:lang text="DodajCertyfikat"/></h4>
        	<input type="file" name="FileName" value="Browse..." size="30"/>
        	<input type="submit" value="Dodaj" name="doSaveCert"  class="btn" onclick="document.getElementById('doSaveCert').value='true';">        
        </td>
      </tr>
      <tr>
      <tr>
      	<td>
      		</br></br>
      		<h4><ds:lang text="SposobLogowania"/></h4>
      	
      		<ww:select name="'level'" list="authLevels" value="level" cssClass="'sel'" />
      		<input type="submit" value="Zapisz" name="doUpdate"  class="btn" onclick="document.getElementById('doUpdate').value='true';">     	
      	</td>
      </tr>
      </table>
	



	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
		</ds:available>
</form>

