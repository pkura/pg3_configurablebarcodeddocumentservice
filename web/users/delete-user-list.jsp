<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<h1><ds:lang text="UzytkownicyUsunieci"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p> 
    <ds:ww-action-errors/> 
    <ds:ww-action-messages/>
</p>
<form action="<ww:url value="'/admin/delete-user-list.action'"/>" method="post" name="MainForm">
	<table>
			<tr>
				<th>
					<nobr>		
						<ds:lang text="NazwaUzytkownika"/>
					</nobr>
				</th>
				<th>
					<nobr>
						<ds:lang text="Imie"/>
					</nobr>
				</th>
				<th>
					<nobr>
						<ds:lang text="Nazwisko"/>
					</nobr>
				</th>
			<th></th>
		</tr>
		<ww:iterator value="users">
			<tr>
				<td>
					<a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
						<ww:property value="name"/></a>
				</td>
				<td>
										<a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
						<ww:property value="firstname"/></a>
				</td>
				<td>
										<a href="<ww:url value="'/users/view-user.do'"><ww:param name="'username'" value="name"/></ww:url>">
						<ww:property value="lastname"/></a>
				</td>
				<td>
					<a href="<ww:url value="'/admin/delete-user-list.action'"><ww:param name="'username'" value="name"/><ww:param name="'doRevert'" value="'true'"/></ww:url>">
						Przywr��</a>
				</td>
			</tr>
		</ww:iterator>
	</table>
</form>