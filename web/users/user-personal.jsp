<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N user-personal.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=" : ${userPersonal.username}"/>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
	<ds:xmlLink path="UserEdit"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UserEdit"/>
	</div>
	
</ds:available>

<script type="text/javascript">
		var __manualSelectedTab = true;
</script>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/user-personal">
	<html:hidden property="username"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="DataUtworzenia"/>:
			</td>
			<td>
				<c:out value="${userPersonal.creationTime}"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>:
			</td>
			<td>
				<c:out value="${userPersonal.username}"/>
			</td>
		</tr>
		<tr>
			<td><ds:lang text="NazwaAlternatywna" />:</td>
			<td>
				<ds:available test="userPersonal.externalName.readonly">
					<html:text property="externalName" size="50" maxlength="50" styleClass="txt" readonly="true" />
				</ds:available> <ds:available test="!userPersonal.externalName.readonly">
					<html:text property="externalName" size="50" maxlength="50" styleClass="txt" />
				</ds:available>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
				<span class="star">*</span>:
			</td>
			<td>   
			<ds:available test="admin.useredit.disable">
				    <html:text property="firstname" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="firstname" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
				<span class="star">*</span>:
			</td>
			<td><ds:available test="admin.useredit.disable">
				   <html:text property="lastname" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="lastname" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		<ds:dockinds test="prosika">
		<tr>
			<td>
				<ds:lang text="PESEL"/>:
			</td>
			<td>
				<html:text property="pesel" size="11" maxlength="11" styleClass="txt"/>
			</td>
		</tr>
		</ds:dockinds>
		
		<ds:modules test="coreoffice">
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
				<ds:available test="admin.useredit.disable">
				   <html:text property="identifier" size="5" maxlength="20" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
					<html:text property="identifier" size="5" maxlength="20" styleClass="txt"/>
					</ds:available>
				</td>
			</tr>
		</ds:modules>
		
		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				  <html:text property="email" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="email" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
	
		<!-- nowe pola RS -->
		<tr>
			<td>
				<ds:lang text="TelStac"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				 <html:text property="phoneNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="phoneNum" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		<ds:available test="userPersonal.extension.show">
			<tr>
				<td>
					<ds:lang text="NumWew"/>:
				</td>
				<td>
					<ds:available test="userPersonal.extension.readonly">
						<html:text property="extension" size="50" maxlength="50" styleClass="txt" readonly="true" />
					</ds:available>
					<ds:available test="!userPersonal.extension.readonly">
						<html:text property="extension" size="50" maxlength="50" styleClass="txt"/>
					</ds:available>
				</td>
			</tr>
		</ds:available>
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				 <html:text property="mobileNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="mobileNum" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				 <html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumerPokoju"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				 <html:text property="roomNum" size="50" maxlength="30" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
				<html:text property="roomNum" size="50" maxlength="30" styleClass="txt"/>
				</ds:available>
			</td>
		</tr>
		
		<!-- ILPOL stopka do maila -->
		<ds:available test="mail.footer">
        <tr>
            <td><ds:lang text="StopkaMailowa"/>:</td>
            <td>
                <html:textarea property="mailFooter" cols="30" styleClass="txt"/>
            </td>
        </tr>
        </ds:available>
		<!-- nowe pola RS -->
		<ds:available test="user.supervisor">
        <tr>
            <td><ds:lang text="Przelozony"/>:</td>
            <td>
                <html:select property="supervisor" styleClass="sel" >
                    <option value=""><ds:lang text="select.wybierz"/></option>
                    <html:optionsCollection name="supervisors"/>
                </html:select>
            </td>
        </tr>
        </ds:available>
		<tr>
			<td>
				<ds:lang text="ZablokowaneLogowanie"/>:
			</td>
			<td><ds:available test="admin.useredit.disable">
				<html:checkbox property="loginDisabled" disabled="true"/>
				  </ds:available>
				  <ds:available test="!admin.useredit.disable">
				  	<html:checkbox property="loginDisabled"/>
				  	 </ds:available>
			</td>
		</tr>
		<ds:available test="active.directory.on">
			<tr>
				<td>
					<ds:lang text="ActiveDirectory"/>:
				</td>
				<td>
					<html:checkbox property="activeDirectory" onchange="changeAD();"/>
					<script language="JavaScript">
						function changeAD()
						{
							var el = document.getElementById('tablePass')
							if(el.style.display == 'none'){
								el.style.display = '';
							} else {
								el.style.display = 'none';
							}
							el = document.getElementById('tableAd')
							if(el.style.display == 'none'){
								el.style.display = '';
							} else {
								el.style.display = 'none';
							}
						}
					</script>
				</td>
			</tr>
		</ds:available>
		<tbody id="tableAd" <c:if test="${!activeDirectory}">style="display:none;"</c:if>>
            <ds:available test="active.directory.urlPerUser">
                <tr>
                    <td>
                        <ds:lang text="AdUrl"/>:
                    </td>
                    <td>
                        <html:text property="adUrl" size="50" maxlength="50" styleClass="txt"/>
                    </td>
                </tr>
            </ds:available>
		</tbody>
		<tbody  id="tablePass" <c:if test="${activeDirectory}">style="display:none;"</c:if>>
            <tr>
                <td>
                    <ds:lang text="Haslo"/>:
                </td>
                <td>
                <ds:available test="admin.useredit.disable">
				<html:password property="password" value="" size="50" maxlength="64" styleClass="txt_opt" disabled="true"/>
				  </ds:available>
				     <ds:available test="!admin.useredit.disable">
                    <html:password property="password" value="" size="50" maxlength="64" styleClass="txt_opt"/>
                      </ds:available>
                </td>
            </tr>
            <tr>
                <td>
                    <ds:lang text="WeryfikacjaHasla"/>:
                </td>
                <td>
                 <ds:available test="admin.useredit.disable">
				 <html:password property="password2" size="50" maxlength="64" styleClass="txt_opt" disabled="true"/>
				  </ds:available>
				   <ds:available test="!admin.useredit.disable">
                    <html:password property="password2" size="50" maxlength="64" styleClass="txt_opt"/>
                      </ds:available>
                </td>
            </tr>
		</tbody>
		<ds:dockinds test="prosika">
		<tr>
			<td>
				<ds:lang text="Uwagi"/>:
			</td>
			<td>
				<html:textarea property="remarks" cols="50" rows="3" styleClass="txt"/>
			</td>
		</tr>
		</ds:dockinds>
	</table>
	
	<html:submit property="doUpdate" styleClass="btn saveBtn" ><ds:lang text="doUpdate"/></html:submit>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</html:form>


<!--N koniec user-personal.jsp N-->