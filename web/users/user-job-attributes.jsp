
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=" : ${userJobAttributes.username}"/>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:xmlLink path="UserEdit"/>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/user-job-attributes">
	<html:hidden property="username"/>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="KPX"/>:
			</td>
			<td>
				<html:text property="kpx" size="20" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Przelozony"/>:
			</td>
			<td>
				<html:select property="superior" size="1" >
					<html:optionsCollection  property="users" label="second" value="first"/>
				</html:select>
				
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="DataWaznosciKonta"/>:
			</td>
			<td>
				<html:text property="validityDate" size="10" styleId="validityDate" styleClass="txt"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="validityDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Lokalizacja"/>:
			</td>
			<td>
				<html:text readonly="true" styleId="locationDesc"  property="locationDesc" styleClass="txt"/>
				<html:hidden styleId="location" property="location" />
				<input type="button" value="<ds:lang text="wybierz"/>" onclick="openDictionary()" 
				 class="btn" />
				<script>
				Calendar.setup({
					inputField	 :	"validityDate",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"validityDateTrigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
				
							function openDictionary()
							{
								/*
								var selObj = document.getElementById('locationSelect');
								var selectedIndex = selObj.selectedIndex;
								if(selectedIndex == null)
								{
									return;
								}
								var idSel = selObj.options[selectedIndex].value;
								*/
								var idSel = document.getElementById('location').value;
								openToolWindow('<ww:url value="\'/office/common/diclocation.action\'"></ww:url>'+'?id='+idSel,
											   'bleble', 400, 350);
							}

							function accept(foo,map)
							{
								/*<ww:property value="dictionaryAccept"/>;
								var sel = document.getElementById('locationSelect');
								removeOptions(sel);
								addOption(sel, map.id, map.description);*/
								document.getElementById('locationDesc').value = map.description;
								document.getElementById('location').value = map.id;
							}
		

				</script>
								
									
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Uwagi"/>:
			</td>
			<td>
				<html:textarea property="remarks" cols="50" rows="3" styleClass="txt"/>
			</td>
		</tr>
	</table>
	
	<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
</html:form>
