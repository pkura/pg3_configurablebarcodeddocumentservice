<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<%@ page contentType="text/html; charset=iso-8859-2" %>


<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h1><ds:lang text="ZmianaHasla"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/self-change-password">

<% if (request.getAttribute("passwordChanged") != null) { %>
    <p> <a href="<%= request.getContextPath() %>"><ds:lang text="PowrotDoSystemu"/></a></p>
        
    <p><a href="<ww:url value="'/help/edit-user-info.action'"><ww:param name="'userImpl.name'" value="username"/><ww:param name="'eid'" value="'true'"/></ww:url>"><ds:lang text="PrzejdzDoEdycjiProfilu"/></a></p>
    <p><a href="<%= request.getContextPath() %>/self-change-password.do"><ds:lang text="PonownaZmianaHasla"/></a></p>
<% } else { %>

<%-- @see PasswordPolicy --%>
<p><ds:lang text="NoweHasloMusiMiecCoNajmniej"/> <%= pageContext.findAttribute("minPasswordLength") %>
<ds:lang text="znakow"/>.
<% Integer minDigits = (Integer) pageContext.findAttribute("minDigits");
   Integer minLetters = (Integer) pageContext.findAttribute("minLetters"); 
   Integer minSpecials = (Integer) pageContext.findAttribute("minSpecials"); 
   Integer minSmallLetters = (Integer) pageContext.findAttribute("minSmallLetters"); 
   minDigits = (minDigits==null)?0:minDigits; 
   minLetters = (minLetters==null)?0:minLetters;
   minSpecials = (minSpecials==null)?0:minSpecials;
   minSmallLetters = (minSmallLetters==null)?0:minSmallLetters;
   int constraints = 0; 
   boolean constraint=true;
   if(minDigits+minLetters+minSpecials+minSmallLetters>0)
   {
	   if(minDigits>0) constraints++; 
	   if(minLetters>0) constraints++; 
	   if(minSpecials>0) constraints++; 
	   if(minSmallLetters>0) constraints++; %>
	
		<ds:lang text="HasloMusiZawieracCoNajmniej"/>
	
		<% constraints--;
		 switch(minDigits) { 
	        case 0: 
	           constraint = false;
			   constraints++;
	    	   break; 
		    case 1: %>
			  1 cyfr�
		 	 <% break;
		    case 2: case 3: case 4:%>
			 <%= minDigits %> cyfry 
			 <% break; 
	        default: %>	
		     <%= minDigits %> cyfr <% } 
	
	 if(constraint)
		 if(constraints==1) 
		 {
		 	%><ds:lang text="oraz"/><%
		 }
		 else  
		 {
			 %>,<%
		 }
	
	 constraint=true;
	 constraints--;
	 
	 switch(minSmallLetters) { 
		 case 0: 
		       constraint = false;
			   constraints++;
			   break; 
		 case 1: %>
			  1 ma�� liter�
			 <% break;
		 case 2: case 3: case 4:%>
			 <%= minSmallLetters %> ma�e litery 
			 <% break; 
		 default: %>	
		  <%= minSmallLetters %> ma�ych liter<% } 
	
	 if(constraint)
		 if(constraints==1) 
		 {
		 	%><ds:lang text="oraz"/><%
		 }
		 else  
		 {
			 %>,<%
		 }
	
	 constraint=true;
	 constraints--;
	 
	 switch(minLetters) { 
		 case 0: 
		       constraint = false;
			   constraints++;
			   break; 
		 case 1: %>
			  1 du�� liter�
			 <% break;
		 case 2: case 3: case 4:%>
			 <%= minLetters %> du�e litery 
			 <% break; 
		 default: %>	
		  <%= minLetters %> du�ych liter<% } 
	 
	 if(constraint)
		 if(constraints==1) 
		 {
		 	%><ds:lang text="oraz"/><%
		 }
		 else  
		 {
			 %>,<%
		 }
	
	 constraint=true;
	 constraints--;
	 
	 switch(minSpecials) { 
		 case 0: 
		       constraint = false;
			   constraints++;
			   break; 
		 case 1: %>
			  1 znak specjalny
			 <% break;
		 case 2: case 3: case 4:%>
			 <%= minSpecials %> <ds:lang text="znakiSpecjalne"/>
			 <% break; 
		 default: %>	
		  <%= minSpecials %> <ds:lang text="znakowSpecjalnych"/> <% }	     
	%>.<% 
}%>
		
	
	

<ds:lang text="NiedozwoloneSaHaslaIdentyczneZnazwaUzytkownikaLubZaczynajaceSieOdNazwyUzytkownika"/>.</p>

<table border='0' cellspacing='0' cellpadding='0'>
<tr>
    <td><ds:lang text="StareHaslo"/><span class="star">*</span>:</td>
    <td><html:password property="oldPassword" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
    <td><ds:lang text="Haslo"/><span class="star">*</span>:</td>
    <td><html:password property="password" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
    <td><ds:lang text="WeryfikacjaHasla"/><span class="star">*</span>:</td>
    <td><html:password property="password2" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
    <td></td>
    <td>
        <html:submit property="doChangePassword" styleClass="btn" ><edm-html:message key="doUpdate" global="true"/></html:submit>
<%--        <html:submit property="doCancel" styleClass="cancel_btn"><edm-html:message key="doCancel" global="true" /></html:submit>--%>
    </td>
</tr>
</table>

<% } %>

</html:form>

<% if (request.getAttribute("passwordChanged") == null) { %>
<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
<% } %>