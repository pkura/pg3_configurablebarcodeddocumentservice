<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>
<!--N profile-list.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Profile"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/profile-list">
	<c:if test="${!empty profiles}">
		<table class="tableMargin">
			<tr>
				<th></th>
				<th><ds:lang text="Nazwa"/></th>
			</tr>
			<c:forEach items="${profiles}" var="profile">
				<tr>
					<td>
						<input type="checkbox" name="delProfileNames" value="<c:out value='${profile.id}'/>"/>
					</td>
					<td>
						<a href='<c:out value="${pageContext.request.contextPath}"/>/users/profile-edit.do?profileId=<c:out value='${profile.id}'/>'>
							<c:out value="${profile.name}"/></a>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn"/>
	</c:if>

	<h4><ds:lang text="NowyProfil"/></h4>

	<ds:lang text="Nazwa"/>:
	<input type="text" name="newProfileName" size="30" class="txt"/>
	<input type="submit" name="doCreate" value="<ds:lang text="doCreate"/>" class="btn"/>
</html:form>

<%--
<h1><ds:lang text="Profile"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/users/profile-list">

<c:if test="${!empty profiles}">
	<table>
	<tr>
		<th></th>
		<th><ds:lang text="Nazwa"/></th>
	</tr>
	<c:forEach items="${profiles}" var="profile">
		<tr>
			<td><input type="checkbox" name="delProfileNames" value="<c:out value='${profile}'/>"/></td>
			<td><a href='<c:out value="${pageContext.request.contextPath}"/>/users/profile-edit.do?profilename=<c:out value='${profile}'/>'><c:out value="${profile}"/></a></td>
		</tr>
	</c:forEach>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn"/>
		</td>
	</tr>
	</table>
</c:if>

<h4><ds:lang text="NowyProfil"/></h4>

<table>
<tr>
	<td><ds:lang text="Nazwa"/>:</td>
	<td><input type="text" name="newProfileName" size="30" class="txt"/></td>
	<td><input type="submit" name="doCreate" value="<ds:lang text="doCreate"/>" class="btn"/></td>
</tr>
</table>

</html:form>
--%>
<!--N koniec profile-list.jsp N-->