<%@ page import="pl.compan.docusafe.web.archive.users.NewUserListAction" %>
<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N delete-user.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><edm-html:message key="deleteUser.h1"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/delete-user">
	<html:hidden property="username"/>
	<html:hidden property="offset"/>
	<html:hidden property="limit"/>
	<html:hidden property="sortField"/>
	
	<p>
		Usuwanie użytkownika
		<span class="bold"><c:out value="${userName}"/></span>
	</p>
	<p>Zadania przekazać :</p><p><html:select property="toUsername" styleClass="sel combox-chosen">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
							<html:optionsCollection name="users"/>
						</html:select>
	<html:submit property="doDelete" styleClass="btn">
		<edm-html:message key="doDelete" global="true"/></html:submit>
	<html:submit property="doCancel" styleClass="cancel_btn">
		<edm-html:message key="doCancel" global="true"/></html:submit>
</html:form>
</p>
<p>
    <ds:available test="menu.left.newWebWorkUserList">
        <a href='<%= request.getContextPath() + NewUserListAction.URL %>'>Lista użytkowników</a>
    </ds:available>
    <ds:available test="menu.left.newuserlist">
        <html:link forward="users/user-list">
            Lista użytkowników</html:link>
    </ds:available>
    <ds:available test="menu.left.olduserlist">
        <html:link forward="users/user-list">
            Lista użytkowników</html:link>
    </ds:available>
</p>
<!--N koniec delete-user.jsp N-->