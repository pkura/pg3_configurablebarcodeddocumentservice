<%@ page import="pl.compan.docusafe.web.archive.users.UserListAction"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<c:if test="${days>0}">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
		<ds:lang text="userList.h1_b"/>
	</h1>
</c:if>

<c:if test="${days<=0}">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
		<ds:lang text="userList.h1_a"/>
	</h1>
</c:if>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:messages/>
<edm-html:errors/>

<html:form action="/users/user-list">
	<p>
		<ds:available test="menu.left.olduserlist">
			<input type="button" value="<ds:lang text="NowyUzytkownik"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${newUserLink}'/>';" class="btn">
	   	</ds:available>
		<ds:available test="menu.left.newuserlist">
			<a href='<c:out value="${pageContext.request.contextPath}"/>/users/new-user.do'><ds:lang text="NowyUzytkownik"/></a>
		</ds:available>		
		<c:if test="${!empty query}">
			<input type="button" value="<ds:lang text="PelnaListaUzytkownikow"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}/users/user-list.do'/>';" class="btn">
		</c:if>
		<ds:available test="kancelaria.wlacz.podpisWewnetrzny">
			<input type="button" value="<ds:lang text="GenerujDlaWszystkich"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}/users/user-list.do?generateInternalSignature=true'/>';" class="btn">
	   	</ds:available>
	</p>
	
	<%
	java.lang.Integer limit = new java.lang.Integer(0);
	java.lang.Integer offset = new java.lang.Integer(0);
	%>
	
	<input type="hidden" name="days" <c:out value="${days}"/>/>
	
	<table cellpadding="0" class="search tableMargin">
		<tr>
			<c:if test="${empty query}">
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "name", false) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="NazwaUzytkownika"/>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "name", true) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "firstname", false) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="Imie"/>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "firstname", true) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "lastname", false) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="Nazwisko"/>
						<a href='<%= request.getContextPath() + UserListAction.getLink(offset.intValue(), limit.intValue(), "lastname", true) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
			</c:if>
			<c:if test="${!empty query}">
				<% String query = (String) pageContext.findAttribute("query"); %>
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "name", false, query) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="NazwaUzytkownika"/>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "name", true, query) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "firstname", false, query) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="Imie"/>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "firstname", true, query) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
				<th>
					<nobr>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "lastname", false, query) %>' title="Sortowanie malej�ce"><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0" /></a>
						<ds:lang text="Nazwisko"/>
						<a href='<%= request.getContextPath() + UserListAction.getSearchLink(offset.intValue(), limit.intValue(), "lastname", true, query) %>' title="Sortowanie rosn�ce"><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0" /></a>
					</nobr>
				</th>
			</c:if>
			<th></th>
			<th></th>
		</tr>
		<c:forEach items="${userBeans}" var="user">
			<tr>
				<td>
					<a href='<c:out value="${pageContext.request.contextPath}${user.editLink}"/>'>
						<c:out value="${user.name}"/></a>
				</td>
				<td>
					<a href='<c:out value="${pageContext.request.contextPath}${user.editLink}"/>'>
						<c:out value="${user.firstname}"/></a>
				</td>
				<td>
					<a href='<c:out value="${pageContext.request.contextPath}${user.editLink}"/>'>
						<c:out value="${user.lastname}"/></a>
				</td>
				<td>
					<a href='<c:out value="${pageContext.request.contextPath}${user.editLink}"/>'>
						<ds:lang text="Modyfikuj"/></a>
				</td>
				<td>
					&nbsp;&nbsp;
					<a href='<c:out value="${pageContext.request.contextPath}${user.deleteLink}"/>'>
						<ds:lang text="Usun"/></a>
				</td>
			</tr>
		</c:forEach>
		<tr>
			<td colspan="4" class="alignCenter line25p">
				<jsp:include page="/pager-links-include.jsp"/>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
	function xyz2()
	{
		var x = 'asd';
		//var field = document.getElementById('selectedUser');
	//	var selectedUserName = field.value;
		document.location = '<%= request.getContextPath()%>"/users/view-user.do?username="+selectedUserName;
	} 
	</script>
</html:form>
	<input type="hidden" name="days" <c:out value="${days}"/>/>
<html:form action="/users/user-list.do?days=" method="post" >
		<c:if test="${days>0}">
			<ds:lang text="IleDni"/>:
			<input id="inpChangeDays" maxlength="5" width="5" value="<c:out value="${days}"/>"/>
			<input type="button" id="btnChangeviewLink" class="btn" onclick="javascript:changeDays()" value="<ds:lang text="Pokaz"/>"/>
			
			<br/>
			<br/>
			<input type="button" value="<ds:lang text="PokazWszystkichUzytkownikow"/>" onclick="javascript:changeView()" class="btn"/>
		</c:if>
		<c:if test="${days<=0}">
		
		  <h3><ds:lang text="PrzegladajWybranegoUzytkownika"/></h3>
			 <html:select  property="selectedUser" styleClass="sel combox-chosen" disabled="false">
				<html:optionsCollection name="usersList" />
				</html:select>
			<input type="submit" name="doSearch" id="doSearch" class="btn searchBtn" value="<ds:lang text="Edytuj"/>"/>
			
			<%-- <ds:lang text="FragmentNazwyUzytkownikaLubNazwiska"/>:
			<html:text property="query" size="20" maxlength="62" styleClass="txt" />
						<input type="submit" name="doSearch" id="doSearch" class="btn searchBtn" value="<ds:lang text="Szukaj"/>"/>
		 --%>
			<ds:dockinds test="prosika">
			<html:hidden property="showAdv" styleId="showHideHidden"/>
			<br><br><a href="javascript:showHideAdv()"><i><ds:lang text="showHide"/></i></a>
				<div id="advancedSearch">
					<table>
						<tr>
							<td>
								<ds:lang text="PESEL"/>:
							</td>
							<td>
								<html:text property="pesel" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="KPX"/>:
							</td>
							<td>
								<html:text property="kpx" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="Przelozony"/>:
							</td>
							<td>
								<html:text property="superior" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="Lokalizacja"/>:
							</td>
							<td>
								<html:text property="location" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="DataZalozenia"/>:
							</td>
							<td>
								Od:<html:text styleId="createdFrom" property="createdFrom" size="20" maxlength="30" styleClass="txt" />
								<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="createdFromTrigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
								Do:<html:text property="createdTo" styleId="createdTo" size="20" maxlength="30" styleClass="txt" />
								<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="createdToTrigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
							</td>
						</tr>
						<tr>
							<td>
									<ds:lang text="DataWaznosciKonta"/>:
							</td>
							<td>
								Od:<html:text property="validFrom" styleId="validFrom" size="20" maxlength="30" styleClass="txt" />
									<img src="<ww:url value="'/calendar096/img.gif'"/>"
									id="validFromTrigger" style="cursor: pointer; border: 1px solid red;"
									title="Date selector" onmouseover="this.style.background='red';"
									onmouseout="this.style.background=''"/>
								Do:<html:text property="validTo" styleId="validTo" size="20" maxlength="30" styleClass="txt" />
									<img src="<ww:url value="'/calendar096/img.gif'"/>"
									id="validToTrigger" style="cursor: pointer; border: 1px solid red;"
									title="Date selector" onmouseover="this.style.background='red';"
									onmouseout="this.style.background=''"/>
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="TelefonStacjonarny"/>:
							</td>
							<td>
								<html:text property="nomadicPhone" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="TelefonKomorkowy"/>:
							</td>
							<td>
								<html:text property="cellPhone" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="Email"/>:
							</td>
							<td>
								<html:text property="email" size="20" maxlength="30" styleClass="txt" />
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="AktywnoscKont"/>:
							</td>
							<td>
								<html:select property="inactive"  styleClass="txt" >
									<html:option value=""><ds:lang text="AktywneNieaktywne"/></html:option>
									<html:option value="false"><ds:lang text="TylkoAktywne"/></html:option>
									<html:option value="true"><ds:lang text="TylkoNieaktywne"/></html:option>
								</html:select>
							</td>
						</tr>
					</table>
					
				</div>
			<script type="text/javascript">
			applyVisibility();
		

			if($j('#validFrom').length > 0)
			{
				Calendar.setup({
					inputField	 :	"validFrom",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"validFromTrigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
			}
	
			if($j('#validTo').length > 0)
			{
				Calendar.setup({
					inputField	 :	"validTo",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"validToTrigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
			}
	
			if($j('#createdFrom').length > 0)
			{
				Calendar.setup({
					inputField	 :	"createdFrom",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"createdFromTrigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
			}
	
			if($j('#createdTo').length > 0)
			{
				Calendar.setup({
					inputField	 :	"createdTo",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"createdToTrigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
			}

			function applyVisibility() {
				if (document.getElementById('showHideHidden').value == 'false') {
					document.getElementById('advancedSearch').style.display = 'none';
				} else if (document.getElementById('showHideHidden').value == 'true') {
					document.getElementById('advancedSearch').style.display = '';
				} else {
					document.getElementById('advancedSearch').style.display = 'none';
				}
			}
			
			function showHideAdv() {
				if (document.getElementById('advancedSearch').style.display == '') {
					document.getElementById('advancedSearch').style.display = 'none';
					document.getElementById('showHideHidden').value = 'false';
				} else {
					document.getElementById('advancedSearch').style.display = '';
					document.getElementById('showHideHidden').value = 'true';
				}
			}
			
			</script>
			</ds:dockinds>
			<br/>
			<br/>
			<ds:available test="!organization.users.list.hide.logged.time">
			<input type="button" value="<ds:lang text="PokazUzytkownikowNielogujacychSieOkreslonaIloscCzasu"/>" onclick="javascript:changeView()" class="btn"/>
			</ds:available>
		</c:if>
	</table>
</html:form>

<script type="text/javascript">
	var days = <c:out value="${days}"/> ;
	var events = new Array("query;doSearch");
	var eventMap = new Array();

	function changeView()
	{
		if (days>=0)
			days = -1;
	   else	 	
			days = 30;
		document.location = '<%= request.getContextPath() + UserListAction.getLink()%>?days=' + days;
	}
	function xyz()
	{
		var x = 'asd';
		//var field = document.getElementById('selectedUser');
	//	var selectedUserName = field.value;
		document.location = '<%= request.getContextPath()%>"/users/view-user.do?username="+selectedUserName;
	} 
	function changeDays()
	{
		var ctl = document.getElementById('inpChangeDays');
		days = ctl.value;
		document.location = '<%= request.getContextPath() + UserListAction.getLink()%>?days=' + days;
	}
  
	function form_init()
	{
		for (var i=0; i < events.length; i++)
		{
			var redir = events[i].split(';');
			if (redir.length != 2) continue;
			var ctl = document.getElementById(redir[1]);
			eventMap[redir[0]] = ctl;
		}
	}

	// http://mazinger.technisys.com.ar/pruebas-nick/mozilla/docs/compat.html
	function form_onkeydown(ev)
	{
		if (!ev) ev = event; // ie

		if ((ev.which && ev.which == 13) || (ev.keyCode && ev.keyCode == 13))
		{
			if (ev.stopPropagation)
				ev.stopPropagation(); // mozilla
			else
				ev.cancelBubble = true;

			var field = null;
			if (ev.target) field = ev.target; // mozilla
			else field = ev.srcElement;
			// przycisk, kt�ry powinien by� wci�ni�ty
			var ctl = eventMap[field.name];

			if (ctl) ctl.click();
			return false;
		}
		return true;
	}

	
	

	
	form_init();
	document.forms[1].onkeydown = form_onkeydown;
</script>
<!--N koniec user-list.jsp N-->