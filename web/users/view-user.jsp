<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N view-users.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=" : ${viewUser.username}"/>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
	<ds:xmlLink path="UserEdit"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UserEdit"/>
	</div>
	
</ds:available>

<script type="text/javascript">
		var __manualSelectedTab = true;
</script>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/view-user">
	<html:hidden property="username"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>:
			</td>
			<td>
				<c:out value="${viewUser.username}"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
			</td>
			<td>
				<c:out value="${viewUser.firstname}"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
			</td>
			<td>
				<c:out value="${viewUser.lastname}"/>
			</td>
		</tr>

		<ds:modules test="coreoffice">
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
					<c:out value="${viewUser.identifier}"/>
				</td>
			</tr>
		</ds:modules>
		<ds:dockinds test="prosika">
			<tr>
				<td>
					<ds:lang text="DataWaznosciKonta"/>:
				</td>
				<td>
					<c:out value="${viewUser.validityDate}"/>
				</td>
			</tr>
		</ds:dockinds>
		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
				<c:out value="${viewUser.email}"/>
			</td>
		</tr>

		<!-- nowe pola RS -->
		<tr>
			<td>
				<ds:lang text="TelStac"/>:
			</td>
			<td>
				<c:out value="${viewUser.phoneNum}"/>
			</td>
		</tr>
		<ds:available test="userPersonal.extension.show">
			<tr>
				<td>
					<ds:lang text="NumWew"/>:
				</td>
				<td>
					<c:out value="${viewUser.extension}"/>
				</td>
			</tr>
		</ds:available>
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
				<c:out value="${viewUser.mobileNum}"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
				<c:out value="${viewUser.communicatorNum}"/>
			</td>
		</tr>
		<!-- ILPOL stopka do maila -->
		<ds:available test="mail.footer">
        <tr>
            <td><ds:lang text="StopkaMailowa"/>:</td>
            <td>
                <html:textarea property="mailFooter" cols="30" styleClass="txt" readonly="true"/>
            </td>
        </tr>
        </ds:available>
		<!-- nowe pola RS -->
		
		<tr>
			<td>
				<ds:lang text="ZablokowaneLogowanie"/>:
			</td>
			<td>
				<html:checkbox property="loginDisabled" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="LogowanieTylkoCertyfikatem"/>:
			</td>
			<td>
				<html:checkbox property="certificateLogin" disabled="true"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="DataUtworzenia"/>:
			</td>
			<td>
				<c:out value="${viewUser.creationTime}"/>
			</td>
		</tr>
		<ds:dockinds test="prosika">
			<tr>
				<td>
					<ds:lang text="Uwagi"/>:
				</td>
				<td>
					<c:out value="${viewUser.remarks}"/>
				</td>
			</tr>
		</ds:dockinds>
	</table>

	<br/>

	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="RoleArchiwum"/>
			</th>
			
			<c:if test="${!empty viewUser.groups}">
				<th>
					<ds:lang text="Grupy"/>
				</th>
			</c:if>
			
			<ds:modules test="coreoffice">
				<th>
					<ds:lang text="RoleKancelarii"/>
				</th>
			</ds:modules>
		</tr>
		<tr>
			<td class="alignTop">
				<table class="tableMargin">
					<c:forEach items="${viewUser.roles}" var="role">
						<tr>
							<td>
								<c:out value="${role}"/>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			
			<c:if test="${!empty viewUser.groups}">
				<td class="alignTop">
					<table class="tableMargin">
						<c:forEach items="${viewUser.groups}" var="group">
							<tr>
								<td>
									<c:out value="${group}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</c:if>
			
			<ds:modules test="coreoffice">
				<td class="alignTop">
					<table class="tableMargin">
						<c:forEach items="${viewUser.officeRoles}" var="role">
							<tr>
								<td>
									<c:out value="${role}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</ds:modules>
		</tr>
	</table>
	
	<c:if test="${!empty viewUser.divisions}">
		<h3><ds:lang text="Dzialy"/></h3>
		
		<c:forEach items="${viewUser.divisions}" var="divi">
			<c:out value="${divi}"/>
			<br/>
		</c:forEach>
	</c:if>
	
	<c:if test="${!empty certificates}">
		<h3><ds:lang text="Certyfikaty"/></h3>
		
		<table class="tableMargin">
			<tr>
				<th></th>
				<th colspan="2">
					<ds:lang text="Certyfikat"/>
				</th>
			</tr>
			<c:forEach items="${certificates}" var="cert" varStatus="status" >
				<tr>
					<td>
						<c:out value="${cert.dn}"/>
						(<span class="italic"><ds:lang text="wydanyPrzez"/></span>
						<c:out value="${cert.issuerDn}"/>)
					</td>
					<td>
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?username=<c:out value="${viewUser.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">DER</a>
						/
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?pem=true&amp;username=<c:out value="${viewUser.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">PEM</a>
					</td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</html:form>

<%--
<h1><ds:lang text="KartaUzytkownika"/><c:out value=" : ${viewUser.username}"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="UserEdit"/>
</p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/users/view-user">
<html:hidden property="username"/>
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
	<td><ds:lang text="NazwaUzytkownika"/>:</td>
	<td><c:out value="${viewUser.username}"/></td>
</tr>
<tr>
	<td><ds:lang text="Imie"/><span class="star">*</span>:</td>
	<td><c:out value="${viewUser.firstname}"/></td>
</tr>
<tr>
	<td><ds:lang text="Nazwisko"/><span class="star">*</span>:</td>
	<td><c:out value="${viewUser.lastname}"/></td>
</tr>
<ds:modules test="coreoffice">
	<tr>
		<td><ds:lang text="KodUzytkownika"/>:</td>
		<td><c:out value="${viewUser.identifier}"/></td>
	</tr>
</ds:modules>
<tr>
	<td><ds:lang text="Email"/>:</td>
	<td><c:out value="${viewUser.email}"/></td>
</tr>
<!-- nowe pola RS -->

<tr>
	<td><ds:lang text="TelStac"/>:</td>
	<td><c:out value="${viewUser.phoneNum}"/></td>
</tr>

<tr>
	<td><ds:lang text="NumWew"/>:</td>
	<td><c:out value="${viewUser.extension}"/></td>
</tr>

<tr>
	<td><ds:lang text="TelKom"/>:</td>
	<td><c:out value="${viewUser.mobileNum}"/></td>
</tr>

<tr>
	<td><ds:lang text="NumKomunikatora"/>:</td>
	<td><c:out value="${viewUser.communicatorNum}"/></td>
</tr>
<!-- nowe pola RS -->

<tr>
	<td><ds:lang text="ZablokowaneLogowanie"/>:</td>
	<td><html:checkbox property="loginDisabled" disabled="true"/></td>
</tr>
<tr>
	<td><ds:lang text="LogowanieTylkoCertyfikatem"/>:</td>
	<td><html:checkbox property="certificateLogin" disabled="true"/></td>
</tr>
<tr>
	<td><ds:lang text="DataUtworzenia"/>:</td>
	<td><c:out value="${viewUser.creationTime}"/></td>
</tr>

</table>
<p></p>
<table>
<tr>
	<th><ds:lang text="RoleArchiwum"/></th>
	<th><ds:lang text="Grupy"/></th>
	<ds:modules test="coreoffice">
		<th><ds:lang text="RoleKancelarii"/></th>
	</ds:modules>
</tr>
<tr>
	<td valign="top">
		<table>
		<c:forEach items="${viewUser.roles}" var="role">
			<tr>
				<td><c:out value="${role}"/></td>
			</tr>
		</c:forEach>
		</table>
	</td>
	<td valign="top">
		<c:if test="${!empty viewUser.groups}">
			<table>
			<c:forEach items="${viewUser.groups}" var="group">
				<tr>
					<td><c:out value="${group}"/></td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
	</td>
	<ds:modules test="coreoffice">
		<td valign="top">
			<table>
				<c:forEach items="${viewUser.officeRoles}" var="role">
					<tr>
						<td><c:out value="${role}"/></td>
					</tr>
				</c:forEach>
			</table>
		</td>
	</ds:modules>
</tr>
</table>

<c:if test="${!empty viewUser.divisions}">
	<p></p>
	<h3><ds:lang text="Dzialy"/></h3>
	<c:forEach items="${viewUser.divisions}" var="divi">
		<c:out value="${divi}"/>
		<br/>
	</c:forEach>
</c:if>



<c:if test="${!empty certificates}">
<p></p>
<h3><ds:lang text="Certyfikaty"/></h3>
<table>
	<tr>
		<th></th>
		<th><ds:lang text="Certyfikat"/></th>
	</tr>
	<c:forEach items="${certificates}" var="cert" varStatus="status" >
		<tr>
			<td valign="top"><input type="checkbox" name="deleteCerts" value="<c:out value="${cert.encoded64}"/>"/></td>
			<td><c:out value="${cert.dn}"/> (<i><ds:lang text="wydanyPrzez"/></i> <c:out value="${cert.issuerDn}"/>)</td>
			<td><a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?username=<c:out value="${viewUser.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">DER</a>
				/ <a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?pem=true&amp;username=<c:out value="${viewUser.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">PEM</a>
			</td>
		</tr>
	</c:forEach>
</table>
</c:if>

</html:form>
--%>
<!--N koniec view-users.jsp N-->