<%@ page import="pl.compan.docusafe.web.archive.users.NewUserListAction" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script type="text/javascript" src="<ww:url value="'/raphael-min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/g.raphael-min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/g.pie-min.js'" />"></script>
<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NewUser"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/users/create-new-user.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <table class="tableMargin">
        <tr>
            <td>
                <ds:lang text="NazwaUzytkownika"/>
                <span class="star">*</span>:
            </td>
            <td>
                <ww:textfield id="newUsername" name="'newUsername'" required="true" />
            </td>
        </tr>
        <tr>
            <td>
                <ds:lang text="Imie"/>
                <span class="star">*</span>:
            </td>
            <td>
                <ww:textfield id="newFirstname" name="'newFirstname'" required="true"/>
            </td>
        </tr>
        <tr>
            <td>
                <ds:lang text="Nazwisko"/>
                <span class="star">*</span>:
            </td>
            <td>
                <ww:textfield id="newLastname" name="'newLastname'" required="true"/>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;
            </td>
            <td align="right">
                <ds:event name="'doCreate'" value="getText('doCreate')"/>
            </td>
        </tr>

        <tr>
            <td colspan="2">
                <a href='<%= request.getContextPath() + NewUserListAction.URL %>'>Lista użytkowników</a>
            </td>
        </tr>
    </table>
</form>