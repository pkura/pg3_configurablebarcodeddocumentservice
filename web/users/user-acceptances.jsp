<!--N user-acceptances.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=": ${userAcceptances.username}" />
</h1>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<ds:xmlLink path="UserEdit"/>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/user-acceptances"><%-- mo�liwa edycja akceptacji w przysz�o�ci --%>
	<html:hidden property="username"/>

	<c:if test="${acceptances != null}">
		<table class="tableMargin">
			<tr>
				<th><ds:lang text="KodAkceptacji" /></th>

				<th><ds:lang text="WartoscSterujaca" /></th>
				<th></th>
			</tr>
			<c:forEach items="${acceptances}" var="acceptance">
				<tr>
					<td><c:out value="${acceptance.cn}" /></td>
					<td><c:out value="${acceptance.fieldValue}" /></td>		
					<td><c:if test="${acceptance.divisionName != null}"><c:out value="(${acceptance.divisionName})"/></c:if></td>			
				</tr>
			</c:forEach>
		</table>
	</c:if>

</html:form>