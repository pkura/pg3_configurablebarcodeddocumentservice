<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<edm-html:select-string-manager name="jspRepositoryLocalStrings"/>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NewUser"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/new-user">
<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="newUsername" size="50" maxlength="62" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="newFirstname" size="50" maxlength="64" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="newLastname" size="50" maxlength="64" styleClass="txt"/>
			</td>
		</tr>
</table>
	
	<html:submit property="doCreate" styleClass="btn">
		<ds:lang text="doCreate"/></html:submit>
</html:form>
<!--N koniec new-user.jsp N-->