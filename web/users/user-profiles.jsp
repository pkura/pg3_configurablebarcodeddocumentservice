<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N user-profiles.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<edm-html:select-string-manager name="jspRepositoryLocalStrings"/>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=": ${userProfiles.username}"/>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
	<ds:xmlLink path="UserEdit"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UserEdit"/>
	</div>
	
</ds:available>

<script type="text/javascript">
		var __manualSelectedTab = true;
</script>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/user-profiles">
	<html:hidden property="username"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<table class="tableMargin">
		<tr>
			<th colspan="2">
				<ds:lang text="Profile"/>
			</th>
		</tr>
		
		
	    <c:forEach items="${allProfilesWithKeys}" var="profile">
			<tr>
				<td>
					<c:out value="${profile.key}"/>
				</td>
				<td>
					<html-el:multibox property="selectedProfiles" value="${profile.key}"/>
				</td>

				<ds:available test="userProfileKeys">
					<td>
						<html-el:select property="keys" value="${profile.value}"
							styleClass="sel">
							<html:option value="#">-- wybierz --</html:option>
							<html:optionsCollection property="divisions" label="value"
								value="key" />
						</html-el:select>
					</td>
				</ds:available>
			</tr>
		</c:forEach>
	</table>

	<html:submit property="doUpdate" styleClass="btn saveBtn" ><ds:lang text="doUpdate"/></html:submit>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</html:form>

<%--
<edm-html:errors />
<edm-html:messages/>

<h1><ds:lang text="KartaUzytkownika" /><c:out
	value=" : ${userProfiles.username}" /></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p><ds:xmlLink path="UserEdit" /></p>
<p></p>

<html:form action="/users/user-profiles">
	<html:hidden property="username" />
	
	<table>
		<tr>
			<th><ds:lang text="Profile" /></th>
		</tr>
		<tr>
			<td valign="top">
			<table>
				<c:forEach items="${allProfiles}" var="profile">
					<tr>
						<td><c:out value="${profile}" /></td>
						<td><html-el:multibox property="selectedProfiles"
										value="${profile}" /></td>
					</tr>
				</c:forEach>
			</table>
			</td>
		</tr>
	</table>
	<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
</html:form>
--%>
<!--N koniec user-profiles.jsp N-->