<%@ page import="pl.compan.docusafe.web.archive.users.NewUserListAction" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="userEdit.h1"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/user-edit">
	<html:hidden property="username"/>
	<html:hidden property="offset"/>
	<html:hidden property="limit"/>
	<html:hidden property="sortField"/>

	<table class="tableMargin">

		<tr>
			<td>
				<ds:lang text="DataUtworzenia"/>:
			</td>
			<td>
				<c:out value="${userEdit.creationTime}"/>
			</td>
		</tr>
		<ds:available test="kancelaria.wlacz.podpisWewnetrzny">
		<tr>
			<td>
				<ds:lang text="WewnetrznaSygnatura"/>:
			</td>
			<td>
				<c:out value="${userEdit.internalSignature}"/>
			</td>
		</tr>
		</ds:available>
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>:
			</td>
			<td>
				<c:out value="${userEdit.username}"/>
			</td>
		</tr>
		<tr>
		<ds:available test="admin.useredit.disable">
			<td>
				<ds:lang text="NazwaAlternatywna"/>:
				<%--<span class="star">*</span>:--%>
			</td>
			<td>
				<html:text property="externalName" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="externalName" />
			</td>
		</ds:available>
		<ds:available test="!admin.useredit.disable">
        			<td>
        				<ds:lang text="NazwaAlternatywna"/>:
        				<%--<span class="star">*</span>:--%>
        			</td>
        			<td>
        				<html:text property="externalName" size="50" maxlength="50" styleClass="txt"/>
        			</td>
        </ds:available>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
				<span class="star">*</span>:
			</td>
			<td>
			    <ds:available test="admin.useredit.disable">
				    <html:text property="firstname" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="firstname" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
                	<html:text property="firstname" size="50" maxlength="50" styleClass="txt"/>
                </ds:available>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
				<span class="star">*</span>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				<html:text property="lastname" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="lastname" />
			</ds:available>
			<ds:available test="!admin.useredit.disable">
            	<html:text property="lastname" size="50" maxlength="50" styleClass="txt"/>
            </ds:available>
			</td>
		</tr>

		<ds:modules test="coreoffice">
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
				<ds:available test="admin.useredit.disable">
					<html:text property="identifier" size="5" maxlength="20" styleClass="txt" disabled="true"/>
					<html:hidden property="identifier" />
			    </ds:available>
			    <ds:available test="!admin.useredit.disable">
                	<html:text property="identifier" size="5" maxlength="20" styleClass="txt"/>
                </ds:available>
				</td>
			</tr>
		</ds:modules>

		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
			    <ds:available test="admin.useredit.disable">
				    <html:text property="email" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				    <html:hidden property="email" />
				</ds:available>
				<ds:available test="!admin.useredit.disable">
                	<html:text property="email" size="50" maxlength="50" styleClass="txt"/>
                </ds:available>
			</td>
		</tr>

		<!-- nowe pola RS -->
		<tr>
			<td>
				<ds:lang text="TelStac"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				<html:text property="phoneNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="phoneNum" />
			</ds:available>
			<ds:available test="!admin.useredit.disable">
            	<html:text property="phoneNum" size="50" maxlength="50" styleClass="txt"/>
            </ds:available>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="NumWew"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				<html:text property="extension" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="extension" />
            </ds:available>
            <ds:available test="!admin.useredit.disable">
                <html:text property="extension" size="50" maxlength="50" styleClass="txt"/>
            </ds:available>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				<html:text property="mobileNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="mobileNum" />
		    </ds:available>
		    <ds:available test="!admin.useredit.disable">
            	<html:text property="mobileNum" size="50" maxlength="50" styleClass="txt"/>
            </ds:available>
			</td>
		</tr>
		
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
			<ds:available test="admin.useredit.disable">
				<html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt" disabled="true"/>
				<html:hidden property="communicatorNum" />
			</ds:available>
			<ds:available test="!admin.useredit.disable">
            	<html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt"/>
            </ds:available>
			</td>
		</tr>
		<!-- nowe pola RS -->

		<ds:available test="pg.showpassworditems">
		<tr>
			<td>
				<ds:lang text="ZablokowaneLogowanie"/>:
			</td>
			<td>
				<html:checkbox property="loginDisabled"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="LogowanieTylkoCertyfikatem"/>:
			</td>
			<td>
				<html:checkbox property="certificateLogin"/>
			</td>
		</tr>
		
		<ds:available test="active.directory.on">
			<tr>
				<td>
					<ds:lang text="ActiveDirectory"/>:
				</td>
				<td>
					<html:checkbox property="activeDirectory" onchange="changeAD();"/>
				</td>
			</tr>
		</ds:available>
		<tr>
			<td colspan="2">
				<table id="tablePass" <c:if test="${activeDirectory}">style="display:none;"</c:if>/>
						<tr>
							<td>
								<ds:lang text="Haslo"/>:
							</td>
							<td>
								<html:password property="password" value="" size="50" maxlength="64" styleClass="txt_opt"/>
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="WeryfikacjaHasla"/>:
							</td>
							<td>
								<html:password property="password2" size="50" maxlength="64" styleClass="txt_opt"/>
							</td>
						</tr>
					
					<script language="JavaScript">
						function changeAD()
						{
							var el = document.getElementById('tablePass')
							if(el.style.display == 'none')
							{
								el.style.display = '';
							}
							else
							{
								el.style.display = 'none';
							}
						}
					</script>
				</table>
			</td>
		</tr>
		</ds:available>
	</table>
	
	<br/>
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="RoleArchiwum"/>
			</th>
			
			<c:if test="${!empty groups}">
				<th>
					<ds:lang text="Grupy"/>
				</th>
			</c:if>
			<ds:available test="officeRoles">
				<th>
					<ds:lang text="RoleKancelarii"/>
				</th>
			</ds:available>
		</tr>
		<tr>
			<td class="alignTop">
				<table class="tableMargin">
					<tr>
						<td>
							<ds:lang text="Administrator"/>:
						</td>
						<td>
							<html:multibox property="roles" value="admin"/>
						</td>
					</tr>
					<tr>
						<td>
							<ds:lang text="AdministratorDanych"/>:
						</td>
						<td>
							<html:multibox property="roles" value="data_admin"/>
						</td>
					</tr>
					<tr>
						<td>
							<ds:lang text="TylkoArchiwum"/>:
						</td>
						<td>
							<html:multibox property="roles" value="archive_only"/>
						</td>
					</tr>
					<tr>
						<td>
							<ds:lang text="TylkoOdbior"/>:
						</td>
						<td>
							<html:multibox property="roles" value="odbior_only"/>
						</td>
					</tr>
				</table>
			</td>
			
			<c:if test="${!empty groups}">
				<td class="alignTop">
					<table class="tableMargin">
						<c:forEach items="${groups}" var="group">
							<tr>
								<td>
									<c:out value="${group.name}"/>:
								</td>
								<td>
									<html-el:multibox property="groups" value="${group.guid}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</c:if>
			<ds:available test="officeRoles">
				<td class="alignTop">
					<table class="tableMargin">
						<c:forEach items="${officeRoles}" var="role">
							<tr>
								<td>
									<c:out value="${role.label}"/>:
								</td>
								<td>
									<html-el:multibox property="officeRoles" value="${role.value}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</ds:available>
		</tr>
	</table>
	
	<c:if test="${!empty divisions}">
		<h4>
			<ds:lang text="Dzialy"/>
		</h4>
		
		<c:forEach items="${divisions}" var="bean">
			<c:out value="${bean.prettyPath}"/>
			<c:if test="${bean.position}">[stanowisko]</c:if>
			<br/>
		</c:forEach>
	</c:if>
	
	<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
	<html:submit property="doCancel" styleClass="cancel_btn"><ds:lang text="doCancel"/></html:submit>
	
	<p>
        <ds:available test="menu.left.newWebWorkUserList">
            <a href='<%= request.getContextPath() + NewUserListAction.URL %>'>Lista użytkowników</a>
        </ds:available>
        <ds:available test="menu.left.newuserlist">
            <html:link forward="users/user-list">
                Lista użytkowników</html:link>
        </ds:available>
        <ds:available test="menu.left.olduserlist">
            <html:link forward="users/user-list">
                Lista użytkowników</html:link>
        </ds:available>
	</p>
	
	<p>
		<span class="star">*</span>
		<ds:lang text="PoleObowiazkowe"/>
	</p>
	
	<h3>
		<ds:lang text="Certyfikaty"/>
	</h3>
	
	<c:if test="${!empty certificates}">
		<table class="tableMargin">
			<tr>
				<th></th>
				<th>
					<ds:lang text="Certyfikat"/>
				</th>
				<th>
					<ds:lang text="Pobierz"/>
				</th>
			</tr>
			<c:forEach items="${certificates}" var="cert" varStatus="status" >
				<tr>
					<td valign="top">
						<input type="checkbox" name="deleteCerts" value="<c:out value="${cert.encoded64}"/>"/>
					</td>
					<td>
						<c:out value="${cert.dn}"/>
						(
						<span class="italic"><ds:lang text="wydanyPrzez"/></span>
						<c:out value="${cert.issuerDn}"/>
						)
					</td>
					<td>
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?username=<c:out value="${userEdit.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">DER</a>
						/
						<a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?pem=true&amp;username=<c:out value="${userEdit.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">PEM</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		
		<input type="submit" class="btn" name="doDeleteCert" value="<ds:lang text="Usun"/>"/>
	</c:if>
</html:form>

<p></p>

<html:form action="/users/user-edit" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);" >
	<html:hidden property="username"/>
	<html:hidden property="offset"/>
	<html:hidden property="limit"/>
	<html:hidden property="sortField"/>
	<input type="hidden" name="doSaveCert" id="doSaveCert"/>
	
	<ds:lang text="NowyCertyfikat"/>:
	<html:file property="certFile" styleClass="txt"/>
	<input type="submit" class="btn" value="<ds:lang text="Wyslij"/>" onclick="document.getElementById('doSaveCert').value='true';"/>
</html:form>

<%--
<h1><ds:lang text="userEdit.h1"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/users/user-edit">
<html:hidden property="username"/>
<html:hidden property="offset"/>
<html:hidden property="limit"/>
<html:hidden property="sortField"/>
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
	<td><ds:lang text="DataUtworzenia"/>:</td>
	<td><c:out value="${userEdit.creationTime}"/></td>
</tr>
<tr>
	<td><ds:lang text="NazwaUzytkownika"/>:</td>
	<td><c:out value="${userEdit.username}"/></td>
</tr>
<tr>
	<td><ds:lang text="NazwaAlternatywna"/><span class="star">*</span>:</td>
	<td><html:text property="externalName" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="Imie"/><span class="star">*</span>:</td>
	<td><html:text property="firstname" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="Nazwisko"/><span class="star">*</span>:</td>
	<td><html:text property="lastname" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<ds:modules test="coreoffice">
	<tr>
		<td><ds:lang text="KodUzytkownika"/>:</td>
		<td><html:text property="identifier" size="5" maxlength="20" styleClass="txt"/></td>
	</tr>
</ds:modules>
<tr>
	<td><ds:lang text="Email"/>:</td>
	<td><html:text property="email" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<!-- nowe pola RS -->

<tr>
	<td><ds:lang text="TelStac"/>:</td>
	<td><html:text property="phoneNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="NumWew"/>:</td>
	<td><html:text property="extension" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="TelKom"/>:</td>
	<td><html:text property="mobileNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="NumKomunikatora"/>:</td>
	<td><html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<!-- nowe pola RS -->

<tr>
	<td><ds:lang text="ZablokowaneLogowanie"/>:</td>
	<td><html:checkbox property="loginDisabled" /></td>
</tr>
<tr>
	<td><ds:lang text="LogowanieTylkoCertyfikatem"/>:</td>
	<td><html:checkbox property="certificateLogin"/></td>
</tr>
<tr>
	<td><ds:lang text="Haslo"/>:</td>
	<td><html:password property="password" size="50" maxlength="64" styleClass="txt_opt"/></td>
</tr>
<tr>
	<td><ds:lang text="WeryfikacjaHasla"/>:</td>
	<td><html:password property="password2" size="50" maxlength="64" styleClass="txt_opt"/></td>
</tr>
<%--
<c:if test="${!empty passwordValidDays}">
	<tr>
		<td>Czas ważności hasła:</td>
		<td>
			<c:out value="${passwordValidDays}"/> dni.
		</td>
	</tr>
</c:if>
-->

<%--
<tr>
	<td colspan="2">
		<table>
			<tr>
				<th>Role archiwum</th><edm-core:module-available moduleName="office" ><th>Role kancelarii</th></edm-core:module-available>
			</tr>
			<tr>
				<!-- archiwum -->
				<td valign="top">
					<table>
					<tr>
						<td>Administrator:</td>
						<td><html:multibox property="roles" value="admin"/></td>
					</tr>
					</table>
				</td>
				<!-- kancelaria -->
				<edm-core:module-available moduleName="office" >
					<td valign="top">
						<table>
							<c:forEach items="${officeRoles}" var="role">
								<tr>
									<td><c:out value="${role.label}"/></td>
									<td><input type="checkbox" name="officeRoles" value="<c:out value='${role.value}'/>" <c:if test="${role.checked}">checked="true"</c:if>/></td>
								</tr>
							</c:forEach>
						</table>
					</td>
				</edm-core:module-available>
			</tr>
		</table>
	</td>
</tr>
-->
</table>

<table>
<tr>
	<th><ds:lang text="RoleArchiwum"/></th>
	<th><ds:lang text="Grupy"/></th>
	<ds:modules test="coreoffice">
		<th><ds:lang text="RoleKancelarii"/></th>
	</ds:modules>
</tr>
<tr>
	<td valign="top">
		<table>
		<tr>
			<td><ds:lang text="Administrator"/>:</td>
			<td><html:multibox property="roles" value="admin"/></td>
		</tr>
		<tr>
			<td><ds:lang text="AdministratorDanych"/>:</td>
			<td><html:multibox property="roles" value="data_admin"/></td>
		</tr>
		</table>
	</td>
	<td valign="top">
		<c:if test="${!empty groups}">
			<table>
			<c:forEach items="${groups}" var="group">
				<tr>
					<td><c:out value="${group.name}"/>:</td>
					<td><html-el:multibox property="groups" value="${group.guid}"/></td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
	</td>
	<ds:modules test="coreoffice">
		<td valign="top">
			<table>
				<c:forEach items="${officeRoles}" var="role">
					<tr>
						<td><c:out value="${role.label}"/>:</td>
						<td><html-el:multibox property="officeRoles" value="${role.value}"/></td>
					</tr>
				</c:forEach>
			</table>
		</td>
	</ds:modules>
</tr>
</table>

<c:if test="${!empty divisions}">
	<h4><ds:lang text="Dzialy"/></h4>
	<c:forEach items="${divisions}" var="bean">
		<c:out value="${bean.prettyPath}"/>
		<c:if test="${bean.position}">[stanowisko]</c:if>
		<br/>
	</c:forEach>
</c:if>

<p></p>

<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
<html:submit property="doCancel" styleClass="cancel_btn"><ds:lang text="doCancel"/></html:submit>

<p><html:link forward="users/user-list"><ds:lang text="ListaUzytkownikow"/></html:link></p>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<h3><ds:lang text="Certyfikaty"/></h3>

<c:if test="${!empty certificates}">
<table>
	<tr>
		<th></th>
		<th><ds:lang text="Certyfikat"/></th>
		<th><ds:lang text="Pobierz"/></th>
	</tr>
	<c:forEach items="${certificates}" var="cert" varStatus="status" >
		<tr>
			<td valign="top"><input type="checkbox" name="deleteCerts" value="<c:out value="${cert.encoded64}"/>"/></td>
			<td><c:out value="${cert.dn}"/> (<i><ds:lang text="wydanyPrzez"/></i> <c:out value="${cert.issuerDn}"/>)</td>
			<td><a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?username=<c:out value="${userEdit.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">DER</a>
				/ <a href="<c:out value="${pageContext.request.contextPath}"/>/certificates/download.action?pem=true&amp;username=<c:out value="${userEdit.username}"/>&amp;signature=<c:out value="${cert.signature64}"/>">PEM</a>
			</td>
		</tr>
	</c:forEach>
</table>
<input type="submit" class="btn" name="doDeleteCert" value="<ds:lang text="Usun"/>"/>
</c:if>

</html:form>

<p></p>

<html:form action="/users/user-edit" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);" >
<html:hidden property="username"/>
<html:hidden property="offset"/>
<html:hidden property="limit"/>
<html:hidden property="sortField"/>
<input type="hidden" name="doSaveCert" id="doSaveCert"/>

<ds:lang text="NowyCertyfikat"/>: <html:file property="certFile" styleClass="txt"/>
<input type="submit" class="btn" value="<ds:lang text="Wyslij"/>" onclick="document.getElementById('doSaveCert').value='true';"/>

</html:form>
--%>
<!--N koniec user-edit.jsp N-->