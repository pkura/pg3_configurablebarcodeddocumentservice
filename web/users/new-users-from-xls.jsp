<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DodajUzytkownikow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/admin/new-users-from-xls.action'"/>" method="post" enctype="multipart/form-data">
<table>
	<tr>
		<td>
			<ww:file name="'file'" id="file" size="60" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:event value="'Importuj'" name="'doImport'" cssClass="'btn'" confirm="'Na pewno importować?'"/>
		</td>
	</tr>
</table>

</form>