<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><c:out value="${profilename}"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p>
	<ds:xmlLink path="ModyfikacjaProfilu"/>
</p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/profile-manage-users" onsubmit="selectAllOptions(this.selectedColumns);">
	<html:hidden property="profilename"/>
	<html:hidden property="profileId"/>
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="SystemUsers"/>
			</th>
			<th>
			</th>
			<th>
				<ds:lang text="ProfileUsers"/>
			</th>
		</tr>
		<tr>
			<td>					
				<html:select style="width: 300px;" size="20" styleId="availableColumns" property="selectedUsersLeft" multiple="true" styleClass="multi_sel">
					<html:options collection="userss" property="name"  labelProperty="lastnameFirstnameName" />
				</html:select>
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; "
						onclick="if (moveOptions(this.form.availableColumns, this.form.selectedColumns) > 0) dirty=true;"
						class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
						onclick="if (moveOptions(this.form.selectedColumns, this.form.availableColumns) > 0) dirty=true;"
						class="btn"/>
			</td>
			<td> 
				<html:select style="width: 300px;" size="20"  styleId="selectedColumns" property="selectedUsersRight" multiple="true" styleClass="multi_sel">
					<html:options collection="profileUsers" property="name" labelProperty="lastnameFirstnameName"/>
				</html:select>
			</td>
		</tr>
	</table>
	
	<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
</html:form>

<%--
<edm-html:select-string-manager name="jspRepositoryLocalStrings" />
<edm-html:errors />
<edm-html:messages />
<html:form action="/users/profile-manage-users" onsubmit="selectAllOptions(this.selectedColumns);">
<html:hidden property="profilename"/>
<h1><c:out value="${profilename}"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p><ds:xmlLink path="ModyfikacjaProfilu"/></p>
<table>
	<tr>
		<td>
			<h3><ds:lang text="SystemUsers"/></h3>
			<html:select style="width: 300px;" size="20" styleId="availableColumns" property="selectedUsersLeft" multiple="true" styleClass="multi_sel">
				<html:options collection="userss" property="name" labelProperty="name"/>
			</html:select>
		</td>
		<td valign="middle">
			<input type="button" value=" &gt;&gt; "
					onclick="if (moveOptions(this.form.availableColumns, this.form.selectedColumns) > 0) dirty=true;"
					class="btn"/>
			<br/>
			<input type="button" value=" &lt;&lt; "
					onclick="if (moveOptions(this.form.selectedColumns, this.form.availableColumns) > 0) dirty=true;"
					class="btn"/>
			<br/>
		</td>
		<td>
			<h3><ds:lang text="ProfileUsers"/></h3>
			<html:select style="width: 300px;" size="20"  styleId="selectedColumns" property="selectedUsersRight" multiple="true" styleClass="multi_sel">
				<html:options collection="profileUsers" property="name" labelProperty="name"/>
			</html:select>
		</td>
	</tr>
</table>
<br>
<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>
</html:form>	
--%>
<!--N koniec profile-manage-users.jsp N-->