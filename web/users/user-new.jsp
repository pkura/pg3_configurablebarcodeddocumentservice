<%@ page import="pl.compan.docusafe.web.archive.users.NewUserListAction" %>
<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N user-new.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><edm-html:message key="newUser.h1"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>

<html:form action="/users/user-new">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NazwaUzytkownika"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="username" size="50" maxlength="62" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="firstname" size="50" maxlength="64" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="lastname" size="50" maxlength="64" styleClass="txt"/>
			</td>
		</tr>

		<ds:modules test="coreoffice">
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
					<html:text property="identifier" size="5" maxlength="20" styleClass="txt"/>
				</td>
			</tr>
		</ds:modules>

		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
				<html:text property="email" size="50" maxlength="50" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="TelStac"/>:
			</td>
			<td>
				<html:text property="phoneNum" size="50" maxlength="50" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumWew"/>:
			</td>
			<td>
				<html:text property="extension" size="50" maxlength="50" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
				<html:text property="mobileNum" size="50" maxlength="50" styleClass="txt"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
				<html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt"/>
			</td>
		</tr>
		<ds:available test="active.directory.on">
			<tr>
				<td>
					<ds:lang text="ActiveDirectory"/>:
				</td>
				<td>
					<html:checkbox property="activeDirectory" onchange="changeAD();"/>
				</td>
			</tr>
		</ds:available>
		<tr>
			<td colspan="2">
				<table id="tablePass"/> 
						<tr>
							<td>
								<ds:lang text="Haslo"/>:
							</td>
							<td>
								<html:password property="password" value="" size="50" maxlength="64" styleClass="txt_opt"/>
							</td>
						</tr>
						<tr>
							<td>
								<ds:lang text="WeryfikacjaHasla"/>:
							</td>
							<td>
								<html:password property="password2" size="50" maxlength="64" styleClass="txt_opt"/>
							</td>
						</tr>
					
					<script language="JavaScript">
						function changeAD()
						{
							var el = document.getElementById('tablePass')
							if(el.style.display == 'none')
							{
								el.style.display = '';
							}
							else
							{
								el.style.display = 'none';
							}
						}
					</script>
				</table>
			</td>
		</tr>
	</table>
	
	<br/>
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="RoleArchiwum"/>
			</th>

			<c:if test="${!empty groups}">
				<th>
					<ds:lang text="Grupy"/>
				</th>
			</c:if>
				
			<ds:modules test="coreoffice">
				<th>
					<ds:lang text="RoleKancelarii"/>
				</th>
			</ds:modules>
		</tr>
		<tr>
			<td class="alignTop">
				<table class="tableMargin">
					<tr>
						<td>
							<ds:lang text="Administrator"/>:
						</td>
						<td>
							<html:multibox property="roles" value="admin"/>
						</td>
					</tr>
				</table>
			</td>
			
			<c:if test="${!empty groups}">
				<td class="alignTop">
					<table>
						<c:forEach items="${groups}" var="group">
							<tr>
								<td>
									<c:out value="${group.name}"/>:
								</td>
								<td>
									<html-el:multibox property="groups" value="${group.guid}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</c:if>
			
			<ds:modules test="coreoffice">
				<td class="alignTop">
					<table class="tableMargin">
						<c:forEach items="${officeRoles}" var="role">
							<tr>
								<td>
									<c:out value="${role.label}"/>:
								</td>
								<td>
									<html-el:multibox property="officeRoles" value="${role.value}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
				</td>
			</ds:modules>
		</tr>
	</table>
	
	<html:submit property="doCreate" styleClass="btn" ><ds:lang text="Utworz"/></html:submit>
	<html:submit property="doCancel" styleClass="cancel_btn"><ds:lang text="Rezygnuj"/></html:submit>
</html:form>

<p>
    <ds:available test="menu.left.newWebWorkUserList">
        <a href='<%= request.getContextPath() + NewUserListAction.URL %>'>Lista użytkowników</a>
    </ds:available>
    <ds:available test="menu.left.newuserlist">
        <html:link forward="users/user-list">
            Lista użytkowników</html:link>
    </ds:available>
    <ds:available test="menu.left.olduserlist">
        <html:link forward="users/user-list">
            Lista użytkowników</html:link>
    </ds:available>
</p>
<p>
	<span class="star">*</span>
	<ds:lang text="PoleObowiazkowe"/>
</p>

<%--
<h1><edm-html:message key="newUser.h1"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<edm-html:errors />

<html:form action="/users/user-new">

<table border='0' cellspacing='0' cellpadding='0'>
<tr>
	<td><ds:lang text="NazwaUzytkownika"/><span class="star">*</span>:</td>
	<td><html:text property="username" size="50" maxlength="62" styleClass="txt"/></td>
</tr>
<tr>
	<td><ds:lang text="Imie"/><span class="star">*</span>:</td>
	<td><html:text property="firstname" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
	<td><ds:lang text="Nazwisko"/><span class="star">*</span>:</td>
	<td><html:text property="lastname" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<ds:modules test="coreoffice">
	<tr>
		<td><ds:lang text="KodUzytkownika"/>:</td>
		<td><html:text property="identifier" size="5" maxlength="20" styleClass="txt"/></td>
	</tr>
</ds:modules>
<tr>
	<td><ds:lang text="Email"/>:</td>
	<td><html:text property="email" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="TelStac"/>:</td>
	<td><html:text property="phoneNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="NumWew"/>:</td>
	<td><html:text property="extension" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="TelKom"/>:</td>
	<td><html:text property="mobileNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>

<tr>
	<td><ds:lang text="NumKomunikatora"/>:</td>
	<td><html:text property="communicatorNum" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="Haslo"/><span class="star">*</span>:</td>
	<td><html:password property="password" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
	<td><ds:lang text="WeryfikacjaHasla"/><span class="star">*</span>:</td>
	<td><html:password property="password2" size="50" maxlength="64" styleClass="txt"/></td>
</tr>
<tr>
</tr>
</table>

<table>
<tr>
	<th><ds:lang text="RoleArchiwum"/></th>
	<th><ds:lang text="Grupy"/></th>
	<ds:modules test="coreoffice">
		<th><ds:lang text="RoleKancelarii"/></th>
	</ds:modules>
</tr>
<tr>
	<td valign="top">
		<table>
		<tr>
			<td><ds:lang text="Administrator"/>:</td>
			<td><html:multibox property="roles" value="admin"/></td>
		</tr>
		</table>
	</td>
	<td valign="top">
		<c:if test="${!empty groups}">
			<table>
			<c:forEach items="${groups}" var="group">
				<tr>
					<td><c:out value="${group.name}"/>:</td>
					<td><html-el:multibox property="groups" value="${group.guid}"/></td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
	</td>
	<ds:modules test="coreoffice">
		<td valign="top">
			<table>
				<c:forEach items="${officeRoles}" var="role">
					<tr>
						<td><c:out value="${role.label}"/>:</td>
						<td><html-el:multibox property="officeRoles" value="${role.value}"/></td>
					</tr>
				</c:forEach>
			</table>
		</td>
	</ds:modules>
</tr>
</table>

<html:submit property="doCreate" styleClass="btn" ><ds:lang text="Utworz"/></html:submit>
<html:submit property="doCancel" styleClass="cancel_btn"><ds:lang text="Rezygnuj"/></html:submit>

</html:form>

<p><html:link forward="users/user-list"><ds:lang text="ListaUzytkownikow"/></html:link></p>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

--%>
<!--N koniec user-new.jsp N-->