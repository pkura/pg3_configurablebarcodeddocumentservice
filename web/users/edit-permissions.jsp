<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-permissions.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="KartaUzytkownika"/>
	<c:out value=": ${editPermissions.username}" />
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
	<ds:xmlLink path="UserEdit"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UserEdit"/>
	</div>
	
</ds:available>

<script type="text/javascript">
		var __manualSelectedTab = true;
</script>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/edit-permissions">
	<html:hidden property="username"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<html:submit property="doUpdate" styleClass="btn saveBtn">
		<ds:lang text="doUpdate"/>
	</html:submit>
	
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="RoleArchiwum"/>
			</th>
			
			<th>
				<ds:lang text="Grupy"/>
			</th>
			
			<ds:modules test="coreoffice">
				<th>
					<ds:lang text="RoleKancelarii"/>
				</th>
			</ds:modules>
		</tr>
		<tr>
			<td class="alignTop">
				<table class="tableMargin">
					<c:forEach items="${rolesSources}" var="role">
						<tr>
							<td>
								<c:if test="${role.key == 'data_admin'}">
									<ds:lang text="AdministratorDanych"/>
								</c:if>
								<c:if test="${role.key == 'admin'}">
									<ds:lang text="Administrator"/>
								</c:if>
								<c:if test="${role.key == 'archive_only'}">
									<ds:lang text="TylkoArchiwum"/>
								</c:if>
								<c:if test="${role.key == 'odbior_only'}">
									<ds:lang text="TylkoOdbior"/>
								</c:if>
							</td>
							
							<c:set var="canRemove" value="false"/>
							<c:set var="sources" value=""/>
							
							<td>
								<c:choose>
									<c:when test="${!empty role.value}">
										<c:forEach items="${role.value}" var="source">
											<c:choose>
												<c:when test="${source == 'own'}">
													<c:set var="canRemove" value="true"/>
													<c:set var="sources" value=" uprawnienie w�asne; ${sources}"/>
												</c:when>
												<c:otherwise>
													<c:set var="sources" value=" uprawnienie z profilu ${source}; ${sources}"/>
												</c:otherwise>
											</c:choose>
										</c:forEach>
										
										<c:if test="${canRemove == true}">
											<html-el:multibox property="selectedArchiveRoles" value="${role.key}"/>
										</c:if>
										
										<c:if test="${canRemove == false}">
											<input type="checkbox" checked="yes" disabled="true"/>
										</c:if>
										
										<img src="<ww:url value="'/img/permission.gif'"/>" 
											title="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											alt="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											class="png permIco"	
										/>
									</c:when>
									<c:otherwise>
										<html-el:multibox property="selectedArchiveRoles" value="${role.key}"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			
			<td class="alignTop">
				<table class="tableMargin">
					<c:forEach items="${groupsSources}" var="group">
						<tr>
							<td>
								<c:out value="${group.first.name}"/>
							</td>
							
							<c:set var="canRemove" value="false"/>
							<c:set var="sources" value=""/>
							
							<td>
								<c:choose>
									<c:when test="${!empty group.second}">
										<c:forEach items="${group.second}" var="source">
											<c:choose>
												<c:when test="${source == 'own'}">
													<c:set var="canRemove" value="true"/>
													<c:set var="sources" value=" uprawnienie w�asne; ${sources}"/>
												</c:when>
												<c:otherwise>
													<c:set var="sources" value=" uprawnienie z profilu ${source}; ${sources}"/>
												</c:otherwise>
											</c:choose>
										</c:forEach>

										<c:if test="${canRemove == true}">
											<html-el:multibox property="selectedGroups" value="${group.first.guid}" />
										</c:if>

										<c:if test="${canRemove == false}">
											<input type="checkbox" checked="yes" disabled="true" />
										</c:if>

										<img src="<ww:url value="'/img/permission.gif'"/>" 
											title="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											alt="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											class="png permIco"
										/>
									</c:when>
									<c:otherwise>
										<html-el:multibox property="selectedGroups" value="${group.first.guid}" />
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
			
			<td class="alignTop">
				<table class="tableMargin">
					<c:forEach items="${officeRolesSources}" var="officeRole">
						<tr>
							<td>
								<c:out value="${officeRole.first.name}"/>
							</td>

							<c:set var="canRemove" value="false"/>
							<c:set var="sources" value=""/>

							<td>
								<c:choose>
									<c:when test="${!empty officeRole.second}">
										<c:forEach items="${officeRole.second}" var="source">
											<c:choose>
												<c:when test="${source == 'own'}">
													<c:set var="canRemove" value="true"/>
													<c:set var="sources" value=" uprawnienie w�asne; ${sources}"/>
												</c:when>
												<c:otherwise>
													<c:set var="sources" value="${sources} uprawnienie z profilu ${source};"/>
												</c:otherwise>
											</c:choose>
										</c:forEach>

										<c:if test="${canRemove == true}">
											<html-el:multibox property="selectedOfficeRoles" value="${officeRole.first.id}" />
										</c:if>
	
										<c:if test="${canRemove == false}">
											<input type="checkbox" checked="yes" disabled="true" />
										</c:if>

										<img src="<ww:url value="'/img/permission.gif'"/>"
											title="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											alt="�r�d�a uprawnienia: <c:out value='${sources}'/>"
											class="png permIco"
										/>
									</c:when>
									<c:otherwise>
										<html-el:multibox property="selectedOfficeRoles" value="${officeRole.first.id}"/>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
	
	<html:submit property="doUpdate" styleClass="btn saveBtn" ><ds:lang text="doUpdate"/></html:submit>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</html:form>
<!--N koniec edit-permissions.jsp N-->