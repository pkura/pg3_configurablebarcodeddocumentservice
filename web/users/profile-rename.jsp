<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>
<!--N profile-edit.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><c:out value="${profilename}"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p>
	<ds:xmlLink path="ModyfikacjaProfilu"/>
</p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/users/profile-rename">
	<html:hidden property="profilename"/>
	<html:hidden property="profileId"/>
	
	<c:if test="${!empty profilename}">
		<table><tr><td>
		<b><ds:lang text="NowaNazwaProfilu"/></b>
		</td><td>
		<html:text property="newName" size="10" />
		</td></tr></table>
		<html:submit property="doUpdate" styleClass="btn" ><ds:lang text="doUpdate"/></html:submit>

		
	</c:if>	
</html:form>