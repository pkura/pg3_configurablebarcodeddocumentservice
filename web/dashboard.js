function Dashboard(jqColumns) {
	var jqColumns = jqColumns;
	var portlets = {}, visiblePortlets = [];
	var _this = this;
	var jqPortletAdd, portletAdd;
	
	
	$j(jqColumns).sortable({
		connectWith: jqColumns.selector,
		handle: '.sp-portlet-header',
		items: '.sp-portlet',
		start: function(event, ui) {
			$j('.ui-sortable-placeholder').height(ui.item.height());
			if(portletAdd.visible) {
				setTimeout(function() {
					jqPortletAdd.addClass('sp-portlet-minimized')
				}, 0);
			}
		},
		stop: function(event, ui) {
			var jqColumn = ui.item.parent();
			var jqFoundedPortletAdd = jqColumn.find('.sp-portlet-special-add');
			var portlet;
			if(jqFoundedPortletAdd.length > 0) {
				jqFoundedPortletAdd.before(ui.item);
			}
			portlet = portlets[ui.item[0].id];
			portlet.column = parseInt(jqColumn.attr('column'), 10);
			if(portletAdd.visible) {
				adjustAddPortletColumn(portlets[ui.item[0].id]);
				setTimeout(function() {
					jqPortletAdd.removeClass('sp-portlet-minimized');
				}, 0);
			}
			saveConfiguration();
		}
	});
	
	function saveConfiguration() {
		var cn, portlet, configJson;
		var config = [], columnIndex = 0;
		jqColumns.each(function() {
			config[columnIndex] = [];
			$j(this).find('.sp-portlet[id]').each(function() {
				var id = this.id;
				config[columnIndex].push(id);
			});
			columnIndex ++;
		});
		configJson = JSON.stringify(config);
		$j.cookie('dashboard', configJson, {path: '/'});
	}
	
	function findLowestColumn(jqIncludedColumns) {
		var heights = [], i = 0, columnIndex = 0;
		var jqIncludedColumns = jqIncludedColumns || jqColumns;
		var minHeight;
		jqIncludedColumns.each(function() {
			var height = 0;
			$j(this).find('.sp-portlet:not(.sp-portlet-special-add)').each(function() {
				height += $j(this).height();
			});
			heights[i] = height;
			i ++;
		});
		minHeight = heights[0];
		for(i = 1; i < heights.length; i ++) {
			if(heights[i] < minHeight) {
				minHeight = heights[i];
				columnIndex = i;
			}
		}
		return jqIncludedColumns.filter(':eq(' + columnIndex + ')');
	}
	
	function adjustAddPortletColumn(callerPortlet) {
		var jqColumn, jqIncludedColumns, column;
		var includedColumnIndexes = [];
		column = callerPortlet ? callerPortlet.column : 1;
		switch(column) {
			case 0:
				// sprawdzamy czy kolumna po prawej (#1) lub na dole (#0) ma wi�cej wolnego miejsca,
				// i je�li tak, to przenosimy [+] do kolumny #1
				includedColumnIndexes = [0, 1];
			break;
			
			case 1:
				includedColumnIndexes = [0, 1, 2];
			break;
			
			case 2:
				includedColumnIndexes = [1, 2];
			break;
			
			default:
				incudedColumnIndexes = [0, 1, 2];
		}
		jqColumn = findLowestColumn(jqColumns.filter(function(index) {
			var i;
			for(i = 0; i < includedColumnIndexes.length; i ++) {
				if(index == includedColumnIndexes[i]){
					return true;
				}
			}
		}));
		jqColumn.append(jqPortletAdd);
	}
	
	function showPortlet(cn, columnIndex) {
		var portlet = portlets[cn];
		var jqPortlet = portlet.jqPortlet, jqColumn = jqColumns.filter(':eq(' + columnIndex + ')');
		var jqPortletAdd = jqColumn.find('.sp-portlet-special-add');
		
		jqPortlet.unbind('click.show').removeClass('sp-portlet-thumbnail').addClass('sp-portlet-minimized');
		jqPortlet.find('.sp-portlet-header').find('.sp-portlet-remove').unbind('click.hide').bind('click.hide', function() {
			hidePortlet($j(this).attr('portletid'));
		});
		jqPortlet.find('.sp-portlet-header').unbind('mousedown.cursor').bind('mousedown.cursor', function() {
			$j(this).addClass('sp-portlet-grabbing');
		}).unbind('mouseup.cursor').bind('mouseup.cursor', function() {
			$j(this).removeClass('sp-portlet-grabbing');
		});
		if(jqPortletAdd.length > 0) {
			jqPortletAdd.before(jqPortlet);
		} else {
			jqColumn.append(jqPortlet);
		}
		setTimeout(function() {
			jqPortlet.removeClass('sp-portlet-minimized');
		}, 50);
		portlet.visible = true;
		if(portlet.jqPortlet.find('.sp-portlet-content[url]').length > 0) {
			portlet.jqPortlet.find('.sp-portlet-content').unbind('click.redirect').bind('click.redirect', function() {
				window.location.href = $j(this).attr('url');
				return false;
			});
		}
		
		saveConfiguration();
	}
	
	function hidePortlet(cn) {
		var portlet = portlets[cn];
		var jqPortlet = portlet.jqPortlet;
		portlet.visible = false;
		jqPortlet.addClass('sp-portlet-minimized');
		setTimeout(function() {
			jqPortlet.remove();
			adjustAddPortletColumn(portlet);
			jqPortletAdd.removeClass('sp-portlet-minimized');
			jqPortlet.find('.sp-portlet-content').unbind('click.redirect');
			saveConfiguration();
		}, 250);
	}
	
	function showAvailablePortlets(jqDialog, domObj, event) {
		var cn, portlet;
		var str = '', availPortlets = [];
		
		for(cn in portlets) {
			portlet = portlets[cn];
			if(!portlet.visible && !portlet.data.type) {
				portlet.jqPortlet.addClass('sp-portlet-thumbnail').removeClass('sp-portlet-minimized')
				availPortlets.push(portlet);
				if(portlet.jqPortlet.hasClass('sp-portlet-cal')) {
					portlet.jqPortlet.find('.sp-portlet-calendar-overlay').unbind('click.show').bind('click.show', function() {
						$j(this).parent().trigger('click.show');
					});				
				}
				portlet.jqPortlet.unbind('click.show').bind('click.show', function() {
					var heights = [], i = 0, columnIndex = 0;
					var minHeight;
					jqColumns.each(function() {
						var height = 0;
						$j(this).find('.sp-portlet:not(.sp-portlet-special-add)').each(function() {
							height += $j(this).height();
						});
						heights[i] = height;
						i ++;
					});
					minHeight = heights[0];
					for(i = 1; i < heights.length; i ++) {
						if(heights[i] < minHeight) {
							minHeight = heights[i];
							columnIndex = i;
						}
					}
					
					showPortlet(this.id, columnIndex);
					for(i = 0; i < availPortlets.length; i ++) {
						if(availPortlets[i].data.cn == this.id) {
							availPortlets.splice(i, 1);
							break;
						}
					}
					if(availPortlets.length == 0) {
						jqDialog.dialog('close');
						setTimeout(function() {
							$j(domObj).addClass('sp-portlet-minimized');
							portletAdd.visible = false;
						}, 250);
					}else if(availPortlets.length > 2) {
						jqDialog.dialog('option', 'width', 467);
						$j(domObj).removeClass('sp-portlet-minimized');
						portletAdd.visible = true;
					} else {
						jqDialog.dialog('option', 'width', 245);
						$j(domObj).removeClass('sp-portlet-minimized');
						portletAdd.visible = true;
					}
				});
			}
		}
		if(availPortlets.length > 2) {
			jqDialog.dialog('option', 'width', 467);
		} else {
			jqDialog.dialog('option', 'width', 245);
		}
		for(i = 0; i < availPortlets.length; i ++) {
			jqDialog.append(availPortlets[i].jqPortlet);
		}
	}
	
	this.portletAddListener = (function() {
		var jqDialog = $j('<div/>').attr('title', 'Dost�pne');
		jqDialog.dialog({
			autoOpen: false,
			dialogClass: 'sp-portlet-dialog',
			modal: true,
			buttons: {
				Ok: function() {
					$j(this).dialog('close');
				}
			}
		});
	
		return function(cn, domObj, event) {
			var jqObj = $j(domObj);
			var pos = jqObj.position();
			showAvailablePortlets(jqDialog, domObj, event);
			pos.left += jqObj.width();
			jqDialog.dialog('open').dialog('option', 'position', [pos.left, pos.top]);
		}
	})();
	
	return {
		createPortlet: function(params) {
			var jqColumn, jqPortlet, str, i, item;
			var data = {};
			var url;
			var pageContext = $j('#request_pageContext').val();
		
			data.cn = params.cn;
			url = params.url ? 'url="' + pageContext + params.url + '"' : '';
			str = '<div class="sp-portlet" id="' + params.cn + '"><div class="sp-portlet-header">' + params.header 
				+ '<div class="sp-portlet-remove" portletid="' + params.cn + '"></div></div>';
			str += '<div ' + url + ' class="sp-portlet-content sp-portlet-content-std">';
			
			// najpierw duzy naglowek;
			item = params.items[0];
			if(item.header === true) {
				data.header = {cn: item.cn, label: params.header, newItems: item.newItems, allItems: item.allItems};
				str += '<div class="sp-portlet-content-summary">'
				str += '<div class="sp-portlet-content-summary-new">' + item.newItems + '</div>';
				str += '<div class="sp-portlet-content-summary-divisor sp-portlet-content-less-important">/</div>';
				str += '<div class="sp-portlet-content-summary-all sp-portlet-content-less-important">' + item.allItems + '</div>';
				str += '<div class="sp-portlet-content-summary-desc">' + item.desc[0] + '</div><div class="clearBoth"></div>';
				
				str += '<div class="sp-portlet-content-summary-new-comment">' + item.desc[1] + '</div>';
				str += '<div class="sp-portlet-content-summary-divisor-comment sp-portlet-content-less-important"></div>';
				str += '<div class="sp-portlet-content-summary-all-comment sp-portlet-content-less-important">' + item.desc[2] + '</div>';
				
				str += '</div>';
			}
			
			data.subItems = {};
			for(i = 1; i < params.items.length; i ++) {
				item = params.items[i];
				data.subItems[item.cn] = {newItems: item.newItems, allItems: item.allItems};
				str += '<div class="sp-portlet-content-item">';
				str += '<div class="sp-portlet-content-summary-new">' + item.newItems + '</div>';
				str += '<div class="sp-portlet-content-summary-divisor sp-portlet-content-less-important">/</div>';
				str += '<div class="sp-portlet-content-summary-all sp-portlet-content-less-important">' + item.allItems + '</div>';
				str += '<div class="sp-portlet-content-summary-desc">' + item.desc[0] + '</div>';
				str += '<div class="clearBoth"></div>';
				
				str += '</div>';
			}			
		
			str += '</div>';
			jqPortlet = $j(str);
			jqPortlet.find('.sp-portlet-header').find('.sp-portlet-remove').bind('click.hide', function() {
				hidePortlet($j(this).attr('portletid'));
			});
			jqPortlet.find('.sp-portlet-header').bind('mousedown.cursor', function() {
				$j(this).addClass('sp-portlet-grabbing');
			}).bind('mouseup.cursor', function() {
				$j(this).removeClass('sp-portlet-grabbing');
			});
			return {
				jqPortlet: jqPortlet,
				data: data
			}
		},
		
		createCalendarPortlet: function(params) {
			var jqColumn, jqPortlet, str, i, item;
			var data = {}, now = new Date();
			var calendarSrc = $j('#request_pageContext').val() + '/office/tasklist/calendar-embedded.action?embedded=true';
		
			data.cn = params.cn;
			str = '<div class="sp-portlet sp-portlet-cal" id="' + params.cn + '"><div class="sp-portlet-header">' + params.header 
				+ '<div class="sp-portlet-remove" portletid="' + params.cn + '"></div></div>';
			str += '<div class="sp-portlet-content sp-portlet-content-cal">';
			str += '<iframe id="sp-calendar-iframe" src="' + calendarSrc
			str += '&year=' + now.getFullYear() + '&month=' + (now.getMonth()) + '&day=' + now.getDate()
				+ '&firstday=' + now.getDay() + '&height=350&mintime=8';
			str += '"></iframe>';
			str += '</div>';
			str += '<div class="sp-portlet-calendar-overlay"></div></div>';
			jqPortlet = $j(str);
			
			jqPortlet.find('.sp-portlet-header').find('.sp-portlet-remove').bind('click.hide', function() {
				hidePortlet($j(this).attr('portletid'));
			});
			jqPortlet.find('.sp-portlet-header').bind('mousedown.cursor', function() {
				$j(this).addClass('sp-portlet-grabbing');
			}).bind('mouseup.cursor', function() {
				$j(this).removeClass('sp-portlet-grabbing');
			});
			return {
				jqPortlet: jqPortlet,
				data: data
			}
		},
		
		createNamedayPortlet: function(params) {
			var jqColumn, jqPortlet, str, i;
			var data = {}, namedays = PobierzImieniny(null, null, 1).split(', ');
			var now = new Date();
			
			data.cn = params.cn;
			str = '<div class="sp-portlet sp-portlet-nameday" id="' + params.cn + '"><div class="sp-portlet-header">' + params.header
				+ '<div class="sp-portlet-remove" portletid="' + params.cn + '"></div></div>';
			str += '<div class="sp-portlet-content sp-portlet-content-std sp-portlet-content-nameday">';
			str += '<div class="sp-portlet-content-nameday-title">' + now.format('d mmmmm yyyy', false, 'pl') + ' imieniny obchodz�:' 
				+ '</div>';
			for(i = 0; i < namedays.length; i ++) {
				str += '<div class="sp-portlet-content-nameday-single">' + namedays[i] + (i < namedays.length-1 ? ',' : '') + '</div>'; 
			}
			str += '<div class="clearBoth"></div></div>';
			jqPortlet = $j(str);
			
			jqPortlet.find('.sp-portlet-header').find('.sp-portlet-remove').bind('click.hide', function() {
				hidePortlet($j(this).attr('portletid'));
			});
			jqPortlet.find('.sp-portlet-header').bind('mousedown.cursor', function() {
				$j(this).addClass('sp-portlet-grabbing');
			}).bind('mouseup.cursor', function() {
				$j(this).removeClass('sp-portlet-grabbing');
			});
			
			return {
				jqPortlet: jqPortlet,
				data: data
			}
		},
		
		createWeatherPortlet: function(params) {
			var jqColumn, jqPortlet, str;
			var data = {};
			var config = WeatherUtils.loadConfiguration();
			
			data.cn = params.cn;
			str = '<div class="sp-portlet sp-portlet-weather" id="' + params.cn + '"><div class="sp-portlet-header">' + params.header
			str += '<div class="sp-portlet-remove" portletid="' + params.cn + '"></div>';
			str	+= '<div id="ajax-anim-container"><div class="ajax-loading-message">ustalanie po�o�enia</div>';
			str += '<div class="circle" id="circle_1"></div><div class="circle" id="circle_2"></div>';
			str += '<div class="circle" id="circle_3"></div><div class="clearfix"></div>';	
			str += '</div></div>';
			str += '<div class="sp-portlet-content sp-portlet-content-std sp-portlet-content-weather"></div>'
			str += '</div>';
			
			jqPortlet = $j(str);
			jqAjaxLoader = jqPortlet.find('.sp-portlet-header').find('#ajax-anim-container');
			jqPortlet.find('.sp-portlet-header').find('.sp-portlet-remove').bind('click.hide', function() {
				hidePortlet($j(this).attr('portletid'));
			});
			jqPortlet.find('.sp-portlet-header').bind('mousedown.cursor', function() {
				$j(this).addClass('sp-portlet-grabbing');
			}).bind('mouseup.cursor', function() {
				$j(this).removeClass('sp-portlet-grabbing');
			});
			jqPortlet.find('.sp-portlet-content').append(WeatherUtils.createWeatherContent(config, jqAjaxLoader));
			
			return {
				jqPortlet: jqPortlet,
				data: data
			}
		},
		
		createSpecialPortlet: function(params, columnIndex) {
			var str, data, jqPortlet;
			data = {cn: params.cn, type: params.type};
			switch(params.type.toLowerCase()) {
				case 'add':
					str = '<div class="sp-portlet sp-portlet-special-add">';
					str += '<div class="sp-portlet-special-add-background"></div>';
					str += '<div class="sp-portlet-special-add-foreground"></div>';
					jqPortlet = $j(str);
					jqPortlet.bind('click.addPortlet', function(event) {
						_this.portletAddListener(params.cn, this, event);
					});
					jqPortletAdd = jqPortlet;
					portletAdd = data;
					//portletAdd.visible = true;
				break;
			}
			
			return {
				jqPortlet: jqPortlet,
				data: data
			}
		},
		
		addPortlet: function(portlet, columnIndex, hidden) {
			portlet.visible = !(hidden === true);
			portlet.column = columnIndex;
			portlets[portlet.data.cn] = portlet;
			if(portlet.visible) {
				jqColumns.filter(':eq(' + columnIndex + ')').append(portlet.jqPortlet);
				if(portlet.jqPortlet.find('.sp-portlet-content[url]').length > 0) {
					portlet.jqPortlet.find('.sp-portlet-content').unbind('click.redirect').bind('click.redirect', function() {
						window.location.href = $j(this).attr('url');
						return false;
					});
				}
			}
		},
		
		adjustAddPortletColumn: adjustAddPortletColumn,
		portlets: portlets,
		jqColumns: jqColumns,
		
		loadConfiguration: function() {
			var configJson = $j.cookie('dashboard');

			return configJson ? JSON.parse(configJson) : null;
		}
	}
}

var WeatherUtils = (function() {
	var cities = [
   		{label: 'Bia�ystok', lat: 53.08, lon: 23.10},
   		{label: 'Bydgoszcz', lat: 53.09, lon: 18.00},
   		{label: 'Gda�sk', lat: 54.21, lon: 18.40},
   		{label: 'Gorz�w Wlkp.', lat: 52.44, lon: 15.14},
   		{label: 'Katowice', lat: 50.16, lon: 19.01},
   		{label: 'Kielce', lat: 50.50, lon: 20.40},
   		{label: 'Koszalin', lat: 54.12, lon: 16.11},
   		{label: 'Krak�w', lat: 50.05, lon: 19.55},
   		{label: 'Lublin', lat: 51.15, lon: 22.34},
   		{label: '��d�', lat: 51.45, lon: 19.28},
   		{label: 'Olsztyn', lat: 50.45, lon: 19.16},
   		{label: 'Opole', lat: 50.40, lon: 17.57},
   		{label: 'Pozna�', lat: 51.41, lon: 22.19},
   		{label: 'Rzesz�w', lat: 50.03, lon: 22.00},
   		{label: 'Suwa�ki', lat: 54.05, lon: 22.55},
   		{label: 'Szczecin', lat: 51.55, lon: 19.43},
   		{label: 'Toru�', lat: 51.06, lon: 23.08},
   		{label: 'Warszawa', lat: 52.15, lon: 21.00},
   		{label: 'Wroc�aw', lat: 51.06, lon: 17.02},
   		{label: 'Zakopane', lat: 49.18, lon: 19.58},
   		{label: 'Zielona G�ra', lat: 51.42, lon: 19.42}
   	];
	var defaultCity = 17;
	var defaultConfig = {cityIndex: 17, today: true};
	
	return {
		cities: cities,
		saveConfiguration: function(config) {
			var config = config || defaultConfig;
			
			$j.cookie('docusafe_weather_config', JSON.stringify(config), {path: '/'});
		},
		loadConfiguration: function() {
			var data = $j.cookie('docusafe_weather_config');
			
			return data ? JSON.parse(data) : defaultConfig;
		},
		findNearestCity: function(lat, lon) {
			function distance(lat1, lon1, lat2, lon2) {
				var deg2rad = function(degr) {
					return (degr / 180) * 3.142
				}
				var R = 6371; // km
				var dLat = deg2rad(lat2-lat1);
				var dLon = deg2rad(lon2-lon1);
				var lat1 = deg2rad(lat1);
				var lat2 = deg2rad(lat2);
	
				var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
				        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
				var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
				var d = R * c;
				return d;
			}
			
			var i, city, distances = [];
			var nearestIndex = 0, nearestDist;
			var allCities = cities.slice();

			for(i = 0; i < allCities.length; i ++) {
			    city = allCities[i];
			    city.dist = distance(lat, lon, city.lat, city.lon);
			    city.index = i;
			}
			allCities.sort(function(a, b) {
				return a.dist - b.dist;
			});
			return allCities[0];
		},
		createWeatherContent: function(config, jqAjaxLoader) {
			var html = '<div class="weatherItem">';
			html += '<select class="weatherCitySelector"></select>';
			html += '<div class="weatherTemp">'+ 0 +'&deg;</div>';
			html += '<div class="forecastSwitcher"><a class="today" href="#">dzisiaj</a><a class="tomorrow" href="#">jutro</a></div>';
			html += '<div class="weatherRange"><div class="weatherRangeMax"></div><div class="weatherRangeMin"></div></div>';
			html += '<div class="weatherWind">'
			html += '<div class="weatherWindSpeed"></div><div class="weatherWindSpeedUnit">km/h</div>';
			html += '</div>';
			html += '<div class="weatherWindDirection"></div>';
			html += '<div class="clearBoth"></div>'
			html += '<div class="geolocationSwitcher"><input class="btn" type="button" value="Ustal moje po�o�enie" /></div>';
			html += '</div>';
			
			var jqHtml = $j(html);	
			var jqGeolocationSwitcher = jqHtml.find('.geolocationSwitcher').find('input');
			var jqCitySelector = jqHtml.find('.weatherCitySelector');
			var jqWeatherTemp = jqHtml.find('.weatherTemp');
			var jqWeatherRangeMax = jqHtml.find('.weatherRange').find('.weatherRangeMax');
			var jqWeatherRangeMin = jqHtml.find('.weatherRange').find('.weatherRangeMin');
			var jqWeatherWindDir = jqHtml.find('.weatherWindDirection');
			var jqWeatherWindSpeed = jqHtml.find('.weatherWind').find('.weatherWindSpeed');
			var jqToday = jqHtml.find('.forecastSwitcher').find('.today');
			var jqTomorrow = jqHtml.find('.forecastSwitcher').find('.tomorrow');
			
			var i, optionsHtml = '';
			for(i = 0; i < cities.length; i ++) {
				optionsHtml += '<option value="' + i + '">' + cities[i].label + '</option>';
			}
			jqCitySelector.html(optionsHtml);
			
			var weatherData = null;
			jqCitySelector.bind('change.city', function() {
				var selectedCity = cities[this.value];
				config.cityIndex = this.value;
				WeatherUtils.saveConfiguration(config);
				// Create World Weather Online URL
				var url = 'http://free.worldweatheronline.com/feed/weather.ashx?q=' + selectedCity.lat + ',' + selectedCity.lon + '&format=json&num_of_days=2&key=d73c162604132049120103';
				$j.ajax({
					url: url,
					dataType: 'jsonp',
					success: function(data) {
						data = data.data;
						weatherData = data;
						var currentCondition = data.current_condition[0];
						var forecastToday = data.weather[0];
						if (data.current_condition != null) {
							if(config.today == true) 
								jqToday.trigger('click.weather');
							else
								jqTomorrow.trigger('click.weather');
						} else {
							if (options.showerror) $e.html('<p>Weather information unavailable</p>');
						}
					},
					error: function(data) {
						if (options.showerror)  $e.html('<p>Weather request failed</p>');
					}
				});
			});
			jqCitySelector.val(config.cityIndex).trigger('change.city');
			
			jqGeolocationSwitcher.bind('click.geolocation', function() {
				if(!geolocationRunning) {
					jqCitySelector.trigger('findNearestCity', [function() {
						jqGeolocationSwitcher.removeAttr('disabled');
					}]);
					$j(this).attr('disabled', 'disabled');
				}
			});
			
			jqToday.bind('click.weather', function() {
				var data = weatherData;
				var currentCondition = data.current_condition[0];
				var forecastToday = data.weather[0];
				jqWeatherTemp.html(currentCondition.temp_C + '&deg;');
				jqWeatherRangeMax.html(forecastToday.tempMaxC + '&deg;');
				jqWeatherRangeMin.html(forecastToday.tempMinC + '&deg;');
				jqWeatherWindDir.html('&uarr;').css('-moz-transform', 'rotate(' + currentCondition.winddirDegree + 'deg)');
				jqWeatherWindSpeed.html(currentCondition.windspeedKmph);
				
				var oldClassName = jqHtml.attr('class').replace('weatherItem', '')
				jqHtml.removeClass(oldClassName).addClass('weather-code-' + currentCondition.weatherCode);
				
				jqTomorrow.removeClass('forecastSelected');
				$j(this).addClass('forecastSelected');
				
				config.today = true;
				WeatherUtils.saveConfiguration(config);
				return false;
			});
			
			jqTomorrow.bind('click.weather', function() {
				var data = weatherData.weather[1];
				var tempC = Math.round((data.tempMaxC - data.tempMinC) / 2);
				jqWeatherTemp.html(tempC+ '&deg;');
				jqWeatherRangeMax.html(data.tempMaxC + '&deg;');
				jqWeatherRangeMin.html(data.tempMinC + '&deg;');
				jqWeatherWindDir.html('&uarr;').css('-moz-transform', 'rotate(' + data.winddirDegree + 'deg)');
				jqWeatherWindSpeed.html(data.windspeedKmph);

				var oldClassName = jqHtml.attr('class').replace('weatherItem', '')
				jqHtml.removeClass(oldClassName).addClass('weather-code-' + data.weatherCode);
				
				jqToday.removeClass('forecastSelected');
				$j(this).addClass('forecastSelected');
				
				config.today = false;
				WeatherUtils.saveConfiguration(config);
				
				return false;
			});
			
			var geolocationRunning = false;
			jqCitySelector.bind('findNearestCity', function(event, callback) {
				geolocationRunning = true;
				jqAjaxLoader.show();
				navigator.geolocation.getCurrentPosition(function(data) {
					geolocationRunning = false;
					var city = WeatherUtils.findNearestCity(data.coords.latitude, data.coords.longitude);
					jqCitySelector.val(city.index).trigger('change.city');
					jqAjaxLoader.hide();
					if(callback) {
						callback();
					}
				},
				function (error)
				{
					switch(error.code) 
					{
						case error.TIMEOUT:
							//alert ('Timeout');
							break;
						case error.POSITION_UNAVAILABLE:
							//alert ('Position unavailable');
							break;
						case error.PERMISSION_DENIED:
							geolocationRunning = false;
							jqAjaxLoader.hide();
							jqGeolocationSwitcher.attr('checked', false).attr('disabled', 'disabled');
							break;
						case error.UNKNOWN_ERROR:
							//alert ('Unknown error');
							break;
					}
				});
			});
			
			return jqHtml;
		}
	}
})(); 



