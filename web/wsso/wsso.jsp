<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>WSSO</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<br/>

    <ww:if test ="link != null">
        <a href="<ww:property value="link"/>"><ds:lang text="PrzejdzWSSO"/></a>
        <ds:available test="wsso.auto">
            <script type="text/javascript">
            $j(document).ready(function() {
                window.location.href = '<ww:property value="link"/>';
            });
            </script>
        </ds:available>
    </ww:if>