<%@page import="pl.compan.docusafe.core.office.Rwa"%>
<%@ page import="java.util.Date,
                 pl.compan.docusafe.core.office.OfficeFolder,
                 pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.util.TextUtils,
                 java.util.List,
                 java.util.Iterator,
                 java.util.Calendar,
                 org.hibernate.Hibernate,
				 java.util.GregorianCalendar,
                 java.io.IOException"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/archiwum/pick-portfolio.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'doPreCreate'" id="doPreCreate"/>
<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
<ww:hidden name="'divisionGuid'" value="divisionGuid"/>
<ww:hidden name="'documentIncomingDate'" value="documentIncomingDate"/>

<table width="100%">
<tr>
    <td>
        <ww:property value="treeHtml" escape="false"/>
    </td>
</tr>

<a href="javascript:selectedPortfolio(<ww:property value="id"/>, '<ww:property value="officeId"/>',
'<ww:property value="name"/>');">
<%!
	
void printPortfolios(List folders, List selected, String divisionGuid, JspWriter out, 
        Boolean showAll, Integer level, Integer minYear, String user, String documentIncomingDate, Integer year, String baseLink) throws IOException
   {
   	Integer prevYear = 0;
   	String style;
   	String finishDate;
       if (folders == null || folders.size() == 0)
           return;
       	
       out.println("<ul style='list-style-type:none'>");
       for (Iterator iter=folders.iterator(); iter.hasNext(); )
       {
           OfficeFolder folder = (OfficeFolder) iter.next();
           
           boolean active = folder.getRwa().getIsActive();



       	if (level.equals(1))
       	{  
           	if ((prevYear.equals(minYear)) && (minYear.equals(folder.getYear() + 1))) {
           		out.print("<p><hr size=\"1\" align=\"left\" color=\"black\" width=\"100%\"/></p>");
         		}
         		prevYear = folder.getYear();
         	}
         	
           Long parentId = (folder.getParent() != null)?folder.getParent().getId():null;
           style = "";
           if(active)
           {
           if ((selected == null) || (selected.contains(folder.getId()))) {
           	if (selected.get(selected.size()-1).equals(folder.getId()))
           		style = "style=\"text-decoration:underline;";
           	out.print("<li style='list-style-type:none'><a href='"+ baseLink +"?targetOfficeFolderId="+parentId+"&divisionGuid="+divisionGuid+"&showAll="+showAll+"&documentIncomingDate="+documentIncomingDate+"&year="+year+"'> - </a>");
          	}
           else
           	out.print("<li style='list-style-type:none'><a href='"+ baseLink +"?targetOfficeFolderId="+folder.getId()+"&divisionGuid="+divisionGuid+"&showAll="+showAll+"&documentIncomingDate="+documentIncomingDate+"&year="+year+"'> + </a>");
           }
           if (user.equals(folder.getAuthor()))
           {
           	if (style.equals(""))
           		style = "style=\"";
           		if(active)
           		{
           			style = style + "font-style:italic;color:#813526";
           		}
           		else
           		{
           			style = style + "font-style:italic;color:#213222";
           		}
           } 
           if (!style.equals(""))
           	style = style + "\"";
           
           documentIncomingDate = TextUtils.trimmedStringOrNull(documentIncomingDate);
           Date date = DateUtils.nullSafeParseJsDate(documentIncomingDate);
           finishDate = null;
			if ((date != null) && (folder.getDays() != null) && (folder.getDays() > 0)) {
				//System.out.println("dni "+folder.getDays());
           	final Calendar cal = new GregorianCalendar();
           	cal.setTime(date);
           	//System.out.println("datka jest: "+DateUtils.formatJsDate(cal.getTime()));
           	cal.add(Calendar.DATE, folder.getDays());
           	finishDate = DateUtils.formatJsDate(cal.getTime());
           }
           
		   if(active)
		   {
           		out.print("<a name='pickPortfolio' id='"+folder.getOfficeId()+"' "+style+"href=\"javascript:selectedPortfolio("+folder.getId()+
               		", '"+folder.getOfficeId()+"', '"+folder.getName()+"', "+((finishDate==null)?"null":("'"+finishDate+"'"))+");\">");
		  		 String tmp = ((showAll)?(" - "+folder.getYear()):"");
          		 out.print("("+folder.getOfficeId()+") " + folder.getName() + tmp + "</a></li>");
		   }
		   else
		   {
			   out.print("<p>" +folder.getId());
				   String tmp = ((showAll)?(" - "+folder.getYear()):"");
		           out.print("("+folder.getOfficeId()+") " + folder.getName() + tmp + " [nieaktywne]</p></li>");
		   }
           if ((selected != null) && (selected.contains(folder.getId())))
           	printPortfolios(folder.getSubfolders(), selected, divisionGuid, out, showAll, level+1, minYear, user, documentIncomingDate, year, baseLink);
                  
       }
       out.println("</ul>");
   }
%>

<ww:if test="targetDivision != null && !targetDivision.root">
    <tr>
        <td>
            <p>Spis akt w wydziale</p>                        
            
            <ww:set name="portfolios" value="portfolios" scope="page"/>
            <ww:set name="selectedOfficeFolders" value="selectedOfficeFolders" scope="page"/>
            <ww:set name="guid" value="divisionGuid" scope="page"/>
            <ww:set name="showAll" value="showAll" scope="page"/>
            <ww:set name="minYear" value="minYear" scope="page"/>
            <ww:set name="user" value="user" scope="page"/>
            <ww:set name="documentDate" value="documentIncomingDate" scope="page"/>
            <ww:set name="baseLink" value="baseLink" scope="page"/>
			<ww:set name="yearC" value="year" scope="page"/>
            
            <% printPortfolios((List) pageContext.findAttribute("portfolios"), (List) pageContext.findAttribute("selectedOfficeFolders"), 
            	(String) pageContext.findAttribute("guid"), out, (Boolean) pageContext.findAttribute("showAll"), 1, (Integer) pageContext.findAttribute("minYear"), 
            	(String) pageContext.findAttribute("user"), (String) pageContext.findAttribute("documentDate"), (Integer) pageContext.findAttribute("yearC"), "pick-portfolio.action"); %>
            	
            	
            	
            	
<%--            <jsp:include page="portfolios-list-rec.jsp"/>--%>
<%--
            <ul>
                <ww:set name="portfolios" value="portfolios" scope="request"/>
                <jsp:include page="portfolios-list-rec.jsp"/>

                <ww:iterator value="portfolios">
                    <li><a href="javascript:selectedPortfolio(<ww:property value="id"/>, '<ww:property value="officeId"/>', '<ww:property value="name"/>');">
                        <ww:property value="name"/> (<ww:property value="officeId"/>)
                    </a></li>

                    <ww:if test="!subFolders.empty

                    <ww:if test="!subPortfolios.empty">
                        <ul>
                            <ww:iterator value="subPortfolios">
                                <li><a href="javascript:selectedPortfolio(<ww:property value="id"/>, '<ww:property value="portfolioId"/>', '<ww:property value="name"/>');">
                                    <ww:property value="name"/> (<ww:property value="portfolioId"/>)
                                </a></li>
                            </ww:iterator>
                        </ul>
                    </ww:if>
                </ww:iterator>
            </ul>
--%>
<!-- 
			<p>
				<ww:if test="!showAll">
					<a href="<ww:url value="'/office/pick-portfolio.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/><ww:param name="'showAll'" value="true"/><ww:param name="'documentIncomingDate'" value="documentIncomingDate"/></ww:url>">Poka� wszystkie teczki w tym wydziale</a>
				</ww:if>
				<ww:else>
					<a href="<ww:url value="'/office/pick-portfolio.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/><ww:param name="'showAll'" value="false"/><ww:param name="'documentIncomingDate'" value="documentIncomingDate"/></ww:url>">Poka� teczki z aktualnego roku</a>
				</ww:else>
			</p>
-->
			Poka� teczki z roku:		   
					<ww:select name="'year'" list="years" cssClass="'sel'"/>
					<input type="submit" value="Poka�" class="btn"/>
					
			<a href="<ww:url value="'/office/pick-portfolio.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/><ww:param name="'showAll'" value="true"/><ww:param name="'documentIncomingDate'" value="documentIncomingDate"/></ww:url>">Poka� wszystkie teczki w tym wydziale</a>
			
            <p>Nowa teczka</p>
            <table>
            <tr>
                <td>Opis teczki<span class="star">*</span>:</td>
                <td><ww:textfield name="'name'" id="name" size="40" maxlength="126" cssClass="'txt'"/></td>
            </tr>
            <tr>
            	<td>Dni na za�atwienie</td>
            	<td><ww:textfield name="'days'" id="days" size="6" maxlength="5"/></td>
            </tr>
            <tr>
                <td>Kategoria RWA<span class="star">*</span>:</td>
                <td><nobr><ww:textfield name="'prettyRwaCategory'" size="40" maxlength="200" cssClass="'txt readOnly'" id="prettyRwaCategory" readonly="true"/>
                    <input type="button" value='Wybierz kategori� RWA' onclick="javascript:void(window.open('<ww:url value="'/office/rwa/pick-rwa.action'"/>', 'pickrwa', 'width=600,height=600,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn"></nobr>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><ds:submit-event value="'Utw�rz teczk�'" name="'doPreCreate'" disabled="!canCreate"/></td>
            </tr>
            </table>
        </td>
    </tr>
</ww:if>

</table>

</form>

<script language="JavaScript">
function selectedPortfolio(id, portfolioId, name, finishDate)
{
    window.opener.__pick_portfolio(id, portfolioId, name, finishDate);
    window.close();
}

// funkcja wywo�ywana przez okienko wyboru RWA
function pickRwa(rootId, categoryId, rwaString)
{
    document.getElementById('rwaCategoryId').value = categoryId;
    document.getElementById('prettyRwaCategory').value = rwaString;
    document.getElementById('name').value = rwaString.substring(rwaString.indexOf(': ')+2);
}

</script>
</script>
