<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N portfolios.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
				 java.util.Date,
				 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Teczki"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/archives/portfolios-archive.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
	<ww:hidden name="'divisionGuid'" value="divisionGuid"/>
	
	<ww:property value="treeHtml" escape="false"/>
	
	<ww:if test="targetDivision != null && !targetDivision.root">
		<p>Spis akt w wydziale</p>
		<table class="tableMargin">
			<tr>
				<th>
					Znak teczki
				</th>
				<th>
					Rok
				</th>
				<th>
					Tytu�
				</th>
				<th>
					Kod kreskowy
				</th>
				<th>
					Dni na za�atwienie
				</th>
			</tr>
			<ww:iterator value="portfolios">
				<tr <ww:if test="author.equals(user)">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>>
					<td class="alignTop">
						<a href="<ww:url value="'/archives/edit-portfolio-archive.action?id='+id"/>">
							<ww:property value="officeId"/></a>
					</td>
					<td class="alignTop">
						<ww:property value="year"/>
					</td>
					<td class="alignTop">
						<ww:property value="name"/>
					</td>
					<td class="alignTop">
						<ww:property value="barcode"/>
					</td>
					<td class="alignTop">
						<ww:property value="days"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
		<%-- 
		<p>
			Poka� teczki z roku:		   
			<ww:select name="'year'" list="years" cssClass="'sel'"/>
			<input type="submit" value="Poka�" class="btn"/>
		</p>
		
		<p>Nowa teczka</p>
		<table class="tableMargin">
			<tr>
				<td>
					Kategoria RWA<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'prettyRwaCategory'" size="40" maxlength="200" cssClass="'txt readOnly'" id="prettyRwaCategory"  readonly="true" />
						<input type="button" value='Wybierz kategori� RWA' onclick="javascript:void(window.open('<ww:url value="'/office/rwa/pick-rwa.action'"/>?id='+document.getElementById('rwaCategoryId').value, null, 'width=700,height=500,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn">
				</td>
			</tr>
			<tr>
				<td>
					Tytu� teczki
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'name'" id="name" size="40" maxlength="126" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Dni na za�atwienie
				</td>
				<td>
					<ww:textfield name="'days'" size="6" maxlength="5"  cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Kod kreskowy:
				</td>
				<td>
					<ww:textfield name="'barcode'" size="40" maxlength="25" cssClass="'txt'"/>
				</td>
			</tr>
			<ds:available test="teczki.otwieranie.poprzednielata">
			<tr>
				<td>
					Rok otwarcia:
				</td>
				<td>
					<ww:textfield name="'rok'" size="4" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>			
			</ds:available>			
		</table>
		
		<ds:submit-event value="'Utw�rz teczk�'" name="'doPreCreate'" disabled="!canCreate"/>
	 --%></ww:if>
</form>

<%-- <p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p> --%>

<%--
<h1><ds:lang text="Teczki"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/portfolios.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
<ww:hidden name="'divisionGuid'" value="divisionGuid"/>
<table width="100%">
<tr>
	<td>
		<ww:property value="treeHtml" escape="false"/>
	</td>
</tr>
<tr><td><br></td></tr>
<ww:if test="targetDivision != null && !targetDivision.root">
	<tr>
		<td valign="top">
			<p>Spis akt w wydziale</p>
			<table>
				<tr>
					<th>Znak teczki</th><th>Rok</th><th>Tytu�</th><th>Kod kreskowy</th>
				</tr>
				<ww:iterator value="portfolios">
					<tr <ww:if test="author.equals(user)">class="highlightedText"</ww:if>>
						<td valign="top"><a href="<ww:url value="'/office/edit-portfolio.action?id='+id"/>"><ww:property value="officeId"/></a></td>
						<td valign="top"><ww:property value="year"/></td>
						<td valign="top"><ww:property value="name"/></td>
						<td valign="top"><ww:property value="barcode"/></td>
					</tr>
				</ww:iterator>
			</table>

		<%--
			<ul>
				<ww:iterator value="portfolios">
					<ww:if test="!subPortfolio">
						<li><a href="<ww:url value="'/office/edit-portfolio.action?id='+id"/>">
							<ww:property value="name"/> (<ww:property value="portfolioId"/>)
						</a></li>
					</ww:if>
					<ww:if test="!subPortfolios.empty">
						<ul>
							<ww:iterator value="subPortfolios">
								<li><a href="<ww:url value="'/office/edit-portfolio.action?id='+id"/>">
									<ww:property value="name"/> (<ww:property value="portfolioId"/>)
								</a></li>
							</ww:iterator>
						</ul>
					</ww:if>
				</ww:iterator>
			</ul> -->

			<p>
					Poka� teczki z roku:		   
					<ww:select name="'year'" list="years" cssClass="'sel'"/>
					<input type="submit" value="Poka�" class="btn"/>
				<!--  <a href="<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/><ww:param name="'showAll'" value="true"/></ww:url>">Poka� wszystkie teczki</a>-->
				<!--<a href="<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/><ww:param name="'showAll'" value="false"/></ww:url>">Poka� teczki z aktualnego roku</a>-->

			</p>
			
			<p>Nowa teczka</p>
			<table>
			<tr>
				<td>Tytu� teczki<span class="star">*</span>:</td>
				<td><ww:textfield name="'name'" id="name" size="40" maxlength="126" cssClass="'txt'"/></td>
			</tr>
			<tr>
				<td>Dni na za�atwienie</td>
				<td><ww:textfield name="'days'" id="days" size="6" maxlength="5"/></td>
			</tr>
			<tr>
				<td>Kod kreskowy:</td>
				<td><ww:textfield name="'barcode'" size="40" maxlength="25" cssClass="'txt'"/></td>
			</tr>
			<tr>
				<td>Kategoria RWA<span class="star">*</span>:</td>
				<td><ww:textfield name="'prettyRwaCategory'" size="40" maxlength="200" cssClass="'txt readOnly'" id="prettyRwaCategory"  readonly="true" />
					<input type="button" value='Wybierz kategori� RWA' onclick="javascript:void(window.open('<ww:url value="'/office/rwa/pick-rwa.action'"/>?id='+document.getElementById('rwaCategoryId').value, null, 'width=700,height=500,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn">
				</td>
			</tr>
			<tr>
				<td></td>
				<td><ds:submit-event value="'Utw�rz teczk�'" name="'doPreCreate'" disabled="!canCreate"/></td>
			</tr>
			</table>
		</td>
	</tr>
</ww:if>
</table>

</form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
--%>
<script type="text/javascript">
// funkcja wywo�ywana przez okienko wyboru RWA
function pickRwa(rootId, categoryId, rwaString)
{
	document.getElementById('rwaCategoryId').value = categoryId;
	document.getElementById('prettyRwaCategory').value = rwaString;
	document.getElementById('name').value = rwaString.substring(rwaString.indexOf(': ')+2);
}
</script>
<!--N koniec portfolios.jsp N-->