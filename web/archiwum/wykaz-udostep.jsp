<%@ page
	import="pl.compan.docusafe.util.DateUtils,pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<h1
	class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="Rejestr akt udostępnionych" />
</h1>
<hr
	class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>" />


<ds:ww-action-errors />
<ds:ww-action-messages />

<form name="formul"
	action="<ww:url value="'/archives/wykaz-udostep.action'"/>"
	method="post">

	<ds:available test="layout2">
		<div id="middleContainer">
			<!-- BIG TABLE start -->
	</ds:available>



	<table>
		<tr>
			<td>L.p. od:</td>
			<td><ww:textfield name="'lpOd'" id="'lpOd'" cssClass="'txt'"></ww:textfield></td>
			<td>L.p. do:</td>
			<td><ww:textfield name="'lpDo'" id="'lpDo'" cssClass="'txt'"></ww:textfield></td>
		<tr>
		<tr>
			<td>Korzystający:</td>
			<td><ww:select name="'person'" cssClass="'sel combox-chosen'" list="users"
					listKey="name" listValue="lastname+' '+firstname" multiple="false"
					size="3" /></td>
		</tr>

		<tr>
					<script type="text/javascript">
								var _wypDateFrom = "wypDateFrom";
								var _wypDateTo = "wypDateTo";
								var _zwDateFrom = "zwDateFrom";
								var _zwDateTo = "zwDateTo";
							</script>
			<td>Data wypożyczenia od:</td>
			<td><ww:textfield name="'wypDateFrom'" id="wypDateFrom"
					size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_wypDateFrom,_wypDateTo,1)'"/> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_wypDateFrom_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>

			<td>Data wypożyczenia do:</td>
			<td><ww:textfield name="'wypDateTo'" id="wypDateTo" size="10"
					maxlength="10" cssClass="'txt'" onchange="'checkDate(_wypDateFrom,_wypDateTo,1)'" /> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_wypDateTo_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>
		</tr>
		<tr>
			<td>Data zwrotu od:</td>
			<td><ww:textfield name="'zwDateFrom'" id="zwDateFrom" size="10"
					maxlength="10" cssClass="'txt'" onchange="'checkDate(_zwDateFrom,_zwDateTo,1)'"/> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_zwDateFrom_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>

			<td>Data zwrotu do:</td>
			<td><ww:textfield name="'zwDateTo'" id="zwDateTo" size="10"
					maxlength="10" cssClass="'txt'" onchange="'checkDate(_zwDateFrom,_zwDateTo,1)'" /> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_zwDateTo_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>
		</tr>
		<tr>
			<td>Rodzaj pisma:</td>
			<td><ww:select name="'rodzajPisma'" cssClass="'sel'"
					list="rodzajePism" listKey="key" listValue="value" multiple="true"
					size="4" /></td>
			<%-- <td><ww:select name="rodzajPisma" class="sel">
					<option value="puste"><ds:lang text="" /></option>
					<option value="wycofane"><ds:lang text="Wycofane" /></option>
					<option value="udostepnione"><ds:lang text="Udostępnione" /></option>

			</ww:select></td> --%>
		</tr>
		<tr class="formTableDisabled">
			<td><input type="submit" name="doSearch" class="btn"
				value="Filtruj wyniki" /></td>

		</tr>
	</table>

	<table class="search">
		<tr>
			<th><a href="<ww:url value="getSortLink('numer', false)"/>"
				alt="<ds:lang text="SortowanieMalejace"/>"
				title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
						test="sortField == 'numer' && ascending == false">
						<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
			</a> Lp <a href="<ww:url value="getSortLink('numer', true)"/>"
				alt="<ds:lang text="SortowanieRosnace"/>"
				title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'numer' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
			</a> </nobr></th>
			<%-- </a> Document ID <a href="<ww:url value="getSortLink('document_id', true)"/>"
				alt="<ds:lang text="SortowanieRosnace"/>"
				title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'document_id' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
			</a> --%>
			</nobr></th>
			<th><a href="<ww:url value="getSortLink('dataWyp', false)"/>"
				alt="<ds:lang text="SortowanieMalejace"/>"
				title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
						test="sortField == 'dataWyp' && ascending == false">
						<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
			</a> Data wypożyczenia <a href="<ww:url value="getSortLink('dataWyp', true)"/>"
				alt="<ds:lang text="SortowanieRosnace"/>"
				title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'dataWyp' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
			</a> </nobr></th>
			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('data', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'data' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Data zwrotu <a href="<ww:url value="getSortLink('data', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'data' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>
			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('korzystajacy', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'korzystajacy' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Nazwisko i imię korzystajacego z akt <a
						href="<ww:url value="getSortLink('korzystajacy', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'korzystajacy' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>

			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('depart', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'depart' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Komórka organizacyjna lub zakład pracy <a
						href="<ww:url value="getSortLink('depart', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'depart' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>
			<th>Znak akt</th>
			<th>Forma elektroniczna/Fizyczne</th>
		</tr>
		<ww:iterator value="trueResults">
			<ww:set name="result" />
			<tr>
				<ww:iterator value="columns">
					<td><a href="<ww:url value="#result['link']"/>"> <ww:property
								value="prettyPrint(#result[property])" />
					</a></td>

				</ww:iterator>
			</tr>
		</ww:iterator>
	</table>
	<ww:set name="pager" scope="request" value="pager" />
	<table>
		<tr>

			<td align="center"><jsp:include page="/pager-links-include.jsp" />
			</td>
		</tr>
	</table>
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div>
		<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>


</form>

<script type="text/javascript">
		Calendar.setup({
			inputField     :    "wypDateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_wypDateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "wypDateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_wypDateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "zwDateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_zwDateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "zwDateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_zwDateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		
	</script>
