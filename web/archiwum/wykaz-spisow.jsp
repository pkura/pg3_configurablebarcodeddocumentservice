<%--T
	@author: T. Nowak 
C--%>
<!--N wykaz-spisow.jsp N-->


<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="webwork" prefix="ww"%>



<h1>Wykaz spis�w zdawczo-odbiorczych</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />




<ds:ww-action-errors />
<ds:ww-action-messages />


<form name="formul"
	action="<ww:url value="'/archives/wykaz-spisow.action'"/>"
	method="post">
	<ds:available test="layout2">
		<div id="middleContainer">
			<!-- BIG TABLE start -->
	</ds:available>

	<table class="formTable">
<%-- 		<tr>
			<td>Wybierz zesp�:</td>
			<td><ww:select name="'team'" id="team" list="teamList"
					listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
		</tr>
 --%>
		<tr>
			<td>Kategoria archiwalna:</td>
			<td><ww:select name="'achome'" id="achome" list="achomeList"
					listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
		</tr>

		<%-- 		<tr>
			<td>Rodzaj akt:</td>
			<td><ww:select name="'act'" id="act" list="actList"
					listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
		</tr> --%>
		<%-- 		<tr>
			<td>Status:</td>
			<td><ww:select name="'status'" id="status" list="statusList"
					listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
		</tr> --%>
		<tr>
			<td>Numer lub zakres numer�w:</td>
			<td><ww:textfield name="'numerySpisow'" id="'numerySpisow'"></ww:textfield>
				<ds:infoBox body="'Pojedy�cza liczba badz zakres rozdzielony - '"
					header="'Informacja'" /></td>
		</tr>

		<tr class="formTableDisabled">
			<td colspan="2"><input type="submit" name="doSearch" class="btn"
				value="Filrtuj wyniki" /> 
				
<%-- 				<input type="button" id="clearAllFields"
				name="clearAllFields" class="btn"
				value="<ds:lang text='WyczyscWszystkiePola'/>"
				onclick="clearFields()" /> --%>
				</td>
		</tr>


	</table>
<!-- 	<script language="JavaScript">
		function clearFields() {
			javascript: document.formul.reset();
			return false;
		}
	</script> -->


	<table width="100%" class="search">
		<tr>
		<th></th>
		<th><nobr>
				<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				<a href="<ww:url value="getSortLink('numer', false)"/>"
					alt="<ds:lang text="SortowanieMalejace"/>"
					title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
						test="sortField == 'numer' && ascending == false">
						<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a> Nr Spisu <a href="<ww:url value="getSortLink('numer', true)"/>"
					alt="<ds:lang text="SortowanieRosnace"/>"
					title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'numer' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a>
				<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
			</nobr></th>
		<th><nobr>
				<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				<a href="<ww:url value="getSortLink('data', false)"/>"
					alt="<ds:lang text="SortowanieMalejace"/>"
					title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
						test="sortField == 'data' && ascending == false">
						<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a> Data przyj�cia akt <a
					href="<ww:url value="getSortLink('data', true)"/>"
					alt="<ds:lang text="SortowanieRosnace"/>"
					title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'data' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a>
				<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
			</nobr></th>

		<th><nobr>
				<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				<a href="<ww:url value="getSortLink('depart', false)"/>"
					alt="<ds:lang text="SortowanieMalejace"/>"
					title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
						test="sortField == 'depart' && ascending == false">
						<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a> Nazwa jednostki przekazuj�cej akta <a
					href="<ww:url value="getSortLink('depart', true)"/>"
					alt="<ds:lang text="SortowanieRosnace"/>"
					title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
						test="sortField == 'depart' && ascending == true">
						<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
					</ww:if> <ww:else>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
					</ww:else> width="11" height="11" border="0"/>
				</a>
				<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
			</nobr></th>
		<!-- <th>Autor</th> -->
		<th>Liczba pozycji spisu</th>
		<th>Liczba teczek</th>
		<th>Uwagi</th>

	</tr>
		<ww:iterator value="results" status="stat">

			<ww:set name="result" />

			<tr>
				<td><ww:checkbox name="'selectedIds'" fieldValue="id" /></td>
				<ww:iterator value="columns" status="status">

					<td><a href="<ww:url value="getWykazLink(#result)"/>"><ww:property
								value="prettyPrint(#result[property])" /></a></td>

				</ww:iterator>
			</tr>
		</ww:iterator>



		<tr class="formTableDisabled">
			<td colspan="2"><input type="submit" name="doExport" class="btn"
				value="Eksportuj do xls" /></td>
			<td colspan="2"><input type="submit" name="doTemplate" class="btn"
				value="Wygeneruj rtf dla pojedy�czej pozycji" /></td>
	</table>
	<ww:set name="pager" scope="request" value="pager" />
	<table>
		<tr>

			<td align="center"><jsp:include page="/pager-links-include.jsp" />
			</td>
		</tr>
	</table>
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div>
		<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>




</form>