<%@ page
	import="pl.compan.docusafe.util.DateUtils,pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<h1
	class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="Wykaz spisów materiałów archiwalnych" />
</h1>
<hr
	class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>" />


<ds:ww-action-errors />
<ds:ww-action-messages />
<form name="formul"
	action="<ww:url value="'/archives/wykaz-mat-arch.action'"/>"
	method="post">

	<ds:available test="layout2">
		<div id="middleContainer">
			<!-- BIG TABLE start -->
	</ds:available>

	<table>
		<tr>
			<td>Numer od:</td>
			<td><ww:textfield name="'numerOd'" id="'numerOd'"></ww:textfield></td>
			<td>Numer do:</td>
			<td><ww:textfield name="'numerDo'" id="'numerDo'"></ww:textfield></td>
		</tr>
<%-- 		<tr>
			<td>Autor:</td>
			<td><ww:select name="'person'" cssClass="'sel'" list="users"
					listKey="name" listValue="lastname+' '+firstname" multiple="true"
					size="3" /></td>
		</tr> --%>
		<tr>
			<script type="text/javascript">
								var _utwFrom = "utwFrom";
								var _utwTo = "utwTo";
							</script>
			<td>Data przekazania od:</td>
			<td><ww:textfield name="'utwFrom'" id="utwFrom" size="10"
					maxlength="10" cssClass="'txt'" onchange="'checkDate(_utwFrom,_utwTo,1)'"/> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_utwFrom_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>

			<td>Data przekazania do:</td>
			<td><ww:textfield name="'utwTo'" id="utwTo" size="10"
					maxlength="10" cssClass="'txt'" onchange="'checkDate(_utwFrom,_utwTo,1)'"/> <img
				src="<ww:url value="'/calendar096/img.gif'"/>"
				id="calendar_utwTo_trigger"
				style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''" /></td>
		</tr>
		<tr>
			<td>Status:</td>
			<td><ww:select name="'status'" cssClass="'sel'" list="statusy"
					listKey="key" listValue="value" multiple="true" size="4" /></td>
			<%-- <td><ww:select name="rodzajPisma" class="sel">
					<option value="puste"><ds:lang text="" /></option>
					<option value="wycofane"><ds:lang text="Wycofane" /></option>
					<option value="udostepnione"><ds:lang text="Udostępnione" /></option>

			</ww:select></td> --%>
		</tr>
		<tr class="formTableDisabled">
			<td><input type="submit" name="doSearch" class="btn"
				value="Filtruj wyniki" /></td>
		</tr>


	</table>

	<ds:available test="layout2">
		<div id="middleContainer">
			<!-- BIG TABLE start -->
	</ds:available>


	<table width="100%" class="search">
		<%-- 			<tr>
				<ww:iterator value="columns">
					<th><nobr>

							<ww:property value="title" />
						</nobr></th>
				</ww:iterator>
			</tr> --%>
		<tr>
			<th><nobr>

					<a href="<ww:url value="getSortLink('numer', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'numer' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Nr Spisu <a href="<ww:url value="getSortLink('numer', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'numer' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>

				</nobr></th>
			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('data', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'data' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Data przekazania akt <a
						href="<ww:url value="getSortLink('data', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'data' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>
			</nobr>
			</th>
			<!-- <th>Autor spisu</th> -->
			<th>Liczba teczek</th>
			<th>Liczba poz. spisu</th>
			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('depart', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="sortField == 'depart' && ascending == false">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Nazwa komórki przekazującej akta <a
						href="<ww:url value="getSortLink('depart', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'depart' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>
			<th>Uwagi</th>
			<th>Status dokumentu</th>
		<tr>

			<ww:iterator value="trueResults">
				<ww:set name="result" />
				<tr>
					<ww:iterator value="columns">
						<td><a href="<ww:url value="#result['link']"/>"> <ww:property
									value="prettyPrint(#result[property])" />
						</a></td>

					</ww:iterator>
				</tr>
			</ww:iterator>
	</table>

	<ww:set name="pager" scope="request" value="pager" />
	<table width="100%">
		<tr>

			<td align="center"><jsp:include page="/pager-links-include.jsp" />
			</td>
		</tr>
	</table>
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div>
		<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>


</form>

<script type="text/javascript">
Calendar.setup({
	inputField     :    "utwFrom",     // id of the input field
	ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
	button         :    "calendar_utwFrom_trigger",  // trigger for the calendar (button ID)
	align          :    "Tl",           // alignment (defaults to "Bl")
	singleClick    :    true
});
Calendar.setup({
	inputField     :    "utwTo",     // id of the input field
	ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
	button         :    "calendar_utwTo_trigger",  // trigger for the calendar (button ID)
	align          :    "Tl",           // alignment (defaults to "Bl")
	singleClick    :    true
});
</script>


