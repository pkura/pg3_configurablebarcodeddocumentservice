<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-journal.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1>Dziennik </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/edit-journal.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<ww:hidden name="'id'"/>
<ww:hidden name="'divisionGuid'"/>
<input type="hidden" name="doUpdate" id="doUpdate"/>

<table cellspacing="0">
    <tr>
        <td><ds:lang text="Dzial"/>:</td>
        <td><ww:property value="division.prettyPath"/> <ww:if test="division.code != null">(<ww:property value="division.code"/>)</ww:if></td>
    </tr>
    <tr>
        <td><ds:lang text="UzytkownikTworzacy"/>:</td>
        <td><ww:property value="author"/></td>
    </tr>
    <tr>
        <td><ds:lang text="DataUtworzenia"/>:</td>
<%--        <ww:set name="ctime" scope="page" value="journal.ctime"/>--%>
        <td><ds:format-date value="journal.ctime" pattern="dd-MM-yy HH:mm"/></td>
<%--        <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></td>--%>
    </tr>
    <ww:if test="journal.main">
        <ww:hidden name="'description'" value="journal.description"/>
        <tr>
            <td><ds:lang text="Nazwa"/>:</td>
            <td><ww:property value="journal.description"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Rodzaj"/>:</td>
            <td><ww:property value="journal.typeDescription"/></td>
        </tr>
    </ww:if>
    <ww:else>
        <tr>
            <td><ds:lang text="Nazwa"/>:</td>
            <td><ww:textfield name="'description'" size="30" maxlength="510" cssClass="'txt'" value="journal.description"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Rodzaj"/>:</td>
            <td><ww:property value="journal.typeDescription"/></td>
        </tr>
        <tr>
            <td>Symbol:</td>
            <td><ww:textfield name="'symbol'" size="20" maxlength="20" cssClass="'txt'" value="journal.symbol"/></td>
        </tr>
    </ww:else>
    <tr>
        <td><ds:lang text="KolejnyNumerPisma"/>:</td>
        <td><ww:textfield name="'sequenceId'" size="6" maxlength="6" cssClass="'txt'" value="journal.sequenceId" readonly="!canSetSequenceId"/></td>
    </tr>
    <tr>
        <td></td>
        <td><input type="submit" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
            onclick="document.getElementById('doUpdate').value='true';"/>
            <input type="button" value="<ds:lang text="Anuluj"/>" class="btn cancelBtn"
            onclick="document.location.href='<ww:url value="'/office/division-journals.action'"><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>';"/>
        </td>
    </tr>
</table>

</form>
