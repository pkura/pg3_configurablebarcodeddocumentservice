<%--T
	@author: T. Nowak 
C--%>
<!--N find-cases-archive.jsp N-->


<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="webwork" prefix="ww"%>

<style type="text/css">

	.search em {
		font-style: normal;
		background: yellow;
	}

</style>

<script language="JavaScript">
	function pickCase(id, caseId, mode)
	{
		window.opener.pickCase(id, caseId, mode);
		window.close();
	}
 	function changeKind()
	{	
 		//alert(document.getElementById('kind').options.selectedIndex+" "+parseInt(document.getElementById('kind').value));
		 var kindIndex=document.getElementById('kind').options.selectedIndex;
		if(kindIndex==0){
			//teczki
			  //document.getElementById("tr_seqNum").style.display = 'none';
				$j('#tr_seqNum,#td_tytul_sprawy,#td_tytul_dokumentu,#td_znak_sprawy,#td_znak_dokumentu,#tr_docKind').hide();
				$j('#td_tytul_teczki,#td_znak_teczki,#tr_symbol,#tr_kategoria_rwa,#tr_rok,#tr_referent,#tr_officeId').show();
		}
		if(kindIndex==1){
			//sprawy
			//document.getElementById("tr_seqNum").style.display = '';
			$j('#td_tytul_teczki,#td_tytul_dokumentu,#td_znak_teczki,#td_znak_dokumentu,#tr_docKind').hide();
			$j('#tr_seqNum,#td_tytul_sprawy,#td_znak_sprawy,#tr_symbol,#tr_kategoria_rwa,#tr_rok,#tr_referent,#tr_officeId').show();

		}
		if(kindIndex==2){
			//dokumenty
			$j('#tr_seqNum,#td_tytul_teczki,#td_tytul_sprawy,#td_znak_teczki,#td_znak_sprawy,#tr_symbol,#tr_kategoria_rwa,#tr_rok,#tr_referent,#tr_officeId').hide();
			$j('#td_tytul_dokumentu,#td_znak_dokumentu,#tr_docKind').show();
			//document.getElementById("tr_seqNum").style.display = 'none';
		}  
	}
</script>
<h1>Wyszukiwanie teczek/spraw w archiwum</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />



<ds:ww-action-errors />
<ds:ww-action-messages />

<ww:if test="results == null || results.count() == 0">
	<form name="formul"
		action="<ww:url value="'/archives/find-archive.action'"/>"
		method="post">
		<ww:hidden name="'popup'" />
		<ww:hidden name="'mode'" />
		<ww:hidden name="'rwaId'" id="rwaId" value="rwaId" />
				<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		<table class="formTable" >
			<tr>
				<td>Wybierz przedmiot wyszukiwania:</td>
				<td><ww:select name="'kind'" id="kind" list="Kinds"
						listKey="key" listValue="value" cssClass="'sel'"
						onchange="'changeKind();'" ></ww:select></td>
			</tr>

			<ds:available test="findArchive.contentField">
				<tr id="tr_content">
					<td id="td_content">Tre��:</td>
					<td>
						<ww:textfield name="'content'" id="content" size="255" maxlength="255" cssClass="'txt'" />
						<ww:checkbox name="'contentWordOrder'" fieldValue="true"/> Bierz pod uwag� kolejno�� s��w
					</td>
				</tr>
			</ds:available>
			<tr id="tr_officeId">

				<td id="td_znak_teczki">Znak teczki:</td>
				<td id="td_znak_sprawy" hidden="true">Znak sprawy:</td>
				<td id="td_znak_dokumentu" hidden="true">Identyfikator dokumentu:</td>
				<td><ww:textfield name="'officeId'" id="officeId" size="25"
						maxlength="84" cssClass="'txt'" /> <%-- <ds:infoBox body="'Ka�de u�ycie znaku podkre�lenia traktowane jest podczas wyszukiwania przez system jako dowolny znak, a znak procenta jako dowolny ci�g znak�w'" header="'Informacja'"/>  --%>
				</td>
			</tr>
			<tr id="tr_ArchDates">
						<script type="text/javascript">
								var _archDateFrom = "archDateFrom";
								var _archDateTo = "archDateTo";
							</script>
				<td>Data zarchiwizowania od/do:</td>
				<td><ww:textfield name="'archDateFrom'" id="archDateFrom"
						size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_archDateFrom,_archDateTo,1)'" /> <img
					src="<ww:url value="'/calendar096/img.gif'"/>"
					id="calendar_archDateFrom_trigger"
					style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" /> <ww:textfield
						name="'archDateTo'" id="archDateTo" size="10" maxlength="10"
						cssClass="'txt'" onchange="'checkDate(_archDateFrom,_archDateTo,1)'"  /> <img
					src="<ww:url value="'/calendar096/img.gif'"/>"
					id="calendar_archDateTo_trigger"
					style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" /></td>
			</tr>
			<tr id="tr_docKind" hidden="true">
				<td>Rodzaj dokumentu docusafe:</td>
				<td><ww:select name="'docKind'" id="docKind" list="docKinds"
						listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
			</tr>
			<tr>
				<td id="td_tytul_teczki">Tytu� teczki:</td>
				<td id="td_tytul_sprawy" hidden="true">Tytu� sprawy:</td>
				<td id="td_tytul_dokumentu" hidden="true">Tytu� dokumentu:</td>
				<td><ww:textfield name="'title'" id="title" size="25"
						maxlength="84" cssClass="'txt'" /> <%-- <ds:infoBox body="'Ka�de u�ycie znaku podkre�lenia traktowane jest podczas wyszukiwania przez system jako dowolny znak, a znak procenta jako dowolny ci�g znak�w'" header="'Informacja'"/>  --%>
				</td>
			</tr>
			
			<tr id="tr_symbol">
				<td>Symbol z wykazu akt:</td>
				<td><ww:select name="'symbol'" id="symbol" list="symbolList"
						listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
				<tr>
				
			<tr id="tr_kategoria_rwa">
					<td>Kategoria RWA:</td>
					<td><ww:textfield name="'prettyRwaCategory'" size="40"
						maxlength="200" cssClass="'txt'" id="prettyRwaCategory"
						readonly="true" /> <input type="button" value='Wybierz RWA'
					onclick="javascript:void(window.open('<ww:url value="'/office/rwa/pick-rwa.action'"/>?id='+document.getElementById('rwaId').value, null, 'width=700,height=500,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"
					class="btn"></td>
				</tr>
				<tr id="tr_rok">
					<td>Rok:</td>
					<td><ww:textfield name="'year'" size="4" maxlength="4"
						cssClass="'txt'" /></td>
				</tr>
				<tr id="tr_referent">
						<td >Referent:</td>

					<td><ww:select name="'assignedUser'" cssClass="'sel combox-chosen'"
						list="users" listKey="name" listValue="lastname+' '+firstname+'-'+externalName"
						multiple="true" size="4" /></td>
				</tr>
			
			<tr>
				<td>U�ytkownik tworz�cy:</td>
				<td><ww:select name="'person'" cssClass="'sel combox-chosen'" list="users"
						listKey="name" listValue="lastname+' '+firstname+'-'+externalName" multiple="true"
						size="4" /></td>
			</tr>
			


			<tr>
				<td>Kategoria archiwalna:</td>
				<td><ww:select name="'achome'" id="achome" list="achomeList"
						listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
			
			<tr>
			
			<tr>
			<script type="text/javascript">
								var _cdateFrom = "cdateFrom";
								var _cdateTo = "cdateTo";
							</script>
				<td>Data utworzenia od/do:</td>
				<td><ww:textfield name="'cdateFrom'" id="cdateFrom" size="10"
						maxlength="10" cssClass="'txt'" onchange="'checkDate(_cdateFrom,_cdateTo,1)'"  /> <img
					src="<ww:url value="'/calendar096/img.gif'"/>"
					id="calendar_cdateFrom_trigger"
					style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" /> <ww:textfield
						name="'cdateTo'" id="cdateTo" size="10" maxlength="10"
						cssClass="'txt'" onchange="'checkDate(_cdateFrom,_cdateTo,1)'" /> <img
					src="<ww:url value="'/calendar096/img.gif'"/>"
					id="calendar_cdateTo_trigger"
					style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" /></td>
			</tr>
			<%-- <tr id="tr_seqNum" hidden=true>
				<td>Numer sekwencyjny sprawy:</td>
				<td><ww:textfield name="'seqNum'" size="4" maxlength="7"
						cssClass="'txt'" /></td>
			</tr> --%>
<%-- 			<tr>
				<td>Wybierz status archiwum:</td>
				<td><ww:select name="'archiveStatus'" id="archiveStatus" list="archiveStatuses"
						listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
			</tr> --%>
			<tr class="formTableDisabled">
				<td colspan="2"><input type="submit" name="doSearch"
					class="btn" value="Szukaj" /> <input type="button"
					id="clearAllFields" name="clearAllFields" class="btn"
					value="<ds:lang text='WyczyscWszystkiePola'/>"
					onclick="clearFields()" /> <ww:if test="popup">
						<input type="button" class="cancel_btn" onclick="window.close();"
							value="Zamknij" />
					</ww:if></td>
			</tr>
		</table>
	</form>
	<script language="JavaScript" >
		changeKind();
    </script>
	<script language="JavaScript">
	    function clearFields() {
	    	javascript:document.formul.reset();
    		document.getElementById('prettyRwaCategory').value = "";
    		document.getElementById('rwaId').value = '';
    		changeKind();
    		return false;
	    }
		// funkcja wywo�ywana przez okienko wyboru RWA
		function pickRwa(rootId, categoryId, rwaString)
		{
			document.getElementById('rwaId').value = categoryId;
			document.getElementById('prettyRwaCategory').value = rwaString;
		}
	    
</script>
				<script type="text/javascript">
		Calendar.setup({
			inputField     :    "archDateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_archDateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "archDateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_archDateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "cdateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_cdateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "cdateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_cdateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		
	</script>

</ww:if>

<ww:else>
	<ds:available test="layout2">
		<div id="middleContainer">
			<!-- BIG TABLE start -->
	</ds:available>
	<ww:if test="kind==1">
	<!-- teczki -->
		<table width="100%" class="search">
			<th><nobr>
					<%--	<a href="<ww:url value="getSortLink('officeId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('officeId', false)"/>"
						alt="<ds:lang text="SortowanieMalejace"/>"
						title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
							test="(sortField == 'officeId' && ascending == false) || sortField == null">
							<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a> Znak teczki <a
						href="<ww:url value="getSortLink('officeId', true)"/>"
						alt="<ds:lang text="SortowanieRosnace"/>"
						title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
							test="sortField == 'officeId' && ascending == true">
							<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
						</ww:if> <ww:else>
							<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
						</ww:else> width="11" height="11" border="0"/>
					</a>
					<%--	<a href="<ww:url value="getSortLink('officeId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr></th>
			<th>Tytu�</th>
			<th>Rok</th>
			<th>Referent</th>
			<th>Autor</th>
			<th>Miejsce przechowywania</th>
			<th>Data zarchiwizowania</th>
			<ww:iterator value="results">
				<ww:set name="archivedDate" scope="page" value="archivedDate" />
				<tr>
					<td><ww:if test="popup">
							<a
								href="javascript:void(pickCase(<ww:property value="id"/>, '<ww:property value="officeId"/>', '<ww:property value="mode"/>'))"
								name="<ww:property value="officeId"/>"> <ww:property
									value="officeId" /></a>
						</ww:if> <ww:else>
							<ww:set name="c" />
							<a href="<ww:url value="getFolderLink(#c)"/>"><ww:property
									value="officeId" /></a>
						</ww:else></td>
					<td><ww:property value="name" escape="false"/></td>
					<td><ww:property value="year" /></td>
					<td><ww:property value="clerk" escape="false"/></td>
					<td><ww:property value="author" escape="false"/></td>
					<td><ww:property value="storagePlace"/></td>
					<td><fmt:formatDate value="${archivedDate}" type="date"/></td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>
	<ww:elseif test="kind==2">
		<!-- sprawy -->
			<table width="100%" class="search">
				<th><nobr>
						<%--	<a href="<ww:url value="getSortLink('officeId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
						<a href="<ww:url value="getSortLink('officeId', false)"/>"
							alt="<ds:lang text="SortowanieMalejace"/>"
							title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
								test="(sortField == 'officeId' && ascending == false) || sortField == null">
								<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a> Znak sprawy <a
							href="<ww:url value="getSortLink('officeId', true)"/>"
							alt="<ds:lang text="SortowanieRosnace"/>"
							title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
								test="sortField == 'officeId' && ascending == true">
								<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a>
						<%--	<a href="<ww:url value="getSortLink('officeId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					</nobr></th>
				<th><nobr>
						<%--	<a href="<ww:url value="getSortLink('officeId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
						<a href="<ww:url value="getSortLink('title', false)"/>"
							alt="<ds:lang text="SortowanieMalejace"/>"
							title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
								test="(sortField == 'title' && ascending == false) || sortField == null">
								<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a> Tytu� sprawy <a
							href="<ww:url value="getSortLink('title', true)"/>"
							alt="<ds:lang text="SortowanieRosnace"/>"
							title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
								test="sortField == 'title' && ascending == true">
								<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a>
						<%--	<a href="<ww:url value="getSortLink('officeId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					</nobr></th>
				<th>Referent sprawy</th>
				<th><nobr>
						<%--	<a href="<ww:url value="getSortLink('openDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
						<a href="<ww:url value="getSortLink('openDate', false)"/>"
							name="date_descending" alt="<ds:lang text="SortowanieMalejace"/>"
							title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
								test="sortField == 'openDate' && ascending == false">
								<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a> Data za�o�enia <a
							href="<ww:url value="getSortLink('openDate', true)"/>"
							name="date_ascending" alt="<ds:lang text="SortowanieRosnace"/>"
							title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
								test="sortField == 'openDate' && ascending == true">
								<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a>
						<%--	<a href="<ww:url value="getSortLink('openDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					</nobr></th>
				<th><nobr>
						<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
						<a href="<ww:url value="getSortLink('finishDate', false)"/>"
							alt="<ds:lang text="SortowanieMalejace"/>"
							title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
								test="sortField == 'finishDate' && ascending == false">
								<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a> Data zako�czenia <a
							href="<ww:url value="getSortLink('finishDate', true)"/>"
							alt="<ds:lang text="SortowanieRosnace"/>"
							title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
								test="sortField == 'finishDate' && ascending == true">
								<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a>
						<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					</nobr></th>
				<th>Opis</th>
				<th>Miejsce przechowywania</th>
				<th><nobr>
						<%--	<a href="<ww:url value="getSortLink('finishDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
						<a href="<ww:url value="getSortLink('archivedDate', false)"/>"
							alt="<ds:lang text="SortowanieMalejace"/>"
							title="<ds:lang text="SortowanieMalejace"/>"> <ww:if
								test="sortField == 'archivedDate' && ascending == false">
								<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a> Data zarchiwizowania <a
							href="<ww:url value="getSortLink('archivedDate', true)"/>"
							alt="<ds:lang text="SortowanieRosnace"/>"
							title="<ds:lang text="SortowanieRosnace"/>"> <ww:if
								test="sortField == 'archivedDate' && ascending == true">
								<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
							</ww:if> <ww:else>
								<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
							</ww:else> width="11" height="11" border="0"/>
						</a>
						<%--	<a href="<ww:url value="getSortLink('finishDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					</nobr></th>
				
				<th></th>

				<ww:iterator value="results">
					<ww:set name="finishDate" scope="page" value="finishDate"/>
					<ww:set name="openDate" scope="page" value="openDate"/>
					<ww:set name="archivedDate" scope="page" value="archivedDate"/>
					<tr>
						<td><ww:if test="popup">
								<a
									href="javascript:void(pickCase(<ww:property value="id"/>, '<ww:property value="officeId"/>', '<ww:property value="mode"/>'))"
									name="<ww:property value="officeId"/>"> <ww:property
										value="officeId" /></a>
							</ww:if> <ww:else>
								<ww:set name="c" />
								<a href="<ww:url value="getCaseLink(#c)"/>"><ww:property
										value="officeId" /></a>
							</ww:else></td>
						<td><ww:property value="title" escape="false"/></td>
						<td>
							<%--	<ww:property value="#u.firstname+' '+#u.lastname"/>	--%> <ww:property value="clerk" escape="false"/>
						</td>
						<td><fmt:formatDate value="${openDate}" type="both"
								pattern="dd-MM-yy" /></td>
						<td><fmt:formatDate value="${finishDate}" type="date"
									pattern="dd-MM-yy" />
						</td>
						<td><ww:property value="description" escape="false"/></td>
						<td><ww:property value="storagePlace" /></td>
						<td><fmt:formatDate value="${archivedDate}" type="date"/></td>
					</tr>
				</ww:iterator>
			</table>
		</ww:elseif>
		<ww:else>
		<!-- dokumenty -->
		<table class="search" width="100%" cellspacing="0" id="mainTable">
		<tr>
			<ww:iterator value="columns" status="status" >
				<th>
					<nobr>
						<ww:if test="sortDesc != null">
							<%--	<a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortDesc"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
								<ww:if test="property == ('document_' + sortField) && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
						<ww:property value="title"/>
						<ww:if test="sortAsc != null">
							<%--	<a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortAsc"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
								<ww:if test="(sortField == null && property == 'document_title') || (property == ('document_' + sortField) && ascending == true)"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
					</nobr>
				</th>
				<ww:if test="!#status.last">
					<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
				</ww:if>
			</ww:iterator>
		</tr>
		<ww:iterator value="trueResults">
			<ww:set name="result"/>
			<tr>
				<ww:iterator value="columns" status="status">
					 <ww:if test="property == 'attachment_link1' and #result[property] != null">
						<td>
							<ww:if test="#result.canReadAttachments">
								<a href="<ww:url value="#result[property]"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Pobierz"/> (wer. <ww:property value="#result['attachment_rev1']"/>)</a>
							</ww:if>
						</td>
					</ww:if>
					<ww:else>
						<td>
							<a href="<ww:url value="#result['link']"/>"><ww:property value="prettyPrint(#result[property])" escape="false"/></a>
						</td>
					</ww:else>
					<ww:if test="!#status.last">
						<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					</ww:if> 
				</ww:iterator>
			</tr>
		</ww:iterator>

	</table>
		</ww:else>

	<ww:set name="pager" scope="request" value="pager" />
	<table width="100%">
		<tr>
			<td align="center"><jsp:include page="/pager-links-include.jsp" />
			</td>
		</tr>
	</table>

	
	<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="history.back();"/>
	<input type="button" value="Nowe wyszukiwanie" class="btn"
		onclick="document.location.href='<ww:url value="'/archives/find-archive.action'"><ww:param name="'popup'" value="popup"/></ww:url>';" />
	<ds:available test="layout2">
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<div class="bigTableBottomSpacer">&nbsp;</div>
		</div>
		<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</ww:else>
