<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="pl.compan.docusafe.util.TextUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<div class="sp-column" column=0></div>

<div class="sp-column" column=1></div>

<div class="sp-column" column=2></div>
<div class="clearBoth"></div>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dashboard.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/imieniny.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dashboard.css'"/>?v=<%= Docusafe.getDistTimestamp() %>" />
<script type="text/javascript">
$j(document).ready(function() {
	var dashboard = new Dashboard($j('.sp-column'));
	$j.post('<ww:url value="'/portlet-start-page.action'"/>',
		{getPortlets: true}, createPortlets);
	
	function createPortlets(strData) {
		var data = JSON.parse(strData), map = {};
		var i, len, columnIndex, config;		
		var columnIndex, column, cn, item;
		
		for(i = 0; i < data.length; i ++) {
			map[data[i].cn] = data[i];
		}
		
		config = dashboard.loadConfiguration();
		if(config) {
			for(columnIndex = 0; columnIndex < config.length; columnIndex ++) {
				column = config[columnIndex];
				for(i = 0; i < column.length; i ++) {
					cn = column[i];
					try {
						item = map[cn];
						switch(item.type) {
							case 'CALENDAR':
								dashboard.addPortlet(dashboard.createCalendarPortlet(item), columnIndex);
							break;
							case 'NAMEDAY':
								dashboard.addPortlet(dashboard.createNamedayPortlet(item), columnIndex);
							break;
							case 'WEATHER':
								dashboard.addPortlet(dashboard.createWeatherPortlet(item), columnIndex);
							break;
							default:
								dashboard.addPortlet(dashboard.createPortlet(item), columnIndex);
						}
						delete map[cn];
					} catch(e) {}
				}
			}
			
			// w mapie zosta�y tylko niewidoczne elementy
			for(cn in map) {
				item = map[cn];
				switch(item.type) {
					case 'CALENDAR':
						dashboard.addPortlet(dashboard.createCalendarPortlet(item), 0, true);
					break;
					case 'NAMEDAY':
						dashboard.addPortlet(dashboard.createNamedayPortlet(item), 0, true);
					break;
					case 'WEATHER':
						dashboard.addPortlet(dashboard.createWeatherPortlet(item), 0, true);
					break;
					default:
						dashboard.addPortlet(dashboard.createPortlet(item), 0, true);
				}
			}
		} else {
			for(i = 0, len = data.length; i < len; i ++) {
				columnIndex = i % 3;
				switch(data[i].type) {
				case 'CALENDAR':
					dashboard.addPortlet(dashboard.createCalendarPortlet(data[i]), 2);	
				break;
				case 'NAMEDAY':
					dashboard.addPortlet(dashboard.createNamedayPortlet(data[i]), 0, true);
				break;
				case 'WEATHER':
					dashboard.addPortlet(dashboard.createWeatherPortlet(data[i]), 0, true);
				break;
				default:
					dashboard.addPortlet(dashboard.createPortlet(data[i]), columnIndex);				
				}
			}
		}
		len = 0;
		var visibleAddPortlet = true;
		for(cn in dashboard.portlets) { len += dashboard.portlets[cn].data.type != 'add' ? 1 : 0; }
		if(dashboard.jqColumns.find('.sp-portlet[id]').length == len)
			visibleAddPortlet = false;
		
		dashboard.addPortlet(
			dashboard.createSpecialPortlet({
				cn: 'add-portlet',
				type: 'add'
			}), 0, !visibleAddPortlet);
		if(visibleAddPortlet)
			dashboard.adjustAddPortletColumn();
	}
});
</script>



