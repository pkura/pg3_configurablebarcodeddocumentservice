/*
 * MImageAreaSelection.js
 *
 * (c) 2006 Michal Wojciechowski <odyniec at odyniec dot net>
 *
 */

if (typeof MClass == "undefined") alert("`MClass.js' is not loaded.");
if (typeof MEvent == "undefined") alert("`MEvent.js' is not loaded.");

var MImageAreaSelection = new MClass({
  frameWidth: 1,
  frameColor1: "#FF0000",
  frameColor2: "#ff0000",
  boxColor: "#fff",
  boxOpacity: 0,
  
  onSelectStart: function () {},
  onSelectChange: function () {},
  onSelectEnd: function () {},
  
  selectionX1: 0,
  selectionY1: 0,
  selectionX2: 0,
  selectionY2: 0,
  selectionWidth: 0,
  selectionHeight: 0,

  image: null,
  overlay: null,
  frame1: null,
  frame2: null,
  frame3: null,
  selecting: null,
  startPos: null,
  
  
  initialize: function (image) {
    this.image = image;

    this.overlay = document.createElement("DIV");
    this.frame1 = document.createElement("DIV");
    this.frame2 = document.createElement("DIV");
    this.frame3 = document.createElement("DIV");

    this.overlay.style.position = "absolute";
    var imgOfs = getOffset(this.image);
    this.overlay.style.left = imgOfs.x + "px";
    this.overlay.style.top = imgOfs.y + "px";
    this.overlay.style.width = this.image.offsetWidth + "px";
    this.overlay.style.height = this.image.offsetHeight + "px";
    this.overlay.style.overflow = "hidden";

    // Method #1: use the image as overlay background (effectively putting it over
    // the original image)
    //this.overlay.style.backgroundImage = "url(" + this.image.src + ")";
    //this.overlay.style.backgroundPosition = this.overlayMargin + "px " +
    //  this.overlayMargin + "px";
    //this.overlay.style.backgroundRepeat = "no-repeat";
    
    // Method #2: set overlay's background color and reset its opacity to zero
    this.overlay.style.backgroundColor = "#fff";
    this.overlay.style.opacity = "0";
    this.overlay.style.filter = "alpha(opacity=0)";

    this.frame1.style.position = this.frame2.style.position = 
	   this.frame3.style.position = "absolute";
    this.frame1.style.display = this.frame2.style.display = 
	   this.frame3.style.display = "none";
    this.frame1.style.lineHeight = this.frame2.style.lineHeight =
	   this.frame3.style.lineHeight = "0px";
    this.frame1.style.borderStyle = "solid";
    this.frame1.style.borderWidth = "1px";
    this.frame2.style.borderStyle = "dashed";
    this.frame2.style.borderWidth = "1px";
    this.frame3.style.borderColor = "transparent";
    this.frame3.style.borderWidth = "1px";

	 this.setFrameColor1(this.frameColor1);
	 this.setFrameColor2(this.frameColor2);
	 this.setBoxColor(this.boxColor);
	 this.setBoxOpacity(this.boxOpacity);

    this.selecting = false;

    this.image.parentNode.appendChild(this.frame1);
    this.image.parentNode.appendChild(this.frame2);
    this.image.parentNode.appendChild(this.frame3);
    this.image.parentNode.appendChild(this.overlay);

    MEvent.addEventHandler(this.overlay, "mousemove",
    	MEvent.bindFunction(this, this.imageMouseMove), true);

    MEvent.addEventHandler(this.overlay, "mousedown",
    	MEvent.bindFunction(this, this.imageMouseDown), true);

    MEvent.addEventHandler(this.overlay, "mouseup",
    	MEvent.bindFunction(this, this.imageMouseUp), true);

    MEvent.addEventHandler(this.overlay, "selectstart", function (event) {
    	stopPropagation(event);
    	
    	return false;
    }, true);

    MEvent.addEventHandler(this.overlay, "dragstart", function (event) {
     	stopPropagation(event);
    	
    	return false;
    }, true);

    MEvent.addEventHandler(this.image, "selectstart", function (event) {
    	stopPropagation(event);
    	
    	return false;
    }, true);
  },


  setFrameWidth: function (width) {
    this.frameWidth = width;
    this.frame1.style.borderWidth = this.frameWidth + "px";
    this.frame2.style.borderWidth = this.frameWidth + "px";
  },

  setFrameColor1: function (color1) {
    this.frameColor1 = color1;
    this.frame1.style.borderColor = this.frameColor1;
  },

  setFrameColor2: function (color2) {
    this.frameColor2 = color2;
    this.frame2.style.borderColor = this.frameColor2;
  },
  

  setBoxColor: function (boxColor) {
    this.boxColor = boxColor;
	 this.frame3.style.backgroundColor = this.boxColor;
  },


  setBoxOpacity: function (boxOpacity) {
    this.boxOpacity = boxOpacity;
    this.frame3.style.opacity = this.boxOpacity;
    this.frame3.style.filter = "alpha(opacity=" +
	   parseInt(this.boxOpacity * 100) + ")";
  },


  imageMouseDown: function (event)
  {
    this.startPos = getMouse(event);

    this.frame2.style.width = this.frame1.style.width = "0px";
    this.frame2.style.height = this.frame1.style.height = "0px";
    this.frame2.style.left = this.frame1.style.left = this.startPos.x + "px";
    this.frame2.style.top = this.frame1.style.top = this.startPos.y + "px";
    this.frame2.style.display = this.frame1.style.display = 
	   this.frame3.style.display = "block";
	   
	 this.frame3.style.width = "0px";
	 this.frame3.style.height = "0px";

    this.selecting = true;

    this.overlay.style.left = "0px";
    this.overlay.style.top = "0px";
    //this.overlay.style.width = document.body.clientWidth + "px";
    //this.overlay.style.height = document.body.clientHeight + "px";
    this.overlay.style.width = getPageDimensions().width + "px";
    this.overlay.style.height = getPageDimensions().height + "px";

    this.onSelectStart();
  },


  imageMouseUp: function (event)
  {
    this.selecting = false;

    var imgOfs = getOffset(this.image);

    this.overlay.style.left = imgOfs.x + "px";
    this.overlay.style.top = imgOfs.y + "px";
    this.overlay.style.width = this.image.offsetWidth + "px";
    this.overlay.style.height = this.image.offsetHeight + "px";

    this.onSelectEnd();
  },


  imageMouseMove: function (event)
  {
    if (this.selecting) {
      var pos = getMouse(event);

      var x1 = (pos.x > this.startPos.x ? this.startPos.x : pos.x);
      var x2 = (pos.x > this.startPos.x ? pos.x : this.startPos.x);
      var y1 = (pos.y > this.startPos.y ? this.startPos.y : pos.y);
      var y2 = (pos.y > this.startPos.y ? pos.y : this.startPos.y);

      var imgOfs = getOffset(this.image);

      /* for IE in non-strict mode, this won't work correctly */
      var d = 2 * this.frameWidth;

      if (x1 < imgOfs.x) x1 = imgOfs.x;
      if (x1 > imgOfs.x + this.image.offsetWidth - d) x1 = imgOfs.x + this.image.offsetWidth - d;
      if (y1 < imgOfs.y) y1 = imgOfs.y;
      if (y1 > imgOfs.y + this.image.offsetHeight - d) y1 = imgOfs.y + this.image.offsetHeight - d;
      if (x2 < imgOfs.x) x2 = imgOfs.x;
      if (x2 > imgOfs.x + this.image.offsetWidth - d) x2 = imgOfs.x + this.image.offsetWidth - d;
      if (y2 < imgOfs.y) y2 = imgOfs.y;
      if (y2 > imgOfs.y + this.image.offsetHeight - d) y2 = imgOfs.y + this.image.offsetHeight - d;
      
      this.selectionX1 = x1 - imgOfs.x;
      this.selectionY1 = y1 - imgOfs.y;
      this.selectionX2 = x2 - imgOfs.x;
      this.selectionY2 = y2 - imgOfs.y;

      this.frame1.style.left = this.frame2.style.left = x1 + "px";
      this.frame1.style.top = this.frame2.style.top = y1 + "px";
      this.frame1.style.width = this.frame2.style.width =
	    (x2 - x1) + "px";
      this.frame1.style.height = this.frame2.style.height =
	    (y2 - y1) + "px";
	    
	   this.frame3.style.left = (x1 + 1) + "px";
	   this.frame3.style.top = (y1 + 1) + "px";
	   this.frame3.style.width = (x2 - x1) + "px";
	   this.frame3.style.height = (y2 - y1) + "px";

      this.selectionWidth = x2 - x1;
      this.selectionHeight = y2 - y1;
      
      this.onSelectChange();
    }
  }
});
