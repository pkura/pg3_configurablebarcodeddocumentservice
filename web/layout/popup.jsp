<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ds:available test="layout2">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
</ds:available>
<!-- Nie dodawac komentarzy html powyzej tej linii --> 

<html:html>
<head>
	<title><ds:lang text="TytulStrony"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
	<script src="<%= request.getContextPath() %>/functions.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>" />
	
	<c:set var="haveCalendar">
	<tiles:getAsString name="haveCalendar" ignore="true" /></c:set>
	<c:if test="${haveCalendar == 'true'}">
		<link rel="stylesheet" type="text/css" media="all" href="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-win2k-cold-1.css"  />
		<!-- main calendar program -->
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar.js"></script>
		<!-- language for the calendar -->
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/lang/calendar-pl.js"></script>
		<!-- the following script defines the Calendar.setup helper function, which makes adding a calendar a matter of 1 or 2 lines of code. -->
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-setup.js"></script>
	</c:if>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/custom-form-elements.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript">
		var $j = jQuery.noConflict();

		// skrypty dla nowego layoutu 2.0
		<ds:available test="layout2">


		$j(document).ready(function() {
		/* przyciski gmenu */
		$j('.cancel_btn').removeClass('cancel_btn').addClass('btn');
		$j('.btn').hover(function() {
			$j(this).addClass('btnHover');
		}, function() {
			$j(this).removeClass('btnHover');
		});

		$j('.gmenuButton').hover(function() {
			$j(this).addClass('gmenuButtonOver');
		}, function() {
			$j(this).removeClass('gmenuButtonOver');
		});

		$j('input:submit:not(:disabled):first').css('border',' 1px solid black');

		/* pola tekstowe gmenu */
		$j('input.txt_opt').removeClass('txt_opt').addClass('txt');
		$j('input.txt, textarea.txt, textarea.txt_opt').hover(function() {
			$j(this).addClass('txtHover');
		}, function() {
			$j(this).removeClass('txtHover');
			if(!ie)		// obejscie bugu w mozilli
				$j('input.txt, textarea.txt, textarea.txt_opt').removeClass('txtHover');
		}).focus(function() {
			$j(this).addClass('txtHover');
		}).blur(function() {
			$j(this).removeClass('txtHover');
			if(!ie)		// obejscie bugu w mozilli
				$j('input.txt, textarea.txt, textarea.txt_opt').removeClass('txtHover');
		});

		/* magiczne checkboxy */
		$j(":checkbox:not(.styled, :disabled)").addClass('styled');
		Custom.init();
		
		$j(':checkbox:disabled:not(:checked)').after('<span class="checkboxDisabled" style="cursor:default">&nbsp;</span>').hide();
		$j(':checkbox:disabled:checked').after('<span class="checkboxDisabled checked" style="cursor:default">&nbsp;</span>').hide();

		/* Zamiana gwiazdek na fajne ikonki */
		$j('.star').attr('innerHTML', "<img title='<ds:lang text="PoleObowiazkowe"/>' src=<c:out value='${pageContext.request.contextPath}'/>/img/warn.gif />");

		});
		</ds:available>
	</script>
</head>

<body marginwidth="0" leftmargin="0" topmargin="0" marginheight="0">
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<%! String upperBarColor = Docusafe.getProperties().getProperty("upperBarColor", "black");%>
			<% if (upperBarColor.equals("black")){%>
				<td class="popup-header" style="text-align: right;" >
					<a href="javascript:window.close();"><ds:lang text="Zamknij"/></a>
				</td>
			<%}else{%>
				<td class="upperBar" style="text-align: right;">
					<a class="upperMenuLink" href="javascript:window.close();"><ds:lang text="Zamknij"/></a>
				</td>
			<%}%>
		</tr>
		<tr>
			<td valign="top" class="mai"  >
				<tiles:insert name="body"/>
			</td>
		</tr>
	</table>
</body>
</html:html>