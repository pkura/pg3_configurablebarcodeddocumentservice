<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
       "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N ww-popup.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<html:html>
<head>
	<title><ds:lang text="TytulStrony"/></title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
	<!-- <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"> -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<script type="text/javascript" src="<%= request.getContextPath() %>/jquery-1.7.1.min.js"></script>
	<script src="<%= request.getContextPath() %>/functions.js"></script>
	<script src="<%= request.getContextPath() %>/json.js"></script>
	<script src="<%= request.getContextPath() %>/dockind.js"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/html.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-1.8.custom.min.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-timepicker-addon.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/custom-form-elements.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.mousewheel.min.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/wtooltip.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
	
	<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>" />
	<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/jquery-ui-1.8.custom.css'"/>?v=<%= Docusafe.getDistTimestamp() %>"  />
	<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/jquery-ui-timepicker-addon.css'"/>?v=<%= Docusafe.getDistTimestamp() %>"  />	
	<link rel="stylesheet" type="text/css" media="all" href="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-win2k-cold-1.css"  />
	
	<ds:available test="!jQueryCalendar">
	<!-- main calendar program -->
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar.js"></script>
	<!-- language for the calendar -->
	<%if(!Docusafe.getCurrentLanguage().equals("pl")){%>
		<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-en.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<%}else{  %>
		<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-pl.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
	<%}%>
	<!-- the following script defines the Calendar.setup helper function, which makes adding a calendar a matter of 1 or 2 lines of code. -->
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-setup.js"></script>
	</ds:available>
	
	<ds:available test="jQueryCalendar">
		<%if(Docusafe.getCurrentLanguage().equals("pl")){%>
			<script type="text/javascript" src="<ww:url value="'/calendar096/lang/ui.datepicker-pl.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<%}else{%>
			<%-- Domyslnie jest angielski --%>
		<%}%>
		</ds:available>
	
	<script type="text/javascript">
		var $j = jQuery.noConflict();

		<ds:available test="jQueryCalendar">
		/* Obejscie starego kalendarza na jquery */
			Calendar = function() {
				
			}

			Calendar.setup = function(params) {
				/*$j('#' + params['button']).click(function() {
					alert('klik');
				});*/
				$j(function() {
				//jQuery.fx.off = true;

				$j('#' + params['inputField']).datepicker({showOn: 'button', buttonImage: '<ww:url value="'/calendar096/img.gif'"/>', 
					buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
					changeMonth: true, changeYear: true});
				});
				$j('#' + params['button']).hide();
			}
	</ds:available>

	// skrypty dla nowego layoutu 2.0
	<ds:available test="layout2">
	var ie = document.all && !window.opera ? 1 : 0;
	var ie7 = (navigator.appVersion.indexOf('MSIE 7.')== -1) ? false : true;
	var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
	var __dontFixItemsWidth = true;

	
	$j('document').ready(function() {
		/* przyciski gmenu */
		$j('.cancel_btn').removeClass('cancel_btn').addClass('btn');
		$j('.btn').hover(function() {
			$j(this).addClass('btnHover');
		}, function() {
			$j(this).removeClass('btnHover');
		});

		$j('input:submit:not(:disabled):first').addClass('defaultBtn');
	
	
		/* pola tekstowe gmenu */
		$j('input.txt_opt').removeClass('txt_opt').addClass('txt');
		$j('input.txt, textarea.txt, textarea.txt_opt, select.sel, select.multi_sel').focus(function() {
			$j(this).addClass('txtHover');
	        $j(this).parent().parent().addClass('oddRows');
		}).blur(function() {
			$j(this).removeClass('txtHover');
	        $j(this).parent().parent().removeClass('oddRows');
		});
		
	
		/* magiczne checkboxy */
		$j(":checkbox:not(.styled, :disabled)").addClass('styled');
		Custom.init();
		
		$j(':checkbox:disabled:not(:checked)').after('<span class="checkboxDisabled" style="cursor:default">&nbsp;</span>').hide();
		$j(':checkbox:disabled:checked').after('<span class="checkboxDisabled checked" style="cursor:default">&nbsp;</span>').hide();

		/* Zamiana na style layout2 */
		$j('#middleMenuContainer img:not(.tab2_img)').css('display', 'none');
		$j('#middleMenuContainer a.highlightedText').attr('id', 'tab2_selected').addClass('middleMenuLink').addClass('middleMenuLinkHover').addClass('middleMenuLinkSelected').removeClass('highlightedText').prepend('<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_pushed.gif" class="tab2_img" S/>');
		$j('#middleMenuContainer a:not(.highlightedText, .middleMenuLink)').addClass('middleMenuLink').prepend('<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left.gif" class="tab2_img" />');
		

		$j('a.middleMenuLink').hover(function() {
			var selected = false;
			if($j(this).attr('id') == 'tab2_selected') {
				selected = true;
			}
			if(!selected) {
				var new_src = $j(this).children('.tab2_img').attr('src');
				new_src = new_src.replace('.gif', '_pushed.gif');

				$j(this).addClass('middleMenuLinkHover');			
				$j(this).children('.tab2_img').attr('src', new_src);	
			}
		}, function() {
			var selected = false;
			if($j(this).attr('id') == 'tab2_selected') {
				selected = true;
			}
			if(!selected) {
				var new_src = $j(this).children('.tab2_img').attr('src');
				new_src = new_src.replace('_pushed.gif', '.gif');
				$j(this).children('.tab2_img').attr('src', new_src);
				$j(this).removeClass('middleMenuLinkHover');
			}
		});

		if(ie)
			$j('.tab2_img').css('margin-left', '-3px');	

		/* Zamiana gwiazdek na fajne ikonki */
		$j('.star').attr('innerHTML', "<img title='<ds:lang text="PoleObowiazkowe"/>' src=<c:out value='${pageContext.request.contextPath}'/>/img/warn.gif />");

		prepareButtons();

		/* grupowanie przycisk�w - te kt�r� s� tworzone "dynamicznie" w jsp (tj. w zale�no�ci od availables�w, if�w, uprawnie� itd.) */
		$j('.btnContainer').each(function() {
			if($j(this).children('input.btn:visible').length > 1) {
				$j(this).children('input.btn:first').addClass('btnLeft');
				$j(this).children('input.btn:last').addClass('btnRight');
				$j(this).children('input.btn:visible:not(:first, :last, .btnLeft, .btnRight)').addClass('btnMiddle');
			}
		});

		/* specjalne szerokosci */
		//$j('#extends_group1').css('width', ($j('#group1').width() - 2) + 'px'); // person.jsp

		});
	</ds:available>
	</script>
	<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/boxover.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
</head>


<body marginwidth="0" leftmargin="0" topmargin="0" marginheight="0" onload="">
	<!-- Poczatek ww-popup.jsp -->
	<table width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<%! String upperBarColor = Docusafe.getProperties().getProperty("upperBarColor", "black");%>
			<% if (upperBarColor.equals("black")){%>
				<td class="popup-header" style="text-align: right;" id="close">
					<a href="javascript:void(null);" onclick="javascript:window.close();"><ds:lang text="zamknijOkno"/></a>
				</td>
			<%}else{%>
				<td class="upperBar" style="text-align: right;" id="close">
					<a class="upperMenuLink" href="javascript:void(null);" onclick="javascript:window.close();"><ds:lang text="zamknijOkno"/></a>
				</td>
			<%}%>
		</tr>
		<tr>
			<td valign="top" class="mai"  id="maiTD">
				<script type="text/javascript">
					if(window.location.href.indexOf('viewserver') != -1) {
						<ww:if test="!viewserverWithData">
							$j('#maiTD').attr('align', 'center');
						</ww:if>
						<ds:available test="viewer.narrow">
							$j('#close').parent('tr').hide()
						</ds:available>
					}
				</script>
				<jsp:include page='<%= (String) request.getAttribute("include.location") %>'/>
			</td>
		</tr>
	</table>
	<!-- Koniec ww-popup.jsp -->
</body>
</html:html>