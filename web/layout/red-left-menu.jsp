<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N red-left-menu.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<!--
    Menu powinno pozosta� w oddzielnym pliku tak d�ugo, jak d�ugo
    istniej� dwa pliki z g��wnym layoutem (jeden dla Struts, drugi
    dla WebWork).
-->

<!--<edm-html:select-string-manager name="jspRepositoryLocalStrings" />-->

<table height="100%">
	<tr>
		<td style="vertical-align:top;" vertical-align="top">
			<ww:if test="@pl.compan.docusafe.web.common.Sitemap@dynamicMenu(1) == 'registries'">        
				<ww:if test="menuInfo != null">
					<ww:iterator value="menuInfo.registries">
						<ul>
							<ww:property value="shortName"/>
							<ds:sitemap menu="2" node="node">
								<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
									<li>
										<a <ww:if test="(isInPath(#node)) && (menuInfo.selectedRegistryId == id)">class="active-link"</ww:if> href="<ww:url value="href"/>?registryId=<ww:property value="id"/>"><ww:property value="label"/></a>
									</li>
								</ww:if>
							</ds:sitemap>
						</ul>
					</ww:iterator>
				</ww:if>
			</ww:if>
			<ww:else>
				<ul>
					<ds:sitemap menu="2" node="node">
						<li>
							<a <ww:if test="isInPath(#node)">class="active-link"</ww:if> href="<ww:url value="href"/>"><ww:property value="label"/></a>
						</li>
					</ds:sitemap>
				</ul>
			</ww:else>
		</td>
	</tr>
	<tr>
		<td style="vertical-align: bottom;" vertical-align="bottom">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/bbit.gif" width="80" height="1" />
			<br>
			<ul>
				<li>
					<a href="<ww:url value="'/self-change-password.do'"/>"><ds:lang text="changePassword"/></a>
				</li>
				<li>
					<a href="<ww:url value="'/settings/index.action'"/>"><ds:lang text="settings"/></a>
				</li>
				<!-- U�ytkownik identyfikuj�cy si� certyfikatem nie mo�e si� wylogowa� -->
				<ds:subject hasCertificate="false">
					<li>
						<html:link forward="logout"> <ds:lang text="logout"/></html:link>
					</li>
				</ds:subject>
			</ul>
		</td>
	</tr>
</table>