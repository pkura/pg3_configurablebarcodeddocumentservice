<%@ page import="pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="webwork" prefix="ww" %>

<!--
    Menu powinno pozosta� w oddzielnym pliku tak d�ugo, jak d�ugo
    istniej� dwa pliki z g��wnym layoutem (jeden dla Struts, drugi
    dla WebWork).
-->

<div id="logout-confirm" title="Potwierdzenie wylogowania">
	Czy na pewno chcesz si� wylogowa�?
</div>

<ds:available test="shortLeftMenu">
<style>

ul.left {
	display: block;
	float: right;
	text-align: left; 
	padding-left: 0;
	
	width: 100%;
	list-style: none;
}

.left_ie_fix {
	font-size: 11px !important;
	font-family: arial !important;
	font-weight: normal !important;
	height: 20px;
	padding-bottom: -30px;
	vertical-align: middle;
	background-repeat: no-repeat;
	background-position: 0px 0px;
	text-align: left;
	padding-left: 5px !important;
	width: 160px;
	
	/*_background-image: %left_roundedIE6%;*/
}		

.left_ie_fixHover {
	color: white !important;
	width: 160px;
}

.left_ie_fixSelected {
	color: white !important;
	font-weight: normal !important;
	width: 160px !important;
}

.left_ie_fixSelectedHover {
	width: 160px !important;
}

ul.left a {
	display: block;
	font-size: 11px;
	font-family: arial;
	font-weight: 400;
	line-height: 20px;
	min-height: 21px;
	vertical-align: middle;
	margin-left: 0px;
	/*padding-left: 20px;*/
	padding-left: 5px;
	text-shadow: none;
	background-image: none;
	*width: 160px;
}

ul.left a:link {
	display: block;
	font-size: 11px;
	font-family: arial;
	font-weight: 400;
	line-height: 20px;
	min-height: 21px;
	vertical-align: middle;
	margin-left: 0px;
	/*padding-left: 20px;*/
	padding-left: 5px;
	text-shadow: none;
	background-image: none;
	*width: 160px;
}

ul.left a:visited {
	display: block;
	font-size: 11px;
	font-family: arial;
	font-weight: 400;
	line-height: 20px;
	min-height: 21px;
	vertical-align: middle;
	margin-left: 0px;
	/*padding-left: 20px;*/
	padding-left: 5px;
	text-shadow: none;
	background-image: none;
	*width: 160px;
}

ul.left a:active {
	display: block;
	font-size: 11px;
	font-family: arial;
	font-weight: 400;
	line-height: 20px;
	min-height: 21px;
	vertical-align: middle;
	margin-left: 0px;
	/*padding-left: 20px;*/
	padding-left: 5px;
	text-shadow: none;
	background-image: none;
	*width: 160px;
}

ul.left a:hover {
	color: white;
	font-size: 11px;
	font-family: arial;
	font-weight: normal;
	padding-right: 0px;
	padding-left: 5px;
	height: 20px;
	vertical-align: middle;
	background-repeat: no-repeat;
	background-position: 0px 0px;
	background-image: none;
	*width: 160px;
}

ul.left a.left_selected {
	color: white !important;
	font-size: 11px;
	font-family: arial;
	font-weight: normal;
	padding-left: 5px;
	height: 20px;
	vertical-align: middle;
	background-repeat: no-repeat;
	background-position: 0px 0px;
	text-decoration: underline;
	background-image: none;
	*width: 160px;
}

ul.left a.left_selected:hover {
	color: white;
	font-size: 11px;
	font-family: arial;
	font-weight: normal;
	padding-left: 5px;
	height: 20px;
	vertical-align: middle;
	background-repeat: no-repeat;
	background-position: 0px 0px;
	text-decoration: underline;
	background-image: none;
	*width: 160px;
}

ul.left a.left_logout {
	color: #DC143C !important;
	background-image: none;
}

ul.left a.left_logout:hover {
	color: White !important;
	background-color: #DC143C;
	background-image: none;
}

</style>

</ds:available>

<!--<edm-html:select-string-manager name="jspRepositoryLocalStrings" />-->
<ds:additions test="tcLayout">
	<div class="NEWleftPanelMenu">
		
		<div class="NEWmenuButton">
			<img class="NEWmaximizeButton NEWhiddenButton" src="<c:out value='${pageContext.request.contextPath}'/>/img/maximize.gif"/>
			<img class="NEWminimizeButton" src="<c:out value='${pageContext.request.contextPath}'/>/img/minimize.gif"/>
		</div> <!-- NEWmenuButton -->
		
		<div class="NEWleftPanelMenuUp">	
			<ww:if test="@pl.compan.docusafe.web.common.Sitemap@dynamicMenu(1) == 'registries'">        
				<ww:if test="menuInfo != null">
					<ww:iterator value="menuInfo.registries">
						<ul>
							<ww:property value="shortName"/>
							<ds:sitemap menu="2" node="node">
								<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
									<li>
										<a <ww:if test="(isInPath(#node)) && (menuInfo.selectedRegistryId == id)">class="NEWactualLink"</ww:if> href="<ww:url value="href"/>?registryId=<ww:property value="id"/>"><ww:property value="label"/></a>
									</li>
								</ww:if>
							</ds:sitemap>
						</ul>
					</ww:iterator>
				</ww:if>
			</ww:if>
			<ww:else>
				<ul>
					<ds:sitemap menu="2" node="node">
						<li>
							<a <ww:if test="isInPath(#node)">class="NEWactualLink"</ww:if> href="<ww:url value="href"/>"><ww:property value="label"/></a>
						</li>
					</ds:sitemap>
				</ul>
			</ww:else>
		</div> <!-- NEWleftPanelMenuUp -->
		
		<div class="NEWleftPanelMenuDown">
			<br/>
			<hr class="NEWleftPanelLine">
			<ul>
				<ds:available test="left_menu_user_data">
					<li>
						<a href="<ww:url value="'/help/user-info.action'"/>"><ds:lang text="MojaKarta"/></a>
					</li>
					</ds:available>
				<edm-sec:active-directory-user negation="false">				
					<li>
						<a href="<ww:url value="'/self-change-password.do'"/>"><ds:lang text="changePassword"/></a>
					</li>
				</edm-sec:active-directory-user>
				<li>
					<a href="<ww:url value="'/settings/index.action'"/>"><ds:lang text="settings"/></a>
				</li>
				<!-- U�ytkownik identyfikuj�cy si� certyfikatem nie mo�e si� wylogowa� -->
				<ds:subject hasCertificate="false">
					<li>
						<html:link styleId="test-logout" forward="logout"> <ds:lang text="logout"/></html:link>
					</li>
				</ds:subject>
			</ul>
		</div>	<!-- NEWleftPanelMenuDown -->
		
	</div> <!-- NEWleftPanelMenu -->
</ds:additions>

<ds:additions test="!tcLayout">
	<!--  Nowy layout 2.0 -->
	<ds:available test="layout2">
	<table height="100%" width="100%" cellspacing="0" cellpadding="0" class="menuLeft" <ds:available test="!layoutHeader">style="position: relative; margin-top: -30px;"</ds:available>>
	<ds:available test="!layoutHeader">
	<tr height="64"><td background="<ww:url value="'/img/logo_c.gif'" />" style="background-repeat: no-repeat; background-position: 5px 0;" valign="top">
	<a href="<c:out value='${pageContext.request.contextPath}'/>" style="display: block; height: 64px; width: 100%;">
		&nbsp;
	</a>		
	</td></tr>
	</ds:available>
	<tr>
	<td valign="top">
				<ww:if test="@pl.compan.docusafe.web.common.Sitemap@dynamicMenu(1) == 'registries'">        
					<ww:if test="menuInfo != null">
						<ww:iterator value="menuInfo.registries" status="stat">
							<ul class="left">
								<ww:property value="shortName"/>
								<ds:sitemap menu="2" node="node">
									<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
										<li>
											<a <ww:if test="(isInPath(#node)) && (menuInfo.selectedRegistryId == id)">class="left_selected"</ww:if> href="<ww:url value="href"/>?registryId=<ww:property value="id"/>"><ww:property value="label"/></a>
										</li>
									</ww:if>
								</ds:sitemap>
							</ul>
						</ww:iterator>
					</ww:if>
				</ww:if>
				<ww:else>
					<ul class="left" id="ul_left_ie">
						<ds:sitemap menu="2" node="node">
                                                    <li <ww:if test="htmlId != null"> id="<ww:property value="htmlId"/>"</ww:if>>
								<a <ww:if test="isInPath(#node)">class="left_selected"</ww:if> href="<ww:url value="href"/>"><ww:property value="label"/></a>
							</li>
						</ds:sitemap>
					</ul>
				</ww:else>
	</td></tr>
	<tr><td valign="bottom">
				<ul class="left">
					<ds:available test="left_menu_user_data">
					<li>
						<a href="<ww:url value="'/help/user-info.action'"/>"><ds:lang text="MojaKarta"/></a>
					</li>
					</ds:available>
					<ds:available test="notification.on">
						<li>
							<a id="notify" <ww:if test="@pl.compan.docusafe.core.news.Notification@notificationsCount() != 0">class="blink"</ww:if> href="<ww:url value="'/news/notification.action'"/>"><ww:property value="'Powiadomienia (' + @pl.compan.docusafe.core.news.Notification@notificationsCount() + ')'"/></a>
						</li>
						
					</ds:available>
					<ds:available test="!block.change.user.password">
					<li>
						<a href="<ww:url value="'/self-change-password.do'"/>"><ds:lang text="changePassword"/></a>
					</li>
					</ds:available>
					<li>
						<a href="<ww:url value="'/settings/index.action'"/>"><ds:lang text="settings"/></a>
					</li>
					<!-- U�ytkownik identyfikuj�cy si� certyfikatem nie mo�e si� wylogowa� -->
					<ds:subject hasCertificate="false">
						<li>
						    <script>
                              $j(function(){
                                    $j( "#logout-confirm" ).dialog({
                                      autoOpen: false,
                                      resizable: false,
                                      height:140,
                                      modal: true,
                                      buttons: {
                                        "Nie": function() {
                                            $j( this ).dialog( "close" );
                                         },
                                        "Tak": function() {
                                          $j( this ).dialog( "close" );
                                          window.location='logout.do';
                                        }
                                      }
                                    });

                                $j("#test-logout").click(function(){
                                    //$j("#logout-confirm").dialog("open");
                                });
                                });

						    </script>
							<ds:available test="logout.confirm">
							    <html:link styleId="test-logout" forward="logout" styleClass="left_logout" onclick="return confirm('Na pewno chcesz si� wylogowa�?');"> <ds:lang text="logout"/></html:link>
							</ds:available>
							<ds:available test="!logout.confirm">
                            	<html:link styleId="test-logout" forward="logout" styleClass="left_logout"> <ds:lang text="logout"/></html:link>
                            </ds:available>
						</li>
					</ds:subject>
				</ul>
	</td></tr>
	</table>
				<ds:available test="!shortLeftMenu">
				<script>
				var ie = document.all && !window.opera;
				var ie7 = (navigator.appVersion.indexOf('MSIE 7.')== -1) ? false : true;
				var $j = jQuery.noConflict();
				
				/* zamiana dlugich nazw w lewym menu na 'nazwa...' i dodanie atrybutu title */
				$j(function() {
					var leftItems = $j('ul.left li a');
					var len = leftItems.length;
					
					for(i = 0; i < len; i ++) {
						var curItem = leftItems.get(i);
						if((curItem.offsetHeight > 25 && !ie7) || (curItem.innerHTML.length > 21 && ie7)) {	
							curItem.title = curItem.innerHTML;
							curItem.innerHTML = curItem.innerHTML.slice(0, 14);
							curItem.innerHTML += '...';
						}
					}

					if(!ie && !ie7)
						$j('.left_selected').css('overflow', 'hidden'); 
				});
				
				// poprawa lewego menu dla ie
				if(ie6 || ie7) {
								
					$j("ul.left li a").addClass('left_ie_fix');
					
					$j("ul.left li a.left_selected").addClass('left_ie_fixSelected');
				
					$j("ul.left li a.left_selected").hover(function() {
						$j(this).addClass('left_ie_fixSelectedHover');
					}, function() {
						$j(this).removeClass('left_ie_fixSelectedHover');
					});
					
					$j("ul.left li a").hover(function() {
						$j(this).addClass('left_ie_fixHover');
					}, function() {
						$j(this).removeClass('left_ie_fixHover')
					});
					var ul_left_ie = document.getElementById('ul_left_ie');
					ul_left_ie.style.paddingTop = '50px'; 
					
				} 
				</script>
				</ds:available>
	</ds:available>
	<!--  Koniec nowego layout2 -->
	<ds:available test="!layout2">
	<table height="100%">
		<tr>
			<td style="vertical-align:top;" valign="top">
				<ww:if test="@pl.compan.docusafe.web.common.Sitemap@dynamicMenu(1) == 'registries'">        
					<ww:if test="menuInfo != null">
						<ww:iterator value="menuInfo.registries" status="stat">
							<ul>
								<ww:property value="shortName"/>
								<ds:sitemap menu="2" node="node">
									<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
										<li id="leftMenuItem_<ww:property value="#stat.index"/>">
											<a <ww:if test="(isInPath(#node)) && (menuInfo.selectedRegistryId == id)">class="active-link"</ww:if> href="<ww:url value="href"/>?registryId=<ww:property value="id"/>"><ww:property value="label"/></a>
										</li>
									</ww:if>
								</ds:sitemap>
							</ul>
						</ww:iterator>
					</ww:if>
				</ww:if>
				<ww:else>
					<ul>
						<ds:sitemap menu="2" node="node">
							<li id="leftMenuItem_<ww:property value="#stat.index"/>">
								<a <ww:if test="isInPath(#node)">class="active-link"</ww:if> href="<ww:url value="href"/>"><ww:property value="label"/></a>
							</li>
						</ds:sitemap>
					</ul>
				</ww:else>
			</td>
		</tr>
		<tr>
			<td style="vertical-align: bottom;" valign="bottom">
				<img src="<c:out value='${pageContext.request.contextPath}'/>/img/bbit.gif" width="80" height="1" />
				<br>
				<ul>
					<ds:available test="left_menu_user_data">
					<li>
						<a href="<ww:url value="'/help/user-info.action'"/>"><ds:lang text="MojaKarta"/></a>
					</li>
					</ds:available>
					<li>
						<a href="<ww:url value="'/self-change-password.do'"/>"><ds:lang text="changePassword"/></a>
					</li>
					<li>
						<a href="<ww:url value="'/settings/index.action'"/>"><ds:lang text="settings"/></a>
					</li>
					<!-- U�ytkownik identyfikuj�cy si� certyfikatem nie mo�e si� wylogowa� -->
					<ds:subject hasCertificate="false">
						<li>
							<html:link styleId="test-logout" forward="logout"> <ds:lang text="logout"/></html:link>
						</li>
					</ds:subject>
				</ul>
			</td>
		</tr>
	</table>
	</ds:available>
</ds:additions>
<!--N koniec left-menu.jsp N-->