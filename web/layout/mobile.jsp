<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.core.PasswordPolicy"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="pl.compan.docusafe.core.GlobalPreferences"%>
<%@ page import="pl.compan.docusafe.core.DSApi"%>
<%@ page import="java.util.Locale" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ds:available test="layout2">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
</ds:available>
<!-- Nie dodawac komentarzy html powyzej tej linii --> 

<!-- Szablon dla WebWork -->
<!-- Wersja: <%= Docusafe.getVersion() %>.<%= Docusafe.getDistTimestamp() %> -->
<html>
	<head>
		<title>
			<% if(Docusafe.getAdditionProperty("docusafe.TytulStrony") == null) {%>
				<ds:lang text="TytulStrony"/>
			<% } else { %>
				<%= Docusafe.getAdditionProperty("docusafe.TytulStrony") %>
			<% } %>
		</title>
		
		<%try{DSApi.openAdmin();
			if (!GlobalPreferences.DOCUSAFE_PRODUCTION_INSTANCE.equals(GlobalPreferences.getDocusafeInstanceType()))
				request.setAttribute("docusafeType", GlobalPreferences.getDocusafeInstanceTypeName());
				DSApi.close();
			}
		catch(Exception e){}%>
		
		<c:if test="${docusafeType == null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		<%--	W tym IFie mozna dodac favicon dla wersji testowej systemu - podmienic tylko nazwe	--%>
		<c:if test="${docusafeType != null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-2"/>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/web-event.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/functions.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dockind.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/html.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/json.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.cookie.min.js"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/boxover.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/common.js?v=<%= Docusafe.getDistTimestamp() %>"></script> 
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/MClass.js?v=<%= Docusafe.getDistTimestamp() %>"></script> 
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/MEvent.js?v=<%= Docusafe.getDistTimestamp() %>"></script> 
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/MImageAreaSelection.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/custom-form-elements.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-1.8.custom.min.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-timepicker-addon.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/wtooltip.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.livequery.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/pngFix/jquery.pngFix.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dwrdockind/chosen.jquery.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<ds:available test="jQueryCalendar">
		<%if(Docusafe.getCurrentLanguage().equals("pl")){%>
			<script type="text/javascript" src="<ww:url value="'/calendar096/lang/ui.datepicker-pl.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<%}else{%>
			<%-- Domyslnie jest angielski --%>
		<%}%>
		</ds:available>

		<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/chosen.css'"/>?v=<%= Docusafe.getDistTimestamp() %>" />
		<link rel="stylesheet" type="text/css" href="<ww:url value="'/main.css'"/>?v=<%= Docusafe.getDistTimestamp() %>" />
		<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/jquery-ui-1.8.custom.css'"/>?v=<%= Docusafe.getDistTimestamp() %>"  />
		<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/jquery-ui-timepicker-addon.css'"/>?v=<%= Docusafe.getDistTimestamp() %>"  />
		
		<%--	Kontrolka kalendarza	--%>
		<ds:available test="!jQueryCalendar">
		<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/calendar096/calendar-win2k-cold-1.css'"/>?v=<%= Docusafe.getDistTimestamp() %>"  />
			
		<script type="text/javascript" src="<ww:url value="'/calendar096/calendar.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		</ds:available>
		
		<%--	language for the calendar	--%>
		<ds:available test="!jQueryCalendar">
		<%if(!Docusafe.getCurrentLanguage().equals("pl")){%>
			<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-en.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<%}else{%>
			<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-pl.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<%}%>
	
		<%--	the following script defines the Calendar.setup helper function, which makes adding a calendar a matter of 1 or 2 lines of code.	--%>
		<script type="text/javascript" src="<ww:url value="'/calendar096/calendar-setup.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		</ds:available>		
		
		<ww:if test="refresh">
			<meta http-equiv="Refresh" content="<ww:property value="refreshTime"/>; url=<ww:url/>"/>
		</ww:if>
		<!--[if IE]>
		<style type="text/css">
			.NEWmain {height: 100%;}
		</style>
		<![endif]-->
		<script type="text/javascript">
			var $j = jQuery.noConflict();
			$j(document).ready(function() {
				formatNumericFields();
			});

			__dontFixItemsWidth = false;

			<ds:available test="jQueryCalendar">
			/* Obejscie starego kalendarza na jquery */
			Calendar = function() {
				
			}

			Calendar.setup = function(params) {
				/*$j('#' + params['button']).click(function() {
					alert('klik');
				});*/
				$j(function() {
				//jQuery.fx.off = true;

				$j('#' + params['inputField']).datepicker({showOn: 'button', buttonImage: '<ww:url value="'/calendar096/img.gif'"/>', 
					buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
					changeMonth: true, changeYear: true, showButtonPanel: true});
				});
				$j('#' + params['button']).hide();
			}
			</ds:available>

			<ds:dockinds test="prosika">
			if(window.document.attachEvent){
				window.document.attachEvent('onmousewheel',stopMouseWheel);
			}
			function stopMouseWheel(){
				if(window.event.srcElement.type)
					{if(window.event.srcElement.type == 'select-one'){return false;}}
			}
			</ds:dockinds>
			
			function dsConfirm(text){
					if(window.showModalDialog){
						return window.showModalDialog('<ww:url value="'/conf.html'"/>?text='+text, '', 'dialogHeight: 70px; dialogWidth: 100px;');		
					} else {
						return(confirm(text));
					}
					return false;	
				}
				
<ds:additions test="tcLayout">
			var minimized = 0;		
			var NEWrightContent;
			
			function szerokosc()
			{
				if ($j.browser.mozilla)
				{
					if ( $j(window).width() < $j(document).width() )
					{
						$j(".NEWmain").width($j(document).width());
					}
					
					else if ( ($j(window).width() > $j(document).width()) || ($j(window).width() > $j(".NEWmain").width()) )
					{
						$j(".NEWmain").css("width","auto");
					}
				}
				
				if ($j.browser.msie)
				{
					var szer_od_okna = ($j(window).width() - ((minimized == 1) ? 1 : 191) );
					
					
					NEWrightContent.width( szer_od_okna );
					if (NEWrightContent.width() > szer_od_okna)
					{
						NEWrightContent.width( NEWrightContent.width() );
						$j(".NEWmain").width( (NEWrightContent.width() + ((minimized == 1) ? 11 : 201)) );
					}
					else
					{
						$j(".NEWmain").css("width","auto");
					}
				}
			}
			
			function wysokosc()
			{
				NEWrightContent.css("height", "auto");
				var wys_okna = ($j(window).height() - 161);
				var wys_zawartosci = NEWrightContent.height();  
				var wys_menu = $j(".NEWleftPanelMenu").height();
				
				var max = (wys_okna > wys_zawartosci) ? ( (wys_okna > wys_menu) ? wys_okna : wys_menu ) : ( (wys_zawartosci>wys_menu) ? wys_zawartosci : wys_menu );
			
				if ($j.browser.msie) max+=10; 
			
				NEWrightContent.height(max);
				$j(".NEWleftPanel").height(max);
				$j(".NEWmenuButton").css("margin-top", (wys_menu/2-25));
			}
			
			function minimize()
			{
				$j(".NEWleftPanelMaximized").animate( {left: "-190px"}, 1000, function() { $j(this).removeClass('NEWleftPanelMaximized').addClass('NEWleftPanelMinimized'); } );
				if ($j.browser.msie) $j(".NEWrightContentMaximized").animate( {marginLeft: "0px", width:"+=201px"}, 1000, function() { $j(this).removeClass('NEWrightContentMaximized').addClass('NEWrightContentMinimized'); } );
				if ($j.browser.mozilla) $j(".NEWrightContentMaximized").animate( {marginLeft: "0px"}, 1000, function() { $j(this).removeClass('NEWrightContentMaximized').addClass('NEWrightContentMinimized'); } );
				$j(".NEWmenuButton").animate( {left: "0px"}, 1000, function() { $j(".NEWminimizeButton").hide(); $j(".NEWmaximizeButton").show(); wysokosc(); scroll();} );
				$j.cookie('minimized', '1', { path: '<c:out value='${pageContext.request.contextPath}'/>' } );
				minimized = '1';
			}
			
			function maximize()
			{
				$j(".NEWleftPanelMinimized").animate( {left: "0px"}, 1000, function() { $j(this).removeClass('NEWleftPanelMinimized').addClass('NEWleftPanelMaximized'); } );
				if ($j.browser.msie) $j(".NEWrightContentMinimized").animate( {marginLeft: "190px", width:"-=179px"}, 1000, function() { $j(this).removeClass('NEWrightContentMinimized').addClass('NEWrightContentMaximized'); } );
				if ($j.browser.mozilla) $j(".NEWrightContentMinimized").animate( {marginLeft: "190px"}, 1000, function() { $j(this).removeClass('NEWrightContentMinimized').addClass('NEWrightContentMaximized'); } );
				$j(".NEWmenuButton").animate( {left: "190px"}, 1000, function() { $j(".NEWmaximizeButton").hide(); $j(".NEWminimizeButton").show(); wysokosc(); scroll();} );
				$j.cookie('minimized', '0', { path: '<c:out value='${pageContext.request.contextPath}'/>' } );
				minimized = '0';
			}
			
			function scroll()
			{
				if ($j(window).scrollLeft() != 0 )
				{
					if (!($j.browser.msie))
					{
						if ( $j(".NEWleftPanelMenuFixed").length )
							minimized == '1' ?
							$j(".NEWleftPanel").css("left", (-190 - $j(window).scrollLeft())) :
							$j(".NEWleftPanel").css("left", (0 - $j(window).scrollLeft()));
					}
				}
				else
				{
					if (!($j.browser.msie))
						minimized == '1' ?
						$j(".NEWleftPanel").css("left", "-190px") :
						$j(".NEWleftPanel").css("left", "0");
				}
				
				
				var ile = $j(window).scrollTop()
				
				if (ile <= 81)
				{
					$j(".NEWleftPanelMenu").removeClass('NEWleftPanelMenuFixed').removeClass('NEWleftPanelMenuFixedIE').removeClass('NEWleftPanelMenuBottom');
				}
				else if (ile <= ( NEWrightContent.height() - $j(".NEWleftPanelMenu").height() + 81) )
				{
					($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) ? $j(".NEWleftPanelMenu").addClass('NEWleftPanelMenuFixedIE') : $j(".NEWleftPanelMenu").addClass('NEWleftPanelMenuFixed');
					$j(".NEWleftPanelMenu").removeClass('NEWleftPanelMenuBottom');
				}
				else
				{
					$j(".NEWleftPanelMenu").addClass('NEWleftPanelMenuBottom');
					($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) ? $j(".NEWleftPanelMenu").removeClass('NEWleftPanelMenuFixedIE').attr("style", "") : $j(".NEWleftPanelMenu").removeClass('NEWleftPanelMenuFixed');
				}
			}
			
			function rozmiar()
			{
				szerokosc();
				wysokosc();
			}
	
			$j(document).ready(function ()
			{
				NEWrightContent = $j(".NEWrightContent");
	
				rozmiar();
				scroll();
				$j(".NEWminimizeButton").click(function() { minimize(); } );
				$j(".NEWmaximizeButton").click(function() { maximize(); } );
				$j(".NEWminimizeButton").hover(
					function()
					{
						$j(this).attr("src", "<c:out value='${pageContext.request.contextPath}'/>/img/minimize_hover.gif")
					},
					function()
					{
						$j(this).attr("src", "<c:out value='${pageContext.request.contextPath}'/>/img/minimize.gif")
					} );
				$j(".NEWmaximizeButton").hover(
					function()
					{
						$j(this).attr("src", "<c:out value='${pageContext.request.contextPath}'/>/img/maximize_hover.gif")
					},
					function()
					{
						$j(this).attr("src", "<c:out value='${pageContext.request.contextPath}'/>/img/maximize.gif")
					} );
				
				$j(window).scroll(
					function()
					{
						scroll();
					} );
				
			});
</ds:additions>

// skrypty dla nowego layoutu 2.0
<ds:available test="layout2">
/*var ie = document.all && !window.opera ? 1 : 0;
var ie7 = (navigator.appVersion.indexOf('MSIE 7.')== -1) ? false : true;
var ie6 = (navigator.appName == "Microsoft Internet Explorer" && parseInt(navigator.appVersion) == 4 && navigator.appVersion.indexOf("MSIE 6.0") != -1);
*/
var ie = $j.browser.msie;
var ie6 = $j.browser.msie && (parseInt($j.browser.version) == 6) ? true : false;
var ie7 = $j.browser.msie && (parseInt($j.browser.version) == 7) ? true : false;
var ie8 = $j.browser.msie && (parseInt($j.browser.version) == 8) ? true : false;

function fixResize() {
	var bar_el = document.getElementById('bar');
	var logout_el = document.getElementById('logout_top');
	var tab_el = document.getElementById('bigTable');

	//traceStart('fixResize');

	//trace('tab_el.offsetWidth: ' + tab_el.offsetWidth);

	if(logout_el) {
		try {
		    bar_el.style.width = tab_el.offsetWidth + 'px';
		    logout_el.style.left = tab_el.offsetWidth - 100 + 'px';
			logout_el.style.top = $j('#logo').height() + 31 + 'px';
		} catch(e) {}
		<ds:available test="!layoutHeader">
			logout_el.style.top = '31px';
		</ds:available>
		
		logout_el.style.visibility = 'visible';
	}


	//traceEnd();
}

/* Tu obsluga menu, zeby wczytywalo sie wczesniej - bez "skakania" w ie */
$j('.upperMenuLinkNew').ready(function() {
	/* Link do forum */
	var forumLink = '<%= Docusafe.getAdditionProperty("phpbb.forumLink") %>';
	
	if(forumLink != null && forumLink != 'null') {
		$j('a.upperMenuLinkNew:last').parent('td').clone()
		.children('a').html(function(index, oldhtml) {
			$j(this).html(oldhtml.replace(/>(\d|\D)*/i, '>Forum'));
		}).attr('href', forumLink).attr('target', '_blank')
		.parent('td').appendTo('tr.all_border');
	}
	
	/* naprawa gornego menu dla ie6 - trzeba dodac szerokosc obrazka tab_left;gif */
	if(ie) {
		$j('.upperMenuLinkNew').each(function() {
			this.style.width = this.offsetWidth + 20 + 'px';
		});
	}
	$j('.upperMenuLinkNew').css('visibility', 'visible');
	
	$j('a.upperMenuLinkNew').hover(function() {
		var selected = false;
		if($j(this).attr('id') == 'tab_selected') {
			selected = true;
		}
		if(!selected) {
			var new_src = $j(this).children('.tab_img').attr('src');
			new_src = new_src.replace('.gif', '_pushed.gif');

			if(ie6)
				new_src = new_src.replace('_pushed.gif', '_pushedIE6.gif');
			
			$j(this).addClass('upperMenuLinkNewHover');			
			$j(this).children('.tab_img').attr('src', new_src);	
		}
	}, function() {
		var selected = false;
		if($j(this).attr('id') == 'tab_selected') {
			selected = true;
		}
		if(!selected) {
			var new_src = $j(this).children('.tab_img').attr('src');
			new_src = new_src.replace('_pushed.gif', '.gif');

			if(ie6)
				new_src = new_src.replace('_pushedIE6.gif', '.gif');
			
			$j(this).children('.tab_img').attr('src', new_src);
			$j(this).removeClass('upperMenuLinkNewHover');
		}
	});
});

var __alignInnerTables = false;

$j(document).ready(function() {

	/* obej¶cie b³êdu niepod¶wietlania wybranej zak³adki w xmlLink.java */
	try {
		if(__manualSelectedTab) {
			$j('#middleMenuContainer a').each(function() {
			    if(this.href == document.location) {
			    	$j(this).addClass('highlightedText');
			    }
			});
		}
	} catch(e) {}

	/* Zamiana na style layout2 */
	$j('#middleMenuContainer img:not(.tab2_img)').css('display', 'none');
	$j('#middleMenuContainer a.highlightedText').attr('id', 'tab2_selected').addClass('middleMenuLink').addClass('middleMenuLinkHover').addClass('middleMenuLinkSelected').removeClass('highlightedText').prepend('<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_pushed.gif" class="tab2_img" S/>');
	$j('#middleMenuContainer a:not(.highlightedText, .middleMenuLink)').addClass('middleMenuLink').prepend('<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left.gif" class="tab2_img" />');

	if(ie6) {
		try {
			var imgSrc = $j('#tab2_selected img.tab2_img').attr('src');
			imgSrc = imgSrc.replace('_pushed.gif', '_pushedIE6.gif');
			$j('#tab2_selected img.tab2_img').attr('src', imgSrc);
		} catch(e) {}

		try {
			$j('img.tab2_img').each(function() {
					var newSrc = $j(this).attr('src');
					newSrc = newSrc.replace('_pushed.gif', '_pushedIE6.gif');
					newSrc = newSrc.replace('_corner.gif', '_cornerIE6.gif');
					$j(this).attr('src', newSrc);
				});			
		} catch(e) {}
	}
	

	$j('a.middleMenuLink').hover(function() {
		var selected = false;
		if($j(this).attr('id') == 'tab2_selected') {
			selected = true;
		}
		if(!selected) {
			var new_src = $j(this).children('.tab2_img').attr('src');
			new_src = new_src.replace('.gif', '_pushed.gif');
			
			if(ie6)
				new_src = new_src.replace('_pushed.gif', '_pushedIE6.gif');

			$j(this).addClass('middleMenuLinkHover');			
			$j(this).children('.tab2_img').attr('src', new_src);	
		}
	}, function() {
		var selected = false;
		if($j(this).attr('id') == 'tab2_selected') {
			selected = true;
		}
		if(!selected) {
			var new_src = $j(this).children('.tab2_img').attr('src');
			new_src = new_src.replace('_pushed.gif', '.gif');

			if(ie6)
				new_src = new_src.replace('_pushedIE6.gif', '.gif');
			
			$j(this).children('.tab2_img').attr('src', new_src);
			$j(this).removeClass('middleMenuLinkHover');
		}
	});	

	/* przyciski gmenu */
	$j('.cancel_btn').removeClass('cancel_btn').addClass('btn');
	
	//prepareButtons();

	$j('.gmenuButton').hover(function() {
		$j(this).addClass('gmenuButtonOver');
	}, function() {
		$j(this).removeClass('gmenuButtonOver');
	});

	//$j('input:submit:not(:disabled):first').addClass('defaultBtn');

	/* pola tekstowe gmenu */
	$j('input.txt_opt').removeClass('txt_opt').addClass('txt');

	/*
	if(!ie7) {
		$j('input.txt, textarea.txt, textarea.txt_opt, select.sel, select.multi_sel').focus(function() {
			$j(this).addClass('txtHover');
	        $j(this).parent().parent('tr:not(.formTableDisabled)').addClass('oddRows');
		}).blur(function() {
			$j(this).removeClass('txtHover');
	        $j(this).parent().parent('tr:not(.formTableDisabled)').removeClass('oddRows');
		});
	} else if(ie7) {
		$j('input.txt').bind('focus.ie7trigger', function() {
			$j(this).addClass('txtHover');
		}).bind('blur.ie7trigger', function() {
			$j(this).removeClass('txtHover');
		});
	}
	*/

	/* magiczne checkboxy */
	<ds:available test="prettyCheckboxes">
	$j(":checkbox:not(.styled, :disabled)").addClass('styled');
	Custom.init();
	
	$j(':checkbox:disabled:not(:checked):not(.styled-disabled)').after('<span class="checkboxDisabled" style="cursor:default">&nbsp;</span>').hide();
	$j(':checkbox:disabled:checked:not(.styled-disabled)').after('<span class="checkboxDisabled checked" style="cursor:default">&nbsp;</span>').hide();
	</ds:available>
	<ds:available test="!prettyCheckboxes">
	$j(":checkbox").addClass('dontFixWidth');
	</ds:available>

	/* magiczne checkboxy - bez obslugi onClick */
	/*$j(':checkbox:not(.styled, .styledDynamic, .labelHide)').addClass('styledGlobal');
	CustomGlobal.init();*/

	/* poprawa wylogowania i footera */
	var screenWidth = document.body.clientWidth;
	var expandFooter = false;

	fixResize();

	if(ie && !ie7)
		$j('#pIEFix').css('padding-top', '1px');


	/* Zamiana gwiazdek na fajne ikonki */
	$j('.star').html("<img title='<ds:lang text="PoleObowiazkowe"/>' src=<c:out value='${pageContext.request.contextPath}'/>/img/warn.gif />");

	/* tooltips */
	$j('a, :not(#mainTable a) #mainTable tr td a img, .star img, img.zoomImg, input.zoomImg, img.zoomImgTree, img.permIco').wTooltip({ 
		style: false, offsetY: 20, fadeIn: 400, delay: 400, className: "dsTooltip"});

	<ds:available test="groupButtons">
	/* grupowanie przycisków - te któr± s± tworzone "dynamicznie" w jsp (tj. w zale¿no¶ci od availablesów, ifów, uprawnieñ itd.) */
	$j('.btnContainer').each(function() {
		if($j(this).children('input.btn:visible').length > 1) {
			$j(this).children('input.btn:first').addClass('btnLeft');
			$j(this).children('input.btn:last').addClass('btnRight');
			$j(this).children('input.btn:visible:not(:first, :last, .btnLeft, .btnRight)').addClass('btnMiddle');
		}
	});
	</ds:available>

	$j(document).pngFix();

	<ds:available test="alignInnerTables">
		__alignInnerTables = true;
	</ds:available>
	
	fixItemsWidth();
});
</ds:available>

$j(document).ready(function() {
	if(window.top != window.self) {
		showAllMenus(false);
	}
});
</script>

		<%-- Od tego momentu $j dzia³a --%>
		<ds:dockinds test="invoice_ic">
			<script type="text/javascript" src="<ww:url value="'/intercars.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
		</ds:dockinds>
</head>

<ds:additions test="!tcLayout">
	<ds:available test="layout2">


<!-- Nowy layout 2.0 -->
	<body leftMargin="0" topMargin="0" marginwidth="0" marginheight="0" onresize="fixResize();">
		<!-- footer css trick -->
	 	<div class="wrapper">
		<table cellSpacing=0 cellPadding=0 width="100%" border=0 id="bigTable">
		
			<!-- Pocz±tek nag³ówka strony -->
			<!-- Koniec nag³ówka strony -->

			<!-- Pocz±tek górnego menu -->
			<!-- Koniec górnego menu -->

			<!-- Pocz±tek g³ównego okna -->
			<tr valign="top">
				<td colspan="2">
					<table border="0" width="100%" height="100%" cellspacing="0" >
						<tr class="mai">
							<!-- Pocz±tek menu (left) -->
							<!-- Koniec menu (left) -->
							
							<!-- <td class="mai2"> -->
							<td class="">
								<!-- Mo¿e byc przydatne przy otwieraniu okien z javascriptu -->
								<input type="hidden" id="request_pageContext" value="<c:out value='${pageContext.request.contextPath}'/>"/>

								<table cellspacing="0" cellpadding="0" width="100%" border="0"><tbody>
								<tr>
									<td>	<!-- zawartosc main //-->
								<% if (request.getAttribute("include.location") != null) { %>
									<jsp:include page="<%= (String) request.getAttribute(\"include.location\") %>"/>
								<% } else {%>
									<tiles:insert name="body"/> <%--	t± liniê usun±æ je¶li wracamy do dwóch layoutów - Strutsa1 i WebWorka, oraz zmieniæ w struts-config-source.xml i tiles-defs-default.xml z main.jsp na red.jsp	--%>
								<% } %>
								</td></tr>
								</tbody></table>
							</td>
							<!-- Koniec okna z tre¶ci± (right) -->
						</tr>
						<!-- Nowy wiersz, zeby odsunac glowne okno od stopki //-->
					</table>
				</td>
			</tr>
			<!-- Koniec g³ównego okna -->
			
			<% Integer daysLeft = (Integer) pageContext.getAttribute(PasswordPolicy.DAYS_LEFT_KEY, PageContext.APPLICATION_SCOPE); %>
			<% if (daysLeft != null && daysLeft.intValue() >= 0) { %>
				<tr height="18" class="zoomImg" class="zoomImg">
					<td colspan="2" class="mai">
						<p> Do wyga¶niêcia has³a pozosta³o dni: <%= daysLeft %>.
						<a href="<ww:url value="'/self-change-password.do'"/>">Kliknij tutaj aby zmieniæ swoje has³o</a></p>
					</td>
				</tr>
			<% } %>
</table>
<!-- css footer trick KONIEC-->
<div class="push">&nbsp;</div> <!-- CLOSED -->
</div> <!-- wrapper koniec -->
<div class="footer">	<!-- CLOSED -->
			<!-- nowa stopka //-->

			</div>
<div id="dialog-confirm"></div>
<%-- Podpis rêczny dla Play --%>
</body>
<!--  Koniec nowego layoutu -->
</ds:available>

<script>
 $j( document ).ready(function() {
            $j('.combox-chosen').chosen({no_results_text: "Nie znaleziono"});
        });
</script>

</ds:additions>

</html>

<!--N koniec main.jsp N-->
