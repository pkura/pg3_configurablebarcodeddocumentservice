<%@ page contentType="text/html; charset=iso-8859-2" %>

<%@ page import="pl.compan.docusafe.boot.Docusafe"%>

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" href="<ww:url value="'/main.css'"/>?v=<%= Docusafe.getDistTimestamp() %>" />

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/functions.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-1.8.custom.min.js?v=<%= Docusafe.getDistTimestamp() %>"></script>

<script type="text/javascript">
	var $j = jQuery.noConflict();
	var ie = $j.browser.msie;
	var ie6 = $j.browser.msie && (parseInt($j.browser.version) == 6) ? true : false;
	var ie7 = $j.browser.msie && (parseInt($j.browser.version) == 7) ? true : false;
	var ie8 = $j.browser.msie && (parseInt($j.browser.version) == 8) ? true : false;
</script>

<input type="hidden" id="request_pageContext" value="<c:out value='${pageContext.request.contextPath}'/>"/>

<style>

#middleContainer {
	border: none;
	padding: 0;
}

</style>
<iframe width="99%" height="99%" src="<ww:url value="pageName"/>"></iframe> 
<jsp:include page="<%= (String) request.getAttribute(\"include.location\") %>"/>