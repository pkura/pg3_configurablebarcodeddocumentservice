<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N red.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.core.GlobalConstants"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="pl.compan.docusafe.core.GlobalPreferences"%>
<%@ page import="pl.compan.docusafe.core.DSApi"%>
<%@ page import="java.util.Locale" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<!-- Wersja: <%= Docusafe.getVersion() %>.<%= Docusafe.getDistTimestamp() %> -->
<html>
	<HEAD>
		<title><ds:lang text="TytulStrony"/></title>
		
		<%try{DSApi.openAdmin();
			if (!GlobalPreferences.DOCUSAFE_PRODUCTION_INSTANCE.equals(GlobalPreferences.getDocusafeInstanceType()))
				request.setAttribute("docusafeType", GlobalPreferences.getDocusafeInstanceTypeName());
				DSApi.close();
			}
		catch(Exception e){}%>
		
		<c:if test="${docusafeType == null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		<%--	W tym IFie mozna dodac favicon dla wersji testowej systemu - podmienic tylko nazwe	--%>
		<c:if test="${docusafeType != null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		
		<META HTTP-EQUIV="content-type" CONTENT="text/html; CHARSET=iso-8859-2">
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/web-event.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/functions.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/dockind.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/html.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/json.js?v=<%= Docusafe.getDistTimestamp() %>"></script>

		<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>?v=<%= Docusafe.getDistTimestamp() %>" title="main" />

		<c:set var="haveCalendar">
			<tiles:getAsString name="haveCalendar" ignore="true" />
		</c:set>
		<c:if test="${haveCalendar == 'true'}">
			<%--	Kontrolka kalendarza	--%>
			<link rel="stylesheet" type="text/css" media="all" href="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-win2k-cold-1.css?v=<%= Docusafe.getDistTimestamp() %>" />

			<%--	main calendar program	--%>
			<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar.js?v=<%= Docusafe.getDistTimestamp() %>"></script>

			<%--	language for the calendar	--%>
			<%if(Docusafe.getCurrentLanguage().equals(new Locale("en", "GB"))){%>
				<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-en.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
			<%}else{%>
				<script type="text/javascript" src="<ww:url value="'/calendar096/lang/calendar-pl.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
			<%}%>
			
			<%--	the following script defines the Calendar.setup helper function, which makes adding a calendar a matter of 1 or 2 lines of code.	--%>
			<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/calendar-setup.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
		</c:if>
		
		
		
	</head>

	<body leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" __onload="layout__setTimeout()" __onunload="layout__clearTimeout()">
		<table cellSpacing=0 cellPadding=0 width="100%" border=0  height="100%">

			<!-- Pocz�tek nag��wka strony -->
			<tr id="logo" height="64">
				<td width="50%">
					<a href="<c:out value='${pageContext.request.contextPath}'/>">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>img/logo_c.gif" border="0" height="64"/>
					</a>
				</td>
				<td align="right">
					<c:if test="${docusafeType != null}">
						<font color="red" size="26" >
							Wersja
							<c:out value="${docusafeType}"/>
						</font>
					</c:if>
					<c:if test="${docusafeType == null}">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/dyskietka.gif" border="0" height="64"/>
					</c:if>
				</td>
			</tr>
			<!-- Koniec nag��wka strony -->

			<!-- Pocz�tek g�rnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<%! String upperBarColor = Docusafe.getProperties().getProperty("upperBarColor", "black");%>
				<td colspan="2" <%if(upperBarColor.equals("black")) {%> style="background-color:<%= upperBarColor %>; color:<%= upperBarColor %>;" <%}else{%> class="upperBar"<%}%> >

				<!-- Jesli kolor jest czarny tzn. ze nic nie zmienial - zostawiamy bar i isInPath -->
					<% if (upperBarColor.equals("black")){%>
						<div id="bar" style="float: right; margin-right: 5px;">
							<html:link forward="logout"><ds:lang text="logout"/></html:link>
						</div>
						
						<table id="bar" cellSpacing=0 cellPadding=0 width="780" border=0 >
							<tr>
								<ds:sitemap menu="1" node="node">
									<td>
										<a href="<ww:url value="href"/>" <ww:if test="isInPath(#node)">style="color:red"</ww:if> ><ww:property value="label"/></a>
									</td>
								</ds:sitemap>
								<%--	<td>	<a href="" onclick="javascript:getLoc();">Pomoc online</a>	</td>	--%>
								<%--	<td>	<a href="/docusafe/helponline/help-online.action?param=<%= (String) request.getAttribute(GlobalConstants.ORIGINAL_REQUEST_URI) %>" target="_blank">Pomoc online</a>	</td>	--%>
							</tr>
						</table>
					<%}else{%>
						<div id="bar2" style="float:right; margin-right: 5px;">
							<html:link forward="logout" styleClass="upperMenuLink"><ds:lang text="logout"/></html:link>
						</div>
						
						<table id="bar2" cellSpacing=0 cellPadding=0 width="640" border=0 >
							<tr>
								<ds:sitemap menu="1" node="node">
									<td>
										<a class="upperMenuLink" href="<ww:url value="href"/>" <ww:if test="isInPath(#node)">style="color:red"</ww:if>> <ww:property value="label"/></a>
									</td>
								</ds:sitemap>
							</tr>
						</table>
					<%}%>
				</td>
			</tr>
			<!-- Koniec g�rnego menu -->

			<!-- Pocz�tek g��wnego okna -->
			<tr>
				<td colspan="2">
					<table border="0" width="100%" height="100%">
						<tr class="mai">
							<!-- Pocz�tek menu (left) -->
							<td width="190" class="mai">
								<table class="mai" width="100%" border="0"  height="100%">
									<tr>
										<td>
											<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="15" height="1"/>
										</td>
										<td width="100%" class="mai">
											<tiles:insert name="menu"/>
										</td>
										<td>
											<img src="<c:out value='${pageContext.request.contextPath}'/>/img/bit.gif" height="100%" width="1"/>
										</td>
									</tr>
								</table>
							</td>
							<!-- Koniec menu (left) -->

							<td width="10">
								<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="10" height="1"/>
							</td>
							
							<!-- Pocz�tek okna z tre�ci� (right) -->
							<td class="mai">
								<tiles:insert name="body"/>
							</td>
							<!-- Koniec okna z tre�ci� (right) -->
							
							
						</tr>
					</table>
				</td>
			</tr>
			<!-- Koniec g��wnego okna -->

			<!-- Pocz�tek dolnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<td  colspan="2" bgcolor="black">
					<table id="bar" cellSpacing=0 cellPadding=0 width="100%" border=0 >
						<tr id="bar">
							<td align="left">
								<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="20" height="1"/>
								<edm-html:message key="loggedUser" global="true"/>:
								<edm-sec:logged-user format="%n %z" />
							</td>
							<td></td>
							<td align="right">
								<a href='<%= Configuration.getProperty("copyright.notice.url") %>'>
									<%= Configuration.getProperty("copyright.notice") %></a>
								&nbsp&nbsp&nbsp&nbsp
								<a href='<%= Configuration.getProperty("copyright.notice.url.2") %>'>
									<%= Configuration.getProperty("copyright.notice.2") %></a>
								&nbsp&nbsp&nbsp&nbsp
							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr height="30">
				<td colspan="2" class="lbar" >
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="20" height="1"/>
					<ds:lang text="OstatnieUdaneLogowanie"/>:
					<edm-sec:last-successful-login/>,
					<ds:lang text="ostatnieNieudaneLogowanie"/>:
					<edm-sec:last-unsuccessful-login/>
				</td>
			</tr>
			<!-- Koniec dolnego menu -->
		</table>

		<%--	<iframe style="display: none" id="layout__main__iframe" src='<%= request.getScheme() %>://<%= request.getServerName() %>:<%= request.getServerPort() %><ww:url value="'/ping.jsp'"/>' width="1" height="1" ></iframe>
		<script>
			var layout__interval = 300000;
			var layout__timeout;

			function layout__reloadIframe()
			{
				if (document.getElementById('layout__main__iframe'))
				{
					document.getElementById('layout__main__iframe').src = '<%= request.getScheme() %>://<%= request.getServerName() %>:<%= request.getServerPort() %><ww:url value="'/ping.jsp'"/>';
					setTimeout("layout__reloadIframe()", layout__interval);
				}
			}
			
			function layout__setTimeout()
			{
				layout__timeout = window.setTimeout("layout__reloadIframe()", layout__interval);
			}
			
			function layout__clearTimeout()
			{
				if (layout__timeout != null) window.clearTimeout(layout__timeout);
			}
		</script>
		
		<script>
			function getLoc()
				{
					document.location ="/docusafe/helponline/help-online.action?param=" ;
				}
		</script>	--%>
	</body>
</html>
