<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N configuration.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<html:html>
<head>
    <title>Konfiguracja</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/init/style.css">
</head>
<body>

    <tiles:insert name="body"/>

<!-- stopka -->

<hr width="100%" size="1" noshade="true"/>

<html:link forward="initial-configuration">Powr�t do pocz�tku konfiguracji</html:link>

<table width="100%" cellspacing="0" cellpadding="0">
<tr>
    <td class="footer">
        Konfiguracja aplikacji DocuSafe
    </td>
    <td class="footer" align="right">
        Copyright (c) 2003 COM-PAN s.c.
    </td>
</tr>
</table>

</body>
</html:html>
