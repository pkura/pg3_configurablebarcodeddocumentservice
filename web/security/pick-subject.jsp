<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N pick-subject.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="pl.compan.docusafe.web.archive.security.PickSubjectAction"%>

<edm-html:errors/>

<script type="text/javascript">
	function multipleSubjectsSelected()
	{
		var sel = document.getElementById('subjects');
		if (sel.options.selectedIndex < 0)
		{
			alert('<ds:lang text="ProszeWybracNazweZlisty"/>');
			return false;
		}
		
		var backup =false;
		<c:if test="${backup}">
			backup=true;
		</c:if>
		var values = new Array();
	
		for (var i=0; i < sel.options.length; i++)
		{
			if (sel.options[i].selected)
				values[values.length] = new Option(sel.options[i].text, sel.options[i].value);
		}
	
		if (window.opener.__pick_subjects)
			window.opener.__pick_subjects(values,backup);
		else
			window.opener.__pick_subject(values[0].value, values[0].text,backup);

		window.close();
	}

	function resize(){
		var selectSubjects = $j("#subjects").get(0);
		selectSubjects.size = (window.innerHeight / selectSubjects.options[0].clientHeight) - 2;
	};
    $j(document).ready(function() {
         window.onresize = resize;
    }); 

</script>

<html:form action="/security/pick-subject">
	<select name="subjects" 
			id="subjects"
			class="multi_sel"
			size="8"
			multiple="true"
			style="width:100%;">
		<c:forEach items="${subjects}" var="subject">
			<option value="<c:out value='${subject.value}'/>">
				<c:out value="${subject.text}"/>
			</option>
		</c:forEach>
	</select>
	
	<input type="button" class="btn" value="<ds:lang text="Wybierz"/>" onclick="multipleSubjectsSelected();"/>
	
	<script type="text/javascript">
		// je�eli otwieraj�ce okno nie mo�e przyj�� wielu u�ytkownik�w wskazanych jednocze�nie,
		// nie ma sensu umo�liwianie wielokrotnego wyboru
		if (!window.opener.__pick_subjects)
			document.getElementById('subjects').multiple = false;
	</script>
</html:form>

<%--
<edm-html:errors />

<script language="JavaScript">
/*
function subjectSelected()
{
	var sel = document.getElementById('subjects');
	if (sel.options.selectedIndex < 0)
	{
		alert('Prosz� wybra� nazw� z listy');
		return false;
	}
 
	var value = sel.options[sel.options.selectedIndex].value;
	var text = sel.options[sel.options.selectedIndex].text;
	window.opener.__pick_subject(value, text);
	window.close();
}
*/

function multipleSubjectsSelected()
{
	var sel = document.getElementById('subjects');
	if (sel.options.selectedIndex < 0)
	{
		alert('<ds:lang text="ProszeWybracNazweZlisty"/>');
		return false;
	}
	var backup =false;
	<c:if test="${backup}">
		backup=true;
	</c:if>
	var values = new Array();

	for (var i=0; i < sel.options.length; i++)
	{
		if (sel.options[i].selected)
			values[values.length] = new Option(sel.options[i].text, sel.options[i].value);
	}

	if (window.opener.__pick_subjects)
		window.opener.__pick_subjects(values,backup);
	else
		window.opener.__pick_subject(values[0].value, values[0].text,backup);

	//var value = sel.options[sel.options.selectedIndex].value;
	//var text = sel.options[sel.options.selectedIndex].text;
	//window.opener.__pick_subject(value, text);

	window.close();
}
</script>

<html:form action="/security/pick-subject">

<select name="subjects" id="subjects" class="multi_sel" size="8" multiple="true" >
	<c:forEach items="${subjects}" var="subject">
		<option value="<c:out value='${subject.value}'/>"><c:out value="${subject.text}"/></option>
	</c:forEach>
</select>

<input type="button" class="btn" value="<ds:lang text="Wybierz"/>" onclick="multipleSubjectsSelected();"/>

<script language="JavaScript">
	// je�eli otwieraj�ce okno nie mo�e przyj�� wielu u�ytkownik�w
	// wskazanych jednocze�nie, nie ma sensu umo�liwianie wielokrotnego
	// wyboru
	if (!window.opener.__pick_subjects)
		document.getElementById('subjects').multiple = false;
</script>

</html:form>
--%>
<!--N koniec pick-subject.jsp N-->