<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N set-permissions.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<edm-html:errors />

<script language="JavaScript">
var subjects = new Array();

// nazwie u�ytkownika odpowiada tablica nazw jego uprawnie�
<c:forEach items="${subjects}" var="subject">
subjects['<c:out value="${subject.value}"/>'] = new Array();
</c:forEach>

// je�eli element tablicy odpowiadaj�cy uprawnieniu ma warto��
// true, u�ytkownik posiada to uprawnienie, w przeciwnym razie
// go nie posiada
<c:forEach items="${subjectPermissions}" var="subjectPermission">
    <c:forEach items="${subjectPermission.value}" var="permissionName">
        subjects['<c:out value="${subjectPermission.key}"/>']['<c:out value="${permissionName}"/>'] = true;
    </c:forEach>
</c:forEach>

/**
    Funkcja wywo�ywana po ka�dej zmianie zaznaczenia na li�cie
    u�ytkownik�w. Zaznacza odpowiednie checkboksy odpowiadaj�ce
    uprawnieniom wybranego u�ytkownika.
*/
function updatePermissions(sel)
{
    if (sel.options.selectedIndex < 0)
        return;
    // tablica nazw uprawnie� nadanych (i odebranych) zaznaczonemu u�ytkownikowi
    var subjectPerms = subjects[sel.options[sel.options.selectedIndex].value];
    var inputs = document.getElementsByTagName('input');

    for (var i=0; i < inputs.length; i++)
    {
        var input = inputs.item(i);
        if (!input.type || input.type.toLowerCase() != 'checkbox' || !input.name.startsWith('perm_'))
            continue;

        var name = input.name.substring(5);
        if (subjectPerms && subjectPerms[name] == true)
            input.checked = true;
        else
            input.checked = false;
    }

    <ds:available test="layout2">
    Custom.clear();
    </ds:available>
}

/**
    Wywo�ywane po zaznaczeniu lub usuni�ciu zaznaczenia pola wyboru
    odpowiadaj�cego jednemu z praw.
*/
function checkPermission(checkbox)
{
    var sel = document.getElementById('subjects');
    if (sel.options.selectedIndex < 0)
        return;
    var subject = sel.options[sel.options.selectedIndex].value;

    if (!subjects[subject])
        subjects[subject] = new Array();

    subjects[subject][checkbox.name.substring(5)] = checkbox.checked ? true : false;
}

/**
    Tworzy tablic� o postaci:
    lk { read write create }; * { delete read }; wk { read create }
*/
function makePermissionsString()
{
    var ps = '';
    for (var subject in subjects)
    {
        ps += subject+' {';
        for (var perm in subjects[subject])
        {
            if (subjects[subject][perm] == true)
                ps += perm+' ';
        }
        ps += '}; ';
    }
    document.getElementById('permissionsString').value = ps;
}

function removeSubject()
{
    var sel = document.getElementById('subjects');
    if (sel.options.selectedIndex < 0)
        return;
    var subject = sel.options[sel.options.selectedIndex].value;
    subjects[subject] = null;
    sel.options[sel.options.selectedIndex] = null;
}

/**
    Funkcja wywo�ywana przez okienko wyboru u�ytkownika (pick-subject.jsp).
*/
function __pick_subject(value, text)
{
    var sel = document.getElementById('subjects');

    // sprawdzam, czy dodawany element jest ju� na li�cie
    for (var i=0; i < sel.options.length; i++)
    {
        if (sel.options[i].value == value)
        {
            document.getElementById('message').innerHTML = '<ds:lang text="WybranyUzytkownikZnajdujeSieJuzNaLiscie"/>';
            return false;
        }
    }
    sel.options[sel.options.length] = new Option(text, value);

    if (value.endsWith('/user'))
        document.getElementById('message').innerHTML = '<ds:lang text="DodanoUzytkownika"/> '+text;
    else if (value.endsWith('/group'))
        document.getElementById('message').innerHTML = '<ds:lang text="DodanoGrupe"/> '+text;

    sel.options.selectedIndex = sel.options.length-1;
    updatePermissions(sel);
}

function __pick_subjects(options)
{
    for (var i=0; i < options.length; i++)
    {
        __pick_subject(options[i].value, options[i].text);
    }
}

</script>

<html:form action="/security/set-permissions" onsubmit="makePermissionsString()">
<html:hidden property="objectType"/>
<html:hidden property="objectId" />
<input type="hidden" name="permissionsString" id="permissionsString"/>

<table width="100%">
<tr>
    <td width="40%">
        <select name="subjects" id="subjects" size="6" onclick="updatePermissions(this);" style="multi_sel">
            <c:forEach items="${subjects}" var="subject" varStatus="status" >
                <option value='<c:out value="${subject.value}"/>' <c:if test="${status.first}">selected="true"</c:if>><c:out value="${subject.text}"/></option>
            </c:forEach>
        </select>
    </td>
    <td valign="top" width="60%">
        <edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${pageContext.request.contextPath}${pickSubjectLink}"  styleClass="btn"><ds:lang text="Dodaj"/></edm-html:window-open-button> <br>
        <input type="button" value="<ds:lang text="Usun"/>" class="btn" onclick="removeSubject();"> <br>
    </td>
</tr>
<tr>
    <td colspan="2">
        <div id="message">
            <c:if test="${updatedPermissions}">
            <ds:lang text="ZaktualizowanoUprawnienia"/>
            </c:if>
        </div>
    </td>
</tr>
<tr>
    <td colspan="2">
        <table width="100%">
        <c:forEach items="${availablePermissions}" var="perm">
            <tr>
                <td><c:out value="${perm.description}"/></td>
                <td><input type="checkbox" name="perm_<c:out value='${perm.name}'/>" onclick="checkPermission(this);"/></td>
            </tr>
        </c:forEach>
        </table>
    </td>
</tr>
</table>

<html:submit property="doUpdate" styleClass="btn" ><edm-html:message key="doUpdate" global="true"/></html:submit>
<input type="button" onclick="window.close();" class="cancel_btn cancelBtn" value="<ds:lang text="Anuluj"/>"/>


</html:form>

<script language="JavaScript">
updatePermissions(document.getElementById('subjects'));
</script>
