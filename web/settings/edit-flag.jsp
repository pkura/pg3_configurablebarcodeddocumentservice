<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-flag.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ww:if test="userFlags">
		<ds:lang text="EdycjaFlagiUzytkownika"/>
	</ww:if>
	<ww:else>
		<ds:lang text="EdycjaFlagiGlobalnej"/>
	</ww:else>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
	<ww:hidden name="'id'"/>

	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="KodFlagi"/>:
			</td>
			<td>
				<ww:textfield name="'c'" id="c" size="1" maxlength="3"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="OpisFlagi"/>:
			</td>
			<td>
				<ww:textfield name="'description'" id="description" size="50" maxlength="80"/>
			</td>
		</tr>
		<ww:if test="!userFlags">
			<tr>
				<td>
					<ds:lang text="PrawoNadawania"/>
				</td>
				<td>
					<ww:select name="'divisionGuid'" list="divisions" cssClass="'sel'" listKey="guid" listValue="name"
						headerKey="'#system#'" headerValue="getText('select.wszystkie')" id="division" value="divisionGuid"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="PrawoDoPodgladu"/>
				</td>
				<td>
					<ww:select name="'viewersDivisionGuid'" list="divisions" cssClass="'sel'" listKey="guid" listValue="name"
						headerKey="'#system#'" headerValue="getText('select.wszystkie')" id="viewersDivision" value="viewersDivisionGuid"/>
				</td>
			</tr>
		</ww:if>
	</table>

	<ds:event name="'doUpdate'" value="getText('ZapiszFlage')" onclick="'if (!(validateFlag())) return false;'"/>
	
	<ww:if test="userFlags">
		<input type="button" value="<ds:lang text="Zrezygnuj"/>" class="btn"
			onclick="document.location.href = '<ww:url value="'/settings/user-flags.action'"/>'"/>
	</ww:if>
	<ww:else>
		<input type="button" value="<ds:lang text="Zrezygnuj"/>" class="btn"
			onclick="document.location.href = '<ww:url value="'/settings/global-flags.action'"/>'"/>
	</ww:else>
</form>

<%--
<ww:if test="userFlags"><h1><ds:lang text="EdycjaFlagiUzytkownika"/></h1></ww:if>
<ww:else><h1><ds:lang text="EdycjaFlagiGlobalnej"/></h1></ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">

	<ww:hidden name="'id'"/>

	<table>
		<tr>
			<td><ds:lang text="KodFlagi"/>:</td>
			<td><ww:textfield name="'c'" id="c" size="1" maxlength="3"/></td>
		</tr>
		<tr>
			<td><ds:lang text="OpisFlagi"/>:</td>
			<td><ww:textfield name="'description'" id="description" size="50" maxlength="80"/></td>
		</tr>
		<ww:if test="!userFlags">
			<tr>
				<td><ds:lang text="PrawoNadawania"/></td>
				<td>
					<ww:select name="'divisionGuid'" list="divisions"
						cssClass="'sel'" listKey="guid" listValue="name" headerKey="''" headerValue="getText('select.wszystkie')"
						id="division" value="divisionGuid"/>
				</td>
			</tr>
			<tr>
				<td><ds:lang text="PrawoDoPodgladu"/></td>
				<td>
					<ww:select name="'viewersDivisionGuid'" list="divisions"
						cssClass="'sel'" listKey="guid" listValue="name" headerKey="''" headerValue="getText('select.wszystkie')"
						id="viewersDivision" value="viewersDivisionGuid"/>
				</td>
			</tr>
		</ww:if>
	</table>

	<ds:event name="'doUpdate'" value="getText('ZapiszFlage')" onclick="'if (!(validateFlag())) return false;'"/>
	<ww:if test="userFlags">
		<input type="button" value="<ds:lang text="Zrezygnuj"/>" class="btn"
			   onclick="document.location.href = '<ww:url value="'/settings/user-flags.action'"/>'"/>
	</ww:if>
	<ww:else>
		<input type="button" value="<ds:lang text="Zrezygnuj"/>" class="btn"
			   onclick="document.location.href = '<ww:url value="'/settings/global-flags.action'"/>'"/>
	</ww:else>
--%>
<script type="text/javascript">
	var usedLetters = {
		<ww:iterator value="flags" status="status">
			'<ww:property value="c.toUpperCase()"/>': true <ww:if test="!#status.last">, </ww:if>
		</ww:iterator>
	}

	var minC = 'A';
	var maxC = 'Z';

	function validateFlag()
	{
		var c = E('c').value.toUpperCase();

		if (c.trim().length > 3 || ! c.match(/\w+/))
		{
			alert('<ds:lang text="KodFlagiPowinienBycLitera"/>(A-Z)')
			return false;
		}

		<%--
		if (usedLetters[c] == true)
		{
			alert('<ds:lang text="LiteraZostalaJuzUzytaDoOznaczeniaInnejFlagi"/>');
			return false;
		}--%>

		if (E('description').value.trim().length == 0)
		{
			alert('<ds:lang text="NiePodanoOpisuFlagi"/>');
			return false;
		}

		return true;
	}
</script>
<!--N koniec edit-flag.jsp N-->