<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<!--N poczatek default-task-list.jsp N-->

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="tabs.selectedTab.title"/> - <ds:lang text="kolumny"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="defaultTabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="defaultTabs" status="status" >
	    <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/settings/default-task-list.action'"/>" method="post" onsubmit="selectAllOptions(this.defaultSelectedColumns);">
	<ww:hidden name="'tab'"/>
	<ww:hidden name="'ascending'"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<p>
		<ds:lang text="PodzialNaStrony"/>:
		<ww:checkbox name="'defaultSimpleTaskList'" fieldValue="true" value="defaultSimpleTaskList"/>
	</p>


	<ww:if test="!'cases'.equals(tab)">
		<p>
			<ds:lang text="DomyslneSortowanieWedlug"/>
			<ww:select name="'defaultSortColumn'" list="allColumns" cssClass="'sel'"/>
			<ww:if test="ascending.equals('false')">
				<a href="<ww:url value="'/settings/task-list.action'"/>?ascending=true&tab=<ww:property value="tab"/>"></a>
				<ds:lang text="Malejaco"/>
			</ww:if>
			<ww:else>
				<a href="<ww:url value="'/settings/task-list.action'"/>?ascending=false&tab=<ww:property value="tab"/>"></a>
				<ds:lang text="Rosnaco"/>
			</ww:else>
		</p>
	</ww:if>
	
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepneKolumy"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="KolumyNaLiscieZadan"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'defaultAvailableColumns'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
					list="defaultAvailableColumns" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; "
					onclick="if (moveOptions(this.form.defaultAvailableColumns, this.form.defaultSelectedColumns) > 0) dirty=true;"	class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="if (moveOptions(this.form.defaultSelectedColumns, this.form.defaultAvailableColumns) > 0) dirty=true;" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'defaultSelectedColumns'" id="defaultSelectedColumns" multiple="true" size="10" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="defaultColumns"/>
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(dirty=true, moveOption('defaultSelectedColumns', -1, -1));"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(dirty=true, moveOption('defaultSelectedColumns', -1, 1));"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>								
		</tr>
	</table>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdateDefault'" cssClass="'btn saveBtn'"/>
	<ds:submit-event value="getText('Powrot')" name="'doCancel'"/>

	<ds:available test="tasklist.resetForOther">
	    <ds:submit-event value="getText('PrzywrocKolumny')" name="'doResetDefaultForUser'"/>
        <ww:select name="'resetUser'" list="userList" cssClass="'sel'" listKey="name" listValue="asLastnameFirstname()" />
	</ds:available>

	<%-- <ds:submit-event value="getText('PrzywrocKolumny')" name="'doResetDefault'"/> --%>
	<%-- <ds:submit-event value="getText('OdswiezListeZadan')" name="'doSynchronize'"/> --%>
	
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<!--N koniec default-task-list.jsp N-->