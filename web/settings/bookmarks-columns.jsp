<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="tabs.selectedTab.title"/> - <ds:lang text="kolumny"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/bookmarks-columns.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedColumns);">
	<ww:hidden name="'tab'"/>
	<ww:hidden name="'bookmarkId'" value="bookmarkId" />
		<ds:available test="layout2">
			<div id="middleContainer">
		</ds:available>
	
	
	<p>
		<ds:lang text="LiczbaZadanNaStronie"/>:
		<ww:select value="taskCount" name="'taskCount'" cssClass="'sel'" list="taskCounts" />
	</p>
	
	<ww:if test="!'cases'.equals(tab) && !'order'.equals(tab)">
		<p>
			<ds:lang text="DomyslneSortowanieWedlug"/>
			<ww:select name="'sortColumn'" list="allColumns" cssClass="'sel'" />
			<ww:select name="'ascending'" list="ascendingType" cssClass="'sel'" />
		</p>
	</ww:if>
	
	<ww:if test="!'order'.equals(tab)">
		<ds:has-permission name="WYBOR_KOLUMN_NA_LISCIE_ZADAN">
			<table class="tableMargin">
				<tr>
					<th>
						<ds:lang text="DostepneKolumy"/>
					</th>
					<th></th>
					<th colspan="2">
						<ds:lang text="KolumyNaLiscieZadan"/>
					</th>
				</tr>
				<tr>
					<td class="alignTop">
						<ww:select name="'availableColumns'" multiple="true" size="10" cssClass="'multi_sel'"
							cssStyle="'width: 300px;'" list="availableColumns"/>
					</td>
					<td class="alignMiddle">
						<input type="button" value=" &gt;&gt; " name="moveRightButton"
							onclick="if (moveOptions(this.form.availableColumns, this.form.selectedColumns) > 0) dirty=true;" class="btn"/>
						<br/>
						<input type="button" value=" &lt;&lt; " name="moveLeftButton"
							onclick="if (moveOptions(this.form.selectedColumns, this.form.availableColumns) > 0) dirty=true;" class="btn"/>
					</td>
					<td class="alignTop">
						<ww:select name="'selectedColumns'" id="selectedColumns" multiple="true" size="10"
							cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="userColumns"/>
					</td>
					<td class="alignMiddle">
						<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, -1));">
							<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, 1));">
							<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
					</td>
				</tr>
			</table>
		</ds:has-permission>
	</ww:if>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/>
	<ds:submit-event value="getText('Powrot')" name="'doCancel'"/>
	<ds:submit-event value="getText('PrzywrocKolumny')" name="'doResetDefault'"/>
	
	<ds:available test="tasklist.userrefresh">
		<ds:submit-event value="getText('OdswiezListeZadan')" name="'doSynchronize'"/>
	</ds:available>
	
	<ds:available test="layout2">
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div>
	</ds:available>
</form>