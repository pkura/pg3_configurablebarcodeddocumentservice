<%-- 
    Document   : ds-parts-config.jsp
    Created on : 2009-10-01, 12:12:04
    Author     : Micha� Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:degradable-select name="configuration" list="configurations"/>

<div id="accordion" style="width:90%;">
	<ww:iterator value="parts.entrySet()">
		<h3><a href="#"><ww:property value="value"/></a></h3>
		<div>content(<ww:property value="value"/>)</div>
	</ww:iterator>
</div>

