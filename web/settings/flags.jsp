<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N flags.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ww:if test="userFlags">
		<ds:lang text="FlagiUzytkownika"/>
	</ww:if>
	<ww:else>
		<ds:lang text="FlagiGlobalne"/>
	</ww:else>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table class="tableMargin">
		<ww:iterator value="flags">
			<tr>
				<td>
					<ww:checkbox name="'flagIds'" fieldValue="id"/>
				</td>
				<td>
					<span class="bold">
						<ww:property value="c"/>
					</span>
				</td>
				<td>
					<ww:property value="description"/>
				</td>
				<ww:if test="userFlags">
					<td>
						[
						<a href="<ww:url value="'/settings/edit-user-flag.action'"><ww:param name="'id'" value="id"/></ww:url>">
							<ds:lang text="edycja"/></a>
						]
					</td>
				</ww:if>
				<ww:else>
					<td>
						[
						<a name="<ww:property value="description"/>" href="<ww:url value="'/settings/edit-global-flag.action'"><ww:param name="'id'" value="id"/></ww:url>">
							<ds:lang text="edycja"/></a>
						]
					</td>
				</ww:else>
			</tr>
		</ww:iterator>
	</table>
	
	<ds:event name="'doDelete'" value="getText('Usun')"/>
	
	<ww:if test="canAddFlags">
		
		<br/>
		<br/>
		<br/>
		<table class="tableMargin">
			<tr>
				<td>
					<ds:lang text="KodFlagi"/>:
				</td>
				<td>
					<ww:textfield name="'c'" id="c" size="5" maxlength="5"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="OpisFlagi"/>:
				</td>
				<td>
					<ww:textfield name="'description'" id="description" size="50" maxlength="80"/>
				</td>
			</tr>
			<ww:if test="!userFlags">
				<tr>
					<td>
						<ds:lang text="PrawoNadawania"/>
					</td>
					<td>
						<ww:select name="'divisionGuid'" list="divisions" cssClass="'sel'" listKey="guid" listValue="name"
							headerKey="'#system#'" headerValue="getText('wszyscy')" id="division" value="divisionGuid"/>
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="PrawoDoPodgladu"/>
					</td>
					<td>
						<ww:select name="'viewersDivisionGuid'" list="divisions" cssClass="'sel'" listKey="guid" listValue="name"
							headerKey="'#system#'" headerValue="getText('wszyscy')" id="viewersDivision" value="viewersDivisionGuid"/>
					</td>
				</tr>
			</ww:if>
		</table>

		<ds:event name="'doAdd'" value="getText('UtworzFlage')" onclick="'if (!(validateFlag())) return false;'"/>
	</ww:if>
	 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<ww:if test="userFlags"><h1><ds:lang text="FlagiUzytkownika"/></h1></ww:if>
<ww:else><h1><ds:lang text="FlagiGlobalne"/></h1></ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ww:iterator value="tabs" status="status" >
	<%-- <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmianChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a> -->
	<a onclick="if (!confirm('<ds:lang text="NieZapisanoZmianChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">

	<table>
		<ww:iterator value="flags">
			<tr>
				<td><ww:checkbox name="'flagIds'" fieldValue="id"/></td>
				<td><b><ww:property value="c"/></b></td>
				<td><ww:property value="description"/></td>
				<ww:if test="userFlags">
					<td>[<a href="<ww:url value="'/settings/edit-user-flag.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
				</ww:if>
				<ww:else>
					<td>[<a name="<ww:property value="description"/>" href="<ww:url value="'/settings/edit-global-flag.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
				</ww:else>
			</tr>
		</ww:iterator>
	</table>

	<ds:event name="'doDelete'" value="getText('Usun')"/>

	<ww:if test="canAddFlags">
		<table>
			<tr>
				<td><ds:lang text="KodFlagi"/>:</td>
				<td><ww:textfield name="'c'" id="c" size="1" maxlength="3"/></td>
			</tr>
			<tr>
				<td><ds:lang text="OpisFlagi"/>:</td>
				<td><ww:textfield name="'description'" id="description" size="50" maxlength="80"/></td>
			</tr>
			<ww:if test="!userFlags">
			<tr>
				<td><ds:lang text="PrawoNadawania"/></td>
				<td>
					<ww:select name="'divisionGuid'" list="divisions"
						cssClass="'sel'" listKey="guid" listValue="name" headerKey="'all'" headerValue="getText('wszyscy')"
						id="division" value="divisionGuid"/>
				</td>
			</tr>
			<tr>
				<td><ds:lang text="PrawoDoPodgladu"/></td>
				<td>
					<ww:select name="'viewersDivisionGuid'" list="divisions"
						cssClass="'sel'" listKey="guid" listValue="name" headerKey="'all'" headerValue="getText('wszyscy')"
						id="viewersDivision" value="viewersDivisionGuid"/>
				</td>
			</tr>
			</ww:if>
		</table>

		<ds:event name="'doAdd'" value="getText('UtworzFlage')" onclick="'if (!(validateFlag())) return false;'"/>
	</ww:if>

</form>
--%>
<script type="text/javascript">
	var usedLetters = {
		<ww:iterator value="flags" status="status">
			'<ww:property value="c.toUpperCase()"/>': true <ww:if test="!#status.last">, </ww:if>
		</ww:iterator>
	}

	var minC = 'A';
	var maxC = 'Z';

	function validateFlag()
	{
		var c = E('c').value.toUpperCase();

		if (c.trim().length > 5 || ! c.match(/\w+/))
		{
			alert('<ds:lang text="KodFlagiPowinienBycLitera"/>(A-Z)')
			return false;
		}

		if (usedLetters[c] == true)
		{
			alert('<ds:lang text="LiteraZostalaJuzUzytaDoOznaczeniaInnejFlagi"/>');
			return false;
		}

		if (E('description').value.trim().length == 0)
		{
			alert('<ds:lang text="NiePodanoOpisuFlagi"/>');
			return false;
		}

		return true;
	}
</script>
<!--N koniec flags.jsp N-->