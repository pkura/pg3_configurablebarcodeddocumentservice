<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<h1>Zarządzanie zakładkami</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />


<ds:available test="!layout2">
	<ds:xmlLink path="Bookmarks"/>
</ds:available>


<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="Bookmarks"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/default-bookmarks.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>


<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="AvailableBookmarks"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="SelectedBookmarks"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'availableBookmarks'" multiple="true" size="10" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="availableBookmarks" listKey="id" listValue="name" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " name="moveRightButton"
					onclick="if (moveOptions(this.form.availableBookmarks, this.form.selectedBookmarks) > 0) dirty=true;" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " name="moveLeftButton"
					onclick="if (moveOptions(this.form.selectedBookmarks, this.form.availableBookmarks) > 0) dirty=true;" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'selectedBookmarks'" id="selectedBookmarks" multiple="true" size="10"
					cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="selectedBookmarksList" listKey="id" listValue="name" />
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(dirty=true, moveOption('selectedBookmarks', -1, -1));">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(dirty=true, moveOption('selectedBookmarks', -1, 1));">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
		
		<tr>
			<td colspan="4" >
				<ds:submit-event value="'Zapisz'" name="'doSave'" ></ds:submit-event>
			</td>
		</tr>
	</table>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>
<script type="text/javascript" >
$j('#form').bind('submit.selectAllOptions', function() {
	$j('#selectedBookmarks option').attr('selected', 'selected')
});
</script>