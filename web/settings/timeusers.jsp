<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N timeusers.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Lista do przypisywania termin�w - <ww:property value="subtitle"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form">
	<ww:hidden name="'divisionGuid'" id="divisionGuid"/>
	<ww:hidden name="'doUpdate'" id="doUpdate"/>
	
	<h3>Aktualna lista do przypisywania termin�w</h3>

	<table class="tableMargin">
		<tr>
			<td>
				<ww:select name="'targetsSelectValues'" list="targetsSelect" cssClass="'sel'" id="targetsSelect" multiple="true" size="10"/>
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(moveOption('targetsSelect', -1, -1))">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(moveOption('targetsSelect', -1, 1))">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</table>
	
	<a href="javascript:void(removeOptions('targetsSelect'))">usu� zaznaczone</a>
	<br/>
	<input type="button" class="btn" value="posortuj" onclick="sort()"/>
	
	<br/>
	<br/>
	<ww:property value="treeHtml" escape="false"/>
	<ww:if test="users.length > 0">
		<h3>Dodaj osob� do listy</h3>
		<ww:iterator value="users" id="status">
			<a href="javascript:void(addUser('<ww:property value="top.name"/>', '<ww:property value="targetDivision.guid"/>', '<ww:property value="top.asFirstnameLastname()"/>', '<ww:property value="targetDivision.name"/>'))">
				<ww:property value="asFirstnameLastname()"/></a>
			<br/>
		</ww:iterator>
	</ww:if>

	<input type="button" class="btn" value="Zapisz" onclick="submitForm()"/>
</form>

<%--
<h1>Lista do przypisywania termin�w - <ww:property value="subtitle"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form">
	<ww:hidden name="'divisionGuid'" id="divisionGuid" />
	<ww:hidden name="'doUpdate'" id="doUpdate"/>
<%--
	<ww:iterator value="targets">
		<ww:hidden name="'targets'" value="top"/>
	</ww:iterator>
-->

	<h3>Aktualna lista do przypisywania termin�w</h3>
	<table>
		<tr>
			<td rowspan="2">
				<ww:select name="'targetsSelectValues'"
						   list="targetsSelect" cssClass="'sel'"
					id="targetsSelect"
					multiple="true" size="10"/>
			</td>
			<td valign="top"><a href="javascript:void(moveOption('targetsSelect', -1, -1))"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a></td>
		</tr>
		<tr>
			<td valign="bottom"><a href="javascript:void(moveOption('targetsSelect', -1, 1))"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a></td>
		</tr>
		<tr>
			<td colspan="2"><a href="javascript:void(removeOptions('targetsSelect'))">usu� zaznaczone</a></td>
		</tr>
		<tr>
			<td align="right"><input type="button" class="btn" value="posortuj" onclick="sort()"/></td>
		</tr>
	</table>
	

	<br/>
	
	<table width="100%">
		<tr>
			<td valign="top">
				<ww:property value="treeHtml" escape="false"/>
				<ww:if test="users.length > 0">
					<br/>
					<h3>Dodaj osob� do listy</h3>
					<ww:iterator value="users" id="status">
						<a href="javascript:void(addUser('<ww:property value="top.name"/>', '<ww:property value="targetDivision.guid"/>', '<ww:property value="top.asFirstnameLastname()"/>', '<ww:property value="targetDivision.name"/>'))"><ww:property value="asFirstnameLastname()"/></a>
						<br/>
					</ww:iterator>
				</ww:if>
			</td>
		</tr>
	</table>

	<input type="button" class="btn" value="Zapisz" onclick="submitForm()"/>
	
</form>
--%>
<script type="text/javascript">
	/**
		Wysy�a formularz bez �adnego zdarzenia, w celu
		od�wie�enia drzewa dzia��w.
	*/
	function post(guid)
	{
		E('divisionGuid').value = guid;
		selectAllOptions('targetsSelect');
		E('form').submit();
	}

	function addUser(username, guid, userDescription, guidDescription)
	{
		var value = username+';'+guid;
		var sel = E('targetsSelect');
		for (var i=0; i < sel.options.length; i++)
		{
			var ss = sel.options[i].value.split(';');
			if (username == ss[0] && guid == ss[1])
			{
				alert('U�ytkownik znajduje si� ju� na li�cie');
				return false;
			}
		}
		var text = userDescription + ' (' + guidDescription + ')';
		sel.options[sel.options.length] = new Option(text, value);
	}

	function sort()
	{
		var sel = E('targetsSelect');

		for(var i=0; i<sel.options.length;i++)
			for(var j=i; j<sel.options.length;j++)
			{
				var tab1 = sel.options[i].text.split(' ');
				var tab2 = sel.options[j].text.split(' ');
				if(tab1[1]>tab2[1])
				{
					var tmp = new Option(sel.options[i].text, sel.options[i].value);
					sel.options[i] = new Option(sel.options[j].text, sel.options[j].value);
					sel.options[j] = tmp
				}
			}
	}

	function submitForm()
	{
		var form = E('form');
		disableFormSubmits(form);
		var sel = E('targetsSelect');
		for (var i=0; i < sel.options.length; i++)
		{
			var opt = sel.options[i].value;
			var hid = document.createElement('INPUT');
			hid.type = 'HIDDEN';
			hid.name = 'targets';
			hid.value = opt + ';' + (i+1);
			form.appendChild(hid);
		}
		E('doUpdate').value = 'true';
		selectAllOptions(sel);
		form.submit();
	}
</script>
<!--N koniec timeusers.jsp N-->