<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Szablony"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>>
		<ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<style type="text/css">

	.templates {
		/*width: 800px;*/
	}

	.user-templates, .global-templates {
		min-width: 500px;
		display: none;
	}

	.templates-global-caption,
	.templates-user-caption {
		font-size: 16px;
		font-weight: bold;
		margin-top: 10px;
		border-bottom: 1px black solid;
	}

	.dockind-view {
		display: none;
	}

	.choose-dockind-view {
		display: none;
		margin-bottom: 10px;
	}

	.file-upload {
		margin-top: 20px;
	}

</style>

<ds:available test="layout2">
    <div id="middleContainer">
</ds:available>

<div id="messages">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</div>

<div class="choose-dockind-view">

	Wybierz rodzaj dokumentu: <select id="select-dockind"></select>
	<button id="select-dockind-button">Wybierz</button>

</div>

<div class="dockind-view">
	<table class="global-templates">
		<caption class="templates-global-caption">Szablony globalne</caption>
	</table>
	<table class="user-templates">
		<caption class="templates-user-caption">Szablony u�ytkownika</caption>
	</table>

	<div class="file-upload">
		
		<!-- <ww:url value="'/settings/user-templates.action'"><ww:param name="'uploadTemplate'" value="'true'"/></ww:url> -->

		<form class="form-file-upload" enctype="multipart/form-data" method="post">

			<input type="hidden" name="dockind" class="input-dockind">

			Dodaj szablon u�ytkownika: <input type="file" name="file"/> <input type="submit" value="Przeci�gij Upu�� - Wy�lij">

		</form>

	</div>
</div>
	
<!-- View logic -->
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/URI-1.11.2.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/lodash.compat-2.4.1.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/ajaxAction.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript">

	var ajaxAction = ajaxActionFactory();

	(function($){
	$(function(){

		var windowQuery = URI.parseQuery(URI.parse(window.location.href)["query"]);
		var dockind = windowQuery["dockind"];

		initChooseDockindView();

		if(dockind) {
			initDockindView();
		}

		function initChooseDockindView() {
			
			ajaxAction.get("dockinds").done(function(dockinds){
				var jqSelect = $("#select-dockind");
				
				_.each(dockinds, function(dockind){
					$("<option/>").attr("value", dockind["cn"]).text(dockind["name"]).appendTo(jqSelect);
				});

				$(".choose-dockind-view").show();

				$("#select-dockind-button").click(function(){
					// $(".choose-dockind-view").hide();

					dockind = jqSelect.val();

					initDockindView(dockind);

				})

			})

		}

		function initDockindView() {

			$(".dockind-view").show();

			initDockindGlobalView();
			initDockindUserView();
		}

		function initDockindGlobalView() {
			$(".global-templates tr").remove();
			var jqTable = $(".global-templates");

			jqTable.hide();

			ajaxAction.get("globalTemplates", {"dockind": dockind}).done(function(templates){

				var downloadUri = URI(window.location.href).query({"templateFile": "true", "dockind": dockind});

				if(templates.length > 0) {
					jqTable.show();

					_.each(templates, function(t){
						var uri = downloadUri.clone().addQuery("filename", t["name"]);
						$("<tr/>")
						.append($("<td colspan='2'/>").html("<a href='"+uri+"'>"+t["name"]+"</a>"))
						.appendTo(jqTable);
					});
				}

			});
		}


		function initDockindUserView() {
			$(".user-templates tr").remove();

			// pobierz templaty u�ytkownika, modyfikowalne
			ajaxAction.get("userTemplates", {"dockind": dockind}).done(function(templates){

				var downloadUri = URI(window.location.href).query({"templateFile": "true", "dockind": dockind});
				var jqTable = $(".user-templates");

				if(templates.length > 0) {
					jqTable.show();

					_.each(templates, function(t){
						var uri = downloadUri.clone().addQuery("filename", t["name"]).addQuery("asUserRepo", true);
						$("<tr/>")
						.append($("<td/>").html("<a href='"+uri+"'>"+t["name"]+"</a>"))
						.append($("<td/>").append($("<button class='del' data-name='"+t["name"]+"'>usu�</button>")))
						.appendTo(jqTable);
					});
				}

			});
		}

		$(".user-templates").on("click", ".del", function(){

			var self = $(this);
			var name = self.data().name;

			if(! confirm("Potwierd� usuni�cie szablonu u�ytkownika "+name)) {
				return false;
			}

			ajaxAction.get("removeTemplate", {"name": name, "dockind": dockind}).done(function(){
				self.parents("tr").first().remove();
			});

		});

		$(".form-file-upload").on("submit", function(){

			$(this).attr("action", URI(window.location.href).query({
				"dockind": dockind,
				"uploadTemplate": "true"
			}));

			$(".input-dockind").val(dockind);

			return true;
		});

	})
	}($j));

</script>

<ds:available test="layout2">
    </div>
	<div class="bigTableBottomSpacer">&nbsp;</div>
</ds:available>
