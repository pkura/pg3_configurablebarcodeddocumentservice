<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N contact.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="InneUstawienia"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/settings/other.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
	<ww:hidden name="'tab'"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
				<ww:textfield name="'email'" id="email" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="PhoneNum"/>:
			</td>
			<td>
				<ww:textfield name="'phoneNum'" id="mobileNum" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="TelKom"/>:
			</td>
			<td>
				<ww:textfield name="'mobileNum'" id="mobileNum" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumWew"/>:
			</td>
			<td>
				<ww:textfield name="'extension'" id="extension" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumKomunikatora"/>:
			</td>
			<td>
				<ww:textfield name="'communicatorNum'" id="communicatorNum" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<ds:dockinds test="prosika">
			<tr>
			<td>
				<ds:lang text="NumPokoju"/>:
			</td>
			<td>
				<ww:textfield name="'roomNum'" id="roomNum" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		</ds:dockinds>
	</table>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'" />
			 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<h1><ds:lang text="InneUstawienia"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>
<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/settings/other.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
<ww:hidden name="'tab'"/>
	<table border='0' cellspacing='0' cellpadding='0'>
	
		<tr>
			<td><ds:lang text="Email"/>:</td>
			<td><ww:textfield name="'email'" id="email" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td><ds:lang text="PhoneNum"/>:</td>
			<td><ww:textfield name="'phoneNum'" id="mobileNum" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td><ds:lang text="TelKom"/>:</td>
			<td><ww:textfield name="'mobileNum'" id="mobileNum" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td><ds:lang text="NumWew"/>:</td>
			<td><ww:textfield name="'extension'" id="extension" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td><ds:lang text="NumKomunikatora"/>:</td>
			<td><ww:textfield name="'communicatorNum'" id="communicatorNum" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		
	</table>
	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" />
</form>
--%>
<!--N koniec contact.jsp N-->