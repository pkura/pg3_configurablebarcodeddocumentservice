<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="taskListMailer"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/mootools.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/timePicker/timePicker.js?v=<%= Docusafe.getDistTimestamp() %>"></script>

<script type="text/javascript">
	var tp = null;
	/* ustawiamy odpowiednie pola po wczytaniu dokumentu */
	$j(document).ready( function() {
			tp = new TimePicker('time_picker', 'time', 'time_toggler', {format24:true, imagesPath:"<ww:url value="'/timePicker/images'"/>",
				offset:{x:0, y:10}});

			showInformForms($j('#termKind').get(0));
		});

	function showInformForms(sender) {
		var classNameToShow = 'inform_' + sender.options[sender.selectedIndex].value;

		// chowamy wszystkie pola
		$j('.inform').hide();

		// pokazujemy odpowiednie poladla wybranego selecta
		$j('.' + classNameToShow).show();

		//alert(classNameToShow);
	}
</script>

<form id="form" action="<ww:url value="'/settings/taskListMailer.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
		<table class="tableMargin">
			<tr>
				<td>
					<ds:lang text="ObslogaTaskListMailer"/> email:
				</td>
				<td>
					<ww:checkbox name="'useTaskListMailer'" fieldValue="true" value="useTaskListMailer"/>
				</td>
			</tr>

			<tr>
				<td>
					<ds:lang text="czasMail" /> : <ww:select name="'termKind'"  id="termKind" list="termKinds" value="termKind" cssClass="'sel'" onchange="'showInformForms(this)'"/>
				</td>
				<td class="inform inform_1 inform_2">
					<ww:textfield name="'timeMail'" size="10" maxlength="20" cssClass="'txt'"/>
				</td>
				<td class="inform inform_5">
					<ds:lang text="dzienMiesiaca" /> : <ww:textfield name="'dayOfMonth'" size="2" maxlength="2" cssClass="'txt'" value="dayOfMonth"/>
				</td>
				<td class="inform inform_4">
					<ds:lang text="ktoregoDnia" /> : <ww:select name="'day'" list="days" value="day" cssClass="'sel'"/>
				</td>
				<td class="inform inform_3 inform_4 inform_5">
					<ds:lang text="oGodzinie" /> : <ww:textfield name="'time'" id="time" size="2" maxlength="20" cssClass="'txt'"/>

					<a href="#" id="time_toggler" title="<ds:lang text="oGodzinie" />" style="line-height: 22px; vertical-align: middle">
						<img src="<ww:url value="'/img/clock.gif'"/>" style="margin-bottom: -6px; _margin-bottom: 0px;" />
					</a>

					<div id="time_picker" class="time_picker_div"></div>
				</td>
				<td>
			</tr>
			<ds:available test="taskListSMS">
			<tr>
				<td>
					<ds:lang text="ObslogaTaskListSMS"/>:
				</td>
				<td>
					<ww:checkbox name="'useTaskListSMS'" fieldValue="true" value="useTaskListSMS"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="czas" /> : <ww:select name="'termKindSMS'"  id="termKindSMS" list="termKinds" value="termKindSMS" cssClass="'sel'" onchange="'showInformForms(this)'"/>
				</td>
				<td class="inform inform_1 inform_2">
					<ww:textfield name="'timeSMS'" size="10" maxlength="20" cssClass="'txt'"/>
				</td>
				<td class="inform inform_5">
					<ds:lang text="dzienMiesiaca" /> : <ww:textfield name="'dayOfMonthSMS'" size="2" maxlength="2" cssClass="'txt'" value="dayOfMonthSMS"/>
				</td>
				<td class="inform inform_4">
					<ds:lang text="ktoregoDniaSMS" /> : <ww:select name="'daySMS'" list="days" value="daySMS" cssClass="'sel'"/>
				</td>
				<td class="inform inform_3 inform_4 inform_5">
					<ds:lang text="oGodzinie" /> : <ww:textfield name="'timeSMSg'" id="timeSMSg	" size="2" maxlength="20" cssClass="'txt'"/>

					<a href="#" id="time_toggler" title="<ds:lang text="oGodzinie" />" style="line-height: 22px; vertical-align: middle">
						<img src="<ww:url value="'/img/clock.gif'"/>" style="margin-bottom: -6px; _margin-bottom: 0px;" />
					</a>

					<div id="time_picker" class="time_picker_div"></div>
				</td>
				<td>
			</tr>
			</ds:available>
			
			
		</table>

		<ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn saveBtn'"/>
		<ww:if test="adminAccess">
			<ds:submit-event value="getText('ZapiszDlaWszystkich')" name="'doSaveForUsers'"/>
		</ww:if>
		
		<ds:dockinds test="invoice">
		<ww:if test="adminAccess">
		<br/>
		<br/>
		<br/>
			<table>
				<tr><th colspan="3"><ds:lang text="fakturaKosztowa" /></th></tr>
				<tr>
					<td><ds:lang text="IloscDniDoPrzypomienia" /> :</td>
					<td><ww:textfield name="'invoiceDays'" size="5" maxlength="5" cssClass="'txt'"/></td>
					<td><ds:submit-event value="getText('Zapisz')" name="'doSaveForInvoice'" cssClass="'btn saveBtn'"/></td>
				</tr>
			</table>
		</ww:if>
		</ds:dockinds>		
</form>