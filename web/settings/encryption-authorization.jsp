<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1"><ds:lang text="AutoryzacjaDostepuDoTajnych"/> <ww:property value="subtitle"/></h1>

<ds:available test="!layout2">
<p>
<ds:xmlLink path="encryption"/>
</p>
</ds:available>

<hr class="fullLine horizontalLine"/>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ds:lang text="JestesObecnieAutoryzowanyDoDostepuONastepujacychNazwachKodowych"/>
<ul>
<ww:iterator value="authorizations">
	<li><ww:property value="keyCode"/></li>
</ww:iterator>
</ul>
<form action="<ww:url value="'/settings/encryption-authorization.action'"/>" method="post" id="form">
<table>
<tr>
	<td>
		<ds:lang text="NazwaKodowa"/>
	</td>
	<td>
		<ww:textfield name="'codeName'" cssClass="'txt'"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Haslo"/>
	</td>
	<td>
		<ww:password name="'password'" cssClass="'txt'"/>
	</td>
</tr>
<tr>
	<td colspan="2">
		<ds:submit-event value="getText('Autoryzuj')" name="'doAuthorize'" cssClass="'btn'"/>
	</td>
	
</tr>
</table>
	
	
</form>