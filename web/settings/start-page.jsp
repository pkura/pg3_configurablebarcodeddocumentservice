<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N start-page.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Strona startowa</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form id="form" action="<ww:url value="'/settings/start-page.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<table>
    <tr>
        <td>Strona startowa:</td>
        <td><ww:select name="'name'" list="pages" cssClass="'sel'"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:submit-event value="'Zapisz'" name="'doUpdate'" /></td>
    </tr>
</table>

</form>