<!--N task-list.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="tabs.selectedTab.title"/> - <ds:lang text="kolumny"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/settings/task-list.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedColumns);">
	<ww:hidden name="'tab'"/>
	<!-- <ww:hidden name="'ascending'"/> -->
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<%-- Jest caly czas wyszarzone, wiec lepiej by nie bylo wyswietlane.
	<p>
		<ds:lang text="PodzialNaStrony"/>:
		<ww:checkbox name="'simpleTaskList'" fieldValue="true" value="simpleTaskList" onchange="'dirty=true;'" disabled="true"/>
	</p>
	 --%>
	
	<p>
		<ds:lang text="LiczbaZadanNaStronie"/>:
		<ww:select value="taskCount" name="'taskCount'" cssClass="'sel'" list="taskCounts" onchange="'dirty=true;'"/>
	</p>
	
	<ww:if test="!'cases'.equals(tab)">
		<p>
			<ds:lang text="DomyslneSortowanieWedlug"/>
			<ww:select name="'sortColumn'" list="allColumns" cssClass="'sel'" onchange="'dirty=true;'"/>
			<!-- 
			<ww:if test="ascending.equals('false')">
				<a href="<ww:url value="'/settings/task-list.action'"/>?ascending=true&tab=<ww:property value="tab"/>"/>
					<ds:lang text="Malejaco"/></a>
			</ww:if>
			<ww:else>
				<a href="<ww:url value="'/settings/task-list.action'"/>?ascending=false&tab=<ww:property value="tab"/>"/>
					<ds:lang text="Rosnaco"/></a>
			</ww:else>
			-->
			<ww:select name="'ascending'" list="ascendingType" cssClass="'sel'" onchange="'dirty=true;'"/>					
		</p>
	</ww:if>
	
	<ds:has-permission name="WYBOR_KOLUMN_NA_LISCIE_ZADAN">
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepneKolumy"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="KolumyNaLiscieZadan"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'availableColumns'" multiple="true" size="10" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="availableColumns"/>
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " name="moveRightButton"
					onclick="if (moveOptions(this.form.availableColumns, this.form.selectedColumns) > 0) dirty=true;" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " name="moveLeftButton"
					onclick="if (moveOptions(this.form.selectedColumns, this.form.availableColumns) > 0) dirty=true;" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'selectedColumns'" id="selectedColumns" multiple="true" size="10"
					cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="userColumns"/>
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, -1));">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, 1));">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</table>
	</ds:has-permission>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/>
	<ds:submit-event value="getText('Powrot')" name="'doCancel'"/>
	<ds:submit-event value="getText('PrzywrocKolumny')" name="'doResetDefault'"/>
	<ds:available test="tasklist.userrefresh">
		<ds:submit-event value="getText('OdswiezListeZadan')" name="'doSynchronize'"/>
	</ds:available>
	
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	
</form>
<!--N koniec task-list.jsp N-->
