<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N ops.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ListaTypowDokumentowDlaOPS"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/ops.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doSave" id="doSave"/>
	
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="TypyDokumentow"/>
			</th>
			<th></th>
			<th>
				<ds:lang text="WybraneTypyDokumentow"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'listaTypow'" multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 400px;'" list="listaTypow"/>
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; "
					onclick="if (moveOptions(this.form.listaTypow, this.form.listaWybranychTypow) > 0) dirty=true;" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="if (moveOptions(this.form.listaWybranychTypow, this.form.listaTypow) > 0) dirty=true;" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'listaWybranychTypow'" id="listaWybranychTypow" multiple="true" size="20" cssClass="'multi_sel'"
					cssStyle="'width: 400px;'" list="listaWidocznychTypow"/>
			</td>
		</tr>
	</table>
	
	<input type="button" value="Zapisz" class="btn" onclick="endThis()"/>
</form>

<%--
<h1><ds:lang text="ListaTypowDokumentowDlaOPS"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form id="form" action="<ww:url value="'/settings/ops.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doSave" id="doSave"/>
	<!--
	<ww:select name="'listaWybranychTypow'" list="listaTypow" multiple="true" cssClass="'multi_sel'" />
	
	<br/><br/>
	<ds:submit-event value="getText('Zapisz')" name="'doSave'"/>
	-->
	
	<table>
		<tr>
			<th><ds:lang text="TypyDokumentow"/></th>
			<th></th>
			<th><ds:lang text="WybraneTypyDokumentow"/></th>
		</tr>
		<tr>
			<td valign="top">
				<ww:select name="'listaTypow'" multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 400px;'"
					list="listaTypow" />
			</td>
			<td valign="middle">
				<input type="button" value=" &gt;&gt; "
					onclick="if (moveOptions(this.form.listaTypow, this.form.listaWybranychTypow) > 0) dirty=true;"
					class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="if (moveOptions(this.form.listaWybranychTypow, this.form.listaTypow) > 0) dirty=true;"
					class="btn"/>
				<br/>
				
			</td>
			<td valign="top">
							<ww:select name="'listaWybranychTypow'"
								id="listaWybranychTypow"
								multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 400px;'"
								list="listaWidocznychTypow"/>
			</td>
		</tr>
	</table>
	<!-- 
	<a href="javascript:void(select(getElementByName('listaWybranychTypow'), true))"><ds:lang text="zaznacz"/></a>
	<ds:submit-event value="getText('Zapisz')" name="'doSave'"/>
	<a href="javascript:void(endThis())">end</a>
	<ds:submit-event value="getText('Zapisz')" name="'doSave'"/>
	 -->
	 <br/>
	 	<input type="button" value="Zapisz" class="btn" onclick="endThis()"/>
</form>
--%>
<script type="text/javascript" charset="iso-8859-2">

	function endThis()
	{
		selectAllOptions(getElementByName('listaWybranychTypow'));
		document.getElementById('doSave').value = 'true';
		document.forms[0].submit();
	}

</script>
<!--N koniec ops.jsp N-->