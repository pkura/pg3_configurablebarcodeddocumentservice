<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N compile-attachments.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KompilacjaPlikowSprawyUstawienia"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/compile-attachments.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedTypes);">
	<h3><ds:lang text="UstalanieKolejnosciKompilowanychDokumentowWedlugIchTypow"/></h3>

	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepneTypyDokumentow"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="WybraneTypyDokumentow"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'availableTypes'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
					list="availableTypes" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; "
					onclick="moveOptions(this.form.availableTypes, this.form.selectedTypes);" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="moveOptions(this.form.selectedTypes, this.form.availableTypes);" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'selectedTypes'" id="selectedTypes" multiple="true" size="10" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="chosenTypes"/>
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(dirty=true, moveOption('selectedTypes', -1, -1));">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(dirty=true, moveOption('selectedTypes', -1, 1));">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</table>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/>
</form>

<%--
<h1><ds:lang text="KompilacjaPlikowSprawyUstawienia"/></h1>
<hr size="1" align="left" class="highlightedText" width="77%" />
<p></p>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/compile-attachments.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedTypes);">

	<h3><ds:lang text="UstalanieKolejnosciKompilowanychDokumentowWedlugIchTypow"/></h3>

	<table>
		<tr>
			<th><ds:lang text="DostepneTypyDokumentow"/></th>
			<th></th>
			<th><ds:lang text="WybraneTypyDokumentow"/></th>
		</tr>
		<tr>
			<td valign="top">
				<ww:select name="'availableTypes'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
					list="availableTypes" />
			</td>
			<td valign="middle">
				<input type="button" value=" &gt;&gt; "
					onclick="moveOptions(this.form.availableTypes, this.form.selectedTypes);"
					class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="moveOptions(this.form.selectedTypes, this.form.availableTypes);"
					class="btn"/>
				<br/>
				
			</td>
			<td valign="top">
				<table width="100%">
					<tr>
						<td rowspan="2">
							<ww:select name="'selectedTypes'"
								id="selectedTypes"
								multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
								list="chosenTypes"/>
						</td>
						<td valign="top"><a href="javascript:void(dirty=true, moveOption('selectedTypes', -1, -1));"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a></td>
					</tr>
					<tr>
						<td valign="bottom"><a href="javascript:void(dirty=true, moveOption('selectedTypes', -1, 1));"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'"/>

</form>
--%>
<!--N koniec compile-attachments.jsp N-->