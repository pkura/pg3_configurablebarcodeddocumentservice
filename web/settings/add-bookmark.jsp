<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<h1><ds:lang text="ZarzadzanieZakladkami" /></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="Bookmarks"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="Bookmarks"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/add-bookmark.action'"/>" enctype="application/x-www-form-urlencoded" method="post">
	<ww:hidden id="fromEdit" name="'fromEdit'" />
	
<ds:available test="layout2">
	<div id="middleContainer">
</ds:available>
<table>
	<tr>
		<td><ds:lang text="Edycja"/>:</td>
		<td><ww:select id="editId" name="'id'" cssClass="'sel'" list="bookmarks" listKey="id" listValue="name" headerKey="''" headerValue="getText('select.wybierz')"/></td>
	</tr>
</table>

<table>
	<tr>
		<td><ds:lang text="Nazwa"/>:</td>
		<td><ww:textfield name="'name'" id="name"  ></ww:textfield> </td>	
	</tr>
	<tr>
		<td><ds:lang text="NazwaKodowa"/>:</td>
		<td><ww:textfield name="'cn'" id="cn"  ></ww:textfield> </td>	
	</tr>
	<tr>
		<td><ds:lang text="Tytul"/>:</td>
		<td><ww:textfield name="'title'" id="title"  ></ww:textfield> </td>	
	</tr>
	<tr>
		<td><ds:lang text="RodzajPism"/>:</td>
		<td><ww:select id="selectedType" name="'selectedType'" cssClass="'sel'" list="types" /></td>
	</tr>
	<tr>
		<td><ds:lang text="LiczbaZadanNaStronie"/>:</td>
		<td><ww:select value="taskCount" name="'taskCount'" cssClass="'sel'" list="taskCounts" /></td>
	</tr>
	<ww:if test="!'cases'.equals(selectedType) && !'order'.equals(selectedType)">
		<tr>
			<td><ds:lang text="DomyslneSortowanieWedlug"/></td>
			<td>
				<ww:select name="'sortColumn'" list="allColumns" cssClass="'sel'" />
				<ww:select name="'ascending'" list="ascendingType" cssClass="'sel'" />
			</td>					
		</tr>
	</ww:if>
</table>

<table class="tableMargin">

	<ww:if test="columnsAvailable">
		<tr>
			<th>
				<ds:lang text="DostepneKolumy"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="KolumyNaLiscieZadan"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'availableColumns'" multiple="true" size="10" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="availableColumns"/>
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " name="moveRightButton"
					onclick="if (moveOptions(this.form.availableColumns, this.form.selectedColumns) > 0) dirty=true;" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " name="moveLeftButton"
					onclick="if (moveOptions(this.form.selectedColumns, this.form.availableColumns) > 0) dirty=true;" class="btn"/>
			</td>
			<td class="alignTop" >
				<ww:select name="'selectedColumns'" id="selectedColumns" multiple="true" size="10"
					cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="selectedMapColumns" />
			</td>
			<td class="alignMiddle" >
				<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, -1));">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(dirty=true, moveOption('selectedColumns', -1, 1));">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</ww:if>
		
		<ww:if test="filtersAvailable">
			<tr>
				<td><ds:lang text="Filtry"/>:</td>
			</tr>
			<tr style="display: none">
				<td>
					<ww:select name="'selectedFilteredColumnsReadOnly'" id="selectedFilteredColumnsReadOnly" cssClass="'multi_sel original'" cssStyle="'width: 300px;'" list="filterColumns"/>
				</td>
				<td colspan="2" >
					<ww:textfield name="'filterValueReadOnly'" id="filterValueReadOnly" cssClass="'txt'" cssStyle="'width: 300px;'" />
				</td>
			</tr>
			
			<ww:iterator value="selectedFilteredColumns" status="rowstatus" >
				<tr class="selectedFilteredColumnsContainer">
					<td class="selectedFilteredColumns_<ww:property value="#rowstatus.index" />" >
						<ww:select name="'selectedFilteredColumns'" value="selectedFilteredColumns[#rowstatus.index]" list="filterColumns" 
							listKey="key" listValue="value" cssClass="'sel'" cssStyle="'width: 300px;'" />
					</td>
					<td colspan="2">
						<script type="text/javascript">
							$j(document).ready(function() {
								$j('td.selectedFilteredColumns_<ww:property value="#rowstatus.index" /> select')
									.val('<ww:property value="selectedFilteredColumns[#rowstatus.index]" />');
							});
						</script>
						<ww:textfield name="'filterValue'" value="filterValue[#rowstatus.index]" cssClass="'txt'" cssStyle="'width: 300px;'" ></ww:textfield>
						<input type="button" value="dodaj" class="btn" name="addFilter" onclick="addFilterRow()" />
						<input type="button" value="usu�" class="btn" name="removeFilter" onclick="removeFilterRow(this)" />
					</td>
				</tr>
			</ww:iterator>
		
			<tr class="selectedFilteredColumnsContainer"> 
				<td>
					<select name="selectedFilteredColumns" class="multi_sel" style="width: 300px;" ></select>
				</td>
				<td colspan="3">
					<input type="text" name="filterValue" class="txt" style="width: 300px;" />
					<input type="button" value="dodaj" class="btn" name="addFilter" onclick="addFilterRow()" />
					<input type="button" value="usu�" class="btn" name="removeFilter" onclick="removeFilterRow(this)" />
				</td>
			</tr>
		</ww:if>
		
		<ww:if test="labelsAvailable">
			<ds:available test="labels">
				<tr>
					<td colspan="3"><ds:lang text="Etykiety"/>:</td>
				</tr>
				<tr style="display: none">
					<td>
						<ww:select id="selectedLabelsReadOnly" name="'selectedLabelsReadOnly'" list="availableLabels" listKey="id" listValue="aliasName" cssClass="'sel'" />
					</td>
				</tr>
				
				<!--  iterator start taga //-->
				<tr class="selectedLabelsContainer" >
					<ww:iterator value="selectedLabels" status="rowstatus" >
					<tr>
						<td colspan="3">
							<ww:select name="'selectedLabels'" value="selectedLabels[#rowstatus.index]" list="availableLabels" listKey="id" listValue="aliasName" cssClass="'sel'" />
							<input type="button" class="btn" value="usu�" onclick="removeLabelRow(this)" />
						</td>
					</tr>
					</ww:iterator>
				</tr>
				
				<tr>
					<td colspan="3"><input type="button" id="addLabelBtn" value="Dodaj etykiet�" class="btn" /></td>
				</tr>
			</ds:available>
		</ww:if>
		
		<tr>
			<td colspan="4" >
				<ww:if test="id != null">
					<ds:submit-event value="'Zapisz'" name="'doSave'" ></ds:submit-event>
					<ds:submit-event value="'Usun'" name="'doRemove'" ></ds:submit-event>
				</ww:if><ww:else>
					<ds:submit-event value="'Dodaj'" name="'doAdd'" ></ds:submit-event>
					<ww:if test="adminUser==true">
						<ds:submit-event value="'Dodaj systemow�'" name="'doAddSystemBookmark'" ></ds:submit-event>
					</ww:if>
					<%--<ds:submit-event value="'Czy��'" name="'doClear'" ></ds:submit-event>--%>
				</ww:else>
			</td>
		</tr>
	</table>

<ds:available test="layout2">
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div>
</ds:available>

</form>
<script type="text/javascript">
	// przy edycji zaznaczenie wszystkich wybranych kolumn
	$j('#form').bind('submit.selectAllOptions', function() {
		$j('#selectedColumns option').attr('selected', 'selected')
	});
	
	// przy wyborze typu zak�adki (in, out, itp.) submit
	$j('#selectedType').bind('change.reloadPage', function() {
		$j('form')[0].submit();
	});
	
	// przy wyborze zakladi do edycji submit
	$j('#editId').bind('change.reloadPage', function() {
		$j('#fromEdit').val('true');
		$j('form')[0].submit();
	});

	function updateFiltersList(jqSelect, jqAvailFilter) {
		trace('updateFiltersList');
		$j('select[name=selectedFilteredColumns]').html('').parent('td').parent('tr').show();
		$j('select[name=selectedFilteredColumns]').append($j('#selectedFilteredColumnsReadOnly').html());
	}

	function addFilterRow() {
		var str  = '<tr class="selectedFilteredColumnsContainer">';
			str += $j('tr.selectedFilteredColumnsContainer:last').html();
			str += '</tr>';
		$j('tr.selectedFilteredColumnsContainer:last').after(str);
	}

	function removeFilterRow(domObj) {
		if($j('select[name=selectedFilteredColumns]').length > 1) {
			$j(domObj).parent('td').parent('tr').remove();
		} else {
			$j('input[name=filterValue]').val('');
		}
	}

	// etykiety
	$j('#addLabelBtn').bind('click.addLabel', function() {
		var str  = '<tr class="selectedLabelsContainer"><td colspan="3">';
			str += '<select class="sel" name="selectedLabels">' + $j('#selectedLabelsReadOnly').html() + '</select>';
			str += '<input type="button" class="btn" value="usu�" onclick="removeLabelRow(this)" />';
			str += '</td></tr>';

		$j('tr.selectedLabelsContainer:last').after(str);
	});

	function removeLabelRow(domObj) {
		$j(domObj).parent('td').parent('tr').remove();
	}

	$j(document).ready(function() {
		if($j('input[name=filterValue]').val() == '')
			updateFiltersList($j('#selectedFilteredColumnsReadOnly'), $j('#selectedColumns'));
	});

</script>