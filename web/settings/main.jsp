<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="userSettings"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:available test="menu.left.userSettings.flagi">
<p>
	<a href="<ww:url value="'/settings/user-flags.action'"/>">
		<ds:lang text="flags"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="flagsSetDocuments"/>
</p>
</ds:available>

<ds:available test="labels">
	<p>
		<a href="<ww:url value="'/settings/view-labels.action'"/>">
			<ds:lang text="labels"/>
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		<br/>
		<ds:lang text="userLabels"/>
	</p>
</ds:available>

<ds:available test="menu.left.kancelaria.listazadan">
<ds:modules test="office">
	<p>
		<a href="<ww:url value="'/settings/task-list.action'"/>">
			<ds:lang text="taskList"/>
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		<br/>
		<ds:lang text="visibleColumnsOnTaskList"/>
	</p>
</ds:modules>
</ds:available>

<ds:available test="bookmarks.on">
<p>
	<a href="<ww:url value="'/settings/bookmarks-columns.action'"/>">
		<ds:lang text="ZakladkiKolumny"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="visibleColumnsOnTaskList"/>
</p>
<p>
	<a href="<ww:url value="'/settings/add-bookmark.action'"/>">
		<ds:lang text="Zakladki"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="bookmarksSettings"/>
</p>
</ds:available>

<ds:available test="menu.left.userSettings.skanowanie">
<p>
	<a href="<ww:url value="'/settings/scanner.action'"/>">
		<ds:lang text="scanner"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="serviceActivityOfScanne"/>
</p>
</ds:available>

<ds:available test="userOptions.taskListMailer">
<p>
	<a href="<ww:url value="'/settings/taskListMailer.action'"/>">
		<ds:lang text="taskListMailer"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:extras test="business">
<ds:modules test="certificate">
	<p>
		<a href="<ww:url value="'/settings/ops.action'"/>">
			<ds:lang text="Ops"/>
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		<br/>
		<ds:lang text="WyborTypowDokumentowDlaOPS"/>
	</p>
</ds:modules>
</ds:extras>

<ds:available test="ARCHIWUM_DOSTEP">
<p>
	<a href="<ww:url value="'/settings/search-documents.action'"/>">
		<ds:lang text="searchingDocumentInArchive"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="visibleColumnsOnSearchListDocument"/>
</p>
</ds:available>

<ds:available test="menu.left.userSettings.zbiorczadekretacja">
<p>
	<a href="<ww:url value="'/settings/user-collective-assignment.action'"/>">
		<ds:lang text="multiDekretacja"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="multiDekretacja"/> 
</p>
</ds:available>

<p>
	<a href="<ww:url value="'/settings/other.action'"/>">
		<ds:lang text="otherSettings"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="otherSettings"/>
</p>

<ds:available test="szyfrowanieZalacznikow">
	<p>
	<a href="<ww:url value="'/settings/encryption-authorization.action'"/>">
		<ds:lang text="encryptionAuthorization"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="encryptionAuthorizationDesc"/>
</p>
</ds:available>

<ds:available test="mojeZastepstwa">
	<p>
	<a href="<ww:url value="'/office/users/user-edit.do?mySubstitude=true'"/>">
		<ds:lang text="mojeZastepstwa"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	<ds:lang text="mojeZastepstwaDesc"/>
</p>
</ds:available>

<%--
<h1><ds:lang text="userSettings"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<p><a href="<ww:url value="'/settings/user-flags.action'"/>"><ds:lang text="flags"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="flagsSetDocuments"/>
</p>
<ds:available test="labels">
<p><a href="<ww:url value="'/settings/view-labels.action'"/>"><ds:lang text="labels"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="userLabels"/>
</p>
</ds:available>

<ds:modules test="office">
	<p><a href="<ww:url value="'/settings/task-list.action'"/>"><ds:lang text="taskList"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
		<ds:lang text="visibleColumnsOnTaskList"/>
	</p>
</ds:modules>

<p><a href="<ww:url value="'/settings/scanner.action'"/>"><ds:lang text="scanner"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="serviceActivityOfScanne"/>
</p>

<p><a href="<ww:url value="'/settings/taskListMailer.action'"/>"><ds:lang text="taskListMailer"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>

<ds:modules test="certificate">
<p><a href="<ww:url value="'/settings/ops.action'"/>"><ds:lang text="Ops"/> <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
<br><ds:lang text="WyborTypowDokumentowDlaOPS"/></p>
</ds:modules>

<p><a href="<ww:url value="'/settings/search-documents.action'"/>"><ds:lang text="searchingDocumentInArchive"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="visibleColumnsOnSearchListDocument"/>
</p>

<p><a href="<ww:url value="'/settings/user-collective-assignment.action'"/>"><ds:lang text="multiDekretacja"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="multiDekretacja"/> 
</p>

<ds:additions test="calendar">
	<ds:subject admin="true">
		<p><a href="<ww:url value="'/settings/default-timeusers.action'"/>"><ds:lang text="ListaDoPrzypisywaniaTerminowDomyslna"/> <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
			<ds:lang text="UstawieniaListyUzytkownikowWidocznejPodczasPrzypisywaniaTerminuUstawieniaDomyslne"/>
		</p>
	</ds:subject>

	<p><a href="<ww:url value="'/settings/user-timeusers.action'"/>"><ds:lang text="ListaDoPrzypisywaniaTerminowUzytkownika"/> <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
		<ds:lang text="UstawieniaListyUzytkownikowWidocznejPodczasPrzypisywaniaTerminuUstawieniaUzytkownika"/>
	</p>
</ds:additions>

<p><a href="<ww:url value="'/settings/other.action'"/>"><ds:lang text="otherSettings"/><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	<ds:lang text="otherSettings"/>
</p>
--%>
<!--N koniec main.jsp N-->