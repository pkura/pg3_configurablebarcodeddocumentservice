<%--T
	Przeróbka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search-documents.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="WyszukiwanieDokumentow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ds:xmlLink path="UsatwieniaWyszukiwarka"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="UsatwieniaWyszukiwarka"/>
	</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/search-documents.action'"/>" method="post" onsubmit="selectAll(this.selectedColumns);">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<ds:extras test="business">
		<ww:select name="'type'" list="types" listKey="key" listValue="value" cssClass="'sel'" id="type"/>
		<input type="button" value="<ds:lang text="Pokaz"/>" onclick="change()" class="btn"/>

		<hr class="shortLine"/>
		
		<h3><ww:property value="types[type]"/></h3>
	</ds:extras>

	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepneKolumy"/>
			</th>
			<th></th>
			<th colspan="2">
				<ds:lang text="KolumyNaLiscieWynikow"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'availableColumns'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="availableColumns" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; "
					onclick="move(this.form.availableColumns, this.form.selectedColumns);" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="move(this.form.selectedColumns, this.form.availableColumns);" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'selectedColumns'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="userColumns"/>
			</td>	
			<td class="alignMiddle">
				<a href="javascript:void(up(document.getElementById('form').selectedColumns));">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(down(document.getElementById('form').selectedColumns));">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/>
	<ds:submit-event value="getText('Powrot')" name="'doCancel'"/>
	<ds:submit-event value="getText('PrzywrocKolumny')" name="'doReset'"/>
	
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<h1><ds:lang text="WyszukiwanieDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="UsatwieniaWyszukiwarka"/>
</p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/search-documents.action'"/>" method="post" onsubmit="selectAll(this.selectedColumns);">
<ds:extras test="business">
<table>
	<tr>
		<td>
			<ww:select name="'type'" list="types" listKey="key" listValue="value"
				cssClass="'sel'" id="type"/>
		</td>
		<td>
			<input type="button" value="<ds:lang text="Pokaz"/>" onclick="change()" class="btn"/>
		</td>
	</tr>
	<tr>
		<td colspan="2"><hr/></td>
	</tr>
</table>

<h3><ww:property value="types[type]"/></h3>
</ds:extras>

<table>
	<tr>
		<th><ds:lang text="DostepneKolumy"/></th>
		<th></th>
		<th><ds:lang text="KolumyNaLiscieWynikow"/></th>
	</tr>
	<tr>
		<td valign="top">
			<ww:select name="'availableColumns'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
				list="availableColumns" />
		</td>
		<td valign="middle">
			<input type="button" value=" &gt;&gt; "
				onclick="move(this.form.availableColumns, this.form.selectedColumns);"
				class="btn"/>
			<br/>
			<input type="button" value=" &lt;&lt; "
				onclick="move(this.form.selectedColumns, this.form.availableColumns);"
				class="btn"/>
		</td>
		<td valign="top">
			<table width="100%">
				<tr>
					<td rowspan="2">
						<ww:select name="'selectedColumns'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
							list="userColumns"/>
					</td>
					<td valign="top"><a href="javascript:void(up(document.getElementById('form').selectedColumns));"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a></td>
				</tr>
				<tr>
					<td valign="bottom"><a href="javascript:void(down(document.getElementById('form').selectedColumns));"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<ds:submit-event value="getText('Zapisz')" name="'doUpdate'"/>
<!--<ds:button-redirect href="'/settings/merge-columns.action'" value="getText('Scal')"></ds:button-redirect>-->
<ds:submit-event value="getText('Powrot')" name="'doCancel'"/>
<ds:submit-event value="getText('PrzywrocKolumny')" name="'doReset'"/>

</form>
--%>
<script type="text/javascript">
	function change()
	{
		// przeładowujemy stronę tylko, jeśli wybrano inne kolumny od tych aktualnie wyświetlonych
		if (document.getElementById('type').value == '<ww:property value="type"/>')
			return false;
		document.getElementById('form').submit();
	}
	
	function move(select1, select2)
	{
		var i=0;
		while (i < select1.options.length)
		{
			if (select1.options[i].selected)
			{
				var opt = select1.options[i];
				select2.options[select2.options.length] = new Option(opt.text, opt.value);
				select1.options[i] = null;
			}
			else
			{
				i++;
			}
		}
	}
	
	function up(select)
	{
		var si = select.options.selectedIndex;
		if (si <= 0)
			return;
		var tmp = select.options[si-1];
		select.options[si-1] = new Option(select.options[si].text, select.options[si].value);
		select.options[si] = new Option(tmp.text, tmp.value);
		select.options.selectedIndex = si-1;
	}
	
	function down(select)
	{
		var si = select.options.selectedIndex;
		if (si >= select.options.length-1 || si < 0)
			return;
		var tmp = select.options[si+1];
		select.options[si+1] = new Option(select.options[si].text, select.options[si].value);
		select.options[si] = new Option(tmp.text, tmp.value);
		select.options.selectedIndex = si+1;
	}
	
	function selectAll(select)
	{
		for (var i=0; i < select.options.length; i++)
			select.options[i].selected = true;
	}
</script>
<!--N koniec search-documents.jsp N-->