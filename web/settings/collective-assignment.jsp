<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N collective-assignment.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZbiorczaDekretacja"/> <ww:property value="subtitle"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmianChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmian,ChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form">
	<ww:hidden name="'divisionGuid'" id="divisionGuid" />
	<ww:hidden name="'doUpdate'" id="doUpdate"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<h3><ds:lang text="AktualnaListaDekretacji"/></h3>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ww:select name="'targetsSelectValues'" list="targetsSelect" cssClass="'sel'" id="targetsSelect" multiple="true" size="10"/>
			</td>
			<td class="alignMiddle">
				<a href="javascript:void(moveOption('targetsSelect', -1, -1))">
					<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<a href="javascript:void(moveOption('targetsSelect', -1, 1))">
					<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
			</td>
		</tr>
	</table>
	
	<a name="deleteMarked" href="javascript:void(removeOptions('targetsSelect'))"><ds:lang text="usuZaznaczone"/></a>
	<br/>
	
	<input type="button" class="btn" value="<ds:lang text="posortuj"/>" onclick="sort()"/>
	
	<br/>
	<br/>
	<ww:property value="treeHtml" escape="false"/>
	
	<h3><ds:lang text="DodajDekretacje"/></h3>
	<a href="javascript:void(addUser('null', '<ww:property value="targetDivision.guid"/>', '', '<ww:property value="targetDivision.name"/>'))">
		<ds:lang text="Dzial"/></a>
	
	<br/>
	<br/>
	<ww:if test="users.length > 0">
		<ww:iterator value="users" id="status">
			<a name="<ww:property value="top.name"/>" href="javascript:void(addUser('<ww:property value="top.name"/>', '<ww:property value="targetDivision.guid"/>', '<ww:property value="top.asLastnameFirstname()"/>', '<ww:property value="targetDivision.name"/>'))"><ww:property value="asFirstnameLastname()"/></a>
			<br/>
		</ww:iterator>
	</ww:if>

	<input type="button" class="btn saveBtn" name="doSave" value="<ds:lang text="Zapisz"/>" onclick="submitForm()"/>
	
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<h1><ds:lang text="ZbiorczaDekretacja"/> <ww:property value="subtitle"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ww:iterator value="tabs" status="status" >
	<a onclick="if (dirty && !confirm('<ds:lang text="NieZapisanoZmianChceszPrzejscNaInnaZakladke"/>?')) return false" href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form">
	<ww:hidden name="'divisionGuid'" id="divisionGuid" />
	<ww:hidden name="'doUpdate'" id="doUpdate"/>
<%--
	<ww:iterator value="targets">
		<ww:hidden name="'targets'" value="top"/>
	</ww:iterator>
-->

	<h3><ds:lang text="AktualnaListaDekretacji"/></h3>
	<table>
		<tr>
			<td rowspan="2">
				<ww:select name="'targetsSelectValues'"
						   list="targetsSelect" cssClass="'sel'"
					id="targetsSelect"
					multiple="true" size="10"/>
			</td>
			<td valign="top"><a href="javascript:void(moveOption('targetsSelect', -1, -1))"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a></td>
		</tr>
		<tr>
			<td valign="bottom"><a href="javascript:void(moveOption('targetsSelect', -1, 1))"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a></td>
		</tr>
		<tr>
			<td colspan="2"><a name="deleteMarked" href="javascript:void(removeOptions('targetsSelect'))"><ds:lang text="usuZaznaczone"/></a></td>
		</tr>
		<tr>
			<td align="right"><input type="button" class="btn" value="<ds:lang text="posortuj"/>" onclick="sort()"/></td>
		</tr>
	</table>
	

	<br/>
	
	<table width="100%">
		<tr>
			<td valign="top">
				<ww:property value="treeHtml" escape="false"/>
				<h3><ds:lang text="DodajDekretacje"/></h3>
				<a href="javascript:void(addUser('null', '<ww:property value="targetDivision.guid"/>', '', '<ww:property value="targetDivision.name"/>'))"><ds:lang text="Dzial"/></a>
					<br/>
				<ww:if test="users.length > 0">
					<br/>
					<ww:iterator value="users" id="status">
						<a name="<ww:property value="top.name"/>" href="javascript:void(addUser('<ww:property value="top.name"/>', '<ww:property value="targetDivision.guid"/>', '<ww:property value="top.asLastnameFirstname()"/>', '<ww:property value="targetDivision.name"/>'))"><ww:property value="asFirstnameLastname()"/></a>
						<br/>
					</ww:iterator>
				</ww:if>
			</td>
		</tr>
	</table>

	<input type="button" class="btn" name="doSave" value="<ds:lang text="Zapisz"/>" onclick="submitForm()"/>
	
</form>
--%>
<script type="text/javascript">
	/**
		Wysy�a formularz bez �adnego zdarzenia, w celu
		od�wie�enia drzewa dzia��w.
	*/
	function post(guid)
	{
		E('divisionGuid').value = guid;
		selectAllOptions('targetsSelect');
		E('form').submit();
	}

	function addUser(username, guid, userDescription, guidDescription)
	{
		var value = username+';'+guid;
		var sel = E('targetsSelect');
		for (var i=0; i < sel.options.length; i++)
		{
			var ss = sel.options[i].value.split(';');
			if (username == ss[0] && guid == ss[1])
			{
				alert('<ds:lang text="UzytkownikZnajdujeSieJuzNaLiscie"/>');
				return false;
			}
		}
		var text = userDescription + ' (' + guidDescription + ')';
		sel.options[sel.options.length] = new Option(text, value);
	}

	function sort()
	{
		var sel = E('targetsSelect');

		for(var i=0; i<sel.options.length;i++)
			for(var j=i; j<sel.options.length;j++)
			{
				var tab1 = sel.options[i].text.split(' ');
				var tab2 = sel.options[j].text.split(' ');
				var str1 = new String(tab1);
				var str2 = new String(tab2);
				if(toPL(str1)>toPL(str2))
				{
					var tmp = new Option(sel.options[i].text, sel.options[i].value);
					sel.options[i] = new Option(sel.options[j].text, sel.options[j].value);
					sel.options[j] = tmp
				}
			}
	}
	function toPL(x){
		return x.replace(/[����񿼦Ư��]/g,function(s){
		return(s=='�'?'a':s=='�'?'e':s=='�'?'s':s=='�'?'c':
		s=='�'?'o':s=='�'?'l':s=='�'?'n':s=='�'?'z':s=='�'?'z':s=='�'?'S':
		s=='�'?'C':s=='�'?'Z':s=='�'?'Z':s=='�'?'Z':s=='�'?'L':s)})
	} 
	function submitForm()
	{
		var form = E('form');
		disableFormSubmits(form);
		var sel = E('targetsSelect');
		for (var i=0; i < sel.options.length; i++)
		{
			var opt = sel.options[i].value;
			var hid = document.createElement('INPUT');
			hid.type = 'HIDDEN';
			hid.name = 'targets';
			hid.value = opt + ';' + (i+1);
			form.appendChild(hid);
		}
		E('doUpdate').value = 'true';
		selectAllOptions(sel);
		form.submit();
	}
</script>
<!--N koniec collective-assignment.jsp N-->