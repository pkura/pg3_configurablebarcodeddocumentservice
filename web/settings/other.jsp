<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N other.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="InneUstawienia"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>>
		<ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/other.action'"/>" method="post" id="form" onsubmit="disableFormSubmits(this);">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table class="tableMargin">

<%-- z kodu wynika �e ta zmienna nie jest nigdzie u�ywana
		<tr>
			<td>
				<ds:lang text="CzestotliwoscOtrzymywaniaPowiadomienOobserwowanychPismach"/>:
			</td>
			<td>
				<ww:textfield name="'deadlineNotificationDaysInterval'"/>
			</td>
		</tr>
--%>
		<ds:extras test="business">  
		 
			<tr>
				<td>
					<ds:lang text="WyswietlanieZalacznikaWrazZarchiwizacjaIuwagami"/>:
				</td>
				<td>
					<ww:checkbox name="'viewserverWithData'" fieldValue="true" value="viewserverWithData"/>
				</td>
			</tr>
			<ds:available test="tiffyApplet">
				<tr>
					<td>
						<ds:lang text="WyswietlZalacznikZuwagami"/>:
					</td>
					<td>
						<ww:checkbox name="'viewserverTiffyApplet'" fieldValue="true" value="viewserverTiffyApplet"/>
					</td>
				</tr>
			</ds:available>
			<tr>
				<td>
					<ds:lang text="KolumnaZarchiwizacjaZprawejStrony"/>:
				</td>
				<td>
					<ww:checkbox name="'viewserverDataSide'" fieldValue="true" value="viewserverDataSide"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="SzerokoscKolumnyZarchiwizacjaWpikselach"/>:
				</td>
				<td>
					<ww:textfield name="'viewserverDataSize'" maxlength="3" size="3"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="WyswietlanieCalejStronyZalacznika"/>:
				</td>
				<td>
					<ww:checkbox name="'viewserverWholePage'" fieldValue="true" value="viewserverWholePage"/>
				</td>
			</tr>
		</ds:extras>
		
	<ww:if test="activity">
		<tr>
			<td>
				<ds:lang text="OkreslanieCeluPodczasZbiorczejDekretacji"/>:
			</td>
			<td>
				<ww:checkbox name="'multiAssignmentObjective'" fieldValue="true" value="multiAssignmentObjective"/>
			</td>
		</tr>
	</ww:if>
	<ds:available test="select.documentmain.type">
		<tr>
			<td>
				<ds:lang text="UzywajSkroconejFormatkiPrzyjeciaPismaPrzychodzacego"/>:
			</td>
			<td>
				<ww:checkbox name="'simpledocumentmainin'" fieldValue="true" value="simpledocumentmainin"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="UzywajSkroconejFormatkiPrzyjeciaPismaWychodzacego"/>:
			</td>
			<td>
				<ww:checkbox name="'simpledocumentmainout'" fieldValue="true" value="simpledocumentmainout"/>
			</td>
		</tr>
	</ds:available>
	
	<ds:available test="tasklist.multiDepartmentEnroll">
		<tr>
			<td>
				<ds:lang text="UmozliwiajZbiorczePrzypisanieDoDziennikaWydzialu"/>:
			</td>
			<td>
				<ww:checkbox name="'multiDepartmentEnroll'" fieldValue="true" value="multiDepartmentEnroll"/>
			</td>
		</tr>
	</ds:available>
	<ds:available test="rwa">
		<tr>
			<td>
				<ds:lang text="UmozliwiajAktualizacjeRwaZPliku"/>:
			</td>
			<td>
				<ww:checkbox name="'rwaUpdateFromFile'" fieldValue="true" value="rwaUpdateFromFile"/>
			</td>
		</tr>
	</ds:available>
 	<ds:available test="ARCHIWUM_DOSTEP">
		<ds:extras test="business">
			<tr>
				<td>
					<ds:lang text="PobierzZalacznikPismaZUwagamiNaOddzielnejStronie"/>:
				</td>
				<td>
					<ww:checkbox name="'remarksOnSeperatePage'" fieldValue="true" value="remarksOnSeperatePage"/>
				</td>
			</tr>
		</ds:extras>
	</ds:available>
		<ww:if test="layoutsAvailable">
			<tr>
				<td>
					<ds:lang text="SchematKolorystyki"/>
				</td>
				<td>
					<ww:select name="'layoutName'" list="layouts" cssClass="'sel'"/>
				</td>
			</tr>
		</ww:if>
		
		<tr>
			<td>
				<ds:lang text="StronaStartowa"/>:
			</td>
			<td>
				<ww:select name="'name'" list="pages" cssClass="'sel'"/>
			</td>
		</tr>
	
		<tr>
			<td>
				<ds:lang text="WybierzJezyk"/>:
			</td>
			<td>
				<ww:select name="'lang'" value="lang" list="local" cssClass="'sel'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WyborSlownika"/>:
			</td>
			<td>
				<ww:select name="'guid'" list="guids" value="guid" cssClass="'sel'" listKey="key" listValue="value"/>
			</td>
		</tr>
		<ds:available test="teczki">
			<tr>
				<td>
					<ds:lang text="LiczbaSprawNaStronie"/>:
				</td>
				<td>
					<ww:select value="caseCount" name="'caseCount'" cssClass="'sel'" list="caseCounts" />
				</td>
			</tr>
		</ds:available>
		<ds:available test="menu.left.admin.barcode.printer">
        <tr>
            <td>
                <ds:lang text="DrukarkaEtykiet" />:
            </td>
            <td>
                <ww:select name="'selectedPrinter'" list="printers" headerKey="''"
                           headerValue="getText('select.wybierz')" value="selectedPrinter" cssClass="'sel'"/>
            </td>
        </tr>
		</ds:available>
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn saveBtn'" />
		 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<h1><ds:lang text="InneUstawienia"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
<form id="form" action="<ww:url value="'/settings/other.action'"/>" method="post" id="form"
	onsubmit="disableFormSubmits(this);">

<table>
	<tr>
		<td><ds:lang text="CzestotliwoscOtrzymywaniaPowiadomienOobserwowanychPismach"/>:</td>
		<td><ww:textfield name="'deadlineNotificationDaysInterval'" /></td>
	</tr>
	<ds:extras test="business">   
		<tr>
			<td><ds:lang text="WyswietlanieZalacznikaWrazZarchiwizacjaIuwagami"/>:</td>
			<td><ww:checkbox name="'viewserverWithData'" fieldValue="true" value="viewserverWithData"/></td>
		</tr>
		<tr>
			<td><ds:lang text="KolumnaZarchiwizacjaZprawejStrony"/>:</td>
			<td><ww:checkbox name="'viewserverDataSide'" fieldValue="true" value="viewserverDataSide"/></td>
		</tr>
		<tr>
			<td><ds:lang text="SzerokoscKolumnyZarchiwizacjaWpikselach"/>:</td>
			<td><ww:textfield name="'viewserverDataSize'" maxlength="3" size="3"/></td>
		</tr>
		<tr>
			<td><ds:lang text="WyswietlanieCalejStronyZalacznika"/>:</td>
			<td><ww:checkbox name="'viewserverWholePage'" fieldValue="true" value="viewserverWholePage"/></td>
		</tr>
	</ds:extras>
	<tr>
		<td><ds:lang text="OkreslanieCeluPodczasZbiorczejDekretacji"/>:</td>
		<td><ww:checkbox name="'multiAssignmentObjective'" fieldValue="true" value="multiAssignmentObjective"/></td>
	</tr>
	<tr>
		<td><ds:lang text="UmozliwiajZbiorczePrzypisanieDoDziennikaWydzialu"/>:</td>
		<td><ww:checkbox name="'multiDepartmentEnroll'" fieldValue="true" value="multiDepartmentEnroll"/></td>
	</tr>
	<tr>
		<td><ds:lang text="UmozliwiajAktualizacjeRwaZPliku"/>:</td>
		<td><ww:checkbox name="'rwaUpdateFromFile'" fieldValue="true" value="rwaUpdateFromFile"/></td>
	</tr>
	<ds:extras test="business">
	<tr>
		<td><ds:lang text="PobierzZalacznikPismaZUwagamiNaOddzielnejStronie"/>:</td>
		<td><ww:checkbox name="'remarksOnSeperatePage'" fieldValue="true" value="remarksOnSeperatePage"/></td>
	</tr>
	</ds:extras>
	<ww:if test="layoutsAvailable">
		<tr>
			<td><ds:lang text="SchematKolorystyki"/></td>
			<td><ww:select name="'layoutName'" list="layouts" cssClass="'sel'"/></td>
		</tr>
	</ww:if>
	<tr>
		<td><ds:lang text="StronaStartowa"/>:</td>
		<td><ww:select name="'name'" list="pages" cssClass="'sel'"/></td>
	</tr>

	<tr>
		<td><ds:lang text="WybierzJezyk"/>:</td>
		<td><ww:select name="'lang'" value="lang" list="local" cssClass="'sel'"/></td>
	</tr>
	<tr>
		<td><ds:lang text="WyborSlownika"/>:</td>
		<td><ww:select name="'guid'" list="guids" value="guid" cssClass="'sel'"
					listKey="key" listValue="value"/></td>
	</tr>
	<tr>
		<td></td>
		<td><ds:submit-event value="getText('Zapisz')" name="'doSave'" /></td>
	</tr>
</table>

</form>
--%>
<!--N koniec other.jsp N-->