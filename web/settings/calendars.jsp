<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<!--N calendar.jsp N-->

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="UstawieniaKalendarza"/></h1>

<ds:available test="!layout2">
<ds:xmlLink path="Calendars"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="Calendars"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:form action="'calendars.action'" namespace="'/settings'" method="'post'">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
<table class="search mediumTable bottomSpacer floatLeft" id="calendars">
	<caption><ds:lang text="MojeKalendarzeZdalne"/></caption>
	<thead>
	<tr>
		<th>W�a�ciciel</th>
		<th>Nazwa</th>
		<th></th>
		<th></th>

	</tr>
	</thead>
	<tbody>
	<ww:iterator value="calendars">
		<ww:set name="result"/>
		<tr calendarId="<ww:property value="#result.calendarId"/>" class="calendar">
			<td>
				<ww:property value="calendar.getTimeResource().getWholeName()"/>
			</td>
			<td>
				<ww:property value="calendar.getName()"/>
			</td>
			<td>
				<ww:if test="show">
					<a href="<ww:url value="'/settings/calendars.action'">
						<ww:param name="'doUpdate'" value="true"/>
						<ww:param name="'calendarId'" value="#result.calendarId"/>
						<ww:param name="'username'" value="#result.username"/>
						<ww:param name="'resourceType'" value="#result.resourceType"/>
						<ww:param name="'show'" value="'false'"/></ww:url>"
						onclick="if (!confirm('Na pewno ukry� wpis')) return false;"><ds:lang text="Ukryj"/>
					</a>
				</ww:if>
				<ww:else>
					<a href="<ww:url value="'/settings/calendars.action'">
						<ww:param name="'doUpdate'" value="true"/>
						<ww:param name="'calendarId'" value="#result.calendarId"/>
						<ww:param name="'username'" value="#result.username"/>
						<ww:param name="'resourceType'" value="#result.resourceType"/>
						<ww:param name="'show'" value="'true'"/></ww:url>"
						onclick="if (!confirm('Na pewno pokaza� wpis')) return false;"><ds:lang text="Pokaz"/>
					</a>
				</ww:else>
			</td>
			<td>
				<ww:if test="!principalName.equals(calendar.getTimeResource().getCn())">
				
				<a href="<ww:url value="'/settings/calendars.action'">
					<ww:param name="'doDeleteMy'" value="true"/>
					<ww:param name="'calendarId'" value="#result.calendarId"/>
					<ww:param name="'username'" value="#result.username"/>
					<ww:param name="'resourceType'" value="#result.resourceType"/></ww:url>"
					onclick="if (!confirm('Na pewno usun�� wpis')) return false;"><ds:lang text="AnulujSubskrypcje"/>
				</a>
				</ww:if>
			</td>
			<td></td>
		</tr>
	</ww:iterator>
	<tr>
		<td><ds:event name="'doUpdatePosn'" value="'Zachowaj kolejno��'" cssClass="'btn'"/></td> 
	</tr>
</tbody>
</table>

<div id="hiddens">

</div>

<script type="text/javascript">
var jqHiddens = $j('#hiddens');
var jqCalendars = $j('#calendars');

//Return a helper with preserved width of cells
var fixHelper = function(e, ui) {
	ui.children().each(function() {
		$j(this).width($j(this).width());
		$j(this).height($j(this).height());
	});
	return ui;
};

$j("#calendars tbody").sortable({
	placeholder: 'placeholder',
	helper: fixHelper,
	tolerance: 'pointer',
	cursor: 'move',
	start: function(event, ui) {
		ui.item.addClass('highlight');
	},
	stop: function(event, ui) {
		ui.item.toggleClass('highlight', 500);
		jqHiddens.children('input:hidden').remove();
		jqCalendars.children('tbody').children('tr.calendar').each(function() {
			var str  = '<input type="hidden" name="calendarsIds" value="';
				str += $j(this).attr('calendarId'); 
				str += '" />';
			jqHiddens.append(str);
		});
	}
}).disableSelection();

</script>

<table class="search mediumTable bottomSpacer floatLeft marginLeft">
<caption>Moje kalendarze</caption>
<thead>
	<tr>
		<th><ww:select name="'myCalendarId'" list="myCalendars" listKey="id" listValue="name" cssClass="'sel'" onchange="'document.forms[0].submit();'"/></th>
		<th><ds:lang text="UdostepnionyDla"/></th>
		<th></th>
	</tr>
</thead>
<tbody>
	<ww:iterator value="shareCalendars">
		<ww:set name="result2"/>
		<tr>
			<td><ww:if test="permissionType == 1"><ds:lang text="DoEdycji"/></ww:if>
				<ww:elseif test="permissionType == 2"><ds:lang text="TylkoDoOdczytu"/></ww:elseif>
				<ww:elseif test="permissionType == 3"><ds:lang text="TylkoDoOdczytuStanuWolnyZajety"/></ww:elseif>
			 </td>
			<td>
				<ww:property value="user.asFirstnameLastname()"/>
			</td>
			<td>
				<a href="<ww:url value="'/settings/calendars.action'">
					<ww:param name="'doDelete'" value="true"/>
					<ww:param name="'myCalendarId'" value="#result2.calendarId"/>
					<ww:param name="'username'" value="#result2.username"/>
					<ww:param name="'resourceType'" value="resourceType"/></ww:url>"
					onclick="if (!confirm('Na pewno usun�� wpis')) return false;"><ds:lang text="AnulujSubskrypcje"/>
				</a>
			</td>
		</tr>
	</ww:iterator>
	<tr>
		<td><ww:select name="'selectUsers'" id="selectUsers" list="users" listKey="name" listValue="asLastnameFirstname()" multiple="true" 
			cssClass="'sel'" size="'5'"/>
		</td>
		<td>
			<select name="perm" class="sel floatLeft">
				<option value="2">
					<ds:lang text="TylkoDoOdczytu"/>
				</option>
				<option value="1">
					<ds:lang text="DoEdycji"/>
				</option>
				<option value="3">
					<ds:lang text="TylkoDoOdczytuStanuWolnyZajety"/>
				</option>
			</select>

		</td>
		<td>
			<ds:event name="'doShare'" value="'Udostepnij'" cssClass="'btn'"/>
		</td>
	</tr>
	</tbody>
</table>
<ww:if test="resources.size() > 0">
<table class="search mediumTable floatLeft clearBoth">
<caption>Dodaj zasoby na swoj� list�</caption>
<tbody>
	<tr>
		<td>Zasoby:</td>
		<td><ww:select name="'selectResources'" list="resources" listKey="calendarId" listValue="name" multiple="true" 
			cssClass="'sel'" size="'4'"/></td>
		<td><ds:event name="'doAddResources'" value="'Dodaj	'" cssClass="'btn'"/></td>
		<td></td>
	</tr>
</tbody>
</table>
</ww:if>
<table class="search mediumTable floatLeft clearBoth">
<caption>Dodaj nowy kalendarz</caption>
<tbody>
	<tr>
		<td>Nazwa:</td>
		<td> <ww:textfield name="'calendarName'"/></td>
		<td><ds:event name="'doAdd'" value="'Dodaj	'" cssClass="'btn'"/></td>
		<td></td>
	</tr>
</tbody>
</table>

<div class="clearBoth"></div>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</ww:form>

<!--N koniec file-import.jsp N-->