<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<!--N resource-calendar.jsp N-->

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Kalendarz"/></h1>

<ds:available test="!layout2">
<ds:xmlLink path="Calendars"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="Calendars"/>
	</div>
</ds:available>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:form action="'resource-calendar.action'" namespace="'/settings'" method="'post'">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
<ww:hidden name="'resourceId'" value="resourceId" id="resourceId"/>
<table class="search mediumTable widePadding bottomSpacer floatLeft marginRight" width="500">
<caption><ds:lang text="Lista zasob�w"/></caption>
<thead>
	<tr>
		<th>Nazwa</th>
		<th>CN</th>
		<ds:available test="kalendarz.ZasobyNiewidoczneANieUsuniete">
			<th>Dost�pny</th>
		</ds:available>
		<th></th>
		<th></th>

	</tr>
</thead>
<tbody>
	<ww:iterator value="resources">
		<tr>
			<td>
				<ww:property value="name"/>
			</td>
			<td>
				<ww:property value="cn"/>
			</td>
			<ww:if test="!czyUsuwac">
				<ww:if test="available">
					<td><ds:lang text="tak"/></td>
				</ww:if>
				<ww:else>
					<td><ds:lang text="nie"/></td>
				</ww:else>	

			
			<td>
				<a href="<ww:url value="'/settings/resource-calendar.action'">
					<ww:param name="'doAvaibility'" value="true"/>
					<ww:param name="'resourceId'" value="id"/></ww:url>"><ds:lang text="zmienDostepnosc"/>
				</a>

			</td>
			</ww:if>
			<td>
				<a href="<ww:url value="'/settings/resource-calendar.action'">
					<ww:param name="'resourceId'" value="id"/></ww:url>"><ds:lang text="Edytuj"/>
				</a>

			</td>

			<ww:if test="czyUsuwac">
				<td>
					<a href="<ww:url value="'/settings/resource-calendar.action'">
						<ww:param name="'doDelete'" value="true"/>
						<ww:param name="'resourceId'" value="id"/></ww:url>"
						onclick="if (!confirm('Na pewno usun�� wpis')) return false;"><ds:lang text="Usun"/>
					</a>
				</td>
			</ww:if>
			
		</tr>
	</ww:iterator>
</tbody>
</table>

<table class="search mediumTable floatLeft" id="calTable" width="500">
<caption>Dodaj nowy zas�b</caption>
<thead>
	<tr>
		<th><ds:lang text="Nazwa"/></th>
		<th><ds:lang text="CN"/></th>
		<th></th><th></th>
	</tr>
</thead>
<tbody>
	<tr>
		<td><ww:textfield name="'name'"/></td>
		<td><ww:textfield name="'cn'"/></td>
		<ww:if test="resourceId == null">
			<td><ds:event name="'doAdd'" value="'Dodaj'" cssClass="'btn'"/></td>
		</ww:if>
		<ww:else>
			<td><ds:event name="'doUpdate'" value="'Zapisz'" cssClass="'btn'"/></td>
		</ww:else>
		<td></td>
	</tr>
</tbody>
</table>
<div class="clearBoth"></div>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:form>

<script type="text/javascript">
$j(document).ready(function() {
	if($j('#resourceId').val() != '') {
		$j('#calTable').children('caption').text('Edytuj zas�b').fadeOut('slow').fadeIn('slow');
	}
});
</script>

<!--N koniec resource-calendar.jsp N-->