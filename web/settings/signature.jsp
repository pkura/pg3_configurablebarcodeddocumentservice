<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N signature.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="PodpisElektroniczny"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/signature.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="ObslugaPodpisu"/>:
			</td>
			<td>
				<ww:checkbox name="'useSignature'" fieldValue="true" value="useSignature"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="ObslugaPodpisuDebug"/>:
			</td>
			<td>
				<ww:checkbox name="'useSignatureDebug'" fieldValue="true" value="useSignatureDebug"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn saveBtn'"/>
</form>

<%--
<h1><ds:lang text="PodpisElektroniczny"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form id="form" action="<ww:url value="'/settings/signature.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">

<table>
	<tr>
		<td><ds:lang text="ObslugaPodpisu"/>:</td>
		<td><ww:checkbox name="'useSignature'" fieldValue="true" value="useSignature"/></td>
	</tr>
	
	<tr>
		<td><ds:lang text="ObslugaPodpisuDebug"/>:</td>
		<td><ww:checkbox name="'useSignatureDebug'" fieldValue="true" value="useSignatureDebug"/></td>
	</tr>
	<tr>
		<td></td>
		<td><ds:submit-event value="getText('Zapisz')" name="'doSave'" /></td>
	</tr>
</table>
--%>
<!--N koniec signature.jsp N-->