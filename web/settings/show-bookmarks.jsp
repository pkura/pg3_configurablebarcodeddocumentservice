<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<h1>Lista zak�adek</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ds:xmlLink path="Bookmarks"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="Bookmarks"/>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/settings/show-bookmarks.action'"/>" enctype="application/x-www-form-urlencoded" method="post" onsubmit="selectAllOptions(document.getElementById('selectedBookmarks'));">
<ww:hidden name="'id'" id="id" ></ww:hidden>

<ds:available test="layout2">
	<div id="middleContainer">
</ds:available>

<table>
	<tr>
		<td colspan="1">
		<table>
			<tr>
				<td>Dost�pne zak�adki</td>
				<td></td>
				<td>Zak�adki u�ytkownika</td>
			</tr>
			<tr>
					<td class="alignTop">
						<ww:select name="'availableBookmarks'" listKey="id" listValue="name" multiple="true" size="10" 
						cssClass="'multi_sel'" cssStyle="'width: 200px;'" list="availableBookmarks"/>
					</td>
					<td class="alignMiddle">
						<input type="button" value=" &gt;&gt; " name="moveRightButton"
							onclick="if (moveOptions(this.form.availableBookmarks, this.form.selectedBookmarks) > 0) dirty=true;" class="btn"/>
						<br/>
						<input type="button" value=" &lt;&lt; " name="moveLeftButton"
							onclick="if (moveOptions(this.form.selectedBookmarks, this.form.availableBookmarks) > 0) dirty=true;" class="btn"/>
					</td>
					<td class="alignTop">
						<ww:select name="'selectedBookmarks'" id="selectedBookmarks" multiple="true" size="10" listKey="id" 
						listValue="name" cssClass="'multi_sel'" cssStyle="'width: 200px;'" list="userBookmarks"/>
					</td>
					<td class="alignMiddle">
						<a href="javascript:void(dirty=true, moveOption('selectedBookmarks', -1, -1));">
							<img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<br/>
						<a href="javascript:void(dirty=true, moveOption('selectedBookmarks', -1, 1));">
							<img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a>
					</td>
			</tr>
		</table>	
		</td>
	</tr>
	<tr>
		<td><ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/></td>
	</tr>

</table>

<ds:available test="layout2">
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div>
</ds:available>

</form>
<script type="text/javascript">
	$j('#form').bind('submit.selectAllOptions', function() {
		$j('#selectedColumns option').attr('selected', 'selected')
	});

	$j('#selectedType').bind('change.reloadPage', function() {
		$j('form')[0].submit();
	});
</script>