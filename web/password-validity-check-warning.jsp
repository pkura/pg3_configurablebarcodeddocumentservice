<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N password-validity-check-warning.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Ostrze�enie </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>Twoje has�o wygasa za <ww:property value="daysToExpire"/>
<ww:if test="daysToExpire == 1">dzie�</ww:if><ww:else>dni</ww:else>.</p>

<input type="button" class="btn" onclick="document.location.href='<ww:url value="url"/>';"
    value="Dalej"/>

<input type="button" class="btn"
    onclick="document.location.href='<ww:url value="'/self-change-password.do'"/>';"
    value="Zmie� has�o"/>
