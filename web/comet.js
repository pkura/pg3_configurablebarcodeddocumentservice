CometRequest = function(options) {
	var REQUEST_STATE = ['uninitialized', 'set up', 'sent', 'in flight', 'complete'];
	
	if(options.url == null) {
		throw 'Connection url is null';
	}
	if(options.callback == null) {
		throw 'Callback function is required';
	}
	
	
	var url = options.url, callback = options.callback,
		autoclose = options.autoclose === true,
		onError = options.error;
	
	var opera = window.opera ? true : false;
	if(opera) {
		autoclose = true;
	}
	if(autoclose) {
		var sep = '?';
		if(url.indexOf('&') == -1) {
			sep = '&';
		}
		url += sep + 'AUTO_CLOSE=true';
	}
	
	var requestReady = function(req) {
		if(autoclose) {
			return req.readyState >= 4 && req.status == 200;
		} else {
			return req.readyState >= 3 && req.status == 200;
		}
	}
	
	var requestError = function(req) {
		if(req.status == 500) {
			return true;
		} else {
			return false;
		}
	}
	
	return {
		init: function() {
			var req = new XMLHttpRequest(), oldResp = '';
			req.open('POST', url, true);
			req.setRequestHeader("Content-Type", "application/x-javascript;");
			req.onreadystatechange = function() {
//				console.info(REQUEST_STATE[req.readyState], req.status);
				if(requestReady(req)) {
//					console.info(req.responseText);
					var newResp = req.responseText + '',
						resp = null;
//					console.info(newResp + '');
					if(autoclose) {
						resp = newResp;
					} else {
						resp = newResp.substr(oldResp.length - newResp.length);
					}
					
					callback(resp, {
						state: REQUEST_STATE[req.readyState]
					});
					oldResp = newResp;
				}
				
				if(requestError(req)) {
					if(options.error != null) {
						options.error();
					}
				}
			}
			req.send();
			
		}
	}
}

