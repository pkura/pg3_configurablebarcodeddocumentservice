<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Konsola</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<script type="text/javascript">
	function processCommand(){
		try{
			$j("#cin").get(0).readOnly=true;
			$j("#terminal").append($j("#cin").get(0).value+"<br/>");
			//$j("#termFrame").get(0).scrollTo(0,$j("#cin").get(0).offsetTop);
			$j.post("termcmd.action",
				{cmd:"true", command:$j("#cin").get(0).value },
				function(data){
					$j("#terminal").append(data.toString()+"<br/>");
					$j("#cin").get(0).readOnly=false;
				});
			$j("#cin").get(0).value="";
		} catch (err) {
			alert(err.toString())
			$j("#cin").get(0).readOnly=false;
		}
		return false;
	}

	$j(document).ready(function(){
		$j("#terminal").ajaxError(function(event, request, settings){
		   $j(this).append("Error requesting page " + settings.url);
		   $j("#cin").get(0).readOnly=false;
		});
	});
</script>

<div id="termFrame" style="background-color:black; font-family:monospace !important; color:silver; height:500px; width:100%; overflow: scroll">
<pre id="terminal" style="width:100%">
Hello mortal
</pre>
	<form onsubmit="return processCommand();" action="">
		<input style="width:100%; background-color:black; font-family:monospace; color:white;" id="cin" value="">
	</form>
</div>