<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1"><ds:lang text="ManageAvailableProperties"/></h1>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/support/available-properties.action'"/>" method="post">

<table border="0" cellspacing="5" cellpadding="2">
<tr>
	<th><ds:lang text="Key"/></th>
	<th><ds:lang text="Default"/></th>
	<th><ds:lang text="License"/></th>
	<th><ds:lang text="Home"/></th>
	<th><ds:lang text="Value"/></th>
</tr>
<ww:iterator value="availableBean" status="status">
<tr>
	<td><ww:property value="getKey()"/><ww:hidden name="'key'" id="key" value="getKey()"></ww:hidden></td>
	<td><ww:property value="getDefaultprop()"/></td>
	<td><ww:property value="getLicenseprop()"/></td>
	<td>
		<ww:select id="homeprop" name="'homeprop'" cssClass="'sel'" list="#{ true : 'true', false : 'false' }" headerKey="''" headerValue="getText('null')"/>
	</td>
	<td><ww:property value="getProp()"/></td>
</tr>
</ww:iterator>
</table>
<input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" />
</form>