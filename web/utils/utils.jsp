<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<form action="<ww:url value="'/utils/utils.action'"/>" id="utilsForm" method="post">
    <ds:available test="tktImport">
		<input type="submit" value="Import Struktury Organizacyjnej TKT" class="btn" name="doImport" id="doImport"/>
		<input type="submit" value="Import Kontrahentów TKT" class="btn" name="doImportContractors" id="doImportContractors"/>
	</ds:available>
	<ds:available test="imgwImport">
        <input type="submit" value="Import Struktury Organizacyjnej IMGW" class="btn" name="doImportIMGW" id="doImportIMGW"/>
	</ds:available>
</form>
