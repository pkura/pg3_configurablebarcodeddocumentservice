<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-cases.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="webwork" prefix="ww"%>

<h1>SŁOWNIK MASZYN</h1>

<table>
	<tr>
		<td valign="top">
			<ww:property value="machineTree" escape="false" />
		</td>
		<td valign="top">
			<button onClick="window.location.href='?doAddNew=doAddNew'">
				Dodaj nowy element
			</button>
			<br>
			<br> 
			<ww:property value="infoForm" escape="false" /> 
			<ww:property value="itemForm" escape="false" /> 
			<ww:property value="addNewForm" escape="false" />
		</td>
		<td valign="top">
			<button onclick="if (!submitStronaMachines()) return false; window.close();">Umieść w formularzu</button><br><br>
        </td>
		<td valign="top">
			<form method="get" action="?doSearch=doSearch">
			<table>
				
				<tr>
					<td>
						Typ elementu:
					</td>
					<td>
					<select name="kind" id="kind">
						<option value="0">-- wybierz --</option>

						<option value="2">Rodzaj maszyny</option>
						<option value="3">Maszyna</option>
						<option value="4">Podzespół</option>
						<option value="5">Część</option>
						<option value="6">Część zamienna</option>
					</select>
					</td>
				</tr>
				<tr>
					<td>
						Kod:
					</td>
					<td>
						<input name="kod" id="kod" type="text" />
					</td>
				</tr>
				<tr> 
					<td>
						Nazwa:
					</td>
					<td> 
						<input name="nazwa" id="nazwa"  type="text" />
					</td>
				</tr>
				<tr> 
				<td>
					<input type="hidden" value="doSearch" name="doSearch"/>
					<input type="submit" value="Wyszukaj" />
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<td>

						<ww:property value="searchResults" escape="false"  />
					</td>
				</tr>
				
			</table>
			</form>
		</td>
	</tr>
</table>


<script language="JavaScript">
function id(id)
        {
            return document.getElementById(id);
        }
        
function submitStronaMachines()
        {
	
            var strona = new Array();
           
            if(id('elementId') != null){
            	strona.elementId = id('elementId').value;
            }else{
            	return false;
            }
            
            if(id('rodzaj') != null){
            	strona.rodzaj = id('rodzaj').value;
            }else{
            	strona.rodzaj = "";
            }
            
            if(id('kod') != null){
            	strona.kod = id('kod').value;
            }else{
            	strona.kod = "";
            }
            
            if(id('nazwa') != null){
            	strona.nazwa = id('nazwa').value;
            }else{
            	strona.nazwa = "";
            }
            
            if(id('nrSeryjny') != null){
            	strona.nrSeryjny = id('nrSeryjny').value;
            }else{
            	strona.nrSeryjny = "";
            }
            
            if(id('producent') != null){
            	strona.producent = id('producent').value;
            }else{
            	strona.producent = "";
            }
            
            if(id('model') != null){
            	strona.model = id('model').value;
            }else{
            	strona.model = "";
            }
            
            if(id('lokalizacja') != null){
            	strona.lokalizacja = id('lokalizacja').value;
            }else{
            	strona.lokalizacja = "";
            }
            
            if(id('strategiczna') != null){
            	strona.strategiczna = id('strategiczna').value;
            }
            
            if(id('zdjecie') != null){
            	strona.zdjecie = id('zdjecie').value;
            }else{
            	strona.zdjecie = "";
            }
            
            if(id('typMaszyny') != null){
            	strona.typMaszyny = id('typMaszyny').value;
            }else{
            	strona.typMaszyny = "";
            }
            
            if(id('opis') != null){
            	strona.opis = id('opis').value;
            }else{
            	strona.opis = "";
            }
            
            
			if (window.opener.accept)
			{
                window.opener.accept('MASZYNA_PODZESPOL_CZESC', strona);
			}
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
        
</script>
