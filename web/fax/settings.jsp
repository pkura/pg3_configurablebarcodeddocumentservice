<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N settings.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Ustawienia obs�ugi faksu</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- u�ytkownicy, na kt�rych wykonywana jest dekretacja
     domy�lny spos�b dostarczenia
     domy�lny typ dokumentu
-->

<form action="<ww:url value="'/fax/settings.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<table>
<tr>
    <td>Domy�lny spos�b dostarczenia:</td>
    <td><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
        value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"
        headerKey="''" headerValue="'-- wybierz --'" /></td>
</tr>
<tr>
    <td>Domy�lny rodzaj pisma:</td>
    <td><ww:select name="'kindId'" list="kinds" listKey="id" id="kindId"
            listValue="name + ' (dni: ' + days + ')'" cssClass="'sel'"
            value="#exists ? kind.id : kindId" disabled="#exists"
            headerKey="''" headerValue="'-- wybierz --'" />
    </td>
</tr>
<tr>
    <td valign="top">U�ytkownicy, na kt�rych<br>odb�dzie si� dekretacja:</td>
    <td>
        <ww:iterator value="users">
            <input type="checkbox" name="deleteUsers" value="<ww:property value="name"/>"/>
            <ww:property value="asLastnameFirstname()"/> <br/>
        </ww:iterator>
        <ww:if test="users.size > 0">
            <ds:submit-event value="'Usu� zaznaczonych'" name="'doDeleteUsers'" cssClass="'btn'"/>
        </ww:if>

        <p></p>
        Dodaj nowego u�ytkownika:<br>
        <ww:select name="'addUser'" list="allUsers" listKey="name" listValue="asLastnameFirstname()"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'" />
        <ds:submit-event value="'Dodaj'" name="'doAddUser'" cssClass="'btn'"/>
    </td>
</tr>
<tr>
    <td></td>
    <td><ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/></td>
</tr>
</table>

</form>
