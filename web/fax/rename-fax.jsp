<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N rename-fax.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>



<h1>Szczeg�y faksu</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/fax/rename-fax.action'"/>" method="post">
<table>
	<tr><td></td>
		<td valign="top" colspan="4">
		<ww:if test="bean.contentLink != null">
				&nbsp;
				<a href="<ww:url value="bean.contentLink"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Pobierz"/></a>
		</ww:if>
		
		
			<ww:set name="revision" value="bean.attach"/>
            <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
            &nbsp;<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'">
			<ww:param name="'id'" value="#revision.id"/>
			<ww:param name="'fax'" value="true"/>
			<ww:param name="'width'" value="1000"/>
			<ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Wy�wietl"/></a>
            </ww:if>
            &nbsp;<a href="<ww:url value="'/office/incoming/main.action'"><ww:param name="'doNewDocument'" value="true"/><ww:param name="'retainedObjectId'" value="id"/></ww:url>"><img src="<ww:url value="'/img/stampel.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Zarejestruj faks w systemie"/></a>
		</td>
	</tr>
	<tr><td >
		Tytu� faksu: 
	</td><td colspan=4>
		<ww:hidden name="'id'"/>
		<input type="text" name="title" id="title" class="txt" size="30" 
		maxlength="90" value="<ww:property value='title'/>"/>
	</td></tr>
	<tr><td>
		Nadawca:
	</td><td colspan=4>
		<ww:property value='source'/>
	</td></tr>
	<tr><td>
		Data wp�yni�cia:
	</td><td colspan=4>
		<ds:format-date value='ctime' pattern='dd-MM-yyyy'/>
	</td></tr>
	<tr><td>
		<input type="submit" name="doUpdate" value="Zapisz" class="btn"/>
	</td></tr>
</table>
</form>