<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N view-staging-area.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PoczekalniaFaksow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- u�ytkownicy, na kt�rych wykonywana jest dekretacja
     domy�lny spos�b dostarczenia
     domy�lny typ dokumentu
-->

<form action="<ww:url value="'/fax/view-staging-area.action'"/>" method="post">

<table>
    <tr>
        <th></th>
        <th><ds:lang text="Tytul"/></th>
        <th></th>
        <th></th>
    </tr>
    <ww:iterator value="results">
        <ww:set name="'id'" value="id"/>
        <tr>
            <td><ww:checkbox name="'deleteIds'" fieldValue="id"/></td>
            
            <td>&nbsp;<a href="<ww:url value="renameLink"/>"><ww:property value="title"/></a></td>
            
            <td><ww:if test="contentLink != null"><a href="<ww:url value="contentLink"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="Pobierz"/>"/></a></ww:if></td>
			<ww:set name="revision" value="attach"/>
            <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
                            <td valign="top">&nbsp;<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'fax'" value="true"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="Wyswietl"/>"/></a></td>
            </ww:if>
            <td>&nbsp;&nbsp;<a href="<ww:url value="'/office/incoming/main.action'"><ww:param name="'doNewDocument'" value="true"/><ww:param name="'retainedObjectId'" value="id"/></ww:url>"><img src="<ww:url value="'/img/stampel.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="ZarejestrujFaksWsystemie"/>"/></a></td>
        </tr>
    </ww:iterator>
    <tr>
        
        <td colspan="4"><ds:web-event name="'doDelete'" value="getText('UsunZaznaczone')"/></td>
    </tr>
</table>

</form>
