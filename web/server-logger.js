window.serverLogger = {};

/**
 * JavaScript to server logger.
 *
 * API for sending JavaScript errors to server.
 *
 * API is using:
 *  BrowserDetect   from http://www.quirksmode.org/js/detect.html         (defined inside)
 *  printStackTrace from https://github.com/eriwen/javascript-stacktrace  (defined inside)
 *
 * WARNING: Script depends on printStackTrace, so include stacktrace.js file before this file.
 *          If printStackTrace is not found script will still work without crashing, but sadly
 *          there will be no stack traces :(
 * 
 * @author Jan Święcki <jan.swiecki@docusafe.pl>
 */
(function(serverLogger,$)
{

	// console.log(printStackTrace);
	// console.log(typeof printStackTrace);
	// if(typeof printStackTrace === 'undefined')
	// {
	// 	var printStackTrace = function()
	// 	{
	// 		return "ERROR: stacktrace.js was not included in the script";
	// 	};
	// }

	var config = {};

	/**
	 * Url that log messages will be send to.
	 * @type {String}
	 */
	config.jsLogUrl = "/docusafe/jslog.action";

	/**
	 * If true then register window.onerror handler
	 * that sends any uncatched errors to server.
	 * @type {Boolean}
	 */
	config.windowOnErrorEnabled = false;

	/**
	 * If true then add non-error stack trace into
	 * logData from serverLogger.error (apart from
	 * adding strack trace from thrown error).
	 * 
	 * @type {Boolean}
	 */
	config.addNonErrorStackTrace = false;

	/**
	 * If true then show all data being send to server
	 * in console using console.log().
	 * 
	 * @type {Boolean}
	 */
	config.consoleDebug = true;

	/**
	 * Allowed logging levels
	 * @type {Array}
	 */
	var allowedLevels = ['info','debug','warn','error'];

	/**
	 * @author http://www.quirksmode.org/js/detect.html
	 */
	var BrowserDetect={init:function(){this.browser=this.searchString(this.dataBrowser)||"An unknown browser";
	this.version=this.searchVersion(navigator.userAgent)||this.searchVersion(navigator.appVersion)||"an unknown version";
	this.OS=this.searchString(this.dataOS)||"an unknown OS"},searchString:function(d){for(var a=0;
	a<d.length;a++){var b=d[a].string;var c=d[a].prop;this.versionSearchString=d[a].versionSearch||d[a].identity;
	if(b){if(b.indexOf(d[a].subString)!=-1){return d[a].identity}}else{if(c){return d[a].identity
	}}}},searchVersion:function(b){var a=b.indexOf(this.versionSearchString);if(a==-1){return
	}return parseFloat(b.substring(a+this.versionSearchString.length+1))},dataBrowser:[{string:navigator.userAgent,subString:"Chrome",identity:"Chrome"},{string:navigator.userAgent,subString:"OmniWeb",versionSearch:"OmniWeb/",identity:"OmniWeb"},{string:navigator.vendor,subString:"Apple",identity:"Safari",versionSearch:"Version"},{prop:window.opera,identity:"Opera",versionSearch:"Version"},{string:navigator.vendor,subString:"iCab",identity:"iCab"},{string:navigator.vendor,subString:"KDE",identity:"Konqueror"},{string:navigator.userAgent,subString:"Firefox",identity:"Firefox"},{string:navigator.vendor,subString:"Camino",identity:"Camino"},{string:navigator.userAgent,subString:"Netscape",identity:"Netscape"},{string:navigator.userAgent,subString:"MSIE",identity:"Explorer",versionSearch:"MSIE"},{string:navigator.userAgent,subString:"Gecko",identity:"Mozilla",versionSearch:"rv"},{string:navigator.userAgent,subString:"Mozilla",identity:"Netscape",versionSearch:"Mozilla"}],dataOS:[{string:navigator.platform,subString:"Win",identity:"Windows"},{string:navigator.platform,subString:"Mac",identity:"Mac"},{string:navigator.userAgent,subString:"iPhone",identity:"iPhone/iPod"},{string:navigator.platform,subString:"Linux",identity:"Linux"}]};

	/**
	 * http://stacktracejs.com/
	 */
	function printStackTrace(a){var a=a||{guess:!0},b=a.e||null,a=!!a.guess,d=new printStackTrace.implementation,b=d.run(b);return a?d.guessAnonymousFunctions(b):b}printStackTrace.implementation=function(){};
	printStackTrace.implementation.prototype={run:function(a,b){a=a||this.createException();b=b||this.mode(a);return"other"===b?this.other(arguments.callee):this[b](a)},createException:function(){try{this.undef()}catch(a){return a}},mode:function(a){return a.arguments&&a.stack?"chrome":a.stack&&a.sourceURL?"safari":"string"===typeof a.message&&"undefined"!==typeof window&&window.opera?!a.stacktrace||-1<a.message.indexOf("\n")&&a.message.split("\n").length>a.stacktrace.split("\n").length?"opera9":!a.stack?
	"opera10a":0>a.stacktrace.indexOf("called from line")?"opera10b":"opera11":a.stack?"firefox":"other"},instrumentFunction:function(a,b,d){var a=a||window,c=a[b];a[b]=function(){d.call(this,printStackTrace().slice(4));return a[b]._instrumented.apply(this,arguments)};a[b]._instrumented=c},deinstrumentFunction:function(a,b){a[b].constructor===Function&&(a[b]._instrumented&&a[b]._instrumented.constructor===Function)&&(a[b]=a[b]._instrumented)},chrome:function(a){a=(a.stack+"\n").replace(/^\S[^\(]+?[\n$]/gm,
	"").replace(/^\s+(at eval )?at\s+/gm,"").replace(/^([^\(]+?)([\n$])/gm,"{anonymous}()@$1$2").replace(/^Object.<anonymous>\s*\(([^\)]+)\)/gm,"{anonymous}()@$1").split("\n");a.pop();return a},safari:function(a){return a.stack.replace(/\[native code\]\n/m,"").replace(/^@/gm,"{anonymous}()@").split("\n")},firefox:function(a){return a.stack.replace(/(?:\n@:0)?\s+$/m,"").replace(/^[\(@]/gm,"{anonymous}()@").split("\n")},opera11:function(a){for(var b=/^.*line (\d+), column (\d+)(?: in (.+))? in (\S+):$/,
	a=a.stacktrace.split("\n"),d=[],c=0,f=a.length;c<f;c+=2){var e=b.exec(a[c]);if(e){var g=e[4]+":"+e[1]+":"+e[2],e=e[3]||"global code",e=e.replace(/<anonymous function: (\S+)>/,"$1").replace(/<anonymous function>/,"{anonymous}");d.push(e+"@"+g+" -- "+a[c+1].replace(/^\s+/,""))}}return d},opera10b:function(a){for(var b=/^(.*)@(.+):(\d+)$/,a=a.stacktrace.split("\n"),d=[],c=0,f=a.length;c<f;c++){var e=b.exec(a[c]);e&&d.push((e[1]?e[1]+"()":"global code")+"@"+e[2]+":"+e[3])}return d},opera10a:function(a){for(var b=
	/Line (\d+).*script (?:in )?(\S+)(?:: In function (\S+))?$/i,a=a.stacktrace.split("\n"),d=[],c=0,f=a.length;c<f;c+=2){var e=b.exec(a[c]);e&&d.push((e[3]||"{anonymous}")+"()@"+e[2]+":"+e[1]+" -- "+a[c+1].replace(/^\s+/,""))}return d},opera9:function(a){for(var b=/Line (\d+).*script (?:in )?(\S+)/i,a=a.message.split("\n"),d=[],c=2,f=a.length;c<f;c+=2){var e=b.exec(a[c]);e&&d.push("{anonymous}()@"+e[2]+":"+e[1]+" -- "+a[c+1].replace(/^\s+/,""))}return d},other:function(a){for(var b=/function\s*([\w\-$]+)?\s*\(/i,
	d=[],c,f;a&&a.arguments&&10>d.length;)c=b.test(a.toString())?RegExp.$1||"{anonymous}":"{anonymous}",f=Array.prototype.slice.call(a.arguments||[]),d[d.length]=c+"("+this.stringifyArguments(f)+")",a=a.caller;return d},stringifyArguments:function(a){for(var b=[],d=Array.prototype.slice,c=0;c<a.length;++c){var f=a[c];void 0===f?b[c]="undefined":null===f?b[c]="null":f.constructor&&(f.constructor===Array?b[c]=3>f.length?"["+this.stringifyArguments(f)+"]":"["+this.stringifyArguments(d.call(f,0,1))+"..."+
	this.stringifyArguments(d.call(f,-1))+"]":f.constructor===Object?b[c]="#object":f.constructor===Function?b[c]="#function":f.constructor===String?b[c]='"'+f+'"':f.constructor===Number&&(b[c]=f))}return b.join(",")},sourceCache:{},ajax:function(a){var b=this.createXMLHTTPObject();if(b)try{return b.open("GET",a,!1),b.send(null),b.responseText}catch(d){}return""},createXMLHTTPObject:function(){for(var a,b=[function(){return new XMLHttpRequest},function(){return new ActiveXObject("Msxml2.XMLHTTP")},function(){return new ActiveXObject("Msxml3.XMLHTTP")},
	function(){return new ActiveXObject("Microsoft.XMLHTTP")}],d=0;d<b.length;d++)try{return a=b[d](),this.createXMLHTTPObject=b[d],a}catch(c){}},isSameDomain:function(a){return"undefined"!==typeof location&&-1!==a.indexOf(location.hostname)},getSource:function(a){a in this.sourceCache||(this.sourceCache[a]=this.ajax(a).split("\n"));return this.sourceCache[a]},guessAnonymousFunctions:function(a){for(var b=0;b<a.length;++b){var d=/^(.*?)(?::(\d+))(?::(\d+))?(?: -- .+)?$/,c=a[b],f=/\{anonymous\}\(.*\)@(.*)/.exec(c);
	if(f){var e=d.exec(f[1]);e&&(d=e[1],f=e[2],e=e[3]||0,d&&(this.isSameDomain(d)&&f)&&(d=this.guessAnonymousFunction(d,f,e),a[b]=c.replace("{anonymous}",d)))}}return a},guessAnonymousFunction:function(a,b){var d;try{d=this.findFunctionName(this.getSource(a),b)}catch(c){d="getSource failed with url: "+a+", exception: "+c.toString()}return d},findFunctionName:function(a,b){for(var d=/function\s+([^(]*?)\s*\(([^)]*)\)/,c=/['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*function\b/,f=/['"]?([0-9A-Za-z_]+)['"]?\s*[:=]\s*(?:eval|new Function)\b/,
	e="",g,j=Math.min(b,20),h,i=0;i<j;++i)if(g=a[b-i-1],h=g.indexOf("//"),0<=h&&(g=g.substr(0,h)),g)if(e=g+e,(g=c.exec(e))&&g[1]||(g=d.exec(e))&&g[1]||(g=f.exec(e))&&g[1])return g[1];return"(?)"}};


	/**
	 * Send json string with logData via POST request to config.jsLogUrl in `log` variable,
	 * i.e. inside request body there will be "log=X&level=Y", where X is a json string,
	 * and Y is `level` passed to function.
	 *
	 * Before sending logData is modified as follows
	 *
	 * logData["time"] = new Date().getTime()
	 *
	 * @throws {Error} If level is not of config.allowedLevels
	 * @throws {TypeError} If logData is not object or is null
	 * @param  {String} level
	 * @param  {Object} logData
	 */
	serverLogger.log = function(level, logData)
	{
		// check level
		if(allowedLevels.indexOf(level) === -1)
		{
			throw new Error("Level '"+level+"' is not allowed");
		}

		// logData must be object
		if(typeof logData !== 'object')
		{
			throw new TypeError("logData is not an object (typeof logData: "+(typeof logData)+")");
		}

		// logData must not be null
		if(logData === null)
		{
			throw new TypeError("logData is null");
		}

		// save client time
		logData["time"] = new Date().getTime();

		// save client info
		BrowserDetect.init();
		logData["clientInfo"] =
		{
			"OS": BrowserDetect["OS"],
			"browserName": BrowserDetect["browser"],
			"browserVersion": BrowserDetect["version"]
		};

		// logData["additionalInfo"] =
		// {
		// };

		if(config.consoleDebug === true
			&& window["console"] && window["console"]["log"])
		{
			// console.log indented logData
			console.log(JSON.stringify(logData,undefined,2));
		}

		$.ajax(config.jsLogUrl,
		{
			type: "POST",
			data:
			{
				level: level,
				log: JSON.stringify(logData)
			}
		});
	};

	/**
	 * Invoke serverLogger.log("error", logData), where logData is in the following format:
	 *
	 * if error is Error object:
	 * --------------------------
	 *   {
	 *     "name": "thrownError",
	 *     "error":
	 *     {
	 *       name         : error["name"],
	 *       message      : error["message"],
	 *       fileName     : error["fileName"],
	 *       lineNumber   : error["lineNumber"],
	 *       windowOnError: error["windowOnError"]
	 *     }
	 *   }
	 *   
	 *   error["windowOnError"] with `true` value is added to error object
	 *   in window.onerror handler (if config.windowOnErrorEnabled is true).
	 *
	 * otherwise treat it as string:
	 * -----------------------------
	 *   {
	 *     "name": "thrownErrorAsString",
	 *     "error": error.toString()
	 *   }
	 *
	 * Furthermore if additionalInfo is specified it is added as
	 *   "additionalInfo": additionalInfo
	 *
	 * into logData object.
	 * 
	 * @param  {Error|String} error
	 * @param @optional {String} additionalInfo
	 */
	serverLogger.error = function(error, additionalInfo)
	{
		if(! error)
		{
			return;
		}

		var logData = {};

		if(error instanceof Error)
		{
			logData =
			{
				"name": "thrownError",
				"error":
				{
					name: error["name"],
					message: error["message"],
					fileName: error["fileName"],
					lineNumber: error["lineNumber"],
					stackTrace: printStackTrace({e: error}),
					windowOnError: error["windowOnError"]
				}
			};
		}
		else
		{
			logData =
			{
				"name": "thrownErrorAsString",
				"error": error.toString()
			};
		}

		// Add addiotionalInfo if defined.
		if(typeof additionalInfo !== 'undefined')
		{
			logData["additionalInfo"] = additionalInfo;
		}

		// Add non-error stackTrace if config says so.
		if(config.addNonErrorStackTrace === true)
		{
			logData["stackTrace"] = printStackTrace();
		}

		serverLogger.log("error", logData);
	}

	if(config.windowOnErrorEnabled === true)
	{
		// Register window.onerror function.
		// If there is already window.onerror callback
		// then when error occurs call it (prevWindowOnError)
		// after our function (window.onerror).
		var prevWindowOnError = window.onerror;

		window.onerror = function(message, url, lineNumber)
		{
			var error = new Error();
			
			// marks that we have an error from window.onerror callback
			error.windowOnError = true;
			error.message = message;
			error.fileName = url;
			error.lineNumber = lineNumber;

			serverLogger.error(error);

			// if exists call previous window.onerror handler
			if(typeof prevWindowOnError === 'function')
			{
				return prevWindowOnError(message,url,lineNumber);
			}
			else
			{
				return false;
			}
		};
	}


}(window.serverLogger, jQuery));

// try
// {
// 	JSON.parse('{"x":1,"y":[}');
// 	JSON.parse('{"x":1,"y":[}');
// }
// catch(error)
// {
// 	serverLogger.error(error);
// 	// console.log(JSON.stringify(error,undefined,2));
// }

// serverLogger.log("debug", {"message": "error message"});

// var error = new Error();
// alert(error.fileName+":"+error.lineNumber)