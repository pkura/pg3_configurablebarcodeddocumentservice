<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N changelog.jsp N-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NajnowszeZmianyWaplikacji"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="!entries.empty">
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="Wersja"/>
			</th>
			<th>
				<ds:lang text="Zmiany"/>
			</th>
		</tr>
		<ww:iterator value="entries">
			<tr>
				<td class="alignTop alignCenter">
					<ww:property value="version"/>
				</td>
				<td>
					<ul>
						<ww:iterator value="changes">
							<li>
								<span class="bold">
									<ww:property value="key"/>
								</span>
								-
								<ww:property value="value"/>
							</li>
						</ww:iterator>
					</ul>
				</td>
			</tr>
		</ww:iterator>
	</table>
</ww:if>

<%--
<h1><ds:lang text="NajnowszeZmianyWaplikacji" /></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="!entries.empty">
	<table>
		<tr>
			<th><ds:lang text="Wersja"/></th>
			<th><ds:lang text="Zmiany"/></th>
		</tr>
		<ww:iterator value="entries">
			<tr>
				<td align="center" valign="top">
					<ww:property value="version"/>
				</td>
				<td>
					<ul>
						<ww:iterator value="changes">
							<li><b><ww:property value="key"/></b> - <ww:property value="value"/></li>
						</ww:iterator>
					</ul>
				</td>
			</tr>
		</ww:iterator>
	</table>
</ww:if>
--%>
<!--N koniec changelog.jsp N-->