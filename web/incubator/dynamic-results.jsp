<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dynamic-results.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<table>
<tr>
    <ww:iterator value="columns">
        <th><ww:property value="title"/></th>
    </ww:iterator>
</tr>
<ww:iterator value="results">
    <ww:set name="result"/>
    <tr>
        <ww:iterator value="columns">
            <td><ww:property value="#result[property]"/></td>
        </ww:iterator>
    </tr>
</ww:iterator>
</table>