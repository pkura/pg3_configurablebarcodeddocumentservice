<%-- 
    Document   : overtime-monthly, wyswietla miesieczne zestawienie nadgodzin pracownika
    Created on : 2011-07-06, 22:11:46
    Author     : �ukasz Wo�niak <l.g.wozniak@gmail.com>
--%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

  <a href="<ww:url value="'/pdf-overtimes.action'">
                        <ww:param name="'month'" value="month" />
                        <ww:param name="'doPdfMonthly'" value="'true'" />
                        <ww:param name="'currentYear'" value="currentYear" />
                 </ww:url>">
        <img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>" />
        <ds:lang text="Pobierz karte PDF" />
  </a>
  <a href="<ww:url value="'/pdf-overtimes.action'">
                    <ww:param name="'month'" value="month" />
                    <ww:param name="'doXlsMonthly'" value="'true'" />
                    <ww:param name="'currentYear'" value="currentYear" />
             </ww:url>">
    <img src="<ww:url value="'/img/report.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>" />
    <ds:lang text="Pobierz karte XLS" />
    </a>
<table class="search mediumTable table100p">
<thead>
	<tr>
		<th class="empty"><ds:lang text="Data"/></th>
		<th class="empty"><ds:lang text="LiczbaWypracowanychNadgodzin"/></th>
		<th class="empty"><ds:lang text="UzasadnienieNadgodzin"/></th>
		<th class="empty"><ds:lang text="DataOdebrania"/></th>
		<th class="empty"><ds:lang text="LiczbaOdebranychNadgodzin"/></th>
	</tr>
</thead>
<tbody>
<ww:iterator value="monthlyOvertimes" status="status">
    <tr>
        <td><ds:format-date pattern="dd-MM-yyyy" value="date"/></td>
        <td><ww:property value="overtimes"/></td>
        <td><ww:property value="description"/></td>
        <td><ww:property value="firstDateReceive"/></td>
        <td><ww:property value="firstOvertimesReceive"/></td>
    </tr>
    <ww:iterator value="empOvertimeReceive" status="status2">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td><ww:property value="date"/></td>
            <td><ww:property value="overtimesReceive"/></td>
        </tr>
    </ww:iterator>
</ww:iterator>
</tbody>
<tfoot>
    <tr>
        <td><ds:lang text="PODSUMOWANIE"/></td>
        <td><ww:property value="overtimes"/></td>
        <td></td>
        <td><ds:lang text="PODSUMOWANIE"/></td>
        <td><ww:property value="overtimesReceives"/></td>
    </tr>
</tfoot>
</table>