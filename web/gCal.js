
var pageCon = $j('#request_pageContext').val();
var jqCal = $j('#ajaxCal');
var jqEventDesc = $j('#calEvent');
var allEvents = new Array(32);		// wszystkie eventy
var jqCalHrefs = new Array(32);		// wszystkie kom�rki-odno�niki kalendarza
var spannedCelsIds = new Array();	// lista kom�rek, kt�rych nie tworzymy
var eventClass = 'evt';
var calendarReady = false;
var curDate = new Date();
var tmp;
var topPos = 0;

$j(document).ready(function() {
$j('#ajaxCalDiv').datepicker({
    beforeShowDay: function(date) {
    	//trace('beforeShowDay');
    	var ret = [true, ''];

    	if(calendarReady) {
			if(allEvents[date.getDate()]) {
				//trace('FOUND for day: ' + date.getDate()); 
				ret[1] = eventClass;
				//colorifyFields();
			}
    	}
		
    	return ret;
	}, 
	onSelect: function(dateText, inst) {
			trace('SELECT: ' + dateText + ', ' + inst);
			var uDay = parseInt(dateText.split(' ')[0].split('-')[2], 10);
			var uMonth = parseInt(dateText.split(' ')[0].split('-')[1], 10);
			var uYear = parseInt(dateText.split(' ')[0].split('-')[0], 10);

			curDate.setDate(uDay);
			curDate.setMonth(uMonth-1);
			curDate.setFullYear(uYear);

			spannedCelsIds.length = 0;
			showEventInfo(uDay);
			colorifyFields();
    },
    onChangeMonthYear: function(y, m, inst) {
    		trace('onChangeMonthYear');		
    		calendarReady = false;
    		$j.post(pageCon + '/office/tasklist/calendar-ajax.action', {month: (m-1)}, function(data) {
    			data = eval('(' + data + ')');
    			trace(data);

    			for(i in allEvents) 
	    			allEvents[i] = null;
    			
    			fillEventsArray(allEvents, data);
    			colorifyFields();
    			calendarReady = true;
    		});        	
    }});
});	//	end of $j(document).ready()

/* koloruje pola */
function colorifyFields() {
	var i = 1;
	trace('colorifyFields');
	$j('#ajaxCalDiv table.ui-datepicker-calendar a').each(function() {
	    if(allEvents[i] != null) {
			$j(this).parent().addClass(eventClass);
			trace('Color field ' + i);
	    } else {
	    	$j(this).parent().removeClass(eventClass);
	    }
	    i ++;
	});
}

/* wype�nia tablic� eventami, gdzie index = dzie� miesi�ca */
function fillEventsArray(allEvents, data) {  
	for(ev in data.events) {
		var curEvent = data.events[ev];

		/* wyci�gamy dzie� miesi�ca - data w formacie 2010-03-30 08:00:00 */
		var uDay = parseInt(curEvent.startTime.split(' ')[0].split('-')[2], 10);

		/* dodajemy eventy do tablicy */
		if(allEvents[uDay] == null) {
			allEvents[uDay] = new Array();
			trace('adding to array');
		}
		allEvents[uDay].push(curEvent);

		//trace('EVENT found: ' + uDay + ' : ' + curEvent.place);
    }
}

/* pokazuje info o eventach w danym dniu */
function showEventInfo(uDay) {
    var curEvent = allEvents[uDay];
    //jqEventDesc.html('');

    traceStart('showEventInfo');
    trace('events for day: ' + uDay + ', ' + allEvents[uDay]);
	

	/* wszystkie eventy w danym dniu */
	$j('table.calEventTimeline tr td:not(.calEventHour)').remove();
	for(i in allEvents[uDay]) {
	    var curEvent = allEvents[uDay][i];
	    //trace('EVENT for day ' + uDay);

    	var startHour = curEvent.startTime.split(' ')[1].split(':')[0];
	    var endHour = curEvent.endTime.split(' ')[1].split(':')[0];
	    var lenHour = endHour - startHour;
	    var startTime = curEvent.startTime.split(' ')[1].slice(0,5);
	    var endTime = curEvent.endTime.split(' ')[1].slice(0,5);

	    var str = '<td class="calEvent" rowspan="' + lenHour + '">'; 
	    str += '<div class="eventDiv" id="eventDiv_' + curEvent.id + '_' + uDay + '_' + i + '">';
	    /* trzy sposoby wy�wietlania */
	   	var spanClassIndex = lenHour > 3 ? 3 : lenHour;

	   	if(spanClassIndex == 3) {
	   		var d = curDate;
	   		str += '<table id="eventTab_' + curEvent.id + '" class="eventTable">';
	   		str += '<tr>';
		   		str += '<td valign="top">';
			   		str += '<input type="hidden" name="eventId" value="' + curEvent.id + '"/>';
		   			str += '<input name="eventDate" class="date" type="text" value="' + d.format('dd-mm-yyyy') + '" />';
	   			str += '</td>';
	   			str += '<td valign="top">';
		   			str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="startTime" class="time" type="text" value="' + startTime + '" />';
		   			str += '<span class="dash">&ndash;</span>';
	   				str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="endTime" class="time" type="text" value="' + endTime + '" />';
	   				str += '<a class="save" href="javascript:deleteEvent(' + curEvent.id + ',' + uDay + ',' + i + ')">usu�</a>';
	   			str += '</td>';
	   		str += '</tr>'; 
			str += '<tr><td>';
				str += 'Miejsce spotkania na zewn�trz: </td><td><input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="place" type="text" value="' + curEvent.place + '" />';
			str += '</td></tr>'; 
			str += '<tr><td>';
			str += 'Opis: </td><td><textarea onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" rows="1" name="desc">' + curEvent.description + '</textarea>';
			str += '</td></tr>';
			str += '</table>';
	   	} else if(spanClassIndex == 2) {
	   		var d = curDate;
	   		str += '<table id="eventTab_' + curEvent.id + '" class="eventTable">';
	   		str += '<tr>';
	   			str += '<td valign="top">';
		   			str += '<input type="hidden" name="eventId" value="' + curEvent.id + '"/>';
	   				str += '<input name="eventDate" class="date" type="text" value="' + d.format('dd-mm-yyyy') + '" />';
	   			str += '</td>';
	   			str += '<td valign="top">';
		   			str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="startTime" class="time" type="text" value="' + startTime + '" />';
		   			str += '<span class="dash">&ndash;</span>';
	   				str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="endTime" class="time" type="text" value="' + endTime + '" />';
	   				str += '<a class="save" href="javascript:deleteEvent(' + curEvent.id + ',' + uDay + ',' + i + ')">usu�</a>';
   				str += '</td>';
   			str += '</tr>'; 
			str += '<tr>';
				str += '<td colspan="2">';
					str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="place" class="place2" type="text" value="' + curEvent.place + '" />';
					str += '<textarea onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" onfocus="expandArea(this)" onblur="shrinkArea(this)" class="desc2" rows="1" name="desc">' + curEvent.description + '</textarea>';
				str += '</td>';
			str += '</tr>'; 
			str += '</table>';
	   	} 

	   	else {
	   		var d = curDate;
	   		
			str += '<table id="eventTab_' + curEvent.id + '" class="eventTable">';
	   		str += '<tr>';
		   		str += '<td>';
			   		str += '<input type="hidden" name="eventId" value="' + curEvent.id + '"/>';
		   			str += '<input name="eventDate" class="date" type="text" value="' + d.format('dd-mm-yyyy') + '" />';
	   			str += '</td>';
		   		str += '<td>';
		   			str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="startTime" class="time" type="text" value="' + startTime + '" />';
		   			str += '<span class="dash">&ndash;</span>';
		   			str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="endTime" class="time" type="text" value="' + endTime + '" />';

		   			str += '<input onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" name="place" class="place1" type="text" value="' + curEvent.place + '" />';
		   			str += '<textarea onchange="updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')" onfocus="expandArea(this)" onblur="shrinkArea(this)" class="desc1" rows="1" name="desc">' + curEvent.description + '</textarea>'; 
				
					str += '<a class="save" href="javascript:deleteEvent(' + curEvent.id + ',' + uDay + ',' + i + ')">usu�</a>';
				str += '</td>';
			str += '</tr>';
			str += '</table>';
	   	}
	    
	    str += '</div></td>';

	    
	    if(lenHour > 1) {
			for(i = parseInt(startHour, 10) + 1; i < parseInt(endHour,10); i ++) {
				trace('EMPTY cell = ' + i);
				var calIndex = i < 10 ? ('0' + i) : i;
				spannedCelsIds.push('cal_' + calIndex);
			}
	    }
    	$j('#cal_' + startHour).append(str);
	}
	
	/* dodajemy puste kom�rki tam gdzie potrzeba */
	var maxLength = 2;	// tyle kom�rek powinno by� w kolumnie
	var len = 0;
	$j('table.calEventTimeline tr').each(function() {
	    len = $j(this).children().length;
	    if(len > maxLength)
		    maxLength = len;
	});
	
	trace('maxLength: ' + maxLength);
	trace('spannedCels: ' + spannedCelsIds);
	$j('table.calEventTimeline tr').each(function() {
		//trace('checking item ' + this.id);
	    var len = $j(this).children().length;
	    //trace('len: ' + len);
	    if((len < maxLength) && (spannedCelsIds.toString().indexOf(this.id) == -1)) {
		    trace('appending cell to ' + this.id);
		    $j(this).append('<td></td>');
	    }
	});

	$j('table.calEventTimeline > tbody > tr > td:not(.calEvent):not(.calEventHour)').addClass('empty').append('dodaj termin')
		.click(function() {
			addEvent(this, uDay);
		});

	$j('input.date').datepicker({dateFormat: 'dd-mm-yy'});
	$j('div.eventDiv').each(function() {
		var jqCurDiv = $j(this);
		var jqParent = jqCurDiv.parent();
		jqCurDiv.css('left', jqParent.position().left - 1 + 'px').css('top', jqParent.position().top - 1 + 'px')
			.css('width', jqParent.width() + 8 + 'px').css('height', jqParent.height() + 8 + 'px');
	});
	
	/* sprawdzamy koliduj�ce eventy */
	var eventWidth = null;
	traceStart('Overlapped events');
	for(i in allEvents[uDay]) {
		var iCurEvent = allEvents[uDay][i];
		var iStartHour = parseInt(iCurEvent.startTime.split(' ')[1].split(':')[0], 10);
		var iEndHour = parseInt(iCurEvent.endTime.split(' ')[1].split(':')[0], 10);
		for(j in allEvents[uDay]) {
			var jCurEvent = allEvents[uDay][j];
			if(i != j) {
				var jStartHour = parseInt(jCurEvent.startTime.split(' ')[1].split(':')[0], 10);
				var jEndHour = parseInt(jCurEvent.endTime.split(' ')[1].split(':')[0], 10);
				
				if(jStartHour < iEndHour && jStartHour > iStartHour) {
					/*if(iCurEvent.overlaps == null)
						iCurEvent.overlaps = new Array();
					if(jCurEvent.overlaps == null)
						jCurEvent.overlaps = new Array();
						
					iCurEvent.overlaps.push(parseInt(j,10));
					jCurEvent.overlaps.push(parseInt(i,10));*/
					var overHours = iEndHour - jStartHour;
					var iDivId = 'eventDiv_' + iCurEvent.id + '_' + uDay + '_' + i;
					var jDivId = 'eventDiv_' + jCurEvent.id + '_' + uDay + '_' + j;
					var jqUpperCell = $j('#' + iDivId).parent();
					var jqLowerDiv = $j('#' + jDivId);
					var rowspan = parseInt(jqUpperCell.attr('rowspan'), 10);
					jqUpperCell.attr('rowspan', rowspan-overHours);
					eventWidth = jqUpperCell.width();
					var jqParent = jqLowerDiv.parent();
					jqLowerDiv.css('left', jqParent.position().left - 1 + 'px').css('top', jqParent.position().top - 1 + 'px')
						.css('width', eventWidth + 8 + 'px').css('height', jqParent.height() + 8 + 'px')
						.addClass('eventDivOverlap');
					
					//alert(overHours + ', ' + iDivId + ', ' + rowspan); 
				}
					
			}
		}
	}
	if(eventWidth)
		$j('div.eventDiv').css('width', eventWidth + 8 + 'px');
	traceEnd();
	
	$j('.eventDiv').resizable( 
		{grid: [20,29], handles: 'n,s',
		start: function(event, ui) {
			$j('td.empty').addClass('duringResize');
			$j('table.eventTable td').addClass('duringResize');
		},
		stop: function(event, ui) {
			tmp = ui;
			$j('td.empty').removeClass('duringResize');
			$j('table.eventTable td').removeClass('duringResize');
			var divIdTab = ui.originalElement[0].id.replace('eventDiv_', '').split('_');
			var tabId = divIdTab[0];
			var uDay = parseInt(divIdTab[1], 10);
			var index = parseInt(divIdTab[2], 10);

			var jqInputStartTime = $j('#' + ui.originalElement[0].id + ' input[name=startTime]');
			var jqInputEndTime = $j('#' + ui.originalElement[0].id + ' input[name=endTime]');
			var startTime = jqInputStartTime.val();
			var endTime = jqInputEndTime.val();
			var startHourDelta = ($j(this).position().top - ui.originalPosition.top) / 29;
			var endHourDelta = ($j(this).height() - ui.originalSize.height) / 29;

			trace(ui.originalPosition.top + ', ' + $j(this).position().top );
			trace(curDate.format('dd-mm-yyyy') + ' ' + incHour(startTime, startHourDelta));

			if(startHourDelta != 0) {
				allEvents[uDay][index].startTime = curDate.format('dd-mm-yyyy') + ' ' + incHour(startTime, startHourDelta);
				jqInputStartTime.val(incHour(startTime, startHourDelta));
			}
			else if(endHourDelta != 0) {
				allEvents[uDay][index].endTime = curDate.format('dd-mm-yyyy') + ' ' + incHour(endTime, endHourDelta);
				jqInputEndTime.val(incHour(endTime, endHourDelta));
			}

			updateEvent(allEvents[uDay][index].id, uDay, index);
			spannedCelsIds.length = 0;
			showEventInfo(uDay);
		}
		}).draggable(
		{axis: 'y', grid: [20,29], cursor: 'crosshair',
		start: function(event, ui) {
			topPos = $j(this).position().top;
		},
		stop: function(event, ui) {
			tmp = ui;
			var divIdTab = this.id.replace('eventDiv_', '').split('_');
			var tabId = divIdTab[0];
			var uDay = parseInt(divIdTab[1], 10);
			var index = parseInt(divIdTab[2], 10);

			var jqInputStartTime = $j('#' + this.id + ' input[name=startTime]');
			var jqInputEndTime = $j('#' +this.id + ' input[name=endTime]');
			var startTime = jqInputStartTime.val();
			var endTime = jqInputEndTime.val();
			var startHourDelta = ($j(this).position().top - topPos) / 29;

			//trace(ui.originalPosition.top + ', ' + $j(this).position().top );
			//trace(curDate.format('dd-mm-yyyy') + ' ' + incHour(startTime, startHourDelta));

			if(startHourDelta != 0) {
				allEvents[uDay][index].startTime = curDate.format('dd-mm-yyyy') + ' ' + incHour(startTime, startHourDelta);
				allEvents[uDay][index].endTime = curDate.format('dd-mm-yyyy') + ' ' + incHour(endTime, startHourDelta);
				jqInputStartTime.val(incHour(startTime, startHourDelta));
				jqInputEndTime.val(incHour(endTime, startHourDelta));
			}

			updateEvent(allEvents[uDay][index].id, uDay, index);
			spannedCelsIds.length = 0;
			showEventInfo(uDay);
		}}
		);

	traceEnd();
}

function editEvent(uDay, evtIndex) {
	traceStart('editEvent ' + evtIndex);

	var evt = allEvents[uDay][evtIndex];
	var d = new Date(evt.startTime.split(' ')[0]);

	$j('#eventId').val(evt.id);
	$j('#eventDate').val(d.format('dd-mm-yyyy'));
	$j('#startTime').val(evt.startTime.split(' ')[1]);
	$j('#endTime').val(evt.endTime.split(' ')[1]);
	$j('#place').val(evt.place);
	
	traceEnd();
}

/* zapisuje event (z tabeli eventTab_tabId na podstawie danych z input�w */
function updateEvent(tabId, uDay, index) {
    
    tabId = '#eventTab_' + tabId;
    $j(tabId + ' a.save').html('&#8987;').addClass('saving');
    //$j(tabId + ' img.calAjaxLoader').show();
    var eventId = $j(tabId + ' input[name=eventId]').val();
    var eventDate = $j(tabId + ' input[name=eventDate]').val();
    var startTime = $j(tabId + ' input[name=startTime]').val();
    var endTime = $j(tabId + ' input[name=endTime]').val();
    var place = $j(tabId + ' input[name=place]').val();
    var desc = $j(tabId + ' textarea[name=desc]').val();

	$j.post(pageCon + '/office/tasklist/calendar-ajax.action', 
	    	{updateEvent: true, eventId: eventId, eventDate: eventDate, startTime: startTime, endTime: endTime, place: place, desc: desc}, 
    	function(data) {
		    if(data == 'OK') {
				var ev = allEvents[uDay][index];
				ev.place = place;
				ev.description = desc;
				$j(tabId + ' a.save').removeClass('saving').addClass('saved').text('zapisano');
				$j(tabId + ' img.calAjaxLoader').hide();
				setTimeout("$j('" + tabId + " a.save').removeClass('saved').text('usu�');", 3000);

				var date = $j(tabId + ' input[name=eventDate]').val().split('-');
				var day = date[0];
				var month = date[1];
				var year = date[2];
				var newStartTime = year + '-' + month + '-' + day + ' ' + startTime;
				var newEndTime = year + '-' + month + '-' + day + ' ' + endTime;
				
				allEvents[uDay][index].startTime = newStartTime;
				allEvents[uDay][index].endTime = newEndTime;
				
				spannedCelsIds.length = 0;
				showEventInfo(uDay);
				//alert('OK');
		    }
    	}
	);
}

function deleteEvent(tabId, uDay, index) {
	
	$j.post(pageCon + '/office/tasklist/calendar-ajax.action', 
	    	{deleteEvent: true, eventId: allEvents[uDay][index].id},
	    	function(data) {
    	    	if(data == 'OK') {
    	    		allEvents[uDay].splice(index, 1);
    		    	spannedCelsIds.length = 0;
    				showEventInfo(uDay);
    	    	}
	    	}
	    );
}

function addEvent(domObj, uDay) {
    traceStart('addEvent');
	var d = new Date();
	curEvent = new Object();
	curEvent.id = 1;
	curEvent.startTime = curEvent.endTime = '10-20-2040 08:00:00';

	var startTime = $j(domObj).parent().attr('id').replace('cal_', '') + ':00';
	var endTime = (parseInt($j(domObj).parent().attr('id').replace('cal_', ''),10)+1) + ':00';
	var eventDate = curDate.format('dd-mm-yyyy');


	$j.post(pageCon + '/office/tasklist/calendar-ajax.action', 
	    	{newEvent: true, eventDate: eventDate, startTime: startTime, endTime: endTime, place: 'Miejsce spotkania na zewn�trz:', desc: 'Opis'},
	    	function(data) {
				data = eval('(' + data + ')');
				trace(data);

				if(allEvents[uDay] == null)
					allEvents[uDay] = new Array();
				allEvents[uDay].push(data);
				spannedCelsIds.length = 0;
				showEventInfo(uDay);
	    	}
	    );
	/*
	var str = '';
		str += '<table id="eventTab_' + curEvent.id + '" class="eventTable"><tr><td valign="top"><input type="hidden" name="eventId" value="' + curEvent.id + '"/>';
		str += '<input name="eventDate" class="date" type="text" value="' + d.format('dd-mm-yyyy') + '" /></td>';
		str += '<td valign="top"><input onchange="changeEventStartTime(' + curEvent.id + ',' + uDay + ',' + i + ')" name="startTime" class="time" type="text" value="' + curEvent.startTime.split(' ')[1] + '" /><span class="dash">&ndash;</span>';
		str += '<input onchange="changeEventEndTime(' + curEvent.id + ',' + uDay + ',' + i + ')" name="endTime" class="time" type="text" value="' + curEvent.endTime.split(' ')[1] + '" />';
		str += '<input name="place" class="place1" type="text" value="' + curEvent.place + '" />';
		str += '<textarea onfocus="expandArea(this)" onblur="shrinkArea(this)" class="desc1" rows="1" name="desc">' + curEvent.description + '</textarea>'; 
	str += '<img class="calAjaxLoader" src="<ww:url value="'/img/ajax_loader_white.gif'"/>" /><a class="save" href="javascript:updateEvent(' + curEvent.id + ',' + uDay + ',' + i + ')">zapisz</a></td></tr></table>';
	*/
	traceEnd();
}

function changeEventStartTime(tabId, uDay, index) {
	traceStart('changeEventStartTime');
	tabId = '#eventTab_' + tabId;
	
	var newTime = $j(tabId + ' input[name=startTime]').val();
	var date = $j(tabId + ' input[name=eventDate]').val().split('-');
	var day = date[0];
	var month = date[1];
	var year = date[2];
	var newStartTime = year + '-' + month + '-' + day + ' ' + newTime;
	trace(newTime);
	
	traceEnd();
	allEvents[uDay][index].startTime = newStartTime;
	spannedCelsIds.length = 0;
	showEventInfo(uDay);
}

function changeEventEndTime(tabId, uDay, index) {
	traceStart('changeEventEndTime');
	tabId = '#eventTab_' + tabId;
	
	var newTime = $j(tabId + ' input[name=endTime]').val();
	var date = $j(tabId + ' input[name=eventDate]').val().split('-');
	var day = date[0];
	var month = date[1];
	var year = date[2];
	var newEndTime = year + '-' + month + '-' + day + ' ' + newTime;
	trace(newTime);
	
	traceEnd();
	allEvents[uDay][index].endTime = newEndTime;
	spannedCelsIds.length = 0;
	showEventInfo(uDay);
}

function expandArea(domObj) {
	$j(domObj).addClass('big');
}

function shrinkArea(domObj) {
	$j(domObj).removeClass('big');
}

function incHour(time, val) {
    var splittedTime = time.split(':');
    var h = parseInt(splittedTime[0],10)+val;
    if(h < 10)
        h = '0' + h;

    return h + ':' + splittedTime[1];
}
