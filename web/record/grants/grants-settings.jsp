<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N grants-settings.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestr wniosk�w o stypendia - ustawienia</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/grants/grants-settings.action'"/>" method="post" enctype="multipart/form-data">
	<table class="tableMargin">
		<tr>
			<td>
				Szablon decyzji pozytywnej - stypendium rzeczowe:
			</td>
			<td>
				<ww:if test="stypRzeczTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="stypRzeczTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'stypRzeczTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji pozytywnej - stypendium pieni�ne:
			</td>
			<td>
				<ww:if test="stypPienTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="stypPienTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'stypPienTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji pozytywnej - zasi�ek rzeczowy:
			</td>
			<td>
				<ww:if test="zasilRzeczTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="zasilRzeczTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'zasilRzeczTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji pozytywnej - zasi�ek pieni�ny:
			</td>
			<td>
				<ww:if test="zasilPienTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="zasilPienTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'zasilPienTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - wniosek niekompletny:
			</td>
			<td>
				<ww:if test="niekompletnyTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="niekompletnyTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'niekompletnyTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - niew�a�ciwy organ:
			</td>
			<td>
				<ww:if test="organNiewlasciwyTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="organNiewlasciwyTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'organNiewlasciwyTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - niew�a�ciwa plac�wka:
			</td>
			<td>
				<ww:if test="placowkaNiewlasciwaTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="placowkaNiewlasciwaTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'placowkaNiewlasciwaTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - niew�a�ciwe dochody:
			</td>
			<td>
				<ww:if test="dochodyNiewlasciweTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="dochodyNiewlasciweTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'dochodyNiewlasciweTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - zbyt wysoki wiek:
			</td>
			<td>
				<ww:if test="wiekZaWysokiTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="wiekZaWysokiTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'wiekZaWysokiTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej - zbyt niski wiek:
			</td>
			<td>
				<ww:if test="wiekZaNiskiTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="wiekZaNiskiTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'wiekZaNiskiTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="'Zapisz'" name="'doUpdate'"/>
</form>

<%--
<h1>Rejestr wniosk�w o stypendia - ustawienia</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/grants/grants-settings.action'"/>" method="post"
	enctype="multipart/form-data">

	<table>
		<tr>
			<td>Szablon decyzji pozytywnej - stypendium rzeczowe:</td>
			<td><ww:if test="stypRzeczTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="stypRzeczTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'stypRzeczTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji pozytywnej - stypendium pieni�ne:</td>
			<td><ww:if test="stypPienTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="stypPienTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'stypPienTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji pozytywnej - zasi�ek rzeczowy:</td>
			<td><ww:if test="zasilRzeczTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="zasilRzeczTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'zasilRzeczTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji pozytywnej - zasi�ek pieni�ny:</td>
			<td><ww:if test="zasilPienTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="zasilPienTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'zasilPienTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - wniosek niekompletny:</td>
			<td><ww:if test="niekompletnyTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="niekompletnyTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'niekompletnyTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - niew�a�ciwy organ:</td>
			<td><ww:if test="organNiewlasciwyTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="organNiewlasciwyTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'organNiewlasciwyTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - niew�a�ciwa plac�wka:</td>
			<td><ww:if test="placowkaNiewlasciwaTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="placowkaNiewlasciwaTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'placowkaNiewlasciwaTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - niew�a�ciwe dochody:</td>
			<td><ww:if test="dochodyNiewlasciweTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="dochodyNiewlasciweTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'dochodyNiewlasciweTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - zbyt wysoki wiek:</td>
			<td><ww:if test="wiekZaWysokiTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="wiekZaWysokiTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'wiekZaWysokiTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej - zbyt niski wiek:</td>
			<td><ww:if test="wiekZaNiskiTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="wiekZaNiskiTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'wiekZaNiskiTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<ds:submit-event value="'Zapisz'" name="'doUpdate'"/>
			</td>
		</tr>
	</table>

</form>
--%>
<!--N koniec grants-settings.jsp N-->