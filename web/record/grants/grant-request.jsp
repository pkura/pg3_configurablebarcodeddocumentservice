<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N grant-request.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Wniosek o stypendium</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/grants/grant-request.action'"/>" method="post"
    onsubmit="if (!validate()) return false; return disableFormSubmits(this);">

    <ww:hidden name="'id'"/>

<table>
    <tr>
        <td>Numer w rejestrze:</td>
        <td><b><ww:property value="sequenceId"/></b></td>
    </tr>
    <tr>
        <td>Sprawa:</td>
        <td><a href="<ww:url value="'/office/edit-case.do?id='+officeCase.id"/>"><ww:property value="officeCase.officeId"/></a></td>
    </tr>
    <tr>
        <td>Data wniosku<span class="warning">*</span>:</td>
        <td><ww:textfield name="'documentDate'" size="10" maxlength="10"
            cssClass="'txt'" id="documentDate" />
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Imi� wnioskodawcy<span class="warning">*</span>:</td>
        <td><ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Nazwisko wnioskodawcy<span class="warning">*</span>:</td>
        <td><ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Adres<span class="warning">*</span>:</td>
        <td><ww:textfield name="'street'" id="street" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Kod pocztowy:</td>
        <td><ww:textfield name="'zip'" id="zip" size="6" maxlength="14"
            cssClass="'txt'" /><br/><span class="warning" id="zipError"> </span></td>
    </tr>
    <tr>
        <td>Miejscowo��<span class="warning">*</span>:</td>
        <td><ww:textfield name="'location'" id="location" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Doch�d<span class="warning">*</span>:</td>
        <td><ww:textfield name="'income'" id="income" size="9" maxlength="9"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
            <input type="button" class="btn" value="Dodaj beneficjenta"
                <ww:if test="id == null">disabled="true"</ww:if>
                onclick="document.location.href='<ww:url value="'/record/grants/beneficiary.action'"><ww:param name="'requestId'" value="id"/></ww:url>';"/>
        </td>
    </tr>
    <ww:if test="beneficiaries.size > 0">
        <tr>
            <td colspan="2"><hr/></td>
        </tr>
        <tr>
            <td colspan="2"><h3>Beneficjenci</h3></td>
        </tr>
        <tr>
            <td colspan="2">
                <table>
                    <tr>
                        <th></th>
                        <th>Imi� i nazwisko</th>
                        <th>Wnioskowana pomoc</th>
                        <th>Przyznana pomoc</th>
                        <th>Status</th>
                    </tr>
                    <ww:iterator value="beneficiaries" >
                        <tr>
                            <td><ww:checkbox name="'posns'" fieldValue="posn"/></td>
                            <td>
                                <a href="<ww:url value="'/record/grants/beneficiary.action'"><ww:param name="'requestId'" value="id"/><ww:param name="'posn'" value="posn"/></ww:url>">
                                    <ww:property value="firstname"/>
                                    <ww:property value="lastname"/>
                                </a>
                            </td>
                            <td>
                                <ww:property value="helpRequested"/>
                            </td>
                            <td>
                                <ww:property value="helpGranted"/>
                            </td>
                            <td>
                                <ww:property value="status"/>
                            </td>
                        </tr>
                    </ww:iterator>
                </table>
                <ds:submit-event value="'Usu� zaznaczonych'" name="'doDeleteBeneficiaries'"
                    cssClass="'btn'"/>
            </td>
        </tr>
    </ww:if>
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td>Status wniosku:</td>
        <td><ww:property value="status"/></td>
    </tr>
    <ww:if test="!terminated">
        <ww:if test="results.size > 0">
            <tr>
                <td>Wybierz wynik:</td>
                <td>
                    <ww:iterator value="results">
                        <nobr>
                            <input type="radio" name="result" value="<ww:property value="key"/>"/>
                            <ww:property value="value"/>
                        </nobr>
                    </ww:iterator>
                </td>
            </tr>
        </ww:if>
        <tr>
            <td colspan="2">
                <ds:submit-event value="'Przejd� do kolejnego kroku obs�ugi wniosku'" name="'doPush'" cssClass="'btn'" confirm="'Na pewno kontynuowa�?'"/>
            </td>
        </tr>
    </ww:if>
    <ww:else>
        <tr>
            <td colspan="2">
                <input type="button" class="btn" value="Powr�� do widoku sprawy"
                    onclick="document.location.href='<ww:url value="'/office/edit-case.do?id='+officeCase.id"/>'"/>
            </td>
        </tr>
        <tr>
            <td>
            	<input type="button" value="Utw�rz odpowied�" class="btn"
                    onclick="document.location.href='<ww:url value="'/office/outgoing/main.action'"><ww:param name="'doAutoCreate'" value="true"/><ww:param name="'inDocumentId'" value="inDocumentId"/><ww:param name="'grantRequestId'" value="id"/></ww:url>';" />
            </td>
            <td>
            	<input type="button" value="Zobacz szablon RTF" class="btn"
                    onclick="document.location.href='<ww:url value="'/record/grants/grant-request.action'"><ww:param name="'id'" value="id"/><ww:param name="'doTemplateRTF'" value="true"/></ww:url>';" />
            </td>
        </tr>
    </ww:else>
</table>

</form>

<script>
    Calendar.setup({
        inputField     :    "documentDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

        // walidacja kodu pocztowego
    var szv = new Validator(document.getElementById('zip'), '##-###');

    szv.onOK = function() {
        this.element.style.color = 'black';
        document.getElementById('zipError').innerHTML = '';
    }

    szv.onEmpty = szv.onOK;

    szv.onError = function(code) {
        this.element.style.color = 'red';
        if (code == this.ERR_SYNTAX)
            document.getElementById('zipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
        else if (code == this.ERR_LENGTH)
            document.getElementById('zipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
    }

    szv.canValidate = function() { return true; }

    // pocz�tkowa walidacja danych
    szv.revalidate();

    function validate()
    {
        if (isEmpty(document.getElementById('documentDate').value))
        {
            alert('Nie podano daty dokumentu');
            return false;
        }

        if (isEmpty(document.getElementById('firstname').value))
        {
            alert('Nie podano imienia');
            return false;
        }

        if (isEmpty(document.getElementById('lastname').value))
        {
            alert('Nie podano nazwiska');
            return false;
        }

        if (isEmpty(document.getElementById('street').value))
        {
            alert('Nie podano adresu');
            return false;
        }

        if (isEmpty(document.getElementById('location').value))
        {
            alert('Nie podano nazwy miejscowo�ci');
            return false;
        }

        if (isEmpty(document.getElementById('income').value) ||
            !validateDecimal(document.getElementById('income').value))
        {
            alert('Nie podano dochodu lub podana warto�� jest nieprawid�owa');
            return false;
        }

        return true;
    }
</script>
