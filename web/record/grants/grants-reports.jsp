<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N grants-reports.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<style type="text/css">
	select#sortType {width:9em;}
	select#sortField {width:9em;}
</style>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Stypendia - raporty</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/grants/grants-reports.action'"/>" method="post" id="form" >
	<table class="tableMargin">
		<tr>
			<td>
				Numer w rejestrze:
			</td>
			<td>
				Od:
				<ww:textfield name="'sequenceIdFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/>
				do:
				<ww:textfield name="'sequenceIdTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/>
			</td>
		</tr>
		<tr>
			<td>
				Imi� wnioskodawcy:
			</td>
			<td>
				<ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50" cssClass="'txt'" />
			</td>
		</tr>
		<tr>
			<td>
				Nazwisko wnioskodawcy:
			</td>
			<td>
				<ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50" cssClass="'txt'" />
			</td>
		</tr>
		<tr>
			<td>
				Adres:
			</td>
			<td>
				<ww:textfield name="'street'" id="street" size="50" maxlength="50" cssClass="'txt'" />
			</td>
		</tr>
		<tr>
			<td>
				Kod pocztowy:
			</td>
			<td>
				<ww:textfield name="'zip'" id="zip" size="6" maxlength="14" cssClass="'txt'" />
			</td>
		</tr>
		<tr>
			<td>
				Miejscowo��:
			</td>
			<td>
				<ww:textfield name="'location'" id="location" size="50" maxlength="50" cssClass="'txt'" />
			</td>
		</tr>
		<tr>
			<td>
				Posortuj wed�ug
			</td>
			<td>
				<ww:select name="'sortField'" id="sortField" list="sortFieldsList" listKey="key" listValue="value" cssClass="'sel'"/>
			</td>
		</tr>
		<tr>
			<td>
				Kolejno��
			</td>
			<td>
				<ww:select name="'sortType'" id="sortType" list="sortTypesList" listKey="key" listValue="value" cssClass="'sel'"/>
			</td>
		</tr>
	</table>
	
	<input type="button" class="btn" value="Drukuj list� wyp�at do kasy" onclick="openPrint(false)"/>
	<input type="button" class="btn" value="Drukuj list� wyp�at do banku" onclick="openPrint(true)"/>
</form>

<%--
<h1>Stypendia - raporty</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

 <form action="<ww:url value="'/record/grants/grants-reports.action'"/>" method="post"
		id="form" >

 <script>
	function openPrint(bank)
	{
		if (!confirm('Zamierzasz wydrukowa� list� wyp�at do '+(bank ? 'banku' : 'kasy')
			+ '. Na pewno zosta�y podane prawid�owe kryteria?'))
			return false;

		var printLink = '<ww:url value="'/record/grants/grants-reports.action'"/>';
		printLink = printLink +
					(bank ? "?doPrintBank=true" : "?doPrintCash=true") +
					'&sequenceIdFrom=' + document.getElementById('sequenceidfrom').value +
					'&sequenceIdTo=' +  document.getElementById('sequenceidto').value +
					'&firstname=' +  document.getElementById('firstname').value +
					'&lastname=' +  document.getElementById('lastname').value +
					'&zip=' +  document.getElementById('zip').value +
					'&street=' +  document.getElementById('street').value +
					'&location=' +  document.getElementById('location').value +
					'&sortField=' +  document.getElementById('sortField').value +
					'&sortType=' +  document.getElementById('sortType').value;

	// alert(printLink);
		window.open(printLink, null);//, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')
	}

 </script>

<table>
	<tr>
		<td>Numer w rejestrze:</td>
		<td>Od: <ww:textfield name="'sequenceIdFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/> do: <ww:textfield name="'sequenceIdTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/></td>
	</tr>
	<tr>
		<td>Imi� wnioskodawcy:</td>
		<td><ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50"
			cssClass="'txt'" /></td>
	</tr>
	<tr>
		<td>Nazwisko wnioskodawcy:</td>
		<td><ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50"
			cssClass="'txt'" /></td>
	</tr>
	<tr>
		<td>Adres:</td>
		<td><ww:textfield name="'street'" id="street" size="50" maxlength="50"
			cssClass="'txt'" /></td>
	</tr>
	<tr>
		<td>Kod pocztowy:</td>
		<td><ww:textfield name="'zip'" id="zip" size="6" maxlength="14"
			cssClass="'txt'" /></td>
	</tr>
	<tr>
		<td>Miejscowo��:</td>
		<td><ww:textfield name="'location'" id="location" size="50" maxlength="50"
			cssClass="'txt'" /></td>
	</tr>
	<tr>
		<td>Posortuj wed�ug</td>
		<td><ww:select name="'sortField'" id="sortField" list="sortFieldsList" listKey="key" listValue="value"
			cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>Kolejno��</td>
		<td><ww:select name="'sortType'" id="sortType" list="sortTypesList" listKey="key" listValue="value"
			cssClass="'sel'"/></td>
	</tr>
	<tr>
		<td>
			<input type="button" class="btn" value="Drukuj list� wyp�at do kasy"
				onclick="openPrint(false)"/>
		</td>
		<td>
			<input type="button" class="btn" value="Drukuj list� wyp�at do banku"
				onclick="openPrint(true)"/>
		</td>
	</tr>
</table>

</form>
--%>
<script>
	function openPrint(bank)
	{
		if (!confirm('Zamierzasz wydrukowa� list� wyp�at do '+(bank ? 'banku' : 'kasy')
			+ '. Na pewno zosta�y podane prawid�owe kryteria?'))
			return false;

		var printLink = '<ww:url value="'/record/grants/grants-reports.action'"/>';
		printLink = printLink +
					(bank ? "?doPrintBank=true" : "?doPrintCash=true") +
					'&sequenceIdFrom=' + document.getElementById('sequenceidfrom').value +
					'&sequenceIdTo=' +  document.getElementById('sequenceidto').value +
					'&firstname=' +  document.getElementById('firstname').value +
					'&lastname=' +  document.getElementById('lastname').value +
					'&zip=' +  document.getElementById('zip').value +
					'&street=' +  document.getElementById('street').value +
					'&location=' +  document.getElementById('location').value +
					'&sortField=' +  document.getElementById('sortField').value +
					'&sortType=' +  document.getElementById('sortType').value;

		window.open(printLink, null);//, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')
	}
</script>
<!--N koniec grants-reports.jsp N-->