<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Stypendia</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<p>
	<a href="<ww:url value="'/record/grants/grants-search.action'"/>">
		Wyszukiwanie
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wyszukiwanie wniosk�w
</p>

<p>
	<a href="<ww:url value="'/record/grants/grants-reports.action'"/>">
		Raporty
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wydruk listy wyp�at do kasy i banku
</p>

<ds:subject admin="true">
	<p>
		<a href="<ww:url value="'/record/grants/grants-settings.action'"/>">
			Konfiguracja
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		<br/>
		Opcje konfiguracyjne
	</p>
</ds:subject>

<%--
<h1>Stypendia</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<p><a href="<ww:url value="'/record/grants/grants-search.action'"/>">Wyszukiwanie <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	Wyszukiwanie wniosk�w
</p>

<p><a href="<ww:url value="'/record/grants/grants-reports.action'"/>">Raporty <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	Wydruk listy wyp�at do kasy i banku
</p>

<ds:subject admin="true">
	<p><a href="<ww:url value="'/record/grants/grants-settings.action'"/>">Konfiguracja <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
		Opcje konfiguracyjne
	</p>
</ds:subject>
--%>
<!--N koniec main.jsp N-->