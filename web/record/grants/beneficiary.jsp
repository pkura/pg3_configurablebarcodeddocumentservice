<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N beneficiary.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Beneficjent stypendium</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script type="text/javascript">

	function openSchoolPopup()
	{
        openToolWindow('<ww:url value="'/office/common/school.action'"/>?'+
            'name='+document.getElementById('schoolName').value+
            '&street='+document.getElementById('schoolStreet').value+
            '&zip='+document.getElementById('schoolZip').value+
            '&location='+document.getElementById('schoolLocation').value+
            '&canAddAndSubmit=true');
	}
	
	function __accept_school(map)
	{
        document.getElementById('schoolName').value = map.name;
        document.getElementById('schoolStreet').value = map.street;
        document.getElementById('schoolZip').value = map.zip;
        document.getElementById('schoolLocation').value = map.location;
	}	

</script>

<form action="<ww:url value="'/record/grants/beneficiary.action'"/>" method="post"
    onsubmit="if (!validate()) return false; return disableFormSubmits(this);">

    <ww:hidden name="'requestId'"/>
    <ww:hidden name="'posn'"/>

<table>
    <tr>
        <td>Imi�<span class="warning">*</span>:</td>
        <td><ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Nazwisko<span class="warning">*</span>:</td>
        <td><ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Data urodzenia<span class="warning">*</span>:</td>
        <td><ww:textfield name="'birthDate'" size="10" maxlength="10"
            cssClass="'txt'" id="birthDate" />
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="birthDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Imi� ojca:</td>
        <td><ww:textfield name="'fathersName'" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Imi� matki:</td>
        <td><ww:textfield name="'mothersName'" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Szko�a<span class="warning">*</span>:</td>
        <td><ww:textfield name="'schoolName'" id="schoolName" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Adres szko�y:</td>
        <td><ww:textfield name="'schoolStreet'" id="schoolStreet" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Adres szko�y (kod pocztowy):</td>
        <td><ww:textfield name="'schoolZip'" id="schoolZip" size="6" maxlength="14"
            cssClass="'txt'" /><br/><span class="warning" id="zipError"> </span></td>
    </tr>
    <tr>
        <td>Adres szko�y (miejscowo��):</td>
        <td><ww:textfield name="'schoolLocation'" id="schoolLocation" size="50" maxlength="50"
            cssClass="'txt'" /></td>
    </tr>
    <tr>
	    <td>
	    </td>
	    <td><input type="button" value="Wybierz szko��" onclick="openSchoolPopup();" class="btn" ></td>
	</tr>
    <tr>
        <td>Konto bankowe:</td>
        <td><ww:textfield name="'bankAccount'" id="bankAccount" size="40"
                          maxlength="40" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td>Wnioskowana pomoc<span class="warning">*</span>:</td>
        <td><ww:select name="'helpRequested'" id="helpRequested"
            list="helpRequestedKinds"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'" />
        </td>
    </tr>
    <ww:if test="helpGrantedKinds.size > 0">
    	<tr>
        	<td>Przyznana pomoc<span class="warning">*</span>:</td>
        	<td><ww:select name="'helpGranted'" list="helpGrantedKinds"
            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        	</td>
    	</tr>
    </ww:if>
    <tr>
        <td>Odmowa:</td>
        <td><ww:checkbox name="'rejected'" fieldValue="true" value="rejected" onchange="'enableJustification(this.checked);'"/></td>
    </tr>
    <tr>
        <td>Przyczyna odmowy:</td>
        <td><ww:textfield name="'justification'" id="justification" size="50" maxlength="200"
            cssClass="'txt'" disabled="true" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
            <input type="button" value="Powr�t do wniosku" class="btn"
                onclick="document.location.href='<ww:url value="'/record/grants/grant-request.action'"><ww:param name="'id'" value="requestId"/></ww:url>';"/>
        </td>
    </tr>
    <ww:if test="posn != null">
        <tr>
            <td colspan="2"><hr/></td>
        </tr>
        <tr>
            <td>Status beneficjenta:</td>
            <td><ww:property value="status"/></td>
        </tr>
        <ww:if test="!terminated and canPush">
            <ww:if test="results.size > 0">
                <tr>
                    <td>Wybierz wynik:</td>
                    <td>
                        <ww:iterator value="results">
                            <nobr>
                                <input type="radio" name="result" value="<ww:property value="key"/>" <ww:if test="result == key">checked="true"</ww:if> />
                                <ww:property value="value"/>
                            </nobr>
                        </ww:iterator>
                    </td>
                </tr>
            </ww:if>
            <tr>
                <td colspan="2">
                    <ds:submit-event value="'Przejd� do kolejnego kroku obs�ugi beneficjenta'"
                        name="'doPush'" cssClass="'btn'"
                        confirm="'Na pewno kontynuowa�?'" />
                </td>
            </tr>
        </ww:if>
    </ww:if>
</table>

</form>

<script>
    Calendar.setup({
        inputField     :    "birthDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "birthDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

    function enableJustification(enable)
    {
        document.getElementById('justification').disabled = !enable;
    }

        // walidacja kodu pocztowego
    var szv = new Validator(document.getElementById('schoolZip'), '##-###');

    szv.onOK = function() {
        this.element.style.color = 'black';
        document.getElementById('zipError').innerHTML = '';
    }

    szv.onEmpty = szv.onOK;

    szv.onError = function(code) {
        this.element.style.color = 'red';
        if (code == this.ERR_SYNTAX)
            document.getElementById('zipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
        else if (code == this.ERR_LENGTH)
            document.getElementById('zipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
    }

    szv.canValidate = function() { return true; }

    // pocz�tkowa walidacja danych
    szv.revalidate();


    function validate()
    {
        if (isEmpty(document.getElementById('firstname').value))
        {
            alert('Nie podano imienia');
            return false;
        }

        if (isEmpty(document.getElementById('lastname').value))
        {
            alert('Nie podano nazwiska');
            return false;
        }

        if (isEmpty(document.getElementById('birthDate').value))
        {
            alert('Nie podano daty urodzenia');
            return false;
        }

        if (isEmpty(document.getElementById('schoolName').value))
        {
            alert('Nie podano nazwy szko�y');
            return false;
        }

        var bankAccount = document.getElementById('bankAccount').value;
        if (bankAccount)
        {
            for (var i=0; i < bankAccount.length; i++)
            {
                var c = bankAccount.charAt(i);
                if (! ((c >= '0' && c <= '9') || c == ' ' || c == '-'))
                {
                    alert('Niepoprawny format numeru rachunku bankowego')
                    return false;
                }
            }
        }

        if (isEmpty(document.getElementById('helpRequested').value))
        {
            alert('Nie wybrano rodzaju wnioskowanej pomocy');
            return false;
        }

        return true;
    }
</script>
