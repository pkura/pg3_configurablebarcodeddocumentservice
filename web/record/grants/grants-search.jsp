<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N grants-search.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<style type="text/css">
	select#justification {width:11em;}
	select#helpGranted {width:11em;}
	select#decision {width:11em;}
</style>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Stypendia - wyszukiwanie wniosk�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form action="<ww:url value="'/record/grants/grants-search.action'"/>" method="post" onsubmit="disableFormSubmits(this);" id="form">
		<table class="tableMargin">
			<tr>
				<td>
					Numer w rejestrze:
				</td>
				<td>
					Od:
					<ww:textfield name="'sequenceIdFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/>
					do:
					<ww:textfield name="'sequenceIdTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/>
				</td>
			</tr>
			<tr>
				<td>
					Data wniosku:
				</td>
				<td>
					Od:
					<ww:textfield name="'startDocumentDate'" id="startDocumentDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_startDocumentDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endDocumentDate'" id="endDocumentDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_endDocumentDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Imi� wnioskodawcy:
				</td>
				<td>
					<ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Nazwisko wnioskodawcy:
				</td>
				<td>
					<ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Adres:
				</td>
				<td>
					<ww:textfield name="'street'" id="street" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Kod pocztowy:
				</td>
				<td>
					<ww:textfield name="'zip'" id="zip" size="6" maxlength="14" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Miejscowo��:
				</td>
				<td>
					<ww:textfield name="'location'" id="location" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<p>
						<h4>Dane beneficjenta</h4>
					</p>
				</td>
			</tr>
			<tr>
				<td>
					Szko�a:
				</td>
				<td>
					<ww:textfield name="'schoolName'" id="schoolName" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Adres szko�y:
				</td>
				<td>
					<ww:textfield name="'schoolStreet'" id="schoolStreet" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Adres szko�y (kod pocztowy):
				</td>
				<td>
					<ww:textfield name="'schoolZip'" id="schoolZip" size="6" maxlength="14" cssClass="'txt'"/>
					<br/>
					<span class="warning" id="zipError"></span>
				</td>
			</tr>
			<tr>
				<td>
					Adres szko�y (miejscowo��):
				</td>
				<td>
					<ww:textfield name="'schoolLocation'" id="schoolLocation" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="button" value="Wybierz szko��" onclick="openSchoolPopup();" class="btn">
				</td>
			</tr>
			<tr>
				<td>
					Decyzja:
				</td>
				<td>
					<ww:select name="'decision'" id="decision" list="decisions" cssClass="'sel'" headerKey="''" headerValue="'-- jest lub nie ma --'"/>
				</td>
			</tr>
			<tr>
				<td>
					Forma przyznanej pomocy:
				</td>
				<td>
					<ww:select name="'helpGranted'" id="helpGranted" list="helpGrantedKinds" headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Przyczyna odmowy:
				</td>
				<td>
					<ww:select name="'justification'" id="justification" list="justifications" headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
		</table>
		
		<ds:submit-event value="'Szukaj'" name="'doSearch'"/>
	</form>

	<script type="text/javascript">
		Calendar.setup({
			inputField	:	"startDocumentDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_startDocumentDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	:	"endDocumentDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_endDocumentDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	
		function openSchoolPopup()
		{
			openToolWindow('<ww:url value="'/office/common/school.action'"/>?'+
				'name='+document.getElementById('schoolName').value+
				'&street='+document.getElementById('schoolStreet').value+
				'&zip='+document.getElementById('schoolZip').value+
				'&location='+document.getElementById('schoolLocation').value+
				'&canAddAndSubmit=true');
		}
		
		function __accept_school(map)
		{
			document.getElementById('schoolName').value = map.name;
			document.getElementById('schoolStreet').value = map.street;
			document.getElementById('schoolZip').value = map.zip;
			document.getElementById('schoolLocation').value = map.location;
		}
	</script>
</ww:if>

<ww:else>
	<table class="search tableMargin table100p" cellspacing="0">
		<tr>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('sequenceId', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Nr w rej.
					<a href="<ww:url value="getSortLink('sequenceId', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('officeId', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Nr sprawy
					<a href="<ww:url value="getSortLink('officeId', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('documentDate', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Data wniosku
					<a href="<ww:url value="getSortLink('documentDate', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th> 
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('lastname', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Wnioskodawca
					<a href="<ww:url value="getSortLink('lastname', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					Adres wnioskodawcy
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					Dziecko
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					Szko�a
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					Decyzja
				</span>
			</th>
		</tr>

		<ww:iterator value="results">
			<tr>
				<td>
					<a href="<ww:url value="'/record/grants/grant-request.action'">
						<ww:param name="'id'" value="id"/></ww:url>"><ww:property value="sequenceId"/></a>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="officeId"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ds:format-date value="documentDate" pattern="dd-MM-yy"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="firstname"/>
					<ww:property value="lastname"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="getAddress()"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="getBeneficiaryName()"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="getBeneficiarySchoolAddress()"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:property value="getDecisionDescription()"/>
				</td>
			</tr>
		</ww:iterator>
	</table>

	<p>
		Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<option value="<ww:url value="printRequestersUrl"/>">zestawienie wnioskodawc�w</option>
			<option value="<ww:url value="printFinishedRequestsUrl"/>">zestawienie wniosk�w po przeprowadzeniu analizy</option>
		</select>
		<input type="button" value="Wydruk" class="btn"
			onclick="if (document.getElementById('printSel').value == '') { alert('Nie wybrano rodzaju wydruku'); return false; }; window.open(document.getElementById('printSel').value, null, '')" />
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div>
</ww:else>

<%--
<h1>Stypendia - wyszukiwanie wniosk�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<ww:if test="results == null || results.count() == 0">

	<script type="text/javascript">

	function openSchoolPopup()
	{
		openToolWindow('<ww:url value="'/office/common/school.action'"/>?'+
			'name='+document.getElementById('schoolName').value+
			'&street='+document.getElementById('schoolStreet').value+
			'&zip='+document.getElementById('schoolZip').value+
			'&location='+document.getElementById('schoolLocation').value+
			'&canAddAndSubmit=true');
	}

	function __accept_school(map)
	{
		document.getElementById('schoolName').value = map.name;
		document.getElementById('schoolStreet').value = map.street;
		document.getElementById('schoolZip').value = map.zip;
		document.getElementById('schoolLocation').value = map.location;
	}

	</script>

	<form action="<ww:url value="'/record/grants/grants-search.action'"/>" method="post"
		onsubmit="disableFormSubmits(this);" id="form" >

		<table>
			<tr>
				<td>Numer w rejestrze:</td>
				<td>Od: <ww:textfield name="'sequenceIdFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/> do: <ww:textfield name="'sequenceIdTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/></td>
			</tr>
			<tr>
				<td>Data wniosku:</td>
				<td>
					Od:
					<ww:textfield name="'startDocumentDate'" id="startDocumentDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_startDocumentDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endDocumentDate'" id="endDocumentDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_endDocumentDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>Imi� wnioskodawcy:</td>
				<td><ww:textfield name="'firstname'" id="firstname" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Nazwisko wnioskodawcy:</td>
				<td><ww:textfield name="'lastname'" id="lastname" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Adres:</td>
				<td><ww:textfield name="'street'" id="street" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Kod pocztowy:</td>
				<td><ww:textfield name="'zip'" id="zip" size="6" maxlength="14"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Miejscowo��:</td>
				<td><ww:textfield name="'location'" id="location" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td colspan="2"><p><h4>Dane beneficjenta</h4></p></td>
			</tr>
			<tr>
				<td>Szko�a:</td>
				<td><ww:textfield name="'schoolName'" id="schoolName" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Adres szko�y:</td>
				<td><ww:textfield name="'schoolStreet'" id="schoolStreet" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>Adres szko�y (kod pocztowy):</td>
				<td><ww:textfield name="'schoolZip'" id="schoolZip" size="6" maxlength="14"
					cssClass="'txt'" /><br/><span class="warning" id="zipError"> </span></td>
			</tr>
			<tr>
				<td>Adres szko�y (miejscowo��):</td>
				<td><ww:textfield name="'schoolLocation'" id="schoolLocation" size="50" maxlength="50"
					cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>
				</td>
				<td><input type="button" value="Wybierz szko��" onclick="openSchoolPopup();" class="btn" ></td>
			</tr>
			<tr>
				<td>Decyzja:</td>
				<td><ww:select name="'decision'" id="decision" list="decisions" cssClass="'sel'"
					headerKey="''" headerValue="'-- jest lub nie ma --'" /></td>
			</tr>
			<tr>
				<td>Forma przyznanej pomocy:</td>
				<td><ww:select name="'helpGranted'" id="helpGranted" list="helpGrantedKinds"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>Przyczyna odmowy:</td>
				<td><ww:select name="'justification'" id="justification" list="justifications"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<ds:submit-event value="'Szukaj'" name="'doSearch'"/>
				</td>
			</tr>
		</table>
			
	</form>

	<script type="text/javascript">
		Calendar.setup({
			inputField	 :	"startDocumentDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_startDocumentDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	 :	"endDocumentDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_endDocumentDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});

	</script>
</ww:if>
<ww:else>
	<table class="search" width="100%" cellspacing="0">
	<tr>
		<th><nobr>
			<a href="<ww:url value="getSortLink('sequenceId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Nr w rej.
			<a href="<ww:url value="getSortLink('sequenceId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('officeId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Nr sprawy
			<a href="<ww:url value="getSortLink('officeId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('documentDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Data wniosku
			<a href="<ww:url value="getSortLink('documentDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th> 
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('lastname', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Wnioskodawca
			<a href="<ww:url value="getSortLink('lastname', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			Adres wnioskodawcy
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			Dziecko
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			Szko�a
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			Decyzja
		</nobr></th>
	</tr>

	<ww:iterator value="results">
		<tr>
			<td><a href="<ww:url value="'/record/grants/grant-request.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="sequenceId"/></a></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td>
				<ww:property value="officeId"/>
			</td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ds:format-date value="documentDate" pattern="dd-MM-yy"/></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td>
				<ww:property value="firstname"/> <ww:property value="lastname"/>
			</td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ww:property value="getAddress()"/></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ww:property value="getBeneficiaryName()"/></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ww:property value="getBeneficiarySchoolAddress()"/></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ww:property value="getDecisionDescription()"/></td>
		</tr>
	</ww:iterator>
	</table>

	<p>Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<option value="<ww:url value="printRequestersUrl"/>">zestawienie wnioskodawc�w</option>
			<option value="<ww:url value="printFinishedRequestsUrl"/>">zestawienie wniosk�w po przeprowadzeniu analizy</option>
	<%--	  <option value="<ww:url value="printABUrl"/>">rejestr zbiorczy</option>
	-->
		</select>
		<input type="button" value="Wydruk" class="btn"
			onclick="if (document.getElementById('printSel').value == '') { alert('Nie wybrano rodzaju wydruku'); return false; }; window.open(document.getElementById('printSel').value, null, '')" />
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<table width="100%">
	<tr>
		<td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
	</tr>
	</table>
</ww:else>
--%>
<!--N koniec grants-search.jsp N-->