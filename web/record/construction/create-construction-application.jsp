<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N create-construction-application.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Wniosek o zezwolenie na budow�</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canCreateApplication">
<form action="<ww:url value="'/record/construction/create-construction-application.action'"/>"
    method="post" onsubmit="disableFormSubmits(this);">

    <ww:hidden name="'caseId'"/>
    <ww:hidden name="'documentId'"/>

    <ww:hidden name="'registryNoVerify'" value="registryNo"/>
    <ww:hidden name="'sequenceIdVerify'" value="sequenceId"/>
    <ww:hidden name="'submitted'" value="true"/>

    <table>
        <tr>
            <td>Data wp�yni�cia wniosku:</td>
            <td><ww:property value="submitDate"/></td>
        </tr>
        <tr>
            <td>Opis <span class="star">*</span>:</td>
            <td><ww:textfield name="'summary'" cssClass="'txt'" size="50" maxlength="200"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Inwestor</h3>
            </td>
        </tr>
        <tr>
            <td>Nazwa<span class="star">*</span>:</td>
            <td><ww:textfield name="'developerName'" cssClass="'txt'" size="50" maxlength="80"/> </td>
        </tr>
        <tr>
            <td>Adres:</td>
            <td><ww:textfield name="'developerStreet'" cssClass="'txt'" size="50" maxlength="80"/> </td>
        </tr>
        <tr>
            <td>Kod pocztowy i miejscowo��:</td>
            <td><ww:textfield name="'developerZip'" id="developerZip" cssClass="'txt'" size="6" maxlength="14"/>
                <ww:textfield name="'developerLocation'" cssClass="'txt'" size="43" maxlength="80"/>
            </td>
        </tr>
        <tr>
            <td>Subrejestr<span class="star">*</span>:</td>
            <td><ww:textfield name="'registryNo'" cssClass="'txt'" size="5" maxlength="10" /></td>
        </tr>
        <tr>
            <td>Numer kolejny w subrejestrze<span class="star">*</span>:</td>
            <td><ww:textfield name="'sequenceId'" cssClass="'txt'" size="8" maxlength="8"/></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <i>Zmiana numeru subrejestru lub numeru kolejnego spowoduje ponowne
                wygenerowanie sugerowanego znaku wniosku.</i>
            </td>
        </tr>
        <tr>
            <td>Sugerowany znak wniosku<span class="star">*</span>:</td>
            <td><ww:textfield name="'applicationNo'" cssClass="'txt'" size="50" maxlength="84"/> </td>
        </tr>
        <tr>
            <td>Powierzchnia u�ytkowa:</td>
            <td><ww:textfield name="'usableArea'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Powierzchnia zabudowy:</td>
            <td><ww:textfield name="'buildArea'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Kubatura:</td>
            <td><ww:textfield name="'volume'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Liczba izb:</td>
            <td><ww:textfield name="'rooms'" cssClass="'txt'" size="4" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Liczba mieszka�</td>
            <td><ww:textfield name="'flats'" cssClass="'txt'" size="4" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Op�ata skarbowa:</td>
            <td><ww:textfield name="'fee'" cssClass="'txt'" size="4" maxlength="6"/></td>
        </tr>
        <tr>
            <td valign="top">Uwagi:</td>
            <td><ww:textarea name="'remarks'" rows="4" cols="50" cssClass="'txt'" /></td>
        </tr>

        <tr>
            <td colspan="2">
                <ds:submit-event value="'Utw�rz wniosek'" name="'doCreate'"/>
            </td>
        </tr>
    </table>

</form>
</ww:if>