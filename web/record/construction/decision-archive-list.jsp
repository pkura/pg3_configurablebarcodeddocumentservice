<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N decision-archive-list.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ListaDecyzjiArchiwalnych"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/record/construction/decision-archive-list.action'"/>" method="post">
	<ww:hidden name="'startSubmitDate'" value="startSubmitDate"/>
	<ww:hidden name="'endSubmitDate'" value="endSubmitDate"/>
	<ww:hidden name="'startDecisionDate'" value="startDecisionDate"/>
	<ww:hidden name="'endDecisionDate'" value="endDecisionDate"/>
	<ww:hidden name="'name'" value="name"/>
	<ww:hidden name="'address'" value="address"/>
	<ww:hidden name="'object_kind'" value="object_kind"/>
	<ww:hidden name="'proposition_no'" value="proposition_no"/>
	<ww:hidden name="'decision_no'" value="decision_no"/>
	<ww:hidden name="'documentId'" value="documentId"/>
	<table class="order table100p" cellspacing="0" id="mainTable">
		<tr>
			<th></th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="Nazwa"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="Adres"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="RodzajObjektu"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="NrWniosku"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="DataWniosku"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="NrDecyzji"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="DataDecyzji"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="InformacjaZlozonymOdwolaniu"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="InformacjaOdecyzjiWydanejWtrybieOdwolawczym"/>
			</th>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="Uwagi"/>
			</th>
		</tr>
		<ww:iterator value="lista">
			<tr>
				<td>
					<ww:checkbox id="1" name="'documentIds'" fieldValue="decision_no"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="name"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="address"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="object_kind"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="proposition_no"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="proposition_in_date"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="decision_no"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="decision_date"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="reference"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="decision_after"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center">
					<ww:property value="notice"/>
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<input type="submit" name="doPdf" value="<ds:lang text="GenerujPdf"/>" class="btn" onclick="generatePdf()"/>
</form>

<%--
<h1><ds:lang text="ListaDecyzjiArchiwalnych"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>
<form id="form" action="<ww:url value="'/record/construction/decision-archive-list.action'"/>" method="post">
<ww:hidden name="'startSubmitDate'" value="startSubmitDate"/>
<ww:hidden name="'endSubmitDate'" value="endSubmitDate"/>
<ww:hidden name="'startDecisionDate'" value="startDecisionDate"/>
<ww:hidden name="'endDecisionDate'" value="endDecisionDate"/>
<ww:hidden name="'name'" value="name"/>
<ww:hidden name="'address'" value="address"/>
<ww:hidden name="'object_kind'" value="object_kind"/>
<ww:hidden name="'proposition_no'" value="proposition_no"/>
<ww:hidden name="'decision_no'" value="decision_no"/>
<ww:hidden name="'documentId'" value="documentId"/>
	<table class="order" width="100%" cellspacing="0" id="mainTable" align="center">
		<tr>
			<td></td>
			<td></td>
			<td align="center"><b><ds:lang text="Nazwa"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="Adres"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="RodzajObjektu"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="NrWniosku"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="DataWniosku"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="NrDecyzji"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="DataDecyzji"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="InformacjaZlozonymOdwolaniu"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="InformacjaOdecyzjiWydanejWtrybieOdwolawczym"/></b></td>
			<td/>
			<td align="center"><b><ds:lang text="Uwagi"/></b></td>
		</tr>
		<ww:iterator value="lista">
			<tr>
				<td><ww:checkbox id="1" name="'documentIds'" fieldValue="decision_no"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="name"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="address"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="object_kind"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="proposition_no"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="proposition_in_date"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="decision_no"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="decision_date"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="reference"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="decision_after"/></td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td align="center"><ww:property value="notice"/></td>
			</tr>
		</ww:iterator>
			<tr>
				<td><input type="submit" name="doPdf" value="<ds:lang text="GenerujPdf"/>" class="btn" onclick="generatePdf()"/></td>
			</tr>
	</table><!--
	<ww:set name="pager" scope="request" value="pager"/>
	<table width="100%">
	<tr>
		<td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
	</tr>
	</table>-->
</form>
--%>
<script type="text/javascript">
	function generatePdf()
	{
		var x = document.getElementsByName('documentIds');
		var i=0;
		var selected=false;
		var str = '';
		for(i=0;i<x.length;i++ )
		{
			if(x.item(i).checked)
			{
				str = str+'&documentIds='+x.item(i).value;
				selected=true;
			}
		}
		if(!selected)
			alert("Zostanie wygenerowany raport z ca�ego zakresu wyszukania");
	}
	var prefix = "check";
	tableId="table";

	prepareCheckboxes();
</script>
<!--N decision-archive-list.jsp N-->