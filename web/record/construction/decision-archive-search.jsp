<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N decision-archive-search.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Architektura - wyszukiwanie archiwalnych wniosk�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form action="<ww:url value="'/record/construction/decision-archive-list.action'"/>" method="post" id="form" >
		<table class="tableMargin">
			<tr>
				<td>
					Nazwa:
				</td>
				<td>
					<ww:textfield name="'name'" cssClass="'txt'" size="20" maxlength="20"/>
				</td>
			</tr>
			<tr>
				<td>
					Rodzaj objektu:
				</td>
				<td>
					<ww:textfield name="'object_kind'" cssClass="'txt'" size="20" maxlength="84"/>
				</td>
			</tr>
			<tr>
				<td>
					Nr wniosku:
				</td>
				<td>
					<ww:textfield name="'proposition_no'" cssClass="'txt'" size="20" maxlength="84"/>
				</td>
			</tr>
			<tr>
				<td>
					Nr decyzji:
				</td>
				<td>
					<ww:textfield name="'decision_no'" cssClass="'txt'" size="20" maxlength="84"/>
				</td>
			</tr>
			<tr>
				<td>
					Data wp�ywu wniosku:
				</td>
				<td>
					Od:
					<ww:textfield name="'startSubmitDate'" id="startSubmitDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_startSubmitDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endSubmitDate'" id="endSubmitDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_endSubmitDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Data decyzji:
				</td>
				<td>
					Od:
					<ww:textfield name="'startDecisionDate'" id="startDecisionDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_startDecisionDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endDecisionDate'" id="endDecisionDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_endDecisionDate_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
		</table>
		
		<ds:submit-event value="'Szukaj'" name="'doSearch'"/>
	</form>

	<script type="text/javascript">
		Calendar.setup({
			inputField	:	"startSubmitDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_startSubmitDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	:	"endSubmitDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_endSubmitDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	:	"startDecisionDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_startDecisionDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	:	"endDecisionDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_endDecisionDate_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	</script>
</ww:if>

<ww:else>
	<table class="search table100p tableMargin" cellspacing="0">
		<tr>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('applicationNo', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Nr wniosku
					<a href="<ww:url value="getSortLink('applicationNo', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('decisionNo', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Nr decyzji
					<a href="<ww:url value="getSortLink('decisionNo', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('accepted', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Decyzja
					<a href="<ww:url value="getSortLink('accepted', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('submitDate', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Data wniosku
					<a href="<ww:url value="getSortLink('submitDate', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('decisionDate', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Data decyzji
					<a href="<ww:url value="getSortLink('decisionDate', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
		</tr>
	
		<ww:iterator value="results">
			<tr>
				<td>
					<a href="<ww:url value="'/record/construction/construction-application.action'"><ww:param name="'id'" value="id"/></ww:url>">
						<ww:property value="applicationNo"/></a>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:if test="decisionNo != null">
						<a href="<ww:url value="'/record/construction/construction-decision.action'"><ww:param name="'id'" value="constructionDecision.id"/></ww:url>">
							<ww:property value="decisionNo"/><a/>
					</ww:if>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ww:if test="accepted">pozytywna</ww:if>
					<ww:elseif test="accepted != null and !accepted">odmowna</ww:elseif>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ds:format-date value="submitDate" pattern="dd-MM-yy"/>
				</td>
				<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				<td>
					<ds:format-date value="decisionDate" pattern="dd-MM-yy"/>
				</td>
			</tr>
		</ww:iterator>
	</table>

	<p>
		Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<option value="<ww:url value="printApplicationsUrl"/>">rejestr wniosk�w</option>
			<option value="<ww:url value="printDecisionsUrl"/>">rejestr decyzji</option>
			<option value="<ww:url value="printABUrl"/>">rejestr zbiorczy</option>
		</select>
		<input type="button" value="Wydruk" class="btn"
			onclick="if (document.getElementById('printSel').value == '') { alert('Nie wybrano rodzaju wydruku'); return false; }; window.open(document.getElementById('printSel').value, null, '')"/>
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div>
</ww:else>

<%--
<h1>Architektura - wyszukiwanie archiwalnych wniosk�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form action="<ww:url value="'/record/construction/decision-archive-list.action'"/>" method="post" id="form" >

		<table>
			<tr>
				<td>Nazwa:</td>
				<td><ww:textfield name="'name'" cssClass="'txt'" size="20" maxlength="20"/></td>
			</tr>
			<tr>
				<td>Rodzaj objektu:</td>
				<td><ww:textfield name="'object_kind'" cssClass="'txt'" size="20" maxlength="84"/> </td>
			</tr>
			<tr>
				<td>Nr wniosku:</td>
				<td><ww:textfield name="'proposition_no'" cssClass="'txt'" size="20" maxlength="84"/> </td>
			</tr>
			<tr>
				<td>Nr decyzji:</td>
				<td><ww:textfield name="'decision_no'" cssClass="'txt'" size="20" maxlength="84"/> </td>
			</tr>
			<tr>
				<td>Data wp�ywu wniosku:</td>
				<td>
					Od:
					<ww:textfield name="'startSubmitDate'" id="startSubmitDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_startSubmitDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endSubmitDate'" id="endSubmitDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_endSubmitDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>Data decyzji:</td>
				<td>
					Od:
					<ww:textfield name="'startDecisionDate'" id="startDecisionDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_startDecisionDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'endDecisionDate'" id="endDecisionDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_endDecisionDate_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<ds:submit-event value="'Szukaj'" name="'doSearch'"/>
				</td>
			</tr>
		</table>


	</form>

	<script type="text/javascript">
		Calendar.setup({
			inputField	 :	"startSubmitDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_startSubmitDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	 :	"endSubmitDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_endSubmitDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	 :	"startDecisionDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_startDecisionDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	 :	"endDecisionDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_endDecisionDate_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});

	</script>
</ww:if>
<ww:else>
	<table class="search" width="100%" cellspacing="0">
	<tr>
		<th><nobr>
			<a href="<ww:url value="getSortLink('applicationNo', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Nr wniosku
			<a href="<ww:url value="getSortLink('applicationNo', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('decisionNo', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Nr decyzji
			<a href="<ww:url value="getSortLink('decisionNo', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('accepted', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Decyzja
			<a href="<ww:url value="getSortLink('accepted', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('submitDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Data wniosku
			<a href="<ww:url value="getSortLink('submitDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
		<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
		<th><nobr>
			<a href="<ww:url value="getSortLink('decisionDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
			Data decyzji
			<a href="<ww:url value="getSortLink('decisionDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
		</nobr></th>
	</tr>

	<ww:iterator value="results">
		<tr>
			<td><a href="<ww:url value="'/record/construction/construction-application.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="applicationNo"/></a></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td>
				<ww:if test="decisionNo != null">
					<a href="<ww:url value="'/record/construction/construction-decision.action'"><ww:param name="'id'" value="constructionDecision.id"/></ww:url>"><ww:property value="decisionNo"/><a/>
				</ww:if>
			</td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td>
				<ww:if test="accepted">pozytywna</ww:if>
				<ww:elseif test="accepted != null and !accepted">odmowna</ww:elseif>
			</td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ds:format-date value="submitDate" pattern="dd-MM-yy"/></td>
			<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
			<td><ds:format-date value="decisionDate" pattern="dd-MM-yy"/></td>
		</tr>
	</ww:iterator>
	</table>

	<p>Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<option value="<ww:url value="printApplicationsUrl"/>">rejestr wniosk�w</option>
			<option value="<ww:url value="printDecisionsUrl"/>">rejestr decyzji</option>
			<option value="<ww:url value="printABUrl"/>">rejestr zbiorczy</option>
		</select>
		<input type="button" value="Wydruk" class="btn"
			onclick="if (document.getElementById('printSel').value == '') { alert('Nie wybrano rodzaju wydruku'); return false; }; window.open(document.getElementById('printSel').value, null, '')" />
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<table width="100%">
	<tr>
		<td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
	</tr>
	</table>
</ww:else>
--%>
<!--N decision-archive-search.jsp N-->