<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Architektura</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>
	<a href="<ww:url value="'/record/construction/construction-search.action'"/>">
		Wyszukiwanie
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
    Wyszukiwanie wniosk�w
</p>

<p>
	<a href="<ww:url value="'/record/construction/decision-archive-search.action'"/>">
		Wyszukiwarka archiwalnych decyzji
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
    Wyszukiwarka archiwalnych decyzji
</p>
<p>
	<a href="<ww:url value="'/record/construction/decision-archive-import.action'"/>">
		Import decyzji
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
    Import decyzji
</p>
<p>
	<a href="<ww:url value="'/record/construction/decision-archive-list.action'"/>">
		Lista zaimportowanych decyzji
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
    Lista zaimportowanych decyzji
</p>

<ds:subject admin="true">
    <p>
    	<a href="<ww:url value="'/record/construction/construction-settings.action'"/>">
    		Konfiguracja
    		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
    	<br/>
        Opcje konfiguracyjne
    </p>
</ds:subject>
<!--N koniec main.jsp N-->