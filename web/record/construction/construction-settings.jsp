<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N construction-settings.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestr wniosk�w o pozwolenia na budow� - ustawienia</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/construction/construction-settings.action'"/>" method="post" enctype="multipart/form-data">
	<table class="tableMargin">
		<tr>
			<td>
				Szablon decyzji pozytywnej:
			</td>
			<td>
				<ww:if test="acceptTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="acceptTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'acceptTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Szablon decyzji odmownej:
			</td>
			<td>
				<ww:if test="unacceptTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="unacceptTemplateId"/></ww:url>">
						pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'unacceptTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="'Zapisz'" name="'doUpdate'"/>
</form>

<%--
<h1>Rejestr wniosk�w o pozwolenia na budow� - ustawienia</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/construction/construction-settings.action'"/>" method="post"
	enctype="multipart/form-data">

	<table>
		<tr>
			<td>Szablon decyzji pozytywnej:</td>
			<td><ww:if test="acceptTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="acceptTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'acceptTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Szablon decyzji odmownej:</td>
			<td><ww:if test="unacceptTemplateId != null">
					<a href="<ww:url value="'/resources/view-resource.action'"><ww:param name="'id'" value="unacceptTemplateId"/></ww:url>">pobierz</a>
					lub wybierz inny plik
				</ww:if>
				<ww:file name="'unacceptTemplate'" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<ds:submit-event value="'Zapisz'" name="'doUpdate'"/>
			</td>
		</tr>
	</table>

</form>
--%>
<!--N koniec construction-settings.jsp N-->