<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N decision-archive-import.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ImportDecyzji"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/record/construction/decision-archive-import.action'"/>" method="post" enctype="multipart/form-data">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="PlikZDecyzjami"/>:
			</td>
			<td>
				<ww:file name="'file'" id="file" cssClass="'txt'" size="50"/>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<ds:submit-event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<ds:lang text="ImportowanyPlikMusiBycWformacieCSVPoszczegolnePolaPowinnyBycRozdzieloneSrednikami"/>
			</td>
		</tr>
		<tr>
			<b>Nazwa; Adres; Typ_obiektu; Numer_wniosku; Data_wpłynięcia; Numer_decyzji; Data_decyzji; Odwołanie; Decyzja_po; Wzmianka</b>
		</tr>
	</table>
</form>

<%--
<h1><ds:lang text="ImportDecyzji"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />



<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/record/construction/decision-archive-import.action'"/>" method="post" enctype="multipart/form-data">

	<table>
		<tr>
			<td><ds:lang text="PlikZDecyzjami"/>:</td>
			<td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
		</tr>
		<tr>
			<td></td>
			<td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
		</tr>
		<tr>
			<td></td>
			<td><ds:lang text="ImportowanyPlikMusiBycWformacieCSVPoszczegolnePolaPowinnyBycRozdzieloneSrednikami"/></td>
		</tr>
	</table>

</form>

</script>
--%>
<!--N decision-archive-import.jsp N-->