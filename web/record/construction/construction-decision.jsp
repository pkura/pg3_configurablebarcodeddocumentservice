<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N construction-decision.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Decyzja o zezwoleniu na budow�</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/construction/construction-decision.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

    <ww:hidden name="'id'"/>
	<ww:if test="id != null">
    <table>
        <tr>
            <td>Wniosek:</td>
            <td><a href="<ww:url value="'/record/construction/construction-application.action'"><ww:param name="'id'" value="applicationId"/></ww:url>"><ww:property value="applicationNo"/> z <ds:format-date
                value="applicationSubmitDate" pattern="dd-MM-yy"/></a></td>
        </tr>
        <tr>
            <td>Rodzaj decyzji:</td>
            <td><ww:if test="cd.accepted">pozytywna</ww:if><ww:else>odmowna</ww:else></td>
        </tr>
        <tr>
            <td>Data decyzji:</td>
            <td><ds:format-date value="cd.date" pattern="dd-MM-yy"/></td>
        </tr>
        <tr>
            <td>Subrejestr:</td>
            <td><ww:property value="cd.registryNo"/></td>
        </tr>
        <tr>
            <td>Numer kolejny w subrejestrze:</td>
            <td><ww:property value="cd.sequenceId"/></td>
        </tr>
        <tr>
            <td>Numer decyzji w subrejestrze:</td>
            <td><ww:textfield name="'decisionNoInRegistry'" cssClass="'txt'" size="16" maxlength="16"/></td>
        </tr>
        <tr>
            <td>Numer decyzji:</td>
            <td><ww:textfield name="'decisionNo'" cssClass="'txt'" size="16" maxlength="84"/></td>
        </tr>
        <tr>
            <td>Za�atwiaj�cy spraw�:</td>
            <td>
                <ww:select name="'operator'" list="users" listKey="name"
                    listValue="asLastnameFirstname()" value="operator"
                    headerKey="''" headerValue="'-- brak --'"
                    cssClass="'sel'" disabled="!canSetClerk"/>
            </td>
        </tr>
        <tr>
            <td>Uwagi:</td>
            <td><ww:textarea name="'remarks'" rows="4" cols="50" cssClass="'txt'" /></td>
        </tr>
        <ww:if test="cd.accepted != null">
            <tr>
                <td>Szablon odpowiedzi:</td>
                <td><a href="<ww:url value="'/record/construction/construction-decision.action'"><ww:param name="'id'" value="cd.id"/><ww:param name="'doTemplateRTF'" value="true"/></ww:url>">pobierz</a></td>
            </tr>
        </ww:if>
        <tr>
            <td colspan="2">
                <ds:submit-event value="'Zapisz'" name="'doUpdate'"/>
                <input type="button" value="Utw�rz odpowied�" class="btn"
                    onclick="document.location.href='<ww:url value='&apos;/office/outgoing/new.action?inDocumentId=&apos;'/><ww:property value='cd.constructionApplication.inDocumentId'/>&rcConstructionApplicationId=<ww:property value="cd.constructionApplication.id"/>';" />
                <input type="button" value="Usu� decyzj�" class="btn" name="doRemove"
                   onclick="if (!confirm('Na pewno usun�� decyzj�?')) return false;document.location.href = '<ww:url value="'/record/construction/remove-construction-decision.action'"><ww:param name="'id'" value="cd.id"/></ww:url>';"/>

                  
               <!-- <ds:submit-event value="'Usu�'" name="'doDelete'" confirm="'Na pewno usun�� decyzj�?'"/> -->

            </td>
        </tr>
    </table>
    </ww:if>
    <ww:else>
    	<p>Przejd� do <a href="<ww:url value="'/office/tasklist/current-user-task-list.action'"/>">listy zada�</a></p>
    </ww:else>

</form>
