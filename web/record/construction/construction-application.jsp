<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N construction-application.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Wniosek o zezwolenie na budow�</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/construction/construction-application.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

    <ww:hidden name="'id'"/>

	<ww:if test="id != null">
    <table>
        <ww:if test="ca.withdrawn">
            <tr>
                <td colspan="2">
                    <b>Wniosek wycofany</b>
                </td>
            </tr>
        </ww:if>
        <tr>
            <td>Data wp�ywu:</td>
            <td><ds:format-date value="ca.submitDate" pattern="dd-MM-yy"/></td>
        </tr>
        <tr>
            <td>Subrejestr:</td>
            <td><ww:property value="ca.registryNo"/></td>
        </tr>
        <tr>
            <td>Numer kolejny w subrejestrze:</td>
            <td><ww:property value="ca.sequenceId"/></td>
        </tr>
        <tr>
            <td>Numer wniosku:</td>
            <td><ww:property value="ca.applicationNo"/></td>
        </tr>
        <tr>
        	<td>U�ytkownik tworz�cy:</td>
        	<td><ww:property value="author.asFirstnameLastname()"/></td>
        </tr>
        <tr>
        	<td>Referent:</td>
        	<td>
        		<ww:select name="'assignedUser'" list="clerks" listKey="name"
                	listValue="asLastnameFirstname()" value="assignedUser"
                	headerKey="''" headerValue="'-- brak --'"
                	cssClass="'sel'" disabled="!canSetClerk"/>
            </td>
        </tr>
        <tr>
            <td>Opis:</td>
            <td><ww:textfield name="'summary'" cssClass="'txt'" size="50" maxlength="200"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>Inwestor</h3>
            </td>
        </tr>
        <tr>
            <td>Nazwa:</td>
            <td><ww:textfield name="'developerName'" cssClass="'txt'" size="50" maxlength="80"/> </td>
        </tr>
        <tr>
            <td>Adres:</td>
            <td><ww:textfield name="'developerStreet'" cssClass="'txt'" size="50" maxlength="80"/> </td>
        </tr>
        <tr>
            <td>Kod pocztowy i miejscowo��:</td>
            <td><ww:textfield name="'developerZip'" id="developerZip" cssClass="'txt'" size="6" maxlength="14"/>
                <ww:textfield name="'developerLocation'" cssClass="'txt'" size="43" maxlength="80"/>
            </td>
        </tr>
        <tr>
            <td>Powierzchnia u�ytkowa:</td>
            <td><ww:textfield name="'usableArea'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Powierzchnia zabudowy:</td>
            <td><ww:textfield name="'buildArea'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Kubatura:</td>
            <td><ww:textfield name="'volume'" cssClass="'txt'" size="9" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Liczba izb:</td>
            <td><ww:textfield name="'rooms'" cssClass="'txt'" size="4" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Liczba mieszka�</td>
            <td><ww:textfield name="'flats'" cssClass="'txt'" size="4" maxlength="24"/></td>
        </tr>
        <tr>
            <td>Op�ata skarbowa:</td>
            <td><ww:textfield name="'fee'" cssClass="'txt'" size="4" maxlength="6"/></td>
        </tr>
        <tr>
            <td>Uwagi:</td>
            <td><ww:textarea name="'remarks'" rows="4" cols="50" cssClass="'txt'" /></td>
        </tr>
        <ww:if test="hasDecision">
            <tr>
                <td>Decyzja:</td>
                <td><a href="<ww:url value="'/record/construction/construction-decision.action'"><ww:param name="'id'" value="decisionId"/></ww:url>"><ww:property value="decisionNo"/> z <ds:format-date
                    value="decisionDate" pattern="dd-MM-yy"/></a></td>
            </tr>
        </ww:if>
        <tr>
            <td colspan="2">
                <ds:submit-event value="'Zapisz'" name="'doUpdate'" disabled="ca.withdrawn"/>
                <%--<ds:submit-event value="'Wydaj decyzj�'" name="'doIssueDecision'"/>--%>
                <ww:if test="!hasDecision and !ca.withdrawn">
                    <input type="button" value="Umie�� w rejestrze decyzji" class="btn"
                        onclick="document.location.href = '<ww:url value="'/record/construction/create-construction-decision.action'"><ww:param name="'applicationId'" value="ca.id"/></ww:url>';"/>
                </ww:if>
                <ds:submit-event value="'Wycofaj wniosek'" name="'doWithdraw'" confirm="'Na pewno wycofa� wniosek?'" disabled="ca.withdrawn"/>
                <ds:submit-event value="'Przywr�� wniosek'" name="'doUnwithdraw'" disabled="!ca.withdrawn"/>
                <input type="button" value="Usu� wniosek" class="btn" name="doRemove"
                        onclick="if (!confirm('Na pewno usun�� wniosek?')) return false;document.location.href = '<ww:url value="'/record/construction/remove-construction-application.action'"><ww:param name="'id'" value="ca.id"/></ww:url>';"/>
                   


                <%--<ds:submit-event value="'Usu� wniosek'" name="'doDelete'" confirm="'Na pewno usun�� wniosek?'" disabled="hasDecision"/>--%>
                <!--<ds:submit-event value="'Usu� wniosek'" name="'doDelete'" confirm="'Na pewno usun�� wniosek?'" disabled="true"/>-->

            </td>
        </tr>
    </table>
    </ww:if>
    <ww:else>
    	<p>Przejd� do <a href="<ww:url value="'/office/tasklist/current-user-task-list.action'"/>">listy zada�</a></p>
    </ww:else>

</form>
