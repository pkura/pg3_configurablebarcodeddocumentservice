<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N create-construction-decision.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Decyzja o zezwoleniu na budow�</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/record/construction/create-construction-decision.action'"/>"
    method="post" onsubmit="disableFormSubmits(this);">

    <ww:hidden name="'applicationId'"/>

    <ww:hidden name="'registryNoVerify'" value="registryNo"/>
    <ww:hidden name="'sequenceIdVerify'" value="sequenceId"/>
    <ww:hidden name="'suggestedDecisionNo'" value="suggestedDecisionNo"/>
    <ww:hidden name="'submitted'" value="true"/>

    <table>
        <tr>
            <td>Wniosek:</td>
            <td><ww:property value="applicationNo"/> z <ds:format-date value="applicationSubmitDate" pattern="dd-MM-yy"/></td>
        </tr>
        <tr>
            <td>Rodzaj decyzji<span class="star">*</span>:</td>
            <td><input type="radio" name="accepted" value="true" <ww:if test="accepted">checked="true"</ww:if> /> pozytywna
                <input type="radio" name="accepted" value="false" <ww:if test="accepted != null and !accepted">checked="true"</ww:if>/> odmowna
                <!-- neutralna :-) -->
            </td>
        </tr>
        <tr>
            <td>Data decyzji<span class="star">*</span>:</td>
            <td><ww:textfield name="'date'" id="date" cssClass="'txt'" size="10" maxlength="10"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="dateTrigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td>Subrejestr:</td>
            <td><ww:property value="registryNo"/></td>
        </tr>
        <tr>
            <td>Numer kolejny w subrejestrze<span class="star">*</span>:</td>
            <td><ww:textfield name="'sequenceId'" cssClass="'txt'" size="8" maxlength="8"/></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <i>Zmiana numeru subrejestru lub numeru kolejnego spowoduje ponowne
                wygenerowanie sugerowanego znaku wniosku.</i>
            </td>
        </tr>
        <tr>
            <td>Numer decyzji w subrejestrze:</td>
            <td><ww:textfield name="'decisionNoInRegistry'" cssClass="'txt'" size="16" maxlength="16"/></td>
        </tr>
        <tr>
            <td>Sugerowany znak decyzji:</td>
            <td><ww:property value="suggestedDecisionNo"/></td>
        </tr>
        <tr>
            <td>Numer decyzji<span class="star">*</span>:</td>
            <td><ww:textfield name="'decisionNo'" cssClass="'txt'" size="16" maxlength="84"/></td>
        </tr>
        <tr>
            <td>Za�atwiaj�cy spraw�:</td>
            <td>
                <ww:select name="'operator'" list="users" listKey="name"
                    listValue="asLastnameFirstname()" value="operator"
                    headerKey="''" headerValue="'-- brak --'"
                    cssClass="'sel'" disabled="!canSetClerk"/>
            </td>
        </tr>
        <tr>
            <td valign="top">Uwagi:</td>
            <td><ww:textarea name="'remarks'" rows="4" cols="50" cssClass="'txt'" /></td>
        </tr>

        <tr>
            <td colspan="2">
                <ds:submit-event value="'Utw�rz decyzj�'" name="'doCreate'"/>
            </td>
        </tr>
    </table>

</form>

<script type="">
    Calendar.setup({
        inputField     :    "date",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "dateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>
