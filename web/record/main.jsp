<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Rejestry</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:modules test="invoices">
    <p><a href="<ww:url value="'/record/invoices/main.action'"/>">Faktury <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
        Rejestr faktur
    </p>
</ds:modules>

<ds:modules test="contracts">
    <p><a href="<ww:url value="'/record/contracts/main.action'"/>">Umowy <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
        Rejestr um�w
    </p>
</ds:modules>
