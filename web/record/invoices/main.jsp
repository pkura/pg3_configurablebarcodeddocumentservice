<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N main.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Faktury</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<p>
	<a href="<ww:url value="'/record/invoices/edit-main.action'"/>">
		Nowa faktura
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Dodanie nowej faktury
</p>

<p>
	<a href="<ww:url value="'/record/invoices/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>">
		Wyszukiwanie
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wyszukiwanie faktur
</p>

<p>
	<a href="<ww:url value="'/record/invoices/search.action'"><ww:param name="'doMyInvoice'" value="'true'"/></ww:url>">
		Faktury do mojej akceptacji
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Lista faktur wprowadzonych przeze mnie
</p>

<p>
	<a href="<ww:url value="'/record/invoices/search.action'"><ww:param name="'doSearch'" value="'true'"/></ww:url>">
		Wszystkie faktury
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wyszukanie wszystkich faktur
</p>
<!--N koniec main.jsp N-->