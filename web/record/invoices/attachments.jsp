<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N attachments.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean,
                 pl.compan.docusafe.core.office.InOfficeDocument"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Faktura</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canView">

<form action="<ww:url value="baseLink"/><ww:param name="saveFile" value="saveFile"/>" method="post" enctype="multipart/form-data">


    <ww:hidden name="'id'" value="id"/>
    <ww:hidden name="'tmp_documentId'" value="tmp_documentId"/>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'attachmentId'" value="attachmentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'selectedAttachmentId'" value="selectedAttachmentId"/>



    <ww:token />

    <ww:if test="attachment != null">

        <table border="0" cellspacing="0" cellpadding="0">
            <ww:if test="attachment.mostRecentRevision != null">
                <tr>
                    <td>Tytu�<span class="star">*</span>:</td>
                    <td><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'" value="attachment.title"/></td>
                </tr>
            </ww:if>
            <ww:else>
                <tr>
                    <td>Tytu�:</td>
                    <td><ww:hidden name="'title'" id="title" value="attachment.title"/><ww:property value="attachment.title"/></td>
                </tr>
            </ww:else>
            <tr>
                <td>Kod kreskowy</td>
                <td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" value="attachment.barcode"/></td>
            </tr>
            <!-- tworz�cy u�ytkownik -->
            <tr>
                <td colspan="2">
                    <ds:event name="'doUpdate'" value="'Zapisz'"
                              onclick="'if (!validateForm1()) return false'"
                              disabled="readOnly || !canUpdate"/>
                </td>
            </tr>
        </table>

        <p></p>

        <!-- lista wersji -->

        <ww:if test="!revisionsReversed.empty" >
            <h4>Wersje za��cznika</h4>

            <table>
            <tr>
                <th>Autor</th>
                <th>Rozmiar</th>
                <th>Data</th>
                <th>Wersja</th>
                <th></th>
            </tr>

            <ww:iterator value="revisionsReversed" status="status">
                <tr>
<%--                    <td>&nbsp;<ww:property value="#u.firstname + ' ' + #u.lastname"/></td>--%>
                    <td>&nbsp;<ww:property value="author.asFirstnameLastname()"/></td>
                    <td>&nbsp;<ds:format-size value="#this['size']"/></td>
                    <td>&nbsp;<ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
                    <td align="center"><ww:property value="revision"/></td>
                    <td>&nbsp;<a <ww:if test="!#status.first">onclick="return confirm('To nie jest najnowsza wersja za��cznika, kontynuowa�?')"</ww:if> href="<ww:url value="'/repository/view-attachment-revision.do?id='+id"/>">pobierz t� wersj�</a></td>
                </tr>
            </ww:iterator>
            </table>
        </ww:if>

        <ww:file name="'revisionFile'" id="revisionFile" size="50" cssClass="'txt'"/>
        <ds:event name="'doAddRevision'" value="'Nowa wersja pliku'"
                  onclick="'if (!validateForm3()) return false'"
                  disabled="readOnly || !canUpdate"/>

        <p></p>

        <input type="button" value="Powr�t do listy za��cznik�w" class="btn"
            onclick="document.location.href='<ww:url value="baseLink"><ww:param name="'id'" value="id"/><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>';"/>

    </ww:if>

    <ww:elseif test="!attachmentBeans.empty">

            <table>
            <tr>
                <th></th>
                <th>Tytu�</th>
                <th>Autor</th>
                <th>Rozmiar</th>
                <th>Data</th>
                <th>Wersja</th>
                <th>Kod kreskowy</th>
                <th></th>
                <th></th>
            </tr>
            <ww:iterator value="attachmentBeans">
<%--                <ds:set-user name="u" username="author" />--%>
                <ww:if test="mostRecentRevision != null">
                    <tr>
                        <td><ww:checkbox name="'attachmentIds'" fieldValue="id" /></td>
                        <td><a href="<ww:url value="baseLink"><ww:param name="'id'" value="invoiceId"/><ww:param name="'attachmentId'" value="id"/><ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>"><ww:property value="title"/></a></td>
                        <td>&nbsp;<ww:property value="author.asFirstnameLastname()"/></td>
                        <td>&nbsp;<ds:format-size value="mostRecentRevision.size"/></td>
                        <td>&nbsp;<ds:format-date value="mostRecentRevision.ctime" pattern="dd-MM-yy HH:mm"/></td>
                        <td align="center">&nbsp;<ww:property value="mostRecentRevision.revision"/></td>
                        <td>&nbsp;<ww:property value="barcode"/></td>
                        <td valign="top">&nbsp;<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+mostRecentRevision.id"/>">pobierz</a>
                        <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mostRecentRevision.mime)">
                            <td valign="top">&nbsp;<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">wy�wietl</a></td>
                        </ww:if>
                        </td>
                    </tr>
                </ww:if>
                <ww:else>
                    <tr>
                        <td></td>
                        <td><a href="<ww:url value="baseLink"><ww:param name="'id'" value="invoiceId"/><ww:param name="'attachmentId'" value="id"/><ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>"><ww:property value="title"/></a></td>
                        <td>&nbsp;<ww:property value="#u.firstname + ' ' + #u.lastname"/></td>
                        <td colspan="3">
                            <ww:if test="lparam != null">
                                <b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)"/></i></b>
                            </ww:if>
                            <ww:else>
                                <i>(pusty)</i>
                            </ww:else>
                        </td>
                        <td>&nbsp;<ww:property value="barcode"/></td>
                        <td>
                            <select name="markAttachment[&quot;<ww:property value="id"/>&quot;]" class="sel">
                                <option value="">-- wybierz --</option>
                                <option value="<%= InOfficeDocument.ATTACHMENT_SEEN %>">przedstawiono do wgl�du</option>
                                <option value="<%= InOfficeDocument.ATTACHMENT_FILED %>">posiadany</option>
                                <option value="<%= InOfficeDocument.ATTACHMENT_INSPE %>">do okazania/uregulowania przy odbiorze</option>
                                <option value="<%= InOfficeDocument.ATTACHMENT_MISSING %>">brak</option>
                                <%--<option value="*">usu� deklaracj�</option>--%>
                            </select>
                        </td>
                        <td></td>
                    </tr>
                </ww:else>
            </ww:iterator>
            <tr>
                <td colspan="8">
                    <ww:if test="showBtnMarkAttachments">
                        <ds:event value="'Oznacz za��czniki'" name="'doMarkAttachment'"
                            disabled="readOnly || !canUpdate"/>
                    </ww:if>
                    <ds:event name="'doDelete'" value="'Usu�'" disabled="readOnly || !canUpdate"/>
                </td>
            </tr>
            </table>


    </ww:elseif>

    <ww:if test="attachment == null">

            <p></p>
            <h4>Nowy za��cznik</h4>
            <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>Tytu�<span class="star">*</span>:</td>
                <td><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td>Kod kreskowy: </td>
                <td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td>Plik<span class="star">*</span>:</td>
                <td><ww:file name="'file'" id="file" size="50" cssClass="'txt'"/></td>
            </tr>
                <%--
                <c:if test="${!empty requiredAttachments}">
                <tr>
                    <td colspan="2">Jest to jeden z wymaganych za��cznik�w:
                        <table>
                        <c:forEach items="${requiredAttachments}" var="bean">
                            <tr>
                                <td><c:out value="${bean.title}"/></td>
                                <td><c:if test="${!bean.present}"><input type="radio" name="posn" value='<c:out value="${bean.posn}"/>'/></c:if>
                                    <c:if test="${bean.present}">posiadany</c:if>
                                </td>
                            </tr>
                        </c:forEach>
                        </table>
                    </td>
                </tr>
                </c:if>
                --%>
            <tr>
                <td></td>
                <td>
                    <ds:event name="'doAdd'" value="'Utw�rz'" disabled="readOnly || !canUpdate" 
                              onclick="'if (!validateForm2()) return false'"/>
                    <ww:if test="useScanner">
                        <script type="text/javascript">
                            var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
                                request.getServerName() + ":" + request.getServerPort() %>';
                        </script>
                        <input type="button" value="Otw�rz applet skanowania" class="btn"
                            onclick="openToolWindow('<ww:url value="'/office/incoming/attachments-applet.action'"/>?openerUrl='+encodeURIComponent(host+'<ww:url><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>')+'&documentId=<ww:property value="documentId"/>&activity='+encodeURIComponent('<ww:property value="activity"/>'), 'applet', 700, 125)"/>
                        </td>
                    </ww:if>
            </tr>
            </table>

<%--
            <ww:if test="useScanner">
                <p></p>

                <table cellspacing="0" cellpadding="0" style="border: 1px solid black; padding: 2px">
                <tr>
                    <td>Skanowanie</td>
                </tr>
                <tr>
                    <td>
                        <script>
                        var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
                            request.getServerName() + ":" + request.getServerPort() %>';
                        // u�yty mo�e by� albo plik jtwain.jar znajduj�cy si� w aplikacji,
                        // albo plik systemowy (w JRE_HOME/lib/ext)
                        var object =  appletObject('scanapplet',
                            'pl.compan.docusafe.applets.scanner.AttachmentsApplet.class',
                            '<ww:url value="'/scanner.jar'"/>, <ww:if test="!useSystemJTwain"><ww:url value="'/jtwain-8.2.jar'"/>, </ww:if> <ww:url value="'/jai_core-1.1.2_01.jar'"/>, <ww:url value="'/jai_codec-1.1.2_01.jar'"/>', 700, 55,
                            { 'uploadUrl' : host+'<ww:url value="baseLink"/>;jsessionid=<%= request.getRequestedSessionId() %>',
                              'refreshUrl' : host+'<ww:url><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>',
                              'reloadPage' : 'true',
                              'activity' : '<ww:property value="activity"/>',
                              'documentId' : '<ww:property value="documentId"/>' },
                            new Array('xMSIE'));
                        //alert('object='+object);
                        document.write(object);
                        </script>
                    </td>
                </tr>
                </table>
            </ww:if>
--%>
    </ww:if>

</form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<script>
function validateForm1()
{
    var title = document.getElementById('title').value;
    //var revisionFile = document.getElementById('revisionFile').value;
    if (title.trim().length == 0) { alert('Nie podano tytu�u'); return false; }
    //else if (revisionFile.trim().length == 0) { alert('Nie wybrano pliku'); return false; }
    return true;
}

function validateForm3()
{
    //var title = document.getElementById('title').value;
    var revisionFile = document.getElementById('revisionFile').value;
    //if (title.trim().length == 0) { alert('Nie podano tytu�u'); return false; }
    if (revisionFile.trim().length == 0) { alert('<ds:lang text="NieWybranoPliku"/>'); return false; }
    return true;
}

function validateForm2()
{
    var title = document.getElementById('title').value;
    var file = document.getElementById('file').value;
    if (title.trim().length == 0) { alert('Nie podano tytu�u'); return false; }
    else if (file.trim().length == 0) { alert('<ds:lang text="NieWybranoPliku"/>'); return false; }
    return true;
}

<%--
function reloadPage(url)
{
    //alert('reloadPage '+url);
    document.location.href = url;
}
--%>
</script>

</ww:if>
<ww:else>
    <p style="color:red">Brak uprawnie� do podgl�du</p>
</ww:else>