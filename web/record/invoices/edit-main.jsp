<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Faktura</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canView">

<script language="Javascript">

function openContractPopup() 
{
	var x = document.getElementById('contract').value;
	var url = '<ww:url value="'/record/contracts/search.action'"/>?popup=true' +
	'&contMap=' + escape(x);   
	
	//alert('openContractPopup x='+x+', url='+url);
	openToolWindow(url, 'contract');
}

/* 
	Funkcja wywolywana z okienka contracts/search.jsp po wybraniu umowy 
*/
function __accept_contract(map)
{
	//alert('accept_contract id='+map.contractId);

 	document.getElementById('contract').value = JSONStringify(map);
    document.getElementById('contractNo').value = map.contractNo;
    document.getElementById('contractSeqId').value = map.sequenceId;
    document.getElementById('contractId').value = map.contractId;
    map.vendor = eval('(' + map.vendor + ')');
    
	if (map.vendor != null && map.vendor != undefined) {
		
		document.getElementById('vendorname').value = '';
		document.getElementById('vendorstreet').value = '';
		document.getElementById('vendorzip').value = '';
		document.getElementById('vendorlocation').value = '';
		document.getElementById('vendorcountry').value = '';
		document.getElementById('vendornip').value = '';
		document.getElementById('vendorregon').value = '';

   		setSafeValue(document.getElementById('vendorname'), map.vendor.name);
   		setSafeValue(document.getElementById('vendorstreet'), map.vendor.street);
   		setSafeValue(document.getElementById('vendorzip'), map.vendor.zip);
   		setSafeValue(document.getElementById('vendorlocation'), map.vendor.location);
   		setSafeValue(document.getElementById('vendorcountry'), map.vendor.country);
   		setSafeValue(document.getElementById('vendornip'), map.vendor.nip);
   		setSafeValue(document.getElementById('vendorregon'), map.vendor.regon);
	}

}

function updateContractMap(cId,cSeqId,cNo,cDate,cDesc)
{
	var map = new Object();

	map.contractId = cId;
    map.sequenceId = cSeqId;
    map.contractNo = cNo;
    if (map.contractNo == null || map.contractNo.length < 1)
    	map.contractNo = '<ww:property value="contractNo"/>';
    map.contractDate = cDate;
    map.description = cDesc;
    map.tmp = 'tmp';
    //alert('update id:'+map.contractId+' sid:'+map.sequenceId+' no:'+map.contractNo+' date:'+map.contractDate+' desc:'+map.description);
    
    document.getElementById('contract').value = JSONStringify(map);
    document.getElementById('contractSeqId').value = map.sequenceId;



}

function resetContract()
{
    document.getElementById('contractNo').value = '';
    document.getElementById('contractSeqId').value = '';
    document.getElementById('contractId').value = '';
    document.getElementById('contract').value = '';
}

</script>

<ww:set name="exists" value="id != null"/>

<form id="form" action="<ww:url value="'/record/invoices/edit-main.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">

<ww:hidden name="'id'" value="id"/>
<ww:hidden name="'contract'" id="contract"/>
<ww:hidden name="'contractId'" id="contractId" value="contractId"/>


<table>
    <ww:if test="#exists">	
    	<tr>
    		<td colspan="2">
            	<ww:if test="cancelled">
            		<span style="color:red">Anulowana</span>
            	</ww:if>
    		</td>
    	</tr>
        <tr>
            <td>Numer w rejestrze:</td>
            <td><b><ww:property value="invoice.sequenceId"/></b>
                <ww:if test="invoice.deleted">
                    <span style="color:red; font-weight: bold">usuni�ta</span>
                </ww:if>
            </td>
        </tr>
    </ww:if>
    <tr>
        <td>Numer faktury<span class="star">*</span>:</td>
        <td><ww:textfield name="'invoiceNo'" id="invoiceNo" size="50" maxlength="50" cssClass="'txt'"/></td>
    </tr>
    <ww:if test="#exists">
        <tr>
            <td>Data utworzenia:</td>
            <td><ds:format-date value="invoice.ctime" pattern="dd-MM-yy HH:mm"/></td>
        </tr>
        <tr>
            <td>U�ytkownik tworz�cy:</td>
            <td><ww:property value="author.asFirstnameLastname()"/></td>
        </tr>
    </ww:if>
    <tr>
        <td>Data faktury<span class="star">*</span>:</td>
        <td><ww:textfield name="'invoiceDate'" id="invoiceDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="invoiceDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Data sprzeda�y<span class="star">*</span>:</td>
        <td><ww:textfield name="'saleDate'" id="saleDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="saleDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Termin p�atno�ci<span class="star">*</span>:</td>
        <td><ww:textfield name="'paymentDate'" id="paymentDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="paymentDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Forma p�atno�ci<span class="star">*</span>:</td>
        <td><ww:select name="'termsOfPayment'" id="termsOfPayment"
            list="termsOfPaymentList" listKey="key" listValue="value"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        </td>
    </tr>
    <tr>
        <td>Rodzaj dokumentu<span class="star">*</span>:</td>
        <td><ww:select name="'kind'" id="kind" list="kindsList" listKey="id" listValue="name"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        </td>
    </tr>
    <tr>
        <td>Kwota brutto<span class="star">*</span>:</td>
        <td><ww:textfield name="'gross'" id="gross" size="20" maxlength="20" cssClass="'txt'"/></td>
    </tr>                         
    <tr>
        <td>Kwota netto<span class="star">*</span>:</td>
        <td><ww:textfield name="'net'" id="net" size="20" maxlength="20" cssClass="'txt'"/></td>
    </tr>
    <ds:available test="invoice.archive.field">
	    <tr>
	    	<td>Archiwalna</td>
	    	<td><ww:checkbox name="'archive'" fieldValue="true" /></td>
	    </tr>
	</ds:available>
    <tr>
        <td>Opis<span class="star">*</span>:</td>
        <td><ww:textfield name="'description'" id="description" size="50" maxlength="160" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <ds:modules test="contracts">
        	<td>Numer w rejestrze umowy/zlecenia:</td>
        	<td><ww:textfield name="'contractSeqId'" id="contractSeqId" size="50" maxlength="50" cssClass="'txt'" readonly="true"/>
        	<ww:if test="contractNo != null">        	
        		<ww:hidden name="'contractNo'" id="contractNo" value="contractNo"/>
        	</ww:if>
        	<ww:else><!--  bez else nie dzia�a -->
        		<ww:hidden name="'contractNo'" id="contractNo" value="''"/>
        	</ww:else>        	
        	</td>
        </ds:modules>
        <ds:modules test="!contracts">
        	<td>Numer umowy/zlecenia:</td>
        	<td><ww:textfield name="'contractNo'" id="contractNo" size="50" maxlength="50" cssClass="'txt'"/>
        	<ww:hidden name="'contractSeqId'" id="contractSeqId"/></td>
        </ds:modules>
    </tr>
<ww:if test="contractId != null">
	<tr>
    	<td></td><td colspan="2"><a href="<ww:url value="'/record/contracts/edit-main.action?id='+contractId"/>"><ww:property value="'Powi�zana umowa nr. ' +contract.sequenceId"/></a></td>
    </tr>
</ww:if>
    <ds:modules test="contracts">
	    <tr>
	    	<td></td>
	    	<td><input type="button" value="Przydziel do umowy" onclick="openContractPopup();" class="btn" >
	    	<input type="button" value="Wyczy��" onclick="resetContract();" class="btn" ></td>
	    </tr>
    </ds:modules>
    <tr>
        <td colspan="2"><h4>Sprzedawca</h4></td>
    </tr>
    <tr>
        <td>Nazwa<span class="star">*</span>:</td>
        <td><ww:textfield name="'vendor.name'" id="vendorname" size="50" maxlength="160" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td>Ulica<span class="star">*</span>:</td>
        <td><ww:textfield name="'vendor.street'" id="vendorstreet" size="50" maxlength="50" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td valign="top">Kod pocztowy i miejscowo��</td>
        <td><ww:textfield name="'vendor.zip'" size="7" maxlength="14" cssClass="'txt'" id="vendorzip"  />
            <ww:textfield name="'vendor.location'" size="39" maxlength="50" cssClass="'txt'" id="vendorlocation"  />
            <br/><span class="warning" id="vendorzipError"> </span></td>
    </tr>
    <tr>
        <td>Kraj:</td>
        <td><ww:select name="'vendorcountry'" id="vendorcountry"
            list="@pl.compan.docusafe.util.Countries@COUNTRIES"
            listKey="alpha2" listValue="name" value="'PL'"
            cssClass="'sel'" onchange="this.form.vendorzip.validator.revalidate();"
            />
        </td>
    </tr>
    <tr>
        <td>NIP:</td>
        <td><ww:textfield name="'vendor.nip'" id="vendornip" size="15" maxlength="15" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td>REGON:</td>
        <td><ww:textfield name="'vendor.regon'" id="vendorregon" size="15" maxlength="15" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td></td>
        <td><input type="button" value="Wybierz sprzedawc�" onclick="openPersonPopup('vendor', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
    </tr>
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td>Akceptacja merytoryczna:</td>
        <td><ww:select name="'acceptingUser'" id="acceptingUser"
            list="users" listKey="name" listValue="asLastnameFirstname()"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/></td>
    </tr>
    <ww:if test="newSearch">
    <tr>
    	<td>Dekretacja rachunkowa:</td>
        <td><ww:select name="'decretation'" id="decretation"
           	list="decretationsList" listKey="name" listValue="name"
           	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        </td>
    </tr>
    <tr>
    	<td>CPV:</td>
        <td><ww:select name="'cpv'" id="cpv"
            list="cpvCodesList"  listKey="name+' '+category" listValue="name+' '+category"
            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        </td>
    </tr>
    </ww:if>
    <tr>
        <td>Data przekazania do akceptacji:</td>
        <td><ww:textfield name="'acceptDate'" id="acceptDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="acceptDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Data zwrotu z akceptacji:</td>
        <td><ww:textfield name="'acceptReturnDate'" id="acceptReturnDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="acceptReturnDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
<%--
    <tr>
        <td>Opis bud�etowy:</td>
        <td><ww:textfield name="'budgetaryDescription'" id="budgetaryDescription" size="50" maxlength="50" cssClass="'txt'"/></td>
    </tr>
--%>
    <tr>
        <td>Przekazano do:</td>
        <td><ww:textfield name="'relayedTo'" id="relayedTo" size="50" maxlength="80" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td>Data przekazania do RB:</td>
        <td><ww:textfield name="'budgetaryRegistryDate'" id="budgetaryRegistryDate" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="budgetaryRegistryDateTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td>Uwagi:</td>
        <td><ww:textarea name="'remarks'" id="remarks" cols="50" rows="5" cssClass="'txt'" /></td>
    </tr>
    <ww:if test="!saveFile">
	    <tr>	        
	        <td>Tytu�:</td>
	        <td><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Kod kreskowy: </td>
	        <td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Plik:</td>
	        <td><ww:file name="'file'" id="file" size="50" cssClass="'txt'"/></td>
	    </tr>
    </ww:if>
    <tr>
        <td></td>
        <td>
            <ww:if test="#exists">
                <input type="hidden" name="doUpdate" id="doUpdate"/>
                <input type="submit" value="Zapisz" onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value='true';" class="btn"
                <ww:if test="!canUpdate">disabled="true" title="Brak uprawnie� do modyfikacji faktury"</ww:if> />
                <ww:if test="cancelled">
                	<input type="hidden" name="doUncancel" id="doUncancel"/>
                	<input type="submit" value="Przywr�� faktur�" onclick="document.getElementById('doUncancel').value='true';" class="btn"
                	<ww:if test="!canUpdate">disabled="true" title="Brak uprawnie� do modyfikacji faktury"</ww:if> />
                </ww:if>
                <ww:else>
                	<input type="hidden" name="doCancel" id="doCancel"/>
                	<input type="submit" value="Anuluj faktur�" onclick="document.getElementById('doCancel').value='true';" class="btn"
                	<ww:if test="!canUpdate">disabled="true" title="Brak uprawnie� do modyfikacji faktury"</ww:if> />
                </ww:else>
            </ww:if>
            <ww:else>
                <input type="hidden" name="doCreate" id="doCreate"/>
                <input type="submit" value="Zapisz" onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true';" class="btn"
                <ww:if test="!canCreate">disabled="true" title="Brak uprawnie� do tworzenia faktury"</ww:if> />
            </ww:else>
        </td>
    </tr>
	<script>
    	updateContractMap('<ww:property value="contract.id"/>', '<ww:property value="contract.sequenceId"/>', '<ww:property value="contract.contractNo"/>', '<ww:property value="contract.contractDate"/>', '<ww:property value="contract.description"/>');
    </script>
</table>

</form>

<script>

	if($j('#invoiceDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "invoiceDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "invoiceDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#saleDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "saleDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "saleDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#paymentDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "paymentDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "paymentDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#budgetaryRegistryDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "budgetaryRegistryDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "budgetaryRegistryDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#acceptDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "acceptDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "acceptDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#acceptReturnDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "acceptReturnDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "acceptReturnDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
    var szv = new Validator(document.getElementById('vendorzip'), '##-###');

    szv.onOK = function() {
        this.element.style.color = 'black';
        document.getElementById('vendorzipError').innerHTML = '';
    }

    szv.onEmpty = szv.onOK;

    szv.onError = function(code) {
        this.element.style.color = 'red';
        if (code == this.ERR_SYNTAX)
            document.getElementById('vendorzipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
        else if (code == this.ERR_LENGTH)
            document.getElementById('vendorzipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
    }

    szv.canValidate = function() {
        var country = document.getElementById('vendorcountry');
        var countryCode = country.options[country.options.selectedIndex].value;

        return (countryCode == '' || countryCode == 'PL');
    }

    // pocz�tkowa walidacja danych
    szv.revalidate();

    function validateForm()
    {
        var invoiceNo = document.getElementById('invoiceNo').value;
        var invoiceDate = document.getElementById('invoiceDate').value;
        var saleDate = document.getElementById('saleDate').value;
        var paymentDate = document.getElementById('paymentDate').value;
        var acceptDate = document.getElementById('acceptDate').value;
        var acceptReturnDate = document.getElementById('acceptReturnDate').value;
        var termsOfPayment = document.getElementById('termsOfPayment').value;
        var kind = document.getElementById('kind').value;
        var gross = document.getElementById('gross').value;
        var net = document.getElementById('net').value;
 //       var vat = document.getElementById('vat').value;
        var description = document.getElementById('description').value;
        var vendorname = document.getElementById('vendorname').value;
        var vendorstreet = document.getElementById('vendorstreet').value;
        var vendornip = document.getElementById('vendornip').value;
        var remarks = document.getElementById('remarks').value;
        var acceptingUser = document.getElementById('acceptingUser').value;

        if (isEmpty(invoiceNo)) { alert('Nie podano numeru faktury'); return false; }
        if (isEmpty(invoiceDate)) { alert('Nie podano daty faktury'); return false; }
        if (isEmpty(saleDate)) { alert('Nie podano daty sprzeda�y'); return false; }
        if (isEmpty(paymentDate)) { alert('Nie podano terminu p�atno�ci'); return false; }
        if (isEmpty(termsOfPayment)) { alert('Nie wybrano formy p�atno�ci'); return false; }
        if (isEmpty(kind)) { alert('Nie wybrano rodzaju (faktura/rachunek)'); return false; }
        if (isEmpty(gross) || isNaN(parseFloat(gross.replace(',', '.')))) { alert('Nie podano sumy brutto lub podana liczba jest niepoprawna'); return false; }
        if (isEmpty(net) || isNaN(parseFloat(net.replace(',', '.')))) { alert('Nie podano sumy netto lub podana liczba jest niepoprawna'); return false; }
        if (!isNaN(parseFloat(net.replace(',', '.'))) && !isNaN(parseFloat(gross.replace(',', '.'))) &&
            parseFloat(net.replace(',', '.')) > parseFloat(gross.replace(',', '.'))) { alert('Suma netto jest wi�ksza od sumy brutto'); return false; }
        //if (isEmpty(vat) || isNaN(parseInt(vat)) || parseInt(vat) < 0) { alert('Nie podano stawki VAT lub podana stawka jest niepoprawna'); return false; }
        if (isEmpty(description)) { alert('Nie podano opisu faktury'); return false; }
        if (isEmpty(vendorname)) { alert('Nie podano nazwy sprzedawcy'); return false; }
        if (isEmpty(vendorstreet)) { alert('Nie podano adresu sprzedawcy'); return false; }
        /*if (isEmpty(vendornip)) { alert('Nie podano numeru NIP sprzedawcy'); return false; }*/
        if (remarks.length && remarks.length > 512) { alert('W polu Uwagi mo�na wpisa� maksymalnie 512 znak�w'); return false; }
        if ((!isEmpty(acceptDate) || !isEmpty(acceptReturnDate)) && isEmpty(acceptingUser)) { alert('Aby ustawi� dat� akceptacji lub zwrotu akceptacji, nale�y wybra� u�ytkownika akceptuj�cego'); return false; }

        return true;
    }

    function openPersonPopup(lparam, dictionaryType)
    {
        if (lparam == 'sender')
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+
                '&organization='+document.getElementById('vendorname').value+
                '&street='+document.getElementById('vendorstreet').value+
                '&zip='+document.getElementById('vendorzip').value+
                '&location='+document.getElementById('vendorlocation').value+
                '&country='+document.getElementById('vendorcountry').value);
                //'&canAddAndSubmit=true');
        }
        else
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true&guid=<ww:property value="personDirectoryDivisionGuid"/>');
        }
    }

    function __accept_person(map, lparam, wparam)
    {
            document.getElementById('vendorname').value =
                ((map.firstname != null || map.lastname != null) ? map.firstname + ' ' + map.lastname + ' ' : '') +
                map.organization;
            document.getElementById('vendorstreet').value = map.street;
            document.getElementById('vendorzip').value = map.zip;
            document.getElementById('vendorlocation').value = map.location;
            document.getElementById('vendorcountry').value = map.country;
            document.getElementById('vendornip').value = map.nip;
            document.getElementById('vendorregon').value = map.regon;
    }
</script>

</ww:if>