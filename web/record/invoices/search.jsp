<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Wyszukiwanie faktur</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script type="text/javascript">
	// lparam - identyfikator
	function openPersonPopup(lparam, dictionaryType)
	{
		if (lparam == 'sender')
		{
			var x = document.getElementById('vendor').map;
			//alert('ognlMap='+x+', wparam='+wparam+', lparam='+lparam);
			var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true' +
				'&ognlMap='+escape(x)+'&lparam='+lparam+'&dictionaryType='+dictionaryType+
				'&guid=<ww:property value="personDirectoryDivisionGuid"/>' +
				'&canAddAndSubmit=true';
			openToolWindow(url);
		}
	
	}
	
	function personSummary(map)
	{
		var s = '';
		if (map.title != null && map.title.length > 0)
			s += map.title;
	
		if (map.firstname != null && map.firstname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.firstname;
		}
	
		if (map.lastname != null && map.lastname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.lastname;
		}
	
		if (map.organization != null && map.organization.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.organization;
		}
	
		if (map.street != null && map.street.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.street;
		}
	
		if (map.location != null && map.location.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.location;
		}
	
		return s;
	}
	
	/*
		Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
	*/
	function __accept_person(map, lparam, wparam)
	{
		if (lparam == 'sender')
		{
			document.getElementById('vendor').map = JSONStringify(map);
			document.getElementById('vendor').value = personSummary(map);
		}
	}
	
	function __accept_person_duplicate(map, lparam, wparam) {
		return false;
	}
	
	function openContractPopup() 
	{
		var x = document.getElementById('contract').value;
		var url = '<ww:url value="'/record/contracts/search.action'"/>?popup=true' + '&contMap=' + escape(x);   

		openToolWindow(url, 'contract');
	}
	
	/*
		Funkcja wywolywana z okienka contracts/search.jsp po wybraniu umowy 
	*/
	function __accept_contract(map)
	{
	 	document.getElementById('contract').value = JSONStringify(map);
		document.getElementById('contractNo').value = map.contractNo;
		document.getElementById('contractSeqId').value = map.sequenceId;
		document.getElementById('contractId').value = map.contractId;

		 
	}
	
	function resetContract()
	{
		document.getElementById('contractNo').value = '';
	    document.getElementById('contractSeqId').value = '';
	    document.getElementById('contractId').value = '';
	    document.getElementById('contract').value = '';
	}
	
	function resetContractId()
	{
		document.getElementById('contractId').value = '';
	}
	
	function selectAll(checkbox)
	{
		<ww:if test="resultsListOfMap != null">
			var boxes = document.getElementsByName('invoiceIds');
		</ww:if>
		if (boxes.length > 0)
		{
			for (var i=0; i < boxes.length; i++)
			{
				boxes.item(i).checked = checkbox.checked;
			}
		}
	    Custom.clear(); 
	}

	function check()
	{
		var x = document.getElementsByName('invoiceIds');
		var selected=false;
		for(i=0;i<x.length;i++ )
		{
			if(x.item(i).checked)
			{
				selected=true;
			}
		}
		if(!selected)
		{
			alert('<ds:lang text="NieWybranoDokumentow"/>');
			return false;
		}
		return true;		
	}
	
	function setActionName(an)
	{
		document.getElementById('doAction').value = true;
		document.getElementById('actionName').value = an;
	}
</script>

<ww:if test="(results == null || results.count() == 0) && (searchResult == null || searchResult.count() == 0)">
	<form id="form" action="<ww:url value="'/record/invoices/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>" method="post" >
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'contract'" id="contract"/>
		<ww:hidden name="'contractId'" id="contractId"/>
		
		<script type="text/javascript">
			var cont = new Array();
			cont.tmp = 'tmp';
			document.getElementById('contract').value = JSONStringify(cont);
		</script>
		
		<table class="tableMargin">
			<tr>
				<td>
					Numer w rejestrze:
				</td>
				<td>
					<ww:textfield name="'sequenceId'" id="sequenceId" value="sequenceId" size="6" maxlength="10" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr>
				<td>
					Rok
				</td>
				<td>
					<ww:textfield name="'year'" id="year" value="year" size="6" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Numer faktury:
				</td>
				<td>
					<ww:textfield name="'invoiceNo'" id="invoiceNo" value="invoiceNo" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
			   	<td>
			   		U�ytkownik tworz�cy:
			   	</td>
			   	<td>
				   	<ww:select name="'creatingUser'" cssClass="'sel'" list="users" listKey="name" listValue="lastname+' '+firstname"
					   	headerKey="''" headerValue="'-- dowolny --'"/>
				</td>
			</tr>
			<tr>
				<td>
					Numer w rejestrze:
				</td>
				<td>
					od
					<ww:textfield name="'sequenceIdFrom'" size="6" maxlength="10" cssClass="'txt'"/>
					do
					<ww:textfield name="'sequenceIdTo'" size="6" maxlength="10" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Data wpisu do rejestru
				</td>
				<td>
					od
					<ww:textfield name="'ctimeDateFrom'" id="ctimeDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="ctimeDateFromTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'ctimeDateTo'" id="ctimeDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="ctimeDateToTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Data faktury:
				</td>
				<td>
					od
					<ww:textfield name="'invoiceDateFrom'" id="invoiceDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="invoiceDateFromTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'invoiceDateTo'" id="invoiceDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="invoiceDateToTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Termin p�atno�ci:
				</td>
				<td>
					od
					<ww:textfield name="'paymentDateFrom'" id="paymentDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="paymentDateFromTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'paymentDateTo'" id="paymentDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="paymentDateToTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Forma p�atno�ci:
				</td>
				<td>
					<ww:select name="'termsOfPayment'" id="termsOfPayment" list="termsOfPaymentList" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- dowolna --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Rodzaj dokumentu:
				</td>
				<td>
					<ww:select name="'kind'" id="kind" list="kindsList" listKey="id" listValue="name" headerKey="''"
						headerValue="'-- dowolny --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Kwota brutto:
				</td>
				<td>
					od
					<ww:textfield name="'grossFrom'" id="grossFrom" size="19" maxlength="50" cssClass="'txt'"/>
					do
					<ww:textfield name="'grossTo'" id="grossTo" size="19" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Opis
				</td>
				<td>
					<ww:textfield name="'description'" id="description" size="50" maxlength="160" cssClass="'txt'"/>
				</td>
			</tr>
			
			<ds:modules test="contracts">
				<tr>
					<td>
						Numer w rejestrze umowy/zlecenia:
					</td>
					<td>
						<ww:textfield name="'contractSeqId'" id="contractSeqId" size="50" maxlength="50" cssClass="'txt'" readonly="true" onchange="'resetContractId();'"/>
						<ww:hidden name="'contractNo'" id="contractNo" value="contractNo"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="button" value="Wybierz umow�" onclick="openContractPopup();" class="btn">
						<input type="button" value="Wyczy��" onclick="resetContract();" class="btn">
					</td>
				</tr>
			</ds:modules>
			
			<ds:modules test="!contracts">
				<tr>
					<td>
						Numer umowy/zlecenia:
					</td>
					<td>
						<ww:textfield name="'contractNo'" id="contractNo" size="50" maxlength="50" cssClass="'txt'" readonly="true" onchange="'resetContractId();'"/>
						<ww:hidden name="'contractSeqId'" id="contractSeqId"/>
					</td>
				</tr>
			</ds:modules>
			
			<tr>
				<td>
					Sprzedawca:
				</td>
				<td>
					<ww:textfield name="'vendor'" id="vendor" size="50" maxlength="160" cssClass="'txt'" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="button" value="Wybierz sprzedawc�" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn">
				</td>
			</tr>
			<tr>
				<td>
					NIP:
				</td>
				<td>
					<ww:textfield name="'nip'" id="nip" size="15" maxlength="15" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					REGON:
				</td>
				<td>
					<ww:textfield name="'regon'" id="regon" size="15" maxlength="15" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Akceptacja merytoryczna:
				</td>
				<td>
					<ww:select name="'acceptingUser'" id="acceptingUser" list="users" listKey="name" listValue="asLastnameFirstname()"
					headerKey="''" headerValue="'-- dowolny --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Przekazano do:
				</td>
				<td>
					<ww:textfield name="'relayedTo'" id="relayedTo" size="50" maxlength="80" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Data przekazania do RB:
				</td>
				<td>
					od
					<ww:textfield name="'budgetaryRegistryDateFrom'" id="budgetaryRegistryDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="budgetaryRegistryDateFromTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'budgetaryRegistryDateTo'" id="budgetaryRegistryDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="budgetaryRegistryDateToTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Pomi� anulowane:
				</td>
				<td>
					<ww:checkbox name="'ignoreCancelled'" fieldValue="true" value="true"/>
				</td>
			</tr>
			<ds:available test="invoice.archive.field">
			    <tr>
			    	<td>Pomi� Archiwalne</td>
			    	<td><ww:checkbox name="'archive'" fieldValue="true" value="true"/></td>
			    </tr>
			</ds:available>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="doSearch" id="doSearch"/>
					<input type="submit" value="Znajd�" onclick="document.getElementById('doSearch').value='true';" class="btn"
					<ww:if test="!canSearch">disabled="true"</ww:if> />
				</td>
			</tr>
		</table>
 
		<script type="text/javascript">
	
	if($j('#ctimeDateFrom').length > 0)
	{
			Calendar.setup({
				inputField	:	"ctimeDateFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"ctimeDateFromTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#ctimeDateTo').length > 0)
	{
			Calendar.setup({
				inputField	:	"ctimeDateTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"ctimeDateToTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#invoiceDateFrom').length > 0)
	{
			Calendar.setup({
				inputField	:	"invoiceDateFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"invoiceDateFromTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#invoiceDateTo').length > 0)
	{
			Calendar.setup({
				inputField	:	"invoiceDateTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"invoiceDateToTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#paymentDateFrom').length > 0)
	{
			Calendar.setup({
				inputField	:	"paymentDateFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"paymentDateFromTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#paymentDateTo').length > 0)
	{
			Calendar.setup({
				inputField	:	"paymentDateTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"paymentDateToTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#budgetaryRegistryDateFrom').length > 0)
	{
			Calendar.setup({
				inputField	:	"budgetaryRegistryDateFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"budgetaryRegistryDateFromTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
	if($j('#budgetaryRegistryDateTo').length > 0)
	{
			Calendar.setup({
				inputField	:	"budgetaryRegistryDateTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"budgetaryRegistryDateToTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
	}
		</script>
</ww:if>

<ww:else>
	<ww:if test="!newSearch">
		<input type="button" class="btn" value="Wydruk"
		onclick="window.open('<ww:url value='printUrl'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>
		<input type="button" class="btn" value="Wydruk uproszczony"
		onclick="window.open('<ww:url value='printUrl'/>'+'&simplePrint=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>

		<table class="search tableMargin table100p">
			<tr>
				<th>
					<span class="continuousElement">
						<a href="<ww:url value="getSortLink('sequenceId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
							Nr
						<a href="<ww:url value="getSortLink('sequenceId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
					</span>
				</th>
				<th>
					<span class="continuousElement">
						<a href="<ww:url value="getSortLink('invoiceNo', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
							Nr faktury
						<a href="<ww:url value="getSortLink('invoiceNo', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
					</span>
				</th>
				<th>
					<span class="continuousElement">
						<a href="<ww:url value="getSortLink('invoiceDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
						Data faktury
						<a href="<ww:url value="getSortLink('invoiceDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
					</span>
				</th>
				<th>
					Opis
				</th>
				<th>
					Sprzedawca
				</th>
				<th>
					Kwota brutto
				</th>
			</tr>
			<ww:iterator value="results">
				<tr>
					<ww:set name="invoiceDate" scope="page" value="invoiceDate"/>
					<ww:if test="canView">
						<td>
							<a href="<ww:url value="'/record/invoices/edit-main.action?id='+id+'&saveFile='+saveFile"/>">
								<ww:property value="sequenceId"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/record/invoices/edit-main.action?id='+id+'&saveFile='+saveFile"/>">
								<ww:property value="invoiceNo"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/record/invoices/edit-main.action?id='+id+'&saveFile='+saveFile"/>">
								<fmt:formatDate value="${invoiceDate}" type="both" pattern="dd-MM-yy"/></a>
						</td>
					</ww:if>
					<ww:else>
						<td>
							<ww:property value="sequenceId"/>
						</td>
						<td>
							<ww:property value="invoiceNo"/>
						</td>
						<td>
							<fmt:formatDate value="${invoiceDate}" type="both" pattern="dd-MM-yy"/>
						</td>
					</ww:else>
					
					<td>
						<ww:property value="description"/>
					</td>
					<td>
						<ww:property value="vendor.summary"/>
					</td>
					<td>
						<ww:property value="gross"/>  
					</td>
				</tr>
			</ww:iterator>
			<tr>
				<td colspan="4"></td>
				<td>
					Suma kwot
				</td>
				<td>
					<ww:property value="grossSum"/>
				</td>
			</tr>
		</table>

		<ww:set name="pager" scope="request" value="pager" />
			<div class="alignCenter line25p">
				<jsp:include page="/pager-links-include.jsp"/>
			</div>
	</ww:if>
	
	<!-- NOWE WYSZUKIWANIE -->
	<ww:else>
		
		<form id="form" action="<ww:url value="'/record/invoices/search.action'"></ww:url>" method="post" >
			<ww:hidden name="'sequenceId'" value="sequenceId"/>
			<ww:hidden name="'year'" value="year"/>
			<ww:hidden name="'sequenceIdFrom'" value="sequenceIdFrom"/>
			<ww:hidden name="'sequenceIdTo'" value="sequenceIdTo"/>
			<ww:hidden name="'invoiceNo'" value="invoiceNo"/>
			<ww:hidden name="'ctimeDateFrom'" value="ctimeDateFrom"/>
			<ww:hidden name="'ctimeDateTo'" value="ctimeDateTo"/>
			<ww:hidden name="'invoiceDateFrom'" value="invoiceDateFrom"/>
			<ww:hidden name="'invoiceDateTo'" value="invoiceDateTo"/>
			<ww:hidden name="'paymentDateFrom'" value="paymentDateFrom"/>
			<ww:hidden name="'paymentDateTo'" value="paymentDateTo"/>
			<ww:hidden name="'termsOfPayment'" value="termsOfPayment"/>
			<ww:hidden name="'grossFrom'" value="grossFrom"/>
			<ww:hidden name="'grossTo'" value="grossTo"/>
			<ww:hidden name="'description'" value="description"/>
			<ww:hidden name="'contractNo'" value="contractNo"/>
			<ww:hidden name="'contractId'" value="contractId"/>
			<ww:hidden name="'vendor'" value="vendor"/>
			<ww:hidden name="'nip'" value="nip"/>
			<ww:hidden name="'regon'" value="regon"/>
			<ww:hidden name="'acceptingUser'" value="acceptingUser"/>
			<ww:hidden name="'relayedTo'" value="relayedTo"/>
			<ww:hidden name="'budgetaryRegistryDateFrom'" value="budgetaryRegistryDateFrom"/>
			<ww:hidden name="'budgetaryRegistryDateTo'" value="budgetaryRegistryDateTo"/>
			<ww:hidden name="'creatingUser'" value="creatingUser"/>
			<ww:hidden name="'ignoreCancelled'" value="ignoreCancelled"/>
			<ww:hidden name="'kind'" value="kind"/>
			<ww:hidden name="'ignoreArchive'" value="ignoreArchive"/>
			
			<input type="button" class="btn" value="Wydruk"
		onclick="window.open('<ww:url value='printUrl'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>
		<input type="button" class="btn" value="Wydruk uproszczony"
		onclick="window.open('<ww:url value='printUrl'/>'+'&simplePrint=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>
			
			<div id="middleContainer"> <!-- BIG TABLE start -->
			
			<table>
				<tr>
					<td><b><ds:lang text="odnalezionoDokumentow"/></b>:</td>
					<td>
						<ww:property value="count"/>
					</td>
				</tr>
			</table>
			
			<table class="search" width="100%" cellspacing="0" id="mainTable">
				<tr>
					<th>
						&nbsp;
					</th>
					<ww:iterator value="columns" status="status" >
						<th>
							<nobr>
							<ww:if test="sortDesc != null">
								<a name="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="sortDesc"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
									<ww:if test="((property == sortField) || (property == ('invoice_' + sortField))) && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
									<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
							</ww:if>
							<ww:property value="title"/>
							<ww:if test="sortAsc != null">
								<a href="<ww:url value="sortAsc"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
									<ww:if test="((property == sortField) || (property == ('invoice_' + sortField))) && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
									<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
							</ww:if>
						</nobr>
						</th>
						<ww:if test="!#status.last">
							<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
						</ww:if>
						<th>
							&nbsp;
						</th>
					</ww:iterator>
				</tr>
		
				<ww:iterator value="resultsListOfMap">
					<ww:set name="result"/>
					<tr>
						<td>
							<ww:checkbox name="'invoiceIds'" fieldValue="invoice_id" id="check" /> 
						</td>
						<ww:iterator value="columns" status="status">
							<td>
								<a href="<ww:url value="#result['link']"/>">
									
								<ww:property value="prettyPrint(#result[property])"/>
			
								</a>
							</td>
							<ww:if test="!#status.last">
								<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
							</ww:if>
							<td>
								&nbsp;
							</td>
						</ww:iterator>
					</tr>
				</ww:iterator>
		
				<tr height="10"></tr>
				<tr>
					<td style="text-align: left" colspan="<ww:property value="columns.size*2  + 1"/>">
						<input type="checkbox" onclick="selectAll(this)"/>
						<ds:lang text="ZaznaczWszystkie/cofnijZaznaczenie"/>
					</td>
				</tr>
				<tr>
					<td colspan="1"></td>
					<td style="text-align: left">
						Suma kwot brutto: <ww:property value="grossSum"/>
					</td>
				</tr>
			</table>
	
			<ww:set name="pager" scope="request" value="pager" />
				<div class="alignCenter line25p">
					<jsp:include page="/pager-links-include.jsp"/>
				</div>
			
 			<span class="mainButtons">
				<input type="submit" value="<ds:lang text="GenerujRaportXLS"/>" name="doGenerateXls" class="btn" onclick="setActionName('doGenerateXls');"/>		 	 
				<input type="submit" value="<ds:lang text="GenerujRaportXLSzZaznaczonych"/>" name="doGenerateXlsFromChecked" class="btn" onclick="if (!check()) return false; setActionName('doGenerateXlsFromChecked');"/>
			</span>
			
			<input type="hidden" name="doAction" id="doAction"/>
			<input type="hidden" name="actionName" id="actionName"/>
		
			<br /> <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		
			<script type="text/javascript">
				prepareTable(E("mainTable"),1,1);
			
				var prefix = "check";
				tableId="mainTable";
			
				prepareCheckboxes();
			</script>
		</form>
	</ww:else>
</ww:else>

