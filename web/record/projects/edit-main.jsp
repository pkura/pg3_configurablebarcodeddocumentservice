<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.record.projects.Project"%>
<%@ page import="pl.compan.docusafe.core.record.projects.ProjectEntry"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Projekt <ww:if test="projectId != null"> <ww:property value="projectName" /></ww:if>  </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/record/projects/edit-main.action'"><ww:param name="'projectId'" value="projectId"/></ww:url>" method="post" enctype="multipart/form-data" >
	<table>
		<ww:hidden name="'projectId'" value="projectId"/>
		<tr>	        
	    	<td>Kierownik projektu:*</td>
		   	<td>
		   		<ww:select name="'projectManager'" id="projectManager"
	            list="users" listKey="name" listValue="asLastnameFirstname()"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
	  	<tr>
			<td>Pe�na nazwa projektu:*</td>
			<td><ww:textarea name="'projectName'" rows="3" cols="30"  cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Numer zlecenia IF PAN:*</td>
			<td><ww:textfield name="'nrIFPAN'" size="150" maxlength="25" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Numer umowy:</td>
			<td><ww:textfield name="'contractNumber'" size="50" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Numer wniosku:</td>
			<td><ww:textfield name="'applicationNumber'" size="50" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Uwagi:</td>
			<td><ww:textarea name="'remarks'" rows="3" cols="30" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Osoba odpowiedzialna z dzia�u DPiR:</td>
			<td><ww:select name="'responsiblePerson'" id="responsiblePerson"
	            list="dpirUsers" listKey="name" listValue="asLastnameFirstname()"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/></td>
		</tr>
		<tr>
			<td>Instytucja finansuj�ca:</td>
			<td>
			<ww:select name="'financingInstitutionKind'" id="financingInstitutionKind" list="financingInstitutionKinds" listKey="id" listValue="title"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'" value="financingInstitutionKind"/>
	            </td>
		</tr>
		<tr>
			<td>Rozliczenie:<ww:checkbox name="'clearance'" fieldValue="true" value="clearance"  /></td>
			<td>Raport ko�cowy :<ww:checkbox name="'finalReport'" fieldValue="true" value="finalReport" /></td>
			<td>Aktywny :<ww:checkbox name="'active'" fieldValue="true" value="active" /></td>
		</tr>
		<tr>
			<td>Domy�lna waluta:</td> 
        	<td><ww:select name="'defaultCurrency'" id="defaultCurrency"  list="currencyKinds" cssClass="'sel'" value="defaultCurrency"/>
      	</td>
		<tr>
		<td>Pe�na kwota projektu:</td>
			<td><ww:textfield name="'fullGross'" value="fullGross" size="50" maxlength="50" onkeyup="checkMoney(this)" cssClass="'txt'"/></td>
		</tr>
		<td>Procent koszt�w po�rednich:</td>
			<td><ww:textfield name="'percent'" value="percent" size="10" maxlength="5" cssClass="'txt'"/>
			<ds:event name="'doCostsCharged'" value="'Nalicz koszty po�rednie'" />
			</td>
		</tr>
		<tr>
	        <td>Data rozpocz�cia:*</td>
	        <td><ww:textfield name="'startDate'" id="startDate" size="10" maxlength="10" cssClass="'txt'"/>
	            <img src="<ww:url value="'/calendar096/img.gif'"/>"
	                id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
	                title="Date selector" onmouseover="this.style.background='red';"
	                onmouseout="this.style.background=''"/>
	        Data zako�czenia:
	        <ww:textfield name="'finishDate'" id="finishDate" size="10" maxlength="10" cssClass="'txt'"/>
	            <img src="<ww:url value="'/calendar096/img.gif'"/>"
	                id="finishDateTrigger" style="cursor: pointer; border: 1px solid red;"
	                title="Date selector" onmouseover="this.style.background='red';"
	                onmouseout="this.style.background=''"/>
	        </td>
	    </tr>
	    <tr>
		        <td>Rodzaj projektu:*</td> 
		        <td><ww:select name="'projectType'" id="projectType"  list="projectTypesAsList" 
					 cssClass="'sel'" value="projectType"/>
        		</td>
		</tr>
		<ww:if test="projectId != null">
		<tr><td colspan="4">
		 <ww:if test="!attachments.empty">
        <table>
        <tr>
            <th></th>
            <th></th>
            <th><ds:lang text="attachment.title"/></th>
            <th><ds:lang text="Pobierz"/></th>
            <th><ds:lang text="Autor"/></th>
            <th><ds:lang text="Rozmiar"/></th>
            <th><ds:lang text="Data"/></th>
        </tr>
    
        <ww:iterator value="attachments">
            <tr>
                    <td><!--<ww:checkbox name="'ids'" fieldValue="id" />--></td>
                     <td><img src="<ww:url value="icon"/>"/></td> 
                    <td>
                      <ww:if test="canReadAttachments">
                    		<a href="<ww:url value="editLink"/>"><ww:property value="title"/></a>
                    	</ww:if>
                    	<ww:else>
                    		<ww:property value="title"/>
                     	</ww:else>
                    </td>
                    <td>

                            <a href="<ww:url value="recentContentLink"/>">
    						<img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="Pobierz"/>"></a>
    
                            <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mime)">
                                <a href="<ww:url value="recentContentLink+'&asPdf=true'"/>">
								<img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
								title="<ds:lang text="Pobierz"/>" /></a>
								&nbsp
                                <a href="javascript:openToolWindow('<ww:url value="showRecentContentLink"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/><ww:param name="'binderId'" value="binderId"/></ww:url>', 'vs', 1000, 750);">
                                &nbsp;&nbsp;
                                    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="Wyswietl"/>">
                                </a>
                            </ww:if><ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(mime)">
                                <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ revisionId"/>')">
								    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
								</a>
                            </ww:elseif>
    
                        <ds:modules test="certificate">
                            <ww:if test="useSignature && canSign">
                                <a href="javascript:openToolWindow('<ww:url value="'/office/common/attachment-signature.action?id='+revisionId"/>', 'sign', 400, 300);"><img src="<ww:url value="'/img/podpisz.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PodpiszZalacznik"/>"/></a>
                            </ww:if>
                        </ds:modules>
                    </td>
                    <td><ww:property value="author"/></td>
                    <td align="right"><ds:format-size value="#this['size']"/></td>
                    <td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
            </tr>
        </ww:iterator>
        </table>
		</td></tr>
		</ww:if>
		
		<!--  	<tr>
	    	<td>Za��cznik :</td>
	    		<td><ww:file name="'file'" id="file" size="30" cssClass="'txt'" /></td>
	    		<td valign="top">
					<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+mostRecentRevision.id"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
					<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mostRecentRevision.mime)">
						&nbsp;
						<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+mostRecentRevision.id"/>"><img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
						&nbsp;
						<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/></a>
					</ww:if>
				</td>
	    	</tr>
    	-->
	   		<tr>
		    	<td colspan="2">Nazwa za��cznika :<ww:textfield name="'attName'" size="10" maxlength="20" cssClass="'txt'"/></td>
		    </tr>
		    <tr>
		    	<td colspan="2">	
		    	<ww:file name="'file'" id="file" size="20" cssClass="'txt'" />
		    	<ds:event name="'doAddAtta'" value="'Za��cz'" confirm="'Na pewno za��czy�'"/></td>
		    </tr>
			<tr>
				<td colspan="3"> <input type="submit" value="Zapisz" name="doUpdate" class="btn"/></td>
			</tr>
		</ww:if>
</table>
<ww:if test="projectId == null">
	<input type="submit" value="Zapisz" name="doCreate" class="btn"/>
</ww:if>
<ww:else>
	<table>
		<tr>
		<td>Waluta:</td> 
        <td><ww:select name="'balanceCurr'" id="balanceCurr"  list="currencyKinds" 
			 cssClass="'sel'" value="balanceCurr"/>
      	</td>
      	 <td>Rozlicznie do daty:</td>
	     <td><ww:textfield name="'dateEntryTo'" id="dateEntryTo" size="10" maxlength="10" cssClass="'txt'"/>
	         <img src="<ww:url value="'/calendar096/img.gif'"/>"
	             id="dateEntryToTrigger" style="cursor: pointer; border: 1px solid red;"
	             title="Date selector" onmouseover="this.style.background='red';"
	             onmouseout="this.style.background=''"/><ds:event name="'Za�aduj'" value="'Za�aduj'"></ds:event>
	             

	     </td>
      	</tr>
	</table>
	
	<input type="button" value="Dodaj wpis" onclick="showProjectEntry()" class="btn" />
	<script type="text/javascript">
		function showProjectEntry() {
			$j('#ProjectEntry').slideDown();
		}
	</script>
	<ww:if test="projectType =='Naukowe' || projectType =='Inwestycyjne'">
	    <ww:include page="/record/projects/p310.jsp"/>		
	</ww:if>
	<ww:else>
		<ww:include page="/record/projects/on24.jsp"/>
	</ww:else>
	
	<ww:if test="entries.size() == 0">
		<input type="submit" value="Usu�" name="doDelete" class="btn"/>
	</ww:if>
	<ww:hidden name="'entryId'" value="entryId" id="entryId"/>
	<table style="left; display: none" id="ProjectEntry">	
		<tr>	        
		    <td>Data:*</td>
		    <td><ww:textfield readonly="true" name="'dateEntry'" id="dateEntry" size="10" maxlength="10" cssClass="'txt'"/>
		    	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="entryDateTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Data wydatku(Planowana data wydatku)</td>
			<td><ww:textfield name="'expenditureDate'" id="expenditureDate" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="expenditureDateTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Data kosztu</td>
			<td><ww:textfield name="'costDate'" id="costDate" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="costDateTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Nazwa kosztu</td>
			<td><ww:textfield name="'costName'" size="25" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Rodzaj kosztu:</td>
			<td>
			<ww:select name="'costKindId'" id="costKindId" list="costKinds" listKey="id" listValue="title"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	            </td>
			</tr>
		<tr>
			<td>Ksi�gowy rodzaj kosztu:</td>
			<td>
			<ww:select name="'accountingCostKind'" id="accountingCostKind" list="accountingCostKindsList" listKey="id" listValue="title"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	            </td>
		</tr>
		<tr>
	        <td>Rodzaj rozliczenia:*</td> 
	        <td><ww:select name="'settlementKind'" id="settlementKind"  list="settlementKindsAsList" 
				 cssClass="'sel'" value="settlementKind" onchange="'changingCostKind()'"/>
       		</td>
		</tr>
			<tr>
				<td>Waluta:*</td> 
		        <td><ww:select name="'currency'" id="currency"  list="currencyKinds" 
					 cssClass="'sel'" value="currency" onchange="'changingCurrency()'"/>
        		</td>
			</tr>
		<tr>	        
		    	<td>Kwota brutto w walucie:</td>
		    	<td><ww:textfield id="gross" name="'gross'" size="25" cssClass="'txt'" onchange="'changingGrossVal()'"/></td>
		</tr>
		<tr>
				<td>Kwota netto w walucie:</td>
				<td><ww:textfield id="net" name="'net'" size="25" cssClass="'txt'" onchange="'changingNetVal()'"/></td>
		</tr>
		<tr>
				<td>Vat</td>
				<td><ww:textfield id="vat" name="'vat'" size="25" cssClass="'txt'"/></td>
		</tr>


			<tr id="TR_rate" >
				<td>Kurs:*</td>
				<td><ww:textfield id="rate" name="'rate'" size="25" cssClass="'txt'" value="rate" onchange="'changingRateVal()'"/></td>
			</tr>
			<tr>
				<td>Warto�� w PLN:</td>
				<td><ww:textfield id="plnVal" name="plnVal" size="25" cssClass="'txt'" onchange="'changingPlnVal()'"/></td>
			</tr>
			
			<tr>
		        <td>Rodzaj wpisu:*</td>
		        <td><ww:select name="'entryType'" id="entryType" list="entryTypeList" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        		</td>
			</tr>
			<script>
				function changeCur()
				{
					var rateObj = document.getElementById('TR_rate');
					var currencyObj = document.getElementById('currency');
	
					if(currencyObj.selectedIndex == 0)
						rateObj.style.display = 'none';
					else
						rateObj.style.display = '';
				}
			</script>
			<tr>
				<td>Numer dokumentu</td>
				<td><ww:textfield name="'docNr'" size="50" cssClass="'txt'"/>
				<a href="<ww:url value="'/repository/edit-dockind-document.action'"/>?id=<ww:property value="docNr"/>">przejd�</a>
				</td>
			</tr>
			<tr>
				<td>Numer zapotrzebowania</td>
				<td><ww:textfield name="'demandNr'" size="10" cssClass="'txt'"/>
				 <a href="<ww:url value="'/repository/edit-dockind-document.action'"/>?id=<ww:property value="demandNr"/>">przejd�</a>
				</td>
			</tr>
			<tr>
		        <td>Opis</td>
		        <td><ww:textarea name="'description'" cols="20" rows="2"  cssClass="'txt'"/></td>
			</tr>
			<tr class="buttonsRow">
				<td colspan="2">
					<input type="submit" value="Zapisz wpis" name="addEntry" class="btn" >
					<input type="button" value="Wyczy��" onclick="clearProjectEntry();" class="btn">
					<ww:if test="entries.totalCount() > 0 && entryId != null">
						<input type="submit" value="Usu� wpis" name="doDeleteEntry" class="btn" >
					</ww:if>
					<input type="button" value="Wyszukiwarka wpis�w" class="btn" onclick="window.location.href='<ww:url value="'/record/projects/search-entry.action?projectId='+projectId"/>'">
				</td>
			</tr>
		</table>
		<div class="clearBoth"></div>
		
		<table class="mediumTable search userBilling" >
		   	<tr>
		   	<ww:iterator value="columns">
				<th>
					<span class="continuousElement">
		    			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/edit-main.action?sortField='+ key + '&projectId='+projectId+'&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		    				<ww:property value="value"/>
		    			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/edit-main.action?sortField='+ key + '&projectId='+projectId+'&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		    		</span>
		    	</th>
		    </ww:iterator>
			</tr>
			<ww:iterator value="entries">
	   		<tr>
	   			<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="id" />
					</a>
				</td>
	   			<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="projectId" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="entryNumber" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="dateEntry" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="expenditureDate" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="costDate" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="costName" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="costKind" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						
						<ww:property  value="gross"/>
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property  value="net"/>
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property  value="vat"/>
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="entryTypeName" />
					</a>
				</td>
				<td>
					 <a href="<ww:url value="'/repository/edit-dockind-document.action'"/>?id=<ww:property value="docNr"/>"><ww:property value="docNr" /></a>
				</td>
				<td>
					 <a href="<ww:url value="'/repository/edit-dockind-document.action'"/>?id=<ww:property value="demandNr"/>"><ww:property value="demandNr" /></a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="description" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="settlementKind" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="currency" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="rate" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="plnCost" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="mtime" pattern="dd-MM-yy hh:mm:ss" />
					</a>
				</td>
		</tr>
	   	</ww:iterator>
		</table>
		<ww:set name="pager" scope="request" value="pager" />
		<div class="alignCenter line25p">
			<jsp:include page="/pager-links-include.jsp"/>
		</div>
    </ww:else>
</form>
<script type="text/javascript" src="<ww:url value="'/jquery.alphanumeric.js'" />" ></script>
<script>
	
	
	$j(document).ready(function() {
		
		
		$j('[name=fullGross]').numeric({allow: "./,"});
		
		try
		{
		var pln = $j("#plnVal");
		var gross = $j("#gross");
		var rate= $j("#rate");
		var vat = $j("#vat");
		var net= $j("#net");
		var costKind = $j("#settlementKind");

		var rateVal = rate.val().replace(",",".");
		var grossVal = gross.val().replace(",",".");
		var netVal = net.val().replace(",",".");
		
		if (costKind.val()=="BRUTTO")
			if (rateVal.length>0 && grossVal.length>0)
				if (!isNaN(rateVal) && !isNaN(grossVal)) {
					pln.val((rateVal*grossVal).toFixed(2));	
				}
		if (costKind.val()=="NETTO")
			if (rateVal.length>0 && netVal.length>0)
				if (!isNaN(rateVal) && !isNaN(netVal)) {
					pln.val((rateVal*netVal).toFixed(2));
				}	
		}
		catch (err)
		{
			
		}
	});
	
	if($j('#entryId').val() != '')
		$j('#ProjectEntry').slideDown();
	
	function changingCurrency() {
		var curr= $j("#currency");
		var rate= $j("#rate");
		if (curr.val() == 'PLN') 
			rate.val('1.0000');
	}
	
	function changingGrossVal() {
		var pln = $j("#plnVal");
		var gross = $j("#gross");
		var rate= $j("#rate");
		var vat = $j("#vat");
		var net= $j("#net");
		var costKind = $j("#settlementKind");
		
		var rateVal = rate.val().replace(",",".");
		var grossVal = gross.val().replace(",",".");
		
		if (costKind.val()=="BRUTTO")	
			if (rateVal.length>0 && grossVal.length>0)
				if (!isNaN(rateVal) && !isNaN(grossVal)) {
					pln.val((rateVal*grossVal).toFixed(2));
					vat.val('');
					net.val('');
				}
	}
	function changingRateVal() {
		var pln = $j("#plnVal");
		var gross = $j("#gross");
		var rate= $j("#rate");
		var vat = $j("#vat");
		var net= $j("#net");
		var costKind = $j("#settlementKind");
		
		var rateVal = rate.val().replace(",",".");
		var grossVal = gross.val().replace(",",".");
		var netVal = net.val().replace(",",".");
		var plnVal = pln.val().replace(",",".");
		
		if (costKind.val()=="BRUTTO") {
			if (rateVal.length>0 && grossVal.length>0 && !isNaN(rateVal) && !isNaN(grossVal)) {
				pln.val((rateVal*grossVal).toFixed(2));	
			} else if (rateVal.length>0 && plnVal.length>0 && !isNaN(rateVal) && !isNaN(plnVal)) {
				gross.val((plnVal/rateVal).toFixed(2));	
			}
		}
		if (costKind.val()=="NETTO") {
			if (rateVal.length>0 && netVal.length>0 && !isNaN(rateVal) && !isNaN(netVal)) {
				pln.val((rateVal*netVal).toFixed(2));	
			} else if (rateVal.length>0 && plnVal.length>0 && !isNaN(rateVal) && !isNaN(plnVal)) {
				net.val((plnVal/rateVal).toFixed(2));
			}
		}
	}
	function changingCostKind() {
		var gross = $j("#gross");
		var net= $j("#net");
		var vat = $j("#vat");
		var costKind = $j("#settlementKind");
		if (costKind.val()=="BRUTTO" && net.val().length>0) {
			gross.val(net.val());
			net.val('');
		} 
		if (costKind.val()=="NETTO" && gross.val().length>0) {
			net.val(gross.val());
			gross.val('');
		}
		vat.val('');	
	}
	function changingPlnVal() {
		var pln = $j("#plnVal");
		var gross = $j("#gross");
		var rate= $j("#rate");
		var vat = $j("#vat");
		var net= $j("#net");
		var costKind = $j("#settlementKind");
		
		var rateVal = rate.val().replace(",",".");
		var grossVal = gross.val().replace(",",".");
		var netVal = net.val().replace(",",".");
		var plnVal = pln.val().replace(",",".");
		
		if (rateVal.length>0 && plnVal.length>0)
			if (!isNaN(rateVal) && !isNaN(plnVal)) {
				if (costKind.val()=="BRUTTO") {
					gross.val((plnVal/rateVal).toFixed(2));
					net.val('');
				} else {
					net.val((plnVal/rateVal).toFixed(2));
					gross.val('');
				}
				vat.val('');
			}
	
	}
	function changingNetVal() {
		var pln = $j("#plnVal");
		var gross = $j("#gross");
		var rate= $j("#rate");
		var vat = $j("#vat");
		var net= $j("#net");

		var costKind = $j("#settlementKind");
		
		var rateVal = rate.val().replace(",",".");
		var grossVal = gross.val().replace(",",".");
		var netVal = net.val().replace(",",".");
		var plnVal = pln.val().replace(",",".");
		
		if (costKind.val()=="NETTO")	
			if (rateVal.length>0 && netVal.length>0)
				if (!isNaN(rateVal) && !isNaN(netVal)) {
					pln.val((rateVal*netVal).toFixed(2));
					vat.val('');
					gross.val('');
				}
	}
	
	function resetEntry()
	{
		document.getElementById('entryId').value = '';
	    document.getElementById('dateEntry').value = '';
	    document.getElementById('expenditureDate').value = '';
	    document.getElementById('costDate').value = '';
	    document.getElementById('costName').value = '';
	    document.getElementById('costKindId').value = '';
	    document.getElementById('gross').value = '';
	    document.getElementById('net').value = '';
	    document.getElementById('vat').value = '';
	    document.getElementById('entryType').value = '';
	    document.getElementById('docNr').value = '';
	    document.getElementById('demandNr').value = '';
	    document.getElementById('description').value = '';
	}

	function clearProjectEntry() {
		$j('#ProjectEntry').children('tbody').children('tr:not(.buttonsRow)').each(function() {
			$j(this).children('td').children('input, select, textarea').val('');
		});
		$j('#entryId').val('');
	}
	if($j('#dateEntryTo').length > 0)
	{
		Calendar.setup({
		    inputField     :    "dateEntryTo",     // id of the input field
		    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    button         :    "dateEntryToTrigger",  // trigger for the calendar (button ID)
		    align          :    "Tl",           // alignment (defaults to "Bl")
		    singleClick    :    true
		});
	}
	if($j('#dateEntry').length > 0)
	{
		Calendar.setup({
		    inputField     :    "dateEntry",     // id of the input field
		    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    button         :    "dateEntryTrigger",  // trigger for the calendar (button ID)
		    align          :    "Tl",           // alignment (defaults to "Bl")
		    singleClick    :    true
		});
	}
	if($j('#expenditureDate').length > 0)
	{
		Calendar.setup({
		    inputField     :    "expenditureDate",     // id of the input field
		    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    button         :    "expenditureDateTrigger",  // trigger for the calendar (button ID)
		    align          :    "Tl",           // alignment (defaults to "Bl")
		    singleClick    :    true
		});
	}
	if($j('#costDate').length > 0)
	{
		Calendar.setup({
		    inputField     :    "costDate",     // id of the input field
		    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    button         :    "costDateTrigger",  // trigger for the calendar (button ID)
		    align          :    "Tl",           // alignment (defaults to "Bl")
		    singleClick    :    true
		});
	}
	if($j('#startDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "startDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
	if($j('#finishDate').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "finishDate",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "finishDateTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
</script>