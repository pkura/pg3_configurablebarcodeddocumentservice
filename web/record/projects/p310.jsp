<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<table border="1" >
		<tr>
			<td>Przekazano z MINISTERSTWA ��cznie:</td>
			<td colspan="5"><ww:property value="impactSummary" /></td>
		</tr>
		<tr>
			<td>Pozosta�o do wydania:</td>
			<td colspan="5"><ww:property  value="toSpend" /></td>
		</tr>
		<tr>
			<td>PLAN i WYKONANIE GRANTU</td>
			<td>PLAN do ko�ca b.r.</td>
			<td>KOSZTY</td>
			<td>BLOKADY</td>
			<td>Pozosta�o do wykonania w b.r.</td>
			<td>% wykonania narastaj�co z blokadami</td>
		</tr>
		<tr>
			<td>KOSZTY BEZPO�REDNIE ��CZNIE:</td>
			<td><ww:property value="prettyPrint(planSummary - posredniePlan)" /></td>
			<td><ww:property value="prettyPrint(costSummary - posrednieKoszt)" /></td>
			<td><ww:property value="prettyPrint(blocadeSummary - posrednieBlokada)" /></td>
			<td><ww:property value="prettyPrint(planSummary - costSummary-blocadeSummary - posrednieKoszt - posrednieBlokada - posredniePlan)" /></td>
			<td><ww:property value="prettyPrint(firstPercent)" />%</td>
		</tr>
		<tr>
			<td>Wynagrodzenia z pochodnymi ��cznie:</td>
			<td><ww:property value="prettyPrint(wynagrodzeniePlan)"/>	</td>
			<td><ww:property value="prettyPrint(wynagrodzenieKoszt)" /></td>
			<td><ww:property value="prettyPrint(wynagrodzenieBlokada)" /></td>
			<td><ww:property value="prettyPrint(wynagrodzeniePlan - wynagrodzenieKoszt-wynagrodzenieBlokada)" /></td>
			<td><ww:property value="procent(wynagrodzenieKoszt,wynagrodzenieBlokada,wynagrodzeniePlan)" />%</td>
		</tr>
		<ww:iterator value="costCounterMap">
		<tr>
			<td><ww:property value="value.costCategory"/>:</td>
			<td><ww:property value="prettyPrint(value.planSummary)"/>	</td>
			<td><ww:property value="prettyPrint(value.costSummary)" /></td>
			<td><ww:property value="prettyPrint(value.blocadeSummary)" /></td>
			<td><ww:property value="prettyPrint(value.planSummary - value.costSummary-value.blocadeSummary)" /></td>
			<td><ww:property value="prettyPrint(value.procent)" />%</td>
		</tr>
		
		</ww:iterator>
		<tr>
			<td>KOSZTY PO�R.:</td>
			<td><ww:property value="posredniePlan" /></td>
			<td><ww:property value="posrednieKoszt" /></td>
			<td><ww:property value="posrednieBlokada" /></td>
			<td><ww:property value="posredniePlan - posrednieKoszt-posrednieBlokada" /></td>
			<td><ww:property value="procent(posrednieKoszt,posrednieBlokada,posredniePlan)" />%</td>
		</tr>
		<tr>
			<td>KOSZTY ��CZNIE (BEZP.+PO�R.):</td>
			<td><ww:property value="planSummary" /></td>
			<td><ww:property value="prettyPrint(costSummary)" /></td>
			<td><ww:property value="prettyPrint(blocadeSummary)" /></td>
			<td><ww:property  value="toSpend" /></td>
			<td><ww:property value="prettyPrint(lastPercent)" />%</td>
		</tr>
</table>
