<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Wyszukiwanie wpis�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="(entries == null || entries.totalCount() == 0)">
	<form id="form" action="<ww:url value="'/record/projects/search-entry.action'"></ww:url>" method="post" >
		<!-- ww:hidden name="'projectId'" value="projectId"/-->
	<table>
		<tr>
			<td>Projekt</td>
			<td>
				<ww:select name="'projectId'" id="projectId" list="projects" listKey="id" listValue="nrIFPAN"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>	        
		</tr>
		<tr>
		    <td>Data wpisu</td>
		    <td>
				od 
				<ww:textfield name="'dateEntryFrom'" id="dateEntryFrom" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dateEntryFromTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	            do 
	            <ww:textfield name="'dateEntryTo'" id="dateEntryTo" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dateEntryToTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Data wydatku(Planowana data wydatku)</td>
			<td>
				od
				<ww:textfield name="'expenditureDateFrom'" id="expenditureDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="expenditureDateFromTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	            do
	            <ww:textfield name="'expenditureDateTo'" id="expenditureDateTo" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="expenditureDateToTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Data kosztu</td>
			<td>
				od
				<ww:textfield name="'costDateFrom'" id="costDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="costDateFromTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	            do
	            <ww:textfield name="'costDateTo'" id="costDateTo" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="costDateToTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	        </td>
		</tr>
		<tr>
			<td>Nazwa kosztu</td>
			<td><ww:textfield name="'costName'" size="25" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Rodzaj kosztu</td>
			<td>
			<ww:select name="'costKindId'" id="costKindId" list="costKinds" listKey="id" listValue="title"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	            </td>
			</tr>
		    <tr>	        
		    	<td>Kwota brutto</td>
		    	<td>
		    		od
		    		<ww:textfield name="'grossFrom'" size="10" cssClass="'txt'"/>
		    		do
		    		<ww:textfield name="'grossTo'" size="10" cssClass="'txt'"/>
		    	</td>
			</tr>
		  	<tr>
				<td>Kwota netto</td>
				<td>
					od
					<ww:textfield name="'netFrom'" size="10" cssClass="'txt'"/>
					do
					<ww:textfield name="'netTo'" size="10" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>Vat</td>
				<td><ww:textfield name="'vat'" size="10" cssClass="'txt'"/></td>
			</tr>
			<tr>
		        <td>Rodzaj wpisu</td>
		        <td><ww:select name="'entryType'" id="entryType" list="entryTypeList" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
        		</td>
			</tr>
			<tr>
				<td>Numer dokumentu</td>
				<td><ww:textfield name="'docNr'" size="25" cssClass="'txt'"/></td>
			</tr>
			<tr>
				<td>Numer zapotrzebowania</td>
				<td><ww:textfield name="'demandNr'" size="25" cssClass="'txt'"/></td>
			</tr>
			<tr>
		        <td>Opis</td>
		        <td><ww:textarea name="'description'" cssClass="'txt'"/></td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" value="Znajd�" name="doSearch" class="btn"/>
				</td>
			</tr>
		</table>
</ww:if>

<ww:else>
	<input type="button" class="btn" value="<ds:lang text="GenerujRaportXLS"/>" onclick="window.location.href='<ww:url value='printUrl'/>'" />
	<form action="<ww:url value='importUrl'/>" method="post" enctype="multipart/form-data">
		Plik do zaimportowania: <input type="file" name="fileXls" id="fileXls"/>
		<input type="submit" name="submit" value="<ds:lang text="ImportujRaportXLS"/>" class="btn"/>
	</form>
<table class="search tableMargin table100p">
		   	<tr>
		   	<ww:iterator value="columns">
				<th>
					<span class="continuousElement">
		    			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/search-entry.action?doSearch='+true+'&sortField='+ key + '&projectId='+projectId+'&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		    				<ww:property value="value"/>
		    			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/search-entry.action?doSearch='+true+'&sortField=' + key + '&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		    		</span>
		    	</th>
		    </ww:iterator>
			</tr>
			<ww:iterator value="entries">
	   		<tr>
	   			<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="id" />
					</a>
				</td>
	   			<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="projectId" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="entryNumber" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="dateEntry" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="expenditureDate" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="costDate" pattern="dd-MM-yy" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="costName" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="costKind" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="gross" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="net" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="vat" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="entryTypeName" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="docNr" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="demandNr" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="description" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="settlementKind" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="currency" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:property value="rate" />
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ww:if test="settlementKind == 'NETTO'">
							<ww:set name="war" value="net*rate"/>
							<ww:property  value="#war"/>
						</ww:if>
						<ww:else>
							<ww:set name="war" value="gross*rate"/>
							<ww:property  value="#war"/>
						</ww:else>
					</a>
				</td>
				<td>
					<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+projectId + '&entryId='+id"/>">
						<ds:format-date value="mtime" pattern="dd-MM-yy hh:mm:ss" />
					</a>
				</td>
		</tr>
	   	</ww:iterator>
	   	<tr>
	   	<br>
	   	</tr>
	   	<tr>
			<td>
				<b>Suma kwot netto:</b>
			</td>
			<td>
				<b><ww:property value="netSum"/></b>
			</td>
		</tr>
		</table>

		<ww:set name="pager" scope="request" value="pager" />
		<div class="alignCenter line25p">
			<jsp:include page="/pager-links-include.jsp"/>
		</div>
			
</ww:else>

<script>

if($j('#dateEntryFrom').length > 0)
{
	Calendar.setup({
	    inputField     :    "dateEntryFrom",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "dateEntryFromTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

if($j('#dateEntryTo').length > 0)
{
	Calendar.setup({
	    inputField     :    "dateEntryTo",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "dateEntryToTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

if($j('#expenditureDateFrom').length > 0)
{
	Calendar.setup({
	    inputField     :    "expenditureDateFrom",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "expenditureDateFromTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

if($j('#expenditureDateTo').length > 0)
{
	Calendar.setup({
	    inputField     :    "expenditureDateTo",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "expenditureDateToTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

if($j('#costDateFrom').length > 0)
{
	Calendar.setup({
	    inputField     :    "costDateFrom",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "costDateFromTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

if($j('#costDateTo').length > 0)
{
	Calendar.setup({
	    inputField     :    "costDateTo",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "costDateToTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}
</script>