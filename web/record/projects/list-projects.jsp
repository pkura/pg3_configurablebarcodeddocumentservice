<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestr projekt�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/record/projects/list-projects.action'"></ww:url>" method="post" >
<table class="mediumTable search userBilling" >
	<tr>
			<td colspan="2">Numer zlecenia IFPAN:
			<ww:textfield name="'numerIFPAN'" size="25" cssClass="'txt'"/></td>
			<td colspan="2">Kierownik projektu:<ww:select name="'projectManager'" id="projectManager"
	            list="users" listKey="name" listValue="asLastnameFirstname()"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
	        <td><input type="submit" value="Znajd�" name="doSearch" class="btn"/></td>
	</tr>
	<tr>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=id&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>					
				ID
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=id&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=projectManager&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Nazwisko Kierownika projektu
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=projectManager&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=projectName&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Nazwa projektu
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=projectName&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=nrIFPAN&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Numer zlecenia IFPAN
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=nrIFPAN&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieRosnace"/>"  href="<ww:url value="'/record/projects/list-projects.action?sortField=startDate&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Data rozpocz�cia
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=startDate&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=finishDate&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Data zako�czenia
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/record/projects/list-projects.action?sortField=finishDate&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
	</tr>
	<ww:iterator value="projects">
		<ww:hidden name="'id'" value="id"/>
		<tr>
		<td>
			<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ww:property value="id" />
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ww:property value="projectManager" />
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ww:property value="projectName"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ww:property value="nrIFPAN" />
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ds:format-date value="startDate" pattern="dd-MM-yy"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/record/projects/edit-main.action?projectId='+id"/>">
					<ds:format-date value="finishDate" pattern="dd-MM-yy"/>
				</a>
			</td>
		</tr>
	</ww:iterator>
</table>
</form>
