<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<table border="1" >
		<tr>
			<td>Pozosta�o do wydania</td>
			<td colspan="4"><ww:property value="impactSummary" /></td>
		</tr>
		<tr>
			<td>WYKONANIE ZLECENIA</td>
			<td>PLAN do ko�ca b.r.</td>
			<td>BLOKADY</td>
			<td>KOSZTY</td>			
			<td>Pozosta�o do wykonania w b.r.</td>
			
		</tr>
		<tr>
			<td>KOSZTY BEZPO�REDNIE ��CZNIE:</td>
			<td><ww:property value="planSummary - values.KOSZT_POSPLAN" /></td>
			<td><ww:property value="blocadeSummary - values.KOSZT_POSBLOCADE" /></td>
			<td><ww:property value="costSummary - values.KOSZT_POSCOST" /></td>
			<td><ww:property value="planSummary - costSummary - values.KOSZT_POSCOST - values.KOSZT_POSPLAN"/></td>
		</tr>
		<tr>
			<ww:set name="wynagrodzeniePlan" value="values.WYNAGRODZENIEPLAN + values.ZUSPLAN"/>
			<ww:set name="wynagrodzeniaKoszt" value="values.WYNAGRODZENIECOST + values.ZUSCOST"/>
			<ww:set name="wynagrodzeniaBlokada" value="values.WYNAGRODZENIEBLOCADE + values.ZUSBLOCADE"/>
			<td>Wynagrodzenia z pochodnymi ��cznie:</td>
			<td><ww:property value="#wynagrodzeniePlan" /></td>
			<td><ww:property value="#wynagrodzeniaBlokada" /></td>
			<td><ww:property value="#wynagrodzeniaKoszt" /></td>
			
			<td><ww:property value="#wynagrodzeniaPlan - #wynagrodzeniaKoszt" /></td>
		</tr>
		<tr>
			<ww:set name="innePlan" value="values.MATERIALPLAN + values.APARATURAPLAN + values.INNE_KOSZTY_BEZPLAN + values.WYJAZDPLAN"/>
			<ww:set name="inneKoszt" value="values.MATERIALCOST + values.APARATURACOST + values.INNE_KOSZTY_BEZCOST + values.WYJAZDCOST"/>
			<ww:set name="inneBlokada" value="values.MATERIALBLOCADE + values.APARATURABLOCADE + values.INNE_KOSZTY_BEZBLOCADE + values.WYJAZDBLOCADE"/>
			<td>Inne (doktorant)</td>
			<td><ww:property value="#innePlan" /></td>
			<td><ww:property value="#inneBlokada" /></td>
			<td><ww:property value="#inneKoszt" /></td>
			
			<td><ww:property value="#innePlan - #inneKoszt" /></td>
		</tr>
		<tr>
			<td>Konferencje</td>
			<td><ww:property value="values.KONFERENCJEPLAN" /></td>
			<td><ww:property value="values.KONFERENCJEBLOCADE" /></td>
			<td><ww:property value="values.KONFERENCJECOST" /></td>
			<td><ww:property value="values.KONFERENCJEPLAN - values.KONFERENCJECOST" /></td>
		</tr>
		<tr>
			<ww:set name="innezPlan" value="values.MATERIAL_ZESPOLPLAN + values.APARATURA_ZESPOLPLAN + values.INNE_KOSZTY_BEZ_ZESPPLAN + values.WYJAZD_ZESPPLAN"/>
			<ww:set name="innezKoszt" value="values.MATERIAL_ZESPOLCOST + values.APARATURA_ZESPOLCOST + values.INNE_KOSZTY_BEZ_ZESPCOST + values.WYJAZD_ZESPCOST"/>
			<ww:set name="innezBlokada" value="values.MATERIAL_ZESPOLBLOCADE + values.APARATURA_ZESPOLBLOCADE + values.INNE_KOSZTY_BEZ_ZESPBLOCADE + values.WYJAZD_ZESPBLOCADE"/>
			<td>Inne (zesp�)</td>
			<td><ww:property value="#innezPlan" /></td>
			<td><ww:property value="#innezBlokada" /></td>
			<td><ww:property value="#innezKoszt" /></td>
			
			<td><ww:property value="#innezPlan - #inneKoszt" /></td>
		</tr>
		<tr>
			<td>Zarz�dzanie</td>
			<td><ww:property value="values.ZARZADZANIEPLAN" /></td>
			<td><ww:property value="values.ZARZADZANIEBLOCADE" /></td>
			<td><ww:property value="values.ZARZADZANIECOST" /></td>
			<td><ww:property value="values.ZARZADZANIEPLAN - values.ZARZADZANIECOST" /></td>
		</tr>
			<td>KOSZTY PO�REDNIE:</td>
			<td><ww:property value="values.KOSZT_POSPLAN" /></td>
			<td><ww:property value="values.KOSZT_POSBLOCADE" /></td>
			<td><ww:property value="values.KOSZT_POSCOST" /></td>
			
			<td><ww:property value="values.KOSZT_POSPLAN - values.KOSZT_POSCOST" /></td>
		</tr>
		<tr>
			<td>KOSZTY ��CZNIE:</td>
			<td><ww:property value="planSummary" /></td>
			<td><ww:property value="blocadeSummary" /></td>
			<td><ww:property value="costSummary" /></td>
			<td><ww:property value="planSummary - costSummary"/></td>
		</tr>
		
		</table>