<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-main.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Umowa</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canView">
<ww:if test="!history">
	<ww:set name="exists" value="id != null"/>

	<form id="form" action="<ww:url value="'/record/contracts/edit-main.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
		<ww:hidden name="'id'" value="id"/>
		<table class="tableMargin">
			<ww:if test="#exists">
				<tr>
					<td>
						Numer w rejestrze:
					</td>
					<td>
						<span class="bold">
							<ww:property value="contract.sequenceId"/>
						</span>
						<ww:if test="contract.deleted">
							<span class="error bold">usuni�ta</span>
						</ww:if>
					</td>
				</tr>
			</ww:if>
			<tr>
				<td>
					Data umowy
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'contractDate'" id="contractDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="contractDateTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector"
						onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Typ
					<span class="star">*</span>:
				</td>
				<td>
					<ww:select name="'contractKind'" id="contractKind" list="kindsList" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Status umowy:
				</td>
				<td>
				<ww:select name="'contractStatus'" id="contractStatus" list="statusList" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Numer umowy:
				</td>
				<td>
					<ww:textfield name="'contractNo'" id="contractNo" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<ww:if test="#exists && author != null">
				<tr>
					<td>
						U�ytkownik tworz�cy:
					</td>
					<td>
						<ww:property value="author.asFirstnameLastname()"/>
					</td>
				</tr>
			</ww:if>
			<tr>
				<td>
					Opis
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'description'" id="description" size="50" maxlength="160" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Warto��
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'gross'" id="gross" size="20" maxlength="20" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Termin obowi�zywania
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'startTerm'" id="startTerm" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="startTermTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector"
						onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					(do
					<ww:textfield name="'endTerm'" id="endTerm" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="endTermTrigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector"
						onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					)
				</td>
			</tr>
			<!-- Tylko dla licencji SZPZLO Brodno -->
			<ds:license test="29">
				<tr>					
					<td>Okres finansowania:</td>
					<td><ww:textfield name="'periodfFrom'" id="periodfFrom" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="periodfFromTrigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
						do <ww:textfield name="'periodfTo'" id="periodfTo" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="periodfToTrigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
					</td>
				</tr>
			</ds:license>
			<tr>
				<td>
					Numer asygnaty:
				</td>
				<td>
					<ww:textfield name="'voucherNo'" id="voucherNo" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<h4>Sprzedawca</h4>
				</td>
			</tr>
			<tr>
				<td>
					Nazwa
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'vendor.name'" id="vendorname" size="50" maxlength="160" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Ulica
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'vendor.street'" id="vendorstreet" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td class="alignTop">
					Kod pocztowy i miejscowo��
				</td>
				<td>
					<ww:textfield name="'vendor.zip'" size="7" maxlength="14" cssClass="'txt'" id="vendorzip"/>
					<ww:textfield name="'vendor.location'" size="39" maxlength="50" cssClass="'txt'" id="vendorlocation"/>
					<br/>
					<span class="warning" id="vendorzipError"></span>
				</td>
			</tr>
			<tr>
				<td>
					Kraj:
				</td>
				<td>
					<ww:select name="'vendor.country'" id="vendorcountry" list="@pl.compan.docusafe.util.Countries@COUNTRIES"
						listKey="alpha2" listValue="name" value="selectedVendor" cssClass="'sel'" onchange="this.form.vendorzip.validator.revalidate();"/>

				</td>
			</tr>
			<tr>
				<td>
					NIP:
				</td>
				<td>
					<ww:textfield name="'vendor.nip'" id="vendornip" size="15" maxlength="15" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					REGON:
				</td>
				<td>
					<ww:textfield name="'vendor.regon'" id="vendorregon" size="15" maxlength="15" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="button" value="Wybierz sprzedawc�" onclick="openPersonPopup('vendor', '<%= Person.DICTIONARY_SENDER %>');" class="btn" >
				</td>
			</tr>
			<ww:if test="#exists && invoices.size() > 0">
				<tr>
					<td colspan="2">
						Z t� umow� powi�zane s� nast�puj�ce faktury
					</td>
				</tr>
				<tr>
					<td>
						<table class="tableMargin">
							<tr>
								<th>
									Numer
								</th>
								<th>
									Kwota brutto
								</th>
							</tr>
							<ww:iterator value="invoices">
								<tr>
									<td>
										<a href="<ww:url value="'/record/invoices/edit-main.action'"><ww:param name="'id'" value="id"/></ww:url>">
											<ww:property value="invoiceNo"/></a>
									</td>
									<td>
										<ww:property value="gross"/>
									</td>
								</tr>
							</ww:iterator>
							<tr>
								<td>
									Suma kwot
								</td>
								<td>
									<ww:property value="invoicesGrossSum"/>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
				</tr>
			</ww:if>
			<tr>
				<td colspan="2">
					<hr class="fullLine"/>
				</td>
			</tr>
			<tr>
				<td>
					Uwagi:
				</td>
				<td>
					<ww:textarea name="'remarks'" id="remarks" cols="50" rows="5" cssClass="'txt'" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<ww:if test="#exists">
						<input type="hidden" name="doUpdate" id="doUpdate"/>
						<input type="submit" value="Zapisz" onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value='true';" class="btn"
						<ww:if test="!canUpdate">disabled="true" title="Brak uprawnie� do modyfikacji umowy"</ww:if> />
					</ww:if>
					<ww:else>
						<input type="hidden" name="doCreate" id="doCreate"/>
						<input type="submit" value="Zapisz" onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true';" class="btn"
						<ww:if test="!canCreate">disabled="true" title="Brak uprawnie� do tworzenia umowy"</ww:if> />
					</ww:else>
				</td>
			</tr>
		</table>
	</form>
</ww:if>
<ww:else>
	<table class="tableMargin">
		<ww:if test="contractHistoryList != null">			
			<ww:iterator value="contractHistoryList">
				<tr>
					<td><b>Modyfikacja u�ytkownika</b> <ww:property value="userFirstnameLastname"/> <b>z dnia</b> <ww:property value="modificationDate"/></td>					
				</tr>
				<tr>
					<td><b>Zmiany:</b> <ww:property value="description"/></td>
				</tr>
				<tr>
				 <td> <hr /> </td>
				</tr>
			</ww:iterator>
		</ww:if>
		<ww:else>
			<span>Brak wpisow w historii</span>
		</ww:else>
	</table>
</ww:else>
</ww:if>

<ww:if test="canView">
	<script type="text/javascript">
	
	if($j('#contractDate').length > 0)
	{
		Calendar.setup({
			inputField	:	"contractDate",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"contractDateTrigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
	if($j('#startTerm').length > 0)
	{
		Calendar.setup({
			inputField	:	"startTerm",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"startTermTrigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
	if($j('#endTerm').length > 0)
	{
		Calendar.setup({
			inputField	:	"endTerm",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"endTermTrigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
	if($j('#periodfFrom').length > 0)
	{
		Calendar.setup({
			inputField	:	"periodfFrom",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"periodfFromTrigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
	if($j('#periodfTo').length > 0)
	{
		Calendar.setup({
			inputField	:	"periodfTo",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"periodfToTrigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
		var szv = new Validator(document.getElementById('vendorzip'), '##-###');
	
		szv.onOK = function() {
			this.element.style.color = 'black';
			document.getElementById('vendorzipError').innerHTML = '';
		}
	
		szv.onEmpty = szv.onOK;
	
		szv.onError = function(code) {
			this.element.style.color = 'red';
			if (code == this.ERR_SYNTAX)
				document.getElementById('vendorzipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
			else if (code == this.ERR_LENGTH)
				document.getElementById('vendorzipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
		}
	
		szv.canValidate = function() {
			var country = document.getElementById('vendorcountry');
			var countryCode = country.options[country.options.selectedIndex].value;
	
			return (countryCode == '' || countryCode == 'PL');
		}
	
		// pocz�tkowa walidacja danych
		szv.revalidate();
	
		function validateForm()
		{
			var voucherNo = document.getElementById('voucherNo').value;
			var contractNo = document.getElementById('contractNo').value;
			var contractDate = document.getElementById('contractDate').value;
			var startTerm = document.getElementById('startTerm').value;
			var endTerm = document.getElementById('endTerm').value;
			var gross = document.getElementById('gross').value;
			var description = document.getElementById('description').value;
			var vendorname = document.getElementById('vendorname').value;
			var vendorstreet = document.getElementById('vendorstreet').value;
			var remarks = document.getElementById('remarks').value;
			var kind = document.getElementById('contractKind').value;
			var status = document.getElementById('contractStatus').value;
	
			//if (isEmpty(voucherNo)) { alert('Nie podano numeru asygnaty'); return false; }
			if (isEmpty(contractDate)) { alert('Nie podano daty umowy'); return false; }
			if (isEmpty(kind)) { alert('Nie wybrano typu umowy'); return false; }
		   // if (isEmpty(contractNo)) { alert('Nie podano numeru umowy'); return false; }
			if (isEmpty(description)) { alert('Nie podano opisu umowy'); return false; }
			if (isEmpty(gross) || isNaN(parseFloat(gross.replace(',', '.')))) { alert('Nie podano sumy brutto lub podana liczba jest niepoprawna'); return false; }
			if (isEmpty(startTerm)) { alert('Nie podano terminu umowy'); return false; }
			if (isEmpty(vendorname)) { alert('Nie podano nazwy sprzedawcy'); return false; }
			if (isEmpty(vendorstreet)) { alert('Nie podano adresu sprzedawcy'); return false; }
			if (remarks.length && remarks.length > 512) { alert('W polu Uwagi mo�na wpisa� maksymalnie 512 znak�w'); return false; }

			if ( !isEmpty(endTerm) && !isAfterDateBigger(startTerm, endTerm)) {
				alert('Data DO musi by� wi�ksza od daty OD');
				return false;
			}
			return true;
		}

		function isAfterDateBigger(before, after) {	
			var _date = new Date();
			var checkResult = true;
			if (before == null || before == "" || after == null || after == "") {
				return false;
			}			
			var tmp = before.split("-");
			var beforeDate = new Date(parseInt(tmp[2],10), (parseInt(tmp[1],10) - 1), parseInt(tmp[0],10));			
			tmp = after.split("-");
			var afterDate = new Date(parseInt(tmp[2],10), (parseInt(tmp[1],10) - 1), parseInt(tmp[0],10));
			if (afterDate < beforeDate) checkResult = false;
				return checkResult;
		}
	
		function openPersonPopup(lparam, dictionaryType)
		{
			if (lparam == 'sender')
			{
				openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+
					'&organization='+document.getElementById('vendorname').value+
					'&street='+document.getElementById('vendorstreet').value+
					'&zip='+document.getElementById('vendorzip').value+
					'&location='+document.getElementById('vendorlocation').value+
					'&country='+document.getElementById('vendorcountry').value);
					//'&canAddAndSubmit=true');
			}
			else
			{
				openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true&guid=<ww:property value="personDirectoryDivisionGuid"/>');
			}
		}
	
		function __accept_person(map, lparam, wparam)
		{
				document.getElementById('vendorname').value =
					((map.firstname != null || map.lastname != null) ? map.firstname + ' ' + map.lastname + ' ' : '') +
					map.organization;
				document.getElementById('vendorstreet').value = map.street;
				document.getElementById('vendorzip').value = map.zip;
				document.getElementById('vendorlocation').value = map.location;
				document.getElementById('vendorcountry').value = map.country;
				document.getElementById('vendornip').value = map.nip;
				document.getElementById('vendorregon').value = map.regon;
		}
	</script>
</ww:if>
<!--N edit-main.jsp N-->