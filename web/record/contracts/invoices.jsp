<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N invoices.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<form id="form" action="<ww:url value="'/record/contracts/invoices.action'"></ww:url>" method="post" >
<h1>Faktury w ramach umowy o numerze w rejestrze: <ww:property value="id"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />


<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="button" class="btn" value="Wydruk"
	onclick="window.open('<ww:url value='printUrl'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>
<input type="button" class="btn" value="Wydruk uproszczony"
	onclick="window.open('<ww:url value='printUrl'/>'+'&simplePrint=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>


<table width="100%" class="search">
	<tr>
    	<th><nobr>Nr</nobr></th>
        <th><nobr>Nr Faktury</nobr></th>
        <th><nobr>Data Faktury</nobr></th>
        <th><nobr>Opis</nobr></th>
        <th><nobr>Sprzedawca</nobr></th>
        <th><nobr>Warto�� brutto</nobr></th>
    </tr>
<ww:if test="canView">
	<ww:iterator value="invoices">
		<tr>
 			<td><a href="<ww:url value="'/record/invoices/edit-main.action?id='+id"/>"><ww:property value="sequenceId"/></a></td>
    		<td><a href="<ww:url value="'/record/invoices/edit-main.action?id='+id"/>"><ww:property value="invoiceNo"/></a></td>
    		<td><a href="<ww:url value="'/record/invoices/edit-main.action?id='+id"/>"><ww:property value="invoiceDate"/></a></td>
    		<td><ww:property value="description"/></td>
    		<td><ww:property value="vendor.summary"/></td>
  			<td><ww:property value="gross"/></td>
  		</tr>
  	</ww:iterator>
</ww:if>
<ww:else>
  	<ww:iterator value="invoices">    	
    	<tr>
           	<td><ww:property value="sequenceId"/></td>
			<td><ww:property value="invoiceNo"/></td> 
			<td><ww:property value="invoiceDate"/></td> 
			<td><ww:property value="description"/></td>
			<td><ww:property value="vendor.summary"/></td>
			<td><ww:property value="gross"/></td>          
        </tr>
    </ww:iterator>
</ww:else>
    <tr>
    	<td colspan="4"></td>
    	<td>Suma kwot</td>
    	<td><ww:property value="grossSum"/></td>
    </tr>
</table>