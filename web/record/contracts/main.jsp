<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Umowy</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>
	<a href="<ww:url value="'/record/contracts/edit-main.action'"/>">
		Nowa umowa
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Dodanie nowej umowy
</p>

<p>
	<a href="<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>">
		Wyszukiwanie
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wyszukiwanie um�w
</p>

<p>
	<a href="<ww:url value="'/record/contracts/search.action'"><ww:param name="'doSearch'" value="'true'"/></ww:url>">
		Wszystkie umowy
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	<br/>
	Wyszukanie wszystkich um�w
</p>

<%--
<h1>Umowy</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><a href="<ww:url value="'/record/contracts/edit-main.action'"/>">Nowa umowa <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	Dodanie nowej umowy
</p>

<p><a href="<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>">Wyszukiwanie <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	Wyszukiwanie um�w
</p>

<p><a href="<ww:url value="'/record/contracts/search.action'"><ww:param name="'doSearch'" value="'true'"/></ww:url>">Wszystkie umowy <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
	Wyszukanie wszystkich um�w
</p>
--%>
<!--N koniec main.jsp N-->