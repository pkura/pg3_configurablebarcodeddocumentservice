<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Wyszukiwanie um�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form id="form" action="<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>" method="post" >
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'popup'" value="popup"/>
	
		<table class="tableMargin">
			<tr>
				<td>
					Numer w rejestrze:
				</td>
				<td>
					<ww:textfield name="'sequenceId'" id="sequenceId" size="6" maxlength="10" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr>
				<td>
					Rok
				</td>
				<td>
					<ww:textfield name="'year'" id="year" size="6" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Numer asygnaty:
				</td>
				<td>
					<ww:textfield name="'voucherNo'" id="voucherNo" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					U�ytkownik tworz�cy:
				</td>
				<td>
					<ww:select name="'creatingUser'" cssClass="'sel'" list="users" listKey="name" listValue="lastname+' '+firstname"
						headerKey="''" headerValue="'-- dowolny --'"/>
				</td>
			</tr>
			<tr>
				<td>
					Data umowy:
				</td>
				<td>
					od
					<ww:textfield name="'contractDateFrom'" id="contractDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="contractDateFromTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'contractDateTo'" id="contractDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="contractDateToTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<!-- Tylko dla licencji SZPZLO Brodno -->
			<ds:license test="29">
				<tr>
					<td>Okres finansowania:</td>
					<td><ww:textfield name="'periodfFrom'" id="periodfFrom" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="periodfFromTrigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
						do <ww:textfield name="'periodfTo'" id="periodfTo" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="periodfToTrigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
					</td>
				</tr>
			</ds:license>
			<tr>
				<td>
					Typ:
				</td>
				<td>
					<ww:select name="'contractKind'" id="contractKind" list="kindsList" listKey="key" listValue="value"
						headerKey="''" headerValue="'-- dowolny --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Status umowy:
				</td>
				<td>
					<ww:select name="'contractStatus'" id="contractStatus" list="statusList" listKey="key" listValue="value"
						headerKey="''" headerValue="'-- dowolny --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Numer umowy:
				</td>
				<td>
					<ww:textfield name="'contractNo'" id="contractNo" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Opis
				</td>
				<td>
					<ww:textfield name="'description'" id="description" size="50" maxlength="160" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Warto��:
				</td>
				<td>
					od
					<ww:textfield name="'grossFrom'" id="grossFrom" size="19" maxlength="50" cssClass="'txt'"/>
					do
					<ww:textfield name="'grossTo'" id="grossTo" size="19" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Sprzedawca:
				</td>
				<td>
					<ww:textfield name="'vendor'" id="vendor" size="50" maxlength="160" cssClass="'txt'" readonly="true"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="button" value="Wybierz sprzedawc�" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" >
				</td>
			</tr>
			<tr>
				<td>
					NIP:
				</td>
				<td>
					<ww:textfield name="'nip'" id="nip" size="15" maxlength="15" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="doSearch" id="doSearch"/>
					<input type="submit" value="Znajd�" onclick="document.getElementById('doSearch').value='true';" class="btn"
					<ww:if test="!canSearch">disabled="true"</ww:if> />
				</td>
			</tr>
		</table>
	</form>

	<script type="text/javascript">
		
		if($j('#contractDateFrom').length > 0)
		{
			Calendar.setup({
				inputField	 :	"contractDateFrom",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"contractDateFromTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#contractDateTo').length > 0)
		{
			Calendar.setup({
				inputField	 :	"contractDateTo",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"contractDateToTrigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#periodfFrom').length > 0)
		{
			Calendar.setup({
				inputField	:	"periodfFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"periodfFromTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#periodfTo').length > 0)
		{
			Calendar.setup({
				inputField	:	"periodfTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"periodfToTrigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		
		var s = document.location.search;
		if (s != null && s.length > 0) s = s.substring(1);
	
		var contMap = null;
		var params = s.split('&');
		for (var i=0; i < params.length; i++)
		{
			var pair = params[i].split('=');
			var name = pair[0];
			var value = pair[1];
			if (name == 'contMap')
			{
				contMap = unescape(value);
				break;
			}
		}
	
		if (contMap != null)
		{
			eval('var map='+contMap);
	
			//alert('parse '+contMap);
			//var map = parseOgnlMap(contMap);
			//for (var a in map) { alert('arr['+a+']='+map[a]); }
	
			//document.getElementById('id').value = map.id != null ? map.id : '';
			document.getElementById('sequenceId').value = map.sequenceId != null ? map.sequenceId : '';
			document.getElementById('contractDateFrom').value = map.contractDate != null ? map.contractDate : '';
			document.getElementById('contractDateTo').value = map.contractDate != null ? map.contractDate : '';
			document.getElementById('contractNo').value = map.contractNo != null ? map.contractNo : '';
			document.getElementById('description').value = map.description != null ? map.description : '';
		   
		}
	
		window.focus();
	</script>
</ww:if>
<ww:else>
	<input type="button" class="btn" value="Wydruk"
		onclick="window.open('<ww:url value='printUrl'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>

	<table class="search tableMargin table100p">
		<tr>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('sequenceId', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Nr
					<a href="<ww:url value="getSortLink('sequenceId', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('contractNo', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Nr umowy
					<a href="<ww:url value="getSortLink('contractNo', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('contractDate', false)"/>">
						<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
					Data umowy
					<a href="<ww:url value="getSortLink('contractDate', true)"/>">
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				Opis
			</th>
			<th>
				Sprzedawca
			</th>
			<th>
				Warto��
			</th>
		</tr>
		<ww:iterator value="results">
			<tr>
				<ww:set name="contractDate" scope="page" value="contractDate"/>
				<ww:if test="!popup">
					<ww:if test="canView">
						<td>
							<a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>"><ww:property value="sequenceId"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>"><ww:property value="contractNo"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>"><fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/></a>
						</td>
						<td>
							<ww:property value="description"/>
						</td>
						<td>
							<ww:property value="vendor.summary"/>
						</td>
						<td>
							<ww:property value="gross"/>
						</td>
					</ww:if>
					<ww:else>
						<td>
							<ww:property value="sequenceId"/>
						</td>
						<td>
							<ww:property value="contractNo"/>
						</td>
						<td>
							<fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/>
						</td>
						<td>
							<ww:property value="description"/>
						</td>
						<td>
							<ww:property value="vendor.summary"/>
						</td>
						<td>
							<ww:property value="gross"/>
						</td>
					</ww:else>
				  </ww:if>
				  <ww:else>
				  		<td>
				  			<a href="javascript:void(submitContract('<ww:property value="id"/>', '<ww:property value="sequenceId"/>', '<ww:property value="contractNo"/>', '<ww:property value="contractDate"/>', '<ww:property value="description"/>', '<ww:property value="vendor.json"/>'))">
				  				<ww:property value="sequenceId"/></a>
				  		</td>
						<td>
							<a href="javascript:void(submitContract('<ww:property value="id"/>', '<ww:property value="sequenceId"/>', '<ww:property value="contractNo"/>', '<ww:property value="contractDate"/>', '<ww:property value="description"/>', '<ww:property value="vendor.json"/>'))">
								<ww:property value="contractNo"/></a>
						</td>
						<td>
							<fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/>
						</td>
						<td>
							<ww:property value="description"/>
						</td>
						<td>
							<ww:property value="vendor.summary"/>
						</td>
						<td>
							<ww:property value="gross"/>
						</td>
				  </ww:else>
			</tr>
		</ww:iterator>
		<tr>
			<td colspan="4"></td>
			<td>
				Suma warto�ci
			</td>
			<td>
				<ww:property value="grossSum"/>
			</td>
		</tr>
	</table>
		
	<ww:if test="popup">
		<input type="button" class="btn" value="Nowe wyszukiwanie"
			onclick="document.location.href='<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/><ww:param name="'popup'" value="true"/></ww:url>';"/>
	</ww:if>
	
	<ww:set name="pager" scope="request" value="pager" />
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div>
</ww:else>

<script type="text/javascript">
	// lparam - identyfikator
	function openPersonPopup(lparam, dictionaryType)
	{
		
		if (lparam == 'sender')
		{	
			
			var x = document.getElementById('vendor').map;
			//alert('ognlMap='+x+', wparam='+wparam+', lparam='+lparam);
			var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true' +
				'&ognlMap='+escape(x)+'&lparam='+lparam+'&dictionaryType='+dictionaryType+
				'&guid=<ww:property value="personDirectoryDivisionGuid"/>' +
				'&canAddAndSubmit=true';
			
			openToolWindow(url);
		}
	
	}
	
	function personSummary(map)
	{
		var s = '';
		if (map.title != null && map.title.length > 0)
			s += map.title;
	
		if (map.firstname != null && map.firstname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.firstname;
		}
	
		if (map.lastname != null && map.lastname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.lastname;
		}
	
		if (map.organization != null && map.organization.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.organization;
		}
	
		if (map.street != null && map.street.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.street;
		}
	
		if (map.location != null && map.location.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.location;
		}
	
		return s;
	}
	
	/*
		Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
	*/
	function __accept_person(map, lparam, wparam)
	{
	  //  alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);
		
	 if (lparam == 'sender')
		{
			document.getElementById('vendor').map = JSONStringify(map);
			document.getElementById('vendor').value = personSummary(map);
		}
	}
	
	function __accept_person_duplicate(map, lparam, wparam)
	{
		return false;
	}
	
	function submitContract(cId,cSeqId,cNo,cDate,cDesc, cVend)
	{
		if (!window.opener)
		{
			alert('Nie znaleziono g��wnego okna');
			return;
		}
	
		if (!window.opener.__accept_contract)
		{
			alert('W oknie wywo�uj�cym nie znaleziono funkcji __accept_contract');
			return;
		}
	
		// te parametry b�d� przekazane z powrotem do okna wywo�uj�cego
		
		var contract = new Array();
	
		contract.contractId = cId;
		contract.sequenceId = cSeqId;
		contract.contractNo = cNo;
		contract.contractDate = cDate;
		contract.description = cDesc;
		contract.vendor = cVend;
	
		//alert('submit id:'+contract.id+' sid:'+contract.sequenceId+' no:'+contract.contractNo+' date:'+contract.contractDate+' desc:'+contract.description);
	
		window.opener.__accept_contract(contract);
		window.close();
		return true;
	}
	
	function id(id)
	{
		return document.getElementById(id);
	}
</script>

<%--
<h1>Wyszukiwanie um�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="Javascript">
	// lparam - identyfikator
function openPersonPopup(lparam, dictionaryType)
{
	
	if (lparam == 'sender')
	{	
		
		var x = document.getElementById('vendor').map;
		//alert('ognlMap='+x+', wparam='+wparam+', lparam='+lparam);
		var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true' +
			'&ognlMap='+escape(x)+'&lparam='+lparam+'&dictionaryType='+dictionaryType+
			'&guid=<ww:property value="personDirectoryDivisionGuid"/>' +
			'&canAddAndSubmit=true';
		
		openToolWindow(url);
	}

}

function personSummary(map)
{
	var s = '';
	if (map.title != null && map.title.length > 0)
		s += map.title;

	if (map.firstname != null && map.firstname.length > 0)
	{
		if (s.length > 0) s += ' ';
		s += map.firstname;
	}

	if (map.lastname != null && map.lastname.length > 0)
	{
		if (s.length > 0) s += ' ';
		s += map.lastname;
	}

	if (map.organization != null && map.organization.length > 0)
	{
		if (s.length > 0) s += ' / ';
		s += map.organization;
	}

	if (map.street != null && map.street.length > 0)
	{
		if (s.length > 0) s += ' / ';
		s += map.street;
	}

	if (map.location != null && map.location.length > 0)
	{
		if (s.length > 0) s += ' / ';
		s += map.location;
	}

	return s;
}


/*
	Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
  //  alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);
	
 if (lparam == 'sender')
	{
		document.getElementById('vendor').map = JSONStringify(map);
		document.getElementById('vendor').value = personSummary(map);
	}
}

function __accept_person_duplicate(map, lparam, wparam) {
	return false;
}

function submitContract(cId,cSeqId,cNo,cDate,cDesc, cVend)
{
	if (!window.opener)
	{
		alert('Nie znaleziono g��wnego okna');
		return;
	}

	if (!window.opener.__accept_contract)
	{
		alert('W oknie wywo�uj�cym nie znaleziono funkcji __accept_contract');
		return;
	}

	// te parametry b�d� przekazane z powrotem do okna wywo�uj�cego
	
	var contract = new Array();

	contract.contractId = cId;
	contract.sequenceId = cSeqId;
	contract.contractNo = cNo;
	contract.contractDate = cDate;
	contract.description = cDesc;

	//alert('submit id:'+contract.id+' sid:'+contract.sequenceId+' no:'+contract.contractNo+' date:'+contract.contractDate+' desc:'+contract.description);

	window.opener.__accept_contract(contract);
	window.close();
	return true;
}

function id(id)
{
	return document.getElementById(id);
}


</script>

<form id="form" action="<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/></ww:url>" method="post" >
<ww:hidden name="'id'" value="id"/>
<ww:hidden name="'popup'" value="popup"/>

<ww:if test="results == null || results.count() == 0">
	<table>
		<tr>
			<td>Numer w rejestrze:</td>
			<td><ww:textfield name="'sequenceId'" id="sequenceId" size="6" maxlength="10" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2"><hr/></td>
		</tr>
		<tr>
			<td>Rok</td>
			<td><ww:textfield name="'year'" id="year" size="6" maxlength="4" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Numer asygnaty:</td>
			<td><ww:textfield name="'voucherNo'" id="voucherNo" size="50" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>U�ytkownik tworz�cy:</td>
			<td>
				<ww:select name="'creatingUser'" cssClass="'sel'" list="users"
					listKey="name" listValue="lastname+' '+firstname"
					headerKey="''" headerValue="'-- dowolny --'"/>
			</td>
		</tr>
		<tr>
			<td>Data umowy:</td>
			<td>od
				<ww:textfield name="'contractDateFrom'" id="contractDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="contractDateFromTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
				do
				<ww:textfield name="'contractDateTo'" id="contractDateTo" size="10" maxlength="10" cssClass="'txt'"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="contractDateToTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
			</td>
		</tr>
		<tr>
			<td>Typ:</td>
			<td><ww:select name="'contractKind'" id="contractKind" list="kindsList" listKey="key" listValue="value"
				headerKey="''" headerValue="'-- dowolny --'" cssClass="'sel'"/>
			</td>
		</tr>
		<tr>
			<td>Status umowy:</td>
			<td><ww:select name="'contractStatus'" id="contractStatus" list="statusList" listKey="key" listValue="value"
				headerKey="''" headerValue="'-- dowolny --'" cssClass="'sel'"/>
			</td>
		</tr>
		<tr>
			<td>Numer umowy:</td>
			<td><ww:textfield name="'contractNo'" id="contractNo" size="50" maxlength="50" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Opis</td>
			<td><ww:textfield name="'description'" id="description" size="50" maxlength="160" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td>Warto��:</td>
			<td>od <ww:textfield name="'grossFrom'" id="grossFrom" size="19" maxlength="50" cssClass="'txt'"/>
				do <ww:textfield name="'grossTo'" id="grossTo" size="19" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>Sprzedawca:</td>
			<td><ww:textfield name="'vendor'" id="vendor" size="50" maxlength="160" cssClass="'txt'" readonly="true"/></td>
		</tr>
		<tr>
			<td>
			</td>
			<td><input type="button" value="Wybierz sprzedawc�" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
		</tr>
		<tr>
			<td>NIP:</td>
			<td><ww:textfield name="'nip'" id="nip" size="15" maxlength="15" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<input type="hidden" name="doSearch" id="doSearch"/>
				<input type="submit" value="Znajd�" onclick="document.getElementById('doSearch').value='true';" class="btn"
				<ww:if test="!canSearch">disabled="true"</ww:if> />
			</td>
		</tr>

	</table>

	</form>

	<script>
		Calendar.setup({
			inputField	 :	"contractDateFrom",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"contractDateFromTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});

		Calendar.setup({
			inputField	 :	"contractDateTo",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"contractDateToTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});

	</script>

	<script>
		var s = document.location.search;
		if (s != null && s.length > 0) s = s.substring(1);
	
		var contMap = null;
		var params = s.split('&');
		for (var i=0; i < params.length; i++)
		{
			var pair = params[i].split('=');
			var name = pair[0];
			var value = pair[1];
			if (name == 'contMap')
			{
				contMap = unescape(value);
				break;
			}
		}
	
		if (contMap != null)
		{
			eval('var map='+contMap);
	
			//alert('parse '+contMap);
			//var map = parseOgnlMap(contMap);
			//for (var a in map) { alert('arr['+a+']='+map[a]); }
	
			//document.getElementById('id').value = map.id != null ? map.id : '';
			document.getElementById('sequenceId').value = map.sequenceId != null ? map.sequenceId : '';
			document.getElementById('contractDateFrom').value = map.contractDate != null ? map.contractDate : '';
			document.getElementById('contractDateTo').value = map.contractDate != null ? map.contractDate : '';
			document.getElementById('contractNo').value = map.contractNo != null ? map.contractNo : '';
			document.getElementById('description').value = map.description != null ? map.description : '';
		   
		}
	
		window.focus();

	</script>


</ww:if>
<ww:else>
	<input type="button" class="btn" value="Wydruk"
		onclick="window.open('<ww:url value='printUrl'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no')"/>

	<table width="100%" class="search">
		<tr>
			<th><nobr>
				<a href="<ww:url value="getSortLink('sequenceId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Nr
				<a href="<ww:url value="getSortLink('sequenceId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th><nobr>
				<a href="<ww:url value="getSortLink('contractNo', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Nr umowy
				<a href="<ww:url value="getSortLink('contractNo', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th><nobr>
				<a href="<ww:url value="getSortLink('contractDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Data umowy
				<a href="<ww:url value="getSortLink('contractDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th><nobr>
				Opis
			</nobr></th>
			<th><nobr>
				Sprzedawca
			</nobr></th>
			<th><nobr>
				Warto��
			</nobr></th>
		</tr>
	<ww:iterator value="results">
		<tr>
			<ww:set name="contractDate" scope="page" value="contractDate"/>
			<ww:if test="!popup">
				<ww:if test="canView">
					<td><a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>"><ww:property value="sequenceId"/></a></td>
					<td>
						<a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>">
							<ww:property value="contractNo"/>
						</a>
					</td>
					<td><a href="<ww:url value="'/record/contracts/edit-main.action?id='+id"/>"><fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/></a></td>
					<td><ww:property value="description"/></td>
					<td><ww:property value="vendor.summary"/></td>
					<td><ww:property value="gross"/></td>
				</ww:if>
				<ww:else>
					<td><ww:property value="sequenceId"/></td>
					<td><ww:property value="contractNo"/></td>
					<td><fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/></td>
					<td><ww:property value="description"/></td>
					<td><ww:property value="vendor.summary"/></td>
					<td><ww:property value="gross"/></td>
				</ww:else>
			  </ww:if>
			  <ww:else>
			  		<td>
			  			<a href="javascript:void(submitContract('<ww:property value="id"/>', '<ww:property value="sequenceId"/>', '<ww:property value="contractNo"/>', '<ww:property value="contractDate"/>', '<ww:property value="description"/>', '<ww:property value="vendor"/>'))">
			  				<ww:property value="sequenceId"/>
			  			</a>
					<td>
						<a href="javascript:void(submitContract('<ww:property value="id"/>', '<ww:property value="sequenceId"/>', '<ww:property value="contractNo"/>', '<ww:property value="contractDate"/>', '<ww:property value="description"/>', '<ww:property value="vendor"/>'))">
							<ww:property value="contractNo"/>
						</a>
					</td>
					<td><fmt:formatDate value="${contractDate}" type="both" pattern="dd-MM-yy"/></td>
					<td><ww:property value="description"/></td>
					<td><ww:property value="vendor.summary"/></td>
					<td><ww:property value="gross"/></td>
			  </ww:else>
		</tr>
	</ww:iterator>
	<tr>
		<td colspan="4"></td>
		<td>Suma warto�ci</td>
		<td><ww:property value="grossSum"/></td>
	</tr>
	</table>
		
	<ww:if test="popup">
		<input type="button" class="btn" value="Nowe wyszukiwanie"
			onclick="document.location.href='<ww:url value="'/record/contracts/search.action'"><ww:param name="'aa'" value="'aa'"/><ww:param name="'popup'" value="true"/></ww:url>';"/>
	</ww:if>
	
	<ww:set name="pager" scope="request" value="pager" />
	<table width="100%">
		<tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
	</table>

</ww:else>
--%>
<!--N koniec search.jsp N-->