<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N insufficient-permissions-main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Wyst�pi� b��d podczas odczytu za��cznika! Prawdopodobnie za��cznik jest niepoprawnie zapisany! Utw�rz ponownie za��cznik i wgraj go do systemu!</h3>

<p><a href="javascript:window.history.back();">Powr�t</a></p>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/URI-1.11.2.js"></script>
<script type="text/javascript">
	var message = new URI(window.location.href).search(true).message;
	if(message) {
		$j(function(){
			var h3 = $j("<h3></h3>").text(message);
			h3.insertAfter($j("h3"));
		});
	}
</script>
