<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N serial-boxing.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Umieszczanie dokument�w w pudle archiwalnym</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script>
    function BusinessId(s)
    {
        if (s == null || s.length == 0 || s.trim().length == 0)
            throw new Error('Nie podano numeru dokumentu');

        s = s.trim();

        var id = s.toUpperCase();
        var prefix = '';

        var i = 0;
        while (i < id.length && id.charAt(i) >= 'A' && id.charAt(i) <= 'Z')
        {
            prefix += id.charAt(i++);
        }

        var sNum = '';

        while (i < id.length && id.charAt(i) >= '0' && id.charAt(i) <= '9')
        {
            sNum += id.charAt(i++);
        }

        if (sNum.length == 0)
            throw new Error('W numerze \''+s+'\' brakuje cyfr');

        var suffix = '';

        while (i < id.length)
        {
            suffix += id.charAt(i++);
        }

        var num = parseInt(sNum, 10);

        this.getPrefix = function() { return prefix; }
        this.getSuffix = function() { return suffix; }
        this.getNumAsString = function() { return sNum; }
        this.getNum = function() { return parseInt(num); }
        this.toString = function() { return prefix + sNum + suffix; }
    }

    function validateForm()
    {
        if (!document.getElementById('boxName').value)
        {
            alert('Nie podano numeru pud�a');
            return false;
        }

        var startId;
        var endId;
        try
        {
            startId = new BusinessId(document.getElementById('startId').value);
            endId = new BusinessId(document.getElementById('endId').value);
        }
        catch (e)
        {
            alert('Jeden z numer�w jest nieprawid�owy ('+e.description+')');
            return false;
        }

        if (startId.toString().length != endId.toString().length)
        {
            alert('Podane numery r�ni� si� d�ugo�ci�.')
            return false;
        }

        if (startId.getPrefix() != endId.getPrefix())
        {
            alert('Podane numery nale�� do r�nych typ�w dokument�w.')
            return false;
        }

        if (startId.getNum() >= endId.getNum())
        {
            alert('Pocz�tkowy numer zakresu jest wi�kszy lub r�wny ko�cowemu numerowi');
            return false;
        }

        /*
        if (Math.abs(startId.getNum() - endId.getNum()) > 100 &&
            !confirm('Podany zakres przekracza sto dokument�w. Czy jeste� pewien, �e chcesz kontynuowa�?'))
        {
            return false;
        }
        */

        return true;
    }


</script>
