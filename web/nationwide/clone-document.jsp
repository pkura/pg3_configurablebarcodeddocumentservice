<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N clone-document.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="KlonowanieDokumentu"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ww:hidden name="'documentId'"/>

<table>
<tr>
    <td><ds:lang text="Tytul"/>:</td>
    <td><ww:property value="title"/></td>
</tr>
<tr>
    <td><ds:lang text="Opis"/>:</td>
    <td><ww:property value="description"/></td>
</tr>

<tr>
    <td valign="top"><ds:lang text="Zalaczniki"/>:</td>
    <td>
        <table>
        <ww:set name="canModify" value="canModify"/>
        <ww:iterator value="attachments">
            <tr>
                <td><ww:property value="title"/></td>
                <td><nobr>
                    <input type="radio" name="copyAttachments[<ww:property value="id"/>]" checked="true" value="copy" /> <ds:lang text="kopiuj"/>
                    <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="move" <ww:if test="!#canModify || blocked">disabled="true"</ww:if> /> <ds:lang text="przenies"/>
                    <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="none" /> <ds:lang text="niePrzenos"/>
                </nobr></td>
            </tr>
        </ww:iterator>
        </table>
    </td>
</tr>

</table>

<ds:event value="getText('Klonuj')" name="'doClone'" cssClass="'btn'" onclick="'if (!validateForm()) return false'"/>

</form>

<script>
function validateForm()
{
    if (!__nw_validate()) return false;

    var rodzaj_sieci = document.getElementById('__nw_RODZAJ_SIECI');
    if (rodzaj_sieci && (!(parseInt(rodzaj_sieci.value) > 0)))
    {
        alert('<ds:lang text="NieWybranoRodzajuSieci"/>');
        return false;
    }
    var typ_dokumentu = document.getElementById('__nw_TYP_DOKUMENTU');
    //alert(parseInt(typ_dokumentu.value));
    if(typ_dokumentu && (!(parseInt(typ_dokumentu.value) > 0))) {
        alert('<ds:lang text="NieWybranoTypuDokumentu"/>');
        return false;
    }

    var rodzaj_raporty = document.getElementById('RODZAJ_RAPORTY');
    var klasa_raportu = document.getElementById('__nw_KLASA_RAPORTU');
    if (rodzaj_raporty && klasa_raportu && (rodzaj_raporty.options.selectedIndex > 0)
    && (klasa_raportu.options.selectedIndex == 0))
    {
        alert('<ds:lang text="NieWybranoKlasyRaportu"/>');
        return false;
    }

    return true;
}

function __nw_updateAttachmentPattern(typeId)
{
}

</script>
