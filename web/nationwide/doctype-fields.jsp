<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N doctype-fields.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<!--
    Wy�wietla list� p�l nale��cych do typu dokumentu.  Lista p�l musi znajdowa�
    si� pod nazw� doctypeFieldList, za� warto�ci w doctypeFields : Map.

    Pole ENUM o nazwie RODZAJ jest dzielone na trzy listy wyboru, dla dokument�w
    typu Og�lne, �wiadczeniowe i Raporty (na podstawie atrybutu Doctype.Field.arg1).

    Je�eli przed w��czeniem tego pliku zostanie zdefiniowana zmienna OGNL o nazwie
    doctypeFieldsDiscriminator i warto�ci napisowej b�d�cej list� nazw (cn) p�l,
    tylko te pola si� pojawi�.
-->

<ww:iterator value="doctypeFieldList">
    <ww:if test="!hidden and (#doctypeFieldsDiscriminator == null or #doctypeFieldsDiscriminator.indexOf(cn) >= 0)">
    <tr <ww:if test="cn == 'AGENT'">id="tr<ww:property value="cn"/>"</ww:if>>
        <td valign="top">
                <ww:property value="name"/>
                <ww:if test="cn == 'NR_POLISY' or cn == 'RODZAJ'">
                    <span class="star">*</span>
                </ww:if>
                <ww:if test="cn == 'RODZAJ_SIECI' or cn == 'TYP_DOKUMENTU'">
                    <span class="star">*</span>
                </ww:if>:
        </td>
        <ww:set name="tagId" value="cn != null ? '__nw_'+cn : '__nw_datefield_'+id"/>
        <td valign="top">
            <ww:if test="type == 'date'">
                <!-- id="datefield_<ww:property value="id"/>" -->
                <ww:if test="doctypeFields[id] != null">
                    <input type="text" size="10" maxlength="10" class="txt"
                        name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>"
                        <ww:if test="readOnly">readonly="true"</ww:if>
                        value="<ds:format-date value="doctypeFields[id]" pattern="dd-MM-yyyy"/>"/>
                </ww:if>
                <ww:if test="doctypeFields == null or doctypeFields[id] == null">
                    <input type="text" size="10" maxlength="10" class="txt"
                        name="doctypeFields[<ww:property value="id"/>]"
                        <ww:if test="readOnly">readonly="true"</ww:if>
                        id="<ww:property value="#tagId"/>"/>
                </ww:if>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="datefield_trigger_<ww:property value="#tagId"/>" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
                <script>
                    Calendar.setup({
                        inputField     :    "<ww:property value="#tagId"/>",     // id of the input field
                        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                        button         :    "datefield_trigger_<ww:property value="#tagId"/>",  // trigger for the calendar (button ID)
                        align          :    "Tl",           // alignment (defaults to "Bl")
                        singleClick    :    true
                    });
                </script>
            </ww:if>
            <ww:if test="type == 'string'">
                <input type="text" name="doctypeFields[<ww:property value="id"/>]"
                    size="<ww:property value="length > 30 ? 30 : length"/>"
                    maxlength="<ww:property value="length"/>"
                    class="txt"
                    id="<ww:property value="#tagId"/>"
                    value="<ww:property value="doctypeFields[id]"/>"
                    <ds:subject admin="false"><ww:if test="readOnly">readonly="true"</ww:if></ds:subject>
                    onchange="__nw_onchange('<ww:property value="cn"/>', this.value, this);"/>
<%--                                <ww:textfield name="'doctypeFields['+id+']'" size="length > 30 ? 30 : length" maxlength="length" cssClass="'txt'" />--%>
            </ww:if>
            <ww:if test="type == 'bool'">
                <input type="checkbox" name="doctypeFields[<ww:property value="id"/>]" value="true"
                    <ww:if test="doctypeFields[id]">checked="true"</ww:if>
                    <ds:subject admin="false"><ww:if test="readOnly">readonly="true"</ww:if></ds:subject>
                    onchange="__nw_onchange('<ww:property value="cn"/>', this.checked, this);" />
<%--                                <ww:checkbox fieldValue="true" name="'doctypeFields['+id+']'" />--%>
            </ww:if>
            <ww:if test="type == 'integer'">

                    <input type="text" name="doctypeFields[<ww:property value="id"/>]"
                        size="6"
                        maxlength="8"
                        class="txt"
                        id="<ww:property value="#tagId"/>"
                        value="<ww:property value="doctypeFields[id]"/>"
                        <ds:subject admin="false"><ww:if test="readOnly">readonly="true"</ww:if></ds:subject>
                        onchange="__nw_onchange('<ww:property value="cn"/>', this.value, this);"/>

            </ww:if>
            <ww:if test="type == 'long' and !hidden">

                <ww:if test="cn == 'AGENT'">
                    <div id="agent">
                    <input
                            type="hidden"
                            name="doctypeFields[<ww:property value="id"/>]"
                            id="<ww:property value="#tagId"/>"
                            value="<ww:property value="doctypeFields[id]"/>" />
                    <table>
                        <tr>
                            <td>Imi�: </td>
                            <td><ww:textfield name="'agent_imie'" id="agent_imie" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agent_imie" /></td>
                        </tr>
                        <tr>
                            <td>Nazwisko: </td>
                            <td><ww:textfield name="'agent_nazwisko'" id="agent_nazwisko" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agent_nazwisko" /></td>
                        </tr>
                        <tr>
                            <td>Numer: </td>
                            <td><ww:textfield name="'agent_numer'" id="agent_numer" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agent_numer" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="button" value="Wybierz specjalist�/OWCA" onclick="openAgentPopup();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                                <input type="button" value="Wyczy��" onclick="clearAgent();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            </td>
                        </tr>
                    </table>
                    </div>


                    <script type="text/javascript" >
                            function __check_accept_agent(map)
                            {
                                  if (parseInt(document.getElementById('<ww:property value="#tagId"/>').value) == parseInt(map.id))
                                  __accept_agent(map);
                            }
                            function __accept_agent(map)
                            {

                                document.getElementById('<ww:property value="#tagId"/>').value = map.id;
                                document.getElementById('agent_imie').value = map.imie;
                                document.getElementById('agent_numer').value = map.numer;
                                document.getElementById('agent_nazwisko').value = map.nazwisko;


                            //ustawienie agencji
                                document.getElementById('__nw_AGENCJA').value = map.agencja_id;
                                document.getElementById('agencja_nazwa').value = map.agencja_nazwa;
                                document.getElementById('agencja_numer').value = map.agencja_numer;
                                document.getElementById('agencja_nip').value = map.agencja_nip;
                                document.getElementById('__nw_RODZAJ_SIECI').selectedIndex = map.rodzaj_sieci;

                            }
                            function openAgentPopup()
                            {
                                openToolWindow('<ww:url value="'/office/common/agent.action'"/>'+
                                    '?imie='+document.getElementById('agent_imie').value+
                                   '&nazwisko='+document.getElementById('agent_nazwisko').value+
                                   '&id='+document.getElementById('<ww:property value="#tagId"/>').value+
                                    '&canAddAndSubmit=true');
                            }
                            function clearAgent()
                            {
                                document.getElementById('<ww:property value="#tagId"/>').value = '';
                                document.getElementById('agent_imie').value = '';
                                document.getElementById('agent_numer').value = '';
                                document.getElementById('agent_nazwisko').value = '';

                                document.getElementById('__nw_AGENCJA').value = '';
                                document.getElementById('agencja_nazwa').value = '';
                                document.getElementById('agencja_numer').value = '';
                                document.getElementById('agencja_nip').value = '';
                            }
                    </script>

                </ww:if>

                <ww:if test="cn == 'AGENCJA'">
                    <input
                            type="hidden"
                            name="doctypeFields[<ww:property value="id"/>]"
                            id="<ww:property value="#tagId"/>"
                            value="<ww:property value="doctypeFields[id]"/>" />

                    <table>
                        <tr>
                            <td>Nazwa:</td>
                            <td><ww:textfield name="'agencja_nazwa'" id="agencja_nazwa" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agencja_nazwa" /></td>
                        </tr>
                        <tr>
                            <td>Numer:</td>
                            <td><ww:textfield name="'agencja_numer'" id="agencja_numer" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agencja_numer" /></td>
                        </tr>
                        <tr>
                            <td>NIP: </td>
                            <td><ww:textfield name="'agencja_nip'" id="agencja_nip" size="50" maxlength="254" cssClass="'txt'" readonly="true" value="agencja_nip" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="button" id="agencjaButton" value="Wybierz biuro/agencj�/bank" onclick="openAgencjaPopup();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                                <input type="button" id="agencjaClearButton" value="Wyczy��" onclick="clearAgencja();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            </td>
                        </tr>
                    </table>

                    <script type="text/javascript" >
                            function __check_accept_agencja(map)
                            {
                                  if (parseInt(document.getElementById('<ww:property value="#tagId"/>').value) == parseInt(map.id))
                                  __accept_agencja(map);
                            }

                            function __accept_agencja(map)
                            {

                                   document.getElementById('<ww:property value="#tagId"/>').value = map.id;
                                   document.getElementById('agencja_nazwa').value = map.nazwa;
                                   document.getElementById('agencja_numer').value = map.numer;
                                   document.getElementById('agencja_nip').value = map.nip;
                                   document.getElementById('__nw_RODZAJ_SIECI').selectedIndex = map.rodzaj_sieci;

                                    //wyczyszczenie pola agenta
                                document.getElementById('__nw_AGENT').value = '';
                                document.getElementById('agent_imie').value = '';
                                document.getElementById('agent_numer').value = '';
                                document.getElementById('agent_nazwisko').value = '';

                            }
                            function openAgencjaPopup()
                            {
                                openToolWindowX('<ww:url value="'/office/common/agencja.action'"/>'+
                                    '?nazwa='+document.getElementById('agencja_nazwa').value+
                                    '&id='+document.getElementById('<ww:property value="#tagId"/>').value+
                                    '&canAddAndSubmit=true', 'Agencja', 950, 650);
                            }
                            function clearAgencja()
                            {
                                document.getElementById('<ww:property value="#tagId"/>').value = '';
                                document.getElementById('agencja_nazwa').value = '';
                                document.getElementById('agencja_numer').value = '';
                                document.getElementById('agencja_nip').value = '';
                            }
                    </script>

                </ww:if>

                <ww:if test="cn != 'AGENT' and cn != 'AGENCJA'">
                    <input type="text" name="doctypeFields[<ww:property value="id"/>]"
                        size="6"
                        maxlength="8"
                        class="txt"
                        id="<ww:property value="#tagId"/>"
                        value="<ww:property value="doctypeFields[id]"/>"
                        <ds:subject admin="false"><ww:if test="readOnly">readonly="true"</ww:if></ds:subject>
                        onchange="__nw_onchange('<ww:property value="cn"/>', this.value, this);"/>
                 </ww:if>
            </ww:if>


            <ww:if test="type == 'enum'">
                <!-- rodzaj jest traktowany szczeg�lnie - rozbijany na trzy listy
                    listy nie maj� nazw, a warto�� wyboru trzymana jest w polu
                    ukrytym
                -->
                <ww:if test="cn == 'RODZAJ'">
                    <input type="hidden" name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>" value="<ww:property value="doctypeFields[id]"/>"/>
                    <!-- zmienna okre�laj�ca aktualn� warto�� pola RODZAJ -->
                    <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                    <table>
                    <tr>
                        <td>Og�lne:</td>
                        <td>
                            <!-- og�lne -->
                            <select class="sel" id="RODZAJ_OGOLNE" onchange="__nw_updateRODZAJ(this);">
                                <option value="">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'OGOLNE'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>
                    <% if (!Boolean.valueOf(Docusafe.getConfigProperty("skania", "false"))) { %>
                    <tr>
                        <td>�wiadczeniowe:</td>
                        <td>
                            <!-- �wiadczeniowe -->
                            <select class="sel" id="RODZAJ_SWIADCZENIOWE" onchange="__nw_updateRODZAJ(this);">
                                <option value="">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'SWIADCZENIOWE'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Raporty:</td>
                        <td>
                            <!-- raporty -->
                            <select class="sel" id="RODZAJ_RAPORTY" onchange="__nw_updateRODZAJ(this);">
                                <option value="">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'RAPORTY'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>
                    <% } else { %>
                        <div style="display:none">
                            <select class="sel" id="RODZAJ_SWIADCZENIOWE" onchange="__nw_updateRODZAJ(this);">
                                <option value="">-- wybierz --</option>
                            </select>
                            <select class="sel" id="RODZAJ_RAPORTY" onchange="__nw_updateRODZAJ(this);">
                                <option value="">-- wybierz --</option>
                            </select>
                        </div>
                    <% } %>
                    </table>

                    <script>
                    // po wybraniu warto�ci w jednej z list usuwa wyb�r z pozosta�ych
                    function __nw_updateRODZAJ(select)
                    {
                        var id = select.id;
                        if (id != 'RODZAJ_OGOLNE') document.getElementById('RODZAJ_OGOLNE').options.selectedIndex = 0;
                        if (id != 'RODZAJ_SWIADCZENIOWE') document.getElementById('RODZAJ_SWIADCZENIOWE').options.selectedIndex = 0;
                        if (id != 'RODZAJ_RAPORTY') document.getElementById('RODZAJ_RAPORTY').options.selectedIndex = 0;
                        var hid = document.getElementById('<ww:property value="#tagId"/>');
                        hid.value = select.value;

                    }
                    </script>
                </ww:if>
                <ww:if test="cn == 'TYP_DOKUMENTU'" >
                    <input type="hidden" name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>" value="<ww:property value="doctypeFields[id]"/>"/>
                    <!--<input type="hidden" name="'canAddWzorUmowy'" value="'false'" />-->
                    <!-- zmienna okre�laj�ca aktualn� warto�� pola TYP_DOKUMENTU -->
                    <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                    <table>
                    <tr>
                        <td>Dokumenty specjalisty/OWCA:</td>
                        <td>
                            <!-- og�lne -->

                            <select class="sel" id="RODZAJ_AGENT" onchange="__nw_updateRODZAJ(this);">
                                <option value="0">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'RODZAJ_AGENT'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>

                        </td>
                    </tr>
                    <tr>
                        <td>Dokumenty biura/agencji/banku:</td>
                        <td>
                            <!-- �wiadczeniowe -->
                            <select class="sel" id="RODZAJ_AGENCJA" onchange="__nw_updateRODZAJ(this);">
                                <option value="0">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'RODZAJ_AGENCJA'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Raporty:</td>
                        <td>
                            <!-- raporty -->
                            <select class="sel" id="RODZAJ_RAPORTY" onchange="__nw_updateRODZAJ(this);">
                                <option value="0">-- wybierz --</option>
                                <ww:iterator value="enumItems">
                                    <ww:if test="arg1 == 'RODZAJ_RAPORT'">
                                        <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                            <ww:property value="title"/>
                                        </option>
                                    </ww:if>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>

                    </table>

                    <script>
                    function changeView()
                    {
                        if (document.getElementById('RODZAJ_RAPORTY').options.selectedIndex > 0 ||
                            document.getElementById('RODZAJ_AGENCJA').options.selectedIndex > 0) {
                            document.getElementById('trAGENT').style.display = "none";
                            <ww:if test="canReadDAASlowniki">
                                document.getElementById('agencjaButton').disabled = undefined;
                                document.getElementById('agencjaClearButton').disabled = undefined;
                            </ww:if> 

                            //wyczyszczenie pola agenta
                            document.getElementById('__nw_AGENT').value = '';
                            document.getElementById('agent_imie').value = '';
                            document.getElementById('agent_numer').value = '';
                            document.getElementById('agent_nazwisko').value = '';
                        }
                        else {
                            document.getElementById('trAGENT').style.display = "";
                            document.getElementById('agencjaButton').disabled = "disabled";
                            document.getElementById('agencjaClearButton').disabled = "disabled";
                        }
                    }

                    changeView();     //aby domyslnie tez zakryc to co niepotrzebne

                    // po wybraniu warto�ci w jednej z list usuwa wyb�r z pozosta�ych
                    function __nw_updateRODZAJ(select)
                    {
                        var id = select.id;
                        if (id != 'RODZAJ_AGENT') document.getElementById('RODZAJ_AGENT').options.selectedIndex = 0;
                        if (id != 'RODZAJ_AGENCJA') document.getElementById('RODZAJ_AGENCJA').options.selectedIndex = 0;
                        if (id != 'RODZAJ_RAPORTY') document.getElementById('RODZAJ_RAPORTY').options.selectedIndex = 0;
                        var hid = document.getElementById('<ww:property value="#tagId"/>');
                        hid.value = select.value;

                        changeView();

                        __nw_onchange('TYP_DOKUMENTU', select.value, select);

                        __nw_updateAttachmentPattern(select.value)
                    }
<%--                    function __validateWzorUmowy(chkbox) {
                        var v = chkbox.checked;
                        var hid = document.getElementById('<ww:property value="#tagId"/>');
                        if(hid.value == '10' || hid.value == '20' || hid.value == '30' || hid.value == '40'
                                || hid.value =='1010' || hid.value == '1020' || hid.value== '1030' || hid.value=='1040') {
                            if(document.getElementById('file') != null)    {
                                document.getElementById('file').setValue = 'c:\\docusafe\\test.txt';
                                alert('ustawiono plik domyslny: '+document.getElementById('file').value);
                                }
                            else
                                alert('Nie znaleziono objektu file');
                        } else {
                            alert('Zaznaczenie tego pola ma sens jedynie dla dokumentu typu Umowa.');
                        }

                    }--%>
                    </script>
                </ww:if>
                <ww:if test="cn == 'KLASA_RAPORTU'" >
                    <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                    <select name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>"
                        class="sel">
                        <option value="">-- wybierz --</option>
                        <ww:iterator value="enumItems">
                            <option value="<ww:property value="id"/>"
                                    <ww:if test="#fieldid == id">selected="true"</ww:if>
                                    ><ww:property value="title"/></option>
                        </ww:iterator>
                    </select>
                </ww:if>
                <ww:if test="cn == 'RODZAJ_SIECI'" >
                    <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                    <select name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>"
                        onchange="wyczysc();__nw_onchange('<ww:property value="cn"/>', this.value, this);"
                        class="sel">
                        <option value="">-- wybierz --</option>
<%--                        <ww:iterator value="enumItems">
                            <option value="<ww:property value="id"/>" <ww:if test="id == rodzaj_sieci">selected="true"</ww:if> >
                                <ww:property value="title"/>
                                --<ww:property value="id" />--<ww:property value="rodzaj_sieci" />
                            </option>
                        </ww:iterator>--%>
                        <ww:iterator value="enumItems">
                            <option value="<ww:property value="id"/>"
                                    <ww:if test="#fieldid == id">selected="true"</ww:if>
                                    ><ww:property value="title"/></option>
                        </ww:iterator>
                    </select>

                    <script type="text/javascript">
                        function wyczysc() {
                            document.getElementById('__nw_AGENT').value = '';
                            document.getElementById('agent_imie').value = '';
                            document.getElementById('agent_nazwisko').value = '';
                            document.getElementById('agent_numer').value = '';

                            document.getElementById('__nw_AGENCJA').value = '';
                            document.getElementById('agencja_nazwa').value = '';
                            document.getElementById('agencja_numer').value = '';
                            document.getElementById('agencja_nip').value = '';

                        }
                    </script>
                </ww:if>
                <ww:if test="cn != 'RODZAJ' and cn != 'TYP_DOKUMENTU' and cn != 'RODZAJ_SIECI' and cn != 'KLASA_RAPORTU'">
                    <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                    <select name="doctypeFields[<ww:property value="id"/>]"
                        id="<ww:property value="#tagId"/>"
                        onchange="__nw_onchange('<ww:property value="cn"/>', this.value, this);"
                        class="sel">
                        <option value="">-- wybierz --</option>
                        <ww:iterator value="enumItems">
                            <option value="<ww:property value="id"/>"
                                    <ww:if test="#fieldid == id">selected="true"</ww:if>
                                    ><ww:property value="title"/></option>
                        </ww:iterator>
                    </select>
                </ww:if>
            </ww:if>
        </td>
    </tr>
    </ww:if>
</ww:iterator>

<script>
function __nw_validate()
{
    var RODZAJ = document.getElementById('__nw_RODZAJ');

    // czy rodzaj wybrano z listy Raporty
    var isRAPORT = RODZAJ && RODZAJ.value && nwIsRAPORT(RODZAJ.value);

    var NR_POLISY = document.getElementById('__nw_NR_POLISY');
    // dla Raportu numer polisy nie jest wymagany

    if (RODZAJ && (parseInt(RODZAJ.value)+'' != RODZAJ.value.trim()))
    {
        alert('Nie wybrano typu dokumentu');
        return false;
    }

    return true;
}

function nwIsRAPORT(valueRODZAJ)
{
    // pusta warto�� (domy�lny wyb�r)
    if (valueRODZAJ.length == 0)
        return false;

    var selRAPORTY = document.getElementById('RODZAJ_RAPORTY');
    for (var i=0; i < selRAPORTY.options.length; i++)
    {
        if (selRAPORTY.options[i].value == valueRODZAJ)
        {
            return true;
        }
    }
}

var __nw_onchange_hooks = new Array();

// public
function nwAddOnChangeHook(cn, hook)
{
    __nw_onchange_hooks[cn] = hook;
}

function __nw_onchange(cn, value, element)
{
    if (__nw_onchange_hooks[cn])
        __nw_onchange_hooks[cn](value, element);
}




</script>