if($ === undefined) {
	$ = jQuery.noConflict();
}
$(function(){
	var $write = $('#write'),
		shift = false,
		capslock = false,
		rightAlt = false;
	$('#keyboard li').click(function(){
		var $this = $(this),
			character = $this.text(); // If it's a lowercase letter, nothing happens to this variable

		if(character.length > 1) {
			character = character[1];
		}
		
		// Shift keys
		if ($this.hasClass('left-shift') || $this.hasClass('right-shift')) {
			$('.letter').toggleClass('uppercase');
			$('.symbol span').toggle();
			shift = (shift === true) ? false : true;
			capslock = false;
			return false;
		}
		// Caps lock
		if ($this.hasClass('capslock')) {
			$('.letter').toggleClass('uppercase');
			capslock = true;
			return false;
		}
		// Delete
		if ($this.hasClass('delete')) {
			var html = $write.html();
			$write.html(html.substr(0, html.length - 1)).trigger('change.vkeyboard');
			return false;
		}
		// Right Alt for polish programmer keyboard layout
		if ($this.hasClass('right-alt')) {
			$('.letter .polish').toggle();
			rightAlt = true;
			return false;
		}
		// Special characters
		if ($this.hasClass('symbol')) character = $('span:visible', $this).html();
		if ($this.hasClass('space')) character = ' ';
		if ($this.hasClass('tab')) character = "\t";
		if ($this.hasClass('return')) character = "\n";
		// Polish letter
		if (rightAlt === true && $this.find('.polish').length > 0) {
			character = $this.find('.polish').html();
		}
		// Uppercase letter
		if ($this.hasClass('uppercase')) character = character.toUpperCase();
		// Remove shift once a key is clicked.
		if (shift === true) {
			$('.symbol span').toggle();
			if (capslock === false) $('.letter').toggleClass('uppercase');
			shift = false;
		}
		// Remove right alt once a key is clicked.
		if (rightAlt === true) {
			$('.letter .polish').toggle();
			rightAlt = false;
		}
		// Add the character
		$write.html($write.html() + character).trigger('change.vkeyboard');
	});
});
