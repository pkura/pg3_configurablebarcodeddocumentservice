<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N sign-xml.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/jquery.json-2.2.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/JSSign.js'" />"></script>

<ww:if test="signatureId == null">
<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Podpisywanie dokumentu</h1>
</ww:if><ww:else>
<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Weryfikacja dokumentu</h1>
</ww:else>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<%-- Przed podpisaniem --%>
<ww:if test="signedFile != null">
    <applet
        id="certlist"
        code="pl.compan.certlist.Applet.class"
        archive="<ww:url value="'/certificates/certlist-applet.jar'"/>"
        width="0" height="0"
        <%-- jsessionid="<%= request.getRequestedSessionId() %>" --%>
        align="middle">
        <%-- <param name="JSESSIONID" value="<%= request.getRequestedSessionId() %>"/> --%>
    </applet>
    <applet
        id="signer"
        codebase="."
        code="pl.podpiselektroniczny.view.applet.SignerView.class"
        archive="<ww:url value="'/certificates/appletpemi.jar'"/>"
        name="applet"
        width="0" height="0"
        hspace="0"
        vspace="0"
        align="middle">
        <param name="visible" value="false">
    </applet>
</ww:if>

<%-- Walidacja --%>
<ww:if test="signatureId != null">
    <h2>Certyfikat</h2>
    <pre><ww:property escape="false" value="certificateData"/></pre>
    <h2>Podpis</h2>
    <pre><ww:property escape="false" value="signatureData"/></pre>
    <applet
        id="verifier"
        codebase="."
        code="pl.podpiselektroniczny.view.applet.VerifierView.class"
        archive="<ww:url value="'/certificates/appletpemi.jar'"/>"
        name="applet"
        width="0" height="0"
        hspace="0"
        vspace="0"
        align="middle">
        <param name="visible" value="false">
    </applet>
</ww:if>

<div id="signedDocumentInfo">
<ww:if test="attachmentRevision != null"><ww:property value="attachmentDescription"/>
    <a class="imgView" href="<ww:url value="'/repository/view-attachment-revision.do?id='+attachmentRevision"/>">
    <img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
</ww:if>

</div><form id="sendSigned" method="post" action="<ww:url value="/certificates/sign-xml.action"/>">

        <ds:available test="debug">
        <textarea id="signatureFile" name="signatureFile" style="width: 40%; height: 200px;" class="dontFixWidth"><ww:property value="signatureFile" escape="true"/></textarea>
        <textarea id="signedFile" name="signedFile" style="width: 40%; height: 200px;" class="dontFixWidth"><ww:property value="signedFile" escape="true"/></textarea>
        </ds:available>
        <ds:available test="!debug">
        <textarea id="signatureFile" name="signatureFile" style="display:none;"><ww:property value="signatureFile" escape="true"/></textarea>
        <textarea id="signedFile" name="signedFile" style="display:none;" class="dontFixWidth"><ww:property value="signedFile" escape="true"/></textarea>
        </ds:available>
        <!-- <input type="hidden" name="doSign" value="true"/> -->
        <ww:hidden name="'attachmentRevision'"/> <br/>
<%--
        Z jakiego� magicznego powodu zmienne przekazywane s� do ww:url w query stringu
        <ww:hidden name="'processActivity'"/>
        <ww:hidden name="'processName'"/>
        <ww:hidden name="'processId'"/>
        <ww:hidden name="'processAction'"/>
--%>
        <ww:if test="signatureId == null">
            <select id="certList" class="sel dontFixWidth"></select><br/>
			<input type="hidden" name="doSign" value="true"/>
            <input id="certSignButton" type="button" value="Podpisz" class="btn btnShield" /><%-- Javascript pod��czony poni�ej --%>
        </ww:if>
        <ww:else>
        	<!-- <input id="certValidateButton" type="button" value="Sprawd� poprawno��" class="btn" /> --><%-- Javascript pod��czony poni�ej --%>
        	<input type="hidden" name="doValidate" value="true"/>
        	<ds:submit-event value="getText('Zweryfikuj poprawno�� podpisu')" name="'doValidate'" cssClass="'btn'"/>
        </ww:else>
</form>

<script language="Javascript">

var exception;

function test(){
    var jsonCertlist = document.certlist.getCertsAsJSON();
//    alert("json = " + jsonCertlist);
//    alert("[1]sha1 = " + eval(jsonCertlist)[1].sha1);
    var content = $j("#signedFile").text();
//    alert("content =" + content);

    try {
        document.signer.setSignatureFile(1, content);
        document.signer.setCertificateSHA1(eval(jsonCertlist)[1].sha1);
        document.signer.sign(1);
    } catch (e) {
        exception = e;
        alert(e);
    }

    var result = document.signer.getSigned();
//    alert("result = " + result);
    $j("#signatureFile").html(result);
    //$j("#sendSigned").get(0).submit();
}

function renderXml(){
    $j.ajax({
       type: 'POST',
       url: "<ww:url value="'/viewserver/xml/'"/>",
       data: {'xml':$j("#signedFile").text()},
       success: function(data){$j("#signedDocumentInfo").html(data)}
    });
}

$j(document).ready(function() {
	var jsSign = new JSSign();
	(function() {
		<ww:if test="signatureId == null">
			var jqSelect = jsSign.createSignaturesSelect($j('#certList'));
			$j('#sendSigned').append(jqSelect);
			if ($j('#certList').get(0).length == 1 ) {
					var signedXML = jsSign.sign($j("#signedFile").text(), jqSelect.val());
					$j("#signatureFile").text(signedXML);
					$j('#sendSigned').submit();
				
			}else {
			
			$j('#certSignButton').click(function() {
				var signedXML = jsSign.sign($j("#signedFile").text(), jqSelect.val());
				$j("#signatureFile").text(signedXML);
				$j('#sendSigned').submit();
			});
			}
		</ww:if><ww:else>
		    $j('#certValidateButton').click(function() {
			    var response = '' + jsSign.validate($j("#signedFile").val(), $j("#signatureFile").val());
                if(response == 'true'){
                    alert("Applet pomy�lnie zweryfikowa� podpisany plik");
                } else {
                    alert("Applet zwr�ci� odpowied� '" + response + "'");
                }
		    });
		</ww:else>
	})();
	<ww:if test="attachmentRevision == null">
	    renderXml();
	</ww:if>
});
//stary modol sign
//  <ww:if test="signatureId == null">
//		    var jqSelect = jsSign.createSignaturesSelect($j('#certList'));
//		    $j('#sendSigned').append(jqSelect);
//		    $j('#certSignButton').click(function() {
//			    var signedXML = jsSign.sign($j("#signedFile").val(), jqSelect.val());
//			    $j("#signatureFile").val(signedXML);
//			    $j('#sendSigned').submit();
//		    });
//		</ww:if>
</script>

<!--N koniec sign-xml.jsp N-->