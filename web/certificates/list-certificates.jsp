<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N list-certificates.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Lista certyfikat�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<object classid="clsid:127698E4-E730-4E5C-A2B1-21490A70C8A1" codebase="<ww:url value="'/xenroll.dll'"/>#Version=5,131,3659,0" id="enroll">
</object>

<script type="text/javascript">
	// kontrola dost�pno�ci obiektu IEnroll
	var enrollEnabled = false;
	try
	{
		enroll.enumProviders(0, 0);
		enrollEnabled = true;
	}
	catch (e)
	{
		enrollEnabled = false;
	}
</script>

<ww:if test="!certificates.empty">
	<table class="tableMargin">
		<tr>
			<th>Certyfikat</th>
			<th>Data podpisu</th>
			<th>Pobierz</th>
			<th></th>
		</tr>
		<ww:iterator value="certificates" status="status">
			<tr>
				<td>
					<ww:property value="subjectDN"/>
					<span class="italic">(wydany przez <ww:property value="issuerDN"/>)</span>
				</td>
				<td>
					<ds:format-date value="stime" pattern="dd-MM-yy"/>
				</td>
				<td>
					<a href="<ww:url value="'/certificates/download.action?signature='+signature"/>">
						DER</a>
					/
					<a href="<ww:url value="'/certificates/download.action?pem=true&signature='+signature"/>">
						PEM</a>
				</td>
				<td>
					<script type="text/javascript">
						if (enrollEnabled) document.write('<a href="javascript:void(installCertificate(<ww:property value="#status.index"/>))">Zainstaluj</a>');
					</script>
				</td>
			</tr>
		</ww:iterator>
	</table>
</ww:if>

<ww:else>
	<p>
		<span class="italic">U�ytkownik nie ma certyfikat�w</span>
	</p>
</ww:else>


<ww:if test="!csrs.empty">
	<h3>CSR oczekuj�ce na podpisanie</h3>
	
	<form action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
		<table class="tableMargin">
			<tr>
				<th></th>
				<th>CSR</th>
			</tr>
			<ww:iterator value="csrs">
				<tr>
					<td>
						<ww:checkbox name="'deleteIds'" fieldValue="id"/>
					</td>
					<td>
						<ww:property value="PKCS10CertificationRequest.certificationRequestInfo.subject"/>
					</td>
				</tr>
			</ww:iterator>
		</table>

		<ds:submit-event value="'Usu� zaznaczone'" name="'doDeleteCsrs'"/>
	</form>
</ww:if>


<h3>Wy�lij nowe CSR do podpisu</h3>

<form id="form" action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				Plik CSR:
			</td>
			<td>
				<ww:file name="'csrFile'" size="50" cssClass="'txt'"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="'Wy�lij'" name="'doSend'" cssClass="'btn'"/>
</form>


<h3>Generowanie CSR w przegl�darce</h3>

<div id="enrollDiv">
	<form id="enrollForm" action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" >
		<input type="hidden" name="pkcs10Data" id="pkcs10Data"/>
		<input type="hidden" name="doSendGen" id="doSendGen"/>
	</form>

	<table class="tableMargin">
		<tr>
			<td>
				Dostawca us�ug kryptograficznych:
			</td>
			<td>
				<select id="providers" class="sel"></select>
			</td>
		</tr>
		<tr>
			<td>
				Klucz prywatny mo�liwy do wyeksportowania:
			</td>
			<td>
				<input type="checkbox" id="exportablePK"/>
			</td>
		</tr>
	</table>

	<input type="button" class="btn" value="Generuj CSR"
		onclick="if (!confirm('Zamierzasz wys�a� ��danie wydania certyfikatu.  Kontynuowa�?')) return false; submitPkcs10()"/>
</div>

<%--
<h1>Lista certyfikat�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<object classid="clsid:127698E4-E730-4E5C-A2B1-21490A70C8A1"
		codebase="<ww:url value="'/xenroll.dll'"/>#Version=5,131,3659,0"
		id="enroll">
</object>

<script>
	// kontrola dost�pno�ci obiektu IEnroll
	var enrollEnabled = false;
	try
	{
		enroll.enumProviders(0, 0);
		enrollEnabled = true;
	}
	catch (e)
	{
		enrollEnabled = false;
	}

</script>

<ww:if test="!certificates.empty">
<table>
<tr>
<ds:lang text="elo"/>
<ds:lang text="melo"/>
	<th>Certyfikat</th>
	<th>Data podpisu</th>
	<th>Pobierz</th>
	<th></th>
</tr>
<ww:iterator value="certificates" status="status">
	<tr>
		<td><ww:property value="subjectDN"/> <i>(wydany przez <ww:property value="issuerDN"/>)</i></td>
		<td><ds:format-date value="stime" pattern="dd-MM-yy"/></td>
		<td><a href="<ww:url value="'/certificates/download.action?signature='+signature"/>">DER</a> /
			<a href="<ww:url value="'/certificates/download.action?pem=true&signature='+signature"/>">PEM</a></td>
		<td>
			<script>
				if (enrollEnabled) document.write('<a href="javascript:void(installCertificate(<ww:property value="#status.index"/>))">Zainstaluj</a>');
			</script>
		</td>
	</tr>
</ww:iterator>
</table>
</ww:if>
<ww:else>
	<p><i>U�ytkownik nie ma certyfikat�w</i></p>
</ww:else>

<ww:if test="!csrs.empty">

<h3>CSR oczekuj�ce na podpisanie</h3>

<form action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<table>
<tr>
	<th></th>
	<th>CSR</th>
</tr>
<ww:iterator value="csrs">
	<tr>
		<td><ww:checkbox name="'deleteIds'" fieldValue="id"/></td>
		<td><ww:property value="PKCS10CertificationRequest.certificationRequestInfo.subject"/></td>
	</tr>
</ww:iterator>
</table>
<ds:submit-event value="'Usu� zaznaczone'" name="'doDeleteCsrs'"/>
</form>

</ww:if>

<h3>Wy�lij nowe CSR do podpisu</h3>

<form id="form" action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">

<table>
	<tr>
		<td>Plik CSR:</td>
		<td><ww:file name="'csrFile'" size="50" cssClass="'txt'"/>
			<ds:submit-event value="'Wy�lij'" name="'doSend'" cssClass="'btn'"/>
		</td>
	</tr>
</table>
</form>

<h3>Generowanie CSR w przegl�darce</h3>

<div id="enrollDiv">
<form id="enrollForm" action="<ww:url value="'/certificates/list-certificates.action'"/>" method="post" >
	<input type="hidden" name="pkcs10Data" id="pkcs10Data"/>
	<input type="hidden" name="doSendGen" id="doSendGen"/>
</form>
<table>
	<tr>
		<td>Dostawca us�ug kryptograficznych:</td>
		<td><select id="providers" class="sel"></select></td>
	</tr>
	<tr>
		<td>Klucz prywatny mo�liwy do wyeksportowania:</td>
		<td><input type="checkbox" id="exportablePK" /></td>
	</tr>
</table>
<input type="button" class="btn" value="Generuj CSR"
	onclick="if (!confirm('Zamierzasz wys�a� ��danie wydania certyfikatu.  Kontynuowa�?')) return false; submitPkcs10()"/>
</div>
--%>
<script type="text/javascript">
	var certificates = new Array();
	<ww:iterator value="certificates" status="status">
		certificates[<ww:property value="#status.index"/>] = '<ww:property value="pkcs7"/>';
	</ww:iterator>

	function installCertificate(index)
	{
		if (index >= certificates.length)
			return;
		var certData = certificates[index];
		if (certData == null || certData.length < 20)
		{
			alert('Wskazany certyfikat nie mo�e by� zainstalowany automatycznie')
			return false;
		}

		var installed = false;
		enroll.DeleteRequestCert = false;
		enroll.WriteCertToCSP = true;
		try
		{
			enroll.AcceptPKCS7(certData);
			installed = true;
		}
		catch (e)
		{
			enroll.WriteCertToCSP = false;
			try
			{
				enroll.AcceptPKCS7(certData);
				installed = true;
			}
			catch (e2)
			{
			}
		}

		if (installed)
		{
			alert('Zainstalowano certyfikat');
		}
	}

	function submitPkcs10()
	{
		try
		{
			var pkcs10 = genPkcs10(document.getElementById('exportablePK').checked);
			if (pkcs10 == null || pkcs10.length < 20)
			{
				alert('Wygenerowanie certyfikatu nie powiod�o si�');
				return false;
			}
			document.getElementById('pkcs10Data').value = pkcs10;
			document.getElementById('doSendGen').value = 'true';
			document.getElementById('enrollForm').submit();
		}
		catch (e)
		{
			alert('Wyst�pi� b��d podczas generowania CSR' + (e.description ? ' ('+e.description+')' : ''));
		}
	}

	function initEnroll()
	{
		enroll.providerType = 1; // PROV_RSA_FULL
	}

	function enumerateProviders(select)
	{
		var availableProviders = new Array();
		var selectedProviderPriority = -1;
		var selectedProvider = -1;
		// numer pozycji w tablicy jest jednocze�nie priorytetem providera
		// (im wy�szy indeks, tym lepszy provider)
		var preferredProviders = new Array(
		'Microsoft Base Cryptographic Provider',
		'Microsoft Strong Cryptographic Provider',
		'Microsoft Enhanced Cryptographic Provider');

		// wype�nienie listy provider�w
		for (var i=0; true; i++)
		{
			try
			{
				var prov = enroll.enumProviders(i, 0);
				for (var p=0; p < preferredProviders.length; p++)
				{
					if (prov.indexOf(preferredProviders[p]) >= 0 && p > selectedProviderPriority)
					{
						selectedProviderPriority = p;
						selectedProvider = i;
					}
				}
				availableProviders[availableProviders.length] = prov;
			}
			catch (x)
			{
				break;
			}
		}

		for (var i=0; i < availableProviders.length; i++)
		{
			select.options[select.options.length] = new Option(availableProviders[i], availableProviders[i]);
		}

		select.options.selectedIndex = selectedProvider;
	}

	function genPkcs10(exportable)
	{
		enroll.providerType = 1;
		enroll.providerName = document.getElementById('providers').value;
		enroll.GenKeyFlags = exportable ? 1 : 0;
		enroll.KeySpec = 1;
		var pkcs10 = enroll.CreatePKCS10('<ww:property value="csrDn"/>', '1.3.6.1.4.1.311.2.1.21');
		return pkcs10;
		// enroll.CreateFilePKCS10('cn=lukasz@wa.home.pl;o=COM-PAN;l=Warszawa;c=PL', '1.3.6.1.4.1.311.2.1.21', 'c:/pkcs10.csr');
	}

	// inicjalizacja tutaj, kiedy element providers ju� istnieje
	if (enrollEnabled)
	{
		initEnroll();
		enumerateProviders(document.getElementById('providers'));
	}
	else
	{
		document.getElementById('enrollDiv').innerHTML = '<p>U�ywana przegl�darka nie umo�liwia wygenerowania CSR.  Prosz� u�y� Internet Explorera.</p>';
	}
</script>
<!--N koniec list-certificates.jsp N-->