<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <h2>Czynność w procesie</h2>
        <table>
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xsl:template match="action">
       <tr>
           <td class="name">Nazwa procesu</td>
           <td class="url"><xsl:value-of select="@process-name"/></td>
       </tr>
        <tr>
           <td class="name">Nazwa czynności</td>
           <td class="url"><xsl:value-of select="@action-name"/></td>
       </tr>
    </xsl:template>

</xsl:stylesheet>