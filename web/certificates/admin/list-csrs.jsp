<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N list-csrs.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Lista CSR</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/certificates/admin/list-csrs.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="!csrs.empty">
		<table class="tableMargin">
			<tr>
				<th></th>
				<th>
					U�ytkownik
				</th>
				<th>
					Certyfikat
				</th>
			</tr>
			<ww:iterator value="csrs">
				<tr>
					<td>
						<ww:checkbox name="'signIds'" fieldValue="id"/>
					</td>
					<td>
						<ww:property value="user.asLastnameFirstnameName()"/>
					</td>
					<td>
						<ww:property value="csr.certificationRequestInfo.subject"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
	
		<p>
			Has�o do klucza prywatnego:
			<ww:password name="'keyPassword'" size="20" maxlength="100" cssClass="'txt'"/>
			<span class="italic">Wymagane do podpisania ��da� (CSR).</span>
		</p>
	
		<ds:submit-event value="'Podpisz zaznaczone'" name="'doSign'" disabled="!canSign"/>
		<ds:submit-event value="'Odrzu� zaznaczone'" name="'doReject'" confirm="'Na pewno chcesz odrzuci� zaznaczone CSR?'"/>
	</ww:if>

	<ww:else>
		<p>
			<span class="italic">Nie ma CSR do podpisania</span>
		</p>
	</ww:else>
</form>

<%--
<h1>Lista CSR</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form action="<ww:url value="'/certificates/admin/list-csrs.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<ww:if test="!csrs.empty">
	<table>
		<tr>
			<th></th>
			<th>U�ytkownik</th>
			<th>Certyfikat</th>
		</tr>
		<ww:iterator value="csrs">
			<tr>
				<td><ww:checkbox name="'signIds'" fieldValue="id"/></td>
				<td><ww:property value="user.asLastnameFirstnameName()"/></td>
				<td><ww:property value="csr.certificationRequestInfo.subject"/></td>
			</tr>
		</ww:iterator>
	</table>

	<p>Has�o do klucza prywatnego: <ww:password name="'keyPassword'" size="20" maxlength="100" cssClass="'txt'"/>
	<i>Wymagane do podpisania ��da� (CSR).</i></p>

	<ds:submit-event value="'Podpisz zaznaczone'" name="'doSign'" disabled="!canSign"/>
	<ds:submit-event value="'Odrzu� zaznaczone'" name="'doReject'" confirm="'Na pewno chcesz odrzuci� zaznaczone CSR?'"/>

</ww:if>
<ww:else>
	<p><i>Nie ma CSR do podpisania</i></p>
</ww:else>

</form>
--%>
<!--N koniec list-csrs.jsp N-->