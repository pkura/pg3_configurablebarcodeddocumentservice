<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Wyszukiwanie certyfikatów</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form action="<ww:url value="'/certificates/admin/search.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
		<table class="tableMargin formTable">
			<tr>
				<td>
					Data podpisania:
				</td>
				<td>
					<ww:textfield name="'signingTimeFrom'" id="signingTimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_signingTimeFrom_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<span class="star">*</span>
					<ww:textfield name="'signingTimeTo'" id="signingTimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_signingTimeTo_trigger"
						style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					Data wygaśnięcia:
				</td>
				<td>
					<ww:textfield name="'expirationTimeFrom'" id="expirationTimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_expirationTimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do
					<span class="star">*</span>
					<ww:textfield name="'expirationTimeTo'" id="expirationTimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_expirationTimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					lub
					<ww:select name="'expired'" list="expiredOptions" headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
				</td>
			</tr>
			<tr>
				<td>
					Użytkownik:
				</td>
				<td>
					<ww:select name="'username'" list="users" cssClass="'sel'" listKey="name" listValue="asLastnameFirstnameName()"
					headerKey="''" headerValue="'-- dowolny --'"/>
				</td>
			</tr>
		</table>
		
		<ds:submit-event value="'Szukaj'" name="'doSearch'" cssClass="'btn searchBtn'"/>

		<script type="text/javascript">
		if($j('#signingTimeFrom').length > 0)
		{
			Calendar.setup({
				inputField	:	"signingTimeFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"calendar_signingTimeFrom_trigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#signingTimeTo').length > 0)
		{
			Calendar.setup({
				inputField	:	"signingTimeTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"calendar_signingTimeTo_trigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#expirationTimeFrom').length > 0)
		{
			Calendar.setup({
				inputField	:	"expirationTimeFrom",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"calendar_expirationTimeFrom_trigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		if($j('#expirationTimeTo').length > 0)
		{
			Calendar.setup({
				inputField	:	"expirationTimeTo",	 // id of the input field
				ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		:	"calendar_expirationTimeTo_trigger",  // trigger for the calendar (button ID)
				align		:	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		}
		</script>
	</form>
</ww:if>

<ww:else>
	<table class="search tableMargin table100p">
		<tr>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('subjectDn', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Wystawiony dla
					<a href="<ww:url value="getSortLink('subjectDn', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('signingTime', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Data podpisu
					<a href="<ww:url value="getSortLink('signingTime', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href="<ww:url value="getSortLink('expirationTime', false)"/>">
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					Data wygaśnięcia
					<a href="<ww:url value="getSortLink('expirationTime', true)"/>">
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				Pobranie
			</th>
		</tr>
		<ww:iterator value="results">
			<tr>
				<td>
					<ww:property value="subjectDn"/>
				</td>
				<td>
					<ds:format-date value="signingTime" pattern="dd-MM-yy"/>
				</td>
				<td>
					<ds:format-date value="expirationTime" pattern="dd-MM-yy"/>
				</td>
				<td>
					<a href="<ww:url value="'/certificates/download.action'"><ww:param name="'id'" value="id"/></ww:url>">
						DER</a>
					/
					<a href="<ww:url value="'/certificates/download.action'"><ww:param name="'pem'" value="true"/><ww:param name="'id'" value="id"/></ww:url>">
						PEM</a>
				</td>
			</tr>
		</ww:iterator>
	</table>

	<ww:set name="pager" scope="request" value="pager" />
	<div class="alignCenter line25p">
		<jsp:include page="/pager-links-include.jsp"/>
	</div>
</ww:else>

<%--
<h1>Wyszukiwanie certyfikatów</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<ww:if test="results == null || results.count() == 0">

	<form action="<ww:url value="'/certificates/admin/search.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

		<table>
			<tr>
				<td>Data podpisania:</td>
				<td>
					<ww:textfield name="'signingTimeFrom'" id="signingTimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_signingTimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do<span class="star">*</span>
					<ww:textfield name="'signingTimeTo'" id="signingTimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_signingTimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>Data wygaśnięcia:</td>
				<td>
					<ww:textfield name="'expirationTimeFrom'" id="expirationTimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_expirationTimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do<span class="star">*</span>
					<ww:textfield name="'expirationTimeTo'" id="expirationTimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_expirationTimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					lub
					<ww:select name="'expired'" list="expiredOptions"
						headerKey="''" headerValue="'-- wybierz --'"
						cssClass="'sel'"/>
	<%--
					<select name="expired" class="sel">
						<option value="">-- wybierz --</option>
						<option value="yes">wygasłe</option>
						<option value="nearly">prawie wygasłe</option>
					</select>
	-->
				</td>
			</tr>
			<tr>
				<td>Użytkownik:</td>
				<td><ww:select name="'username'" list="users" cssClass="'sel'"
					listKey="name" listValue="asLastnameFirstnameName()"
					headerKey="''" headerValue="'-- dowolny --'"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<ds:submit-event value="'Szukaj'" name="'doSearch'" cssClass="'btn'"/>
				</td>
			</tr>
		</table>

		<script type="text/javascript">
			Calendar.setup({
				inputField	 :	"signingTimeFrom",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"calendar_signingTimeFrom_trigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
			Calendar.setup({
				inputField	 :	"signingTimeTo",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"calendar_signingTimeTo_trigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
			Calendar.setup({
				inputField	 :	"expirationTimeFrom",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"calendar_expirationTimeFrom_trigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
			Calendar.setup({
				inputField	 :	"expirationTimeTo",	 // id of the input field
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
				button		 :	"calendar_expirationTimeTo_trigger",  // trigger for the calendar (button ID)
				align		  :	"Tl",		   // alignment (defaults to "Bl")
				singleClick	:	true
			});
		</script>

	</form>

</ww:if>
<ww:else>
	<table width="100%" class="search">
		<tr>
			<th><nobr>
				<a href="<ww:url value="getSortLink('subjectDn', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Wystawiony dla
				<a href="<ww:url value="getSortLink('subjectDn', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th><nobr>
				<a href="<ww:url value="getSortLink('signingTime', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Data podpisu
				<a href="<ww:url value="getSortLink('signingTime', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th><nobr>
				<a href="<ww:url value="getSortLink('expirationTime', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
				Data wygaśnięcia
				<a href="<ww:url value="getSortLink('expirationTime', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
			</nobr></th>
			<th>Pobranie</th>
		</tr>
	<ww:iterator value="results">
		<tr>
			<td><ww:property value="subjectDn"/></td>
			<td><ds:format-date value="signingTime" pattern="dd-MM-yy"/></td>
			<td><ds:format-date value="expirationTime" pattern="dd-MM-yy"/></td>
			<td><a href="<ww:url value="'/certificates/download.action'"><ww:param name="'id'" value="id"/></ww:url>">DER</a>
				/ <a href="<ww:url value="'/certificates/download.action'"><ww:param name="'pem'" value="true"/><ww:param name="'id'" value="id"/></ww:url>">PEM</a>
			</td>
		</tr>
	</ww:iterator>
	</table>

	<ww:set name="pager" scope="request" value="pager" />
	<table width="100%">
		<tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
	</table>

</ww:else>
--%>
<!--N koniec search.jsp N-->