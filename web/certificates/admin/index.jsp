<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N index.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Certyfikaty</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<p><a href="<ww:url value="'/certificates/admin/list-csrs.action'"/>">CSR oczekujące na podpisanie <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>

<p><a href="<ww:url value="'/certificates/admin/paths.action'"/>">Ustawienia <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
