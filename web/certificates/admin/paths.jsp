<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N paths.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">�cie�ki do certyfikat�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>
	Wszystkie �cie�ki powinny odnosi� si� do serwera, na kt�rym dzia�a serwer aplikacji.  Pliki powinny by� w formacie PEM.
</p>

<form action="<ww:url value="'/certificates/admin/paths.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				Certyfikat CA:
			</td>
			<td>
				<ww:textfield name="'caCertPath'" size="50" maxlength="1024" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				Klucz prywatny CA:
			</td>
			<td>
				<ww:textfield name="'caKeyPath'" size="50" maxlength="1024" cssClass="'txt'"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
</form>

<%--
<h1>�cie�ki do certyfikat�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<p>Wszystkie �cie�ki powinny odnosi� si� do serwera, na kt�rym dzia�a
serwer aplikacji.  Pliki powinny by� w formacie PEM.</p>

<form action="<ww:url value="'/certificates/admin/paths.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<table>
	<tr>
		<td>Certyfikat CA:</td>
		<td><ww:textfield name="'caCertPath'" size="50" maxlength="1024" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Klucz prywatny CA:</td>
		<td><ww:textfield name="'caKeyPath'" size="50" maxlength="1024" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td colspan="2">
			<ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
		</td>
	</tr>
</table>

</form>
--%>
<!--N koniec paths.jsp N-->