<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <h2>Pola dokumentu</h2>
        <table>
            <xsl:apply-templates/>
        </table>
    </xsl:template>

    <xsl:template match="field">
        <tr>
            <td class="name"><xsl:value-of select="@name"/></td>
            <td class="url"><xsl:value-of select="."/></td>
        </tr>
    </xsl:template>

</xsl:stylesheet>