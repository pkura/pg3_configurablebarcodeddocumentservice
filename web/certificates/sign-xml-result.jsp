<!--N sign-xml-result.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Podpisywanie dokumentu</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<form>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="button" class="btn" value="Kontynuuj" onclick="javascript:window.location='<ww:property value="returnUrl"/>'"/>

</form>
<!--N koniec sign-xml-result.jsp N-->

