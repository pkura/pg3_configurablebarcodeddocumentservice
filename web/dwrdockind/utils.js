var jqAjaxTopInfo = null;
var jqAjaxTopInfoBtnHide = null;
var jqAjaxTopInfoBtnShow = null;
var jqAjaxTopInfoToggler = null;

if(window.console === undefined) {
    console = {};
    console.error = console.info = console.warn = console.debug = console.assert = function() {}
}

var TMP = [];

var FieldStatus = {ADD: 'ADD', REMOVE: 'REMOVE', NOCHANGE: 'NOCHANGE', CHANGED: 'CHANGED'};
var Validator = {
	INTEGER: {
		RegExp: new RegExp(/^-?[0-9]*$/i),
		Message: DWR.MSG.VALIDATOR_INCORRECT_NUMBER
	},
	MONEY: {
		RegExp: new RegExp(/(^-?[0-9]*\.?\d{1,2}$)|(^\s*$)/),
		Message: DWR.MSG.VALIDATOR_INCORRECT_AMOUNT
	},
	DATE: {
		RegExp: new RegExp(/^[0-9]{1,2}[-]{1}[0-9]{1,2}[-]{1}[0-9]{4}$/i),
		Message: DWR.MSG.VALIDATOR_INCORRECT_DATE
	},
	TIMESTAMP: {
		RegExp: new RegExp(/^([0-1][0-9]|2[0-3]):([0-5][0-9])$/i),
		Message: DWR.MSG.VALIDATOR_INCORRECT_TIME
	}
}
var KEYS = {
    BACKSPACE:8,
    TAB:9,
    ENTER:13,
    SHIFT:16,
    CTRL:17,
    ALT:18,
    PAUSEBREAK:19,
    CAPSLOCK:20,
    ESC:27,
    ESCAPE:27,
    PAGEUP:33,
    PAGEDOWN:34,
    END:35,
    HOME:36,
    ARROW_LEFT:37,
    ARROW_UP:38,
    ARROW_RIGHT:39,
    ARROW_DOWN:40,
    INSERT:45,
    DELETE:46,
    DIGITS: {
        from:48,
        to:57
    },
	NUM_DIGITS: {
		from: 96,
		to: 105
	},
    LATIN:{
        from:65,
        to:90
    },
    PLUS:43,
    ADD:43,
    MINUS:45,
    MINUS2:109,
    MINUS3:173,
    SUBSTRACT:45, // minus
    DASH:45, // -
    COMMA:44, // ,       
    DECIMAL_POINT:46,
    POINT:46,
    DIVIDE:47,
    FORWARD_SLASH:47,
    SEMICOLON:58, // :
    EQUAL_SIGN:61,  
    BACK_SLASH:92,
	NUM_POINT: 110,
	COMMA2: 188,
	POINT2: 190,
    isDigit: function(keyCode){
        return (keyCode>=this.DIGITS.from && keyCode<=this.DIGITS.to)
				|| (keyCode>=this.NUM_DIGITS.from && keyCode<=this.NUM_DIGITS.to);
    },
    isChar: function(keyCode){
        return keyCode>=this.LATIN.from && keyCode<=this.LATIN.to;
    },
    isCommandKey: function(keyCode) {
    	var commandKeys = [this.TAB, this.ARROW_RIGHT, this.ARROW_LEFT, this.DELETE, this.HOME, this.END, this.PAGEDOWN, this.PAGEUP, 
    	         			this.ESC, this.BACKSPACE, this.ENTER];
    	var i = commandKeys.length;
    	while(i >= 0) {
    		if(commandKeys[i--] == keyCode)
    			return true;
    	}
    	return false;
    }
};

var FLAGS = {
	STRING_TRIM_LEFT:	0x01,
	STRING_TRIM_RIGHT:	0x02,
	STRING_TRIM_BOTH:	0x03,
	STRING_TRIM:		0x03
}

var KeyFilter = {
	INTEGER: function(event) {
		var k = event.keyCode;
		if((KEYS.isDigit(k) && event.shiftKey == false) || (!KEYS.isDigit(k) && (event.ctrlKey == true)) 
			|| KEYS.isCommandKey(k) || k == KEYS.MINUS3 || k == KEYS.MINUS2 || k == KEYS.MINUS)
			return true;
		return false;
	},
	MONEY: function(event) {
		var k = event.keyCode;
		if(k == KEYS.COMMA || k == KEYS.COMMA2 || k == KEYS.DECIMAL_POINT || k == KEYS.POINT2 || k == KEYS.NUM_POINT)
			return true;
		return KeyFilter.INTEGER(event);
	}
};

Number.MAX_INT = (function(){ return Math.pow(2, 31) - 1; })();

var RANGE_CHECKER_MSG = {
	INTEGER: {
		MAX: DWR.MSG.RANGE_TOO_BIG_NUMBER,
		MIN: DWR.MSG.RANGE_TOO_SMALL_NUMBER
	}
}
var RangeChecker = {
	INTEGER: function(val) {
		if(val > Number.MAX_INT)
			return {inRange: false, message: RANGE_CHECKER_MSG.INTEGER.MAX};
		return {inRange: true};
	}
}
var Formatter = {
	// 123 456 788.09
	MONEY: (function() {
		var re = {};
		re.whitespace = RegExp(/\s/g); 
		re.comma = RegExp(/,/g);
		re.integerPart = RegExp(/\-?\d*(?!,)/);
		re.groupThree = RegExp(/(\d{3})/gi);
		re.floatPart = RegExp(/\.\d*/);
		re.floatPart2 = RegExp(/(\.\d{0,2})/g);
		re.dot = RegExp(/\./g);
		return (
		function(val, extFormat) {
			var value = val.trim().replace(re.whitespace, '').replace(re.comma, '.') + '';
			
			var formattedValue = value.match(re.integerPart).join();
			formattedValue = formattedValue.split('').reverse().join('').replace(re.groupThree, '$1 ').split('').reverse().join('').trim();
			formattedValue = formattedValue.replace(/^-\s/,'-');
			
			try {
				formattedValue += value.match(re.floatPart).join();
			} catch(e) {
				
			}
			
			if(formattedValue.length == 0)
				value = '';
			else if(value.length > 0 && extFormat) {
				if (value.search(re.dot) == -1) {
					formattedValue += '.00';
				}
				else {
					formattedValue = formattedValue.replace(re.floatPart2, '$1' + (new Array(4 - formattedValue.match(re.floatPart2).join('').length)).join('0'));
				}
			}
			
			formattedValue = formattedValue.replace(re.dot, ',');
			
			return {value: value, formattedValue: formattedValue};
		}
		);
		})()
}

showError = function(err, file, line) {
	if(err && err.javaClassName) {
		jqAjaxTopInfo.children('div.content').html('<span class="warningBig"></span>' + err.javaClassName + ': ' + err.message);
		if(err.stackTrace && err.stackTrace.length) {
			var i, len;
			var str = '<a href="#" style="color: white; text-decoration: underline">show stacktrace</a><div style="display: none">';
			for(i = 0, len = err.stackTrace.length; i < len; i ++) {
				str += 'at ' + err.stackTrace[i].className + '.' + err.stackTrace[i].methodName 
					+ '(' + err.stackTrace[i].fileName + ':' + err.stackTrace[i].lineNumber + ')<br />';
			}
			str += '</div>';
			jqAjaxTopInfo.children('div.content').append(str);
			jqAjaxTopInfo.children('div.content').children('a').click(function() {
				$j(this).hide();
				jqAjaxTopInfo.css('height', 'auto');
				jqAjaxTopInfo.children('div.content').children('div').show();
				return false;
			});
		}
	} else {
		try {
			file = file.split('/').reverse()[0];
		} catch(e) {}
		jqAjaxTopInfo.children('div.content').html('<span class="warningBig"></span>Javascript error: ' + err + '<br />in file: ' + file + ':' + line);
	}
	jqAjaxTopInfo.slideDown();
	return true;
}


$j(document).ready(function() {
	if ($j('#ajaxTopInfo').length < 1) {
		var str = '<div id="ajaxTopInfo"><div class="content"></div>';
		str += '<img class="buttonHide" src="' + $j('#request_pageContext').val() + '/img/strzalka-gora-lista.gif" /></div>';
		str += '<div id="ajaxTopInfoToggler"><img class="buttonShow" src="' + $j('#request_pageContext').val() + '/img/strzalka-dol-lista-red.gif" /></div>';
		$j('body').append(str);
	}
	
	jqAjaxTopInfo = $j('#ajaxTopInfo');
	jqAjaxTopInfoBtnHide = jqAjaxTopInfo.children('img.buttonHide');
	jqAjaxTopInfoBtnShow = $j('img.buttonShow');
	jqAjaxTopInfoToggler = $j('#ajaxTopInfoToggler');
	
	jqAjaxTopInfoBtnHide.bind('click', function() {
		jqAjaxTopInfo.slideUp('normal', function() {
			jqAjaxTopInfoToggler.fadeIn();
			jqAjaxTopInfoBtnShow.bind('click', function() {
				jqAjaxTopInfo.slideDown();
				jqAjaxTopInfoToggler.fadeOut();
			});
		});
	});
	
	dwr.engine.setErrorHandler(function(message, ex) { 
		showError(ex, ex.fileName, ex.lineNumber);
	});
	
	dwr.engine.setTextHtmlHandler(function() {
		if(dwr && dwr.engine){
			//window.history.go(0); zapetlalo sie dlatego jest window.location.href
			 window.location.href = window.location.href + '';
		}
		}
	);
});

function clone(obj) {
    // A clone of an object is an empty object 
            // with a prototype reference to the original.

    // a private constructor, used only by this one clone.
            function Clone() { } 
    Clone.prototype = obj;
    var c = new Clone();
            c.constructor = Clone;
            return c;
}

/**
 * Funkcja sortuje liczby i stringi rosn�co (liczby przed stringami)
 * @param {Object} a
 * @param {Object} b
 */
function uniSort(a, b) {
	if(parseInt(a) && parseInt(b))
		return a-b;
	else if(parseInt(a))
		return -1;
	else if(parseInt(b))
		return 1;
	else
		return a == b ? 0 : ((a > b) ? 1 : -1);
}

var Utils = (function() {
	var functions = {};
	
	/**
	 * Funkcja wyci�ga np.: nazw� opcji z p�l typu selekt
	 * @param {Object} fieldType
	 * @param {Object} values
	 */
	functions.extractStringValue = function(fieldType, vals, itemId) {
		switch(fieldType) {
			case 'ENUM':
				var allOpts = vals[itemId].allOptions;
				for(var i = 0; i < allOpts.length; i ++) {
					if(allOpts[j][itemId]) {
						return allOpts[j][itemId];
					}
				}
				return null;
			break;
			default:
				return null;
		}
	}
	
	/**
	 * Formatuje string zgodnie z flagami FLAGS.
	 * @param {String} str String do sformatowania
	 * @param {Number} flags Flagi
	 * @return {String} Nowy sformatowany string
	 */
	functions.formatString = function(str, flags) {
		var ret = str;
		
		if(flags & FLAGS.STRING_TRIM_LEFT) {
			ret = String.prototype.trimLeft ? ret.trimLeft() : ret.replace(/^\s*/g, '');
			//ret = ret.replace(/^\s*/g, '')
		}
		if(flags & FLAGS.STRING_TRIM_RIGHT) {
			ret = String.prototype.trimRight ? ret.trimRight() : ret.replace(/\s*$/g, ''); 
		}

		return ret;
	}
	
	/**
	 * Funkcja usuwa z tablicy elementy undefined i null 
	 * i zwraca t� sam� znormalizowan� tablic� 
	 * @method
	 * @param {Array} arr Tablica, kt�ra ma zosta� znormalizowana (<b>UWAGA</b> - tablica zostanie zmieniona!)
	 */
	functions.normalizeArray = function(arr) {
		var i, len;
		for(i = 0, len = arr.length; i < len; i ++) {
			if(arr[i] == undefined || arr[i] == null) {
				arr.splice(i,1);
			}
		}
	}
	
	/**
	 * Funkcja znajduje eventy jQuery w postaci event_name.event_namespace.
	 * TODO - w jQuery 1.8 zmieni�y si� eventy wewn�trzne 
	 * @method
	 * @param {Object{ jqObject Obiekt jQuery, w kt�rym szukamy event�w.
	 * @param {String} eventWithNamespace Nazwa eventu do znalezienia. Je�li podamy sam namespace
	 * (<code>.name_space</code>) to zwraca wszystkie eventy z danej przestrzeni nazw,
	 * je�li podamy bez namespace (<code>event_name</code>) to zwraca wszystkie eventy event_name
	 * bez wzgl�du na przynale�no�� do przestrzeni nazw.
	 * @return {Array} Tablica znalezionych obiekt�w jQuery.Event (<code>null</code> je�li nie znaleziono �adnych element�w)
	 */
	functions.findEvents = function(jqObject, eventWithNamespace) {
		var name, namespace, key, i, len; 
		var splittedEventName = eventWithNamespace.split('.'),
			jqObjectEvents = jqObject.data('events'),
			foundEvents = [];
			
		name = splittedEventName[0];
		namespace = splittedEventName[1];
		
		// je�li podano nazw� eventu
		if(name) {
			try {
				for(i = 0, len = jqObjectEvents[name].length; i < len; i ++) {
					if(namespace && jqObjectEvents[name][i].namespace == namespace) {
						// podali�my namespace w parametrze eventWithNamespace
						// i istnieje taki event
						foundEvents.push(jqObjectEvents[name][i]);
					} else if(!namespace) { // nie podali�my namespace - nie sprawdzamy ju� namespace dla eventu
						foundEvents.push(jqObjectEvents[name][i]);
					}
				}
			} catch(e) {
				// nie ma eventu o nazwie 'name' -> czyli nie musimy sprawdza� przestrzeni nazw
				return null;
			} 
		} else { // je�li nie podano nazwy to sprawdzamy namespace dla WSZYSTKICH event�w
			for(key in jqObjectEvents) {
				for(i = 0, len = jqObjectEvents[key].length; i < len; i ++) {
					if(jqObjectEvents[key][i].namespace == namespace)
						foundEvents.push(jqObjectEvents[key][i]);
				}
				
			}
		}
		
		return foundEvents.length > 0 ? foundEvents : null;
	}
	
	return functions;
})();





