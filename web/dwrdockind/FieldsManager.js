﻿/**
 * @projectDescription DWR Dockind Fields
 * @author Kamil Omelańczuk kamil.omelanczuk@docusafe.pl
 * @version 1.0
 * @namespace pl.compan.dwr
 */

/**
 * Zarządza wyświetlaniem, uaktualnianiem i usuwaniem pól.
 * <b>Uwaga</b> Obiekt można stworzyć jedynie po wczytaniu całej strony
 * (w document.ready).
 * @classDescription Tworzy nową instancję fieldsManager. Pobiera obiekty DOM dla kolumn.
 * @author Kamil Omelańczuk
 * @version 1.0
 * @constructor
 */
FieldsManager = function() {
	/**
	 * Mapa wszystkich pól.
	 * @property
	 * @type {Object} Mapa cn->Field
	 */
	this.fields = {};
	/**
	 * Wyskakujące okienko dla słownika.
	 * @property
	 * @type {Object} Obiekt DictionaryPopup
	 */
	this.dictionaryPopup = null;
	/**
	 * Kolumna istniejąca już w html-u.
	 * @memberOf FieldsManager
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqColumn0 = $j('#column0');
	/**
	 * Kolumna istniejąca już w html-u.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqColumn1 = $j('#column1');
	/**
	 * Kolumna istniejąca już w html-u.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqColumn2 = $j('#column2');
	/**
	 * Flaga oznaczająca poprawność WSZYSTKICH pól.
	 * @property
	 * @type {Boolean}
	 */
	this.valid = true;
	/**
	 * Oryginalny obiekt Java przekazany przez DWR.
	 * @property
	 * @type {Object} Obiekt Java DictionaryField
	 */
	this.jDictionaryField = null;
	/**
	 * Fieldset, w którym umieszczane są pola słownika.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqDictionaryFieldset = null;
	/**
	 * Pole (Field) dla słownika - czyli schowane pole LONG.
	 * @property
	 * @type {Object} Obiekt Field 
	 */
	this.dictionaryField = null;
	/**
	 * Przycisk "dodaj następne" dla pól wielowartościowych.
	 */
	this.jqAddNext = null;
	/**
	 * Instancja obiektu MultipleFieldsHelper dla słowników wielowartościowych
	 * @property
	 * @type {Function} Obiekt MultipleFieldsHelper
	 */
	this.multipleFieldsHelper = null;
	/**
	 * Kopie wartości dla pól.
	 * W szczególności jeżeli pole <code>enum</code> nie jest widoczne na formatce,
	 * ale jest np.: w popupie słownika:
	 * <ul>
	 * <li> Dla pola <code>enum</code> i podobnych mapa o kluczu "wartość <code>option</code>"
	 * 		i wartości "tekst <code>option</code>".
	 * </ul>
	 * @property
	 * @type {Object} Mapa <code>cn pola --> Obiekt (mapa) z wartościami</code>
	 */
	this.cachedValues = null;
	/**
	 * Parametry (documentId, nazwa akcji, itd.) potrzebne przy komunikacji z DWR.
	 * @property
	 * @type {Object} Mapa
	 */
	this.dwrParams = null;
	/**
	 * Jeśli rysujemy w formacie szachownicy
	 */
	this.grid = null;
	/**
	 * Mapa wszystkich popupów
	*/
	this.popups = {};
	
	FieldsManager.jqColumn0 = this.jqColumn0;
	FieldsManager.jqColumn1 = this.jqColumn1;
	FieldsManager.jqColumn2 = this.jqColumn2;

    /**
    * Flaga określająca, czy na dokumencie został już raz ustawiony na jednum z pól autofocus
    */
	this.focusSet = false;
}

/**
 * Inicjalizuje mechanizm wyświetlania pól. Wymaga ustawionej zmiennej dwrParams.
 * @param {Object} options Dodatkowe opcje:
 * <ul>
 * <li>grid {Grid} Obiekt typu Grid (tabelka do rysowania)
 * </ul>
 * @exception {FieldNotInitialisedException}
 * @method
 */
FieldsManager.prototype.init = function(options) {
	var _this = this;
	// window.__start = (new Date()).getTime();
	this.dwrParams = dwrParams; //this.createDwrParams();
	if(this.dwrParams == null)
		throw new FieldNotInitialisedException('DWR Parameters not set');
	if(options) {
		this.grid = options.grid || null;
		if(this.grid != null) {
		}
	}

	var guid = FieldsManager.Blocker.getNextHookGuid();
	DwrTest.getFieldsList(this.dwrParams, {callback: function(data) {
			// console.info(data);
			// TODO TYMCZASOWO - to będzie w Javie - flaga ustawiająca dane pole na klucz główny słownika
			var curField, cn;
			for(var i = 0; i < data.length; i ++) {
				// TODO - na czas testów
				/*if(window.__PURE === true && data[i].cn == 'DWR_MPK') {
					data[i].asText = true;
				}*/


				if(data[i] != null && data[i].type == 'DICTIONARY') {
					if(data[i].dictionary.asText === true) {
						data[i].dictionary.pure = true;
						for(var j = 0; j < data[i].dictionary.fields.length; j ++) {
							data[i].dictionary.fields[j].pure = true;
						}
					}
					
					if(data[i].coords != null && (data[i].dictionary.tableView === true || data[i].dictionary.pure === true)) {
						data[i].dictionary.loadingMode = 'ALL_AT_ONCE';
					} else {
						data[i].dictionary.loadingMode = 'DEFAULT';
					}
					for(var j = 0; j < data[i].dictionary.fields.length; j ++) {
						curField = data[i].dictionary.fields[j];
						cn = curField.cn.toLowerCase();
						if(cn == 'id' || cn.lastIndexOf('_id_') != -1) {
	//						curField.hidden = false;
	//						curField.submit = true;
							//curField.dictionaryKey = true;
						}
					}
				}
				//if(data[i].type == 'DICTIONARY' && data[i])
			}
			_this.updateFields(data);
		},
		preHook: FieldsManager.Blocker.getPreHook({name: 'DwrTest.getFieldsList', guid: guid}),
		postHook: FieldsManager.Blocker.getPostHook({name: 'DwrTest.getFieldsList', guid: guid})
	});
}

FieldsManager.prototype.dispose = function() {
	if(this.dictionaryPopup) {
		this.dictionaryPopup.dispose();
	}
	var cn;
	for(cn in this.fields) {
		this.fields[cn].remove();
		this.fields[cn].dispose();
		delete this.fields[cn];
	}
}

/**
 * Inicjalizuje mechanizm wyświetlania pól dla słownika. Działa analogicznie
 * do init.
 * @param {Field} dictionaryField Obiekt typu Field dla słownika.
 * @param {Object} options [Opcjonalna] mapa z dodatkowymi wartościami:
 * <ul>
 * <li><code>dwrParams</code> parametry DWR - te same co przy <code>getFieldsList</code>
 * </ul>
 * @method
 */

FieldsManager.prototype.initDictionary = function(dictionaryField, options) {
	var _this = this;
	this.jDictionaryField = dictionaryField.jField;
	this.dictionaryField = dictionaryField;
	this.jqDictionaryFieldset = dictionaryField.jqFieldContainer;
	this.cachedValues = {};
	this.parentFieldsManager = dictionaryField.parentFieldsManager;
	
	if(options) {
		if(options.dwrParams) {
			this.dwrParams = options.dwrParams;
		}
	}
	
	this.dictionaryField.dictionaryButtons.clearDictionary.bind('click.clear_dictionary', function() {
		var iterField = null;
		for (var cn in _this.fields) {
			iterField = _this.fields[cn];
			if(iterField.isClearable()) {
				iterField.clearValue();
				iterField.jqMsg.hide();
				iterField.jqField.removeClass('invalid');
			}
			
		}
		_this.fields.id.jqVisibleField.trigger('change.changeListener');
		_this.dictionaryField.clearValue();
		_this.dictionaryField.jqMsg.hide();
		_this.dictionaryField.dictionaryButtons.addToDictionary.css('visibility', 'hidden');
		_this.submitFieldsFromDictionary();
		return false;
	});
	
	/* Zmieniamy fieldset, żeby nie powtarzał się dla kilku słowników */
	for(var i = 0; i < this.jDictionaryField.dictionary.fields.length; i ++) {
		this.jDictionaryField.dictionary.fields[i].fieldset = this.jDictionaryField.dictionary.name + '_fieldset';
	}
	
	/* Aktualizujemy tylko pola, które mają być widoczne na formatce */
	var jVisibleFields = [];
	/* Id jest zawsze na sztywno dla każdego słownika */
	for (var i = 0; i < this.jDictionaryField.dictionary.fields.length; i++) {
		if(this.jDictionaryField.dictionary.fields[i].cn == 'id') {
			jVisibleFields.push(this.jDictionaryField.dictionary.fields[i]);
		}
	}
	for(var i = 0; i < this.jDictionaryField.dictionary.fields.length; i ++) {
		for(var j = 0; j < this.jDictionaryField.dictionary.visibleFieldsCns.length; j ++) {
			//if(this.jDictionaryField.dictionary.visibleFieldsCns[j] == this.jDictionaryField.dictionary.fields[i].cn) {
				jVisibleFields.push(this.jDictionaryField.dictionary.fields[i]);
				//break;
			//}
		}
	}
	
	this.updateFields(this.jDictionaryField.dictionary.fields);

	this.dictionaryPopup = new DictionaryPopup(this.jDictionaryField.dictionary.name);
	
	
	/* Dla pól wielowartościowych zmieniamy funkcję wowyływaną po wybraniu elementu, 
	 * gdyż musimy rozróżnić poszczególne pola wielowartościowe
	 */
	if(this.jDictionaryField.dictionary.multiple === true) {
		this.orgAcceptItem = this.acceptItem;
		this.acceptItem = function(data, params) {
			if(params == undefined || params.multipleFieldsNumber == null || params.multipleFieldsNumber == undefined)
				throw new VariableNotInitialisedException('multipleFieldsNumber in acceptFunction is not defined');
			var cn, fieldCn, newVals;
			var multipleFieldsNumber = params.multipleFieldsNumber,
				newData = {};
			// zmieniamy nazwy pól z NAZWA_POLA na NAZWA_POLA_3
			for(cn in data) {
				fieldCn = cn + '_' + multipleFieldsNumber;
				// wycinamy nazwę słownika na początku
				//fieldCn = fieldCn.replace(new RegExp('^' + _this.jDictionaryField.dictionary.name + '_'), '');
				if(cn == 'id') { // id powinno miec postac NAZWA_SLOWNIKA_ID_3
					fieldCn = _this.jDictionaryField.dictionary.name + '_ID_' + multipleFieldsNumber;
				}
				newData[fieldCn] = data[cn];
			}
			// aktualizujemy wartość słownika
			newVals = _this.dictionaryField.multipleFieldsValues.slice(0);
			if(data.id == null) {
				data.id = data[_this.jDictionaryField.dictionary.name + '_ID'];
			}
			console.assert(data.id != null || data[_this.jDictionaryField.dictionary.name + '_ID'], 'Item from dictionary does not have ID');
			newVals[multipleFieldsNumber-1] = isNaN(data.id) ? data.id : Number(data.id);
			Utils.normalizeArray(newVals);
			_this.dictionaryField.setValue(newVals, {syncMultipleFieldsValues: true});
//			_this.acceptItem(newData);
			
			_this.orgAcceptItem(newData, params);
		}
	} 
	
	this.dictionaryPopup.create(
		function (data, params) {
			_this.acceptItem(data, params);
		},
		this.jDictionaryField.dictionary.popUpFields, 
		{
			title: this.dictionaryField.jField.label,
			visibleButtons: this.jDictionaryField.dictionary.popUpButtons,
			multiple: this.jDictionaryField.dictionary.multiple,
			fieldsManager: this
		}
	);
	
	if(this.jDictionaryField.dictionary.jsPopupFunction) {
		if(this.jDictionaryField.dictionary.multiple === true) {
			this.dictionaryPopup.show = function(vals, params) {
				window[_this.jDictionaryField.dictionary.jsPopupFunction](_this, false, vals, params);
			}
		} else {
			this.dictionaryField.dictionaryButtons.showDictionary.bind('click.show_dictionary', function() {
				window[_this.jDictionaryField.dictionary.jsPopupFunction](_this);
				
				return false;
			});
		}
	} else {
		if(this.jDictionaryField.dictionary.multiple === true) {
			this.dictionaryField.dictionaryButtons.showDictionary.bind('click.show_dictionary', function() {
				var vals = {};
				for (var cn in _this.fields) 
					vals[cn] = _this.fields[cn].getValue();
				_this.dictionaryPopup.show(vals);
			
				return false;
			});
		} else {
			this.dictionaryField.dictionaryButtons.showDictionary.bind('click.show_dictionary', function() {
				var vals = {};
				for (var cn in _this.fields) {
					if(_this.fields[cn].getSimpleValue())
						vals[cn] = _this.fields[cn].getValue();
				}
				_this.dictionaryPopup.show(vals);
			
				return false;
			});
		}
	}
}

/**
 * Metoda wpisująca id wybranego elementu ze słownika.
 * W parametrze <code>data</code> musi znajdować się element 
 * o kluczu <code>id</code>. Metoda ignoruje pola, których nie ma na formatce.
 * Dla każdego pola słownika zostaje ustawiona wartość.
 * @param {Object} data Mapa cn->Java Field
 * @param {Object} params Opcjonalna mapa z ustawieniami:
 * <ul>
 * <li><code>calledFromPopup = true | false</code> - czy funkcja została wywołana z okienka Popup słownika.
 * Ma to znaczenie dla wysyłanie całego formularza przy zmianie pól słownikowych (domyślnie <code>false</code>).
 * <li><code>callerField</code> - pole z którego wysłano żądanie do acceptItem - to jest pole w którym był focus,
 * gdy wybraliśmy element z listy
 * </ul>
 * @method
 */
FieldsManager.prototype.acceptItem = function(data, callParams) {
	var key, callerField;
	var params = {allOptions: false};
	var pure = false, enumValue = null, shortCn = null;
	var regExp = RegExp(/_\d+$/);

	function getEnumValues(shortCn, selectedVal, cachedValues, lookupMap) {
		var cachedVal = cachedValues[shortCn], ret = {allOptions: [], selectedOptions: []};
		var i, len;

		if(lookupMap != null && lookupMap[shortCn] != null) {
			if(lookupMap[shortCn][selectedVal] != null) {
				var opt = {};
				opt[selectedVal] = lookupMap[shortCn][lookupMap];
				ret.allOptions.push(opt);
				ret.selectedOptions.push(selectedVal + '');

				return ret;
			}
		}

		var cachedAllOptions = cachedVal.allOptions, iterOption = null, iterLabel = null, iterVal = null;
		for(i = 0, len = cachedAllOptions.length; i < len; i ++) {
			iterOption = cachedAllOptions[i];

			for(iterVal in iterOption) {
				iterLabel = iterOption[iterVal];
			}

			if((iterVal + '') === (selectedVal + '')) {
				ret.allOptions.push(iterOption);
				ret.selectedOptions.push(iterVal);
			}
		}

		return ret;
	}

	try {
		pure = this.dictionaryField.jField.dictionary.pure === true;
	} catch(e) {

	}
	for(key in data) {
		this.dictionaryField.dictionaryButtons.addToDictionary.css('visibility', 'hidden');
		try {
			if(this.fields[key].jField.type == 'ENUM' && pure === true) {
				shortCn = key.replace(regExp, '');
				if(this.cachedValues[shortCn] != null || this.lookupMap[shortCn] != null) {
					enumValue = getEnumValues(shortCn, data[key], this.cachedValues, this.lookupMap);
					// console.info('Found', shortCn, enumValue);
					this.fields[key].setValue(enumValue, params);
				}
				// console.info(key, orgValue);
				// console.dir(this.cachedValues);
			} else {
				// console.info('Plain value', key, data[key]);
				this.fields[key].setValue(data[key], params);
			}
			/* Ustawiamy poprzednią wartość, bo inaczej zostanie np.: jedna litera */
			this.fields[key].jqField.data('previousValue', data[key]);
			/* Usuwamy komunikat o walidacji pola (jeśli był) */
			this.validateField(this.fields[key]);
			if(key == 'id') {
				this.dictionaryField.setValue(data[key]);
			}
			callerField = this.fields[key];
		} catch(e) {
			console.error(e);
//			console.error(key);
//			console.error(e);
		}
	}
	// wysyłamy event - wybrano element z listy
	if(callParams && callParams.callerField)
		callerField = callParams.callerField;
	callerField.jqVisibleField.trigger('complete');
	
	// wysyłamy event change do id słownika
	// dla słowników wielowartościowych mamy id_1, itd,
	/*if(callParams && callParams.calledFromPopup === true) {
		try {
			this.fields.id.jqVisibleField.trigger('valuechange');
		} catch(e) {
			var field;
			// szukamy głównego klucza słownika (id_multipleFieldsNumber)
			for(key in this.fields) {
				field = this.fields[key];
				if(field.jField.dictionaryKey === true && field.multipleFieldsNumber == callParams.multipleFieldsNumber) {
					field.jqVisibleField.trigger('valuechange');
					break;
				}
			}
		}
	}*/
}

/**
 * Metoda aktualizuje wszystkie pola:
 * <ul>
 * <li> Wyświetlanie komunikatu (jeśli podany) dla użytkownika.
 * <li> Dodanie nowego pola (z zachowaniem kolejności).
 * <li> Uaktualnienie pola, jeśli ma być zmienione.
 * <li> Usunięcie pola.
 * <li> Ustawienie optymalnej szerokości kolumn.
 * </ul>
 * @method
 * @param {Array} jFields Tablica obiektów Java Field
 */
FieldsManager.prototype.updateFields = function(jFields, options) {
//    console.dir(jFields);
	var _this = this, cn = null, i = 0, present = false, diff = null;
	if(this.fields == null || this.fields == undefined)
		throw new VariableNotInitialisedException('fields');
	
	for(cn in this.fields) {
		for(i = 0; i < jFields.length; i ++) {
			present = false;
			if(cn == jFields[i].cn) {
				present = true;
				break;
			}
		}
		if(!present) {
			this.fields[cn].jField.status = FieldStatus.REMOVE;
		}
	}
	
	var lastField = null;
    var autofocusCn = null;
	for(i = 0; i < jFields.length; i ++) {
		var field = null, diff = null;
		var jField = jFields[i];

        if (jField.autofocus === true)
            autofocusCn = jField.cn;

		var prevField = null;
		try {
			prevField = this.fields[jFields[i-1].cn];
		} catch(e) {
			prevField = null;
		}
		jqAjaxTopInfoToggler.hide();
		jqAjaxTopInfo.hide();
		if(jField.kind == 'MESSAGE') {
			jqAjaxTopInfo.children('div.content').html(jField.label);
			
			jqAjaxTopInfo.fadeOut().slideDown();
			(document.documentElement && document.documentElement.scrollTop)
            ? document.documentElement.scrollTop = 0
            : document.body.scrollTop = 0;

			continue;
		} 
		
		switch(jField.status) {
			case FieldStatus.ADD:
				field = new Field(jFields[i], jFields[i].column, {
					singletonCal: jField.kind == 'DICTIONARY_FIELD' && (this.jDictionaryField.dictionary.multiple === true 
							&& this.jDictionaryField.dictionary.tableView === true)});
				lastField = field;
				var bCreate = true;
				if(jField.type == 'DICTIONARY') {
					field.parentFieldsManager = this;
					if(jField.dictionary.multiple === true) {
						(function() {
							var _field = field;
							MultipleFieldsHelperObserver.subscribe(_field.jField.cn, 'create', null, function() {
//								console.info('invoke "create" observer', _field.jField.cn.toString());
								_field.dictionaryFieldsManager.multipleFieldsHelper.createMultipleFields();
								if(_field.jField.dictionary.autohideMultipleDictionary === true) {
									_field.dictionaryFieldsManager.multipleFieldsHelper.toggleTable(false);
								}
								$j('body').trigger('grid-resize');
							});
							
						})();
					}
				}
				if(jField.kind == 'DICTIONARY_FIELD') {
					field.parentFieldsManager = this;
					var dic = this.jDictionaryField.dictionary;
					bCreate = false;
					
					/* Tworzymy tylko pola widoczne na formatce */
					/* TODO w edytorze powtrzają się nazwy słownika w przedrostku*/
//					dic.visibleFieldsCns = this.normalizeDictionaryFieldList(this.jDictionaryField.dictionary.name, dic.visibleFieldsCns);
//					console.warn('normalized:' + dic.visibleFieldsCns + '');
					for (var fj = 0; fj < dic.visibleFieldsCns.length; fj++) {
						if(jField.cn == dic.visibleFieldsCns[fj]) {
							jField.fieldset = this.jDictionaryField.dictionary.name + '_fieldset';
							/* Pole jest widoczne na formatce */
							//trace(jField.cn  + ' is visible');
							
							/* Ustawiamy wyszukiwanie tylko po polach z tablicy searchableFieldsCns */
							var j = 0;
							for (j = 0; j < dic.searchableFieldsCns.length; j++) {
								if (jField.cn == dic.searchableFieldsCns[j]) {
									//trace(jField.cn + ' is searchable');
									field.isSearchable = true;
									break;
								}
							}
							bCreate = true;
							break;
						} 
					}
					/* Pole nie jest widoczne - nie tworzymy pola */
					/* czyli bCreate == false */
					if(options && options.createHiddenFields === true) {
						if(bCreate == false) {
							bCreate = true;
							jField.hidden = true;
						}
					}
				}
				
				if (bCreate) {
					if (this.jqDictionaryFieldset != null) {
						field.jqColumn = this.jqDictionaryFieldset;
						field.htmlId = this.jDictionaryField.dictionary.name + '_' + field.jField.cn;
					}
					field.create(prevField);
					var assignToMainGrid = false;
					if(field.jField.coords) {
						field.coords = Grid.parseCoords(field.jField.coords);
						if(!$j.isEmptyObject(field.jField.popups)) {
						    var popupCn = null, popupCaption, popupCloseable, popupCloserefcn;
						    for(popupCn in field.jField.popups) {
//						        console.warn(field.jField.cn, field.jField.popups[popupCn]);
						        popupCaption = field.jField.popups[popupCn].caption;
                                popupCloseable = field.jField.popups[popupCn].closeable;
                                popupCloserefcn = field.jField.popups[popupCn].closerefcn;
                                if(this.popups[popupCn] == null) {
                                    this.popups[popupCn] = new Popup({cn: popupCn});
                                    this.popups[popupCn].create();
                                }
                                if(popupCaption != null)
                                    this.popups[popupCn].caption = popupCaption;
                                if(popupCloseable != null)
                                    this.popups[popupCn].closeable = popupCloseable;
                                if(popupCloserefcn != null)
                                    this.popups[popupCn].closerefcn = popupCloserefcn;
                                if(field.jField.popups[popupCn].popupcn != null) {
//                                    console.info(field.jField.cn, ' assigned to popup');
                                    this.popups[popupCn].assignField(field);
                                    assignToMainGrid = false;
                                } else {
                                    assignToMainGrid = true;
                                }
						    }
						} else {
						    this.grid.assignField(field);
						}
						if(assignToMainGrid) {
						    this.grid.assignField(field);
						}
						// ostatnie pole
						if(i == jFields.length - 1) {
							this.grid.normalizeTable();
						}
					}
					field.jField.status = FieldStatus.NOCHANGE;
					//if (!field.jField.hidden) 
					this.registerFieldListener(field);
					
					// pierwsze pole wielowartościowe wczytane z xmla ma indeks 1
					if (dic && dic.multiple === true) {
						field.multipleFieldsNumber = 1;
					}
					
					this.fields[field.jField.cn] = field;
					field.sendComplete();
				} 
			break;
			
			case FieldStatus.NOCHANGE:
				this.fields[jFields[i].cn].jField.status = FieldStatus.NOCHANGE;
				field = this.fields[jFields[i].cn];
				if(jField.type == 'DICTIONARY') {
//					for(var i = 0; i < jFields[i].dictionary.fields.length; i ++)
				}
				break;
			case FieldStatus.CHANGED:
				try {

				    diff = this.fields[jFields[i].cn].compare(jField);
					this.fields[jFields[i].cn].jField = jField;
					if(this.fields[jFields[i].cn].dictionaryFieldsManager != null) {
						this.fields[jFields[i].cn].dictionaryFieldsManager.jDictionaryField = jField;
					}
					this.fields[jFields[i].cn].refresh(diff);
				}catch(e) {
					console.error(e);
					//console.error(e, cn);
				}
			break;		
			case FieldStatus.REMOVE:
				
			break;
			
			default:
				throw 'Unsupported field status: ' + jField.status;
		}

	}
	
	var cn;
	for(cn in this.fields) {
		if(this.fields[cn].jField.status == FieldStatus.REMOVE
			/*&& this.fields[cn].jField.type != 'DICTIONARY'*/) {
			//trace('removing field ' + cn + ', kind: ' + this.fields[cn].jField.kind + ', status: ' + this.fields[cn].jField.status);
			if(this.fields[cn].jField.coords) {
				this.grid.unassignField(this.fields[cn]);
			}
			this.fields[cn].remove();
			delete this.fields[cn];
		}
	}
	
	//trace(this.fields);
	if(this.jDictionaryField /*&& this.jDictionaryField.status == FieldStatus.ADD*/) {
		/* Jeśli podamy pusty string, to otrzymamy wartości pól (np.: listę opcji w selekcie */
		var guid = FieldsManager.Blocker.getNextHookGuid();
		Dictionary.getFieldsValues(this.jDictionaryField.dictionary.name, null, {callback: function(data) {
				// console.info(_this.jDictionaryField.dictionary.name, data);
				var cn, value;
				// tylko dla pierwszego, pustego pola stworzonego z xml-a
				if(_this.jDictionaryField.dictionary.multiple === true) {
	//				console.warn('TUTAJ');
					_this.dictionaryField.multipleFieldsValues = [];
					if(_this.multipleFieldsHelper == null) {
						_this.multipleFieldsHelper = new MultipleFieldsHelper(_this);
						
						// cache dla domyślnych wartości
						if(data != null) {
							for(cn in data) {
								value = data[cn];
								if(value != null) {
									//if(value.selectedOptions != null && value.selectedOptions[0] != '') {
										if(_this.multipleFieldsHelper.fieldPropertiesCache[cn] == null) {
											_this.multipleFieldsHelper.fieldPropertiesCache[cn] = {};
										}
	//									console.info('insert in cache', value);
										_this.multipleFieldsHelper.fieldPropertiesCache[cn].defaultValue = value.selectedOptions;
										_this.multipleFieldsHelper.fieldPropertiesCache[cn].allOptions = value.allOptions;
									//}
								}
							}
						}
						
						MultipleFieldsHelperObserver.publish(_this.jDictionaryField.cn, 'create');
						MultipleFieldsHelperObserver.publish(_this.jDictionaryField.cn, 'load');
					} 
				} 
				
				//if(!_this.multipleFieldsValues) {
				_this.setFieldsValues(data);
				$j('body').trigger('grid-resize');
				if(options && options.callback) {
					options.callback();
				}
			},
			preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getFieldsValues', guid: guid}),
			postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getFieldsValues', guid: guid})
		});
	} else {
		var guid = FieldsManager.Blocker.getNextHookGuid();
		DwrTest.getFieldsValues(this.dwrParams, {callback: function(data) {
	//			console.warn('getFieldsValues');
	//			console.warn(data);
				_this.setFieldsValues(data);
				_this.fixColumnWidth();
				$j('body').trigger('grid-resize');
				if(options && options.callback) {
					options.callback();
				}
			},
			preHook: FieldsManager.Blocker.getPreHook({name: 'DwrTest.getFieldsValues', guid: guid}),
			postHook: FieldsManager.Blocker.getPostHook({name: 'DwrTest.getFieldsValues', guid: guid})
		});
		for(i = 0; i < jFields.length; i ++)
		{
            if (jFields[i].cn == autofocusCn && !this.focusSet)
            {
                this.fields[autofocusCn].jqVisibleField.focus();
                this.focusSet = true;
                //$(function() {$('[autofocus]').focus()});
                break;
            }
        }
	}
}

FieldsManager.prototype.updateCachedValues = function(val, cn) {
	var regExp = RegExp(/_\d+$/);
	if(this.cachedValues == null)
		this.cachedValues = {};
	cn = cn.replace(regExp, '');
	if(this.cachedValues[cn] == null) {
		this.cachedValues[cn] = val;
	} else {
		return;
		if(val.allOptions != null && this.cachedValues[cn].allOptions != null) {
			var newAllOptions = val.allOptions, oldAllOptions = this.cachedValues[cn].allOptions;
			var i, len, oldOption, newOption, oldOptionVal, newOptionVal, newOptionLabel;
			var j, jLen, present = false;
			var options2add = [];

			if(newAllOptions.length < oldAllOptions.length)
				return;

			console.info('updateCachedValues', newAllOptions.length, oldAllOptions.length, cn);

			for(i = 0, len = newAllOptions.length; i < len; i ++) {
				newOption = newAllOptions[i];
				present = false;
				for(newOptionVal in newOption) {
					newOptionLabel = newOption[newOptionVal];
				}
				for(j = 0, jLen = oldAllOptions.length; j < jLen; j ++) {
					oldOption = oldAllOptions[j];
					for(oldOptionVal in oldOption) {
						oldOptionLabel = oldOption[oldOptionVal];
					}
					if(oldOptionVal == newOptionVal) {
						present = true;
						break;
					}
				}
				if(present === true) {
					//console.info('Option', newOptionVal, newOptionLabel, 'is already in cache');
					continue;
				}	
				// dodajemy do cache
				options2add.push(newOption);
			}
			if(options2add.length > 0) {
				console.info('Adding options to cache', cn);
				console.dir(options2add);
				this.cachedValues[cn].allOptions = this.cachedValues[cn].allOptions.concat(options2add);
			}
		}
	}
	
}

/**
 * Ustawia wartości wszystkich pól. Dla słownika dodatkowo wysyła żądanie 
 * o wartości pól.
 * @method
 * @param {Object} vals Mapa cn->wartość pola (Obiekt Java FieldData)
 */
FieldsManager.prototype.setFieldsValues = function(vals) {
	var cn, submitQueue = [];
//	console.warn('setFieldsValues', vals);
	for(cn in vals) {
		try {
			if (this.jDictionaryField && this.jDictionaryField.dictionary.multiple === true) {
//				console.info(cn + '_1');
				this.fields[cn + '_1'].setValue(vals[cn]);
			} else {
				this.fields[cn].setValue(vals[cn]);
				if(this.fields[cn].jField.submitForm === true && this.fields[cn].jqVisibleField.attr('clicked') == 'true') {
//				    console.info('we will submit!!!', cn);
				    submitQueue.push(this.fields[cn]);
				}
			}
			if(this.fields[cn].jField.type == 'DICTIONARY') {
				var _this = this;
				var field = this.fields[cn];
				var itemId = field.dictionaryFieldsManager.dictionaryField.getSimpleValue();
				// wartości dla pól wielowartościowych wczytujemy w metodzie CreateMultipleFields
				if (itemId && !field.multipleFieldsValues) {
					(function(){
						var dicField = _this.fields[cn];
						var guid = FieldsManager.Blocker.getNextHookGuid();
						Dictionary.getFieldsValues(field.jField.dictionary.name, itemId, {callback: function(data){
								dicField.dictionaryFieldsManager.setFieldsValues(data);
							},
							preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getFieldsValues', guid: guid}),
							postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getFieldsValues', guid: guid})
						});
					})();
				}
				
				// chowamy etykietę nowy wpis, jeśli jest podane id wpisu w słowniku
				if(field.jField.dictionary.multiple == false && vals[cn] != null) {
					//console.warn('dictionary', cn, 'id is', vals[cn] + '');
					field.jqField.trigger('toggleNewDictionaryItem', [false]);
				}
			}
		} catch(e) {
			/* Nie mamy pola w kolekcji fields, ale jego wartość może być przydatna dla słownika */
			//this.cachedValues[cn] = vals[cn];
//			console.error(e);
//			console.info(this);
//			console.error('cause of the', cn);
		} 
		
		try {
			/* Wartość może być przydatna dla słownika */
			//this.cachedValues[cn] = vals[cn];
			this.updateCachedValues(vals[cn], cn);
		} catch(e) {}
		
	}
	if(this.dictionaryPopup) {
		this.dictionaryPopup.updateFieldsValues(this.cachedValues);
	}

	for(cn in this.fields) {
	    if(this.fields[cn].jField.type == 'DICTIONARY') {
            if(this.fields[cn].submitQueue != null) {
//                console.warn('FOUND', cn, this.fields[cn].submitQueue);
                submitQueue = this.fields[cn].submitQueue.concat(submitQueue);
            }
	    }
	}

	var i;
	for(i = 0; i < submitQueue.length; i ++) {
        submitQueue[i].jqVisibleField.trigger('submitForm');
	}
	
	$j(document).trigger('dwr_loaded');
}

/**
 * Ustawia walidację, formater, kalendarz, wysyłanie formularza, wyszukiwanie dla pola.
 * Dla wpisów wielowartościowych ustawia również listener do metody słownikowej getMultipleFieldsValues.
 * Dla pól lazy ustawia listener do opóźnionego wczytywania danych.
 * Metoda sprawdza, które listenery należy włączyć.
 * @method
 * @param {Object} field Obiekt Field
 */
FieldsManager.prototype.registerFieldListener = function(field) {
	var _this = this;
	
	if(field.jField.kind == 'DICTIONARY_FIELD') {
		var _this = this;
		var valOptions = {allOptions: false};
		
		var searchFun = function(term) {
			var params = {};
			var multiNum = field.multipleFieldsNumber || null;
			
			params.dictionaryName = _this.jDictionaryField.dictionary.name;
			params.fields = {};
			
			// je¶li słownik wielowarto¶ciowy, to wyci±gamy pola o indeksie multipleFieldsNumber
			for(var key in _this.fields) {
				if(key == 'id')
					continue;
				var curField = _this.fields[key];
				var strKey = curField.jField.cn;
				if(multiNum != null && curField.multipleFieldsNumber != null && curField.multipleFieldsNumber != multiNum)
					continue;
				if(multiNum != null && curField.multipleFieldsNumber != null)
					strKey = strKey.replace(/_\d+$/i, '');
				if(curField.jField.type == 'BOOLEAN' && curField.getSimpleValue() != null)
                    params.fields[strKey] = curField.getValue(valOptions);
				else if(curField.getSimpleValue())
					params.fields[strKey] = curField.getValue(valOptions);
				if(curField.jField.cn == field.jField.cn && params.fields[strKey] != null)
					params.fields[strKey].sender = true;
			}
			return params;
		}
		
		var hiddenFields = {};
		var fieldsHeader = "";
		var promptFieldsCnsCount = _this.jDictionaryField.dictionary.promptFieldsCns.length;
		for(var i = 0; i < _this.jDictionaryField.dictionary.fields.length; i ++) {
			var curField = _this.jDictionaryField.dictionary.fields[i];
			if(curField.hidden)
				hiddenFields[curField.cn] = curField;
			if (curField.label) {

				var j = _this.jDictionaryField.dictionary.promptFieldsCns.length;
				while (j-- > 0) {
					if (_this.jDictionaryField.dictionary.promptFieldsCns[j] == curField.cn) {
						fieldsHeader += '<th class="' + curField.cn + '">' + curField.label + '</th>';
					}
				}
			
			}
		}
	
		if (field.isSearchable) {
			field.jqVisibleField.autocomplete(searchFun, {
				formatItem: function(row, i, max, term) {
					var ret = '', str = '';
					
					for (var i = 0; i < promptFieldsCnsCount; i++) {
						var curCn = _this.jDictionaryField.dictionary.promptFieldsCns[i];
						if(_this.jDictionaryField.dictionary.multiple === true) {
							curCn = curCn.replace(/_\d+$/, '');
						}
						if (row && row[curCn]) {
							if(row[curCn] instanceof Date) {
//								console.info('Date:', row[curCn]);
								str = row[curCn].format(DWR.DEFAULT_DATE_FORMAT);
							} else if(typeof row[curCn] === 'boolean' && row[curCn] === true) {
							    str = '&#10003;';
							} else {
								str = row[curCn];
							}
							ret += '<td>' + str + '</td>';
						}
						else 
							ret += '<td></td>';
					}
					
					
					return (ret);
				},
				cacheLength: 1,
				minChars: 1,
				matchRegExp: field.jField.autocompleteRegExp ? RegExp(field.jField.autocompleteRegExp) : null,
				extraParams: {
					header: fieldsHeader
				},
				delay: 200,
				showDictionaryPopup: function(){
					var vals = {}, curCn;
					for (var cn in _this.fields) {
						if(_this.jDictionaryField.dictionary.multiple === true) {
							curCn = cn.replace(/_\d+$/, '');
						} else {
							curCn = cn; 
						}
						vals[curCn] = _this.fields[cn].getValue();
					}
					if(_this.jDictionaryField.dictionary.jsPopupFunction) {
						window[_this.jDictionaryField.dictionary.jsPopupFunction](_this, true, vals, 
								{multipleFieldsNumber: field.multipleFieldsNumber, callerField: field});
					} else {
						_this.dictionaryPopup.show(vals);
					}
				}
				
			}).result(function(event, data, formatted) {
				_this.acceptItem(data, {multipleFieldsNumber: field.multipleFieldsNumber, callerField: field});
			}).emptyList(function() {
				//field.jqVisibleField.trigger('complete');
			}).close(function() {
				//field.jqVisibleField.trigger('complete');
			});
		}

		eventName = 'keyup';		
		var eventPrevValName = eventName;
		switch(field.jField.type) {
			case 'ENUM':
			case 'ENUM_MULTIPLE':
				eventName = 'change';
				eventPrevValName = 'focus';
			break;
			case 'STRING':
				eventPrevValName = 'keydown';
		}
		field.jqField.bind(eventPrevValName + '.checkNewDictionaryItem', function() {
			//console.warn('setting prevval', $j(this).val());
			$j(this).data('previousValue', $j(this).val());			
		}).bind(eventName + '.checkNewDictionaryItem', function() {
//			console.info('checkNewDictionaryItem', $j(this).data('previousValue'), $j(this).val());
			/* Jeśli wartość pola zmieniła się, to nasz wpis już nie zgadza się z id */
			/* TODO zobaczyc */
			if($j(this).data('previousValue') != undefined && $j(this).data('previousValue') != $j(this).val()) {
				//console.warn('tututu', field.jField.cn);
//				console.info('wartosc', _this.dictionaryField);
				/*_this.dictionaryField.setValue('');
				if(_this.fields.id != null)
				    _this.fields.id.clearValue();*/
			}
			if(_this.dictionaryField.getSimpleValue() != '')  {
				_this.dictionaryField.jqField.trigger('toggleNewDictionaryItem', [false]);
			} else {
				var emptyDic = true;
				for(var cn in _this.fields) {
					if(_this.fields[cn].getSimpleValue() != '') {
						emptyDic = false;
						break;
					}
				}
				if (emptyDic) {
					/* Jeśli wszystkie pola są puste, to chowamy etykietę "nowy wpis" */
					_this.dictionaryField.jqField.trigger('toggleNewDictionaryItem', [false]);
				}
				else {
					_this.dictionaryField.jqField.trigger('toggleNewDictionaryItem', [true]);
				}
			}
			_this.newDictionaryEntry = emptyDic;
		});
	}
	
	if(field.keyFilter) {
		field.jqVisibleField.bind('keydown.keyFilter', function(event) {
			if (!field.keyFilter(event)) {
				if(field.jField.required)
					field.require();
				return false;
			}
			return true;
		});
	}
	
	if(field.formatter) {
		field.jqFormattedField.bind('change.format', function() {
			//field.formatter(field.jqField, $j(this), true);
			var ret = field.formatter($j(this).val(), true);
			field.jqField.val(ret.value);
			field.jqFormattedField.val(ret.formattedValue);
		});
		
		field.jqFormattedField.bind('keyup.format', function() {
			if ($j(this).val() == '') {
				field.jqField.val('');
				return false;
			}
			var ret = field.formatter($j(this).val());
			field.jqField.val(ret.value);
			$j(this).val(ret.formattedValue);
		});
		
	}
	
	if(field.rangeChecker) {
		field.jqVisibleField.bind('keyup.rangeCheck', function() {
			var res = field.rangeChecker(field.jqField.val());
			if(!res.inRange) {
				field.errorMsg = res.message;
				field.showError();
				field.valid = false;
			} else {
				field.errorMsg = null;
				field.hideError();
				field.valid = true;
			}
		});
	}
	
	var validateEventName = ['keyup'];
	var calcEventName = ['change'];
	var popupEventName = ['valuechange'];
	var lazyLoadEventName = ['focus'];
	switch(field.jField.type) {
		case 'ENUM':
		case 'ENUM_MULTIPLE':
			if($j.browser.msie === true) {
				validateEventName = ['click', 'keyup'];
			} else {
			validateEventName = ['mouseup', 'keyup'];
			}
		break;
		case 'ENUM_AUTOCOMPLETE':
			validateEventName = ['itemSelected'];
		break;
		case 'DATE':
			validateEventName = ['keyup', 'change'];
			calcEventName = ['dateSelect'];
		break;
		case 'MONEY':
			calcEventName = ['valuechange'];
		break;
		case 'BUTTON':
			calcEventName = ['valuechange'];
		break;
		case 'BOOLEAN':
			calcEventName = ['click'];
		break;
	}
	
	// Eventy walidacji
	field.jqVisibleField.bind('validate', function() {
		_this.validateField(field);
		if(field.jField.submit && field.jField.submitTimeout != 0 
			&& field.valid && _this.valid) {
			_this.submitFieldWithTimeout(field);
		}
	});
	var validateEventNames = validateEventName.concat(['']).join('.validate ').trim();
	field.jqVisibleField.bind(validateEventNames, function() {
		$j(this).trigger('validate');
	});

	/* Eventy popupa */
	var popupEventNames = popupEventName.concat(['']).join('.popup ').trim();
//	console.info(field.jField.popup);
	if(!$j.isEmptyObject(field.jField.popups)) {
	    var popupCn = null, curPopup = null;
	    for(popupCn in field.jField.popups) {
	        curPopup = field.jField.popups[popupCn];
	        // do walidacji popupa
	        field.jqVisibleField.bind('validate.popup', {popups: this.popups, field: field}, function(event) {
	            var field = event.data.field, popups = event.data.popups;
//	            console.info(field.jField.cn, curPopup, popups);
	            if(field.jField.validate === true) {
	                var valid = _this.popups[curPopup.popupcn].validate();
//	                console.info('valid', valid);
	                if(valid) {
	                    field.jqVisibleField.trigger('valuechange');
	                } else {
	                    $j(this).removeAttr('clicked');
	                    $j(this).removeAttr('disabled');
	                }
	            }
	        });
	        if(curPopup.closepopupcn != null) {
	            field.jqVisibleField.bind(popupEventNames, {popups: this.popups, field: field}, function(event) {
                    var field = event.data.field, popups = event.data.popups;
                    popups[curPopup.closepopupcn].show(false);
                });
	        }
	        if(curPopup.showpopupcn != null) {
                if($j.isEmptyObject(this.popups)) {
                    this.popups = this.parentFieldsManager.popups;
                }
                field.jqVisibleField.bind(popupEventNames, {popups: this.popups, field: field}, function(event) {
                    var field = event.data.field, popups = event.data.popups;
                    popups[curPopup.showpopupcn].show(true);
                });
            }
	    }
	}

	// lazy - opóźnione wczytywanie
	if(field.jField.type == 'ENUM' && $j.browser.msie === true && $j.browser.version <= 8) {
		// w ie8 jest specjalny event
		field.jqVisibleField[0].attachEvent('onbeforeactivate', function() {
			field.jqVisibleField.trigger('lazyload');
		});		
	} else {
		var lazyLoadEventNames = lazyLoadEventName.concat(['']).join('.lazyload ').trim();
		if(field.jField.lazyEnumLoad === true && field.jField.autocomplete === false) {
			field.jqVisibleField.bind(lazyLoadEventNames, function() {
				// console.info(field.jField.cn, field.jField.lazyEnumLoad);
				$j(this).trigger('lazyload');
			});
		}		
	}
	
	
	/* Obsługa zdarzenie dla metody Dictionary.getMultipleFieldsValues */
	var calculateEventNames = calcEventName.concat(['']).join('.calculateDicValues ').trim();
	var pure = false;
	if(this.fieldsManager != null && this.fieldsManager.dictionaryField.jField.dictionary.pure === true)
		pure = true;
	if(this.jDictionaryField || pure === true) {
//		console.info('Register', field.jField.cn);
		field.jqVisibleField.bind(calculateEventNames, function() {
			if(_this.multipleFieldsHelper) {
				_this.multipleFieldsHelper.calculateValues.call(_this, field);
			} else {
				/**
				 * TODO - wydzielić ten kawałek ?
				 */
				vals = _this.getValues(false);
				vals[field.jField.cn].sender = true;
//				console.warn('multiple');
//				console.warn(vals);
				var guid = FieldsManager.Blocker.getNextHookGuid();
				// console.info(JSON.stringify(vals));
				Dictionary.getMultipleFieldsValues(_this.dwrParams, _this.jDictionaryField.dictionary.name, vals, {callback: function(data) {
					    var submitQueue = [];
	//					console.info(data);
						var cn, indexedCn, field, requiredIndexedCn = null;
						if(data) {
							for(cn in data) {
								try {
									_this.fields[cn].setValue(data[cn], {allOptions: true, addToLookup: true});
								} catch(e) {
									console.error(e);
	//								console.error(cn);
	//								console.error(e);
								}
								
								try {
									_this.fields[cn].jqVisibleField.trigger('validate');
								} catch(e) {
									console.error(e);
								}
								if(data[cn] == 'not-required') {
									requiredIndexedCn = cn;
								}
							}
						} 
						if(requiredIndexedCn == null) {
	//						console.info('przywracamy required');
							for(cn in _this.fields) {
								field = _this.fields[cn];
								field.revertRequiredState();
							}
						}

						for(cn in _this.fields) {
						    if(_this.fields[cn].jqVisibleField.attr('clicked') == 'true') {
	//                            console.info('we will submit from dic!!!', cn);
	                            submitQueue.push(_this.fields[cn]);
	                        }
						}

						var i;
	//					console.info(submitQueue);
						_this.dictionaryField.submitQueue = submitQueue;
	                    /*for(i = 0; i < submitQueue.length; i ++) {
	                        submitQueue[i].jqVisibleField.trigger('submitForm');
	                    }*/
					},
					preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getMultipleFieldsValues', guid: guid}),
					postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getMultipleFieldsValues', guid: guid})
				});
			}
			//return false;
		});
	}
	
	if(field.jField.kind == 'DICTIONARY_FIELD') {
		field.jqVisibleField.bind('complete', function completeEventHandler(event) {
			// jeśli pole nie ma submita, to przekazujemy event 'change' do id słownika
			try {
				field.parentFieldsManager.fields.id.jqVisibleField.trigger('valuechange');
			} catch(e) {
				field.jqVisibleField.trigger('valuechange');
			}
		});
	} 
	
	// chowa lub pokazuje etykietę nowy spis, jeśli zostanie ustawione id słownika
	if(field.jField.type == 'DICTIONARY' && field.jField.dictionary.multiple !== true) {
		field.jqField.bind('toggleNewDictionaryItem', function(event, show) {
			var jqAddToDic = field.dictionaryButtons.addToDictionary;
//			console.warn('toggleNewDictionaryItem', show);
			if(show === true) {
				jqAddToDic.css('visibility', 'visible');
			} else {
				jqAddToDic.css('visibility', 'hidden');
			}
		});
	}
	
	/* Wysyłanie pól, które mają ustawiony submit */
	if(field.jField.submit && field.jField.submitTimeout == 0 /*&& field.jField.kind != 'DICTIONARY_FIELD'*/) {
		field.jqVisibleField.bind('valuechange', function eventHandler(event) {
//			console.warn('valuechange0', this.id, $j(this).val());
			_this.submitFields(field);
		});
	}
	
	/* Nie rozwijanie selecta z tylko jedną opcją */
	if(field.jField.type == 'ENUM') {
		field.jqVisibleField.bind('mousedown.optioncheck', function() {
			var jqOptions = $j(this).children('option');
			if(jqOptions.length <= 1) {
				return false;
			}
			
			return true;
		});
	}
	
	field.jqField[0].initialvalue = field.getSimpleValue();
	field.assignValueChangeEvent();
}


/**
 * Sprawdza poprawność pola i ustawia poprawność całego formularza.
 * @method
 * @param {Object} field Obiekt Field
 * @return {Boolean} Poprawność pola.
 */
FieldsManager.prototype.validateField = function(field) {
	/* Pole obowiązkowe lub słownik */
	if(field.jField.required || field.jField.dictionary) {
		if(!field.require()) {
			field.showError();
		} else {
			field.hideError();
			/* Jeśli pole ma przypisany walidator to sprawdzamy, czy zgadza się pattern */
			if((field.validator || field.jField.type == 'DATE') && !field.validate()) {
				field.showError();
			}
		}
	} else {
	    field.valid = true;
		field.hideError();
		/* Jeśli pole ma przypisany walidator to sprawdzamy, czy zgadza się pattern */
		if((field.validator || field.jField.type == 'DATE') && !field.validate()) {
			field.showError();
		}
	}
	
	if(field.rangeChecker) {
		var ret = field.rangeChecker(field.jqField.val());
		if(!ret.inRange)
			field.valid = false;
	}
	
	/* Ustawiamy poprawność całego formularza */
	if(!field.valid)
		this.valid = false;
	else {
		this.valid = true;
		for(key in this.fields) {
			var item = this.fields[key];
			if(!item.valid) {
				this.valid = false;
				break;
			}
		}
	}
	return field.valid;
}

/**
 * Sprawdza poprawność całego formularza.
 * @method
 * @return {Boolean} Poprawność całego formularza.
 */
FieldsManager.prototype.validate = function() {
	var cn = null, validationNeeded = false,
	iterField = null;

	// dowolne pole ze słownika zostało zmienione przez użytkownika
	var anyFieldChangedByUser = false;
	
	// dowolne pole w slowniku wielowartościowym jest nieprawidłowe
	var allFieldsValid = true;
	
	// jeśli cały słownik jest obowiązkowy, to przeprowadzamy walidację wszystkich pól
	if(this.dictionaryField && this.dictionaryField.jField.required === true)
		anyFieldChangedByUser = true;
	
	if(this.dictionaryField) {
		for(cn in this.fields) {
			iterField = this.fields[cn];
			// dowolne pole zmienione w słowniku powoduje walidację reszty pól
			if(iterField.changedByUser === true) {
				anyFieldChangedByUser = true;
			}
		}
	}
	
	for(cn in this.fields) {
		iterField = this.fields[cn];
		validationNeeded = iterField.jField.hidden !== true;

		// jeśli słownik nie jest obowiązkowy i żadne pole nie zostało zmienione, to nie przeprowadzamy walidacji
		if(this.dictionaryField && this.dictionaryField.jField.required !== true && anyFieldChangedByUser !== true) {
			validationNeeded = false;
		}
		
		// zawsze przeprowadzamy walidację słowników
		if(iterField.jField.type == 'DICTIONARY') {
			validationNeeded = true;
		}
		
		// jeśli pole znajduje się w słowniku i jego wartość została zmieniona
		// przez użytkownika, to również przeprowadzamy walidację
		if(anyFieldChangedByUser) {
			validationNeeded = true;
		}

		if(Popup.FieldPool.isPopupField(iterField)) {
		    // jeśli pole jest popupie i jest wyświetlone (widoczne), to przeprowadzamy walidację
//		    console.warn('field on popup', iterField.jField.cn);
            if(Popup.FieldPool.isVisibleField(iterField)) {
                validationNeeded = true;
            } else {
                validationNeeded = false;
            }
		}

		if(validationNeeded) {
			if(this.validateField(iterField) == false) {
				allFieldsValid = false;
//				console.warn('cause', cn);
			}
		}
	}
	
	// żadne pole w słowniku nie zostało zmienione oraz słownika nie jest obowiązkowy,
	// wtedy wynik walidacji jest pomyślny
	if(this.dictionaryField && this.dictionaryField.jField.required == false && anyFieldChangedByUser == false) {
		this.valid = true;
	}
	
	// przynajmniej jedno pole jest nieprawidłowe
	// w słowniku wielowartościowym
	if(this.dictionaryField && this.dictionaryField.jField.dictionary.multiple === true 
			&& this.dictionaryField.jField.required === true) {
		this.valid = allFieldsValid;
		
//		console.warn(this.dictionaryField.jField.cn, 'all fields valid', allFieldsValid);
	}
	
	return this.valid;
}

/**
 * Wysyła żądanie do DWR z czasowym opóźnieniem dla pola STRING.
 * @method
 * @param {Object} field Obiekt Field
 */
FieldsManager.prototype.submitFieldWithTimeout = function(field) {
	var _this = this;
	
	try {
		clearInterval(field.counterHandler);
		field.counterValue = field.jField.submitTimeout;
		field.jqProgress.width(0).show();
		
		field.jqMsg.addClass('messageSubmit').html(DWR.MSG.SAVE_WITH_TIMEOUT).show().unbind('click.submit').bind('click.submit', function() {
			clearInterval(field.counterHandler);
			_this.submitFields();
			field.jqProgress.hide();
			field.jqMsg.hide().removeClass('messageSubmit');
		});
	} catch(e) {
		console.error(e);
		//trace('counterHandler is not set');
	}

	field.counterHandler = setInterval(function() {
		if(field.valid && _this.valid) {
			var newWidth = field.jqProgress.width() + field.pixelInterval;
			newWidth = newWidth > field.intervalMaxWidth ? field.intervalMaxWidth : newWidth;
			field.jqProgress.css('width',  newWidth+ 'px');
			
			if(field.counterValue > 0)
				field.counterValue -= 100;
			else {
				clearInterval(field.counterHandler);
				_this.submitFields();
				field.jqProgress.hide();
				field.jqMsg.hide().removeClass('messageSubmit');
			}
		} else {
			clearInterval(field.counterHandler);
			field.jqProgress.hide();
			field.jqMsg.hide().removeClass('messageSubmit');
		}
	}, 100);
}

/**
 * Wysyła wszystkie pola do DWR.
 * @exception {VariableNotInitialisedException} Rzuca wyjątek jeśli pola 
 * FieldsManager nie są ustawione.
 * @method
 */
//var __hidden = true;
FieldsManager.prototype.submitFields = function(causeField, dictionary) {
	var _this = this, cn = null;
	
	// Jeśli submit został wysłany z pola słownikowego,
	// to musimy wziąć rodzica (fieldsManager w którym znajduje się słownik
	if(this.parentFieldsManager) {
		this.parentFieldsManager.submitFields(causeField, this);
		return;
	}
	
	if(_this.fields == null)
		throw new VariableNotInitialisedException('fields');
	
	var vals = this.getValues();
	if(causeField != null) {
		var causeFieldCn;
	    if (dictionary && dictionary.jDictionaryField) {
	        causeFieldCn = dictionary.jDictionaryField.cn
	    } else {
	        causeFieldCn = causeField.jField.cn
	    }
		for(cn in vals) {
			if(cn == causeFieldCn) {
				vals[cn].sender = true;
				break;
			}
		}
		causeField.loading.fire({progress: 0, start: true});
	}
//	console.info('set');
//	console.info(vals);
//	console.trace();
	var guid = FieldsManager.Blocker.getNextHookGuid();
	DwrTest.setFieldsValues(vals, this.dwrParams, {callback: function(data) {
			_this.updateFields(data);
			if(causeField != null) {
				causeField.loading.fire({progress: 100, end: true});
			}
		},
		preHook: FieldsManager.Blocker.getPreHook({name: 'DwrTest.setFieldsValues', guid: guid}),
		postHook: FieldsManager.Blocker.getPostHook({name: 'DwrTest.setFieldsValues', guid: guid})
	});
}

/**
 * Zwraca wszystkie wartości pól (łącznie ze słownikami) w postaci mapy.
 * @method
 * @return {Object} Wartości pól w postaci mapy
 */
FieldsManager.prototype.getValues = function(isDictionary, options) {
	var key, dicKey;
	var vals = {};
	
	for (key in this.fields) {
		vals[key] = this.fields[key].getValue({allOptions: false});
		if(this.fields[key].jField.type == 'DICTIONARY') {
			vals[key] = {type: 'DICTIONARY'};
			vals[key].dictionaryData = this.fields[key].dictionaryFieldsManager.getValues(true);
		}
	}
	
	if(isDictionary === true && this.checkValuesChanges() == true) {
		try {
			vals.id.stringData = '';
		} catch(e) {}
	}
	
	return vals;
}

/**
 * Zwraca <code>true</code>, jeśli zmieniła się wartość któregokolwiek z pól fieldsManagera
 */
FieldsManager.prototype.checkValuesChanges = function() {
    // TODO
    return false;
	var key, field;
	for(key in this.fields) {
		field = this.fields[key];
		if(field.jqVisibleField.data('previousValue') != null && field.jqVisibleField.data('previousValue') != field.getSimpleValue()) {
//		    console.info('checkValuesChanges', key, field.jqVisibleField.data('previousValue'), field.getSimpleValue());
			return true;
		}
	}
	
	return false;
}

/**
 * Wyróżnia pola, np.: jeśli nie przeszły walidacji.
 * @method
 * @param {String} filterName Nazwa filtra używanego do animacji.
 * @exception {FilterUnsupportedNameException} Rzuca wyjątek dla nieznanego filtra.
 */
FieldsManager.prototype.blinkFields = function(filterName) {
	switch(filterName) {
		case FieldsManager.BlinkFilter.INVALID:
			for(key in this.fields) {
				var item = this.fields[key];
				if(!item.valid)
					item.jqField.effect('bounce');
			}
		break;
		default:
			throw new FilterUnsupportedNameException(filterName);
	}
}

/**
 * Wysyła cały formularz DWR (tj. główny <code>fieldsManager</code>),
 * jeśli przynajmniej jedno z pól słownika ma flagę <code>submit</code>.
 * Metoda powinna być wywoływana w kontekście słownika <code>this = dictionaryFieldsManager</code>.
 * @method
 */
FieldsManager.prototype.submitFieldsFromDictionary = function() {
	// sprawdzamy, czy któreś z pól jest oznaczone jako 'submit',
	// jeśli tak, to oznacza że musimy wysłać cały formularz.
	// Najprościej to zrobić przez wysłanie zdarzenia submit to pola słownika
	for(key in this.fields) {
		if(this.fields[key].jField.submit === true) {
			// szukamy pierwszego eventu .submit dla pola słownikowego
			var evts = Utils.findEvents(this.dictionaryField.jqField, '.submit');
			if(evts) {
				this.dictionaryField.jqField.trigger(evts[0]);
			}
			break;
		}
	}
}

/**
 * Zwraca mapę pól
 * @return {Map} Mapa pól - tylko do odczytu!
 */
FieldsManager.prototype.getFields = function() {
	return this.fields;
}

/**
 * Usuwa pole (z wewnętrznej listy) - żeby usunąć pole z formatki należy wywołać field.remove()
 * @param  {String} fieldCn Cn pola do usunięcia
 * @return {Boolean}         <code>true</code> jeśli udało się usunąć pole
 */
FieldsManager.prototype.deleteField = function(fieldCn) {
	return delete this.fields[fieldCn];
}

FieldsManager.prototype.fixColumnWidth = function() {
	var availWidth = this.jqColumn0.parent('.dwrDockindContainer').width();
	var cols = new Array(this.jqColumn0, this.jqColumn1, this.jqColumn2);
	var neededWidth = 0;
	for(var i = 0; i < cols.length; i ++) {
		var newWidth = cols[i].data('expWidth')
		if(newWidth != null) {
			if (newWidth > cols[i].width() && newWidth < availWidth) {
				cols[i].width(newWidth);
				availWidth -= newWidth;
			}
		}
	}
}

FieldsManager.prototype.normalizeDictionaryFieldList = function(dicName, fieldNames) {
	var i, len, shortName,
		normalizedNames = [],
		reg = RegExp('^(' + dicName + '_)*', 'g');
	
	for(i = 0, len = fieldNames.length; i < len; i ++) {
		shortName = fieldNames[i].replace(reg, '');
		normalizedNames.push(dicName + '_' + shortName);
	}
	
	return normalizedNames;
}

/**
 * Mechanizm blokowania pól w trakcie trwania żądania ajaksowego.
 */
if(DWR.BLOCKER === true) {
FieldsManager.Blocker = (function() {
	/**
	 * Licznik żądań - rozpoczęcie żadania zwiększa licznik, zakończenie żądania zmniejsza
	 * @type {Number}
	 */
	var ajaxCounter = 0;
	/**
	 * Statystyki żądań
	 */
	var stats = new buckets.MultiDictionary(function(key) {
			return key;
		}, function(val1, val2) {
			return val1.guid === val2.guid;
		});
	/**
	 * Uchwyt do setTimeout dla blokowania pól
	 * @type {Handle}
	 */
	var hLock = null;
	/**
	 * Uchwyt do setTimeout dla odblokowania pól
	 * @type {Handle}
	 */
	var hRelease = null;
	/**
	 * Aktualny stan zablokowania pól
	 * @type {Boolean}
	 */
	var locked = false;
	/**
	 * Unikalny guid dla każdego żądania
	 * @type {Number}
	 */
	var guid = 0;
	
	/**
	 * Blokuje formatkę
	 */
	function block(block) {
		if(block === true) {
			locked = true;
			$j.blockUI({message: DWR.MSG.BLOCKER_LOADING, fadeIn: 0, fadeOut: 0});
		} else {
			locked = false;
			$j.unblockUI();
		}
	}

	// skróty dla setTimeout
	var blockFields = function() {
		block(true);
		hLock = null;
	}
	var releaseFields = function() {
		block(false);
		hRelease = null;
	}

	// reset css dla blockUI
	$j.blockUI.defaults.css = {};

	return {
		/**
		 * Rejestruje rozpoczęcie żądania ajaksowego
		 * @param  {Map} options (opcjonalne) {name: identifikator żądania (opcjonalne)}
		 */
		inc: function(options) {
			ajaxCounter ++;
			if(DWR.BLOCKER_STATS === true && options != null && options.name != null && options.guid) {
				if(options.guid == 1) {
					stats.set('__GLOBAL', {
						name: options.name,
						guid: -1,
						start: new Date()
					});
				}

				stats.set(options.name, {
					name: options.name,
					guid: options.guid,
					start: new Date()
				});
			}
			//console.info('inc ajaxCounter', ajaxCounter);

			if(ajaxCounter == 1) {
				
				if(hRelease != null) {
					// console.info('Clearing release timeout', ajaxCounter);
					clearTimeout(hRelease);
					hRelease = null;
				}
				
				// jeśli formatka nie jest już zablokowana, to blokujemy
				if(hLock == null && locked === false) {
					// console.info('Setting lock timeout', ajaxCounter);
					hLock = setTimeout(blockFields, DWR.BLOCK_TIMEOUT);
				} 
			}
		},
		/**
		 * Rejestruje zakończenie żądania ajaksowego
		 * @param  {Map} options 
		 * @see inc
		 */
		dec: function(options) {
			ajaxCounter --;
			if(ajaxCounter < 0)
				throw 'Blocker error! ajaxCounter = ' + ajaxCounter;
			if(DWR.BLOCKER_STATS === true && options != null && options.name != null && options.guid != null) {
				if(!stats.containsKey(options.name))
					throw 'name ' + options.name + ' has not been registered';

				stats.get('__GLOBAL')[0].end = new Date();

				buckets.arrays.forEach(stats.get(options.name) , function(val) {
					if(val.guid === options.guid) {
						val.end = new Date();
						return false;
					}
				});
			}
			//console.info('dec ajaxCounter', ajaxCounter);
			if(ajaxCounter == 0) {
				if(hLock != null) {
					// console.info('Clearing lock timeout');
					clearTimeout(hLock);
					hLock = null;
				}

				// jeśli jest już timeout na odblokowanie pól, to go wydłużamy
				if(hRelease != null) {
					// console.info('Clearing release timeout');
					clearTimeout(hRelease);
					hRelease = null;
				}
				// console.info('Setting release timeout', ajaxCounter);
				hRelease = setTimeout(releaseFields, DWR.RELEASE_TIMEOUT);
			}
		},
		/**
		 * Zwraca liczbę żądań ajaksowych będących w toku
		 * @return {Number} Liczba trwających żądań
		 */
		getAjaxCounter: function() {
			return ajaxCounter;
		},
		/**
		 * Aktualny stan zablokowania pól
		 * @return {Boolean} Pola są zablokowane
		 */
		isLocked: function() {
			return locked;
		},
		/**
		 * PreHook dla dwr
		 * @param  {Map} options (opcjonalne) {name: identifikator żądania (opcjonalne)}
		 * @return {function} Funkcja dla preHook
		 */
		getPreHook: function(options) {
			return function() {
				FieldsManager.Blocker.inc(options || null);
			}
		},
		/**
		 * PostHook dla dwr
		 * @param  {Map} options
		 * @see getPreHook
		 * @return {function} Funkcja dla postHook
		 */
		getPostHook: function(options) {
			return function() {
				FieldsManager.Blocker.dec(options || null);
			}
		},
		/**
		 * Zwraca następny (unikalny) guid dla żądania. Ten sam guid powinien być wykorzystywany
		 * dla tokena w pre- i postHook
		 * @return {Number} Unikalny guid
		 */
		getNextHookGuid: function() {
			return ++guid;
		},
		/**
		 * Stytstyki żądań
		 * @return {Map} Statystyki żądań
		 */
		getStats: function() {
			buckets.arrays.forEach(stats.values(), function(val) {
				val.time = val.end.getTime() - val.start.getTime();
			});
			return stats.values();
		}
	}

})();
} else {
// wyłączamy blocker
FieldsManager.Blocker = {};
FieldsManager.Blocker.getNextHookGuid = function() { return -1; };
FieldsManager.Blocker.inc = FieldsManager.Blocker.dec = function() {};
FieldsManager.Blocker.getPreHook = FieldsManager.Blocker.getPostHook = function() { return function() {} };
}

if(window.Hook !== undefined)
	FieldsManager.Hook = new Hook(FieldsManager);

/* Stałe */
FieldsManager.BlinkFilter = { 'INVALID' : 1 };
