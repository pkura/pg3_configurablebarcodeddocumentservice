/**
 * @projectDescription DWR Dockind Fields
 * @author Kamil Omela�czuk kamil.omelanczuk@docusafe.pl
 * @version 1.0
 * @namespace pl.compan.dwr
 */

/**
 * Zarz�dza wy�wietlaniem, uaktualnianiem i usuwaniem p�l.
 * @classDescription Tworzy now� instancj� Field. Przypisuje pobrane wcze�niej 
 * w FieldsManager kolumny DOM.
 * @author Kamil Omela�czuk
 * @version 1.0
 * @constructor
 */
function Field(jField, column, options) {
	/**
	 * Element DIV, w kt�rym znajduj� si� wszystkie elementy pola:
	 * <ul>
	 * <li>jqField
	 * <li>jqLabel
	 * <li>jqMsg
	 * <li>jqProgress
	 * </ul>
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqFieldContainer = null;
	/**
	 * Pole (select, input, textarea, checkbox, hidden), kt�re jest wy�wietlane
	 * na formatce.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqField = null;
	/**
	 * Etykieta pola
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqLabel = null;
	/**
	 * Sformatowane pole (w/g patternu), kt�re widzi u�ytkownik: <br/>
	 * np.: dla p�l MONEY
	 */
	this.jqFormattedField = null;
	/**
	 * Pole widoczne dla u�ytkownika:
	 * <ul>
	 * <li><code>jqField</code> dla zwyk�ych p�l
	 * <li><code>jqFormattedField</code> dla p�l z formaterem (np.: MONEY)
	 * </ul>
	 */
	this.jqVisibleField = null;
	/**
	 * Komunikat wy�wietlany obok pola (przy walidacji).
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqMsg = null;
	/**
	 * Kolumna, w kt�rej znajduje si� fieldset (a w nim pole).
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqColumn = null;
	/**
	 * Fieldset (grupa), w kt�rej znajduje si� pole.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqFieldset = null;
	/**
	 * Pasek post�pu u�ywany w przypadku wysy�ania pola tekstowego 
	 * z op�nieniem.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqProgress = null;
	/**
	 * Oryginalny obiekt Java Field otrzymany z DWR
	 * @property
	 * @type {Object} Obiekt Java Field
	 */
	this.jField = jField;
	/**
	 * Walidator przypisany do pola. Mo�e to by� walidator automatycznie 
	 * dodawany (np.: dla p�l typu INTEGER) lub stworzony na bazie wyra�enia
	 * regularnego.
	 * @property
	 * @type {Object} Obiekt Validator
	 */
	this.validator = null;
	/**
	 * Formater przypisany do pola.
	 * @property
	 * @type {Function} Funkcja Formatter
	 */
	this.formatter = null;
	/**
	 * Filtr klawiszy przekazywanych do formatera.
	 * @property
	 * @type {Function} Funkcja KeyFilter
	 */
	this.keyFilter = null;
	/**
	 * Sprawdza czy pole mie�ci si� w zakresie (np.: INTEGER)
	 */
	this.rangeChecker = null;
	/**
	 * Flaga okre�laj�ca poprawno�� pola
	 * @property
	 * @type {Boolean}
	 */
	this.valid = true;
	/**
	 * �a�cuch tekstowy z komunikatem dotycz�cym walidacji pola.
	 * @property
	 * @type {String}
	 */
	this.errorMsg = null;
	/**
	 * Flaga dla p�l s�ownik�w okre�laj�ca czy wyszukiwanie mo�e odbywa� si�
	 * po tym polu
	 * @property
	 * @type {Boolean}
	 */
	this.isSearchable = false;
	/**
	 * Osobny FieldsManager dla s�ownik�w.
	 * @property
	 * @type {Object} Obiekt FieldsManager
	 */
	this.dictionaryFieldsManager = null;
	/**
	 * �a�cuch tekstowy z <code>id</code> elementu jqField. Domy�lnie
	 * przypisany na <code>cn</codename> pola.
	 * @property
	 * @type {String}
	 */
	this.htmlId = jField.cn;
	/**
	 * Mapa przycisk�w dla s�ownika
	 * @property
	 * @type {Object} Mapa cn przycisku -> obiekt jQuery
	 */
	this.dictionaryButtons = null;
	/**
	 * Flagi w��czonych przycisk�w
	 */
	this.availDictionaryButtons = {};
	/**
	 * Mapa dodatkowych element�w o kluczach:<br/>
	 * Dla za��cznik�w:
	 * <ul>
	 * <li><code>jqAddAtt</code> - dodaj kolejny za��cznik
	 * <li><code>jqRemoveAtt</code> - usu� za��cznik
	 * </ul>
	 * @property 
	 * @type {Object} Mapa cn elementu -> obiekt jQuery
	 */
	this.additionalFields = null;
	/**
	 * Warto�ci p�l wielowarto�ciowych dla s�ownika.
	 * @property
	 * @type {Array}
	 */
	this.multipleFieldsValues = null;
	/**
	 * Indeks kolejnego pola wielowarto�ciowego
	 */
	this.multipleFieldsNumber = null;
	/**
	 * R�ne opcje dla p�l
	 */
	this.options = options;
	/**
	 * jQuery Deferred - wczytywanie danych
	 */
	this.loading = $j.Callbacks();

	switch(column) {
		case 1:
			this.jqColumn = FieldsManager.jqColumn1;
		break;
		case 0:
		default:
			this.jqColumn = FieldsManager.jqColumn0;
	}

	if(this.jField.pure === true) {
		PureField.call(this);
		return;
	}
	
	switch(this.jField.type) {
		case 'STRING':
			CommonInputField.call(this);
			StringField.call(this);
		break;
		case 'TEXTAREA':
			TextareaField.call(this);
		break;
		case 'HTML':
			HtmlField.call(this);
		break;
		case 'HTML_EDITOR':
			HtmlEditorField.call(this);
		break;
		case 'ENUM':
			EnumField.call(this);
		break;
		case 'ENUM_MULTIPLE':
			EnumMultipleField.call(this);
		break;
		case 'DATE':
			DateField.call(this);
		break;
		case 'TIMESTAMP':
			TimestampField.call(this);
		break;
		case 'MONEY':
			CommonInputField.call(this);
			MoneyField.call(this);
		break;
		case 'INTEGER':
			CommonInputField.call(this);
			IntegerField.call(this);
		break;
		case 'LONG':
			CommonInputField.call(this);
			LongField.call(this);
		break;
		case 'BOOLEAN':
			BooleanField.call(this);
		break;
		case 'LINK':
			LinkField.call(this);
		break;
		case 'DICTIONARY':
			DictionaryField.call(this);
		break;
		case 'ATTACHMENT':
			AttachmentField.call(this);
		break;
		case 'BUTTON':
			ButtonField.call(this);
		break;
	}
}

/*
 * Mixins
 */
var BaseField = {
	/**
	 * Tworzy etykiet� dla pola (<code>label</code>):
	 * <ul> 
	 * <li>ustawia atrybut <code>for</code></li>
	 * <li>dodaje dwukropek po nazwie etykiety </li>
	 * <li>dodaje klas� CSS <code>required</code>, je�li pole jest obowi�zkowe
	 * <li>ukrywa etykiet�, je�li pole jest niewidoczne
	 * </ul>
	 * @method
	 * @param {Field} field Obiekt typu Field, dla kt�rego zostanie utworzona etykieta
	 * @return {Object} Obiekt jQuery utworzonej etykiety 
	 */
	createLabel: function(field) {
		var jField = field.jField,
			jqLabel = $j('<label/>').attr('for', field.htmlId).html(jField.label + ':');
		if(jField.required == true) {
			jqLabel.addClass('required');
		}
		if(jField.hidden === true) {
			jqLabel.hide();
		}

		/*
		if(jField.submit === true) {
			jqLabel.css('background-color', 'gray');
		}*/
		
		return jqLabel;
	},
	/**
	 * Tworzy dodatkowy hidden (np.: dla p�l money, �eby przes�a� niesformatowan� liczb�).
	 * Ustawia <code>name</code> tworzonego hiddena na <code>cn</code> pola.
	 * @method
	 * @param {Field} field Obiekt typu Field, dla kt�rego zostanie utworzony hidden
	 * @return {Object} Obiekt jQuery utworzonego hiddena
	 */
	createAdditionalHidden: function(field) {
		var jField = field.jField,
			jqHidden = $j('<input/>').attr('type', 'hidden').attr('name', jField.cn);
		if(jField.disabled === true)
			jqHidden.attr('disabled', 'disabled');
		
		return jqHidden;
	},
	/**
	 * Tworzy element <code>fieldset</code> ustawiaj�c <code>id</code> na nazw� fieldset pola.
	 * @method
	 * @param {Field} field Obiekt typu Field, dla kt�rego zostanie utworzony fieldset
	 * @return {Object} Obiekt jQuery utworzonego fieldseta
	 */
	createFieldset: function(field) {
		var jField = field.jField,
			jqFieldset = $j('<fieldset/>').attr('id', jField.fieldset);
		
		return jqFieldset;
	},
	/**
	 * Tworzy element <code>fieldset</code> ustawiaj�c <code>id</code> na nazw� fieldset pola.
	 * @method
	 * @param {Field} field Obiekt typu Field, dla kt�rego zostanie utworzony fieldset
	 * @return {Object} Obiekt jQuery utworzonego fieldseta
	 */
	createMessage: function() {
		return $j('<span/>').addClass('message').css('display', 'none');
	},
	createProgress: function() {
		return $j('<span/>').addClass('progress');
	},
	createContainer: function(field) {
		var jField = field.jField,
			jqDiv = $j('<div/>').addClass('field').addClass(jField.cn).addClass(jField.type).addClass(jField.kind);
		if(jField.cssClass)
			jqDiv.addClass(jField.cssClass);
		if(jField.hidden === true)
			jqDiv.addClass('hidden');
			
		return jqDiv;
	},
	unpackValues: function(val) {
		var key;
		for(key in val) {
			if(key.search(/data/i) != -1) {
				return val[key];
			}
		}
		
	},
	createRegExpValidator: function(field) {
		return {RegExp: RegExp(field.jField.validatorRegExp), Message: DWR.MSG.VALIDATOR_INCORRECT};
	},
	createOrAssignFieldset: function(field) {
		if($j('#' + field.jField.fieldset).length < 1) {
			field.jqColumn.append(field.jqFieldset = BaseField.createFieldset(field));
		} else {
				field.jqFieldset = $j('#' + field.jField.fieldset);
		}
	}
}

BaseField.createFieldTemplate = function(field, params) {
	field.jqFieldContainer = this.createContainer(field);
	field.jqLabel = this.createLabel(field).appendTo(field.jqFieldContainer);
	field.jqFieldContainer.append(this.createMessage()).append(this.createProgress());
	
	if(params) {
		if(params.additionalHidden === true) {
			field.jqFieldContainer.append(this.createAdditionalHidden(field));
		}
		if(params.fieldset === true) {
			this.createFieldset(field).append(field.jqFieldContainer);
		}
	}
	
	return template;
}

/**
 * Wsp�lne metody dla p�l typu input (numeric, money, string, etc)
 */
var CommonInputField = (function() {
	var prepareForMultipleDictionaryTable = function() {
	    Field.prototype.prepareForMultipleDictionaryTable.call(this);
		this.addPlaceholder();
	}
	
	return function() {
		this.prepareForMultipleDictionaryTable = prepareForMultipleDictionaryTable;
	}
})();

var PureField = (function() {
	var create = function(afterField) {
		var jField = this.jField;

		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqField = BaseField.createAdditionalHidden(this);
		this.jqVisibleField = $j('<span class="pure"/>');
		this.jqFieldContainer.append(this.jqField, this.jqVisibleField);

		switch(jField.type) {
			case 'BOOLEAN':
				this.jqVisibleField.html("NIE");
			break;
		}
	}

	var getSelectedOption = function(enumVals) {
		var allOptions = enumVals.allOptions,
			selectedOptions = enumVals.selectedOptions;
		var iterOption = null, iterOptionValue = null;

		if(selectedOptions != null && selectedOptions.length > 0) {
			if(this.parentFieldsManager != null && this.parentFieldsManager.lookupMap != null) {
				var lookupMap = this.parentFieldsManager.lookupMap;
				var regExp = RegExp(/_\d+$/);
				var shortCn = this.jField.cn.replace(regExp, '');
				if(lookupMap[shortCn] != null) {
					lookupMap = lookupMap[shortCn];
					if(lookupMap[selectedOptions[0]] != null) {
						// console.warn('FOUND', shortCn, lookupMap[selectedOptions[0]]);
						var opt = {};
						opt[selectedOptions[0]] = lookupMap[selectedOptions[0]];

						return opt;
					}
				}
			}
		}

		for(var i = 0, len = allOptions.length; i < len; i ++) {
			iterOption = allOptions[i];
			for(iterOptionValue in iterOption) {
				if($j.inArray(iterOptionValue + '', selectedOptions) != -1) {
					return iterOption;
				}
			}
		}
	}

	var setValue = function(val) {
		var strValue = '';
		var jField = this.jField;

		switch(jField.type) {
			case 'ENUM':
				if(val.allOptions != null) {
					//console.warn(val.allOptions);
					this.parentFieldsManager.updateCachedValues(val, jField.cn);
					var selectedOption = this.getSelectedOption(val);
					if(selectedOption == null) {
						//console.info('Error', val);
						strValue = 'ERROR';
					}
					for(var selectedOptionValue in selectedOption) {
						strValue = selectedOption[selectedOptionValue];
						this.enumValue = selectedOptionValue;
						this.jqField.val(selectedOptionValue);
					}
				} else {
					throw 'AllOptions is null for field ' + jField.cn + ' with val = ' + (val != null ? val : '[NULL]');
				}				
			break;
			case 'BOOLEAN':
				if(val == true || val === '1') {
					strValue = "TAK";
					this.booleanValue = true;
				} else {
					strValue = "NIE";
					this.booleanValue = false;
				}
				this.jqField.val(val);
			break;
			default:
				strValue = val;
				this.jqField.val(strValue);
		}

		
		this.jqVisibleField.html(strValue);
	}
	
	var clearValue = function() {
		var jField = this.jField;
		
		switch(jField.type) {
			case 'ENUM':
				this.setValue({allOptions: [{'': '-- wybierz--'}], selectedOptions: ['']});
			break;
			case 'BOOLEAN':
				this.setValue(false);
			break;
			default:
				this.setValue('');
		}
	}

	var getValue = function(params) {
		var jField = this.jField, ret = null;

		switch(jField.type) {
			case 'INTEGER':
				ret = {type: 'INTEGER', integerData: this.jqField.val()};
			break;
			case 'BOOLEAN':
				ret = {type: 'BOOLEAN', booleanData: this.booleanValue};
			break;
			case 'MONEY':
				ret = {type: 'MONEY', moneyData: this.jqField.val()};
			break;
			case 'ENUM':
				if(params && params.allOptions === false) {
					ret = {type: 'ENUM', enumValuesData: {allOptions: [], selectedOptions: [this.jqField.val()]}};
				}
			break;
		}

		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}

		// console.info(jField.cn, ret);
		return ret;
	}

	var getSimpleValue = function() {
		var jField = this.jField;

		switch(jField.type) {
			case 'ENUM':
				return this.enumValue;
			break;
			default:
				return this.getValue({packed: false});
		}
	}

	var showError = function() {

	}

	var hideError = function() {
		
	}

	var require = function() {
		return true;
	}

	return function() {
		this.create = create;
		this.setValue = setValue;
		this.clearValue = clearValue;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
		this.getSelectedOption = getSelectedOption;
		this.showError = showError;
		this.hideError = hideError;
		this.require = require;
	}
})();

var StringField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId).attr('name', jField.cn).addClass('txt');
		
		if(jField.readonly === true)
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		if(jField.disabled === true)
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		if(jField.required)
			this.jqVisibleField.addClass('required');
        if(jField.autofocus === true)
            this.jqVisibleField.attr('autofocus','autofocus');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(this.isSearchable === true)
			this.jqVisibleField.addClass('searchable');
		if(jField.validatorRegExp)
			this.validator = BaseField.createRegExpValidator(this);

		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);
	}
	
	var setValue = function(val) {
		this.jqField.val(val);
		this.jqVisibleField[0].prevval = val;
		this.markEmpty();
	}
	
	var getValue = function(params) {
		var stringData = this.jqField.val();
		if(params && params.flags)
			stringData = Utils.formatString(stringData, params.flags);
		
		var ret = {type: 'STRING', stringData: stringData};
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function(flags) {
		var ret = this.jqField.val();
		if(flags)
			ret = Utils.formatString(ret, flags);
		return ret;
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
	}
})();

var TextareaField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<textarea/>').attr('id', this.htmlId).attr('name', jField.cn).addClass('txt');
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		}
		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required)
			this.jqVisibleField.addClass('required');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(jField.validatorRegExp)
			this.validator = BaseField.createRegExpValidator(this);
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	var setValue = function(val) {
		this.jqField.val(val);
		this.jqField.html(val);
		this.jqVisibleField[0].prevval = val;
		this.markEmpty();
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.setValue('');
		this.markEmpty();
	}
	
	var getValue = function(params) {
		var ret = {type: 'TEXTAREA', textareaData: this.jqField.val().trim()};
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function() {
		return this.jqField.val().trim();
	}
	
	var prepareForMultipleDictionaryTable = function(opts) {
	    Field.prototype.prepareForMultipleDictionaryTable.call(this);
		var shortCn = this.jField.cn,
			cache = opts.helper.fieldPropertiesCache;
		
		if(opts != null && opts.helper != null) {
			
			shortCn = shortCn.replace(/_\d+$/i, '');
			if(cache[shortCn] == null) {
				cache[shortCn] = {};
			}
			if(cache[shortCn].minimalDimensionsWithoutLabel == null) {
				cache[shortCn].minimalDimensionsWithoutLabel = this.getMinimalDimensions({label: false});
			} 
			this.jqFieldContainer.width(cache[shortCn].minimalDimensionsWithoutLabel.width);
		} else {
			this.jqFieldContainer.width(this.getMinimalDimensions({label: false}).width);
		}
		this.addPlaceholder();
		this.jqVisibleField.attr('wrap', 'off');
		this.jqVisibleField.bind('focus.toggleSize', function() {
			$j(this).attr('wrap', 'on').css('z-index', '9999');
			
		}).bind('blur.toggleSize', function() {
			$j(this).attr('wrap', 'off').css('z-index', '1');
		});
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.clearValue = clearValue;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
		this.prepareForMultipleDictionaryTable = prepareForMultipleDictionaryTable;
	}
})();

var HtmlField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<div/>').attr('id', this.htmlId).addClass('html');
		
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId);		
	}
	
	var setValue = function(val) {
		this.jqField.html(val);
		this.jqVisibleField[0].prevval = val;
		this.markEmpty();
	}
	
	var getValue = function(params) {
		var ret = {type: 'HTML', htmlData: this.jqField.html()};
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function() {
		return this.jqField.html();
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.jqField.html('');
		this.markEmpty();
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
		this.clearValue = clearValue;
	}
})();

var HtmlEditorField = (function() {
	var loadingCke = null;
	var defaultCkeConfig = {
		toolbarGroups: [
		    {name: 'undo'},
		    {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
		    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
		    {name: 'styles'},
		    {name: 'colors'}
        ],
        removePlugins: 'elementspath',
        extraPlugins: 'onchange'
	};
	
	var create = function(afterField) {
		var jField = this.jField;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = BaseField.createAdditionalHidden(this); 
		this.jqVisibleField = $j('<div/>').attr('id', this.htmlId).addClass('html-editor');
		
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		
		this.jqFieldContainer.append(this.jqLabel, this.jqField, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
		this.createDynamic();
	}
	
	var showField = function(field, visible) {
		if(visible) {
			field.jqFieldContainer.removeClass('force_hidden');
		} else {
			field.jqFieldContainer.addClass('force_hidden');
		}
	}
	
	var createDynamic = function() {
		function setupCke() {
			var cfg = $j.extend(true, {}, defaultCkeConfig);
			
			if(this.jField.readonly === true) {
				//cfg.readOnly = true;
				cfg.toolbarGroups = [];
			}
			
			this.ckeditor = CKEDITOR.appendTo(this.jqVisibleField[0], cfg);
		}
		
		loadingCke.done($j.proxy(setupCke, this));
		if(this.jqField.val() != '')
		    this.setValue(this.jqField.val());
	}

	var destroyDynamic = function() {
	    this.ckeditor.destroy();
	}

	var loadCke = function(loadingCke, callback) {
//	    console.warn('loadCke', this);
//        console.info('loadCke...', '[pending]')
        loadingCke.done(callback);
	}

	var initCke = function(callback) {
//	    console.warn('initCke', this);
	    if(this.ckeditor.instanceReady === true) {
//	        console.info('initCke...', '[done]');
	        callback.call(this);
	    } else {
//	        console.info('initCke...', '[pending]');
	        this.ckeditor.on('instanceReady', callback, this);
	    }
	}
	
	var isEmptyVal = function(htmlVal) {
		var jqTmp = $j('<span />');
		jqTmp.html(htmlVal);
		
		return jqTmp.text().trim() == '';
	}
	
	var setValue = function(val) {
		this.jqField.val(val);

		loadCke.call(this, loadingCke, $j.proxy(function() {
		    initCke.call(this, function(evt) {
//                console.warn('initCke...', '[done]');
                try {
                    this.ckeditor.setData(val);

                    var hideEmpty = this.jqFieldContainer.hasClass('dwr-hide-empty');
                    if(hideEmpty && isEmptyVal(val)) {
//                        console.info('hide field', this.jField.cn);
                        showField(this, false);
                    } else {
//                        console.info('show field', this.jField.cn);
                        showField(this, true);
                    }
                    this.sendResizeNotification();
                } catch(e) { console.error(e, this.jField.cn);}
		    });
		}, this));
	}
	
	var getValue = function(params) {
		var ret = {type: 'HTML_EDITOR', htmlEditorData: {html: ''}};
		
		try {
			ret.htmlEditorData.html = this.ckeditor.getSnapshot();
			if(ret.htmlEditorData.html == true) {
			    throw 'Snapshot error';
			}
		} catch(e) {
//		    console.error(e);
			ret.htmlEditorData.html = this.jqField.val();
		} finally {
			if(isEmptyVal(ret.htmlEditorData.html) || ret.htmlEditorData.html === '<p>true</p>' || ret.htmlEditorData.html == '<p>true<br></p>') {
				ret.htmlEditorData.html = '';
			}
		}
		
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function() {
		return this.jqField.val();
	}
	
	var assignValueChangeEvent = function() {
		var _this = this;

		loadingCke.done($j.proxy(function() {
			this.chg = false;
			var onChange = $j.proxy(function(evt) {
				this.jqField.val(this.ckeditor.getData());
				this.chg = true;
			}, this);
			
			this.ckeditor.on('change', onChange);
			
			var hideEmpty = this.jqFieldContainer.hasClass('dwr-hide-empty');
			if(hideEmpty) {
				showField(this, false);
			}
		}, this));
	}
	
	var getMinimalDimensions = function(options) {
		var dim = {width: 100, height: 100};
		
		try {
			dim = Field.prototype.getMinimalDimensions.call(this, {label: false});
		} catch(e) {
			var hideEmpty = this.jqFieldContainer.hasClass('dwr-hide-empty');
			if(hideEmpty) {
				dim.width = 0;
				dim.height = 0;
			}
		}
		
		return dim;
	}

	var isDynamic = function() {
	    return true;
	}
	
	return function() {
		if(loadingCke == null) {
			loadingCke = $j.getScript(DWR.CKEDITOR_URL);
		}
		
		this.create = create;
		this.setValue = setValue;
		this.getSimpleValue = getSimpleValue;
		this.getValue = getValue;
		this.assignValueChangeEvent = assignValueChangeEvent;
		this.createDynamic = createDynamic;
		this.destroyDynamic = destroyDynamic;
		this.getMinimalDimensions = getMinimalDimensions;
		this.isDynamic = isDynamic;
	}
})();

var EnumField = (function() {
	var create = function(afterField) {	
		var jField = this.jField;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		
		var jqOption = $j('<option/>').val('').html('loading...');
		this.jqVisibleField = $j('<select/>').attr('id', this.htmlId).addClass('sel').append(jqOption);
		
		if(jField.onchange != null){
			this.jqVisibleField.bind('change', function(){
				eval(jField.onchange);
			});
		}
		// disabled i readonly dla selecta oznacza to samo
		if(jField.readonly === true || jField.disabled === true) {
			this.jqField = BaseField.createAdditionalHidden(this);
			this.jqVisibleField.attr('disabled', 'disabled').addClass('readonly');
			this.jqFieldContainer.append(this.jqField);
		} else {
			this.jqField = this.jqVisibleField.attr('name', jField.cn);
		}
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		if(jField.required)
			this.jqVisibleField.addClass('required');
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqVisibleField.attr('id', this.htmlId);	
		if(this.jField.readonly === true || this.jField.disabled === true) {
			this.jqField.attr('name', newCn);
		} else {
			this.jqVisibleField.attr('name', newCn);
		}
		
		// reinitializujemy Chosen
		if(this.jField.autocomplete === true) {
//			console.info(newCn, 'removing chosen');
			this.jqFieldContainer.children('.chzn-container').remove();
			this.jqVisibleField.removeClass('chzn-done').css('display', 'block');
			this.setValue(this.getSimpleValue(), {allOptions: false});
		}
	}

	var getPlaceholder = function(allOptions, clearAfterGet, createdFakeOption) {
		var default_ = {
			'': DWR.MSG.DEFAULT_SELECT_LABEL
		};

		if(allOptions == null || allOptions.length == 0) {
			if(createdFakeOption != null) {
				createdFakeOption[''] = default_[''];
			}
			return default_;
		}

		var i, len, optionValue, optionLabel, iterOption;
		for(i = 0, len = allOptions.length; i < len; i ++) {
			iterOption = allOptions[i];
			for(optionValue in iterOption) {
				if(optionValue == '' && iterOption[optionValue] != '') {
					// znale�li�my -- wybierz --
					var ret = {};
					ret[optionValue] = iterOption[optionValue] + '';

					if(clearAfterGet === true) {
						iterOption[optionValue] = '';
					}

					return ret;
				}
			}
		}

		if(createdFakeOption != null) {
			createdFakeOption[''] = default_[''];
		}
		return default_;
	}
	
	var setValue = function(val, params) {
		var i, key, allOptions, allOptionsLen, selOption,
			str, value, realWidth, minWidth;
		var jField = this.jField;
		var hidden = jField.readonly === true || jField.disabled === true;

		if(val != null) {
			if(val.allOptions != null) {
				this.allOptions = val.allOptions;
			}
			if(val.selectedOptions != null) {
				this.selectedOptions = val.selectedOptions;
			}
		}
		
		if (params && params.allOptions === false) {
			this.jqVisibleField.val(val).trigger('liszt:updated');
			if(hidden)
				this.jqField.val(val);
		}
		else {
			allOptions = val.allOptions;
			selOption = val.selectedOptions && val.selectedOptions[0] ? val.selectedOptions[0] : null;
			
			if(jField.autocomplete === true) {
				try {
					// ustawiamy a potem usuwamy placeholder z listy podpowiedzi
					this.fakePlaceholder = {};
					this.jqVisibleField.attr('data-placeholder', getPlaceholder(allOptions, true, this.fakePlaceholder)['']);

					/*for(key in allOptions[0]) {
						this.jqVisibleField.attr('data-placeholder', allOptions[0][key] + '');
						allOptions[0][key] = '';
					}*/
				} catch(e) {
					// debugger;
					console.error(e);
				}
			}


			if(params != null && params.addToLookup === true && this.parentFieldsManager != null) {
				if(this.parentFieldsManager.lookupMap == null) {
					this.parentFieldsManager.lookupMap = {};
				}
				if(this.parentFieldsManager.lookupMap[jField.cn] == null) {
					this.parentFieldsManager.lookupMap[jField.cn] = {};
				}
			}
			
			str = '';
			for (i = 0, allOptionsLen = allOptions.length; i < allOptionsLen; i ++) {
				/* val.allOptions[i] = { "warto�� opcji" : "nazwa opcji" } */
				for(key in allOptions[i]) {
					value = allOptions[i][key];  
				}

				try {
					if(params != null && params.addToLookup === true && this.parentFieldsManager != null) {
						this.parentFieldsManager.lookupMap[jField.cn][key + ''] = value;
					}
				} catch(e) {
					console.error(e);
				}
				
				str += '<option value="' + key + '"';
				str += '>' + value + '</option>';
			}
			this.jqVisibleField.html(str);

			if(selOption) {
				this.jqVisibleField.val(selOption);
				this.jqField.val(selOption);
			}
		}
		this.jqVisibleField.addClass(' ');
		this.markEmpty();

		if(this.jqVisibleField.hasClass('chzn-done')) {
			// console.info(this.jField.cn, 'updating chosen', this.jqVisibleField);
			this.jqVisibleField.trigger('liszt:updated');
		} else {
//				console.info(this.jField.cn, 'creating chosen');
			(function(field) {
				if(field.jField.lazyEnumLoad === true && field.jField.disabled !== true) {
					var getPromise = function() {
						if(field.lazyLoading == null) {
							// pierwsze klikni�cie - pobranie danych
							field.lazyLoading = $j.Deferred();
							field.jqFieldContainer.addClass('ac_loader');
							var dicName = field.parentFieldsManager.jDictionaryField.dictionary.name;
							// console.info('Sending', dicName, field.jField.cn.replace(/_\d+$/i, ''));
							Dictionary.getFieldValue(dicName, field.jField.cn.replace(/_\d+$/i, ''), {
								async: field.jField.autocomplete, // dla zwyk�ych enum�w synchronicznie
								callback: function(data) {
									field.setValue(data, {addToLookup: true});
									field.jqFieldContainer.removeClass('ac_loader');
									field.lazyLoading.resolve();										
								}
							});
							return field.lazyLoading.promise();			
						} else if(field.lazyLoading.state() == 'pending') {
							// w trakcie odczytywania
						} else if(field.lazyLoading.state() == 'resolved') {
							// ju� wczytane
							if(field.jField.coords) {
								realWidth = field.jqFieldContainer.width();
								minWidth = field.getMinimalDimensions().width;
								field.jqFieldContainer.css('min-width', minWidth + 'px');
							}								
						}
					}
				}

				if(field.jField.autocomplete === true) {
					field.jqVisibleField.chosen({
						allow_single_deselect: true,
						/*search_contains: true,*/
						search: field.jField.autocompleteRegExp || null,
						clip: $j('.wrapper'),
						max_width: window.innerWidth - 10,
						default_label: DWR.MSG.DEFAULT_SELECT_LABEL,
						get_container_mousedown_promise: field.jField.lazyEnumLoad === true && field.jField.disabled !== true ? getPromise : null,
						opt_level: 2
					});
				} else {
					field.jqVisibleField.one('lazyload', function() {
						try {
							var promise = getPromise();
							if(promise != null) {
								promise.done(function() {
									//console.info('done');
								});
							}
						} catch(e) {
							console.error(e);
						}
					})
				}
			})(this);
		}
		//this.jqVisibleField.css('display', 'block');
		// po dodaniu opcji do selekta, jego szeroko�� mog�a si� zwi�kszy� (wa�ne dla siatki)
		if(jField.coords) {
			realWidth = this.jqFieldContainer.width();
			minWidth = this.getMinimalDimensions().width;
			this.jqFieldContainer.css('min-width', minWidth + 'px');
		}
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.setValue('', {allOptions: false});
		this.markEmpty();
	}
	
	var getSimpleValue = function(params) {
		if(params && params.selectedOptionName === true) {
			return this.jqVisibleField.children('option:selected').html();
		} else {
			return this.jqVisibleField.val();
		}
	}

	var addPlaceholder = function(allOptions, field) {
		var default_ = { '': DWR.MSG.DEFAULT_SELECT_LABEL};

		var i, len, iterOption, 
			present = false;
		for(i = 0, len = allOptions.length; i < len; i ++) {
			iterOption = allOptions[i];
			if(iterOption[''] != null) {
				present = true;
				iterOption[''] = field.jqVisibleField.attr('data-placeholder');
				return;
			}
		}

		if(!present) {
			allOptions.unshift(default_);
		}
	}
	
	var getValue = function(params) {
		var curOption, ret, retObj;
		var allOptions = [], selectedOptions = null;
		
		if(!params || (params && params.allOptions !== false)) {
			if(this.allOptions != null && this.allOptions.length > 0) {
				allOptions = this.allOptions;
			} else {
			this.jqVisibleField.children('option').each(function() {
				curOption = {};
				curOption[$j(this).val()] = $j(this).text();
				allOptions.push(curOption);
			});
			}
			if(this.jField.autocomplete === true) {
				try {
					//allOptions[0][''] = this.jqField.attr('data-placeholder');
					if(params == null || (params != null && params.placeholder === true)) {
						addPlaceholder(allOptions, this);
					}
				} catch(e) {
					console.error(e);
				}
			}
		}
		
        selectedOptions = [this.jqVisibleField.val()];

		ret = {type: 'ENUM', enumValuesData: {allOptions: allOptions, selectedOptions: selectedOptions}};
		
		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			retObj = {};
			retObj.allOptions = ret.enumValuesData.allOptions;
			retObj.selectedOptions = ret.enumValuesData.selectedOptions;
			ret = retObj;
		}
		
		return ret;
	}
	
	var refresh = function(diff) {
		var jField = this.jField;
		// readonly == disabled
		if(jField.readonly === true || jField.disabled === true) {
			// if auto and is disabled
			if (jField.autocomplete == true)
			{
					this.jqField.show();
					this.jqVisibleField.hide();
			}
			else
			{
				jField.readonly = jField.disabled = true;
			}
		} else if(jField.readonly === false && jField.disabled === false) {
			jField.readonly = jField.disabled = false;
		}
		
		Field.prototype.refresh.call(this, diff);
	}
	
	var markEmpty = function() {
		Field.prototype.markEmpty.call(this);
		var opts = this.getValue().enumValuesData,
			first = opts.selectedOptions[0];
		var empty = false;

		// je�li opcja -- wybierz -- jest oszukana, tj. stworzona przez js
		if(!$j.isEmptyObject(this.fakePlaceholder)) {
			// console.info(this.jField.cn, this.fakePlaceholder);
			if(opts.allOptions != null && opts.allOptions.length == 1) {
				empty = true;
			}
		} else {
			if(opts.allOptions.length == 0 && (opts.selectedOptions.length == 0 || first == null || first == '')) {
				empty = true;
			} else {
				empty = false;
			}
		}

		if(empty) {
			this.jqField.addClass('empty-no-options');
			this.jqVisibleField.addClass('empty-no-options');
			this.jqLabel.addClass('empty');
		} else {
			this.jqField.removeClass('empty-no-options');
			this.jqVisibleField.removeClass('empty-no-options');
			this.jqLabel.removeClass('empty');
		}
	}
	
	var assignValueChangeEvent = function() {
		Field.prototype.assignValueChangeEvent.call(this);

		this.jqVisibleField.bind('valuechange.updateCache', $j.proxy(function() {
			this.selectedOptions = [this.jqVisibleField.val()];
		}, this));
	}
	
	var getMinimalDimensions = function(options) {
		var dim = Field.prototype.getMinimalDimensions.call(this, options);
//		console.info(this.jField.cn, dim);
		
		return dim;
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.clearValue = clearValue;
		this.getSimpleValue = getSimpleValue;
		this.getValue = getValue;
		this.refresh = refresh;
		this.markEmpty = markEmpty;
		this.assignValueChangeEvent = assignValueChangeEvent;
		this.getMinimalDimensions = getMinimalDimensions;
		
		return this;
	}
})();

var EnumMultipleField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		
		var jqOption = $j('<option/>').val('dummy').html('loading...');
		this.jqVisibleField = $j('<select/>').attr('id', this.htmlId).attr('multiple', true)
			.attr('size', 4).addClass('sel').append(jqOption);
		
		// disabled i readonly dla selecta oznacza to samo
		if(jField.readonly === true || jField.disabled === true) {
			this.jqField = BaseField.createAdditionalHidden(this);
			this.jqVisibleField.attr('disabled', 'disabled').addClass('readonly');
			this.jqFieldContainer.append(this.jqField);
		} else {
			this.jqField = this.jqVisibleField.attr('name', jField.cn);
		}
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		if(jField.required)
			this.jqVisibleField.addClass('required');
		this.jqLabel.height(this.jqField.height());
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId);	
		if(this.jField.readonly === true || this.jField.disabled === true) {
			this.jqField.attr('name', newCn);
		} else {
			this.jqVisibleField.attr('name', newCn);
		}
	}
	
	var setValue = function(val, params) {
		var i, key, allOptions, allOptionsLen, selOptions,
			str, value;
		var jField = this.jField;
		var hidden = jField.readonly === true || jField.disabled === true;
		
		if (params && params.allOptions === false) {
			this.jqVisibleField.val(val);
			if(hidden)
				this.jqField.val(val);
		} else {
			allOptions = val.allOptions;
			selOptions = val.selectedOptions;
			
			str = '';
			for(i = 0, allOptionsLen = allOptions.length; i < allOptionsLen; i ++) {
				/* val.allOptions[i] = { "warto�� opcji" : "nazwa opcji" } */
				for(key in allOptions[i]) {
					value = allOptions[i][key];  
				}
				str += '<option value="' + key + '"';
				str += '>' + value + '</option>';
			}
			this.jqVisibleField.html(str);
			if(selOptions && selOptions.length > 0) {
				this.jqVisibleField.find('[value=' + selOptions.join('],[value=') + ']').attr('selected', 'selected');
			} else {
				this.jqVisibleField.val('');
			}
		}
		this.jqVisibleField.addClass(' ');
		
		if(jField.autocomplete == true) {
			// TODO - jeszcze nie obs�ugiwane
		}
		
		/* Zapisujemy szeroko�� pola + etykiety */
		/*var oldWidth = this.jqColumn.data('expWidth');
		var newWidth = this.jqField.width() + this.jqLabel.width() + 10;
		if(oldWidth == null)
			this.jqColumn.data('expWidth', newWidth);
		else if(newWidth > oldWidth)
			this.jqColumn.data('expWidth', newWidth);*/
		
		this.markEmpty();
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.jqVisibleField.val('');
		this.markEmpty();
	}
	
	var getValue = function(params) {
		var allOptions = [], selectedOptions = [];
		
		if (params && params.allOptions === false) {
			this.jqField.find('option:selected').each(function() {
				selectedOptions.push($j(this).val() ? $j(this).val() : '');
			});
		}
		else {
			this.jqField.find('option').each(function() {
				var curOption = new Object();
				curOption[$j(this).val()] = $j(this).text();
				allOptions.push(curOption);
				if ($j(this).attr('selected')) 
					selectedOptions.push($j(this).val() ? $j(this).val() : '');
			});
		}
		ret = {type: 'ENUM', enumValuesData: {allOptions: allOptions, selectedOptions: selectedOptions}};
		
		if(params && params.packed === false) {
			retObj = {};
			retObj.allOptions = ret.enumValuesData.allOptions;
			retObj.selectedOptions = ret.enumValuesData.selectedOptions;
			ret = retObj;
		}
		
		return ret;
	}
	
	var getSimpleValue = function(params) {
        if(params && params.selectedOptionName === true) {
            return this.jqVisibleField.children('option:selected').html();
        } else {
            try {
                return this.getValue({allOptions: false, packed: false}).selectedOptions.join(',');
            } catch(e) {
            	console.error(e);
                return '';
            }
        }
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.clearValue = clearValue;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
	}
})();

var DateField = (function() {
	var create = function(afterField) {
		var timeFormat;
		var jField = this.jField;
		var params = jField.params,
			dateFormat = DWR.DEFAULT_DATE_FORMAT;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId).attr('name', jField.cn).addClass('txt date').attr('autocomplete','off');
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		}
		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required) {
			this.jqVisibleField.addClass('required');
		}
		if(params) {
			if(params.seconds === true) {
				dateFormat += ' ' + DWR.DEFAULT_TIME_FORMAT;
				timeFormat = DWR.DEFAULT_TIME_FORMAT;
			} else if(params.minutes === true) {
				dateFormat += ' ' + DWR.DEFAULT_HOUR_MINUTE_FORMAT;
				timeFormat = DWR.DEFAULT_HOUR_MINUTE_FORMAT;
			} else if(params.hours === true) {
				dateFormat += ' ' + DWR_DEFAULT_HOUR_FORMAT;
				timeFormat = DWR.DEFAULT_HOUR_FORMAT;
			}
		}
		this.dateFormat = dateFormat;
		this.timeFormat = timeFormat;
		
		if(this.timeFormat) {
			this.jqVisibleField.addClass('time');
			if(params) {
				if(params.seconds === true) {
					this.jqVisibleField.addClass('time-seconds');
				} else if(params.minutes === true) {
					this.jqVisibleField.addClass('time-minutes');
				} else if(params.hours === true) {
					this.jqVisibleField.addClass('time-hours');
				}
			}
			this.jqFieldContainer.addClass('TIME');
		}
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		if(!jField.readonly && !jField.disabled) {
			this.setCalendar();
		}
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	/**
	 * Przypisuje kalendarz do pola
	 * @method
	 * @private
	 */
	var setCalendar = function() {
		var singletonCal = this.options ? this.options.singletonCal === true : false;
		var jqCalImage;
		var jqField = this.jqVisibleField,
			_this = this,
			jField = this.jField,
			showHour = false,
			showMinute = false,
			showSecond = false;
		var params = jField.params;

                   		if(singletonCal === true) {
                   			this.jqVisibleField.addClass('singleton-cal');
                   			this.jqFieldContainer.addClass('singleton-cal-container calendar-image')
                   			this.jqVisibleField.data('timeFormat', this.timeFormat);
                   			if(this.timeFormat) {
                   				this.jqVisibleField.bind('change.dateChanged', function() {
                   					if(_this.validate()) {
                   						_this.hideError();
                   						try {
                   							_this.value = Date.prototype.fromString($j(this).val(), _this.dateFormat);
                   						} catch(e) {
                   							console.error(e);
                   							_this.value = null;
                   						}
                   						if($j(this).data('timepickerOpened') !== true) {
                   							$j(this).trigger('dateSelect').trigger('valuechange');
                   						}
                   					} else {
                   						_this.showError();
                   					}
                   				});
                   			} else {
                   				this.jqVisibleField.bind('change.dateChanged', function() {
                   					if(_this.validate()) {
                   						_this.hideError();
                   						try {
                   							_this.value = Date.prototype.fromString($j(this).val(), _this.dateFormat);
                   						} catch(e) {
                   							console.error(e);
                   							_this.value = null;
                   						}
                   						$j(this).trigger('dateSelect.submit');
                   					} else {
                   						_this.showError();
                   					}
                   				});
                   			}
                   			return;
		}
		
		if(this.timeFormat) {
			if(params.seconds === true) {
				showSecond = showMinute = showHour = true;
			} else if(params.minutes === true) {
				showMinute = showHour = true;
			} else {
				showHour = true;
			}
			jqField.datetimepicker({showOn: 'button', buttonImage: CALENDAR_IMAGE, 
				buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
				changeMonth: true, changeYear: true, showButtonPanel: true, timeFormat: this.timeFormat.replace(/M/g, 'm'),
				showHour: showHour, showMinute: showMinute, showSecond: showSecond,
				beforeShow: function(input, inst) {
					_this.timepickerOpened = true; 
				},
				onClose: function(dateText, inst) {
					_this.timepickerOpened = false;
					inst.input.trigger('change.dateChanged');
				}/*,
				onSelect: function dateSelectHandler() {
					_this.value = $j(this).datepicker('getDate');
					$j(this).trigger('dateSelect');
					$j(this).trigger('dateSelect.submit');
				}*/
				}).bind('change.dateChanged', function() {
					if(_this.validate()) {
						_this.hideError();
						try {
							_this.value = Date.prototype.fromString($j(this).val(), _this.dateFormat);
						} catch(e) {
							console.error(e);
							_this.value = null;
						}
						if(!_this.timepickerOpened) {
							$j(this).trigger('dateSelect').trigger('valuechange');
						}
					} else {
						_this.showError();
					}
				});
		} else {
			jqField.datepicker({showOn: 'button', buttonImage: CALENDAR_IMAGE, 
				buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
				changeMonth: true, changeYear: true, showButtonPanel:false,
				onSelect: function dateSelectHandler() {
					_this.value = $j(this).datepicker('getDate');
					//$j(this).trigger('dateSelect.submit');
					$j(this).trigger('change.dateChanged');
				}
				}).bind('change.dateChanged', function() {
					if(_this.validate()) {
						_this.hideError();
						try {
							_this.value = Date.prototype.fromString($j(this).val(), _this.dateFormat);
						} catch(e) {
							_this.value = null;
						}
						$j(this).trigger('dateSelect.submit');
					} else {
						_this.showError();
					}
				});
		}
			
		jqCalImage = jqField.next('img.ui-datepicker-trigger');
		jqField.before(jqCalImage);
		jqField.width(jqField.width() - 25);
		this.jqCalImage = jqCalImage;
	}
	
	var validate = function() {
		Field.prototype.validate.call(this);
		if(this.jqField.val().trim() != '') {
			try {
				Date.prototype.fromString(this.jqField.val(), this.dateFormat);
				this.errorMsg = null;
				this.valid = true;
			} catch(e) {
				switch(e) {
					case 'Invalid day':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_DAY;
					break;
					case 'Invalid month':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_MONTH;
					break;
					case 'Invalid year':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_YEAR;
					break;
					case 'Invalid hour':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_HOUR;
					break;
					case 'Invalid minute':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_MINUTE;
					break;
					case 'Invalid second':
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_SECOND;
					break;
					default:
						this.errorMsg = DWR.MSG.VALIDATOR_INCORRECT_DATE;
				}
				this.valid = false;
			}
		}
		
		return this.valid;
	}
	
	var remove = function() {
		try {
			this.jqVisibleField.datepicker('destroy');
		} catch(e) {}
		this.jqFieldContainer.remove();
	}
	
	var setValue = function(val) {
		var jField = this.jField;
//		console.info(jField.cn, val.toString());
		
		// TODO - trzeba to wyrzuci� gdy ju� b�d� normalne s�owniki wielowarto�ciowe
		if(val == 'not-required') {
//			console.info(jField.cn, 'is NOT required now');			
			this.setRequiredState(false);
			return;
		} 
		
		if(val == null) {
			this.jqVisibleField.val('');
		} else {
			switch(Object.prototype.toString.call(val)) {
			case '[object String]':
				this.jqVisibleField.val(val);
				break;
			case '[object Date]':
				this.jqVisibleField.val(val.format(this.dateFormat.replace(/h/g, 'H')));
				break;
			default:
				throw 'Internal Error - unknown type ' + Object.prototype.toString.call(val) + ' for Date field';
			}
		}
		
		this.value = val;
		if(this.formatter) {
			try {
				this.jqVisibleField.val(this.formatter(val + '', true).formattedValue);
			} catch(e) {
				this.jqVisibleField.val(val);
			}
		}
		
		this.markEmpty();
	}
	
	var getSimpleValue = function(params) {
		if(params && params.formatted === true) {
			return this.getValue({packed: false}).format(this.dateFormat.replace(/h/g, 'H'));
		} else {
			return this.jqVisibleField.val();
		}
	}
	
	var getValue = function(params) {
		var ret;
		var date = this.value ? this.value : null;

		//przejscie na DwrCustomDateConverter
		//date = Date.prototype.fromString(this.jqField.val(), this.dateFormat);
		//date = DWR_DATE_FORMAT_ID[this.dateFormat]+'#'+this.jqField.val();
		date = this.jqField.val();

		ret = {type: 'DATE', dateData: date};
		
		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.setValue();
		this.markEmpty();
	}
	
	var getMinimalDimensions = function(options) {
		var dims = Field.prototype.getMinimalDimensions.call(this);
		dims.width += 28;
		
		return dims;
	}
	
	var prepareForMultipleDictionaryTable = function() {
	    Field.prototype.prepareForMultipleDictionaryTable.call(this);
		this.jqField.css('width', 'auto');
		this.addPlaceholder();
	}
	
	var assignValueChangeEvent = function() {
		var _this = this;
		this.jqVisibleField.bind('dateSelect.submit', function() {
			$j(this).trigger('valuechange');
		});
		this.jqField.bind('valuechange', function() {
			var jqThis = $j(this);
			_this.changedByUser = _this.getSimpleValue() !== this.initialvalue;
		});
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setCalendar = setCalendar;
		this.remove = remove;
		this.setValue = setValue;
		this.getSimpleValue = getSimpleValue;
		this.getValue = getValue;
		this.clearValue = clearValue;
		this.validate = validate;
		this.getMinimalDimensions = getMinimalDimensions;
		this.prepareForMultipleDictionaryTable = prepareForMultipleDictionaryTable;
		this.assignValueChangeEvent = assignValueChangeEvent;
	}
})();

var TimestampField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId)
			.attr('name', jField.cn).addClass('txt time');
		
		if(jField.readonly === true)
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		if(jField.disabled === true)
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		if(jField.required)
			this.jqVisibleField.addClass('required');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(this.isSearchable === true)
			this.jqVisibleField.addClass('searchable');
		if(jField.validatorRegExp)
			this.validator = BaseField.createRegExpValidator(this);
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	var getValue = function(params) {
		var ret = {type: 'TIMESTAMP', dateData: new Date()};
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var setValue = function(val) {
		this.jqField.setValue(val);
		this.markEmpty();
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.getValue = getValue;
		this.setValue = setValue;
	}
})();

var MoneyField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		
		this.validator = Validator.MONEY;
		this.keyFilter = KeyFilter.MONEY;
		
		if(jField.format) {
			this.validator = null;
			this.formatter = function(val, extFormat) {
				var strFormat = jField.format;
				var ret = {};
				
				ret.value = val.replace(',', '.').replace(' ', '');
				if(extFormat === true) {
					ret.formattedValue = Number(ret.value).format(strFormat);
				} else {
					ret.formattedValue = val;
				}
				return ret;
			}
		} else {
			this.formatter = Formatter.MONEY;
		}
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = BaseField.createAdditionalHidden(this);
		
		this.jqFormattedField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId).addClass('txt money');
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		}
		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required)
			this.jqVisibleField.addClass('required');
		
		this.jqFieldContainer.append(this.jqLabel, this.jqField, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('name', newCn);
		this.jqVisibleField.attr('id', this.htmlId);
	}
	
	var getSimpleValue = function(params) {
		if(params && params.formatted === true) {
			return this.formatter(this.jqField.val(), true).formattedValue;
		} else {
			return this.jqField.val();
		}
	}
	
	var getValue = function(params) {
		var ret = {type: 'MONEY', moneyData: this.jqField.val() };
		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		
		return ret;
	}
	
	var setValue = function(val) {
		this.jqField.val(val);
		this.jqVisibleField[0].prevval = val;
	
		if(this.formatter) {
			try {
				this.jqVisibleField.val(this.formatter(val + '', true).formattedValue);
			} catch(e) {
				this.jqVisibleField.val(val);
			}
		}
		
		this.markEmpty();
	}
	
	var assignValueChangeEvent = function() {
		var _this = this;
		
		this.jqVisibleField.bind('focus.changeListener', function() {
			this.prevval = this.value;
		});
		
		this.jqVisibleField.bind('blur.changeListener', function() {
			if(this.prevval != this.value) {
				this.prevval = this.value;
				$j(this).trigger('valuechange');
			}
		});
		
		this.jqField.bind('valuechange', function() {
			var jqThis = $j(this);
			_this.changedByUser = _this.getSimpleValue() !== this.initialvalue;
		});
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.getSimpleValue = getSimpleValue;
		this.getValue = getValue;
		this.setValue = setValue;
		this.assignValueChangeEvent = assignValueChangeEvent;
	}
})();

var IntegerField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		this.keyFilter = KeyFilter.INTEGER;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId).attr('name', jField.cn)
			.addClass('txt integer');
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		}
		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required)
			this.jqVisibleField.addClass('required');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(jField.validatorRegExp)
			this.validator = BaseField.createRegExpValidator(this);
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	var getValue = function(params) {
		var ret = {type: 'INTEGER', integerData: this.jqField.val()};
		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function() {
		return this.jqField.val();
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
	}
})();

var LongField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		this.keyFilter = KeyFilter.INTEGER;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'text').attr('id', this.htmlId).attr('name', jField.cn)
			.addClass('txt long');
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
		}
		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required)
			this.jqVisibleField.addClass('required');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(jField.validatorRegExp)
			this.validator = BaseField.createRegExpValidator(this);
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	var getValue = function(params) {
		var ret = {type: 'LONG', longData: this.jqField.val()};
		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getSimpleValue = function() {
		return this.jqField.val();
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.getValue = getValue;
		this.getSimpleValue = getSimpleValue;
	}
})();

var BooleanField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<input/>').attr('type', 'checkbox')
			.attr('id', this.htmlId).attr('name', jField.cn);
		
		if(jField.readonly === true) {
			this.jqVisibleField.attr('readonly', 'readonly').addClass('readonly');
			this.jqVisibleField.click(function() {
			    return false;
			});
		}

		if(jField.onchange != null){
        	this.jqVisibleField.bind('change', function(){
        		eval(jField.onchange);
        });
        }

		if(jField.disabled === true) {
			this.jqVisibleField.attr('disabled', 'disabled').addClass('disabled');
		}
		if(jField.required)
			this.jqVisibleField.addClass('required');
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('id', this.htmlId).attr('name', newCn);		
	}
	
	var setValue = function(val) {
		if(val === true || val === '1' || val === 1) {
			this.jqField.attr('checked', 'checked');
		} else {
			this.jqField.removeAttr('checked');
		}
		this.markEmpty();
	}

	var clearValue = function() {
	    Field.prototype.clearValue.call(this);
	    this.setValue(false);
	}

	var getValue = function(params) {
		var ret = {type: 'BOOLEAN', booleanData: this.jqField.prop('checked')};

		// je�li podano parametr packed = false, to zwracamy "niespakowane" warto�ci
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var prepareForMultipleDictionaryTable = function() {
	    Field.prototype.prepareForMultipleDictionaryTable.call(this);
		this.jqFieldContainer.width(16);
	}
	
	var getSimpleValue = function() {
		return this.jqField.prop('checked');
	}
	
	var assignValueChangeEvent = function() {
		this.jqVisibleField.bind('change.changeListener', function() {
			// dla czekboksa zawsze mamy zmienion� warto��
			$j(this).trigger('valuechange');
		});
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.getValue = getValue;
		this.clearValue = clearValue;
		this.getSimpleValue = getSimpleValue;
		this.prepareForMultipleDictionaryTable = prepareForMultipleDictionaryTable;
		this.assignValueChangeEvent = assignValueChangeEvent;
	}
})();

var LinkField = (function() {
	var create = function(afterField) {
		var jField = this.jField;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = BaseField.createAdditionalHidden(this);
		this.jqVisibleField = $j('<a/>').attr('id', this.htmlId).html('&nbsp;');
		
		if(jField.linkTarget != null)
			this.jqVisibleField.attr('target', jField.linkTarget);
		
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		
		this.jqFieldContainer.append(this.jqLabel, this.jqField, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('name', newCn);
		this.jqVisibleField.attr('id', this.htmlId);		
	}
	
	var setValue = function(val) {
		this.jqField.val(val.link);
		this.jqVisibleField.attr('href', val.link).html(val.label);
		if(val.label != null && val.label != '' &&  val.label.indexOf('title="') == -1) {
			this.jqVisibleField.attr('title', val.label);
		}
		
		this.markEmpty();
	}
	
	var clearValue = function() {
		Field.prototype.clearValue.call(this);
		this.setValue({label: '', link: '#'});
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.clearValue = clearValue;
	}
})();

var AttachmentField = (function() {
	var ctx = $j('#request_pageContext').val();

	var initializePlupload = function(up) {
	    var jqHeaders = $j('.plupload_filelist_header td');
	    jqHeaders.filter('.plupload_file_name').text(DWR.MSG.PLUPLOAD_FILE_NAME);
	    jqHeaders.filter('.plupload_file_status').text(DWR.MSG.PLUPLOAD_FILE_STATUS);
	    jqHeaders.filter('.plupload_file_size').text(DWR.MSG.PLUPLOAD_FILE_SIZE);
	    $j('.plupload_droptext').text(DWR.MSG.PLUPLOAD_DRAG_HERE);
	    $j('.plupload_add span.ui-button-text').text(DWR.MSG.PLUPLOAD_ADD_FILES);
	    $j('.plupload_start span.ui-button-text').text(DWR.MSG.PLUPLOAD_START_UPLOAD);
	}

	var refreshPlupload = function(up) {
//	    console.info('refresh');
        $j('.plupload_add span.ui-button-text').text(DWR.MSG.PLUPLOAD_ADD_FILES);
	}

	var filesRemoved = function(up, files) {
	    var i, len;
	    for(i = 0, len = files.length; i < len; i ++) {
            $j.post(DWR.UPLOAD_URL, {
                fieldCn: up.settings.multipart_params.fieldCn,
                id: files[i].id
            });
	    }
	}

	var beforeUpload = function(up, file) {
	    up.settings.multipart_params.id = file.id;
	}

	var createPlupload = function(field) {
	    var jqUpload = $j('<div/>');
        field.jqFieldContainer.append(jqUpload);

        jqUpload.plupload({
           runtimes: 'html5,silverlight,html4',
//           runtimes: 'flash',
           url: DWR.UPLOAD_URL,
           multipart: true,
           flash_swf_url: DWR.PLUPLOAD_FLASH_URL,
           silverlight_xap_url: DWR.PLUPLOAD_SILVERLIGHT_URL,
           multipart_params: {'fieldCn': field.jField.cn},
           preinit: {
               PostInit: initializePlupload
           },
           init: {
               Refresh: refreshPlupload,
               BeforeUpload: beforeUpload,
               FilesRemoved: filesRemoved
           }
        });
	}

	var createCompactPlupload = function(field) {
        var jqUpload = $j('<div id="' + field.jField.cn + '_container" class="dwr-attachment-container"/>');
        jqUpload.append('<button class="btn dwr-attachment-pickfiles" id="' + field.jField.cn + '_pickfiles">' + DWR.MSG.PLUPLOAD_ADD_FILES + '</button>');
        jqUpload.append('<div class="dwr-attachment-info"></div>');
        jqUpload.append('<div class="dwr-file-list"></div>');
        field.jqFieldContainer.append(jqUpload);

        var uploader = new plupload.Uploader({
           runtimes: 'silverlight,html5,html4',
           browse_button: field.jField.cn + '_pickfiles',
           container: field.jField.cn + '_container',
           url: DWR.UPLOAD_URL,
           multipart: true,
           flash_swf_url: DWR.PLUPLOAD_FLASH_URL,
           silverlight_xap_url: DWR.PLUPLOAD_SILVERLIGHT_URL,
           multipart_params: {'fieldCn': field.jField.cn},
           init: {
               BeforeUpload: beforeUpload,
               FilesRemoved: filesRemoved
           }
        });
        uploader.init();
        uploader.bind('FilesAdded', function(up, files) {
            var jqFile = null;
            $j.each(files, function(i, file) {
                jqFile = $j('<div />').attr('id', file.id).append('<div class="progress"></div>' + file.name).attr('title', DWR.MSG.PLUPLOAD_REMOVE);
                field.jqFieldContainer.find('.dwr-file-list').append(jqFile);
                jqFile.bind('click.remove', function() {
                    up.removeFile(up.getFile(this.id));
                    $j(this).remove();
                });
            });
            field.jqFieldContainer.find('.dwr-attachment-info').html(DWR.MSG.PLUPLOAD_SENDING);
            up.start();
        });
        uploader.bind('UploadProgress', function(up, file) {
            $j('#' + file.id).find('.progress').css('width', file.percent + '%');
        });
        uploader.bind('UploadComplete', function() {
            field.jqFieldContainer.find('.dwr-attachment-info').html(DWR.MSG.PLUPLOAD_COMPLETE);
        });

	}

	var create = function(afterField) {
		var jField = this.jField,
			_this = this;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		//this.jqField = $j('<input/>').attr('type', 'file').attr('name', jField.cn);
		this.jqField = BaseField.createAdditionalHidden(this);
		this.jqVisibleField = $j('<div/>').attr('id', this.htmlId).addClass('attachmentInfo');
		this.jqInfoContainer = $j('<span class="att-container-info"/>');

		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(jField.disabled)
			this.jqField.attr('disabled','disabled').addClass('disabled');

		this.jqFieldContainer.append(this.jqLabel, this.jqField, this.jqVisibleField, this.jqMsg, BaseField.createProgress());

		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);

		this.attFields = {};

		if(jField.readonly !== true && !jField.disabled) {
			this.attFields.newRevision = $j('<a/>').attr('href', '#').html(DWR.MSG.ATT_ADD_REVISION)
				.addClass('new-revision').hide().appendTo(this.jqVisibleField);
			this.attFields.prevRevision = $j('<a/>').attr('href', '#').html(DWR.MSG.ATT_REVERT_REVISION)
				.addClass('prev-revision').hide().appendTo(this.jqVisibleField);
		}
		this.attFields.upload = $j('<input/>').attr('type', 'file').attr('name', jField.cn)
			.attr('id', this.htmlId + '__').appendTo(this.jqVisibleField);
		this.jqInfoContainer.appendTo(this.jqVisibleField);
		if(jField.disabled) {
		    this.attFields.upload.attr('disabled', 'disabled');
		}

		this.attFields.title = $j('<span class="att-title"/>').appendTo(this.jqInfoContainer);
		this.attFields.userSummary = $j('<span class="att-user-summary"/>').appendTo(this.jqInfoContainer);
		this.attFields.ctime = $j('<span  class="att-ctime"/>').appendTo(this.jqInfoContainer);
		this.attFields.downloadFile = $j('<a/>').addClass('download-icon download icon').attr('title', DWR.MSG.ATT_DOWNLOAD)
			.attr('href', '#').hide().appendTo(this.jqInfoContainer);
		this.attFields.downloadPdf = $j('<a/>').addClass('pdf-icon pdf icon').attr('title', DWR.MSG.ATT_DOWNLOAD_AS_PDF)
			.attr('href', '#').hide().appendTo(this.jqInfoContainer);
		this.attFields.viewserver = $j('<a/>').addClass('zoom-icon zoom icon').attr('title', DWR.MSG.ATT_VIEWSERVER)
			.attr('href', '#').hide().appendTo(this.jqInfoContainer);

		if(jField.required === true) {
			this.attFields.upload.addClass('required');
		}

		this.jqField.attr('disabled', 'disabled');
		this.markEmpty();

		if(jField.multiple === true) {
            this.attFields.upload.hide();
            if(!this.jqFieldContainer.hasClass('dwr-attachment-compact')) {
                createPlupload(this);
            } else {
                createCompactPlupload(this);
            }
		}
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		Field.prototype.refreshCnChanges.call(this, oldCn, newCn);
		this.jqField.attr('name', newCn);
		this.attFields.upload.attr('name', newCn);
		this.jqVisibleField.attr('id', this.htmlId);	
	}
	
	var setValue = function(val) {
		var _this = this;
		// console.info(val);
		if(val && val.attachmentId != null) {
			this.attFields.upload.hide();
			this.attFields.title.html(val.title || '').show();
			if(val.title) {
				this.attFields.title.attr('title', val.title);
			}
			this.attFields.userSummary.html(val.userSummary || '').show();
			this.attFields.ctime.html(val.ctime != null ? 
					val.ctime.format(DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_HOUR_MINUTE_FORMAT) : '').show();
			
			this.attFields.downloadFile.unbind('click')
				.bind('click', function() {
					location.href = ctx + '/repository/view-attachment-revision.do?id=' + val.revisionId; 
				}).show();
			this.attFields.upload.unbind('change').bind('change', function(event) {
				_this.jqInfoContainer.hide();
				_this.attFields.prevRevision.show();
				_this.attFields.newRevision.hide();
				_this.attFields.upload.show();
			});

			if(val.mimeAcceptable === true) {
				this.attFields.downloadPdf.unbind('click')
					.bind('click', function() {
						location.href = ctx + '/repository/view-attachment-revision.do?asPdf=true&id=' + val.revisionId;
						return false;
					}).show();
				this.attFields.viewserver.unbind('click')
					.bind('click', function() {
						openToolWindow(ctx + '/viewserver/viewer.action?id=' + val.revisionId);
						return false;
					}).show();
			} else {
				this.attFields.downloadPdf.hide();
				this.attFields.viewserver.hide();
			}
			if(this.jField.readonly !== true) {
				if(this.attFields.newRevision != null) {
					this.attFields.newRevision.unbind('click').bind('click', function() {
						
						_this.attFields.upload.removeAttr('disabled').trigger('click');
						
						return false;
					}).show();
				}
				if(this.attFields.prevRevision != null) {
					this.attFields.prevRevision.unbind('click').bind('click', function() {
						_this.jqInfoContainer.show();
						_this.attFields.upload.val('').attr('disabled', 'disabled').hide();
						_this.attFields.newRevision.show();
						_this.attFields.prevRevision.hide();
						
						return false;
					});
				}
			}
			
			this.jqField.val(val.attachmentId);
			if(this.jField.disabled !== true) {
			    this.jqField.removeAttr('disabled');
			}
            if(this.jField.multiple === true && this.jqFieldContainer.hasClass('dwr-attachment-compact')) {
                if(this.jqFieldContainer.find('.dwr-file-list #ds_'+ val.attachmentId).length < 1) {
                    var jqFile = $j('<div />').attr('id', 'ds_' + val.attachmentId).append('<div class="progress" style="width: 100%"></div>' + val.title).attr('title', DWR.MSG.PLUPLOAD_REMOVE);
                    jqFile.append(this.attFields.downloadFile).append(this.attFields.downloadPdf).append(this.attFields.viewserver);
                    this.jqFieldContainer.find('.dwr-file-list').append(jqFile);
                    this.jqVisibleField.hide();

                    var fieldCn = this.jField.cn;
                    jqFile.bind('click.remove', function() {
                        $j.post(DWR.UPLOAD_URL, {fieldCn: fieldCn, id: val.attachmentId});
                        $j(this).remove();
                    });
                }
            }
		} else {
			this.attFields.downloadFile.hide();
			this.attFields.downloadPdf.hide();
			this.attFields.viewserver.hide();
			this.attFields.title.hide();
			this.attFields.userSummary.hide();
			this.attFields.ctime.hide();
			this.attFields.upload.show();
			if(this.attFields.newRevision != null)
				this.attFields.newRevision.hide();
			this.jqField.val('').attr('disabled', 'disabled');
		}

		this.markEmpty();
	}

	var clearValue = function() {
		this.setValue(null);
	}
	
	var require = function() {
		var fieldValid = true;
		
		if(this.jField.required) {
			if(this.jqField.val() == '' && this.attFields.upload.val() == '') {
				fieldValid = false;
			}
		}
		
		if(fieldValid) {
			this.errorMsg = null;
			this.valid = true;
		} else {
			this.errorMsg = Field.Messages.FIELD_REQUIRED;
			this.valid = false;
		}
		
		return this.valid;
	}
	
	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.setValue = setValue;
		this.clearValue = clearValue;
		this.require = require;
		this.createPlupload = createPlupload;
	}
})();

var ButtonField = (function() {
	var create = function(afterField) {
		var jField = this.jField,
			_this = this;
		
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = $j('<button/>').attr('id', this.htmlId).attr('name', jField.cn)
			.attr({'onclick':  'return false', 'type': 'button'}).addClass('btn');
		
		if(jField.hidden)
			this.jqVisibleField.addClass('hidden');
		if(jField.disabled) {
			//this.jqVisibleField.attr('disabled', 'disabled');
		}
		
		this.jqFieldContainer.append(this.jqLabel, this.jqField, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.markEmpty();
		
        this.jqVisibleField.bind('click.button', function() {
            $j(this).attr('clicked', 'true');
//            console.info('clicked!!!');
            if(jField.submit === true) {
                $j(this).attr('disabled', 'disabled');
                // waliduje popup, je�li trzeba
                if(jField.validate === true && Popup.FieldPool.isPopupField(_this)) {
//                    console.warn(jField.cn, 'popup validate', jField);
                    var evt = $j.Event('validate.popup', {
                        data: {
                            field: _this,
                            popups: _this.jField.popups
                        }
                    });

                    $j(this).trigger(evt);
                } else {
                    $j(this).trigger('valuechange');
                }
                //return false;
            }
            _this.align();
        });

		if(jField.submitForm === true) {
//		    console.warn('submitform na true');
            this.jqVisibleField.bind('submitForm', $j.proxy(function() {
//                console.info('submitForm!!!');
                this.submitForm(this.jField.listenersList);
            }, this));
		}
		
		this.loading.add($j.proxy(function(prg) {
			if(prg.end === true) {
//			    console.warn('END', this.jField.cn);
//			    console.info(this.jField.disabled);
				this.jqVisibleField.removeAttr('disabled');
			}
		}, this));
	}
	
	var setValue = function(val) {
		this.jqVisibleField.html(val.label).attr('buttonValue', val.value);

        if(val.clicked != null){
            this.jqVisibleField.html(val.label).attr('clicked',val.clicked);
        }
	}
	
	var getValue = function(params) {
		var clicked = this.jqVisibleField.attr('clicked') == 'true';
		var ret = {
			type: 'BUTTON',
			buttonData: {
				label: this.jqField.text(),
				value: this.jqVisibleField.attr('buttonValue'),
				clicked: clicked
			}
		}
		
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
			
		return ret;
	}
	
	var getMinimalDimensions = function(options) {
		if(this.jqVisibleField.attr('clicked') == 'true') {
			return {width: 0, height: 0};
		} else {
			return Field.prototype.getMinimalDimensions.call(this);
		}
	}
	
	return function() {
		this.create = create;
		this.setValue = setValue;
		this.getValue = getValue;
		this.getMinimalDimensions = getMinimalDimensions;
	}
})();

var DictionaryField = (function() {
	var create = function(afterField) {
		var jField = this.jField, i = 0;
		var jqAddToDic, jqShowDic, jqClearDic;
		/* Tworzymy grup� je�li trzeba */
		BaseField.createOrAssignFieldset(this);
		this.jqFieldContainer = BaseField.createContainer(this);
		this.jqLabel = BaseField.createLabel(this);
		this.jqMsg = BaseField.createMessage();
		this.jqField = this.jqVisibleField = BaseField.createAdditionalHidden(this).attr('id', this.htmlId);
		
		if(jField.dictionary.dicButtons != null) {
			for(i = 0; i < jField.dictionary.dicButtons.length; i ++) {
				this.availDictionaryButtons[jField.dictionary.dicButtons[i]] = true;
			}
		}
		
		jqClearDic = $j('<a/>').attr('href', '#').html(DWR.MSG.DIC_CLEAR_FIELDS).addClass('clear_dictionary');
		jqShowDic = $j('<a/>').attr('href', '#').html(DWR.MSG.DIC_SHOW_POPUP).addClass('show_dictionary');
		jqAddToDic = $j('<span/>').html(this.jField.newItemMessage || '').addClass('add_to_dictionary').css('visibility', 'hidden');
		this.jqFieldContainer.append(this.jqLabel, this.jqVisibleField, this.jqMsg, BaseField.createProgress());
		this.jqField.after(jqClearDic, jqShowDic, jqAddToDic);
		
		if(afterField && afterField.jField.fieldset == jField.fieldset)
			afterField.jqFieldContainer.after(this.jqFieldContainer);
		else
			this.jqFieldset.append(this.jqFieldContainer);
		
		this.dictionaryButtons = {
			addToDictionary: this.jqFieldContainer.children('span.add_to_dictionary'),
			showDictionary: this.jqFieldContainer.children('a.show_dictionary'),
			clearDictionary: this.jqFieldContainer.children('a.clear_dictionary')
		};
		
		if(jField.disabled === true) {
			this.jqFieldContainer.addClass('disabled_dictionary');
			for(var btnName in this.dictionaryButtons) {
				this.dictionaryButtons[btnName].addClass('force_hidden');
			}
		}
		
		/*if(jField.dictionary.showDictionaryPopup !== true && this.availDictionaryButtons.showPopup !== true) {
			this.dictionaryButtons.showDictionary.addClass('force_hidden');
		}*/

		if(this.availDictionaryButtons.showPopup !== true) {
		    this.dictionaryButtons.showDictionary.addClass('force_hidden');
		}
		
		if(this.availDictionaryButtons.doClear !== true) {
			this.dictionaryButtons.clearDictionary.addClass('force_hidden');
		}
		
		if(jField.dictionary.tableView === true && jField.coords) {
			this.jqFieldContainer.addClass('multipleDictionaryTable');
			this.jqLabel.html(this.jqLabel.html().slice(0, -1));
			this.jqFieldContainer.css('min-height', (this.getMinimalDimensions({heightOnly: true})) + 'px');
		}
		
		if(this.dictionaryFieldsManager == null) {
			this.dictionaryFieldsManager = new FieldsManager();
		}
		this.dictionaryFieldsManager.initDictionary(this, dwrParams ? {dwrParams: dwrParams} : null);
		this.markEmpty();
	}
	
	var refreshCnChanges = function(oldCn, newCn) {
		throw 'Dictionary codename cannot be changed [' + oldCn + ' -> ' + newCn + ']';		
	}
	
	var refresh = function(dicDiff) {
		Field.prototype.refresh.call(this, dicDiff);

		// TODO - czy aby na pewno mo�emy od�wie�ac warto�ci s�ownika kaskadowo,
		// a nie w updateFields()?
		var curField = null, cn = null, shortCn = null, i = 0, len = 0,
			dicjFields = this.jField.dictionary.fields,
			dicFields = this.dictionaryFieldsManager.fields,
			jField = this.jField,
			visibleFieldsCns = this.jField.dictionary.visibleFieldsCns,
			diff = null;
		
//		console.warn('refresh', this.jField.cn, dicFields);
//		console.info('asdf', dicjFields);
		for(cn in dicFields) {
			curField = dicFields[cn];
			
			for(i = 0, len = dicjFields.length; i < len; i ++) {
				shortCn = cn;
				if(this.jField.dictionary.multiple === true) {
					shortCn = cn.replace(/_\d+$/i, '');
//					console.warn('Short cn', shortCn);
				}
				if(dicjFields[i].status == 'CHANGED' && dicjFields[i].cn.replace(/_\d+$/i, '') == shortCn) {
//				    console.warn(dicjFields[i].cn, 'refresh');
                    diff = curField.compare(dicjFields[i]);
					curField.jField = dicjFields[i];
					curField.jField.cn = cn;
					curField.refresh(diff);
					break;
				}
			}
		}
		
		// przyciski na s�owniku
		if(dicDiff.disabled != null) {
			if(dicDiff.disabled.to === false) {
				this.jqFieldContainer.removeClass('disabled_dictionary');
			} else if(dicDiff.disabled.to === true) {
				this.jqFieldContainer.addClass('disabled_dictionary');
			}
		}
		this.availDictionaryButtons = {};
		if(jField.dictionary.dicButtons != null) {
			for(i = 0; i < jField.dictionary.dicButtons.length; i ++) {
				this.availDictionaryButtons[jField.dictionary.dicButtons[i]] = true;
			}
		}
		
		// nag��wek tabelki
		if(this.dictionaryFieldsManager.multipleFieldsHelper != null) {
			this.dictionaryFieldsManager.multipleFieldsHelper.refreshTableHeader();
		} else {
//			console.error(jField.cn, 'helper is null');
		}
		
		if(jField.dictionary.showDictionaryPopup !== true && this.availDictionaryButtons.showPopup !== true) {
			this.dictionaryButtons.showDictionary.addClass('force_hidden');
		} else {
			this.dictionaryButtons.showDictionary.removeClass('force_hidden');
		}
		if(this.availDictionaryButtons.doClear !== true) {
			this.dictionaryButtons.clearDictionary.addClass('force_hidden');
		} else {
			this.dictionaryButtons.clearDictionary.removeClass('force_hidden');
		}
		
		if(jField.dictionary.multiple === true) { 
			this.dictionaryFieldsManager.multipleFieldsHelper.refreshButtons();
		}
	}
	
	var remove = function(params) {
		for (var f in this.dictionaryFieldsManager.fields) {
			this.dictionaryFieldsManager.fields[f].remove();
			// todo czy nie dac delete
		}
		this.jqFieldContainer.remove();
	}
	
	var require = function() {
		var fieldValid = true;
		
		//if(this.jField.required) {
			fieldValid = this.dictionaryFieldsManager.validate();
		//}
		
		if(fieldValid) {
			this.errorMsg = null;
			this.valid = true;
		} else {
			this.errorMsg = Field.Messages.FIELD_REQUIRED;
			this.valid = false;
		}
		
		return this.valid;
	}
	
	var setValue = function(val, params) {
		if(this.jField.dictionary.multiple === true) {
		    // console.warn(this.jField.cn, val);
			try {
				//this.multipleFieldsValues = val + '';
				this.multipleFieldsValues = (val + '').split(',');
				// console.info('setting multiple dictionary', this.dictionaryFieldsManager.jDictionaryField.cn, this.multipleFieldsValues + ' ');
			} catch(e) {
				console.error(e);
			}
			
			if(!MultipleFieldsHelperObserver.isSubscribed(this.dictionaryFieldsManager.jDictionaryField.cn, 'load')) {
				MultipleFieldsHelperObserver.subscribe(
					this.dictionaryFieldsManager.jDictionaryField.cn,
					'load',
					{afterEvent: 'create'},
					$j.proxy(function(observerArgs) {
							// console.info('invoke "load" observer ',  this.dictionaryFieldsManager.jDictionaryField.cn, val + ' y', this.multipleFieldsValues + ' x');
						// wywo�ujemy Dictionary.getFieldsValues tylko je�li mamy jakie� warto�ci
						var newValuesPresent = false, i = 0;
//						console.info('KAMIL', this.multipleFieldsValues.length);
						
						try {
							if((this.multipleFieldsValues && this.multipleFieldsValues.length > 0) || (val && val.length > 0)) {
								// console.warn(this.dictionaryFieldsManager.jDictionaryField.cn, 'multivals exist', this.multipleFieldsValues,
										// Object.prototype.toString.call(val));
								
								// console.info('[Observer] from', observerArgs);
								if(observerArgs === undefined) {
								    observerArgs = val;
								}
								this.dictionaryFieldsManager.multipleFieldsHelper.createMultipleFields(observerArgs);
								this.dictionaryFieldsManager.multipleFieldsHelper.toggleTable(true);
								
							} else {
	//							console.warn('no multivals present', val + ' ');
							}
						} catch(e) { 
							console.error(e); 
						}
						$j('body').trigger('grid-resize');
					}, this)
				);
			}
//			console.warn(_this.dictionaryFieldsManager.jDictionaryField.cn, 'load');
			// console.info('[Observer] to', val);
			MultipleFieldsHelperObserver.publish(this.dictionaryFieldsManager.jDictionaryField.cn, 'load', {args: val});
			
//			console.warn('settings dictionary value', this.jField.cn.toString());
//			console.warn(this.jField.cn, 'value is', val);
			
			if(params && params.syncMultipleFieldsValues === true) {
				if(Object.prototype.toString.call(val) != '[object Array]')
					throw new ObjectUnsupportedTypeException('Value for syncMultipleFieldsValues must be an Array.');
				this.multipleFieldsValues = val.slice(0);
				val = this.multipleFieldsValues.join();
			} else {
				try {
					this.multipleFieldsValues = val.slice(0);
					if(val.join('') == '')
						this.multipleFieldsValues.length = 0;
				} catch(e) {
					this.multipleFieldsValues = [];
				}
//				console.warn(this.jField.cn + '');
				
					
				for(cn in this.dictionaryFieldsManager.fields) {
					if(this.multipleFieldsValues.length && cn.search(/id_\d?/) != -1) {
						this.dictionaryFieldsManager.fields[cn].setValue(this.multipleFieldsValues[0]);
					}	
				}
			}
			
		}
		this.jqField.val(val);
		this.markEmpty();
	}
	
	var getValue = function(params) {
		var ret = {type: 'STRING', stringData: this.jqField.val()};
		if(params && params.packed === false) {
			ret = BaseField.unpackValues(ret);
		}
		return ret;
	}
	
	var getMinimalDimensions = function(options) {
		var verticalLabel = !this.jqFieldContainer.hasClass('dwr-horizontal-dic-title') && this.jField.dictionary.tableView === true;
		var visibleLabel = !this.jqFieldContainer.hasClass('dwr-no-label');
		if(options && options.heightOnly === true)
			return verticalLabel ? this.jqLabel.outerWidth() : 10;
		
		var maxLabelWidth = this.jqLabel.outerWidth(), maxFieldWidth = 0;
		var jqContainer = this.jqFieldContainer;
		var jqTd = jqContainer.parent('td');
		var horizontalPadding = parseInt(jqContainer.css('paddingLeft'), 10) + parseInt(jqContainer.css('paddingRight'), 10);
			horizontalPadding += parseInt(jqTd.css('paddingLeft'), 10) + parseInt(jqTd.css('paddingRight'), 10);
		var cn, field, width;
		var curLabelWidth, curFieldWidth;
		
		for(cn in this.dictionaryFieldsManager.fields) {
			field = this.dictionaryFieldsManager.fields[cn];
			curFieldWidth = field.getMinimalDimensions({label: false}).width;
			curLabelWidth = field.getMinimalDimensions().width - curFieldWidth;
			if(curFieldWidth > maxFieldWidth)
				maxFieldWidth = curFieldWidth;
			if(curLabelWidth > maxLabelWidth)
				maxLabelWidth = curLabelWidth;
		}
		
		// je�li s�ownik w tabelce, to bierzemy wi�kszy wymiar
		if(this.jField.dictionary.tableView === true) {
			width = Math.max(maxFieldWidth, maxLabelWidth) + horizontalPadding;
		} else {
			width = maxLabelWidth + maxFieldWidth + horizontalPadding;
		}
		
		// etykieta dla s�ownika jest obr�cona o 90 stopni (je�li nie ma klasy dwr-horizontal-dic-title)
//		console.info('getMinimalDimensions', width);

		return {
			width: width,
			height: (verticalLabel && visibleLabel) ? this.jqLabel.outerWidth() : 10
		}
	}
	
	var remove = function(params) {
		Field.prototype.remove.call(this, params);
//		console.warn('Removed dictionary', this.jField.cn);
		try {
			MultipleFieldsHelperObserver.unsubscribe(this.dictionaryFieldsManager.jDictionaryField.cn);
		} catch(e) {
			console.error(e);
		}
	}

	var markEmpty = function() {
	    var empty = Field.prototype.markEmpty.call(this);
	    var hideEmpty = this.jqFieldContainer.hasClass('dwr-hide-empty');
	    if(hideEmpty) {
            if(empty) {
                this.jqFieldContainer.addClass('dwr-hide-empty-dic');
            } else {
                this.jqFieldContainer.removeClass('dwr-hide-empty-dic');
            }
	    }

	    return empty;
	}

	return function() {
		this.create = create;
		this.refreshCnChanges = refreshCnChanges;
		this.refresh = refresh;
		this.remove = remove;
		this.require = require;
		this.setValue = setValue;
		this.getValue = getValue;
		this.markEmpty = markEmpty;
		this.getMinimalDimensions = getMinimalDimensions;
		this.remove = remove;
	}
})();


/**
 * Metoda rysuje pole na formatce oraz inicjalizuje walidatory i s�ownik.
 * @param {Field} afterField OPCJONALNY Obiekt typu Field, poni�ej 
 * kt�rego zostanie umieszczone pole.  
 * @exception {FieldNotInitialisedException} Rzuca wyj�tek w przypadku 
 * niezainicjalizowania pola.
 * @exception {FieldUnsupportedTypeException} Rzuca wyj�tek je�li typ pola
 * nie zosta� rozpoznany.
 * @method
 */
Field.prototype.create = function(afterField) {
	/*if(!this.jqColumn || !this.jField || this.jqColumn.length < 1)
		throw new FieldNotInitialisedException('jqColumn or jField');*/	
	this.additionalFields = this.jField.params ? {} : null;

	var strClassName = 'field ' + this.jField.cn + ' ' + this.jField.type + ' ' + this.jField.kind;
	var _this = this;
	if (this.jField.hidden)
		strClassName += ' hidden';
	var str = '<div class="' + strClassName + '"><label for="' + this.htmlId + '" class="' + (this.jField.required ? 'required' : '') + '">' 
		+ this.jField.label + ':</label>';
	if (this.jField.type == 'HIDDEN') 
		str = "";
	var className = this.isSearchable === true ? 'searchable' : '';
	switch(this.jField.type) {
		case 'ENUM_AUTOCOMPLETE':
			str += '<div class="autocompleteArrow" style="background-image:url(' + $j('#request_pageContext').val() + '/img/gm_arrow_down.gif);"></div>';
			str += '<input readonly="readonly" id="autocomplete_button_' + this.htmlId + '"  class="txt autocompleteButton autocomplete_' + this.jField.cn + '" value="' + DWR.MSG.ENUM_MULTIPLE_SELECT + '" />';
			str += '<span id="autocomplete_container_' + this.htmlId + '" class="autocompleteContainer autocomplete_' + this.jField.cn + '">';
			str += '<input type="text" id="autocomplete_' + this.htmlId + '" class="autocomplete autocomplete_' + this.jField.cn + '"/>';
			str += '<input type="hidden" id="' + this.htmlId + '" name="' + this.jField.cn + '" style="width: 10px"/>';
			str += '<div class="clearBoth"></div></span>';
		break;
		default:
			throw new FieldUnsupportedTypeException(this.jField.type);
	}
	
	str += '<span class="message" style="display: none;"></span>';
	str += '<span class="progress"></span></div>';
	
	if(!this.jField.fieldset)
		throw new FieldNotInitialisedException('fieldset');
		
	/* Tworzymy grup� je�li trzeba */
	BaseField.createOrAssignFieldset(this);
	if(afterField && afterField.jField.fieldset == this.jField.fieldset)
		afterField.jqFieldContainer.after(str);
	else
		this.jqFieldset.append(str);
	
	this.jqField = $j('#' + this.htmlId);
	this.jqLabel = this.jqField.prev('label');
	this.jqFieldContainer = this.jqField.parent();
	this.jqMsg = this.jqField.parent().children('span.message');
	this.pixelInterval = this.jqField.width() / (this.jField.submitTimeout / 100);
	this.intervalMaxWidth = this.jqField.outerWidth();
	
	if(this.jField.readonly === true)
		this.jqField.attr('readonly', 'readonly');
	
	if (this.jField.disabled === true)
		this.jqField.attr('disabled', 'disabled');
	
	if (this.jField.hidden) {
		this.jqField.addClass('hidden');
		this.jqLabel.hide();
		//return;
	}
	
	if(this.jField.cssClass != '')
		this.jqFieldContainer.addClass(this.jField.cssClass);
	
	if(!this.jField.validatorRegExp) {
		switch(this.jField.type) {
			case 'INTEGER':
				//this.validator = Validator.INTEGER;
			break;
			case 'TIMESTAMP':
				//this.validator = Validator.TIMESTAMP;
			break;
		}
	} else {
		this.validator = {RegExp: RegExp(this.jField.validatorRegExp), Message: 'Nieprawidlowe'};
	}
	
	this.jqVisibleField = this.jqField;
	
	switch(this.jField.type) {
		case 'ENUM_AUTOCOMPLETE':
			this.jqAutocompleteContainer = $j('#autocomplete_container_' + this.htmlId);
			this.jqAutocompleteButton = $j('#autocomplete_button_' + this.htmlId);
			this.jqFieldContainer = this.jqAutocompleteButton.parent();
			this.jqAutocompleteField = $j('#autocomplete_' + this.htmlId);
			this.jqAutocompleteButton.bind('click.toggleAutocomplete', function() {
				_this.toggleEnumAutocomplete();
			});
			this.jqVisibleField = this.jqAutocompleteButton;
			this.registerEnumAutocomplete();
		break;
	}
	
	if(this.jField.required)
		this.jqVisibleField.addClass('required');
}

/**
 * Sprawdza, czy pole jest puste i je�li tak do dodaje klas�
 * 'empty' do jqVisibleField
 * @return <code>true</code> je�li pole jest puste
 */
Field.prototype.markEmpty = function() {
	if(this.getSimpleValue() == '') {
		this.jqVisibleField.addClass('empty');
		this.jqLabel.addClass('empty');
		return true;
	} else {
		this.jqVisibleField.removeClass('empty');
		this.jqLabel.removeClass('empty');
		return false;
	}
}

/**
 * Rejestruje listener wywo�ywany w przypadku zmiany warto�ci pola.<br />
 * Metoda powinna sprawdza� czy faktycznie warto�� pola si� zmieni�a
 * i je�li tak to wywo�ywa� event "valuechange" na polu jqVisibleField
 */
Field.prototype.assignValueChangeEvent = function() {
	var _this = this;
	
	this.jqVisibleField.bind('keydown.changeListener', function(event) {
		if(KEYS.isCommandKey(event.keyCode) != true) {
			this.prevval = this.value;
		}
	});
	
	this.jqVisibleField.bind('change.changeListener', function() {
//		console.warn('change.changeListener', this.id, this.prevval, this.value);
		if(this.prevval != this.value) {
			this.prevval = this.value;
			$j(this).trigger('valuechange');
		} else {
			//console.warn(_this.jField.cn, 'value not changed', this.prevval, this.value);
		}
	});
	
	this.jqField.bind('valuechange', function() {
		var jqThis = $j(this);
//		console.warn('valuechange1', this.id, jqThis.val());
		_this.changedByUser = _this.getSimpleValue() !== this.initialvalue;
		
		/*if(_this.changedByUser) {
			console.info('value has been changed');
		} else {
			console.info('value has been NOT changed');
		}*/
	});
}

/**
 * Przerysowuje pole na podstawie zmienionych atrybut�w.
 * @method
 */
Field.prototype.refresh = function(diff) {
//	console.info('Diff', diff);

	if(diff.readonly != null) {
        if(diff.readonly.to === true)
            this.jqVisibleField.attr('readonly', 'readonly');
        else
            this.jqVisibleField.removeAttr('readonly');
	}
	if(diff.required != null) {
	    if(diff.required.to === true) {
	        this.jqVisibleField.addClass('required');
            this.jqLabel.addClass('required');
	    } else {
	        this.jqVisibleField.removeClass('required');
            this.jqLabel.removeClass('required');
	    }
	}
	if(diff.disabled != null) {
	    if(diff.disabled.to === true)
	       this.jqVisibleField.attr('disabled', 'disabled');
	    else
	       this.jqVisibleField.removeAttr('disabled');
	}
	if(diff.hidden != null) {
	    if(diff.hidden.to === true) {
	        this.jqFieldContainer.addClass('hidden');
            this.jqVisibleField.addClass('hidden');
            this.jqLabel.hide();
	    } else {
	        this.jqFieldContainer.removeClass('hidden');
            this.jqVisibleField.removeClass('hidden');
            this.jqLabel.show();
	    }
	}
	if(diff.label != null) {
	    this.jqLabel.html(this.jField.label + (diff.label.to != '' ? ':' : ''));
        this.align();
	}

	if(this.isSearchable === true) {
        this.jqVisibleField.addClass('searchable');
    } else {
        this.jqVisibleField.removeClass('searchable');
    }

    if(this.transformedToTable === true) {
        this.jqLabel.hide();
        if(this.jField.hidden === true) {
            this.jqCell.addClass('force_hidden');
        } else {
            this.jqCell.removeClass('force_hidden');
        }
    }
    // na potrzeby edytora dockind�w
    try {
        //this.jqFieldContainer.revertablecss('update', ['width', 'min-width', 'height']);
        this.jqFieldContainer.revertablecss('delete');
        this.jqFieldContainer.parent('td').revertablecss('update', ['min-width', 'min-height', 'height']);
    } catch(e) {}
}

// TODO tymczasowo 
Field.prototype.setRequiredState = function(required) {
	if(this.originalRequired == null) {
		this.originalRequired = this.jField.required;
	}
	this.jField.required = required;
	
	if(this.jField.required)
		this.jqField.addClass('required');
	else
		this.jqField.removeClass('required');
}

// TODO tymczasowo
Field.prototype.revertRequiredState = function() {
	if(this.originalRequired == null)
		return;
//	console.info(this.jField.cn, 'required =', this.originalRequired);
	this.jField.required = this.originalRequired;
	this.setRequiredState(this.originalRequired);
}


/**
 * Usuwa pole (z drzewa DOM przegl�darki). W przypadku s�ownika usuwa
 * r�wnie� pola s�ownika
 * @param {Object} params [dodatkowy argument] Mapa z dodatkowymi ustawieniami:
 * <ul>
 * <li><code>animation = true | false</code> Animacja usuwania element�w (domy�lnie <code>false</code>)
 * </ul>
 * @method
 */
Field.prototype.remove = function(params) {
	this.jqFieldContainer.remove();
}

/**
 * Sprawdza poprawno�� pola (tj. uruchamia przypisany do pola walidator).
 * @method
 */
Field.prototype.validate = function() {
	if(this.validator && this.jqField.val().search(this.validator.RegExp) == -1) {
		this.errorMsg = this.validator.Message;
		this.valid = false;
	} else {
		this.errorMsg = null;
		this.valid = true;
	}
	
	return this.valid;
}

/**
 * Sprawdza, czy pole jest uzupe�nione.
 * Dla s�ownik�w musi by� wpisane <code>id</code> elementu
 * lub wype�nione obowi�zkowe pola (wtedy tworzymy nowy wpis).
 * Kuba Tarasiuk - zmiana - tylko wype�nione obowi�zkowe pola (wtedy tworzymy nowy wpis).
 * @method
 * @return {Boolean} <code>true</code>, je�li pole jest uzupe�nione.
 */
Field.prototype.require = function() {
	var fieldValid = true,
		validationNeeded = true;
	
	if(this.jField.required && this.jqFieldContainer.hasClass('hide-empty-select')) {
		try {
			//TODO trzeba rozszerzy� o inne typy p�l
			validationNeeded = this.getValue({allOptions: true, placeholder: false}).enumValuesData.allOptions.length > 0;
		} catch(e) {
			validationNeeded = true;
		}
	}
	
	//klasa dla ca�ego s�ownika, je�eli select nie ma �adnych opcji
	if(this.jField.required && validationNeeded) {
		fieldValid = this.getSimpleValue() ? true : false;
	}
	
	if(fieldValid) {
		this.errorMsg = null;
		this.valid = true;
	} else {
		this.errorMsg = Field.Messages.FIELD_REQUIRED;
		this.valid = false;
	}
	
	//trace(this.jField.cn + ' = ' + this.valid);
	
	return this.valid;
}

/**
 * Pokazuje komunikat o b��dzie.
 * @method
 */
Field.prototype.showError = function() {
	this.jqLabel.addClass('invalid');
	this.jqMsg.html(this.errorMsg).show();
	this.jqVisibleField.addClass('invalid');
}

/**
 * Ukrywa komunikat o b��dzie.
 * @method
 */
Field.prototype.hideError = function() {
	this.jqLabel.removeClass('invalid');
	this.jqMsg.hide();
	this.jqVisibleField.removeClass('invalid');
}

/**
 * Ustawia warto�� pola.
 * @method
 * @param {Object} val Obiekt z warto�ci� pola
 * @param {Object} params [dodatkowy argument] Mapa z dodatkowymi ustawieniami:
 * <ul>
 * <li><code>allOptions = true | false</code> Dla <code>ENUM</code> i podobnych - nadpisuje wszystkie opcje w selekcie (domy�lnie <code>true</code>)
 * <li><code>syncMultipleFieldsValues = true | false</code> Dla s�ownik�w wielowarto�ciowych - nadpisuje tablic� <code>multipleFieldsValues</code>
 * tablic� <code>val</code> i ustawia warto�� pola. <b>UWAGA</b> - je�li w��czone, to nie wywo�ujemy <code>createMultipleFields</code>
 * </ul>
 */
Field.prototype.setValue = function(val, params) {
	var _this = this;
	var i, j, len, key, value, items, cn;
	switch(this.jField.type) {
		case 'ENUM_AUTOCOMPLETE':
			items = [];
			for(i = 0, len = val.allOptions.length; i < len; i ++) {
				for(key in val.allOptions[i]) { // key = warto�� opcji
					items.push({label: val.allOptions[i][key], value: key});
				}
			}
			
			this.jqAutocompleteField.autocomplete(items, 
			{
				minChars: 0,
				max: val.allOptions.length,
				autoFill: false,
				matchContains: true,
				formatItem: function(row, i, max, term){
					return '<td>' + row.label + '</td>';
				}
			}).bind('result.put', function(event, data, formatted) {
				//trace('result');
				_this.jqField.val(data.value);
				_this.jqAutocompleteButton.val(data.label.slice(0,20));
				_this.toggleEnumAutocomplete();
				_this.jqAutocompleteButton.trigger('itemSelected');
			}).bind('blur.noMatch', function() {
				if($j(this).val().trim() == '')
					_this.jqField.val('');
			});
			this.jqField.val(val.selectedOptions[0]);
			for(i = 0, len = items.length; i < len; i ++) {
				if(items[i].value == val.selectedOptions[0]) {
					_this.jqAutocompleteButton.val(items[i].label.slice(0,20));
				}
			}
			_this.jqAutocompleteButton.val(data.label.slice(0,20));
			this.jqField.addClass(' ');
		break;
		default:
			this.jqField.val(val);
	}	
	
	if(this.formatter) {
		try {
			this.jqVisibleField.val(this.formatter(val + '', true).formattedValue);
		} catch(e) {
			this.jqVisibleField.val(val);
		}
	}
	
	/* Zapisujemy szeroko�� pola + etykiety */
	/*var oldWidth = this.jqColumn.data('expWidth');
	var newWidth = this.jqField.width() + this.jqLabel.width() + 10;
	if(oldWidth == null)
		this.jqColumn.data('expWidth', newWidth);
	else if(newWidth > oldWidth)
		this.jqColumn.data('expWidth', newWidth);*/
}

/**
 * Czy�ci warto�� pola:
 * <ul>
 * <li>Dla typ�w <code>string</code> i podobnych ustawia warto�� na pusty string
 * <li>Dla typu <code>enum</code> odznacza wszystkie opcje
 * </ul>
 * @method
 */
Field.prototype.clearValue = function() {
	switch(this.jField.type) {
		case 'ENUM_AUTOCOMPLETE':
			this.jqField.val('');
		break;
		default:
			this.jqField.val('');
			this.jqVisibleField.val('');
			try {
				this.jqVisibleField[0].prevval = null;
			} catch(e) {}
	}
}

/**
 * @method Czy pole mo�e by� wyczyszczone
 * @return <code>true</code> je�li mo�na wyczy�ci� pole
 */
Field.prototype.isClearable = function() {
	var jField = this.jField;
	if(jField.readonly === true || jField.disabled === true) {
		return false;
	}
	
	return true;
}

/**
 * Zwraca warto�� pola jako String.
 * @method
 * @param {Object} params [dodatkowy argument] Mapa z dodatkowymi ustawieniami
 * <ul>
 * <li><code>selectedOptionName = true | false </code> dla enum i podobnych 
 * - zwraca TESKT opcji a nie jej warto�� (domy�lnie <code>false</code>)
 * <li><code>formatted = true | false </code> sformatowana warto�� pola (np.: DATE, MONEY) (domy�lnie <code>false</code>)
 * </ul>
 * @return {String} �a�cuch tekstowy(mo�e by� pusty) z warto�ci� pola. 
 */
Field.prototype.getSimpleValue = function(params) {
	switch(this.jField.type) {
		default:
			return this.jqField.val();
	}
}

/**
 * Zwraca warto�� pola opakowan� w obiekt Java FieldData - taki obiekt mo�na
 * przes�a� do DWR.
 * @param {Object} params [dodatkowy argument] Mapa z dodatkowymi ustawieniami:
 * <ul>
 * <li><code>allOptions = true | false</code> dla <code>ENUM</code> i podobnych 
 * - umieszcza w warto�ci zwracanej wszystkie opcje z selecta (domy�lnie true)
 * <li> <code>packed = true | false </code> Okre�la czy warto�� ma by� opakowana 
 * w pola <code>type</code> i <code>typ_polaData</code>. Domy�lnie true. 
 * </ul>
 * @method
 * @return {Object} Obiekt Java FieldData
 */
Field.prototype.getValue = function(params) {
	var ret = {type: 'STRING', stringData: this.jqField.val()};
	// je�li podano parametr, to zwracamy "niespakowane" warto�ci
	if(params && params.packed === false) {
		(function() {
			var key, retObj;
			switch(this.jField.type) {
				default:
					for(key in ret) {
						if(key != 'type')
							ret = ret[key];
					}
			}
		}).apply(this);
	}	
	
	return ret;	
}

/**
 * Pokazuje lub ukrywa (na podstawie przypisanej klasy CSS) pole podpowiedzi
 * dla pola typu ENUM_AUTOCOMPLETE.
 * @method
 * @private
 */
Field.prototype.toggleEnumAutocomplete = function() {
	if(this.jqAutocompleteButton.hasClass('autocompleteButtonPressed')) {
		this.jqAutocompleteButton.removeClass('autocompleteButtonPressed');
		this.jqAutocompleteContainer.hide();
		this.jqAutocompleteField.val('');
	} else {
		this.jqAutocompleteButton.addClass('autocompleteButtonPressed');
		this.jqAutocompleteContainer
			.css('left', this.jqAutocompleteButton.position().left + 'px')
			.css('top', this.jqAutocompleteButton.position().top + this.jqAutocompleteButton.outerHeight() + 'px');
		this.jqAutocompleteContainer.show();
		this.jqAutocompleteField.focus().click();
	}
}

/**
 * Przypisuje obs�ug� zdarze� dla pola typu ENUM_AUTOCOMPLETE.
 * @method
 * @private
 */
Field.prototype.registerEnumAutocomplete = function() {
	var _this = this;
	$j(document).bind('click.' + this.htmlId, function(event) {
		var ourClassName = 'autocomplete_' + _this.jField.cn;
		if(event.target.className.indexOf(ourClassName) == -1 && _this.jqAutocompleteButton.hasClass('autocompleteButtonPressed'))
			_this.toggleEnumAutocomplete();
		tmp10 = event;
	});
}

/**
 * Ustawia odpowiedni� szeroko�� kontenera pola, aby elementy mie�ci�y si� w jednej linii
 * @method
 */
Field.prototype.align = function(options) {
	var dim = this.getMinimalDimensions();
	this.jqFieldContainer.parent('td')
		.css('width', dim.width + 'px').css('min-width', dim.width + 'px').css('height', dim.height + 'px');
}

/**
 * Zwraca minimalne wymiary pola przy kt�rych elementy u�o�one s� w jednej linii
 * @method
 * @param {Object} options Mapa z opcjami:
 * <ul>
 * <li>label: <code>true | false</code></li> Do liczenia szeroko�ci dodawaj d�ugo�� etykiety (domy�lnie <code>true</code>)
 * </ul>
 * @return {Object} Mapa <code>width</code> - minimalna d�ugo�� pola, 
 * 	<code>height</code> - minimalna wysoko�� pola
 */
Field.prototype.getMinimalDimensions = function(options) {
	var includeLabel = options ? options.label : true;
	var width = this.jqVisibleField.outerWidth(),
		height = Math.max(this.jqLabel.outerHeight(), this.jqVisibleField.outerHeight());
	
	if(includeLabel) {
		if(this.jqLabel.filter(':visible').length) {
			width += this.jqLabel.outerWidth() + 5; // 5 dla odst�pu
		}
	}
	if(this.isSearchable === true) {
		width += 19;
	}
	if(this.jField.required === true) {
		width += 12;
	}
	return {
		width: width,
		height: height
	}
}

/**
 * Dodaje atrybut placeholder do pola.
 * @mathod
 * @param {String} text Tekst dodawany jako placeholder (je�li nie podano to funkcja wstawia
 * tekst 'puste')
 */
Field.prototype.addPlaceholder = function(text) {
	this.jqVisibleField.attr('placeholder', text != null ? text : 'puste');
}

/**
 * Przygotowuje pole w przypadku, gdy znajduje si� w s�owniku wielowarto�ciowym w tabelce.
 * @method
 */
Field.prototype.prepareForMultipleDictionaryTable = function() {
    if(ie7 === true) {
        this.jqFieldContainer.width(this.getMinimalDimensions({label: false}).width);
    }
}

/**
 * Tworzy odno�nik "dodaj nast�pny" dla p�l wielowarto�ciowych
 * @param {Object} afterField Obiekt typu Field poni�ej 
 * kt�rego zostanie umieszczone pole.
 * @param {Function} callFun Funkcja wywo�ywana po nacisni�ciu odno�nika w kontek�cie (this) = ahref 
 * @param {Object} callFunParams Opcjonalne parametry dla funkcji <code>callFun</code> <br />
 * Metod� mo�na wywo�a� bez obiektu Field <code>Field.prototype.createMultipleSwitcher.call(null, afterField)</code>  
 * @return {Object} Obiekt jQuery utworzonego przycisku
 */
Field.prototype.createMultipleSwitcher = function(afterField, callFun, callFunParams) {
	var jqAfterField = afterField.jqFieldContainer;
	var jqSwitch = $j('<div/>').addClass('field').append('<a href="#" class="addItem">Dodaj nast�pny element</a>');
	
	jqSwitch.insertAfter(jqAfterField).children().bind('click', function() {
		return callFun.call(this, callFunParams);
		/*var jqFieldset = $j(this).parent('div').parent('fieldset');
		var jqNewFieldset = jqFieldset.clone(true);
		
		jqNewFieldset.insertAfter(jqFieldset);
		return false;*/ // prevents go to '#'
	});
	
	return jqSwitch;
}

/**
 * Wysy�a ca�y formularz, ustawiaj�cy hiddeny w listenersList na <code>true</code>
 *
 */
Field.prototype.submitForm = function(listenersList) {
//    console.info(this.jField.cn, listenersList);
    var i = 0, len = listenersList ? listenersList.length : 0,
        jqEl = null;
    for(i = 0; i < len; i ++) {
        jqEl = $j('[name=' + listenersList[i] + ']:last');
        if(jqEl.is('[type=button]') || jqEl.is('[type=submit]')) {
//            console.info('found', jqEl);
            jqEl.click();
            return;
        } else {
            jqEl.val(true);
        }
    }
    //this.jqVisibleField.parents('form').submit();
}

/**
 * Tworzy kopi� obiektu klasy Javy Field
 * @return {Object} Obiekt JField
 */
Field.prototype.clonejField = function() {
	var jField = {};
	var key;
	for (key in this.jField) {
		jField[key] = this.jField[key];
	}
	
	return jField;
}

/**
 * Od�wie�a pole (zmienia id, nazwy klas),
 * je�li zosta�o zmienione <code>cn</code> pola.
 * Ukatualnia <code>htmlId</code>
 */
Field.prototype.refreshCnChanges = function(oldCn, newCn) {
	this.htmlId = this.htmlId.replace(oldCn, newCn);
	
	this.jqFieldContainer.removeClass(oldCn).addClass(newCn);
	this.jqLabel.attr('for', this.htmlId);
}

/**
 * Por�wnuje jField pola z drugim jField2
 * Zwraca <code>null</code> gdy identyczne
 */
Field.prototype.compare = function(jField2) {
    var diff = {}, propName = null,
        jField1 = this.jField;

    for(propName in jField1) {
        if(propName == 'dictionary')
            continue;
        if(jField1[propName] !== jField2[propName]) {
            equals = false;
            diff[propName] = {
                from: jField1[propName],
                to: jField2[propName]
            }
        }
    }

    return diff;
}

Field.prototype.dispose = function() {
	if(this.dictionaryFieldsManager)
		this.dictionaryFieldsManager.dispose();
}

Field.prototype.sendComplete = function() {
	$j(document).trigger('dwr_field_complete', 
			[this.jField.cn, 
			 this.jqFieldContainer, 
			 this.jField.kind == 'DICTIONARY_FIELD' ? this.parentFieldsManager.jDictionaryField.cn : null]);
}

Field.prototype.sendResizeNotification = function() {
    this.jqVisibleField.trigger({
        type: 'resize',
        field: this
    });
}

/**
 * Zwraca <code>true</code> je�li pole jest rysowane dynamicznie
 */
Field.prototype.isDynamic = function() {
    return false;
}

Field.prototype.destroyDynamic = function() {

}

Field.prototype.createDynamic = function() {

}

Field.prototype.setPreHook = function(functionName, fun) {
	if(this.registeredPreHooks == null)
		this.registeredPreHooks = {};

	if(functionName == null) 
		throw 'Function name is required';
	if(this.registeredPreHooks[functionName] != null)
		throw 'Function ' + functionName + ' already hooked';
	if(this[functionName] == null)
		throw 'Function ' + functionName + ' does not exist in field ' + this.cn;
	if(fun == null)
		throw 'Function fun must be provided';
	if(Object.prototype.toString.call(fun) != '[object Function]')
		throw 'Function fun is not callable function';

	var oldFun = this[functionName];
	this[functionName] = function() {
		fun.apply(this, arguments);
		oldFun.apply(this, arguments);
	}
	this.registeredPreHooks[functionName] = true;
}

Field.prototype.setPostHook = function(functionName, fun) {
	if(this.registeredPostHooks == null)
		this.registeredPostHooks = {};

	if(functionName == null) 
		throw 'Function name is required';
	if(this.registeredPostHooks[functionName] != null)
		throw 'Function ' + functionName + ' already hooked';
	if(this[functionName] == null)
		throw 'Function ' + functionName + ' does not exist in field ' + this.cn;
	if(fun == null)
		throw 'Function fun must be provided';
	if(Object.prototype.toString.call(fun) != '[object Function]')
		throw 'Function fun is not callable function';

	var oldFun = this[functionName];
	this[functionName] = function() {
		oldFun.apply(this, arguments);
		fun.apply(this, arguments);
	}
	this.registeredPostHooks[functionName] = true;
}

Field.prototype.isHooked = function(functionName) {
	var _this = this;
	return {
		pre: this.registeredPreHooks != null && this.registeredPreHooks[functionName] === true,
		post: this.registeredPostHooks != null && this.registeredPostHooks[functionName] === true
	}
}

/* sta�e */
Field.Messages = { FIELD_REQUIRED: DWR.MSG.FIELD_REQUIRED };
















