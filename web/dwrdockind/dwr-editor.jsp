<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ul id="menu" style="display: none;">
	<li>Plik
		<ul>
			<li id="menuFileNew">Nowy</li>
			<li id="menuFileOpen">Otw�rz</li>
			<li id="menuFileSave">Zapisz w DocuSafe</li>
			<li id="menuFileSaveAs">Zapisz jako plik XML</li>
			<li id="menuFileInfo">Informacje</li>
			<li class="sep sepDockindList"></li>
			<li class="sep"></li>
			<li id="menuFileClose" data-url="<ww:url value="'/'" />">Zako�cz i wr�� do DocuSafe</li>
		</ul>
	</li>
	
	<li>Edycja
		<ul>
			<!-- <li id="menuEditUndo">Cofnij</li>  -->
			<li class="expandable">Wstaw
				<ul>
					<li id="menuEditInsertRow">Wiersz</li>
					<!-- <li id="menuEditInsertColumn">Kolumn�</li> //-->
				</ul>
			</li>
			<li class="sep"></li>
			<li id="menuEditDelete" class="dsb-menu-item-disabled" itemdisabled="true">Usu�
				<span class="dsb-menu-item-shortcut">Delete</span></li>
		</ul>
	</li>
	
	<li>Widok
		<ul>
			<li class="dsb-menu-item-disabled" id="menuViewRefresh" itemdisabled="true" itemchecked="false">
				<span class="dsb-menu-checkbox check-mini"></span>Od�wie�</li>
			<li class="dsb-menu-item-disabled" id="menuViewXmlEditor" itemchecked="false" itemdisabled="true">
				<span class="dsb-menu-checkbox check-mini"></span>Kod XML</li>
			<li id="menuViewHiddenFields" itemchecked="false">
				<span class="dsb-menu-checkbox check-mini"></span>Ukryte pola</li>
		</ul>
	</li>
	<li>Pomoc
		<ul>
			<li id="menuHelpShowWelcomeScreen">Poka� ekran startowy</li>
		</ul>
	</li>
	<!-- 
	<li>Narz�dzia
		<ul>
			<li id="menuToolsOptions">Opcje</li>		
		</ul>
	</li>
	 -->
</ul>

<div id="dsb-dockind-dialog" title="Informacje">
	<div id="dsb-dockind-dialog-title"></div>
	<div id="dsb-dockind-dialog-content">
		<label for="dsb-dockind-dialog-name">Nazwa dla u�ytkownika</label>
		<input type="text" id="dsb-dockind-dialog-name" />

		<label for="dsb-dockind-dialog-cn">Nazwa kodowa</label>
		<input type="text" id="dsb-dockind-dialog-cn" />
		 
		<label for="dsb-dockind-dialog-tablename">Tabela w bazie</label>
		<input type="text" id="dsb-dockind-dialog-tablename" />

		<label for="dsb-dockind-dialog-updatedb">Zapisz w bazie danych</label>
        <input type="checkbox" class="styled-disabled dontFixWidth" id="dsb-dockind-dialog-updatedb" />
	</div>
</div>

<div id="dsb-welcome-dialog" title="Edytor rodzaj�w dokument�w">
	<div>Aby powr�ci� do systemu DocuSafe wybierz "Plik/Zako�cz i wr�� do DocuSafe".</div>
	<div id="dsb-welcome-dialog-hide-container">
		<input type="checkbox" id="dsb-welcome-dialog-hide" class="styled-disabled dontFixWidth" />
		<label for="dsb-welcome-dialog-hide">Nie pokazuj wi�cej tego okna</label>
	</div>
</div>

<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbUtils.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript">
//var fieldCollector = new DsbUtils.FieldCollector();

try {
	DwrTest.getFieldsList = function() {}
	DwrText.getFieldsValues = function() {}
} catch(e) {}

</script>

<form class="dwr">
<jsp:include page="./dwr-document-base.jsp"/>
</form>

<script type="text/javascript" src="<ww:url value="'/dwrdockind/jquery.fileupload.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/jquery.revertablecss.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/jwerty.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbObserver.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbXmlDocument.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbField.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbDictionaryField.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbSchemaDocument.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbDockind.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbEditorSettings.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbMenu.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbNotification.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbXmlEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbPropertiesEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbEnumEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbProcessEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbActivitiProcessEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbDockindDialog.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbWelcomeDialog.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbEditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/jquery.clickmenu.pack.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
 
<script type="text/javascript">

var webkit = $j.browser.webkit === true;

/**
 * TODO - rozje�d�aj� si� s�owniki wielowarto�ciowe w dwr,
 * a je�li tego nie ma, to w edytorze dubluj� si� fieldsety w s�ownikach
 */
BaseField.createOrAssignFieldset = function(field) {
	var domFieldset = null;
	if($j('#' + field.jField.fieldset).length < 1 && field.jqColumn.find('fieldset[id=' + field.jField.fieldset + ']').length < 1) {
		field.jqColumn.append(field.jqFieldset = BaseField.createFieldset(field));
		domFieldset = document.createElement('fieldset');
		domFieldset.id = field.jField.fieldset;
//		console.info('tworzymy fieldset', field.jqColumn, field.jqColumn[0]);
	} else {
//		console.info('dodajemy do fieldseta', field.jField.cn, field.jqColumn.find('fieldset[id=' + field.jField.fieldset + ']')[0]);
		field.jqFieldset = field.jqColumn.find('fieldset[id=' + field.jField.fieldset + ']')[0];
		field.jqFieldset = $j(field.jqFieldset);
		/*if($j('#' + field.jField.fieldset))
			field.jqFieldset = $j('#' + field.jField.fieldset);
		else
			field.jqFieldset = $j(field.jqColumn.find('fieldset[id=' + field.jField.fieldset + ']')[0]);*/
	}
}

var __orgUpdateFields = FieldsManager.prototype.updateFields; 

FieldsManager.prototype.updateFields = function(jFields, options) {
	var opts = options;
	if(opts == null)
		opts = {};
	opts.createHiddenFields = true;
	
	__orgUpdateFields.call(this, jFields, opts);
}


	$j('#column0, #column1, #column2').remove();
	var editor = new DsbEditor($j('#request_pageContext').val());
	editor.init();
</script>

<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/editor.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/clickmenu.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/editor-webkit.css'" />" 
	media="screen and (-webkit-min-device-pixel-ratio:0)"   />

<form action="<ww:url value="'/dwrdockind/dwr-editor.action'"/>" method="post" enctype="multipart/form-data">
	<input id="pingBackXML" accept="text/xml" type="hidden" name="pingBackXML" value="false" />
	<ww:file name="'xml'" id="file" cssStyle="'position: absolute; left: -200px; top: -200px;'" />
	
</form>
