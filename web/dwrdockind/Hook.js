var Hook = function(obj) {
	if(obj == null)
		throw 'Object is required';

	var registeredPreHooks = {},
		registeredPostHooks = {};

	var validateHookParams = function(functionName, fun) {
		if(functionName == null) 
			throw 'Function name is required';
		if(obj.prototype[functionName] == null)
			throw 'Function ' + functionName + ' does not exist in ' + obj.name;
		if(fun == null)
			throw 'Function fun must be provided';
		if(Object.prototype.toString.call(fun) != '[object Function]')
			throw 'Function fun is not callable function';
	}

	return {
		addPreHook: function(functionName, fun) {
			validateHookParams(functionName, fun);
			throw 'Not implemented';
		},

		addPostHook: function(functionName, fun, options) {
			validateHookParams(functionName, fun);

			var hooks = null;
			if(registeredPostHooks[functionName] == null) {
				hooks = registeredPostHooks[functionName] = [{
					fun: obj.prototype[functionName],
					options: null
				}];
				obj.prototype[functionName] = function() {
					var i = 0, len = hooks.length;

					for(i = 0; i < len; i ++) {
						var self = hooks[i].options && hooks[i].options.self ? hooks[i].options.self : this;
						if(hooks[i].options && hooks[i].options.params) {
							hooks[i].fun.apply(self, hooks[i].options.params);
						} else {
							hooks[i].fun.apply(self, arguments);
						}
					}
				}
			} else {
				hooks = registeredPostHooks[functionName];
			}
			hooks.push({
				fun: fun,
				options: options || null
			});

			return hooks.length;
		},

		getPostHooks: function() {
			return registeredPostHooks;
		},

		isPostHookPresent: function(functionName, fun) {
			validateHookParams(functionName, fun);
			if(registeredPostHooks[functionName] == null)
				return false;
			
			return _.some(registeredPostHooks[functionName], function(val, idx, arr) {
				return val.fun === this;
			}, fun);
		}
	}

}