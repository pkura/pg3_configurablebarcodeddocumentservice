﻿/**
 * @projectDescription DWR Dockind Fields
 * @author Kamil Omelańczuk kamil.omelanczuk@docusafe.pl
 * @version 1.0
 * @namespace pl.compan.dwr
 */

/**
 * Procedury pomocnicze do obsługi słowników wielowartościowych.
 * @classDescription Tworzy nową instancję MultipleFieldsHelper. 
 * @author Kamil Omelańczuk
 * @version 1.0
 * @constructor
 * @param {Object} dictionaryFieldsManager Obiekt <code>FieldsManager</code> używany do wyciągnięcia
 * grupy pól wielowartościowych
 */
MultipleFieldsHelper = function MultipleFieldsHelper(dictionaryFieldsManager) {
	var callCounter = 0;
	var dfm = dictionaryFieldsManager;
	var fieldNumber = Number(1);
	var buttons = []; // tablica przycisków
	var newCn;	// nowy CN uwzględniający indeks pola
	var ommitField = false;
	var switcherOptions = {
		position: dictionaryFieldsManager.jqDictionaryFieldset.hasClass('dwr-small-new-button-dic') ? 'inside label' : 'bottom' 
	};
	
	// priviledged
	this.name = dfm.jDictionaryField.cn;
	this.getFieldNumber = function() { return fieldNumber; }
	this.setFieldNumber = function(num) { fieldNumber = num; }
	
	this.getButtons = function() { return buttons; }
	this.setButtons = function(btns) { buttons = btns; }
	this.deleteButton = function(num) { buttons[num] = undefined; }
	this.fieldPropertiesCache = {};
	this.paintQueue = [];
	this.paintCached = false;
	this.pure = dfm.jDictionaryField.dictionary.pure === true;
	
	/**
	 * Aktualizuje widoczność przycisków zgodnie z mapą availDictionaryButtons
	 */
	this.refreshButtons = function() {
		var availButtons = dfm.dictionaryField.availDictionaryButtons,
			i = 0, len = buttons.length, btn = null;
		if(availButtons.doAdd === true) {
			this.jqAddNext.removeClass('force_hidden').show();
		} else {
			this.jqAddNext.addClass('force_hidden').hide();
		}
		
		for(i = 0; i < len; i ++) {
			btn = buttons[i];
			if(btn == null)
				continue;
			try {
				if(availButtons.doRemove === true) {
					btn.deleteDictionaryItem.removeClass('force_hidden');
				} else {
					btn.deleteDictionaryItem.addClass('force_hidden');
				}
				/*if(availButtons.doClear === true) {
					btn.clearDictionary.removeClass('force_hidden');
				} else {
					btn.clearDictionary.addClass('force_hidden');
				}*/
				if(availButtons.showPopup === true) {
					btn.showDictionary.removeClass('force_hidden');
				} else {
					btn.showDictionary.addClass('force_hidden');
				}
			} catch(e) {
				console.error(e);
			}
		}
	}
	
	/**
	 * Aktualizuje nagłówek zgodnie z elementami w visibleFieldsCns 
	 */
	this.refreshTableHeader = function() {
		var cns = dfm.dictionaryField.jField.dictionary.visibleFieldsCns,
			jqThead = $j('#dwrDictionaryTable_' + this.name).children('thead'),
			i = 0, len = 0, cn = null, str = '', shortCn = null, headers = {};
		
		jqThead.html('');
		for(cn in dfm.fields) {
			if(dfm.fields[cn].jField.hidden == false) {
				shortCn = cn.replace(/_\d+$/i, '');
				if(headers[shortCn] !== true) {
					headers[shortCn] = true;
					str += '<th class="' + cn + '">' + dfm.fields[cn].jField.label + '</th>';
				}
			} 
		}
		str += '<th></th>';
		jqThead.html(str);
	}
	
	this.createMultipleFields = function(vals) {
		var multiVals = vals || dfm.dictionaryField.multipleFieldsValues,
			newFields = [], even = false;
			//if(multiVals == '549')
			  //  debugger;
		var newField, newjField,
			i, j, k, cn, len, lastMultipleFieldsNumber,
			jqDicButtonsContainer, // jQuery div zawierających sklonowane przyciski;
			groupedFields,	// pola o tym samym indeksie multipleFieldsNumber
			firstFieldInGroup,	// pierwsze pole w danej grupie
			allAtOnceIds = [],	// id wszystkich wpisów w słowniku
			fieldCn, iterField, jqTable, newCn, oldMultiNum;
		var refresh = dfm.jDictionaryField.dictionary.refreshMultipleDictionary === true,
			autoclean = dfm.jDictionaryField.dictionary.autocleanEmptyItems === true,
			dictionaryIdTemplate = dfm.jDictionaryField.dictionary.name + '_ID_',
			redundantMultipleFieldsNumbers = null, omitFirstRow = false, irremovableFieldNumber = null, 
			iterFieldNumber = null;
		var singletonCal = dfm.dictionaryField.jField.dictionary.tableView === true;
		var paintCached = this.paintCached, paintQueue = this.paintQueue;
		var _this = this;
		
		// console.warn('createMultipleFields');
		// console.warn('cn', dfm.jDictionaryField.cn, 'callCounter', callCounter, 'refresh', refresh);
		// console.warn('multiVals', multiVals.toString());
		
		function setItemValue(data, fieldNumber) {
			var dataCn, fieldCn;
			var fieldData;
			// dataCn jest w postaci bez podkreślnika i indeksu: dsg_moj_slownik_lastname
			for(dataCn in data) {
				fieldCn = dataCn + '_' + fieldNumber;

				if(dfm.fields[fieldCn] != null) {
					dfm.fields[fieldCn].setValue(data[dataCn]);
				} else {
					// próbujemy wyłuskać id wpisu, które może być w postaci (różna wielkość liter)
					// dsg_moj_slownik_ID_3
					// TODO: Trzeba to uporządkować w Javie
					fieldCn = dfm.jDictionaryField.dictionary.name + '_' + fieldCn.replace(/.*_(?=id_\d+)/i, '').toUpperCase();
					if(dfm.fields[fieldCn] != null) {
						dfm.fields[fieldCn].setValue(data[dataCn]);
					} else {
						console.warn('Cannot find field', fieldCn, 'with data cn', dataCn);
					}
				}

			}
		}

		function fixDisplay() {
		    if(ie7 === true) {
                var verticalLabel = !dfm.dictionaryField.jqFieldContainer.hasClass('dwr-horizontal-dic-title') && dfm.dictionaryField.jField.dictionary.tableView === true;
                if(verticalLabel) {
                    $j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn).css('margin-top', (-dfm.dictionaryField.jqLabel.height() + 20) + 'px');
                }
            }
		}
		
		function beginPaint() {
			if(dfm.jDictionaryField.dictionary.loadingMode != 'ALL_AT_ONCE') {
				return;
			}
			paintQueue = [];
			paintCached = true;
		}
		
		function endPaint() {
			if(dfm.jDictionaryField.dictionary.loadingMode != 'ALL_AT_ONCE' || paintQueue.length < 1) {
				paintCached = false;
				return;
			}
			var jqTable = $j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn + ' tbody');
				rows = document.createDocumentFragment(),
				i, len = paintQueue.length;
				
			for(i = 0; i < len; i ++) {
				if(paintQueue[i] != null) {
					rows.appendChild(paintQueue[i][0]);
				}
			}
			
			jqTable[0].appendChild(rows);
			
			paintCached = false;

            fixDisplay();
		}
		
		// jeśli mamy tylko jedno pole wielowartościowe wczytane z xml-a,
		// to jest to pierwsze wywołanie funkcji
		if(callCounter == 0) {
			groupedFields = {};
			firstFieldInGroup = null;
			for(cn in dfm.fields) {
				if(dfm.jDictionaryField.disabled === true)
					dfm.fields[cn].jField.disabled = true;
				dfm.fields[cn].multipleFieldsNumber = 1;
				if(firstFieldInGroup == null)
					firstFieldInGroup = dfm.fields[cn];
				groupedFields[cn] = dfm.fields[cn];
			}
			newField = dfm.fields[cn];
			try {
				newField.jqFieldContainer.addClass('groupSeparator');
			} catch(e) {
				console.error(e);
			}
			dfm.jqAddNext = this.jqAddNext = MultipleFieldsHelper.createMultipleSwitcher(newField, dfm, switcherOptions);
			
			if(dfm.dictionaryField.jField.dictionary.autohideMultipleDictionary === true) {
				dfm.jqAddNext.bind('click.toggleTableVisibility', function(event) {
					if(dfm.multipleFieldsHelper.isTableCollapsed() == false) {
						event.stopImmediatePropagation();
						
						dfm.multipleFieldsHelper.toggleTable(true);
						return false;
					}
				});
			}
			dfm.jqAddNext.bind('click', function() {
				fieldNumber = dfm.multipleFieldsHelper.addNext.call(dfm, fieldNumber, 
					{pure: dfm.dictionaryField.jField.dictionary.pure === true});
				return false; // przerywamy kolejkę obsługi zdarzeń
			});
			
			buttons[firstFieldInGroup.multipleFieldsNumber] = MultipleFieldsHelper.createDictionaryButtons.call(dfm, groupedFields, firstFieldInGroup.multipleFieldsNumber);
			buttons[firstFieldInGroup.multipleFieldsNumber].container.insertBefore(firstFieldInGroup.jqFieldContainer).addClass('MULTIPLE_DICTIONARY_FIRST_ELEMENT');
			
			// ukrywamy oryginalne przyciski
			(function() { // this = dictionaryFieldsManager
				var cn;
				for(cn in this.dictionaryField.dictionaryButtons) {
					this.dictionaryField.dictionaryButtons[cn].hide();
				}
			}).call(dfm);
			
			// ukrywamy przycisk "usuń element"
			//buttons[firstFieldInGroup.multipleFieldsNumber].deleteDictionaryItem.hide();
			
			if(dfm.dictionaryField.jField.dictionary.tableView === true && dfm.dictionaryField.jField.coords 
					&& multiVals.length < 1) {
				this.drawAsTable(groupedFields, {helper: this, switcherOptions: switcherOptions, 
					pure: dfm.dictionaryField.jField.dictionary.pure === true});
			}
			if(switcherOptions.position == 'bottom') {
				dfm.jqAddNext.parent().insertAfter($j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn));
			}
			fixDisplay();
		} 
		// każde następne wywołanie 
		// po odebraniu wartości dla słownika
		else if(refresh || callCounter == 1) {
			allAtOnceIds = [];
			// znajdujemy niepotrzebne pola
			redundantMultipleFieldsNumbers = [];
			existingMultipleFieldsNumbers = [];
			
			for(cn in dfm.fields) {
				iterField = dfm.fields[cn];
				// console.warn('searching for', dictionaryIdTemplate);
				// szukamy pola oznaczającego id pojedynczego wpisu w słowniku
				fieldCn = cn.replace(/_\d+/, '_');
				
				if(fieldCn == dictionaryIdTemplate) {
					// znaleźliśmy pole, które jest ID
					// console.info('FOUND:', cn, iterField.getSimpleValue(), multiVals);
					// pole możemy ewentualnie usunąć, tylko wtedy gdy jego wartość id nie jest pusta
					try {
					if((iterField.getSimpleValue() != '' || autoclean === true) && $j.inArray(Number(iterField.getSimpleValue()), multiVals) == -1) {
						// wartości id nie ma w liście multiVals, czyli usuwamy wiersz
						// console.info('deleting field', cn);
						redundantMultipleFieldsNumbers.push(iterField.multipleFieldsNumber);
					}
					existingMultipleFieldsNumbers.push(iterField.multipleFieldsNumber);
					} catch(e) { console.error(e) }
				}
			}
			// console.warn(dfm.dictionaryField.jField.cn, 'fields to remove', redundantMultipleFieldsNumbers);
//			console.trace();
			
			redundantMultipleFieldsNumbers.sort(function(a, b) { return a-b; });
			existingMultipleFieldsNumbers.sort(function(a, b) { return a-b; });

			console.assert(redundantMultipleFieldsNumbers.length <= existingMultipleFieldsNumbers.length, 'More reduntant than existing fields');
			if(redundantMultipleFieldsNumbers.length == existingMultipleFieldsNumbers.length) {
				irremovableFieldNumber = existingMultipleFieldsNumbers[0];
			}
			
			omitFirstRow = false;
//			console.info('redundant', dfm.jDictionaryField.dictionary.name, redundantMultipleFieldsNumbers + ' ', omitFirstRow, multiVals + ' ');
//			console.info('existing ', dfm.jDictionaryField.dictionary.name, existingMultipleFieldsNumbers + ' ');
			
			if(redundantMultipleFieldsNumbers.length > 0) {
				deleteFieldsIterator:
				for(cn in dfm.fields) {
					// console.warn('checking field', cn);
					iterFieldNumber = Number(dfm.fields[cn].multipleFieldsNumber);
					console.assert(isNaN(iterFieldNumber) != true, 'Multiple field number for ' + cn + ' is not a number: ' + dfm.fields[cn].multipleFieldsNumber);
					if($j.inArray(iterFieldNumber, redundantMultipleFieldsNumbers) != -1) {
						if(iterFieldNumber === irremovableFieldNumber) {
							// console.warn('clearing field', cn);
							dfm.fields[cn].clearValue();
						} else {
							// console.warn('deleting field', cn);
							dfm.fields[cn].remove();
							delete dfm.fields[cn];
						}
						continue deleteFieldsIterator;
					}
				}
			}

			// usuwamy ikonki i wiersz
			jqTable = $j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn);
//			console.info(jqTable);
			iterField = null;
			for(i = 0; i < redundantMultipleFieldsNumbers.length; i ++) {
				iterField = redundantMultipleFieldsNumbers[i];
				if(iterField != irremovableFieldNumber) {
					dfm.multipleFieldsHelper.getButtons()[iterField] = undefined;
					// console.warn('removing row', iterField);
					jqTable.find('tr.multipleFieldsNumber_' + iterField).remove();
				}
			}
			// szukamy numeru ostatniego wiersza
			lastMultipleFieldsNumber = null;
			for(cn in dfm.fields) {
				iterField = dfm.fields[cn];
				if(lastMultipleFieldsNumber == null || iterField.multipleFieldsNumber > lastMultipleFieldsNumber) {
					lastMultipleFieldsNumber = iterField.multipleFieldsNumber;
				}
			}
			dfm.multipleFieldsHelper.setFieldNumber(Number(lastMultipleFieldsNumber));
			
			// numerujemy pola od nowa
			if(redundantMultipleFieldsNumbers.length) {
				groupedFields = [];
				for(cn in dfm.fields) {
					iterField = dfm.fields[cn];
					if(groupedFields[iterField.multipleFieldsNumber] == null)
						groupedFields[iterField.multipleFieldsNumber] = [];
					
					groupedFields[iterField.multipleFieldsNumber].push(iterField);
				}
				// i oznacza numer przed usunięciem
				// j numer prawidłowy (po usunięciu)
				j = 1;
				for(i = 1; i < groupedFields.length; i ++) {
					if(groupedFields[i] == null) {
						// jeśli brakuje pola na ktorejś pozycji, to nie zwiększamy
						// prawidłowego numeru
//						console.warn('missing field #', i);
						continue;
					}
					for(k = 0; k < groupedFields[i].length; k ++) {
						oldMultiNum = Number(groupedFields[i][k].multipleFieldsNumber);
						groupedFields[i][k].multipleFieldsNumber = j;
						
						cn = groupedFields[i][k].jField.cn;
						newCn = cn.replace(/_\d+/, '_' + j);
						
						// tylko raz dla jednej grupy pól
						if(k == 0) {
							try {
								MultipleFieldsHelper.updateButtons(dfm.multipleFieldsHelper.getButtons(), oldMultiNum, j);
							} catch(e) {
								console.error(e);
//								console.warn(dfm.multipleFieldsHelper.getButtons());
//								console.warn(oldMultiNum);
//								console.warn(j);
//								console.error(e);
							}
						}
						
						if(newCn != cn) {
//							console.info('zmieniamy', cn, 'na', newCn);
							dfm.fields[newCn] = dfm.fields[cn];
							dfm.fields[newCn].jField.cn = newCn;
							delete dfm.fields[cn];
							MultipleFieldsHelper.refreshField(dfm.fields[newCn],
								{oldCn: cn, newCn: newCn}); 
						}
					}
					j ++;
				}
				dfm.multipleFieldsHelper.setFieldNumber(j-1);
//				console.warn('TUTAJ', j-1, groupedFields);
				groupedFields = null;
			}
//			console.trace();
			// Jeśli mamy więcej niż jedno pole, to musimy resztę pól stworzyć "ręcznie"
			if(multiVals.length > 1) {
				multiValsIterator:
				for(i = 1, len = multiVals.length; i < len; i ++) {
					even = !(i % 2 == 0);
					for(cn in dfm.fields) { // tutaj iterujemy tylko po polu z xml-a
						newCn = cn.replace('_1', '_' + (i+1));
						if(refresh && dfm.fields[newCn]) {
							continue multiValsIterator;
						} 
						dfm.fields[cn].multipleFieldsNumber = 1;
						newjField = dfm.fields[cn].clonejField();
						newjField.cn = newCn;
						if(this.pure === true) 
							newjField.pure = true;
						newField = new Field(newjField, 0, {singletonCal: singletonCal});
						newField.multipleFieldsNumber = i + 1;
						
						newField.jqColumn = dfm.fields[cn].jqColumn;
						newField.isSearchable = dfm.fields[cn].jqColumn.isSearchable;
						if(newField.parentFieldsManager == null)
							newField.parentFieldsManager = dfm;
						newField.create();
						if(even)
							newField.jqFieldContainer.addClass('even');
									
						newFields.push(newField);
						dfm.registerFieldListener(newField);
					}
					newField.jqFieldContainer.addClass('groupSeparator');
				}
			
				// przesuwamy guzik "dodaj następny" poniżej utworzonych pól
				if(newField != null && switcherOptions.position == 'bottom')
					newField.jqFieldContainer.after(dfm.jqAddNext);
				
				// uaktualniamy indeks ostatniego pola
				fieldNumber = i;
				
				// dodajemy pola do fieldsManagera
				for(i = 0, len = newFields.length; i < len; i ++) {
					dfm.fields[newFields[i].jField.cn] = newFields[i];
				}
				
				// pokazujemy przycisk "usuń element" dla pierwszego elementu
				/* TODO - uporządkować przyciski */
				if(buttons != null && buttons[1] != null && buttons[1].deleteDictionaryItem != null) {
					buttons[1].deleteDictionaryItem.show();
				}
			}
			
			// wczytujemy wartości pól
//			console.time('drawAsTable');
			beginPaint();
			for(i = 0, len = multiVals.length; i < len; i ++) {
				groupedFields = {};
				firstFieldInGroup = null;
				for(cn in dfm.fields) {
					if(dfm.fields[cn].multipleFieldsNumber == i+1) {
						groupedFields[cn] = dfm.fields[cn];
						if(firstFieldInGroup == null)
							firstFieldInGroup = groupedFields[cn];
					}
				}

				switch(dfm.jDictionaryField.dictionary.loadingMode) {
				case 'ALL_AT_ONCE':
					allAtOnceIds.push(multiVals[i]);
				break;
				default:
					(function(itemId, fieldNumber) { // this = dictionaryFieldsManager
						var _this = this;
						var guid = FieldsManager.Blocker.getNextHookGuid();
						Dictionary.getFieldsValues(this.jDictionaryField.dictionary.name, itemId, {callback: function(data) {
								// console.info('Dictionary.getFieldsValues', data);
								// console.info(data);
								//window.__global_start = window.__global_start ? window.__global_start : new Date();
								setItemValue(data, fieldNumber);
								//console.info((new Date()).getTime() - window.__global_start.getTime());
							},
							preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getFieldsValues', guid: guid}),
							postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getFieldsValues', guid: guid})
						});
					}).call(dfm, multiVals[i], i+1);
				break;
				}
				
				// kopiujemy przyciski pokaż słownik, wyczyść pola i etykietę "nowy wpis"
				// bez klonowania eventów, bo i tak przypisujemy nowe
				// dla pierwszego pola wielowartościowego przyciski zostały utworzone już przy pierwszym
				// wywołaniu funkcji (callCounter == 0), więc nie tworzymy duplikatów
				/* TODO - znów przyciski */
				try {
					if(i != 0 && !buttons[firstFieldInGroup.multipleFieldsNumber]) {					
						buttons[firstFieldInGroup.multipleFieldsNumber] = MultipleFieldsHelper.createDictionaryButtons.call(dfm, groupedFields, firstFieldInGroup.multipleFieldsNumber);
						buttons[firstFieldInGroup.multipleFieldsNumber].container.insertBefore(firstFieldInGroup.jqFieldContainer);
					}
				} catch(e) {
					console.error(e);
				}
				if(dfm.dictionaryField.jField.dictionary.tableView === true) {
					this.drawAsTable(groupedFields, {helper: this, switcherOptions: switcherOptions});
				}
			}
			endPaint();
			if(switcherOptions.position == 'bottom' && dfm.jDictionaryField.dictionary.pure !== true) {
				dfm.jqAddNext.parent().insertAfter($j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn));
			}
//			console.timeEnd('drawAsTable');
			
			switch(dfm.jDictionaryField.dictionary.loadingMode) {
			case 'ALL_AT_ONCE':
				var guid = FieldsManager.Blocker.getNextHookGuid();
				// console.info('all at once load', dfm.jDictionaryField.dictionary.name);
				// console.trace();

				// console.info('Wait for it...');
				// $j('table.grid').dblclick($j.proxy(function() {
					// console.info('Fired!');
					Dictionary.getFieldsValuesList(dfm.jDictionaryField.dictionary.name, allAtOnceIds, {callback: function(data) {
							// console.info('Dictionary.getFieldsValuesList', dfm.jDictionaryField.dictionary.name, allAtOnceIds, data);
							// console.trace();
						    // console.info(dfm.jDictionaryField.dictionary.name);
						    // console.warn(data);
						    // console.profile(dfm.jDictionaryField.dictionary.name);
							var i, len;
							if(data != null) {
								for(i = 0, len = data.length; i < len; i ++) {
									setItemValue(data[i], i+1);
								}
								_this.setValuesComplete(data);
							}
						    // console.profileEnd(dfm.jDictionaryField.dictionary.name);
						    // window.__end = (new Date()).getTime();
						    // $j('#ul_left_ie').before('<span>' + (window.__end - window.__start) + '&nbsp;&nbsp</span>');

						    if(dfm.jDictionaryField.dictionary.pure === true) {
						    	// console.warn('ADD NEXT move');
						    	$j('#dwrDictionaryTable_' + dfm.jDictionaryField.cn).after(dfm.jqAddNext);
						    }
						},
						preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getFieldsValuesList', guid: guid}),
						postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getFieldsValuesList', guid: guid})
					});
				// }, this));				
			break;
			default:
				// nop
			}
			
			if(dfm.dictionaryField.jField.dictionary.tableView === true && !dfm.dictionaryField.jField.coords) {
				this.renderFields(groupedFields, {helper: this, asTable: true});
			}
		}

		
		callCounter ++; 

	} // end of function this.createMultipleFields

}

/**
 * Przerysowuje grupę pól wielowartościowych.
 * @deprecated
 * @method
 */
MultipleFieldsHelper.prototype.renderFields = function(groupedFields, options) {
	var cn, field, jqCell, jqRow, jqTable = null, jqThead;
	var i = 0, createHeader = false, groupedFields = groupedFields;
	if(options && options.asTable === true) {		
		for(cn in groupedFields) {
			field = groupedFields[cn];
			if(field.transformedToTable === true)
				continue;
			if(field.jField.hidden === true )
				continue;
			
			jqTable = $j('#dwrDictionaryTable_' + this.name);
			if(jqTable.length < 1) {
				jqTable = $j('<table id="dwrDictionaryTable_' + this.name + '"/>');
				jqTable.addClass('dictionary-table');
				field.jqColumn.append(jqTable);
				createHeader = true;
				jqThead = $j('<thead>');
				jqTable.append(jqThead);
			}
			if(createHeader && !options.dontCreateHeader) {
				jqThead.append('<th>' + field.jField.label + '</th>');
			}
			if(i == 0) {
				jqRow = jqTable.find('tr.multipleFieldsNumber_' + field.multipleFieldsNumber);
				if(jqRow.length < 1) {
					jqRow = $j('<tr/>');
					jqRow.addClass('multipleFieldsNumber_' + field.multipleFieldsNumber);
				}
				var jqBtnCell = $j('<td/>');				
			}
			
			jqCell = jqRow.find('td.' + field.jField.cn);
			if(jqCell.length < 1) {
				jqCell = $j('<td/>');
				jqCell.addClass(field.jField.cn);
			}
			
			field.jqLabel.hide();
			field.jqFieldContainer.hide();
			jqCell.append(field.jqFieldContainer);
			jqCell.append('<span/>');
			field.jqCell = jqCell;
			field.jqCellSpan = jqCell.find('span');
			
			(function() {
				var setValueForTable = function(val, params) {
					Field.prototype.setValue.call(this, val, params);
					var strVal = this.getSimpleValue({selectedOptionName: true, formatted: true});
					if(!strVal) {
						strVal = 'puste';
						this.jqCellSpan.addClass('empty');
					} else {
						this.jqCellSpan.removeClass('empty');
					}
					this.jqCellSpan.html(strVal);
				}
				setValueForTable.prototype = Field.prototype.setValue.prototype;
				field.setValue = setValueForTable;
				
				var clearValueForTable = function() {
					Field.prototype.clearValue.call(this);
					var strVal = this.getSimpleValue({selectedOptionName: true, formatted: true});
					if(!strVal) {
						strVal = 'puste';
						this.jqCellSpan.addClass('empty');
					} else {
						this.jqCellSpan.removeClass('empty');
					}
					this.jqCellSpan.html(strVal);
				}
				clearValueForTable.prototype = Field.prototype.clearValue.prototype;
				field.clearValue = clearValueForTable;
			})();
			
			if(!field.jField.disabled) {
				(function(field) {
					field.jqCell.bind('click.showedit', function() {
						field.jqFieldContainer.css('width', field.jqCell.width()).css('height', field.jqCell.height());
						field.jqFieldContainer.show();
						field.jqCellSpan.hide();
						field.jqVisibleField.focus();
					});
					field.jqVisibleField.bind('blur.hideedit', function() {
						field.jqFieldContainer.hide();
						field.jqCellSpan
							.html(field.getSimpleValue({selectedOptionName: true, formatted: true}))
							.show();
					});
				})(field);
			}
			
			jqRow.append(jqCell);
			
			if(i == 0)
				jqTable.append(jqRow);
			field.transformedToTable = true;
			
			(function() {
				var jqItemRow = jqRow;
				options.helper.getButtons()[field.multipleFieldsNumber].deleteDictionaryItem.unbind('click.deleterow')
					.bind('click.deleterow', function() {
						jqItemRow.remove();
					});
			})();
			
			i ++;
			
		}
		var jqButtonsCell = $j('<td/>');
		jqButtonsCell.append(options.helper.getButtons()[field.multipleFieldsNumber].container);
		jqRow.append(jqButtonsCell);
		if(!options.dontCreateHeader && jqThead)
			jqThead.append('<th>&nbsp;</th>');
		options.helper.jqAddNext.parent().insertAfter(jqTable);
		
		(function() {
			var cn;
			var helper = options.helper;
			var addNextForTable = function(lastMultipleFieldNumber) {				
				var newFieldNumber = MultipleFieldsHelper.prototype.addNext.call(this, lastMultipleFieldNumber);
				this.multipleFieldsHelper.setFieldNumber(3);
				
				var groupedFields = MultipleFieldsHelper.findLastGroupedFields(this.fields, {asMap: true});
				
				this.multipleFieldsHelper.renderFields(groupedFields, {helper: this.multipleFieldsHelper, asTable: true, dontCreateHeader: true});
				for(cn in groupedFields) {
					groupedFields[cn].clearValue();
				}
				
				return newFieldNumber;
			}
			addNextForTable.prototype = MultipleFieldsHelper.prototype.addNext.prototype;
			options.helper.addNext = addNextForTable;
		})();
	}
}

/**
 * Tworzy przycisk (a href) "dodaj następny element" - umieszczony po polu <code>afterField</code>. <br />
 * NIE przypisuje żadnego eventu do utworzonego elementu.
 * @method
 * @param {Object} afterField Obiekt typu <code>Field</code>, czyli mający pole <code>jqFieldContainer</code>, pod którym
 * zostanie utworzony przycisk
 * @return {Object} Obiekt jQuery zawierający przycisk
 */
MultipleFieldsHelper.createMultipleSwitcher = function(afterField, dictionaryFieldsManager, options) {
	var opts = {
		position: options ? options.position : 'bottom'
	}
	var jqAfterField = afterField.jqFieldContainer, dfm = dictionaryFieldsManager;
	var jqSwitch = $j('<div/>').addClass('field').append('<a href="#" class="addItem">' + DWR.MSG.DIC_ADD_NEXT + '</a>'),
		addNextVisible = dfm.dictionaryField.availDictionaryButtons.doAdd === true;
	
	switch(opts.position) {
		case 'inside label':
			dictionaryFieldsManager.jqDictionaryFieldset.children('label').append(jqSwitch);
		break;
		case 'bottom':
		default:
			jqSwitch.insertAfter(jqAfterField).children();
	}
	
	if(dfm && dfm.jDictionaryField.disabled === true) {
		addNextVisible = false;
	}
	
	if(addNextVisible == false)
		jqSwitch.addClass('force_hidden');
	// console.info('tab', this.jqTable);
	return jqSwitch;
}

MultipleFieldsHelper.prototype.drawAsTable = function(groupedFields, options) {
	var cn, field, jqCell, jqRow, jqTable = null, jqThead;
	var i = 0, createHeader = false, groupedFields = groupedFields;
		
	for(cn in groupedFields) {
		field = groupedFields[cn];
		if(field.transformedToTable === true)
			continue;
		/*if(field.jField.hidden === true )
			continue;*/
		
		jqTable = $j('#dwrDictionaryTable_' + this.name);
		if(jqTable.length < 1) {
			jqTable = $j('<table id="dwrDictionaryTable_' + this.name + '"/>');
			jqTable.addClass('dictionary-table-compact');
			field.jqColumn.append(jqTable);
			createHeader = true;
			jqThead = $j('<thead>');
			jqThead.append('<tr>');
			jqTable.append(jqThead);
			jqTable.on('focus', '.singleton-cal', function() {
				var jqThis = $j(this),
					timeFormat = jqThis.data('timeFormat');
				if(jqThis.data('singletonDatepicker') !== true) {
					if(timeFormat) {
						jqThis.datetimepicker({showOn: 'focus', 
							buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
							changeMonth: true, changeYear: true, showButtonPanel: true, timeFormat: timeFormat.replace(/M/g, 'm'),
							showHour: true, showMinute: true, showSecond: true,
							beforeShow: function(input, inst) {
								inst.input.data('timepickerOpened', true);
							},
							onClose: function(dateText, inst) {
								inst.input.data('timepickerOpened', false);
								inst.input.trigger('change.dateChanged');
							}
							});
					} else {
						jqThis.datepicker({showOn: 'focus', 
							buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
							changeMonth: true, changeYear: true, showButtonPanel:false,
							onSelect: function dateSelectHandler() {
								jqThis.trigger('change.dateChanged');
							}
						});
					
					}
					jqThis.data('singletonDatepicker', true);					
				}
				//jqThis.datepicker('show');
			});
			jqTable.on('click.toggleCalendar', 'div.DATE', function() {
				$j(this).find('input').focus().focus();
			});
		}
		if(createHeader) {
			jqThead.find('tr').append('<th class="' + field.jField.cn + ' ' + (field.jField.hidden === true ? 'force_hidden' : '') + '">'
					+ field.jField.label + '</th>');
		}
		if(this.paintCached === true) {
			jqRow = paintQueue[field.multipleFieldsNumber];
		} else {
			jqRow = jqTable.find('tr.multipleFieldsNumber_' + field.multipleFieldsNumber);
		}
		if(jqRow == null || jqRow.length < 1) {
			jqRow = $j('<tr/>');
			jqRow.addClass('multipleFieldsNumber_' + field.multipleFieldsNumber);
		}
		jqCell = jqRow.find('td.' + field.jField.cn);
		if(jqCell.length < 1) {
			jqCell = $j('<td/>');
			jqCell.addClass(field.jField.cn);
		}
		
//		field.jqLabel.hide(); ustawiane w css
		field.jqFieldContainer.removeClass('even');
		jqCell.append(field.jqFieldContainer);
		field.jqCell = jqCell;
		jqRow.append(jqCell);
		
		if(field.jField.hidden === true) {
			jqCell.addClass('force_hidden');
		}
		
		if(this.paintCached === true) { 
			this.paintQueue[field.multipleFieldsNumber] = jqRow;
		} else {
			jqTable.append(jqRow);
		}
		
		field.transformedToTable = true;
		field.prepareForMultipleDictionaryTable({helper: options.helper});
	}
	
	if(this.pure === true) {
		// console.info('PURE', this.pure);
	} else {
		if(options.switcherOptions.position == 'bottom') {
		options.helper.jqAddNext.parent().insertAfter(jqTable);
		}
	}
	
	if(createHeader)
		jqThead.find('tr').append('<th>&nbsp;</th>');
	
	if(jqRow) {
		var jqButtonsCell = $j('<td/>');
		/** TODO przyciski w słownikach */
		try {
			jqButtonsCell.append(options.helper.getButtons()[field.multipleFieldsNumber].container);
		} catch(e) {
			console.error(e);
		}
		jqRow.append(jqButtonsCell);
	}
	
	(function() {
		var cn;
		var switcherOptions = options.switcherOptions;
		var helper = options.helper;
		var addNextForTable = function(lastMultipleFieldNumber) {				
			var newFieldNumber = MultipleFieldsHelper.prototype.addNext.call(this, lastMultipleFieldNumber);
			this.multipleFieldsHelper.setFieldNumber(3);
			
			var groupedFields = MultipleFieldsHelper.findLastGroupedFields(this.fields, {asMap: true});
			
			this.multipleFieldsHelper.drawAsTable(groupedFields, {helper: this.multipleFieldsHelper, switcherOptions: switcherOptions,
				pure: options && options.pure === true});
			
			return newFieldNumber;
		}
		addNextForTable.prototype = MultipleFieldsHelper.prototype.addNext.prototype;
		options.helper.addNext = addNextForTable;
	})();
}

/**
 * Dodaje następną grupę elementów wielowartościowych łącznie z przyciskami słownika. <br />
 * Przesuwa przycisk "dodaj następne" poniżej utworzonych elementów. Ponadto:
 * <ul>
 * <li>Dodaje utworzone pola (obiekty <code>Field</code>) do <code>fieldsManager</code>
 * <li>Przypisuje listenery do pól
 * <li>Ustawia wartości (np.: listę opcji dla enumów)
 * <li>Czyści wartości nowo utworzonych pól
 * <li>Jeśli określono przypisuje listener pokazania okienka popup słownika 
 * do przycisku "dodaj następny" TODO Trzeba sparametryzować w Javie 
 * </ul>
 * @method
 * @param {Number} lastMultipleFieldsNumber Liczba istniejących pól wielowartościowych
 * (numer pola poczynając od 1)
 */
MultipleFieldsHelper.prototype.addNext = function(lastMultipleFieldsNumber) { // this = dictionaryFieldsManager
	var regExp = new RegExp(/_\d+$/);
	var cn, i, len;	
	var lastjFieldsGroup = [], 
		lastFieldsGroup = []; // TYLKO do odczytu
	var singletonCal = this.dictionaryField.jField.dictionary.tableView === true;
	
	// szukamy ostatniej grupy pól
	for(cn in this.fields) {
		if (this.fields[cn].multipleFieldsNumber == lastMultipleFieldsNumber) {
			lastjFieldsGroup.push(this.fields[cn].clonejField());
			lastFieldsGroup.push(this.fields[cn]);
		}
	}
	
	// powiększamy indeks o jeden
	for(i = 0, len = lastjFieldsGroup.length; i < len; i ++) {
		lastjFieldsGroup[i].cn = lastjFieldsGroup[i].cn.replace(regExp, '_' + (lastMultipleFieldsNumber+1));
	}
	
	// tworzymy pola
	(function(jFields, afterField) { // this = dictionaryFieldsManager
		var i, len, newField, value, even, buttons, switcherOptions;
		var lastField = afterField,
			firstNewField = null,
			newFields = {}, // mapa utworzonych pól
			shortCn = null,
			fieldCache = null;
		
		switcherOptions = {
			position: this.jqDictionaryFieldset.hasClass('dwr-small-new-button-dic') ? 'inside label' : 'bottom'
		}
		
		even = (lastMultipleFieldsNumber + 1) % 2 ? false : true;
		for(i = 0, len = lastjFieldsGroup.length; i < len; i ++) {
			newField = new Field(lastjFieldsGroup[i], lastjFieldsGroup[i].column, {singletonCal: singletonCal});
			newField.isSearchable = lastFieldsGroup[i].isSearchable;
			newField.create(lastField);
			newField.multipleFieldsNumber = lastMultipleFieldsNumber + 1;
			lastField = newField;
			if(even)
				newField.jqFieldContainer.addClass('even');
			
			// dodajemy do menedżera pól dla słownika i do listy utworzonych pól
			this.fields[newField.jField.cn] = newFields[newField.jField.cn] = newField;
			if(newField.parentFieldsManager == null)
				newField.parentFieldsManager = this;
			
			//ustawiamy listener
			this.registerFieldListener(newField);
			
			if(lastFieldsGroup[i].jField.pure !== true) {
			// ustawiamy wartości (potrzebne np.: dla enumów - lista opcji)
			newField.setValue(lastFieldsGroup[i].getValue({packed: false}));
			}
			
			// zerujemy zaznaczone selekty, wpisane stringi, etc
			newField.clearValue();
			shortCn = newField.jField.cn.replace(/_\d+$/i, '');
			fieldCache = this.multipleFieldsHelper.fieldPropertiesCache[shortCn];
			if(fieldCache != null && fieldCache.defaultValue != null) {
//				console.warn('cache hit for', shortCn, fieldCache.defaultValue[0], fieldCache.allOptions);
				if(newField.jField.type == 'ENUM' /*&& fieldCache.defaultValue[0] != null*/) {
					newField.setValue(
						{selectedOptions: [fieldCache.defaultValue[0]], allOptions: fieldCache.allOptions || []}, 
						{allOptions: true});
				}
			}
			if(firstNewField == null)
				firstNewField = newField;
		}
		newField.jqFieldContainer.addClass('groupSeparator');
		
		if(switcherOptions.position == 'bottom') {
			// przesuwamy guzik "dodaj następny" poniżej utworzonych pól
			newField.jqFieldContainer.after(this.jqAddNext);
		}
		
		if(this.jDictionaryField.disabled === true || this.dictionaryField.availDictionaryButtons.doAdd !== true) {
			this.jqAddNext.addClass('force_hidden');
		}
		
		// kopiujemy przyciski pokaż słownik, wyczyść pola i etykietę "nowy wpis"
		// bez klonowania eventów, bo i tak przypisujemy nowe
		buttons = this.multipleFieldsHelper.getButtons();
		buttons[lastMultipleFieldsNumber + 1] = MultipleFieldsHelper.createDictionaryButtons.call(this, newFields, lastMultipleFieldsNumber + 1);
		buttons[lastMultipleFieldsNumber + 1].container.insertBefore(firstNewField.jqFieldContainer);
		
		// pokazujemy wszystkie przyciski "usuń element", bo jeden z nich mógl zostać schowany
		i = buttons.length;
		do {
			i --;
			if(buttons != null && buttons[i] != null) {
				buttons[i].deleteDictionaryItem.show();
			}

			/*try { // buttons[i] może być 'undefined'
				
			} catch(e) { 
				console.error(e);
			}*/
		} while(i > 0)
		
		// jeśli potrzeba, to od razu wywołujemy okienko popup
		var pure = false;
		try {
			pure = this.jDictionaryField.dictionary.pure === true;
		} catch(e) {}
		if(pure) {
			var evt = $j.Event('click.show_dictionary');
			evt.data = {dictionaryPopup: this.dictionaryPopup, fields: newFields, adds: {createNew: true}};
			buttons[lastMultipleFieldsNumber + 1].showDictionary.trigger(evt);
		}
	}).call(this, lastjFieldsGroup);
	
	return ++lastMultipleFieldsNumber;
}

/**
 * Otwiera popup słownika. Funkcję należy wywoływać przez metodę jQuery <code>bind</code>
 * lub wywołać w kontekście obiektu dom (tzn. <code>showDictionaryHandler.call(document.getElementById('nasz_element'))</code>) 
 * Ponadto obiekt powinien mieć przypisaną właśność <code>data</code> jQuery <code>multipleFieldsNumber</code>
 * z numerem pola wielowartościowego.<br />
 * Założenia odnośnie wywoływania słownika:
 * <ol>
 * <li><code>id</code> wpisu jest zapisane w polu <code>nazwa_pola_id_NUMER</code>
 * <li>Pola wielowartościowe są w postaci <code>nazwa_pola_NUMER</code>
 * </ol>
 * @method
 * @param {Object} event Event javascript zawierający obiekt data:
 * <ul>
 * <li><code>data.fields</code> Grupa (mapa) obiektów <code>Field</code> dla których wywołujemy okienko
 * <li><code>data.dictionaryPopup</code> Obiekt <code>DictionaryPopup</code> z zainicjalizowanym słownikiem
 * </ul>
 * @return {Boolean} ZAWSZE zwracamy <code>false</code>
 */
MultipleFieldsHelper.showDictionaryHandler = function(event) {
	var cn;
	var data = event.data,
		vals = {};
	var createNew = data.createNew === true;
	for (var cn in data.fields) {
		if(data.fields[cn].getSimpleValue()) {
			// interesuje nas tylko id wpisu w postaci nazwa_pola_id_3
			if(cn.search(/_id_\d+$/i) != -1)
				vals.id = data.fields[cn].getValue();
			vals[cn.replace(/_\d+$/, '')] = data.fields[cn].getValue();
		}
	}

	var helper = null;

	try {
		helper = data.fields[cn].parentFieldsManager.multipleFieldsHelper;
	} catch(e) {

	}
	// console.info('show popup', vals);
	data.dictionaryPopup.show(vals, {multipleFieldsNumber: $j(this).attr('multipleFieldsNumber'), createNew: createNew, onCancel: function(params) {
		// console.warn('Close createNew', params.createNew);
		if(params.createNew === true) {
			var multipleFieldsNumber = Number(params.multipleFieldsNumber);
			if(helper != null) {
				try {
					// zamykamy popup
					helper.getButtons()[multipleFieldsNumber].deleteDictionaryItem.trigger('click');
				} catch(e) {
					console.error(e);
				}
			}
		}
	}});
	
	return false;
}

/**
 * Czyści pola wielowartościowe, tj. wywoływuje metodę <code>clearValue</code> na każdym polu. <br />
 * Usuwa również wartość z tablicy wartości dla całego słownika.
 * Ponadto obiekt powinien mieć przypisaną właśność <code>data</code> jQuery <code>multipleFieldsNumber</code>
 * z numerem pola wielowartościowego.<br />
 * @method
 * @param {Object} event Event javascript zawierający obiekt data:
 * <ul>
 * <li><code>data.fields</code> Grupa (mapa) obiektów <code>Field</code> dla których wywołujemy okienko
 * </ul>
 * @return {Boolean} ZAWSZE zwracamy <code>false</code>
 */
MultipleFieldsHelper.clearDictionaryHandler = function(event) {
	var cn, newVals, orderedFieldsIndex;
	var data = event.data,
		multipleFieldsNumber = $j(this).attr('multipleFieldsNumber'),
		
	orderedFieldsIndex = MultipleFieldsHelper.findElementIndex(data.fieldsManager.fields, multipleFieldsNumber);
	
	if(orderedFieldsIndex == -1) {
		throw 'Cannot find orderedFieldsIndex for multipleFieldsNumber ' + multipleFieldsNumber;
	}
	
	for(cn in data.fieldsManager.fields) {
		if(data.fieldsManager.fields[cn].multipleFieldsNumber == $j(this).attr('multipleFieldsNumber')
				&& data.fieldsManager.fields[cn].isClearable()) {
			// console.info('Clearing field', cn);
			data.fieldsManager.fields[cn].clearValue();
		}
	}
	newVals = data.fieldsManager.dictionaryField.multipleFieldsValues.slice(0);
	//newVals.splice(fieldIndex, 1);
	newVals[orderedFieldsIndex] = undefined;
	
	// data.fieldsManager.dictionaryField.setValue(newVals, {syncMultipleFieldsValues: true});
	if(data.fieldsManager.jDictionaryField.submit === true)
		data.fieldsManager.submitFieldsFromDictionary();
	
	// TODO - jak to rozwiązać?
	data.fieldsManager.dictionaryField.jqField.trigger('valuechange');

	return false;
}

/**
 * Usuwa pole wielowartościowe. Aktualizuje zmienna <code>fieldNumber</code>, jeśli potrzeba
 * Ponadto obiekt powinien mieć przypisaną właśność <code>data</code> jQuery <code>multipleFieldsNumber</code>
 * z numerem pola wielowartościowego.<br />
 * @method
 * @param {Object} event Event javascript zawierający obiekt data:
 * <ul>
 * <li><code>data.fields</code> Grupa (mapa) obiektów <code>Field</code> dla których wywołujemy okienko
 * </ul>
 * @return {Boolean} ZAWSZE zwracamy <code>false</code>
 */
MultipleFieldsHelper.deleteDictionaryItemHandler = function(event) {
	var cn, newFieldNumber, evt, orderedFieldsIndex, dicField, 
		newCn, iterField, jqTr = null, i, fieldValue, dictionaryIdTemplate,
		oldMultiNum, newMultiNum;
	var data = event.data, 
		fields = event.data.fieldsManager.getFields(),
		groupedFields = event.data.groupedFields,
		fieldsNumbers = [],
		multipleFieldsNumber = Number($j(this).attr('multipleFieldsNumber'));
	
	// znajdujemy wartość wpisu (jednego wiersza)
	dicField = data.fieldsManager.dictionaryField;
	dictionaryIdTemplate = dicField.jField.dictionary.name + '_ID_';
	for(cn in groupedFields) {
		if(cn.replace(/_\d+/, '_') == dictionaryIdTemplate) {
			fieldValue = Number(groupedFields[cn].getSimpleValue());
		}
	}
	
//	console.warn('Usuwamy element o wartości', fieldValue);
	// Usuwamy element z tablicy multipleFieldsValues
	for(i = dicField.multipleFieldsValues.length-1; i >= 0; i --) {
		if(dicField.multipleFieldsValues[i] == fieldValue) {
			dicField.multipleFieldsValues.splice(i, 1);
			break;
		}
	}
	//dicField.multipleFieldsValues.splice(orderedFieldsIndex, 1);

	if(dicField.jField.dictionary.tableView === true) {
		jqTr = $j(this).parent('div').parent('td').parent('tr'); 
		// czy jest to jedyny wiersz
		var onlyOneRow = jqTr.siblings('tr').length == 0;
	} else {
		var onlyOneRow = $j(this).parent('div.DICTIONARY').siblings('div.MULTIPLE_DICTIONARY_ELEMENT').length == 0;
	}
	
	for (cn in fields) {
		if(fields[cn].multipleFieldsNumber == multipleFieldsNumber) {
			if(onlyOneRow == false) {
				fields[cn].remove();
				data.fieldsManager.multipleFieldsHelper.deleteButton(multipleFieldsNumber);
				data.fieldsManager.deleteField(cn);
			} else {
				fields[cn].clearValue();
			}
			// console.info(cn, 'is deleted?', deleted);
		} else {
			// zmniejszamy indeksy pól
			if(fields[cn].multipleFieldsNumber > multipleFieldsNumber) {
//				console.warn('decreasing number', cn);
				oldMultiNum = fields[cn].multipleFieldsNumber;
				newMultiNum = oldMultiNum - 1;
				fields[cn].multipleFieldsNumber --;
				newCn = cn.replace(/_\d+/, '_' + (fields[cn].multipleFieldsNumber));
				fields[newCn] = fields[cn];
				delete fields[cn];
				MultipleFieldsHelper.refreshField(fields[newCn], 
					{oldCn: cn, newCn: newCn});
				
				// uaktualniamy przycisk tylko raz dla każdego wiersza
				if(fieldsNumbers[newMultiNum] == null) {
					fieldsNumbers[newMultiNum] = true;
					MultipleFieldsHelper.updateButtons(
						data.fieldsManager.multipleFieldsHelper.getButtons(),
						oldMultiNum, newMultiNum);
				}
			}
		}
	}
	
	fieldsNumbers = [];
	for(cn in fields) {
		if($j.inArray(fields[cn].multipleFieldsNumber, fieldsNumbers) == -1) {
			fieldsNumbers.push(fields[cn].multipleFieldsNumber);
		}
	}
	
	// dicField.setValue(dicField.multipleFieldsValues, {syncMultipleFieldsValues: true});
//	data.fieldsManager.multipleFieldsHelper.getButtons()[multipleFieldsNumber] = undefined;
	
	// ustawiamy fieldNumber na ostatnie pole
	newFieldNumber = Math.max.apply(undefined, fieldsNumbers);
	if(newFieldNumber == -Infinity)
		newFieldNumber = 0;
	data.fieldsManager.multipleFieldsHelper.setFieldNumber(newFieldNumber);

	// usuwamy wiersz tylko wtedy gdy nie jest on ostatnim wierszem
	if(jqTr != null && onlyOneRow == false) {
		jqTr.remove();
	}
	
	// chowamy usuń element dla pierwszego wiersza
	/*if(newFieldNumber == 1) {
		(function() {
			var btns = data.fieldsManager.multipleFieldsHelper.getButtons();
			
			if(btns != null && btns[1] != null && btns[1].deleteDictionaryItem != null) {
				btns[1].deleteDictionaryItem.hide();
			}
		})();
	}*/
	
	
	
	if(data.fieldsManager.jDictionaryField.submit === true)
		data.fieldsManager.submitFieldsFromDictionary();
	
	dicField.jqField.trigger('valuechange');
	
	if(onlyOneRow == false && dicField.jField.dictionary.tableView === false) {
		$j(this).parent().remove();
	}

	// console.warn('After remove', data.fieldsManager.fields);
	// console.info(fields);
	
	return false;
}

/**
 * Kopiuje obiekty dom przycisków i przypisuje odpowiednie eventy: 
 * <ul>
 * <li>"pokaż słownik"
 * <li>"wyczyść pola"
 * <li>"nowy wpis" <b>jeszcze nie obsługiwane</b>
 * <li>"usuń element" <b>jeszcze nie obsługiwane</b>
 * <ul>
 * <b>UWAGA</b> Metodę należy wywoływać w kontekście obiektu <code>FieldsManager</code>
 * @method
 * @param {Object} groupedFields Mapa pól wielowartościowych w danej grupie
 * @param {Number} multipleFieldsNumber Liczba istniejących pól wielowartościowych (numer od 1)
 * @return {Object} Mapa skopiowanych przycisków:
 * <ul>
 * <li><code>container</code> Obiekt jQuery div zawierający wszystkie przyciski
 * <li><code>clearDictionary</code> Obiekt jQuery zawierający skopiowany przycisk "wyczyść pola"
 * <li><code>showDictionary</code> Obiekt jQuery zawierający skopiowany przycisk "pokaż słownik"
 * <li><code>deleteDictionaryItem</code> Obiekt jQuery zawierający nowy przycisk "usuń element"
 * </ul>
 */
MultipleFieldsHelper.createDictionaryButtons = function(groupedFields, multipleFieldsNumber) { // this = dictionaryFieldsManager
	var jqDicButtonsContainer = $j('<div class="DICTIONARY MULTIPLE_DICTIONARY_ELEMENT"/>'),
		newButtons = {}, availButtons = this.dictionaryField.availDictionaryButtons;
	
	newButtons.container = jqDicButtonsContainer;
	/*newButtons.clearDictionary = this.dictionaryField.dictionaryButtons.clearDictionary.clone(false, false)
		.attr('multipleFieldsNumber', multipleFieldsNumber)
		.appendTo(jqDicButtonsContainer)
		.bind('click.clear_dictionary',
				{fieldsManager: this},
				MultipleFieldsHelper.clearDictionaryHandler
		);*/
	newButtons.showDictionary = this.dictionaryField.dictionaryButtons.showDictionary.clone(false, false)
		.attr('multipleFieldsNumber', multipleFieldsNumber)
		.appendTo(jqDicButtonsContainer)
		.bind('click.show_dictionary', 
				{dictionaryPopup: this.dictionaryPopup, fields: groupedFields}, 
				MultipleFieldsHelper.showDictionaryHandler
		);
	newButtons.deleteDictionaryItem = $j('<a href="#" class="delete_dictionary_item"><div class="smallIcons"></div>'
			+ DWR.MSG.DIC_DELETE_ELEMENT + '</a>')
		.attr('multipleFieldsNumber', multipleFieldsNumber)
		.appendTo(jqDicButtonsContainer)
		.bind('click.delete_dictionary_item',
				{fieldsManager: this, groupedFields: groupedFields},
				MultipleFieldsHelper.deleteDictionaryItemHandler
		);
	
	if(availButtons.doClear === true) {
		//newButtons.clearDictionary.show();
	} else {
		//newButtons.clearDictionary.addClass('force_hidden');
	}
	if(availButtons.showPopup === true) {
		newButtons.showDictionary.show();
	} else {
		newButtons.showDictionary.addClass('force_hidden');
	}
	if(availButtons.doRemove === true) {
		newButtons.deleteDictionaryItem.show();
	} else {
		newButtons.deleteDictionaryItem.addClass('force_hidden');
	}
	
	//jqDicButtonsContainer.children('a').show();
	return newButtons;
}

/**
 * Przesyła do DWR wartości pól dla jednego wpisu słownika wielowartościowego i ustawia pola na odebrane wartości. <br />
 * Odebrane i wysłane cn pól nie powinny mieć indeksów. <br />
 * Metoda powinna być wywołana w kontekście <code>dictionaryFieldsManager</code>
 * @method
 * @param {Object} senderField Obiekt Field, który wywołał zdarzenie
 */
MultipleFieldsHelper.prototype.calculateValues = function(senderField) { // this = dictionaryFieldsManager
	(function(dfm) {
		var vals = {}, shortCn = null, senderShortCn = null,
			fieldNumber = senderField.multipleFieldsNumber;
		var cn;

		senderShortCn = senderField.jField.cn.replace(/_\d+$/, '');

		for(cn in dfm.fields) {
			if(dfm.fields[cn].multipleFieldsNumber == fieldNumber) {
			    shortCn = cn.replace(/_\d+$/, '');
				vals[shortCn] = dfm.fields[cn].getValue({allOptions: false});
				if(shortCn == senderShortCn) {
                    vals[shortCn].sender = true;
				}
			}
		}
		var guid = FieldsManager.Blocker.getNextHookGuid();
		Dictionary.getMultipleFieldsValues(dfm.dwrParams, dfm.jDictionaryField.dictionary.name, vals, {callback: function(data) {
				var cn, indexedCn, field, requiredIndexedCn = null;
	//			console.warn(data);
				if(data) {
					for(cn in data) {
						indexedCn = cn + '_' + fieldNumber;
						if(dfm.fields[indexedCn] != null) {
							dfm.fields[indexedCn].setValue(data[cn], {allOptions: true, addToLookup: true});
						}
						try {
							dfm.fields[indexedCn].jqVisibleField.trigger('validate');
						} catch(e) {
							console.error(e);
						}
						if(data[cn] == 'not-required') {
							requiredIndexedCn = indexedCn;
						}
					}
				} 
				if(requiredIndexedCn == null) {
	//				console.info('przywracamy required');
					for(cn in dfm.fields) {
						field = dfm.fields[cn];
						if(field.multipleFieldsNumber === fieldNumber) {
							field.revertRequiredState();
						}
					}
				}
			},
			preHook: FieldsManager.Blocker.getPreHook({name: 'Dictionary.getMultipleFieldsValues', guid: guid}),
			postHook: FieldsManager.Blocker.getPostHook({name: 'Dictionary.getMultipleFieldsValues', guid: guid})
		});
	})(this);
}

/**
 * Uaktualnia wewnętrzną strukturę (nazwy klas, itd)
 * jeśli zmieniliśmy kolejność pola
 */
MultipleFieldsHelper.refreshField = function(field, params) {
	var multiNum = field.multipleFieldsNumber,
		jqTr = field.jqFieldContainer.parent('td').parent('tr'),
		jqTd = jqTr.children('td'),
		className = jqTr.attr('class');
	
	field.refreshCnChanges(params.oldCn, params.newCn);
	
	if(jqTr.length) {
		if(className) {
			className = className.replace(/_\d+(?=\s|$)/, '_' + multiNum);
			jqTr.attr('class', className);
		}
		jqTr.find('a.clear_dictionary, a.delete_dictionary_item')
			.attr('multipleFieldsNumber', multiNum + '');
	}
	if(jqTd.length) {
		jqTd.each(function() {
			var jqThis = $j(this);
			className = jqThis.attr('class') || null;
			if(className) {
				className = jqThis.attr('class').replace(/_\d+(?=\s|$)/, '_' + multiNum);
				jqThis.attr('class', className);
			}
		});
	}
}

/**
 * Uaktualnia kolejność przycisków
 * @param buttons tablica przycisków
 * @param oldMultiNum stary indeks przycisku
 * @param newMultiNum nowy indeks przycisku
 */
MultipleFieldsHelper.updateButtons = function(buttons, oldMultiNum, newMultiNum) {
	buttons[newMultiNum] = buttons[oldMultiNum];
	
	// pierwszy guzik zawsze widoczny
	if(oldMultiNum != 1) {
		buttons[oldMultiNum] = undefined;
	}
	
	var btns = buttons[newMultiNum];
	
	if(btns != null) {
		/*if(btns.clearDictionary != null) {
			btns.clearDictionary.attr('multipleFieldsNumber', newMultiNum);
		}*/
		if(btns.deleteDictionaryItem != null) {
			btns.deleteDictionaryItem.attr('multipleFieldsNumber', newMultiNum);
		}
		if(btns.showDictionary != null) {
			btns.showDictionary.attr('multipleFieldsNumber', newMultiNum);
		}
	}
	
//	console.warn(buttons);
}

MultipleFieldsHelper.prototype.toggleTable = function(bVisible) {
	var jqTable = $j('#dwrDictionaryTable_' + this.name);
	if(bVisible) {
		jqTable.removeClass('collapsed');
	} else {
		jqTable.addClass('collapsed');
	}
}

MultipleFieldsHelper.prototype.isTableCollapsed = function() {
	var jqTable = $j('#dwrDictionaryTable_' + this.name);
		
	return !jqTable.hasClass('collapsed');
}

MultipleFieldsHelper.prototype.fixAddNextDisplay = function() {
	var jqTable = $j('#dwrDictionaryTable_' + this.name);

	jqTable.after(this.jqAddNext);
}

/**
 * Metoda znajduje rzeczywisty indeks (licząc od zera) w tabeli wartości słownika na podstawie
 * <code>multipleFieldsNumber</code> - numeru pola (liczonego od jedynki i niekoniecznie co jeden)
 * @method
 * @param {Object} fields Mapa wszystkich pól słownika
 * @param {Number} multipleFieldsNumber Numer pola
 * @return {Number} Indeks pola
 */
MultipleFieldsHelper.findElementIndex = function(fields, multipleFieldsNumber) {
	var cn, curNumber;
	var orderedFieldsNumbers = [];	// pola od indeksu zero
	
	multipleFieldsNumber = Number(multipleFieldsNumber);
	// znajdujemy indeks elementu, bo jeśli mamy kolekcję pól o numerach [1,2,5],
	// i chcemy ustawić 5 na undefined, to musimy zmienić element o indeksie 2 (licząc od zera)
	for(cn in fields) {
		curNumber = Number(fields[cn].multipleFieldsNumber);
		if($j.inArray(curNumber, orderedFieldsNumbers) == -1 && curNumber != undefined) {
			orderedFieldsNumbers.push(curNumber);
		}
	}
	orderedFieldsNumbers.sort(function(a, b) {
			return a-b;
		});
	
	return $j.inArray(multipleFieldsNumber, orderedFieldsNumbers);
}

if(window.Hook !== undefined)
	MultipleFieldsHelper.Hook = new Hook(MultipleFieldsHelper);


MultipleFieldsHelper.findLastGroupedFields = function(fields, options) {
	var cn, lastFieldsGroup;
	var lastNumber = -1, empty = true;
	
	// szukamy największego indeksu
	for(cn in fields) {
		if (fields[cn].multipleFieldsNumber > lastNumber) {
			lastNumber = fields[cn].multipleFieldsNumber;
		}
	}
	
	if(lastNumber == -1)
		return null;
	// szukamy pól o znalezionym indeksie
	if(options && options.asMap === true) {
		lastFieldsGroup = {};
		for(cn in fields) {
			if(fields[cn].multipleFieldsNumber == lastNumber) {
				lastFieldsGroup[cn] = fields[cn];
				empty = false;
			}
		}
	} else {
		lastFieldsGroup = [];
		for(cn in fields) {
			if(fields[cn].multipleFieldsNumber == lastNumber) {
				lastFieldsGroup.push(fields[cn]);
				empty = false;
			}
		}
	}
	
	return empty ? null : lastFieldsGroup;
}

/**
 * Pusta metoda, żeby można było podpi±ć HOOK na wczytanie wszystkich warto¶ci
 */
MultipleFieldsHelper.prototype.setValuesComplete = function(data) {

}


MultipleFieldsHelperObserver = (function() {
	/**
	 * Mapa:
	 * 		helpersEvents 	->
	 * 			helperCn1 	->
	 * 				events	-> []
	 * 			helperCn2	->
	 * 				events	-> []	
	 */
	var helpersEvents = {},
	/**
	 * Mapa:
	 * 		helpersEventsWaitingQueue	->
	 * 				helperCn1	-> [event1, event2, ...]
	 * 				helperCn2	-> [event1, event2, ...]
	 */
	helpersEventsWaitingQueue = {},
	/**
	 * Mapa:
	 * 		helperCn -> [eventNames]
	 */
	helpersPublishedEvents = {},
	helpersPendingEvents = {},
	uuid = 0;
	
	function equals(event1, event2) {
		return true;
	}
	
	return {
		subscribe: function(helperCn, eventName, opts, func) {
//			console.warn('Subscribing event', event, 'for helper', helperCn);

			if(!helpersEvents[helperCn]) {
				helpersEvents[helperCn] = {};
			}
			if(!helpersEvents[helperCn][eventName]) {
				helpersEvents[helperCn][eventName] = new buckets.LinkedList();
			}
			
			var eventList = helpersEvents[helperCn][eventName],
				token = (++uuid).toString();
			
			eventList.add({
				token: token,
				func: func,
				published: 0,
				afterEvent: opts != null ? opts.afterEvent : null,
				pendingEventList: []
			});
			
			var eventsQueue = helpersEventsWaitingQueue[helperCn],
				normalizedEventsQueue = [];
			var i, len;
//			console.warn('Events queue is', eventsQueue);
			if(eventsQueue && eventsQueue.length) {
				for(i = 0, len = eventsQueue.length; i < len; i ++) {
					// console.warn('Publishing event', helperCn, eventsQueue[i], 'from waiting queue');
					if(MultipleFieldsHelperObserver.publish(helperCn, eventsQueue[i], {queue: false}) === true) {
						eventsQueue[i] = undefined;
					}
//					console.warn('debug state', MultipleFieldsHelperObserver.getDebug().helpersEventsWaitingQueue[helperCn].toString());
				}
				for(i = 0, len = eventsQueue.length; i < len; i ++) {
					if(eventsQueue[i]) {
						normalizedEventsQueue.push(eventsQueue[i]);
					}
				}
				helpersEventsWaitingQueue[helperCn] = normalizedEventsQueue.slice(0);
			}
			
			
			return token;
		},
		
		unsubscribe: function(helperCn, event) {
//			console.info('unsubscribe', helperCn, event);
			if(event == null) {
				delete helpersEvents[helperCn];
			} else {
				delete helpersEvents[helperCn][event];
			}
		},
		
		isSubscribed: function(helperCn, event) {
			if(helpersEvents[helperCn] && helpersEvents[helperCn][event]) {
				return true;
			}
			
			return false;
		},
		
		publish: function(helperCn, eventName, options) {
			var opts = {
				queue: options ? options.queue : true,
				args: options ? options.args : undefined
			}
			
//			console.info('Publish', helperCn, event, helpersEvents, helpersEventsWaitingQueue);
			
			if(!helpersEvents[helperCn]) {
//				console.warn('No events for helper', helperCn);
				return;
			}
			if(!helpersEvents[helperCn][eventName]) {
//				console.warn('Event', event, 'not found for helper', helperCn);
				if(opts.queue === true) {
//					console.warn('Adding event ', event, 'from helper', helperCn, 'to queue');
					if(!helpersEventsWaitingQueue[helperCn]) {
						helpersEventsWaitingQueue[helperCn] = [];
					}
					helpersEventsWaitingQueue[helperCn].push(eventName);
				}
				return false;
			}
			
			var eventList = helpersEvents[helperCn][eventName];
			//var len = eventList.length;
			var i;
			
			eventList.forEach(function(event) {
				var pendingEvents = null;
				try {
					if(helpersPublishedEvents[helperCn] == null) {
						helpersPublishedEvents[helperCn] = [];
					}
					
					// czekamy na poprzednie zdarzenie?
					if(event.afterEvent != null) {
						if($j.inArray(event.afterEvent, helpersPublishedEvents[helperCn]) == -1) {
							console.info(helperCn, eventName, 'is waiting for ', helperCn, event.afterEvent);
							event.pendingEventList.push(event.afterEvent);
							if(helpersPendingEvents[helperCn] == null) {
								helpersPendingEvents[helperCn] = new buckets.MultiDictionary();
							}
							helpersPendingEvents[helperCn].set(event.afterEvent, eventName);
						} else {
							helpersPublishedEvents[helperCn].push(eventName);
							if(opts.args) {
								// console.info('Publish with arguments', opts.args);
								event.published ++;
								event.func.apply(event.func, [opts.args]);
							} else {
								event.published ++;
								event.func();
							}
						}
					} else {
						helpersPublishedEvents[helperCn].push(eventName);
						if(opts.args) {
							// console.info('Publish with arguments', opts.args);
							event.published ++;
							event.func.apply(event.func, [opts.args]);
						} else {
							event.published ++;
							event.func();
						}
					}
					
					// czy musimy wywołać zdarzenia po naszym evencie?
					if(helpersPendingEvents[helperCn] != null && helpersPendingEvents[helperCn].containsKey(eventName)) {
						pendingEvents = helpersPendingEvents[helperCn].get(eventName);
						buckets.arrays.forEach(pendingEvents, function(evtName) {
//							console.info(eventName, '->', evtName);
							MultipleFieldsHelperObserver.publish(helperCn, evtName);
							buckets.arrays.remove(pendingEvents, evtName);
						});
					}
				} catch(e) {
					console.error('Observer function error');
					console.error(e);
				}
			});
			
			return true;
		},
		
		getDebug: function() {
			return {
				helpersEvents: helpersEvents,
				helpersEventsWaitingQueue: helpersEventsWaitingQueue,
				helpersPublishedEvents: helpersPublishedEvents,
				helpersPendingEvents: helpersPendingEvents
			}
		}
	}
})();