function Popup(params) {
    this.cn = params.cn;
    this.caption = params.caption || null;
    this.closeable = params.closeable;
    this.closerefcn = params.closerefcn;
    this.grid = null;
    this.jqPopup = null;
    this.fields = {};

    var fieldComparator = function(field) { return field.jField.cn; }
    this.dynamicFields = new buckets.Set(fieldComparator);
    this.dynamicFieldsQueue = new buckets.Set(fieldComparator);
    this.opening = null;

    // priviledged
    this.align = function() {
        Popup.prototype.align.apply(this, arguments);
    }
}

Popup.prototype.create = function() {
//    console.warn('Creating popup', this.cn);
    this.jqPopup = $j('<div/>').attr('id', this.POPUP_PREFIX + this.cn).addClass('dwr-popup').addClass('dwrColumn');
    if(this.caption != null) {
        this.jqPopup.attr('title', this.caption);
    }
    $j(document).append(this.jqPopup);
    this.grid = new Grid(this.jqPopup);

    this.createDialog();
}

Popup.prototype.createDialog = function() {
    this.jqPopup.dialog({
        modal: true,
        autoOpen: false,
        resizable: true,
        beforeClose: $j.proxy(function(event, ui) {
            var closeClicked = this.jqPopup.dialog('widget').find('.ui-dialog-titlebar-close')[0] === event.currentTarget;
            if(closeClicked === true && this.closerefcn != null && this.fields[this.closerefcn] != null) {
                this.fields[this.closerefcn].jqVisibleField.trigger('valuechange');
                return false;
            }
        }, this),
        create: $j.proxy(function(event, ui) {
            if(this.caption == null) {
                this.jqPopup.dialog('widget').find('.ui-dialog-titlebar').hide();
            }
        }, this)
    });
    this.jqPopup.parent().appendTo('form.dwr');
    this.jqPopup.on('resize', '.grid tbody tr td div.field', $j.proxy(function(event) {
        if(this.opening != null) {
//            console.warn('resize', this.cn, event.field.jField.cn);
//            console.warn(event);
            this.resizeToGrid();
            if(this.dynamicFieldsQueue.remove(event.field)) {
//                console.warn('Removed from queue', event.field.jField.cn);
            }
            if(this.dynamicFieldsQueue.isEmpty()) {
//                console.info('resolve queue');
                this.opening.resolve();
            }
            this.jqPopup.dialog('option', 'position', {my: 'center', at: 'center', of: window});
        }
    }, this));
}

Popup.prototype.updateDialogParams = function() {
    if(this.caption != null) {
        this.jqPopup.dialog('option', 'title', this.caption);
        this.jqPopup.dialog('widget').find('.ui-dialog-titlebar').show();
    } else {
        this.jqPopup.dialog('widget').find('.ui-dialog-titlebar').hide();
    }
}

Popup.prototype.show = function(visible) {
    var sharedFields = Popup.FieldPool.getSharedFields(),
        fieldCn = null, takingFields = {};

    if(visible === true) {
        this.updateDialogParams();
        for(fieldCn in this.fields) {
            if(sharedFields[fieldCn] != null) {
                takingFields[fieldCn] = sharedFields[fieldCn];
            }
            if(this.fields[fieldCn].isDynamic()) {
                this.fields[fieldCn].destroyDynamic();
            }
            Popup.FieldPool.addVisibleField(this.fields[fieldCn]);
        }
        Popup.FieldPool.takeOverFields(this, takingFields);
        this.dynamicFields.forEach($j.proxy(function(field) {
//            console.warn('Found dynamic field', field.jField.cn);
            this.dynamicFieldsQueue.add(field);
        }, this));
       console.info('Dynamic fields', this.cn, this.dynamicFieldsQueue.toArray());
        if(this.opening == null)
            this.opening = new $j.Deferred();
        if(this.dynamicFieldsQueue.isEmpty()) {
            this.opening.resolve();
        }
        this.opening.done($j.proxy(function() {
            this.grid.normalizeTable();
            this.jqPopup.dialog('open');
            for(fieldCn in this.fields) {
                if(this.fields[fieldCn].isDynamic()) {
                    this.fields[fieldCn].createDynamic();
                }
            }
            this.dynamicFieldsQueue.forEach($j.proxy(function(field) {
                //field.createDynamic();
            }, this));
            this.resizeToGrid();
        }, this));
    } else {
        for(fieldCn in this.fields) {
            Popup.FieldPool.removeVisibleField(this.fields[fieldCn]);
        }
        this.jqPopup.dialog('close');
    }
}

Popup.prototype.isOpen = function() {
    return this.jqPopup.dialog('isOpen');
}

Popup.prototype.assignField = function(field) {
//    console.info('assignField', field.jField.cn, this.cn);
    this.grid.assignField(field);
    this.fields[field.jField.cn] = field;
    Popup.FieldPool.addField(this, field);
    if(field.isDynamic() === true) {
        this.dynamicFields.add(field);
    }
    if(Popup.RESIZABLE_FIELDS[field.jField.type]) {
        if(field.jField.type == 'DICTIONARY' && field.jField.dictionary.multiple === true) {
            if(!MultipleFieldsHelper.Hook.isPostHookPresent('setValuesComplete', this.align)) {
                MultipleFieldsHelper.Hook.addPostHook('setValuesComplete', this.align, {self: this, params: [this, true]});
            }
        }
        if(!FieldsManager.Hook.isPostHookPresent('updateFields', this.align)) {
            // console.info('Nie ma hooka', this.cn);
            FieldsManager.Hook.addPostHook('updateFields', this.align, {self: this, params: [this]});
        } else {
            // console.info('Jest hook', this.cn);
        }
    }
}

Popup.prototype.validate = function() {
    // tworzymy tymczasowy, oszukany fieldsmanager
    var fm = {
        fields: this.fields,
        validateField: FieldsManager.prototype.validateField
    };
    return FieldsManager.prototype.validate.call(fm);
}

Popup.prototype.resizeToGrid = function() {
    var popupWidth = this.jqPopup.outerWidth(),
        gridWidth = this.grid.jqTable.outerWidth(true);
//    console.info(this.jqPopup.outerWidth(), this.grid.jqTable.outerWidth(true));
    if(gridWidth - popupWidth > 20)
        this.jqPopup.dialog('option', 'width', this.grid.jqTable.outerWidth(true) + 15);
}

Popup.prototype.align = function(popup) {
    // console.info('align', this.cn, arguments);
    if(popup && popup.jqPopup) {
        popup.jqPopup.dialog('option', 'width', popup.jqPopup[0].scrollWidth);
        popup.jqPopup.dialog('option', 'position', {my: 'center', at: 'center', of: window});
    }
}

Popup.prototype.POPUP_PREFIX = '__DWR_POPUP_';

Popup.FieldPool = (function() {
    var uniqueFields = {}, sharedFields = {},
        assignedFields = {},
        visibleFields = {};

    return {
        addField: function(popup, field) {
            var fieldCn = field.jField.cn;
            if(uniqueFields[fieldCn] == null) {
                uniqueFields[fieldCn] = field;
                assignedFields[fieldCn] = popup;
                return true;
            } else {
                delete uniqueFields[fieldCn];
                sharedFields[fieldCn] = field;
            }
        },

        takeOverFields: function(popup, fields) {
            var fieldCn = null, field = null, oldPopup = null;
            for(fieldCn in fields) {
                field = fields[fieldCn];
                oldPopup = assignedFields[fieldCn];
                //if(oldPopup.cn != popup.cn) {
//                    console.warn('taking field', fieldCn, 'from', oldPopup.cn, 'to', popup.cn);
                    oldPopup.grid.unassignField(field);
                    popup.grid.assignField(field);
                    assignedFields[fieldCn] = popup;
                //}
            }
        },

        addVisibleField: function(field) {
            visibleFields[field.jField.cn] = field;
        },

        removeVisibleField: function(field) {
            delete visibleFields[field.jField.cn];
        },

        isSharedField: function(field) {
            return sharedFields[field.jField.cn];
        },

        acquireField: function(popup, field) {
            popup.grid.assignField(field);
        },

        releaseField: function(field) {
            var fieldCn = field.jField.cn;
            var popup = assigedFields[fieldCn];
            popup.grid.unassignField(field);
        },

        getSharedFields: function() {
            return sharedFields;
        },

        getUniqueFields: function() {
            return uniqueFields;
        },

        getAssignedFields: function() {
            return assignedFields;
        },

        isVisibleField: function(field) {
            return visibleFields[field.jField.cn] != null;
        },

        isPopupField: function(field) {
            var fieldCn = field.jField.cn;
            return (uniqueFields[fieldCn] != null) || (sharedFields[fieldCn] != null);
        }
    }
})();

Popup.RESIZABLE_FIELDS = {
    'DICTIONARY': true
}