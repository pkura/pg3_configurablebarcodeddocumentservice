<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<!-- <dwr-document-main.jsp> -->
<script type="text/javascript">
var DWR = {
	PAGE_SIZE: 10,
	DEFAULT_DATE_FORMAT: 'dd-mm-yyyy',
	DEFAULT_TIME_FORMAT: 'hh:MM:ss',
	DEFAULT_HOUR_FORMAT: 'hh',
	DEFAULT_HOUR_MINUTE_FORMAT: 'hh:MM',
	CKEDITOR_URL: '<ww:url value="'/dwrdockind/ckeditor/ckeditor.js'"/>?v=<%= Docusafe.getDistTimestamp() %>',
	UPLOAD_URL: '<ww:url value="'/dwr-upload.ajx'"/>',
	PLUPLOAD_FLASH_URL: '<ww:url value="'/dwrdockind/plupload/plupload.flash.swf'"/>',
	PLUPLOAD_SILVERLIGHT_URL: '<ww:url value="'/dwrdockind/plupload/plupload.silverlight.xap'"/>',
	BLOCKER: false,
	BLOCKER_STATS: false,
	BLOCK_TIMEOUT: <ds:addition-value name="dwr.blocker.blockTimeout" documentKindCn="documentKindCn" />,
	RELEASE_TIMEOUT: <ds:addition-value name="dwr.blocker.releaseTimeout" documentKindCn="documentKindCn" />
};

var DWR_DATE_FORMAT_ID = new Object();
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT] = 1;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_TIME_FORMAT] = 2;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_HOUR_FORMAT] = 3;
    DWR_DATE_FORMAT_ID[DWR.DEFAULT_DATE_FORMAT + ' ' + DWR.DEFAULT_HOUR_MINUTE_FORMAT] = 4;

<ds:available test="dwr.blocker" documentKindCn="documentKindCn">
DWR.BLOCKER = true;
</ds:available>

/* musi by� globalna przed wczytaniem CKEditor */
CKEDITOR_BASEPATH = '<ww:url value="'/dwrdockind/ckeditor/'"/>';

//tutaj wrzucam komunikaty w r�nych j�zykach, �eby by�y obs�u�one z poziomu jsp
DWR.MSG = {
		DEFAULT_SELECT_LABEL:				'<ds:lang text="select.wybierz"/>',
		SAVE_WITH_TIMEOUT:					'<ds:lang text="DWR_SAVE_WITH_TIMEOUT"/>',
		DIC_ADD_NEXT:						'<ds:lang text="DWR_DIC_ADD_NEXT"/>',
		DIC_DELETE_ELEMENT:					'<ds:lang text="DWR_DIC_DELETE_ELEMENT"/>',
		DIC_CLEAR_FIELDS:					'<ds:lang text="DWR_DIC_CLEAR_FIELDS"/>',
		DIC_SHOW_POPUP:						'<ds:lang text="DWR_DIC_SHOW_POPUP"/>',
		DIC_NEW_ITEM:						'<ds:lang text="DWR_DIC_NEW_ITEM"/>',
		VALIDATOR_INCORRECT:				'<ds:lang text="DWR_VALIDATOR_INCORRECT"/>',
		VALIDATOR_INCORRECT_NUMBER:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_NUMBER"/>',
		VALIDATOR_INCORRECT_AMOUNT:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_AMOUNT"/>',
		VALIDATOR_INCORRECT_DATE:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_DATE"/>',
		VALIDATOR_INCORRECT_DAY:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_DAY"/>',
		VALIDATOR_INCORRECT_MONTH:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_MONTH"/>',
		VALIDATOR_INCORRECT_YEAR:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_YEAR"/>',
		VALIDATOR_INCORRECT_TIME:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_DATE"/>',
		VALIDATOR_INCORRECT_HOUR:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_HOUR"/>',
		VALIDATOR_INCORRECT_MINUTE:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_MINUTE"/>',
		VALIDATOR_INCORRECT_SECOND:			'<ds:lang text="DWR_VALIDATOR_INCORRECT_SECOND"/>',
		RANGE_TOO_BIG_NUMBER:				'<ds:lang text="DWR_RANGE_TOO_BIG_NUMBER"/>',
		RANGE_TOO_SMALL_NUMBER:				'<ds:lang text="DWR_RANGE_TOO_SMALL_NUMBER"/>',
		ENUM_MULTIPLE_SELECT:				'<ds:lang text="DWR_ENUM_MULTIPLE_SELECT"/>',
		FIELD_REQUIRED:						'<ds:lang text="DWR_FIELD_REQUIRED"/>',
		DIC_POPUP_ADD:						'<ds:lang text="DWR_DIC_POPUP_ADD"/>',
		DIC_POPUP_SAVE:						'<ds:lang text="DWR_DIC_POPUP_SAVE"/>',
		DIC_POPUP_CANCEL:					'<ds:lang text="DWR_DIC_POPUP_CANCEL"/>',
		DIC_POPUP_EDIT:						'<ds:lang text="DWR_DIC_POPUP_EDIT"/>',
		DIC_POPUP_DELETE:					'<ds:lang text="DWR_DIC_POPUP_DELETE"/>',
		DIC_POPUP_CLEAR:					'<ds:lang text="DWR_DIC_POPUP_CLEAR"/>',
		DIC_POPUP_SELECT:					'<ds:lang text="DWR_DIC_POPUP_SELECT"/>',
		DIC_POPUP_ADD_TO_FORM:				'<ds:lang text="DWR_DIC_POPUP_ADD_TO_FORM"/>',
		DIC_POPUP_MORE_RESULTS:				'<ds:lang text="DWR_DIC_POPUP_MORE_RESULTS"/>',
		DIC_POPUP_ITEM_SAVED: 				'<ds:lang text="DWR_DIC_POPUP_ITEM_SAVED"/>',
		DIC_POPUP_ITEM_ADDED: 				'<ds:lang text="DWR_DIC_POPUP_ITEM_ADDED"/>',
		DIC_POPUP_ITEM_DELETED: 			'<ds:lang text="DWR_DIC_POPUP_ITEM_DELETED"/>',
		DIC_POPUP_ITEM_SAVED_FAILED: 		'<ds:lang text="DWR_DIC_POPUP_ITEM_SAVED_FAILED"/>',
		DIC_POPUP_ITEM_ADDED_FAILED: 		'<ds:lang text="DWR_DIC_POPUP_ITEM_ADDED_FAILED"/>',
		/*DIC_POPUP_ITEM_ADDED_FAILED_EXIST   '<ds:lang text="DWR_DIC_POPUP_ITEM_ADDED_FAILED_EXIST"/>',*/
		DIC_POPUP_ITEM_DELETED_FAILED: 		'<ds:lang text="DWR_DIC_POPUP_ITEM_DELETED_FAILED"/>',
		DIC_POPUP_ITEM_ADDED_EMPTY_WARNING: '<ds:lang text="DWR_DIC_POPUP_ITEM_ADDED_EMPTY_WARNING"/>',
		DIC_POPUP_ITEM_ADDED_NOT_VALID: 	'<ds:lang text="DWR_DIC_POPUP_ITEM_ADDED_NOT_VALID"/>',
		DIC_POPUP_NO_RESULTS_FOUND:			'<ds:lang text="DWR_DIC_POPUP_NO_RESULTS_FOUND"/>',
		AUTOCOMPLETE_SEE_MORE_RESULTS:		'<ds:lang text="DWR_AUTOCOMPLETE_SEE_MORE_RESULTS"/>',
		AUTOCOMPLETE_SHOWING_FIRST:			'<ds:lang text="DWR_AUTOCOMPLETE_SHOWING_FIRST"/>',
		AUTOCOMPLETE_RESULTS:				'<ds:lang text="DWR_AUTOCOMPLETE_RESULTS"/>',
		ATT_ADD_REVISION:					'<ds:lang text="NowaWersjaPliku"/>',
		ATT_REVERT_REVISION:				'<ds:lang text="Anuluj"/>',
		ATT_DOWNLOAD:						'<ds:lang text="PobierzZalacznik"/>',
		ATT_DOWNLOAD_AS_PDF:				'<ds:lang text="PobierzZalacznik"/>',
		ATT_VIEWSERVER:						'<ds:lang text="WyswietlZalacznikWprzegladarce"/>',
		PLUPLOAD_FILE_NAME:                 '<ds:lang text="DWR_PLUPLOAD_FILE_NAME"/>',
		PLUPLOAD_FILE_STATUS:               '<ds:lang text="DWR_PLUPLOAD_FILE_STATUS"/>',
		PLUPLOAD_FILE_SIZE:                 '<ds:lang text="DWR_PLUPLOAD_FILE_SIZE"/>',
		PLUPLOAD_ADD_FILES:                 '<ds:lang text="DWR_PLUPLOAD_ADD_FILES"/>',
		PLUPLOAD_START_UPLOAD:              '<ds:lang text="DWR_PLUPLOAD_START_UPLOAD"/>',
		PLUPLOAD_DRAG_HERE:                 '<ds:lang text="DWR_PLUPLOAD_DRAG_HERE"/>',
		PLUPLOAD_SENDING:                   '<ds:lang text="DWR_PLUPLOAD_SENDING"/>',
		PLUPLOAD_COMPLETE:                  '<ds:lang text="DWR_PLUPLOAD_COMPLETE"/>',
		PLUPLOAD_REMOVE:                    '<ds:lang text="doDelete"/>',
		BLOCKER_LOADING:                    '<ds:lang text="DWR_BLOCKER_LOADING"/>'
}
</script>
<script type="text/javascript" src="<ww:url value="'/buckets-minified.js'"/>?v=<%= Docusafe.getDistTimestamp() %>" ></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/lodash.compat-2.4.1.min.js"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/jquery.blockUI.js'"/>?v=<%= Docusafe.getDistTimestamp() %>" ></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/chosen.jquery.js'"/>?v=<%= Docusafe.getDistTimestamp() %>" ></script>
<script type="text/javascript" src="<ww:url value="'/dwr/interface/DwrTest.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/jquery.autocomplete.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwr/engine.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/utils.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/Exceptions.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/Hook.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/Field.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/FieldsManager.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/MultipleFieldsHelper.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/DictionaryPopup.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/Grid.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/Popup.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwr/interface/Dictionary.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/plupload/plupload.full.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/plupload/jquery.ui.plupload/jquery.ui.plupload.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>

<link rel="stylesheet" type="text/css" href="<ww:url value="'/office/tasklist/calendar/jquery.autocomplete.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/dwr.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/chosen.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css'" />" />
<link rel="alternate stylesheet" title="Tryb programisty" type="text/css" href="<ww:url value="'/dwrdockind/dwr-debug.css'" />" />

<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/dwr.ie7.css'" />" />
<![endif]-->

<!--[if IE 8]>
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/dwr.ie8.css'" />" />
<![endif]-->


<script type="text/javascript">
var dwrParams = {
    context: "dockind",
    action: "new",
    documentType: "<ww:property value='docType'/>",
    dockind: "<ww:property value='documentKindCn'/>",
    documentId: "<ww:property value='documentId'/>",
    docIds: "<ww:property value='dependsDocsJson' escape="false"/>",
    activity: "<ww:property value='activity'/>"
};
                         
//window.onerror = showError;
var fieldsManager = null;
var grid = null;
var CALENDAR_IMAGE  = '<ww:url value="'/calendar096/img.gif'" />';

$j(document).ready(function() {
	fieldsManager = new FieldsManager();
	fieldsManager.init({
		grid: new Grid($j('#grid'))
	});
	// zeby autocomplete moglo wychodzic poza ramke
	$j('#middleContainer').css('overflow', 'visible');
});

// poprawia rozmiar strony
$j('body').bind('dwr_grid_normalized', function() {
	fixResize();
});

</script>
<div class="dwrDockindContainer">



<fieldset id="column0" class="dwrColumn">
<ww:hidden name="'dockindEventValue'" id="dockindEventValue"/>
<ww:hidden name="'doDockindEvent'" id="doDockindEvent"/>
	<fieldset id="dockind-select-fieldset">
		<div class="field" id="dockind-select"><label for="documentKindCn">
		<ds:available test="RodzajDokumentuASTypDokumentu">
          	<ds:lang text="TypDokumentu"/>:</label>
		</ds:available>
		<ds:available test="!RodzajDokumentuASTypDokumentu">
			<ds:available test="RodzajDokumentuASFormularz">
          		<ds:lang text="Formularz"/>:</label>
          	</ds:available>
          	<ds:available test="!RodzajDokumentuASFormularz">
          		<ds:lang text="RodzajDokumentu"/>:</label>
          	</ds:available>
		</ds:available>

		<ww:if test="document == null || canChangeDockind"> 
			<ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key"
            	listValue="value" cssClass="'sel dontFixWidth'" onchange="'this.form.submit()'" />
        </ww:if>
        <ww:else>

        	<ww:hidden name="'documentKindCn'" value="documentKindCn"></ww:hidden>
            <ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key"
            	listValue="value" cssClass="'sel dontFixWidth'" onchange="'this.form.submit()'" disabled="true" />
        </ww:else>
            
            <script type="text/javascript">
				var qs = (function(a) 
					    {     
				    if (a == "") return {};     
				    var b = {};     
				    for (var i = 0; i < a.length; ++i)     
					    {         
					    var p=a[i].split('=');         
					    if (p.length != 2) continue;         
					    b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));     
					    }     
				    return b; 
				    })(window.location.search.substr(1).split('&'));
				
					if (qs["hide_document_types"]=="1" && qs["documentKindCn"]!="")
					{							
						 selectedDocumentKind = $j('#documentKindCn option:selected');
					        $j('#documentKindCn').remove();
				
					        $j('#dockind-select').append('<input disabled="disabled" type="text" name="documentKindCn" id="documentKindCn" value="'+qs["documentKindCn"]+'"/>');

					        var p = $j('#form').attr('action');
					        p = p + '?documentKindCn='+ qs["documentKindCn"]+ '&hide_document_types=1';
					        $j('#form').attr('action',p);
					      
					}
			</script>
		</div>
	</fieldset>
</fieldset>
<fieldset id="column1" class="dwrColumn"></fieldset>
<fieldset id="column2" class="dwrColumn"></fieldset>

<fieldset id="grid" class="dwrColumn grid"></fieldset>
<div class="clearBoth"></div>

</div>
<!-- </dwr-document-main.jsp> -->