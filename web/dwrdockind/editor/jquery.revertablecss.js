(function($) {
	$.fn.extend({
		revertablecss: function(mode, cssNames) {
			return this.each(function() {
				var jqThis = $j(this),
					name = null, elCssMap = null, i = 0;
				
				switch(mode) {
					case 'save':
						elCssMap = {};
						for(i = 0; i < cssNames.length; i ++) {
							name = cssNames[i];
							elCssMap[name] = jqThis.css(name);
							try {
								if(this.style[name] == '') {
									elCssMap[name] = '';
								}
							} catch(e) {}
						}
//						console.info(this.id, 'css saving', elCssMap);
						jqThis.data('orgcss', elCssMap);
					break;
					case 'update':
						elCssMap = jqThis.data('orgcss') || {};
						for(i = 0; i < cssNames.length; i ++) {
							name = cssNames[i];
							elCssMap[name] = jqThis.css(name);
							try {
								if(this.style[name] == '') {
									elCssMap[name] = '';
								}
							} catch(e) {}
						}
						jqThis.data('orgcss', elCssMap);
					break;
					case 'delete':
						jqThis.removeData('orgcss');
					break;
					case 'load':
						elCssMap = jqThis.data('orgcss');
						if(elCssMap) {
							jqThis.css(elCssMap);
						}
						jqThis.removeData('orgcss');
//						console.info(this.id, 'css loading', elCssMap);
					break;
					default:
						throw('Mode ' + mode + ' is not supported!');
						return;
				}
			});
		}
	});
})(jQuery);