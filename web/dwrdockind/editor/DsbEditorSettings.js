DsbEditorSettings = function() {
	// private
	var defaultSettings = {
		xmlEditor: false,
		xmlEditorX: 20,
		xmlEditorY: 20,
		xmlEditorW: 300,
		xmlEditorH: 500,
		welcomeScreen: true
	};
	
	// privileged
	this.settings = null;
	
	this.load = function() {
		var key = null, upgradeNeeded = false;
		this.settings = localStorage.getItem('dsb-settings');
		if(!this.settings) {
			this.settings = defaultSettings;
			localStorage.setItem('dsb-settings', JSON.stringify(this.settings));
		} else {
			this.settings = JSON.parse(this.settings);
			for(key in defaultSettings) {
				if(this.settings[key] === undefined) {
					this.settings[key] = defaultSettings[key];
					upgradeNeeded = true;
				}
			}
			if(upgradeNeeded) {
				localStorage.setItem('dsb-settings', JSON.stringify(this.settings));
			}
		}
	}
	
	this.save = function() {
		localStorage.setItem('dsb-settings', JSON.stringify(this.settings));
	}
}