DsbField = function(dsbXmlDoc, jFld) {
	// private
	var _this = this;
	var dsbXmlDocument = null,
		value = null,
		jField = null;
	// pola z przedrostkiem dck okre�laj� bezpo�rednie parametry z xmla
	var dsbFields = [],
		jqFieldContainer,
		jqCell = null,
		jqCalculatedDestCell = null,
		jqHighlightedCells = null,
		selectCounter = 0,
		dictionarySelectableInitialized = false,
		selectable = true;
	
	// constructor
	jField = jFld;
	dsbXmlDocument = dsbXmlDoc;
	
	var calculateSnapGrid = function(grid) {
		var jqCellsByColumn = grid.getCellsByColumn(jqCell.attr('column')),
			jqCellsByRow = grid.getCellsByRow(jqCell.attr('row')),
			snapGrid = {xPos: [], yPos: [], jqColCells: [], jqRowCells: []};
		
		jqCellsByColumn.each(function() {
			var jqThis = $j(this);
			snapGrid.yPos.push(jqThis.position().top);
			snapGrid.jqColCells.push(jqThis);
		});
		
		jqCellsByRow.each(function() {
			var jqThis = $j(this);
			snapGrid.xPos.push(jqThis.position().left);
			snapGrid.jqRowCells.push(jqThis);
		});
		
		return snapGrid;
	}
	
	var getNearestSnap = function(snapGrid, x, y) {
		var i;
		var snap = {x: 0, y: 0, jqColCell: null, jqRowCell: null},
			snapEdge = {left: 0, right: 0, distance: 0};
		
		/*for(i = snapGrid.yPos.length-1; i > 0; i --) {
			if(y <= snapGrid.yPos[i] - DsbEditor.SNAP_GRID_TOLERANCE) {
				snap.y = snapGrid.yPos[i];
				snap.jqColCell = snapGrid.jqColCells[i-1];
			}
		}*/
		
		for(i = snapGrid.xPos.length-1; i > 0; i --) {
			snapEdge.left = snapGrid.xPos[i-1];
			snapEdge.right = snapGrid.xPos[i];
			snapEdge.distance = snapEdge.right - snapEdge.left;
			if(x > (snapEdge.left + snapEdge.distance * 0.75) && x < snapEdge.right) {
				snap.x = snapGrid.xPos[i];
				snap.jqRowCell = snapGrid.jqRowCells[i-1];
				break;
			}
		}
		
		return snap;
	}
	
	var initResizable = function(grid) {
		var	outtersHeight = 0, outtersWidth = 0,
			snapGrid = null;
		
		// marginesy, paddingi i bordery kt�re odejmujemy od rzeczywistej wysoko�ci
		outtersHeight = parseInt(jqFieldContainer.css('padding-top'), 10) + parseInt(jqFieldContainer.css('border-top-width'), 10)
			+ parseInt(jqCell.css('padding-bottom'), 10);
		outtersWidth = parseInt(jqFieldContainer.css('padding-left'), 10) + parseInt(jqFieldContainer.css('border-left-width'), 10);
			/*+ parseInt(jqCell.css('padding-left'), 10);*/
		
		grid.calculateSnap();
		
		jqFieldContainer.resizable({
			start: function(event, ui) {
				
			},
			resize: function(event, ui) {
				var snap = grid.getNearestSnap(ui.originalPosition.left + ui.size.width - 10, ui.originalPosition.top + ui.size.height),
					newHeight = null, newWidth = null;
				
				if(snap.vert != null) {
					newWidth = snap.vert.xPos - ui.originalPosition.left - outtersWidth + 1;
				}
				if(snap.hor != null) {
					newHeight = snap.hor.yPos - ui.originalPosition.top - outtersHeight;
				}
				if(webkit) {
					newWidth ++;
					newHeight ++;
				}
				
				if(newWidth != null) {
					ui.size.width = newWidth;
				}
				if(newHeight != null) {
					ui.size.height = newHeight;
				}
				$j(this).data('snap', snap);
			},
			stop: function(event, ui) {
				var snap = $j(this).data('snap');
				jField.coords = Grid.transformCoords('translate-end', jField.coords, 
					snap.vert ? snap.vert.col-1 : null, snap.hor ? snap.hor.row-1 : null);
				jqCalculatedDestCell = $j(this).parent('td');
				
				var cmd = _this.moveToCell(grid, jqCalculatedDestCell);
				if(cmd != null) {
					dsbXmlDocument.execCommand(cmd);
				}
				$j(this).removeData('snap');
				grid.normalizeTable();
			}
		});
	}
	
	var unInitResizable = function() {
		jqFieldContainer.resizable('destroy');
	}
	
	// privileged
	/**
	 * Warto�� pola - do wywo�ania przez DWRTest.getFieldsValues
	 */
	this.getValue = function() {
		return dsbXmlDocument.getFieldValue(jField.cn);
	}
	
	this.dictionarySearch = function(values, limit, offset) {
		var data = [];
		var i, cn, item;
		var j, jLen, dsbField;
		
		for(i = 0; i < 11; i ++) {
			item = {
				id: (i+1)
			};
			for(j = 0, jLen = dsbFields.length; j < jLen; j ++) {
				dsbField = dsbFields[j];
				cn = dsbField.getCn();
				item[cn] = DsbUtils.unpackFieldValue(values[cn]);
			}
			data.push(item);
		}
	
		return data;
	}
	
	/**
	 * Obiekt dla DWR Field
	 */
	this.getjField = function() {
		return jField;
	}
	
	this.setjField = function(newjField) {
		jField = newjField;
	}
	
	this.getCn = function() {
		return jField.cn;
	}
	
	this.initDragAndDrop = function(jqContainer) {
		console.info('dnd', jField.cn);
		jqFieldContainer = jqContainer;
		jqCell = jqContainer.parent('td');
//		console.info(jqFieldContainer, jField.cn);
		jqFieldContainer.addClass('dsb-field-draggable').data('cn', jField.cn);
		jqFieldContainer.draggable('destroy').draggable({
			revert: 'invalid',
			opacity: 0.7,
			/*delay: 100,*/
			/*distance: 10,*/
			start: function(event, ui) {
//				console.info(event);
				var pos = ui.offset,
					opts = {resetPosition: false};
				if(!$j(this).hasClass('ui-selected')) {
					ui.helper.css({
						left: pos.left + 'px',
						top: pos.top + 'px'
					});
				}
				$j('div.field.ui-selected').not(this).trigger('unselected');
				//$j(this).trigger('selected', [opts]);
				
				jqPrevCell = jqCell;
				$j(this).data('start-cell-row', Grid.extractCellPosition(jqCell).row);
				$j(this).addClass('dsb-auto-width');
				//$j(this).css('position', 'absolute');
			},
			stop: function(event, ui) {
//				console.info('drag stop');
				var i = 0;
				if(jqHighlightedCells) {
					for(i = 0; i < jqHighlightedCells.length; i ++) {
						jqHighlightedCells[i].removeClass('dsb-drop-highlight');
					}
				}
				$j(this).removeClass('dsb-auto-width');
				//$j(this).trigger('click.selectable');
			}
		});
		
		if(jField.type == 'DICTIONARY') {
			this.initDictionarySelectable();
		}
	}
	
	this.initDictionarySelectable = function() {
		/*if(dictionarySelectableInitialized === true) {
			return;
		}*/
//		console.info('init dic selectable', jqFieldContainer);
		
		jqFieldContainer.children('fieldset').on('mousedown.multisel', 'div.field', function(event) {
			console.info('Mousedown! fieldset > div.field');
			if(selectable) {
				if(jqFieldContainer.hasClass('ui-selected')) {
					$j(this).trigger('select');
					jqFieldContainer.addClass('ui-multi-selected');
				
					return false;
				} else {
					return true;
				}
			} else {
				console.warn('selectable for field', jField.cn, 'is disabled');
			}
		});
		
		
		dictionarySelectableInitialized = true;
	}
	
	this.setSelectable = function(sel) {
		console.warn('setting selectable for', jField.cn, 'to', sel);
		selectable = sel;
	}
	
	this.selectDictionaryField = function(fieldCn, grid, selected, options) {
		this.select(grid, selected, options);
		this.getDsbDictionaryField(fieldCn).select(grid, selected, options);
		if(selected === true) {
			jqFieldContainer.addClass('ui-multi-selected');
		} else {
			jqFieldContainer.removeClass('ui-multi-selected');
		}
	}
	
	this.moveToCell = function(grid, jqDestCell) {
		var parsedCoords, distance, splittedCoords,
			cmd = null, cmdMode = 'fast update',
			srcCellMinWidth = 0;
		
		if(jqCalculatedDestCell) {
			// je�eli pole jest rozci�gni�te na kilka kom�rek, to pe�ny update
			splittedCoords = jField.coords.split(':');
			if(splittedCoords.length > 1 && splittedCoords[0] != splittedCoords[1]) {
				cmdMode = 'update';
			}
			
			cmd = new DsbXmlDocument.Command({cn: jField.cn}, cmdMode);
			cmd.from.coords = jField.coords + '';
			
			parsedCoords = Grid.parseCoords(jField.coords);
			distance = Grid.calculateCellDistance(jqCell, jqCalculatedDestCell);
			if(!parsedCoords.end) {
				parsedCoords.end = {
					colspan: 1,
					rowspan: 1
				}
			}
			srcCellMinWidth = jqCell.css('min-width');
			
			jqFieldContainer.css({'position': '', 'left': '', 'top': ''}).appendTo(jqCalculatedDestCell);
			jqCalculatedDestCell.attr('fieldcn', jField.cn);
			grid.partialNormalize(jqCell, jqCalculatedDestCell, parsedCoords.end.colspan, 
				parsedCoords.end.rowspan, {copyStyle: false});
			jField.coords = Grid.transformCoords('translate', jField.coords, distance.x, distance.y);
			cmd.to.coords = jField.coords + '';
			
			
			jqCell.css({width: '', height: '', 'min-width': '', 'min-height': ''});
			jqCell = jqCalculatedDestCell;
			//jqCell.trigger('unselected');
			//this.updateSelection();
			jqCell.css({'min-width': srcCellMinWidth});
			
			// je�li ostatni wiersz/kolumna, to musimy doda� nowe wiersze/kolumny do grida
			var destRow = Number(jqCalculatedDestCell.attr('row')),
				destCol = Number(jqCalculatedDestCell.attr('column')),
				newCol = 0, newRow = 0;
			if(destCol >= grid.size.cols)
				newCol = 1;
			if(destRow >= grid.size.rows)
				newRow = 1;
			if(newCol || newRow) {
				setTimeout(function() {
					grid.expand(newCol, newRow);
				}, 0);
			}
			
			//jqCell.trigger('selected');
			
			$j('.dsb-drop-highlight').removeClass('dsb-drop-highlight');
			return cmd;
			
		} else {
//			console.info('Could not move to cell', jqDestCell);
			jqFieldContainer.css({'position': 'relative', 'left': 0, 'top': 0});
		}
	}
	
	this.showPlaceholder = function(grid, jqDestCell, xPos, yPos) {
		var parsedCoords = Grid.parseCoords(jField.coords);
		var distance = Grid.calculateCellDistance(jqCell, jqDestCell),
			jqCells = null, i = 0;
		if(!parsedCoords.end) {
			parsedCoords.end = {
				colspan: 1,
				rowspan: 1
			}
		}
		
		if(jqHighlightedCells) {
			for(i = 0; i < jqHighlightedCells.length; i ++) {
				jqHighlightedCells[i].removeClass('dsb-drop-highlight');
			}
		}
		
		if(jqDestCell.attr('id') != jqCell.attr('id')) {
			jqCells = grid.getAvailableCellsAreas(jField.cn, jqDestCell, parsedCoords.end.colspan, parsedCoords.end.rowspan, xPos, yPos);
			if(jqCells && jqCells.length) {
				jqCalculatedDestCell = jqCells[0];
				jqHighlightedCells = jqCells;
				for(i = 0; i < jqCells.length; i ++) {
					jqCells[i].addClass('dsb-drop-highlight');
				}
			} else {
				jqCalculatedDestCell = null;
				jqHighlightedCells = null;
			}
		}
	}
	
	this.select = function(grid, selected, options) {
		var width, height, position, orgCss, cmd,
			containerOuttersWidth, containerOuttersHeight, i;
		var opts = {
			resetPosition: options ? options.resetPosition : true
		}
//		console.warn(opts);
		
		if(selected) {
			selectCounter ++;
//			console.warn('select', selectCounter);
			/*jqFieldContainer.data('orgcss', {
				'width': jqFieldContainer.css('width'),
				'height': jqFieldContainer.css('height'),
				'padding-right': jqFieldContainer.css('padding-right'),
				'position': jqFieldContainer.css('position')
			});*/
			if(selectCounter == 1) {
				jqFieldContainer.revertablecss('save', ['width', 'height', 'padding-right', 'padding-left', 'padding-top', 'padding-bottom', 'position']);
				jqCell.revertablecss('save', ['min-width', 'min-height', 'height']);
			} else {
//				console.trace();
			}
			
			containerOuttersHeight = jqFieldContainer.outerHeight() - jqFieldContainer.height();
//			console.info('outer', containerOuttersHeight);
			// webkit inaczej liczy wysoko�� kom�rki i padding�w
			var cellHeight = jqCell.outerHeight();
			var cellMinWidth = jqCell.width();
			if(webkit) {
				cellHeight -= 7;
				cellMinWidth ++;
			}
			jqCell.css({
				'min-width': cellMinWidth + 'px',
				'min-height': jqCell.height() + 'px',
				'height': cellHeight + 'px'
			});
			
			var fieldHeight = jqCell.height() - containerOuttersHeight + parseInt(jqCell.css('padding-bottom'));
			var fieldWidth = jqFieldContainer.width();
			if(webkit) {
				fieldHeight ++;
			}
			if(opts.resetPosition) {
				jqFieldContainer.css({
					'position': 'absolute',
					'width': fieldWidth + 'px',
					//'padding-right': '+=1',
					'height': fieldHeight + 'px',
					'left': '',
					'top': ''
				});
			}
			
			jqFieldContainer.addClass('ui-selected');
			initResizable(grid);
		} else {
			selectCounter --;
//			console.warn('unselect', selectCounter);
			jqCell.revertablecss('load');
			jqFieldContainer.revertablecss('load');
			jqFieldContainer.css({height: ''});
			
			jqFieldContainer.removeClass('ui-selected');
			unInitResizable();
			jqFieldContainer.css({'position': '', 'left': '', 'top': ''});
			if(dsbFields != null) {
				for(i = 0; i < dsbFields.length; i ++) {
					if(dsbFields[i].isSelected()) {
						console.info('unselect', dsbFields[i].getjField().cn);
						dsbFields[i].unselect();
					}
				}
				jqFieldContainer.removeClass('ui-multi-selected');
			}
		}
	}
	
	this.updateSelection = function() {
		var cellHeight, innerHeight, outtersHeight,
			newHeight;
		
//		console.info('update selection', jField.cn);
		if(jqFieldContainer.hasClass('ui-selected')) {
			jqFieldContainer.revertablecss('delete').css({'position': '', 'width': '', 'height': ''})
				.revertablecss('save', ['width', 'height', 'position']);
			outtersHeight = jqFieldContainer.outerHeight() - jqFieldContainer.height();
			cellHeight = jqCell.outerHeight();
			
			newHeight = cellHeight - outtersHeight + 4; // TODO - zobaczy� dlaczego akurat 4
			jqFieldContainer.css('height', newHeight + 'px'); // 1 to border-bottom-width
		}
	}
	
	this.selectCell = function() {
		jqCell.trigger('selected');
	}
	
	this.unselectCell = function() {
		jqCell.trigger('unselected');
	}
	
	this.addDsbDictionaryField = function(dsbDictionaryField) {
		dsbFields.push(dsbDictionaryField);
	}
	
	this.deleteDsbDictionaryField = function(fieldCn) {
		var i, len, index,
		jFields = jField.dictionary.fields;
		
		for(i = 0, len = dsbFields.length; i < len; i ++) {
			if(dsbFields[i].getjField().cn == fieldCn) {
				index = i;
				break;
			}
		}
		dsbFields.splice(index, 1);
		index = null;
		
		for(i = 0, len = jFields.length; i < len; i ++) {
			if(jFields[i].cn == fieldCn) {
				index = i;
				break;
			}
		}
		jFields.splice(index, 1);
	}
	
	this.deleteDsbDictionaryFields = function() {
		dsbFields = [];
	}

	this.getDsbDictionaryFields = function() {
		return dsbFields;
	}
	
	this.getDsbDictionaryField = function(fieldCn) {
		var i, len;
		
		for(i = 0, len = dsbFields.length; i < len; i ++) {
			if(dsbFields[i].getjField().cn == fieldCn) {
				return dsbFields[i];
			}
		}
		
		return null;
	}
}

