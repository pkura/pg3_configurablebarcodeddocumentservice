<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE>
<html>
<head>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-ui-1.8.custom.min.js"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/codemirror/codemirror.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/codemirror/xml.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/codemirror/fullscreen.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/diff_match_patch.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<ww:url value="'/dwrdockind/editor/DsbXmlEditorPopup.js'"/>?v=<%= Docusafe.getDistTimestamp() %>"></script>
<link rel="stylesheet" type="text/css" href="<ww:url value="'/dwrdockind/editor/codemirror/codemirror.css'" />" />

</head>

<style>
body {
	margin: 0;
}
/*fullscreen {
	display: block;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	z-index: 9999;
}*/
</style>

<body>

<textarea id="xml-textarea">

</textarea>

<script type="text/javascript">
var $j = jQuery.noConflict();
var xmlEditor = new DsbXmlEditorPopup($j('#xml-textarea'));

window.addEventListener('message', function(event) {
	var xml = (new DOMParser()).parseFromString(event.data, 'text/xml');
	
	xmlEditor.assignXML(xml);
	xmlEditor.update();
}, false);


</script>

</body>
</html>