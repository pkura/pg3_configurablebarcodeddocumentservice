DsbXmlDocument = function(uid) {
	// private
	var _this = this,
		jqXmlDoc = null,
		jqXmlFieldMap = {},
		jqXmlDicFieldMap = {},
		fieldValueMap = {},
		fieldTranslateMap = {},
		commands = [],
		commandsHistory = [],
		observer = new DsbObserver(),
		uuid = uid || -1,
		newFieldCounter = 2;
	
	var putjqXmlField = function(fieldCn, dictionaryCn, jqXmlField) {
		var shortFieldCn = fieldCn,
			extFieldCn = fieldCn;
		
		if(dictionaryCn != null && jqXmlDicFieldMap[dictionaryCn] != null) {
			shortFieldCn = shortFieldCn.replace(RegExp('^' + dictionaryCn + '_'), '');
			extFieldCn = dictionaryCn + '_' + shortFieldCn;
			jqXmlDicFieldMap[dictionaryCn][shortFieldCn] = jqXmlField;
		}
		
		jqXmlFieldMap[extFieldCn] = jqXmlField;
	}
	
	var getjqXmlField = function(fieldCn, dictionaryCn) {
		var shortFieldCn = fieldCn,
			jqXmlField = null;
		
		if(dictionaryCn != null && jqXmlDicFieldMap[dictionaryCn] != null) {
			shortFieldCn = shortFieldCn.replace(RegExp('^' + dictionaryCn + '_'), '');
			jqXmlField = jqXmlDicFieldMap[dictionaryCn][shortFieldCn];
		}
		
		if(jqXmlField == null) {
			jqXmlField = jqXmlFieldMap[fieldCn];
		}
		
		return jqXmlField;
	}
	
	var changeFieldCn = function(oldFieldCn, newFieldCn, dictionaryCn) {
		var shortOldFieldCn = oldFieldCn,
			shortNewFieldCn = newFieldCn;
		
		if(dictionaryCn != null) {
			shortOldFieldCn = shortOldFieldCn.replace(RegExp('^' + dictionaryCn + '_'), '');
			shortNewFieldCn = shortNewFieldCn.replace(RegExp('^' + dictionaryCn + '_'), '');
			jqXmlDicFieldMap[dictionaryCn][shortNewFieldCn] = jqXmlDicFieldMap[dictionaryCn][shortOldFieldCn];
			delete jqXmlDicFieldMap[dictionaryCn][shortOldFieldCn];
			
			newFieldCn = DsbXmlDocument.normalizeFieldCn(newFieldCn, dictionaryCn);
			oldFieldCn = DsbXmlDocument.normalizeFieldCn(oldFieldCn, dictionaryCn);
		}
		
		jqXmlFieldMap[newFieldCn] = jqXmlFieldMap[oldFieldCn];
		delete jqXmlFieldMap[oldFieldCn];
		
		
		fieldValueMap[newFieldCn] = fieldValueMap[oldFieldCn];
		delete fieldValueMap[oldFieldCn];
	}
	
	// privileged
	this.init = function(xmlDocument) {
		jqXmlDoc = $j(xmlDocument);
	}
	
	this.dispose = function() {
		observer = null;
		observer = new DsbObserver();
	}
	
	// observer interface
	this.subscribe = observer.subscribe;
	this.unsubscribe = observer.unsubscribe;
	
	this.execCommand = function(cmd, options) {
		//console.info('exec cmd', cmd.mode, cmd, options);
		var opts = {
			history: options ? options.history : true
		}
		if(cmd.from == null)
			throw "Command FROM values cannot be null";
		if(cmd.to == null)
			throw "Command TO values cannot be null";
		
		var jqXmlField = getjqXmlField(cmd.from.cn, cmd.options ? cmd.options.parentFieldCn : null), 
			jqXmlFieldType = null,
			props = null, typeProps = null, customProps = null, key = null, 
			newCmd = null, newFieldCn = null, prevFieldCn = null,
			newjField = null, prefixCn = null;
		switch(cmd.mode.toLowerCase()) {
			case 'select':
//				console.warn('CN:', cmd.from.cn);
//				console.info(jqXmlFieldMap);
				props = _this.getFieldProperties(jqXmlField);
				typeProps = _this.getFieldTypeProperties(jqXmlField);
				customProps = _this.getFieldCustomProperties(jqXmlField);
				console.info('customProps', customProps);
				for(key in props) {
					if(key != 'cn') {
						// cn musi by� a dla s�ownik�w jest z przedrostkiem
						cmd.from[key] = cmd.to[key] = props[key];
					}
				}
				for(key in typeProps) {
					cmd.from[key + '__TYPE'] = cmd.to[key + '__TYPE'] = typeProps[key];
				}
				if(customProps['enum-list']) {
					cmd.from['enum-list__CUSTOM'] = cmd.to['enum-list__CUSTOM'] = customProps['enum-list'];
				}
				if(customProps['pop-up-display'] != null) {
					cmd.from['pop-up-display__CUSTOM'] = cmd.to['pop-up-display__CUSTOM'] = customProps['pop-up-display'];
				}
				if(customProps['search-display'] != null) {
					cmd.from['search-display__CUSTOM'] = cmd.to['search-display__CUSTOM'] = customProps['search-display'];
				}
				if(customProps['multiline'] != null) {
					cmd.from['multiline__CUSTOM'] = cmd.to['multiline__CUSTOM'] = customProps['multiline'];
				}
			break;
			case 'unselect':
				
			break;
			case 'update dictionary':
//				console.info(jqXmlFieldMap);
				jqXmlField = jqXmlFieldMap[cmd.options.dictionaryFieldCn + '_' + cmd.from.cn];
			case 'fast update':
			case 'update':
				// je�li zmieni�o si� cn pola,
				// to uaktualniamy map�
				if(cmd.from.cn != cmd.to.cn) {
					/*jqXmlFieldMap[cmd.to.cn] = jqXmlFieldMap[cmd.from.cn];
					jqXmlField = jqXmlFieldMap[cmd.to.cn];
					fieldValueMap[cmd.to.cn] = fieldValueMap[cmd.from.cn];
					delete jqXmlFieldMap[cmd.from.cn];
					delete fieldValueMap[cmd.from.cn];
					console.dir(jqXmlFieldMap);*/
					changeFieldCn(cmd.from.cn, cmd.to.cn, cmd.options ? cmd.options.parentFieldCn : null);
					jqXmlField = getjqXmlField(cmd.to.cn, cmd.options ? cmd.options.parentFieldCn : null);
				}
				
				/*console.info(jqXmlFieldMap);
				console.warn(cmd);
				console.warn(jqXmlField);
				console.trace();*/
				
				if(cmd.to['length__TYPE'] != null && cmd.to['multiline__CUSTOM'] == null) {
					cmd.to['multiline__CUSTOM'] = cmd.to['length__TYPE'] > 250;
					cmd.from['multiline__CUSTOM'] = !cmd.to['multiline__CUSTOM'];
				} else if(cmd.to['multiline__CUSTOM'] != null) {
					cmd.to['length__TYPE'] = cmd.to['multiline__CUSTOM'] ? 251 : 250;
					cmd.from['length__TYPE'] = jqXmlField.children('type').attr('length') || 0;
				}
								
				for(key in cmd.to) {
					if(key.indexOf('__TYPE') != -1) {
						jqXmlField.children('type').attr(key.replace('__TYPE', ''), cmd.to[key]);
					} else if(key.indexOf('__CUSTOM') != -1) {
						
					} else {
						jqXmlField.attr(key, cmd.to[key]);
					}
				}
				
				// je�li zmieniamy cn pola w s�owniku, to od�wie�amy r�wnie� parametry samego s�ownika
				if(cmd.options && cmd.options.parentFieldCn != null && (cmd.from.cn != cmd.to.cn)) {
					(function() {
						var jqDic = jqXmlFieldMap[cmd.options.parentFieldCn],
							jqDicType = jqDic.children('type'),
							visFields = jqDicType.attr('visible-fields'),
							popupFields = jqDicType.attr('pop-up-fields'),
							fromFieldCn = cmd.from.cn.replace(cmd.options.parentFieldCn + '_', ''),
							idx = null;
						
						visFields = visFields != null ? visFields.split(',') : [];
						popupFields = popupFields != null ? popupFields.split(',') : [];
						
						idx = $j.inArray(fromFieldCn, visFields);
						if(idx != -1) {
							// cn pola znajduje si� w li�cie cn�w p�l widocznych w s�owniku
							visFields[idx] = cmd.to.cn;
						}
						visFields = visFields.join(',');
						
						idx = $j.inArray(fromFieldCn, popupFields);
						if(idx != -1) {
							popupFields[idx] = cmd.to.cn;
						}
						popupFields = popupFields.join(',');
						
						jqDicType.attr('visible-fields', visFields);
						jqDicType.attr('pop-up-fields', popupFields);
						console.info('dictionary field', jqDicType.attr('visible-fields'), visFields, cmd);
						
					})();
				}
				
//				console.info(cmd.mode, jqXmlField);
			break;
			case 'update enum items':
//				console.warn('update enum items');
//				console.warn(cmd);
				(function() {
					var enumItems = cmd.to['enum-list__CUSTOM'],
						jqEnum = jqXmlField.children('type[name=enum]'),
						jqEnumItem = null, enumItem = null, propName = null;
					
					jqEnum.children('enum-item').remove();
					for(i = 0; i < enumItems.length; i ++) {
						enumItem = enumItems[i];
						// jQuery dodaje domy�lny namespace do xml-a
						jqEnumItem = $j(jqXmlDoc[0].createElement('enum-item'));
						for(propName in enumItem) {
							jqEnumItem.attr(propName, enumItem[propName]);
						}
						jqEnum.append(jqEnumItem);
					}
					fieldValueMap[cmd.from.cn] = _this.parseFieldValue['ENUM'](jqXmlField);
				})();
//				console.warn(jqXmlField[0]);
			break;
			case 'update dictionary cns':
				console.warn('update dictionary cns', cmd);
				(function() {
					var jqParent = jqXmlField.parent('type[name=dictionary]'),
						popupFields = jqParent.attr('pop-up-fields'),
						searchFields = jqParent.attr('search-fields')
						i = 0, idx = null;
					
					if(cmd.from['pop-up-display__CUSTOM'] != null) {
						popupFields = popupFields ? popupFields.split(',') : [];
						idx = $j.inArray(jqXmlField.attr('cn'), popupFields);
						if(cmd.to['pop-up-display__CUSTOM'] === false) {
							// usuwamy
							if(idx != -1) {
								popupFields.splice(idx, 1);
							}
						} else {
							// dodajemy
							if(idx == -1) {
								popupFields.push(jqXmlField.attr('cn'));
							}
						}
						console.info(popupFields);
						jqParent.attr('pop-up-fields', popupFields.join(','));
					}
					if(cmd.from['search-display__CUSTOM'] != null) {
						searchFields = searchFields ? searchFields.split(',') : [];
						idx = $j.inArray(jqXmlField.attr('cn'), searchFields);
						if(cmd.to['search-display__CUSTOM'] === false) {
							if(idx != -1) {
								searchFields.splice(idx, 1);
							}
						} else {
							if(idx == -1) {
								searchFields.push(jqXmlField.attr('cn'));
							}
						}
						
						jqParent.attr('search-fields', searchFields.join(','));
					}
				})();
			break;
			case 'refresh':
				// nop	
			break;
			case 'create':
				if(cmd.from.xml != null) {
					jqXmlField = cmd.from.xml;
					newFieldCn = jqXmlField.attr('cn').replace(/\d/, '') + (newFieldCounter++);
				} else {
					jqXmlField = $j(this.createFieldXML(cmd.from));
					jqXmlField.attr('name', cmd.from.cn);
					if(cmd.from.cssClass != null) {
						jqXmlField.attr('cssClass', cmd.from.cssClass);
					}
					newFieldCn = cmd.from.cn;
				}
				jqXmlField.attr('cn', newFieldCn);
				jqXmlFieldType = jqXmlField.children('type');
				
				if(jqXmlField.attr('column') == null) {
					jqXmlField.attr('column', newFieldCn);
				}
				if(jqXmlFieldType.attr('name').toUpperCase() == 'DICTIONARY' && jqXmlFieldType.attr('tableName') == null) {
					jqXmlFieldType.attr('tableName', newFieldCn);
				}
				jqXmlDoc.children('doctype').children('fields').append(jqXmlField);
				if(jqXmlFieldMap[newFieldCn] != null) {
					throw 'Field exists';
				}
				jqXmlFieldMap[newFieldCn] = jqXmlField;
				
				newjField = _this.parseField(jqXmlField).jField;
				newjField.status = 'ADD';
				
				props = _this.getFieldProperties(jqXmlField);
				typeProps = _this.getFieldTypeProperties(jqXmlField);
				customProps = _this.getFieldCustomProperties(jqXmlField);
//				console.info('props', props);
				newCmd = new DsbXmlDocument.Command({cn: newjField.cn}, 'update');
				for(key in props) {
					if(key != 'cn') {
						// cn musi by� a dla s�ownik�w jest z przedrostkiem
						newCmd.from[key] = newCmd.to[key] = props[key];
					}
				}
				for(key in typeProps) {
					newCmd.from[key + '__TYPE'] = newCmd.to[key + '__TYPE'] = typeProps[key];
				}
				if(customProps['enum-list']) {
					newCmd.from['enum-list__CUSTOM'] = newCmd.to['enum-list__CUSTOM'] = customProps['enum-list'];
				}
				//debugger;
				_this.execCommand(newCmd);
				//observer.publish('execute command', newCmd);
			break;
			case 'create in dictionary':
				console.warn('create in dictionary', cmd);
				jqXmlField = cmd.from.xml;
				jqXmlField.removeAttr('coords');
				newFieldCn = jqXmlField.attr('cn').replace(/\d/, '') + (newFieldCounter++);
				jqXmlField.attr('cn', newFieldCn);
				if(jqXmlField.attr('column') == null) {
					jqXmlField.attr('column', newFieldCn);
				}
				
				if(cmd.options.prevFieldCn != null) {
//					console.info('prevFieldCn', cmd.options.prevFieldCn);
					prevFieldCn = cmd.options.prevFieldCn.replace(cmd.options.dictionaryFieldCn + '_', '');
//					console.info(jqXmlDoc.find('field[cn=' + cmd.options.dictionaryFieldCn + ']').children('type').children('[cn=' + prevFieldCn + ']'));
					jqXmlDoc.find('field[cn=' + cmd.options.dictionaryFieldCn + ']')
						.children('type').children('[cn=' + prevFieldCn + ']').after(jqXmlField);
				} else {
					jqXmlDoc.find('field[cn=' + cmd.options.dictionaryFieldCn + ']').children('type').append(jqXmlField);
				}
				//jqXmlFieldMap[cmd.options.dictionaryFieldCn + '_' + newFieldCn] = jqXmlField;
				putjqXmlField(newFieldCn, cmd.options.dictionaryFieldCn, jqXmlField);
//				console.info('enf', jqXmlFieldMap);
				
				newjField = _this.parseField(jqXmlField).jField;
				newjField.status = 'ADD';
//				console.info('create in dictionary, field.cn', newjField.cn);
				newCmd = new DsbXmlDocument.Command({cn: newjField.cn}, 'update dictionary');
				newCmd.options = cmd.options;
				_this.execCommand(newCmd);
				
				newCmd = new DsbXmlDocument.Command({cn: newjField.cn}, 'update dictionary cns');
				newCmd.options = cmd.options;
				newCmd.options.parentFieldCn = cmd.options.dictionaryFieldCn;
				newCmd.from['pop-up-display__CUSTOM'] = false;
				newCmd.to['pop-up-display__CUSTOM'] = true;
				_this.execCommand(newCmd);
			break;
			case 'delete':
				console.warn('deleting');
				console.warn(jqXmlField[0]);
				jqXmlField.remove();
				delete jqXmlFieldMap[cmd.from.cn];
				
				newCmd = new DsbXmlDocument.Command({}, 'refresh');
				_this.execCommand(newCmd);
			break;
			case 'delete from dictionary':
//				console.warn('deleting', cmd.from.cn, cmd.options.dictionaryFieldCn);
//				console.warn(jqXmlField[0]);
				
				/*jqXmlDoc.find('field[cn=' + cmd.options.dictionaryFieldCn + ']')
					.children('type').children('[cn=' + cmd.from.cn + ']').remove();*/
				
				jqXmlField.remove();
				delete jqXmlFieldMap[cmd.from.cn];
				
				// usuwamy z tablicy visible-fields, popup, prompt, search i dockind-search
				(function() {
					var jqXmlType = jqXmlDoc.find('field[cn=' + cmd.options.dictionaryFieldCn + ']').children('type'),
						shortFieldCn = cmd.from.cn.replace(cmd.options.dictionaryFieldCn + '_', ''),
						attrs = ['visible-fields', 'pop-up-fields', 'prompt-fields', 'search-fields', 'dockind-search-fields'],
						fieldCns, i, j, len, index, attrName;
					
					for(j = 0; j < attrs.length; j ++) {
						attrName = attrs[j];
					
						fieldCns = jqXmlType.attr(attrName).split(',');
						for(i = 0, len = fieldCns.length; i < len; i ++) {
							if(fieldCns[i] == shortFieldCn) {
								index = i;
								fieldCns.splice(index, 1);
								jqXmlType.attr(attrName, fieldCns.join(','));
								break;
							}
						}
					}
				})();
				
				newCmd = new DsbXmlDocument.Command({}, 'refresh');
				_this.execCommand(newCmd);
			break;
            case 'update processes spring':
                (function() {
                    jqProcesses = jqXmlDoc.children('doctype').children('processes').children('spring-process-definition');
                    jqProcess = jqProcesses.eq(0);
                    jqProcess.text(cmd.to);
                })();
            break;
			case 'update processes':
				(function() {
					var i = 0, len = cmd.to.length, j = 0, lenj = 0,
						jqProcesses = jqXmlDoc.children('doctype').children('processes').children('process'),
						newProc = null, oldProc = null, propName = null, jqProcess = null,
						jqParam = null, params = null;
					
					for(i = 0; i < len; i ++) {
						newProc = cmd.to[i];
						oldProc = cmd.from[i];
						jqProcess = jqProcesses.eq(i);
						for(propName in newProc) {
							if(newProc[propName] != oldProc[propName]) {
								switch(propName) {
								case 'name':
								case 'creation':
									jqProcess.attr(propName, newProc[propName]);
								break;
								case 'view':
								case 'locator':
								case 'logic':
									jqProcess.children(propName).text(newProc[propName]);
								break;
								case 'params':
									jqProcess.children('params').children('param').remove();
									params = newProc.params;
									for(j = 0, lenj = params.length; j < lenj; j ++) {
										// jQuery dodaje domy�lny namespace do xml-a
										jqParam = $j(jqXmlDoc[0].createElement('param'));
										jqParam.attr('name', params[j]['param-name']).text(params[j]['param-value']);
										jqProcess.children('params').append(jqParam);
									}
								break;
								}
							}
						}
					}
				})();
			break;
			case 'insert row':
				console.warn('insert', cmd.from.coords);
				(function(cmd) {
					var rowCoords = Grid.parseCoords(cmd.from.coords),
						fieldCn = null, fieldCoords = null, cns2move = [],
						i = 0, cmd = null, oldCoords = null, newCoords = null;
					
					rowCoords.start.row = Number(rowCoords.start.row);
					for(fieldCn in jqXmlFieldMap) {
						fieldCoords = jqXmlFieldMap[fieldCn].attr('coords');
						if(fieldCoords != null) {
							fieldCoords = Grid.parseCoords(fieldCoords);
							fieldCoords.start.row = Number(fieldCoords.start.row);
							if(fieldCoords.start.row >= rowCoords.start.row) {
								cns2move.push(fieldCn);
							}
						}
					}
					
					for(i = 0; i < cns2move.length; i ++) {
						fieldCn = cns2move[i];
						oldCoords = jqXmlFieldMap[fieldCn].attr('coords');
						newCoords = Grid.transformCoords('translate', oldCoords, 0, 1);
						
						cmd = new DsbXmlDocument.Command({cn: fieldCn, coords: oldCoords}, 'update');
						cmd.to.coords = newCoords;
						_this.execCommand(cmd);
					}
				})(cmd);
			break;
			default:
				throw 'Unknown Command mode ' + mode;
		}

		/**
		 * TODO
		 * Obs�uga historii - cofnij/ponow
		 */
		/*commands.push(cmd);
		console.info('commands.length', commands.length);
		if(opts.history) {
			commandsHistory.push(cmd);
		}*/
//		console.info('exec cmd', cmd);
		observer.publish('execute command', cmd);
	}
	
	this.undo = function() {
		if(commands == null || commands.legth == 0)
			return null;
		
		var lastCmd = commands.pop(),
			revertedLastCmd = null;
		revertedLastCmd = new DsbXmlDocument.Command();
		revertedLastCmd.mode = lastCmd.mode;
		revertedLastCmd.from = lastCmd.to;
		revertedLastCmd.to = lastCmd.from;
//		console.warn(revertedLastCmd);
		_this.execCommand(revertedLastCmd);
		commands.pop();
	}
	
	this.refresh = function() {
		var cmd = new DsbXmlDocument.Command(null, 'refresh');
		observer.publish('execute command', cmd);
	}
	
	this.getXML = function() {
		return jqXmlDoc[0];
	}
	
	this.parse = function() {
		var i, len, jField;
		var xmlFields = jqXmlDoc.children('doctype').children('fields').children('field'),
			jFields = [], parsedField;
		
		for(i = 0, len = xmlFields.length; i < len; i ++) {
			parsedField = _this.parseField(xmlFields[i]);
			jFields[i] = parsedField.jField;
			jField = jFields[i];
			
			jqXmlFieldMap[jField.cn] = $j(xmlFields[i]);
			fieldTranslateMap[jField.cn] = parsedField.translateMap;
			fieldValueMap[jField.cn] = parsedField.value;
		}
		return jFields;
	}
	
	this.parseProcesses = function() {
		var i, len, jqXmlProc;
		var xmlProcs = jqXmlDoc.children('doctype').children('processes').children('process'),
			procs = [], proc = null;
		
		for(i = 0, len = xmlProcs.length; i < len; i ++) {
			jqXmlProc = $j(xmlProcs[i]);
			proc = {};
			proc.name =  jqXmlProc.attr('name');
			proc.creation = jqXmlProc.attr('creation');
			proc.view = jqXmlProc.children('view').text();
			proc.locator = jqXmlProc.children('locator').text();
			proc.logic = jqXmlProc.children('logic').text();
			
			proc.params = [];
			jqXmlProc.children('params').children('param').each(function() {
				var jqThis = $j(this);
				proc.params.push({
					name: jqThis.attr('name'),
					value: jqThis.text()
				});
			});
			
			procs.push(proc);
		}
		
		return procs;
	}
	
	/**
	 * Zwraca jField.dictionary na podstawie xmla
	 */
	this.parseDictionary = function(jFieldDic, xmlField) {
		var jqXmlField = $j(xmlField),
			jqXmlType = jqXmlField.children('type'),
			jqXmlDicFields = jqXmlType.find('field'),
			jFields = [], jField = null, shortCns = [];
		
		for(i = 0, len = jqXmlDicFields.length; i < len; i ++) {
			jFields.push(this.parseField(jqXmlDicFields[i]).jField);
		}
		
		var dic = {};
		var dicPrefix = jFieldDic.cn + '_';
		var dicPopUpFieldsCns = DsbUtils.addPrefix((jqXmlType.attr('pop-up-fields') || '').toUpperCase().split(','), dicPrefix);
		
		dic.fields = [];
		dic.popUpFields = []; 
		
		dic.name = jFieldDic.cn;
		dic.visibleFieldsCns = 		DsbUtils.addPrefix((jqXmlType.attr('visible-fields') || '').split(','), dicPrefix);
		dic.searchableFieldsCns = 	DsbUtils.addPrefix((jqXmlType.attr('search-fields') || '').split(','), dicPrefix);
		dic.promptFieldsCns = 		DsbUtils.addPrefix((jqXmlType.attr('prompt-fields') || '').split(','), dicPrefix);
		dic.popUpButtons = 			(jqXmlType.attr('popUpButtons') || 'doAdd,doEdit,doRemove,doSelect,doClear').split(',');
		dic.tableView = 			jqXmlType.attr('asTable') == 'true';
		dic.multiple = 				jqXmlType.attr('multiple') == 'true';
		dic.cantUpdate = 			jqXmlField.attr('cantUpdate') == 'true';
		dic.onlyPopUpCreate =		jqXmlField.attr('onlyPopUpCreate') == 'true';
		//dic.showDictionaryPopup = 	jqXmlType.attr('pop-up') != 'false';
		dic.dicButtons = 			jqXmlType.attr('dictionary-buttons') != null ? jqXmlType.attr('dictionary-buttons').split(',') : [];
		
		for(i = 0, len = jFields.length; i < len; i ++) {
			jField = jFields[i];
			shortCns.push(jField.cn);
			jField.cn = dic.name + '_' + jField.cn;
			jField.kind = 'DICTIONARY_FIELD';
			dic.fields.push(jField);
			
			if($j.inArray(jField.cn, dicPopUpFieldsCns) != -1) {
				dic.popUpFields.push(jField);
			}
		}
		
		jqXmlDicFieldMap[dic.name] = {};
		for(i = 0, len = jqXmlDicFields.length; i < len; i ++) {
//			console.info(jFields[i].cn);
			jqXmlDicFieldMap[dic.name][shortCns[i]] = jqXmlFieldMap[jFields[i].cn] = $j(jqXmlDicFields[i]);
		}
		return dic;
	}
	
	this.getFieldValue = function(cn) {
		return fieldValueMap[cn];
	}
	
	this.reparseField = function(fieldCn) {
		var parsedField = _this.parseField(jqXmlFieldMap[fieldCn]);
		parsedField.jField.status = 'CHANGED';
		fieldValueMap[fieldCn] = parsedField.value;
		
		return parsedField;
	}
	
	this.getFieldXML = function(fieldCn) {
		return jqXmlFieldMap[fieldCn].clone();
	}
	
	/**
	 * Tworzy nowy fragment xmla dla pola
	 */
	this.createFieldXML = function(params) {
		var jqXmlField = $j(jqXmlDoc[0].createElement('field')),
			jqXmlType = $j(jqXmlDoc[0].createElement('type'));
		
		jqXmlField.attr('cn', params.cn).attr('name', params.name || params.cn).attr('coords', params.coords);
		jqXmlType.attr('name', params.name__TYPE);
		jqXmlField.append(jqXmlType);
		
		if(params.name__TYPE == 'enum' && params['enum-list__CUSTOM'] != null) {
			(function() {
				var enumItems = params['enum-list__CUSTOM'],
					enumItem = null, i = 0, jqEnumItem = null;
				
				for(i = 0; i < enumItems.length; i ++) {
					enumItem = enumItems[i];
					jqEnumItem = $j(jqXmlDoc[0].createElement('enum-item'));
					if(enumItem.cn != null) 
						jqEnumItem.attr('cn', enumItem.cn);
					if(enumItem.title != null) 
						jqEnumItem.attr('title', enumItem.title);
					if(enumItem.title_en != null)
						jqEnumItem.attr('title_en', enumItem.title_en);
					if(enumItem.id != null)
						jqEnumItem.attr('id', enumItem.id);
					
					jqXmlType.append(jqEnumItem);
				}
			})();
		}
		
		return jqXmlField[0];
	}
	
	this.debug = function() {
		return {
			jqXmlDoc: jqXmlDoc,
			jqXmlFieldMap: jqXmlFieldMap,
			jqXmlDicFieldMap: jqXmlDicFieldMap,
			fieldValueMap: fieldValueMap,
			fieldTranslateMap: fieldTranslateMap,
			commands: commands,
			commandsHistory: commandsHistory,
			observer: observer,
			uuid: uuid
		}
	}
	
	a = {
		jqXmlFieldMap: jqXmlFieldMap,
		jqXmlDicFieldMap: jqXmlDicFieldMap
	}
}


DsbXmlDocument.Command = function(constParams, mode) {
	//private
	var key;
	
	// privileged
	this.from = null;
	this.to = null;
	this.mode = mode || 'update';
	this.options = null;
	
	// constructor
	if(constParams != null) {
		this.from = {};
		this.to = {};
		for(key in constParams) {
			this.from[key] = this.to[key] = constParams[key];
		}
	}
}

DsbXmlDocument.normalizeFieldCn = function(fieldCn, dictionaryName, options) {
	var skipDicName = options ? options.skipDicName : false;
	if(skipDicName == false && fieldCn.match('^' + dictionaryName + '_') == null) {
		return dictionaryName + '_' + fieldCn;
	} else if(skipDicName && dictionaryName != null) {
		return fieldCn.replace(RegExp('^' + dictionaryName + '_'), '');
	}
	return fieldCn;
}

DsbXmlDocument.prototype.getFieldProperties = function(xmlField) {
	var jqXmlField = $j(xmlField), props = {},
		i = 0, attrs = jqXmlField[0].attributes, attr = null;
	
	for(i = 0; i < attrs.length; i ++) {
		attr = attrs[i];
		props[attr.name] = attr.value;
	}
	
	return props;
}

DsbXmlDocument.prototype.getFieldTypeProperties = function(xmlField) {
	var jqXmlField = $j(xmlField), props = {},
		i = 0, attrs = jqXmlField.children('type')[0].attributes, attr = null;
	
	for(i = 0; i < attrs.length; i ++) {
		attr = attrs[i];
		props[attr.name] = attr.value;
	}
	return props;
}

DsbXmlDocument.prototype.getFieldCustomProperties = function(xmlField, opts) {
	var jqXmlField = $j(xmlField), 
		jqEnumItems = jqXmlField.children('type').children('enum-item'),
		enumItems = [], i = 0, attr = null, enumItem = null;
	
	jqEnumItems.each(function() {
		enumItem = {};
		for(i = 0; i < this.attributes.length; i ++) {
			enumItem[this.attributes[i].name] = this.attributes[i].value;
		}
		enumItems.push(enumItem);
	});
	
	var jqXmlParent = jqXmlField.parent('type[name=dictionary]'),
		popupFieldsCns = jqXmlParent.attr('pop-up-fields'),
		popupDisplay = jqXmlParent.length > 0 ? false : null;
	
	if(popupFieldsCns != null) {
		popupFieldsCns = popupFieldsCns.split(',');
		for(i = 0; i < popupFieldsCns.length; i ++) {
			if(popupFieldsCns[i] == jqXmlField.attr('cn')) {
//				console.info('FOUND!');
				popupDisplay = true;
				break;
			}
		}
	}
	
	var searchFieldsCns = jqXmlParent.attr('search-fields'),
		searchDisplay = jqXmlParent.length > 0 ? false : null;
		
	if(searchFieldsCns != null) {
		searchFieldsCns = searchFieldsCns.split(',');
		for(i = 0; i < searchFieldsCns.length; i ++) {
			if(searchFieldsCns[i] == jqXmlField.attr('cn')) {
				searchDisplay = true;
				break;
			}
		}
	}
	
	var jqType = jqXmlField.children('type'),
		multiline = null;
	
	if(jqType.attr('name').toUpperCase() == 'STRING') {
		multiline = jqType.attr('length') > 250; 
	}
	
	return {
		'enum-list': enumItems,
		'pop-up-display': popupDisplay,
		'search-display': searchDisplay,
		'multiline': multiline
	}
}

/**
 * Zwraca jField, translateMap i value na podstawie fragmentu xmla
 */
DsbXmlDocument.prototype.parseField = function(xmlField) {
	var f = new DsbUtils.FieldTemplate(),
		jqXmlField = $j(xmlField),
		translateMap = null;
	
	f.cn = 					jqXmlField.attr('cn');
	f.label = 				jqXmlField.attr('name');
	f.hidden = 				jqXmlField.attr('hidden') == 'true';
	f.required = 			jqXmlField.attr('required') == 'true';
	f.coords = 				jqXmlField.attr('coords');
	f.readonly = 			jqXmlField.attr('readonly') == 'true';
	f.disabled = 			jqXmlField.attr('disabled') == 'true';
	f.submit = 				jqXmlField.attr('submit') == 'true';
	f.format =				jqXmlField.attr('format') || null;
	f.cssClass = 			jqXmlField.attr('cssClass') || '';
	
	var translatedType = false;
	var jqXmlType = jqXmlField.children('type');
	var type = (jqXmlType.attr('name') || '').toUpperCase();
	if(DsbUtils.FieldTypeLookup[type]) {
		translateMap = {
			type: {
				from: type.toUpperCase(), 
				to: null
			}
		};
		type = DsbUtils.FieldTypeLookup[type];
		if(Object.prototype.toString.call(type) == '[object Function]') {
			type = type(jqXmlField, jqXmlType);
		}
		translateMap.type.to = type.toUpperCase();
		translatedType = true;
	}
	
	f.type = 				type;
	f.autocomplete = 		jqXmlType.attr('auto') == 'true';
	f.autocompleteRegExp = 	jqXmlType.attr('autocompleteRegExp') || null;
	f.column = 				jqXmlType.attr('dwr-column') || 0;
	f.fieldset = 			jqXmlType.attr('dwr-fieldset') || 'default';
	
	// String o d�ugo�ci > 250, to textarea
	if(f.type.toUpperCase() == 'STRING' && jqXmlType.attr('length') > 250) {
		f.type = 'TEXTAREA';
	}
	
	// je�li nie ma wsp�rz�dnych pola, to wstawiamy pola od lewej do prawej
	// pole nie mo�e by� polem s�ownikowym
	if(!f.coords && jqXmlField.parent('fields').length > 0) {
		if(!this.iterCoords) {
			this.iterCoords = {col: 1, row: 1};
		}
		
		f.coords = DsbUtils.translateCoords(this.iterCoords);
		jqXmlField.attr('coords', f.coords);
		this.iterCoords.col ++;
		if(this.iterCoords.col > 5) {
			this.iterCoords.col = 1;
			this.iterCoords.row ++;
		}
	}
	
	if(type == 'DICTIONARY') {
		f.dictionary = this.parseDictionary(f, jqXmlField);
	}
	
	var val = '', fieldType = f.type.toUpperCase();
		
	if(translateMap && translateMap.type) {
		fieldType = translateMap.type.from;
	}
	if(this.parseFieldValue[fieldType] != null) {
		val = this.parseFieldValue[fieldType](jqXmlField);
	}
	
	return {
		jField: f,
		translateMap: translateMap,
		value: val
	}
}

DsbXmlDocument.prototype.parseFieldValue = {
	'ENUM': function(xmlField) {
		var jqXmlType = $j(xmlField).children('type'),
			val = { 
				allOptions: [{'': '-- wybierz --'}], 
				selectedOptions: []
			};
		
		jqXmlType.children('enum-item').each(function() {
			var fieldVal = {};
			fieldVal[$j(this).attr('id') + ''] = $j(this).attr('title');
			
			val.allOptions.push(fieldVal);
		});
		return val;
	},
	'DATABASE': function() {
		return {
			allOptions: [{'': '-- wybierz --'}, {'1': 'Baza danych'}],
			selectedOptions: []
		}
	},
	'BUTTON': function(xmlField) {
		var jqXmlType = $j(xmlField).children('type');
		return {
			label: jqXmlType.attr('field-label') || 'BRAK',
			value: jqXmlType.attr('value') || 'BRAK'
		}
	}
	/*,
	'DICTIONARY': function(itemId) {
		var i, len, dsbField, cn;
		var vals = {};
		
		if(!itemId) {
			for(i = 0, len = dsbFields.length; i < len; i ++) {
				dsbField = dsbFields[i];
				cn = dsbField.getCn();
				if(jField.dictionary.multiple) {
					cn = cn.replace(/_\d/, '');
				} 
				vals[cn] = dsbField.getValue();
			}
		}
		
		return vals;
	}*/
}

