DsbNotification = (function() {
	// private 'static'
	var jqNotif = $j('<div/>').addClass('dsb-notification');
	var cssClass = '', hTimeout = null;
	
	var init = function() {
		$j('body').prepend(jqNotif);
	}
	init();
	
	return {
		pushMessage: function(message, params) {
			if(hTimeout != null) {
				clearTimeout(hTimeout);
				jqNotif.removeClass('dsb-notification-visible');
				hTimeout = null;
			}
			
			jqNotif.html(message);
			var cssClass = '';
			switch(params.type) {
				case 'error':
					cssClass = 'dsb-notification-error';
				break;
				case 'info':
				default:
					cssClass = 'dsb-notification-info'
			}
			jqNotif.removeClass('dsb-notification-error dsb-notification-info').addClass(cssClass);
			jqNotif.addClass('dsb-notification-visible');
			if(params.type != 'error') {
				hTimeout = setTimeout(function() {
					jqNotif.removeClass('dsb-notification-visible');
				}, 5000);
			}
		},
		translateXmlError: function(xmlDoc) {
			var str = (new XMLSerializer()).serializeToString(xmlDoc);

			return str;
		},
		showOverlayMsg: function(message, params) {
			$j('.dwrDockindContainer').addClass('dsb-overlay-notification');
		},
		hideOverlayMsg: function(params) {
			$j('.dwrDockindContainer').removeClass('dsb-overlay-notification');
		}
	}
	
})();