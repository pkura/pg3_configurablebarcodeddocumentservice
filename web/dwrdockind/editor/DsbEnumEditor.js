DsbEnumEditor = function(params) {
	// private
	var dsbSchemaDocument = null,
		enumProperties = null;
		jqEnumEditor = null,
		jqTbody = null;
	
	var draw = function() {
		var props = dsbSchemaDocument.getEnumItemProperties(),
			allProps = DsbEnumEditor.properties,
			str = '', propName = null,
			_this = this;
		jqEnumEditor = $j('<div/>').addClass('dsb-enum-editor');
		
		/*jqTitle = $j('<div/>').addClass('dsb-enum-editor-title').html('Edycja listy opcji');
		jqEnumEditor.append(jqTitle);*/
		
		str += '<table><thead><tr>';
		str += '<th></th>'
		for(propName in props) {
			str += '<th><textarea readonly="readonly">' + allProps[propName].title + '</textarea></th>';
		}
		str += '<th></th>';
		str += '</tr></thead><tbody></tbody></table>';
		
		jqEnumEditor.append(str);
		$j('body').append(jqEnumEditor);
		jqTbody = jqEnumEditor.children('table').children('tbody');
		
		jqButtons = $j('<div/>');
		enumProperties = props;
		
		jqEnumEditor.dialog({
			autoOpen: false,
			width: 600 + (webkit ? 2 : 0),
			zIndex: 9999,
			buttons: [
	        {
	        	cn: 'dsb-enum-editor-cancel',
	        	text: 'Anuluj',
	        	click: function() {
	        		  jqEnumEditor.trigger('cancel');
	        	}
	        },
			{
				cn: 'dsb-enum-editor-ok',
				text: 'OK',
				click: function() {
					var vals = [];
					jqTbody.children('tr').each(function() {
						var rowVal = {}, emptyItem = true, key = null;
						$j(this).find('input[type=text]').each(function() {
							rowVal[this.name] = this.value;
						});
						for(key in rowVal) {
							if(rowVal[key] != '') {
								emptyItem = false;
								break;
							}
						}
						if(emptyItem == false) {
							vals.push(rowVal);
						}
					});
					jqEnumEditor.data('values', vals);
					jqEnumEditor.trigger('save');
				}
			}],
			open: function() {
				$j('[cn=dsb-enum-editor-ok]').focus();
			}
		})
	}
	
	// privileged
	this.init = function(dsbSchemaDoc) {
		dsbSchemaDocument = dsbSchemaDoc;
		draw();
	}
	
	this.update = function(values) {
		var str = '', allProps = DsbEnumEditor.properties,
			enumProps = enumProperties, vals = values.slice(0);
		var i, len, propName;
		
		vals.push({});
		for(i = 0, len = vals.length; i < len; i ++) {
			str += '<tr>';
			str += '<td class="ui-state-highlight"><div class="ui-icon dsb-enum-editor-move-ico"></div></td>';
			for(propName in enumProps) {
				str += '<td>';
				str += '<input type="text" value="' + ((vals[i])[propName] || '') + '" '
					+ 'name="' + propName + '" '
					+ 'placeholder="' + allProps[propName].title + '" />';
				/*str += '<textarea name="' + propName + '" >';
				str += ((vals[i])[propName] || '');
				str += '</textarea>';*/
				str += '</td>';
			}
			str += '<td><div class="smallIcons dsb-enum-editor-delete-row"></div>';
			str += '</tr>';
		}
		jqTbody.html(str);
		
		jqTbody.off('click.delete-row').on('click.delete-row', 'tr td .dsb-enum-editor-delete-row', function() {
			$j(this).parent('td').parent('tr').remove();
		}).off('keydown').on('keydown', 'tr td input', function(event) {
			var jqRow = $j(this).parent('td').parent('tr'),
				jqNext = jqRow.next(),
				jqPrev = jqRow.prev();
			switch(event.keyCode) {
				case KEYS.ENTER:
					if(jqNext.length == 0) {
						jqRow.trigger('createrow').trigger('move', ['down', jqRow.find('input').index(this)]);
					}
					return false;
				break;
				case KEYS.ARROW_UP: 
					jqRow.trigger('move', ['up', jqRow.find('input').index(this)]);
				break;
				case KEYS.ARROW_DOWN:
					jqRow.trigger('move', ['down', jqRow.find('input').index(this)]);
				break;
				case KEYS.ARROW_LEFT:
					jqRow.trigger('move', ['left', jqRow.find('input').index(this)]);
				break;
				case KEYS.ARROW_RIGHT:
					jqRow.trigger('move', ['right', jqRow.find('input').index(this)]);
				break;
			}
		}).off('createrow').on('createrow', 'tr', function() {
			var jqNewRow = $j(this).clone();
			jqNewRow.find('input').val('');
			$j(this).after(jqNewRow);
			jqNewRow.find('input:first').focus();
			
			return false;
		}).off('move').on('move', 'tr', function(event, direction, index) {
			var jqThis = $j(this), jqDestRow = null;
			switch(direction) {
				case 'up':
					jqDestRow = jqThis.prev();
				break;
				case 'down':
					jqDestRow = jqThis.next();
				break;
				case 'left':
					jqDestRow = jqThis;
					index --;
				break;
				case 'right':
					jqDestRow = jqThis;
					index ++;
				break;
			}
			if(jqDestRow.length && jqDestRow.find('input')[index]) {
				jqDestRow.find('input')[index].focus();
			}
		});
		jqTbody.sortable('destroy').sortable({
			axis: 'y',
			cancel: 'input, .dsb-enum-editor-delete-row',
			containment: 'parent'
		});
	}
	
	this.show = function(opts, onSave, onCancel) {
		/*jqEnumEditor.css({
			left: opts.left + 'px',
			top: opts.top + 'px'
		}).show();*/
		jqEnumEditor.dialog('option', 'title', opts.title).dialog('open');
		
		jqEnumEditor.unbind('save').bind('save', function() {
			onSave(jqEnumEditor.data('values'));
		});
		
		jqEnumEditor.unbind('cancel').bind('cancel', function() {
			onCancel();
		});
	}
	
	this.hide = function() {
		jqEnumEditor.dialog('close');
	}
}

DsbEnumEditor.properties = {
	'id': {
		title: 'warto�� (id)'
	},
	'cn': {
		title: 'nazwa kodowa'
	},
	'title': {
		title: 'etykieta'
	},
	'title_en': {
		title: 'etykieta angielska'
	},
	'arg1': {
		title: 'Opcjonalny argument #1'
	},
	'arg2': {
		title: 'Opcjonalny argument #2'
	},	
	'arg3': {
		title: 'Opcjonalny argument #3'
	},	
	'arg4': {
		title: 'Opcjonalny argument #4'
	},
	'arg5': {
		title: 'Opcjonalny argument #5'
	}
}