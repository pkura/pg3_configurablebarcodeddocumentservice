DsbMenu = function() {
	// private
	
	// privileged
	this.init = function() {
		$j('#menu').clickMenu({
			onclick: function() {
				return false;
			}
		});
		
		return this;
	}
	
	this.show = function() {
		$j('#menu').fadeIn();
	}
	
	this.addCommand = function(selector, func) {
		$j(selector).bind('click.command', function() {
			var _this = this, params = {};
			var jqThis = $j(this);
			
			params.disabled = jqThis.attr('itemdisabled') === 'true'; 			
			
			setTimeout(function() {
				func(_this, params);
			}, 0);

			$j('#menu').trigger('closemenu');
			return false;
		});
	}
	
	this.enableCommand = function(selector) {
		$j(selector).removeClass('dsb-menu-item-disabled').attr('itemdisabled', 'false');
	}
	
	this.disableCommand = function(selector) {
		$j(selector).addClass('dsb-menu-item-disabled').attr('itemdisabled', 'true');
	}
	
	this.isDisabled = function(selector) {
		return $j(selector).attr('itemdisabled') == 'true';
	}
	
	this.toggleCommandCheckbox = function(selector, visible) {
		$j(selector).attr('itemchecked', visible);
	}
	
	this.isChecked = function(selector) {
		return $j(selector).attr('itemchecked') === 'true';
	}
}

