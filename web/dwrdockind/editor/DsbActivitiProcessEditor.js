DsbActivitiProcessEditor = function(params) {

    var allProcesses = null,
        jqSelect = null,
        initialProcesses = null;

    this.init = function(allProcs, dsbSchemaDoc) {
        allProcesses = allProcs;
        draw();
    }

    this.show = function(opts, onSave, onCancel) {
        jqEditor.dialog('open');
        jqEditor.unbind('save').bind('save', function() {
            onSave(initialProcesses,jqSelect.val());
        }).unbind('cancel').bind('cancel', function() {
            onCancel();
        });
    }

    this.hide = function() {
        jqEditor.dialog('close');
    }

    this.update = function(processes) {
        initialProcesses = processes;
    }

    var draw = function() {
        jqEditor = $j('<div/>').addClass('dsb-flex-table-editor').attr('title', 'Lista proces�w');
        str = '';
        str += '<select name="process">';
        for(proc in allProcesses) {
            proc = allProcesses[proc];
            str += '<option value="' + proc.name + '">' + proc.name + '</option>';
        }
        //<option value="A">A</option>
        str += '</select>';
        jqEditor.append(str)
        $j('body').append(jqEditor);
        jqSelect = jqEditor.children('select');

        jqEditor.dialog({
            autoOpen: false,
            zIndex: 9999,
            width: 400,
            buttons: [
                {
                    cn: 'dsb-process-editor-cancel',
                    text: 'Anuluj',
                    click: function() {
                        jqEditor.trigger('cancel');
                    }
                },
                {
                    cn: 'dsb-process-editor-ok',
                    text: 'OK',
                    click: function() {
                        jqEditor.trigger('save-values');
                        jqEditor.trigger('save');
                    }
                }
            ],
            open: function() {
                $j('dsb-process-editor-ok').focus();
            }
        });
    }

    this.getProcessesRequiredFields = function(procs, callback) {}
}