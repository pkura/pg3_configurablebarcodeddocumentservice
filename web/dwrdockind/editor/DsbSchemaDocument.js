DsbSchemaDocument = function() {
	// private
	var _this = this,
		jqXmlDoc = null,
		fieldProperties = null,
		typeProperties = null,
		enumItemProperties = null,
		processProperties = null;
	
	// privileged
	this.init = function(xmlDocument) {
		jqXmlDoc = $j(xmlDocument);
	}
	
	this.getXML = function() {
		return jqXmlDoc[0];
	}
	
	this.parse = function() {
		if(jqXmlDoc == null)
			throw 'No schema loaded';
		
		fieldProperties = this.parseField(jqXmlDoc);
		typeProperties = this.parseType(jqXmlDoc);
		processProperties = this.parseProcess(jqXmlDoc);
		enumItemProperties = this.parseEnumItemType(jqXmlDoc);
	}
	
	this.getFieldProperties = function(fieldType) {
		return {
			field: fieldProperties,
			type: typeProperties
		}
	}
	
	this.getEnumItemProperties = function() {
		return enumItemProperties;
	}
	
	this.getProcessProperties = function() {
		return processProperties;
	}
}

DsbSchemaDocument.prototype.parseField = function(jqXmlDoc) {
	var jqField = jqXmlDoc.children().children('[name=ds_field]'),
		props = {};
	
	jqField.children('[name]').each(function() {
		var jqThis = $j(this),
			jqRestriction = null,
			name = jqThis.attr('name'),
			prop = {};
		
		prop.required = jqThis.attr('use') != null && jqThis.attr('use').toLowerCase() == 'required';
		prop.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text()
			.trim().replace(/\n/g, '').replace(/\t/g, '');
		prop.type = 'string';
		
		if(jqThis.attr('type')) {
			prop.type = jqThis.attr('type').replace('xsd:', '');
		} else if(jqThis.children('xsd\\:simpleType').length) {
			jqRestriction = jqThis.children('xsd\\:simpleType').children('xsd\\:restriction');
			if(jqRestriction.attr('base')) {
				prop.type = jqRestriction.attr('base').replace('xsd:', '');
			}
			if(jqRestriction.children('xsd\\:pattern').length) {
				prop.pattern = jqRestriction.children('xsd\\:pattern').attr('value');
			}
		}
		
		props[name] = prop;
	});
	
	return props;
}

DsbSchemaDocument.prototype.parseType = function(jqXmlDoc) {
	var jqType = jqXmlDoc.children().children('[name=ds_type]'),
		jqRestriction = null,
		props = {};
	function parseNestedType(typeName) {
		var jqNestedType = jqXmlDoc.find('[name=' + typeName + ']'),
			enums = [];
		jqNestedType.children('xsd\\:restriction').children('xsd\\:enumeration').each(function() {
			enums.push({
				value: $j(this).attr('value')
			})
		});
		
		return enums;
	}
	
	jqType.children('xsd\\:attribute').each(function() {
		var jqThis = $j(this),
			name = jqThis.attr('name'),
			prop = {};
		
		prop.friendlyName = name;
		prop.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text().trim();
		prop.type = 'string';
		if(jqThis.attr('type')) {
			if(jqThis.attr('type').indexOf('xsd') != -1) {
				prop.type = jqThis.attr('type').replace('xsd:', '');
			} else {
				prop.type = 'list';
				prop.list = parseNestedType(jqThis.attr('type'));
			}
		} else if(jqThis.children('xsd\\:simpleType').length) {
			jqRestriction = jqThis.children('xsd\\:simpleType').children('xsd\\:restriction'); 
			if(jqRestriction.attr('base')) {
				if(jqRestriction.attr('base')) {
					prop.type = jqRestriction.attr('base').replace('xsd:', '');
				}
				if(jqRestriction.children('xsd\\:pattern').length) {
					prop.pattern = jqRestriction.children('xsd\\:pattern').attr('value');
				}
				if(jqRestriction.children('xsd\\:minInclusive').length) {
					prop.minInclusive = jqRestriction.children('xsd\\:minInclusive').attr('value');
				}
			}
		}
		props[name] = prop;
	});
	
	return props;
}

DsbSchemaDocument.prototype.parseProcess = function(jqXmlDoc) {
	var jqProcess = jqXmlDoc.children().children('[name=ds_process]'),
		jqProcessChildren = jqProcess.children('xsd\\:all').children('xsd\\:element'),
		jqProcessParam = jqXmlDoc.children().children('[name=ds_proc_param]'),
		props = {};
	
	jqProcess.children('xsd\\:attribute').each(function() {
		var jqThis = $j(this),
			jqSimpleType = jqThis.children('xsd\\:simpleType'),
			jqRestriction = jqSimpleType.children('xsd\\:restriction'),
			name = jqThis.attr('name'),
			prop = {};
		 
		prop.name = name;
		prop.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text();
		
		if(jqSimpleType.length > 0 && jqRestriction.length > 0) {
			prop.values = [];
			jqRestriction.children('xsd\\:enumeration').each(function() {
				var jqThis = $j(this), item = {};
				item.value = jqThis.attr('value');
				item.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text();
				
				prop.values.push(item);
			});
		}
		
		props[name] = prop;
	});
	
	jqProcessChildren.each(function() {
		var jqThis = $j(this),
			name = jqThis.attr('name'),
			prop = {};
		
		prop.name = name;
		prop.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text();
		
		props[name] = prop;
	});
	
	jqProcessParam.children('xsd\\:attribute').each(function() {
		var jqThis = $j(this),
			jqRestriction = jqThis.children('xsd\\:simpleType').children('xsd\\:restriction'),
			prop = {};
		
		prop.name = $j(this).attr('name');
		prop.values = [];
		jqRestriction.children('xsd\\:enumeration').each(function() {
			var jqThis = $j(this), item = {};
			
			item.value = jqThis.attr('value');
			item.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text();
			prop.values.push(item);
		});
		props.params = prop;
	});
	
	return props;
}

DsbSchemaDocument.prototype.parseEnumItemType = function(jqXmlDoc) {
	var jqEnumItemType = jqXmlDoc.children().children('[name=ds_enum-item]'),
		props = {};
	
	jqEnumItemType.children('xsd\\:attribute').each(function() {
		var jqThis = $j(this),
			name = jqThis.attr('name'),
			prop = {};
		
		prop.friendlyName = name;
		prop.desc = jqThis.children('xsd\\:annotation').children('xsd\\:documentation').text().trim();
		prop.type = 'string';
		
		props[name] = prop;
	});
	
	return props;
}

