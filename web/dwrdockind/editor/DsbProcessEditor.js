DsbProcessEditor = function(params) {
	// private
	var dsbSchemaDocument = null,
		schemaProps = null,
		allProcesses = null,
		initialProcesses = null,
		actionUrl = params ? params.actionUrl : null,
		jqTbody = null,
		jqEditor = null;
	
	var draw = function() {
		var str = '', allProps = DsbProcessEditor.properties,
			mainProps = DsbProcessEditor.mainProperties,
			schemaProps = dsbSchemaDocument.getProcessProperties(),
			propName = null, propDesc = null,
			i = 0, len = mainProps.length;
		
		jqEditor = $j('<div/>').addClass('dsb-flex-table-editor').attr('title', 'Lista proces�w');
		
		str += '<table><thead><tr>';
		for(i = 0; i < len; i ++) {
			try {
				propDesc = 'title="' + schemaProps[mainProps[i]].desc + '" ';
			} catch(e) {
				propDesc = null;
			} finally {
				propDesc = propDesc || '';
			}
			str += '<th><textarea ' + propDesc + ' readonly="readonly" id="dsb-process-header-' + propName + '">' + allProps[mainProps[i]].title + '</textarea></th>';
		}
		str += '<th>&nbsp;</th>';
		str += '</tr></thead><tbody></tbody></table>';
		
		jqEditor.append(str);
		$j('body').append(jqEditor);
		jqTbody = jqEditor.children('table').children('tbody');
		
		jqEditor.dialog({
			autoOpen: false,
			zIndex: 9999,
			width: 970,
			buttons: [
			{
				cn: 'dsb-process-editor-cancel',
				text: 'Anuluj',
				click: function() {
					jqEditor.trigger('cancel');
				}
			},
			{
				cn: 'dsb-process-editor-ok',
				text: 'OK',
				click: function() {
					jqEditor.trigger('save-values');
					jqEditor.trigger('save');
				}
			}],
			open: function() {
				$j('dsb-process-editor-ok').focus();
			}
		}).bind('save-values', function() {
			var values = [];
			jqTbody.children('tr').each(function() {
				var value = {}, index = null;
				value.params = [];
				$j(this).find('select,input[type=text]').each(function() {
					index = $j(this).attr('index');
					if(index != null) {
						if(value.params[index] == null) {
							value.params[index] = {};
						}
						value.params[index][this.name] = $j(this).val();
					} else {
						value[this.name] = $j(this).val();
					}
				});
				values.push(value);
			});
			
			$j(this).data('values', values);
		});
	}
	
	var buildParamNameSelect = function(selectedItem, index) {
		var str = '', options = schemaProps.params.values,
			i = 0, len = options.length,
			selected = false;
		str += '<select name="param-name" index="' + index + '">';
		for(i = 0; i < len; i ++) {
			selected = '';
			if(selectedItem == options[i].value) {
				selected = ' selected="selected"';
			}
			str += '<option value="' + options[i].value + '"' + selected + '>' + options[i].desc + '</option>';
		}
		str += '</select>';
		return str;
	}
	
	var addProcessParameter = function(domBtn) {
		var jqBtn = $j(domBtn),
			jqPrevRow = jqBtn.prev(),
			jqRow = null, index = null, str = '';
		
		str += '<div class="dsb-flex-table-inner-container">';
		str += '<div class="dsb-flex-table-inner-content" ' + '' + '>' + buildParamNameSelect(null, 0) + '</div>';
		str += '<div class="dsb-flex-table-inner-content">';
		str += '<input name="param-value" type="text" value="" />';
		str += '<div class="smallIcons dsb-flex-table-inner-delete-row" title="Usu� parametr"></div>';
		str += '</div>';
		str += '</div>';
		
		jqRow = $j(str);
		index = Number(jqPrevRow.find('select[name=param-name]').attr('index')) + 1;
		if(isNaN(index))
			index = 0;
		
		jqRow.find('select[name=param-name], input[name=param-value]').attr('index', index);
		jqRow.find('select[name=param-name], input[name=param-value]').val('');
		jqBtn.before(jqRow);
	}
	
	var deleteProcessParameter = function(domBtn) {
		var jqBtn = $j(domBtn),
			jqRow = jqBtn.parent().parent(),
			jqInnerTable = jqRow.parent(),
			index = 0;
		
		jqRow.remove();
		
		jqInnerTable.children('div').each(function() {
			var jqThis = $j(this),
				jqParamName = jqThis.find('select[name=param-name]'),
				jqParamValue = jqThis.find('input[name=param-value]');
			
			if(jqParamName.length > 0 && jqParamValue > 0) {
				jqParamName.attr('index', index);
				jqParamValue.attr('index', index);
			}
		});
	}
	
	// privileged
	this.init = function(allProcs, dsbSchemaDoc) {
		allProcesses = allProcs;
		dsbSchemaDocument = dsbSchemaDoc;
		schemaProps = dsbSchemaDocument.getProcessProperties();
		draw();
	}
	
	this.update = function(processes) {
		var i = 0, len = processes.length,
			str = '', 
			allProps = DsbProcessEditor.properties,
			mainProps = DsbProcessEditor.mainProperties,
			minorProps = DsbProcessEditor.minorProperties,
			j = 0, lenj = mainProps.length;
			propName = null,
			proc = null, selected = false;
		
		initialProcesses = processes; 
		for(i = 0; i < len; i ++) {
			selected = false;
			str += '<tr>';
			for(j = 0; j < lenj; j ++) {
				propName = mainProps[j];
				str += '<td>';
				
				switch(allProps[propName].type) {
				case 'process name':
					str += '<select name="' + propName + '">';
					for(proc in allProcesses) {
						proc = allProcesses[proc];
						selected = proc.name == processes[i].name ? 'selected = "selected"' : '';
						str += '<option value="' + proc.name + '" ' + selected + '>' + proc.name + '</option>';
					}
					str += '</select>';
				break;
				case 'process creation':
					str += '<select name="' + propName + '">';
					(function() {
						var props = schemaProps.creation,
							vals = props.values, 
							item = null, itemLabel = null, itemValue = null,
							j = 0, len = vals.length,
							selected = false; 
						
						for(j = 0; j < len; j ++) {
							item = vals[j];
							itemLabel = item.desc || item.value;
							itemValue = item.value;
							selected = itemValue == processes[i].creation ? 'selected = "selected"' : '';
							str += '<option value="' + itemValue + '" ' + selected + '>' + itemLabel + '</option>';
						}
					})();
					str += '</select>';
				break;
				}
				
				str += '</td>';
			}
			str += '<td><div class="dsb-flex-table-inner">';
			
			(function() {
				var k = 0, lenk = minorProps.length,
					minorPropName = null,
					desc = null;
				
				for(k = 0; k < lenk; k ++) {
					minorPropName = minorProps[k];
					try {
						desc = 'title="' + schemaProps[minorPropName].desc + '" ';
					} catch(e) {
						desc = null;
					} finally {
						desc = desc || '';
					}
					
					switch(allProps[minorPropName].type) {
					case 'process params':
						(function() {
							var procParams = processes[i].params, 
								m = 0, lenm = procParams.length;
							
							for(m = 0; m < lenm; m ++) {
								str += '<div class="dsb-flex-table-inner-container">';
								str += '<div class="dsb-flex-table-inner-content" ' + '' + '>' + buildParamNameSelect(procParams[m].name, m) + '</div>';
								str += '<div class="dsb-flex-table-inner-content">';
								str += '<input name="param-value" type="text" value="' + procParams[m].value + '" index="' + m + '" />';
								str += '<div class="smallIcons dsb-flex-table-inner-delete-row" title="Usu� parametr"></div>';
								str += '</div>';
								str += '</div>';
							}
						})();
					break;
					default:
						str += '<div class="dsb-flex-table-inner-container">';
						str += '<div class="dsb-flex-table-inner-content" ' + desc + '>' + allProps[minorPropName].title + '</div>';
						str += '<div class="dsb-flex-table-inner-content">';
						str += '<input name="' + minorPropName + '" type="text" value="' + processes[i][minorPropName] + '" />';
						str += '</div>';
						str += '</div>';
					}
				}
				str += '<div class="dsb-flex-table-inner-add-next">Dodaj parametr</div>';
			})();
			
			str += '</div></td>';
			str += '</tr>';
		}
		
		jqTbody.html(str);
		
		jqTbody.off('click.add-param').on('click.add-param', 'tr td div div.dsb-flex-table-inner-add-next', function() {
			addProcessParameter(this);
		}).off('click.delete-param').on('click.delete-param', 'tr td div div.dsb-flex-table-inner-delete-row', function() {
			deleteProcessParameter(this);
		});
	}
	
	this.getProcessesRequiredFields = function(procs, callback) {
		var i = 0, len = procs.length, reqs = [null],
			fields = {};
		
		function getReq(procName) {
			return $j.post(actionUrl, {processFields: true, processName: procName});
		}
		
		for(i = 0; i < len; i ++) {
			reqs.push(getReq(procs[i].name));
		}
		
		$j.when.apply($j, reqs).then(function() {
			var fieldCn = null, procFields = null;
			
			for(i = 0; i < arguments.length; i ++) {
				try {
					procFields = arguments[i][0].fields;
					for(fieldCn in procFields) {
						fields[fieldCn] = procFields[fieldCn];
					}
				} catch(e) {}
			}
			
			callback(fields);
		});
	}
	
	this.show = function(opts, onSave, onCancel) {
		jqEditor.dialog('open');
		jqEditor.unbind('save').bind('save', function() {
			onSave(initialProcesses, jqEditor.data('values'));
		}).unbind('cancel').bind('cancel', function() {
			onCancel();
		});
	}
	
	this.hide = function() {
		jqEditor.dialog('close');
	}
}

DsbProcessEditor.properties = {
	'name': {
		title: 'nazwa',
		type: 'process name'
	},
	'creation': {
		title: 'wywo�anie',
		type: 'process creation'
	},
	'view': {
		title: 'Dost�pne akcje'
	},
	'locator': {
		title: 'Wyszukiwanie istniejacych proces�w'
	},
	'logic': {
		title: 'Logika procesu'
	},
	'params': {
		title: 'parametry',
		type: 'process params'
	}
}

DsbProcessEditor.mainProperties = ['name', 'creation'];
DsbProcessEditor.minorProperties = ['view', 'locator', 'logic', 'params'];