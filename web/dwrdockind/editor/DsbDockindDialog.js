DsbDockindDialog = (function() {
	// private
	var jqDialog = $j('#dsb-dockind-dialog'),
		jqCn = null,
		jqName = null,
		jqTableName = null,
		jqUpdateDb = null,
		dockind = null,
		opts = null;
	
	var create = function() {
		jqCn = $j('#dsb-dockind-dialog-cn');
		jqName = $j('#dsb-dockind-dialog-name');
		jqTableName = $j('#dsb-dockind-dialog-tablename');
		jqUpdateDb = $j('#dsb-dockind-dialog-updatedb');
		jqDialog.dialog({
			autoOpen: false,
			modal: true,
			resizable: false,
			height: 210,
			width: 400,
			buttons: [
			          {
			        	  cn: 'dsb-dockind-dialog-cancel',
			        	  text: 'Anuluj',
			        	  click: function() {
			        		  $j(this).dialog('close');
			        	  }
			          },
			          {
			        	  cn: 'dsb-dockind-dialog-save',
			        	  text: 'Zapisz',
			        	  click: function() {
			        		  opts.onSave({
			        			 cn: jqCn.val(),
			        			 name: jqName.val(),
			        			 tableName: jqTableName.val(),
			        			 updateDb: jqUpdateDb.is(':checked')
			        		  });
			        	  }
			          },
			          {
			        	  cn: 'dsb-dockind-dialog-ok',
			        	  text: 'OK',
			        	  click: function() {
			        		  $j(this).dialog('close');
			        	  }
			          }
			],
			open: function(event, ui) {
				switch(opts.mode) {
				case 'info':
					$j('[cn=dsb-dockind-dialog-cancel],[cn=dsb-dockind-dialog-save]').hide();
					$j('[cn=dsb-dockind-dialog-ok]').show();
					jqCn.add(jqName).add(jqTableName).attr('readonly', 'readonly');
                    jqUpdateDb.attr('disabled', 'disabled');
				break;
				case 'save':
				default:
					$j('[cn=dsb-dockind-dialog-cancel],[cn=dsb-dockind-dialog-save]').show();
					$j('[cn=dsb-dockind-dialog-ok]').hide();
					jqCn.add(jqName).add(jqTableName).removeAttr('readonly');
					jqUpdateDb.removeAttr('disabled');
				break;
				}
			}
		});
		
		jqName.unbind('keyup.fill').bind('keyup.fill', function() {
			var emptyCn = jqCn.data('empty') === true,
				emptyTableName = jqTableName.data('empty') === true;
			if(emptyCn || emptyTableName) {
				var normalizedTxt = (DsbUtils.normalizeString($j(this).val()) || '').toLowerCase().trim();
				normalizedTxt = normalizedTxt.replace(/\s/gi, '_');
				
				if(emptyCn)
					jqCn.val(normalizedTxt);
				if(emptyTableName)
					jqTableName.val(normalizedTxt);
			}
		});
		
		jqCn.unbind('keyup.fill').bind('keyup.fill', function() {
			$j(this).data('empty', false);
		});
		
		jqTableName.unbind('keyup.fill').bind('keyup.fill', function() {
			$j(this).data('empty', false);
		});
	}
	
	// constructor
	create();
	
	// public
	return {
		update: function(dck) {
			dockind = dck;
			jqName.val(dockind.params.name);
			jqCn.val(dockind.params.cn).data('empty', true);
			jqTableName.val(dockind.params.tableName).data('empty', true);
			
			if(dockind.params.cn != null && dockind.params.cn.trim() != '') {
				jqCn.data('empty', false);
			}
			if(dockind.params.tableName != null && dockind.params.tableName.trim() != '') {
				jqTableName.data('empty', false);
			}
		},
		show: function(options) {
			opts = options;
			jqDialog.dialog('open');
		},
		hide: function() {
			jqDialog.dialog('close');
		}
	}
})();