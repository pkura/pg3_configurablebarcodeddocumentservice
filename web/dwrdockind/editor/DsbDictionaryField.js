DsbDictionaryField = function(dsbXmlDoc, jFld, jqFldCnt, parentCn) {
	// private
	var _this = this;
	var dsbXmlDocument = null,
		jField = null,
		parentFieldCn = null,
		selected = false;
	
	var jqFieldContainer;
	
	var init = function() {
		jqFieldContainer.data('cn', jField.cn).bind('select', function(event) {
			var jqThis = $j(this);
			var fieldCn, cmd;

			jqThis.addClass('ui-selected');
			jqThis.siblings('.ui-selected').trigger('unselect');
			
			fieldCn = jqThis.data('cn');
			cmd = new DsbXmlDocument.Command({cn: fieldCn}, 'select');
			cmd.options = {
				parentFieldCn: parentFieldCn
			};
			dsbXmlDocument.execCommand(cmd);
			selected = true;
		}).bind('unselect', function(event) {
			$j(this).removeClass('ui-selected');
			selected = false;
		}).bind('mouseover', function(event) {
			//console.info('over', jField.cn);
		}).bind('mouseout', function(event) {
			//$j(this).removeClass('')
		});
	}
	
	// constructor
	jField = jFld;
	dsbXmlDocument = dsbXmlDoc;
	jqFieldContainer = jqFldCnt;
	parentFieldCn = parentCn;
	init();
	
	// privileged
	this.getjField = function() {
		return jField;
	}
	
	this.getCn = function() {
		return jField.cn;
	}
	
	this.isSelected = function() {
		return selected;
	}
	
	this.update = function(dsbXmlDoc, jFld, jqFldCnt, parentCn) {
		jField = jFld;
		dsbXmlDocument = dsbXmlDoc;
		jqFieldContainer = jqFldCnt;
		parentFieldCn = parentCn;
		init();
//		console.info('cont', jqFieldContainer);
	}
	
	this.select = function() {
		jqFieldContainer.trigger('select');
	}
	
	this.unselect = function() {
		jqFieldContainer.trigger('unselect');
	}
}