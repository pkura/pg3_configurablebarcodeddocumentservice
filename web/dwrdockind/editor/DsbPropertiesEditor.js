DsbPropertiesEditor = function(params) {
	// private
	var _this = this,
		dsbSchemaDocument = null,
		dsbXmlDocument = null,
		dsbEnumEditor = null,
		dsbProcessEditor = null,
		jqEditor = null,
		fieldCn = null,
		observer = new DsbObserver(),
		visible = true,
		actionUrl = params ? params.actionUrl : null,
		options = null;
	var jqInfoText = null;

	var webkitGlitchFix = function() {
		try {
			$j('.dsb-properties-editor').css('position', 'absolute');
		} catch(e) {
			console.error(e);
		}
	}

	if($j.browser.webkit === true) {
		webkitGlitchFix();
	}
	
	var createControl = function(type, name, params) {
		var jqControl = null,
			prop = DsbPropertiesEditor.properties[name],
			eventName, i, str, cmd;
		switch(type.toLowerCase()) {
			case 'int':
			case 'string':
				jqControl = $j('<input/>').attr('id', 'dsbe_' + name).attr('type', 'text').addClass('dontFixWidth');
				if(params.pattern != null) {
					jqControl.attr('pattern', params.pattern);
				}
				if(params.minInclusive != null) {
					jqControl.attr('min', params.minInclusive);
				}
				jqControl.bind('setvalue', function(event, val, initialVal, opts) {
					if(prop.convertIn != null) {
						val = prop.convertIn(val, opts);
						if(initialVal != null) {
							initialVal = prop.convertIn(initialVal, opts);
						}
					}
					$j(this).val(val);
					if(initialVal != null)
						$j(this).data('prev', initialVal);
				}).bind('clearvalue', function() {
					$j(this).trigger('setvalue', ['']);
				}).bind('keydown.bsde', function() {
					if(prop.cmd == 'fast update') {
						$j(this).data('prev', this.value);
					}
				}).bind('keyup.bsde', function() {
					$j('#dsbe_submit').trigger('click');
				}).bind('getvalue', function(event, ret) {
					var val = $j(this).val();
					//ret.value = $j(this).val();
					//console.warn(this.value);
					if(prop.convertOut != null) {
						//ret.value = prop.convertOut(ret.value, options);
						val = prop.convertOut(val, options);
					}
					ret(val);
				});
				
				if(prop.cmd == 'fast update') {
					eventName = 'keyup';
				} else if(prop.cmd == 'update') {
					eventName = 'change';
				}
				
				// je�li zmieniamy cn, to musimy uaktualni� istniej�cy
				if(name == 'cn') {
					jqControl.bind(eventName + '.bsde-update-cn', function() {
						if(prop.convertIn != null) {
							fieldCn = prop.convertIn(this.value, options);
						} else {
							fieldCn = this.value;
						}
					});
				}
				
				jqControl.bind(eventName + '.bsde', function() {
					var fieldCn = null;
					
					fieldCn = $j('#dsbe_cn').val();
					if(prop.convertOut != null) {
						fieldCn = prop.convertOut(fieldCn, options);
					}
					
					if($j(this).val() != $j(this).data('prev')) {
						/**
						 * TODO - kopiowanie _this z nieaktualn� warto�ci�
						 */
						cmd = new DsbXmlDocument.Command({cn: fieldCn}, prop.cmd);
						cmd.from[name] = $j(this).data('prev') || null;
						cmd.to[name] = this.value;
						cmd.options = options;
						if(cmd.options == null) {
							cmd.options = {
								singleParameterUpdate: true
							}
						} else {
							cmd.options.singleParameterUpdate = true;
						}
						observer.publish('change', cmd);
					}
				});
			break;
			case 'boolean':
				jqControl = $j('<input/>').attr('id', 'dsbe_' + name).attr('type', 'checkbox').addClass('styled-disabled dontFixWidth');
				jqControl.bind('setvalue', function(event, val) {
					var checked = val.toString() == 'true';
					$j(this).prop('checked', checked);
				}).bind('clearvalue', function() {
					$j(this).trigger('setvalue', [false]);
				}).bind('change.bsde', function() {
					var fieldCn = null;
					fieldCn = $j('#dsbe_cn').val();
					if(prop.convertOut != null) {
						fieldCn = prop.convertOut(fieldCn, options);
					}
					
					cmd = new DsbXmlDocument.Command({cn: fieldCn}, prop.cmd);
					cmd.from[name] = !this.checked;
					cmd.to[name] = this.checked;
					cmd.options = options;
					if(cmd.options == null) {
						cmd.options = {
							singleParameterUpdate: true
						}
					} else {
						cmd.options.singleParameterUpdate = true;
					}
						
//					setTimeout(function() {
						observer.publish('change', cmd);
//					}, 0);
					return false;
				});
			break;
			case 'list':
				jqControl = $j('<select/>').attr('id', 'dsbe_' + name).addClass('dontFixWidth');
				str = '';
				for(i = 0; i < params.list.length; i ++) {
					str += '<option value="' + params.list[i].value + '">';
					str += DsbPropertiesEditor.properties[name].listTitles[params.list[i].value];
					str += '</options>';
				}
				jqControl.html(str);
				jqControl.bind('setvalue', function(event, val) {
					$j(this).val(val);
				}).bind('clearvalue', function() {
					// nop
				}).bind('change.bsde', function() {
					var fieldCn = null;
					fieldCn = $j('#dsbe_cn').val();
					if(prop.convertOut != null) {
						fieldCn = prop.convertOut(fieldCn, options);
					}
					
					cmd = new DsbXmlDocument.Command({cn: fieldCn}, prop.cmd);
					cmd.from[name] = this.value;
					cmd.to[name] = this.value;
					cmd.options = options;
					if(cmd.options == null) {
						cmd.options = {
							singleParameterUpdate: true
						}
					} else {
						cmd.options.singleParameterUpdate = true;
					}
					observer.publish('change', cmd);
				});
			break;
			case 'enum-list':
				jqControl = $j('<button/>').html('...').attr('id', 'dsbe_' + name);
				jqControl.bind('setvalue', function(event, val) {
					$j(this).data('enum-items', val);
				}).bind('clearvalue', function() {
					$j(this).removeData('enum-items');
				}).bind('click.bsde', function() {
					var oldVals = $j(this).data('enum-items'),
						_this = this;
					var opts = {
						title: $j('#dsbe_name').val()
					}
					dsbEnumEditor.update(oldVals);
					dsbEnumEditor.show(opts, function onSave(vals) {
						$j(_this).data('enum-items', vals);
						dsbEnumEditor.hide();
						
						var fieldCn = $j('#dsbe_cn').val();
						if(prop.convertOut != null) {
							fieldCn = prop.convertOut(fieldCn, options);
						}
						
						cmd = new DsbXmlDocument.Command({cn: fieldCn}, prop.cmd);
						cmd.from[name] = oldVals;
						cmd.to[name] = vals;
						cmd.options = options;
						observer.publish('change', cmd);
					}, function onCancel() {
						dsbEnumEditor.hide();
					});
				});
			break;
			case 'boolean-ico-list':
				jqControl = $j('<div/>', {
					id: 'dsbe_' + name,
					'class': 'dsbe-boolean-ico-list'
				});
				for(i = 0; i < params.list.length; i ++) {
					$j('<input/>', {
						type: 'checkbox',
						value: params.list[i].value,
						id: 'dsbe_' + name + '_' + params.list[i].value,
						'class': 'styled-disabled dontFixWidth dsbe-disabled-off'
					}).appendTo(jqControl);
					$j('<label/>', {
						'for': 'dsbe_' + name + '_' + params.list[i].value,
						title: params.list[i].title,
						html: '&nbsp;',
						'class': params.list[i].value + ' ' + (params.list[i].cssClass || '')
					}).appendTo(jqControl);
				}
				jqControl.buttonset();
				jqControl.bind('setvalue', function(event, val) {
					var jqThis = $j(this),
						vals = val.split(','),
						i = 0, len = vals.length,
						btnId = null;
					
					for(i = 0; i < len; i ++) {
						btnId = this.id + '_' + vals[i];
						$j('#' + btnId).attr('checked', true);
					}
					
					jqThis.removeAttr('disabled').buttonset('refresh');
				}).bind('change.bsde', function() {
					var jqThis = $j(this),
						val = [], popup = true;
					
					jqThis.find(':checkbox:checked').each(function() {
						val.push(this.value);
					});
					if($j.inArray('showPopup', val) == -1) {
						popup = false;
					}
					val = val.join(',');
					
					var fieldCn = null;
					fieldCn = $j('#dsbe_cn').val();
					if(prop.convertOut != null) {
						fieldCn = prop.convertOut(fieldCn, options);
					}
					
					cmd = new DsbXmlDocument.Command({cn: fieldCn}, prop.cmd);
					cmd.from[name] = val;
					cmd.to[name] = val;
					cmd.options = options;
					if(cmd.options == null) {
						cmd.options = {
							singleParameterUpdate: true
						}
					} else {
						cmd.options.singleParameterUpdate = true;
					}
					
					observer.publish('change', cmd);
				});
				
			break;
		}
		if(prop.readonly === true) {
			jqControl.attr('disabled', 'disabled').addClass('dsbe-readonly');
		}
		
		return jqControl;
	}
	
	var draw = function() {
		var props = dsbSchemaDocument.getFieldProperties(),
			allProps = DsbPropertiesEditor.properties,
			name = null, jqRow = null, jqControl = null; 
			propertyNames = null, i = 0, property, propertyType = null, 
			params = {}, propertyGroupName = null, desc = null;
		jqEditor = $j('<div/>');
		// w�asno�ci podstawowe
		for(propertyGroupName in DsbPropertiesEditor.PropertiesGroup) {
			property = DsbPropertiesEditor.PropertiesGroup[propertyGroupName];
			propertyNames = property.props;
			$j('<h4/>').html(property.friendlyName).appendTo(jqEditor);
//			console.info(props);
			for(i = 0; i < propertyNames.length; i ++) {
				name = propertyNames[i],
				typeName = name;
				desc = null;
				params = {};
				jqRow = $j('<div/>').attr('id', 'dsb-property-' + name);
				try {
					if(name.indexOf('__TYPE') != -1) {
						typeName = name.replace('__TYPE', '');
						propertyType = props.type[typeName].type;
						desc = props.type[typeName].desc;
						params.list = props.type[typeName].list;
						if(props.type[typeName] != null && props.type[typeName].pattern != null) {
							params.pattern = props.type[typeName].pattern;
						}
						if(props.type[typeName] != null && props.type[typeName].minInclusive != null) {
							params.minInclusive = props.type[typeName].minInclusive;
						}
					} else if(name.indexOf('__CUSTOM') != -1) {
						switch(name) {
						case 'enum-list__CUSTOM':
							propertyType = 'enum-list';
						break;
						case 'pop-up-display__CUSTOM':
						case 'search-display__CUSTOM':
							propertyType = 'boolean';
						break;
						}
					} else {
						propertyType = props.field[name].type;
						desc = props.field[name].desc;
						if(props.field[name] != null && props.field[name].pattern != null) {
							params.pattern = props.field[name].pattern;
						}
						if(props.field[name] != null && props.field[name].minInclusive != null) {
							params.minInclusive = props.field[name].minInclusive;
						}
					}
					if(allProps[name] != null && allProps[name].type != null) {
						propertyType = allProps[name].type;
						params = params ? params : {};
						params.list = allProps[name].list;
						// console.warn('overriding type to', allProps[name].type);
					}
				} catch(e) {
					console.error(e, name);
				}
				jqControl = createControl(propertyType, name, params);
				$j('<label/>').attr('for', 'dsbe_' + name).attr('title', desc)
					.html(allProps[name].title).appendTo(jqRow);
				
				if(jqControl != null)
					jqControl.appendTo(jqRow);
				jqEditor.append(jqRow);
			}
		}
		
		jqEditor.addClass('dsb-properties-editor');
		/*.css({
			'left': $j('.upperMenuLeftCell').offset().left + 'px',
			'top': $j('.upperMenuLeftCell').offset().top + 'px'
		})*/
		var jqForm = $j('<form/>');
		jqForm.submit(function(event) {
			event.preventDefault();
		});
		jqForm.append('<input style="display: none" type="submit" id="dsbe_submit" name="sss" value="sss" />');
		jqForm.append(jqEditor);
		$j('body').append(jqForm);
	}
	
	var drawPalette = function() {
		var jqPalette = $j('<div/>').addClass('dsb-palette');
		jqPalette.append('<div class="dsb-palette-group"></div>');
		jqPalette.append('<div class="dsb-palette-group"></div>');
		
		$j('body').append(jqPalette);
		var jqPaletteGroups = jqPalette.children('.dsb-palette-group');
		tmp200 = jqPaletteGroups;
		
		$j.post(actionUrl, {dockindFields: true}, function(data) {
			dsbXmlDocument = new DsbXmlDocument('template');
			dsbXmlDocument.init(data);
			var jFields = dsbXmlDocument.parse(),
				i, jField, jqButton, jqIcon, typeTitle = '';
			
			jqInfoText = $j('<div/>').addClass('info-text').html('&nbsp;');
			jqPaletteGroups.eq(0).append(jqInfoText);
			
			jqIcon = $j('<div class="dsb-icon dsb-cursor"/>');
			jqButton = $j('<button/>').attr('name', 'cursor').attr('disabled', 'disabled').attr('title', 'Narz�dzie zaznaczania');
			jqButton.append(jqIcon);
			jqButton.bind('click.dsbe', function() {
				observer.publish('new', null);
				jqPaletteGroups.eq(0).find('button').removeAttr('disabled');
				$j(this).attr('disabled', 'disabled');
				return false;
			});
			jqPaletteGroups.eq(0).append(jqButton);
			
			for(i = 0; i < jFields.length; i ++) {
				jField = jFields[i];
//				typeTitle = DsbPropertiesEditor.properties['name__TYPE'].listTitles[jField.type.toLowerCase()];
				jqButton = $j('<button/>').attr('fieldcn', jField.cn.toString());
				jqButton.bind('click.dsbe', function() {
					var fieldCn = $j(this).attr('fieldcn'),
						newField = {
							xml: dsbXmlDocument.getFieldXML(fieldCn),
							cursor: $j(this).children('.dsb-icon').css('background-image')
						};
					
					observer.publish('new', newField);
					jqPaletteGroups.eq(0).find('button').removeAttr('disabled');
					$j(this).attr('disabled', 'disabled');
					
					return false;
				});
				jqIcon = $j('<div/>').addClass(DsbPropertiesEditor.palette[jField.type.toLowerCase()].cssClass).addClass('dsb-icon');
				jqButton.append(jqIcon);
				jqPaletteGroups.eq(0).append(jqButton);
			}
			
		});
		drawProcessPalette(jqPaletteGroups.eq(1));
	}
	
	var drawProcessPalette = function(jqPalette) {
		$j.post(actionUrl, {processList: true}, function(data) {
			var i = 0, procs = data.processes, len = procs.length;
			
			/*for(i = 0; i < len; i ++) {
				console.info(procs[i].name)
			}*/
			
			var jqProcsContainer = $j('<div class="dsb-procs"/>');
			
			var jqProcButton = $j('<button/>').attr('name', 'dsb-proc').attr('title', 'Lista proces�w przypisanych do rodzaju dokumentu');
			jqProcButton.append('<div class="dsb-icon dsb-processes" />');
			jqProcButton.append('<span class="text">Procesy</span>');
			
			jqProcsContainer.append(jqProcButton);
			jqPalette.append(jqProcsContainer);
			
			//dsbProcessEditor = new DsbProcessEditor({actionUrl: actionUrl});
            dsbProcessEditor = new DsbActivitiProcessEditor({actionUrl: actionUrl});
			dsbProcessEditor.init(procs, dsbSchemaDocument);
			
			jqProcButton.click(function() {
				dsbProcessEditor.show(null, function onSave(oldProcesses, newProcesses) {
					dsbProcessEditor.hide();
					
					//cmd = new DsbXmlDocument.Command(null, 'update processes');
                    cmd = new DsbXmlDocument.Command(null, 'update processes spring');
					cmd.from = oldProcesses;
					cmd.to = newProcesses;
					/*cmd.from[name] = oldVals;
					cmd.to[name] = vals;
					cmd.options = options;*/
					observer.publish('change processes', cmd);
					
				}, function onCancel() {
					dsbProcessEditor.hide();
				});
			});
		});
	}
	
	// privileged
	// observer interface
	this.subscribe = observer.subscribe;
	this.unsubscribe = observer.unsubscribe;
	
	this.init = function(dsbSchemaDoc) {
		dsbSchemaDocument = dsbSchemaDoc;
		draw();
		drawPalette();
		dsbEnumEditor = new DsbEnumEditor();
		dsbEnumEditor.init(dsbSchemaDoc);
	}
	
	this.show = function() {
		//jqEditor.show();
		jqEditor.find('input,select').not('.dsbe-readonly').removeAttr('disabled', 'disabled');
		visible = true;
	}
	
	this.hide = function() {
		jqEditor.find('input,select').not('.dsbe-disabled-off').attr('disabled', 'disabled');
//		jqEditor.hide();
		visible = false;
	}
	
	this.getFieldCn = function() {
		return fieldCn;
	}
	
	this.clear = function() {
		jqEditor.find('input[id^=dsbe_]').trigger('clearvalue');
	}
	
	this.update = function(props, fcn, opts) {
		var cn = null;
		options = opts;
		if(fcn != undefined) {
			fieldCn = fcn;
		}
		for(cn in props) {
			jqEditor.find('[id=dsbe_' + cn + ']').trigger('setvalue', [props[cn], props[cn], opts]);
		}
		if(opts && opts.singleParameterUpdate === true && props['name__TYPE'] == null) {
			
		} else {
			// wy�wietlamy w�asno�ci dla danego typu pola
			var typeName = props['name__TYPE'],
				typeGroup = DsbPropertiesEditor.TypePropertiesGroup[typeName] || {props: []},
				advProps = DsbPropertiesEditor.PropertiesGroup.advanced.props;
			var typeProps, i;
			
			if(typeGroup) {
				typeProps = typeGroup.props;
				if(props['pop-up-display__CUSTOM'] != null) {
					typeProps = typeProps.concat(DsbPropertiesEditor.TypePropertiesGroup['dictionary-field'].props);
				}
				for(i = 0; i < advProps.length; i ++) {
					if($j.inArray(advProps[i], typeProps) == -1) {
						$j('#dsb-property-' + advProps[i]).hide();
					} else {
						$j('#dsb-property-' + advProps[i]).show();
					}
				}
			} else {
				for(i = 0; i < advProps.length; i ++) {
					$j('#dsb-property-' + advProps[i]).hide();
				}
			}
		}
	}
	
	this.updateProcessEditor = function(procs) {
		dsbProcessEditor.update(procs);
	}
	
	this.triggerButton = function(name) {
		$j('.dsb-palette').find('button[name=' + name + ']').removeAttr('disabled').trigger('click');
	}
	
	this.hasFocus = function() {
		return jqEditor.find(':focus').length > 0;
	}
	
	this.setInfoText = function(txt) {
		jqInfoText.html(txt);
		document.title = txt;
	}

	this.getProcessesRequiredFields = function(procs, callback) {
		dsbProcessEditor.getProcessesRequiredFields(procs, callback);
	}
}

/**
 * Przyrostek __CUSTOM oznacza pole obs�ugiwane oddzielnie (nie z XML-a)
 */
DsbPropertiesEditor.PropertiesGroup = {
	basic: {
		friendlyName: 'Podstawowe',
		props: ['name__TYPE', 'cn', 'column', 'name', 'coords', 'required', 'hidden', 'readonly', 'disabled', 'submit']
	},
	advanced: {
		friendlyName: 'Zaawansowane',
		props: ['enum-list__CUSTOM', 'auto__TYPE', 'autocompleteRegExp__TYPE', 'length__TYPE', 'multiline__CUSTOM', 
		        'pop-up-display__CUSTOM', 'tableName__TYPE', 'dictionary-buttons__TYPE', 'search-display__CUSTOM',
		        'field-label__TYPE', 'multiple__TYPE']
	}
}

DsbPropertiesEditor.TypePropertiesGroup = {
	string: {
		friendlyName: 'Tekst',
		props: ['length__TYPE', 'multiline__CUSTOM'],
	},
	enum: {
		friendlyName: 'Lista',
		props: ['enum-list__CUSTOM', 'auto__TYPE', 'autocompleteRegExp__TYPE', 'multiple__TYPE']
	},
	dictionary: {
		props: ['tableName__TYPE', 'dictionary-buttons__TYPE']
	},
	'dictionary-field': {
		friendlyName: 'S�ownik',
		props: ['pop-up-display__CUSTOM', 'search-display__CUSTOM']
	},
	button: {
		friendlyName: 'Przycisk',
		props: ['field-label__TYPE']
	}
}

/**
 * Przyrostek __TYPE oznacza w�asno�� elementu <type> a nie <field>
 */
DsbPropertiesEditor.properties = {
	'auto__TYPE': {
		title: 'Podpowiedzi dla pola',
		cmd: 'update'
	},
	'autocompleteRegExp__TYPE': {
		title: 'Wyra�enie regularne podpowiedzi',
		cmd: 'update'
	},
	'cantUpdate__TYPE': {
		title: 'Blokada edycji istniej�cych wpis�w',
		cmd: 'update'
	},
	'cn': {
		title: 'Nazwa kodowa',
		cmd: 'update',
		convertIn: function(val, options) {
			if(options != null && options.parentFieldCn != null) {
				return DsbXmlDocument.normalizeFieldCn(val, options.parentFieldCn, {
					skipDicName: true
				});
			}
			
			return val;
		},
		convertOut: function(val, options) {
			if(options != null && options.parentFieldCn != null) {
				return DsbXmlDocument.normalizeFieldCn(val, options.parentFieldCn);
			}
			
			return val;
		}
	},
	'cssClass': {
		title: 'Klasa CSS',
		cmd: 'update'
	},
	'column': {
		title: 'Kolumna w bazie danych',
		cmd: 'fast update'
	},
	'coords': {
		title: 'Wsp�rz�dne',
		cmd: 'update'
	},
	'disabled': {
		title: 'Wy��czone',
		cmd: 'fast update'
	},
	'format': {
		title: 'Format',
		cmd: 'update'
	},
	'hidden': {
		title: 'Ukryte',
		cmd: 'fast update'
	},
	'length__TYPE': {
		title: 'Maksymalna liczba znak�w',
		cmd: 'update'
	},
	'multiline__CUSTOM': {
		title: 'Pole wielowierszowe',
		cmd: 'update',
		type: 'boolean'
	},
	'multiple__TYPE': {
	    title: 'Pole wielowarto�ciowe',
	    cmd: 'update'
	},
	'name': {
		title: 'Etykieta',
		cmd: 'fast update'
	},
	'name__TYPE': {
		title: 'Rodzaj',
		readonly: true,
		cmd: 'update',
		listTitles: {
			date: 'data',
			timestamp: 'czas',
			string: 'tekst',
			bool: 'pole wyboru',
			enum: 'lista',
			float: 'float',
			double: 'double',
			integer: 'liczba',
			long: 'long',
			'class': 'class',
			'enum-ref': 'enum-ref',
			'centrum-koszt': 'centrum koszt�w',
			dataBase: 'baza danych',
			money: 'kwota',
			dsuser: 'lista u�ytkownik�w',
			dsdivision: 'lista dzia��w',
			doclist: 'lista dokument�w',
			dockindButton: 'przycisk',
			document: 'dokument',
			'drools-button': 'przycisk drools',
			'list-value': 'lista warto�ci',
			dictionary: 's�ownik',
			'document-person': 'document-person',
			'document-postal-reg-nr': 'kod pocztowy',
			'document-user-division': 'dzia�',
			'document-user-division-second': 'dzia�2',
			'document-date': 'data dokumentu',
			'document-description': 'opis dokumentu',
			'document-journal': 'dziennik',
			'document-field': 'pole dokumentu',
			'document-params': 'parametr dokumentu',
			link: 'odno�nik',
			attachment: 'za��cznik',
			html: 'html',
			'html-editor': 'edytowalny html',
			button: 'przycisk',
			'full-text-search': 'wyszukiwanie pe�notekstowe'
		}
	},
	'readonly': {
		title: 'Tylko do odczytu',
		cmd: 'fast update'
	},
	'required': {
		title: 'Obowi�zkowe',
		cmd: 'fast update'
	},
	'submit': {
		title: 'Automatycznie wysy�ane',
		cmd: 'fast update'
	},
	'onlyPopupCreate': {
		title: 'Dodawanie element�w tylko z wyskakuj�cego okienka',
		cmd: 'update'
	},
	'tableName__TYPE': {
		title: 'Nazwa tabeli',
		cmd: 'fast update'
	},
	'dictionary-buttons__TYPE': {
		title: 'Przyciski s�ownika',
		cmd: 'update',
		type: 'boolean-ico-list', 
		list: [
	        {value: 'showPopup', 	title: 'Poka� s�ownik'},
	        {value: 'doAdd', 		title: 'Dodaj nast�pny element'},
	        {value: 'doRemove',		title: 'Usu� element',				cssClass: 'smallIcons'},
	        {value: 'doClear',		title: 'Wyczy��',					cssClass: 'smallIcons'}	
        ]
	},
	'enum-list__CUSTOM': {
		title: 'Lista opcji',
		cmd: 'update enum items'
	},
	'pop-up-display__CUSTOM': {
		title: 'Widoczne w popupie',
		cmd: 'update dictionary cns'
	},
	'search-display__CUSTOM': {
		title: 'Wyszukiwanie',
		cmd: 'update dictionary cns'
	},
	'field-label__TYPE': {
		title: 'Nazwa przycisku',
		cmd: 'fast update'
	}
}

DsbPropertiesEditor.palette = {
	'date': {
		cssClass: 'dsb-date'
	},
	'string': {
		cssClass: 'dsb-text'
	},
	'enum': {
		cssClass: 'dsb-list'
	},
	'boolean': {
		cssClass: 'dsb-checkbox'
	},
	'integer': {
		cssClass: 'dsb-number'
	},
	'money': {
		cssClass: 'dsb-money'
	},
	'dictionary': {
		cssClass: 'dsb-dictionary'
	}
}
