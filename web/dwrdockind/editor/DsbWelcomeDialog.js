DsbWelcomeDialog = function() {
	// private
	var jqDialog = $j('#dsb-welcome-dialog'),
		onClose = null;
	
	var create = function() {
		jqDialog.dialog({
			autoOpen: false,
			modal: true,
			resizable: false,
			buttons: [
			{
				text: 'OK',
				click: function() {
					if(onClose) {
						onClose({
							welcomeScreen: !($j('#dsb-welcome-dialog-hide').attr('checked') == 'checked')
						});
					}
					$j(this).dialog('close');
				}
			}
            ],
            open: function() {
            	$j(':focus').blur();
            }
		});
	}
	
	// constructor
	create();
	
	// public
	return {
		show: function(options) {
			if(options.welcomeScreen === true) {
				$j('#dsb-welcome-dialog-hide').removeAttr('checked');
			} else if(options.welcomeScreen === false) {
				$j('#dsb-welcome-dialog-hide').attr('checked', 'checked');
			}
			jqDialog.dialog('open');
		},
		addCloseListener: function(listener) {
			onClose = listener;
		}
	}
	
}