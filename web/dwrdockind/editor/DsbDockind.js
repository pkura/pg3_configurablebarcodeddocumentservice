DsbDockind = function(dockindParams) {
	// private
	var _this = this,
		originalXML = null,
		workingXML = null,
		jqWorkingXML = null,
		dsbXmlDocument = new DsbXmlDocument(dockindParams.cn);
	var dsbFields = {},
		processes = [],
		dockindCn = dockindCn || 'nowy',
		fieldCollector = null,
		grid = null,
		gridExpanded = false,
		newField = null,
		newRow = null,
		newCol = null,
		selectedFieldCns = {},
		skipFieldCns = [],
		selectable = true;
	
	var initFields = function() {
		var jFields = dsbXmlDocument.parse();
		var i;
		
		for(i = 0; i < jFields.length; i ++) {
			dsbFields[jFields[i].cn] = new DsbField(dsbXmlDocument, jFields[i]);
		}
	}
	
	var initProcesses = function() {
		processes = dsbXmlDocument.parseProcesses();
	}
	
	var initFieldsManager = function() {
		dsbXmlDocument.subscribe('execute command', function dsbDockindExecuteCommand(topic, cmd) {
			var fields = null, i = 0, jField = null, dsbField = null, jFields = null, 
				popupjFields = null, dicField = null, oldVisibleFieldsCns = null, 
				updateCallback = null, fieldCn = null, fieldList = null;
			var cmdMode = cmd.mode.toLowerCase(), fm = null;
			
//			console.warn(cmdMode);
			
			switch(cmdMode) {
			case 'select':
//				console.info('select', cmd, newField);
				selectedFieldCns[cmd.from.cn] = true;
				
				/*if(cmd.options && cmd.options.parentFieldCn != null) {
					selectedParentFieldCn = cmd.options.parentFieldCn;
				} else {
					selectedParentFieldCn = null;
				}*/
			break;
			case 'unselect':
				console.info('unselect', cmd);
				try {
					delete selectedFieldCns[cmd.from.cn];
				} catch(e) {}
			break;
			case 'update':
				// pole znajduje si� w s�owniku
				dicField = cmd.options != null && cmd.options.parentFieldCn != null; 
				if(dicField === true) {
					fieldCn = cmd.options.parentFieldCn;
				} else {
					fieldCn = cmd.to.cn;
				}
				jField = dsbXmlDocument.reparseField(fieldCn).jField;
				
				// w dsbFields nie ma pola s�ownikowego - jest tylko g��wne pole s�ownika
				if(dicField === true) {
					dsbFields[cmd.options.parentFieldCn].setjField(jField);
					jField.status = 'ADD';
				} else {
					if(dsbFields[cmd.from.cn] != null) {
						dsbFields[cmd.from.cn].setjField(jField);
					} else {
						dsbField = new DsbField(dsbXmlDocument, jField);
						dsbFields[cmd.to.cn] = dsbField;
					}
				}
				
				// wysy�amy list� bez naszego pola
				if(dicField === true) {
					fieldList = _this.getFieldsList([cmd.options.parentFieldCn]);
				} else {
					fieldList = _this.getFieldsList([cmd.from.cn]);
				}
				fieldsManager.updateFields(fieldList, 
				{
					callback: function fmCallback() {
						// i z naszym zmodyfikowanym polem
						var fieldList = null, i = 0, normalizedCn = null;
						
						if(dicField === true) {
							fieldList = _this.getFieldsList();
							// usuwamy pole listy p�l s�ownikowych
							/*normalizedCn = {
								from: DsbXmlDocument.normalizeFieldCn(cmd.from.cn, cmd.options.parentFieldCn)
							};*/
							//dsbFields[cmd.options.parentFieldCn].deleteDsbDictionaryField(normalizedCn.from);
						} else {
							fieldList = _this.getFieldsList();
							// zmieniamy klucz, gry zmieni�o si� cn pola
							if(cmd.from.cn != cmd.to.cn) {
								dsbFields[cmd.to.cn] = dsbFields[cmd.from.cn];
								delete dsbFields[cmd.from.cn];
								delete selectedFieldCns[cmd.from.cn];
//								console.dir(dsbFields);
							}
						}
						
						for(i = 0; i < fieldList.length; i ++) {
							if(fieldList[i].cn == cmd.to.cn) {
								fieldList[i].status = 'ADD';
								break;
							}
						}
						
						if(dicField === true) {
							fieldList = _this.getFieldsList();
						} else {
							fieldList = _this.getFieldsList()
						}
						
						fieldsManager.updateFields(fieldList, {
							callback: function fm2Callback() {
								var normalizedCn = {};
								if(dicField === true) {
									normalizedCn = {
										from: DsbXmlDocument.normalizeFieldCn(cmd.from.cn, cmd.options.parentFieldCn),
										to: DsbXmlDocument.normalizeFieldCn(cmd.to.cn, cmd.options.parentFieldCn)
									};
									/*dsbFields[cmd.options.parentFieldCn].select(grid, true);
									dsbFields[cmd.options.parentFieldCn].getDsbDictionaryField(normalizedCn.to).select(grid, true);*/
									dsbFields[cmd.options.parentFieldCn].selectDictionaryField(normalizedCn.to, grid, true);
									delete selectedFieldCns[normalizedCn.from];
								} else {
									var jqTd = $j('#td-' + jField.coords.split(':')[0]);
									collectGrid(jqTd);
//									console.dir(dsbFields);
									dsbFields[fieldCn].select(grid, true);
									delete selectedFieldCns[cmd.from.cn];
									selectedFieldCns[cmd.to.cn] = true;
								}
							}
						});
					}
				});
			break;
			case 'fast update':
			case 'update enum items':
				if(cmd.options && cmd.options.skip === true) {
					console.warn('skipping fast update', cmd.from.cn);
					if(dsbFields[cmd.from.cn]) {
						dsbFields[cmd.from.cn].updateSelection();
					}
					break;
				}
				console.warn('fast update', cmd);
				if(cmd.options != null && cmd.options.parentFieldCn != null) {
					jField = dsbXmlDocument.reparseField(DsbXmlDocument.normalizeFieldCn(cmd.from.cn, cmd.options.parentFieldCn)).jField;
				} else {
					jField = dsbXmlDocument.reparseField(cmd.from.cn).jField;
				}
				jField.status = 'CHANGED';
				if(dsbFields[cmd.from.cn]) {
					dsbFields[cmd.from.cn].setjField(jField);
					fieldsManager.updateFields(_this.getFieldsList());
					dsbFields[cmd.from.cn].updateSelection();
				} else if(cmd.options && cmd.options.parentFieldCn != null) {
					// update dla s�ownika
//					console.warn('update dictionary field!');
					jField.kind = 'DICTIONARY_FIELD';
					jField.cn = DsbXmlDocument.normalizeFieldCn(jField.cn, cmd.options.parentFieldCn);
					jFields = dsbFields[cmd.options.parentFieldCn].getjField().dictionary.fields;
					popupjFields = dsbFields[cmd.options.parentFieldCn].getjField().dictionary.popUpFields;
					for(i = 0; i < jFields.length; i ++) {
						if(cmd.from.cn == DsbXmlDocument.normalizeFieldCn(jFields[i].cn, cmd.options.parentFieldCn, {skipDicName: true})) {
							jFields[i] = jField;
							break;
						}
					}
					for(i = 0; i < popupjFields.length; i ++) {
						if(cmd.from.cn == popupjFields[i].cn) {
//							console.warn('FOUND', jField.cn);
							popupjFields[i] = jField;
							break;
						}
					}
//					console.info('dictionary update', fieldsManager.fields[cmd.options.parentFieldCn]);
					fieldsManager.fields[cmd.options.parentFieldCn].dictionaryFieldsManager.updateFields(jFields);
					fieldsManager.fields[cmd.options.parentFieldCn].dictionaryFieldsManager
						.dictionaryPopup.updateFields([jField]);
				}
			break;
			case 'update dictionary cns':
				jField = dsbXmlDocument.reparseField(cmd.from.cn).jField;
				jField.status = 'CHANGED';
				jField.cn = cmd.from.cn;
				if(cmd.from['pop-up-display__CUSTOM'] != null) {
					jField.hidden = !cmd.to['pop-up-display__CUSTOM'];
					fieldsManager.fields[cmd.options.parentFieldCn].dictionaryFieldsManager
						.dictionaryPopup.updateFields([jField]);
				}
				
				(function() {
					var normalizedCn = DsbXmlDocument.normalizeFieldCn(jField.cn, cmd.options.parentFieldCn);
					if(cmd.from['search-display__CUSTOM'] != null) {
						fieldsManager.fields[cmd.options.parentFieldCn].dictionaryFieldsManager
							.fields[normalizedCn].isSearchable = cmd.to['search-display__CUSTOM'];
						fieldsManager.fields[cmd.options.parentFieldCn].dictionaryFieldsManager
							.fields[normalizedCn].refresh();
					}
				})();
			break;
			case 'update dictionary':
				jField = dsbXmlDocument.reparseField(cmd.options.dictionaryFieldCn + '_' + cmd.from.cn).jField;
				dicField = fieldsManager.fields[cmd.options.dictionaryFieldCn];
				jFields = dsbFields[cmd.options.dictionaryFieldCn].getjField().dictionary.fields;
				console.info('update dictionary:', popupjFields);
				
				oldVisibleFieldsCns = dicField.jField.dictionary.visibleFieldsCns.slice(0);
				// usuwamy przedrostek s�ownika
				for(i = 0; i < oldVisibleFieldsCns.length; i ++) {
					oldVisibleFieldsCns[i] = oldVisibleFieldsCns[i].replace(dicField.jField.cn + '_', '');
				}
				// dodajemy tymczasowo do listy widocznych p�l
				dicField.jField.dictionary.visibleFieldsCns.push(dicField.jField.cn + '_' + jField.cn);
				
				updateCallback = function() {
//					console.warn('READY!!!');
					// dodajemy pole do listy widocznych p�l w s�owniku
					var newVisibleFieldsCns = oldVisibleFieldsCns.slice(0),
						cmdUpdate = new DsbXmlDocument.Command({cn: dicField.jField.cn}, 'fast update');
					
					newVisibleFieldsCns.push(jField.cn);
					// usuwamy przedrostek s�ownika
					for(i = 0; i < newVisibleFieldsCns.length; i ++) {
						newVisibleFieldsCns[i] = newVisibleFieldsCns[i].replace(dicField.jField.cn + '_', '');
					}
					
					cmdUpdate.options = {
						skip: true
					};
					cmdUpdate.from['visible-fields__TYPE'] = oldVisibleFieldsCns.join(',');
					cmdUpdate.to['visible-fields__TYPE'] = newVisibleFieldsCns.join(',');
					dsbXmlDocument.execCommand(cmdUpdate);
				}
				
				// wysy�amy list� bez naszego pola
				dicField.dictionaryFieldsManager.updateFields(jFields, {callback: 
					function fmDicCallback() {
						var prevFieldCn = cmd.options && cmd.options.prevFieldCn != null ? cmd.options.prevFieldCn : null,
							prevFieldIndex = null;
						// i razem z nowym polem
						jField.status = 'ADD';
						jField.kind = 'DICTIONARY_FIELD';
						jField.cn = dicField.jField.cn + '_' + jField.cn;
						if(prevFieldCn != null) {
//							console.warn('searching', prevFieldCn);
							for(i = 0; i < jFields.length; i ++) {
								if(jFields[i].cn == prevFieldCn) {
//									console.warn('found at index', i);
									prevFieldIndex = i;
									break;
								}
							}
							jFields.splice(prevFieldIndex + 1, 0, jField);
						} else {
							jFields.push(jField);
						}
						// gdy ju� mamy prze�adowany s�ownik to ustawiamy list� widocznych p�l
						// w updateCallback
						dicField.dictionaryFieldsManager.updateFields(jFields, 
							{callback: updateCallback});
					}
				});
			break;
			case 'refresh':
				fieldsManager.updateFields([], {
					callback: function refreshCallback() {
						var jFields = _this.getFieldsList(),
							i = 0, j = 0;
						for(i = 0; i < jFields.length; i ++) {
							jFields[i].status = 'ADD';
							if(jFields[i].type == 'DICTIONARY') {
								for(j = 0; j < jFields[i].dictionary.fields.length; j ++) {
									jFields[i].dictionary.fields[j].status = 'ADD';
								}
							}
						}
						fieldsManager.updateFields(_this.getFieldsList());
					}
				});
			break;
			case 'create':
			case 'create in dictionary':
				console.warn('create', cmd);
			break;
			}
		});
	}
	
	var collectField = function(fieldCn, jqFieldContainer, parentFieldCn) {
		var	jField = null,
			jFields = null, 
			i = 0, dsbDicField = null, dsbParentField = null;
		console.warn('collect field', fieldCn);
		if(dsbFields[fieldCn] != null) {
			dsbFields[fieldCn].initDragAndDrop(jqFieldContainer);
		} else if(parentFieldCn != null) {
			jFields = dsbFields[parentFieldCn].getjField().dictionary.fields;
			for(i = 0; i < jFields.length; i ++) {
				if(jFields[i].cn == fieldCn) {
					jField = jFields[i];
					break;
				}
			}
//			console.info('add dic field', jField.cn, parentFieldCn);
			dsbParentField = dsbFields[parentFieldCn];
			try {
				dsbDicField = dsbParentField.getDsbDictionaryField(jField.cn);
			} catch(e) {}
			
			if(dsbDicField == null) {
				/* TODO */
				// powinni�my usun�� stare pola
				dsbParentField.addDsbDictionaryField(new DsbDictionaryField(dsbXmlDocument, jField, jqFieldContainer, parentFieldCn));
			} else {
				dsbDicField.update(dsbXmlDocument, jField, jqFieldContainer, parentFieldCn);
			}
		}
	}
	
	
	var collectGrid = function(jqCells) {
		if(!jqCells) return;
		var cn;
		jqCells.droppable('destroy').droppable({
			accept: '.dsb-field-draggable',
			hoverClass: 'dsb-drop-hover',
			tollerance: 'pointer',
			drop: function(event, ui) {
				var jqThis = $j(this);
				var fieldCn = ui.draggable.data('cn');
				
				var cmd = dsbFields[fieldCn].moveToCell(grid, jqThis);
				
				if(cmd != null) {
					dsbXmlDocument.execCommand(cmd);
				}
			},
			over: function(event, ui) {
				var jqThis = $j(this);
				var fieldCn = ui.draggable.data('cn');
				
				dsbFields[fieldCn].showPlaceholder(grid, jqThis, ui.offset.left, ui.offset.top);
			}
		});
		
		function createNewField(params) {
//			console.info(newField);
			newField.xml.attr('coords', params.coords);
			//newField.jField.coords = params.coords;
			var cmd = new DsbXmlDocument.Command(newField, 'create');
			dsbXmlDocument.execCommand(cmd);
			newField = null;
		}
		
		function createNewDictionaryField(params) {
			var cmd = new DsbXmlDocument.Command(newField, 'create in dictionary');
			cmd.options = {
				dictionaryFieldCn: params.dictionaryFieldCn,
				prevFieldCn: params.prevFieldCn
			};
			dsbXmlDocument.execCommand(cmd);
			newField = null;
		}
		
		function createNewRow(params) {
			var cmd = new DsbXmlDocument.Command(params, 'insert row');
			dsbXmlDocument.execCommand(cmd);
		}
		
		function selectCell(event, ui, options) {
			console.warn('select cell', $j(event.currentTarget).data('cn'));
			var dictionaryCn = null, cmd = null;
			if(newField != null) {
				if($j(event.target).hasClass('field')) {
//					console.warn(event.target);
//					console.info('dodajemy pole do s�ownika');
					dictionaryCn = $j(event.currentTarget).data('cn');
//					console.info(dictionaryCn);
					createNewDictionaryField({
						dictionaryFieldCn: dictionaryCn,
						prevFieldCn: $j(event.target).data('cn')
					});
				} else {
					$j('div.field.ui-selected').trigger('unselected');
					createNewField({
						coords: ui.selected.id.replace(grid.uid + 'td-', '')
					});
				}
				return false;
			}
			if(newRow != null) {
				createNewRow({
					coords: ui.selected.id.replace('td-', '')
				});
				return false;
			}
			if($j(ui.selected).children('div.field').length) {
				var jqField = $j(ui.selected).children('div.field');
				var fieldCn = jqField.data('cn');
				
				// odznaczamy wszystkie inne pola
				$j('div.field.ui-selected').not(jqField).trigger('unselected');
				
				dsbFields[fieldCn].select(grid, true, options);
				cmd = new DsbXmlDocument.Command({cn: fieldCn}, 'select');
				dsbXmlDocument.execCommand(cmd);
			} else {
				$j('div.field.ui-selected').trigger('unselected');
			}
		}
		
		function unSelectCell(event, ui) {
			if($j(ui.unselected).children('div.field').length) {
				var jqField = $j(ui.unselected).children('div.field');
				var fieldCn = jqField.data('cn');
//				console.dir(dsbFields);
				dsbFields[fieldCn].select(grid, false);
				cmd = new DsbXmlDocument.Command({cn: fieldCn}, 'unselect');
				dsbXmlDocument.execCommand(cmd);
			}
		}
		
		function select(event) {
			console.warn('mouse down div.field');
			if($j(this).hasClass('ui-selected') == false) {
				$j('div.field.ui-selected').not(this).trigger('unselected');
				selectCell(event, {selected: $j(this).parent('td')});
				if(event.target !== this) {
					event.stopPropagation();
					event.stopImmediatePropagation();
				}
				
				return false;
			} else if(selectable == false) {
				selectCell(event, {selected: $j(this).parent('td')});
			}
		}
		
		jqCells.unbind('selected').bind('selected', function(event, options) {
			selectCell(null, {selected: this}, options);
		}).unbind('unselected').bind('unselected', function(event) {
			unSelectCell(null, {unselected: this});
		}).off('mousedown.selectable', 'div.field:not(.DICTIONARY_FIELD)', select)
		.on('mousedown.selectable', 'div.field:not(.DICTIONARY_FIELD)', select);
		
		
		/*
		 * .children('div.field').unbind('mousedown.selectable').bind('mousedown.selectable', function(event) {
			console.warn('mouse down div.field');
			if($j(this).hasClass('ui-selected') == false) {
				$j('div.field.ui-selected').not(this).trigger('unselected');
				selectCell(event, {selected: $j(this).parent('td')});
				if(event.target !== this) {
					event.stopPropagation();
					event.stopImmediatePropagation();
				}
				
				return false;
			} else if(selectable == false) {
				selectCell(event, {selected: $j(this).parent('td')});
			}
		});
		 */
		
		jqCells.parent('tr').parent('tbody').selectable('destroy').selectable({
			filter: 'td',
			selected: selectCell,
			unselected: unSelectCell
		});
	}
	
	// privileged
	this.params = dockindParams;
	this.init = function(xmlDoc) {
		dsbXmlDocument.init(xmlDoc);
		initFields();
		initProcesses();
		initFieldsManager();
		$j(document).unbind('dwr_grid_normalized.dsbdockind').bind('dwr_grid_normalized.dsbdockind', function() {
			// tylko raz
			$j(document).unbind('dwr_grid_normalized.dsbdockind');
			// minimalny wymiar grida to 10x10
			var actualCols = grid.size.cols, actualRows = grid.size.rows,
				newCols = 10-actualCols, newRows = 10-actualRows;
			
			if(newCols < 0)
				newCols = 1;
			if(newRows < 0)
				newRows = 1;
			grid.expand(newCols, newRows);
		});
	}
	
	this.getXML = function() {
		return dsbXmlDocument.getXML();
	}
	
	this.getDsbXmlDocument = function() {
		return dsbXmlDocument;
	}
	
	this.setGrid = function(grd) {
		grid = grd;
	}
	
	this.getGrid = function() {
		return grid;
	}
	
	this.setNewField = function(params) {
		console.warn('set new field', params);
		
		var cn = null;
		selectable = params ? false : true;
		
		if(!params) {
			newField = null;
			grid.jqTable.removeClass('new-field').css('cursor', 'auto');
		} else {
			grid.jqTable.addClass('new-field').css('cursor', params.cursor + ' 5 8, auto');
			newField = {
				jField: params.jField,
				xml: params.xml
			}
		}
		
		for(cn in dsbFields) {
			dsbFields[cn].setSelectable(selectable);
		}
	}
	
	this.setNewRow = function(params) {
		if(!params) {
			newRow = null;
			grid.jqTable.removeClass('insert-row');
			return;
		}
		
		grid.jqTable.addClass('insert-row');
		newRow = params;
	}
	
	this.deleteField = function() {
		var counter = 0, key = null,
			cmd = null, childKey = null,
			dsbField = null, dsbChildField = null,
			child = false;
		for(key in selectedFieldCns) {
			if(selectedFieldCns[key] === true) {
				counter ++;
				dsbField = dsbFields[key] || null;
				// je�eli zaznaczono s�ownik i pole w s�owniku to usuwamy tylko pole w s�owniku
				if(dsbField && dsbField.getjField().type == 'DICTIONARY') {
					for(childKey in selectedFieldCns) {
						// pomijamy s�ownik
						if(childKey == key)
							continue;
						// zaznaczone pole w s�owniku
						dsbChildField = dsbField.getDsbDictionaryField(childKey) || null;
						if(dsbChildField && dsbChildField.getjField().cn == childKey) {
//							console.warn('usuwamy pole w s�owniku', childKey);
							child = true;
							break;
						}
					}
				}
				
				if(child) {
					// usuwamy pole ze s�ownika
					dsbField.deleteDsbDictionaryField(childKey);
					delete selectedFieldCns[childKey];
					cmd = new DsbXmlDocument.Command({cn: childKey}, 'delete from dictionary');
					cmd.options = {
						dictionaryFieldCn: key
					};
					dsbXmlDocument.execCommand(cmd);
					break;
				} else {
					delete dsbFields[key];
					delete selectedFieldCns[key];
					cmd = new DsbXmlDocument.Command({cn: key}, 'delete');
					dsbXmlDocument.execCommand(cmd);
					break;
				}
				
			}
		}
	}
	
	this.initFieldCollector = function() {
		fieldCollector = new DsbUtils.FieldCollector(collectField, collectGrid);
	}
	
	this.getDockindCn = function() {
		return dockindCn;
	}
	
	this.getFieldsList = function(skipCns) {
		var cn;
		var fieldsList = [], skip = skipCns || [];
		
		if(skipCns == null || skipCns.length == 0)
			skipFieldCns = [];
		for(cn in dsbFields) {
			//fieldsList.push(Field.prototype.clonejField.call({jField: dsbFields[cn].getjField()}));
			if($j.inArray(cn, skip) == -1) {
				fieldsList.push(dsbFields[cn].getjField());
			} else {
				skipFieldCns.push(cn);
			}
		}
		return fieldsList;
	}
	
	this.getDictionaryFieldsList = function(dictionaryCn, skipCns) {
		var cn = null, fieldsList = [], skip = skipCns || [],
			dsbDicField = dsbFields[dictionaryCn],
			dsbDicFields = dsbDicField.getDsbDictionaryFields(),
			i = 0, len = dsbDicFields.length, cn = null;
		
		if(skipCns == null || skipCns.length == 0)
			skipFieldCns = [];
		
		for(i = 0; i < len; i ++) {
			cn = dsbDicFields[i].getjField().cn;
			if($j.inArray(cn, skip) == -1) {
				fieldsList.push(dsbDicFields[i].getjField());
			} else {
				skipFieldCns.push(dsbDicFields[i].getjField().cn);
			}
		}
		return fieldsList;
	}
	
	this.getFieldsValues = function() {
		var cn;
		var fieldsValues = {};
		
		for(cn in dsbFields) {
			if($j.inArray(cn, skipFieldCns) == -1) {
				fieldsValues[cn] = dsbFields[cn].getValue();
			}
		}
		
		return fieldsValues;
	}
	
	this.getDictionaryFieldsValues = function(dictionaryName, itemId) {
		return dsbFields[dictionaryName].getValue(itemId);
	}
	
	this.getProcesses = function() {
		return processes;
	}
	
	this.searchDictionary = function(dictionaryName, values, limit, offset) {
		return dsbFields[dictionaryName].dictionarySearch(values, limit, offset);
	}
	
	this.dispose = function() {
		if(fieldCollector != null) {
			fieldCollector.stop();
		}
		if(dsbXmlDocument != null) {
			dsbXmlDocument.dispose();
			delete dsbXmlDocument;
		}
	}
	
	this.debug = {
		dsbFields: dsbFields,
		dsbXmlDocument: dsbXmlDocument,
		selectedFieldCns: selectedFieldCns
	}
}