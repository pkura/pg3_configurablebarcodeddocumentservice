DsbEditor = function(pageContext) {
	// private
	var dsbEditorSettings = null,
		settings = null,
		dsbDockind = null,
		dsbMenu = null,
		dsbXmlEditor = null,
		dsbSchemaDocument = null,
		dsbPropertiesEditor = null,
		dsbWelcomeDialog = null;
	var actionUrl = pageContext + '/dwrdockind/dwr-editor.action',
		dockinds = null;
	var initMenu = function() {
		dsbMenu = new DsbMenu();
		dsbMenu.init();
		dsbMenu.show();
		
		dsbMenu.addCommand('#menuFileNew', function() {
			$j.post(actionUrl, {dockindTemplate: true}, function(data) {
				if(data == -1)
					throw 'Error loading dockind template!';
				initDockind(data, {cn: ''});
				dsbPropertiesEditor.setInfoText('nowy');
				$j(document).trigger('dwr_grid_normalized');
			});
		});
		
		dsbMenu.addCommand('#menuFileOpen', function() {
			var jqFileInput = $j('#file');
			var jqPingBackXML = $j('#pingBackXML');
			jqFileInput.fileupload({
				done: function(e, data) {
					try {
						initDockind(data.result, {cn: 'plik'});
					} catch(e) {
						var msg = 'Nieznany b��d';
						// je�li niepoprawny XML
						if(Object.prototype.toString.call(e) == '[object HTMLCollection]') {
							msg = DsbNotification.translateXmlError(data.result);
						} else {
							msg = e + '';
						}
						DsbNotification.pushMessage(msg, {type: 'error'});
						console.error(e);
						console.trace();
						throw e;
					}
					
					DsbNotification.pushMessage('Wczytano plik ' + (data.files[0].name).bold(), {type: 'info'});
					dsbPropertiesEditor.setInfoText(data.files[0].name);
					dsbMenu.enableCommand('#menuViewXmlEditor');
					dsbMenu.enableCommand('#menuViewRefresh');
				}
			});
			jqPingBackXML.val('true');
			jqFileInput.trigger('click');
		});
		
		dsbMenu.addCommand('#menuFileSave', function() {
			DsbDockindDialog.update(dsbDockind);
			DsbDockindDialog.show({
				mode: 'save',
				onSave: function(dockindParams) {
					DsbDockindDialog.hide();
					DsbNotification.showOverlayMsg();
					var fd = new FormData(),
						xmlString = (new XMLSerializer()).serializeToString(dsbDockind.getXML());
					
					fd.append('dockindUpdate', true);
					fd.append('xml', new Blob([xmlString], {type: 'text/xml'}));
					fd.append('json', JSONStringify(dockindParams));
					
					$j.ajax({
						url: actionUrl,
						type: 'POST',
						data: fd,
						processData: false,
						contentType: false
					}).done(function(result) {
						var xml = null;
						
						DsbNotification.hideOverlayMsg();
						console.info(result);
						if(result.success === true) {
							DsbNotification.pushMessage('Zapisano', {type: 'info'});
							
							try {
								xml = $j.parseXML(result.xmlString);
								/*console.info(xml);
								tmp100 = xml;*/
							} catch(e) {
								console.error(e);
							}
							
							initDockind(xml, result.props);
							dsbPropertiesEditor.setInfoText(result.props.name);
						} else {
							DsbNotification.pushMessage(result.errorMessage || 'Error', {type: 'error'});
							console.error(result);
						}
					});
				}
			});
		});
		
		dsbMenu.addCommand('#menuFileSaveAs', function() {
			var xmlString = (new XMLSerializer()).serializeToString(dsbDockind.getXML());
			
			$j('#dsb-form-downloader').remove();
			var jqFormDown = $j('<form/>').attr('action', actionUrl).attr('method', 'post').attr('accept-charset', 'utf-8')
				.append('<input type="hidden" name="pingBackXMLFile" value="true" />')
				.append('<input type="hidden" name="xmlString" value="' + encodeURIComponent(xmlString) + '" />');
			$j('body').append(jqFormDown);
			jqFormDown.submit();
		});
		
		dsbMenu.addCommand('#menuFileInfo', function() {
			DsbDockindDialog.update(dsbDockind);
			DsbDockindDialog.show({mode: 'info'});
		});
		
		dsbMenu.addCommand('#menuFileClose', function(domObj, params) {
			window.location.href = $j(domObj).attr('data-url');
		});
		
		dsbMenu.addCommand('#menuEditInsertRow', function() {
			dsbDockind.setNewRow(true);
		});
		
		dsbMenu.addCommand('#menuEditUndo', function() {
			dsbDockind.getDsbXmlDocument().undo();
		});
		
		dsbMenu.addCommand('#menuEditDelete', function(domObj, params) {
			if(params.disabled === false) {
				dsbDockind.deleteField();
			}
		});
		
		dsbMenu.addCommand('#menuHelpShowWelcomeScreen', function(domObj, params) {
			dsbWelcomeDialog.show({
				welcomeScreen: dsbEditorSettings.settings.welcomeScreen
			});
		});
		
		dsbMenu.addCommand('#menuViewRefresh', function(domObj, params) {
			if(params.disabled === false) {
				dsbDockind.getDsbXmlDocument().refresh();
			}
		});
		
		dsbMenu.addCommand('#menuViewXmlEditor', function(domObj, params) {
			if(params.disabled === false) {			
				if(dsbXmlEditor.isVisible() === false) {
					dsbXmlEditor.open();
				} else {
					dsbXmlEditor.close();
				}
				dsbMenu.toggleCommandCheckbox('#menuViewXmlEditor', !dsbMenu.isChecked('#menuViewXmlEditor'));
			}
		});
		
		dsbMenu.addCommand('#menuViewHiddenFields', function(domObj) {
			if(dsbMenu.isChecked('#menuViewHiddenFields')) {
				DsbUtils.Stylesheet.removeRule('hiddenFields');
				dsbMenu.toggleCommandCheckbox('#menuViewHiddenFields', false);
			} else {
				DsbUtils.Stylesheet.addRule('hiddenFields');
				dsbMenu.toggleCommandCheckbox('#menuViewHiddenFields', true);
			}
		});
	}
	
	var initXmlEditor = function() {
		dsbXmlEditor = new DsbXmlEditor();
		dsbXmlEditor.setSettings({
			xPos: settings.xmlEditorX,
			yPos: settings.xmlEditorY,
			width: settings.xmlEditorW,
			height: settings.xmlEditorH
		});
		dsbXmlEditor.subscribe('close', function() {
			var sets = dsbXmlEditor.getSettings();
			
			dsbEditorSettings.settings.xmlEditorX = sets.xPos;
			dsbEditorSettings.settings.xmlEditorY = sets.yPos;
			dsbEditorSettings.settings.xmlEditorW = sets.width;
			dsbEditorSettings.settings.xmlEditorH = sets.height;
			
			dsbEditorSettings.save();
			dsbMenu.toggleCommandCheckbox('#menuViewXmlEditor', false);
		});
		$j(window).bind('beforeunload', function() {
			if(dsbXmlEditor != null && dsbXmlEditor.isVisible()) {
				dsbXmlEditor.close();
			}
		});
	}
	
	var initPropertiesEditor = function() {
		dsbSchemaDocument = new DsbSchemaDocument();
		dsbPropertiesEditor = new DsbPropertiesEditor({actionUrl: actionUrl});
		$j.post(actionUrl, {dockindSchema: true}, function(data) {
			if(data == '-1')
				alert('Error loading schema from database');
			dsbSchemaDocument.init(data);
			dsbSchemaDocument.parse();
			dsbPropertiesEditor.init(dsbSchemaDocument);
			dsbPropertiesEditor.hide();
		});
	}
	
	var initWelcomeDialog = function() {
		dsbWelcomeDialog = new DsbWelcomeDialog();
		dsbWelcomeDialog.addCloseListener(function(options) {
			settings.welcomeScreen = options.welcomeScreen;
			dsbEditorSettings.save();
		});
	}
	
	var initKeyboard = function() {
		jwerty.key('delete', function() {
			if(dsbPropertiesEditor && !dsbPropertiesEditor.hasFocus()) {
				console.warn('delete');
				dsbDockind.deleteField();
			}
		});
		jwerty.key('ctrl+alt+j', function() {
			alert(JSON.stringify(dsbDockind.getFieldsList()));
		});
		jwerty.key('arrow-right', function() {
			console.info('right');
		});
	}
	
	var assignDWR = function(dsbDockind) {
		dwrParams.dockind = dsbDockind.getDockindCn();
		dwrParams.documentType = "1";
		
		try {
			fieldsManager.grid.dispose();
			fieldsManager.dispose();
			dsbDockind.getGrid().updateSizeInfo(0, 0);
		} catch(e) {
			console.error(e);
		}
		
		// tworzenie ukrytych p�l s�ownika
		
		DwrTest.getFieldsList = function(dwrParams, obj) {
//			console.info('fake dwr');
//			console.info(dsbDockind.getFieldsList());
			// console.info(func);
			obj.callback(dsbDockind.getFieldsList());
		}
		
		DwrTest.getFieldsValues = function(dwrParams, obj) {
//			console.info('fake values');
			obj.callback(dsbDockind.getFieldsValues());
		}
		
		Dictionary.getFieldsValues = function(dictionaryName, itemId, obj) {
//			console.info('fake dictionary values');
			setTimeout(function() {
				obj.callback(dsbDockind.getDictionaryFieldsValues(dictionaryName, itemId));
			}, 0);
		}
		
		Dictionary.getFieldsValuesList = function(dictionaryName, itemIdList, obj) {
			setTimeout(function() {
				obj.callback(dsbDockind.getDictionaryFieldsValues(dictionaryName, itemIdList[0]));
			}, 0);
		}
		
		Dictionary.dwrSearch = function(dictionaryName, values, limit, offset, obj) {
			setTimeout(function() {
				obj.callback(dsbDockind.searchDictionary(dictionaryName, values, limit, offset));
			}, 0);
		}
		
		var grid = fieldsManager.grid;
		fieldsManager = new FieldsManager();
		fieldsManager.init({
			grid: new Grid($j('#grid'), {grid: grid})
		});
	}

	var loadDockindList = function(func) {
		$j.post(actionUrl, {dockindList: true}, function(data) {
			if(data == -1)
				throw 'Error loading dockinds list from database';
			dockinds = data;
			func();
		});
	}
	
	var initDockind = (function initDockindSingleton() {
		// static
		var tokens = {
			'execute command': null,
			'change': null,
			'new': null
		}
		
		// function body
		return function initDockind_(data, dockindParams) {
			if(dsbDockind != null) {
				if(tokens['execute command'] != null) {
					if(dsbDockind.getDsbXmlDocument().unsubscribe(tokens['execute command']) === false) {
						alert('Could not unsubscribe ' + tokens['execute command'] || 'NULL');
					}
				}
				
				dsbDockind.dispose();
				dsbDockind = null;
			}
			if(data.getElementsByTagName('parsererror').length > 0) {
				throw data.getElementsByTagName('parsererror');
			}		
			
			dsbDockind = new DsbDockind(dockindParams);
			dsbDockind.init(data);
			dsbDockind.setGrid(fieldsManager.grid);
			dsbDockind.initFieldCollector();
			assignDWR(dsbDockind);
			dsbMenu.enableCommand('#menuViewXmlEditor');
			dsbMenu.enableCommand('#menuViewRefresh');
			dsbXmlEditor.assignDsbXmlDocument(dsbDockind.getDsbXmlDocument());
			
			//var dsbXmlDocument = dsbDockind.getDsbXmlDocument();
			dsbPropertiesEditor.clear();
			dsbPropertiesEditor.updateProcessEditor(dsbDockind.getProcesses());
			/*dsbXmlDocument.subscribe('select', function(topic, props) {
				console.warn('select');
				dsbPropertiesEditor.clear();
				dsbPropertiesEditor.update(props);
			})*/
			
			tokens['execute command'] = dsbDockind.getDsbXmlDocument().subscribe('execute command', 
				function dsbEditorExecuteCommand(topic, cmd) {
//					console.trace();
					switch(cmd.mode.toLowerCase()) {
						case 'update':
							/*console.info('UPDATE');
							console.dir(cmd);
							console.trace();*/
							dsbPropertiesEditor.update(cmd.to, cmd.from.cn, cmd.options);
							dsbPropertiesEditor.show();
						break;
						case 'select':
//							console.warn('select ptop editor', cmd);
							dsbPropertiesEditor.clear();
							dsbPropertiesEditor.update(cmd.to, cmd.from.cn, cmd.options);
							dsbPropertiesEditor.show();
							dsbMenu.enableCommand('#menuEditDelete');
						break;
						case 'unselect':
							dsbPropertiesEditor.hide();
							dsbPropertiesEditor.clear();
							dsbMenu.disableCommand('#menuEditDelete');
						break;
						case 'refresh':
							dsbPropertiesEditor.hide();
							dsbPropertiesEditor.clear();
						break;
						case 'create':
						case 'create in dictionary':
	//						console.info('created!');
							dsbPropertiesEditor.triggerButton('cursor');
						break;
						case 'delete':
							dsbMenu.disableCommand('#menuEditDelete');
						break;
						case 'insert row':
							dsbDockind.setNewRow(null);
							$j('div.field.ui-selected').trigger('unselected');
							dsbDockind.getDsbXmlDocument().refresh();
						break;
					}
				});
			
			if(tokens['change'] != null && dsbPropertiesEditor.unsubscribe(tokens['change']) === false) {
				alert('Could not unsubscribe ' + tokens['change'] || 'NULL');
			}
			tokens['change'] = dsbPropertiesEditor.subscribe('change', function(topic, cmd) {
//				console.warn('CHANGE');
				if(dsbPropertiesEditor.getFieldCn() != null) {
					dsbDockind.getDsbXmlDocument().execCommand(cmd);
				}
			});
			
			if(tokens['new'] != null && dsbPropertiesEditor.unsubscribe(tokens['new']) === false) {
				alert('Could not unsubscribe ' + tokens['new'] || 'NULL');
			}
			tokens['new'] = dsbPropertiesEditor.subscribe('new', function(topic, params) {
//				console.info(params);
				dsbDockind.setNewField(params);
			});
			
			if(tokens['change processes'] != null && dsbPropertiesEditor.unsubscribe(tokens['change processes']) === false) {
				alert('Could not unsubscribe ' + tokens['change processes'] || 'NULL');
			}
			tokens['change processes'] = dsbPropertiesEditor.subscribe('change processes', function(topic, cmd) {
				var procs = null;
				dsbDockind.getDsbXmlDocument().execCommand(cmd);
				
				dsbPropertiesEditor.getProcessesRequiredFields(cmd.to, function(fields) {
					var fieldCn = null, iterField = null, xmls = [],
						i = 0, cmd = null, fieldParams = null,
						jqEmptyCells = fieldsManager.grid.getEmptyCells();
					
					for(fieldCn in fields) {
						iterField = fields[fieldCn];
						fieldParams = {
							cn: fieldCn,
							name__TYPE: iterField.TYP,
							coords: Grid.extractCellCoords($j(jqEmptyCells[i]))
						};
						if(iterField.TYP == 'enum') {
							fieldParams['enum-list__CUSTOM'] = [];
							(function() {
								var items = iterField.VALUE, i = 0,
									enumItem = null;
								for(i = 0; i < items.length; i ++) {
									enumItem = {
										cn: items[i],
										title: items[i]
									};
									fieldParams['enum-list__CUSTOM'].push(enumItem)
								}
							})();
						}
						cmd = new DsbXmlDocument.Command(fieldParams, 'create');
						try {
							dsbDockind.getDsbXmlDocument().execCommand(cmd);
						} catch(e) {
							if(e == 'Field exists') {
								delete fieldParams.coords;
								cmd = new DsbXmlDocument.Command(fieldParams, 'update');
								dsbDockind.getDsbXmlDocument().execCommand(cmd);
							} else {
								throw e;
							}
						}
						
						$j('div.field.ui-selected').trigger('unselected');
						i ++;
					}
				});
			});
		}
	})();
	
	// privileged
	this.init = function() {
		dsbEditorSettings = new DsbEditorSettings();
		dsbEditorSettings.load();
		settings = dsbEditorSettings.settings;
		initMenu();
		initXmlEditor();
		initKeyboard();
		initWelcomeDialog();
		$j(document).bind('dwr_loaded.dsb', function() {
//			console.warn('complete');
			// tylko raz
			$j(document).unbind('dwr_loaded.dsb');
			//$j('#menuFileNew').trigger('click');
		});
		if(settings.welcomeScreen === true) {
			dsbWelcomeDialog.show({welcomeScreen: true});
		}
		try {
			initPropertiesEditor();
		} catch(e) {
			alert(e);
		}
		try {
			loadDockindList(function() {
				var i;
				var str = '';
				for(i = 0; i < dockinds.length; i ++) {
					str += '<li cn="' + dockinds[i].cn + '">' + (i+1)+'.&nbsp;' + dockinds[i].name + '</li>';
				}
				
				$j('.sepDockindList').after(str);
				
				dsbMenu.addCommand('#menu li[cn]', function(domMenuItem) {
					var cn = $j(domMenuItem).attr('cn');
					DsbNotification.showOverlayMsg();
					$j.post(
						actionUrl, 
						{
							dockindXML: true,
							loadXML: true,
							dockindCn: cn
						},
						function(data) {
							try {
								DsbNotification.pushMessage('Wczytano dokument o nazwie kodowej ' + cn.bold(), {type: 'info'});
								
								var i = 0, dockindParams = null;
								for(i = 0; i < dockinds.length; i ++) {
									if(dockinds[i].cn == cn) {
										dockindParams = dockinds[i];
										break;
									}
								}
								var dockindName = dockindParams.name != '' ? dockindParams.name : cn;
								
								initDockind(data, dockindParams);
								dsbPropertiesEditor.setInfoText(dockindName);
								DsbNotification.hideOverlayMsg();
							} catch(e) {
								DsbNotification.pushMessage('B��d wczytywanie dokumentu ' + cn.bold() + ': ' + e, {type: 'error'});
							}
						},
						'xml'
					);
				});
			});
		} catch(e) {
		} finally {
			
		}
	}

	this.JSON = {
		getFields: function() {
			return JSON.stringify(dsbDockind.getFieldsList());
		}
	};
	
	// debug
	this.debug = {
		getXML: function() {
			return dsbDockind.getXML();
		},
		getDockind: function() {
			return dsbDockind;
		}
	}
}

DsbEditor.SNAP_GRID_TOLERANCE = 10;

