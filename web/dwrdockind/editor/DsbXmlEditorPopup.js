DsbXmlEditorPopup = function(jqTextarea) {
	// private
	var xml = null,
		jqTextarea = jqTextarea,
		codeMirror = null;

	function winHeight() {
      return window.innerHeight || (document.documentElement || document.body).clientHeight;
    }
    
	function setFullScreen(full) {
      var jqWrap = $j(codeMirror.getWrapperElement());
	  // alert(winHeight());
      
      if(full) {
    	  jqWrap.addClass('fullscreen');
    	  jqWrap.height(winHeight());
    	  $j('html').css('overflow', 'hidden');
      } else {
    	  jqWrap.addClass('fullscreen');
    	  jqWrap.css('height', 'auto');
    	  $j('html').css('overflow');
      }
      
      codeMirror.setOption('fullscreen', true);
      codeMirror.refresh();
    }
	
	var initCodeMirror = function() {
		var xmlString = (new XMLSerializer()).serializeToString(xml);
		jqTextarea.html(xmlString);
		
		codeMirror = CodeMirror.fromTextArea(jqTextarea[0], {
			onChange: function(inst, params) {
				console.info('code mirror changed!');
				console.info(params);
			},
			readOnly: true
		});
		
		setFullScreen(true);
		
		$j(window).resize(function() {
			console.warn('resize!');
			$j(codeMirror.getWrapperElement()).height(winHeight());
		});
		
		window.onresize = function() {
			console.info('RESIZE!');
		}
		
		/*var jqScrollerElement = $j(codeMirror.getScrollerElement());
		jqScrollerElement.height($j('body').height());*/
		//codeMirror.refresh();
		
		/* CodeMirror.on(window, "resize", function() {
	      var showing = document.body.getElementsByClassName("CodeMirror-fullscreen")[0];
	      if (!showing) return;
	      showing.CodeMirror.getWrapperElement().style.height = winHeight() + "px";
	    });*/
	}
	
	//privileged
	this.assignXML = function(xmlDocument) {
		xml = xmlDocument;
		console.dirxml(xml);
	}
	
	this.update = function() {
		var xmlString = null;
		
		if(codeMirror == null) {
			initCodeMirror();
		} else {
			xmlString = (new XMLSerializer()).serializeToString(xml);
			var oldVal = codeMirror.getValue();

			codeMirror.setValue(xmlString);
			setFullscreen(true);
			
			/*var newVal = codeMirror.getValue();
			console.dir(differ.diff_main(oldVal, newVal));
			
			var patches = differ.patch_make(oldVal, newVal);
			console.dir(patches);
			try {
				var start = codeMirror.posFromIndex(patches[0].start2);
				var end = codeMirror.posFromIndex(patches[0].start2 + patches[0].length2);
				codeMirror.setSelection(start, end);
			} catch(e) {
				console.error(e);
			}*/		
		}

		tmp200 = codeMirror;
	}
}