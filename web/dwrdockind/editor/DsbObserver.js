DsbObserver = function() {
	var topics = {}, subUid = -1;
	
	this.publish = function(topic, args) {
		if(!topics[topic]) {
//			console.warn('Unknown topic', topic);
			return false;
		}
		
		var subscribers = topics[topic],
			len = subscribers ? subscribers.length : 0,
			i = 0;
		
		for(i = 0; i < len; i ++) {
			subscribers[i].func(topic, args);
		}
		/*
		while(len--) {
			subscribers[len].func(topic, args);
		}*/
		
		return true;
	}
	
	this.subscribe = function(topic, func) {
		if(!topics[topic])
			topics[topic] = [];
		
		var token = (++subUid).toString();
		topics[topic].push({
			token: token,
			func: func
		});
		
		return token;
	}
	
	this.unsubscribe = function(token) {
		var t, i, len;
		
		for(t in topics) {
			if(topics[t]) {
				for(i = 0, len = topics[t].length; i < len; i ++) {
					if(topics[t][i].token === token) {
						topics[t].splice(i, 1);
						return token;
					}
				}
			}
		}
		
		return false;
	}
	
	this.debug = function() {
		return {
			topics: topics
		}
	}
}