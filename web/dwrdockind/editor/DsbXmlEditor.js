DsbXmlEditor = function() {
	// private
	var dsbXmlDocument = null,
		settings = null,
		wnd = null,
		observer = new DsbObserver(),
		popupUrl = $j('#request_pageContext').val() + '/dwrdockind/dwr-editor-popup.action',
		visible = false,
		firstUnloadEvent = true;
	
	// privileged
	this.subscribe = observer.subscribe;
	
	this.assignDsbXmlDocument = function(dsbXmlDoc) {
		dsbXmlDocument = dsbXmlDoc;
	}
	
	this.update = function() {
		var jqTextarea = $j(wnd.document.body).children('textarea'),
			xml = dsbXmlDocument.getXML();
		
		jqTextarea.html((new XMLSerializer()).serializeToString(xml));
	}
	
	this.setSettings = function(sets) {
		settings = sets;
	}
	
	this.getSettings = function() {
		return settings;
	}
	
	this.open = function() {
		var _this = this;
		var xml = dsbXmlDocument.getXML();
		var xmlString = (new XMLSerializer()).serializeToString(xml);
		
		var features = 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,' + 
			'left=' + settings.xPos + ',' +
			'top=' + settings.yPos + ',' +
			'width=' + settings.width + ',' + 
			'height=' + settings.height + '';
		
		wnd = window.open(popupUrl, 'Edytor', features);
		
		wnd.onload = function() {
			wnd.postMessage(xmlString, '*');
		}
		
		firstUnloadEvent = true;
		dsbXmlDocument.subscribe('execute command', function(topic, cmd) {
			var xml = dsbXmlDocument.getXML();
			var xmlString = (new XMLSerializer()).serializeToString(xml);
			wnd.postMessage(xmlString, '*');
		});
		
		$j(wnd).bind('unload', function() {
			if(!firstUnloadEvent) {
				settings.width = $j(this).width();
				settings.height = $j(this).height();
				settings.xPos = wnd.screenX;
				settings.yPos = wnd.screenY;
				visible = false;
				observer.publish('close');
			}
			firstUnloadEvent = false;
		});
		
		visible = true;
	}
	
	this.close = function() {
		if(wnd != null) {
			wnd.close();
			visible = false;
		}
	}
	
	this.isVisible = function() {
		return visible;
	}
}