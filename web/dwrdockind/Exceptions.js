function FieldNotInitialisedException(msg) { return new Error(this.constructor.name + ': ' + msg); }
function FieldUnsupportedTypeException(msg) { return new Error(this.constructor.name + ': ' + msg); }
function FieldUnsupportedStatusException(msg) { return new Error(this.constructor.name + ': ' + msg); }

function ValidatorUnsupportedTypeException(msg) { return new Error(this.constructor.name + ': ' + msg); }

function VariableNotInitialisedException(msg) { return new Error(this.constructor.name + ': ' + msg); }

function FilterUnsupportedNameException(msg) {	return new Error(this.constructor.name + ': ' + msg); }

function ObjectUnsupportedTypeException(msg) { return new Error(this.constructor.name + ': ' + msg); }