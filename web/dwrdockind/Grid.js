function Grid(jqContainer, options) {
	if(jqContainer == null || !jqContainer.length)
		throw 'No container for grid found!';
	
	this.jqContainer = jqContainer;
	this.jqTable = $j('<table/>').addClass('grid');
	this.snap = null;
	this.size = {cols: 0, rows: 0};
	if(options != null && options.grid != null) {
		this.uid = options.grid.uid;	
		this.tab = options.grid.tab;
		this.jqCachedRows = options.grid.jqCachedRows;
	} else {
		this.uid = Grid.getNextUid();
		this.tab = []; // tablica wiersze (od 1) x kolumna (od 1)
		this.jqCachedRows = []; // cache obiekt�w jQuery dla wierszy
	}
	this.opt = 1;
	
	jqContainer.append(this.jqTable);

	$j('body').bind('grid-resize', function() {
		fixResize();
	});
}

Grid.getNextUid = (function() {
    var uuid = 0;

    return function() {
        return '__GRID_' + (++uuid) + '_';
    }
})();

Grid.prototype.reset = function() {
	if(this.opt > 0) {
		this.tab.length = 0;
		this.jqCachedRows.length = 0;
		this.jqTable.remove();
		this.size = {cols: 0, rows: 0};
	}
}

Grid.prototype.updateSizeInfo = function(col, row) {
	if(col != null && col > this.size.cols)
		this.size.cols = col;
	if(row != null && row > this.size.rows)
		this.size.rows = row;
}

Grid.prototype.dispose = function() {
	console.warn('dispose');
	this.jqTable.remove();
	this.size.cols = this.size.rows = 0;
	this.tab = [];
	this.jqCachedRows = [];
}

Grid.prototype.assignField = function(field) {
	var _this = this;
	var start = field.coords.start, end = field.coords.end,
		col = start.x + 1, row = start.y + 1;
	var jqTd, jqTr, jqNewTd, cellIndex, rowIndex, jqPrevRow;
	if(!start)
		throw 'No coords found for field #' + field.jField.cn;
	
	jqNewTd = this.createCell(col, row) || this.getCell(col, row);
	if(end) {
		if(end.colspan) { 
			jqNewTd.attr('colspan', end.colspan);
		}
		if(end.rowspan) {
			jqNewTd.attr('rowspan', end.rowspan);
		}
	} else {
		jqNewTd.attr('colspan', 1).attr('rowspan', 1);
	}
	jqNewTd.append(field.jqFieldContainer);
	jqNewTd.attr('fieldcn', field.jField.cn);

	if(field.jField.cssClass != null) {
		var parentClassName = field.jField.cssClass.split(' ').join(' dwr-child-has-class-');
		if(parentClassName != '') {
			parentClassName = 'dwr-child-has-class-' + parentClassName;
		}
		jqNewTd.addClass(parentClassName);
	}
	
	/*field.jqField.bind('valueChange', function() {
		//field.align();
	});*/
	
	(function() {
		var setValue = field.setValue;
		var setValueForTable = function(val, params) {
			setValue.call(field, val, params);
			field.jqField.trigger('valueChange');
		}
		field.setValue = setValueForTable;
	})();
	field.align();
	
	if(field.jField.cssClass == 'dwr-special-hidden-field') {
		jqNewTd.addClass('empty-cell');
	} else {
		jqNewTd.removeClass('empty-cell');
	}

	var assignedCounter = this.getCellAssignedCounter(jqNewTd);
	this.setCellAssignedCounter(jqNewTd, ++assignedCounter);

	this.updateSizeInfo(col, row);
}

Grid.prototype.unassignField = function(field) {
	var start = field.coords.start;
	if(!start)
		throw 'No coords found for field #' + field.jField.cn;

	var jqCell = this.getCell(start.x + 1, start.y + 1);

    var counter = this.getCellAssignedCounter(jqCell);
    if(counter > 0) {
        this.setCellAssignedCounter(jqCell, --counter);
    }
    if(counter == 0) {
        jqCell.css({
            'height': '',
            'min-width': '',
            'width': ''
        }).addClass('empty-cell').removeAttr('fieldcn');
    }
}

Grid.prototype.normalizeTable = function() {
	var maxCols = 1, maxRows = 1,
		spannedCells = null, iRow = null, iCol = null,
		jqCreatedCell = null;
	
	function createArray(rowCount, colCount) {
		var i;
		var arr = new Array(rowCount);
		
		for(i = 0; i < rowCount; i ++) {
			arr[i] = new Array(colCount);
		}
		
		return arr;
	}
	
	// wyliczamy liczb� kolumn i wierszy
	this.jqTable.find('td[column][row]').each(function() {
		var jqThis = $j(this);
		var	colspan = jqThis.attr('colspan') ? Number(jqThis.attr('colspan')) : 1,
			column = Number(jqThis.attr('column')) + colspan - 1;
		var rowspan = jqThis.attr('rowspan') ? Number(jqThis.attr('rowspan')) : 1,
			row = Number(jqThis.attr('row')) + rowspan - 1;
		
		if(column > maxCols) {
			maxCols = column;
		}
		if(row > maxRows) {
			maxRows = row;
		}
	});
	
	// oznaczamy kom�rki, kt�rych nie rysujemy (zawieraj�ce si� w colspan lub rowspan)
	spannedCells = createArray(maxRows, maxCols);
	this.jqTable.find('td[column][row]').each(function() {
		var jqThis = $j(this);
		var	colspan = jqThis.attr('colspan') ? Number(jqThis.attr('colspan')) : 1,
			column = Number(jqThis.attr('column'));
		var rowspan = jqThis.attr('rowspan') ? Number(jqThis.attr('rowspan')) : 1,
			row = Number(jqThis.attr('row'));
		
		var i, j, curRow, curCol;
		if(rowspan != 1 || colspan != 1) {
			for(i = 0; i < rowspan; i ++) {
				curRow = i + row - 1;
				for(j = 0; j < colspan; j ++) {
					curCol = j + column - 1;
					spannedCells[curRow][curCol] = true;
				}
			}
		}
	});

	// rysujemy brakuj�ce kom�rki
	for(iRow = 0; iRow < maxRows; iRow ++) {
		var rowIndex = iRow + 1;
		if(this.tab[rowIndex] == null) {
			// szukamy poprzedniego wiersza
			var prevRowIndex = this.getPrevRowIndex(rowIndex);
			jqRow = $j('<tr/>').attr('id', this.uid + 'tr-' + rowIndex).attr('index', rowIndex);
			this.tab[rowIndex] = [];
			
			if(prevRowIndex == null) {
				// jedyny wiersz
				this.jqTable.prepend(jqRow);
			} else {
				var jqPrevRow = this.jqCachedRows[prevRowIndex];

				if(Number(jqPrevRow.attr('index')) < Number(jqRow.attr('index'))) {
					// wstawiamy za
					jqPrevRow.after(jqRow);
				} else {
					// wstawiamy przed
					jqPrevRow.before(jqRow);
				}
			}
			this.jqCachedRows[rowIndex] = jqRow;
		}
		for(iCol = 0; iCol < maxCols; iCol ++) {
			if(spannedCells[iRow][iCol] !== true) {
				jqCreatedCell = this.createCell(iCol+1, iRow+1);
				if(jqCreatedCell != null) {
					jqCreatedCell.addClass('empty-cell');
				}
			} 
		}
	}

	if(ie && this.jqTable.css('table-layout') == 'fixed') {
		this.jqTable.find('tr').each(function() {
			$j(this).attr('height', '30');
		});

		this.jqTable.find('tr:last').attr('height', 'auto');
	}

	this.updateSizeInfo(maxCols, maxRows);
	$j(document).trigger('dwr_grid_normalized', [this.jqTable.find('td')]);
}

Grid.prototype.partialNormalize = function(jqSourceCell, jqDestCell, itemColspan, itemRowspan, options) {
	var opts = {
		copyStyle: options && options.copyStyle === true
	}
	
	var sourceCellStyle = jqSourceCell.attr('style'),
		cells = [];
	var jqSourceRow = jqSourceCell.parent('tr'), 
		sourceCellColumn = Number(jqSourceCell.attr('column')), 
		sourceCellRow = Number(jqSourceCell.attr('row'));
	var jqNewCell, jqIterRow, cellPos, cellIndex, i, j;
	
	if(jqDestCell) {
		var destCellColumn = Number(jqDestCell.attr('column')), destCellRow = Number(jqDestCell.attr('row')),
			jqDestRow = jqDestCell.parent('tr');
	}
	
	
	jqSourceCell.attr('colspan', '1').attr('rowspan', '1').css({'width': 'auto', 'min-width': 0});
	
	if(jqDestCell) {
		jqDestCell.attr('colspan', itemColspan).attr('rowspan', itemRowspan).removeClass('empty-cell');
		if(opts.copyStyle) {
			jqDestCell.attr('style', sourceCellStyle);
		}
	}
	
	for(i = sourceCellColumn; i < sourceCellColumn + itemColspan; i ++) {
		for(j = sourceCellRow; j < sourceCellRow + itemRowspan; j ++) {
			jqNewCell = this.createCell(i, j);
			if(jqNewCell) {
//				jqNewCell.css('background-color', 'yellow');
				cells.push(jqNewCell);
			}
		}
	}
	
	if(jqDestCell) {
		for(i = destCellColumn; i < destCellColumn + itemColspan; i ++) {
			for(j = destCellRow; j < destCellRow + itemRowspan; j ++) {
				if(!(i == destCellColumn && j == destCellRow)) {
					this.removeCell(i, j);
				}
			}
		}
	}
	
	$j(document).trigger('dwr_grid_normalized', [$j(cells)]);
}

Grid.prototype.getAvailableCellsAreas = function(fieldCn, jqCell, itemColspan, itemRowspan, xPos, yPos) {
	var _this = this;
	var initCol = Number(jqCell.attr('column')),
		initRow = Number(jqCell.attr('row')),
		cellPosition = jqCell.position(), iterCellPosition = null,
		nearestWndIndex = 0, smallestDistance = null;
	
	// pocz�tek okienka
	var wnd = {
		col: initCol - itemColspan + 1,
		row: initRow - itemRowspan + 1,
		width:	itemColspan,
		height: itemRowspan
	}
	
	var isWindowEmpty = function(col, row, width, height) {
		var x, y, jqCell, selfCell,
			cellRowspan = 1, cellColspan = 1;
		for(x = col; x < col + width; x ++) {
			for(y = row; y < row + height; y ++) {
				jqCell = _this.getCell(x, y);
				selfCell = jqCell.attr('fieldcn') ? jqCell.attr('fieldcn') == fieldCn : false;
				cellRowspan = Number(jqCell.attr('rowspan'));
				cellColspan = Number(jqCell.attr('colspan'));
				
				if(cellColspan > 1) {
					x += (cellColspan-1);
				}
				if(cellRowspan > 1) {
					y += (cellRowspan-1);
				}
				
				if(!selfCell && (!jqCell.length || jqCell.children().length)) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	// przesuwamy okienko znajduj�c kom�rki
	var dx = 0, dy = 0, foundWnds = [], iterWnd = null,
		jqCells = [], 
		col, row, i;
	for(dx = 1-itemColspan; dx < itemColspan; dx ++) {
		for(dy = 1-itemRowspan; dy < itemRowspan; dy ++) {
			if(isWindowEmpty(wnd.col + dx, wnd.row + dy, wnd.width, wnd.height)) {
				foundWnds.push({
					col: wnd.col += dx,
					row: wnd.row += dy,
					width: wnd.width,
					height: wnd.height
				});
			}
		}		
	}
	
	if(!foundWnds.length)
		return null;
	
	//console.info('distances');
	for(i = 0; i < foundWnds.length; i ++) {
		iterWnd = foundWnds[i];
		jqCells[i] = [];
		for(col = iterWnd.col; col < iterWnd.col + iterWnd.width; col ++) {
			for(row = iterWnd.row; row < iterWnd.row + iterWnd.height; row ++) {
				jqCells[i].push(this.getCell(col, row));
			}
		}
		
		iterCellPosition = jqCells[i][0].position();
		distance = Math.sqrt(
				(xPos - iterCellPosition.left) * (xPos - iterCellPosition.left) + 
				(yPos - iterCellPosition.top) * (yPos - iterCellPosition.top)
		);
		//console.info(distance);
		if(smallestDistance == null) {
			smallestDistance = distance;
			nearestWndIndex = 0;
		} else if(distance < smallestDistance) {
			smallestDistance = distance;
			nearestWndIndex = i;
		}
	}
	//console.warn('smallest', smallestDistance);
	
	
	return jqCells[nearestWndIndex];
}

Grid.prototype.getTable = function() {
	return this.jqTable;
}

Grid.prototype.getCells = function() {
	return this.jqTable.find('td');
}

Grid.prototype.getCell = function(col, row) {
	return this.jqTable.find('[column=' + col + '][row=' + row + ']');
}

Grid.prototype.getCellsByColumn = function(col) {
	return this.jqTable.find('[column=' + col + ']');
}

Grid.prototype.getCellsByRow = function(row) {
	return this.jqTable.find('tr[index=' + row + ']').children('td');
}

Grid.prototype.getEmptyCells = function() {
	return this.jqTable.find('td[column][row]:empty');
}

Grid.prototype.isCellAvailable = function(jqCell, itemColspan, itemRowspan) {
	if(jqCell.children().length) {
		return false;
	}
	
	var colspanIter = itemColspan,
		jqCellIter = jqCell;
	while(colspanIter > 1) {
		jqCellIter = jqCellIter.next();
		if(!jqCellIter.length || jqCellIter.children().length) {
			return false;
		}
		
		colspanIter --;
	}
	
	return true;
}

Grid.prototype.getCellAssignedCounter = function(jqCell) {
    var counter = jqCell.attr('assigned');
    if(counter == null) {
        counter = 0;
    } else {
        counter = Number(counter);
    }

    return counter;
}

Grid.prototype.setCellAssignedCounter = function(jqCell, counter) {
    jqCell.attr('assigned', counter);
}

Grid.prototype.getCachedRow = function(row) {
	if(this.jqRowsCache[row] == null)
		this.jqRowsCache[row] = this.jqTable.find('tr[index=' + row + ']');

	return this.jqRowsCache[row];
}

Grid.prototype.createCell = function(col, row) {
	var uCol = Number(col), uRow = Number(row);

	// kom�rka ju� jest stworzona
	if(this.opt > 0) {
		if(this.tab[uRow] != null && this.tab[uRow][uCol] === true)
			return null;
	} else {
		if(this.jqTable.find('[column=' + uCol + '][row=' + uRow + ']').length)
			return null;
	}

	var jqCell = $j('<td/>')
		.attr('id', this.uid + 'td-' + String.fromCharCode(96+uCol) + uRow)
		.addClass('td-' + (uCol-1) + '-' + (uRow-1))
		.attr('column', uCol).attr('row', uRow)
		.attr('colspan', '1').attr('rowspan', '1');
	
	if(this.opt > 0) {
		/*var jqRow = this.getCachedRow(uRow);
		var jqPrevRow = this.getCachedRow(1);*/
	} else {
		var jqRow = this.jqTable.find('tr[index=' + uRow + ']');
		var jqPrevRow = this.jqTable.find('tr:first');
	}

	if(this.opt > 0) {
		var jqRow = null;
		if(this.tab[uRow] == null) {
			// tworzymy wiersz
			this.tab[uRow] = [];
			
			// szukamy poprzedniego wiersza
			var prevRowNum = null, iterRowNum = uRow;
			while(iterRowNum > 1) {
				if(this.tab[iterRowNum-1] != null) {
					prevRowNum = iterRowNum-1;
					break;
				}
				iterRowNum --;
			}
			jqRow = $j('<tr/>').attr('id', this.uid + 'tr-' + uRow).attr('index', uRow);

			// je�li nie ma wiersza przed nami, to jeste�my pierwszym i jedynym jak do tej pory wierszem
			if(prevRowNum == null) {
				this.jqTable.append(jqRow);
			} else {
				var jqPrevRow = this.jqCachedRows[prevRowNum];

				if(Number(jqPrevRow.attr('index')) < Number(jqRow.attr('index'))) {
					// wstawiamy za
					jqPrevRow.after(jqRow);
				} else {
					// wstawiamy przed
					jqPrevRow.before(jqRow);
				}
			}
			this.jqCachedRows[uRow] = jqRow;
		} else {
			// wiersz istnieje
			jqRow = this.jqCachedRows[uRow];
		}
	} else {
		if(!jqRow.length) {
			// tworzymy wiersz
			jqRow = $j('<tr/>').attr('id', 'tr-' + uRow).attr('index', uRow);

			this.jqTable.find('tr').each(function() {
				if($j(this).attr('index') < uRow) {
					jqPrevRow = $j(this);
				}
			});
			if(jqPrevRow.length) {
				if(Number(jqPrevRow.attr('index')) < Number(jqRow.attr('index'))) {
					jqPrevRow.after(jqRow);
				} else {
					jqPrevRow.before(jqRow);
				}
			} else {
				this.jqTable.append(jqRow);
			}
		}		
	}

	var jqPrevCell = jqRow.children('td:first');
	jqRow.children('td').each(function() {
		if($j(this).attr('column') < uCol) {
			jqPrevCell = $j(this);
		}
	});
	if(jqPrevCell.length) {
		if(Number(jqPrevCell.attr('column')) < Number(jqCell.attr('column'))) {
			jqPrevCell.after(jqCell);
		} else {
			jqPrevCell.before(jqCell);
		}
	} else {
		jqRow.append(jqCell);
	}

	// dodajemy stworzon� kom�rk� do cache
	if(this.opt > 0) {
		this.tab[uRow][uCol] = true;
	}
	
	return jqCell;
}


Grid.prototype.getPrevRowIndex = function(rowIndex) {
	var prevRowNum = null;
	while(rowIndex >= 1) {
		if(this.tab[rowIndex] != null) {
			prevRowNum = rowIndex;
			break;
		}
		rowIndex --;
	}

	return prevRowNum;
}

Grid.prototype.removeCell = function(col, row) {
	var jqCell = this.jqTable.find('[column=' + col + '][row=' + row + ']');
	if(jqCell.length) {
		jqCell.remove();
		return 1;
	}
	
	return 0;
}

Grid.prototype.calculateSnap = function() {
	this.snap = {
		hor: [],
		vert: []
	}
	
	var snap = this.snap;
	var checkPosition = function(x, y) {
		var i, len, ret = {x: false, y: false};
		
		for(i = 0, len = snap.vert.length; i < len; i ++) {
			if(snap.vert[i].xPos == x) {
				ret.x = true;
				break;
			}
		}
		for(i = 0, len = snap.hor.length; i < len; i ++) {
			if(snap.hor[i].yPos == y) {
				ret.y = true;
				break;
			}
		}
		
		return ret;
	}
	
	this.jqTable.find('td').each(function() {
		var jqThis = $j(this),
			pos = jqThis.position(),
			contains = checkPosition(pos.left, pos.top);
		
		if(!contains.y) {
			snap.hor.push({
				yPos: pos.top,
				row: Number(jqThis.attr('row'))
			});
		}
		if(!contains.x) {
			snap.vert.push({
				xPos: pos.left,
				col: Number(jqThis.attr('column'))
			});
		}
		
		//jqThis.attr('title', pos.left + ' x ' + pos.top);
	});
	
}

Grid.prototype.getNearestSnap = function(x, y) {
	if(this.snap == null)
		throw 'Grid snap is not initialized';
	
	var i, len, iterSnap,
		matchingSnap = {hor: null, vert: null};
	
	if(x != null) {
		for(i = this.snap.vert.length-1; i > 0; i --) {
			iterSnap = this.snap.vert[i];
			if(x <= iterSnap.xPos - DsbEditor.SNAP_GRID_TOLERANCE) {
				matchingSnap.vert = iterSnap;
			}
		}
	}
	
	if(y != null) {
		for(i = this.snap.hor.length-1; i > 0; i --) {
			iterSnap = this.snap.hor[i];
			if(y <= iterSnap.yPos - DsbEditor.SNAP_GRID_TOLERANCE) {
				matchingSnap.hor = iterSnap;
			}
		}
	}
	
	return matchingSnap;
}

/**
 * Rozszerza tabel� do podanych rozmiar�w przez dodanie pustych kom�rek
 */
Grid.prototype.expand = function(cols, rows) {
	var jqTable = this.jqContainer.children('table:first'),
		cssRule = DsbUtils.Stylesheet.findCssRule('editor.css', '#grid td'),
		cellWidth = null, cellHeight = null;

	console.warn('expand', cols, rows);
//	console.warn('expand');
	/* TODO */
	this.jqTable = jqTable;
	
	try {
		cellWidth = parseInt(cssRule.style.width, 10);
		cellHeight = parseInt(cssRule.style.height, 10);
	} catch(e) {
		cellWidth = cellHeight = 20;
	} finally {
		if(isNaN(cellWidth)) cellWidth = 20;
		if(isNaN(cellHeight)) cellHeight = 20;
	}
	
	var lastCol = 1, lastRow = 1,
		i = 0, j = 0, createdCells = [], jqCell = null;;
	
	// nie wi�cej ni� 5, bo zabijemy przegl�dark�
//	cols = Math.min(cols, 5);
//	rows = Math.min(rows, 5);
	
	// szukamy ostatniej kolumny
	while(this.getCellsByColumn(lastCol).length) {
		lastCol ++;
	}
	// szukamy ostatniego wiersza
	while(this.getCellsByRow(lastRow).length) {
		lastRow ++;
	}
	
	// tworzymy kolumny
	for(i = lastCol; i < lastCol + cols; i ++) {
		for(j = 1; j < lastRow + rows; j ++) {
			jqCell = this.createCell(i, j);
			if(jqCell != null)
				createdCells.push(jqCell);
		}
	}
	
	// tworzymy wiersze
	for(j = lastRow; j < lastRow + rows; j ++) {
		for(i = 1; i < lastCol + cols; i ++) {
			jqCell = this.createCell(i, j);
			if(jqCell != null)
				createdCells.push(jqCell);
		}
	}
//	console.info(createdCells.length);
	var jqCells = $j(createdCells).map(function() {
		return this.toArray();
	});
	
	this.updateSizeInfo(lastCol + cols - 1, lastRow + rows - 1);

	$j(document).trigger('dwr_grid_normalized', [jqCells]);
}

Grid.parseCoords = function(coords) {
	var i, splitted;
	var ret = {};
	splitted = coords.split(':');
	
	function parse(item) {
		var cell = {};
		cell.col = item.replace(/\d{1,2}/g, '');
		cell.row = item.replace(/[^\d]/g, '');
		cell.x = cell.col.toLowerCase().charCodeAt(0) - 97;
		cell.y = parseInt(cell.row, 10) - 1;
		cell.id = 'td-' + cell.col + cell.row;
		cell.className = 'td-' + cell.x + '-' + cell.y;
		return cell;
	}
	
	if(splitted[0] != null) {
		ret.start = parse(splitted[0]);
	}	
	if(splitted[1] != null) {
		ret.end = parse(splitted[1]);
		ret.end.colspan = ret.end.x - ret.start.x + 1;
		ret.end.rowspan = ret.end.y - ret.start.y + 1;
	}

	return ret;
}

Grid.transformCoords = function(operation, coords, cx, cy) {
	if(cx != null && isNaN(cx) || cy != null && isNaN(cy))
		throw new TypeError("Cx or cy is not a number");
	var splitted = coords.split(':');
	var start = splitted[0],
		end = splitted[1] || null;
	
	start = {
		col: start.replace(/\d{1,2}/g, ''),
		row: Number(start.replace(/[^\d]/g, ''))
	}
	if(end) {
		end = { 
			col: end.replace(/\d{1,2}/g, ''),
			row: Number(end.replace(/[^\d]/g, ''))
		}
	}
	
	switch(operation.toLowerCase()) {
		case 'translate':
			start.col = String.fromCharCode(start.col.toLowerCase().charCodeAt(0) + cx);
			start.row += cy;
			if(end) {
				end.col = String.fromCharCode(end.col.toLowerCase().charCodeAt(0) + cx);
				end.row += cy;
			}
		break;
		case 'translate-end':
			if(end == null) {
				end = {
					col: start.col,
					row: start.row
				};
			}
			if(cx != null) {
				end.col = String.fromCharCode(96 + cx);
			}
			if(cy != null) {
				end.row = cy;
			}
		break;
		default:
			throw 'Unsupported transform operation';
		
	}	
	return start.col + start.row + (end ? ':' + end.col + end.row : '');
}

Grid.extractCellPosition = function(jqCell) {
	var strPos = jqCell.attr('class');
	var pos = strPos.match(/td-\d{1,2}-\d{1,2}/)[0].split('-');
	
	return {
		col: Number(pos[1]),
		row: Number(pos[2])
	}
}

Grid.extractCellCoords = function(jqCell) {
	return jqCell[0].id.replace('td-', '');
}

Grid.calculateCellDistance = function(jqCellOne, jqCellTwo) {
	var pos1 = Grid.extractCellPosition(jqCellOne),
		pos2 = Grid.extractCellPosition(jqCellTwo);
	
	return {
		x: pos2.col - pos1.col,
		y: pos2.row - pos1.row
	}
}