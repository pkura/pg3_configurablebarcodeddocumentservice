/**
 * @projectDescription DWR Dockind Fields
 * @author Kamil Omela�czuk kamil.omelanczuk@docusafe.pl
 * @version 1.0
 * @namespace pl.compan.dwr
 */
/**
 * Globalna zmienna okre�laj�ca offset przy klikni�ciu na nast�pna strona
 * @property
 * @type {Number}
 */
var __LOAD_NEXT_RESULTS_OFFSET = 0;
/**
 * Zarz�dza ca�ym s�ownikiem w okienku.
 * @classDescription Tworzy now� instancj� DictionaryPopup.
 * @author Kamil Omela�czuk
 * @version 1.0
 * @constructor
 * @param {String} dictionaryName Nazwa s�ownika
 */
function DictionaryPopup(dictionaryName) {
	/**
	 * Nazwa s�ownika
	 * @property
	 * @type {String}
	 */
	this.dictionaryName = dictionaryName;
	/**
	 * Funkcja wywo�ywana po wybraniu konkretnego wpisu w postaci
	 * function(data, [params]), gdzie:
	 * <ul>
	 * <li><code>data</code> to dane odebrane z DWR
	 * <li><code>[params]</code> OPCJONALNA mapa z opcjami:
	 * multipleFieldsNumber - numer pola wielowarto�ciowego
	 * </ul>
	 * @property
	 * @type {Function}
	 */
	this.resultFunction = null;
	/**
	 * Liczba widocznych p�l
	 * @property
	 * @type {Number}
	 */
	this.visibleFieldsCount = 0;
	/**
	 * Element, na kt�rym wy�wietlane s� informacje (np.: o zapisaniu danego wpisu)
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqInfobar = null;
	/**
	 * Element, na kt�rym rysowane s� wszystkie elementy s�ownika
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqPopup = null;
	/**
	 * Element, zawieraj�cy wyniki (konkretnie tabel�, nag��wek i przycisk nast�pna strona)
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqResults = null;
	this.jqResultsTable = null;
	this.jqResultsTableHeader = null;
	this.jqResultsTableRowTemplate = null;
	/**
	 * Element zawieraj�cy pola s�ownika
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqFieldset = null;
	this.jFields = null;
	this.fields = null;
	this.searchFields = null;
	this.buttons = {};
	this.searchMode = true;
	
	this.jqSelectedRow = null;
	this.popupWidth = 0;
	this.resultsTableWidth = 0;
	/**
	 * Pierwsze pole po kt�rym mo�na wyszukiwa� w s�owniku. Przydatne, 
	 * gdy chcemy odpali� "puste" wyszukiwanie.
	 * @property
	 * @type {Object} Obiekt jQuery
	 */
	this.jqFirstSearchableField = null;
	/**
	 * Czy mo�na doda� nowy wpis (pola niepuste).
	 * @property
	 * @type {Boolean}
	 */
	this.valid = true;
	
	this.cachedValues = null;
	/**
	 * Tylko podczas pierwszego pokazania okienka ustalamy d�ugo�ci p�l.
	 * @property
	 * @type {Boolean}
	 */
	this.fieldsWidthAlreadyFixed = false;
	
	/**
	 * Czy popup jest wywo�ywany dla p�l wielowarto�ciowych
	 * @property
	 * @type {Boolean}
	 */
	this.multiple = false;
	
	/**
	 * Numer pola wielowarto�ciowego. <br/>
	 * Number powinien by� przypisywany w metodzie <code>show</code>
	 * i dalej przekazywany w <code>resultFunction</code>
	 * @property
	 * @type {Number}
	 */
	this.multipleFieldsNumber = null;
	
	/**
	 * Mapa zawieraj�ca widoczne przyciski:
	 * <ul>
	 * <li>doAdd
	 * <li>doEdit
	 * <li>doRemove
	 * <li>doSelect (nie mo�e by� jednocze�nie doSelectSmall)
	 * <li>doSelectSmall (nie mo�e by� jednocze�nie doSelect)
	 * </ul>
	 * Je�li r�wna <code>null</code>, to wszystkie przyciski s� widoczne
	 * @property
	 * @type {Object}
	 */
	this.visibleButtons = null;
}

DictionaryPopup.prototype.create = function(resultFunction, jFields, params) {
	//trace(jFields);
	if(!jFields)
		throw new VariableNotInitialisedException("Fields for Dictionary popup not defined.");
	if(!resultFunction)
		throw new VariableNotInitialisedException("Result function for Dictionary popup not defined.");
	
	this.jFields = jFields;
	this.resultFunction = resultFunction;
	this.fields = {};
	
	if($j('#' + this.dictionaryName + '_DIC_POPUP').length > 0) {
		$j('#' + this.dictionaryName + '_DIC_POPUP').remove();
	}
	
	var str = '<div id="' + this.dictionaryName + '_DIC_POPUP" class="dictionaryPopup"><div class="col">';
	if(params.fieldsManager != null && params.fieldsManager.dictionaryField.jField.dictionary.pure === true) {
		str += '<input type="text" style="width: 1px; border: none; position: absolute; left: -10px;" />';
	}
	str += '</div><div class="results"></div></div>';
	$j('body').append(str);
	
	this.jqPopup = $j('#' + this.dictionaryName + '_DIC_POPUP');
	this.jqResults = this.jqPopup.children('div.results');
	
	if(params) {
		if(params.fieldsManager != null) {
			try {
				this.pure = params.fieldsManager.dictionaryField.jField.dictionary.pure === true;
				this.fieldsManager = params.fieldsManager;
				this.dwrParams = this.fieldsManager.dwrParams;
				this.jDictionaryField = this.fieldsManager.jDictionaryField;
				this.dictionaryField = this.fieldsManager.dictionaryField;
				if(this.pure === true) {
					this.jqPopup.addClass('dictionaryPopupPure');
				}
			} catch(e) {
				// console.error(e);
			}
		}			
		if(params.title)
			this.jqPopup.attr('title', params.title);	
		if(params.multiple === true)
			this.multiple = true;
		if(params.visibleButtons) {
			if(Object.prototype.toString.call(params.visibleButtons) == '[object Object]')
				this.visibleButtons = params.visibleButtons;
			else if(Object.prototype.toString.call(params.visibleButtons) == '[object Array]') {
				this.visibleButtons = {};
				(function() { // this = dictionaryPopup
					var i, len;
					for(i = 0, len = params.visibleButtons.length; i < len; i ++) {
						this.visibleButtons[params.visibleButtons[i]] = true;
					}
					if(this.visibleButtons.doSelect === true && this.visibleButtons.doSelectSmall === true)
						this.visibleButtons.doSelect = false;
				}).call(this);
			} else
				throw new ObjectUnsupportedTypeException('visibleButtons for dictionaryPopup is not Array or readable Object');
		} else {
			this.visibleButtons = DictionaryPopup.DefaultVisibleButtons;
		}		
	}
	var field, clonedFields;
	clonedFields = (function(fields) {
		var cloned = [];
		var i, len;
		for(i = 0, len = fields.length; i < len; i ++) {
			cloned.push(Field.prototype.clonejField.call({jField: fields[i]}));
		}
		
		// je�li s�ownik dla p�l wielowarto�ciowych, to zmieniamy NAZWA_POLA_3 na NAZWA_POLA
		if(params && params.multiple === true) {
			for(i = 0; i < len; i ++) {
				cloned[i].cn = cloned[i].cn.replace(/_\d+$/, '');
				cloned[i].pure = false;
			}
		}
		
		return cloned;
	})(jFields);
	for(var i = 0; i < clonedFields.length; i ++) {
		clonedFields[i].fieldset = this.dictionaryName + '_POPUP_FIELDSET';
		field = new Field(clonedFields[i], 0);
		field.jqColumn = this.jqPopup.children('div.col');
		field.htmlId = field.jField.cn + '_POPUP_' + this.dictionaryName;
		field.create();
		if(params.fieldsManager != null) {
			field.parentFieldsManager = params.fieldsManager;
		}
		// Nie potrzebujemy pola z informacj�
		//field.jqMsg.remove();
		field.jField.kind= 'NORMAL';
		if(this.pure === true) {
			if(field.jField.cn == this.dictionaryName + '_ID') {
				this.registerFieldListener(field);	
			} else {
				this.registerFieldListener(field, true);
			}
		} else {
		this.registerFieldListener(field);
		}
		
		this.fields[field.jField.cn] = field;
		if(!jFields[i].hidden && jFields[i].type != 'HIDDEN')
			this.visibleFieldsCount ++;
	}
	this.popupWidth = Math.min(Math.max(this.visibleFieldsCount * 120, DictionaryPopup.MIN_WIDTH), $j('body').width() - 100);
	if(this.popupWidth)
		this.resultsTableWidth = this.popupWidth - 25 - 12;
	//if(ie7) this.resultsTableWidth -= 18;
	
	this.jqFieldset = $j('#' + this.dictionaryName + '_POPUP_FIELDSET');
	var fieldsetWidth = Math.ceil(this.visibleFieldsCount / 2) * 180;
	this.jqFirstSearchableField = this.jqFieldset.children('div').children('input:visible, select:visible, textarea:visible').filter(':first');
	//this.jqFieldset.width(fieldsetWidth);
	
	this.createInfobar();
	this.createToolbar();
	this.createResultsTable();
	this.createDialog();
}

DictionaryPopup.prototype.updateFields = function(modifiedjFields) {
	var i = 0, modifiedjField = null, j = 0, jField = null;
	
	for(i = 0; i < modifiedjFields.length; i ++) {
		modifiedjField = modifiedjFields[i];
		for(j = 0; j < this.jFields.length; j ++) {
			jField = this.jFields[j];
			
			if(jField.cn == modifiedjField.cn) {
//				console.info('FOUND', jField.cn);
				this.jFields[j] = modifiedjField;
				this.fields[jField.cn].jField = modifiedjField;
				this.fields[jField.cn].refresh();
				
				break;
			}
		}
	}
}

DictionaryPopup.prototype.createInfobar = function() {
	var str = '<div class="infobar"></div>';
		
	this.jqPopup.children('div.col').before(str); 
	this.jqInfobar = this.jqPopup.children('div.infobar');
}

DictionaryPopup.prototype.showMessage = function(strMessage, bValid) {
	if(bValid)
		this.jqInfobar.removeClass('messageInvalid').addClass('messageValid').html(strMessage);
	else
		this.jqInfobar.removeClass('messageValid').addClass('messageInvalid').html(strMessage);
}

DictionaryPopup.prototype.createToolbar = function() {
	var _this = this;
	var str = '<div class="toolbar">';
	if(this.visibleButtons.doSelectSmall) {
		str += '<button id="' + this.dictionaryName + 'select-button" class="popupButton popupButtonSelect" disabled="disabled"><div class="action">' 
		+ DWR.MSG.DIC_POPUP_SELECT + '</div><div class="desc"></div></button>';
	}
	if(this.visibleButtons.doAdd) {
		str += '<button id="' + this.dictionaryName 
			+ 'create-item-button" class="popupButton popupButtonCreate" ><div class="action">' 
			+ DWR.MSG.DIC_POPUP_ADD + '</div><div class="desc"></div></button>';
	}
	if(this.visibleButtons.doEdit) {
		str += '<button id="' + this.dictionaryName 
			+ 'save-item-button" class="popupButton popupButtonSave" disabled="disabled" style="display: none"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_SAVE + '</div><div class="desc"></div></button>';
		str += '<button id="' + this.dictionaryName 
			+ 'cancel-item-button" class="popupButton popupButtonUndo" disabled="disabled" style="display: none"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_CANCEL + '</div><div class="desc"></div></button>';
		str += '<button id="' + this.dictionaryName 
			+ 'edit-item-button" class="popupButton popupButtonEdit" disabled="disabled"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_EDIT + '</div><div class="desc"></div></button>';
	}
	if(this.visibleButtons.doRemove) {
		str += '<button id="' + this.dictionaryName 
			+ 'delete-item-button" class="popupButton popupButtonDelete" disabled="disabled"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_DELETE + '</div><div class="desc"></div></button>';
	}
	if(this.visibleButtons.doClear) {
		str += '<button id="' + this.dictionaryName 
			+ 'clear-item-button" class="popupButton popupButtonCancel"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_CLEAR + '</div><div class="desc"></div></button>';
	}
	str += '</div>';
	
	this.jqResults.before(str);
	if(this.visibleButtons.doSelect) {
		str = '<button id="' + this.dictionaryName + 'select-button" class="bigButton bigButtonSelect" disabled="disabled"><div class="action">' 
			+ DWR.MSG.DIC_POPUP_SELECT + '</div><div class="desc">' + DWR.MSG.DIC_POPUP_ADD_TO_FORM + '</div></button>';	
		this.jqInfobar.after(str);
	}
	
	
	this.buttons.jqSelectButton = $j('#' + this.dictionaryName + 'select-button');
	this.buttons.jqSaveButton = $j('#' + this.dictionaryName + 'save-item-button');
	this.buttons.jqEditButton = $j('#' + this.dictionaryName + 'edit-item-button');
	this.buttons.jqCreateButton = $j('#' + this.dictionaryName + 'create-item-button');
	this.buttons.jqCancelButton = $j('#' + this.dictionaryName + 'cancel-item-button');
	this.buttons.jqDeleteButton = $j('#' + this.dictionaryName + 'delete-item-button');
	this.buttons.jqClearButton = $j('#' + this.dictionaryName + 'clear-item-button');
	
	this.buttons.jqSelectButton.click(function() {
		var data = _this.jqResultsTable.children('tbody').children('tr.selectedItem').data('item'),
			params = {calledFromPopup: true};
		_this.jqPopup.dialog('close');
		
		if(_this.multipleFieldsNumber != null)
			params.multipleFieldsNumber = _this.multipleFieldsNumber;
		
		_this.resultFunction(data, params);
	});
	
	this.buttons.jqSaveButton.click(function() {
		var cn, fieldCn;
		var vals = {};
		if(!FieldsManager.prototype.validate.call(_this)) {
			_this.showMessage(DictionaryPopup.Messages.ITEM_ADDED_NOT_VALID, false);
			return;
		}
		if(_this.multiple === true) {
			for(cn in _this.fields) {
				if(_this.fields[cn].getSimpleValue() || _this.fields[cn].jField.type == 'BOOLEAN') {
					fieldCn = cn;
					if(fieldCn == _this.dictionaryName + '_ID') {
						fieldCn = 'id';
					} 
					vals[fieldCn] = _this.fields[cn].getValue({allOptions: false});
				}
			}
		} else {
			for(cn in _this.fields) {
				if(_this.fields[cn].getSimpleValue() || _this.fields[cn].jField.type == 'BOOLEAN')
					vals[cn] = _this.fields[cn].getValue({allOptions: false});
			}
		}
		
		_this.searchMode = true;	
		Dictionary.dwrSave(_this.dictionaryName, vals, function(data) {
			//_this.jqResultsTable.removeClass('inactiveTable', 1000);
			if(data > 0) {
				_this.buttons.jqSaveButton.hide();
				_this.buttons.jqCancelButton.hide();
				_this.buttons.jqSelectButton.attr('disabled', false);
				_this.buttons.jqEditButton.attr('disabled', false).show();
				_this.buttons.jqDeleteButton.attr('disabled', 'disabled').show();
				
				var rowData = {};
				for(var cn in _this.fields) {
					var field = _this.fields[cn];
					_this.jqSelectedRow.children('.' + cn).html(field.getSimpleValue({selectedOptionName: true}));
					rowData[cn] = field.getSimpleValue();
				}
				_this.jqSelectedRow.data('item', rowData);
				_this.jqSelectedRow.delay(100).addClass('savedItem', 200, function() {
				   _this.jqSelectedRow.removeClass('savedItem', 200);
				   _this.showMessage(DictionaryPopup.Messages.ITEM_SAVED, true);
				});
				if(_this.pure === true) {
					_this.buttons.jqSelectButton.trigger('click');
				}
			} else {
				_this.showMessage(DictionaryPopup.Messages.ITEM_SAVED_FAILED, false);
			}
				
		});
	});
	
	this.buttons.jqEditButton.click(function() {
		//_this.jqResultsTable.addClass('inactiveTable');
		_this.searchMode = false;
		_this.selectItem($j(this).data('item'));
		_this.buttons.jqSelectButton.attr('disabled', 'disabled');
		_this.buttons.jqDeleteButton.hide();
		$j(this).hide();
		_this.buttons.jqSaveButton.attr('disabled', 'disabled').show();
		_this.buttons.jqCancelButton.attr('disabled', false).show();
	});
	
	this.buttons.jqCreateButton.click(function() {
		var vals = new Object();
		if(!FieldsManager.prototype.validate.call(_this)) {
			_this.showMessage(DictionaryPopup.Messages.ITEM_ADDED_NOT_VALID, false);
			return;
		}
			
		for(var cn in _this.fields) {
			if(cn == 'id')
				continue;
			if(_this.fields[cn].getSimpleValue() || _this.fields[cn].jField.type == 'BOOLEAN')
				vals[cn] = _this.fields[cn].getValue({allOptions: false});
		}
		
		_this.searchMode = true;		
		// console.info('dwrAdd', vals);
		Dictionary.dwrAdd(_this.dictionaryName, vals, function(data) {
			if (data > 0) {
				if(_this.pure === true) {
					for(var cn in _this.fields) {
						if(!_this.fields[cn].hidden && _this.fields[cn].jField.type != 'HIDDEN') {
							var itemId = 'item_' + data;

							for(var key in _this.fields) {
								_this.fields[key].clearValue();
							}

							_this.fields[_this.dictionaryName + '_ID'].setValue(data);
							break;
						}	
					}
					_this.fields[_this.dictionaryName + '_ID'].jqField.trigger('search', [function() {
							$j('#' + itemId).trigger('dblclick.selectItem');
							_this.createNewCompleted = true;
					}]);									
				} else {
					for(var cn in _this.fields) {
						if(!_this.fields[cn].hidden && _this.fields[cn].type != 'HIDDEN') {
							var itemId = 'item_' + data;
							var idFieldCn = _this.dictionaryName + '_ID';
                            if(_this.fields[idFieldCn] == null) {
                                idFieldCn = 'id';
                            }

							_this.fields[idFieldCn].setValue(data);
							_this.fields[cn].jqField.trigger('search', [function() {
							var pure = false
							try {
								pure = _this.pure;
								// console.warn('is pure?', pure);
							} catch(e) {

							}
							if(pure) {
								$j('#' + itemId).trigger('dblclick.selectItem');
								_this.createNewCompleted = true;
							} else {
								$j('#' + itemId).trigger('click.selectItem');
							}
							}]);
							break;
						}
					}
				}
				_this.showMessage(DictionaryPopup.Messages.ITEM_ADDED, true);
			/*} else if (data == -10 ){
				_this.showMessage(DictionaryPopup.Messages.ITEM_ADDED_FAILED_EXIST, false);*/
			}else {	
				_this.showMessage(DictionaryPopup.Messages.ITEM_ADDED_FAILED, false);
				}
		});
	});
	
	this.buttons.jqCancelButton.click(function() {
		//_this.jqResultsTable.removeClass('inactiveTable');
		_this.searchMode = true;
		$j(this).hide();
		_this.buttons.jqSaveButton.hide();
		_this.buttons.jqEditButton.attr('disabled', false).show();
		_this.buttons.jqDeleteButton.attr('disabled', false).show();
		_this.buttons.jqSelectButton.attr('disabled', false);
		_this.restoreSearchValues();
	});
	
	this.buttons.jqDeleteButton.click(function() {
		var id = $j(this).data('item').id;
		Dictionary.dwrRemove(_this.dictionaryName, id, function(data) {
			if (data > 0) {
				_this.buttons.jqEditButton.attr('disabled', 'disabled');
				_this.buttons.jqSelectButton.attr('disabled', 'disabled');
				_this.buttons.jqDeleteButton.attr('disabled', 'disabled');
				_this.jqSelectedRow.fadeOut('fast');
				_this.showMessage(DictionaryPopup.Messages.ITEM_DELETED, true);
			} else {
				_this.showMessage(DictionaryPopup.Messages.ITEM_DELETED_FAILED, false);
			}
		});
	});
	
	this.buttons.jqClearButton.click(function() {
		for(var cn in _this.fields) {
			if(_this.pure === true) {
				_this.fields[cn].setValue([""], {allOptions: false});
			} else {
			_this.fields[cn].clearValue();
			}
			_this.fields[cn].jqField.removeClass('invalid');
		}
		_this.buttons.jqSelectButton.attr('disabled', 'disabled');
		_this.buttons.jqEditButton.attr('disabled', 'disabled');
		_this.buttons.jqDeleteButton.attr('disabled', 'disabled');
		_this.jqFirstSearchableField.trigger('search');
	});
}

DictionaryPopup.prototype.createResultsTable = function() {
	var _this = this;
	
	/* nag��wek */
	var colsCount = 0;
	var str = '<div class="stHeader">';
	for(var key in this.fields) {
		if (!this.fields[key].jField.hidden) {
			str += '<div>' + this.fields[key].jField.label + '</div>';
			colsCount++;
		}
	}
	str += '<div class="clearBoth" style="float: none; padding: 0"></div></div>';
	this.jqResults.before(str);
	this.jqResultsTableHeader = this.jqResults.prev('div.stHeader');
	
	str = '<table class="ac_results" cellspacing="0" width="' + this.resultsTableWidth + '">';
	/* szablon jednego wiersza */
	str += '<tbody><tr class="fieldsOrder">';
	for(var key in this.fields) {
		if (!this.fields[key].jField.hidden) {
			str += '<td class="' + this.fields[key].jField.cn + '">' + this.fields[key].jField.label + '</td>';
		}
	}
	str += '</tr></tbody></table>';
	
	this.jqResults.append(str);
	str = '<div class="moreResults">' + DWR.MSG.DIC_POPUP_MORE_RESULTS + '</div>';
	this.jqResults.after(str);
	this.jqResultsTable = this.jqResults.children('table');
	this.jqResultsTableRowTemplate = this.jqResultsTable.children('tbody').children('tr.fieldsOrder');
	this.buttons.jqNextPageButton = this.jqResults.next('div.moreResults');
	
	this.buttons.jqNextPageButton.bind('click.showMore', function() {
		__LOAD_NEXT_RESULTS_OFFSET += DWR.PAGE_SIZE;
		Dictionary.dwrSearch(_this.dictionaryName, _this.searchFields, DWR.PAGE_SIZE + 1, __LOAD_NEXT_RESULTS_OFFSET, function(data) {
			_this.fillResults(data);
		});
	});
}

DictionaryPopup.prototype.fillResults = function(data, onComplete) {
	var scrollableResults = false;
	var _this = this;
	
	if (data && data.length > 0) {
		for (var i = 0; i < data.length; i++) {
			if (i >= DWR.PAGE_SIZE) {
				scrollableResults = true;
				break;
			}
			
			var dataRow = data[i];
			var htmlId = 'item_' + data[i].id;
			var jqRow = this.jqResultsTableRowTemplate.clone();
			jqRow.children('td').html('');
			for (var item in dataRow) {
				var itemData = dataRow[item] ? dataRow[item] : '&nbsp;';
				if(itemData instanceof Date)
					itemData = itemData.format(DWR.DEFAULT_DATE_FORMAT);
				try {
					switch(this.fields[item].jField.type) {
						case 'ENUM':
						case 'ENUM_AUTOCOMPLETE':
							if(this.cachedValues[item]) {
								var allOpts = this.cachedValues[item].allOptions;
								for(var j = 0; j < allOpts.length; j ++) {
									if(allOpts[j][itemData]) {
										itemData = allOpts[j][itemData];
										//break;
									}
								}
							}
						break;
						case 'MONEY':
							itemData = this.fields[item].formatter(String(itemData), true).formattedValue;
						break;
					}
					var fieldVal = this.fields[item].getSimpleValue({selectedOptionName: true, formatted: true});
					
					if(fieldVal) {
						var regexp = RegExp('(' + RegExp.escape(fieldVal) + ')', "i");
						itemData = itemData.replace(regexp, '<b>$1</b>');
					}
				} catch(e) {
				}
				jqRow.children('td.' + item).html(itemData);
			}
			this.jqResultsTable.children('tbody').children('tr:last').after(jqRow);
			jqRow.data('item', data[i]).hover(function(){
				$j(this).addClass('ac_over');
			}, function(){
				$j(this).removeClass('ac_over');
			}).bind('mousedown', function() { // �eby po dwukrotnym klikni�ciu tekst si� nie zaznacza�
				return false;
			}).bind('click.selectItem', function() {
				if (!$j(this).hasClass('selectedItem')) {
					_this.jqResultsTable.children('tbody').children('tr.selectedItem').removeClass('selectedItem');
					$j(this).addClass('selectedItem');
					_this.buttons.jqEditButton.attr('disabled', false).data('item', $j(this).data('item'));
					_this.buttons.jqDeleteButton.attr('disabled', false).data('item', $j(this).data('item'));
					_this.jqSelectedRow = $j(this);
					if (!_this.searchMode) {
						_this.selectItem($j(this).data('item'));
					}
					else {
						_this.buttons.jqSelectButton.attr('disabled', false);
					}
				}
				else {
					if (_this.searchMode) {
						$j(this).removeClass('selectedItem');
						_this.restoreSearchValues();
						_this.buttons.jqSelectButton.attr('disabled', 'disabled');
						_this.buttons.jqEditButton.attr('disabled', 'disabled');
						_this.buttons.jqDeleteButton.attr('disabled', 'disabled');
						_this.jqSelectedRow = null;
					}
				}
			}).bind('dblclick.selectItem', function(event) {
				if (!$j(this).hasClass('selectedItem')) {
					$j(this).trigger('click.selectItem');
				}
				_this.buttons.jqSelectButton.trigger('click');
			}).removeClass('fieldsOrder').attr('id', htmlId).show();
		}
		
		/* Ustawiamy szeroko�� nag��wka tabeli */
		var cellsArray = jqRow.children('td').toArray();
		var j = 0;
		this.jqResultsTableHeader.width(this.jqResultsTable.width() + 16);
		this.jqResults.width(this.jqResultsTable.width() + 16);
		this.jqResultsTableHeader.children('div').each(function() {
			try {
				$j(this).width($j(cellsArray[j]).width());
			} catch(e) {} // w IE7 'invalid argument'
			j ++;
		});
		
		/* Przewijanie */
		this.jqResults.scrollTop(this.jqResults[0].scrollHeight);
		if(scrollableResults) {
			this.buttons.jqNextPageButton.show();
		} else {
			this.buttons.jqNextPageButton.slideUp('fast');
		}
	} else {
		/* Nic nie znale�li�my */
		var colCount = this.jqResultsTableRowTemplate.children('td').length;
		var str = '<tr><td class="noResults" colspan="' + colCount + '">' + DictionaryPopup.Messages.NO_RESULTS_FOUND + '</td></tr>';
		this.jqResultsTable.children('tbody').children('tr:last').after(str);
		this.buttons.jqNextPageButton.hide();
	}
	
	if(onComplete)
		onComplete();
}

DictionaryPopup.prototype.clearResults = function() {
	this.jqResultsTable.children('tbody').children('tr:not(:first)').remove();
}

DictionaryPopup.prototype.clearFields = function() {
	for (var cn in this.fields) {
		this.fields[cn].setValue('');
	}
}

DictionaryPopup.prototype.restoreSearchValues = function() {
	for(var cn in this.fields) {
		var field = this.fields[cn];
		field.clearValue();
		if(field.lastSearch) {
			field.setValue(field.lastSearch, {allOptions: false});
			/*for(var key in field.lastSearch) {
				if(key != 'type') {
					field.setValue(field.lastSearch[key]);
				}
			}*/			
		}
	}
}

DictionaryPopup.prototype.moveLabelInside = function(field) {
	field.jqLabel.html(field.jqLabel.html().replace(':', ''));
	field.jqField.bind('focus.labelDecoration', function() {
		field.jqLabel.hide();
	}).bind('blur.labelDecoration', function() {
		field.jqLabel.show();
	});
}

DictionaryPopup.prototype.selectItem = function(data, disableEditButton) {
	if(this.pure === true) {
		for(var cn in this.fields) {
			if(data[cn] != null) {
				try {
					this.fields[cn].setValue(data[cn], {allOptions: false});
				} catch(e) {}
			} else {
				this.fields[cn].clearValue();
			}
		}
	} else {
	for(var cn in data) {
		try {
			this.fields[cn].setValue(data[cn], {allOptions: false});
		} catch(e) {
			
		}	
	}
	}
}

/**
 * Waliduje jedno pole
 * @param {Object} field
 */
DictionaryPopup.prototype.validateField = FieldsManager.prototype.validateField;

/**
 * Ta metoda jest wywo�ywana po otrzymaniu zdarzenia .submit lub .submitDictionary
 * Zostawiamy pust�, by ewentualnie co� doda� w przysz�o�ci
 */
DictionaryPopup.prototype.submitFields = function() {}

DictionaryPopup.delay = (function(){
	var timer = null;
	return function(callback, time){
		if(timer != null)
			clearTimeout(timer);
		timer = setTimeout(callback, time);
	};
})();

DictionaryPopup.prototype.registerFieldListener = function(field, disableSearch) {
	FieldsManager.prototype.registerFieldListener.call(this, field);
	
	var _this = this;
	var params = {allOptions : false};
	var eventName = ['keyup'];
	
	/** w ms */
	var searchKeyupDelay = 1000;
	
	switch(field.jField.type) {
		case 'BOOLEAN':
			eventName = ['change'];
		break;
		case 'ENUM':
		case 'ENUM_AUTOCOMPLETE':
		case 'ENUM_MULTIPLE':
			eventName = ['change', 'keyup'];
		break;
		case 'DATE':
			eventName = ['dateSelect.submit'];
		break;
	}
		
	eventName = eventName.concat(['']).join('.search ').trim();
	field.disableSearch = disableSearch === true;
	field.jqField.bind('search', function(event, onComplete) {
		if (_this.searchMode) {
			field.lastSearch = $j(this).val();
			_this.searchFields = {};
			var i = 0;
			if(_this.pure === true) {
				// console.info('PURE search - id only');
				_this.searchFields[_this.dictionaryName + '_ID'] = _this.fields[_this.dictionaryName + '_ID'].getValue(params);
			} else {
			for (var key in _this.fields) {
				if (key != 'id') {
					//if(_this.fields[key].getSimpleValue())
						_this.searchFields[key] = _this.fields[key].getValue(params);
					_this.fields[key].jqField.removeClass('invalid');
				}
			}
			}
			
			// dla daty szukamy dopiero gdy pe�na data jest wpisana (05-11-1987)
			if(field.jField.type == 'DATE') {
				/*try {
					var d = Date.prototype.fromString($j(this).val(), field.dateFormat);
					_this.searchFields[field.jField.cn] = {type: 'DATE', dateData: d};
				} catch(e) {
					return;
				}*/
			}
			// console.info('SEARCH', _this.searchFields);
			if(field.disableSearch !== true) {
			Dictionary.dwrSearch(_this.dictionaryName, _this.searchFields, DWR.PAGE_SIZE + 1, 0, function(data) {
				_this.clearResults();
				_this.fillResults(data, onComplete);
			});
			}
		} else {
			_this.buttons.jqEditButton.hide();
			_this.buttons.jqSaveButton.attr('disabled', false).show();
		}
	});
	field.jqVisibleField.bind(eventName, function(event, onComplete) {
		if(_this.pure === true) {
			field.jqField.trigger('search', [onComplete]);
		} else {
		DictionaryPopup.delay(function(){
			field.jqField.trigger('search', [onComplete]);
		}, searchKeyupDelay );
		}
	});
	// jQuery datepicker wysy�a tylko zdarzenie change bez przestrzeni nazw
	/*if(field.jField.type == 'DATE') {
		field.jqField.bind('change ', function(event, onComplete) {
			$j(this).trigger('search', [onComplete]);
		});
	}*/
}

/**
 * Dopasowuje d�ugo�ci p�l, �eby wszystko by�o wy�wietlane r�wno
 * @method
 */
DictionaryPopup.prototype.fixFieldsWidth = function() {
	/* Wzorzec d�ugo�ci pola */
	var widthTmpl = this.jqFieldset.children('div.field.STRING:visible:first').outerWidth();
	for(var cn in this.fields) {
		if(this.fields[cn].jField.type == 'ENUM' || this.fields[cn].jField.type == 'ENUM_AUTOCOMPLETE') {
			var jqField = this.fields[cn].jqField;
			var newWidth = Math.ceil(jqField.width() / widthTmpl) * 172 - 14;
			jqField.css('width', newWidth + 'px');			
		}
	}
}

/**
 * Ustawia warto�ci p�l z mapy cachedValues.
 * Nie mo�emy tego zrobi� w metodzie <code>create<code>,
 * gdy� nie wiemy kiedy sko�czy si� zapytanie ajaxowe o warto�ci p�l s�ownika
 * @method
 */
DictionaryPopup.prototype.updateFieldsValues = function(cachedValues) {
	this.cachedValues = cachedValues;
	for (var cn in this.fields) {
		var field = this.fields[cn];
		/* Ustawiamy warto�� dla enum�w - np.: opcje */
		if (cachedValues) {
			try {
				//trace('Trying to set cachedValue for ' + cn + ' : ' + cachedValues[cn]);
				if (cachedValues[cn]) {
					//trace('Cached value for ' + cn + ' exists');
					field.setValue(cachedValues[cn]);
				}
			} catch(e) {
				
			}
		}
	}
}

DictionaryPopup.prototype.createDialog = function() {
	try {
		this.jqPopup.dialog('destroy');
	} catch(e) {}
	this.jqPopup.dialog(
		{
			width: this.popupWidth,
			modal: true,
			autoOpen: false,
			resizable: false
		}
	);

	if(this.pure === true) {
		this.jqPopup.on('dialogclose', $j.proxy(function(event, ui) {
			// console.info('closing dialog with event', event);
			if(!this.searchMode) {
				this.buttons.jqCancelButton.trigger('click');
			}
			try {
				this.fieldsManager.multipleFieldsHelper.fixAddNextDisplay();
			} catch(e) {

			}
			if(event.altKey != null && this.onCancel != null) {
				// console.info('Firing onCancel');
				this.onCancel({multipleFieldsNumber: this.multipleFieldsNumber, createNew: this.createNew});
			}
			//console.info(this.createNew, this.createNewCompleted);
		}, this));
	}
}

/**
 * Pokazuje okienko popup dla s�ownika i automatycznie uruchamia wyszukiwanie
 * z warto�ciami <code>values</code>
 * @method
 * @param {Object} values Mapa z warto�ciami p�l
 * @param {Object} params [dodatkowy argument] Mapa z dodatkowymi ustawieniami
 * <ul>
 * <li><code>multipleFieldsNumber {Number} </code> Numer pola dla p�l wielowarto�ciowych
 * </ul>
 */
DictionaryPopup.prototype.show = function(values, params) {
	var _this = this;
	var curCn, key, curField, curFieldVal;
	//przeniesienie wymagalno�ci p�l ze s�ownika na popup
	for (var property in this.fieldsManager.fields) {
        if (this.fieldsManager.fields.hasOwnProperty(property)) {
            if(this.fieldsManager.dictionaryPopup.fields.hasOwnProperty(property)){
    			this.fieldsManager.dictionaryPopup.fields[property].jField.required = this.fieldsManager.fields[property].jField.required;
    		}
    	}
    }
	if(params && params.multipleFieldsNumber) {
		this.multipleFieldsNumber = params.multipleFieldsNumber;
	}
	if(params && params.onCancel != null) {
		this.onCancel = params.onCancel;
	}
	for(curCn in values) {
		try {
			curField = values[curCn];
			curFieldVal = null;
			for (key in curField) {
				if(key != 'type')
					curFieldVal = curField[key];
			}
			if (curFieldVal != null) {
				if(this.multipleFieldsNumber != null) {
					// zmieniamy NAZWA_POLA_3 na NAZWA_POLA
					//curCn = curCn.replace(/_\d+$/,'');
				}
				this.fields[curCn].setValue(curFieldVal);
				this.fields[curCn].lastSearch = curFieldVal;
			}
		} catch(e) {}
	}
	
	this.showMessage('', true);
	this.buttons.jqSelectButton.attr('disabled', 'disabled');
	this.buttons.jqEditButton.attr('disabled', 'disabled');
	_this.searchFields = {};
	
	var createNew = false;
	if(this.pure === true) {
		if($j.isEmptyObject(values) === true) {
			// console.info('popup');
			this.jqPopup.removeClass('dictionaryPopupPureEdit');
			createNew = true;
		} 
	}

	if(createNew) {
		this.jqPopup.removeClass('dictionaryPopupPureEdit');
		this.createNew = true;
		this.createNewCompleted = false;
	} else if(this.pure === true) {
		this.jqPopup.addClass('dictionaryPopupPureEdit');
		this.createNew = false;
	}
	
	// dla p�l wielowarto�ciowych potrzebujemy id (je�li jest podane)
	if(this.multiple) {
		if(values.id)
			_this.searchFields.id = values.id;
	} 
	// console.warn('vals', values);
	if(!createNew) {
		if(this.pure === true) {
			_this.searchFields[this.dictionaryName + '_ID'] = values[this.dictionaryName + '_ID'];
		} else {
	for(key in _this.fields) {
		if(values[key])
			_this.searchFields[key] = values[key];
		else if(_this.fields[key].getSimpleValue() != null)
			_this.searchFields[key] = _this.fields[key].getValue({allOptions: false});
	}
		}
	}
	
	__LOAD_NEXT_RESULTS_OFFSET = 0;
	this.jqPopup.dialog('open');
	if (_this.searchFields && !createNew) {
		// console.info(_this.searchFields);
		Dictionary.dwrSearch(_this.dictionaryName, _this.searchFields, DWR.PAGE_SIZE + 1, 0, function(data) {
			_this.clearResults();
			_this.fillResults(data);
			if(_this.pure === true) {
				// console.info('selecting...')
				// zaznaczamy wiersz do edycji
				_this.jqResultsTable.find('tr:not(.fieldsOrder):first').trigger('click.selectItem');
				_this.buttons.jqEditButton.trigger('click');
			}
		});
	} else {
		this.buttons.jqClearButton.trigger('click');		
	}

	if(createNew) {
		for(key in _this.fields) {
			_this.fields[key].setValue([""], {allOptions: false});
		}
		// fieldsManager.fields.DWR_MPK.dictionaryFieldsManager.dictionaryPopup.fields.MPK_ACCEPTINGCENTRUMID.clearValue()
	}

	if (this.fieldsWidthAlreadyFixed == false) {
		this.fixFieldsWidth();
		this.fieldsWidthAlreadyFixed = true;
	}
}

DictionaryPopup.prototype.getValues = function(isDictionary) {
	return FieldsManager.prototype.getValues.call(this, isDictionary);
}

DictionaryPopup.prototype.dispose = function() {
}

DictionaryPopup.Messages = {
	ITEM_SAVED: DWR.MSG.DIC_POPUP_ITEM_SAVED,
	ITEM_ADDED: DWR.MSG.DIC_POPUP_ITEM_ADDED,
	ITEM_DELETED: DWR.MSG.DIC_POPUP_ITEM_DELETED,
	ITEM_SAVED_FAILED: DWR.MSG.DIC_POPUP_ITEM_SAVED_FAILED,
	ITEM_ADDED_FAILED: DWR.MSG.DIC_POPUP_ITEM_ADDED_FAILED,
	ITEM_DELETED_FAILED: DWR.MSG.DIC_POPUP_ITEM_DELETED_FAILED,
	ITEM_ADDED_EMPTY_WARNING: DWR.MSG.DIC_POPUP_ITEM_ADDED_EMPTY_WARNING,
	ITEM_ADDED_NOT_VALID: DWR.MSG.DIC_POPUP_ITEM_ADDED_NOT_VALID,
	NO_RESULTS_FOUND: DWR.MSG.DIC_POPUP_NO_RESULTS_FOUND
}

DictionaryPopup.MIN_WIDTH = 915;

DictionaryPopup.DefaultVisibleButtons = {
	doAdd: true,
	doEdit: true,
	doRemove: true,
	doSelect: true,
	doSelectSmall: false,
	doClear: true
}



// Do otwierania starych popup�w
function accept(param, values) {
	var popupCn, dicCn;
	var helper = PopupAcceptHelper[param];
	var dwrVals = {};
	
	for(popupCn in values) {
		dicCn = helper.lookup[popupCn];
		dwrVals[dicCn] = values[popupCn];
		//PopupAcceptHelper[param].dfm.fields[dicCn].setValue(values[popupCn]);
	}
	helper.dfm.acceptItem(dwrVals, helper.popupParams);
}

function createPopupValues(param, fields, popupVals) {
	var revLookup = PopupAcceptHelper[param].reversedLookup;
	var dicCn, popupCn, url = '', oldPopupVals = [], unpackedValue;
	
	if(popupVals != null) {
		for(dicCn in popupVals) {
			unpackedValue = BaseField.unpackValues(popupVals[dicCn]);
			if(unpackedValue != '') {
				popupCn = revLookup[dicCn];
				if(popupCn != null) {
					oldPopupVals.push(popupCn + '=' + unpackedValue);
				}
			}
		}
	} else {
		for(dicCn in fields) {
			unpackedValue = fields[dicCn].getSimpleValue();
			if(unpackedValue != '') {
				popupCn = revLookup[dicCn];
				oldPopupVals.push(popupCn + '=' + unpackedValue);
			}
		}
	}
	
	return oldPopupVals.join('&');
}

function createPopupValuesForFields(param, fields, chosedFields) {
	var revLookup = PopupAcceptHelper[param].reversedLookup;
	var dicCn, popupCn, url = '', oldPopupVals = [], unpackedValue;

	if(chosedFields != null) {
		for(dicCn in chosedFields) {
		    var field = fields[chosedFields[dicCn]];
		    unpackedValue = field.getSimpleValue();
			if(unpackedValue != '') {

				popupCn = revLookup[chosedFields[dicCn]];

				if(popupCn != null) {
					oldPopupVals.push(popupCn + '=' + unpackedValue);
				}
			}
		}
	}

	return oldPopupVals.join('&');
}

var PopupAcceptHelper = {
	'KLIENT': {
		dfm: null,
		lookup: {
			'id': 'id',
			'name': 'KLIENT_NAME',
			'phoneNumber': 'KLIENT_PHONENUMBER'
		}, 
		reversedLookup: {}
	},
	'DOSTAWCA': {
		dfm: null,
		lookup: {
			'id': 'id',
			'name': 'DOSTAWCA_NAME',
			'phoneNumber': 'DOSTAWCA_PHONENUMBER'
		},
		reversedLookup: {}
	},
	'NUMER_UMOWY': {
		dfm: null,
		lookup: {
			'id': 'id',
			'numerUmowy': 'NUMER_UMOWY_NUMERUMOWY'
		},
		reversedLookup: {}
	},
	'NUMER_WNIOSKU': {
		dfm: null,
		lookup: {
			'id': 'id',
			'numerWniosku': 'NUMER_WNIOSKU_NUMERWNIOSKU'
		},
		reversedLookup: {}
	},
	'MASZYNA_PODZESPOL_CZESC': {
		dfm: null,
		lookup: {
			'elementId': 'id',
			'rodzaj': 'MASZYNA_PODZESPOL_CZESC_RODZAJCZESCI',
			'kod' : 'MASZYNA_PODZESPOL_CZESC_KOD',
			'nazwa' : 'MASZYNA_PODZESPOL_CZESC_NAZWA',
			'nrSeryjny' : 'MASZYNA_PODZESPOL_CZESC_NR_SERYJNY',
			'producent' : 'MASZYNA_PODZESPOL_CZESC_PRODUCENT',
			'model' : 'MASZYNA_PODZESPOL_CZESC_MODEL',
			'lokalizacja' : 'MASZYNA_PODZESPOL_CZESC_LOKALIZACJA',
			'strategiczna' : 'MASZYNA_PODZESPOL_CZESC_STRATEGICZNA',
			'zdjecie' : 'MASZYNA_PODZESPOL_CZESC_ZDJECIE',
			'typMaszyny' : 'MASZYNA_PODZESPOL_CZESC_TYP',
			'opis' : 'MASZYNA_PODZESPOL_CZESC_OPIS'
		},
		reversedLookup: {}
	},
	'SWD_ORGANISATION_UNIT': {
		dfm: null,
		lookup: {
			'ouId'          : 'SWD_ORGANISATION_UNIT_SWDID',
			'ouName'        : 'SWD_ORGANISATION_UNIT_NAME',
		},
		reversedLookup: {}
	},
	'SWD_USER': {
		dfm: null,
		lookup: {
			'userId'        : 'SWD_USER_SWDUID',
			'userFirstName' : 'SWD_USER_FIRST_NAME',
			'userLastName'  : 'SWD_USER_LAST_NAME',
		},
		reversedLookup: {}
	},
	'SWD_USER_NEW': {
		dfm: null,
		lookup: {
			'userId'        : 'SWD_USER_NEW_SWDUID',
			'userFirstName' : 'SWD_USER_NEW_FIRST_NAME',
			'userLastName'  : 'SWD_USER_NEW_LAST_NAME',
		},
		reversedLookup: {}
	},
	'SWD_USER_DATA_MODIFIED': {
		dfm: null,
		lookup: {
			'userId'        : 'SWD_USER_DATA_MODIFIED_SWDUID',
			'userFirstName' : 'SWD_USER_DATA_MODIFIED_FIRST_NAME',
			'userLastName'  : 'SWD_USER_DATA_MODIFIED_LAST_NAME',
		},
		reversedLookup: {}
	},
	'SWD_USER_DEACTIVATED': {
		dfm: null,
		lookup: {
			'userId'        : 'SWD_USER_DEACTIVATED_SWDUID',
			'userFirstName' : 'SWD_USER_DEACTIVATED_FIRST_NAME',
			'userLastName'  : 'SWD_USER_DEACTIVATED_LAST_NAME',
		},
		reversedLookup: {}
	},
	'ILPOL_SPLIT_TIFF': {
	    dfm: {
	        acceptItem: function(values) {
	            var cn;
	            for(cn in values) {
	                window.fieldsManager.fields[cn].setValue(values[cn]);
	            }
	        }
	    },
	    lookup: {
	        'DWR_SPLIT_MESSAGE': 'DWR_SPLIT_MESSAGE'
	    },
	    reversedLookup: {}
	}
};

// wyliczamy reversed lookup
function generateReversedLookup() {
	var param, helper;
	var key, val;
	
	for(param in PopupAcceptHelper) {
		helper = PopupAcceptHelper[param];
		for(key in helper.lookup) {
			val = helper.lookup[key];
			helper.reversedLookup[val] = key;
		}
	}
}
generateReversedLookup();

// KLIENT
function ClientPopup(dfm, search) {
	PopupAcceptHelper.KLIENT.dfm = dfm;
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('KLIENT', dfm.fields);
	
	if(search == true) {
		params += '&doSearch=true';
	}
	
	openToolWindow(ctx + '/office/common/contractor.action?param=KLIENT&' + params, 
			'KLIENT', 700, 770);
}

// DOSTAWCA
function SupplierPopup(dfm, search) {
	PopupAcceptHelper.DOSTAWCA.dfm = dfm;
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('DOSTAWCA', dfm.fields);
	
	if(search == true) {
		params += '&doSearch=true';
	}
	
	openToolWindow(ctx + '/office/common/contractor.action?param=DOSTAWCA&' + params, 
			'KLIENT', 700, 770);
}

// KONTRAHENT
function ContractorPopup(dfm, search) {
	PopupAcceptHelper.KLIENT.dfm = dfm;
	PopupAcceptHelper.KLIENT.lookup = {
		'id': 'id',
		'name': 'NUMER_KONTRAHENTA_NAME',
		'phoneNumber': 'NUMER_KONTRAHENTA_PHONENUMBER'
	}
	generateReversedLookup();
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('KLIENT', dfm.fields);
	
	if(search == true) {
		params += '&doSearch=true';
	}
	
	openToolWindow(ctx + '/office/common/contractor.action?param=KLIENT&' + params, 
			'KLIENT', 700, 770);
}

// UMOWA - r�wnie� multiple
function ContractPopup(dfm, search, popupVals, popupParams) {
	PopupAcceptHelper.NUMER_UMOWY.dfm = dfm;
	PopupAcceptHelper.NUMER_UMOWY.popupVals = popupVals;
	PopupAcceptHelper.NUMER_UMOWY.popupParams = popupParams;
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('NUMER_UMOWY', dfm.fields, popupVals);
	
	if(search == true) {
		params += '&doSearch=true';
	}
	
	openToolWindow(ctx + '/office/common/dlContractDictionary.action?param=NUMER_UMOWY&' + params, 
			'NUMER_UMOWY', 700, 770);
}

// WNIOSEK - dla s�ownika multiple
function ApplicationPopup(dfm, search, popupVals, popupParams) {
	PopupAcceptHelper.NUMER_WNIOSKU.dfm = dfm;
	PopupAcceptHelper.NUMER_WNIOSKU.popupVals = popupVals;
	PopupAcceptHelper.NUMER_WNIOSKU.popupParams = popupParams;
	
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('NUMER_WNIOSKU', dfm.fields, popupVals);
	
	if(search == true) {
		params += '&doSearch=true';
	}
	openToolWindow(ctx + '/office/common/dlApplicationDictionary.action?param=NUMER_WNIOSKU&' + params, 
			'NUMER_WNIOSKU', 700, 770);
}

//PREWENCJA - s�ownik maszyn
function MachinesPopup(dfm, search, popupVals, popupParams) {
	PopupAcceptHelper.MASZYNA_PODZESPOL_CZESC.dfm = dfm;
	PopupAcceptHelper.MASZYNA_PODZESPOL_CZESC.popupVals = popupVals;
	PopupAcceptHelper.MASZYNA_PODZESPOL_CZESC.popupParams = popupParams;
	
	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('MASZYNA_PODZESPOL_CZESC', dfm.fields, popupVals);
	
	openToolWindow(ctx + '/machinetree/show-tree-pop-up.action');
}

//SWD Organisation Unit
function OrganisationUnitPopup(dfm, search, popupVals, popupParams) {

	PopupAcceptHelper.SWD_ORGANISATION_UNIT.dfm = dfm;
	PopupAcceptHelper.SWD_ORGANISATION_UNIT.popupVals = popupVals;
	PopupAcceptHelper.SWD_ORGANISATION_UNIT.popupParams = popupParams;

	var ctx = $j('#request_pageContext').val();
	var params = createPopupValues('SWD_ORGANISATION_UNIT', dfm.fields, popupVals);

	openToolWindowX(ctx + '/swd/management/organisation-unit-list.action?layout=popup',"SWD_ORGANISATION_UNIT", screen.width * 0.9, screen.height * 0.9);
}

//SWD User
function SwdUserPopup(dfm, search, popupVals, popupParams) {


	PopupAcceptHelper[dictionaryName].dfm = dfm;
	PopupAcceptHelper[dictionaryName].popupVals = popupVals;
	PopupAcceptHelper[dictionaryName].popupParams = popupParams;

	var ctx = $j('#request_pageContext').val();
	var ouId = getSwdOUid(dfm);
	var params = "clientID="+ouId;
    var status = checkDocumentStatus(dfm)
    //if(status ==  null || status == 10){
        params += "&readonly=true";
    //}


    //screen.width , screen.height * 0.9 otwiera okno na ca�y ekran z miejscem na pasek zada� windows
	openToolWindowX(ctx + '/swd/management/users-list.action?pop=true&'+params,dictionaryName, screen.width * 0.9 , screen.height * 0.9);
}

//SWD Create User
function SwdCreateUserPopup(dfm, search, popupVals, popupParams) {
    var dictionaryName = dfm.dictionaryPopup.dictionaryName;

	PopupAcceptHelper[dictionaryName].dfm = dfm;
	PopupAcceptHelper[dictionaryName].popupVals = popupVals;
	PopupAcceptHelper[dictionaryName].popupParams = popupParams;

	var ctx = $j('#request_pageContext').val();
    var ouId = getSwdOUid(dfm);
    var params = "organisationUnit="+ouId;
    params+= "&pop=true";
	openToolWindowX(ctx + '/swd/management/user-view.action?'+params,dictionaryName, screen.width * 0.9 , screen.height * 0.9);
}

function checkDocumentStatus(dfm){

    var fm = dfm.parentFieldsManager;
    var unpackedValue;

    var field = fm.fields["DWR_STATUS"];
    unpackedValue = field.getSimpleValue();

    return unpackedValue;
}

//Zaka�ada, �e s�ownik z organisation unit ma cn = SWD_ORGANISATION_UNIT
//a organisation unit id dodaje si� do pola s�ownika o cn = SWDID
function getSwdOUid(dfm){
    var fm = dfm.parentFieldsManager;
    var value;

	var field = fm.fields["DWR_SWD_ORGANISATION_UNIT"]
    var ouFM = field.dictionaryFieldsManager;

	value = ouFM.fields["SWD_ORGANISATION_UNIT_SWDID"].getSimpleValue();


    return value;
}

