<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N move-division.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal,
				 pl.compan.docusafe.core.users.DSDivision"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Przenie� dzia� lub stanowisko</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/organization/move-division.action'"/>" method="post">
	<ww:hidden name="'firstDivisionGuid'"/>
	<ww:hidden name="'guid'"/>
	
	<ww:if test="moved==false">
		Wybierz dzia�, do kt�rego chcesz przeniesc
		<ww:property value="name"/>
		
		<br/>
		<br/>
		<ww:property value="treeHtml" escape="false"/>
		
		<br/>
		<input type="submit" name="doMove" value="Przenie�" class="btn"/>
	</ww:if>
	
	<ww:else>
		Przeniesiono
		<ww:property value="name"/>
		
		<br/>
		<br/>
		<input type="button" class="btn" value="Powr�t" onclick="redirect()"/>
	</ww:else>
</form>

<script type="text/javascript">
	function redirect()
	{
		document.location='<ww:url value="'/organization/edit-organization.do'"/>';
	}
</script>

<%--
<h1>Przenie� dzia� lub stanowisko</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/organization/move-division.action'"/>" method="post">

<script>
	function redirect()
	{
		document.location='<ww:url value="'/organization/edit-organization.do'"/>';
	}
</script>


<ww:hidden name="'firstDivisionGuid'"/>
<ww:hidden name="'guid'"/>

<table width="100%">
<ww:if test="moved==false">
	<tr>
		<td>
			Wybierz dzia�, do kt�rego chcesz przeniesc <ww:property value="name"/>  <br/><br/>
		</td>
	</tr>
	<tr>
		<td>
			<ww:property value="treeHtml" escape="false" /><br/><br/>
		</td>
	</tr>	
	<tr>
		<td>
			<input type="submit" name="doMove" value="Przenie�" class="btn"/> 	
		</td>
	</tr>	
</ww:if>

<ww:else>
	<tr>
		<td>
			Przeniesiono <ww:property value="name"/>  <br/><br/><br/>
		</td>
	</tr>
	<tr>
		<td>
			<input type="button" class="btn" value="Powr�t"
				onclick="redirect()" />
		</td>
	</tr>
</ww:else>

</table>
</form>
--%>
<!--N move-division.jsp N-->