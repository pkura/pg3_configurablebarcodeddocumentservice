<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N division.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script language="JavaScript">
/**
    Funkcja wywo�ywana przez okienko wyboru u�ytkownika (pick-subject.jsp).
*/
function __pick_subject(value, text)
{
    var form = document.getElementById('division');

    document.getElementById('username').value = value;
    document.getElementById('doAddUser').value = 'true';

    form.submit();
}
</script>

<h3><edm-html:message key="division.h1"/></h3>

<edm-html:errors />

<html:form action="/organization/division" styleId="division" >
<html:hidden property="divisionGuid"/>
<html:hidden property="username" styleId="username" />
<html:hidden property="doAddUser" styleId="doAddUser" />

<!-- �cie�ka do bie��cego dzia�u -->
<p class="division-path">
    <c:forEach items="${divisionPath}" var="pathElement" varStatus="status">
        <a href='<c:out value="${pageContext.request.contextPath}${pathElement.link}"/>'><c:out value="${pathElement.name}"/></a>
        <c:if test="${!status.last}"> / </c:if>
    </c:forEach>
</p>

<h4>Podrz�dne dzia�y</h4>

<table border="0" cellspacing="2" cellpadding="0">
<tr>
    <th>Nazwa dzia�u</th><th></th><th></th>
</tr>
<c:forEach items="${divisionBeans}" var="bean">
    <c:if test="${!bean.position and !bean.group}">
    <tr>
        <td><a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.name}"/></a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.editLink}"/>'>edycja</a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.deleteLink}"/>'>usu�</a></td>
    </tr>
    </c:if>
</c:forEach>
</table>

<h4>Stanowiska</h4>

<table border="0" cellspacing="2" cellpadding="0">
<tr>
    <th>Nazwa stanowiska</th><th></th><th></th>
</tr>
<c:forEach items="${divisionBeans}" var="bean">
    <c:if test="${bean.position}">
    <tr>
        <td><a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.name}"/></a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.editLink}"/>'>edycja</a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.deleteLink}"/>'>usu�</a></td>
    </tr>
    </c:if>
</c:forEach>
</table>

<h4>Grupy</h4>

<table border="0" cellspacing="2" cellpadding="0">
<tr>
    <th>Nazwa grupy</th><th></th><th></th>
</tr>
<c:forEach items="${divisionBeans}" var="bean">
    <c:if test="${bean.group}">
    <tr>
        <td><a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.name}"/></a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.editLink}"/>'>edycja</a></td>
        <td>&nbsp;&nbsp;<a href='<c:out value="${pageContext.request.contextPath}${bean.deleteLink}"/>'>usu�</a></td>
    </tr>
    </c:if>
</c:forEach>
</table>

<p></p>

<!-- nowy dzia� i stanowisko -->
<table border="0" cellspacing="2" cellpadding="0">
<c:if test="${enableAddingDivisions}">
    <tr>
        <td>Dodaj dzia�</td>
        <td><html:text property="divisionName" size="30" maxlength="128" styleClass="txt"/>
            <html:submit property="doCreateDivision" styleClass="btn" ><edm-html:message key="doCreate" global="true"/></html:submit>
        </td>
    </tr>
</c:if>
<c:if test="${enableAddingPositions}">
    <tr>
        <td>Dodaj stanowisko</td>
        <td><html:text property="positionName" size="30" maxlength="128" styleClass="txt"/>
            <html:submit property="doCreatePosition" styleClass="btn" ><edm-html:message key="doCreate" global="true"/></html:submit>
        </td>
    </tr>
    <tr>
        <td>Dodaj grup�</td>
        <td><html:text property="groupName" size="30" maxlength="128" styleClass="txt"/>
            <html:submit property="doCreateGroup" styleClass="btn" ><edm-html:message key="doCreate" global="true"/></html:submit>
        </td>
    </tr>
</c:if>
</table>

<p></p>

<!-- u�ytkownicy w dziale -->

<c:if test="${enableAddingUsers}">
    <h4>U�ytkownicy w tym dziale</h4>

    <table border="0" cellspacing="2" cellpadding="0">
    <c:forEach items="${userBeans}" var="bean">
        <tr>
            <td><input type="checkbox" name="selectedUsers" value="<c:out value='${bean.name}'/>"/></td>
            <td><a href='<c:out value="${pageContext.request.contextPath}${bean.editLink}"/>'><c:out value="${bean.firstname}"/> <c:out value="${bean.lastname}"/> (<c:out value="${bean.name}"/>)</a></td>
        </tr>
    </c:forEach>
    <tr>
        <td></td>
        <td><html:submit property="doRemoveUsers" styleClass="btn" >Usu� u�ytkownik�w</html:submit>
            <edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${addUserLink}" value="DodajUzytkownika..." styleClass="btn"></edm-html:window-open-button>
        </td>
    </tr>
    </table>
</c:if>


</html:form>

