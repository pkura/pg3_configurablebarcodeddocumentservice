<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-organization.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.core.office.Journal,
				 pl.compan.docusafe.core.users.DSDivision"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="StrukturaOganizacyjna"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/organization/edit-organization" styleId="editOrganization">
	<html:hidden property="divisionGuid"/>
	<html:hidden property="doAddUser" styleId="doAddUser" />
	<html:hidden property="doAddUsers" styleId="doAddUsers" />
	<html:hidden property="doAddBackup" styleId="doAddBackup" />

	<span id="hidden_usernames"></span>
	
	<table id="editOrgToolbar">
		<tr>
		<td>
			&nbsp;
			<input class="zoomImg" type="image" height="24" width="24"
				onclick="javascript:document.location.href='<c:out value='${pageContext.request.contextPath}${renameDivisionLink}'/>'; return false" 
				title="<ds:lang text="ZmianaNazwyDzialuStanowiskaLubGrupy"/>"
				src="<c:out value='${pageContext.request.contextPath}'/>/img/edit-folder.gif"	
			/>
			</td>
		<td>
			&nbsp;
			<c:if test="${canDeleteDivision}">
			<input class="zoomImg" type="image" height="24" width="24"
				onclick="javascript:document.location.href='<c:out value='${pageContext.request.contextPath}'/>/organization/delete-division.do?divisionGuid=<c:out value="${divisionGuid}"/>'; return false" 
				title="<ds:lang text="UsunDzialStanowiskoLubGrupe"/>"
				src="<c:out value='${pageContext.request.contextPath}'/>/img/delete-folder.gif"	
			/>
			</c:if>
		
			<c:if test="${!canDeleteDivision}">
				<input class="zoomImg zoomDisabled" type="image" height="24" width="24"					 
					title="<ds:lang text="UsunDzialStanowiskoLubGrupe"/>"
					src="<c:out value='${pageContext.request.contextPath}'/>/img/delete-folder_g.gif"	
				/>
			</c:if>
		</td>
		<td>
			&nbsp;
			<c:if test="${canDeleteDivision}">
				<input class="zoomImg" type="image" height="24" width="24"
					onclick="javascript:document.location.href='<c:out value='${pageContext.request.contextPath}'/>/organization/move-division.action?firstDivisionGuid=<c:out value="${divisionGuid}"/>'; return false" 
					title="<ds:lang text="ZmienLokalizacjeDzialu"/>"
					src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-move.gif"	
				/>
			</c:if>
		
			<c:if test="${!canDeleteDivision}">
				<input type="image" width="24" height="24" src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-move_g.gif" class="zoomImg zoomDisabled"
					title="<ds:lang text="ZmienLokalizacjeDzialu"/>" disabled="true">
			</c:if>
		</td>
		<td>
			&nbsp;
			<c:if test="${canSetRoles}">
				<input class="zoomImg" type="image" height="24" width="24"
					onclick="javascript:document.location.href='<c:out value='${pageContext.request.contextPath}${setRolesLink}'/>'; return false" 
					title="<ds:lang text="PrzypiszRoleStanowiskuLubGrupie"/>"
					src="<c:out value='${pageContext.request.contextPath}'/>/img/lock-folder.gif"	
				/>
			</c:if>
		
			<c:if test="${!canSetRoles}">
				<input class="zoomImg zoomDisabled" type="image" height="24" width="24" 
					title="<ds:lang text="PrzypiszRoleStanowiskuLubGrupie"/>"
					src="<c:out value='${pageContext.request.contextPath}'/>/img/lock-folder_g.gif"	
				/>
			</c:if>
		</td>
		</tr>
	</table>

	<br/>
	<br/>
	
	<ds:additions test="!extendedTree">
		<c:out value="${tree}" escapeXml="false"/>
	</ds:additions>

	<ds:additions test="extendedTree">
		<c:out value="${etree}" escapeXml="false"/>
	</ds:additions>
	
	<br/>
	<ds:lang text="Legenda"/>:
	
	<br/>
	<span class="continuousElement">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-closed.gif" class="img17" title="<ds:lang text="GrupaUzytkownikow"/>"/>
		-
		<ds:lang text="dzial"/>
	</span>
	<span class="continuousElement">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" class="img17" title="<ds:lang text="Stanowisko"/>"/>
		-
		<ds:lang text="stanowisko"/>
	</span>
	<span class="continuousElement">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" class="img17" title="<ds:lang text="GrupaUzytkownikow"/>"/>
		-
		<ds:lang text="grupa"/>
	</span>
	<ds:modules test="workflow">
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-swimlane.gif" class="img17" title="<ds:lang text="RolaWzewnetrznymWorkflow"/>"/>
			-
			<ds:lang text="swimlane"/>
		</span>
	</ds:modules>
	<ds:additions test="extendedTree">
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-user.gif" class="img17" title="user"/>
			-
			<ds:lang text="User"/>
		</span>
	</ds:additions>

	<c:if test="${enableAddingDivisions or enableAddingGroups or enableAddingPositions or enablingAddingSwimlanes}">
		<h4><ds:lang text="NowyElementStruktury"/></h4>
		
		<span class="continuousElement">
			<c:if test="${enableAddingDivisions}">
				<html:radio property="divisionType" value="<%= DSDivision.TYPE_DIVISION %>"/>
				<ds:lang text="dzial"/>
			</c:if>
			<c:if test="${enableAddingGroups}">
				<html:radio property="divisionType" value="<%= DSDivision.TYPE_GROUP %>"/>
				<ds:lang text="grupa"/>
			</c:if>
			<c:if test="${enableAddingPositions}">
				<html:radio property="divisionType" value="<%= DSDivision.TYPE_POSITION %>"/>
				<ds:lang text="stanowisko"/>
			</c:if>
			<c:if test="${enableAddingSwimlanes}">
				<html:radio property="divisionType" value="<%= DSDivision.TYPE_SWIMLANE %>"/>
				<ds:lang text="swimlane"/>
			</c:if>

		</span>
		
		<table class="tableMargin">
			<tr>
				<td>
					<ds:lang text="Nazwa"/>
					<span class="star">*</span>:
				</td>
				<td>
					<html:text property="name" size="30" maxlength="62" styleClass="txt"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Kod"/>:
				</td>
				<td>
					<html:text property="code" size="5" maxlength="20" styleClass="txt"/>
				</td>
			</tr>
		</table>
		
		<input type="submit" name="doCreateDivision" value="<ds:lang text="Dodaj"/>" class="btn"/>
	</c:if>
	
	<ds:modules test="workflow">
		<h4><ds:lang text="IdentyfikatorUczestnikaProcesuWorkflow"/></h4>
		<html:text property="wfParticipantId" size="50" readonly="true" styleClass="txt"/>
	</ds:modules>
	
	<c:if test="${!empty userBeans or enableAddingUsers}">
		<ds:additions test="!extendedTree"><h4><ds:lang text="UzytkownicyWtymDzialeGrupieStanowisku"/></h4>
		</ds:additions>

		
		<c:if test="${enableAddingUsers}">
			<br/>
			<edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${addUserLink}"  styleClass="btn"><ds:lang text="DodajUzytkownika..."/></edm-html:window-open-button>
			<edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${addBackupLink}"  styleClass="btn"><ds:lang text="DodajZastepstwa"/></edm-html:window-open-button>
		</c:if>
		<c:if test="${!empty userBeans}">
			<input type="submit" name="doRemoveUsers" class="btn" value="<ds:lang text="UsunUzytkownikow"/>"/>
		</c:if>

		<ds:additions test="!extendedTree">
			<ul class="clearList">
				<c:forEach items="${userBeans}" var="bean">
					<li><input type="checkbox" name="selectedUsers"
						value="<c:out value='${bean.name}'/>" /> <c:out
						value="${bean.description}" /></li>
				</c:forEach>
				<c:forEach items="${userBackupBeans}" var="bean">
					<li><input type="checkbox" name="selectedUsers"
						value="<c:out value='${bean.name}'/>" /> <i><b><c:out
						value="${bean.description}" /></b></i></li>
				</c:forEach>
			</ul>

			<c:if test="${enableAddingUsers}">
				<edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${addUserLink}" styleClass="btn"><ds:lang text="DodajUzytkownika..." /></edm-html:window-open-button>
				<edm-html:window-open-button windowHeight="200" windowWidth="300" windowTarget="_blank" href="${addBackupLink}" styleClass="btn"><ds:lang text="DodajZastepstwa" /></edm-html:window-open-button>
			</c:if>
			<c:if test="${!empty userBeans}">
				<input type="submit" name="doRemoveUsers" class="btn"
					value="<ds:lang text="UsunUzytkownikow"/>" />
			</c:if>
		</ds:additions>
	</c:if>
	
	<p>
		<span class="star">*</span>
		<ds:lang text="PoleObowiazkowe"/>
	</p>
</html:form>

<script type="text/javascript">
	/**
		Funkcja wywo�ywana przez okienko wyboru u�ytkownika (pick-subject.jsp).
	*/
	function __pick_subject(value, text,backup)
	{
		var form = document.getElementById('editOrganization');
	
		document.getElementById('username').value = value;
		if(backup)
		{
			document.getElementById('doAddBackup').value = 'true';
			document.getElementById('doAddUsers').value = 'false';
		}
		else
		{
			document.getElementById('doAddUsers').value = 'true';
			document.getElementById('doAddBackup').value = 'false';
		}
	
		form.submit();
	}
	
	function __pick_subjects(options,backup) 
	{
		for (var i=0; i < options.length; i++)
		{
			var el = document.createElement("input");
			el.type = "hidden";
			el.name = "usernames";
			el.value = options[i].value;
			document.getElementById('hidden_usernames').appendChild(el);
		}
	
		if(backup)
		{
			document.getElementById('doAddBackup').value = true;
		}
		else
		{
			document.getElementById('doAddUsers').value = true;
		}

		var form = document.getElementById('editOrganization');
		form.submit();
	}
</script>
<!--N koniec edit-organization.jsp N-->