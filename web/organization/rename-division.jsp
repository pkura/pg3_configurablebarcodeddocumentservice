<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N rename-division.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZmianaNazwyDzialuStanowiskaLubGrupy"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/organization/rename-division">
	<html:hidden property="divisionGuid"/>
	
	<c:out value="${tree}" escapeXml="false" />

	<br/>
	<ds:lang text="Legenda"/>:
	
	<br/>
	<span class="continuousElement">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" class="img17" alt="<ds:lang text="Stanowisko"/>"/>
		-
		<ds:lang text="Stanowisko"/>
	</span>
	
	<span class="continuousElement">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" class="img17" alt="<ds:lang text="GrupaUzytkownikow"/>"/>
		-
		<ds:lang text="GrupaUzytkownikow"/>
	</span>
	
	<ds:modules test="workflow">
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-swimlane.gif" class="img17" alt="Rola w zewnętrznym Workflow"/>
			-
			swimlane
		</span>
	</ds:modules>
	
	<br/>
	<br/>
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="Nazwa"/>
				<span class="star">*</span>:
			</td>
			<td>
				<html:text property="name" size="50" maxlength="62" styleClass="txt"/>
			</td>
		</tr>
		<c:if test="${codeNeeded}">
			<tr>
				<td>
					<ds:lang text="Kod"/>:
				</td>
				<td>
					<html:text property="code" size="5" maxlength="20" styleClass="txt_opt"/>
				</td>
			</tr>
		</c:if>
	</table>
	
	<input type="submit" name="doRename" value="<ds:lang text="Zapisz"/>" class="btn saveBtn">
	<input type="submit" name="doReturn" value="<ds:lang text="PowrotDoEdycjiStruktury"/>" class="btn">
</html:form>

<%--
<h1><ds:lang text="ZmianaNazwyDzialuStanowiskaLubGrupy"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/organization/rename-division">
<html:hidden property="divisionGuid"/>


<table width="100%">
<tr>
	<td width="250" valign="top">
		<div style="overflow-x: auto; width: 250px; ">
			<c:out value="${tree}" escapeXml="false" />

			<hr size="1" noshade="noshade"/>

			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="Stanowisko"/>"/> - <ds:lang text="Stanowisko"/>
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="GrupaUzytkownikow"/>"/> - <ds:lang text="GrupaUzytkownikow"/>
			<ds:modules test="workflow">
				<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-swimlane.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Rola w zewnętrznym Workflow"/> - swimlane</nobr>
			</ds:modules>
		</div>
	</td>
</tr>
<tr>
	<td valign="top">

		<table>
		<tr>
			<td><ds:lang text="Nazwa"/><span class="star">*</span>:</td>
			<td><html:text property="name" size="30" maxlength="62" styleClass="txt"/></td>
		</tr>
		<c:if test="${codeNeeded}">
			<tr>
				<td><ds:lang text="Kod"/>:</td>
				<td><html:text property="code" size="5" maxlength="5" styleClass="txt_opt"/></td>
			</tr>
		</c:if>
		<tr>
			<td></td>
			<td><input type="submit" name="doRename" value="<ds:lang text="Zapisz"/>" class="btn">
				<input type="submit" name="doReturn" value="<ds:lang text="PowrotDoEdycjiStruktury"/>" class="btn">
			</td>
		</tr>
		</table>

	</td>
</tr>
</table>

</html:form>
--%>
<!--N koniec rename-division.jsp N-->
