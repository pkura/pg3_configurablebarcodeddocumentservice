<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-organization-confirm-add-user.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1>Dodawanie u�ytkownika</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<edm-html:errors />
<edm-html:messages />

<html:form action="/organization/edit-organization" styleId="editOrganization">
<html:hidden property="divisionGuid"/>
<html:hidden property="username" styleId="username" />
<html:hidden property="confirmAddUser" styleId="confirmAddUser" />

<p>U�ytkownik <c:out value="${userDescription}"/> znajduje si� ju� w nast�puj�cych
dzia�ach:
<ul>
    <c:forEach items="${userDivisions}" var="divisionName">
        <li><c:out value="${divisionName}"/></li>
    </c:forEach>
</ul>
Czy nadal chcesz doda� u�ytkownika do dzia�u <c:out value="${newDivision}"/>?</p>

<input type="submit" name="doAddUser" value="Tak" onclick="document.getElementById('confirmAddUser').value='true';" class="btn">
<input type="submit" name="doAddBackup" value="Dodaj zast�pstwo" onclick="document.getElementById('confirmAddUser').value='true';" class="btn">
<input type="button" value="Nie" onclick="document.location.href='<c:out value='${returnUrl}'/>';" class="btn">

</html:form>
