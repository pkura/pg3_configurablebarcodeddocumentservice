<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N division-roles.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Role zwi�zane ze stanowiskiem lub grup�</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/organization/division-roles">
	<html:hidden property="divisionGuid"/>
	
	<table class="tableMargin">
		<tr>
			<td class="alignTop">
				<c:out value="${tree}" escapeXml="false" />
				
				<br/>
				<ds:lang text="Legenda"/>:
				
				<br/>
				<span class="continuousElement">
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" class="img17" alt="Stanowisko"/>
					-
					stanowisko
				</span>
				
				<span class="continuousElement">
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" class="img17" alt="Grupa u�ytkownik�w"/>
					-
					grupa
				</span>
			</td>
			<td class="alignTop">
				<c:if test="${!empty officeRoles}">
					<table class="tableMargin">
						<c:forEach items="${officeRoles}" var="role">
							<tr>
								<td>
									<c:out value="${role.label}"/>
								</td>
								<td>
									<input type="checkbox" name="officeRoles" value="<c:out value='${role.value}'/>" <c:if test="${role.checked}">checked="true"</c:if>/>
								</td>
							</tr>
						</c:forEach>
					</table>
					
					<input type="submit" name="doUpdate" value="Zapisz" class="btn"/>
					<input type="submit" name="doReturn" value="Powr�t do edycji struktury" class="btn">
				</c:if>
			</td>
		</tr>
	</table>
</html:form>

<%--
<h3>Role zwi�zane ze stanowiskiem lub grup�</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/organization/division-roles">
<html:hidden property="divisionGuid"/>


<table width="100%">
<tr>
	<td width="250" valign="top">

		<div style="overflow-x: auto; width: 250px; ">
			<c:out value="${tree}" escapeXml="false" />

			<hr size="1" noshade="noshade"/>

			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Stanowisko"/> - stanowisko
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Grupa u�ytkownik�w"/> - grupa
		</div>
	</td>
	<td valign="top">
		<c:if test="${!empty officeRoles}">
		<table>
			<c:forEach items="${officeRoles}" var="role">
				<tr>
					<td><c:out value="${role.label}"/></td>
					<td><input type="checkbox" name="officeRoles" value="<c:out value='${role.value}'/>" <c:if test="${role.checked}">checked="true"</c:if>/></td>
				</tr>
			</c:forEach>
			<tr>
				<td colspan="2">
					<input type="submit" name="doUpdate" value="Zapisz" class="btn"/>
					<input type="submit" name="doReturn" value="Powr�t do edycji struktury" class="btn">
				</td>
			</tr>
		</table>
		</c:if>
	</td>
</tr>
</table>


</html:form>
--%>
<!--N division-roles.jsp N-->