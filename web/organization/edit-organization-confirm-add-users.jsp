<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-organization-confirm-add-users.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DodawanieUzytkownikow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<edm-html:errors />
<edm-html:messages />

<html:form action="/organization/edit-organization" styleId="editOrganization">
<html:hidden property="divisionGuid"/>
<html:hidden property="username" styleId="username" />
<html:hidden property="confirmAddUsers" styleId="confirmAddUsers" />

<c:forEach items="${confirmedUsernames}" var="username">
    <input type="hidden" name="confirmedUsernames" value="<c:out value="${username}"/>"/>
</c:forEach>

<p><ds:lang text="uzytkownicyWdzialachCzyDodac"/>
<c:out value="${newDivision}"/>.
</p>

<table>
<tr>
    <th></th>
    <th><ds:lang text="Uzytkownik"/></th>
    <th><ds:lang text="ZnajdujeSieWDzialach"/></th>
</tr>
<c:forEach items="${usersToConfirm}" var="user">
    <tr>
        <td><input type="checkbox" name="confirmedUsernames" value="<c:out value="${user.user.name}"/>/user"/></td>
        <td><c:out value="${user.user.firstname} ${user.user.lastname}"/></td>
        <td><i><c:forEach items="${user.divisionPaths}" var="path" varStatus="status" >
                <c:out value="${path}"/><c:if test="${!status.last}">, </c:if>
            </c:forEach></i>
        </td>
    </tr>
</c:forEach>
</table>

<input type="submit" name="doAddUsers" value="<ds:lang text="DodajUzytkownikow"/>" onclick="document.getElementById('confirmAddUsers').value='true';" class="btn">
<input type="submit" name="doAddBackup" value="<ds:lang text="DodajZastepstwo"/>" onclick="document.getElementById('confirmAddUsers').value='true' " class="btn">

<input type="button" value="<ds:lang text="Rezygnuj"/>" onclick="document.location.href='<c:out value='${returnUrl}'/>';" class="btn">


</html:form>
