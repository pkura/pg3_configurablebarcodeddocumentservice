<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-division.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><edm-html:message key="division.h1"/></h3>

<edm-html:errors />

<html:form action="/organization/edit-division">
<html:hidden property="divisionGuid"/>
<html:hidden property="contextDivisionGuid"/>

<p>
    <c:forEach items="${divisionPath}" var="pathElement" varStatus="status">
        <a href='<c:out value="${pageContext.request.contextPath}${pathElement.link}"/>'><c:out value="${pathElement.name}"/></a>
        <c:if test="${!status.last}"> / </c:if>
    </c:forEach>
</p>

<table cellspacing="0" cellpadding="0">
<tr>
    <td>Nazwa<span class="star">*</span>:</td>
    <td><html:text property="name" size="30" maxlength="128" styleClass="txt"/></td>
</tr>
</table>

<html:submit property="doUpdate" styleClass="btn" ><edm-html:message key="doUpdate" global="true"/></html:submit>
<html:submit property="doCancel" styleClass="cancel_btn"><edm-html:message key="doCancel" global="true"/></html:submit>

</html:form>

