<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N delete-division.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><edm-html:message key="division.h1"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>

<html:form action="/organization/delete-division" styleId="form">
	<html:hidden property="divisionGuid"/>
	<html:hidden property="substGuid" styleId="substGuid" />
	<input type="hidden" name="doDelete" id="doDelete"/>
	
	<p>
		<c:forEach items="${divisionPath}" var="pathElement" varStatus="status">
			<a href='<c:out value="${pageContext.request.contextPath}${pathElement.link}"/>'>
				<c:out value="${pathElement.name}"/></a>
			<c:if test="${!status.last}"> / </c:if>
		</c:forEach>
	</p>
	
	<p>
		<ds:lang text="UsuwanieDzialu"/>
		<c:out value="${division.name}"/>
	</p>
	
	<c:if test="${canDelete}">
		<p>
			<ds:lang text="WybierzDzialDoKtoregoPoUsunieciuBiezacego"/>
			<input type="button" value="<ds:lang text="Wybierz"/>" class="btn" onclick="javascript:void(window.open('<c:out value="${pageContext.request.contextPath}"/>/office/pick-division.action?constraints=<c:out value="${division.divisionType}"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"/>
		</p>
	</c:if>
	<c:if test="${!canDelete}">
		<p>
			<ds:lang text="WybranyDzialNieMozeZostacUsunietyPoniewazWdzialach"/>
		</p>
	</c:if>

	<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/organization/edit-organization.do?divisionGuid=<c:out value="${division.guid}"/>';"/>
</html:form>

<%--
<h3><edm-html:message key="division.h1"/></h3>

<edm-html:errors />

<html:form action="/organization/delete-division" styleId="form">
<html:hidden property="divisionGuid"/>
<html:hidden property="substGuid" styleId="substGuid" />
	<input type="hidden" name="doDelete" id="doDelete"/>

<p>
	<c:forEach items="${divisionPath}" var="pathElement" varStatus="status">
		<a href='<c:out value="${pageContext.request.contextPath}${pathElement.link}"/>'><c:out value="${pathElement.name}"/></a>
		<c:if test="${!status.last}"> / </c:if>
	</c:forEach>
</p>

<p><ds:lang text="UsuwanieDzialu"/> <c:out value="${division.name}"/></p>

	<c:if test="${canDelete}">
		<p><ds:lang text="WybierzDzialDoKtoregoPoUsunieciuBiezacego"/>
		<input type="button" value="<ds:lang text="Wybierz"/>" class="btn" onclick="javascript:void(window.open('<c:out value="${pageContext.request.contextPath}"/>/office/pick-division.action?constraints=<c:out value="${division.divisionType}"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"/>
		</p>
	</c:if>
	<c:if test="${!canDelete}">
		<p><ds:lang text="WybranyDzialNieMozeZostacUsunietyPoniewazWdzialach"/></p>
	</c:if>

	<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/organization/edit-organization.do?divisionGuid=<c:out value="${division.guid}"/>';"/>

</html:form>
--%>
<script type="text/javascript">
	function pickDivision(reference, guid, prettyPath)
	{
		if (guid != null)
		{
			if (guid == '<c:out value="${division.guid}"/>')
			{
				alert('<ds:lang text="WybierzInnyDzialNizBiezacy"/>');
				return false;
			}

			document.getElementById('substGuid').value = guid;
			document.getElementById('doDelete').value = 'true';
			disableFormSubmits(document.getElementById('form'));
			document.getElementById('form').submit();
		}

		return true;
	}
</script>
<!--N delete-division.jsp N-->