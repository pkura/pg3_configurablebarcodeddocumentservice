
function loadAcceptanceView(documentId){
	$j("#acceptanceView").fadeOut('slow');
	$j.post("acceptance-view.action", {documentId:documentId}, function(data){
		$j("#acceptanceView").stop().hide().html(data).fadeIn().hide().show('slow');
	});
}

$j(document).ready(function(){
	$j("#ajaxError").ajaxError(function(event, request, settings){
	   $j(this).append("Error requesting page " + settings.url);
	});
});