/**
 * 
 * @param {Object} params Jesli nie podane to uÄąÄ˝ywamy parametrÄ‚Ĺ‚w domyÄąâ€şlnych
 */
JSSign = function(params) {
	/**
	 * DomyÄąâ€şlne parametry. <br />
	 * domCertSignApplet - obiekt DOM appletu Java sÄąâ€šuÄąÄ˝Ă„â€¦cego do podisu (PEMI)
	 * domCertListApplet - obiekt DOM appletu Java sÄąâ€šuÄąÄ˝Ă„â€¦cego do pobrania listy certifikatÄ‚Ĺ‚w 
	 */
	var defaultParams = {
		domCertListApplet: document.certlist,
		domCertSignApplet: document.signer,
		domCertVerifierApplet: document.verifier
	}
	var params = params === undefined ? defaultParams : params;
	var certificates = null;

	return {
		/**
		 * Tworzy select z listĂ„â€¦ certyfikatÄ‚Ĺ‚w do wyboru. <br />
		 * @param {Object} jqSelect Jesli podane, to wpisuje opcje do tego selekta.
		 * @return {Object} Obiekt jQuery z wypeÄąâ€šnionym selectem
		 */
		createSignaturesSelect: function(jqSelect) {
			if(!params.domCertSignApplet)
				throw "Certificates Sign Applet not found";
			if(!certificates) {
				try {
					certificates = eval('(' + params.domCertListApplet.getCertsAsJSON() + ')');
				} catch(e) {
					certificates = null;
					throw "Certificates list is not valid JSON string";
				}
			}
			if(certificates.length < 1){
				alert("Nieznaleziono czytnika kart kryptograficznych!")
				throw "Certificates list is empty";
			}else if(certificates.length == 1){
				var	curItem = null;
				jqOption = null;
				curItem = certificates[0];
				jqOption = $j('<option/>');
				jqOption.val(curItem.sha1).html("Wlasciel certyfikatu ->:" +curItem.alias +" wazny do dnia : "+curItem.expirationDate);
				jqSelect.append(jqOption);
			}else {	
			var jqSelect = jqSelect === undefined || jqSelect.length < 1 ? $j('<select/>').addClass('sel') : jqSelect;
			var i = 0;
				len = 0;
				curItem = null;
				jqOption = null;
			for(i = 0, len = certificates.length; i < len; i ++) {
				curItem = certificates[i];
				jqOption = $j('<option/>');
//				jqOption.val(curItem.sha1).html(curItem.alias).data('cert', curItem);
				jqOption.val(curItem.sha1).html("Wlasciel certyfikatu ->:" +curItem.alias +" wazny do dnia do dnia : "+curItem.expirationDate);
				jqSelect.append(jqOption);
			}
			}
			return jqSelect;
		},
		/**
		 * Podpisuje certifikatem
		 * @param {String} content Podpisywany XML
		 * @param {String} sha SHA1 certyfikatu 
		 * @return {String} Podpisany XML
		 */
		sign: function(content, sha) {
			if(!params.domCertSignApplet){
				throw "Certificates Sign Applet not found";
			}
			try {
				var temp = content;
			/*var stringToFind = "Ä„";
				var stringToReplace = "A";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				var stringToFind = "Ä…";
				var stringToReplace = "a";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				var stringToFind = "Ĺ›";
				var stringToReplace = "s";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				var stringToFind = "Ĺš";
				var stringToReplace = "S";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				var stringToFind = "Ĺş";
				var stringToReplace = "z";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				var stringToFind = "Ĺą";
				var stringToReplace = "Z";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}
				
				var stringToFind = "Ĺ‚";
				var stringToReplace = "l";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}	
				var stringToFind = "Ĺ";
				var stringToReplace = "L";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}			
				var stringToFind = "Ä…";
				var stringToReplace = "a";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}	
				var stringToFind = "Ä‡";
				var stringToReplace = "c";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}	
				var stringToFind = "Ä†";
				var stringToReplace = "C";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);
				}		
				var stringToFind = "Ĺ„";
				var stringToReplace = "n";
				var index = temp.indexOf(stringToFind);
				while(index != -1){
					temp = temp.replace(stringToFind,stringToReplace);
					index = temp.indexOf(stringToFind);*/
														
					
				var content2 = temp;
		        params.domCertSignApplet.setSignatureFile(1, content2);
		        params.domCertSignApplet.setCertificateSHA1(sha);
		        params.domCertSignApplet.sign(1);
		    } catch (e) {
		        exception = e;
		        alert(sha + " " + e);
				throw "Certificate signing error";
		    }
			
			return params.domCertSignApplet.getSigned();
		},

        /**
         * Sprawdza poprawnoÂ¶Ä‡ podpisu
         * @param {String} signedFile plik, ktory byĹ‚ podpisany
         * @param {String} signatureFile plik podpisu
         * @return {String}
         */
		validate: function(signedFile, signatureFile) {
            if(!params.domCertSignApplet){
				throw "Certificates Sign Applet not found";
			}
			try {
		        params.domCertVerifierApplet.setSignatureFile(1, signatureFile);
		        params.domCertVerifierApplet.setSignatureNode("Signature-1");
		        var status = params.domCertVerifierApplet.verify();
		        return status;
		    } catch (e) {
		        alert(e);
				throw "Certificate signing error";
		    }
		}
	}
}