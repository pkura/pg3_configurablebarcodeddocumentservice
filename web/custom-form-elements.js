/*

CUSTOM FORM ELEMENTS

Created by Ryan Fait
www.ryanfait.com

The only thing you need to change in this file is the following
variables: checkboxHeight, radioHeight and selectWidth.

Replace the first two numbers with the height of the checkbox and
radio button. The actual height of both the checkbox and radio
images should be 4 times the height of these two variables. The
selectWidth value should be the width of your select list image.

You may need to adjust your images a bit if there is a slight
vertical movement during the different stages of the button
activation.

Visit http://ryanfait.com/ for more information.

*/

var checkboxHeight = "25";
var radioHeight = "25";
var selectWidth = "190";
var ie = !window.opera && document.all ? 1 : 0;

/* No need to change anything after this */

document.write('<style type="text/css">input.styled, fieldset.container p input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; }</style>');
document.write('<style type="text/css">input.styledDynamic { display: none; } select.styledDynamic { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; }</style>');
document.write('<style type="text/css">input.styledGlobal { display: none; } select.styledGlobal { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; }</style>');

Custom = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className == "styled") {
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;
				if(ie) {
					span[a].innerHTML = '&nbsp;';
					span[a].style.cursor = 'default';
				}

				if(inputs[a].checked == true) {
					if(inputs[a].type == "checkbox") {
						position = "0 -" + (checkboxHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					} else {
						position = "0 -" + (radioHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					}
				}
				
				if(inputs[a].onchange) {
					if(!inputs[a].id)
						inputs[a].id = 'pk_' + a;
					var str = inputs[a].onchange.toString();
					//var str2 = 'document.getElementById("' + 'pk_' + a + '")';
					var str2 = 'document.getElementById("' + inputs[a].id + '")';
					str = str.replace('this', str2);
					var startOfs = str.indexOf('{');
					var endOfs = str.lastIndexOf('}');
					str = str.slice(++startOfs, endOfs);
					
					span[a].onclick = new Function(str);
				}
				
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.clear;
				span[a].onmousedown = Custom.pushed;
				span[a].onmouseup = Custom.check;
				document.onmouseup = Custom.clear;
				
				if(inputs[a].onclick) {
					inputs[a].id = 'pk_' + a;
					var str = inputs[a].onclick.toString();
					var str2 = 'document.getElementById("' + 'pk_' + a + '")';
					str = str.replace('this', str2);
					var startOfs = str.indexOf('{');
					var endOfs = str.lastIndexOf('}');
					str = str.slice(++startOfs, endOfs);
					
					span[a].onclick = new Function(str);
				}
			}
		}
		inputs = document.getElementsByTagName("select");
		for(a = 0; a < inputs.length; a++) {
			if(inputs[a].className == "styled") {
				option = inputs[a].getElementsByTagName("option");
				active = option[0].childNodes[0].nodeValue;
				textnode = document.createTextNode(active);
				for(b = 0; b < option.length; b++) {
					if(option[b].selected == true) {
						textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
					}
				}
				span[a] = document.createElement("span");
				span[a].className = "select";
				span[a].id = "select" + inputs[a].name;
				span[a].appendChild(textnode);
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.choose;
			}
		}
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
		} else if(element.checked == true && element.type == "radio") {
			this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
		} else if(element.checked != true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		}
	},
	check: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 0";
			element.checked = false;
		} else {
			if(element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
				group = this.nextSibling.name;
				inputs = document.getElementsByTagName("input");
				for(a = 0; a < inputs.length; a++) {
					if(inputs[a].name == group && inputs[a] != this.nextSibling) {
						inputs[a].previousSibling.style.backgroundPosition = "0 0";
					}
				}
			}
			element.checked = true;
		}
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else if(inputs[b].type == "checkbox" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
			} else if(inputs[b].type == "radio" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			}
		}
	},
	choose: function() {
		option = this.getElementsByTagName("option");
		for(d = 0; d < option.length; d++) {
			if(option[d].selected == true) {
				document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
			}
		}
	}
}


CustomDynamic = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className == "styledDynamic") {
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;
				if(ie) {
					span[a].innerHTML = '&nbsp;';
					span[a].style.cursor = 'default';
				}

				if(inputs[a].checked == true) {
					if(inputs[a].type == "checkbox") {
						position = "0 -" + (checkboxHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					} else {
						position = "0 -" + (radioHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					}
				}
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = CustomDynamic.clear;
				span[a].onmousedown = CustomDynamic.pushed;
				span[a].onmouseup = CustomDynamic.check;
				document.onmouseup = CustomDynamic.clear;
			}
		}
		inputs = document.getElementsByTagName("select");
		for(a = 0; a < inputs.length; a++) {
			if(inputs[a].className == "styledDynamic") {
				option = inputs[a].getElementsByTagName("option");
				active = option[0].childNodes[0].nodeValue;
				textnode = document.createTextNode(active);
				for(b = 0; b < option.length; b++) {
					if(option[b].selected == true) {
						textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
					}
				}
				span[a] = document.createElement("span");
				span[a].className = "select";
				span[a].id = "select" + inputs[a].name;
				span[a].appendChild(textnode);
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = CustomDynamic.choose;
			}
		}
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
		} else if(element.checked == true && element.type == "radio") {
			this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
		} else if(element.checked != true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		}
	},
	check: function() {
		element = this.nextSibling;
		
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 0";
			element.checked = false;
		} else {
			if(element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
				group = this.nextSibling.name;
				inputs = document.getElementsByTagName("input");
				for(a = 0; a < inputs.length; a++) {
					if(inputs[a].name == group && inputs[a] != this.nextSibling) {
						inputs[a].previousSibling.style.backgroundPosition = "0 0";
					}
				}
			}
			element.checked = true;
		}
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		//alert('clear');
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className == "styledDynamic") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else if(inputs[b].type == "checkbox" && inputs[b].className == "styledDynamic") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className == "styledDynamic") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
			} else if(inputs[b].type == "radio" && inputs[b].className == "styledDynamic") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			}
		}
	},
	choose: function() {
		option = this.getElementsByTagName("option");
		for(d = 0; d < option.length; d++) {
			if(option[d].selected == true) {
				document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
			}
		}
	}
}

CustomGlobal = {
		init: function() {
			var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
			for(a = 0; a < inputs.length; a++) {
				if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className == "styledGlobal") {
					span[a] = document.createElement("span");
					span[a].className = inputs[a].type;
					if(ie) {
						span[a].innerHTML = '&nbsp;';
						span[a].style.cursor = 'default';
					}

					if(inputs[a].checked == true) {
						if(inputs[a].type == "checkbox") {
							position = "0 -" + (checkboxHeight*2) + "px";
							span[a].style.backgroundPosition = position;
						} else {
							position = "0 -" + (radioHeight*2) + "px";
							span[a].style.backgroundPosition = position;
						}
					}
					inputs[a].parentNode.insertBefore(span[a], inputs[a]);
					inputs[a].onchange = Custom.clear;
					span[a].onmousedown = Custom.pushed;
					span[a].onmouseup = Custom.check;
					document.onmouseup = Custom.clear;
					
					if(inputs[a].onclick) {
						inputs[a].id = 'pk_' + a;
						var str = inputs[a].onclick.toString();
						var str2 = 'document.getElementById("' + 'pk_' + a + '")';
						str = str.replace('this', str2);
						var startOfs = str.indexOf('{');
						var endOfs = str.lastIndexOf('}');
						str = str.slice(++startOfs, endOfs);
						
						span[a].onclick = new Function(str);
					}
				}
			}
			inputs = document.getElementsByTagName("select");
			for(a = 0; a < inputs.length; a++) {
				if(inputs[a].className == "styledGlobal") {
					option = inputs[a].getElementsByTagName("option");
					active = option[0].childNodes[0].nodeValue;
					textnode = document.createTextNode(active);
					for(b = 0; b < option.length; b++) {
						if(option[b].selected == true) {
							textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
						}
					}
					span[a] = document.createElement("span");
					span[a].className = "select";
					span[a].id = "select" + inputs[a].name;
					span[a].appendChild(textnode);
					inputs[a].parentNode.insertBefore(span[a], inputs[a]);
					inputs[a].onchange = Custom.choose;
				}
			}
		},
		pushed: function() {
			element = this.nextSibling;
			if(element.checked == true && element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
			} else if(element.checked == true && element.type == "radio") {
				this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
			} else if(element.checked != true && element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight + "px";
			}
		},
		check: function() {
			element = this.nextSibling;
			if(element.checked == true && element.type == "checkbox") {
				this.style.backgroundPosition = "0 0";
				element.checked = false;
			} else {
				if(element.type == "checkbox") {
					this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
				} else {
					this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
					group = this.nextSibling.name;
					inputs = document.getElementsByTagName("input");
					for(a = 0; a < inputs.length; a++) {
						if(inputs[a].name == group && inputs[a] != this.nextSibling) {
							inputs[a].previousSibling.style.backgroundPosition = "0 0";
						}
					}
				}
				element.checked = true;
			}
		},
		clear: function() {
			inputs = document.getElementsByTagName("input");
			for(var b = 0; b < inputs.length; b++) {
				if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className == "styledGlobal") {
					inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
				} else if(inputs[b].type == "checkbox" && inputs[b].className == "styledGlobal") {
					inputs[b].previousSibling.style.backgroundPosition = "0 0";
				} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className == "styledGlobal") {
					inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
				} else if(inputs[b].type == "radio" && inputs[b].className == "styledGlobal") {
					inputs[b].previousSibling.style.backgroundPosition = "0 0";
				}
			}
		},
		choose: function() {
			option = this.getElementsByTagName("option");
			for(d = 0; d < option.length; d++) {
				if(option[d].selected == true) {
					document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
				}
			}
		}
	}