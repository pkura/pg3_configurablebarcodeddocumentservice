<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.core.cfg.Configuration,
				 pl.compan.docusafe.boot.Docusafe,
				 org.apache.commons.logging.Log,
				 org.apache.commons.logging.LogFactory,
				 pl.compan.docusafe.core.users.auth.AuthUtil,
				 pl.compan.docusafe.core.cfg.Mail,
				 pl.compan.docusafe.core.users.UserNotFoundException,
				 pl.compan.docusafe.core.*,
				 pl.compan.docusafe.util.StringManager,
				 pl.compan.docusafe.core.users.DSUser"%>
<%@ page import="pl.compan.docusafe.service.mail.Mailer"%>
<%@ page import="pl.compan.docusafe.service.ServiceManager"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<%!
	private static final Log log = LogFactory.getLog("password-recovery");
%>
<ds:available test="uwlodzki">
<%
	String redirectURL = "https://logowanie.uni.lodz.pl/changepw/";
	response.sendRedirect(redirectURL);
%>
</ds:available>


<%
	String username = request.getParameter("username");
	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);

	log.debug("username="+username);

	if (username != null && username.trim().length() > 0)
	{
		username = username.trim();
		try
		{
			DSContext ctx = DSApi.openAdmin();
			ctx.begin();

			DSUser user;

			if (username.indexOf('@') > 0)
			{
				user = DSUser.findByEmail(username);
			}
			else
			{
				user = DSUser.findByUsername(username);
			}
			System.out.println("ad  "+user.getAdUser());
			if(user.getAdUser() != null && user.getAdUser())
			{
				System.out.println("ad2  "+user.getAdUser());
				request.setAttribute("passwordSent", Boolean.FALSE);
				request.setAttribute("userOnAD", Boolean.TRUE);
				log.error(sm.get("UzytkownikZnajdujeSieWActiveDirectory"));
				
				if (Docusafe.getAdditionProperty("link_odzyskiwania_hasla_typu_ad") != null	&& !Docusafe.getAdditionProperty("link_odzyskiwania_hasla_typu_ad").equals("")) {
					request.setAttribute("userOnADLink", Boolean.TRUE);
				}
			}
			log.debug("user="+user);

			// nie mo�na w ten spos�b odzyska� has�a administratora

			if (!user.getName().equals(GlobalPreferences.getAdminUsername()) &&
				user.getEmail() != null)
			{
				log.debug("email="+user.getEmail());

				String newPassword = AuthUtil.generatePassword(8);

				user.setPassword(newPassword);
				
				Mailer mailer = (Mailer) ServiceManager.getService(Mailer.NAME);

				Map<String, Object> context = new HashMap<String, Object>();
				context.put("username", user.getName());
				context.put("password", newPassword);
				context.put("loginUrl", Configuration.getBaseUrl());

				mailer.send(user.getEmail(), user.asFirstnameLastname(), null,
					Configuration.getMail(Mail.PASSWORD_RECOVERY), context);
				
				request.setAttribute("passwordSent", Boolean.TRUE);
			}else{
				request.setAttribute("adminPassword", Boolean.TRUE);
			}
			ctx.commit();
		}
		catch (UserNotFoundException e)
		{
			request.setAttribute("passwordSent", Boolean.FALSE);
			log.error(e.getMessage(), e);
		}
		catch (EdmException e)
		{
			log.error(e.getMessage(), e);
		}
		catch (Exception e)
		{
			log.error(e.getMessage(), e);
		}
		finally
		{
			DSApi._close();
			
		}
	}
%>


<%@page import="pl.compan.docusafe.util.StringManagerExeption"%>
<%@page import="antlr.ASdebug.ASDebugStream"%><html>

	<head>
		<title><ds:lang text="TytulStrony"/></title>
		
		<c:if test="${docusafeType == null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		<%--	W tym IFie mozna dodac favicon dla wersji testowej systemu - podmienic tylko nazwe	--%>
		<c:if test="${docusafeType != null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		
		<META HTTP-EQUIV="content-type" CONTENT="text/html; CHARSET=iso-8859-2">
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/functions.js"></script>
		<link rel="stylesheet" type="text/css" media="all" href="/docusafe/main.css" />

<ds:additions test="tcLayout">
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.3.2.min.js"></script>
		<script type="text/javascript">
			var $j = jQuery.noConflict();
			
			function wysokosc()
			{
				$j(".NEWrightContent").css("height", "auto");
				if ($j(".NEWrightContent").height() < ($j(window).height() - 161)) $j(".NEWrightContent").height(($j(window).height() - 161));
				if ($j.browser.msie) $j(".NEWrightContent").height(($j(".NEWrightContent").height() + 10));
			}

			$j(document).ready(function ()
			{
				wysokosc();
			});
		</script>
</ds:additions>
</head>

<ds:additions test="tcLayout">
	<body onresize="wysokosc();">
		<div class="NEWmain">
			<!-- Pocz�tek nag��wka strony -->
			<div class="NEWtopBar">
				<div class="NEWlogoLeft">
					<a href="<c:out value='${pageContext.request.contextPath}'/>">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/logo_c.gif"/>
					</a>
				</div> <!-- NEWlogoLeft -->
				<div class="NEWlogoRight">
					<c:if test="${docusafeType != null}">
						<div class="NEWversion">
							Wersja
							<c:out value="${docusafeType}"/>
						</div>
					</c:if>
					<c:if test="${docusafeType == null}">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/dyskietka.gif"/>
					</c:if>
				</div> <!-- NEWlogoRight -->
			</div> <!-- NEWtopBar -->
			<!-- Koniec nag��wka strony -->
			
			<!-- Pocz�tek g�rnego menu -->
			<div class="NEWtopPanel">
			</div> <!-- NEWtopPanel -->
			<!-- Koniec g�rnego menu -->

			<!-- Pocz�tek zawarto�ci strony -->
			<div class="NEWcontent">			
				
				<!-- Pocz�tek okna z tre�ci� (right) -->
				<div class="NEWrightContent">
					<div class="NEWrightContentInner">
						<form action="<%= request.getRequestURI() %>" method="post">
							<div class="NEWpasswordRecovery">
								<ds:lang text="UserNameOrEmail"/>
								<input type="text" name="username" size="15" class="logintxt">
								<p></p>
								<input type="submit" value="<ds:lang text="SendPassword"/>" class="loginbtn">
								<p></p>
								<c:if test="${passwordSent}">
									<ds:lang text="PasswordWasSentOnYourEmail"/>
									<p></p>
								</c:if>	
								<a href="<%= request.getContextPath() %>"><ds:lang text="BackToLoginSite"/></a>
							</div>	
						</form>
					</div> <!-- NEWrightContentInner -->
				</div> <!-- NEWrightContent -->
				<!-- Koniec okna z tre�ci� (right) -->

			</div> <!-- NEWcontent -->
			<!-- Koniec zawarto�ci strony -->
						
			<!-- Pocz�tek dolnego menu -->
			<div class="NEWbottomPanel">
				<div class="NEWcopyrightNotice">
				<a href='<%= Configuration.getProperty("copyright.notice.url") %>'>
					<%= Configuration.getProperty("copyright.notice") %></a>
				<a class="NEWsecondCopyrightNotice" href='<%= Configuration.getProperty("copyright.notice.url.2") %>'>
					<%= Configuration.getProperty("copyright.notice.2") %></a>
				</div> <!-- NEWcopyrightNotice -->
			</div> <!-- NEWbottomPanel -->
			<!-- Koniec dolnego menu -->
			
			<!-- Pocz�tek stopki strony -->
			<div class="NEWbottomBar">
			</div> <!-- NEWbottomBar -->
			<!-- Koniec stopki strony -->
		</div> <!-- NEWmain -->
	</body>
</ds:additions>

<ds:additions test="!tcLayout">	
	<body leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">
		<table cellSpacing=0 cellPadding=0 width="100%" border=0  height="100%">
	
			<!-- Pocz�tek nag��wka strony -->
			<tr id="logo" height="64">
				<td  width="220">
					<a href="<c:out value='${pageContext.request.contextPath}'/>">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/logo_c.gif" border="none" />
					</a>
				</td>
				<td align="right">
					<c:if test="${docusafeType != null}">
						<font color="red" size="26" >
							Wersja
							<c:out value="${docusafeType}"/>
						</font>
					</c:if>
					<c:if test="${docusafeType == null}">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/dyskietka.gif" border="0"  height="64"/>
					</c:if>
				</td>
			</tr>
			<!-- Koniec nag��wka strony -->
			
			<!-- Pocz�tek g�rnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<td  colspan="2" bgcolor="black">
					<table id="bar" cellSpacing=0 cellPadding=0 width="640" border=0 >
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- Koniec g�rnego menu -->
			
			<!-- Pocz�tek g��wnego okna -->
			<tr>
				<td colspan="2">
					<table border="0" width="100%" height="100%">
						<tr class="mai">
							<!-- Pocz�tek menu (left) -->
							<%--	<td width="190" class="mai">
								<table class="mai" width="100%" border="0"  height="100%">
									<tr>
										<td>
											<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="15" height="1"/>
										</td>
										<td width="100%" class="mai">
											<tiles:insert name="menu"/>
										</td>
										<td>
											<img src="<c:out value='${pageContext.request.contextPath}'/>/img/bit.gif" height="100%" width="1"/>
										</td>
									</tr>
								</table>
							</td>
							<td>
								<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="10" height="1"/>
							</td>	--%>
							<!-- Koniec menu (left) -->
							
							<!-- Pocz�tek okna z tre�ci� (right) -->
							<td class="mai">
								<form action="<%= request.getRequestURI() %>" method="post">
								
									<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
										<tr height="25%">
											<td colspan="3" height="25%">
												&nbsp;
											</td>
										</tr>
										<tr height="50%">
											<td width="25%" height="50%">
											</td>
											<td valign="middle" height="50%">
												<table width="400" class="login">
													<tr>
														<td colspan="2" class="login">
															<%--<b>DocuSafe <%= Configuration.getVersionString() %></b>--%>
														</td>
													</tr>
													<tr>
														<td align="right" class="login">
															<ds:lang text="UserNameOrEmail"/>
														</td>
														<td>
															<input type="text" name="username" size="15" class="logintxt">
															<br><br>
														</td>
													</tr>
													<tr>
													<!--<td></td>-->
														<td colspan=2 align="center">
															<input type="submit" value="<ds:lang text="SendPassword"/>" class="loginbtn">
															<br><br>
														</td>
													</tr>
													<c:if test="${passwordSent}">
														<tr>
															<td colspan="2" class="login">
																<ds:lang text="PasswordWasSentOnYourEmail"/>
																<br><br>
															</td>
														</tr>
													</c:if>
													<c:if test="${userOnAD}">
														<tr>
															<td colspan="2" class="login">
																<ds:lang text="UzytkownikZnajdujeSieWActiveDirectory"/>
																<br><br>
															</td>
														</tr>
													</c:if>
													<c:if test="${adminPassword}">
														<tr>
															<td colspan="2" class="login">
																<ds:lang text="AdministratorPasswordReminderIsNotPossible"/>
																<br><br>
															</td>
														</tr>
													</c:if>
													<c:if test="${userOnADLink}">
														<tr>
															<td colspan="2" class="login"><ds:lang text="AbyOdzyskacHaslo" /> 
															<a target="_blank" href='<%=Docusafe.getAdditionProperty("link_odzyskiwania_hasla_typu_ad")%>'><ds:lang text="KliknijTutaj" /></a>
															<br><br><br></td>
														</tr>
													</c:if>
													<tr>
														<td colspan="2" class="login">
															<a href="<%= request.getContextPath() %>"><ds:lang text="BackToLoginSite"/></a>
														</td>
													</tr>
												</table>
											</td>
											<td width="25%" height="50%">
											</td>
										</tr>
										<tr height="25%">
											<td colspan="3" height="25%">
												&nbsp;
											</td>
										</tr>
									</table>
								</form>
							</td>
							<!-- Koniec okna z tre�ci� (right) -->
						</tr>
					</table>
				</td>
			</tr>
			<!-- Koniec g��wnego okna -->
			
			<!-- Pocz�tek dolnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<td  colspan="2" bgcolor="black">
					<table id="bar" cellSpacing=0 cellPadding=0 width="100%" border=0 >
						<tr id="bar">
							<td align="right">
								<a href='<%= Configuration.getProperty("copyright.notice.url") %>'>
									<%= Configuration.getProperty("copyright.notice") %></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr height="30">
				<td colspan="2" class="lbar" >
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="20" height="1"/>
					<%--	Ostatnie udane logowanie: <edm-sec:last-successful-login/>, ostatnie nieudane logowanie: <edm-sec:last-unsuccessful-login/>	--%>
				</td>
			</tr>
			<!-- Koniec dolnego menu -->
		</table>
	</body>
</ds:additions>
</html>

<!--N koniec password-recovery.jsp N-->