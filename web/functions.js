var VK_LEFT = 37;
var VK_RIGHT = 39;
var VK_DELETE = 46;
var VK_BACKSPACE = 8;
var VK_TAB = 9;

var KEY_DOWN = 40;
var KEY_UP = 38;
var KEY_ENTER = 13;
var KEY_ESCAPE = 27;

var selectedItem = 0;
var __tabIndexCount = 0;


/*
 * Date Format 1.2.3
 * (c) 2007-2009 Steven Levithan <stevenlevithan.com>
 * MIT license
 *
 * Includes enhancements by Scott Trenda <scott.trenda.net>
 * and Kris Kowal <cixar.com/~kris.kowal/>
 *
 * Accepts a date, a mask, or a date and a mask.
 * Returns a formatted version of the given date.
 * The date defaults to the current date/time.
 * The mask defaults to dateFormat.masks.default.
 */

var dateFormat = function () {
	var	token = /d{1,4}|m{1,5}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = "0" + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc, lang) {
		var dF = dateFormat;
		var i18lang = lang ? lang : 'en';

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == "[object String]" && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date;
		if (isNaN(date)) throw SyntaxError("invalid date");

		mask = String(dF.masks[mask] || mask || dF.masks["default"]);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == "UTC:") {
			mask = mask.slice(4);
			utc = true;
		}

		var	_ = utc ? "getUTC" : "get",
			d = date[_ + "Date"](),
			D = date[_ + "Day"](),
			m = date[_ + "Month"](),
			y = date[_ + "FullYear"](),
			H = date[_ + "Hours"](),
			M = date[_ + "Minutes"](),
			s = date[_ + "Seconds"](),
			L = date[_ + "Milliseconds"](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d:    d,
				dd:   pad(d),
				ddd:  dF.i18n[i18lang].dayNames[D],
				dddd: dF.i18n[i18lang].dayNames[D + 7],
				m:    m + 1,
				mm:   pad(m + 1),
				mmm:  dF.i18n[i18lang].monthNames[m],
				mmmm: dF.i18n[i18lang].monthNames[m + 12],
				mmmmm:dF.i18n[i18lang].monthNamesCase[m],
				yy:   String(y).slice(2),
				yyyy: y,
				h:    H % 12 || 12,
				hh:   pad(H % 12 || 12),
				H:    H,
				HH:   pad(H),
				M:    M,
				MM:   pad(M),
				s:    s,
				ss:   pad(s),
				l:    pad(L, 3),
				L:    pad(L > 99 ? Math.round(L / 10) : L),
				t:    H < 12 ? "a"  : "p",
				tt:   H < 12 ? "am" : "pm",
				T:    H < 12 ? "A"  : "P",
				TT:   H < 12 ? "AM" : "PM",
				Z:    utc ? "UTC" : (String(date).match(timezone) || [""]).pop().replace(timezoneClip, ""),
				o:    (o > 0 ? "-" : "+") + pad(Math.floor(Math.abs(o) / 60) * 100 + Math.abs(o) % 60, 4),
				S:    ["th", "st", "nd", "rd"][d % 10 > 3 ? 0 : (d % 100 - d % 10 != 10) * d % 10]
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
}();

// Some common format strings
dateFormat.masks = {
	"default":      "ddd mmm dd yyyy HH:MM:ss",
	shortDate:      "m/d/yy",
	mediumDate:     "mmm d, yyyy",
	longDate:       "mmmm d, yyyy",
	fullDate:       "dddd, mmmm d, yyyy",
	shortTime:      "h:MM TT",
	mediumTime:     "h:MM:ss TT",
	longTime:       "h:MM:ss TT Z",
	isoDate:        "yyyy-mm-dd",
	isoTime:        "HH:MM:ss",
	isoDateTime:    "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
	eventDate:		"dddd, d mmmmm yyyy"
};

// Internationalization strings
dateFormat.i18n = {};
dateFormat.i18n.en = {
	dayNames: [
		"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat",
		"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
	],
	monthNames: [
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
		"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
	]
};
dateFormat.i18n.en.monthNamesCase = dateFormat.i18n.en.monthNames.slice(12); 

dateFormat.i18n.pl = {
	dayNames: [
		"Nie", "Pon", "Wt", "�r", "Czw", "Pi�", "Sob",
		"Niedziela", "Poniedzia�ek", "Wtorek", "�roda", "Czwartek", "Pi�tek", "Sobota"
	],
	monthNames: [
		"Sty", "Lu", "Mar", "Kw", "Maj", "Cze", "Lip", "Sie", "Wrz", "Pa", "Lis", "Gru",
		"Stycze�", "Luty", "Marzec", "Kwiecie�", "Maj", "Czerwiec", "Lipiec", "Sierpie�", "Wrzesie�", "Pa�dziernik", "Listopad", "Grudzie�"
	],
	monthNamesCase: [
		"stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "wrze�nia", "pa�dziernika", "listopada", "grudnia"
	]
}

/**
 * Tworzy nowy obiekt Date w formacie "yyyy-mm-dd HH:MM:ss"
 * @param {String} str Napis z data i czasem w formacie "yyyy-mm-dd HH:MM:ss"
 */
function createIsoDate(str) {
	var d = str.split(' ')[0].split('-');
        var t = str.split(' ')[1].split(':');

    return new Date(d[0], d[1], d[2], t[0], t[1], t[2]);
}

// For convenience...
Date.prototype.format = function (mask, utc, lang) {
	return dateFormat(this, mask, utc, lang);
};

Date.prototype.toUnixStamp = function() {
	return Math.floor(this.getTime() / 1000)
};

Date.prototype.fromString = function(strDate, mask) {
    var token = /d{1,4}|m{1,5}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		flags = {
			d:    '(\\d)',
			dd:   '(\\d?\\d)',
			yyyy: '(\\d\\d\\d\\d)'
		};
		flags.m = flags.h = flags.H = flags.M = flags.s = flags.d;
		flags.mm = flags.yy = flags.hh = flags.HH = flags.ss = flags.MM = flags.dd;
	var funs = {
		d: 'date',
		m: 'month',
		y: 'year',
		yyyy: 'year',
		h: 'hour',
		M: 'minute',
		s: 'second'
	};
	funs.dd = funs.d;
	funs.MM = funs.M;
	funs.yy = funs.y;
	funs.hh = funs.HH = funs.h;
	funs.mm = funs.m;
	funs.ss = funs.s;
	var items = [];
    var newMask = mask.replace(token, function($0) {
		items.push($0);
		return flags[$0];
	});
	
	var requiredFields = {};
	var field;
	for(field in funs) {
		if(field.length == 1) {
			if(mask.indexOf(field) != -1) {
				requiredFields[field] = true;
			}
		}
	}
	
	
	var matched = (strDate.match(new RegExp(newMask)) || []).slice(1);
	if(matched.length != 0 && matched.length != items.length)
		throw "Invalid date";
		
	var d = new Date(0); 
	// 1 stycznia 1971 (na wszelki wypadek nie 1970)
	d.setFullYear(1971); d.setHours(0);
	
	var parsedDate = {};
	for(var i = 0; i < matched.length; i ++) {
		var value = Number(matched[i]);
		if(items[i].indexOf('m') != -1)
			value --;
		parsedDate[funs[items[i]]] = value;
	}
	
	function daysInFebruary (year){
		// February has 29 days in any year evenly divisible by four,
		// EXCEPT for centurial years which are not also divisible by 400.
		return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
	}
	
	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
			if (i==2) {this[i] = 29}
	   } 
	}
	
	var daysInMonth = new DaysArray(12);
	var year = parsedDate.year;
	if(requiredFields.y === true && (year == null || year <= 0 || year > 9999))
		throw "Invalid year";
	var month = parsedDate.month + 1;
	if(requiredFields.m === true && (month == null || month < 1 || month > 12))
		throw "Invalid month";
	var day = parsedDate.date;
	if(requiredFields.d === true && (day == null || day < 1 || day > 31 
		|| (month == 2 && day > daysInFebruary(year))
		|| day > daysInMonth[month]))
		throw "Invalid day";
	var hour = parsedDate.hour;
	if(requiredFields.h === true && (hour == null || hour < 0 || hour > 23))
		throw "Invalid hour";
	var minute = parsedDate.minute;
	if(requiredFields.M === true && (minute == null || minute < 0 || minute > 59))
		throw "Invalid minute";
	var second = parsedDate.second;
	if(requiredFields.s === true &&(second === null || second < 0 || second > 59))
		throw "Invalid second";
	
	for(field in requiredFields) {
		switch(field) {
			case 'd': d.setDate(day); break;
			case 'm': d.setMonth(month-1); break;
			case 'y': d.setFullYear(year); break;
			case 'h': d.setHours(hour); break;
			case 'M': d.setMinutes(minute); break;
			case 's': d.setSeconds(second); break;
		}
	}
	return d;	
};


/**
 * Por�wnuje dwie tablice. Tablice s� r�wne wtedy, gdy maj� jednakowe elementy.
 * Kolejno�� element�w nie jest brana pod uwag�.
 * 
 * @param soft
 *            Je�eli true, element o warto�ci '' (pusty napis) jest traktowany
 *            jak element o warto�ci null.
 */
function compareArrays(A, B, soft)
{
    for (var e in A)
    {
        if (A[e] != B[e] && !(soft && A[e] == '' && B[e] == null))
            return false;
    }

    for (var e in B)
    {
        if (A[e] != B[e] && !(soft && B[e] == '' && A[e] == null))
            return false;
    }

    return true;
}

/**
    Zwraca true, je�eli tablica nie ma element�w, lub ka�dy element
    tablicy jest r�wny null.
    @param soft Je�eli true, element o warto�ci '' (pusty
     napis) jest traktowany jak element o warto�ci null.
*/
function arrayEmpty(A, soft)
{
    for (var e in A)
    {
        if (A[e] != null && (!soft || A[e] != ''))
            return false;
    }

    return true;
}

function isWhitespace(c)
{
    return c != null && (c == ' ' || c == '\n' || c == '\r' || c == '\t');
}

String.prototype.trim = function()
{
    var string = this.substring(0, this.length);
    while (string.length > 0 && isWhitespace(string.charAt(0)))
    {
        string = string.substring(1);
    }
    while (string.length > 0 && isWhitespace(string.charAt(string.length-1)))
    {
        string = string.substring(0, string.length-1);
    }
    return string;
}

String.prototype.trimAll = function() {
	var regStart = new RegExp("^[\\s]*");
	var regEnd = new RegExp("[\\s]*$");
	return this.replace(regStart, '').replace(regEnd, '');
}

String.prototype.replaceAll = function(search, replacement)
{
    var string = this.substring(0, this.length);
    var p;
    while ((p = string.indexOf(search)) >= 0)
    {
        if (p > 0)
        {
            string = string.substring(0, p) + replacement + string.substring(p + search.length, string.length);
        }
        else
        {
            string = replacement + string.substring(search.length, string.length);
        }
    }
    return string;
}

RegExp.escape = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

function isEmpty(string)
{
    return string == null || string.length == 0 || string.trim().length == 0;
}

String.prototype.startsWith = function(string)
{
    if (string == null || string.length == 0 || string.length > this.length)
        return false;

    if (this.substring(0, string.length) == string)
        return true;

    return false;
}

String.prototype.endsWith = function(string)
{
    if (string == null || string.length == 0 || string.length > this.length)
        return false;

    if (this.substring(this.length-string.length, this.length) == string)
        return true;

    return false;
}

/*
    Walidator dla p�l tekstowych (typ text).
    Przyk�ad u�ycia w incoming-document-main.jsp
*/
function __Validator(element, pattern)
{
    this.element = element;
    // odniesienie do tej instancji zapisuj� w obserwowanym obiekcie
    // do odczytu przez funkcje obs�ugi wydarze�
    this.element.validator = this;
    this.pattern = pattern;

    this.validate = function(value)
    {
        if (this.canValidate && !this.canValidate())
        {
            this.element.style.color = this.normalColor;
            if (this.messageElement != null)
                this.messageElement.innerHTML = '';
            return true;
        }

        // je�eli nic nie wpisano - bez walidacji
        if (value == null || value.length == 0)
        {
            this.element.style.color = this.normalColor;
            if (this.messageElement != null)
                this.messageElement.innerHTML = '';
            return true;
        }

        // walidacja tylu znak�w ile wpisano w polu tekstowym
        for (var i=0; i < value.length && i < this.pattern.length; i++)
        {
            var v = value.charAt(i);
            var p = pattern.charAt(i);

            if (!this.validateCharacter(p, v))
            {
                this.element.style.color = this.errorColor;
                if (this.messageElement != null)
                    this.messageElement.innerHTML = this.syntaxErrorMessage;
                return false;
            }
        }

        // je�eli walidacja wpisanych znak�w przebieg�a pomy�lnie
        // nast�puje kontrola d�ugo�ci wpisanego tekstu
        if (value.length != this.pattern.length)
        {
            this.element.style.color = this.errorColor;
            if (this.messageElement != null)
                this.messageElement.innerHTML = this.lengthErrorMessage;
            return false;
        }

        this.element.style.color = this.normalColor;
        if (this.messageElement != null)
            this.messageElement.innerHTML = '';
        return true;
    };

    this.validateCharacter = function(patternChar, valueChar)
    {
        if (patternChar == '#')
        {
            if (valueChar < '0' || valueChar > '9')
                return false;
        }
        else
        {
            if (valueChar != patternChar)
                return false;
        }

        return true;
    };

    this.setSyntaxErrorMessage = function(msg) { this.syntaxErrorMessage = msg; return this; };
    this.setLengthErrorMessage = function(msg) { this.lengthErrorMessage = msg; return this; };
    this.setMessageElementId = function(id) { this.messageElement = document.getElementById(id); return this; };
    this.setErrorColor = function(color) { this.errorColor = color; return this; };
    this.setNormalColor = function(color) { this.normalColor = color; return this; };

    this.toString = function() { return 'Validator ['+this.pattern+']'; };

    this.revalidate = function() { this.validate(this.element.value); };

    this.element.onkeyup = function()
    {
        // this = obiekt input
        var validator = this.validator;
        validator.validate(this.value);
    };
}

/*
    Klasa s�u��ca do walidacji danych wprowadzanych do p�l tekstowych.
    U�ytkownik tej klasy powinien zaimplementowa� w niej metody:
    onOK() - Wywo�ywana, gdy warto�� w polu odpowiada spodziewanemu
        formatowi oraz gdy funkcja canValidate zwr�ci false
    onError(code) - Wywo�ywana, gdy wyst�pi b��d walidacji. Parametr
        code ma warto�� this.ERR_SYNTAX, gdy b��d polega na wprowadzeniu
        przynajmniej jednego niepoprawnego znaku oraz this.ERR_LENGTH,
        gdy wprowadzony tekst jest zbyt d�ugi lub zbyt kr�tki (ale nie
        gdy jest pusty)
    onEmpty - Wywo�ywana, gdy do pola tekstowego nie wprowadzono warto�ci.
    canValidate - Wywo�ywana zawsze na pocz�tku metody validate(). Je�eli
        zwr�ci false, walidacja nie jest przeprowadzana, wywo�ywana jest
        tylko metoda onOK, je�eli istnieje.

    U�ytkownik obiektu ma dost�p do nast�puj�cych zmiennych instancyjnych:
    element - Element INPUT, kt�rego zawarto�� ma by� walidowana
*/
function Validator(element, pattern)
{
    this.ERR_SYNTAX = 1;
    this.ERR_LENGTH = 2;
    this.element = element;
    // odniesienie do tej instancji zapisuj� w obserwowanym obiekcie
    // do odczytu przez funkcje obs�ugi wydarze�
    this.element.validator = this;
    this.pattern = pattern;

    // funkcja przeprowadzaj�ca walidacj�
    this.validate = function(value)
    {
        // je�eli walidacja nie mo�e by� przeprowadzona,
        // wywo�uj� onEmpty
        if (this.canValidate && !this.canValidate())
        {
            this.onOK && this.onOK();
            // if (this.onOK) this.onOK();
            return true;
        }

        // je�eli nic nie wpisano - bez walidacji
        if (value == null || value.length == 0)
        {
            this.onEmpty && this.onEmpty();
            return true;
        }

        // walidacja tylu znak�w ile wpisano w polu tekstowym
        for (var i=0; i < value.length && i < this.pattern.length; i++)
        {
            var v = value.charAt(i);
            var p = pattern.charAt(i);

            if (!this.validateCharacter(p, v))
            {
                this.onError && this.onError(this.ERR_SYNTAX);
                return false;
            }
        }

        // je�eli walidacja wpisanych znak�w przebieg�a pomy�lnie
        // nast�puje kontrola d�ugo�ci wpisanego tekstu
        if (value.length != this.pattern.length)
        {
            this.onError && this.onError(this.ERR_LENGTH);
            return false;
        }

        this.onOK && this.onOK();
        return true;
    };

    // funkcja waliduj�c pojedynczy znak
    this.validateCharacter = function(patternChar, valueChar)
    {
        if (patternChar == '#')
        {
            if (valueChar < '0' || valueChar > '9')
                return false;
        }
        else
        {
            if (valueChar != patternChar)
                return false;
        }

        return true;
    };

    this.toString = function() { return 'Validator ['+this.pattern+']'; };

    this.revalidate = function() { this.validate(this.element.value); };

    // funkcja wykonuje si� w kontek�cie obiektu input
    // walidacja b�dzie przeprowadzana po wprowadzeniu ka�dego znaku
    this.element.onkeyup = function()
    {
        // this = obiekt input
        var validator = this.validator;
        validator.validate(this.value);
        return true;
    };

    // funkcja wykonuje si� w kontek�cie obiektu input
    /*this.element.onkeypress = function(event)
    {
        // this = obiekt input
        var validator = this.validator;

        if (validator.canValidate && !validator.canValidate())
            return true;

        if (this.value && this.value.length >= validator.pattern.length &&
            event.keyCode != VK_BACKSPACE && event.keyCode != VK_DELETE &&
            event.keyCode != VK_LEFT && event.keyCode != VK_RIGHT &&
            event.keyCode != VK_TAB)
            return false;
        return true;
    }*/
}


function selectAllOptions(sel)
{
	traceStart('selectAllOptions ' + $j(sel).attr('id'));
    
	if (sel == null) {
		trace('sel = null');
		return;
	}
        
    for (var i=0; i < sel.options.length; i++)
    {
        sel.options[i].selected = true;
    }
	
	traceEnd();
    return true;
}

function deleteSelectedOptions(sel)
{
    for (var i=0; i < sel.options.length; i++)
    {
        if (sel.options[i].selected)
        {
            sel.options[i] = null;
            i--;
        }
    }
}

function addOption(sel, value, label)
{
    sel.options[sel.options.length] = new Option(label, value);
}

function setOption(sel, position, value, label)
{
    if (position > sel.options.length || position < 0)
        position = sel.options.length;
    sel.options[position] = new Option(label, value);
}

function getSelectedOption(sel)
{
    return sel.options[sel.options.selectedIndex];
}

/* zeruje ciasteczka viewservera */
function resetViewerCookie() {
	//('Setting area cookie to -1 ... ');
	$j.cookie('areaWidth', -1, { path: '/' });
	$j.cookie('areaHeight', -1, { path: '/' });
	$j.cookie('rotation', 0, { path: '/' });
	// alert('reset');
	// trace('Setting area cookie to -1 ... [OK]');
}

/**
 * Otwiera wyskakuj�ce okno.
 */
function openToolWindow(url, name)
{
	 try {
		 resetViewerCookie();
	 } catch(e) {}
	 
    var width = 700;
    var height = 550;
    if (arguments.length > 2)
        width = arguments[2];
    if (arguments.length > 3)
        height = arguments[3];
    window.open(url,  name, 'width='+width+',height='+height+',location=no,status=no,toolbar=no,menu=no,resizable=yes,scrollbars=yes');
}

/**
 * Otwiera wyskakuj�ce okno. Pozwala ustalic wysokosc i szerokosc.
 */
function openToolWindowX(url, name, width, height)
{
   /* if (arguments.length > 2)
        width = arguments[2];
    if (arguments.length > 3)
        height = arguments[3];*/
    window.open(url,  name, 'width='+width+',height='+height+',location=no,status=no,toolbar=no,menu=no,resizable=yes,scrollbars=yes');
}

function escapeString(s)
{
    if (s == null || s.length == 0) return '';
    var string = '';
    for (var i=0; i < s.length; i++)
    {
        var c = s.charAt(i);
        switch (c) {
            case '"': string += '\\"'; break;
            case '\'': string += '\\\''; break;
            default: string += c;
        }
    }
    return string;
}

/**
 * Na podstawie przekazanej tablicy asocjacyjnej funkcja tworzy
 * wyra�enie OGNL tworz�ce obiekt java.util.Map o tej samej zawarto�ci,
 * co przekazana tablica. Kluczami i warto�ciami tablicy b�d� obiekty
 * java.lang.String.
 */
function makeOgnlMap(map)
{
    var ognl = '#{ ';
    for (var key in map)
    {
        ognl += '"' + key + '": "' + escapeString(map[key]) + '",';
    }
    ognl = ognl.substring(0, ognl.length-1);
    ognl += ' }';

    return ognl;
}

function disableFormSubmits(form)
{
    var s = '';
    for (var i=0; i < form.elements.length; i++)
    {
        var el = form.elements[i];
        if (el.type != null && (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button'))
            el.disabled = true;
    }
    return true;
}

function setEvent(button)
{
    document.getElementById(button.name).value = 'true';
}

function Tokenizer(str)
{
    if (str == null || str.length < 3) return; // przynajmniej #{}
    this.NONE = 0;
    this.STRING = 1;
    this.NUMBER = 2;
    this.CHAR = 3;
    this.UNKNOWN = 10;

    this.str = str.trim();
    this.pos = 0;

    this.findToken = Tokenizer__findToken;
    this.getToken = Tokenizer__getToken;
    this.getTokenType = Tokenizer__getTokenType;
    this.next = Tokenizer__next;
}

function Tokenizer__findToken()
{
    // pomijanie spacji
    var c = this.str.charAt(this.pos++);
    while (isWhitespace(c) && this.pos < this.str.length)
    {
        c = this.str.charAt(this.pos++);
    }

    if (this.pos > this.str.length)
    {
        this.token = null;
        this.tokenType = 0;
        return;
    }

    if (c == ',' || c == ':' || c == '{' || c == '}' || c == '#')
    {
        this.token = c;
        this.tokenType = this.CHAR;
    }
    else if (c >= '0' && c <= '9')
    {
        var number = c;
        while (this.pos < this.str.length && ((c >= '0' && c <= '9') || c == '.'))
        {
            c = this.str.charAt(this.pos++);
            number += c;
        }
        this.token = number.indexOf('.') >= 0 ? parseFloat(number) : parseInt(number);
        this.tokenType = this.NUMBER;
    }
    else if (c == '"')
    {
        var string = '';
        var lastc = '';

        do
        {
            lastc = c;
            c = this.str.charAt(this.pos++);
            if (c == '"')
            {
                if (lastc != '\\')
                {
                    break;
                }
                // ucinam backslash z ko�ca napisu
                string = string.substring(0, string.length-1);
            }
            string += c;
        }
        while (this.pos < this.str.length);

        this.token = string;
        this.tokenType = this.STRING;
    }
    else
    {
        this.token = c;
        this.tokenType = this.UNKNOWN;
    }
}

function Tokenizer__next()
{
    this.findToken();
    return this.tokenType != this.NONE;
}

function Tokenizer__getToken()
{
    return this.token;
}

function Tokenizer__getTokenType()
{
    return this.tokenType;
}

function parseOgnlMap(str)
{
    var EMPTY_ARRAY = new Array();

    var tokens = '';
    var t = new Tokenizer(str);

    if (!(t.next() && t.getToken() == '#' && t.next() && t.getToken() == '{'))
        return EMPTY_ARRAY;

    var map = new Array();

    while (t.next())
    {
        var key = t.getToken();
        if (key == '}' && t.getTokenType() == Tokenizer.CHAR)
            break;
        if (!t.next()) break; // dwukropek
        if (!t.next()) break; // warto��
        var value = t.getToken();
        t.next(); // przecinek
        map[key] = value;
    }

    return map;
}


function getRadioValue(name)
{
    var kinds = document.getElementsByName(name);
    for (var i=0; i < kinds.length; i++)
    {
        if (kinds[i].checked)
        {
            return kinds[i].value;
        }
    }
    return null;
}

/**
    @param divId Identyfikator elementu, kt�ry jest ukrywany/pokazywany.
    @param aId Identyfikator odno�nika (A), kt�rego opis nale�y zmienia�.
    @param showTxt Domy�lnie 'poka�'.
    @param hideTxt Domy�lnie 'ukryj'.
*/ 
function toggleVisibility(divId, aId, showTxt, hideTxt)
{
    var div = document.getElementById(divId);
    var a = document.getElementById(aId);
    if (div.style.visibility == 'visible')
    {
        div.style.visibility = 'hidden';
        a.innerHTML = showTxt != null ? showTxt : 'poka�';
    }
    else
    {
        div.style.visibility = 'visible';
        a.innerHTML = hideTxt != null ? hideTxt : 'ukryj';
    }
}


/* kody kreskowe */

/*
 * Klasa BarcodeInput. Na jednej stronie mo�e si� znale�� tylko jedna instancja
 * tej klasy.
 */
function BarcodeInput(conf)
{
    // pola prywatne
    var inputId = conf['inputId'];
    var prefix = conf['prefix'];
    var suffix = conf['suffix'];
    var submitId = conf['submitId'];
    var submit = conf['submit'];
    var tabStarts = conf['tabStarts'];

    var saveText = null;
    var saveTextfield = null;

    var WAIT_TAB = 1; // oczekiwanie na tabulator - niepotrzebne?
    var AFTER_TAB = 2; // pierwszy znak po tabulatorze
    var PREFIX_WAIT = 3; // oczekiwanie na prefiks
    var ACC = 4;        // akumulacja kodu kreskowego

    var LENGTH = 30;

    var state = tabStarts ? WAIT_TAB : PREFIX_WAIT;

    if (window.acc != null)
    {
        // throw?
        alert('Na stronie znajduje si� ju� jedna instancja BarcodeInput');
        return;
    }

    // zmienna globalna - na stronie mo�e by� tylko jeden obiekt BarcodeInput
    window.acc = '';

    var element = document.getElementById(inputId);
    if (!element)
        alert('Nie istnieje element o identyfikatorze '+inputId);

    if (!prefix || prefix.length == 0)
        alert('Nie podano prefiksu dla kodu kreskowego');



    function debug(s) {
        var d = document.getElementById('debug');
        if (d && (d.type == 'text' || d.type == 'textarea'))
            d.value += (s + '\n');
    }

    this.onkeypress = function (event)
    {
        if (!event) event = window.event; // ie
        var key = event.which || event.keyCode;
        var src = event.srcElement ? event.srcElement : event.target;
        if (window.acc.length > LENGTH) window.acc = window.acc.substring(1);
        window.acc += String.fromCharCode(key);

        if (key == 9 && state == WAIT_TAB)
        {
            state = AFTER_TAB;
            debug('state=AFTER_TAB');
            window.acc = '';
        }
        else if (state == AFTER_TAB)
        {
            // akumulowanie kolejnego znaku
            // if (window.acc.length > LENGTH) window.acc =
			// window.acc.substring(1);
            // window.acc += String.fromCharCode(key);

            // je�eli poprzednim znakiem by� tabulator, kursor jest teraz
            // w polu tekstowym i wprowadzenie do niego kolejnego znaku
            // spowoduje usuni�cie bie��cej zawarto�ci - trzeba j� zapami�ta�
            // na wypadek, gdyby teraz nast�pi� prefiks kodu kreskowego
            if (src && (src.type == 'text' || src.type == 'textarea'))
            {
                saveText = src.value;
                saveTextfield = src;
            }
            else
            {
                saveTextfield = null;
            }

            state = PREFIX_WAIT;
            debug('state=PREFIX_WAIT');
        }
        else if (state == PREFIX_WAIT)
        {
            //if (window.acc.length > LENGTH) window.acc = window.acc.substring(1);
            // window.acc += String.fromCharCode(key);

            // akumulowanie kolejnego znaku
            debug('state=PREFIX_WAIT, window.acc='+window.acc);

            if (!tabStarts &&
                String.fromCharCode(key) == prefix.charAt(0) &&
                (src.type == 'text' || src.type == 'textarea'))
            {
                saveText = src.value;
                saveTextfield = src;
            }

            // znaleziono prefiks kodu kreskowego
            if ((tabStarts && window.acc.indexOf(prefix) == 0) ||
                (!tabStarts && window.acc.indexOf(prefix) >= 0))
            {
                state = ACC;
                debug('state=ACC, window.acc='+window.acc);
                window.acc = '';

                // odtworzenie warto�ci pola, w kt�rym po tabulatorze
                // wpisywane by�y znaki prefiksu
                if (saveTextfield != null)
                {
                    saveTextfield.value = saveText;
                }

                // przeniesienie kursora do pola przeznaczonego na kod kreskowy
                try
                {
                    document.getElementById(inputId).value = tabStarts ? String.fromCharCode(key) : '';
                    document.getElementById(inputId).focus();
                }
                catch (e)
                {
                    debug('exception '+e);
                }

                return false;
            }
        }
        else if (state == ACC)
        {
            //if (window.acc.length > LENGTH) window.acc = window.acc.substring(1);
            // window.acc += String.fromCharCode(key);

            if (window.acc.endsWith(suffix))
            {
                //alert('barcode='+src.value);
                state = tabStarts ? TAB_WAIT : PREFIX_WAIT;
                if (submit && document.getElementById(submitId))
                    document.getElementById(submitId).click();
                return false;
            }
        }

        return true;
    }

    document.onkeypress = this.onkeypress;
}

function appletObject(id, classname, archive, width, height, params, req)
{
    var nav = navigator.appName;
    var tag = '';
    var MSIE = nav.indexOf('MSIE') >= 0 || nav.indexOf('Internet Explorer') >= 0;
    var reqMSIE = false;
    if (req != null)
    {
        for (var i=0; i < req.length; i++)
        {
            if (req[i] == 'MSIE') reqMSIE = true;
        }
    }

    if (reqMSIE && !MSIE)
    {
        return "<p>Do obs�ugi applet�w wymagana jest przegl�darka Internet Explorer</p>";
    }

    tag += ('<OBJECT ID="'+id+'" WIDTH="'+width+'" HEIGHT="'+height+'" ');
    if (MSIE)
    {
        tag += (' CLASSID="CLSID:8AD9C840-044E-11d1-B3E9-00805F499D93">');
    }
    else
    {
        tag += (' CLASSID="java:'+classname+'">');
    }
    tag += ('<PARAM NAME="code" value="'+classname+'">');
    tag += ('<PARAM NAME="archive" value="'+archive+'">');

    if (params)
    {
        for (var key in params)
        {
            tag += ('<PARAM name="'+key+'" value="'+params[key]+'">');
        }
    }

    tag += ('</OBJECT>');
    return tag;
}

/**
    Zaznacza, odznacza lub zmienia stan zaznaczenia wszystkich element�w
    tagu SELECT.
    @param yesnoxor true - zaznacza wszystkie, false - odznacza wszystkie,
        brak warto�ci - zmienia stan zaznaczenia.
*/
function select(sel, yesnoxor)
{
    if (!sel || !sel.options)
        return;

    if (yesnoxor != null)
    {
        for (var i=0; i < sel.options.length; i++)
        {
            sel.options[i].selected = yesnoxor;
        }
    }
    else
    {
        for (var i=0; i < sel.options.length; i++)
        {
            sel.options[i].selected = !sel.options[i].selected;
        }
    }
}

/**
    Zwraca pierwszy znaleziony element o danej nazwie lub null.
*/
function getElementByName(name)
{
    var list = document.getElementsByName(name);
    if (list && list.length && list.length > 0)
        return list.item(0);
    return null;
}

function SetCookie (name, value) {
	var expires = new Date();
	expires.setTime( expires.getTime() + (24 * 60 * 60 * 1000) * 180 ); // 6
																		// months
																		// from
																		// now

	document.cookie = name + "=" + escape (value) + "; expires=" + expires.toGMTString();
}

function GetCookie (name) {
  var arg = name + "=";
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while (i < clen) {
    var j = i + alen;
    if (document.cookie.substring(i, j) == arg)
      return getCookieVal (j);
    i = document.cookie.indexOf(" ", i) + 1;
    if (i == 0) break;
  }
  return null;
}

function getCookieVal (offset) {
	var endstr = document.cookie.indexOf (";", offset);
	if (endstr == -1)
    	endstr = document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

function validateDecimal(s)
{
    if (isEmpty(s))
        return false;

    s = s.trim();

    for (var i=0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (! ((c >= '0' && c <= '9') || c == '.'))
            return false;
    }

    if (isNaN(parseFloat(s)))
        return false;

    return true;
}

function validateDate(s)
{
    return true;
}

/**
    Funkcja powinna by� wywo?ywana w momencie klikni�cia przycisku wysy?aj?cego
    formularz.  Funkcja wy??cza (disabled=true) wszystkie przyciski w formularzu,
    a nast�pnie tworzy element ukryty (INPUT HIDDEN) o takiej samej nazwie, jak
    klikni�ty przycisk i o warto�ci 'true'.
    Je?eli elementem wysy?aj?cym wydarzenie jest przycisk typu BUTTON, wywo?ywana
    jest metoda submit() formularza, w kt�rym umieszczony jest przycisk.
*/
function sendEvent(btn)
{
    var form = btn.form;

    var hid = document.createElement('INPUT');
    hid.type = 'HIDDEN';
    hid.name = btn.name;
    hid.value = 'true';
    form.appendChild(hid);

    for (var i=0; i < form.elements.length; i++)
    {
        var el = form.elements[i];
        if (el.type != null && (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button'))
        {
            el.disabled = true;
        }
    }

    form.submit();

    return true;
}
    
    /**
    Funkcja powinna by� wywo?ywana w momencie klikni�cia przycisku wysy?aj?cego
    formularz.  Funkcja wy??cza (disabled=true) wszystkie przyciski w formularzu,
*/
function disabledAllBtn(btn)
{
    var form = btn.form;
    for (var i=0; i < form.elements.length; i++)
    {
        var el = form.elements[i];
        if (el.type!= null && (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button'))
        {
            el.disabled = true;
        }
    }
    return true;
}

/* Submituje pierwszy formularz na stronie i dodaje do niego input o przekazanej nazwie i warto�ci true
 *
 **/
function submitFormEvent(actionName){
	$j("form").append("<input type='hidden' id='"+actionName+"' name='"+actionName+"' value='true'/>").get(0).submit();
}


function attachmentRevisionViewer(contextPath, id)
{
    var width = arguments.length > 1 ? arguments[1] : 640;
    var height = arguments.length > 2 ? arguments[2] : 480;
    window.open()
}
function addEventHandler(target, eventName, handler)
{
    if (target.addEventListener)
    {
        target.addEventListener(eventName, handler, false);
    }
    else if (target.attachEvent)
    {
        target.attachEvent("on" + eventName, handler);
    }
    else
    {
        target["on" + eventName] = handler;
    }
}

/**
 * Sprawdza czy pole formularza podane w parametrach jest puste.
 */
function emptyField(field)
{
    if (field.tagName.toUpperCase() == 'INPUT')
    {
        return ((field.value == null) || (field.value.trim().length == 0));
    }
    else if (field.tagName.toUpperCase() == 'SELECT')
    {
        if (field.multiple)
            return (field.options.length == 0);
        else
            return (field.options.selectedIndex == 0 && (field.value == null || field.value == ''));
    }
    else if (field.tagName.toUpperCase() == 'TEXTAREA')
    {
        return ((field.value == null) || (field.value.trim().length == 0));
    }
    return true;
}

function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}

//function checkDate(beforeId, afterId, changedArg) {
//	checkDate(beforeId, afterId, changedArg, false);
// }

/**
 * funkcja sprawdza daty wpisane w pola o podanych identyfikatorach sa w
 * porzadku chronologicznym
 */
function checkDate(beforeId, afterId, changedArg) {//, checkCurrentDay) {
	var checkCurrentDay = false;
	var _date = new Date();	
	var before = document.getElementById(beforeId).value;
	var after = document.getElementById(afterId).value;

	if (before == null || before == "" || after == null || after == "") {
		return;
	}
	var tmp = before.split("-");
	var beforeDate = new Date(parseInt(tmp[2],10), (parseInt(tmp[1],10) - 1), parseInt(tmp[0],10));
	tmp = after.split("-");
	var afterDate = new Date(parseInt(tmp[2],10), (parseInt(tmp[1],10) - 1), parseInt(tmp[0],10));
	if (checkCurrentDay == true && beforeDate > _date) {
		alert("Data 'od' nie mo�e by� p�niejsza od bie�acej");
		document.getElementById(beforeId).value = "";
		before.value = "";
		return;
	}
	if (checkCurrentDay == true && afterDate > _date) {
		alert("Data 'do' nie mo�e by� p�niejsza od bie�acej");
		document.getElementById(afterId).value = "";
		after.value = "";
		return;
	}
	if (afterDate < beforeDate) {
		if (changedArg == 1) {
			alert("Data 'od' jest p�zniejsza ni� data 'do'");
			document.getElementById(beforeId).value = "";
			return;
		} else {
			alert("Data 'do' jest wczeniejsza ni� data 'od'");
			document.getElementById(afterId).value = "";
			after.value = "";
			return;
		}	
	}
}

/**
 * funkcja sprwadza czy obecna data jest wcze�niejsza od obecnej
 */
function checkIfSelectedIsBeforeToday(beforeId) {
	var checkCurrentDay = true;
	var _date = new Date();
	var before = document.getElementById(beforeId).value;

	if (before == null || before == "") {
		return;
	}
	var tmp = before.split("-");
	var beforeDate = new Date(parseInt(tmp[2],10), (parseInt(tmp[1],10) - 1), parseInt(tmp[0],10));

	if (checkCurrentDay == true && beforeDate < _date) {
		alert("Wybrana data jest wcze�niejsza od bie��cej");
		document.getElementById(beforeId).value = "";
		before.value = "";
		return;
	}
}

/**
 * Sprawdza czy kwota -OD- nie jest wieksza od kwoty -DO-
 * @param num1 kwota -OD-
 * @param num2 kwota -DO-
 */
function compareNumbers(num1, num2)
{
	var _num1 = document.getElementById(num1).value;
	var _num2 = document.getElementById(num2).value;
	if (_num1 == null || _num1 == "" || _num2 == null || _num2 == "") {
		return;
	}
	_num1.replace(',', '.');
	_num2.replace(',', '.');
	if (!IsNumeric(_num1) || !IsNumeric(_num2)) {
		alert("Warto�� musi by� liczb�");
		return;
	}
	if (parseFloat(_num1) > parseFloat(_num2)){
		document.getElementById(num2).value = "";
		_num2.value="";
		alert("Warto�� 'od' nie mo�e by� wi�ksza od liczby 'do'");
	}
}

var polskieZnaki = new Array();
var zamiana = new Array();

{
	polskieZnaki[0] = "�";
	polskieZnaki[1] = "�";
	polskieZnaki[2] = "�";
	polskieZnaki[3] = "�";
	polskieZnaki[4] = "�";
	polskieZnaki[5] = "�";
	polskieZnaki[6] = "�";
	polskieZnaki[7] = "�";
	polskieZnaki[8] = "�";

	polskieZnaki[9] = "�";
	polskieZnaki[10] = "�";
	polskieZnaki[11] = "�";
	polskieZnaki[12] = "�";
	polskieZnaki[13] = "�";
	polskieZnaki[14] = "�";
	polskieZnaki[15] = "�";
	polskieZnaki[16] = "�";
	polskieZnaki[17] = "�";

	zamiana[0] = "^a";
	zamiana[1] = "^e";
	zamiana[2] = "^c";
	zamiana[3] = "^l";
	zamiana[4] = "^o";
	zamiana[5] = "^n";
	zamiana[6] = "^s";
	zamiana[7] = "^x";
	zamiana[8] = "^z";

	zamiana[9] = "^A";
	zamiana[10] = "^E";
	zamiana[11] = "^C";
	zamiana[12] = "^L";
	zamiana[13] = "^O";
	zamiana[14] = "^N";
	zamiana[15] = "^S";
	zamiana[16] = "^X";
	zamiana[17] = "^Z";
}


function showCode(zipId, locationId) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var location = locationId.value;
	for (i = 0; i < 18; i++) {
		while (location.indexOf(polskieZnaki[i]) > -1) {
			pos = location.indexOf(polskieZnaki[i]);
			location = ""
					+ (location.substring(0, pos) + zamiana[i] + location
							.substring((pos + polskieZnaki[i].length),
									location.length));
		}
	}
	if (location == null) {
		alert("No element with id 'location' found on the page");
		return;
	}
	xmlHttp.open("GET", getWindowPath() + '/code.ajx?location=' + location, true);
	xmlHttp.onreadystatechange = function() {
		stateChanged(zipId, null, xmlHttp);
	};
	xmlHttp.send(null);
}

function showLocation(zipId, locationId) {
	xmlHttp = GetXmlHttpObject();
	if (xmlHttp == null) {
		alert("Browser does not support HTTP Request");
		return;
	}
	var zip = zipId.value;
	if (zip == null) {
		alert("No element with id 'zip' found on the page");
		return;
	}
	xmlHttp.open("GET", getWindowPath() + '/location.ajx?zip=' + zip, true);
	xmlHttp.onreadystatechange = function() {
		stateChanged(null, locationId, xmlHttp);
	};
	xmlHttp.send(null);
}

function stateChanged(zipId, locationId, xmlHttp) {
	if (xmlHttp == null) {
		return;
	}	
	var element = null;
	var variable = null;
	
	if (zipId == null) {
		variable = 'location';
		element = locationId;
	} else {
		variable = 'zip';
		element = zipId;
	}	
	if (xmlHttp.readyState == 4) {
		if (xmlHttp.status == 200) {
			var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];

			if (valid.childNodes[0].nodeValue == 'LocNotFound'
					&& variable == 'zip') {
				alert("Nie znalaz�em kodu dla tej miejscowo�ci.");
			} else {
				var message = valid.childNodes[0].nodeValue;
				for (i = 0; i < 18; i++) {
					while (message.indexOf(zamiana[i]) > -1) {
						pos = message.indexOf(zamiana[i]);
						message = ""
								+ (message.substring(0, pos) + polskieZnaki[i] + message
										.substring((pos + zamiana[i].length),
												message.length));
					}
				}
			}
			if (valid.childNodes[0].nodeValue == 'CodeNotFound'
					&& variable == 'location') {
				alert("Nie znalaz�em miejscowo�ci dla danego kodu.");
				element.value = '';
			}
			if (message.trim().length == 6) {
				element.value = message;
			} else if (message.trim().length > 6 && variable == 'zip') {
				alert("Nie ma jednego kodu pocztowego dla danej miejscowo�ci. Zakres(y) to: "
						+ message + ".");
				element.value = message.substring(0, 6);
			} else if (variable == 'location'
					&& valid.childNodes[0].nodeValue != 'CodeNotFound') {
				element.value = message;
			}
		} else {
			alert("Error loading page" + xmlHttp.status + ":"
					+ xmlHttp.statusText);
		}
	} else if (xmlHttp.readyState == 0) {
		alert("	Object is uninitialized");
	}
}

function GetXmlHttpObject() {
	var xmlHttp = null;
	try {
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function getWindowPath() {
	var w = window.location;
	return w.protocol+'//' + w.host + '/' + w.pathname.split('/')[1];
}

/* obs�uga konsoli dla firefoxa */
function traceStart(str) {
	try {
		console.groupCollapsed(str + ' start at ' + (new Date().toLocaleTimeString()));
	} 
	catch (e) {
	}
}

function trace(str) {
    try {
        console.log(str);
    } 
    catch (e) {
    
    }
}

function traceInfo(str) {
	try {
		console.info(str);
	} catch(e) {}
}

function traceEnd() {
    try {
    	console.log(' end at ' + (new Date().toLocaleTimeString()));
    	console.groupEnd();
    } 
    catch (e) {
    
    }
}

function error(str) {
	try {
		console.error(str);
	} catch(e) {}
}

/* pomiar czasu */
var __timeElapsed = 0;
function timeStart() {
	var d = new Date();
	__timeElapsed = d.getTime();
}

function timeEnd(id) {
	var d = new Date();
	//__timeElapsed -= d.getTime();
	var t = d.getTime() - __timeElapsed
	
	// document.getElementById(id).innerHTML = __timeElapsed;
	window.status = 'TIME: ' + t;
}

function getPos (obj) {
		var output = new Object();
		var mytop=0, myleft=0;
		while( obj) {
			mytop+= obj.offsetTop;
			myleft+= obj.offsetLeft;
			obj= obj.offsetParent;
		}
		output.left = myleft;
		output.top = mytop;
		return output;
	}
	
function highlightItem(id) {
		/* usuwamy stare pod�wietlenie */
		$j('#prompt_item_' + selectedItem).removeClass('selected');

		$j('#' + id).addClass('selected');

		/* wyciagamy numer */
		selectedItem = parseInt((id).substr(12));
	}

	function unHighlightItem(id) {
		$j('#' + id).removeClass('selected');

		/* podswietlenie aktualnego zostaje */
		$j('#prompt_item_' + selectedItem).addClass('selected');
	}

/*
		wyswietla podpowiedz:
			senders - mapa jsonowa
			id - id inputa
			hiddenItems - tych elementow z senders nie wyswietla
	*/	
	function displayPrompt(senders, id, hiddenItems) {
		var pd = $j('#promptDiv');
		var ln = $j('#' + id);
		var str = '';
		
		try {
			console.log('displayPrompt!');
			console.log('pressedKey: ' + pressedKey);
			console.log('pd: ' + pd.attr('id') + ' ln: ' + ln.attr('id'));
		} catch(e) {}
			switch(pressedKey) {

			/* Obs�uga klawiszy */
			case KEY_DOWN: {
				if(selectedItem < itemsLength - 1) {
					var nextItem = selectedItem + 1;
					highlightItem('prompt_item_' + nextItem);
					
					/* przewijanie */
					if(pd.css('overflow-y') == 'auto') {
						var jNext = $j('#prompt_item_' + nextItem);
						
					
						var selItemTop = jNext.attr('offsetTop');
						var selItemHeight = jNext.attr('offsetHeight');
						var divHeight = pd.height();
						
						if(selItemTop + 25 > divHeight) {
							trace('We are scrolling down ... ');
							//trace('Before: ' + $j('#gmenu_scroll').scrollTop());
							
							var newScroll = selItemHeight * (selectedItem - 8);
							if(newScroll < 0) newScroll = 0;
							pd.scrollTop(newScroll);
							
							//trace('After: ' + $j('#gmenu_scroll').scrollTop());
						}
					}
				}
			}
			break;

			case KEY_UP: {
				if(selectedItem > 0) {
					var prevItem = selectedItem - 1;
					highlightItem('prompt_item_' + prevItem);
					
					/* przewijanie */
					if(pd.css('overflow-y') == 'auto') {
						var jPrev = $j('#prompt_item_' + prevItem);
						
					
						var selItemTop = jPrev.attr('offsetTop');
						var selItemHeight = jPrev.attr('offsetHeight');
						var divHeight = pd.height();
						
						if(selItemTop + 50 > divHeight) {
							trace('We are scrolling down ... ');
							//trace('Before: ' + $j('#gmenu_scroll').scrollTop());
							
							var newScroll = selItemHeight * (selectedItem - 8);
							if(newScroll < 0) newScroll = 0;
							pd.scrollTop(newScroll);
							
							//trace('After: ' + $j('#gmenu_scroll').scrollTop());
						}
					}
				}
			}
			break;

			case KEY_ENTER:
				pd.css('visibility', 'hidden').css('display', 'none');
			break;

			default:
		//	console.log('We are going to default option');
			var i = 0;

			pd.css('visibility', 'visible');
			
			pd.css('left', getPos(ln.get(0)).left  + 'px');
			pd.css('top', getPos(ln.get(0)).top + ln.height() + 'px');
			

			str = '<table class="prompt"><tbody>';
			
			
			try {
				console.log('items go ... ');
			} catch (e) {
				
			}


			for(sender in senders){
				//var c = 0;

				sender = senders[sender];

				try {
						console.log('We are in item: ' + sender.toString());
					} catch (e) {
						
					}

				str += '<tr id="prompt_item_' + i + '" onclick="putItem()">';
				for(property in sender){
					var txt = sender[property].replace(/&quot;/g, '"');
					var hideStr = '';
					
					try {
						console.log('Preparing item: ' + txt);
					} catch (e) {
						
					}

					/* pomijamy niektore pola */
					hideStr = '';
					for(j = 0; j < hiddenItems.length; j ++) {
						if(hiddenItems[j] == property) {
							hideStr = ' style="display:none;" ';
							break;
						}
					}


					/* szukamy w podanym id wpisanego tekstu */
					// if(c == 0) {
					if(property == id) {
						var pattern = ln.attr('value').trim();
						var pat = new RegExp(pattern, 'i');
						// znaleziony string
						var fs = txt.match(pat);
						if(fs != null) {
							var boldPattern = '<b class="found">' + fs + '</b>';
							txt = txt.replace(fs, boldPattern);
						}
					}

					str += '<td class="' + property + '" ' + hideStr + '>' + txt + '</td>';
					// c ++;
					//alert(str);
				}
				str += '</tr>';
				i ++;
			}
			str += '</tbody></table>';
			itemsLength = i;
			
			/* gdy nie znaleziono wynik�w */
			if(itemsLength == 0) {
				str += '<span class="promptNoResult">Nie znaleziono</span>';
			}
			
			pd.html(str);

			/* poprawa dla ie6, bo selecty zakrywaja diva */
			if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
				pd.append('<iframe class="hideIE" id="iframeHideIE" style="width: ' + (pd.get(0).offsetWidth+2) + 'px; height: ' +
					(pd.get(0).offsetHeight+2) + 'px; "></iframe>');
			}
			
			

			/* domyslnie wybieramy pierwsza pozycje */
			selectedItem = 0;
			$j('#prompt_item_' + selectedItem).addClass('selected'); 

			/* dodajemy onmouseover i out */
			$j('.prompt tr').hover(function() {
					highlightItem($j(this).attr('id'));
				}, function() {
					unHighlightItem($j(this).attr('id'));
			});
			}

			/* czy jest scrollbar ? */
			if(pd[0].scrollHeight > 200)
				pd.css('padding-right', '15px');
			else 
				pd.css('padding-right', '0px');
		return false;
		}

		
		
/*
 * wyrownuje pola
 * 
 */

function fixMultiItemWidth(domItem) {
	//traceStart('fixMultiItemWidth');
	
	var len = domItem.offsetWidth;
	trace(len);
		
	if (!ie6 && !ie7) {
		if (len <= 100) 
			domItem.style.width = '104px';
		else 
			if (len <= 200 && len > 100) 
				domItem.style.width = '204px';
			else 
				domItem.style.width = '304px';
	}
		
	//traceEnd();	
}

function fixItemsWidth() {
	//traceStart('fixItemsWidth');
	var maxLength = 0;
	
	if(!__dontFixItemsWidth) { 
	
	var jqItems = $j('input:not(.btn):not(:image):not(.dontFixWidth):not(:hidden):visible, textarea:not(.dontFixWidth)');
	
	//trace('inputs:');
	jqItems.each(function() {
		var len = this.offsetWidth;
			
		if(len <= 102)
			this.style.width = '100px';
		else if(len <= 202 && len > 102 )
			this.style.width = '200px';
		else
			this.style.width = '300px';		
			
		if(this.offsetWidth > maxLength)
			maxLength = len;
		
		//trace(this.id + ', ' + this.name + ' oldWidth: ' + len + ', newWidth: ' + this.style.width);
	});
	
	//trace('selects');
	jqItems = $j('select:not(.dontFixWidth):visible');
	jqItems.each(function() {
		var len = this.offsetWidth;
		
		if(len <= 104)
			this.style.width = '104px';
		else if(len <= 204 && len > 104 )
			this.style.width = '204px';
		else
			this.style.width = '304px';	
			
		if(this.offsetWidth > maxLength)
			maxLength = len;
			
		//trace(this.id + ', ' + this.name + ' oldWidth: ' + len + ', newWidth: ' + this.style.width);		
	});
	
	if (__alignInnerTables) {
		//trace('alignInnerTables');
		jqItems = $j('table.leftAlign').each(function(){
			var lenTd = $j(this).children('tbody.leftAlignInner').children('tr').children('td')[0].offsetWidth;
			//alert(lenTd);
			$j(this).css('position', 'relative').css('left', '-' + (lenTd + 2) + 'px').css('margin-right', '-' + (lenTd + 2) + 'px').css('margin-left', '-3px');
			// $j(this).filter(':not(.oneLine)').css('margin-top',
			// '10px').css('margin-bottom', '10px');
			
			try {
				$j(this).children('tbody.leftAlignInner').children('tr').children('td.alignLeftBtns')[0].style.paddingLeft = lenTd + 4 + 'px';
			} 
			catch (e) {
			}
			
		});
	}
	
	/* ustalamy jednakow� szeroko�� pierwszych kolumn "powi�zanych" tabel (pierwsza kom�rka w formTableMain nie mo�e by� pusta!) */
	var mainWidth = $j('table.formTableMain td:first').width();
	//trace('mainWidth: ' + mainWidth);
	
	$j('table.formTableConnected td:first').width(mainWidth + 'px');
	
	/*
	 * $j('table.leftAlign tbody.leftAlignInner tr td')[0].offsetWidth;
	 * $j('table.leftAlign').css('margin-left', '-97px').css('margin-top',
	 * '10px');
	 */
	
	//trace('maxLength: ' + maxLength);
	}
	
	//traceEnd();
}

 function enumTabIndex() {
		$j('input, button').each(function() {
			$j(this).attr('tabindex', __tabIndexCount ++);
		});
	}
 
 
 function getInputValue(inputId) {
	 var el = document.getElementById(inputId);
	 
	 if(el) {
		 return el.value;
	 }
	 
	 return '';
 }
 
 function __debugPause(millis) 
 {
	 var date = new Date();
	 var curDate = null;
	
	 do { curDate = new Date(); } 
	 while(curDate-date < millis);
 }
 
 
 function sortParts(j) {
		trace(j);

		// kontenery
		var containersCount = j.containers.length;
		trace('Number of containers: ' + containersCount);
		
		for(var ii = 0; ii < containersCount; ii ++) {
			var jqContainer = $j('#' + j.containers[ii].name + '_container');
			trace('container ' + jqContainer.attr('id'));
			
			// grupy
			traceStart('groups');
			var groupsCount = j.containers[ii].groups.length;
			trace('Number of groups: ' + groupsCount);
			for(var i = 0; i < groupsCount; i ++) {
				var groupEl = j.containers[ii].groups[i];
				var partsCount = groupEl.parts.length;
				traceStart(groupEl.name);
					
					/* Tworzymy grupe w container */
					var newDiv = document.createElement('div');
					jqContainer.append(newDiv);
					var jqGroup = $j(newDiv).attr('id', groupEl.name + '_group').addClass('group');
				
					trace('Group id: ' + jqGroup.attr('id'));
					trace('Number of parts: ' + partsCount);
					// jqGroup.css('border', '1px solid black').css('margin',
					// '10px').css('display', 'block').css('float', 'left');
					// jqGroup.append('<span class="label">' + groupEl.name +
					// '</span>');
					
					for(var k = 0; k < partsCount; k ++) {
						var partEl = groupEl.parts[k];
						var jqPart = null;
						
						/* na wszelki wypadek, gdyby partEl == '' */
						if(partEl) {
							jqPart = $j('#' + partEl + '_part');
							if(jqPart.attr('id')) {
								trace('part: ' + partEl + '  id: ' + jqPart.attr('id'));
								jqGroup.append(jqPart);
							} else {
								trace('NOT FOUND: ' + '#' + partEl + '_part');
							}
						}
					}
					
				traceEnd();
			}
			traceEnd();
		}
		traceEnd();
}
 
/* Zmniejsza d�ugo�� selecta do __selectMaxWidth i dodaje dymek (title) z .wTooltip */
var __selectMaxWidth = 150;
 
function fixSelectWidth(jqSel) {
	var elSel = jqSel.get(0);
	
	if(jqSel.attr('offsetWidth') > __selectMaxWidth) {
		jqSel.css('width', __selectMaxWidth + 'px');
		var tit = elSel.options[elSel.selectedIndex].text;
	
		jqSel.attr('title', tit);
		
		try {
	        jqSel.wTooltip({style: false, className: "dsTooltip", 
								offsetY: 20, fadeIn: 400, delay: 400});
		} catch(e) {}
	}
}

/* 
 * Ustawia value input texta - obej�cie b��du litery '�' 
 * jQuery	jqTmpDiv 	- tymczasowy div (zawarto�� zostanie nadpisana)
 * jQuery	jqInput		- input, do kt�rego chcemy zapisa� 'str'
 * String	str			- string do wpisania
 */
function setUTFInputVal(jqTmpDivUnused, jqInput, str) {
	 
	 jqTmpDiv = $j(document.createElement("div"));
	
	 jqTmpDiv.html(str);
	 jqInput.val(jqTmpDiv.html());
	 
	 jqTmpDiv.remove();
}

function rgb2hex(rgb) {
	rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	function hex(x) {
		hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	}
	return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
}

function changeOneColor(sender) {
	trace('sender = ' + sender.id);

	var len = sender.value.length;

	if(len == 7) {
		trace('newColor = ' + sender.value);
		$j('.upperBarNew, img.tab_img, .all_border, a.upperMenuLinkNewPushed, a.upperMenuLinkNew, ' + 
				'#logout_top, a.left_selected, tr.footer_top td, .highlightedRows').css('background-color', sender.value);
		$j('a.upperMenuLinkNewPushed, a.upperMenuLinkNewHover, ul.left a:not(.left_selected)').css('color', sender.value);

		$j('.mai2').css('border-color', sender.value);
	}
}

function changeTwoColor(sender) {
	trace('sender2 = ' + sender.id);

	var len = sender.value.length;

	if(len == 7) {
		trace('newColor = ' + sender.value);
		$j('#middleMenuContainer, a.middleMenuLinkHover, img.tab2_img').css('background-color', sender.value);
		$j('a.middleMenuLinkHover').css('color', sender.value);
		$j('#middleContainer').css('border-color', sender.value);
	}
}

function initColorChanger() {
	var oneColor = new Array();
	var twoColor = new Array();

	oneColor[0] = 'white';
	oneColor[1] = $j('.upperBarNew').css('background-color');

	twoColor[0] = 'white';
	twoColor[1] = $j('#middleMenuContainer').css('background-color');

	$j('div.wrapper').before('<div class="colorChangerDiv" >' + 
			'<input id="oneColor" type="text" onkeyup=\"changeOneColor(this);\" value="' + rgb2hex(oneColor[1]) + '" style="color: ' + oneColor[0] + '; background-color: ' + oneColor[1] + ';"></br>' + 
			'<input id="twoColor" type="text" onkeyup=\"changeTwoColor(this);\" value="' + rgb2hex(twoColor[1]) + '" style="color: ' + twoColor[0] + '; background-color: ' + twoColor[1] + ';"></br>' + 
	'</div>');

}

function hideInfoList(domSender, domEl) {
	$j(domSender).hide();
	$j(domEl).slideUp('fast')
}

/*
 * 	pageContext	-	kontekst strony
 * 	domSender	-	klikni�ty guzik (typu DOM element)
 * 	addInfo		- 	id elementu do wy�wietlania informacji o rezultacie zapisu
 * 	documentId	-	id dokumentu
 * 	noteContent	-	zawarto�� uwagi
 */

var jqInfoBox__ = null;
var oldBtnVal__ = null;
var infoBoxTimer__ = null;

function hideInfoBox__() {
	jqInfoBox__.fadeOut('normal');
}

function ajaxSaveRemark(pageContext, domSender, addInfo, docId, noteStr, onSuccess) {
	traceStart('ajaxSaveRemark()');
	
	
	$j(domSender).addClass('btnLoading');
	oldBtnVal__ = $j(domSender).val();
	$j(domSender).val('');
	
	trace('domSender: ' + domSender.id + ', documentId: ' + docId);
	trace('noteStr: ' + noteStr);
	
	$j.post(pageContext + '/office/common/remark-ajax.action',
				{createRemark: true, documentId: docId, content: noteStr},
				function(data) {
					$j(domSender).removeClass('btnLoading').val(oldBtnVal__);
					
					if (data.toLowerCase() == 'saved') {
						showAjaxInfoBox(addInfo, 'dodano uwag�');
						onSuccess();
					} else {
						showAjaxInfoBox(addInfo, 'wyst�pi� b��d', true);
					}
				}
			);
	
	traceEnd();
}

function ajaxLoadRemarks(pageContext, docId, remCount, callFun) {
	$j.post(pageContext + '/office/common/remark-ajax.action',
		{getRemarks: true, documentId: docId, remarksCount: remCount},
		function(data) {
			callFun(data);
		});	
}

/*
 * 	Wy�wietla kr�tkie powiadomienie i chowa je po up�ywie 8000ms (mo�na zmieni� w funkcji)
 * 	boxId		-	id elementu dom, w kt�rym ma by� umieszczony tekst msg
 * 	msg			-	string z komunikatem ( != null)
 * [isErrorBox]	-	je�li true, to dodaje klas� errorInfo i nie chowa automatycznie komunikatu
 */
function showAjaxInfoBox(boxId, msg, isErrorBox) {
	var jqBox = $j('#' + boxId);
	clearTimeout(infoBoxTimer__);
	jqBox.text(msg);
	
	if(isErrorBox) {
		jqBox.addClass('errorInfo');
		jqBox.fadeIn('normal');
	} else {
		jqBox.removeClass('errorInfo'); // na wszelki wypadek
		jqBox.fadeIn('normal');
		jqInfoBox__ = jqBox;
		infoBoxTimer__ = setTimeout('hideInfoBox__()', 8000);
	}
}

/*
 * 	Zamiennik standardowego okna confirm (tj przyciski OK, ANULUJ)
 * 	Przypisujemy do zdarzenia onclick przycisku submit. Funkcja zawsze zwraca false.
 * 	Je�eli u�ytkownik kliknie OK, to wtedy wysy�amy formularz.
 * 	title		-	tytu� okna
 * 	msg			- 	wiadomo�� do wy�wietlenia
 * [disableInp]	-	wy��czanie (disabled=true) przycisk�w (domy�lnie false, wywo�ywane po callFun)
 * [callFun]	-	funkcja wywo�ywana przed wys�aniem formularza
 * [jqForm]		-	formularz jako obiekt jQuery (domy�lnie wywo�ujemy wszystkie znalezione na stronie)	
 */
function confirmDialog(title, msg, disableInp, callFun, jqForm) {
	$j('#dialog-confirm').dialog('destroy');
	$j("#dialog-confirm").attr('title', title).html('<p>' + msg + '</p>').dialog({
		resizable: false,
		modal: true,
		buttons: {
			'Anuluj': function() {
				$j(this).dialog('close');
			},
			'OK': function() {
				$j(this).dialog('close');
				if(callFun) 
					callFun();
				if(disableInp == true)
					$j('input[type=button], input[type=submit]').attr('disabled', true);
				if(!jqForm)	
					jqForm = $j('form');
				jqForm.submit();
				return true;
			}
		}
	});
	
	return false;
}


/*
 * Do debugu - pokazuje id, name i warto�� w konsoli FF ka�dego hiddena na stronie
 */
function showHiddens() {
	$j('input[type=hidden]').each(function() {
	    trace('ID: ' + this.id + ', NAME: ' + this.name + ', VALUE = ' + this.value);
	});
}

/*
 * Dodaje obs�ug� zdarze� hover i active przycisk�w
 */
function prepareButtons() {
	$j('input.btn:not(.saveBtn, .cancelBtn)').hover(function() {
		$j(this).addClass('btnHover');
	}, function() {
		$j(this).removeClass('btnHover');
	}).bind('mousedown.activegroup', function() {
		$j(this).addClass('btnActive');
	}).bind('mouseup.activegroup mouseleave.activegroup', function() {
		$j(this).removeClass('btnActive');
	});
	$j('input.saveBtn').bind('mousedown.activegroup', function() {
		$j(this).addClass('saveBtnActive');
	}).bind('mouseup.activegroup mouseleave.activegroup', function() {
		$j(this).removeClass('saveBtnActive');
	}).hover(function() {
		$j(this).addClass('saveBtnHover');
	}, function() {
		$j(this).removeClass('saveBtnHover');
	});
	$j('input.cancelBtn').bind('mousedown.activegroup', function() {
		$j(this).addClass('cancelBtnActive');
	}).bind('mouseup.activegroup mouseleave.activegroup', function() {
		$j(this).removeClass('cancelBtnActive');
	}).hover(function() {
		$j(this).addClass('cancelBtnHover');
	}, function() {
		$j(this).removeClass('cancelBtnHover');
	});
	$j('textarea.txt').bind('focus.activegroup', function() {
		$j(this).addClass('txtHover');
	}).bind('blur.activegroup', function() {
		$j(this).removeClass('txtHover');
	});
}

/**
 * Tworzy now� kopi� obiektu
 * TODO:
 * 	error 'too much recursion'
 */
function cloneObject(what) {
    for (i in what) {
        if (typeof what[i] == 'object') {
            this[i] = new cloneObject(what[i]);
        }
        else
            this[i] = what[i];
    }
}

/**
 * Dodaje pogrubienie dla etykiet input�w i select�w, gdy zaznaczone
 */
function bindLabelDecoration() {
	$j('fieldset.container input, fieldset.container select').bind('focus.forLabel', function() {
		$j('label[for="' + $j(this).attr('id') + '"]').addClass('bold');
	}).bind('blur.forLabel', function() {
		$j('label[for="' + $j(this).attr('id') + '"]').removeClass('bold');
	});
}

/**
 * Wy�wietla na ��to strza�k� aktualnego sortowania - ukrywa inne strza�ki
 * Przyk�ad w address-book.jsp
 * @param strSortBy Po czym sortujemy
 */
function showSortArrow(strSortBy){
	var jqImg = $j('a[name=' + strSortBy + '][ascending=' + $j('input[name=ascending]').val() + ']').children('img');
	var newImgSrc = jqImg.attr('src');
	newImgSrc = newImgSrc.replace('.gif', '-red.gif');
	jqImg.attr('src', newImgSrc);
}

function initExtendedSearch() {
	var SIMPLE_SEARCH = 'Wyszukiwanie proste';
	var EXTENDED_SEARCH = 'Wyszukiwanie zaawansowane';

	$j('fieldset.container p:not(.simpleSearch) input[type=text]').each(function() {
		if($j(this).val() != '') {
			$j('#searchSwitcher').attr('extendedSearch', 'true');
		}
	});

	$j('fieldset.container p:not(.simpleSearch) select').each(function() {
		if($j(this).attr('selectedIndex') != 0) {
			$j('#searchSwitcher').attr('extendedSearch', 'true');
		}
	});
	
	$j('fieldset.container p:not(.simpleSearch, .simpleSearchNotVisible) input[type=checkbox]').each(function() {
		if($j(this).attr('checked'))
			$j('#searchSwitcher').attr('extendedSearch', 'true');
	});

	if ($j('#searchSwitcher').attr('extendedSearch') == 'false') {
		$j('fieldset p:not(.simpleSearch)').hide();
		$j('#searchSwitcher').text(EXTENDED_SEARCH).attr('extendedSearch', 'true');
	}
	else {
		$j('fieldset p:not(.simpleSearch)').show();
		$j('#searchSwitcher').text(SIMPLE_SEARCH).attr('extendedSearch', 'false');;
	}

	$j('#searchSwitcher').click(function() {
		if($j(this).attr('extendedSearch') == 'false')
			simpleSearch(this);
		else
			extendedSearch(this); 
	});

	$j('#clearFields').click(function() {
		$j('fieldset.container input').val('');
	});
	
	function simpleSearch(domObj) {
		$j('fieldset p:not(.simpleSearch)').slideUp('fast');
		$j(domObj).text(EXTENDED_SEARCH).attr('extendedSearch', 'true');
	}

	function extendedSearch(domObj) {
		$j('fieldset p:not(.simpleSearch)').slideDown('fast');
		$j(domObj).text(SIMPLE_SEARCH).attr('extendedSearch', 'false');
	}
}

/**
 * Zwraca <code>true</code> je�li element jest w tablicy
 * @param {Array} array Tablice
 * @param {Object} el Szukany element
 */
function contains(array, el) {
	for(i = 0; i < array.length; i ++) {
		if(array[i] == el)
			return true;
	}
	
	return false;
}

/**
 * Formatowanie p�l liczbowych
 */
function formatNumericFields() {
$j('input.formatNumber').each(function() {
		trace('Formatting ' + $j(this).attr('name'));
		var str  = '<input type="text" onClick="toggleFormatNumber(this)" class="txt formattedNumber" style="width: ' + $j(this).width() + 'px;" ';
			str += 'value="' + formatNumber($j(this).val()) + '"';
			str += '/>';	
		$j(this).hide();
		$j(this).after(str);
	});
	
	$j('input.formatNumber').bind('blur.formatNumber', function() {
		$j(this).next('input.txt').val(formatNumber($j(this).val()));
		$j(this).hide();
		$j(this).next('input.txt').show();
	});
}
	
function toggleFormatNumber(domObj) {
	$j(domObj);
	$j(domObj).hide();
	$j(domObj).prev('input.formatNumber').show().focus();
}

function formatNumber(nStr)
{
	nStr += '';
	x = nStr.split('.').length > 1 ? nStr.split('.') : nStr.split(',');
	x1 = x[0];
	x2 = x.length > 1 ? x[1] : '00';
	var num = x1 + '.' + x2;
	num = +num;
	num = num.toFixed(2);
	num = num.toString();
	x = num.split('.');
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x[0])) {
		x[0] = x[0].replace(rgx, '$1' + ' ' + '$2');
	}
	
	return x[0] + '.' + x[1];
}

function setSafeValue(domObj, strValue) {
	if(strValue != null && strValue != undefined) {
		try {
			domObj.value = strValue;
		} catch(e) {
			return false;
		}
		return true;
	}
	
	return false;
}

/**
* Zwraca sformatowan� liczb�:
* format musi by� w postaci (dla liczby 123456.789):
* ### ###,## = 123 456,79
* ### ###, = 123 456,789
* ### ### = 123 457
*/ 

Number.prototype.format = function(format) {
    var commaOperator = (format.split('').reverse().join('').match(/[,|\.]/) || []).join('');
    var strNum = this + '', ret = '';
    var splittedNum, splittedFormat, fract;
    
    function pat(num, format) {
    	var arrNum = (num + '').split('');
    	var arrFormat = (format + '').split('');
    	var i = arrFormat.length, ret = '';
    	do {
    		i --;
    		if(arrFormat[i] == '#') {
    			ret = arrNum.pop() + ret;
    		} else {
    			ret = arrFormat[i] + ret;
    		}
    	} while(i > 0 && arrNum.length > 0)
    	if(arrNum.length > 0) {
    		ret = arrNum.join('') + ret;
    	}
    	return ret;
    }

    if(commaOperator) {
        splittedNum = strNum.split('.');
        splittedFormat = format.split(commaOperator);
        
        ret += pat(splittedNum[0], splittedFormat[0]);
        fract = (this.toFixed(splittedFormat[1].length) + '').split('.')[1] || splittedNum[1] || '';
        if(fract) {
        	ret += commaOperator + fract;
        } 
    } else {
    	ret = pat(Math.round(this), format);
    }
    ret += '';
    ret = ret.replace(/^-\s/,'-');

    return ret;
};

function showAllMenus(show) {
	if(show === true) {
		$j('body').removeClass('hideAllMenus');
	} else {
		$j('body').addClass('hideAllMenus');
	}
}



 