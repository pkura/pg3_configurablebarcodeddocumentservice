<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="baseLink"/>" method="post">

    <ww:if test="jspComponentCheckboxes.jspTitle != null">
        <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="jspComponentCheckboxes.jspTitle"/></h2>
    </ww:if>

    <ww:if test="jspComponentCheckboxes.jspData != null">

	    <table>
            <ww:iterator value="jspComponentCheckboxes.jspData">
                <tr>
                    <td>
                        <ww:property value="jspTitle"/>
                     </td>
                     <td>
                        <ww:checkbox name="jspComponentCheckboxes.jspSelectedIds" fieldValue="jspId" value="jspComponentCheckboxes.isSelected(jspId)" />
                    </td>
                </tr>
            </ww:iterator>
        </table>

    </ww:if>
</form>
