<!--
########################################################################################################################
########################################################################################################################

W jsp wykorzystuj�cym ten komponent musi by� przypisany obiekt klasy pochdnej [pl.compan.docusafe.web.jsp.component.JspComponentTable]
pod nazw� "jspComponentTable" oraz nazwa tego obiektu pod "jspComponentTableName"
Przyk�ad:

    ww:set name="jspComponentTable" value="<userList object>" /                 -- obiekt
    ww:set name="jspComponentTableName" value="'userList'" /                    -- nazwa
    ww:set name="jspPermissionManager" value="<permissionManager object>" /     -- mened�er uprawnie�
    ww:set name="ComponentNoCheckPermissions" value="true" /               -- je�eli maja byc nie sprawdzane uprawnienia okreslajace: readonly, disabled, hidden
	ww:include page="/jsp/component/table.jsp" /                                -- dolaczenie komponentu

	ww:set name="DEBUG_ON" value="true" /                                       -- wyswielta parametry pol: hidden, disabled, readonly

########################################################################################################################
########################################################################################################################
-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<ww:if test="#DEBUG_ON==true">
    isComponentHidden=<ww:property value="#ComponentNoCheckPermissions!=true && #jspPermissionManager.isComponentHidden(#jspComponentTableName)"/></br>
    ComponentNoCheckPermissions=<ww:property value="#ComponentNoCheckPermissions"/></br>
    ComponentNoCheckPermissions=<ww:property value="#ComponentNoCheckPermissions!=true"/></br>
    jspPermissionManager=<ww:property value="#jspPermissionManager"/></br>
    jspPermissionManager.isComponentHidden=<ww:property value="#jspPermissionManager.isComponentHidden(#jspComponentTableName)"/></br>
</ww:if>



<ww:if test="!(#ComponentNoCheckPermissions!=true && #jspPermissionManager.isComponentHidden(#jspComponentTableName))">

    <ww:if test="#jspComponentTable.jspTitle != null">
        <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="#jspComponentTable.jspTitle"/></h2>
    </ww:if>

	<table class="search" width="100%" cellspacing="0" id="mainTable">

		<ww:if test="#jspComponentTable.jspHeaderDisabled == null || #jspComponentTable.jspHeaderDisabled == false">
            <tr class="tableheader">

                <ww:if test="#jspComponentTable.getChosenItemGetter()!=null">
                    <th/>
                </ww:if>

                <ww:iterator value="#jspComponentTable.jspColumns" status="status" > <%-- kolumny --%>
                    <ww:set name="column"/>

                    <th width="<ww:property value="width"/>">
                        <nobr>
                            <ww:if test="sortDesc != null">
                                <a href="<ww:url value="sortDesc"/>" class="nonDroppable"
                                    name="<ww:property value="title"/>_descending"
                                    id="<ww:property value="title"/>_descending"
                                    alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
                                        <ww:if test="#jspComponentTable.isOrderBy(#column,false)">
                                            <img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
                                        </ww:if>
                                        <ww:else>
                                            <img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
                                        </ww:else>
                                        width="0" height="0" border="0"/>
                                        <ww:param name="'name'" value="ascending"/>
                                </a>
                            </ww:if>

                            <ww:property value="title"/>

                            <ww:if test="sortAsc != null">
                                <a href="<ww:url value="sortAsc"/>" class="nonDroppable"
                                    name="<ww:property value="title"/>_ascending"
                                    alt="<ds:lang text="SortowanieRosnace"/>"
                                    title="<ds:lang text="SortowanieRosnace"/>">
                                        <ww:if test="#jspComponentTable.isOrderBy(#column,true)">
                                            <img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
                                        </ww:if>
                                        <ww:else>
                                            <img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
                                        </ww:else>
                                        width="0" height="0" border="0"/>
                                </a>
                            </ww:if>
                        </nobr>
                    </th>
                </ww:iterator>

                <ww:if test="#jspComponentTable.jspFilterColumnTitle != null">
                    <th width="<ww:property value="#jspComponentTable.jspFilterColumnWidth"/>">
                        <ww:property value="#jspComponentTable.jspFilter"/>
                    </th>
                </ww:if>
                <ww:else><ww:if test="#jspComponentTable.jspFilterColumnWidth != null">
                    <th width="<ww:property value="#jspComponentTable.jspFilterColumnWidth"/>"></th>
                </ww:if></ww:else>

            </tr>
        </ww:if>

		<ww:if test="#jspComponentTable.jspFilterEnabled != null && #jspComponentTable.jspFilterEnabled == true">
		    <tr class="tableheader">

                <ww:if test="#jspComponentTable.getChosenItemGetter()!=null">
                    <th/>
                </ww:if>

                <ww:iterator value="#jspComponentTable.jspColumns"> <%-- filtry kolumn --%>
                    <ww:set name="column"/>

                    <th width="<ww:property value="width"/>">

						<ww:if test="#column.filter != null">

                            <ww:if test="#column.filter.jspTitle != null">
                                <ww:property value="#column.filter.jspTitle"/>
                            </ww:if>

                            <ww:if test="#column.filter.jspType == null"></ww:if>
                            <ww:elseif test="#column.filter.jspType.jspCode == 'SELECT'">
                                <select name="<ww:property value="(#jspComponentTableName)+'.'+#column.filter.jspFieldName"/>" class="sel">
                                    <ww:iterator value="#column.filter.jspValue.jspItems">
                                        <option value="<ww:property value="jspId"/>" <ww:if test="#jspComponentTable.isSelected(#column,jspId)">selected="true"</ww:if> >
                                            <ww:property value="jspTitle"/>
                                        </option>
                                    </ww:iterator>
                                </select>
                            </ww:elseif>
                            <ww:else>
                                <ww:textfield name="(#jspComponentTableName)+'.'+(#column.filter.jspFieldName)" cssClass="'txt'"/>
                            </ww:else>

                        </ww:if>

                    </th>
                </ww:iterator>
                <th>
					<ds:submit-event value="'Filtruj'" name="'doFilter'" cssClass="'btn'"/>
                </th>
		    </tr>
        </ww:if>

		<ww:if test="#jspComponentTable.jspData != null">

            <ww:iterator value="#jspComponentTable.jspData">
                <ww:set name="row"/>

                <tr
                        <ww:if test="#jspComponentTable.getDataItemId(#row)!=null">id="<ww:property value="#jspComponentTable.getDataItemId(#row)"/>"</ww:if>
                        <ds:available test="tasklist.highlightedRows">
                        onmouseover="$j(this).addClass('highlightedRows')"
                        onmouseout="$j(this).removeClass('highlightedRows')" </ds:available> >

                    <ww:if test="#jspComponentTable.getChosenItemGetter()!=null">
                        <td>

                            <ww:if test="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldHidden('BUTTON_CHOOSE')!=true">

                                <input type="button"
                                    value="<ww:property value="#jspComponentTable.getChosenItemGetter().getChosenInputName()"/>"
                                    class="<ww:property value="#jspComponentTable.getChosenItemGetter().getChosenInputClass()"/>"
                                    onclick="choose(<ww:property value="#jspComponentTable.getDataItemId(#row)"/>)"
                                    style="width:100%"
                                    <ww:if test="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldDisabled('BUTTON_CHOOSE')==true">disabled="true"</ww:if>
                                    >

                            </ww:if>

                        </td>
                    </ww:if>

                    <ww:iterator value="#jspComponentTable.jspColumns">
                        <ww:set name="column"/>
                        <td class="<ww:property value="getColumnName()"/>" width="<ww:property value="width"/>">

                            <ww:if test="#jspComponentTable.getLink(#row, #column)!=null">
                                <a href="<ww:url value="#jspComponentTable.getLink(#row, #column)"/>" class="nonDroppable">
                                    <ww:property value="#jspComponentTable.getData(#row, #column)"/>
                                </a>
                            </ww:if>
                            <ww:else>
                                 <ww:property value="#jspComponentTable.getData(#row, #column)"/>
                            </ww:else>

                        </td>
                    </ww:iterator>
                </tr>
            </ww:iterator>
        </ww:if>

    </table>

</ww:if>
