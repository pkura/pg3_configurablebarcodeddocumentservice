<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="baseLink"/>" method="post">

    <ww:if test="jspComponentAttributes.jspTitle != null">
        <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="jspComponentAttributes.jspTitle"/></h2>
    </ww:if>

    <ww:if test="jspComponentAttributes.jspData != null">

	    <table>
            <ww:iterator value="jspComponentAttributes.jspData">
                <tr>
                    <td>
                        <ww:property value="getValue().jspTitle"/>
                    </td>
                    <td>
                        <ww:if test="editable == false">
                            <ww:property value="getValue().jspValue"/>
                        </ww:if>
                        <ww:else>
                            <ww:if test="getValue().jspType == null"></ww:if>
                            <ww:elseif test="getValue().jspType.jspCode == 'SELECT'">
                                <select name="<ww:property value="getValue().jspValue()"/>" class="sel">
                                    <ww:iterator value="getValue().jspValue.jspItems">
                                        <option value=<ww:property value="jspId"/> >
                                            <ww:property value="jspTitle"/>
                                        </option>
                                    </ww:iterator>
                                </select>
                            </ww:elseif>
                            <ww:else>
                                <!-- TAK NIE ZADZIALA USTAWIENIE TEGO POLA, ZADZIALA TYLKO POBRANIE WARTOSCI -->
                                <ww:textfield name="'jspComponentAttributes.jspData.get(\"'+getKey()+'\").jspValue'" cssClass="'txt'"/>
                            </ww:else>
                        </ww:else>
                    </td>
                </tr>
            </ww:iterator>
        </table>

    </ww:if>
</form>
