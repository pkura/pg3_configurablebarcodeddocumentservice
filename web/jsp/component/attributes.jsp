<!--
########################################################################################################################
########################################################################################################################

W jsp wykorzystuj�cym ten komponent musi by� przypisany obiekt klasy pochdnej [pl.compan.docusafe.web.jsp.component.JspComponentAttributes]
pod nazw� "jspComponentAttributes" oraz nazwa tego obiektu pod "jspComponentAttributesName"
Przyk�ad:

    ww:set name="jspComponentAttributes" value="<list object>" /            -- obiekt
    ww:set name="jspComponentAttributesName" value="'<list name>'" /        -- nazwa
    ww:set name="ComponentNoCheckPermissions" value="true" /      -- je�eli maja byc nie sprawdzane uprawnienia okreslajace: readonly, disabled, hidden
    ww:set name="jspPermissionManager" value="<permissionManager object>" / -- mened�er uprawnie�
	ww:include page="/jsp/component/attributes.jsp" /                       -- dolaczenie komponentu

	ww:set name="DEBUG_ON" value="true" /                                   -- wyswielta parametry pol: hidden, disabled, readonly


########################################################################################################################
########################################################################################################################
-->
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>



<ww:if test="#DEBUG_ON==true">
    isComponentHidden=<ww:property value="#ComponentNoCheckPermissions!=true && #jspPermissionManager.isComponentHidden(#jspComponentTableName)"/></br>
    ComponentNoCheckPermissions=<ww:property value="#ComponentNoCheckPermissions"/></br>
    ComponentNoCheckPermissions=<ww:property value="#ComponentNoCheckPermissions!=true"/></br>
    jspPermissionManager=<ww:property value="#jspPermissionManager"/></br>
    jspPermissionManager.isComponentHidden=<ww:property value="#jspPermissionManager.isComponentHidden(#jspComponentTableName)"/></br>
</ww:if>



    <ww:if test="#jspComponentAttributes!=null && !(#ComponentNoCheckPermissions!=true && #jspPermissionManager.isComponentHidden(#jspComponentAttributesName))">

        <ww:if test="#jspComponentAttributes.jspTitle != null">
            <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="#jspComponentAttributes.jspTitle"/></h2>
        </ww:if>

        <ww:if test="#jspComponentAttributes.jspData != null">

            <table>
                <ww:iterator value="#jspComponentAttributes.jspData">
                    <ww:set name="attr"/>
                    <tr>
                        <td>
                            <ww:property value="jspTitle"/>
                        </td>
                        <td>

                            <ww:if test="#DEBUG_ON==true">
                                hidden=<ww:property value="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldHidden(#jspComponentAttributesName,jspFieldName)"/>
                                disabled=<ww:property value="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldDisabled(#jspComponentAttributesName,jspFieldName)==true || editable!=true"/>
                            </ww:if>

                            <ww:if test="#ComponentNoCheckPermissions==true || #jspPermissionManager!=null && #jspPermissionManager.isFieldHidden(#jspComponentAttributesName,jspFieldName)!=true">

                                <ww:if test="jspType == null"></ww:if>
                                <ww:elseif test="jspType.jspCode == 'SELECT'">
                                    <select name="<ww:property value="#jspComponentAttributesName+'.'+jspFieldName"/>" class="sel"
                                            <ww:if test="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldDisabled(#jspComponentAttributesName,jspFieldName)==true || editable!=true">disabled="true"</ww:if> >
                                        <ww:iterator value="jspValue.jspItems">
                                            <option value=<ww:property value="jspId"/> <ww:if test="#jspComponentAttributes.isSelected(jspFieldName,jspId)">selected="true"</ww:if> >
                                                <ww:property value="jspTitle"/>
                                            </option>
                                        </ww:iterator>
                                    </select>
                                </ww:elseif>
                                <ww:else>
                                    <ww:if test="jspFieldName != null && jspFieldName.length() > 0">
                                        <ww:textfield name="#jspComponentAttributesName+'.'+jspFieldName" cssClass="'txt ' + jspCssClass"
                                                disabled="#ComponentNoCheckPermissions!=true && #jspPermissionManager!=null && #jspPermissionManager.isFieldDisabled(#jspComponentAttributesName,jspFieldName)==true || editable!=true"/>
                                    </ww:if>
                                    <ww:else>
                                        <ww:if test="!editable">
                                            <ww:property value="jspValue"/>
                                        </ww:if>
                                        <ww:else>
                                            <input type="text" name="no-name" value="<ww:property value="jspValue"/>" readonly="true" class="txt" style="width: 200px;">
                                        </ww:else>
                                    </ww:else>
                                </ww:else>
                            </ww:if>

                        </td>
                    </tr>
                </ww:iterator>
            </table>

        </ww:if>

    </ww:if>
