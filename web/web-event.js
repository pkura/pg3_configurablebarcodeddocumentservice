function sendWebEvent(btn, event)
{
    if (typeof btn.form == 'undefined')
    {
        alert('Przycisk "'+btn.name+'" nie znajduje si� w formularzu');
        return false;
    }

    var form = btn.form;

    // TODO: przeszuka� formularz i pousuwa� inne przyciski maj�ce nazw� z !event.
    // (zabezpieczenie przed back)
    // albo nie pozwala� na ponowne wys�anie formularza

    var hid = document.createElement('INPUT');
    hid.type = 'HIDDEN';
    hid.name = '!event.'+event; //btn.name;
    hid.value = 'EXEC'; // cokolwiek - aby tylko pole mia�o warto��
    form.appendChild(hid);

    for (var i=0; i < form.elements.length; i++)
    {
        var el = form.elements[i];
        if (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button')
        {
            el.disabled = true;
        }
    }

    //alert('dodano hidden '+hid.name);

    form.submit();

    return true;
}
