/*
 * MEvent.js
 *
 * (c) 2005-2006 Michal Wojciechowski <odyniec at odyniec dot net>
 *
 */

if (typeof MClass == "undefined") alert("`MClass.js' is not loaded.");

var MEvent = new MClass({
  Static: {
    eventHandlers: {},
  	 lastId: -1,
	 elements: [],


	 bindFunction: function (object, func) {
	   var obj = object;	 
	 
	 	return function () {
	 	  return func.apply(obj, arguments);
	 	};
	 },
	 

  	_addEventHandler: function (element, type, handler, propagate) {
      if (element.addEventListener)
        element.addEventListener(type, handler, propagate);
      else if (element.attachEvent)
        element.attachEvent("on" + type, handler);
      else
        element["on" + type] = handler;
    },


    _handleEvent: function (name, event) {
      var ret = null;
    
      for (var i = 0; i < MEvent.eventHandlers[MEvent.elements[this].id][name].length; i++)
        if (MEvent.eventHandlers[MEvent.elements[this].id][name][i].active &&
	       (ret = MEvent.eventHandlers[MEvent.elements[this].id][name][i].handler.apply(this, [event])) == false)
		  {
          return false;
		  }

      return ret;
    },


    addEventHandler: function (element, name, handler, propagate) {
      var id;
	  
	   if (MEvent.elements[element])
	     id = MEvent.elements[element].id;
	   else
	     MEvent.elements[element] = { id: id = ++MEvent.lastId };

      if (!MEvent.eventHandlers[id])
        MEvent.eventHandlers[id] = { };

      if (!MEvent.eventHandlers[id][name]) {
        window.status += " * " + name + " ";

        MEvent.eventHandlers[id][name] = [ ];

        if (element == window && name == "load")
          element["on" + name] = function (event) {
  	         MEvent._handleEvent.apply(element, [name, event]); 
  	       };
        else
          MEvent._addEventHandler(element, name, function (event) {
	         MEvent._handleEvent.apply(element, [name, event]);
	       }, propagate);
      }

      var length = MEvent.eventHandlers[id][name].length;
      MEvent.eventHandlers[id][name][length] = { 
        "handler": handler,
        "active": true
      };
    },


    removeEventHandler: function (element, name, handler, propagate) {
      if (MEvent.eventHandlers[element.MEvent.id] && 
	     MEvent.eventHandlers[element.MEvent.id][name]) 
      {
        var handlers = MEvent.eventHandlers[element.MEvent.id][name];

        for (var i = 0; i < handlers.length; i++)
          if (handlers[i].handler == handler)
	        MEvent.eventHandlers[element.MEvent.id][name].splice(i, 1);
      }
    },


    activateEventHandler: function (element, name, handler) {
      if (MEvent.eventHandlers[element.MEvent.id] && 
	     MEvent.eventHandlers[element.MEvent.id][name]) 
      {
        var handlers = MEvent.eventHandlers[element.MEvent.id][name];

        for (var i = 0; i < handlers.length; i++)
          if (handlers[i].handler == handler)
	         MEvent.eventHandlers[element.MEvent.id][name][i].active = true;
      }
    },
  

    deactivateEventHandler: function (element, name, handler) {
      if (MEvent.eventHandlers[element.MEvent.id] && 
	     MEvent.eventHandlers[element.MEvent.id][name]) 
      {
        var handlers = MEvent.eventHandlers[element.MEvent.id][name];

        for (var i = 0; i < handlers.length; i++)
          if (handlers[i].handler == handler)
	         MEvent.eventHandlers[element.MEvent.id][name][i].active = false;
      }
    }
  }
});
