<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Odbierz dokument papierowy"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/get-document.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">

	<hr class="shortLine" />


	<div class="field multiFiles_part">
	
		<ds:lang text="Podaj barcode" />:
    	<a href="#" onclick="addAtta(this); return false;" class="addLink" ><ds:lang text="Dodaj kolejny" /></a>
		<ww:textfield id="barcode" name="'barcode'" size="10" cssClass="'txt'" value="''"/>

		<img src="<ww:url value="'/img/minus2.gif'" />" class="removeLink" onclick="delAtta(this);" title="Usu�" style="cursor: pointer; position: relative; top: 3px; left: 3px;">
	</div>
	<ds:submit-event value="getText('Przyjmij')" name="'doSearch'" />

</form>
<script type="text/javascript">
$j('img.removeLink').hide();
var attCounter = 2;
window.onload = function() {
	  document.getElementById("barcode").focus();
	};
	
	$("#barcode").change(function(){ alert("Hello"); }); 
function addAtta(domObj)
{
	$j('img.removeLink').show();
	var str = '<div class="field multiFiles_part">';
		str += $j('div.multiFiles_part:last').html(); 
		str += '</div>';
		
	trace(str);
	$j(domObj).parent().after(str).fadeIn();
	$j('div.multiFiles_part:last').children('input.barcode').attr('barcode' + attCounter);
	var jqAlmostLast = $j('div.multiFiles_part').eq(-2);
	jqAlmostLast.children('a.addLink').css('visibility', 'hidden');
	jqAlmostLast.children('img.removeLink').show();
	attCounter ++;
	
}

function delAtta(domObj)
{
	if($j('div.multiFiles_part').length > 1) {
		if ($j('img.removeLink:last').filter(domObj).length > 0) {
			$j('a.addLink').eq(-2).show();	// pokazujemy przedostatni
		}
		
		if($j('div.multiFiles_part').length < 3)
			$j('img.removeLink').hide();
		
		$j(domObj).parent('div.multiFiles_part').slideUp(300, function() {
			$j(this).remove();
			$j('div.multiFiles_part:last').children('a.addLink').css('visibility', 'visible');
		});
	}
}
</script>