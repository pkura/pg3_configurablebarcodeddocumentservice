<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="ZarzadzanieBarkodami"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="Barcodes"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<br>
<table>
<tr>
	<th>
		<ds:lang text="id"/>
	</th>
	<th>
		<ds:lang text="ctime"/>
	</th>
	<th>
		<ds:lang text="Uzytkownik"/>
	</th>
	<th>
		<ds:lang text="LiczbaBarkodow"/>
	</th>
	<th>
		<ds:lang text="PrzydzielonaLiczbaBarkodow"/>
	</th>
	<th>
		<ds:lang text="status"/>
	</th>
	<th>
		<ds:lang text="PozostaloBarkodow"/>
	</th>
</tr>
<ww:iterator value="ranges">
<tr>
	<td>
		<ww:property value="id"/>
	</td>
	<td>
		<ww:property value="ctime"/>
	</td>
	<td>
		<ww:property value="fullUsername"/>
	</td>
	<td>
		<ww:property value="numberOfBarcodes"/>
	</td>
	<td>
		<ww:property value="assignedNumberOfBarcodes"/>
	</td>
	<td>
		<ww:property value="statusDesc"/>
	</td>
	<td>
		<ww:property value="barcodesLeft"/>
	</td>
	
	<td>
		<a href="<ww:url value="'/office/assign-from-range.action'"/>?masterRangeIdentity=<ww:property value="id"/>&rangeId=<ww:property value="rangeId"/>"/><ds:lang text="Przydziel"/></a>
	</td>
	
</tr>
</ww:iterator>
</table>
<ww:if test="masterRangeIdentity!=null">
<form action="<ww:url value="'/office/assign-from-range.action'"/>" method="post" id="form">
<ww:hidden name="'rangeId'" id="rangeId"/>
<ww:hidden name="'masterRangeIdentity'" id="masterRangeIdentity"/>
<table>
	<tr>
		<td>
			<ds:lang text="PoczatekZakresu"/>
		</td>
		<td>
			<ww:textfield name="'rangeStart'" id="rangeStart"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="KoniecZakresu"/>
		</td>
		<td>
			<ww:textfield name="'rangeEnd'" id="rangeEnd"/>
		</td>
	</tr>
	
	<tr>
		<td colspan=2>
			<ds:submit-event value="'Przydziel'" name="'doAssignFromOwn'" cssClass="'btn'" confirm=""></ds:submit-event>
		</td>
	</tr>
</table>
</form>
</ww:if>

<ww:if test="confirmAssign">
<table>
<tr>
	<th>
		<ds:lang text="id"/>
	</th>
	
	<td>
		<ww:property value="br.getId()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="ctime"/>
	</th>
	
	<td>
		<ww:property value="br.getCtime()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="Uzytkownik"/>
	</th>
	
	<td>
		<ww:property value="br.getFullUsername()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="LiczbaBarkodow"/>
	</th>
	
	<td>
		<ww:property value="br.getNumberOfBarcodes()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="PrzydzielonaLiczbaBarkodow"/>
	</th>
	
	<td>
		<ww:property value="br.getAssignedNumberOfBarcodes()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="status"/>
	</th>
	
	<td>
		<ww:property value="br.getStatusDesc()"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="PozostaloBarkodow"/>
	</th>
	<td>
		<ww:property value="br.getBarcodesLeft()"/>
	</td>
</tr>
<tr>	
	<td>
		<a href="<ww:url value="'/office/manage-barcodes.action'"/>"/><ds:lang text="OK"/></a>
	</td>
	
</tr>
</table>
</ww:if>


