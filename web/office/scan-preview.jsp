<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N scan-preview.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
    <link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>" />
    <title>Podglad</title>
</head>
<body marginwidth="0" leftmargin="0" topmargin="0" marginheight="0" onload="window.focus();">

<script>
    var path = '<%= request.getParameter("path") %>';
    var width = '<%= request.getParameter("width") %>';
    var height = '<%= request.getParameter("height") %>';

    var wh = document.body.clientWidth;
    var ww = document.body.clientHeight;

    var imax = width > height ? width : height;
    var wmax = wh > ww ? wh : ww;

    var iw = width;
    var ih = height;

    if (imax > wmax)
    {
        iw = Math.round(width * (wmax / imax) * 0.7);
        ih = Math.round(height * (wmax / imax) * 0.7);
    }



    function acceptImage()
    {
        if (window.opener)
        {
            alert('title='+window.opener.document.getElementById('title')+
                ' file='+window.opener.document.getElementById('file'));
            window.opener.document.getElementById('title').value = 'Zeskanowany dokument';
            window.opener.document.getElementById('file').value = path;
            window.close();
        }
        else
        {
            alert('Brak g��wnego okna aplikacji');
        }
    }
</script>

<table>
    <tr>
        <td>
            <script>document.write('<img src="'+path+'" width='+iw+' height='+ih+' alt="Zeskanowany dokument" style="border: 1px solid black; padding: 2px">');</script>
        </td>
        <td valign="top">
            <input type="button" class="btn" value="Akceptacja" onclick="acceptImage();"><br/><input type="button" class="btn" value="Odrzucenie" onclick="window.close();">
        </td>
    </tr>
</table>

</body>
</html>