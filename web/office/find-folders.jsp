<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-cases.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script language="JavaScript">
	function selectRadio(value)
	{
		document.getElementById(value).checked = true;
	}

	function pickCase(id, caseId, mode)
	{
		window.opener.pickCase(id, caseId, mode);
		window.close();
	}
</script>

<h1>Wyszukiwanie teczek</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:if test="!popup">
	<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>
</ww:if>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form name="formul" action="<ww:url value="'/office/find-folders.action'"/>" method="post">
		<ww:hidden name="'popup'" />
		<ww:hidden name="'mode'" />
		<ww:hidden name="'rwaId'" id="rwaId" value="rwaId"/>
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		<table class="formTable">
			<tr>
				<td>
					Znak teczki:
				</td>
				<td>
					<ww:textfield name="'officeId'" size="25" maxlength="84" cssClass="'txt'"/>
					<ds:infoBox body="'Ka�de u�ycie znaku podkre�lenia traktowane jest podczas wyszukiwania przez system jako dowolny znak, a znak procenta jako dowolny ci�g znak�w'" header="'Informacja'"/> 
				</td>
			</tr>
			<tr>
				<td>
					Tytu� teczki:
				</td>
				<td>
					<ww:textfield name="'name'" size="25" maxlength="84" cssClass="'txt'"/>
					<ds:infoBox body="'Ka�de u�ycie znaku podkre�lenia traktowane jest podczas wyszukiwania przez system jako dowolny znak, a znak procenta jako dowolny ci�g znak�w'" header="'Informacja'"/> 
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr>
				<td>
					U�ytkownik tworz�cy:
				</td>
				<td>
					<ww:select name="'person'" cssClass="'sel combox-chosen'" list="users" listKey="name"
					listValue="lastname+' '+firstname" multiple="true" size="3"/>
				</td>
			</tr>
			<%-- <tr>
				<td>
					Numer kolejny sprawy:
				</td>
				<td>
					<ww:textfield name="'seqNum'" size="4" maxlength="7" cssClass="'txt'"/>
				</td>
			</tr> --%>
			<%-- <tr>
				<td>
					Tytu� sprawy:
				</td>
				<td>
					<ww:textfield name="'title'" size="25" maxlength="84" cssClass="'txt'"/>
					<ds:infoBox body="'Ka�de u�ycie znaku podkre�lenia traktowane jest podczas wyszukiwania przez system jako dowolny znak, a znak procenta jako dowolny ci�g znak�w'" header="'Informacja'"/> 
				</td>
			</tr> --%>
			<tr>
				<td>
					Kategoria RWA:
				</td>
				<td>
					<ww:textfield name="'prettyRwaCategory'" size="40" maxlength="200" cssClass="'txt'" id="prettyRwaCategory" readonly="true"/>
					<input type="button" value='Wybierz RWA'
						onclick="javascript:void(window.open('<ww:url value="'/office/rwa/pick-rwa.action'"/>?id='+document.getElementById('rwaId').value, null, 'width=700,height=500,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn">
				</td>
			</tr>
			<tr>
				<td>
					Rok:
				</td>
				<td>
					<ww:textfield name="'year'" size="4" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Data za�o�enia:
				</td>
				<td>
					<script type="text/javascript">
								var _cdateFrom = "cdateFrom";
								var _cdateTo = "cdateTo";
							</script>
					<ww:textfield name="'cdateFrom'" id="cdateFrom" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_cdateFrom,_cdateTo,1)'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_cdateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					<ww:textfield name="'cdateTo'" id="cdateTo" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_cdateFrom,_cdateTo,1)'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_cdateTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<%-- <tr>
				<td>
					Przewidywany termin zako�czenia:
				</td>
				<td>
					<ww:textfield name="'finishDateFrom'" id="finishDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_finishDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					<ww:textfield name="'finishDateTo'" id="finishDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_finishDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr> --%>
			<tr>
				<td>
					Referent sprawy:
				</td>
				<td>
					<ww:select name="'assignedUser'" cssClass="'sel combox-chosen'" list="users" listKey="name"
						listValue="lastname+' '+firstname" multiple="true" size="3"/>
				</td>
			</tr>
			<%-- <tr>
				<td>
					Stan za�atwienia:
				</td>
				<td>
					<ww:select name="'statusId'" cssClass="'sel'" list="statuses" listKey="id"
						listValue="name" multiple="true" size="3"/>
				</td>
			</tr> --%>
			<ds:available test="wyszukiwarki.wlacz.rejestr">
			<tr>
				<td>
					Rejestr:
				</td>
				<td>
					<ww:select name="'record'" list="records" cssClass="'sel'" multiple="true" size="3"/>
				</td>
			</tr>
			</ds:available>
			<%--	<tr>
				<td>
					Czas wykonania up�ywa za:
				</td>
				<td>
					<html:text property="expiresIn" size="3" styleClass="txt_opt"/>
					<html:select property="expiresInMod" styleClass="sel_opt">
						<html:option value="1">lub wi�cej dni</html:option>
						<html:option value="-1">lub mniej dni</html:option>
						<html:option value="0">dni</html:option>
					</html:select>
					albo up�yn��
					<html:text property="expiredBy" size="3" styleClass="txt_opt"/>
					<html:select property="expiredByMod" styleClass="sel_opt">
						<html:option value="1">lub wi�cej dni</html:option>
						<html:option value="-1">lub mniej dni</html:option>
						<html:option value="0">dni</html:option>
					</html:select>
					temu
				</td>
			</tr>	--%>
			<%--	<tr>
				<td>
					Tylko sprawy przeterminowane:
				</td>
				<td>
					<html:checkbox property="findExpired" value="true"/>
				</td>
			</tr>	--%>
			<%--	<tr>
				<td>
					Liczba wynik�w na stronie:
				</td>
				<td>
					<html:select property="limit" styleClass="sel" >
						<html:option value="10">10</html:option>
						<html:option value="25">25</html:option>
						<html:option value="50">50</html:option>
					</html:select>
				</td>
			</tr>	--%>
			<tr class="formTableDisabled">
				<td colspan="2">
					<input type="submit" name="doSearch" class="btn" value="Szukaj"/>
					 <ds:available test="menu.left.user.to.briefcase">
					<input type="submit" name="doShowPzypisane" class="btn" value="Poka� przypisane "/>
					</ds:available>
					<input type="button" id="clearAllFields" name="clearAllFields" class="btn" value="<ds:lang text='WyczyscWszystkiePola'/>" onclick="clearFields()"/>					
					<ww:if test="popup">
						<input type="button" class="cancel_btn" onclick="window.close();" value="Zamknij"/>
					</ww:if>
				</td>
			</tr>
		</table>
		 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	</form>

	<script language="JavaScript">
	    function clearFields() {
	    	javascript:document.formul.reset();
    		document.getElementById('prettyRwaCategory').value = "";
    		document.getElementById('rwaId').value = '';
    		return false;
	    }

		// funkcja wywo�ywana przez okienko wyboru RWA
		function pickRwa(rootId, categoryId, rwaString)
		{
			document.getElementById('rwaId').value = categoryId;
			document.getElementById('prettyRwaCategory').value = rwaString;
		}
	</script>

	<script type="text/javascript">
		Calendar.setup({
			inputField     :    "cdateFrom",     // id of the input field
			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
			button         :    "calendar_cdateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "cdateTo",     // id of the input field
			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
			button         :    "calendar_cdateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
	</script>
</ww:if>

<ww:else>
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table width="100%" class="search">
		<th>
			<nobr>
				<%--	<a href="<ww:url value="getSortLink('officeId', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				<a href="<ww:url value="getSortLink('officeId', false)"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
					<ww:if test="(sortField == 'officeId' && ascending == false) || sortField == null"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
					<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
				Znak teczki
				<a href="<ww:url value="getSortLink('officeId', true)"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
					<ww:if test="sortField == 'officeId' && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
					<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
				<%--	<a href="<ww:url value="getSortLink('officeId', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
			</nobr>
		</th>
		<th>
			Tytu�
		</th>
		<th>
			Rok
		</th>
		<th>
			Referent
		</th>
		<th>
			Autor
		</th>
		<th>
			Kod kreskowy
		</th>
		<th>
			Dni na za�atwienie
		</th>
		
		<ww:iterator value="results">
			<ww:set name="officeId" scope="page" value="officeId"/>
			<ww:set name="name" scope="page" value="name"/>
			<tr>
				<td>
					<ww:if test="popup">
						<a href="javascript:void(pickCase(<ww:property value="id"/>, '<ww:property value="officeId"/>', '<ww:property value="mode"/>'))" name="<ww:property value="officeId"/>">
							<c:out value="${officeId}" escapeXml="no"/></a>
					</ww:if>
					<ww:else>
						<ww:set name="c"/>
						<a href="<ww:url value="getFolderLink(#c)"/>"><c:out value="${officeId}" escapeXml="no"/></a>
					</ww:else>
				</td>
				<td>
					<c:out value="${name}" escapeXml="no"/>
				</td>
				<td>
					<ww:property value="year"/>
				</td>
				<td>
					<ww:property value="clerk"/>
				</td>
				<td>
					<ww:property value="author"/>
				</td>
				<td>
					<ww:property value="barcode"/>
				</td>
				<td>
					<ww:property value="days"/>
				</td>
			</tr>
		</ww:iterator>
	</table>

	<ww:set name="pager" scope="request" value="pager" />
	<table width="100%">
		<tr>
			<td align="center">
				<jsp:include page="/pager-links-include.jsp"/>
			</td>
		</tr>
	</table>
	
	<input type="button" value="Nowe wyszukiwanie" class="btn" onclick="document.location.href='<ww:url value="'/office/find-folders.action'"><ww:param name="'popup'" value="popup"/></ww:url>';"/>
	 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:else>
