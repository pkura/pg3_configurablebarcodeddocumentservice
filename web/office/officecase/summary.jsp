<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N summary.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Sprawa</h1>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<table>
    <tr>
        <td>Numer sprawy:</td>
        <td><ww:property value="officeId"/></td>
    </tr>
    <tr>
        <td>Przewidywana data zakończenia:</td>
        <td><ds:format-date value="finishDate" pattern="dd-MM-yy"/></td>
    </tr>
    <tr>
        <td>Użytkownik tworzący:</td>
        <td><ww:property value="author"/></td>
    </tr>
    <tr>
        <td>RWA:</td>
        <td><ww:property value="rwa"/></td>
    </tr>
    <tr>
        <td>Teczka:</td>
        <td><ww:property value="portfolioOfficeId"/></td>
    </tr>
    <tr>
        <td>Priorytet:</td>
        <td><ww:property value="priority"/></td>
    </tr>
    <tr>
        <td>Stan załatwienia:</td>
        <td><ww:property value="status"/></td>
    </tr>
</table>

<h3>Pisma</h3>

<table class="search">
    <tr>
        <th>Numer KO</th>
        <th>Nadawca</th>
        <th>Odbiorca</th>
        <th>Data przyjęcia</th>
    </tr>
</table>
<!--N koniec summary.jsp N-->