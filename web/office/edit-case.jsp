<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-case.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://hoohoo.mine.nu/tabs" prefix="tabs" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Sprawa</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />


<script language="JavaScript">
// funkcja wywolywana przez okienko wyboru sprawy
	function pickCase(id, caseId)
	{
		document.getElementById('precedentId').value = id;
		document.getElementById('precedentCaseId').innerHTML = caseId;
	}

	function clearPrecedent()
	{
		document.getElementById('precedentId').value = null;
		document.getElementById('precedentCaseId').innerHTML = '';
	}

	/*
	Pomieszane nazwy - w formularzu portfolioId oznacza Portfolio.id,
	za� portfolio oznacza Portfolio.portfolioId.
	*/
	function __pick_portfolio(id, portfolioId)
	{
		document.getElementById('portfolioId').value = id;
		document.getElementById('portfolio').value = portfolioId;
		document.getElementById('doSetPortfolio').value = 'true';
		document.forms[0].submit();
	}

	var windowName = 'jna9wp347thg';
</script>

<c:if test="${!delete}">
	<tabs:tabs var="tabs" selectedParameter="tabId" selectedDefaultId="main" selectedVar="selId" >
		<!-- tytu� strony -->
			
		<%--	<tabs:tab tabId="main" var="tab">
			<h3>Sprawa <c:out value="${case.caseId}"/>: <c:out value="${tab.title}"/></h3>
		</tabs:tab>
	
		<tabs:tab tabId="documents" var="tab">
			<h3>Sprawa <c:out value="${case.caseId}"/>: <c:out value="${tab.title}"/></h3>
		</tabs:tab>
	
		<tabs:tab tabId="remarks" var="tab">
			<h3>Sprawa <c:out value="${case.caseId}"/>: <c:out value="${tab.title}"/></h3>
		</tabs:tab>
	
		<tabs:tab tabId="history" var="tab">
			<h3>Sprawa <c:out value="${case.caseId}"/>: <c:out value="${tab.title}"/></h3>
		</tabs:tab>	--%>
		
		<!-- zak�adki na dole, pod tytu�em bie��cej zak�adki w H3 -->
		<ds:available test="!layout2">
		<c:forEach items="${tabs}" var="tab" varStatus="status" >
			<c:choose>
				<c:when test="${tab.id == selId}">
					<a class="highlightedText" href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
				</c:when>
				<c:otherwise>
					<a href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
				</c:otherwise>
			</c:choose>
			<c:if test="${!status.last}">
				<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
			</c:if>
		</c:forEach>
		</ds:available>
		
		<ds:available test="layout2">
			<div id="middleMenuContainer">
				<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
				<c:forEach items="${tabs}" var="tab" varStatus="status" >
					<a href='<c:out value="${tab.link}"/>' 
					<c:choose>
						<c:when test="${tab.id == selId}">
						class="highlightedText"
						</c:when>
					</c:choose>
					><c:out value="${tab.title}"/>
					</a>
					
					<%--
					<c:choose>
						<c:when test="${tab.id == selId}">
							<a class="highlightedText" ><c:out value="${tab.title}"/></a>
						</c:when>
						<c:otherwise>
							<a href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
						</c:otherwise>
					</c:choose>
					<c:if test="${!status.last}">
						<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
					</c:if>
					--%>
				</c:forEach>
			</div>
		</ds:available>
	
		<edm-html:errors />
		<edm-html:messages />
	
		<p></p>
	
		<c:if test="${canView}">
			<!-- Zak�adka: Podsumowanie -->
			<tabs:tab tabId="summary" var="tab">
			<ds:available test="layout2">
				<div id="middleContainer"> <!-- BIG TABLE start -->
			</ds:available>
				<table class="summaryTable">
					<tr>
						<td valign="top">
							Znak sprawy:
						</td>
						<td valign="top">
							<b><c:out value="${case.officeId}"/></b>
						</td>
		            
						<td valign="top">
							Przewidywany termin zako�czenia:
						</td>
						<td valign="top">
							<b><c:out value="${case.finishDate}"/></b>
						</td>
					</tr>
					<tr>
						<td valign="top">
							U�ytkownik tworz�cy:
						</td>
						<td valign="top">
							<b><c:out value="${caseAuthor}"/></b>
						</td>
		            
						<td valign="top">
							Kategoria RWA:
						</td>
						<td valign="top">
							<b><c:out value="${rwa.description} (${rwa.rwa})"/></b>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Znak teczki:
						</td>
						<td valign="top">
							<b><c:out value="${portfolio}"/></b>
						</td>
		           
						<td valign="top">
							Priorytet sprawy:
						</td>
						<td valign="top">
							<b><c:out value="${casePriority}"/></b>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Stan za�atwienia:
						</td>
						<td valign="top">
							<b><c:out value="${caseStatus}"/></b>
						</td>
						
						<td valign="top">
							Tytu� sprawy:
						</td>
						<td valign="top">
							<b><c:out value="${caseTitle}"/></b>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Opis:
						</td>
						<td>
							<b><c:out value="${description}"/></b>
						</td>
					</tr>
				</table>

				<h3>Pisma</h3>

				<c:if test="${empty documents}">
					<i>Nie znaleziono dokument�w</i>
				</c:if>
				
				<c:if test="${!empty documents}">
					<table width="100%" class="search" style="display: inline;">
						<tr>
							<th>
								l.p.
							</th>
							<th>
								Numer KO
							</th>
							<th>
								Nadawca
							</th>
							<th>
								Odbiorca
							</th>
							<th>
								Data przyj�cia
							</th>
							<th>
								Symbol w sprawie
							</th>
							<th>
								Rodzaj pisma
							</th>
							<th>
								Opis pisma
							</th>
						</tr>
						
						<% int lp = 0; %> <%-- numer porz�dkowy pisma w sprawie --%>
						<c:forEach items="${documents}" var="bean">
							<c:if test="${viewOnly}">
								<tr>
									<td>
										<%-- numer l.p. pisma w sprawie --%>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><%=++lp %></a>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.officeNumber}"/></a>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.sender}"/></a>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.recipient}"/></a>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><fmt:formatDate value="${bean.incomingDate}" pattern="dd-MM-yyyy"/></a>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.caseDocumentIdentifier}"/></a>
									</td>
									<td>
										<c:if test="${bean.incoming}">
											<b>przychodz�ce</b>
										</c:if>
										<c:if test="${bean.internal}">
											<b>wewn�trzne</b>
										</c:if>
										<c:if test="${bean.outgoing}">
											<b>wychodz�ce</b>
										</c:if>
									</td>
									<td>
										<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.description}"/></a>
									</td>
								</tr>
							</c:if>
							
							<c:if test="${!viewOnly}">
								<tr>
									<td>
										<%--numer porz�dkowy pisma w sprawie --%>
										<a href="<c:out value="${bean.link}"/>"><%=++lp%></a>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.officeNumber}"/></a>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.sender}"/></a>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>">
										<%--	<c:out value="${bean.recipient}"/>	--%>
										<c:forEach items="${bean.recipients}" var="recipient" varStatus="status" >
											<c:out value="${recipient.summary}"/>
											<c:if test="${!status.last}">, </c:if>
										</c:forEach></a>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>"><fmt:formatDate value="${bean.incomingDate}" pattern="dd-MM-yyyy"/></a>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.caseDocumentIdentifier}"/></a>
									</td>
									<td>
										<c:if test="${bean.incoming}">
											przychodz�ce
										</c:if>
										<c:if test="${bean.internal}">
											wewn�trzne
										</c:if>
										<c:if test="${bean.outgoing}">
											wychodz�ce
										</c:if>
									</td>
									<td>
										<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.description}"/></a>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</table>
				</c:if>
				
				<ds:available test="layout2">
					<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
					<div class="bigTableBottomSpacer">&nbsp;</div>
					</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
				</ds:available>
			</tabs:tab>
			
			<!-- Zak�adka: Og�lne -->
			<tabs:tab tabId="main" var="tab">
				<html:form action="/office/edit-case" styleId="form" >
					<html:hidden property="id"/>
					<html:hidden property="precedentId" styleId="precedentId" />
					<html:hidden property="application"/>
					<html:hidden property="tabId"/>
					<html:hidden property="portfolioId" styleId="portfolioId"/>
					
					<input type="hidden" name="doSetPortfolio" id="doSetPortfolio"/>
					
					<ds:available test="layout2">
						<div id="middleContainer"> <!-- BIG TABLE start -->
					</ds:available>
					
					<c:if test="${suggestedSequenceId != null}">
						<html:hidden property="officeIdPrefix"/>
						<html:hidden property="officeIdMiddle"/>
						<html:hidden property="officeIdSuffix"/>
						<html:hidden property="suggestedOfficeId"/>
						<html:hidden property="suggestedSequenceId"/>
						
						<c:if test="${actionType eq 'settingPortfolio'}">
							<p>Zamierzasz przenie�� spraw� do innej teczki. Zostanie jej przydzielony symbol
							<c:out value="${suggestedOfficeId}"/>
							oraz pozycja
							<c:out value="${suggestedSequenceId}"/>
							(
							<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>
							)</p>
						</c:if>
						<c:if test="${actionType eq 'beforeChangingNumber'}">
							<p>Aktualny znak sprawy to
							<c:out value="${suggestedOfficeId}"/>
							oraz aktualna pozycja to
							<c:out value="${suggestedSequenceId}"/>
							(
							<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>
							)</p>
						</c:if>
						<c:if test="${actionType eq 'changingNumber'}">
							<p>Sprawie ma by� przydzielony symbol
							<c:out value="${suggestedOfficeId}"/>
							oraz pozycja
							<c:out value="${suggestedSequenceId}"/>
							(
							<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>
							)</p>
						</c:if>
						
						<div id="breakdown" style="display: none">
							<html:hidden property="customOfficeId" styleId="customOfficeId"/> <!-- bool -->
							Lp:
							<html:text property="customSequenceId" styleClass="txt" size="4" maxlength="6"/>.
							&nbsp;
							<html:text property="customOfficeIdPrefix" styleClass="txt" size="6" maxlength="20" />
							<html:text property="customOfficeIdPrefixSeparator" styleClass="txt" size="1" maxlength="1" />
							<html:text property="customOfficeIdMiddle" styleClass="txt" size="15" maxlength="50"/>
							<html:text property="customOfficeIdSuffixSeparator" styleClass="txt" size="1" maxlength="1" />
							<html:text property="customOfficeIdSuffix" styleClass="txt" size="6" maxlength="20" />
						</div>
						
						<p></p>
						
						<c:if test="${'settingPortfolio' eq actionType}">
							<input type="submit" name="doFinishSetPortfolio" value="Przenie� spraw�" class="btn"/>
						</c:if>
						<c:if test="${'settingPortfolio' ne actionType}">
							<input type="submit" name="doChangeNumber" value="Zapisz numer" class="btn"/>
						</c:if>
						
						<script language="JavaScript">
							function customOfficeId()
							{
								toggle_div('breakdown', true);
								document.getElementById('customOfficeId').value = 'true';
							}
							
							function toggle_div(id, show)
							{
								var div = document.getElementById(id);
								div.style.display = show ? '' : 'none';
							}
						</script>
					</c:if>
					
					<div id="formDiv" style="overflow: hidden;" >
						<table>
							<%--	<tr>
								<td>
									Zamkni�ta:
								</td>
								<td>
									<c:if test="${case.closed}">Tak</c:if>
									<c:if test="${!case.closed}">Nie</c:if>
								</td>
							</tr>	--%>
							<tr>
								<td valign="top">
									Znak sprawy:
								</td>
								<td valign="top">
									<b><c:out value="${case.officeId}"/></b>
								</td>
							</tr>
							<tr>
								<td valign="top">
									Data za�o�enia:
								</td>
								<td valign="top">
									<b><fmt:formatDate value="${case.openDate}" pattern="dd-MM-yyyy"/></b>
								</td>
							</tr>
							<tr>
								<td valign="top">
									U�ytkownik tworz�cy:
								</td>
								<td valign="top">
									<b><c:out value="${caseAuthor}"/></b>
								</td>
							</tr>
							<tr>
								<td valign="top">
									Kategoria RWA:
								</td>
								<td valign="top">
									<b><c:out value="${rwa.description} (${rwa.rwa})"/></b>
								</td>
							</tr>
							<tr>
								<td>
									Znak teczki<span class="star">*</span>:
								</td>
								<td>
									<html:text property="portfolio" styleId="portfolio" styleClass="txt" readonly="true" />
									<c:if test="${canSetPortfolio}">
										<input type="button" class="btn" value="Wybierz"
											onclick="window.open('<c:out value="${pageContext.request.contextPath}/office/pick-portfolio.action"/>', 'pickportfolio', 'width=500,height=450,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes')"/>
									</c:if>
									<c:if test="${!empty editCase.portfolioId}">
										<a href="<c:out value="${pageContext.request.contextPath}/office/edit-portfolio.action?id=${editCase.portfolioId}"/>">(przejd� do teczki)</a>
									</c:if>
								</td>
							</tr>
							<tr>
								<td>
									Referent sprawy:
								</td>
								<td>
									<html:select property="assignedUser" styleClass="sel">
										<html:optionsCollection name="clerks"/>
									</html:select>
								</td>
								<%--	<td>
									<c:out value="${caseAssignedUser}"/>
								</td>	--%>
							</tr>
							<tr>
								<td>
									Przewidywany termin zako�czenia<span class="star">*</span>:
								</td>
								<td>
									<c:if test="${SPRAWA_ZMIANA_TERMINU}">
										<html:text property="finishDate" size="10" maxlength="10" styleClass="txt" styleId="finishDate"/>
										<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
											id="calendar_finishDate_trigger" style="cursor: pointer; border: 1px solid red;"
											title="Date selector" onmouseover="this.style.background='red';"
											onmouseout="this.style.background=''"/>
									</c:if>
									<c:if test="${!SPRAWA_ZMIANA_TERMINU}">
										<html:text property="finishDate" size="10" maxlength="10" styleClass="txt" disabled="true" />
									</c:if>
								</td>
							</tr>
							<tr>
								<td>
									Priorytet sprawy<span class="star">*</span>:
								</td>
								<c:if test="${viewOnly}">
									<td>
										<html:select property="priority" styleClass="sel" disabled="true" >
											<html:optionsCollection name="priorityBeans"/>
										</html:select>
									</td>
								</c:if>
								<c:if test="${!viewOnly}">
									<td>
										<html:select property="priority" styleClass="sel">
											<html:optionsCollection name="priorityBeans"/>
										</html:select>
									</td>
								</c:if>
							</tr>
							<tr>
								<td>
									Stan za�atwienia:
								</td>
								<c:if test="${viewOnly}">
									<td>
										<ww:select id="status" name="'status'" list="statusBeans" value="status" cssClass="'sel'" />
											<!--	<html:select property="status" styleClass="sel" disabled="true" >
											<html:optionsCollection name="statusBeans"/>
										</html:select>	-->
									</td>
								</c:if>
								<c:if test="${!viewOnly}">
									<td>
										<html:select property="status" styleClass="sel" >
											<html:optionsCollection name="statusBeans"/>
										</html:select>
																	
										<script type="text/javascript">										      
										      var statusOnLoad = document.getElementsByName('status')[0].value;
										      //alert('stat' + statusOnLoad); 
										</script>
									</td>
								</c:if>
							</tr>
							<tr>
								<td>
									Tytu� sprawy<span class="star">*</span>:
								</td>
								<c:if test="${!viewOnly}">
									<td>
										<html:text property="caseTitle" styleClass="txt" />
									</td>
								</c:if>
								<c:if test="${viewOnly}">
									<td>
										<c:out value="${caseTitle}"/>
									</td>
								</c:if>
							</tr>
							<tr>
								<td>
									Opis sprawy:
								</td>
								<c:if test="${!viewOnly}">
									<td>
										<html:textarea property="description" rows="8" cols="90" styleClass="txt" />
									</td>
								</c:if>
								<c:if test="${viewOnly}">
									<td>
										<c:out value="${description}"/>
									</td>
								</c:if>
							</tr>
							<c:if test="${!empty grantRequestId}">
								<tr>
									<td>
										Wniosek o stypendium:
									</td>
									<td>
										<a href="<c:out value="${pageContext.request.contextPath}"/>/record/grants/grant-request.action?id=<c:out value="${grantRequestId}"/>"><c:out value="${grantRequestSequenceId}"/></a>
									</td>
								</tr>
							</c:if>
							<%--	<tr>
								<td>
									Poprzednik sprawy:
								</td>
								<c:if test="${viewOnly}">
									<td>
										<c:out value="${case.precedent.caseId}"/>
									</td>
								</c:if>
								<c:if test="${!viewOnly and !case.closed}">
									<td>
										<span id="precedentCaseId">
											<a href="<c:out value="${precedentLink}"/>"><c:out value="${case.precedent.caseId}"/></a>
										</span>
										<c:if test="${!empty case.precedent}">
											<i><a href="javascript:void(clearPrecedent());">(wyczy��)</a></i>
										</c:if>
										<input type="button" class="btn" value="Wybierz spraw�" onclick="javascript:void(window.open('<c:out value='${caseRegistryPopupLink}'/>', null, 'width=600,height=400,menubar=no,toolbar=no,status=no,location=no'));"/>
									</td>
								</c:if>
							</tr>	--%>
						</table>
					
						<c:if test="${!viewOnly}">
							<input type="submit" name="doUpdate" value="Zapisz" class="btn" onclick="return onUpdate();"/>
							<input type="submit" name="doChangeNumber" value="Zmie� znak sprawy" class="btn" <c:if test="${!canChangeCaseNumber}">disabled="disabled"</c:if>/>
							<input type="submit"  name="doDelete" value="Usu�" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}/office/edit-case.do?delete=true&Id=${Id}"/>';" class="btn"/>
							<a href="<c:out value="${pageContext.request.contextPath}/office/edit-case.do?delete=true&Id=${case.id}"/>"></a>
							<%--	<input type="submit" name="doClose" value="Zamknij spraw�" class="btn" onclick="return confirm('Na pewno zamkn�� spraw�?');"/>	--%>
						</c:if>
						
						<ds:available test="metrykaSprawy">
							<input type="button" value="Metryka sprawy" class="btn" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}/office/edit-case.action?doMetrics=true&officeCaseId=${case.id}"/>';"/>
						</ds:available>

						<ds:available test="paczkaArchiwalna">
                            <input type="submit" name="doArchivePackage" value="Paczka archiwalna" class="btn" />
                        </ds:available>

						<script language="JavaScript">
							function onUpdate()
							{
								var stat = document.getElementsByName('status');
								if(stat[0].value==statusOnLoad)
								{
								
								}
								
								else
								{
									if(stat[0].value == 2 || stat[0].value == 3 )
									{
										return confirm('Zmieni�e� status sprawy na zako�czona. Spowoduje to zamkni�cie sprawy oraz pism znajduj�cych si� w sprawie. Czy na pewno to zrobi�?')
									}
								}
								return true;
							}
						</script>
						
						<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
					</div>
				
					<c:if test="${suggestedSequenceId != null}">
						<script>
							document.getElementById('formDiv').style.display = "none";
						</script>
					</c:if>
					
					<p>
						<%--	<c:if test="${!viewOnly}">
							<input type="button" value="Nowe wyszukiwanie" class="btn" onclick="document.location.href='<c:out value="${caseRegistryLink}"/>';"/></p>
						</c:if>	--%>
						<c:if test="${!empty returnUrl}">
							<a href="<c:out value="${returnUrl}"/>">Powr�t do wynik�w wyszukiwania</a>
						</c:if>
					</p>
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>	
				</html:form>
				
				<script type="text/javascript">
					<c:if test="${SPRAWA_ZMIANA_TERMINU}">
						Calendar.setup({
							inputField     :    "finishDate",     // id of the input field
							ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
							button         :    "calendar_finishDate_trigger",  // trigger for the calendar (button ID)
							align          :    "Tl",           // alignment (defaults to "Bl")
							singleClick    :    true
						});
					</c:if>
				</script>
			</tabs:tab>
		
			<!-- Zak�adka: Dokumenty w sprawie -->
			<tabs:tab tabId="documents" var="tab">
				<html:form action="/office/edit-case" styleId="form" >
					<html:hidden property="id"/>
					<html:hidden property="precedentId" styleId="precedentId" />
					<html:hidden property="application"/>
					<html:hidden property="tabId"/>
					
					<ds:available test="layout2">
						<div id="middleContainer"> <!-- BIG TABLE start -->
					</ds:available>
					
					<c:if test="${empty documents}">
						<i>Nie znaleziono dokument�w</i>
					</c:if>
					
					<c:if test="${!empty documents}">
						W sprawie
						<b><c:out value="${case.officeId}"/></b>
						znajduj� sie nast�puj�ce pisma:
						<table width="100%" class="search">
							<tr>
								<th>
									Numer KO
								</th>
								<th>
									Nadawca
								</th>
								<th>
									Odbiorca
								</th>
								<th>
									Data przyj�cia
								</th>
								<th>
									Symbol w sprawie
								</th>
								<th>
									Rodzaj pisma
								</th>
								<th>
									Opis pisma
								</th>
							</tr>
							
							<c:forEach items="${documents}" var="bean">
								<c:if test="${viewOnly}">
									<tr>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.officeNumber}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.sender}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.recipient}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><fmt:formatDate value="${bean.incomingDate}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.caseDocumentIdentifier}"/></a>
										</td>
										<td>
											<c:if test="${bean.incoming}">przychodz�ce</c:if>
											<c:if test="${bean.internal}">wewn�trzne</c:if>
											<c:if test="${bean.outgoing}">wychodz�ce</c:if>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.link}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.description}"/></a>
										</td>
									</tr>
								</c:if>
								
								<c:if test="${!viewOnly}">
									<tr>
										<td>
											<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.officeNumber}"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.sender}"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.link}"/>">
												<%--	<c:out value="${bean.recipient}"/>	--%>
												<c:forEach items="${bean.recipients}" var="recipient" varStatus="status" >
													<c:out value="${recipient.summary}"/><c:if test="${!status.last}">, </c:if>
												</c:forEach>
											</a>
										</td>
										<td>
											<a href="<c:out value="${bean.link}"/>"><fmt:formatDate value="${bean.incomingDate}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.caseDocumentIdentifier}"/></a>
										</td>
										<td>
											<c:if test="${bean.incoming}">przychodz�ce</c:if>
											<c:if test="${bean.internal}">wewn�trzne</c:if>
											<c:if test="${bean.outgoing}">wychodz�ce</c:if>
										</td>
										<td>
											<a href="<c:out value="${bean.link}"/>"><c:out value="${bean.description}"/></a>
										</td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
					</c:if>
					
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>		
				</html:form>
			</tabs:tab>
		
			<!-- Zak�adka: Uwagi -->
			<tabs:tab tabId="remarks" var="tab">
				<html:form action="/office/edit-case" styleId="form" >
					<html:hidden property="id"/>
					<html:hidden property="precedentId" styleId="precedentId" />
					<html:hidden property="application"/>
					<html:hidden property="tabId"/>
					
					<ds:available test="layout2">
						<div id="middleContainer"> <!-- BIG TABLE start -->
					</ds:available>
					
					<table width="100%">
						<c:forEach items="${remarks}" var="bean">
							<tr>
								<td class="historyAuthor">
									<c:out value="${bean.author}"/>
								</td>
								<td align="right" class="historyDate">
									<fmt:formatDate value="${bean.ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="historyContent">
									<c:out value="${bean.content}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
					
					<c:if test="${!viewOnly and !case.closed}">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									Tre�� uwagi:
								</td>
							</tr>
							<tr>
								<td>
									<html:textarea property="remarkContent" rows="8" cols="90" styleClass="txt" />
								</td>
							</tr>
							<tr>
								<td>
									<html:submit property="doAddRemark" styleClass="btn" >Dodaj uwag�</html:submit>
								</td>
							</tr>
						</table>
					</c:if>
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>		
				</html:form>
			</tabs:tab>
		
		<!-- Zak�adka: sprawy powiazane  -->
		<ds:available test="CaseToCase">
			<tabs:tab tabId="casetocase" var="tab">
				<html:form action="/office/edit-case" styleId="form" >
					<html:hidden property="id"/>
					<html:hidden property="precedentId" styleId="precedentId" />
					<html:hidden property="application"/>
					<html:hidden property="tabId"/>
					<html:hidden property="selectetIdFromSearchCases" styleId="selectetIdFromSearchCases"/>
					<input type="hidden" name="doAddCaseToCaseFromSearch" id="doAddCaseToCaseFromSearch"/>
						
					<ds:available test="layout2">
						<div id="middleContainer"> <!-- BIG TABLE start -->
					</ds:available>
					
				
					<c:if test="${empty listLinkedCases }">
						<h3> Nie znaleziono powi�zanych spraw  dla sprawy : 
						<b><c:out value="${case.officeId}"/></b></h3>
					</c:if>
					
					
					<c:if test="${listLinkedCases !=null && !empty listLinkedCases}">
						<h3> Sprawa 
						<b><c:out value="${case.officeId}"/></b>
						Powi�zana jest z nastepuj�cymi sprawami : </h3>
						<table width="100%" class="search">
							<tr>
								<th>
									Powaizanie przez
								</th>
								<th>
									Data powi�zania
								</th>
								<th>
									Symbol sprawy
								</th>
								<th>
									Tytu� sprawy 
								</th>
								<th>
									Autor sprawy
								</th>
								<th>
									Otwarta dnia
								</th>
								<th>
									Przewidywany termin zako�czenia
								</th>
								
							</tr>
							<c:forEach items="${listLinkedCases}" var="bean">
								<c:if test="${viewOnly}">
									<tr>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.creatingUserName}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><fmt:formatDate value="${bean.creationTime}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.caseOfficeId}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.caseTitle}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><c:out value="${bean.caseAuthor}"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><fmt:formatDate value="${bean.caseOpenDate}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="javascript:void(window.open('<c:out value='${bean.linkToCase}'/>', windowName, 'width=700,height=500,scrollbars=yes,menubar=no,toolbar=no,status=no,location=no'));"><fmt:formatDate value="${bean.caseFinishDate}" pattern="dd-MM-yyyy"/></a>
										</td>
									</tr>
								</c:if>
								
								<c:if test="${!viewOnly}">
									<tr>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><c:out value="${bean.creatingUserName}"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><fmt:formatDate value="${bean.creationTime}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><c:out value="${bean.caseOfficeId}"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><c:out value="${bean.caseTitle}"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><c:out value="${bean.caseAuthor}"/></a>
										</td>
									
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><fmt:formatDate value="${bean.caseOpenDate}" pattern="dd-MM-yyyy"/></a>
										</td>
										<td>
											<a href="<c:out value="${bean.linkToCase}"/>"><fmt:formatDate value="${bean.caseFinishDate}" pattern="dd-MM-yyyy"/></a>
										</td>
									</tr>
								</c:if>
							</c:forEach>
						</table>
					</c:if>
					<td>
		</td>

					<table width="800">
							
						<tr><td>
						<td>
						</tr>
						<tr>
							<td><h4><b>Wyszukaj spraw� do powi�zania :</b></h4></td>
							<td><h3><b><a href="javascript:void(window.open('<ww:url value="'/office/find-cases.action?popup=true'"/>', 'Rwa', 'width=750, height=600, menubar=no, toolbar=no, status=no, location=no, scrollbars=yes, resizable=yes'));"
								name="doAddCaseToCase"> <ds:lang text="Szukaj spawy" /></a></b></h3></td>
						</tr>
						
					
					<c:if test="${listCasesToLink !=null && !empty listCasesToLink}">
						<tr>
						<td><h4>Lub:</h4></td></tr>
						<tr>
							<td>Wybie� spraw� do powi�zania :</td>
							<td><html:select property="caseToCaseIdToLink"
									styleClass="sel" disabled="false">
									<html:optionsCollection name="listCasesToLink" />
								</html:select></td>
							<td><input type="submit" name="doAddCaseToCase"
								value="dodaj wybran� spraw� jako powiazan�" class="btn" /></td>
						</tr>
					</c:if>
					
					<c:if test="${caseToCaseListToDelete !=null && !empty caseToCaseListToDelete}">
						<tr>
						<td><h4>Lub:</h4></td>
						</tr>
						
						<tr>
						
							<td>Wybie� powi�zanie do usuni�cia:</td>
							<td><html:select property="caseToCaseIdToDelete"
									styleClass="sel" disabled="false">
									<html:optionsCollection name="caseToCaseListToDelete" />
								</html:select></td><td><input type="submit" name="doDelateCaseToCase"
								value="Usu� powiazanie z t� spraw�" class="btn" /></td>
						
						</tr>
					</c:if>
					</table>
  
					<%-- 	
					    
					   <a href="javascript:void(window.open('<ww:url value="'/office/find-cases.action?popup=true'"/>', 'Rwa', 'width=750, height=600, menubar=no, toolbar=no, status=no, location=no, scrollbars=yes, resizable=yes'));" name="addToCase">
			<ds:lang text="Szukaj"/></a><br>
					   </tr>
					   <tr>
					<td>Usuniecie powiazania :</td>
						<td><html:select property="caseToCaseIdToDelete" styleClass="sel"
								disabled="false">
								<html:optionsCollection name="caseToCaseListToDelete" />
							</html:select>
					   </td>
					</tr> --%>

						<%-- <ds:available test="metrykaSprawy">
							<input type="submit" name="doAddCaseToCase" value="dodaj link do sprawy " class="btn" />
						</ds:available>	 --%>		
					
					
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>		
				</html:form>
				
		

			</tabs:tab>
		</ds:available>
			<!-- Zak�adka: Historia zmian -->
			<tabs:tab tabId="history" var="tab">
				<html:form action="/office/edit-case" styleId="form" >
					<html:hidden property="id"/>
					<html:hidden property="precedentId" styleId="precedentId" />
					<html:hidden property="application"/>
					<html:hidden property="tabId"/>
					
					
					<ds:available test="layout2">
						<div id="middleContainer"> <!-- BIG TABLE start -->
					</ds:available>
					
					<table width="100%">
						<c:forEach items="${audit}" var="bean">
							<tr>
								<td class="historyAuthor">
									<c:out value="${bean.user}"/>
								</td>
								<td align="right" class="historyDate">
									<fmt:formatDate value="${bean.ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/>
								</td>
							</tr>
							<tr>
								<td colspan="2" class="historyContent">
									<c:out value="${bean.description}"/>
								</td>
							</tr>
						</c:forEach>
					</table>
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>			
				</html:form>
			</tabs:tab>
		</c:if>
	</tabs:tabs>
</c:if>

<c:if test="${delete}">
	<edm-html:errors />
	<edm-html:messages />
	
	<!-- Usuni�cie spray -->
	<html:form action="/office/edit-case" styleId="form" >
		<html:hidden property="id"/>
		<html:hidden property="precedentId" styleId="precedentId" />
		<html:hidden property="application"/>
		<html:hidden property="tabId"/>
		<table width="100%">
			<tr>
				<td>
					Podaj pow�d zamkni�cia sprawy:
				</td>
			</tr>
			<tr>
				<td>
					<html:textarea property="reason" rows="8" cols="90"	styleClass="txt" />
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit"  name="doDelete" value="Usu�" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}/office/edit-case.do?delete=true&Id=${Id}"/>';" class="btn"/>
				</td>
			</tr>
		</table>
	</html:form>
</c:if>  

<script language="JavaScript">
	
    // funkcja wywolywana przez okienko wyboru sprawy
    function pickCase(id, caseId, mode)
    {
       if (!confirm('Na pewno powi�za� Sprawe ' +caseId+ ' do sprawy  ?'))
           return;
       
        document.getElementById('selectetIdFromSearchCases').value = id;
		document.getElementById('doAddCaseToCaseFromSearch').value = 'true';
		document.forms[0].submit();
    }
    
</script>