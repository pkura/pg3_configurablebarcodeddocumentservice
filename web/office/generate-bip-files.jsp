<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1">Generowanie pliku dla BIP</h1>
<ds:ww-action-errors />
<ds:ww-action-messages />
<form action="<ww:url value="'/office/file-generator-for-any-bip-files.action'"/>" method="post">


<table>
	<tr>
		<td>
			<ds:lang text="UsunacPlikiBipZFolderu"/>:
			<ww:checkbox name="'deleteBipFiles'" value="deleteBipFiles" fieldValue="true"/>
			<ds:submit-event value="'Generuj'" name="'doGenerate'" cssClass="'btn'"/>
		</td>
	</tr>
	<tr>
		<td>
			Poni�ej widzisz pliki wygenerowane ostatnio. Je�li wygenerujesz nowe, te widoczne poni�ej zostan� skasowane.
		</td>
	</tr>
	<ww:iterator value="files">
		<tr>
			<td>
				<a href="<ww:url value="'/office/file-generator-for-any-bip-files.action'">
								<ww:param name="'fileName'" value="name"/>
								<ww:param name="'doGetFile'" value="'true'"/></ww:url>">				
				<ww:property value="getName()"/>
				</a>
				
			</td>
		</tr>
	</ww:iterator>
</table>


</form>