<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="Dzienniki"/> </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/division-journals.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<ww:hidden name="'divisionGuid'"/>
<input type="hidden" name="doCreate" id="doCreate"/>
<input type="hidden" name="doDelete" id="doDelete"/>

<%--
<ul>
    <li><a href="<ww:url value="'/office/view-journal.do'"><ww:param name="'journalId'" value="mainIncoming.id"/></ww:url>">Główny dziennik pism przychodzących</a></li>
    <li><a href="<ww:url value="'/office/view-journal.do'"><ww:param name="'journalId'" value="mainOutgoing.id"/></ww:url>">Główny dziennik pism wychodzących</a></li>
</ul>
--%>

<table width="100%">
	<ww:if test="!SHOW_JOURNALS_WITHOUT_TREE">
		<tr>
		    <td width="250" valign="top">
		        <ww:property value="tree" escape="false"/>
		    </td>
		</tr>
	</ww:if>
<tr>
    <td valign="top">
        <p><ww:property value="division.prettyPath"/></p>
			
			<ww:if test="SHOW_JOURNALS_WITHOUT_TREE">
				<h4><ds:lang text="OtwarteDzienniki"/></h4>
			</ww:if>
			<ww:else>
            	<h4><ds:lang text="DziennikiDostepneWdzialeA"/></h4>
            </ww:else>

            <ww:if test="journals.size > 0 || division.root">
            <table>
                <tr>
                    <th></th>
                    <th><ds:lang text="Opis"/></th>
                    <th><ds:lang text="Rodzaj"/></th>
                    <th></th>
                </tr>
                <ww:if test="division.root || SHOW_JOURNALS_WITHOUT_TREE">
                    <tr>
                        <td><input type="checkbox" disabled="true"/></td>
                        <ww:if test="canViewJournals">
                            <td><a href="<ww:url value="'/office/view-in-journal.action'"><ww:param name="'id'" value="mainIncoming.id"/></ww:url>"><ds:lang text="GlownyDziennikPismPrzychodzacych"/></a></td>
                       		<td><ds:lang text="pismaPrzychodzace"/></td>
	                        <td></td>
	                        <td><ww:if test="canEditJournals">[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="mainIncoming.id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</ww:if></td>
                        </ww:if>
                        <ww:else>
                            <td><span title="<ds:lang text="BrakUprawnienDoPrzegladaniaTegoDziennika"/>"><ds:lang text="GlownyDziennikPismPrzychodzacych"/></span></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </ww:else>
                          </tr>
                    <tr>
                        <td><input type="checkbox" disabled="true"/></td>
                        <ww:if test="canViewJournals">
                            <td><a href="<ww:url value="'/office/view-out-journal.action'"><ww:param name="'id'" value="mainOutgoing.id"/></ww:url>"><ds:lang text="GlownyDziennikPismWychodzacych"/></a></td>
                        	<td><ds:lang text="pismaWychodzace"/></td>
	                        <td></td>
	                        <td><ww:if test="canEditJournals">[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="mainOutgoing.id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</ww:if></td>
                        </ww:if>
                        <ww:else>
                            <td><span title="<ds:lang text="BrakUprawnienDoPrzegladaniaTegoDziennika"/>"><ds:lang text="GlownyDziennikPismWychodzacych"/></span></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </ww:else>
                    </tr>
                    <ww:if test="mainInternal != null">
                    <tr>
                        <td><input type="checkbox" disabled="true"/></td>
                        <ww:if test="canViewJournals">
                            <td><a href="<ww:url value="'/office/view-int-journal.action'"><ww:param name="'id'" value="mainInternal.id"/></ww:url>"><ds:lang text="GlownyDziennikPismWewnetrznych"/></a></td>
                        	<td><ds:lang text="pismaWewnetrzne"/></td>
	                        <td></td>
	                        <td><ww:if test="canEditJournals">[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="mainInternal.id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</ww:if></td>
	                    </ww:if>
                        <ww:else>
                            <td><span title="<ds:lang text="BrakUprawnienDoPrzegladaniaTegoDziennika"/>"><ds:lang text="GlownyDziennikPismWewnetrznych"/></span></td>
                        	<td></td>
                        	<td></td>
                        	<td></td>
                        </ww:else>
                    </tr>
                    </ww:if>
                </ww:if>
                
                <ww:if test="availableJournals != null">
                	<ww:iterator value="availableJournals">
	                    <tr>
	                        <td><ww:checkbox name="'journalIds'" fieldValue="id"/></td>
	                            <ww:if test="incoming">
	                                <td><a href="<ww:url value="'/office/view-in-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
	                                        <ww:property value="description"/>
	                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
	                                    </a>
	                                </td>
	                            </ww:if>
	                            <ww:elseif test="outgoing">
	                                <td><a href="<ww:url value="'/office/view-out-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
	                                        <ww:property value="description"/>
	                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
	                                    </a>
	                                </td>
	                            </ww:elseif>
	                            <ww:elseif test="internal">
	                                <td><a href="<ww:url value="'/office/view-int-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
	                                        <ww:property value="description"/>
	                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
	                                    </a>
	                                </td>
	                            </ww:elseif>
	                        <td><ww:property value="typeDescription"/></td>
	                        <td></td>
	                        <td>[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
	                    </tr>
                	</ww:iterator>
                </ww:if>
                <ww:else><ww:iterator value="journals">
                    <tr>
                        <td><ww:checkbox name="'journalIds'" fieldValue="id"/></td>
                        <ww:if test="canViewJournals">
                            <ww:if test="incoming">
                                <td><a href="<ww:url value="'/office/view-in-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                        <ww:property value="description"/>
                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                                    </a>
                                </td>
                            </ww:if>
                            <ww:elseif test="outgoing">
                                <td><a href="<ww:url value="'/office/view-out-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                        <ww:property value="description"/>
                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                                    </a>
                                </td>
                            </ww:elseif>
                            <ww:elseif test="internal">
                                <td><a href="<ww:url value="'/office/view-int-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                        <ww:property value="description"/>
                                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                                    </a>
                                </td>
                            </ww:elseif>
                        </ww:if>
                        <ww:else>
                            <td><span title="<ds:lang text="BrakUprawnienDoPrzegladaniaTegoDziennika"/>">
                                    <ww:property value="description"/>
                                    <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                                </span>
                            </td>
                        </ww:else>
                        <td><ww:property value="typeDescription"/></td>
                        <td></td>
                        <td><ww:if test="canEditJournals">[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</ww:if></td>
                    </tr>
               </ww:iterator></ww:else>
            </table>
            </ww:if>

            <input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn"
            onclick="if (!confirm('<ds:lang text="NaPewnoUsunac"/>?')) return; document.getElementById('doDelete').value='true';"/>
		<ww:if test="SHOW_JOURNALS_WITHOUT_TREE">
			<h4><ds:lang text="WszystkieZamknieteDzienniki"/></h4>
		</ww:if>
		<ww:else>
        	<h4><ds:lang text="ZamknieteDzienniki"/></h4>
		</ww:else>
        <ww:if test="!empty">
			<ww:select name="'year'" list="years" cssClass="'sel'"/><input type="submit" value="<ds:lang text="Pokaz"/>" class="btn"/>
		</ww:if>
        <table>
        <ww:iterator value="closedJournals">
            <tr>
                <ww:if test="canViewJournals">
                    <ww:if test="incoming">
                        <td><a href="<ww:url value="'/office/view-in-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                <ww:property value="description"/> (<ww:property value="cyear"/>)
                                <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                            </a>
                        </td>
                    </ww:if>
                    <ww:elseif test="outgoing">
                        <td><a href="<ww:url value="'/office/view-out-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                <ww:property value="description"/> (<ww:property value="cyear"/>)
                                <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                            </a>
                        </td>
                    </ww:elseif>
                    <ww:elseif test="internal">
                        <td><a href="<ww:url value="'/office/view-int-journal.action'"><ww:param name="'id'" value="id"/></ww:url>">
                                <ww:property value="description"/> (<ww:property value="cyear"/>)
                                <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                            </a>
                        </td>
                    </ww:elseif>
                </ww:if>
                <ww:else>
                    <td><span title="<ds:lang text="BrakUprawnienDoPrzegladaniaTegoDziennika"/>">
                            <ww:property value="description"/> (<ww:property value="cyear"/>)
                            <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
                        </span>
                    </td>
                </ww:else>
                <td><ww:property value="typeDescription"/></td>
                <td></td>
                <td>[<a href="<ww:url value="'/office/edit-journal.action'"><ww:param name="'id'" value="id"/><ww:param name="'divisionGuid'" value="divisionGuid"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
            </tr>
        </ww:iterator>
        </table>

	<ds:available test="division.journals.NowyDziennik">
            <h4><ds:lang text="NowyDziennik"/></h4>

            <table cellspacing="0">
                <tr>
                    <td><ds:lang text="Nazwa"/>:</td>
                    <td><ww:textfield name="'description'" size="30" maxlength="510" cssClass="'txt'"/></td>
                </tr>
                <tr>
                    <td><ds:lang text="Symbol"/>:</td>
                    <td><ww:textfield name="'symbol'" size="20" maxlength="20" cssClass="'txt'" value="division.code"/></td>
                </tr>
                <tr>
                    <td><ds:lang text="Rodzaj"/>:</td>
                    <td><ww:select name="'type'" list="#@java.util.LinkedHashMap@{ 'incoming': getText('PismaPrzychodzace'), 'outgoing': getText('PismaWychodzace'), 'internal': getText('PismaWewnetrzne') }"
                        cssClass="'sel'" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="doCreate" value="<ds:lang text="Utworz"/>" class="btn"
                        onclick="document.getElementById('doCreate').value='true';"/></td>
                </tr>
            </table>
	</ds:available>
        </c:if>
    </td>
</tr>
</table>

</form>
