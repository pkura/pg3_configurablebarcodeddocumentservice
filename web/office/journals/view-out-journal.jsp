<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.web.office.journals.JournalActionBase,
				 pl.compan.docusafe.core.office.Person"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1><ds:lang text="Dziennik"/> </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h4><ww:property value="title"/></h4>

<form action="<ww:url value="baseLink"/>" method="post">
<ww:hidden name="'id'" value="id"/>
<ww:if test="internal">
     <ww:hidden name="'alldelivery'" value="true"/>
</ww:if>
<ds:lang text="PokazWpisy"/>:

<table cellspacing="0" cellpadding="0">

<tr>
     <td>
         
     </td>
 </tr>
<tr>
    <td><hr/></td>
</tr>

<tr>
    <td>
        <table>
        <tr>
            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_DAY %>" id="searchType_day" <ww:if test="searchType == 'day'">checked="true"</ww:if>/></td>
            <td><ds:lang text="zDnia"/> <ww:textfield name="'day'" id="day" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_day&quot;).checked = true'"  />
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_day" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_DAYRANGE %>" id="searchType_dayrange" <ww:if test="searchType == 'dayrange'">checked="true"</ww:if>/></td>
            <td><ds:lang text="odDnia"/> <ww:textfield name="'fromDay'" id="fromDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_fromDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                <ds:lang text="Godzina"/> : <input type="text" name="fromHour" id="fromHour" size="3" maxlength="2" class="txt" value="<ww:property value="fromHour"/>"/>
                <ds:lang text="doDnia"/> <ww:textfield name="'toDay'" id="toDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_toDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/> 
               <ds:lang text="Godzina"/> : <input type="text" name="toHour" id="toHour" size="3" maxlength="2" class="txt" value="<ww:property value="toHour"/>"/>
            </td>
        </tr>
         <ds:available test="officeNumber">
	        <ds:available test="dailyOfficeNumber">
		        <tr>
		        	<td colspan="2">
		        		<ds:lang text="NumerKO"/>
		        	</td>
		        </tr>
	        </ds:available>
	        <tr>
	            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_NUMRANGE %>" id="searchType_numrange" <ww:if test="searchType == 'numrange'">checked="true"</ww:if>/></td>
	            <td>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeru"/> </ds:available>
	            	<input type="text" name="fromNum" id="fromNum" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="fromNum"/>"/>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeru"/> </ds:available>
	                <input type="text" name="toNum" id="toNum" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="toNum"/>"/>
	            </td>
	        </tr>
	     </ds:available>
        <ds:available test="dailyOfficeNumber">
	        <ds:available test="officeNumber">
		        <tr>
		        	<td colspan="2">
		        		<ds:lang text="DziennyNumerKO"/>
		        	</td>
		        </tr>
	        </ds:available>
	        <tr>
	            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_NUMRANGE_DAILY %>" id="searchType_numrange_daily" <ww:if test="searchType == 'numrangedaily'">checked="true"</ww:if>/></td>
	            <td>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeru"/> </ds:available>
	            	<input type="text" name="fromNumD" id="fromNumD" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="fromNumD"/>"/>
	                <ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeru"/> </ds:available> 
	                <input type="text" name="toNumD" id="toNumD" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="toNumD"/>"/>
	            	<ds:lang text="zDnia"/> <ww:textfield name="'dayDailyOfficeNumber'" id="dayDailyOfficeNumber" cssClass="'txt'" size="10" maxlength="10" />
               		<img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_dayDailyOfficeNumber" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
	            </td>
	        </tr>
        </ds:available>
        <tr>
            <td valign="top"><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_NUMLIST %>" id="searchType_numlist" <ww:if test="searchType == 'numlist'">checked="true"</ww:if>/></td>
            <td><ds:lang text="dowolne"/>
             <input type="text" name="numList" id="numList" size="30" maxlength="100" onkeypress="document.getElementById('searchType_numlist').checked = true" class="txt" value="<ww:property value="numList"/>"/>
             <ds:available test="dailyOfficeNumber">
	             <ds:lang text="zDnia"/> <ww:textfield name="'dayNumList'" id="dayNumList" cssClass="'txt'" size="10" maxlength="10" />
	               		<img src="<ww:url value="'/calendar096/img.gif'"/>"
	                            id="calendar_dayNumList" style="cursor: pointer; border: 1px solid red;"
	                            title="Date selector" onmouseover="this.style.background='red';"
	                            onmouseout="this.style.background=''"/>
             </ds:available>      
             <br><i>(<ds:lang text="np"/>. 1,3,5,10-15)</i>
             </td>
       		
        </tr>
        <tr>
            <td><input type="radio" name="searchType" id="searchType_numrange"/></td>
            <td><ds:lang text="wszystkie"/></td>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <td><hr/></td>
</tr>


    <ww:if test="!internal">
        <tr>
            <td>
                <table>
                <ds:available test="view.journal.NrPrzesylkiRejestrowanej">
		       	<tr align="left">
					<td><ds:lang text="NrPrzesylkiRejestrowanej"/> : </td>
					<td>
						<ww:textfield name="'postalregnumber'" value="postalregnumber" onchange="'document.getElementById('searchType_numrange').checked = true'"/>
					</td>
				</tr>
				</ds:available>
                <tr>
                    <td valign="top"><ds:lang text="SposobyDostarczeniaPisma"/>:</td>
                    <td>
                        <select id="delivery" name="delivery" class="multi_sel" multiple="true" size="4">
                            <ww:iterator value="deliveries">
                                <option value="<ww:property value="id"/>" <ww:if test="id in delivery or alldelivery">selected="true"</ww:if> >
                                    <ww:property value="name"/>
                                </option>
                            </ww:iterator>
                        </select><br/><a href="javascript:void(select(document.getElementById('delivery'), true))"><ds:lang text="zaznacz"/></a>
                            / <a href="javascript:void(select(document.getElementById('delivery'), false))"><ds:lang text="odznacz"/></a>
                            <ds:lang text="wszystkie"/>
                    </td>
                    <td></td>
                </tr>
                </table>
            </td>
        </tr>
    </ww:if>
<tr>
    <td>
        <table>
        <ds:available test="view.journal.OpisPisma">
        <tr>
            <td><ds:lang text="OpisPisma"/>:</td>
            <td><ww:textfield name="'summary'" size="30" maxlength="30" cssClass="'txt'"/></td>
        </tr>
        </ds:available>
        <ww:if test="!internal">
            <tr>
                <td><ds:lang text="Odbiorca"/>:</td>
                <td><ww:textfield name="'recipient'" size="30" maxlength="100" cssClass="'txt'" id="recipient"/></td>
				<td><input type="button" value="<ds:lang text="WybierzOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" ></td>
            </tr>
        </ww:if>
		
        <tr>
            <td><ds:lang text="OsobaPrzyjmujaca"/>:</td>
            <td><ww:select name="'author'" list="users" headerKey="''" headerValue="getText('select.wybierz')"
                listKey="name" listValue="asLastnameFirstname()" cssClass="'sel combox-chosen'"/>
            </td>
        </tr>
		
		<tr>
            <td><ds:lang text="DzialPrzyjmujacy"/>:</td>
            <td><ww:select name="'authorDivision'" list="fastAssignmentDivision" headerKey="''" headerValue="getText('select.wybierz')"
                listKey="guid" listValue="name" cssClass="'sel combox-chosen'"/>
        </tr>
        <ds:available test="journal.weight"> 
         <tr>
            <td><ds:lang text="Waga"/>:</td>
            <td>
            
            <input type="checkbox" id="weigthCheck" onclick="checkboxclick(this)">
            <script type="text/javascript">
                function checkboxclick(cb){
                    if (cb.checked == 1){
                        document.getElementById("weight").style.display = '';
                    }else {
                        document.getElementById("weight").style.display = 'none';
                        document.getElementById("weight").selectedIndex = 0;
                      }
                     
                };
            </script>            
            <select id="weight" name="weigth"  class="sel" style="display:none;">
					<option value="">-- wybierz --</option>
					<option value="less" >Do 50g</option>
					<option value="more" >Powy�ej 50g</option>                         
            </select>
        </tr>
        </ds:available>
        <ds:available test="journal.user.creatingEntry">
        <tr>
            <td><ds:lang text="PracownikWpisujacyDoDziennika"/>:</td>
            <td><ww:select name="'creatingEntry'" list="users" headerKey="''" headerValue="getText('select.wybierz')"
                listKey="name" listValue="asLastnameFirstname()" cssClass="'sel combox-chosen'"/>
            </td>
        </tr>
        </ds:available>
        <ds:available test="journal.zpo">
        	<tr>
                <td><ds:lang text="ZPO"/>:</td>
                <td><ww:checkbox name="'zpo'" fieldValue="true" /></td>
            </tr>
        </ds:available>
        <ww:if test="!internal">
            <tr>
                <td><ds:lang text="SortowaniePoMiejscowosciOdbiorcy"/>:</td>
                <td><ww:checkbox name="'sortByLocation'" fieldValue="true" /></td>
            </tr>
            <tr>
                <td><ds:lang text="SortowaniePoNumerzeR"/>:</td>
                <td><ww:checkbox name="'sortByRegNumber'" fieldValue="true" /></td>
            </tr>
        </ww:if>
            
    
        </table>
    </td>
</tr>
<tr>
    <td>
        <input type="submit" name="doSearch" value="<ds:lang text="Pokaz"/>" class="btn" onclick="if (!validateForm()) return false;"/>
           	
        
        <ww:if test="results != null && results.size() > 0">
            <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&extended=false', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="WydrukPodstawowy"/>"/>
            <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&extended=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="WydrukRozszerzony"/>"/>
               <ds:available test="journal.printOut.ilosciowy">
                 <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&ilosciowy=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="WydrukIlosciowyNierejestrowanych"/>"/>
               	</ds:available>
          
             <ds:available test="journal.print.param">
            	 <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printParamLink'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="Wydruk dla kuriera"/>"/>
        	</ds:available>
        </ww:if>
    </td>
   
</tr>
<ds:available test="commonAddingPostalRegistrationNumber">
	<tr>
		 <td><input type="button" value="<ds:lang text="ZbiorczeNadawanieNrPrzesylkiRejestrowanej"/>" onclick="openRegNumbersPopup('<ww:property value="id"/>');" class="btn" ></td>
	</tr>
</ds:available>
</table>

</form>

<ww:if test="results != null && results.size() > 0">

    <table class="search" width="100%" cellspacing="0">
    <tr>
	<ww:iterator value="columns" status="status">
			<th>
					<nobr>
						<a href="<ww:url value="sortDesc"/>" 
						name="<ww:property value="title"/>_descending"
						id="<ww:property value="title"/>_descending"
						alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
							<ww:if test="sortField == property && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
							<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/><ww:param name="'name'" value="ascending"/></a>
						<ww:property value="title"/>
						<a href="<ww:url value="sortAsc"/>"
						name="<ww:property value="title"/>_ascending"
						alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
							<ww:if test="sortField == property && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
							<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
					</nobr>
			</th>
			<ww:if test="!#status.last"> 
	        	<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
	        </ww:if>
    </ww:iterator>
    </tr>

	<ww:iterator value="results">
    	<ww:set name="result"/>
    	<tr>
    		<ww:if test="!internal">
    	<td valign="top">
  
    	  <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&etykieta=true&documentId=<ww:property value="#result.documentId"/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="Drukuj naklejke "/>"/>
    		 	
    		 	 </td>
    		 	 </ww:if>
    		<ww:iterator value="columns" status="status">
	        	<ww:if test="property == 'officenumber'" >
		            <td valign="top">
		                <a href="<ww:url value="baseUrl"><ww:param name="'documentId'" value="#result.documentId"/></ww:url>">
		                    <b><ww:property value="#result.officeNumber"/></b>
		                </a>
		            </td>
	            </ww:if>
	            <ww:elseIf test="property == 'sequenceId'">
	                <td valign="top">
	                    <a href="<ww:url value="baseUrl"><ww:param name="'documentId'" value="#result.documentId"/></ww:url>">
	                        <b><ww:property value="#result.sequenceId"/></b>
	                    </a>
	                </td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'incomingdate'">
		            <td>
		                <ww:if test="#result.incomingDate != null">
		                    <ds:format-date value="#result.incomingDate" pattern="dd-MM-yy HH:mm"/>
		                </ww:if>
		            </td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'senderSummary'">
	            	<td><ww:property value="#result.senderSummary"/></td>
	            </ww:elseIf>
				<ww:elseIf test="property == 'creatingUser'">
					<td><ww:property value="creatingUser"/></td>
				</ww:elseIf>
	            <ww:elseIf test="property == 'summary'">
	            	<td valign="top"><ww:property value="#result.summary" /></td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'recipientSummary'">
		            <td valign="top">
		                <ww:property value="#result.recipientSummary"/>
		            </td>	           	 	
	           	 </ww:elseIf>
	            <ww:elseIf test="property == 'divisionName'">
	            	<td valign="top"><ww:property value="#result.divisionName"/><ww:if test="#result.divisionName != null && #result.currentAssignmentUsername != null"> /
	            	</ww:if><ww:property value="#result.currentAssignmentUsername"/></td>
	        	</ww:elseIf>
	        	<ww:else>
	        		<ww:property value="property"/>
	        	</ww:else>
	        	<ww:if test="!#status.last">
	        		<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
	        	</ww:if>
			</ww:iterator>
		</tr>
    </ww:iterator>
    </table>
    <ww:set name="pager" scope="request" value="pager" />
    <table width="100%">
        <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
    </table>
</ww:if>

<ds:available test="view.journal.zestawienie.ilosciowo.wartosciowe">
<ww:if test="results != null && results.size() > 0 && journal.isOutgoing()">
<h4><ds:lang text="ZestawieniaTitle"/></h4>
<tr>
    <td>
            <input type="button" class="btn" onclick="(window.open('<ww:url value='printLink'/>&zestawienie=true&krajowy=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="Wydruk krajowy"/>"/>

            <input type="button" class="btn" onclick="(window.open('<ww:url value='printLink'/>&zestawienie=true&zagraniczny=true', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="Wydruk zagraniczny"/>"/>
    </td>
</tr>
</ww:if>
</ds:available>

<script type="text/javascript">

if($j('#day').length > 0)
{
    Calendar.setup({
        inputField     :    "day",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_day",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}

if($j('#dayDailyOfficeNumber').length > 0)
{
    Calendar.setup({
        inputField     :    "dayDailyOfficeNumber",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_dayDailyOfficeNumber",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}

if($j('#dayNumList').length > 0)
{
    Calendar.setup({
        inputField     :    "dayNumList",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_dayNumList",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}

if($j('#fromDay').length > 0)
{
    Calendar.setup({
        inputField     :    "fromDay",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_fromDay",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}

if($j('#toDay').length > 0)
{
    Calendar.setup({
        inputField     :    "toDay",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_toDay",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}

    var minNum = <ww:property value="minNum != null ? minNum : -1"/>;
    var maxNum = <ww:property value="maxNum != null ? maxNum : -1"/>;

	function openPersonPopup(lparam, dictionaryType)
    {
        openToolWindowX('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true', 'person', 700,650);
    }
    
    function validateForm()
    {
        if (minNum < 0 && maxNum < 0) { alert('<ds:lang text="DziennikJestPusty"/>'); return false; }

        var radios = document.getElementsByName('searchType');
        var radioChecked = null;
        for (var i=0; i < radios.length; i++)
        {
            if (radios.item(i).checked)
            {
                radioChecked = radios.item(i).value; 
                break;
            }
        }

        if (!radioChecked) { alert('<ds:lang text="NalezyWybracSposobWyszukiwania"/>'); return false; }

        if (radioChecked == '<%= JournalActionBase.SEARCH_DAY %>' &&
            document.getElementById('day').value.length == 0)
            { alert('<ds:lang text="NiePodanoDaty"/>'); return false; }

        if (radioChecked == '<%= JournalActionBase.SEARCH_DAYRANGE %>')
        {
            if (document.getElementById('fromDay').value.length == 0)
                { alert('<ds:lang text="NiePodanoPoczatkowejDatyZakresu"/>'); return false; }
            if (document.getElementById('toDay').value.length == 0)
                { alert('<ds:lang text="NiePodanoKoncowejDatyZakresu"/>'); return false; }
        }
        
        if (radioChecked == '<%= JournalActionBase.SEARCH_NUMRANGE_DAILY %>')
        {
            if (document.getElementById('day').value.length == 0)
                { alert('<ds:lang text="MusiszWypelnicPoleZdnia"/>'); return false; }
        }

        if (radioChecked == '<%= JournalActionBase.SEARCH_NUMRANGE %>')
        {
            if (isNaN(parseInt(document.getElementById('fromNum').value)))
                { alert('<ds:lang text="NieprawidlowyPoczatkowyNumerZakresu"/>'); return false; }
            if (isNaN(parseInt(document.getElementById('toNum').value)))
                { alert('<ds:lang text="NieprawidlowyKoncowyNumerZakresu"/>'); return false; }
            if (parseInt(document.getElementById('fromNum').value) < minNum)
                { alert('<ds:lang text="MinimalnaWartoscPoczatkuZakresuTo"/> '+minNum); return false; }
            if (parseInt(document.getElementById('toNum').value) > maxNum)
                { alert('<ds:lang text="MaksymalnaWartoscKoncaZakresuTo"/> '+maxNum); return false; }
        }
		valHour();
        return true;
    }
	function openRegNumbersPopup(id)
    {
        openToolWindowX('<ww:url value="'/office/adding-reg-numbers.action'"/>'+'?id='+id, 300,650);
    }
    

        function __accept_person(map, lparam, wparam)
    {
        //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

        var txt = document.getElementById('recipient');

        if (!isEmpty(map.lastname))
            txt.value += map.lastname + " ";
        if (!isEmpty(map.organization))
            txt.value += map.organization + " ";
        if (!isEmpty(map.firstname))
            txt.value += map.firstname + " ";
        if (!isEmpty(map.street))
            txt.value += map.street + " ";
        if (!isEmpty(map.zip))
            txt.value += map.zip;
    }
    function valHour()
    {
    	var fh = document.getElementById('fromHour').value;
    	var th = document.getElementById('toHour').value;
    	if(isNaN(fh) || isNaN(th) )
    	{
    		alert('W polu godziana podano warto�� nie liczbow�' + fh + th);
    		return false;
    	}
    	else
    	{
    		if(!(0 <= fh) || !(fh <= 24))
    		{
    			alert('Pole godzina mo�e zawiera� warto�ci z przedzia�u od 0 do 24');
    			return false;
    		}
    		else if(!(0 <= th) || !(th <= 24))
    		{
    			alert('Pole godzina mo�e zawiera� warto�ci z przedzia�u od 0 do 24');
    			return false;
    		}
    	}
    }
  
    
</script>