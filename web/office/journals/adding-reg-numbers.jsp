<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%> 

<ds:ww-action-errors/>
<ds:ww-action-messages/>

	<form action="<ww:url value="baseLink"/>" method="post">
		<table cellspacing="0" cellpadding="0">
			<tr>
	         	<ds:lang text="ZbiorczeNadawanieNrPrzesylkiRejestrowanej"/>:
	         </tr>
	         <tr>
	             <td>
	                 <td><ds:lang text="firstKONumber"/> 
	             </td>
	             <td>    
	                 <ww:textfield name="'koNr1'" id="koNr1" cssClass="'txt'" size="10" maxlength="10" />
	             </td>
             </tr>
             <tr>
	             <td>
	                 <td><ds:lang text="lastKONumber"/> 
	             </td> 
	             <td>   
	                 <ww:textfield name="'koNr2'" id="koNr2" cssClass="'txt'" size="10" maxlength="10" />
	             </td>
             </tr>
             <tr>
	             <td>
	                 <td><ds:lang text="firstRegNumber"/>
	             </td>
	             <td> 
	                 <ww:textfield name="'regNr'" id="regNr" cssClass="'txt'" size="10" maxlength="10" />
	             </td>
             </tr>
             
             <tr>
             	<td>
             		<input type="submit" name="doAddNumbers" value="<ds:lang text="AddNumbers"/>" class="btn"/>
             	</td>
             </tr>
		</table>
	</form>
