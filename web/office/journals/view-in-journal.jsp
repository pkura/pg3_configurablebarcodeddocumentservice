<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.web.office.journals.JournalActionBase,
                 pl.compan.docusafe.core.office.Person"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<div id="promptDiv" style="overflow-y: auto; overflow-x: hidden; max-height: 200px;"></div>
<div id="senderData" style="display: none"></div>
<div id="recipientData" style="display: none"></div>
<div id="divisiontData" style="display: none"></div>

<h1><ds:lang text="Dziennik"/> </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h4><ww:property value="title"/></h4>

<form action="<ww:url value="'/office/view-in-journal.action'"/>" method="post">
    <ww:hidden name="'id'" value="id"/>
    <ww:hidden name="'sortBy'" value="sortBy"/>

<ds:lang text="PokazWpisy"/>:

<table cellspacing="0" cellpadding="0">
<tr>
    <td>
        <table>
        <tr>
            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_DAY %>" id="searchType_day" <ww:if test="searchType == 'day'">checked="true"</ww:if>/></td>
            <td><ds:lang text="zDnia"/> <ww:textfield name="'day'" id="day" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_day&quot;).checked = true'"  />
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_day" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_DAYRANGE %>" id="searchType_dayrange" <ww:if test="searchType == 'dayrange'">checked="true"</ww:if>/></td>
            <td><ds:lang text="odDnia"/> <ww:textfield name="'fromDay'" id="fromDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_fromDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/> 
               <ds:available test="view.journal.godzina">
               Godzina : <input type="text" name="fromHour" id="fromHour" size="3" maxlength="2" class="txt" value="<ww:property value="fromHour"/>"/> 
               </ds:available>
                <ds:lang text="doDnia"/> <ww:textfield name="'toDay'" id="toDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_toDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
              <!--  -->
              <ds:available test="view.journal.godzina">
              Godzina : <input type="text" name="toHour" id="toHour" size="3" maxlength="2" class="txt" value="<ww:property value="toHour"/>"/>
              </ds:available>
            </td>
        </tr>
        <ds:available test="officeNumber">
	        <ds:available test="dailyOfficeNumber">
		        <tr>
		        	<td colspan="2">
		        		<ds:lang text="NumerKO"/>
		        	</td>
		        </tr>
	        </ds:available>
	        <tr>
	            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_NUMRANGE %>" id="searchType_numrange" <ww:if test="searchType == 'numrange'">checked="true"</ww:if>/></td>
	            <td>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeru"/> </ds:available>
	            	<input type="text" name="fromNum" id="fromNum" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="fromNum"/>"/>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeru"/> </ds:available> 
	            	<input type="text" name="toNum" id="toNum" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange').checked = true" class="txt" value="<ww:property value="toNum"/>"/>
	            </td>
	        </tr>
        </ds:available>
        <ds:available test="dailyOfficeNumber">
	        <ds:available test="officeNumber">
		        <tr>
		        	<td colspan="2">
		        		<ds:lang text="DziennyNumerKO"/>
		        	</td>
		        </tr>
	        </ds:available>
	        <tr>
	            <td><input type="radio" name="searchType" value="<%= JournalActionBase.SEARCH_NUMRANGE_DAILY %>" id="searchType_numrange_daily" <ww:if test="searchType == 'numrangedaily'">checked="true"</ww:if>/></td>
	            <td>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="odNumeru"/> </ds:available> 
	            	<input type="text" name="fromNumD" id="fromNumD" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange_daily').checked = true" class="txt" value="<ww:property value="fromNumD"/>"/>
	            	<ds:available test="dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeruKolejnego"/> </ds:available>
	            	<ds:available test="!dzienniki.odDoNumeruNaOdDoNumeruKolejnego"><ds:lang text="doNumeru"/> </ds:available> 
	            	<input type="text" name="toNumD" id="toNumD" size="6" maxlength="10" onkeypress="document.getElementById('searchType_numrange_daily').checked = true" class="txt" value="<ww:property value="toNumD"/>"/>
	            	<ds:lang text="zDnia"/> <ww:textfield name="'dayDailyOfficeNumber'" id="dayDailyOfficeNumber" cssClass="'txt'" size="10" maxlength="10" />
               		<img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_dayDailyOfficeNumber" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
	            </td>
	        </tr>
        </ds:available>
        <tr>
            <td><input type="radio" name="searchType" id="searchType_numrange"/></td>
            <td><ds:lang text="wszystkie"/></td>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <td><hr/></td>
</tr>
<tr>
    <td>
        <table>
        	<ds:available test="view.journal.NrPrzesylkiRejestrowanej">
	       	<tr align="left">
				<td><ds:lang text="NrPrzesylkiRejestrowanej"/> : </td>
				<td>
					<ww:textfield name="'postalregnumber'" value="postalregnumber" onchange="'document.getElementById('searchType_numrange').checked = true'"/>
				</td>
			</tr>
			</ds:available>
            <tr align="left">
                <td valign="top"><ds:lang text="SposobyDostarczeniaPisma"/>:</td>
                <td valign="top">
                    <!-- sposoby dostarczenia -->
                    <select id="delivery" name="delivery" class="multi_sel" multiple="true" size="4">
                        <ww:iterator value="deliveries">
                            <option value="<ww:property value="id"/>" <ww:if test="id in delivery or alldelivery">selected="true"</ww:if> >
                                <ww:property value="name"/>
                            </option>
                        </ww:iterator>
                    </select><br/><a href="javascript:void(select(document.getElementById('delivery'), true))"><ds:lang text="zaznacz"/></a>
                        / <a href="javascript:void(select(document.getElementById('delivery'), false))"><ds:lang text="odznacz"/></a>
                        <ds:lang text="wszystkie"/>
                </td>
                <ds:available test="view.journal.RodzajePism">
                <td valign="top"><ds:lang text="RodzajePism"/>:</td>
                <td valign="top">
                    <!-- rodzaje -->
                    <select id="kindIds" name="kindIds" class="multi_sel" multiple="true" size="4">
                        <ww:iterator value="kinds">
                            <option value="<ww:property value="id"/>" <ww:if test="id in kinds or allKinds">selected="true"</ww:if> >
                                <ww:property value="name"/> (dni:<ww:property value="days"/>)
                            </option>
                        </ww:iterator>
                    </select><br/><a href="javascript:void(select(document.getElementById('kindIds'), true))"><ds:lang text="zaznacz"/></a>
                        / <a href="javascript:void(select(document.getElementById('kindIds'), false))"><ds:lang text="odznacz"/></a>
                        <ds:lang text="wszystkie"/>
                </td>
                </ds:available>
                <ds:available test="view.journal.Odbiorca">
               	<td valign="top"><ds:lang text="Odbiorca"/>:</td>
           		<td>
					<table>
						<tr class="formTableDisabled">
							<td>Imi�:</td>
							<td>
								<ww:textfield id="recipientFirstname" name="'userFirstname'" cssClass="'txt dontFixWidth'" cssStyle="'width: 184px'"/>
								<a href="javascript:clearRecipientInputs()" title="Wyczy�� pola" class="clearFieldsIco">
					    			<img src="<ww:url value="'/img/minus2.gif'"/>" />
					    		</a>
							</td>
						</tr>
						<tr class="formTableDisabled">
							<td>Nazwisko:</td>
							<td>
							<div>
								<ww:textfield id="recipientLastname" name="'userLastname'" cssClass="'txt dontFixWidth'" cssStyle="'width: 200px'"  
								onkeyup="'showMatchingRecipient(event, $j(this));'" />
								</div>
							</td>
						</tr>
						<tr class="formTableDisabled">
							<td>Dzia�:</td>
							<td>
								<ww:textfield id="recipientDivision" name="'userDivision'" cssClass="'txt dontFixWidth'" cssStyle="'width: 200px'"
								onkeyup="'showMatchingDivision(event, $j(this));'" />
								<a href="javascript:clearDivInputs()" title="Wyczy�� dzia�" class="clearFieldsIco">
					    			<img src="<ww:url value="'/img/minus2.gif'"/>" />
					    		</a>
							</td>
						</tr>
						<tr class="formTableDisabled">
							<td>
								<ww:select id="fastAssignmentSelectUser" name="'fastAssignmentSelectUser'" list="fastAssignmentUser"  listKey="name" listValue="asLastnameFirstname()" 
									size="10" cssClass="'sel dontFixWidth'" cssStyle="'width: 100x;'" />
							</td>
							<td>
								<ww:select name="'fastAssignmentSelectDivision'" id="fastAssignmentSelectDivision" list="fastAssignmentDivision" listKey="guid" listValue="name" 
									size="10" cssClass="'sel'" cssStyle="'width: 190px;'" />
							</td>
						</tr>
					</table>
				
					<script type="text/javascript">
						$j(document).ready(function() {
							$j('#recipientLastname').attr('autocomplete', 'off');

							$j('#fastAssignmentSelectUser').bind('change', function() {
								writeUsername2Inputs(this, ' ', 'recipientFirstname', 'recipientLastname');

								$j.post("incoming/recipient-lookup.action",
								{
									lastname:getInputValue('recipientLastname').trim(),
									firstname:getInputValue('recipientFirstname').trim()
								},
								function(data){
									$j("#recipientData").html(data);
									//alert(data);
									senders = eval(data);
									<%--trace(senders[0]);--%>
									//$j('#recipientDivision').get(0).value = senders[0].recipientDivisionName;
									setUTFInputVal($j('#senderAjaxError'), $j('#recipientDivision'), senders[0].recipientDivisionName); 

									trace('Selected division: ' + senders[0].recipientDivision);
									$j('#fastAssignmentSelectDivision option[value=' + senders[0].recipientDivision + ']').attr('selected', true);
								});
							});

							$j('#fastAssignmentSelectDivision').bind('change', function() {
								writeUsername2Inputs(this, null, null, 'recipientDivision');
							});
							
							$j('#recipientLastname').keypress(function(event) {
								if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
									putItem(selectedItem);
	
									$j('#form').bind('submit', function() {
										return false;
									});
									dontSendForm = true;
									return false;
								}
	
								if(dontSendForm) {
									dontSendForm = false;
									$j('#form').unbind('submit');
								}
	
								return true;
							});
						});

						function writeUsername2Inputs(src, delimiter, firstnameId, lastnameId) {
				
							var jqSel = $j(src);

							var selIndex = jqSel.attr('selectedIndex');

							var names  = jqSel.children()[selIndex].text.split(delimiter);

							try {
								$j('#' + lastnameId).val(names[0]);
								$j('#' + firstnameId).val(names[1]);
							} catch(e) {}
						}

						function clearRecipientInputs() {
							var form_el = document.forms[0];
						    var selectDiv = document.getElementById('fastAssignmentSelectDivision');
						    var selectUsr = document.getElementById('fastAssignmentSelectUser');
							traceStart('clearRecipientInputs()');

							for(i = 0; i < form_el.length; i ++) {
								if((form_el.elements[i].type == 'text') && (form_el.elements[i].id.indexOf('recipient') != -1)) {
									trace('Clearing item [ID] ' + form_el.elements[i].id);
									form_el.elements[i].value = '';
								}	
							}

							/* jeszcze odznaczamy selecty */
							selectDiv.selectedIndex = -1;
							selectUsr.selectedIndex = -1;

							traceEnd();
						}
						
						function clearDivInputs() {
						    document.getElementById("recipientDivision").value = '';
						    var selectDiv = document.getElementById('fastAssignmentSelectDivision');
						    selectDiv.selectedIndex = -1;
						}
	
						/* wyswietla podpowiedzi dla nazwiska - sprawdza czy pole nie jest puste - wtedy podpowiedzi nie wyswietla */
						var selectedItem = 0;
						var KEY_DOWN = 40;
						var KEY_UP = 38;
						var KEY_ENTER = 13;
						var KEY_ESCAPE = 27;
						var pressedKey = 0;
						var itemsLength = 0;
						var senders = null;
						var ie = document.all && !window.opera;
						var dontSendForm = false;
	
						/* tych pol nie wyswietlamy w podpowiedzi, ale sa dostepne (display: none) */
						var hiddenItems = new Array('recipientDivision', 'recipientName', 'personZip',
								'personEmail', 'senderId');
	
						/* usuwa poczatkowe spacje */
						String.prototype.trim = function() {
							return this.replace(/^\s*|\s*$/i, '');
						}
						
						window.onload = function () {
						    var selectDiv = document.getElementById('fastAssignmentSelectDivision');
						    var selectUsr = document.getElementById('fastAssignmentSelectUser');
						    var textDiv = document.getElementById('recipientDivision');
						    var textLast = document.getElementById('recipientLastname');
						    var textFirst = document.getElementById('recipientFirstname');
						    
						    selectDiv.onclick = function () {
						        var toCopyDiv = selectDiv.options[selectDiv.selectedIndex].text;
						        textDiv.value = toCopyDiv;
						    };
						    
						    selectUsr.onclick = function () {
						        var toCopyName = selectUsr.options[selectUsr.selectedIndex].text.split(" ");
						        textLast.value = toCopyName[0];
						        textFirst.value = toCopyName[1];
						    };
						};
						
	
						
						window.onload = function () {
						    var selectDiv = document.getElementById('fastAssignmentSelectDivision');
						    var selectUsr = document.getElementById('fastAssignmentSelectUser');
						    var textDiv = document.getElementById('recipientDivision');
						    var textLast = document.getElementById('recipientLastname');
						    var textFirst = document.getElementById('recipientFirstname');
						    
						    selectDiv.onclick = function () {
						        var toCopyDiv = selectDiv.options[selectDiv.selectedIndex].text;
						        textDiv.value = toCopyDiv;
						    };
						    
						    selectUsr.onclick = function () {
						        var toCopyName = selectUsr.options[selectUsr.selectedIndex].text.split(" ");
						        textLast.value = toCopyName[0];
						        textFirst.value = toCopyName[1];
						    };
						};
						
						
						/* poprawia pozycje promptDiv, zeby miescil sie w ekranie przegladarki */
						function fixPromptDivPos(docWidth, pd) {
							traceStart('fixPromptDivPos()');
							
							var rightPos = pd[0].offsetLeft + pd[0].scrollWidth;
	
							/* mozilla nie liczy scrollbara */
							if($j.browser.mozilla && pd[0].scrollHeight)
								rightPos += 15;
	
							trace('docWidth: ' + docWidth);
							trace('rightPos: ' + rightPos);
	
							//alert('posX: ' + docWidth + ', ' + rightPos + ', ' + (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px'); 
	
							if(rightPos > docWidth) {
								
								trace('Changing leftPos');
								pd.css('left', (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px');
							}
	
							traceEnd();	
						}
	
						function highlightItem(id) {
							/* usuwamy stare pod�wietlenie */
							$j('#prompt_item_' + selectedItem).removeClass('selected');
	
							$j('#' + id).addClass('selected');
	
							/* wyciagamy numer */
							selectedItem = parseInt((id).substr(12));
						}
	
						function unHighlightItem(id) {
							$j('#' + id).removeClass('selected');
	
							/* podswietlenie aktualnego zostaje */
							$j('#prompt_item_' + selectedItem).addClass('selected');
						}
	
						function putItem() {
							sender = senders[selectedItem];
	
							if(sender) {
								var c = 0;
								for(property in sender){
									var jq_sel = '#prompt_item_' + selectedItem + ' td.' + property; // wybieramy komorke
									var val = '';
	
									 // pobieramy tekst z komorki
									val = $j(jq_sel).text();
	
									
									
									//zapisujemy do input text
									$j('#' + property).val(val);
									
								}
	
								$j('#promptDiv').css('visibility', 'hidden').css('display', 'none');
	
								/* odznaczamy checkbox dodaj do s�ownika */
								if(sender.personLastname) {
									$j('#createPerson').attr('checked', false);
									<ds:available test="layout2">
							        Custom.clear();
							        </ds:available>
								}
	
								/* zapisujemy dzial do multiselecta */
								var divInput = $j('#recipientDivision').get(0);
								var divSel = $j('#fastAssignmentSelectDivision').get(0);
	
								for(i = 0; i < divSel.options.length; i ++) {
									if(divSel.options[i].value == sender.recipientDivision) {
										divSel.options[i].selected = true;
	
										
										setUTFInputVal($j('#senderAjaxError'), $j(divInput), sender.recipientDivisionName); 
										break;
									}
								}
	
								/* zapisujemy osobe do multiselecta */
								var usrSel = $j('#fastAssignmentSelectUser').get(0);
	
								for(i = 0; i < usrSel.options.length; i ++) {
									if(usrSel.options[i].value == sender.recipientName) {
										usrSel.options[i].selected = true;
										break;
									}
								}
	
							}
	
						}
					
						function showMatchingRecipient(e, caller){
							pressedKey = e.keyCode;
				
							var ln = caller;
							var pd = $j('#promptDiv');
							var str = '';
				
				
							if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
								if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
									pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\"/>";
				
									traceStart('promptDiv position');
									trace('x: ' + getPos(ln.get(0)).left);
									trace('y: ' + getPos(ln.get(0)).top);
									traceEnd();
									
									
									pd.css('left', getPos(ln.get(0)).left  + 'px');
									pd.css('top', getPos(ln.get(0)).top + ln.attr('offsetHeight') + 'px');
									pd.css('visibility', 'visible').css('display', 'inline');
								} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
									trace('Only navigation (without sending ajax request)');
									displayPrompt(senders, ln.attr('id'), hiddenItems);
									return;
								}
				
								trace('Sending request to recipient-lookup.action ...');
								$j.post("incoming/recipient-extension-lookup.action",
									{
										lastname:getInputValue('recipientLastname').trim(),
										division:getInputValue('recipientDivision').trim(),
										firstname:getInputValue('recipientFirstname').trim()
									},
									 function(data){
										
										$j("#recipientData").html(data.toString());
										//alert(data);
										senders = eval(data);
										var docWidth = $j(document).width();
										displayPrompt(senders, ln.attr('id'), hiddenItems);
										fixPromptDivPos(docWidth, pd);
										trace('Request recipient-lookup.action completed and prompt shown');
										traceEnd();
										
									});
							} else {
								pd.css('visibility', 'hidden').css('display', 'none');
							}
				
				
							/*$j.post("recipient-lookup.action", {lastname:$j("#recipientLastname").get(0).value, division:$j('#recipientDivision').get(0).value}, function(data){
								$j("#recipientData").html(data);
								recipients = eval(data);
							});*/
						}
						
						function showMatchingDivision(e, caller){
							pressedKey = e.keyCode;
							
							var ln = caller;
							var pd = $j('#promptDiv');
							var str = '';
				
				
							if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
								if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
									pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\"/>";
				
									traceStart('promptDiv position');
									trace('x: ' + getPos(ln.get(0)).left);
									trace('y: ' + getPos(ln.get(0)).top);
									traceEnd();
									
									
									pd.css('left', getPos(ln.get(0)).left  + 'px');
									pd.css('top', getPos(ln.get(0)).top + ln.attr('offsetHeight') + 'px');
									pd.css('visibility', 'visible').css('display', 'inline');
								} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
									trace('Only navigation (without sending ajax request)');
									displayPrompt(senders, ln.attr('id'), hiddenItems);
									return;
								}
				
								trace('Sending request to division-lookup.action ...');
								$j.post("incoming/division-lookup.action",
									{
										name:getInputValue('recipientDivision').trim(),
									},
									 function(data){
										
										$j("#divisiontData").html(data.toString());
										//alert(data);
										senders = eval(data);
										var docWidth = $j(document).width();
										displayPrompt(senders, ln.attr('id'), hiddenItems);
										fixPromptDivPos(docWidth, pd);
										trace('Request division-lookup.action completed and prompt shown');
										traceEnd();
										
									});
							} else {
								pd.css('visibility', 'hidden').css('display', 'none');
							}
				
				
							/*$j.post("recipient-lookup.action", {lastname:$j("#recipientLastname").get(0).value, division:$j('#recipientDivision').get(0).value}, function(data){
								$j("#recipientData").html(data);
								recipients = eval(data);
							});*/
						}
				
					</script>
			</td>
                </ds:available>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>
        <table>
        <ds:available test="view.journal.OpisPisma">
        <tr>
            <td><ds:lang text="OpisPisma"/>:</td>
            <td><ww:textfield name="'summary'" size="30" maxlength="30" cssClass="'txt'" /></td>
        </tr>
        </ds:available>
        <ds:available test="view.journal.Nadawca">
        <tr>
            <td><ds:lang text="Nadawca"/>:</td>
            <td><ww:textarea name="'sender'" rows="2" cols="50" cssClass="'txt-200p'" cssStyle="width: 183px; height: 71px;"  id="sender"/></td>
          <%--   <td><ww:text name="'sender'" size="30"  maxlength="200" rows cssClass="'txt'" id="sender"/></td> --%>
			<td><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
        </tr>
        </ds:available>
        
        <ds:available test="view.journal.ZnakPisma">
        <tr>
            <td><ds:lang text="ZnakPisma"/>:</td>
            <td><ww:textfield name="'referenceId'" size="30" maxlength="30" cssClass="'txt'"/></td>
        </tr>
        </ds:available>
        <tr>
            <td><ds:lang text="OsobaPrzyjmujaca"/>:</td>
            <td><ww:select name="'author'" list="users" headerKey="''" headerValue="getText('select.wybierz')"
                listKey="name" listValue="asLastnameFirstname()" cssClass="'sel combox-chosen'"/>
        </tr>
        <tr>
            <td><ds:lang text="DzialPrzyjmujacy"/>:</td>
            <td><ww:select name="'authorDivision'" list="fastAssignmentDivision" headerKey="''" headerValue="getText('select.wybierz')"
                listKey="guid" listValue="name" cssClass="'sel combox-chosen'"/>
        </tr>
        </table>
    </td>
</tr>
<tr>
    <td>																																			
        <input type="submit" name="doSearch" value="<ds:lang text="Pokaz"/>"  <ww:if test="!canReadDictionary">disabled="true"</ww:if> class="btn" onclick="if (!validateForm()) return false;" />
		&nbsp
        <ww:if test="results != null && results.size() > 0">
            <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&fontsize=7', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="WydrukMaly"/>" <ww:if test="!canReadDictionary">disabled="true"</ww:if>/>
        </ww:if>
		&nbsp
		<ww:if test="results != null && results.size() > 0">
            <input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printLink'/>&fontsize=9', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="WydrukDuzy"/>" <ww:if test="!canReadDictionary">disabled="true"</ww:if>/>
            <ds:available test="journal.print.param">
        		<input type="button" class="btn" onclick="javascript:void(window.open('<ww:url value='printParamLink'/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));" value="<ds:lang text="Wydruk dla kuriera"/>" <ww:if test="!canReadDictionary">disabled="true"</ww:if>/>
        	</ds:available>
        </ww:if>
    </td>
</tr>
</table>

</form>

<ww:if test="results != null && results.size() > 0">

    <table class="search" width="100%" cellspacing="0">
    <tr>
	<ww:iterator value="columns" status="status">
			<th>
					<nobr>
						<a href="<ww:url value="sortDesc"/>" 
						name="<ww:property value="title"/>_descending"
						id="<ww:property value="title"/>_descending"
						alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
							<ww:if test="sortField == property && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
							<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/><ww:param name="'name'" value="ascending"/></a>
						<ww:property value="title"/>
						<a href="<ww:url value="sortAsc"/>"
						name="<ww:property value="title"/>_ascending"
						alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
							<ww:if test="sortField == property && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
							<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
					</nobr>
			</th>
			<ww:if test="!#status.last"> 
	        	<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
	        </ww:if>
    </ww:iterator>
    </tr>    
    <ww:iterator value="results">
    	<ww:set name="result"/>
    	<tr>
    		<ww:iterator value="columns" status="status">
	        	<ww:if test="property == 'officenumber'" >
		            <td valign="top">
		                <a href="<ww:url value="baseUrl"><ww:param name="'documentId'" value="#result.documentId"/></ww:url>">
		                    <b><ww:property value="#result.officeNumber"/></b>
		                </a>
		            </td>
	            </ww:if>
	            <ww:elseIf test="property == 'sequenceId'">
	                <td valign="top">
	                    <a href="<ww:url value="baseUrl"><ww:param name="'documentId'" value="#result.documentId"/></ww:url>">
	                        <b><ww:property value="#result.sequenceId"/></b>
	                    </a>
	                </td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'incomingdate'">
		            <td>
		                <ww:if test="#result.incomingDate != null">
		                    <ds:format-date value="#result.incomingDate" pattern="dd-MM-yy HH:mm"/>
		                </ww:if>
		            </td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'documentDate'">
		            <td>
		                <ww:if test="#result.documentDate != null">
		                    <ds:format-date value="#result.documentDate" pattern="dd-MM-yy"/>
		                </ww:if>
		            </td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'referenceId'">
	            	<td><ww:property value="#result.referenceId"/></td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'postalRegNumber'">
	            	<td><ww:property value="#result.postalregnumber"/></td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'senderSummary'">
	            	<td><ww:property value="#result.senderSummary"/></td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'summary'">
	            	<td valign="top"><ww:property value="#result.summary" /></td>
	            </ww:elseIf>
	            <ww:elseIf test="property == 'recipientSummary'">
		            <td valign="top">
		                <ww:property value="#result.recipientSummary"/>
		            </td>	           	 	
	           	 </ww:elseIf>
	           	 <ww:elseif test="property == 'recipientUser'">
	           	 	<td valign="top">
	           	 		<ww:if test="#result.recipientUser != null">
	           	 		<nobr>
	           	 			<ww:property value="#result.recipientUser.firstname" />
	           	 			<ww:property value="#result.recipientUser.lastname" />
	           	 			(<ww:property value="#result.recipientUser.name" />)
	           	 		<a class="imgView" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'documentId'" value="#result.masterDocumentId"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">
						    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="ShowHandWrittenSignature"/>"/>
						</a>
	           	 		</nobr>
	           	 		</ww:if>
	           	 	</td>
	           	 </ww:elseif>	           	 
	            <ww:elseIf test="property == 'divisionName'">
	            	<td valign="top"><ww:property value="#result.divisionName"/><ww:if test="#result.divisionName != null && #result.currentAssignmentUsername != null"> /
	            	</ww:if><ww:property value="#result.currentAssignmentUsername"/></td>
	        	</ww:elseIf>
	        	<ww:elseIf test="property == 'delivery'">
		            <td valign="top">
		                <ww:property value="#result.delivery"/>
		            </td>	           	 	
	           	 </ww:elseIf>
	        	<ww:else>
	        		<ww:property value="property"/>
	        	</ww:else>
	        	<ww:if test="!#status.last">
	        		<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
	        	</ww:if>
			</ww:iterator>
		</tr>
    </ww:iterator>
    </table>
    <ww:set name="pager" scope="request" value="pager"/>
    <table width="100%">
        <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
    </table>
</ww:if>

<script type="text/javascript">

	if($j('#day').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "day",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_day",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#dayDailyOfficeNumber').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "dayDailyOfficeNumber",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_dayDailyOfficeNumber",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#fromDay').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "fromDay",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_fromDay",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

	if($j('#toDay').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "toDay",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_toDay",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

    var minNum = <ww:property value="minNum != null ? minNum : -1"/>;
    var maxNum = <ww:property value="maxNum != null ? maxNum : -1"/>;

    function validateForm()
    {
        if (minNum < 0 && maxNum < 0) { alert('Dziennik jest pusty'); return false; }

        var radios = document.getElementsByName('searchType');
        var radioChecked = null;
        for (var i=0; i < radios.length; i++)
        {
            if (radios.item(i).checked)
            {
                radioChecked = radios.item(i).value;
                break;
            }
        }

        if (!radioChecked) { alert('<ds:lang text="NalezyWybracSposobWyszukiwania"/>'); return false; }

        if (radioChecked == '<%= JournalActionBase.SEARCH_DAY %>' &&
            document.getElementById('day').value.length == 0)
            { alert('<ds:lang text="NiePodanoDaty"/>'); return false; }

        if (radioChecked == '<%= JournalActionBase.SEARCH_DAYRANGE %>')
        {
            if (document.getElementById('fromDay').value.length == 0)
                { alert('<ds:lang text="NiePodanoPoczatkowejDatyZakresu"/>'); return false; }
            if (document.getElementById('toDay').value.length == 0)
                { alert('<ds:lang text="NiePodanoKoncowejDatyZakresu"/>'); return false; }
        }
        
        if (radioChecked == '<%= JournalActionBase.SEARCH_NUMRANGE_DAILY %>')
        {
            if (document.getElementById('dayDailyOfficeNumber').value.length == 0)
                { alert('<ds:lang text="MusiszWypelnicPoleZdnia"/>'); return false; }
           	if (isNaN(parseInt(document.getElementById('fromNumD').value)))
                { alert('<ds:lang text="NieprawidlowyPoczatkowyNumerZakresu"/>'); return false; }
            if (isNaN(parseInt(document.getElementById('toNumD').value)))
                { alert('<ds:lang text="NieprawidlowyKoncowyNumerZakresu"/>'); return false; }
        }

        if (radioChecked == '<%= JournalActionBase.SEARCH_NUMRANGE %>')
        {
            if (isNaN(parseInt(document.getElementById('fromNum').value)))
                { alert('<ds:lang text="NieprawidlowyPoczatkowyNumerZakresu"/>'); return false; }
            if (isNaN(parseInt(document.getElementById('toNum').value)))
                { alert('<ds:lang text="NieprawidlowyKoncowyNumerZakresu"/>'); return false; }
            if (parseInt(document.getElementById('fromNum').value) < minNum)
                { alert('<ds:lang text="MinimalnaWartoscPoczatkuZakresuTo"/> '+minNum); return false; }
            if (parseInt(document.getElementById('toNum').value) > maxNum)
                { alert('<ds:lang text="MaksymalnaWartoscKoncaZakresuTo"/> '+maxNum); return false; }
        }

        return true;
    }
	function openPersonPopup(lparam, dictionaryType)
    {
		document.getElementBy
        openToolWindowX('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true', 'person', 700,650);
    }

    function __accept_person(map, lparam, wparam)
    {
        //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

        var txt = document.getElementById('sender');
        txt.value="";
        if (!isEmpty(map.lastname))
            txt.value += map.lastname + " ";
        if (!isEmpty(map.firstname))
            txt.value += map.firstname + " ";
        if (!isEmpty(map.organization))
            txt.value += map.organization + " ";
        if (!isEmpty(map.street))
            txt.value += map.street + " ";
        if (!isEmpty(map.zip))
            txt.value += map.zip;
    }
</script>