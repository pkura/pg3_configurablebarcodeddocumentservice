<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Dodaj plik zip ze skanami"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form action="<ww:url value="'/office/add-barcode-package-scans.action'"/>" method="post" enctype="multipart/form-data">
<ww:hidden name="'doAdd'" id="doAdd"/>
	<table>
	<tr>
	<td style="vertical-align:top; padding-left:20px;">
			<p>Wczytaj plik ze skanami:</p>
			<p><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></p>
			<p>
				<ds:submit-event value="'Wczytaj plik'" name="'doAdd'"/>
			</p>
			<ww:if test="properties!=null">
				<p>
					Wczytany plik:<br/>
					<ww:property value="properties" escape="false"/>
				</p>
			</ww:if>
		</td>
	</tr>
</table>
</form>