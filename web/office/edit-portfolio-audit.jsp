<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-portfolio-audit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<ww:if test="portfolio.subFolder">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Podteczka <ww:property value="portfolio.officeId"/></h1>
</ww:if>
<ww:else>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Teczka <ww:property value="portfolio.officeId"/></h1>
</ww:else>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="layout2">
				<div id="middleContainer"> <!-- BIG TABLE start -->
			</ds:available>
<table class="tableMargin table100p">
	<ww:iterator value="workHistory">
	    <ww:set name="date" scope="page" value="date"/>
	    <tr>
	        <td class="historyAuthor">
	       	 <ww:property value="author"/>
	        </td>
	        <td class="alignRight historyDate">
	      	  <fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
	        </td>
	    </tr>
	    <tr>
	        <td colspan="2" class="historyContent">
	            <ww:property value="content"/>
	        </td>
	    </tr>
	</ww:iterator>
</table>

<p></p>

<input type="button" value="Lista teczek w dziale" class="btn"
	onclick="document.location.href='<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="portfolio.divisionGuid"/></ww:url>';"/>

<ds:available test="layout2">
					<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
					<div class="bigTableBottomSpacer">&nbsp;</div>
					</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
				</ds:available>		

<%--
<ww:if test="portfolio.subFolder">
<h1>Podteczka <ww:property value="portfolio.officeId"/></h1>
</ww:if>
<ww:else>
<h1>Teczka <ww:property value="portfolio.officeId"/></h1>
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<table width="100%">
<ww:iterator value="workHistory">
    <ww:set name="date" scope="page" value="date"/>
    <tr>
        <td><ww:property value="author"/></td>
        <td align="right"><fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/></td>
    </tr>
    <tr>
        <td colspan="2" style="border: 1px solid black;">
            <ww:property value="content"/>
        </td>
    </tr>
</ww:iterator>
</table>

<p></p>

<input type="button" value="Lista teczek w dziale" class="btn"
    onclick="document.location.href='<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="portfolio.divisionGuid"/></ww:url>';"/>
--%>
<!--N koniec edit-portfolio-audit.jsp N-->