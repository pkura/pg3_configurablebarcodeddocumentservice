<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,pl.compan.docusafe.core.office.barcode.RodzajPrzesylki"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="PrzyjmijPaczkeNaPodstawieNumerowDokumentow"/></h1>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="PackageIn"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PackageIn"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
	
<form action="<ww:url value="'/office/package-in-nr.action'"/>" method="post" enctype="multipart/form-data" onsubmit="if(!submm()) return false;"> 
	<ds:available test="layout2">
		<div id="middleContainer" style="border-top: none;"> <!-- BIG TABLE start -->
	</ds:available>	
		<hr size="1" align="left" class="horizontalLine" width="77%" />
		<i><ds:lang text="WpisywanieNumerowDokumentowZamiastBarkodow"/></i>	
		<ww:include page="/office/package-param.jsp"/>
		<table>
 			<tr>
 				<td><ds:lang text="PodajNrWczytywanejPaczki"/>:<span class="star">*</span></td>
				<td><input name="nrpaczki" class="txt" type="text" size="30" id="nrpaczki"/></td>
				<td><input name="zmiennrpaczki" class="btn" value="<ds:lang text='GenerujNumerPaczki'/>" cssClass="btn" type="button" id="zmiennrpaczki" onclick="generujNumerPaczki();"/></td>
 			</tr>
 			<tr>
 				<td><ds:lang text="NrDokumentu"/>:<span class="star">*</span></td>
				<td><input name="nrdok" class="txt" type="text" size="30" id="nrdok" onchange="if (!validate()) return false;"/></td>
				<td><input name="wpr_klik" class="btn" value="<ds:lang text='WprowadzNumerDok'/>" cssClass="btn" type="button" id="wpr_klik" onclick="if (!validate()) return false;"/></td>
 			</tr>
 		</table>
	
	<div id="divbar"></div>	

	<p id="tyt">		
	</p>

	<p id="po">
	</p>	

	<p>
 		<table>	
 			<tr>
 				<td><input type="submit" class="btn" value="<ds:lang text='WprowadzNumery'/>" name="doAdd" cssClass="btn" onclick="click_();"/></td>
				<td><input name="delete_all" class="btn" value="<ds:lang text='UsunCalaListe'/>" cssClass="btn" type="button" id="delete_all" onClick="deleteAll();"/></td>
 			</tr>
 		</table>
 		<table>
 			<tr>
 				<td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
 			</tr>
 			<tr>
 				<td>
 					<ds:event name="'doAddFromXls'" value="'Zaladuj z XLS'"></ds:event>
 				</td>
 			</tr>
 		</table>
 	</p>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>
<input type="hidden" id="nr_paczki" name="nr_paczki"/>

<script type="text/javascript">

var ilosc=0;
var bool=false;

	$j(document).ready(function ()
	{		
		//$j("#zmiennrpaczki").hide(); 
		setCursor("p1");
	});

	function setCursor(pole)
	{	
		if(pole == "p1")
		{
			$j("#nrpaczki").focus();
		}
		if(pole == "p2")
		{
			$j("#nrdok").focus();
		}
		
		return true;
	}

	function validate()
	{		
		var nrpaczki = $j("#nrpaczki").attr("value");
		var nrdok = $j("#nrdok").attr("value");
		var test = true;
		var text;
		
		if (nrpaczki == null || nrpaczki.trim().length == 0) 
		{ 	
			window.alert("<ds:lang text='PolePodajNrWczytywanejPaczkiNieMozeBycPuste'/>");
			setCursor("p1");
			return false;
		}
		else
		{	
			$j("#zmiennrpaczki").show();
			//$j("#nrpaczki").attr("readonly", true).addClass("grey"); 
			
			$j("#nrdok").attr("value", "");		
			
			if (nrdok == null || nrdok.trim().length == 0) 
			{ 	
				setCursor("p2");
				return false;
			}
			else
			{		 
				$j("#divbar > input").each(function()
		  		{
		 			text = $j(this).val();
		
					if (nrdok == text) 
					{ 
						window.alert("<ds:lang text='TenDokumentZostalJuzWczytany'/>" + ": " + text); 
						test = false;
					}
		  		});
		  
		  		if (test)
		  		{		
		  			ilosc=ilosc+1;	
					$j("#divbar").html($j("#divbar").html() + "<input name=\"numery\" value=\"" + nrdok + "\" id=\"opt_nr" + ilosc + "\" type=\"hidden\" />");
					
					createTable();
		  		}
				
				setCursor("p2");
				
				return true;
			}	
		}
	}	
	
	function createTable()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		var text;
		var id_;
		var nr_lp = 0;
	
		$j("#tyt > div").remove();
		$j("#po > table").remove();

		$j("#po").html($j("#po").html() + "<table id=\"to\">");

  		$j("#divbar > input").each(function()
  		{
  			text = $j(this).val();
  			id_ = $j(this).attr("id");			

  			if (nr_lp == 0)
  			{
  				$j("#tyt").html($j("#tyt").html() + "<div class=\"bold\"><ds:lang text='NrWczytywanejPaczki'/>: " + "<span class=\"warning\">" +nrpaczki + "</span></div>");
  				$j("#tyt").html($j("#tyt").html() + "<div class=\"bold\"><ds:lang text='WczytaneNumeryDokumentow'/>:</div>");
  				$j("#to").html($j("#to").html() + "<tr> <td class=\"alignCenter\"><span class=\"bold\"><ds:lang text='lp'/></span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td class=\"alignCenter\"><span class=\"bold\"><ds:lang text="NrDokumentu"/></span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td></td> </tr>");
  			}
  			nr_lp = nr_lp+1;
			$j("#to").html($j("#to").html() + "<tr> <td style=\"width: 3em\" class=\"alignCenter\">" + nr_lp + "</td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td style=\"padding: 0px 5px\"><span id=" + id_ + " style=\"white-space:nowrap\">" + text + "</span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td style=\"width: 3em\" class=\"alignCenter bold\"> <a class=\"barcodeLink\" onClick=\"javascript:deleteLink('" + id_ + "')\">usu�</a></td> </tr>");
  		});	 
  		
  		$j("#po").html($j("#po").html() + "</table>");

		$j("#to tr:odd").addClass("oddRows");
		  		
  		if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 8))
			$j("#to tr").hover(
				function() 
				{
					$j(this).addClass("toHover");
				}, 
				function() 
				{
					$j(this).removeClass("toHover");
				}
			); 
  		
  		return true;
	}
	
	function deleteLink(id_ha)
	{	
	
		if (confirm("<ds:lang text='CzyNaPewnoUsunacDokument'/>"))
		{			
			$j("#divbar > input[id='" + id_ha + "']").remove();
		
  			createTable();
		} 
		else 
		{
		}	

  		setCursor("p2");
		
		return true;
	}
	
	function deleteAll()
	{	
		var id_number;
		
		if (confirm("<ds:lang text='CzyNaPewnoUsunacCalaLista'/>"))
		{			
			$j("#divbar > input").each(function()
			{
				id_number = $j(this).attr("id");
				$j("#divbar > input[id='" + id_number + "']").remove();
			})
  			createTable();
		} 
		else 
		{
		}	

  		setCursor("p2");
		
		return true;
	}
	
	function click_()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		var nrdok = $j("#nrdok").attr("value");
		var nr_paczki;
	
		if (nrpaczki == null || nrpaczki.trim().length == 0)
		{ 	
			window.alert("<ds:lang text='PolePodajNrWczytywanejPaczkiNieMozeBycPuste'/>");	
			setCursor("p1");
		}
	//	if (nrdok == null || nrdok.trim.length == 0) {
	//		window.alert("<ds:lang text='PoleNumerDokumentuNieMozeBycPuste'/>");	
	//		setCursor("p2");
	//	}

		if ($j("#lokalizacja")[0].selectedIndex == 0) {
			window.alert("<ds:lang text='PoleLokalizacjaNieMozeBycPuste'/>");
			return false;
		}
		if ($j("#typDokumentu")[0].selectedIndex == 0) {
			window.alert("<ds:lang text='PoleTypDokumentowNieMozeBycPuste'/>");
			return false;
		}

			$j("#nr_paczki").val(nrpaczki);
			nr_paczki = $j("#nr_paczki").attr("value");
			bool = true;
	}
	
	function submm()
	{		
		if(bool)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function generujNumerPaczki()
	{
		var d = new Date();
		
		document.getElementById('nrpaczki').value = d.getTime();;
	}

</script>