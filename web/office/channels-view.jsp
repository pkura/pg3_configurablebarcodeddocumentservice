<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<form action="<ww:url value="'/office/channels-view.action'"/>"  method="post" >
	<p><ds:xmlLink path="ChannelsView"/></p>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DefinicjeKolejek"/></h1>
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
	<p></p>
	<ds:ww-action-errors/>
	<div style="padding: 20px;">
		<ww:if test="channelBeans.size() > 0">
		<table border="1px" cellpadding="1px" style="border-collapse:collapse; ">
			<tr>
				<th>Nazwa</th>
				<th>Warunki</th>
				<th>Dzia�</th>
				<th>U�ytkownicy</th>
			</tr>
			<ww:iterator value="channelBeans" id="channel">
				<tr>
					<td>
					<b><ww:property value="name"/></b>
					</td>
					<td>
					<ww:if test="conditions.size > 0">
						<ww:iterator value="conditions" id="cond">
							<ww:set name="result"/>
							<ww:property value="#result"/><br>  
						</ww:iterator>
					</ww:if>
					<ww:else>
						Brak warunk�w
					</ww:else>
					</td>
					<td>
					<ww:property value="guid"/>
					</td>
					<td>
					<ww:if test="users.size == 1">
						<ww:property value="users.get(0).asFirstnameLastname()"/>
					</ww:if>
					<ww:elseif test="users.size == 0">
					---
					</ww:elseif>
					<ww:else>
					<a href="#"  style="font-size: 7px;" onclick="showHide('<ww:property value="cn"/>')"><i>poka�/ukryj</i></a>
					<div id="users_<ww:property value="cn"/>" style="display : none;">
					<ww:iterator value="users">
						<ww:property value="asFirstnameLastname()"/><br>
					</ww:iterator>
					</div>
					</ww:else>
					</td>
				</tr>	
			</ww:iterator>
		</table>
		</ww:if>
		<ww:else>
		Nie zdefiniowano kana��w
		</ww:else>
		<p></p>

		<script type="text/javascript">
			function showHide(cn) {
				var id = 'users_'+cn;
				var div = document.getElementById(id);
				if (div.style.display == 'none') {
					div.style.display = '';
				} else {
					div.style.display = 'none';
				}
			}
		</script>
	</div>
</form>