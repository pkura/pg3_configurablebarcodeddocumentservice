<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N user-list.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Zastepstwa"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors/>
<edm-html:messages/>

<c:if test="${canEditSubstitutions}">
	<html:form action="/office/users/user-list">
		<html:hidden property="divisionGuid"/>
	
		<c:out value="${tree}" escapeXml="false"/>
		
		<br/>
		<ds:lang text="Legenda"/>:
		
		<br/>
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-closed.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="GrupaUzytkownikow"/>"/>
			-
			<ds:lang text="dzial"/>
		</span>
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="Stanowisko"/>"/>
			-
			<ds:lang text="stanowisko"/>
		</span>
		<span class="continuousElement">
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="GrupaUzytkownikow"/>"/>
			-
			<ds:lang text="grupa"/>
		</span>

		<c:if test="${!empty userBeans}">
			<h3><ds:lang text="UzytkownicyWtymDzialeGrupieStanowisku"/></h3>
			
			<ul class="clearList">
				<c:forEach items="${userBeans}" var="bean">
					<li>
						<a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'>
							<c:out value="${bean.title}"/></a>
					</li>
				</c:forEach>
			</ul>
		</c:if>
		
		<c:if test="${empty userBeans}">
			<br/>
			<br/>
		</c:if>
		
		<span class="star">*</span><ds:lang text="PoleObowiazkowe"/>
	</html:form>
</c:if>

<c:if test="${!canEditSubstitutions}">
	<p>
		<ds:lang text="BrakUprawnienDoEdycjiZastepstw"/>.
	</p>
</c:if>

<%--
<h1><ds:lang text="Zastepstwa"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />
<%System.out.println("user-list.jsp"); %>
<c:if test="${canEditSubstitutions}">

	<html:form action="/office/users/user-list">
	<html:hidden property="divisionGuid"/>


	<table width="100%">
	<tr>
		<td>
			<c:out value="${tree}" escapeXml="false" />

			<hr size="1" noshade="noshade"/>
			<ds:lang text="Legenda"/>: <br/>

			<nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-closed.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="GrupaUzytkownikow"/>"/> - <ds:lang text="dzial"/></nobr>
			<nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="Stanowisko"/>"/> - <ds:lang text="stanowisko"/></nobr>
			<nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="<ds:lang text="GrupaUzytkownikow"/>"/> - <ds:lang text="grupa"/></nobr>
		</td>
	</tr>
	<tr>
		<td>

			<c:if test="${!empty userBeans}">
				<p>&nbsp;</p>
				<h3><ds:lang text="UzytkownicyWtymDzialeGrupieStanowisku"/></h3>

				<table border="0" cellspacing="2" cellpadding="0">
					<c:forEach items="${userBeans}" var="bean">
						<tr>
							<td><a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.title}"/></a></td>
						</tr>
					</c:forEach>
				</table>
			</c:if>

		</td>
	</tr>
	</table>

	<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

	</html:form>

</c:if>
<c:if test="${!canEditSubstitutions}">
	<p><ds:lang text="BrakUprawnienDoEdycjiZastepstw"/>.</p>
</c:if>

<!--
<script>
	document.getElementById('username').value = null;
	document.getElementById('doAddUser').value = null;
</script>
-->
--%>
<!--N koniec user-list.jsp N-->