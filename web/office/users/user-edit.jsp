<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N user-edit.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />


<c:forEach items="${tabs}" var="tab" varStatus="status" >
	<c:choose>
		<c:when test="${tab.selected}">
			<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><c:out value="${tab.title}"/></h1>
			<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
		</c:when>
	</c:choose>
</c:forEach>

<ds:available test="!layout2">
<c:forEach items="${tabs}" var="tab" varStatus="status" >
	<c:choose>
		<c:when test="${tab.selected}">
			<a class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>" href='<c:out value="${tab.link}"/>'>
				<c:out value="${tab.title}"/></a>
		</c:when>
		<c:otherwise>
			<a href='<c:out value="${tab.link}"/>'>
				<c:out value="${tab.title}"/></a>
		</c:otherwise>
	</c:choose>
	<c:if test="${!status.last}">
		<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
	</c:if>
</c:forEach>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<c:forEach items="${tabs}" var="tab" varStatus="status" >
		<a href='<c:out value="${tab.link}"/>' <c:choose><c:when test="${tab.selected}">class="highlightedText"</c:when></c:choose> ><c:out value="${tab.title}"/></a>
		<c:if test="${!status.last}">
			<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
		</c:if>
	</c:forEach>
</div>
</ds:available>

<p></p>

<edm-html:errors/>
<edm-html:messages/>

<c:if test="${tab=='data'}">
	<html:form action="/office/users/user-edit" onsubmit="check()">
		<html:hidden property="divisionGuid"/>
		<html:hidden property="username"/>
		<html:hidden property="tab"/>
		<html:hidden property="substId"/>
		<html:hidden property="mySubstitude"/>
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
		<table class="tableMargin">
			<tr>
				<td>
					<ds:lang text="NazwaUzytkownika"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.username}"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Imie"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.firstname}"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Nazwisko"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.lastname}"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="KodUzytkownika"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.identifier}"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Email"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.email}"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Przelozony"/>:
				</td>
				<td>
					<c:out value="${office_userEdit.supervisor}"/>
				</td>
			</tr>
			<c:if test="${canEditSubstitutions}">
				<tr>
					<td>
						<ds:lang text="ZastepowanyPrzez"/>:
					</td>
					<td>
						<html:select property="substitutedBy" styleClass="sel combox-chosen">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
							<html:optionsCollection name="substitutedBys"/>
						</html:select>
						
						<ds:lang text="od"/>
						<html:text property="substitutedFrom" styleId="substitutedFrom" size="10" maxlength="10" styleClass="txt"/>
						<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif" id="calendar_substitutedFrom_trigger"
							style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
						<ds:lang text="do"/>
						<html:text property="substitutedThrough" styleId="substitutedThrough" size="10" maxlength="10" styleClass="txt"/>
						<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif" id="calendar_substitutedThrough_trigger"
							style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
						<html:submit property="doAddSubst" styleClass="btn">Dodaj zastępstwo</html:submit>
					</td>
				</tr>
				
				<script type="text/javascript">
					Calendar.setup({
						inputField	:	"substitutedThrough",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"calendar_substitutedThrough_trigger",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
					Calendar.setup({
						inputField	:	"substitutedFrom",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"calendar_substitutedFrom_trigger",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
				</script>
			</c:if>
			<c:if test="${!canEditSubstitutions}">
				<tr>
					<td>
						<ds:lang text="ZastepowanyPrzez"/>:
					</td>
					<td>
						<html:select property="substitutedBy" styleClass="sel combox-chosen" disabled="true">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
							<html:optionsCollection name="substitutedBys"/>
						</html:select>
						
						<ds:lang text="do"/>
						<html:text property="substitutedThrough" styleId="substitutedThrough" size="10" maxlength="10" styleClass="txt" disabled="true" />
						<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif" id="calendar_substitutedThrough_trigger"
							style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
					</td>
				</tr>
			</c:if>
		</table>
		
		<c:if test="${!empty acctualSubst}">
			<ds:available test="layout2">
				<div id="middleContainer"> <!-- BIG TABLE start -->
			</ds:available>
			<h4>Zastępstwa:</h4>
			
			<table class="tableMargin">
				<tr>
					<th style="width:100px" class="alignCenter">
						<ds:lang text="Poczatek"/>
					</th>
					<th style="width:100px" class="alignCenter">
						<ds:lang text="Koniec"/>
					</th>
					<th class="alignCenter">
						Osoba zastępująca
					</th>
					<th class="alignCenter">
                        <ds:lang text="KtoStworzylZastepstwo"/>
                    </th>
                    <th class="alignCenter">
                        <ds:lang text="Status"/>
                    </th>
                    <th class="alignCenter"></th>
				</tr>
				
				<c:forEach items="${acctualSubst}" var="acctualSubst" >
					<tr>
						<td class="alignCenter">
							<fmt:formatDate value="${acctualSubst.substitutedFrom}" pattern="yyyy-MM-dd"/>
						</td>
						<td class="alignCenter">
							<fmt:formatDate value="${acctualSubst.substitutedThrough}" pattern="yyyy-MM-dd"/>
						</td>
						<td class="alignCenter">
							<c:out value="${acctualSubst.substUsername}"/>
						</td>
						<td class="alignCenter">
                            <c:out value="${acctualSubst.substituteCreator}"/>
                        </td>
                        <td class="alignCenter">
                            <c:out value="${acctualSubst.status}"/>
                        </td>
						<td>
							<c:if test="${!office_userEdit.mySubstitude}">
								<a href='<c:out value="${pageContext.request.contextPath}/office/users/user-edit.do?doDeleteSubst=true&username=${acctualSubst.login}&substId=${acctualSubst.substId}"/>'>
								<ds:lang text="Usun"/></a>
							</c:if>
							<c:if test="${office_userEdit.mySubstitude}">
								<a href='<c:out value="${pageContext.request.contextPath}/office/users/user-edit.do?doDeleteSubst=true&mySubstitude=true&substId=${acctualSubst.substId}"/>'>
								<ds:lang text="Usun"/></a>
							</c:if>
							
						</td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
		
		<c:if test="${!empty divisionNames}">
			<h4><ds:lang text="Dzialy"/></h4>
			
			<c:forEach items="${divisionNames}" var="name">
				<c:out value="${name}"/>
				<br/>
			</c:forEach>
		</c:if>
		
		<c:if test="${!empty substitutedUsers}">
			<h4><ds:lang text="ZastepowaniUzytkownicy"/></h4>
			
			<c:forEach items="${substitutedUsers}" var="bean">
				<a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'>
					<c:out value="${bean.title}"/></a>
				<br/>
			</c:forEach>
		</c:if>
		
		<br/>
		<!--  
		<html:submit property="doUpdate" styleClass="btn">
			<edm-html:message key="doUpdate" global="true"/>
		</html:submit>
		
		<html:submit property="doCancel" styleClass="cancel_btn">
			<edm-html:message key="doCancel" global="true"/>
		</html:submit>
		
		<c:if test="${substituted}">
			<html:submit property="doDelete" styleClass="btn">
				<ds:lang text="ZakonczZastepstwo"/>
			</html:submit>
		</c:if>
		-->
		<p>
			<span class="star">*</span>
			<ds:lang text="PoleObowiazkowe"/>
		</p>
		
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	</html:form>
			
	<script type="text/javascript">
		function check()
		{
			if((document.forms[0].substitutedFrom != null || document.forms[0].substitutedThrough != null) && document.forms[0].substitutedFrom == null)
			alert("<ds:lang text="NiePodanoNazwyUzytkownikaZastepujacegoZastepstwoNieZotanieZapisane"/>");
		}
	</script>
</c:if>

<c:if test="${tab=='history'}">
	<c:if test="${!empty histBy}">
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		<h4><ds:lang text="OsobyZastepujaceUzytkownika"/></h4>
		
		<table class="tableMargin">
			<tr>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Poczatek"/>
				</th>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Koniec"/>
				</th>
				<th class="alignCenter">
					<ds:lang text="KtoZastepowal"/>
				</th>
				<th class="alignCenter">
                    <ds:lang text="KtoStworzylZastepstwo"/>
                </th>
                <th class="alignCenter">
                    <ds:lang text="Status"/>
                </th>
               	<th class="alignCenter">
                    <ds:lang text="Data anulowania"/>
                </th>
			</tr>
			
			<c:forEach items="${histBy}" var="histBy" >
				<tr>
					<td class="alignCenter">
						<fmt:formatDate value="${histBy.substitutedFrom}" pattern="yyyy-MM-dd"/>
					</td>
					<td class="alignCenter">
						<fmt:formatDate value="${histBy.substitutedThrough}" pattern="yyyy-MM-dd"/>
					</td>
					<td class="alignCenter">
						<c:out value="${histBy.substUsername}"/>
					</td>
					<td class="alignCenter">
						<c:out value="${histBy.substituteCreator}"/>
					</td>
					<td class="alignCenter">
						<c:out value="${histBy.status}"/>
					</td>
					<td class="alignCenter">
                        <c:out value="${histBy.cancelDate}"/>
                    </td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
	
	<c:if test="${!empty hist}">
		<h4><ds:lang text="OsobyZastepowanePrzezUzytkownika"/></h4>
		
		<table class="tableMargin">
			<tr>
				<th class="alignCenter">
					<ds:lang text="KogoZastepowal"/>
				</td>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Poczatek"/>
				</td>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Koniec"/>
				</td>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="KtoStworzylZastepstwo"/>
				</td>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Status"/>
				</td>
				<th style="width:100px" class="alignCenter">
					<ds:lang text="Data anulowania"/>
				</td>
			</tr>
			
			<c:forEach items="${hist}" var="hist" >
				<tr>
					<td class="alignCenter">
						<c:out value="${hist.username}"/>
					</td>
					<td class="alignCenter">
						<fmt:formatDate value="${hist.substitutedFrom}" pattern="yyyy-MM-dd"/>
					</td>
					<td class="alignCenter">
						<fmt:formatDate value="${hist.substitutedThrough}" pattern="yyyy-MM-dd"/>
					</td>
					<td class="alignCenter">
                        <c:out value="${hist.substituteCreator}"/>
					</td>
					<td class="alignCenter">
                        <c:out value="${hist.status}"/>
					</td>
					<td class="alignCenter">
                        <c:out value="${hist.cancelDate}"/>
                    </td>
				</tr>
			</c:forEach>
		</table>
	</c:if>
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</c:if>

<%--
<c:forEach items="${tabs}" var="tab" varStatus="status" >
		<c:choose>
		 <c:when test="${tab.selected}">
				<h1><c:out value="${tab.title}"/></h1>
				<hr size="1" align="left" class="horizontalLine" width="77%" />
		 </c:when>
		</c:choose>
</c:forEach>

<c:forEach items="${tabs}" var="tab" varStatus="status" >
		<c:choose>
		 <c:when test="${tab.selected}">
				<a class="highlightedText" href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
		 </c:when>
		 <c:otherwise>
				<a href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
		 </c:otherwise>
		</c:choose>
	<c:if test="${!status.last}"> <img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/> </c:if>
</c:forEach>

<edm-html:errors />
<edm-html:messages />



<html:form action="/office/users/user-edit" onsubmit="check()">
<html:hidden property="divisionGuid"/>
<html:hidden property="username"/>
<html:hidden property="tab"/>
<c:if test="${tab=='data'}">
<table border='0' cellspacing='0' cellpadding='0'>
<tr>
	<td><ds:lang text="NazwaUzytkownika"/>:&nbsp</td>
	<td><c:out value="${office_userEdit.username}"/></td>
</tr>
<tr>
	<td><ds:lang text="Imie"/><span class="star">*</span>:</td>
	<td><html:text property="firstname" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="Nazwisko"/><span class="star">*</span>:</td>
	<td><html:text property="lastname" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="KodUzytkownika"/>:</td>
	<td><html:text property="identifier" size="5" maxlength="20" styleClass="txt"/></td>
</tr>
<tr>
	<td><ds:lang text="Email"/>:</td>
	<td><html:text property="email" size="50" maxlength="50" styleClass="txt" /></td>
</tr>
<tr>
	<td><ds:lang text="Przelozony"/>:</td>
	<td><html:select property="supervisor" styleClass="sel_opt" >
			<option value=""><ds:lang text="select.wybierz"/></option>
			<html:optionsCollection name="supervisors"/>
		</html:select></td>
</tr>
<c:if test="${canEditSubstitutions}">
	<tr>
		<td><ds:lang text="ZastepowanyPrzez"/>:</td>
		<td><html:select property="substitutedBy" styleClass="sel_opt" >
				<option value=""><ds:lang text="select.wybierz"/></option>
				<html:optionsCollection name="substitutedBys"/>
			</html:select>
			<ds:lang text="od"/>
			<html:text property="substitutedFrom" styleId="substitutedFrom" size="10" maxlength="10" styleClass="txt"/>
			<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
				id="calendar_substitutedFrom_trigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<ds:lang text="do"/>
			<html:text property="substitutedThrough" styleId="substitutedThrough" size="10" maxlength="10" styleClass="txt"/>
			<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
				id="calendar_substitutedThrough_trigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
		</td>
	</tr>
</c:if>
<c:if test="${!canEditSubstitutions}">
	<tr>
		<td><ds:lang text="ZastepowanyPrzez"/>:</td>
		<td><html:select property="substitutedBy" styleClass="sel_opt" disabled="true" >
				<option value=""><ds:lang text="select.wybierz"/></option>
				<html:optionsCollection name="substitutedBys"/>
			</html:select>
			<ds:lang text="do"/>
			<html:text property="substitutedThrough" styleId="substitutedThrough" size="10" maxlength="10" styleClass="txt" disabled="true" />
			<img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
				id="calendar_substitutedThrough_trigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
		</td>
	</tr>
</c:if>
</table>

<%--
<table>
<tr>
	<th>Grupy</th>
	<th>Role kancelarii</th>
</tr>
<tr>
	<td valign="top">
		<c:if test="${!empty groups}">
			<table>
			<c:forEach items="${groups}" var="group">
				<tr>
					<td><c:out value="${group.name}"/>:</td>
					<td><html-el:multibox property="groups" value="${group.guid}"/></td>
				</tr>
			</c:forEach>
			</table>
		</c:if>
	</td>
	<td valign="top">
		<table>
			<c:forEach items="${officeRoles}" var="role">
				<tr>
					<td><c:out value="${role.label}"/>:</td>
					<td><html-el:multibox property="officeRoles" value="${role.value}"/></td>
				</tr>
			</c:forEach>
		</table>
	</td>
</tr>
</table>
-->

<c:if test="${!empty divisionNames}">
	<h4><ds:lang text="Dzialy"/></h4>
	<c:forEach items="${divisionNames}" var="name">
		<c:out value="${name}"/><br/>
	</c:forEach>
</c:if>

<c:if test="${!empty substitutedUsers}">
	<h4><ds:lang text="ZastepowaniUzytkownicy"/></h4>
	<c:forEach items="${substitutedUsers}" var="bean">
		<a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.title}"/></a><br/>
	</c:forEach>
</c:if>

<p></p>

<script type="text/javascript">
function check(){
	if((document.forms[0].substitutedFrom!=null||document.forms[0].substitutedThrough!=null)&&document.forms[0].substitutedFrom==null)
	alert("<ds:lang text="NiePodanoNazwyUzytkownikaZastepujacegoZastepstwoNieZotanieZapisane"/>");
	}
</script>
<html:submit property="doUpdate" styleClass="btn" ><edm-html:message key="doUpdate" global="true"/></html:submit>
<html:submit property="doCancel" styleClass="cancel_btn"><edm-html:message key="doCancel" global="true" /></html:submit>
<c:if test="${substituted}">
	<html:submit property="doDelete" styleClass="btn"><ds:lang text="ZakonczZastepstwo"/></html:submit>
</c:if>
<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
<%--<p><html:link forward="users/user-list">Lista użytkowników</html:link></p>-->
</c:if>
</html:form>



<c:if test="${tab=='data'}">
<script type="text/javascript">
	<c:if test="${canEditSubstitutions}">
		Calendar.setup({
			inputField	:	"substitutedThrough",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_substitutedThrough_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	:	"substitutedFrom",	 // id of the input field
			ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		:	"calendar_substitutedFrom_trigger",  // trigger for the calendar (button ID)
			align		:	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	</c:if>
</script>
</c:if>

<c:if test="${tab=='history'}">


<c:if test="${!empty histBy}">
<table cellspacing='0' cellpadding='1' style="border: 1px solid black;">
<tr><td colspan='3' valign='center'><strong><ds:lang text="OsobyZastepujaceUzytkownika"/></strong></td></tr>
<tr>
	<td width=120px><strong><ds:lang text="Poczatek"/></strong></td>
	<td width=120px><strong><ds:lang text="Koniec"/></strong></td>
	<td width=200px><strong><ds:lang text="KtoZastepowal"/></strong></td>
</tr>

<c:forEach items="${histBy}" var="histBy" >
	<tr>
		<td ><fmt:formatDate value="${histBy.substitutedFrom}" pattern="yyyy-MM-dd"/></td>
		<td ><fmt:formatDate value="${histBy.substitutedThrough}" pattern="yyyy-MM-dd"/></td>
		<td ><c:out value="${histBy.substUsername}"/></td>
	</tr>
</c:forEach>
</table>
</c:if>
<c:if test="${!empty hist}">
<br><br>
<table cellspacing='0' cellpadding='1' style="border: 1px solid black;">
<tr><td colspan='3' valign='center'><strong><ds:lang text="OsobyZastepowanePrzezUzytkownika"/></strong></td></tr>
<tr>
	<td width='200px'><strong><ds:lang text="KogoZastepowal"/></strong></td>
	<td width='120px'><strong><ds:lang text="Poczatek"/></strong></td>
	<td width='120px'><strong><ds:lang text="Koniec"/></strong></td>
</tr>

<c:forEach items="${hist}" var="hist" >
	<tr>
		<td ><c:out value="${hist.username}"/></td>
		<td ><fmt:formatDate value="${hist.substitutedFrom}" pattern="yyyy-MM-dd"/></td>
		<td ><fmt:formatDate value="${hist.substitutedThrough}" pattern="yyyy-MM-dd"/></td>
		
	</tr>
</c:forEach>
</table>
</c:if>
</c:if>

--%>
<!--N koniec user-edit.jsp N-->