<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-task.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Pismo</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<p>Skr�cone informacje o pi�mie</p>

<table>
    <tr>
        <td width="50%">Przyj�te przez: <b><ww:property value="creatingUser"/></b></td>
        <td width="50%">Dzia�: <b><ww:property value="assignedDivision"/></b></td>
    </tr>
    <tr>
        <td width="50%">Numer kancelaryjny: <b><ww:if test="document.masterDocument != null">[</ww:if><ww:property value="document.officeNumber"/><ww:if test="document.masterDocument != null">]</ww:if></b></td>
        <td width="50%">Numer w sprawie: <b><ww:property value="caseDocumentId"/></b></td>
    </tr>
    <tr>
        <td width="50%">
            Nadawca: <b><ww:property value="document.sender.summary"/></b>
        </td>
        <td width="50%">
            Odbiorcy: <b><ww:iterator value="document.recipients" status="status">
                            <ww:property value="summary"/><ww:if test="!#status.last">, </ww:if>
                        </ww:iterator></b>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            Opis: <b><ww:property value="document.description"/></b>
        </td>
    </tr>
</table>

<ww:if test="tasks.size > 0">
<h4>Zadania, w kt�rych znajduje si� pismo</h4>
<ul>
    <ww:iterator value="tasks">
        <li><ww:property value="_activity.name"/></li>
    </ww:iterator>
</ul>
</ww:if>
