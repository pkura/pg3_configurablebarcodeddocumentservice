<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="RaportKolejek"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/office/queue-stats.action'"/>" method="post">
<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="Aspekt"/>
			</th>
			<th>
				<ds:lang text="Segment"/>
			</th>
			<th colspan="2">
				<ds:lang text="Typ"/>
			</th>
		</tr>
		<tr>
			<td class="alignTop">
				<ww:select name="'selectedAspects'" id="selectedAspects" multiple="true" size="8" cssClass="'multi_sel'"
					cssStyle="'width: 100px;'" list="availableAspects" onchange="'refreachForm()'"/>
			</td>
			<td class="alignTop">
				<ww:select name="'selectedSegments'" id="selectedSegments" multiple="true" size="8" cssClass="'multi_sel'"
					cssStyle="'width: 200px;'" list="availableSegments"/>
				<br>
				<a href="javascript:void(select(document.getElementById('selectedSegments'), true))"><ds:lang text="zaznacz"/></a>
				/
				<a href="javascript:void(select(document.getElementById('selectedSegments'), false))"><ds:lang text="odznacz"/></a>
			</td>
		
			<td class="alignTop">
				<ww:select name="'selectedTypes'" id="selectedTypes" multiple="true" size="8" cssClass="'multi_sel'"
					cssStyle="'width: 300px;'" list="availableTypes"/>
				<br>
				<a href="javascript:void(select(document.getElementById('selectedTypes'), true))"><ds:lang text="zaznacz"/></a>
				/
				<a href="javascript:void(select(document.getElementById('selectedTypes'), false))"><ds:lang text="odznacz"/></a>
			</td>
		</tr>
</table>
<ds:submit-event value="getText('Pokaz')" name="'doUpdate'"/>
</form>
<ww:if test="list.size()>0">
<table>
	<tr>
		<!--<th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=segment&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Segment"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=segment&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
			
		</th>
		--><th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=type&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Typ"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=type&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=lessthanday&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Ponizej24h"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=lessthanday&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=onetothreedays&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Od24hdo72h"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=onetothreedays&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=morethanthreedays&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Powyzej72h"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=morethanthreedays&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=total&ascending=false&doUpdate=true" 
			alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
			<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Total"/>
			<a href="<ww:url value="'/office/queue-stats.action'"/>?sortField=total&ascending=true&doUpdate=true"
			alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
			<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
	</tr>

<ww:iterator value="list">

	<tr>
		<!--<td>
			<ww:property value="segment"/>
		</td>
		--><td>
			<ww:property value="type"/>
		</td>
		<td align="center">
			<ww:property value="less24"/>
		</td>
		<td align="center">
			<ww:property value="between24and72"/>
		</td>
		<td align="center">
			<ww:property value="more72"/>
		</td>
		<th style="text-align:center">
			<ww:property value="total"/>
		</th>
	</tr>
</ww:iterator>
	<tr>
		<th style="text-align:right">
			<ds:lang text="Suma"/>
		</th>
		<th style="text-align:center">
			<ww:property value="tot.less24"/>
		</th>
		<th style="text-align:center">
			<ww:property value="tot.between24and72"/>
		</th >
		<th style="text-align:center">
			<ww:property value="tot.more72"/>
		</th>
		<th style="text-align:center">
			<ww:property value="tot.total"/>
		</th>
	</tr>

</table>
</ww:if>

<script type="text/javascript">
	function refreachForm()
	{
		document.getElementById('form').submit();
	}
</script>
