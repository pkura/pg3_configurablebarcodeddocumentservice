<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://www.opensymphony.com/oscache" prefix="cache" %>

<h1><ds:lang text="StatystykaListZadan"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table width="100%">
<tr>
<th><a href="<ww:url value="tasklist-stats.action"><ww:param name="'sortStyle'" value="'name'"/></ww:url>"><ds:lang text="uzytkownik"/></a></th>
<th><a href="<ww:url value="tasklist-stats.action"><ww:param name="'sortStyle'" value="'new'"/></ww:url>"><ds:lang text="noweZadania"/></a></th>
<th><a href="<ww:url value="tasklist-stats.action"><ww:param name="'sortStyle'" value="'old'"/></ww:url>"><ds:lang text="zaakceptowaneZadania"/></a></th>
<th><a href="<ww:url value="tasklist-stats.action"><ww:param name="'sortStyle'" value="'all'"/></ww:url>"><ds:lang text="zadaniaLacznie"/></a></th>
</tr>
<ww:iterator value="taskListBeans">
<tr>
	<td>
	<a href="<ww:url value="'/office/tasklist/any-user-task-list.action'"><ww:param name="'username'" value="user.getName()"/></ww:url>"><ww:property value="user.asLastnameFirstname()"/></a>
	</td>
	<td><ww:property value="newTasks" /></td>
	<td><ww:property value="oldTasks" /></td>
	<td><ww:property value="getAllTasks()"/></td>
</tr>
</ww:iterator>
</table>