<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="MojeStatystyki"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
<tr>
	<th>
		<ds:lang text="LiczbaPrzeprocesowanychZadanDzisiaj"/>
	</th>
	<td>
		<ww:property value="processed"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="LiczbaPrzeprocesowanychZadanWTymMiesiacu"/>
	</th>
	<td>
		<ww:property value="processedFromBeg"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="LiczbaOtrzymanychZadanDzisiaj"/>
	</th>
	<td>
		<ww:property value="received"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="LiczbaOtrzymanychZadanWTymMiesiacu"/>
	</th>
	<td>
		<ww:property value="receivedFromBeg"/>
	</td>
</tr>


</table>



