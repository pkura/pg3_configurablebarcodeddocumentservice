<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-rwa.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Nowe RWA</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/admin/new-rwa">

<table>
<tr>
    <td>Nazwa <span class="star">*</span>:</td>
    <td><html:text property="name" size="50" styleClass="txt"/></td>
</tr>
<tr>
    <td>Liczba poziom�w <span class="star">*</span>:</td>
    <td><html:text property="numLevels" size="3" styleClass="txt"/></td>
</tr>
<tr>
    <td></td>
    <td><input type="submit" name="doCreate" value="Utw�rz" class="btn"/>
        <input type="submit" name="doCancel" value="Powr�t" class="btn"/>
    </td>
</tr>
</table>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

</html:form>
