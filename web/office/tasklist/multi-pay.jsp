<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N multi-pay.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="OznaczanieWieluPismJakoZaplacone"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/multi-pay.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:iterator value="documentIds">
        <ww:hidden name="'documentIds'" value="top"/>
    </ww:iterator>
    <ww:hidden name="'tab'"/>
    <table>
    	<tr>
    		<td><ds:lang text="DataZaplaty"/> :</td>
    		<td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        		<img src="<ww:url value="'/calendar096/img.gif'"/>"
            	id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            	title="Date selector" onmouseover="this.style.background='red';"
            	onmouseout="this.style.background=''"/></td>
    	</tr>
    	<tr>
    		<td><ds:submit-event value="getText('OznaczJakoZaplacone')" name="'doPay'" cssClass="'btn'"/></td>
    		<td><ds:button-redirect href="'/office/index.jsp'" value="getText('Rezygnacja')"/></td>
    	</tr>
    </table>
    
</form> 

<script type="text/javascript">
Calendar.setup({
        inputField     :    "documentDate",     							// id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      	// format of the input field
        button         :    "documentDateTrigger",  						// trigger for the calendar (button ID)
        align          :    "Tl",           								// alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>