<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="org.apache.commons.logging.Log,
                 org.apache.commons.logging.LogFactory"%>
<%@ page import="pl.compan.docusafe.core.users.DSUser"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%!
	private static final Log log = LogFactory.getLog("task");
%>
<%
    Object substitute = session.getAttribute("substitute");
    session.removeAttribute("substitute");
    request.setAttribute("substitute", substitute);
%>

<c:if test="${substitute}">
    <script type="text/javascript">
        $j(document).ready(function(){
            $j("#loginConfirm").dialog({
                resizable: false,
                modal:true,
                buttons: {
                    OK: function() {
                        $j( '#loginConfirm' ).dialog( "close" );
                    }
                }
            });
        });
    function loginConfirm(){
        $j('#loginConfirm').dialog('open');
    }
    </script>
    <div id="loginConfirm" title="Zast�pstwo">
        <p> Zast�puje Ci� inny u�ytkownik. </p>
    </div>
</c:if>

<script type="text/javascript">

function showActivityKey(activityKey, tit) {
	$j.post('task-position.action',
			{activityKey: activityKey},
			function(data) {
				//alert(data);
				$j("#tp_ajax").html('<B>' + data + '</B>').dialog(
						{close: function(event, ui) { $j("#tp_ajax").dialog('destroy')},
						 title: tit}
				);
			});
}

</script>

<h1><ww:property value="tabs.selectedTab.title"/></h1>
<div id="tp_ajax" style="display: none">
</div>

<ds:available test="nowe_menu_filtrowania">
<script language="javascript" src="<ww:url value="'/gmenu.js'"/>" ></script>
<script language="javascript">
						var $j = jQuery.noConflict();
						$j(document).ready(function() {
							if(!document.getElementById('selectActionButton')) {
								var el = document.createElement('DIV');
								var parent = document.getElementById('action').parentNode;
								el.id = 'selectActionButton';
								el.className = 'gmenuButton';
								el.onclick = Function("gmenuShowFilters(this, 1, true)");
								el.onmouseover = Function("gmenuButtonOver(this,true)");
								el.onmouseout = Function("gmenuButtonOver(this,false)");
								el.innerHTML = '<img src="' + '<ww:url value="'/img/gm_arrow_down.gif'"/>' + '" id=\"selectActionButton_img\" class="arrow" />';
								el.innerHTML += 'Wybierz akcj�';

								parent.insertBefore(el, document.getElementById('action'));
								document.getElementById('action').nextSibling.nextSibling.className = 'gbtn';
								document.getElementById('action').style.display = 'none';
							}
							
							/*$j('.gtxt').hover(function() {
								$j(this).addClass('gtxtOver');
							}, function() {
								$j(this).removeClass('gtxtOver');
							});*/

							$j('.gtxt').focus(function() {
								$j(this).addClass('gtxtOver');
							});
							$j('.gtxt').blur(function() {
								$j(this).removeClass('gtxtOver');
							});
							
							$j('.gbtn').hover(function() {
								$j(this).addClass('gbtnOver');
							}, function() {
								$j(this).removeClass('gbtnOver');
							});

							/*
							<div id="selectActionButton" class="gmenuButton" onclick="gmenuShowFilters(this, 1, true)" onmouseover="gmenuButtonOver(this,true)" onmouseout="gmenuButtonOver(this,false)">
							<img src="<ww:url value="'/img/gm_arrow_down.gif'"/>" id="selectActionButton_img" class="arrow" />
								Wybierz akcj�
							</div>
							*/

							if(document.getElementById('filterBy'))
								gmenuImportForm(0, 'filterBy', 'filterByButton', 'Filtruj', 'Brak', 'gmenuFilterName');
							if(document.getElementById('action'))
								gmenuImportForm(1, 'action', 'selectActionButton', 'Wybierz akcj�', '-- wybierz akcje --', '');
							

							// jesli stary layout, to pole jest za niskie - naprawiamy
							<ds:available test="!layout2">
								$j('#gmenuFilterName').css('height', '22px');
							</ds:available>

							$j('#mainTable a:not(.nonDroppable)').draggable({
								revert: true,
								start: function(event, ui) {
									$j(this).addClass('highlightedRows');
								},
								stop: function(event, ui) {
									$j(this).removeClass('highlightedRows');
								} 
							});
							$j("#gmenuFilterName").droppable({
										drop: function(event, ui) {
											
											trace($j(ui.draggable).attr('title'));
											//$j('#gmenuFilterName').val($j(obj).html());
											$j(this).val($j(ui.draggable).text().trim());
											rewrite($j(ui.draggable).get(0));
											$j('input[name=DEFAULT]').trigger('click');
										},
										over: function(event, ui) {
											$j(this).css('background-color', '#fffbc4');
										},
										out: function(event, ui) {
											$j(this).css('background-color', '');
										}
									});
						});
					</script>	
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
<ww:iterator value="tabs" status="status" >
	<a name="<ww:property value='idName' />" bookmarkId="<ww:property value='idbookamrk'/>" href="<ww:url value='link'/>" title="<ww:property value='title'/>" class="middleMenuLink<ww:if test="selected"> middleMenuLinkHover middleMenuLinkSelected</ww:if>" <ww:if test="selected">id="tab2_selected"</ww:if>>
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left<ww:if test="selected">_pushed</ww:if>.gif" class="tab2_img" />
	<ww:property value="name"/>
	</a>
</ww:iterator>
</div>
</ds:available>

<ds:available test="!layout2">
<hr size="1" align="left"  width="77%" class="horizontalLine" />
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>
<!-- <div> -->  <!-- A start -->


<ds:available test="layout2">
<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
<p></p>

<ww:if test="'order'.equals(tab)">
	<ww:iterator value="orderTabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last">
			<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
		</ww:if>
	</ww:iterator>
	<p></p>
</ww:if>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form" onsubmit="disableFormSubmits(this);">
	
	<ww:hidden name="'tab'"/>
	<!--  
	<ds:available test="tasklist.jbpm">
		<ds:event name="'doSwitchWorkflow'" value="'Zamie� workflow'"/> 
	</ds:available>
	-->
	<input type="hidden" name="doManualFinish" id="doManualFinish"/>
	<input type="hidden" name="doAssignOfficeNumber" id="doAssignOfficeNumber"/>
	<input type="hidden" name="doReplyUnnecessary" id="doReplyUnnecessary"/>
	<input type="hidden" name="doDeleteOfficeNumber" id="doDeleteOfficeNumber"/>
	<input type="hidden" name="doSendToEva" id="doSendToEva"/>
	
	<input type="hidden" name="doDockindEvent" id="doDockindEvent"/>
	<input type="hidden" name="dockindEventValue" id="dockindEventValue"/>
	
	<input type="hidden" name="doTaskListEvent" id="doTaskListEvent"/>
	<input type="hidden" name="eventValue" id="eventValue"/>
	
	<input type="hidden" name="doManualPushToCoor" id="doManualPushToCoor"/>
	<input type="hidden" name="doAcceptance" id="doAcceptance"/>
	<input type="hidden" name="doAddToCase" id="doAddToCase"/>
	<input type="hidden" name="doMultiAssignment" id="doMultiAssignment"/>
	<input type="hidden" name="doRemoveWatches" id="doRemoveWatches"/>
	<input type="hidden" name="doRemoveOrders" id="doRemoveOrders"/>
	<input type="hidden" name="doAssignMe" id="doAssignMe"/>
	<input type="hidden" name="caseId" id="caseId"/>
	<input type="hidden" name="caseName" id="caseName"/>
	<input type="hidden" name="addToTask" id="addToTask"/>
	<input type="hidden" name="nameOfSmallAssignment" id="nameOfSmallAssignment"/>
	<input type="hidden" name="doSmallAssignment" id="doSmallAssignment"/>
	<input type="hidden" name="nameOfJournal" id="nameOfJournal"/>
	<input type="hidden" name="doDepartmentEnroll" id="doDepartmentEnroll"/>
	<input type="hidden" name="filterDockindNames" id="filterDockindNames"/>
	<input type="hidden" name="acceptanceCn" id="acceptanceCn"/>
	<input type="hidden" name="caseReopen" id="caseReopen"/>
	<input type="hidden" name="removeLabelId" id="removeLabelId"/>
	<input type="hidden" name="printEnvelopeId" id="printEnvelopeactionGenerateRtf.availableId"/>
	<input type="hidden" name="doGenerateXls" id="doGenerateXls"/>
	<input type="hidden" name="doGenerateCsv" id="doGenerateCsv"/>
	<input type="hidden" name="doGenerateXml" id="doGenerateXml"/>

	<input type="hidden" name="doGenerateXlsAll" id="doGenerateXlsAll"/>
    <input type="hidden" name="doGenerateCsvAll" id="doGenerateCsvAll"/>
    <input type="hidden" name="doGenerateXmlAll" id="doGenerateXmlAll"/>
	
	<ww:hidden name="'addToReceiver'" id="addToReceiver"/>
	<ww:hidden name="'withBackup'" id="withBackup"/>
	<ww:hidden name="'showNumberNewTask'" id="showNumberNewTask"/>
	
	<!--<div> --><!-- B start -->
		<ww:if test="usersAndDivisions != null">
			<div> <!-- C start -->
				<ww:select name="'username'" list="usersAndDivisions" value="username"
			
					headerKey="''" headerValue="'----'"
					cssClass="'sel combox-chosen'" />
							<ds:submit-event value="getText('PokazZadania')" name="'doXxx'" cssClass="'btn'"/>
				<ds:available test="tasklist.adminrefresh">
				<ww:if test="adminAccess">
					<ds:submit-event value="getText('doRefreshUserTaskList')" name="'doRefreshUserTaskList'" cssClass="'btn'"/>
				</ww:if>
				</ds:available>
				
				<p></p>
			</div>
		</ww:if>
		<div>  
			<!-- D start -->
				<ds:available test="labels">
					<ds:available test="labels.li"> 
						<div class="select-free"> 
							<div> <!-- E start -->
								<ul class="clearList noMarginPadding sf-menu">
									<li>
										<ds:lang text="PokazEtykiete"/>
										<ul class="clearList noMarginPadding">
											<li>
												<ds:lang text="FlagiUzytkownika"/>
												<ul class="clearList noMarginPadding">
													<ww:iterator value="userFlags_label">
													
														<li id="<ww:property value="id"/>" class="list-opacity">
															<ww:property value="getName()"/>
														</li>
													</ww:iterator>
												</ul>
											</li>
											<li> 
												<ds:lang text="FlagiWspolne"/>
												<ul class="clearList noMarginPadding">
													<ww:iterator value="systemFlags_label">
														<li id="<ww:property value="id"/>" class="list-opacity">
															<ww:property value="getName()"/>
														</li>
													</ww:iterator>
												</ul>
											</li>
											
											<li>
												<ds:lang text="EtykietySystemowe"/>
												<ul class="clearList noMarginPadding">
													<ww:iterator value="systemLabels_label">
														<li id="<ww:property value="id"/>" class="list-opacity">
															<ww:property value="getAliasName()"/>
														</li>
													</ww:iterator>
												</ul>
											</li>
											
											<li>
												<ds:lang text="EtykietyModyfikowalne"/>
												<ul class="clearList noMarginPadding">
													<ww:iterator value="labels_label">
														<li id="<ww:property value="id"/>" class="list-opacity">
															<ww:property value="getAliasName()"/>
														</li>
													</ww:iterator>
												</ul>
											</li>
										</ul>
									</li>
								</ul>
							</div>
							<%--<!--[if lte IE 6.5]><iframe></iframe><![endif]-->--%>
						</div> 
						
						
						<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/superfish.js"></script>
						
						<script type="text/javascript">
							$j(document).ready(function(){
								$j(".sf-menu").superfish(); 
								if ($j.browser.msie) $j(".sf-menu > li > ul >li > ul > li").hover(
									function() {
										$j(this).addClass("sfHover");
									}, 
									function() {
										$j(this).removeClass("sfHover");
									}
								);
								$j(".sf-menu > li > ul >li > ul > li").click(function() {
									$j("#viewedLabelId > option[value='" + $j(this).attr("id") + "']").attr("selected", "true").length ? submitLabel() : true ;
								});
							});
						</script>
						<ds:additions test="tcLayout">
							<div class="inMenuLabels hidden"> 
								<div class="round_top"> 
									<div class="round_1"></div>
									<div class="round_2"></div>
									<div class="round_3"></div>
									<div class="round_4"></div>
								</div>
								<div class="inMenuLabelsContent round_content">
									<img class="hidden inMenuLabelsContentImg" id="-1" src="<ww:url value="'/img/all.gif'"/>"/> 
									<ds:lang text="EtykietyModyfikowalne"/>
									<ul class="clearList noMarginPadding">
										<ww:iterator value="labels_label">
											<li id="<ww:property value="id"/>">
												<ww:property value="getAliasName()"/> <!-- (x) -->
											</li>
										</ww:iterator>
									</ul>
									<div class="inMenuLabelsArrows hidden alignCenter">
										<img class="listImg listUpFast" src="<ww:url value="'/img/arrow_fast_up.gif'"/>">
										<img class="listImg listUp" src="<ww:url value="'/img/arrow_up.gif'"/>">
										<img class="listImg listDown marginLeft10" src="<ww:url value="'/img/arrow_down.gif'"/>">
										<img class="listImg listDownFast" src="<ww:url value="'/img/arrow_fast_down.gif'"/>">
									</div>
								</div>
								<div class="round_bottom">
									<div class="round_4"></div>
									<div class="round_3"></div>
									<div class="round_2"></div>
									<div class="round_1"></div>
								</div>
							</div>
							
							<script type="text/javascript">
								var scrolluj = 0;
								function scrollList(what, where)
								{
									if (scrolluj == 0) return;
									$j(what).scrollTop( $j(what).scrollTop() + where );
									
									var command = "scrollList(\"" + what + "\", " + where + ")"; 
									setTimeout(command, 50);
								}
								
								$j(document).ready(function(){
									$j(".inMenuLabels").removeClass("hidden").insertAfter(".NEWleftPanelMenuUp");
									
									rozmiar();
									
									$j("#actualLabels > div").each(function() {
										$j(".inMenuLabelsContent > ul > li[id='" + $j(this).attr("id") + "']").addClass("selected");
									});
									
									$j(".inMenuLabelsContent > ul > li[class*='selected']").length ? $j(".inMenuLabelsContentImg").removeClass("hidden").click(function() {$j("#viewedLabelId > option[value='" + $j(this).attr("id") + "']").attr("selected", "true").length ? submitLabel() : true ;}) : true ;
									
									if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) $j(".inMenuLabelsContentImg").hover(
										function() {
											$j(this).addClass("highlightedRed");
										}, 
										function() {
											$j(this).removeClass("highlightedRed");
										}
									);
									
									$j(".inMenuLabelsContent > ul > li[class*='selected']").each(function() {
										if ($j(".inMenuLabelsContent > ul > li:first").hasClass("selected"))
											$j(this).prependTo(".inMenuLabelsContent > ul");
											
										$j(this).insertBefore($j(".inMenuLabelsContent > ul > li[class!='selected']:first"));
									});
									
									if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) $j(".inMenuLabelsContent > ul > li").hover(
										function() {
											$j(this).addClass("inMenuLabelsHover");
										}, 
										function() {
											$j(this).removeClass("inMenuLabelsHover");
										}
									);

									$j(".inMenuLabelsContent > ul > li").click(function() {
										if ( $j(this).hasClass("selected") )
										{
											removeLabel(this);
										}
										else
										{
											$j("#viewedLabelId > option[value='" + $j(this).attr("id") + "']").attr("selected", "true").length ? submitLabel() : true ;
										}
									});
									
									if ( $j(".inMenuLabelsContent li").length > 11 )
									{
										$j(".inMenuLabelsContent ul").css({height:"200px", overflow:"hidden"});
										$j(".inMenuLabelsArrows").removeClass("hidden");
										
										$j(".listUp").hover(
											function() {
												scrolluj = 1;
												$j(this).attr("src", "<ww:url value="'/img/arrow_up_hover.gif'"/>");
												scrollList(".inMenuLabelsContent ul", -4);
											},
											function() {
												scrolluj = 0;
												$j(this).attr("src", "<ww:url value="'/img/arrow_up.gif'"/>");
											}
										);
										
										$j(".listDown").hover(
											function() {
												scrolluj = 1;
												$j(this).attr("src", "<ww:url value="'/img/arrow_down_hover.gif'"/>");
												scrollList(".inMenuLabelsContent ul", 4);
											},
											function() {
												scrolluj = 0;
												$j(this).attr("src", "<ww:url value="'/img/arrow_down.gif'"/>");
											}
										);
										
										$j(".listUpFast").hover(
											function() {
												scrolluj = 1;
												$j(".listUp").attr("src", "<ww:url value="'/img/arrow_up_hover.gif'"/>");
												$j(this).attr("src", "<ww:url value="'/img/arrow_fast_up_hover.gif'"/>");
												scrollList(".inMenuLabelsContent ul", -12);
											},
											function() {
												scrolluj = 0;
												$j(".listUp").attr("src", "<ww:url value="'/img/arrow_up.gif'"/>");
												$j(this).attr("src", "<ww:url value="'/img/arrow_fast_up.gif'"/>");
											}
										);
										
										$j(".listDownFast").hover(
											function() {
												scrolluj = 1;
												$j(".listDown").attr("src", "<ww:url value="'/img/arrow_down_hover.gif'"/>");
												$j(this).attr("src", "<ww:url value="'/img/arrow_fast_down_hover.gif'"/>");
												scrollList(".inMenuLabelsContent ul", 12);
											},
											function() {
												scrolluj = 0;
												$j(".listDown").attr("src", "<ww:url value="'/img/arrow_down.gif'"/>");
												$j(this).attr("src", "<ww:url value="'/img/arrow_fast_down.gif'"/>");
											}
										);
									}
								});
							</script>
						</ds:additions>
					</ds:available>
					
					<ds:available test="labels.li">
						
					</ds:available>
					
					<div <ww:if test="!showLabels">class="hidden"</ww:if> style="padding-bottom: 10px;">
					
						<ww:if test="!('order'.equals(tab)) && !('watches'.equals(tab))">
							<div <ds:available test="labels.li">class="hidden"</ds:available>>
								<ds:lang text="PokazEtykiete"/>
								<ww:select id="viewedLabelId" name="'viewedLabelId'" list="availableLabels" listKey="id" listValue="aliasName" cssClass="'sel'" onchange="'submitLabel()'"/>
							</div>
							
						</ww:if>
						<div id="actualLabels"> 
							<ww:iterator value="viewedLabels">
								<div class="inline_flags" id="<ww:property value="id"/>">
									<b><ww:property value="name" /><a>
											<img id="<ww:property value="id"/>" src="<ww:url value="'/img/delete.gif'"/>" width="18" height="18" 
											title="<ds:lang text="UsunEtykiete"/>" onclick="removeLabel(this);"/></a></b>
								</div>
							</ww:iterator>
							<input type="hidden" name="doRemoveLabelFromView" id="doRemoveLabelFromView"/>
						</div>
						
					</div>
				</ds:available>
			</div> <!-- D ends -->
			<ww:if test="tasks != null && !('order'.equals(tab))">
				<ww:if test="watchesShowFilter || !('watches'.equals(tab)) " >
				
				<%--	<p><ds:lang text="ZnajdzNaLisciePismoOnumerzeKO"/>: <ww:textfield name="'searchOfficeNumber'" size="5" maxlength="7" cssClass="'txt'" />
				<ds:submit-event value="getText('Znajdz')" cssClass="'btn'" />  
				&nbsp	--%>
				
				<ds:available test="nowe_menu_filtrowania">
				<div style="display:none;"> <!-- ukryte menu filtrowania stare start -->
					<ww:select id="filterBy" name="'filterBy'" list="filterColumns" listKey="name" listValue="title" cssClass="'sel'" />
				</ds:available> 
				<ds:available test="tasklist.filtrujOld">
					
					<ds:lang text="FiltrujPo"/>:
					<ww:select id="filterBy" name="'filterBy'" list="filterColumns" listKey="name" listValue="title" cssClass="'sel'" />
					<ww:textfield name="'filterName'" cssClass="'txt'" size="20" maxlength="50"/>
					<ds:event name="'DEFAULT'" value="getText('Filtruj')" cssClass="'btn'"/>
					<ds:event name="'doRemoveFilters'" value="getText('UsunFiltry')" cssClass="'btn'" />
					
				</ds:available>
				<ds:available test="nowe_menu_filtrowania">
					</div>
				</ds:available>
				<ds:available test="nowe_menu_filtrowania">
					<div id="gmenu_container">opcja1</div>
					<div id="gmenu_shadow"></div>
					<div id="filterByButton" class="gmenuButton" onclick="gmenuShowFilters(this, 0, false)" onmouseover="gmenuButtonOver(this,true)" onmouseout="gmenuButtonOver(this,false)">
					<img src="<ww:url value="'/img/gm_arrow_down.gif'"/>" id="filterByButton_img" class="arrow" />
						Wybierz filtr
					</div>	
			
						
					
					<ww:textfield id="gmenuFilterName" name="'filterName'" maxlength="50" cssClass="'gtxt'" ><ww:param name="'onmouseover'" value="'insert(this);'"/></ww:textfield>
				
				<ds:event name="'DEFAULT'" value="getText('Filtruj')" cssClass="'gbtn'" />
				<ds:event name="'doRemoveFilters'" value="getText('UsunFiltry')" cssClass="'gbtn'" />
				</ds:available>
				</ww:if>
			</ww:if>
		
			<ww:if test="cases != null">
				<%--	<p>	--%>
				<ds:lang text="ZnajdzNaLiscieSpraweOnumerze"/>: <ww:textfield name="'searchOfficeId'" size="20" maxlength="30" cssClass="'txt'" />
				<ww:submit value="getText('Znajdz')" cssClass="'btn searchBtn'" />
				<%--	</p>	--%>
			</ww:if>
			<%--	<input type="submit" class="btn" value="Filter" onclick="setFilter()"/>	--%>
			
			<ds:available test="nowe_menu_filtrowania">
				<input type="text" style="visibility: hidden; width: 1px;" class="dontFixWidth"/>
				<span class="gquestCounter" <ww:if test="tasks == null">style="position: relative;"</ww:if> <ww:if test="'order'.equals(tab)"> style="position: relative" </ww:if> >
				
			</ds:available>
			
			<ds:available test="!tasklist.hidenumberoftasks">
				<ds:lang text="LiczbaZadanNaLiscie"/> :
				<ww:property value="taskCount"/>
				<ds:available test="menu.left.repository.paczki.dokumentow">
				<ds:lang text="LiczbaZadanWpaczkach"/> :
				<ww:property value="taskInPackageCount"/>
				</ds:available>
				
			
			<ww:if test="onTaskList">
				<ww:if test="withBackup">
					<a href="<ww:url value="getBaseLink()"><ww:param name="'tab'" value="tab"/><ww:param name="'withBackup'" value="false"/><ww:param name="'username'" value="username"/></ww:url>">
						<ds:lang text="UkryjBackup"/>
					</a>
				</ww:if>
	
				<ww:else>
					<ds:available test="tasklist.pokazZastepstwa">
						<a href="<ww:url value="getBaseLink()"><ww:param name="'tab'" value="tab"/><ww:param name="'withBackup'" value="true"/><ww:param name="'username'" value="username"/></ww:url>">
						<ds:lang text="PokazBackup"/>
						</a>
					</ds:available>
				</ww:else>
			</ww:if>
			</ds:available>
			<ds:available test="nowe_menu_filtrowania"></span></ds:available>
		<!-- koniec duzej tabelki -->
		
		<ds:available test="pager.tasklist.top">
			<ww:if test="tasks != null"> 
				<ww:set name="pager" scope="request" value="pager"/>
					<table width="100%">
						<tr>
							<td align="center">
								<jsp:include page="/pager-links-include.jsp"/>
							</td>
						</tr>
					</table>
			</ww:if>
		</ds:available>
		
	
	<table class="search" width="100%" cellspacing="0" id="mainTable">
		<tr class="tableheder">
			<ww:if test="ownTasks || (actionAssignMe && tasks != null)">
				<th class="empty"></th>
			</ww:if>
			<ww:if test="tab != 'cases'">
				<th class="empty"></th>
				<th class="empty"></th>
				<th class="empty"></th>
			</ww:if>
			<ww:iterator value="columns" status="status" > <%-- kolumny --%> 
				<th>
                                    <nobr>
                                        <a href="<ww:url value="sortDesc"/>" class="nonDroppable"
                                            name="<ww:property value="title"/>_descending"
                                            id="<ww:property value="title"/>_descending"
                                            alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
                                                    <ww:if test="sortField == property && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
                                                    <ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/><ww:param name="'name'" value="ascending"/></a>
                                        <ww:property value="title"/>
                                        <a href="<ww:url value="sortAsc"/>" class="nonDroppable"
                                            name="<ww:property value="title"/>_ascending"
                                            alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
                                                    <ww:if test="sortField == property && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
                                                    <ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
                                    </nobr>
				</th>
			</ww:iterator>
		</tr>
		
		<%--	zadania	--%>
		<ww:set name="numberRows" value="1"/>
		<ww:if test="tasks != null">
			<ww:iterator value="tasks" status="taskStat">
				<ww:set name="result"/>
				<tr   <ww:if test="#taskStat.odd != true">class="oddRows"</ww:if> <ds:available test="tasklist.highlightedRows"> onmouseover="$j(this).addClass('highlightedRows')" onmouseout="$j(this).removeClass('highlightedRows')" </ds:available> >
					<ww:if test="( ownTasks || actionAssignMe ) && !('watches'.equals(tab))">
						<td>
							<ww:checkbox name="'activityIds'" id="check" fieldValue="#result.activityId"/>
						</td>
					</ww:if>
					<ww:elseif test="('watches'.equals(tab))">
						<td>
							<ww:checkbox name="'urns'" fieldValue="#result.urn"/>
						</td>
					</ww:elseif>
					<td>
						<input type="button" style="background:url('<ww:url value="'/img/arrow.png'" />') no-repeat left top/20px 20px; width:20px !important; height:20px; border:none; cursor:pointer;" title="Dekretacja" onclick="selectAll({checked:false}); jQuery(jQuery(this).closest('tr')).find(&quot;input[name='activityIds']&quot;)[0].checked=true; selectAction(this, 'multiAssignment');"/>
					</td>
                    <td>
                        <ds:available test="tasklist.task.count">
                            <ds:has-permission name="TASK_COUNT">
                                <ww:if test="!accepted && sameProcessSnapshotsCount > 1"> <img src="<ww:url value="'/img/division.gif'" />"/></ww:if>
                            </ds:has-permission>
                        </ds:available>
                    </td>
					<td>

						<ww:if test="#result.pastDeadline">
							<span style="color:red;font-weight:bold;">!</span>
						</ww:if>
						<span style="color:red;font-weight:bold;"><ww:property value="statusSymbols(#result)"/></span>
						<span class="<ww:property value="#result.markerClass"/>" >&nbsp;</span>
					</td>
					
					<ww:iterator value="columns" status="status">
						<ww:if test="property == 'flags' || property == 'userFlags'">
							<td>
								<ww:iterator value="#result[property]">
								
									<span style="font-weight: bold; cursor: pointer;" title="<ww:property value="description"/>"><ww:property value="c"/></span>
								</ww:iterator>
							</td>
						</ww:if>
						<ww:elseIf test="property == 'taskPosition'">
							<td><img src="<ww:url value="'/img/boxinfo.gif'" />" id="tp_<ww:property value="#result.getActivityKey()"/>" 
										onclick="showActivityKey('<ww:property value="#result.getActivityKey()"/>', '<ww:property value="title"/>')"
										style="cursor: pointer;" />
							</td>
							
						</ww:elseIf>
						<ww:elseIf test="property == 'deadlineTime'">
							<td>
								<ds:format-date value="#result['deadlineTime']" pattern="dd-MM-yyyy"/>
							</td>
						</ww:elseIf>
						<ww:elseIf test="property == 'officeCase'">
							<td>
    							<a <ww:if test="needsReminder(#result)">style="color: red"</ww:if> class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getCaseLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a>
							</td>
						</ww:elseIf>
						<ww:elseIf test="property == 'documentReferenceId'">
							<td>
							    <a title="<ww:property value="title"/>" class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="	"/>"><ww:property value="prettyPrint(#result[property], property)"/></a>
							</td>
						</ww:elseIf>
						<ww:elseIf test = "property =='answerCounter'">
							<td>
								<a title="<ww:property value="title"/>" 
								   class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" 
								   href="<ww:url value="getAnswerLink(#result)"/>">
							    <ww:set name="answerCounterVar" value="getAnswerCounter(#result)"/>
							    
							    <ww:if test="#answerCounterVar == -1">
								   	<ds:lang text="NieWymagaOdp"/>&nbsp;
							   	</ww:if>
							   	<ww:elseif test="#answerCounterVar == 0">
								   	<ds:lang text="BrakOdp"/>&nbsp;
							   	</ww:elseif>
							   	<ww:else>
							   		<ww:property value="prettyPrint(#answerCounterVar, property)"/>
						   		</ww:else>
								</a>
							</td>
						</ww:elseIf>
						<ww:elseIf test = "property =='numberRows'">
							<td>
								<ww:property value="#numberRows"/>
								<ww:set name="numberRows" value="#numberRows + 1"/>
							</td>
						</ww:elseIf>
						<ww:elseIf test = "property =='viewerLink'">
							<td>
								<ww:if test="#result.isAnyImage()">
									<a class="zoomImg nonDroppable" title="<ww:property value="title"/>" href="<ww:url value="getLink(#result)"/>&openViewer=true">
										<img class="tasklistIoc png" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" title="<ds:lang text="WyswietlZalacznik"/>"/>
									</a>
									<%--<img class="attPreview" src="<ww:url value="attsSrc.get(#taskStat.index)" />" >--%>
								</ww:if><ww:elseif test="#result.isAnyPdf()">
									<a class="zoomImg nonDroppable" title="<ww:property value="title"/>" href="<ww:url value="getLink(#result)"/>&openViewer=true">
										<img class="tasklistIoc png" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" title="<ds:lang text="WyswietlZalacznik"/>"/>
									</a>
								</ww:elseif>
							</td>
						</ww:elseIf>
						<ww:elseIf test = "property == 'docPreview'">
							<td>
								<a class="zoomImg nonDroppable" href="<ww:url value="getLink(#result)"/>">
									<img class="png" src="<ww:url value="'/img/documentInfo.gif'"/>" width="18" height="18"/>
								</a>
								<div class="docPreview">
										<table>
											<caption>Metryka</caption>
											<tr>
												<td>Rodzaj dokumentu:</td>
												<td><ww:property value="docs.get(#taskStat.index).documentKind.name" /></td>
											</tr>
											<tr>
												<td>Numer KO:</td>
												<td><ww:property value="#result.documentOfficeNumber" /></td>
											</tr>
											<tr>
												<td>Opis:</td>
												<td class="desc"><ww:property value="docs.get(#taskStat.index).description" /></td>
											</tr>
											<tr>
												<td>Barkod:</td>
												<td><ww:property value="fms.get(#taskStat.index).getKey(\"BARCODE\")" /></td>
											</tr>
											<tr>
												<td>Data przyj�cia:</td>
												<td><ds:format-date value="fms.get(#taskStat.index).getKey(\"DATA_PRZYJECIA\")" pattern="dd.MM.yyyy"/></td>
											</tr>
											<tr>
												<td>Status:</td>
												<td><ww:property value="fms.get(#taskStat.index).getValue(\"STATUS\")" /></td>
											</tr>
										</table>
									</div>
							</td>
						</ww:elseIf>
						<ww:elseIf test="property=='lastRemark'">
							<ww:if test="#result[property]==null">
								<td>
									<a title="<ww:property value="title"/>" class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint('-', '-')"/></a>
								</td> 
							</ww:if>
							<ww:else> 
								<td>
									<ds:available test="tasklist.cutLastRemark">
										<a title="<ww:property value="prettyPrint(#result[property], property)"/>" class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], 'cutLastRemark')"/></a>
									</ds:available>
									<ds:available test="!tasklist.cutLastRemark">
										<a title="<ww:property value="title"/>" class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a>
									</ds:available>
								</td>
							</ww:else>
						</ww:elseIf>
						<ww:elseIf test="#result[property].time % 60000 == 0">
							<td>
								<a title="<ww:property value="title"/>" class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getLink(#result)"/>"><ds:format-date value="#result[property]" pattern="dd-MM-yyyy HH:mm"/></a>
							</td>
						</ww:elseIf>
						<ww:else>
							<td>
								<a title="<ww:property value="title"/>"
								   name="<ww:property value="property"/>"
								<ww:if test="needsReminder(#result)">style="color: red"</ww:if>
									class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>"
									href="<ww:url value="getLink(#result)"/>">
									<ww:property value="prettyPrint(#result[property], property)"/>
									<ww:if test="property == 'documentOfficeNumber'">
										<ww:if test="#result.documentBok">
											<%--	<ds:lang text="BOK"/>	--%>
											<img src="<ww:url value="'/img/bok.gif'"/>" width="11" height="11" border="0" alt="BOK" title="BOK"/>
										</ww:if>
										<ww:if test="#result.documentFax">
											<%--	<ds:lang text="FAX"/>	--%>
											<img src="<ww:url value="'/img/fax.gif'"/>" width="11" height="11" border="0" alt="FAX" title="FAX"/>
										</ww:if>
										<ww:if test="#result.documentCrm">
											<%--	<ds:lang text="CRM"/>	--%>
											<img src="<ww:url value="'/img/crm.gif'"/>" width="11" height="11" border="0" alt="CRM" title="CRM"/>
										</ww:if>
									</ww:if>
								</a>
							</td>
						</ww:else>
						
						
					</ww:iterator>
				</tr>
			</ww:iterator>

			<ww:if test="ownTasks || actionAssignMe">
				<tr <ds:available test="layout2">class="last" </ds:available>>
					<td colspan="<ww:property value="columns.size*2 - 1 + 2"/>">
						&nbsp;
					</td>
				</tr>
				<tr <ds:available test="layout2">class="selectBtns" </ds:available>>
					<%--	columns.size*2 + 1 ze wzgl�du na kolumny z separatorami	--%>
					<td style="text-align: left;<ds:available test="layout2"> padding-left: 7px; padding-bottom: 10px;</ds:available>" colspan="<ww:property value="columns.size*2  + 1"/>">
						<input type="checkbox" onclick="selectAll(this)"/>
						<ds:lang text="ZaznaczWszystkie/cofnijZaznaczenie"/>

						<ds:available test="!hide.select10.tasklist">
						&nbsp&nbsp&nbsp&nbsp
						<input type="checkbox" onclick="selectFew(this, 10)"/>
						<ds:lang text="Zaznacz10/cofnijZaznaczenie"/>
						&nbsp&nbsp&nbsp&nbsp
						<input type="checkbox" onclick="selectFew(this, 20)"/>
						<ds:lang text="Zaznacz20/cofnijZaznaczenie"/>
						</ds:available>

					</td>
				</tr>
			</ww:if>
		</ww:if>
		
		<%--	sprawy	--%>
		<ww:elseif test="cases != null">
			<ww:iterator value="cases" status="taskStat">
				<ww:set name="result"/>
				<tr <ww:if test="#taskStat.odd != true">class="oddRows"</ww:if>  <ds:available test="tasklist.highlightedRows"> onmouseover="$j(this).addClass('highlightedRows')" onmouseout="$j(this).removeClass('highlightedRows')" </ds:available> >

					<ww:if test="ownTasks">
						<td>
							<ww:checkbox name="'actionCaseIds'" fieldValue="#result.caseId"/>
						</td>
					</ww:if>
					<ww:iterator value="columns" status="status">
						<td>
							<a <ww:if test="#result.caseDaysToFinish < 0">style="color: red"</ww:if> class="<ww:if test="!#result.acceptedIlpolHack">bigger</ww:if>" href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a>
						</td>
						
						
						
					
						
					</ww:iterator>
				</tr>
			</ww:iterator>
						
			<ww:if test="ownTasks">
				<tr>
					<td colspan="<ww:property value="columns.size*2 - 1 + 2"/>">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<input type="checkbox" onclick="selectAll(this)"/>
					</td>

					<%--	columns.size*2 - 1 ze wzgl�du na kolumny z separatorami	--%>
					<td id="selectAll" style="text-align: left" colspan="<ww:property value="columns.size*2 + 1"/>">
						<ds:lang text="ZaznaczWszystkie/CofnijZaznaczenie"/>
					</td>
				</tr>
			</ww:if>
		</ww:elseif>
		
		<%--	obserwowane	--%>
		<ww:if test="watches != null">
			<ww:iterator value="watches" status="taskStat">
				<ww:set name="result"/>
				<tr <ww:if test="#taskStat.odd != true">class="oddRows"</ww:if>  <ds:available test="tasklist.highlightedRows"> onmouseover="$j(this).addClass('highlightedRows')" onmouseout="$j(this).removeClass('highlightedRows')" </ds:available> >
					<td>
						<ww:checkbox name="'urns'" fieldValue="#result.urn"/>
					</td>
					<td>
						<ww:if test="#result.pastDeadline">
							<span style="color:red;font-weight:bold;">!</span>
						</ww:if>
					</td>
					<ww:iterator value="columns" status="status">
						<ww:if test="property == 'flags' or property == 'userFlags'">
							<td>
								<ww:iterator value="#result[property]">
									<span style="font-weight: bold; cursor: pointer;" title="<ww:property value="description"/>"><ww:property value="c"/></span>
								</ww:iterator>
							</td>
						</ww:if>						
						<ww:elseIf test="property == 'deadlineTime'">
							<td>
								<ds:format-date value="#result['deadlineTime']" pattern="dd-MM-yy"/>
							</td>
						</ww:elseIf>
						<ww:else>
							<td>
								<a href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a>
							</td>
						</ww:else>
						
					</ww:iterator>
				</tr>
			</ww:iterator>
		</ww:if>
	</table>
	
	<div> <!-- G start -->
		
		
		<div>
			<ww:if test="tasks != null && ('order'.equals(tab)) && ('watch_order'.equals(orderTab))">
				<!-- test="tasks != null && ('order'.equals(tab)) && ('watch_order'.equals(orderTab))" -->
				<select id="action" name="action" class="sel">
					<option value="">
						<ds:lang text="wybierzAkcje"/>
					</option>
					<option value="removeOrders">
						<ds:lang text="Usun"/>
					</option>
				</select>
				<input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectAction(this, document.getElementById('action').value);"/>
			</ww:if>
			
			<ww:if test="('watches'.equals(tab)) && ownWatches">
				<!-- test="('watches'.equals(tab))" -->
				<select id="action" name="action" class="sel">
					<option value="">
						<ds:lang text="wybierzAkcje"/>
					</option>
					
					<ds:available test="tasklist.watches.generateXls">
							<option value="generateXls">
							<ds:lang text="GenerujXLS"/>
							</option>
                    </ds:available>	<ds:available test="tasklist.watches.generateXlsAll">
								<option value="generateXlsResults">
                                <ds:lang text="GenerujZbiorczyRaportXLS"/>
                                </option>

					</ds:available>	
					<ds:available test="tasklist.watches.generateCsv">
							<option value="generateCsv">
							<ds:lang text="GenerujCSV"/>
							</option>
             </ds:available>	<ds:available test="tasklist.watches.generateCsvAll">
							<option value="generateCsvResults">
                            <ds:lang text="GenerujZbiorczyRaportCSV"/>
                            </option>

					</ds:available>	
					<ds:available test="tasklist.watches.generateXml">
							<option value="generateXml">
							<ds:lang text="GenerujXML"/>
							</option>
                 </ds:available>	<ds:available test="tasklist.watches.generateXmlAll">
							<option value="generateXmlResults">
                            <ds:lang text="GenerujZbiorczyRaportXML"/>
                            </option>
					</ds:available>	
					
					<option value="removeWatches">
						<ds:lang text="Usun"/>
					</option>
					
				</select>
				<input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectWatchesAction(this, document.getElementById('action').value);"/>
			</ww:if>
		<ds:available test="!tasklist.show_tasks_operations">
			<ww:if test="ownTasks and tasks != null && !('watch_order'.equals(orderTab)) && !('watches'.equals(tab))">
				<ds:available test="nowe_menu_filtrowania"><div style="display:none"></ds:available>
				<select id="action" name="action" class="sel">
					<option value="">
						<ds:lang text="wybierzAkcje"/>
					</option>
					<ds:available test="tasklist.ilpol.acceptances">
						<ww:iterator value="enableAcceptances">
							<option value="acceptance.<ww:property value="cn"/>">
								<ww:property value="name"/>
							</option>
						</ww:iterator>
					</ds:available>	
					<ds:available test="tasklist.generateXls">
                    							<option value="generateXls">
                    							<ds:lang text="GenerujXLS"/>
                    							</option>
                                     </ds:available>	<ds:available test="tasklist.watches.generateXlsAll">
                    							<option value="generateXlsResults">
                                                <ds:lang text="GenerujZbiorczyRaportXLS"/>
                                                </option>

                    					</ds:available>
                    					<ds:available test="tasklist.generateCsv">
                    							<option value="generateCsv">
                    							<ds:lang text="GenerujCSV"/>
                    							</option>
                                         </ds:available>	<ds:available test="tasklist.watches.generateCsvAll">
                    							<option value="generateCsvResults">
                                                <ds:lang text="GenerujZbiorczyRaportCSV"/>
                                                </option>
                    					</ds:available>
                    					<ds:available test="tasklist.generateXml">
                    							<option value="generateXml">
                    							<ds:lang text="GenerujXML"/>
                    							</option>
                                         </ds:available>	<ds:available test="tasklist.watches.generateXmlAll">
                    							<option value="generateXmlResults">
                                                <ds:lang text="GenerujZbiorczyRaportXML"/>
                                                </option>

                    					</ds:available>
					<ww:if test="actionManualFinish">
						<option value="manualFinish">
							<ds:lang text="ZakonczPrace"/>
						</option>
					</ww:if>
					<ww:if test="actionAssignOfficeNumber">
						<option value="assignOfficeNumber">
							<ds:lang text="NadajNumerKO"/>
						</option>
					</ww:if>
					<ww:if test="actionAddToCase">
						<option value="addToCase">
							<ds:lang text="DodajDoIstniejacejSprawy"/>
						</option>
					</ww:if>
					<ww:if test="actionAttachmentsAsPdf">
						<option value="attachmentsAsPdf">
							<ds:lang text="PodgladZalacznikowJakoPDF"/>
						</option>
					</ww:if>
					<ww:if test="actionMultiAssignment">
						<ds:available test="!canNotDecree">
							<option value="multiAssignment">
								<ds:lang text="Dekretacja"/>
							</option>
						</ds:available>
					</ww:if>
					<ww:if test="actionMultiBoxing">
						<option value="multiBoxing">
							<ds:lang text="DodanieDoPudlaArchiwalnego"/>
						</option>
					</ww:if>
					<ww:if test="actionMultiNote">
						<option value="multiNote">
							<ds:lang text="DodanieZbiorowejNotatki"/>
						</option>
					</ww:if>
					<ds:available test="zadanie.odpowiedzi">
					<ww:if test="actionReplyUnnecessary">
						<option value="replyUnnecessary">
							<ds:lang text="NieWymagaOdp"/>
						</option>
					</ww:if>
					</ds:available>
					<ds:available test="usun.ko">
                    <ww:if test="actionDeleteOfficeNumber">
                    	<option value="deleteOfficeNumber">
                    		<ds:lang text="UsunNrKO"/>
                    	</option>
                    </ww:if>
                    </ds:available>
					<ds:dockinds test="rockwell">
						<option value="sendToEva">
							<ds:lang text="sendToEva"/>
						</option>
					</ds:dockinds>
					<ww:iterator value="smallAssignments">
						<option value="dekr.<ww:property value="name"/>$<ww:property value="substituteUser"/>">
							<ww:property value="nameToDisplay"/>
							<ds:lang text="Dekretuj"/>
						</option>
					</ww:iterator>
					<ds:dockinds test="invoice">
						<option value="multiPay">
							<ds:lang text="multiPay"/>
						</option>
					</ds:dockinds>
					<ds:available test="tasklist.multiDepartmentEnroll">
					<ww:if test="multiDepartmentEnroll">
						<ww:iterator value="incomingDepartments">
							<option value="przyjmij.<ww:property value="name"/>">
								<ww:property value="nameToDisplay"/>
								<ds:lang text="Umiesc"/>						
							</option>
						</ww:iterator>
					</ww:if>
					</ds:available>
					<ds:available test="tasklist.acceptances">
						<ww:iterator value="enableAcceptances">
							<option value="acceptance.<ww:property value="cn"/>">
								<ds:lang text="Akceptacja"/> 
								<ww:property value="name"/>
							</option>
						</ww:iterator>
					</ds:available>
					<ds:available test="tasklist.liberty.acceptances">
						<ww:iterator value="enableAcceptances">
							<option value="acceptance.<ww:property value="cn"/>">
								<ww:property value="name"/>
							</option>
						</ww:iterator>
					</ds:available>
					<ds:additions test="uproszczonaWysylka">
						<ww:if test="'out'.equals(tab)">
							<option value="dispatch">
								<ds:lang text="PrzekazDoWysylki"/>
							</option>
						</ww:if>
					</ds:additions>
					<ds:available test="tasklist.manualToCoordinator">
					<option value="manualToCoordinator">
						<ds:lang text="PrzekazDoKoordynatora"/>
					</option>
					</ds:available>
					<ww:if test="buttons.size() > 0">
						<ww:iterator value="buttons">
							<option value="dockindButton.<ww:property value="cn"/>">
								<ww:property value="name"/>
							</option>
						</ww:iterator>			
					</ww:if>
					<ww:if test="taskListEvents.size() > 0">
						<ww:iterator value="taskListEvents">
						<option value="eventButton.<ww:property value="key"/>">
								<ww:property value="value"/>
							</option>	
							</ww:iterator>
					</ww:if>
					<ds:available test="actionPrintEnvelopes.available">
					<ww:if test="actionPrintEnvelopes">
						<ww:if test="'out'.equals(tab)">
							<option value="printEnvelopes">
								<ds:lang text="GenerujKoperty"/>
							</option>
						</ww:if>
					</ww:if>
					</ds:available>
				</select>
				
				<input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectAction(this, document.getElementById('action').value);"/>
					
				<ds:available test="nowe_menu_filtrowania"></div></ds:available>
				<ds:available test="nowe_menu_filtrowania">
				<div id="selectActionButton" class="gmenuButton" onclick="gmenuShowFilters(this, 1, true)" onmouseover="gmenuButtonOver(this,true)" onmouseout="gmenuButtonOver(this,false)">
				<img src="<ww:url value="'/img/gm_arrow_down.gif'"/>" id="selectActionButton_img" class="arrow" />
					Wybierz akcj�
				</div>	
				<input type="button"  style="min-width: 170px; width: 170px" class="gbtn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectAction(this, document.getElementById('action').value);"/>	
			</ds:available >
			</ww:if>
		</ds:available>
			<ww:if test="ownTasks and cases != null">
				<!-- test="ownTasks and cases != null" -->
				<select id="action" name="action" class="sel">
					<option value="">
						<ds:lang text="wybierzAkcje"/>
					</option>
					<option value="manualFinish">
						<ds:lang text="ZakonczSprawe"/>
					</option>
					<ds:available test="tasklist.cases.generateXlsAll">
							<option value="generateXlsResults">
							<ds:lang text="GenerujZbiorczyRaportXLS"/>
							</option>
					</ds:available>	
					<ds:available test="tasklist.cases.generateCsvAll">
							<option value="generateCsvResults">
							<ds:lang text="GenerujZbiorczyRaportCSV"/>
							</option>
					</ds:available>	
					<ds:available test="tasklist.cases.generateXmlAll">
							<option value="generateXmlResults">
							<ds:lang text="GenerujZbiorczyRaportXML"/>
							</option>
					</ds:available>	
				</select>
				<input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectAction(this, document.getElementById('action').value);"/>
			</ww:if>
			
			<ww:if test="!('watches'.equals(tab)) && !ownTasks and tasks != null and actionAssignMe">
				<!-- test="!ownTasks and tasks != null and actionAssignMe" -->
				<select id="action" name="action" class="sel">
					<option value="">
						<ds:lang text="wybierzAkcje"/>
					</option>
					<option value="assignMe">
						<ds:lang text="PrzekazZadaniaSobie"/>
						<ww:iterator value="smallAssignments">
						<option value="dekrTo.<ww:property value="name"/>">
							<ww:property value="nameToDisplay"/>
							<ds:lang text="Dekretuj"/>
						</option>
						</ww:iterator>						
					</option>
					<ds:additions test="uproszczonaWysylka">
						<ww:if test="'out'.equals(tab)">
							<option value="dispatch">
								<ds:lang text="PrzekazDoWysylki"/>
							</option>
						</ww:if>
					</ds:additions>
					<ww:if test="actionMultiAssignment">
						<ds:available test="!canNotDecree">
							<option value="multiAssignment">
								<ds:lang text="Dekretacja"/>
							</option>
						</ds:available>
					</ww:if>
					<ds:available test="tasklist.manualToCoordinator">
					<option value="manualToCoordinator">
						<ds:lang text="PrzekazDoKoordynatora"/>
					</option>
					</ds:available>
				</select>
				<input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
					onclick="selectAction(this, document.getElementById('action').value);"/>
			</ww:if>
			<ds:available test="!tasklist.hideOpenTask">
				<input type="button" class="btn" value="<ds:lang text="Otw�rz zadanie"/>"
					onclick="window.location.href='<ww:url value="getLink(#result)"/>'">
				</ds:available>	
		</div>
		<div>
			<ww:if test="users == null">
				<ds:available test="labels">
					<ww:if test="availableForAddLabels.size > 0">
						<ww:select name="'labelId'" list="availableForAddLabels" cssClass="'sel'" listKey="id" listValue="name"/>
						<input type="button" class="btn" value="<ds:lang text="DodajEtykiete"/>" onclick="addLabelSubmit()"/>
						<input type="hidden" name="doAddLabel" id="doAddLabel"/>
					</ww:if>
				</ds:available>
			</ww:if>
		</div>
	</div>
	</div>

<ds:available test="pager.tasklist.bottom">
	<ww:if test="tasks != null"> <%--	&& 'in'.equals(tab)">	--%>
		<ww:set name="pager" scope="request" value="pager"/>
		<table width="100%">
			<tr>
				<td align="center">
					<jsp:include page="/pager-links-include.jsp"/>
				</td>
			</tr>
		</table>
	</ww:if>
</ds:available>
	<ww:if test="taskas != null">
		<table width="100%">
			<tr>
				<td colspan="3" align="center">
				</td>
			</tr>
			<tr>
				<td align="right">
					<a href="<ww:url value="getBaseLink()"><ww:param name="'tab'" value="tab"/><ww:param name="'username'" value="username"/><ww:param name="'simpleTaskList'" value="true"/></ww:url>"><ds:lang text="UproszczonaLista"/></a>
				</td>
				<td>
					&nbsp;
				</td>
				<td align="left">
					<a href="<ww:url value="getBaseLink()"><ww:param name="'tab'" value="tab"/><ww:param name="'username'" value="username"/><ww:param name="'fullTaskList'" value="true"/></ww:url>"><ds:lang text="PelnaLista"/></a>
				</td>
			</tr>
		</table>
	</ww:if>
</form>
<script>

	// tomek
	var text_for_input = new String();
	
	function rewrite(element)
	{
		//text_for_input = element.firstChild.nodeValue;
		
		for(i=0; i<document.getElementById('filterBy').options.length; i++)		
		{
			if(document.getElementById('filterBy').options[i].value == element.name)
			{
				document.getElementById('filterBy').options[i].selected = true;
			}			
		}
		document.body.style.cursor = 'wait';	
	}

	function insert(element)
	{
		if(text_for_input != null)
			element.value = text_for_input;									
	}
	
	// end:tomek
	
         
        var prefix = "check";
        tableId="mainTable";

        prepareCheckboxes();
	
	
    var activityId_to_documentId = new Array();
    <ww:iterator value="tasks">
        activityId_to_documentId['<ww:property value="activityId"/>'] = '<ww:property value="documentId"/>';
    </ww:iterator>

    var activityId_is_manual = new Array();
    <ww:iterator value="tasks">
        activityId_is_manual['<ww:property value="activityId"/>'] = <ww:property value="manual"/>;
    </ww:iterator>

    function selectAll(checkbox)
    {
    	<ww:if test="tasks != null">
        var boxes = document.getElementsByName('activityIds');
        </ww:if>
    	<ww:if test="cases != null">
        var boxes = document.getElementsByName('actionCaseIds');
        </ww:if>
		<ww:if test="('watches'.equals(tab))">
        var boxes = document.getElementsByName('urns');
        </ww:if>
        if (boxes.length > 0)
        {
            for (var i=0; i < boxes.length; i++)
            {
                boxes.item(i).checked = checkbox.checked;
            }
        }
        <ds:available test="layout2">
        Custom.clear();
        </ds:available>
    }
    
    function selectFew(checkbox, num)
    {
    	<ww:if test="tasks != null">
        var boxes = document.getElementsByName('activityIds');
        </ww:if>
    	<ww:if test="cases != null">
        var boxes = document.getElementsByName('actionCaseIds');
        </ww:if>
        <ww:if test="('watches'.equals(tab))">
        var boxes = document.getElementsByName('urns');
        </ww:if>
        if (boxes.length > 0)
        {
            var count = boxes.length;
            if(count>num)
                count=num;
            for (var i=0; i < count; i++)
            {
                boxes.item(i).checked = checkbox.checked;
            }
        }
        <ds:available test="layout2">
        Custom.clear();
        </ds:available>
    }

    function getDocumentIdsQuery()
    {
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            var search = '';
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                var documentId = activityId_to_documentId[box.value];
                if (documentId != null && box.checked)
                {
                    if (search.length > 0)
                        search += '&';
                    search += 'documentIds='+documentId;
                    ++count;
                }
            }

            return search;
        }

        return null;
    }

    function getActivityIdsQuery()
    {
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            var search = '';
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }

            return search;
        }

        return null;
    }

    function nwViewPdf()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }

        openToolWindow('<ww:url value="'/office/'+urlTabString+'/view-attachments-as-pdf.action'"/>?'+search, 'nwPdf');
    }

    

    function multiAssignment()
    {
        var search = 'tab=<ww:property value="tab"/>';
        var boxes = document.getElementsByName('activityIds');
        var nonManualWarning = false;
        if (boxes.length > 0)
        {
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    /*if (!activityId_is_manual[box.value])
                    {
                        box.checked = false;
                        nonManualWarning = true;
                        continue;
                    }*/

                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }
        }

        if (search.length == 0)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }

        if (nonManualWarning)
        {
            alert('<ds:lang text="PismaPrzyjeteDoWiadomosciNieBedaDalejDekretowane.ZaznaczenieTychPismZostaloCofniete"/>.');
        }

        <ds:available test="tasklist.jbpm4">
            <ds:available test="manual-multi-assignment">
                document.location.href = '<ww:url value="'/office/'+urlTabString+'/manual-multi-assignment.action'"/>?'+search;
            </ds:available>
            <ds:available test="!manual-multi-assignment">
                document.location.href = '<ww:url value="'/office/'+urlTabString+'/manual-assignment.action'"/>?'+search;
            </ds:available>
        </ds:available>
        <ds:available test="!tasklist.jbpm4">
            document.location.href = '<ww:url value="'/office/tasklist/multi-assignment.action'"/>?'+search;
        </ds:available>
    }

    function smallAssignment()
    {
        var search = 'tab=<ww:property value="tab"/>';
        var boxes = document.getElementsByName('activityIds');
        var nonManualWarning = false;
        if (boxes.length > 0)
        {
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    /*if (!activityId_is_manual[box.value])
                    {
                        box.checked = false;
                        nonManualWarning = true;
                        continue;
                    }*/

                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }
        }

        if (search.length == 0)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }

        if (nonManualWarning)
        {
            alert('<ds:lang text="PismaPrzyjeteDoWiadomosciNieBedaDalejDekretowane.ZaznaczenieTychPismZostaloCofniete"/>.');
        }

        
         document.getElementById('doSmallAssignment').value = 'true';
         document.getElementById('form').submit();
    }

    
    function multiBoxing()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }
        search += '&tab=<ww:property value="tab"/>';

        document.location.href = '<ww:url value="'/office/tasklist/multi-boxing.action'"/>?'+search;
    }
    
    function multiNote()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }
        
        window.open('<ww:url value="'/office/tasklist/multi-note.action'"/>?' + search, 'pickportfolio', 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes');
     
    } 
    
    function departmentEnroll()
    {
        var search = 'tab=<ww:property value="tab"/>';
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    /*if (!activityId_is_manual[box.value])
                    {
                        box.checked = false;
                        continue;
                    }*/

                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }
        }

        if (search.length == 0)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }

         document.getElementById('doDepartmentEnroll').value = 'true';
         document.getElementById('form').submit();    
    }

    function assignMe()
    {
        var search = 'tab=<ww:property value="tab"/>';
        var boxes = document.getElementsByName('activityIds');
        var nonManualWarning = false;
        if (boxes.length > 0)
        {
            var count = 0; 

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                   /* if (!activityId_is_manual[box.value])
                    {
                        box.checked = false;
                        nonManualWarning = true;
                        continue;
                    }*/

                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }
        }

        if (search.length == 0)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }

        if (nonManualWarning)
        {
            <ds:modules test="workflow">
            alert('<ds:lang text="TylkoPismaPrzyjeteDoRealizacjiWobieguRecznymBedaPrzekazane.ZaznaczeniePozostalychPismZostaloCofniete"/>.');
            </ds:modules>
            <ds:modules test="!workflow">
            alert('<ds:lang text="PismaPrzyjeteDoWiadomosciNieBedaDalejDekretowane.ZaznaczenieTychPismZostaloCofniete"/>.');
            </ds:modules>
        }

        document.getElementById('doAssignMe').value = 'true';
        document.getElementById('form').submit();
    }

    function selectWatchesAction(btn, act)
    {
        var urns = document.getElementsByName('urns');
        var anySelected = false;
        for (var i=0; i < urns.length; i++)
         {
            if (urns.item(i).checked == true)
            {
                anySelected = true;
                break;
            }
        }

        if (!anySelected)
        {
            alert('<ds:lang text="NieZaznaczonoElementow"/>');
            return false;
        }

        if (act == '')
        {
            alert('<ds:lang text="NieWybranoAkcji"/>');
            return false;
        }
        
        if (act == 'removeWatches') 
        {
      		document.getElementById('doRemoveWatches').value = 'true';
     	   	document.getElementById('form').submit();
		}
        
        else if (act == 'generateXls')
        {
      	    document.getElementById('doGenerateXls').value = 'true';
       	    document.getElementById('form').submit();
        }
        
        else if (act == 'generateCsv')
        {
      	    document.getElementById('doGenerateCsv').value = 'true';
       	    document.getElementById('form').submit();
        }
        
        else if (act == 'generateXml')
        {
      	    document.getElementById('doGenerateXml').value = 'true';
       	    document.getElementById('form').submit();
        }
    }

    function selectAction(btn, act)
    {
    	<ww:if test="tasks != null">
        var boxes = document.getElementsByName('activityIds');
        </ww:if>
    	<ww:if test="cases != null">
        var boxes = document.getElementsByName('actionCaseIds');
        </ww:if>
        var anySelected = false;
        for (var i=0; i < boxes.length; i++)
         {
            if (boxes.item(i).checked == true)
            {
                anySelected = true;
                break;
            }
        }

       	  	 if (act == 'generateXlsResults') {
	       	  		document.getElementById('doGenerateCsv').value = '';
			  		document.getElementById('doGenerateXml').value = '';
	            	document.getElementById('doGenerateXls').value = '';
        		  	document.getElementById('doGenerateCsvAll').value = '';
        		  	document.getElementById('doGenerateXmlAll').value = '';
       	            document.getElementById('doGenerateXlsAll').value = 'true';
       	            document.getElementById('form').submit();

       	     }

       		 else if (act == 'generateCsvResults') {
       			document.getElementById('doGenerateCsv').value = '';
		  		document.getElementById('doGenerateXml').value = '';
            	document.getElementById('doGenerateXls').value = '';
       			document.getElementById('doGenerateXmlAll').value = '';
    		  	document.getElementById('doGenerateXlsAll').value = '';
       	       	document.getElementById('doGenerateCsvAll').value = 'true';
       	       	document.getElementById('form').submit();

       	     }

       		 else if (act == 'generateXmlResults') {
       			document.getElementById('doGenerateCsv').value = '';
		  		document.getElementById('doGenerateXml').value = '';
            	document.getElementById('doGenerateXls').value = '';
       			document.getElementById('doGenerateCsvAll').value = '';
    		  	document.getElementById('doGenerateXlsAll').value = '';
       	        document.getElementById('doGenerateXmlAll').value = 'true';
     	        document.getElementById('form').submit();
       	     }


        else if (!anySelected)
        {
        	<ww:if test="tasks != null">
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            </ww:if>
        	<ww:if test="cases != null">
            alert('<ds:lang text="NieZaznaczonoSpraw"/>');
            </ww:if>
            return false;
        }

        if (act == '')
        {
            alert('<ds:lang text="NieWybranoAkcji"/>');
            return false;
        }

		<ww:if test="tasks != null">
        createDocumentIds();
        </ww:if>

        if (act == 'manualFinish')
        {
            document.getElementById('doManualFinish').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'assignOfficeNumber')
        {
            document.getElementById('doAssignOfficeNumber').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'assignOfficeNumber')
        {
            document.getElementById('doAssignOfficeNumber').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'replyUnnecessary')
        {
            document.getElementById('doReplyUnnecessary').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'deleteOfficeNumber')
        {
        	document.getElementById('doDeleteOfficeNumber').value = 'true';
        	document.getElementById('form').submit();
        }
        else if (act == 'sendToEva')
        {
            document.getElementById('doSendToEva').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act.startsWith("acceptance."))
        {
            document.getElementById('doAcceptance').value = 'true';
            document.getElementById('acceptanceCn').value = act.substring(11,act.length);
            document.getElementById('form').submit();
        }
        else if (act == 'addToCase')
        {
            window.open('<ww:url value="'/office/find-cases.action?popup=true'"/>', 'Rwa', 'width=750,height=600,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes, resizable=yes');
        }
        else if (act == 'attachmentsAsPdf')
        {
            nwViewPdf();
        }
        else if (act == 'manualToCoordinator')
        {
        	document.getElementById('doManualPushToCoor').value = 'true';
            document.getElementById('form').submit();
        }    
        else if (act == 'multiAssignment')
        {
            multiAssignment();
        }
        else if (act == 'multiBoxing')
        {
            multiBoxing();
        }
        else if (act == 'multiNote')
        {
        	multiNote();
        }
        else if(act.startsWith("dockindButton."))
        {
        	var dockindE = act.substring(14,act.length); 
			document.getElementById('doDockindEvent').value = true;
			document.getElementById('dockindEventValue').value = dockindE;
			document.getElementById('form').submit();
        }
        else if(act.startsWith("eventButton."))
        {
        	var dockindE = act.substring(12,act.length); 
			document.getElementById('doTaskListEvent').value = true;
			document.getElementById('dockindEventValue').value = dockindE;
			document.getElementById('form').submit();
        }
		else if (act == 'multiPay')
		{
			var search = getDocumentIdsQuery();
			if (search == null)
        	{
            	alert('<ds:lang text="NieZaznaczonoPism"/>');
            	return false;
        	}        	
        	search += '&tab=<ww:property value="tab"/>';
        	document.location.href = '<ww:url value="'/office/tasklist/multi-pay.action'"/>?'+search;
		}


		else if(act == 'withdraw') {
			$j("#form").append("<input type='hidden' name='doWithdraw' value='true'/>");
			document.getElementById('form').submit();
		}
        
		 else if (act == 'generateXls') {
			 	document.getElementById('doGenerateXmlAll').value = '';
 		  		document.getElementById('doGenerateXlsAll').value = '';
    	       	document.getElementById('doGenerateCsvAll').value = '';
			 	document.getElementById('doGenerateCsv').value = '';
 		  		document.getElementById('doGenerateXml').value = '';
	            document.getElementById('doGenerateXls').value = 'true';
	            document.getElementById('form').submit();
	     }
        
		 else if (act == 'generateCsv') {
			 	document.getElementById('doGenerateXmlAll').value = '';
		  		document.getElementById('doGenerateXlsAll').value = '';
 	       		document.getElementById('doGenerateCsvAll').value = '';
			 	document.getElementById('doGenerateXls').value = '';
		  		document.getElementById('doGenerateXml').value = '';
	            document.getElementById('doGenerateCsv').value = 'true';  
	            document.getElementById('form').submit();        
	     }
        
		 else if (act == 'generateXml') {
			 	document.getElementById('doGenerateXmlAll').value = '';
		  		document.getElementById('doGenerateXlsAll').value = '';
 	       		document.getElementById('doGenerateCsvAll').value = '';
			 	document.getElementById('doGenerateXls').value = '';
		  		document.getElementById('doGenerateCsv').value = '';
	            document.getElementById('doGenerateXml').value = 'true';   
	            document.getElementById('form').submit();
	     }
		
		
		
		else if(act == 'dispatch'){
		 	var search = getDocumentIdsQuery();
		 	search += "&";
		 	search += getActivityIdsQuery();
			if (search == null)
        	{
            	alert('<ds:lang text="NieZaznaczonoPism"/>');
            	return false;
        	} 
		 	document.location.href = '<ww:url value="'/office/tasklist/multi-dispatch.action'"/>?'+search+'&username=<ww:property value="username"/>';
		}
        else if(act.startsWith("dekr."))
        {
            dollar_char = act.indexOf('$');
        	substituteUser = act.substring(dollar_char + 1, act.length);
        	if (substituteUser.length > 2) {
            	result = confirm("Uzytkownik jest zastepowany przez " + substituteUser + ". Czy dekretowac?");
        		if (result == false)
            		return false;
        	}
            document.getElementById('nameOfSmallAssignment').value = act.substring(5, dollar_char);  
            smallAssignment();
        }
        
         else if(act.startsWith("dekrTo."))
        {
            document.getElementById('nameOfSmallAssignment').value = act.substring(7,act.length);  
			smallAssignment();
        }

        else if (act == 'assignMe')
        {
            assignMe();
        }
        
        else if (act.startsWith("przyjmij."))
        {
            document.getElementById('nameOfJournal').value = act.substring(9,act.length);
            departmentEnroll();
        }
        else if (act == 'removeOrders')
        {            
            document.getElementById('doRemoveOrders').value = 'true';
            document.getElementById('form').submit();
        }
        else if(act == 'printEnvelopes'){
        	openWith();
        }

        return true;
    }

	function openWith()
	{
		var search = getDocumentIdsQuery();
		if (search == null)
    	{
        	alert('<ds:lang text="NieZaznaczonoPism"/>');
        	return false;
    	} 
		openToolWindow('<ww:url value="'/office/tasklist/print-envelopes-pdf.action'"/>?'+search);
	}
    
    // funkcja wywolywana przez okienko wyboru sprawy
    function pickCase(id, caseId)
    {
        if (!confirm('<ds:lang text="NaPewnoChceszDodacWybranePismaDoSprawy"/> '+caseId+'?'))
            return false;
        if (confirm('Wznowi� spraw� '+caseId+ ' je�li jest ju� zamkni�ta?'))
        {
        	//alert('caseReopen -> true');
         	document.getElementById('caseReopen').value = 'true';
        }
        else 
        {	
        	//alert('caseReopen -> false');
        	document.getElementById('caseReopen').value = 'false';
        } 
        
        document.getElementById('doAddToCase').value = 'true';
        document.getElementById('caseId').value = id;
        document.getElementById('addToTask').value = 'true';
        document.getElementById('caseName').value = caseId;
        document.getElementById('form').submit();
        
        
    }

    function createDocumentIds()
    {
        var form = document.getElementById('form');

        var boxes = document.getElementsByName('activityIds');
        for (var i=0; i < boxes.length; i++)
        {
            if (boxes.item(i).checked == true)
            {
                var hid = document.createElement("input");
                hid.setAttribute("type", "hidden");
                
                hid.setAttribute("name", "documentIds");
                hid.setAttribute("value", activityId_to_documentId[boxes.item(i).value]);
                form.appendChild(hid);
            }
        }
    }

    function addLabelSubmit()
    {
    	createDocumentIds();
    	document.getElementById('doAddLabel').value = true;
    	document.getElementById('form').submit();
    }

    function submitLabel()
    {
    	document.getElementById('form').submit();
    }

    function removeLabel(elem)
    {
        document.getElementById('removeLabelId').value = elem.id;
    	document.getElementById('doRemoveLabelFromView').value = true;
    	document.getElementById('form').submit();
    }
    <ds:available test="layout2">

    /* magiczne checkboxy */
	/*$j("#mainTable :checkbox").addClass('styled');
	Custom.init();*/
	
	/*$j('#mainTable tr:not(.tableheder, .selectBtns, .last)').hover(function() {
		$j(this).addClass('hover');
	}, function() {
		$j(this).removeClass('hover');
	});*/


	/* liczba zada� niezaakceptowanych na zak�adkach */
	$j(document).ready(function() {
		var user = '';

		if($j('#showNumberNewTask').val() == true || $j('#showNumberNewTask').val() == 'true') {
			<ww:if test="users != null">
				user = $j('select[name=username] option:selected').val();
			</ww:if>
	
			$j('#middleMenuContainer a').each(function() {
				var item = this;
				$j.post("task-list-count.action", {	username: user, documentType: item.name, bookmarkId : $j(item).attr('bookmarkId')},
						function(data){
							if(data > 0)
								$j(item).append('&nbsp;(' + data + ')');
						});
			});
		}
         
	});	//	end of $j(document).ready(function()
	</ds:available>
        
--></script>

     <ds:available test="project.parametrization.ima">
         <ww:set name="typeIma" value="'tasklist'"/>
        <jsp:include page="/parametrization/ima/left-menu.jsp"/>
     </ds:available>


