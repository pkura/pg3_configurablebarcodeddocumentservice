<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N watchlist.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Obserwowane</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<a href="<ww:url value="'/office/tasklist/current-user-task-list.action?a=a&tab=in'"/>" title="Pisma przychodz�ce">Pisma przychodz�ce</a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/office/tasklist/current-user-task-list.action?a=a&tab=internal'"/>" title="Pisma przychodz�ce">Pisma przychodz�ce</a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/office/tasklist/current-user-task-list.action?a=a&tab=out'"/>" title="Pisma przychodz�ce">Pisma przychodz�ce</a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/office/tasklist/watchlist.action'"/>" title="Obserwowane" class="highlightedText">Obserwowane</a>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/watchlist.action"/>" method="post" id="form" onsubmit="disableFormSubmits(this);">

    <ww:if test="tasks != null">
        <p>Znajd� na li�cie pismo o numerze KO: <ww:textfield name="'searchOfficeNumber'" size="5" maxlength="7" cssClass="'txt'" />
        <ww:submit value="'Znajd�'" cssClass="'btn'" /></p>
    </ww:if>

    <table class="search" width="100%" cellspacing="0">
    <tr>
        <ww:if test="ownTasks"><th></th></ww:if>
        <th></th>
        <ww:iterator value="columns" status="status" >
            <th><nobr><a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ww:property value="title"/> <a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr></th>
            <ww:if test="!#status.last">
                <th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
            </ww:if>
        </ww:iterator>
    </tr>
        <!-- pisma -->
        <ww:if test="tasks != null">
            <ww:iterator value="tasks">
                <ww:set name="result"/>
                <tr>
                    <ww:if test="ownTasks"><td><ww:checkbox name="'activityIds'" fieldValue="#result.activityId"/></td></ww:if>
                    <td><ww:if test="#result.documentBok">BOK</ww:if><ww:if test="#result.documentFax">FAX</ww:if></td>
                    <ww:iterator value="columns" status="status">
                        <ww:if test="property == 'flags' or property == 'userFlags'">
                            <td>
                                <ww:iterator value="#result[property]">
                                    <span style="font-weight: bold; cursor: pointer;" title="<ww:property value="description"/>"><ww:property value="c"/></span>
                                </ww:iterator>
                            </td>
                        </ww:if>
                        <ww:else>
                            <td><a <ww:if test="accepted">class="thin"</ww:if> href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a></td>
                        </ww:else>
                        <ww:if test="!#status.last">
                            <td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
                        </ww:if>
                    </ww:iterator>
                </tr>
            </ww:iterator>

            <ww:if test="ownTasks">
                <tr>
                    <td colspan="<ww:property value="columns.size*2 - 1 + 2"/>">&nbsp;</td>
                </tr>
                <tr>
                    <td><input type="checkbox" onclick="selectAll(this)"/></td>
                    <td></td>
                    <!-- columns.size*2 - 1 ze wzgl�du na kolumny z separatorami -->
                    <td id="selectAll" style="text-align: left" colspan="<ww:property value="columns.size*2 - 1"/>">Zaznacz wszystkie / cofnij zaznaczenie</td>
                </tr>
            </ww:if>
        </ww:if>
        <!-- sprawy -->
        <ww:elseif test="cases != null">
            <ww:iterator value="cases">
                <ww:set name="result"/>
                <tr>
                    <ww:if test="ownTasks"><td></td></ww:if>
                    <td></td>
                    <ww:iterator value="columns" status="status">
                        <td><a <ww:if test="#result.caseDaysToFinish < 0">style="color: red"</ww:if> <ww:if test="accepted">class="thin"</ww:if> href="<ww:url value="getLink(#result)"/>"><ww:property value="prettyPrint(#result[property], property)"/></a></td>
                        <ww:if test="!#status.last">
                            <td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
                        </ww:if>
                    </ww:iterator>
                </tr>
            </ww:iterator>
        </ww:elseif>
    </table>

    <ww:if test="ownTasks and tasks != null">
        <select id="action" class="sel">
            <option value="">-- wybierz akcj� --</option>
            <ww:if test="actionManualFinish">
                <option value="manualFinish">Zako�cz prac�</option>
            </ww:if>
            <ww:if test="actionAssignOfficeNumber">
                <option value="assignOfficeNumber">Nadaj numer KO</option>
            </ww:if>
            <ww:if test="actionAddToCase">
                <option value="addToCase">Dodaj do istniej�cej sprawy</option>
            </ww:if>
            <ww:if test="actionAttachmentsAsPdf">
                <option value="attachmentsAsPdf">Podgl�d za��cznik�w jako PDF</option>
            </ww:if>
            <ww:if test="actionMultiAssignment">
                <option value="multiAssignment">Dekretacja</option>
            </ww:if>
            <ww:if test="actionMultiBoxing">
                <option value="multiBoxing">Dodanie do pud�a archiwalnego</option>
            </ww:if>
            <ww:if test="actionMultiNote">
                <option value="multiNote">Dodaj zbiorow� notatk�</option>
            </ww:if>
        </select>
        <input type="button" class="btn" value="Wykonaj na zaznaczonych"
            onclick="selectAction(this, document.getElementById('action').value);"/>

<%--
        <ds:submit-event value="'Zako�cz prac� z zaznaczonymi'" name="'doManualFinish'" cssClass="'btn'"/>

--%>
    </ww:if>


</form>

<script>
    var activityId_to_documentId = new Array();
    <ww:iterator value="tasks">
        activityId_to_documentId['<ww:property value="activityId"/>'] = '<ww:property value="documentId"/>';
    </ww:iterator>

    var activityId_is_manual = new Array();
    <ww:iterator value="tasks">
        activityId_is_manual['<ww:property value="activityId"/>'] = <ww:property value="manual"/>;
    </ww:iterator>

    function selectAll(checkbox)
    {
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            for (var i=0; i < boxes.length; i++)
            {
                boxes.item(i).checked = checkbox.checked;
            }
        }
    }

    function getDocumentIdsQuery()
    {
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            var search = '';
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                var documentId = activityId_to_documentId[box.value];
                if (documentId != null && box.checked)
                {
                    if (search.length > 0)
                        search += '&';
                    search += 'documentIds='+documentId;
                    ++count;
                }
            }

            return search;
        }

        return null;
    }

    function getActivityIdsQuery()
    {
        var boxes = document.getElementsByName('activityIds');
        if (boxes.length > 0)
        {
            var search = '';
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }

            return search;
        }

        return null;
    }

    function nwViewPdf()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('Nie zaznaczono pism');
            return false;
        }

    }

    function multiAssignment()
    {
        var search = 'tab=<ww:property value="tab"/>';
        var boxes = document.getElementsByName('activityIds');
        var nonManualWarning = false;
        if (boxes.length > 0)
        {
            var count = 0;

            for (var i=0; i < boxes.length; i++)
            {
                var box = boxes.item(i);
                if (box.checked)
                {
                    if (!activityId_is_manual[box.value])
                    {
                        box.checked = false;
                        nonManualWarning = true;
                        continue;
                    }

                    if (search.length > 0)
                        search += '&';
                    search += 'activityIds='+box.value;
                    ++count;
                }
            }
        }

        if (search.length == 0)
        {
            alert('Nie zaznaczono pism');
            return false;
        }

        if (nonManualWarning)
        {
            alert('Pisma przyj�te do wiadomo�ci nie b�d� dalej dekretowane.  Zaznaczenie tych pism zosta�o cofni�te.');
        }

        document.location.href = '<ww:url value="'/office/tasklist/multi-assignment.action'"/>?'+search;
    }

    function multiBoxing()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('Nie zaznaczono pism');
            return false;
        }

        document.location.href = '<ww:url value="'/office/tasklist/multi-boxing.action'"/>?'+search;
    }
    
    function multiNote()
    {
        var search = getDocumentIdsQuery();
        if (search == null)
        {
            alert('<ds:lang text="NieZaznaczonoPism"/>');
            return false;
        }
        search += '&tab=<ww:property value="tab"/>';
        
        window.open('<ww:url value="'/office/tasklist/multi-note.action'"/>?+search', 'pickportfolio', 'width=800,height=990,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes');
     
    } 
    
    function selectAction(btn, act)
    {
        var boxes = document.getElementsByName('activityIds');
        var anySelected = false;
        for (var i=0; i < boxes.length; i++)
        {
            if (boxes.item(i).checked == true)
            {
                anySelected = true;
                break;
            }
        }

        if (!anySelected)
        {
            alert('Nie zaznaczono pism');
            return false;
        }

        if (act == '')
        {
            alert('Nie wybrano akcji');
            return false;
        }

        createDocumentIds();

        if (act == 'manualFinish')
        {
            document.getElementById('doManualFinish').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'assignOfficeNumber')
        {
            document.getElementById('doAssignOfficeNumber').value = 'true';
            document.getElementById('form').submit();
        }
        else if (act == 'addToCase')
        {
            window.open('<ww:url value="'/office/find-cases.action?popup=true'"/>', null, 'width=700,height=550,menubar=no,toolbar=no,status=no,location=no,scrollbars=auto');
        }
        else if (act == 'attachmentsAsPdf')
        {
            nwViewPdf();
        }
        else if (act == 'multiAssignment')
        {
            multiAssignment();
        }
        else if (act == 'multiBoxing')
        {
            multiBoxing();
        }
        else if (act == 'multiNote')
        {
        	multiNote();
        }
        return true;
    }

    // funkcja wywolywana przez okienko wyboru sprawy
    function pickCase(id, caseId)
    {
        if (!confirm('Na pewno chcesz doda� wybrane pisma do sprawy '+caseId+'?'))
            return false;
        document.getElementById('doAddToCase').value = 'true';
        document.getElementById('caseId').value = id;
        document.getElementById('form').submit();
    }

    function createDocumentIds()
    {
        var form = document.getElementById('form');

        var boxes = document.getElementsByName('activityIds');
        for (var i=0; i < boxes.length; i++)
        {
            if (boxes.item(i).checked == true)
            {
                var hid = document.createElement("input");
                hid.setAttribute("type", "hidden");
                hid.setAttribute("name", "documentIds");
                hid.setAttribute("value", activityId_to_documentId[boxes.item(i).value]);
                form.appendChild(hid);
            }
        }
    }
</script>
