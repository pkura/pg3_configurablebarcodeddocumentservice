<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1><ww:property value="tabs.selectedTab.title"/></h1>

<ds:available test="layout2">
    <div id="middleMenuContainer">
        <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
        <ww:iterator value="tabs" status="status" >
            <a name="<ww:property value='idName' />" bookmarkId="<ww:property value='idbookamrk'/>" href="<ww:url value='link'/>" title="<ww:property value='title'/>" class="middleMenuLink<ww:if test="selected"> middleMenuLinkHover middleMenuLinkSelected</ww:if>" <ww:if test="selected">id="tab2_selected"</ww:if>>
                <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left<ww:if test="selected">_pushed</ww:if>.gif" class="tab2_img" />
                <ww:property value="name"/>
            </a>
        </ww:iterator>
    </div>
</ds:available>

<ds:available test="!layout2">
    <hr size="1" align="left"  width="77%" class="horizontalLine" />
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
    </ww:iterator>
</ds:available>
<!-- <div> -->  <!-- A start -->


<ds:available test="layout2">
    <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/assignment-journal.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'type'"/>
    <ds:lang text="Dzien"/>:
    <ww:textfield name="'fromDate'" id="fromDate" size="10" maxlength="10" cssClass="'txt'"/>
    <img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_fromDate_trigger"
         style="cursor: pointer; border: 1px solid red;" title="Date selector"
         onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
    <ds:event value="getText('Przejdz')" id="btnPrzejdz"
          type="'submit'" name="'doChangeDate'" cssClass="'btn'"/>

    <table class="search" width="100%" cellspacing="0" id="mainTable">
        <tr class="tableheder">
            <th><ds:lang text="ID"/></th>
            <th><ds:lang text="Data"/></th>
            <th><ds:lang text="Tytul"/></th>
            <ww:if test="type == 'out'">
                <th><ds:lang text="CelUzytkownik"/></th>
                <th><ds:lang text="CelDzial"/></th>
            </ww:if>
        </tr>
        <ww:iterator value="entries" status="status" >
            <tr <ww:if test="#status.odd != true">class="oddRows"</ww:if>">
                <td>
                    <a title="<ww:property value="getDocumentId()"/>" href="<ww:url value="'/office/incoming/document-archive.action'"><ww:param name="'documentId'" value="getDocumentId()"/></ww:url>"><ww:property value="getDocumentId()"/></a>
                </td>
                <td>
                    <a title="<ww:property value="getActionDate()"/>" href="<ww:url value="'/office/incoming/document-archive.action'"><ww:param name="'documentId'" value="getDocumentId()"/></ww:url>"><ww:property value="getActionDate()"/></a>
                </td>
                <td>
                    <a title="<ww:property value="getDocumentTitle()"/>" href="<ww:url value="'/office/incoming/document-archive.action'"><ww:param name="'documentId'" value="getDocumentId()"/></ww:url>"><ww:property value="getDocumentTitle()"/></a>
                </td>
                <ww:if test="type == 'out'">
                    <td>
                        <a title="<ww:property value="getTargetUser()"/>" href="<ww:url value="'/office/incoming/document-archive.action'"><ww:param name="'documentId'" value="getDocumentId()"/></ww:url>"><ww:property value="getTargetUser()"/></a>
                    </td>
                    <td>
                        <a title="<ww:property value="getTargetDivision()"/>" href="<ww:url value="'/office/incoming/document-archive.action'"><ww:param name="'documentId'" value="getDocumentId()"/></ww:url>"><ww:property value="getTargetDivision()"/></a>
                    </td>
                </ww:if>
            </tr>
        </ww:iterator>
    </table>

    <ul>

    </ul>

</form>

<script type="text/javascript">
    Calendar.setup({
        inputField	 :	"fromDate",	 // id of the input field
        ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
        button		 :	"calendar_fromDate_trigger",  // trigger for the calendar (button ID)
        align		  :	"Tl",		   // alignment (defaults to "Bl")
        singleClick	:	true
    });
</script>