<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N multi-assignment-confirm.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DodanieZbiorowejNotatki"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/multi-note.action'"/>" method="post">
	
	
	<ww:iterator value="documentIds" ><input type="hidden" name="documentIds" value="<ww:property/>"/></ww:iterator>

	<ww:textarea name="'note'" cols="50" rows="30" cssClass="'txt'" id="note"/> <br/><br/>
	<ds:submit-event value="getText('Dodaj notatk�')" name="'doAdd'" cssClass="'btn'"/>
	<input type="button" value="Zamknij" class="btn" 
			onclick="javascript:window.close()" />
</form> 
