<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N multi-assignment.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DekretacjaWieluPism"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/multi-assignment.action'"/>" method="post">
    <ww:hidden name="'divisionGuid'" id="divisionGuid"/>
    <ww:iterator value="activityIds">
        <ww:hidden name="'activityIds'" value="top"/>
    </ww:iterator>
    <ww:iterator value="plannedAssignments">
        <ww:hidden name="'plannedAssignments'" value="top"/>
    </ww:iterator>
    <ww:hidden name="'newPlannedAssignment'" id="newPlannedAssignment"/>
    <ww:hidden name="'onePlannedAssignment'" id="onePlannedAssignment"/>
    <ww:hidden name="'doPlanAssignment'" id="doPlanAssignment"/>
    <ww:hidden name="'tab'" value="tab"/>


    <table>
        <tr>
            <td>
                <ww:property value="treeHtml" escape="false"/>
            </td>
            <td>
            </td>
        </tr>
        <ds:available test="dekretacja.userlist">
		<tr>
			<td>
				Lista u�ytkownik�w:
			</td>
		</tr>
		<tr>
			<td>
				<ww:select cssClass="'sel'" name="'notImportantName'" list="allUsers" listKey="name" listValue="lastname+' '+firstname" id="selectedUser" size="10"/>
				<script  type="text/javascript">
					$j('#showTree').hide();
					function assignSelectedUser(){
						select = document.getElementById('selectedUser');
						userString = select.options[select.selectedIndex].value;
						openAssignmentWindow(null, userString, <ww:property value="hasExecutePlanned"/>,true);
					}
				</script>
				<input type="button" value="Dekretuj" onclick="assignSelectedUser()"/>
			</td>
		</tr>
		</ds:available>
        <tr>
            <td colspan="2">
                <ds:lang text="PrzygotowaneDekretacje"/>: <br/>

                <ww:if test="!plannedAssignmentBeans.empty">
                    <table>
                        <ww:iterator value="plannedAssignmentBeans">
                            <tr>
                                <td valign="top"><ww:checkbox name="'plannedAssignmentIds'" fieldValue="id" /></td>
                                <td valign="top">
                                    <b><ww:if test="user != null"><ww:property value="user.asFirstnameLastname()"/> w</ww:if>
                                    <ww:property value="division.prettyPath"/></b>
                                    <ds:lang text="celem"/> <b><ww:property value="objective"/></b> (<ww:property value="kindDescription"/>).
                                    <ww:if test="user == null"><ww:property value="scopeDescription"/></ww:if>
                                </td>
                            </tr>
                        </ww:iterator>
                    </table>
                </ww:if>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ww:if test="canAssign">
                    <ds:submit-event value="getText('Dekretuj')" name="'doManualPush'" confirm="getText('NaPewnoDekretowac')"/>
                    <ww:if test="!plannedAssignmentBeans.empty">
                        <ds:submit-event value="getText('UsunZaznaczone')" name="'doDelete'" />
                    </ww:if>
                    <ds:button-redirect href="'/office/index.jsp'" value="getText('Rezygnacja')"/>
                </ww:if>
            </td>
        </tr>
    </table>

</form>

<script type="text/javascript">
    /**
        Funkcja wywo�ywana przez odno�niki z drzewa dzia��w.
    */
    function post(guid)
    {
        document.getElementById('divisionGuid').value = guid;
        document.forms[0].submit();
    }

		function openAssignmentWindow(divisionGuid, username, hasExecutePlanned, hasManualPush)
		{
			window.open('<ww:url value="'/office/common/workflow-assignment.action'"/>?divisionGuid='+divisionGuid+'&hasExecutePlanned='+hasExecutePlanned+'&hasManualPush='+hasManualPush+(username != null ? '&username='+username : ''), 'assignmentwindow', 'width=500,height=400,resizable=yes,scrollbars=auto,status=no,toolbar=no,menu=no');
		}

		// je�eli push=true, od razu doManualPush
		function __accept_assignment(divisionGuid, username, kind, objective, scope, push)
		{
			var assignment = divisionGuid+'/'+username+'/'+kind+'/'+scope+'/'+objective;
			if (push)
			{
				document.getElementById('onePlannedAssignment').value = assignment;
				document.getElementById('doManualPush').value = 'true';
			}
			else
			{
				document.getElementById('newPlannedAssignment').value = assignment;
				document.getElementById('doPlanAssignment').value = 'true';
        }
        document.forms[0].submit();
    }
</script>
