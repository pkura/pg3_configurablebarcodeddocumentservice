<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N multi-boxing.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DodawanieWieluPismDoPudlaArchiwalnego"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/tasklist/multi-boxing.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
    <ww:iterator value="documentIds">
        <ww:hidden name="'documentIds'" value="top"/>
    </ww:iterator>
  <%--  <ww:hidden name="'boxId'" id="boxId"/> --%>
    <ww:hidden name="'tab'"/>

   <table>
   <ww:iterator value="boxLines">
       
           <tr>
                <td><ds:lang text="Linia"/>:</td>
                <td><ww:property value="name"/></td>
           </tr>
           <tr>
                <td><ds:lang text="Pudlo"/>:</td>
                <td><input type="text" name="<ww:property value="'boxNumbers.'+line"/>" id="<ww:property value="'boxNumber_'+line"/>" size="30" maxlength="30" class="txt" />
                    <input type="button" class="btn" value="<ds:lang text="NadajNumerOtwartegoPudla"/>"
                    onclick="document.getElementById('<ww:property value="'boxNumber_'+line"/>').value = '<ww:property value="currentBoxNumber"/>';"
                    <ww:if test="currentBoxNumber == null">disabled="true"</ww:if> />
                    <input type="button" class="btn" value="<ds:lang text="AnulujWybor"/>"
                    onclick="document.getElementById('<ww:property value="'boxNumber_'+line"/>').value = '';"
                    <ww:if test="currentBoxNumber == null">disabled="true"</ww:if> />
                </td>
            </tr>            
   </ww:iterator>
   <tr>
        <td colspan="2">
            <ds:submit-event value="getText('UmiescWpudle')" name="'doBox'" cssClass="'btn'"/>

            <ds:button-redirect href="'/office/index.jsp'" value="getText('Rezygnacja')"/>
        </td>
    </tr>
    </table>  
   
   <%-- <table>
        <tr>
            <td>Pud�o:</td>
            <td><ww:textfield name="'boxNumber'" id="boxNumber" size="30" maxlength="30" cssClass="'txt'" />
                <input type="button" class="btn" value="Nadaj numer otwartego pud�a"
                onclick="document.getElementById('boxNumber').value = '<ww:property value="currentBoxNumber"/>'; <ww:if test="currentBoxId != null">document.getElementById('boxId').value = <ww:property value="currentBoxId"/>;</ww:if>"
                <ww:if test="currentBoxNumber == null">disabled="true"</ww:if> />
                <input type="button" class="btn" value="Anuluj wyb�r"
                onclick="document.getElementById('boxNumber').value = ''; document.getElementById('boxId').value = null"
                <ww:if test="currentBoxNumber == null">disabled="true"</ww:if> />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ds:submit-event value="'Umie�� w pudle'" name="'doBox'" cssClass="'btn'"/>

                <ds:button-redirect href="'/office/index.jsp'" value="'Rezygnacja'"/>
            </td>
        </tr>
    </table> --%>

</form>
