<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N calendar.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1>Zarz�dzanie terminami</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:set name="canDelete" value="ownerPermissions"/>

<form action="<ww:property value="calendarBaseLink"/>" method="post" id="form">
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>

	<ww:if test="!own && users != null">
		<ww:select name="'username'" list="users" value="username"
			listKey="name" listValue="asLastnameFirstname()"
			headerKey="''" headerValue="'-- wybierz --'"
			cssClass="'sel'" />
		<ww:submit value="'Poka� terminy przypisane u�ytkownikowi'" cssClass="'btn'"/><br/>
	</ww:if>

	<ww:hidden name="'daily'" value="daily" />
	<input type="hidden" name="doDelete" id="doDelete" value="false" disabled="true" />
	<ww:hidden name="'sortField'" value="sortField" />
	<ww:hidden name="'ascending'" value="ascending" />

<ww:if test="own || username != null">
	
    <br/>
    <table width="100%">
    	<tr>
			<td>
				<ww:if test="daily">
					<ww:submit value="'Przejd� do dnia:'" cssClass="'btn'"/>
				</ww:if>
				<ww:else>
					<ww:submit value="'Przejd� do tygodnia zawieraj�cego dzie�:'" cssClass="'btn'"/>
				</ww:else>
    			<ww:textfield name="'firstDayLabel'" size="10" maxlength="10" cssClass="'txt'" id="firstDayLabel"/>
    				<img src="<ww:url value="'/calendar096/img.gif'"/>"
    				id="firstDayLabelTrigger" style="cursor: pointer; border: 1px solid red;"
    				title="Date selector" onmouseover="this.style.background='red';"
    				onmouseout="this.style.background=''"/>
    		</td>
        	<td align="right">
    			<ww:if test="daily">
    				<a href="<ww:property value='linkForView'/>&daily=false">Widok tygodniowy</a>
    			</ww:if>
    			<ww:else>
					<a href="<ww:property value='linkForView'/>&daily=true">Widok dzienny</a>
    			</ww:else>
    		</td>
    	</tr>
    	<tr>
    		<ww:if test="daily">
    			<td></td>
    			<td align="right">
    				<input type="submit" name="doPreviousDay" class="btn" value="<<" title="Poprzedni dzie�"/>
    				<input type="submit" name="doNextDay" class="btn" value=">>" title="Nast�pny dzie�"/>
    			</td>
    		</ww:if>
    		<ww:else>
				<td></td>
				<td align="right">
					<input type="submit" name="doPreviousWeek" class="btn" value="<<" title="Poprzedni tydzie�"/>
					<input type="submit" name="doNextWeek" class="btn" value=">>" title="Nast�pny tydzie�"/>
    			</td>
   			</ww:else>
    	</tr>
    
    	<ww:if test="!daily">
    		<tr>
    			<td colspan="2">Tydzie�: <ww:property value="firstDayLabel"/> - <ww:property value="lastDayLabel"/></td>
    		</tr>
    	</ww:if>
    </table>

    <br/><br/>
    <ww:if test="own">
    	<input type="button" value="Dodaj termin" class="btn" 
    		onclick="window.location.assign('time-new-own.action?backOwn=true&backDaily=<ww:property value='daily'/>');" />
    </ww:if>
    <ww:else>
		<input type="button" value="Dodaj termin" class="btn" 
			onclick="window.location.assign('time-new-any.action?backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>');" />
    </ww:else>
    
    <ww:if test="#canDelete">
    	<ww:set name="colspan" value="8"/>
    </ww:if>
    <ww:else>
		<ww:set name="colspan" value="3"/>
    	<ww:set name="startTimeAlign" value="'right'"/>
    	<ww:set name="endTimeAlign" value="'left'"/>
    </ww:else>
    

    <br/><br/>
    <table class="search" width="100%" cellspacing="0" id="mainTable">
		<ww:if test="#canDelete">
			<th width="2%"></th>
		</ww:if>
        <th align="<ww:property value='#startTimeAlign'/>">
        	<nobr>
        		<a href="<ww:property value='sortDesc'/>&sortField=startTime">
        			<ww:if test="sortField == 'startTime' && ascending == 'false'">
        				<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
        			</ww:if>
        			<ww:else>
        				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
        			</ww:else>
        		</a> 
        		Rozpocz�cie 
        		<a href="<ww:url value="sortAsc"/>&sortField=startTime">
        			<ww:if test="sortField == 'startTime' && ascending == 'true'">
        				<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/>
        			</ww:if>
        			<ww:else>
        				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
        			</ww:else>
        		</a>
        	</nobr>
        </th>
        <th align="<ww:property value='#endTimeAlign'/>">
        	<nobr>
        		<a href="<ww:property value='sortDesc'/>&sortField=endTime">
        			<ww:if test="sortField == 'endTime' && ascending == 'false'">
        				<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
        			</ww:if>
        			<ww:else>
        				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
        			</ww:else>
        		</a> 
        		Zako�czenie 
        		<a href="<ww:url value="sortAsc"/>&sortField=endTime">
        			<ww:if test="sortField == 'endTime' && ascending == 'true'">
        				<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/>
        			</ww:if>
        			<ww:else>
        				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
        			</ww:else>
       			</a>
       		</nobr>
       	</th>
    	<ww:if test="#canDelete">
            <th width="14%">
            	<nobr>
            		<a href="<ww:property value="sortDesc"/>&sortField=place">
            			<ww:if test="sortField == 'place' && ascending == 'false'">
	        				<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
            		</a> 
            		Miejsce 
            		<a href="<ww:url value="sortAsc"/>&sortField=place">
            			<ww:if test="sortField == 'place' && ascending == 'true'">
	        				<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
           			</a>
         		</nobr>
       		</th>
            <th width="29%">
            	<nobr>
            		<a href="<ww:property value="sortDesc"/>&sortField=description">
            			<ww:if test="sortField == 'description' && ascending == 'false'">
	        				<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
           			</a> 
           			Opis 
           			<a href="<ww:url value="sortAsc"/>&sortField=description">
           				<ww:if test="sortField == 'description' && ascending == 'true'">
	        				<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
           			</a>
           		</nobr>
           	</th>
            <th width="14%">
            	<nobr>
            		<a href="<ww:property value="sortDesc"/>&sortField=creator">
            			<ww:if test="sortField == 'creator' && ascending == 'false'">
	        				<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
            		</a> 
            		Dodany przez 
            		<a href="<ww:url value="sortAsc"/>&sortField=creator">
            			<ww:if test="sortField == 'creator' && ascending == 'true'">
	        				<img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:if>
	        			<ww:else>
	        				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
	        			</ww:else>
            		</a>
            	</nobr>
            </th>
    	</ww:if>
    	<ww:set name="lastDay" value="'first'"/>
    	<ww:if test="daily">
			<tr>
				<th class="calendarDay" colspan="<ww:property value='#colspan'/>"><nobr><ww:property value="currentDayTitle"/></nobr></th>
			</tr>    			
    	</ww:if>
    	<ww:iterator value="times">
    		<ww:if test="!daily && #lastDay != weekdayString">
				<ww:set name="lastDay" value="weekdayString"/>
    			<tr>
    				<th class="calendarDay" colspan="<ww:property value='#colspan'/>"><nobr><ww:property value="weekdayString"/> <ww:property value="dateString"/></nobr></th>
    			</tr>    			
    		</ww:if>
    		<tr>
    			<ww:if test="!#canDelete">
    				<td><ww:property value="startTimeString"/></td>
    				<td><ww:property value="endTimeString"/></td>
    			</ww:if>
    			<ww:else>
        			<td><input type="checkbox" name="actionIds" class="chk" value="<ww:property value='id'/>" /></td>
    				<td>
    			    	<ww:if test="own">
    			    		<a href="time-new-own.action?timeId=<ww:property value='id'/>&backOwn=true&backDaily=<ww:property value='daily'/>" /><ww:property value="startTimeString"/></a>
    			    	</ww:if>
    			    	<ww:else>
			    			<a href="time-new-any.action?timeId=<ww:property value='id'/>&backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>" /><ww:property value="startTimeString"/></a>
			    		</ww:else>
    			    </td>
    				<td>
			    		<ww:if test="own">
			    			<a href="time-new-own.action?timeId=<ww:property value='id'/>&backOwn=true&backDaily=<ww:property value='daily'/>" /><ww:property value="endTimeString"/></a>
			    		</ww:if>
			    		<ww:else>
		    				<a href="time-new-any.action?timeId=<ww:property value='id'/>&backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>" /><ww:property value="endTimeString"/></a>
		    			</ww:else>
			    	</td>
    				<td>
		    			<ww:if test="own">
		    				<a href="time-new-own.action?timeId=<ww:property value='id'/>&backOwn=true&backDaily=<ww:property value='daily'/>" /><ww:property value="place"/></a>
		    			</ww:if>
		    			<ww:else>
		    				<a href="time-new-any.action?timeId=<ww:property value='id'/>&backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>" /><ww:property value="place"/></a>
		    			</ww:else>
		    		</td>
    				<td>
		    			<ww:if test="own">
		    				<a href="time-new-own.action?timeId=<ww:property value='id'/>&backOwn=true&backDaily=<ww:property value='daily'/>" title="<ww:property value='description'/>" /><ww:property value="getShortDescription(30)"/></a>
		    			</ww:if>
		    			<ww:else>
	    					<a href="time-new-any.action?timeId=<ww:property value='id'/>&backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>" title="<ww:property value='description'/>" /><ww:property value="getShortDescription(30)"/></a>
	    				</ww:else>
		    		</td>
    				<td>
		    			<ww:if test="own">
		    				<a href="time-new-own.action?timeId=<ww:property value='id'/>&backOwn=true&backDaily=<ww:property value='daily'/>" /><ww:property value="creator.firstname"/> <ww:property value="creator.lastname"/></a>
		    			</ww:if>
		    			<ww:else>
	    					<a href="time-new-any.action?timeId=<ww:property value='id'/>&backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>" /><ww:property value="creator.firstname"/> <ww:property value="creator.lastname"/></a>
	    				</ww:else>
		    		</td>
    			</ww:else>
    		</tr>
    	</ww:iterator>
    	
    	<ww:if test="#canDelete && times.size() > 0">
    		<tr><td colspan="<ww:property value='#colspan'/>">&nbsp;</td></tr>
    		<tr>
            	<td><input type="checkbox" onclick="selectAll(this)"/></td>
            	<td id="selectAll" style="text-align: left" colspan="<ww:property value='#colspan'/>">Zaznacz wszystkie / cofnij zaznaczenie</td>
            </tr>
        </ww:if>
    </table>

	<ww:if test="#canDelete && times.size() > 0">
		<select id="action" class="sel">
        	<option value="">-- wybierz akcj� --</option>
        	<option value="doDelete">Usu�</option>
        </select>
        <input type="button" class="btn" value="Wykonaj na zaznaczonych"
        	onclick="selectAction(document.getElementById('action').value);"/>
    </ww:if>    
    
   	<br/><br/>
    <ww:if test="own">
		<input type="button" value="Dodaj termin" class="btn" 
			onclick="window.location.assign('time-new-own.action?backOwn=true&backDaily=<ww:property value='daily'/>');" />
		</ww:if>
	<ww:else>
		<input type="button" value="Dodaj termin" class="btn" 
			onclick="window.location.assign('time-new-any.action?backOwn=false&backDaily=<ww:property value='daily'/>&backUsername=<ww:property value='username'/>');" />
	</ww:else>
    
</ww:if>
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>  
</form>

<%--
Docusafe&reg; <br/>
Wersja: <%= Configuration.getVersionString() %>.<%= Configuration.getDistNumber() %> <br/>
--%>


<ww:if test="own || username != null">

<script type="text/javascript">
Calendar.setup({
    inputField     :    "firstDayLabel",     // id of the input field
    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
    button         :    "firstDayLabelTrigger",  // trigger for the calendar (button ID)
    align          :    "Tl",           // alignment (defaults to "Bl")
    singleClick    :    true
});

function selectAll(checkbox)
{
    var boxes = document.getElementsByName('actionIds');
    if (boxes.length > 0)
    {
        for (var i=0; i < boxes.length; i++)
        {
            boxes.item(i).checked = checkbox.checked;
        }
    }
}

function selectAction(act)
{
    var ids = document.getElementsByName('actionIds');
    var anySelected = false;
    for (var i=0; i < ids.length; i++)
     {
        if (ids.item(i).checked == true)
        {
            anySelected = true;
            break;
        }
    }

    if (!anySelected)
    {
        alert('Nie zaznaczono element�w');
        return false;
    }

    if (act == '')
    {
        alert('Nie wybrano akcji');
        return false;
    }

    document.getElementById(act).disabled = false;
    document.getElementById(act).value = 'true';
    document.getElementById('form').submit();
}

</script>

</ww:if>