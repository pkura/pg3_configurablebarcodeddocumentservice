<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N time-new.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>" title="main" />
</head>
<body>

<h1>
	<ww:if test="timeId == null">
		Dodawanie terminu
	</ww:if>
	<ww:else>
		Modyfikacja terminu
	</ww:else>
</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<%-- wykrywanie kolizji termin�w chwilowo wy��czone
<ww:if test="collisionError">
	<ul class="warning">
Podany termin koliduje z nast�puj�cymi terminami:<br/>
		<ww:iterator value="collisions">
			<li><ww:property /></li>
		</ww:iterator>
	</ul>
</ww:if>
--%>

<form action="time-new-own.action" method="post">
	<ww:hidden name="'timeId'" value="timeId" />
	<ww:hidden name="'backDaily'" value="backDaily" />
	<ww:hidden name="'backOwn'" value="backOwn" />
	<ww:hidden name="'backUsername'" value="backUsername" />
	<table border='0' cellspacing='0' cellpadding='0'>
		<tr>
			<td>Data<span class="star">*</span>:</td>
			<td>
				<ww:if test="modifyPermission || allTimesPermission || timeId == null">
					<ww:textfield name="'date'" size="10" maxlength="10" cssClass="'txt'" id="date"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="dateTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
		        </ww:if>
		        <ww:else>
		        	<ww:property value="date"/>
		        </ww:else>
			</td>
		</tr>
		<tr>
			<td>Godzina rozpocz�cia<span class="star">*</span>:</td>
			<td>
				<ww:if test="modifyPermission || allTimesPermission || timeId == null">
					<ww:textfield name="'startTime'" size="10" maxlength="10" cssClass="'txt'" id="startTime"/>
				</ww:if>
				<ww:else>
					<ww:property value="startTime"/>
				</ww:else>
			</td>
		</tr>
		<tr>
			<td>Godzina zako�czenia<span class="star">*</span>:</td>
			<td>
				<ww:if test="modifyPermission || allTimesPermission || timeId == null">
					<ww:textfield name="'endTime'" size="10" maxlength="10" cssClass="'txt'" id="endTime"/>
				</ww:if>
				<ww:else>
					<ww:property value="endTime"/>
				</ww:else>
			</td>
		</tr>
		<tr>
			<td>Miejsce:</td>
			<td>
				<ww:if test="modifyPermission || allTimesPermission || timeId == null">
					<ww:textfield name="'place'" size="64" maxlength="64" cssClass="'txt'" id="place"/>
				</ww:if>
				<ww:else>
					<ww:property value="place"/>
				</ww:else>
			</td>
		</tr>
		<tr valign="top">
			<td>Opis<span class="star">*</span>:</td>
			<td>
				<ww:if test="modifyPermission || allTimesPermission || timeId == null">
					<ww:textarea name="'description'" rows="3" cols="47" cssClass="'txt'" id="description"/>
				</ww:if>
				<ww:else>
					<ww:property value="description"/>
				</ww:else>
				<input type="button" value="Dodaj osob�" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" >
			</td>
		</tr>
	</table>

	
	<table>
    	<tr>
        	<th>Dost�pni u�ytkownicy:</th>
        	<th></th>
        	<th>Przypisz termin u�ytkownikom:</th>
        </tr>
        <tr>
        	<td valign="top">
            	<ww:select name="'availableUsers'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
            		list="usersAll" />
            </td>
            <td valign="middle">
            	<input type="button" value=" &gt;&gt; "
            		onclick="moveOptions(this.form.availableUsers, this.form.usernames);"
            		class="btn"/>
            	<br/>
            	<input type="button" value=" &lt;&lt; "
            		onclick="moveOptions(this.form.usernames, this.form.availableUsers);"
            		class="btn"/>
            	<br/>
            </td>
            <td valign="top">
            	<table width="100%">
                	<tr>
                    	<td rowspan="2">
                        	<ww:select name="'usernames'"
                        		id="usernames"
                        		multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                        		list="usersSelected"/>
                        </td>
                    </tr>
               </table>
            </td>
        </tr>
    </table>

	<ww:if test="timeId == null">
		<input type="submit" class="btn" name="doCreate" value="Dodaj termin"
			onclick="if (!validateForm()) return false; document.getElementById('doCreate').disabled = false;"/>
	</ww:if>
	<ww:else>
		<input type="submit" class="btn saveBtn" name="doUpdate" value="Zapisz zmiany"
			onclick="if (!validateForm()) return false; document.getElementById('doCreate').disabled = false;"/>
	</ww:else>
	<ww:if test="backOwn">
		<input type="button" class="btn cancelBtn" value="Anuluj"
			onclick="window.location.assign('calendar-own.action?daily=<ww:property value='backDaily'/>&firstDayLabel=<ww:property value='date'/>');"/>
	</ww:if>
	<ww:else>
		<input type="button" class="btn" value="Anuluj"
			onclick="window.location.assign('calendar-any.action?daily=<ww:property value='backDaily'/>&username=<ww:property value='backUsername'/>&firstDayLabel=<ww:property value='date'/>');"/>
	</ww:else>
	
</form>

<br/>
<ww:property value="newUsername"/>

<%--
Docusafe&reg; <br/>
Wersja: <%= Configuration.getVersionString() %>.<%= Configuration.getDistNumber() %> <br/>
--%>

</body>
</html>

<script type="text/javascript">
Calendar.setup({
    inputField     :    "date",     // id of the input field
    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
    button         :    "dateTrigger",  // trigger for the calendar (button ID)
    align          :    "Tl",           // alignment (defaults to "Bl")
    singleClick    :    true
});
</script>

<script type="text/javascript">
function getValue(str, st, en)
{
	var i;
	var out = 0;
	var c = 1;
	
	if (en < 0)
		en = str.length - 1;
	for (i = en; i >= st; --i){
		var cc = str.charAt(i);
		if ('0' <= cc && cc <= '9'){
			out += c * cc;
			c = c * 10;
		} else{
			return -1;
		}
	}
	return out;
}

</script>

<script type="text/javascript">

function validateDate(date)
{
	var ind1 = date.indexOf('-', 0);
	var ind2 = date.indexOf('-', ind1 + 1);
	if (ind1 == -1 || ind2 == -1){
		return -1;
	}
	var d = getValue(date, 0, ind1 - 1);
	var m = getValue(date, ind1 + 1, ind2 - 1);
	var y = getValue(date, ind2 + 1, -1);
	if (d < 0 || d > 31 || m < 0 || m > 12 || y < 0 || y > 10000){
		return -2;
	}
	return 0;
}

function validateTime(time)
{
	var ind1 = time.indexOf(':', 0);
	if (ind1 == -1){
		return -1;
	}
	var h = getValue(time, 0, ind1 - 1);
	var m = getValue(time, ind1 + 1, -1);
	if (h < 0 || h > 23 || m < 0 || m > 59){
		return -2;
	}
	return m + h * 100;
}

</script>

<script type="text/javascript">
function validateForm()
{
    var description = document.getElementById('description').value;
    if (description.trim().length == 0) { alert('Nie podano opisu terminu'); return false; }
    if (description.length > 256) { alert('Podany opis terminu jest zbyt d�ugi. Opis mo�e mie� co najwy�ej 256 znaki'); return false; }
    var place = document.getElementById('place').value;
    if (place.length > 64) { alert('Podana nazwa miejsca jest zbyt d�uga. Nazwa mo�e mie� co najwy�ej 64 znaki'); return false; }

<ww:if test="allTimesPermission || timeId == null">
    var selnames = document.getElementById('usernames');
    var anySelected = (selnames.length > 0);
    if (!anySelected)
    {
        alert('Nie przypisano terminu �adnemu z u�ytkownik�w');
        return false;
    }
</ww:if>

	var date = document.getElementById('date').value;
	var res = validateDate(date);
	if (res == -1){
		alert('Niew�a�ciwy format daty');
		return false;
	} else if (res == -2){
		alert('Niew�a�ciwa data');
		return false;
	}
	
	var time = document.getElementById('startTime').value;
	t1 = validateTime(time);
	if (t1 == -1){
		alert('Niew�a�ciwy format godziny rozpocz�cia');
		return false;
	} else if (t1 == -2){
		alert('Niew�a�ciwa godzina rozpocz�cia');
		return false;
	}
	
	time = document.getElementById('endTime').value;
	t2 = validateTime(time);
	if (t2 == -1){
		alert('Niew�a�ciwy format godziny zako�czenia');
		return false;
	} else if (t2 == -2){
		alert('Niew�a�ciwa godzina zako�czenia');
		return false;
	}
	
	if (t1 >= t2){
		alert('Godzina zako�czenia nie jest p�niejsza od godziny rozpocz�cia')
		return false;
	}
	
	<ww:if test="allTimesPermission || timeId == null">
    	selectAllOptions(selnames);
    </ww:if>

    return true;
}

// lparam - identyfikator
// wparam - pozycja na li�cie
function openPersonPopup(lparam, dictionaryType)
{
    var url = '<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
    //alert('url='+url);
    openToolWindow(url, 'person', 700, 650);
}

/*
    Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
    //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

    if (lparam == 'recipient')
    {
        var descr = document.getElementById('description');
        if (descr.value.length == 0)
        	descr.value = 'Spotkanie z:' + "\n";
        descr.value = descr.value.concat(map.firstname + ' ' + map.lastname);
        if (map.organization.length > 0)
            descr.value = descr.value.concat('(' + map.organization + ')');
        descr.value = descr.value.concat("\n");
    }
}

</script>
