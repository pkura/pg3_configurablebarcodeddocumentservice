<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N calendar.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<style>

textarea.cal[readonly], textarea.cal[readonly]:focus, textarea.cal[readonly]:hover {
	background: url('<ww:url value="'/img/lock_mini.gif'" />') no-repeat right bottom;
}

span.user, span.res {
	background-image: url(<ww:url value="'/img/close_mini.gif'" />);
}

#loading {
	background-image: url('<ww:url value="'/img/ajax_loader_white.gif'"/>');
}

</style>

<h1>Zarz�dzanie terminami</h1>

<input type="button" class="debugButton" value="Clear monitor events" onclick="cme()" />
<input type="button" class="debugButton" value="Disable refreshing" onclick="monitorTimer = false;" />
<input type="button" class="debugButton" value="Show/hide console" id="showHideConsoleBtn" />
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:set name="canDelete" value="ownerPermissions"/>

<form action="<ww:property value="calendarBaseLink"/>" method="post" id="form">
	<ds:available test="layout2">
		<div id="middleContainer" style="*padding: 0 5px 0 5px;"> <!-- BIG TABLE start -->
	</ds:available>
	
	<script type="text/javascript">
		var MINI_CLOSE_IMG = '<ww:url value="'/img/close_mini.gif'" />';
		var MINI_ACCEPT_IMG = '<ww:url value="'/img/accept_mini.gif'" />';
		var MINI_LOCK_IMG = '<ww:url value="'/img/readonly_mini.gif'" />';
		var MINI_READONLY_IMG = '<ww:url value="'/img/readonly_mini.gif'" />';
		var MINI_CAL_COLOR_IMG = '<ww:url value="'/img/circle_mini_down_white.gif'" />';
		var MINI_PERIOD_IMG = '<ww:url value="'/img/period_mini.gif'" />';
		var DSUSER_RESOURCE_TYPE = 'DSUSER';
		var ANY_RESOURCE_TYPE = 'ANY';
	</script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/main.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/grid.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/agenda.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/view.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/util.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/fullCalendar/src/jquery.json-2.2.min.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/jquery.growfield-1.1.js'" />" ></script>
	<script type="text/javascript" src="<ww:url value="'/office/tasklist/calendar/jquery.autocomplete.js'" />"></script>
	<script type="text/javascript" src="<ww:url value="'/jquery.mousewheel.min.js'" />" ></script>
	<script type="text/javascript" src="<ww:url value="'/office/tasklist/calendar/ajax-calendar.js'" />" ></script>
	
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/office/tasklist/calendar/jquery.autocomplete.css'" />" />
	
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/fullCalendar/css/main.css'" />" />
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/fullCalendar/css/grid.css'" />" />
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/fullCalendar/css/agenda.css'" />" />
	<link rel="stylesheet" type="text/css" href="<ww:url value="'/office/tasklist/calendar/ajax-calendar.css'" />" />
	
	<link rel="alternate stylesheet" title="Debug" type="text/css" href="<ww:url value="'/fullCalendar/css/debug.css'" />" />
	
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" href="<ww:url value="'/fullCalendar/css/main.ie8.css'" />" />
	<![endif]-->
	
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" href="<ww:url value="'/fullCalendar/css/main.ie9.css'" />" />
	<![endif]-->

	<div id="calendar"></div>
	<div id="eventInfo" title="Wczytywanie...">
		<div>Wczytywanie kalendarza&hellip;</div>
		<div id="progressbar"></div>
	</div>
	<div id="loading">wczytywanie&hellip;</div>
	<div id="calendarsList"></div>
	<div id="ajaxInfo">NO INFO</div>
	<div id="ajaxTopInfo"></div>
	<div id="lastRefresh" style="text-align: left;">NO REFRESH</div>
	<div id="debugObsolete">DEBUG OBSOLETE</div>
	<div id="colorTable"></div>
	<div id="eventStatusReason"></div>
	<div id="confirmDesc"></div>
	<svg id="svg-image-blur">
    <filter id="blur-effect-1">
        <feGaussianBlur stdDeviation="2" />
    </filter>
</svg>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>

<div id="periodModeSelection" class="period-mode-selection-hidden">
		<div class="container-table">
			<div class="container-row">
				<div class="container-cell">
					<button id="periodModeSingle">Tylko to wydarzenie</button>
				</div>
				<div class="container-cell period-mode-desc">
					Wszystkie pozosta�e wydarzenia w serii pozostan� takie same.
				</div>
			</div>
			<div class="container-row">
				<div class="container-cell">
					<button id="periodModeFuture">Kolejne wydarzenia</button>
				</div>
				<div class="container-cell period-mode-desc">
					To wydarzenie oraz kolejne wydarzenia zostan� zmienione.
				</div>
			</div>
			<div class="container-row">
				<div class="container-cell">
					<button id="periodModeCancel">Anuluj</button>
				</div>
				<div class="container-cell period-mode-desc">
					Wydarzenie nie zostanie zmienione.
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$j('#periodModeSelection').find('button').button();
	</script>

<style>
@media print {

#leftCell, td.upperBarNew, #middleMenuContainer {
	display: none;
}

div.fc-agenda-body {
	height: auto !important;
}

}
</style>