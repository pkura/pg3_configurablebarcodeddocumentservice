var JSON_FAILED = "JSON_FAILED";
var MSG_ADD_USER = 'dodaj u�ytkownika';
var MSG_ADD_RES = 'dodaj zas�b';
var MSG_ADD_DESC = 'dodaj opis';
var MSG_ADD_PLACE = 'dodaj miejsce';
var MSG_ADD_STATUS_REASON = 'podaj pow�d odrzucenia';
var MSG_ADD_DELETE_REASON = 'podaj pow�d odrzucenia';
var MSG_NO_STATUS_REASON = 'nie podano powodu';
var MSG_SELECT_CAL = 'wybierz kalendarz';
var MSG_EVENT_CONFIRM = 'akceptuj';
var MSG_EVENT_CONFIRMED = 'zaakceptowano';
var MSG_EVENT_REJECTED = 'odrzucono';
var MSG_ONE_RESOURCE_BUSY = {
    res: 'zas�b',
    busy: 'jest zaj�ty'
};
var MSG_MANY_RESOURCE_BUSY = {
    res: 'zasoby',
    busy: 's� zaj�te'
};
var MSG_BUSY = 'zaj�ty(-a)';
var MSG_TRY_TO_REFRESH = 'Spr�buj od�wie�y� kalendarz.';
var MSG_CHANGE_CAL_COLOR = 'Zmie� kolor kalendarza.';
var TEXT_OWNER = 'w�a�ciciel';
var TEXT_DEFAULT_COLOR = 'domy�lny';

var PERMISSION_EDIT = 1;
var PERMISSION_SEE_ONLY = 2;
var PERMISSION_SEE_ONLY_STATE = 3;
var USER_CALENDAR = "USER_CALENDAR";
var ANY_CALENDAR = "ANY_CALENDAR";
var RESOURCE_BUSY = "RESOURCE_BUSY";
var RESOURCE_FREE = "RESOURCE_FREE";
var USER_BUSY = "USER_BUSY";
var USER_FREE = "USER_FREE";
var CALENDAR_COLOR_CHANGED = "CALENDAR_COLOR_CHANGED";


var LOAD_CALENDARS_FAILED = 'Nie mo�na wczyta� listy kalendarzy.';
var LOAD_USERS_FAILED = 'Nie mo�na wczyta� listy u�ytkownik�w.';
var LOAD_RES_FAILED = 'Nie mo�na wczyta� listy zasob�w.';
var LOAD_CALENDAR_FAILED = 'Nie mo�na wczyta� kalendarza';
var UNKNOWN_LOAD_FAILED = 'Nieznany b��d';
var LOAD_DESC_FAILED = 'Informacje techniczne';

var DIALOG_SIZE = {
    width: 570
};
var REFRESH_TIME = 5000 * 60;
var TOP_INFO_TIME = 5000;

var __currentUser = null;
var __currentCalIndex = 0;
var __firstEnabledCalIndex = null;
var __creationSource = null;
var __skipEventId = null;
var __confirmImgHover = false;
var __colorChangeCalId = null;
var availCalHeight = 0;
var availCalWidth = 0;
var calAspectRatio = 0;

var pageCon = $j('#request_pageContext').val();
var mapAllUsers = null;
var arrayAllUsers = null;
var mapAllRes = null;
var monitorTimer = true;
var dragCursorStr = '';
var calsList = null;
var editableCal = null; // pierwszy na li�cie kalendarz z prawem do zapisu
var idCalToIndexList = null; // "cal_id" (cal_#num) -> calendar_index (in calsList)
var lastRefreshList = null;
var ajaxTopInfoTimer = null;

var jqCalsList = null;
var jqAjaxInfo = null;
var jqEventInfo = null;
var jqlastRefresh = null;
var jqProgressBar = null;
var jqLoading = null;
var jqAjaxTopInfo = null;
var jqColorTable = null;
var jqEventStatusReason = null;
var jqConfirmDesc = null;
var progressItemSize = 100;
var calsListTop = null;
var urlParams = null;		// mapa .year, .month, .day, .eventId
var topScroll = null;

monthNames = ['Stycze�', 'Luty', 'Marzec', 'Kwiecie�', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpie�', 'Wrzesie�', 'Pa�dziernik', 'Listopad', 'Grudzie�'];
monthNamesShort = ['Sty', 'Lu', 'Mar', 'Kw', 'Maj', 'Cze', 'Lip', 'Sie', 'Wrz', 'Pa', 'Lis', 'Gru'];
dayNames = ['Niedziela', 'Poniedzia�ek', 'Wtorek', '�roda', 'Czwartek', 'Pi�tek', 'Sobota'];
dayNamesShort = ['Nie', 'Pon', 'Wt', '�r', 'Czw', 'Pi', 'Sob'];

var periodCases = [];
periodCases[0] = ['dzie�', 'dni', 'dni'];
periodCases[1] = ['tydzie�', 'tygodnie', 'tygodni'];
periodCases[2] = ['miesi�c', 'miesi�ce', 'miesi�cy'];
periodCases[3] = ['rok', 'lata', 'lat'];


/**
 * DEBUG STUFF
 */
var tmpEvts = new Array();
var DEBUG_MODE = false;

$j(document).ready(function(){
    availCalHeight = $j(document).height() - 330;
    availCalWidth = $j('#middleMenuContainer').width() - 50; // 24 = padding + border
    availCalWidth = ie7 ? availCalWidth - 20 : availCalWidth;
    calAspectRatio = availCalWidth / availCalHeight;
    dragCursorStr = ie ? 'move' : '-moz-grabbing';
    dragCursorStr = $j.browser.webkit ? '-webkit-grabbing' : dragCursorStr;
    jqCalsList = $j('#calendarsList');
    jqAjaxInfo = $j('#ajaxInfo');
    jqEventInfo = $j('#eventInfo');
    jqlastRefresh = $j('#lastRefresh');
    jqProgressBar = $j('#progressbar');
    jqLoading = $j('#loading');
    jqAjaxTopInfo = $j('#ajaxTopInfo');
    jqColorTable = $j('#colorTable');
    jqEventStatusReason = $j('#eventStatusReason');
    jqConfirmDesc = $j('#confirmDesc');
    try {
    	calsListTop = $j('#ul_left_ie li:last').position().top + 30;
    } catch(e) {
    	calsListTop = 0;
    }
	$j('#ul_left_ie').after(jqCalsList).after($j('#loading'));	    
	
	urlParams = getUrlParams();
	
	if(urlParams.embedded === true) {
		$j('h1, .horizontalLine, #middleMenuContainer, #calendarsList').hide();
	}
    
    $j('#eventInfo').dialog({
        modal: true,
        closeOnEscape: false,
        draggable: false,
        resizable: false,
        width: 400,
        height: 100,
        open: function(ev, ui){
            $j('a.ui-dialog-titlebar-close').hide();
            jqProgressBar.progressbar();
        }
    });
    
    if (!DEBUG_MODE) {
        jqlastRefresh.hide();
        $j('input.debugButton').hide();
    }
    else {
        $j('#showHideConsoleBtn').toggle(function(){
            $j('#debugObsolete').show();
        }, function(){
            $j('#debugObsolete').hide();
        });
    }
    
    if (ie7) {
        availCalHeight -= 30;
        /*$j('#loading').css('left', $j('#middleMenuContainer').position().left + $j('#middleMenuContainer').width() - $j('#loading').width() - 5 + 'px')
        
         .css('top', $j('#middleMenuContainer').position().top + 5 + 'px');*/
        
    }
    
    $j('#eventInfo').ajaxError(function(e, xhr, settings, exception){
        $j(this).dialog('destroy');
        $j(this).attr('title', 'B��D');
        
        if (settings.data.indexOf('getCalendars=true') != -1) 
            $j(this).html(LOAD_CALENDARS_FAILED);
        else 
            if (settings.data.indexOf('getUsers=true') != -1) 
                $j(this).html(LOAD_USERS_FAILED);
            else 
                if (settings.data.indexOf('getResources=true') != -1) 
                    $j(this).html(LOAD_RES_FAILED);
                else 
                    if (settings.url.indexOf('calendarId=') != -1) {
                        $j(this).html(LOAD_CALENDAR_FAILED);
                        var str = settings.url.toString();
                        var calName = calsList[idCalToIndexList['cal_' + str.slice(str.indexOf('calendarId=') + 'calendarId'.length + 1, str.length)]].label;
                        $j(this).html($j(this).html() + ': <b>' + calName + '</b>');
                    }
                    else 
                        $j(this).html(UNKNOWN_LOAD_FAILED + ': ' + settings.data);
        
        $j(this).html($j(this).html() + '<br />' + MSG_TRY_TO_REFRESH);
		
		var str = '<br/><div style="color: #666; font-size: 10px;">' + LOAD_DESC_FAILED + ': <table border=0>';
		str += '<tr><td>TIME:</td><td>' + (new Date()).toString() + '</td></tr>';
		str += '<tr><td>STATUS:</td><td>' + xhr.status + ' ' + xhr.statusText + '</td></tr>';
		str += '<tr><td>URL:</td><td>' + settings.url + '</td></tr>';
		str += '<tr><td>DATA:</td><td>' + settings.data + '</td></tr>';
		
		str += '</table></div>';
		
		$j(this).html($j(this).html() + '<br />' + str);
        
        $j(this).dialog({
            modal: true,
            closeOnEscape: false,
            draggable: false,
            resizable: false,
            width: 400,
            open: function(ev, ui){
                $j('a.ui-dialog-titlebar-close').hide();
                jqProgressBar.progressbar();
                $j(this).addClass('errorInfo');
            },
            close: function(ev, ui){
                $j(this).removeClass('errorInfo');
                $j('#calendar').fullCalendar('destroy');
            },
            buttons: {
                'Zamknij': function(){
                    $j(this).dialog('close');
                },
                'Od�wie� kalendarz': function(){
                    window.location.reload();
                }
            }
        });
        //trace(settings);
    });
    initCalendarStuff();
    
    jqAjaxInfo.css('left', ($j(document).width() / 2 - 100) + 'px');
});

function initFullCalendar(){
	var embedded = urlParams.embedded ? true : false;
	urlParams.month = urlParams.month != null ? (Number(urlParams.month)-1) : undefined;
    $j('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        firstDay: urlParams.firstday != null ? urlParams.firstday : 1,
        monthNames: monthNames,
        monthNamesShort: monthNamesShort,
        dayNames: dayNames,
        dayNamesShort: dayNamesShort,
        timeFormat: {
            agenda: 'HH:mm{-HH:mm}',
            '': 'HH(:MM)'
        },
        axisFormat: 'H(:mm)',
        columnFormat: {
            week: 'ddd dd-MM',
            day: 'dddd dd-MM'
        },
        buttonText: {
            prev: '&nbsp;&#9668;&nbsp;',
            next: '&nbsp;&#9658;&nbsp;',
            prevYear: '&nbsp;&lt;&lt;&nbsp;',
            nextYear: '&nbsp;&gt;&gt;&nbsp;',
            today: 'dzisiaj',
            month: 'miesi�c',
            week: 'tydzie�',
            day: 'dzie�'
        },
        editable: true,
        contentHeight: urlParams.height || availCalHeight,
        events: pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + calsList[__firstEnabledCalIndex].id,
        defaultView: 'agendaWeek',
		year: urlParams.year,
		month: urlParams.month,
		date: urlParams.day,
		allDayText: '24h',
		minTime: urlParams.mintime != null ? urlParams.mintime : 0,
		
        eventRender: function(event, element, view){
            var upperBarStr = event.allDay ? 'span.fc-event-title' : 'span.fc-event-time';
            var calNameId = null;
            var displayResEvent = false;
            event.listResOnly = new Array();
			event.desc = event.desc ? event.desc : '';
			event.place = event.place ? event.place : '';
            
            /*try {
             if (event.source == undefined || typeof(event.source) == 'object') {
             //alert('before ' + event.source);
             event.source = __creationSource;
             //alert('after ' + event.source)
             }
             event.calendarId = parseInt(event.source.split('calendarId=')[1], 10);
             calNameId = 'cal_' + event.calendarId;
             } catch(e) {
             calNameId = 'cal_' + calsList[0].id;
             }*/
            if (event.calendarId == undefined) {
                //trace('default calendar for event #' + event.id);
                event.calendarId = editableCal.id;
            }
            calNameId = 'cal_' + event.calendarId;
            var eventCalendar = calsList[idCalToIndexList[calNameId]];
            
            if (calNameId == null) 
                alert('Error');
            
            
            element.addClass('fc-event-' + eventCalendar.colorId);
			element.addClass('fc-event-id-' + event.id);
            
            var eventType = eventCalendar.type;
            
            event.permissionType = eventCalendar.permissionType;
            event.permissionType = event.owner.cn != __currentUser.cn ? PERMISSION_SEE_ONLY : PERMISSION_EDIT;
			
            
            event.editable = event.permissionType == PERMISSION_EDIT ? true : false;
            if(embedded)
            	event.editable = false;
            
            /* je�li mo�emy edytowa� event lub znajduje si� on na naszym kalendarzu */
            if (event.permissionType != PERMISSION_EDIT && calsList[idCalToIndexList['cal_' + event.calendarId]].owner != __currentUser.cn) {
                element.children('a').children(upperBarStr).append('<img class="eventReadonly" src="' + MINI_READONLY_IMG + '" />');
            }
            else 
                if (event.owner.cn == __currentUser.cn) { // w�a�ciciel eventu usuwa bez komentarza
                    element.children('a').children(upperBarStr).append('<img title="Usu�" onclick="removeEvent(' +
                    event.id +
                    ');event.cancelBubble=true;" class="eventRemove" src="' +
                    MINI_CLOSE_IMG +
                    '" />');
                }
                else { // pozostali dodaj� komentarz
                    element.children('a').children(upperBarStr).append('<img title="Usu�" class="eventRemove" src="' + MINI_CLOSE_IMG + '" />');
                    element.children('a').children(upperBarStr).children('img.eventRemove').click(function(){
                        showDeleteReasonDialog(event);
                        return false;
                    });
                }
            /* Je�li zdarzenie cykliczne */
			if(event.periodic) {
				element.children('a').children(upperBarStr).append('<img title="Usu�" class="eventPeriodic" src="' + MINI_PERIOD_IMG + '" />');
			}
            if (event.allDay) 
                element.addClass('fc-event-allDay');
            if (event.permissionType != PERMISSION_SEE_ONLY_STATE) {
                element.children('a').append('<span class="fc-event-desc">' + event.desc + '</span>').append('<span class="fc-event-owner">' + event.owner.partOneOfName + '&nbsp;' + event.owner.partTwoOfName + '</span>').append('<span class="fc-event-place">' + event.place + '</span>');
                
                if (DEBUG_MODE) 
                    element.children('a').append('calNameId=' + event.calendarId + ' permission=' + event.permissionType);
                event.calendarsId = new Array();
                for (i in event.resources) {
                    var sep = i < event.resources.length - 1 ? ',&nbsp;' : '';
                    var confirmed = event.resources[i].confirm ? '' : '?';
                    
                    if (event.resources[i].type == ANY_RESOURCE_TYPE) {
                        event.listResOnly.push(event.resources[i]);
                        /*if(eventType == ANY_CALENDAR) {
                        
                         for(k = 0; k < mapAllRes.length; k ++) {
                        
                         if(event.resources[i].cn == mapAllRes[k].cn) {
                        
                         displayResEvent = true;
                        
                         }
                        
                         }
                        
                         }*/
                        
                    }
                    
                    element.children('a').append('<span class="fc-event-user">' +
                    confirmed +
                    event.resources[i].partTwoOfName +
                    sep +
                    '</span>');
                    
                    if (!contains(event.calendarsId, event.resources[i].calendarId)) 
                        event.calendarsId.push(event.resources[i].calendarId);
                    
                    /* je�li nie zaakceptowali�my zaproszenia i event jest na edytowalnym kalendarzu */
                    if (eventCalendar.permissionType == PERMISSION_EDIT &&
                    event.resources[i].cn == __currentUser.cn &&
                    !event.resources[i].confirm) {
                        element.children('a').before('<img title="Akceptuj" id="confirmEvent_' + event.id + '" class="eventAccept" src="' +
                        MINI_ACCEPT_IMG +
                        '" />');
                        
                        $j('#confirmEvent_' + event.id).bind('click.event', function(evt) {
                            confirmEvent(event.id, event.resources, function() {
                                $j('#confirmEvent_' + event.id).remove();
                            }, {periodicDetached: true});
                            return false;
                        });
                    }
                }
            }
            else 
                if (event.permissionType == PERMISSION_SEE_ONLY_STATE) {
                    element.children('a').append('<span class="fc-event-desc">' + MSG_BUSY + '</span>');
                    element.children('a').removeClass('dontClick').addClass('dontClick');
                }
            if (DEBUG_MODE) 
                element.children('a').append('<span class="fc-event-desc">' + event.id + '</span>');
            
            
            
            //if(eventType == ANY_CALENDAR && !displayResEvent)
            //return false;
        },
        
        eventAfterRender: function(event, element, view){
            if (!event.allDay) {
                try {
                    if (element.width() < 55) {
                        var jqUpperBar = element.children('a').children('span.fc-event-time');
                        jqUpperBar.html(jqUpperBar.html().replace(/[0-9][0-9]:[0-9][0-9]\s-\s[0-9][0-9]:[0-9][0-9]/i, '&nbsp;'));
                    }
                    else 
                        if (element.width() < 100) {
                            var jqUpperBar = element.children('a').children('span.fc-event-time');
                            jqUpperBar.html(jqUpperBar.html().replace(/\s-\s[0-9][0-9]:[0-9][0-9]/i, '&nbsp;'));
                        }
                } 
                catch (e) {
                    //trace('error: ' + e);
                }
            }
        },
        
        eventDrop: function(event, delta){
            updateEvent(event);
            monitorTimer = true;
            //trace('monitorTimer resumed');
            multiEventUpdater();
        },
        
        eventResize: function(event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view){
            if(event.orgStart == null) 
            	event.orgStart = new Date(event.start);
            if(event.orgEnd == null)
            	event.orgEnd = new Date(event.end.getTime() - minuteDelta*60*1000);
            updateEvent(event);
            monitorTimer = true;
            //trace('monitorTimer resumed');
            multiEventUpdater();
        },
        
        eventDragStart: function(event, jsEvent, ui, view){
            monitorTimer = false;
            //trace('monitorTimer paused');
            event.oldStart = new Date(event.start);
            event.oldEnd = new Date(event.end);
            if(event.orgStart == null) 
            	event.orgStart = new Date(event.oldStart);
            if(event.orgEnd == null)
            	event.orgEnd = new Date(event.oldEnd);
            ui.helper.children('a').children().css('cursor', dragCursorStr);
            $j('#calendar').css('cursor', dragCursorStr);
            $j('#periodModeSelection').css('opacity', '0');
        },
        
        eventDragStop: function(event, jsEvent, ui, view){
            $j(ui.helper[0]).css('cursor', 'pointer');
            $j('#calendar').css('cursor', 'auto');
            $j('#periodModeSelection').css('opacity', '');
            //monitorTimer = true;
        },
        
        eventResizeStart: function(event, jsEvent, ui, view){
            monitorTimer = false;
            //trace('monitorTimer paused');
        },
        
        eventClick: function(calEvent, jsEvent, view){
        	if(embedded) {
        		parent.window.location.href = pageCon + '/office/tasklist/calendar-own.action?eventId=' + calEvent.id;
        	} else {
        		showEventInfo(calEvent);
        	}
        },
        
        dayClick: function(date, allDay, jsEvent, view){
        	if(embedded)
        		return false;
            monitorTimer = false;
            var newUserList = new Array();
            newUserList.push(__currentUser);
            newUserList[0].confirm = true;
            jqEventInfo.html('').dialog('destroy');
            
            var event = new Object();
            var endDate = new Date(date);
            //trace('getHours: ' + (endDate.getHours() + 1));
            endDate.setHours(endDate.getHours() + 1);
            event.id = -1; // tymczasowy event
            event.start = date;
            event.end = endDate;
            event.title = 'tytul';
            event.desc = '';
            event.place = '';
            event.allDay = allDay;
            //event.className = 'fc-event-new';
            event.resources = newUserList;
            event.editable = true;
            event.permissionType = PERMISSION_EDIT;
            event.calendarId = editableCal.id;
			event.periodic = false;
			event.periodKind = 0;
			event.periodLength = 0;
			event.periodEnd = new Date(endDate);
            event.owner = cloneUser(__currentUser);
            fillEventInfoDiv(jqEventInfo, event, newUserList);
            
            // tymczasowy event, �eby u�ytkownik widzial gdzie sie utworzy event
            //event.source = pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + editableCal.id;
            $j('#calendar').fullCalendar('renderEvent', event, false);
            
            jqEventInfo.dialog({
                resizable: true,
                modal: true,
                width: DIALOG_SIZE.width,
                buttons: [
                    {
                    	cn: 'dlg-cancel',
                    	text: 'Anuluj',
                    	click: function() {
                    		$j(this).dialog('close');
                    	}
                    },
                    {
                    	cn: 'dlg-save',
                    	text: 'Zapisz',
                    	click: function() {
                            var newStart = event.allDay ? [10, 0] : extractTime($j('#eventStart').val());
                            var newEnd = event.allDay ? [12, 0] : extractTime($j('#eventEnd').val());
                            
                            if (newStart && newEnd) {
                                $j(this).dialog('close');
                                if ($j('#eventDesc').hasClass('empty') && $j('#eventDesc').val() == MSG_ADD_DESC) 
                                    $j('#eventDesc').val('');
                                if ($j('#eventPlace').hasClass('empty') && $j('#eventPlace').val() == MSG_ADD_PLACE) 
                                    $j('#eventPlace').val('');
                                event.desc = $j('#eventDesc').val();
                                event.place = $j('#eventPlace').val();
                                event.start.setHours(newStart[0]);
                                event.start.setMinutes(newStart[1]);
                                event.end.setHours(newEnd[0]);
                                event.end.setMinutes(newEnd[1]);
                                
                                var calId = $j('#calSelect').val();
                                for (i in event.resources) {
                                    if (event.resources[i].cn == __currentUser.cn) {
                                        //alert('zmieniamy idCal na ' + calId);
                                        event.resources[i].calendarId = parseInt(calId, 10);
                                        event.source = pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + calId;
                                        break;
                                    }
                                }
    							
    							if($j('#periodSwitch').attr('checked') == 'checked') {
    								event.periodic = true;
    								event.periodLength = $j('#periodLength').val();
    								event.periodKind = $j('#periodKind').val();
    								event.periodEnd = Date.prototype.fromString($j('#periodTo').val(), 'dd-mm-yyyy');
    							} else {
    								event.periodic = false;
    							}
                                
                                createEvent(event);
                                //$j('#calendar').fullCalendar('updateEvent', calEvent);
                            }
                        }
                    }
                ],
                close: function(event, ui){
                    // usuwamy tymczasowy event
                    $j('#calendar').fullCalendar('removeEvents', -1);
                    monitorTimer = true;
                    multiEventUpdater();
                }
            });
        },
        
        loading: function(bool){
            if (bool) {
				//trace('loading start');
                $j('#loading').css('visibility', 'visible');
			}
            else {
				//trace('loading end');
                updateProgress(progressItemSize / calsList.length);
                $j('#loading').css('visibility', 'hidden');
				
				/* Pomijamy nieaktywne kalendarze */
				while(++__currentCalIndex < calsList.length) {
					var curCal = calsList[__currentCalIndex];
					
					if(!curCal.disable && __currentCalIndex != __firstEnabledCalIndex) {
						addCalendar(curCal.id);
						break;
					} else if(curCal.disable) {
						$j('#cal_' + curCal.id).addClass('hide');
						curCal.visible = false;
					}
				
				}
				
				
                if(__currentCalIndex >= calsList.length) {
                    updateProgress(progressItemSize);
                    //setTimeout('jqProgressBar.slideUp(\'slow\')', 2000);
                    $j('#eventInfo').dialog('destroy');
                    var newHeight = $j('#leftCell').next().height() - 120;
                    //$j('#leftCell').next().css('height', newHeight + 'px');
                    $j('#bigTable').css('height', '400px');
					
					// Pokazujemy okienko eventu, je�li zosta� podany w urlu
					if(urlParams.eventId) {
						var allEvents = $j('#calendar').fullCalendar('clientEvents');
						var i = 0;
						var fcEvent = null;
						for(i = 0;i < allEvents.length; i ++) {
							if(allEvents[i].id == urlParams.eventId) {
								fcEvent = allEvents[i];
								break;
							}
						}
						
						if(fcEvent != null) {
							var jqEventInfoDialog = showEventInfo(fcEvent);
							if(urlParams.accept === true) {
								$j('button.accept').trigger('click');
							} else if(urlParams.reject === true) {
								$j('button.reject').trigger('click');
							}
						}
						else 
							ajaxInfo('FAILED: Event #' + urlParams.eventId + ' not found');
					}
					
					// dostosowanie wysoko�ci
					var calHeight = $j('td.mai2').height() - 120;
					if(calHeight > 1050)
						calHeight = 1050;
					//$j('#calendar').fullCalendar('option', 'contentHeight', calHeight);
					
					// wyliczamy topScroll dla godziny 7:00
					topScroll = $j('div.fc-agenda-body div table tr:first').outerHeight() * 14;
					addPrintButton();
                }
            }
        }
    });
    
    if(urlParams.embedded === true) {
    	$j('.fc-header').hide();
    }
}

function initCalendarStuff(){
    createColorTable();
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        getCalendars: true
    }, function(data){
        data = safeEval(data);
        //trace('Calendars list loaded!');
        calsList = new Array();
        idCalToIndexList = new Object();
        lastRefreshList = new Array();
        
        jqCalsList.html('');
        var cssIndex = 0;
        var i = 0;
        for (i in data) {
            var curCal = data[i];
            curCal.label = curCal.name;
            curCal.visible = curCal.disable ? false : true;
            curCal.poll = false;
            calsList.push(curCal);
            lastRefreshList.push(new Date());
            idCalToIndexList['cal_' + curCal.id] = i;
            //trace('curCal.id = ' + curCal.id);
            
            if (editableCal == null && curCal.permissionType == PERMISSION_EDIT) 
                editableCal = curCal;
            
            var className = curCal.colorId;
			if(curCal.disable) {
				className += ' hide';
				
			} else if(__firstEnabledCalIndex == null) {
				__firstEnabledCalIndex = parseInt(i, 10);
			}
            var str = '<div style="color: black;" id="cal_' + curCal.id + '" class="calItem cal-' + className + '">';
            str += '<span class="calName">' + curCal.name + '</span>';
            if (DEBUG_MODE) {
                str += 'x' + curCal.id;
                str += ' (' + curCal.poll + ')';
            }
            
            str += '<img title="' + MSG_CHANGE_CAL_COLOR + '" id="colorTrigger_' + curCal.id + '" class="selectColor" src="' + MINI_CAL_COLOR_IMG + '" />';
            if (curCal.permissionType != PERMISSION_EDIT) 
                str += '<img class="locked" src="' + MINI_LOCK_IMG + '" />';
            
			str += '<span class="clearBoth" style="display: block;"></span>';
            str += '</div>';
            
            jqCalsList.append(str);
            
            /*$j('#cal_' + curCal.id).toggle(function(){
                var calId = this.id.replace('cal_', '');
                $j(this).addClass('hide');
                setTimeout('removeCalendar(' + calId + ')', 0);
				setCalendarVisibility(calId, false);
            }, function(){
                $j(this).removeClass('hide');
				var calId = this.id.replace('cal_', '');
                addCalendar(calId);
				setCalendarVisibility(calId, true);
            });*/
			
			$j('#cal_' + curCal.id).click(function(event){
                var calId = this.id.replace('cal_', '');
				var cal = calsList[idCalToIndexList['cal_' + calId]];
				if(!cal.visible) {
					$j(this).removeClass('hide');
					addCalendar(calId);
					setCalendarVisibility(calId, true);	
				} else {
					$j(this).addClass('hide');
					setTimeout('removeCalendar(' + calId + ')', 0);
					setCalendarVisibility(calId, false);	
				}
				
            });
            
            $j('#colorTrigger_' + curCal.id).click(function(evt){
				//evt.stopImmediatePropagation();
                if (jqColorTable.css('display') == 'none') {
                    $j(this).addClass('selectColorActive');
                    $j(this).parent().addClass('selectColorActive');
                    var yPos = getPos($j(this).parent()[0]).top + 26;
                    showColorTable(this.id.replace('colorTrigger_', ''), 33, yPos);
                }
                else 
                    hideColorTable();
                
                return false;
            });
            
            cssIndex++;
        }
        
        //trace(calsList);
        
        //updateProgress(progressItemSize);
		__firstEnabledCalIndex = __firstEnabledCalIndex == null ? 0 : __firstEnabledCalIndex;
        loadUsersAndResources();
        
        /*
         for(i = 1; i < calsList.length; i ++) {
         var curCal = calsList[i];
         trace('loading calendar #' + curCal.id + ' ' + curCal.name);
         addCalendar(curCal.id);
         }
         */
    });
    
}

function loadUsersAndResources(){
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        getUsers: true
    }, function(data){
        data = safeEval('(' + data + ')');
        
        arrayAllUsers = new Array();
        mapAllUsers = new Object();
        __currentUser = cloneUser(data[0]);
        data.splice(0, 1);
        
        for (i in data) {
            data[i].label = data[i].partTwoOfName + ' ' + data[i].partOneOfName;
            data[i].text = data[i].label;
            arrayAllUsers.push(data[i]);
            mapAllUsers[data[i].cn] = data[i];
        }
        
        $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
            getResources: true
        }, function(data){
            data = safeEval(data);
            mapAllRes = data;
            for (i in mapAllRes) {
                mapAllRes[i].label = mapAllRes[i].partTwoOfName + ' ' + mapAllRes[i].partOneOfName;
            }
            //trace('initializing fullCalendar');
            //updateProgress(progressItemSize);
            initFullCalendar();
            multiEventUpdater();
        });
    });
}

function bindAutocomplete(jqObj, srcMap, newUserList, selectFun){
    jqObj.autocomplete(srcMap, {
        minChars: 0,
        max: srcMap.length,
        autoFill: false,
        formatItem: function(row, i, max, term){
            return row.label;
        }
    });
    
    jqObj.result(function(event, data, formatted){
        selectFun(data);
    });
}


function fillEventInfoDiv(jqEventInfo, calEvent, newUserList){
    jqEventInfo.attr('title', 'Edycja: ' + calEvent.desc);
    var calId = calsList[0].id;
    
	jqEventInfo.append('<div class="humanEventDate">' + calEvent.start.format('eventDate', true, 'pl') + '</div>');
    if (!calEvent.allDay) {
        jqEventInfo.append('<input class="calTime" id="eventStart" onkeyup="validateTime(this)" value="' + calEvent.start.format('HH:MM') + '" />' +
        '<span class="ndash">&ndash;</span>');
        jqEventInfo.append('<input class="calTime" id="eventEnd" onkeyup="validateTime(this)" value="' + calEvent.end.format('HH:MM') + '" />');
        
        $j('#eventStart').bind('blur.format', function(){
            if (!$j(this).hasClass('invalidTime')) {
                var formattedTime = extractTime($j(this).val());
                formattedTime[0] = formattedTime[0] < 10 ? '0' + formattedTime[0] : formattedTime[0];
                formattedTime[1] = formattedTime[1] < 10 ? '0' + formattedTime[1] : formattedTime[1];
                $j(this).val(formattedTime[0] + ':' + formattedTime[1]);
            }
        });
        $j('#eventEnd').bind('blur.format', function(){
            if (!$j(this).hasClass('invalidTime')) {
                var formattedTime = extractTime($j(this).val());
                formattedTime[0] = formattedTime[0] < 10 ? '0' + formattedTime[0] : formattedTime[0];
                formattedTime[1] = formattedTime[1] < 10 ? '0' + formattedTime[1] : formattedTime[1];
                $j(this).val(formattedTime[0] + ':' + formattedTime[1]);
            }
        });
        
    }
    else {
        jqEventInfo.append('<span class="allDayTitle">Ca�y dzie�</span>');
    }
    
    jqEventInfo.append('<label class="owner">' + TEXT_OWNER + ':&nbsp;</label>' +
    '<span class="owner">' +
    calEvent.owner.partOneOfName +
    '&nbsp;' +
    calEvent.owner.partTwoOfName +
    '</span>');
	
    /* select z wyborem kalendarza */
    if (calEvent.permissionType == PERMISSION_EDIT) {
        var str = '<select id="calSelect" class="sel calSelect">';
        for (i = 0; i < calsList.length; i++) {
            if (calsList[i].type == USER_CALENDAR && calsList[i].permissionType == PERMISSION_EDIT) 
                str += '<option value="' + calsList[i].id + '" >' + calsList[i].label + '</option>';
        }
        str += '</select>';
        jqEventInfo.append(str);
        //trace(calEvent.calendarId);
        $j('#calSelect').val(calEvent.calendarId + '');
    }
    else {
        jqEventInfo.append('<span class="calInfo">' + calsList[idCalToIndexList['cal_' + calEvent.calendarId]].label +
        '</span><label class="calInfo">kalendarz:&nbsp;</label>');
    }
	
	/* Formatka wydarze� cyklicznych */
	if(calEvent.permissionType == PERMISSION_EDIT) {
		(function() {
			if(!calEvent.periodic) {
				calEvent.periodEnd = new Date(calEvent.end);
			}
			/* czeckbox w��czaj�cy wydarzenie cykliczne */
			var str = '<div id="periodSwitchContainer"><input type="checkbox" id="periodSwitch"/><label for="periodSwitch">Wydarzenie cykliczne</label></div>';
			jqEventInfo.children('select.calSelect').after(str);
			$j('#periodSwitch').change(function() {
				if($j(this).attr('checked') == 'checked') {
					$j('div.period').animate({'height': 30, 'opacity': 1.0}, 'fast');
				} else {
					$j('div.period').animate({'height': 0, 'opacity': 0}, 'fast');
				}
			});
			
			str = '<div class="period">Powtarzaj co';
			
			str += '<input id="periodLength" type="text" class="calTime" value="1"/>';
			
			str += '<select id="periodKind">';
			str += '<option value="0">' + periodCases[0][0] + '</option>';
			str += '<option value="1">' + periodCases[1][0] + '</option>';
			str += '<option value="2">' + periodCases[2][0] + '</option>';
			str += '<option value="3">' + periodCases[3][0] + '</option>';
			str += '</select>';
			str += '&nbsp;&nbsp;do&nbsp;<input id="periodTo" type="text" class="calTime calDate" value="' + calEvent.periodEnd.format('dd-mm-yyyy') + '" />';
			
			str += '</div>';
			jqEventInfo.append(str);
			
			$j('#periodLength').bind('mousewheel', function(event, delta) {
				if(delta > 0)
					$j(this).val(parseInt($j(this).val()) + 1);
				else if($j(this).val() > 1)
					$j(this).val(parseInt($j(this).val()) - 1);
				$j(this).trigger('matchPeriodCase');
				return false;
			});
			$j('#periodTo').datepicker({
				dateFormat: 'dd-mm-yy',
				duration: 0
			});
			
			jqEventInfo.children('div.period').children('#periodLength').data('jqPeriodKind', jqEventInfo.children('div.period').children('#periodKind'))
			.bind('matchPeriodCase', function() {
				if ($j(this).val()) {
					var val = parseInt($j(this).val(), 10);
					var jqPeriodKind = $j(this).data('jqPeriodKind');
					var i = 0;
					if(val == 1) {
						jqPeriodKind.children('option').each(function() {
							$j(this).html(periodCases[i][0]);
							i ++;
						});
					} else if(val >= 2 && val < 5) {
						jqPeriodKind.children('option').each(function() {
							$j(this).html(periodCases[i][1]);
							i ++;
						});
					} else if(val >= 5) {
						jqPeriodKind.children('option').each(function() {
							$j(this).html(periodCases[i][2]);
							i ++;
						});
					}
				}
			}).bind('keyup.matchPeriodCase', function() {
				$j(this).trigger('matchPeriodCase');
			});
		/* Je�li jest wydarzenie cykliczne, to ustawiamy pola */
		if(calEvent.periodic) {
			$j('#periodSwitch').attr('checked', true);
			$j('#periodLength').val(calEvent.periodLength).trigger('keyup');
			$j('#periodKind').val(calEvent.periodKind);
			$j('div.period').height('30');
		}
		})();
	} else if(calEvent.permissionType != PERMISSION_EDIT && calEvent.periodic == true) {
		/* wy�wietlamy tylko informacj� o wydarzeniu cyklicznym */
		(function() {
			var str = '<div class="periodReadOnly">Wydarzenie powtarzane co <span>';
				str += calEvent.periodLength + ' ';
				str += (function(len, kind) {
						var pCase = 0;
						
						if(len >= 2 && len < 5) pCase = 1;
						else if(len >= 5) pCase = 2;
						
						return periodCases[kind][pCase];
						})(calEvent.periodLength, calEvent.periodKind) + '</span> ';
				str += '<br/>od ' + calEvent.start.format('dd-mm-yyyy');
				str += ' do ' + calEvent.end.format('dd-mm-yyyy');
				
				str += '</span></div>';
			jqEventInfo.append(str);
		})();
	}

    jqEventInfo.append('<label class="cal" for="eventDesc" style="width: 90%;">Opis</label><textarea class="cal" id="eventDesc">' + calEvent.desc + '</textarea>');
    jqEventInfo.append('<label class="cal" for="eventPlace" style="width: 90%;">Miejsce spotkania na zewn�trz</label><textarea class="cal" id="eventPlace">' + calEvent.place + '</textarea>');
    
    if (calEvent.permissionType != PERMISSION_EDIT) {
        $j('#eventInfo input, #eventInfo textarea').attr('readonly', true);
        $j('#eventEnd').after('<img class="lockTime" src="' + MINI_LOCK_IMG + '" />');
    }
    else {
        if (calEvent.desc == '') 
            $j('#eventDesc').addClass('empty').val(MSG_ADD_DESC);
        $j('#eventDesc').bind('focus.clear', function(){
            if ($j(this).hasClass('empty')) {
                $j(this).removeClass('empty').val('');
            }
        }).bind('blur.clear', function(){
            if ($j(this).val() == '' || $j(this).val() == MSG_ADD_DESC) {
                $j(this).addClass('empty').val(MSG_ADD_DESC);
            }
        });
        
        if (calEvent.place == '') 
            $j('#eventPlace').addClass('empty').val(MSG_ADD_PLACE);
        $j('#eventPlace').bind('focus.clear', function(){
            if ($j(this).hasClass('empty')) {
                $j(this).removeClass('empty').val('');
            }
        }).bind('blur.clear', function(){
            if ($j(this).val() == '' || $j(this).val() == MSG_ADD_PLACE) {
                $j(this).addClass('empty').val(MSG_ADD_PLACE);
            }
        });
    }
    
    /* powod odrzucenia */
    if (calEvent.reasons && calEvent.reasons.length) {
        var str = '<label class="cal">Odrzucone zaproszenia</label><div id="rejectionReasons">';
        var i;
        
        for (i in calEvent.reasons) {
            with (calEvent.reasons[i]) {
                if (reason == undefined || reason == '') 
                    continue;
                
                str += '<div class="reasonContainer" >';
                str += '<span class="usr">' + mapAllUsers[username].label + '</span>';
                str += '<span class="reason">' + reason + '</span>';
                str += '</div>';
            }
        }
        str += '</div>';
        jqEventInfo.append(str);
    }
    
    /* u�ytkownicy */
    jqEventInfo.append('<label class="cal user">U�ytkownicy<span class="usrInfo"></label><label class="cal res">Zasoby<span class="resInfo"></span></label>');
    jqEventInfo.append('<span class="userList"></span><span class="resList"></span>');
    
    var jqEventInfoUsers = jqEventInfo.children('span.userList');
    for (i = 0; i < calEvent.resources.length; i++) {
        var confirmStr = calEvent.resources[i].confirm ? '' : '?';
        
        if (calEvent.resources[i].type != DSUSER_RESOURCE_TYPE) 
            continue;
        jqEventInfoUsers.append('<span class="user" reason="' + calEvent.resources[i].confirmDesc + '" title="Usu�" id="' + calEvent.resources[i].cn + '">' +
        confirmStr +
        calEvent.resources[i].partTwoOfName +
        ' ' +
        calEvent.resources[i].partOneOfName +
        '</span>');
        
        /* ustawiamy select z kalendarzem */
        /*if(calEvent.resources[i].cn == __currentUser.cn) {
         calId = calEvent.resources[i].calendarId;
         $j('#calSelect option[value=' + calId + ']').attr('selected', true);
         }*/
        if (calEvent.permissionType == PERMISSION_EDIT) {
            $j('span.user:last').bind('click', function(){
                if (removeUserFromList(this.id, newUserList)) 
                    $j(this).remove();
            });
        }
    }
    
    if (calEvent.permissionType == PERMISSION_EDIT) {
        jqEventInfoUsers.append('<input id="dialogUserSelect" class="cal addUser empty" value="' + MSG_ADD_USER + '" />');
        bindAutocomplete($j('#dialogUserSelect'), arrayAllUsers, newUserList, function(usr){
            var addUser = true;
            
            for (k in newUserList) {
                if (newUserList[k].cn == usr.cn) {
                    addUser = false;
                    break;
                }
            }
            
            if (addUser) {
                var startTime = extractTime($j('#eventStart').val());
                var endTime = extractTime($j('#eventEnd').val());
                
                if (calEvent.allDay) {
                    startTime = new Array(0, 0);
                    endTime = new Array(23, 59);
                }
                
                //trace("ADDed user");
                newUserList.push(usr);
                
                if (startTime != null && endTime != null) {
                    var startDate = new Date(calEvent.start);
                    var endDate = new Date(calEvent.end);
                    var fakeUsrArray = new Array();
                    fakeUsrArray.push(usr.cn);
                    
                    startDate.setHours(startTime[0]);
                    startDate.setMinutes(startTime[1]);
                    startDate.setMilliseconds(0);
                    endDate.setHours(endTime[0]);
                    endDate.setMinutes(endTime[1]);
                    endDate.setMilliseconds(0);
                    
                    checkUserAvailability(startDate.toUnixStamp(), endDate.toUnixStamp(), calEvent.id, fakeUsrArray, function(data){
                        var data = safeEval(data);
                        var item = data[0]; // sprawdzamy tylko jednego
                        for (item in data) {
                            if (data[item] == USER_BUSY) {
                                $j('span.usrInfo').addClass('resError').html(usr.partTwoOfName + ' zaj�ty');
                                //$j('#' + res.cn).trigger('click'); nie usuwamy
                            }
                            else {
                                $j('span.usrInfo').removeClass('resError').html(usr.partTwoOfName + ' wolny');
                            }
                        }
                    });
                }
                
                var str = '<span class="user" title="Usu�" id="' + usr.cn + '">?' + $j('#dialogUserSelect').val() + '</span>';
                if ($j('span.user:last').length > 0) 
                    $j('span.user:last').after(str);
                else 
                    $j('#dialogUserSelect').before(str);
                
                $j('span.user:last').bind('click', function(){
                    //removeUserFromList(this.id, newUserList);
                    if (removeUserFromList(this.id, newUserList)) {
                        //trace('Removing span ' + this.id);
                        $j(this).remove();
                    }
                });
            }
            $j('#dialogUserSelect').val('');
            //$j('#dialogUserSelect').trigger('dblclick');
        });
        
        $j('#dialogUserSelect').bind('focus.clear', function(){
            if ($j(this).hasClass('empty')) {
                $j(this).removeClass('empty').val('');
                $j(this).trigger('click');
            }
        }).bind('blur.clear', function(){
            if ($j(this).val() == '' || $j(this).val() == MSG_ADD_USER) {
                $j(this).addClass('empty').val(MSG_ADD_USER);
            }
        });
        
        {
            $j('#eventInfo textarea, #eventInfo input, #dialogUserSelect').bind('focus.changeClass', function(){
                $j(this).addClass('focusIE');
            }).bind('blur.changeClass', function(){
                $j(this).removeClass('focusIE');
            });
        }
    }
    
    /* zasoby */
    var jqEventInfoRes = jqEventInfo.children('span.resList');
    var busyResCount = 0;
    
    for (i in calEvent.listResOnly) {
        var confirmStr = calEvent.listResOnly[i].confirm ? '' : '?';
        
        
            jqEventInfoRes.append('<span class="res"' + (calEvent.permissionType == PERMISSION_EDIT ? ' title="Usu�"' : '') 
			+ ' id="' + calEvent.listResOnly[i].cn + '">' +
            confirmStr +
            calEvent.listResOnly[i].partTwoOfName +
            ' ' +
            calEvent.listResOnly[i].partOneOfName +
            '</span>');
        if (calEvent.permissionType == PERMISSION_EDIT) {    
            $j('span.res:last').bind('click', function(){
                if (removeUserFromList(this.id, newUserList)) 
                    $j(this).remove();
                //trace(newUserList);
            });
        }
    }
    
    function updateResAvailability(res, options){
        var addRes = true, i = 0,
        	opts = options || {};
    	var multipleResources = res.length > 1,
    		firstRes = res[0];
        	
        opts.checkExisting = opts.checkExisting || false;
        
        for (k in newUserList) {
            if (newUserList[k].cn == res.cn) {
                addRes = false;
                break;
            }
        }
        
        if (addRes || opts.checkExisting) {
            var startTime = extractTime($j('#eventStart').val());
            var endTime = extractTime($j('#eventEnd').val());
            
            if (calEvent.allDay) {
                startTime = new Array(0, 0);
                endTime = new Array(23, 59);
            }
            
            if (startTime != null && endTime != null) {
                var startDate = new Date(calEvent.start);
                var endDate = new Date(calEvent.end);
                
                startDate.setHours(startTime[0]);
                startDate.setMinutes(startTime[1]);
                startDate.setMilliseconds(0);
                endDate.setHours(endTime[0]);
                endDate.setMinutes(endTime[1]);
                endDate.setMilliseconds(0);
                var fakeResArray = new Array();
            	for(i = 0; i < res.length; i ++) {
            		fakeResArray.push(res[i].calendarId);
            	}
                
				//jqEventInfo.nextAll('.ui-dialog-buttonpane').children('button:eq(1)').attr('disabled', 'disabled');
                $j('[cn=dlg-save]').attr('disabled', 'disabled');
				$j('span.resInfo').addClass('resError').html('czekaj...');
                checkResourceAvailability(startDate.toUnixStamp(), endDate.toUnixStamp(), calEvent.id, fakeResArray, function(data){
                    var data = safeEval(data);
                    var item = null, i = 0, freeLabel = ' wolny', busyLabel = ' zaj�ty',
                    	freeRes = [], busyRes = [], labels = [];
                    
                    for (item in data) {
                        if (data[item] == RESOURCE_BUSY) {
                        	for(i = 0; i < res.length; i ++) {
                        		if(res[i].calendarId == item) {
                        			busyRes.push(res[i]);
                        			break;
                        		}
                        	}
                        }
                        else {
                        	for(i = 0; i < res.length; i ++) {
                        		if(res[i].calendarId == item) {
                        			freeRes.push(res[i]);
                        			break;
                        		}
                        	}
                        }
                    }
                    
                    // je�li wszystkie elementy s� wolne to wy�wietlamy etykiet� dost�pnych elementow
                    busyResCount = busyRes.length;
                    if(busyRes.length == 0) {
                    	for(i = 0; i < freeRes.length; i ++) {
                    		labels.push(freeRes[i].label);
                    		$j('#' + freeRes[i].cn).removeClass('resBusy');
                    	}
                    	if(freeRes.length > 1) {
                    		freeLabel = ' wolne';
                    	}
                    	$j('span.resInfo').removeClass('resError').html(labels.join(', ') + freeLabel);
                    } else {
                    	// je�li kt�rykolwiek element jest zaj�ty, to wy�wietlamy info
                    	// tylko o zaj�tych zasobach
                    	for(i = 0; i < busyRes.length; i ++) {
                    		labels.push(busyRes[i].label);
                    		$j('#' + busyRes[i].cn).addClass('resBusy');
                    	}
                    	if(busyRes.length > 1) {
                    		busyLabel = ' zaj�te';
                    	}
                    	$j('span.resInfo').addClass('resError').html(labels.join(', ') + busyLabel);
                    	
                    	if(opts.checkExisting == false) {
                    		$j('#' + firstRes.cn).trigger('click');
                    	}
                    }
                    
                    if(busyResCount == 0) {
                    	$j('[cn=dlg-save]').removeAttr('disabled');
                    }
                });
            }
            
            if(opts.checkExisting == false) {
            	newUserList.push(firstRes);
	            var str = '<span class="res" title="Usu�" id="' + firstRes.cn + '">' + $j('#dialogResSelect').val() + '</span>';
	            if ($j('span.res:last').length > 0) 
	                $j('span.res:last').after(str);
	            else 
	                $j('#dialogResSelect').before(str);
	            
	            for (i in newUserList) {
	                if (newUserList[i].cn == firstRes.cn) 
	                    newUserList[i].confirm = true;
	            }
            
	            $j('span.res:last').bind('click', function(){
	                //removeUserFromList(this.id, newUserList);
	                if (removeUserFromList(this.id, newUserList)) {
	                    //trace('Removing span ' + this.id);
	                    $j(this).remove();
	                    busyResCount --;
	                    if(busyResCount < 0)
	                    	busyResCount = 0;
	                    if(busyResCount == 0) {
	                    	$j('[cn=dlg-save]').removeAttr('disabled');
	                    }
	                }
	            });
            }
        }
        $j('#dialogResSelect').val('');
    }
    
    if (calEvent.permissionType == PERMISSION_EDIT) {
        jqEventInfoRes.append('<input id="dialogResSelect" class="cal addRes empty" value="' + MSG_ADD_RES + '" placeholder="' + MSG_ADD_RES + '"/>');
        bindAutocomplete($j('#dialogResSelect'), mapAllRes, newUserList, function(res) {
        	updateResAvailability([res]);
        });
        $j('#dialogResSelect').bind('focus.clear', function(){
            if ($j(this).hasClass('empty')) {
                $j(this).removeClass('empty').val('');
                $j(this).trigger('click');
            }
        }).bind('blur.clear', function(){
            if ($j(this).val() == '' || $j(this).val() == MSG_ADD_RES) {
                $j(this).addClass('empty').val(MSG_ADD_RES);
            }
        });
        $j('#eventEnd, #eventStart').bind('blur.checkres', function() {
        	var resList = [], i = 0;
        	
        	for(i = 0; i < calEvent.resources.length; i ++) {
        		if(calEvent.resources[i].type == 'ANY') {
        			resList.push(calEvent.resources[i]);
        		}
        	}
        	
        	if($j(this).hasClass('invalidTime') == false) {
        		if(resList.length) {
        			updateResAvailability(resList, {checkExisting: true});
        		}
        	}
        });
    }
    else {
        $j('span.user, span.res').addClass('readonly');
    }
}

function showStatusReasonDialog(calEvent){
    /* pow�d odrzucenia */
    jqEventStatusReason.html('').dialog('destroy');
    jqEventStatusReason.attr('title', 'Odrzu� zaproszenie.');
    jqEventStatusReason.append('<textarea class="cal empty" id="statusDesc">' + MSG_ADD_STATUS_REASON + '</textarea>');
    
    $j('#statusDesc').bind('focus.clear', function(){
        if ($j(this).hasClass('empty')) {
            $j(this).removeClass('empty').val('');
        }
    }).bind('blur.clear', function(){
        if ($j(this).val() == '' || $j(this).val() == MSG_ADD_STATUS_REASON) {
            $j(this).addClass('empty').val(MSG_ADD_STATUS_REASON);
        }
    });
    jqEventStatusReason.dialog({
        modal: true,
        buttons: {
            'Anuluj': function(){
                $j(this).dialog('close');
            },
            'Odrzu�': function(){
                $j(this).dialog('close');
                if ($j('#statusDesc').hasClass('empty')) 
                    $j('#statusDesc').val(MSG_NO_STATUS_REASON);
                
                rejectEvent(calEvent.id, $j('#statusDesc').val(), calEvent.resources, function(){
                    $j('span.accepted, span.rejected').remove();
                    jqEventInfo.nextAll('.ui-dialog-buttonpane').children('.reject').before('<span class="rejected">' + MSG_EVENT_REJECTED + '</span>').hide();
                    jqEventInfo.nextAll('.ui-dialog-buttonpane').children('.accept').fadeIn();
                    $j('span.rejected').fadeIn();
                    /* szukamy spana  z naszym cn i dodajemy zapytnik */
                    $j('span.user#' + __currentUser.cn).text('?' + $j('span.user#' + __currentUser.cn).text());
                });
            }
        }
    });
}

function showPeriodModeSelection(calEvent, periodicSingleCall, periodicFutureCall, periodicCancelCall) {
	// przywracamy poprzednie wydarzenie (na kt�rym u�ytkownik nie wybra� �adnej opcji
	if($j('#periodModeCancel').data('event-id') != null && $j('#periodModeCancel').data('event-id') != calEvent.id) {
		$j('#periodModeCancel').trigger('click');
	}
	var jqDlg = $j('#periodModeSelection'),
		jqEvent = $j('.fc-event-id-' + calEvent.id);
	jqDlg.removeClass('period-mode-selection-left');
	var dim = {
		left: jqEvent.offset().left + jqEvent.width() - 10,
		top: jqEvent.offset().top + Math.floor(jqEvent.outerHeight() / 2) - Math.floor(jqDlg.outerHeight() /2)
	};
	var availWidth = $j('#calendar').offset().left + $j('#calendar').width();
	if(dim.left + jqDlg.width() > availWidth) {
		dim.left -= jqDlg.width() + $j('#calendar').offset().left + 20;
		jqDlg.addClass('period-mode-selection-left');
	}
	
	jqDlg.find('button').blur();
	jqDlg.css({left: dim.left, top: dim.top}).removeClass('period-mode-selection-hidden');
	
	$j('#periodModeSingle').unbind('click').bind('click', 
		function(evt) {
			periodicSingleCall(calEvent);
			hide();
			
			evt.preventDefault();
			return false;
		});
	
	$j('#periodModeFuture').unbind('click').bind('click', 
			function(evt) {
				periodicFutureCall(calEvent);
				hide();
				
				evt.preventDefault();
				return false;
			});
	
	$j('#periodModeCancel').unbind('click').bind('click', 
			function(evt) {
				periodicCancelCall(calEvent);
				hide();
				
				evt.preventDefault();
				return false;
			}).data('event-id', calEvent.id);
	
	function hide() {
		jqDlg.css({left: '', top: ''}).addClass('period-mode-selection-hidden');
	}
}

function showDeleteReasonDialog(calEvent, options){
	var opts = {
		omitPeriodicSelection: options ? options.omitPeriodicSelection === true : false,
		periodicDetached: options ? options.periodicDetached === true : false,
		periodicFuture: options ? options.periodicFuture === true : false,
		omitPeriodicChecking: options ? options.omitPeriodicChecking === true : false
	};
	
	if(calEvent.periodic === true) {
		if(opts.omitPeriodicSelection === false) {
			showPeriodModeSelection(calEvent, reasonSingle, reasonFuture, reasonCancel);
			return false;
		} else if(opts.omitPeriodicChecking === false) {
			if(opts.periodicDetached === true) {
				commitReason(calEvent);
				return;
			} else if(opts.periodicFuture === true) {
				commitReason(calEvent);
				return;
			}
		}
	} 
	commitReason();
    
    function commitReason() {
    	 /* pow�d odrzucenia */
        jqEventStatusReason.html('').dialog('destroy');
        jqEventStatusReason.attr('title', 'Odrzu� zaproszenie.');
        jqEventStatusReason.append('<textarea class="cal empty" id="statusDesc">' + MSG_ADD_DELETE_REASON + '</textarea>');
        
        $j('#statusDesc').bind('focus.clear', function(){
            if ($j(this).hasClass('empty')) {
                $j(this).removeClass('empty').val('');
            }
        }).bind('blur.clear', function(){
            if ($j(this).val() == '' || $j(this).val() == MSG_ADD_DELETE_REASON) {
                $j(this).addClass('empty').val(MSG_ADD_DELETE_REASON);
            }
        });
        
	    jqEventStatusReason.dialog({
	        modal: true,
	        buttons: {
	            'Anuluj': function(){
	                $j(this).dialog('close');
	            },
	            'Odrzu�': function(){
	                $j(this).dialog('close');
	                if ($j('#statusDesc').hasClass('empty')) 
	                    $j('#statusDesc').val(MSG_NO_STATUS_REASON);
	                
	                removeEvent(calEvent.id, $j('#statusDesc').val(), {
	                	periodicDetached: opts.periodicDetached,
	                	omitPeriodicSelection: true
	                });
	                jqEventInfo.dialog('close').dialog('destroy');
	            }
	        }
	    });
    }
    
    function reasonSingle(evt) {
    	showDeleteReasonDialog(evt, {
			periodicDetached: true, omitPeriodicSelection: true,
			omitPeriodicChecking: true			
		});
	}
	
	function reasonFuture(evt) {
		showDeleteReasonDialog(evt, {
			periodicDetached: false, omitPeriodicSelection: true,
			omitPeriodicChecking: true			
		});
		
		/*confirmEvent(evt.id, newUserList, callFun, {
			omitPeriodicSelection: true,
			omitPeriodicChecking: true			
		});
		var evts = getFutureEvents(evt.periodParentId, evt.start),
			i = 0;
		for(i = 0; i < evts.length; i ++) {
			$j('#confirmEvent_' + evts[i].id).remove();
		}*/
	}
	
	function reasonCancel(evt) {
		// nop
	}
}

function removeUserFromList(cn, userList){
    //trace('userList length = ' + userList.length + ', ' + userList);
    if (userList.length > 1) {
        //trace('Removing user ' + cn);
        for (i in userList) {
            if (userList[i].cn == cn) {
                //trace('User ' + cn + ' removed');
                userList.splice(i, 1);
                return true;
            }
        }
    }
    
    return false;
}

function confirmEvent(eventId, newUserList, callFun, options){
	var opts = {
		omitPeriodicSelection: options ? options.omitPeriodicSelection === true : false,
		periodicDetached: options ? options.periodicDetached === true : false,
		periodicFuture: options ? options.periodicFuture === true : false,
		omitPeriodicChecking: options ? options.omitPeriodicChecking === true : false
	};
	var evt = $j('#calendar').fullCalendar('clientEvents', eventId)[0];
	
	if(evt.periodic === true) {
		if(opts.omitPeriodicSelection === false) {
			showPeriodModeSelection(evt, confirmPeriodicSingle, confirmPeriodicFuture, cancelPeriodic);
			return false;
		} else if(opts.omitPeriodicChecking === false) {
			if(opts.periodicDetached === true) {
				confirmPeriodicSingle(evt);
				return;
			} else if(opts.periodicFuture === true) {
				confirmPeriodicFuture(evt);
				return;
			}
		}
	} 
	
	commitConfirm();
	
	function commitConfirm() {
	    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
	        changeEventStatus: true,
	        eventId: eventId,
	        periodicDetached: opts.periodicDetached,
	        eventStatus: true
	    }, function(data){
	        ajaxInfo(data);
	        if (data == 'EVENT_STATUS_CHANGED') 
	            callFun();
	        
	        if (newUserList != null && newUserList != 'undefined') {
	            for (i in newUserList) {
	                if (newUserList[i].cn == __currentUser.cn) {
	                    newUserList[i].confirm = true;
	                    break;
	                }
	            }
	        }
	    });
	}
	
	function confirmPeriodicSingle(evt) {
		confirmEvent(evt.id, newUserList, callFun, {
			periodicDetached: true, omitPeriodicSelection: true,
			omitPeriodicChecking: true			
		});
		$j('#confirmEvent_' + evt.id).remove();
	}
	
	function confirmPeriodicFuture(evt) {
		confirmEvent(evt.id, newUserList, callFun, {
			omitPeriodicSelection: true,
			omitPeriodicChecking: true			
		});
		var evts = getFutureEvents(evt.periodParentId, evt.start),
			i = 0;
		for(i = 0; i < evts.length; i ++) {
			$j('#confirmEvent_' + evts[i].id).remove();
		}
	}
	
	function cancelPeriodic(evt) {
		// nop
	}
}

function rejectEvent(eventId, desc, newUserList, callFun){
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        changeEventStatus: true,
        eventId: eventId,
        eventStatusDesc: desc,
        eventStatus: false
    }, function(data){
        ajaxInfo(data);
        if (data == 'EVENT_STATUS_CHANGED') 
            callFun();
        
        if (newUserList != null && newUserList != 'undefined') {
            for (i in newUserList) {
                if (newUserList[i].cn == __currentUser.cn) {
                    newUserList[i].confirm = false;
                    break;
                }
            }
        }
    });
}

function updateEvent(event, dontCheck, options){
	var opts = {
		omitPeriodicSelection: options ? options.omitPeriodicSelection === true : false,
		periodicDetached: options ? options.periodicDetached === true : false,
		periodicFuture: options ? options.periodicFuture === true : false,
		omitPeriodicChecking: options ? options.omitPeriodicChecking === true : false
	};
	
	trace(event);
    monitorTimer = false;
    var cEvt = cloneEvent(event);
    cEvt.start = event.start.getTime() / 1000;
    cEvt.end = event.end.getTime() / 1000;
	if(event.periodEnd)
		cEvt.periodEnd = Math.floor(event.periodEnd.getTime() / 1000);
    
	if(event.periodic === true) {
		if(opts.omitPeriodicSelection === false) {
			showPeriodModeSelection(cEvt, updatePeriodicSingle, updatePeriodicFuture, cancelPeriodic);
			return;
		} else if(opts.omitPeriodicChecking === false) {
			if(opts.periodicDetached === true) {
				updatePeriodicSingle(cEvt);
				return;
			} else if(opts.periodicFuture === true) {
				updatePeriodicFuture(cEvt);
				return;
			}
		}
	}
    
    var resIds = new Array();
    var i = 0;
    for (i = 0; i < cEvt.resources.length; i++) {
        if (cEvt.resources[i].type == ANY_RESOURCE_TYPE) 
            resIds.push(event.resources[i].calendarId);
    }
    
    if (resIds.length > 0 && (dontCheck == undefined || dontCheck == false)) {
        checkResourceAvailability(cEvt.start, cEvt.end, cEvt.id, resIds, function(data){
            var data = safeEval(data);
            var busyRes = new Array();
            var item = null;
            
            for (item in data) {
                if (data[item] == RESOURCE_BUSY) {
                    var resLabel = null;
                    var j = 0;
                    for (j in mapAllRes) {
                        if (mapAllRes[j].calendarId == item) 
                            resLabel = mapAllRes[j].label.trim();
                    }
                    busyRes.push(resLabel);
                }
            }
            
            
            if (busyRes.length == 0) {
                commitUpdate(cEvt);
            }
            else {
                var busyStart = new Date(event.start);
                var busyEnd = new Date(event.end);
                
                event.start = new Date(event.oldStart);
                event.end = new Date(event.oldEnd);
                
                $j('#calendar').fullCalendar('updateEvent', event);
                
                var str = 'Zdarzenie zosta�o przywr�cone do poprzedniego terminu, <br/> gdy� ';
                str += 'od ' + busyStart.format('HH:MM') + ' do ' + busyEnd.format('HH:MM') + '<br />';
                str += busyRes.length > 1 ? MSG_MANY_RESOURCE_BUSY.res + ': <i>' + busyRes + '</i> ' + MSG_MANY_RESOURCE_BUSY.busy : MSG_ONE_RESOURCE_BUSY.res + ' <i>' + busyRes + '</i> ' + MSG_ONE_RESOURCE_BUSY.busy;
                str += '.';
                
                clearTimeout(ajaxTopInfoTimer);
                jqAjaxTopInfo.fadeOut('fast');
                jqAjaxTopInfo.html(str);
                jqAjaxTopInfo.slideDown('fast');
                ajaxTopInfoTimer = setTimeout('jqAjaxTopInfo.fadeOut("fast")', TOP_INFO_TIME);
                
                jqAjaxTopInfo.hover(function(){
                    clearTimeout(ajaxTopInfoTimer);
                }, function(){
                    ajaxTopInfoTimer = setTimeout('jqAjaxTopInfo.fadeOut("fast")', TOP_INFO_TIME);
                });
            }
        });
    }
    else {
        commitUpdate(cEvt);
    }
    
    function commitUpdate(cEvt) {
		var updateAllPeriodEvents = cEvt.periodParentId ? true : false,
			allEvents = $j('#calendar').fullCalendar('clientEvents'),
			i = 0;
		// usuwamy zale�ne zdarzenia cykliczne
		for(i = 0; i < allEvents.length; i ++) {
			if(allEvents[i].periodParentId == cEvt.id && allEvents[i].id != cEvt.id) {
				trace('3. removing event #' + allEvents[i].id);
                $j('#calendar').fullCalendar('removeEvents', allEvents[i].id);
			}
		}
		
        $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
            updateEvent: true,
            periodicDetached: opts.periodicDetached,
            eventJson: $j.toJSON(cEvt)
        }, function(data) {
            clearTimeout(ajaxTopInfoTimer);
            jqAjaxTopInfo.fadeOut('fast');
            
            //trace('updateEvent result: ' + data);
            ajaxInfo(data);
            monitorTimer = true;
            // pomijamy tylko dla zwyk�ych (nie cyklicznych) wydarze�
            if(cEvt.periodic !== true) {
            	__skipEventId = cEvt.id;
            	trace('we will skip event #' + cEvt.id);
            }
			if(cEvt.periodic /*&& opts.periodicDetached === false*/) {
				removeChildEvents(cEvt.periodParentId);
				$j('#calendar').fullCalendar('refetchEvents');
			}
        });
        
        var calEvent = $j('#calendar').fullCalendar('clientEvents', cEvt.id)[0];
        cEvt.orgStart = cEvt.orgEnd = calEvent.orgStart = calEvent.orgEnd = null;
        $j('#periodModeCancel').removeData('event-id');
    }
    
    function updatePeriodicSingle(evt) {
    	evt.start = new Date(evt.start * 1000);
    	evt.end = new Date(evt.end * 1000);
    	evt.periodEnd = new Date(evt.periodEnd * 1000);
    	//evt.periodic = false;
    	//evt.periodParentId = evt.periodKind = evt.periodLength = 0;
    	updateEvent(evt, undefined, {
    		periodicDetached: true, omitPeriodicSelection: true,
    		omitPeriodicChecking: true
    		}
    	);
    }
    
    function updatePeriodicFuture(evt) {
    	evt.start = new Date(evt.start * 1000);
    	evt.end = new Date(evt.end * 1000);
    	evt.periodEnd = new Date(evt.periodEnd * 1000);
    	updateEvent(evt, undefined, {
    		omitPeriodicSelection: true,
    		omitPeriodicChecking: true
    		}
    	);
    }
    
    function cancelPeriodic(evt) {
    	if(evt.orgStart && evt.orgEnd) {
    		var calEvent = $j('#calendar').fullCalendar('clientEvents', evt.id)[0];
    		calEvent.start = evt.orgStart;
    		calEvent.end = evt.orgEnd;
    		calEvent.orgStart = calEvent.orgEnd = null;
    		$j('#calendar').fullCalendar('updateEvent', calEvent);
    	}
    }
}

function createEvent(event){
    var cEvt = cloneEvent(event);
    cEvt.start = event.start.getTime() / 1000;
    cEvt.end = event.end.getTime() / 1000;
	if(cEvt.periodEnd) {
		cEvt.periodEnd = event.periodEnd.getTime() / 1000;
	}
    
    __creationSource = event.source;
    
    /* u�ytkownik, kt�ry stworzy� event zaakceptowa� swoje zaproszenie */
    for (i in cEvt.resources) {
        var res = cEvt.resources[i];
        //res.calendarId = 0;
        if (res.cn == __currentUser.cn) {
            //trace('Accepting confirm for user ' + __currentUser.cn);
            res.confirm = true;
            break;
        }
    }
//    alert($j.toJSON(cEvt));
    // r�cznie dodajemy do pami�ci
    //$j('#calendar').fullCalendar('clientEvents').push(event);
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        createEvent: true,
        eventJson: $j.toJSON(cEvt)
    }, function(data){
        data = safeEval('(' + data + ')');
        //$j('#calendar').fullCalendar('renderEvent', data, false);
		
		$j('#calendar').fullCalendar('refetchEvents'); // ca�y kalendarz
    });
}

function removeEvent(eventId, desc, options){
    var ev = $j('#calendar').fullCalendar('clientEvents', eventId)[0];
    var opts = {
		omitPeriodicSelection: options ? options.omitPeriodicSelection === true : false,
		periodicDetached: options ? options.periodicDetached === true : false,
		periodicFuture: options ? options.periodicFuture === true : false
	};
    
    if(opts.omitPeriodicSelection === false && ev.periodic === true) {
    	showPeriodModeSelection(ev, removePeriodicSingle, removePeriodicFuture, cancelPeriodic);
		return;
    } else {
    	$j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
            removeEvent: true,
            eventId: eventId,
            eventStatusDesc: desc,
            periodicDetached: opts.periodicDetached
        }, function(data){
            //data = eval('(' + data + ')');
            $j('#calendar').fullCalendar('removeEvents', eventId);
            if(ev.periodic === true && opts.periodicDetached === false) {
            	// usuwamy zdarzenia w przysz�o�ci
            	removeChildEvents(ev.periodParentId, {afterDate: ev.start});
            }
            ajaxInfo(data);
        });
    }
    
    function removePeriodicSingle(cEvt) {
    	removeEvent(cEvt.id, desc, 
    		{omitPeriodicSelection: true, periodicDetached: true});
    }
    
    function removePeriodicFuture(cEvt) {
    	removeEvent(cEvt.id, desc, 
        		{omitPeriodicSelection: true, periodicDetached: false});
    }
    
    function cancelPeriodic(cEvt) {
    	
    }
}

function multiEventUpdater(){
    //trace('multiEventUpdater');
    var i = 0;
    for (i = 0; i < calsList.length; i++) {
        var strCalId = calsList[i].id;
        if (!calsList[i].poll) {
            calsList[i].poll = true;
            $j('#cal_' + strCalId).css('color', 'white');
            //trace('MultiEventUpdater fired for cal #' + strCalId);
            setTimeout('eventUpdater(' + strCalId + ')', 300 * i);
        }
        //eventUpdater(calsList[i].id);
    }
}

function removeChildEvents(parentId, options) {
	var allEvents = $j('#calendar').fullCalendar('clientEvents');
	var i = 0, removeEvent = true;
	var opts = {
		afterDate: options ? options.afterDate : null
	}
	for(i = 0; i < allEvents.length; i ++) {
		if(allEvents[i].periodic === true && allEvents[i].periodParentId != allEvents[i].id
			&& allEvents[i].periodParentId == parentId) {
			removeEvent = true;
			if(opts.afterDate != null && allEvents[i].start < opts.afterDate) {
				removeEvent = false;
			}
			
			if(removeEvent) {
				$j('#calendar').fullCalendar('removeEvents', allEvents[i].id);
			}
		}
	}
}

function eventUpdater(calId){
    var uCalId = parseInt(calId, 10);
    var skip = false;
    
    if (calsList[idCalToIndexList['cal_' + calId]].visible && !skip) {
        $j.post(pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + calId, {
            getObsoleteEvents: true,
            lastRefresh: lastRefreshList[idCalToIndexList['cal_' + uCalId]].toUnixStamp()
        }, function(data){
            trace('Event Updater for ' + calsList[idCalToIndexList['cal_' + uCalId]].name);
            if (DEBUG_MODE) {
                $j('#debugObsolete').append('<b>' + new Date() + '</b><br />' + data + '<hr style="display: block; color: black;">');
                $j('#debugObsolete')[0].scrollTop = 1000000;
                var txt = $j('#cal_' + uCalId).html();
                txt = txt.replace(/\([a-z]*\)/i, '(' + calsList[idCalToIndexList['cal_' + uCalId]].poll + ')');
                $j('#cal_' + uCalId).html(txt);
            }
            if (!monitorTimer) {
                calsList[idCalToIndexList['cal_' + uCalId]].poll = false;
                $j('#cal_' + uCalId).css('color', 'black');
                var txt = $j('#cal_' + uCalId).html();
                txt = txt.replace(/\([a-z]*\)/i, '(' + calsList[idCalToIndexList['cal_' + uCalId]].poll + ')');
                $j('#cal_' + uCalId).html(txt);
                //trace('monitorTimer canceled');
                return;
            }
            
            var data = safeEval(data);
            if(DEBUG_MODE) {
            	try {
            		console.dir(data);
            	} catch(e) {}
            }
            
            lastRefreshList[idCalToIndexList['cal_' + uCalId]] = new Date(data.time * 1000);
            
            if (DEBUG_MODE) {
                jqlastRefresh.html(lastRefreshList[0].format('mm:ss') + '&nbsp;' + calsList[0].name + '<br>');
                for (j = 1; j < calsList.length; j++) {
                    jqlastRefresh.append(lastRefreshList[j].format('mm:ss') + '&nbsp;' + calsList[j].name + '<br>')
                }
            }
            data = data.events;
            if (data) {
                try {
                    ajaxInfo('Event #' + data[0].id + ' calId #' + data[0].calendarId);
                    //$j('#cal_' + uCalId).fadeOut('fast').fadeIn('fast');
                } 
                catch (e) {
                }
                var i = 0;
                // usuwamy zale�ne wydarzenia cykliczne (bo i tak s� w li�cie odebranej od serwera)
                var periodParentIds = [];
                for(i = 0; i < data.length; i ++) {
                	if(data[i].periodic === true && data[i].periodParentId != null) {
                		if($j.inArray(data[i].periodParentId, periodParentIds) == -1) {
                			periodParentIds.push(data[i].periodParentId);
                		}
                	}
                }
                for(i = 0; i < periodParentIds.length; i ++) {
                	if(periodParentIds[i] != 0) {
                		removeChildEvents(periodParentIds[i]);
                	}
                }
                for (i = 0; i < data.length; i++) {
                    var newEvent = false, j = 0;
                    var modifiedEvent = $j('#calendar').fullCalendar('clientEvents', data[i].id);
                    if (__skipEventId == data[i].id) {
                        //trace('SKIPPING ' + __skipEventId);
                        __skipEventId = null;
                        continue;
                    }
                    if (modifiedEvent.length == 0) { // musimy stworzy� event
                        trace('NEW EVENT ' + i);
                        trace(data[i]);
                        var sd = new Date(data[i].start * 1000);
                        var ed = new Date(data[i].end * 1000);
                        
                        var evt = {};
                        evt.id = data[i].id;
                        evt.calendarId = uCalId;
                        evt.source = pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + uCalId;
                        evt.start = sd;
                        evt.end = ed;
                        evt.resources = cloneUserList(data[i].resources);
                        evt.reasons = data[i].reasons;
                        evt.owner = cloneUser(data[i].owner);
                        evt.title = data[i].title;
                        evt.desc = data[i].desc;
                        evt.place = data[i].place
                        evt.permissionType = PERMISSION_EDIT;
                        evt.allDay = data[i].allDay;
                        evt.periodKind = data[i].periodKind;
                        evt.periodLength = data[i].periodLength;
                        evt.periodParentId = data[i].periodParentId;
                        evt.periodic = data[i].periodic;
                        if(evt.periodic === true)
                        	evt.periodEnd = new Date(data[i].periodEnd * 1000);
                        tmpEvts.push(evt);
                        
                        var cEvt = cloneEvent(evt);
                        
                        $j('#calendar').fullCalendar('renderEvent', cEvt, true);
                        // dopiero po narysowaniu eventu ustawiamy jego url                        
                        $j('#calendar').fullCalendar('clientEvents', cEvt.id)[0].source = evt.source;
                        modifiedEvent = new Array();
                        modifiedEvent.push(cEvt);
                    }
                    else {
                        modifiedEvent[0].calendarId = uCalId;
                        modifiedEvent[0].source = pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + uCalId;
                        modifiedEvent[0].permissionType = PERMISSION_EDIT;
                        modifiedEvent[0].start = data[i].start;
                        modifiedEvent[0].end = data[i].end;
                        modifiedEvent[0].resources = data[i].resources;
                        modifiedEvent[0].reasons = data[i].reasons;
                        modifiedEvent[0].owner = cloneUser(data[i].owner);
                        modifiedEvent[0].title = data[i].title;
                        modifiedEvent[0].desc = data[i].desc;
                        modifiedEvent[0].place = data[i].place
                        modifiedEvent[0].allDay = data[i].allDay;
                        modifiedEvent[0].periodKind = data[i].periodKind;
                        modifiedEvent[0].periodLength = data[i].periodLength;
                        modifiedEvent[0].periodParentId = data[i].periodParentId;
                        modifiedEvent[0].periodic = data[i].periodic;
                        if(modifiedEvent[0].periodic === true)
                        	modifiedEvent[0].periodEnd = new Date(data[i].periodEnd * 1000);
                        tmpEvts.push(modifiedEvent[0]);
                        
                        $j('#calendar').fullCalendar('updateEvent', modifiedEvent[0]);
                    }
                    //trace(' i is ' + i);
                    /* 1. Usuwamy event, kt�ry jest na kalendarzu zasobu,
                     *    a w swoich zasobach nie zawiera sam siebie
                     *
                     */
                    var displayResEvent = false;
                    var eventCalendar = calsList[idCalToIndexList['cal_' + modifiedEvent[0].calendarId]];
                    if (eventCalendar.type == ANY_CALENDAR) {
                        for (j in modifiedEvent[0].resources) {
                            var curRes = modifiedEvent[0].resources[j];
                            if (curRes.type == ANY_RESOURCE_TYPE) {
                                //trace('checking res ' + curRes.cn);
                                for (k in mapAllRes) {
                                    var curMapRes = mapAllRes[k];
                                    if (curRes.cn == curMapRes.cn) {
                                        displayResEvent = true;
                                        break;
                                    }
                                }
                            }
                        }
                        if (!displayResEvent) {
                            trace('1. removing event #' + modifiedEvent[0].id);
                            $j('#calendar').fullCalendar('removeEvents', modifiedEvent[0].id);
                        }
                    }
                    
                    /* 2. je�eli nie ma nas na li�cie zasob�w eventu, to usuwamy ten event z kalendarza
                     * pod warunkiem, �e jeste�my wlascicielem kalendarza
                     */
                    if (eventCalendar.owner == __currentUser.cn) {
                        var removedRes = true;
                        
                        for (j in modifiedEvent[0].resources) {
                            if (modifiedEvent[0].resources[j].cn == __currentUser.cn) {
                                removedRes = false;
                                break;
                            }
                        }
                        if (removedRes) {
                            //alert('removing event from calendar ' + modifiedEvent[0].calendarId);
                        	trace('2. removing event #' + modifiedEvent[0].id);
                            $j('#calendar').fullCalendar('removeEvents', modifiedEvent[0].id);
                        }
                    }
                }
                
                /**
                 * 3. usuwamy nadmiarowe wydarzenia cyklicnze
                 * TODO - dubluj� si� eventy
                 */
                /*var allEvents = $j('#calendar').fullCalendar('clientEvents');
                for(i = 0; i < allEvents.length; i ++) {
                	var removeEvent = true;
                	for(j = 0; j < data.length; j ++) {
                		if(allEvents[i].id == data[j].id) {
                			removeEvent = false;
                			break;
                		}
                	}
                	if(removeEvent && allEvents[i].periodic === true) {
                		trace('3. removing event #' + allEvents[i].id);
                        //$j('#calendar').fullCalendar('removeEvents', allEvents[i].id);
                	}
                }*/
            }
            
            monitorTimer = true;
            //multiEventUpdater();
            setTimeout('eventUpdater(' + uCalId + ')', REFRESH_TIME);
        }); //ENDOF function(data);
    }
    else {
        monitorTimer = true;
        //multiEventUpdater();
        //trace('skipped ' + uCalId);
        setTimeout('eventUpdater(' + uCalId + ')', REFRESH_TIME);
    }
}

/**
 * zwraca tablic� int ze stringa(h:M), gdzie [0] - godzina, [1] - minuty, zeruje minuty gdy nie ma dwukropka, null gdy b��d
 UWAGA - godziny w javascript s� w przedziale <code>0-23</code><br />.
 Zamiast dwukropka <code>:</code> mo�e by� przecinek <code>,</code> lub kropka <code>.</code>
 */
function extractTime(str){
    if (str == undefined) 
        return null;
    str = str.replace(',', ':');
    str = str.replace('.', ':');
    
    splittedStr = str.split(':');
    if (splittedStr[0].search(/[^0-9]/i) != -1 || (splittedStr[1] && splittedStr[1].search(/[^0-9]/i) != -1)) 
        return null;
    var ret = new Array();
    ret.push(parseInt(splittedStr[0], 10));
    ret.push(splittedStr[1] ? parseInt(splittedStr[1], 10) : 0);
    ret[0] = (ret[0] == 24) ? 0 : ret[0];
    
    if (ret[0] >= 0 && ret[0] <= 23 && ret[1] >= 0 && ret[1] <= 59) 
        return ret;
    
    return null;
}

/* sprawdza czy wpisany jest poprawny format czasu
 i nadaje klas� invalidTime je�li potrzeba
 */
function validateTime(domObj){
    var jqObj = $j(domObj);
    if (extractTime(jqObj.val()) == null) 
        jqObj.addClass('invalidTime');
    else 
        jqObj.removeClass('invalidTime');
}

/**
 * Sprawdza dost�pno�� zasobu
 * @param {UnixStamp} start - czas rozpocz�cia
 * @param {UnixStamp} end - czas zako�czenia
 * @param {Array} calendarsId - id kalendarzy zasob�w
 * @param {Function} callFun - funkcja z parametrem (dost�pny?)
 */
function checkResourceAvailability(start, end, eventId, calendarsId, callFun){
    //trace('checkResourceAvailability ' + new Date(start * 1000).format('HH:MM') +
    //' - ' +
    //new Date(end * 1000).format('HH:MM'));
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        checkResourceAvailability: true,
        eventId: eventId,
        calendarsId: $j.toJSON(calendarsId),
        start: start,
        end: end
    }, function(data){
        if (data == JSON_FAILED) 
            alert('B��d!');
        callFun(data);
    });
}

/**
 * Sprawdza dost�pno�� u�ytkownika
 * @param {UnixStamp} start - czas rozpocz�cia
 * @param {UnixStamp} end - czas zako�czenia
 * @param {Array} usersCn - codename u�ytkownik�w
 * @param {Function} callFun - funkcja z parametrem (dost�pny?)
 */
function checkUserAvailability(start, end, eventId, usersCn, callFun){
    //trace('checkResourceAvailability ' + new Date(start * 1000).format('HH:MM') +
    //' - ' +
    //new Date(end * 1000).format('HH:MM'));
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        checkUserAvailability: true,
        eventId: eventId,
        usersCn: $j.toJSON(usersCn),
        start: start,
        end: end
    }, function(data){
        if (data == JSON_FAILED) 
            alert('B��d!');
        callFun(data);
    });
}

/**
 * Zwraca eval z data lub null gdy b��d (lub gdy data puste)
 */
function safeEval(data){
    try {
        d = eval('(' + data + ')');
    } 
    catch (e) {
        d = null;
    }
    
    return d;
}

function cloneEvent(e){
    var ne = {};
    ne.id = e.id;
    ne.title = e.title;
    ne.desc = e.desc;
    ne.place = e.place;
    ne.start = new Date(e.start);
    ne.end = new Date(e.end);
    ne.allDay = e.allDay;
	ne.periodic = e.periodic;
	ne.periodLength = e.periodLength;
	ne.periodKind = e.periodKind;
	ne.periodEnd = new Date(e.periodEnd);
	ne.periodParentId = e.periodParentId;
    ne.resources = cloneUserList(e.resources);
    ne.calendarId = e.calendarId;
    ne.permissionType = e.permissionType;
    ne.owner = cloneUser(e.owner);
    ne.orgStart = e.orgStart ? new Date(e.orgStart) : null;
    ne.orgEnd = e.orgEnd ? new Date(e.orgEnd) : null;
    
    if (e.reasons) 
        ne.reasons = e.reasons;
    
    if (ne.desc == undefined || ne.desc.length == 0) 
        ne.desc = '';
    if (ne.place == undefined || ne.place.length == 0) 
        ne.place = '';
    
    return ne;
}

function cloneUser(u){
    var nu = new Object();
    nu.cn = u.cn;
    nu.calendarId = u.calendarId ? u.calendarId : 0;
    nu.partTwoOfName = u.partTwoOfName;
    nu.partOneOfName = u.partOneOfName;
    nu.confirm = u.confirm;
    nu.type = u.type;
    nu.label = u.label;
    nu.label = nu.label == undefined ? nu.partTwoOfName + ' ' + nu.partOneOfName : nu.label;
    
    return nu;
}

function cloneUserList(ua){
    var nua = new Array();
    for (_i in ua) 
        nua.push(cloneUser(ua[_i]));
    
    return nua;
}

/* Dodaje kalendarz do wyswietlenia */
function addCalendar(calendarId){
    $j('#calendar').fullCalendar('addEventSource', pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + calendarId);
    trace('CALENDAR #' + calendarId + ' added!');
    calsList[idCalToIndexList['cal_' + calendarId]].visible = true;
}

/* Usuwa kalendarza z widocznych */
function removeCalendar(calendarId){
    $j('#calendar').fullCalendar('removeEventSource', pageCon + '/office/tasklist/calendar-ajax.action?calendarId=' + calendarId);
    trace('CALENDAR #' + calendarId + ' removed!');
    calsList[idCalToIndexList['cal_' + calendarId]].visible = false;
}

function createColorTable(){
    var i = null;
    jqColorTable.append('<div class="default">' + TEXT_DEFAULT_COLOR + '</div>');
    for (i = 0; i < 21; i++) {
        var str = '<div class="cal-' + i + '" ></div>';
        jqColorTable.append(str);
    }
    
    jqColorTable.children('div').click(function(){
        var newColorId = this.className.replace('selected', '').replace('cal-', '');
        var oldColorId = calsList[idCalToIndexList['cal_' + __colorChangeCalId]].colorId;
        
        if ($j(this).hasClass('default')) 
            newColorId = newColorId.replace('default', '').trim();
        
        //trace('replace ' + oldColorId + ' -->> ' + newColorId);
        
        //alert(oldClassName + ' -->> ' + newClassName);
        //$j('.fc-event-' + oldColorId).removeClass('fc-event-' + oldColorId).addClass('fc-event-' + newColorId);
        jqCalsList.children('#cal_' + __colorChangeCalId).removeClass('cal-' + oldColorId).addClass('cal-' + newColorId);
        
        /*if($j(this).hasClass('default'))
         newColorId = 0;*/
        hideColorTable();
        $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
            setCalendarColor: true,
            calendarId: __colorChangeCalId,
            colorId: newColorId
        }, function(data){
            if (data == CALENDAR_COLOR_CHANGED) {
                calsList[idCalToIndexList['cal_' + __colorChangeCalId]].colorId = newColorId;
                $j('#calendar').fullCalendar('rerenderEvents');
            }
            ajaxInfo(data);
        });
    });
    
    $j(document).bind('click', function(){
        if (jqColorTable.css('display') != 'none') 
            hideColorTable();
    });
}

function showColorTable(calId, xPos, yPos){
    jqColorTable.css('left', xPos + 'px').css('top', yPos + 'px');
    __colorChangeCalId = calId;
    var defaultColorId = idCalToIndexList['cal_' + calId];
    jqColorTable.children('div').removeClass('selected').show();
    
    jqColorTable.children('div.cal-' + defaultColorId).hide();
    jqColorTable.children('div.default').attr('className', 'default').addClass('cal-' + defaultColorId).show();
    
    var className = $j('#cal_' + calId)[0].className.replace('calItem ', '');
    jqColorTable.children('div.' + className).addClass('selected');
    jqColorTable.fadeIn('fast');
}

function hideColorTable(){
    $j('img.selectColorActive, div.selectColorActive').removeClass('selectColorActive');
    jqColorTable.fadeOut('fast');
}

function ajaxInfo(msg){
    jqAjaxInfo.fadeOut().fadeIn();
    jqAjaxInfo.removeClass('errorInfo').html(msg);
    if (msg.indexOf('FAILED') != -1) {
        jqAjaxInfo.addClass('errorInfo');
    }
}

function updateProgress(val){
    jqProgressBar.progressbar('value', jqProgressBar.progressbar('value') + val);
}

function cme(){
    $j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
        clearMonitorEvents: true
    }, function(data){
        //trace('Monitor events cleared');
    });
}

function gce(){
    return $j('#calendar').fullCalendar('clientEvents');
}

function getUrlParams() {
	var map = {};
	
	function toInt(val) {
		return parseInt(val, 10);
	}
	
	function toFloat(val) {
		return parseFloat(val);
	}
	
	function toBoolean(val) {
		return val.toLowerCase() == 'true';
	}
	
	function toDate(val) {
		return Date.prototype.toString(val, 'dd-mm-yyyy');
	}
	
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		var callFun;
		
		switch(key) {
		case 'year':
		case 'month':
		case 'day':
		case 'eventId':
		case 'firstday':
			callFun = toInt;
		break;
		
		case 'start':
		case 'end':
			callFun = toDate;
		break;
		
		case 'accept':
		case 'reject':
		case 'embedded':
			callFun = toBoolean;
		break;
		default:
			callFun = function(val) { return val; };
		}
		map[key] = callFun(value);
	});
	
	/*
	var ret = {};
	
	try {
		var allPars = window.location.href.split('?')[1].split('&');
		
		for (i = 0; i < allPars.length; i++) {
			if (allPars[i].search('year=') != -1) {
				ret.year = parseInt(allPars[i].replace('year=', ''), 10);
			} else if (allPars[i].search('month=') != -1) {
				ret.month = parseInt(allPars[i].replace('month=', ''), 10);
				ret.month--;
			} else if (allPars[i].search('day=') != -1) {
				ret.day = parseInt(allPars[i].replace('day=', ''), 10);
			} else if(allPars[i].search('eventId=') != -1) {
				ret.eventId = parseInt(allPars[i].replace('eventId=', ''), 10);					
			} else if(allPars[i].search('accept=') != -1) {
				ret.accept = allPars[i].replace('accept=', '').toLowerCase() == 'true'; 
			} else if(allPars[i].search('reject=') != -1) {
				ret.reject = allPars[i].replace('reject=', '').toLowerCase() == 'true';
			} 
		}
	} catch(e) {}
    
    return ret;
    */
	
	return map;
}

/**
 * Funkcja zapisuje status kalendarza (aktywny, nieaktywny) ajaxowo
 */
function setCalendarVisibility(calendarId, bVisible) {
	$j.post(pageCon + '/office/tasklist/calendar-ajax.action', {
		setCalendarVisibility: true,
		calendarId: calendarId,
		calendarVisible: bVisible
	}, function(data) {
		ajaxInfo(data);
	});
}

function showEventInfo(calEvent) {
	var periodSaveMode = null, periodAcceptMode = null;
if (calEvent.permissionType == PERMISSION_SEE_ONLY_STATE) 
		return;
	
	monitorTimer = false;
	var newUserList = cloneUserList(calEvent.resources);
	__jqCurDiv = $j(this);
	__jqCurDiv.addClass('fc-event-new');
	__jqCurDiv.mouseleave();
	//trace(newUserList);
	jqEventInfo.html('').dialog('destroy');
	fillEventInfoDiv(jqEventInfo, calEvent, newUserList);
	
	jqEventInfo.dialog({
		resizable: true,
		modal: true,
		width: DIALOG_SIZE.width,
		buttons: [
		    {
		    	cn: 'dlg-cancel',
		    	text: 'Anuluj',
		    	click: function() {
					$j(this).dialog('close');
				} 
		    },
		    {
		    	cn: 'dlg-save',
		    	text: 'Zapisz',
		    	click: function(event) {
					//trace(calEvent.allDay);
					var newStart = calEvent.allDay ? [2, 0] : extractTime($j('#eventStart').val());
					var newEnd = calEvent.allDay ? [22, 0] : extractTime($j('#eventEnd').val());
					var i = 0;
					
					if (newStart && newEnd) {
						$j(this).dialog('close');
						if ($j('#eventDesc').hasClass('empty') && $j('#eventDesc').val() == MSG_ADD_DESC) 
							$j('#eventDesc').val('');
						if ($j('#eventPlace').hasClass('empty') && $j('#eventPlace').val() == MSG_ADD_PLACE) 
							$j('#eventPlace').val('');
						calEvent.desc = $j('#eventDesc').val();
						calEvent.place = $j('#eventPlace').val();
						calEvent.start.setHours(newStart[0]);
						calEvent.start.setMinutes(newStart[1]);
						calEvent.end.setHours(newEnd[0]);
						calEvent.end.setMinutes(newEnd[1]);
						calEvent.resources = newUserList;
						if($j('#periodSwitch').attr('checked') == 'checked') {
							calEvent.periodic = true;
							calEvent.periodLength = $j('#periodLength').val();
							calEvent.periodKind = $j('#periodKind').val();
							calEvent.periodEnd = Date.prototype.fromString($j('#periodTo').val(), 'dd-mm-yyyy');
						} else {
							calEvent.periodic = false;
						}
						
						var calId = $j('#calSelect').val();
						for (i = 0; i < calEvent.resources.length; i ++) {
							if (calEvent.resources[i].cn == __currentUser.cn) {
								//alert('zmieniamy idCal na ' + calId);
								calEvent.resources[i].calendarId = parseInt(calId, 10);
								calEvent.source = calEvent.source.split('calendarId')[0] + 'calendarId=' + calId;
								break;
							}
						}
						calEvent.calendarId = calId;
						
						var opts = {
							omitPeriodicSelection: true
						};
						
						if(periodSaveMode != null) {
							if(periodSaveMode == 'single') {
								opts.periodicDetached = true;
							} else if(periodSaveMode == 'future') {
								opts.periodicFuture = true;
							}
						}
						
						updateEvent(calEvent, undefined, opts);
						$j('#calendar').fullCalendar('updateEvent', calEvent);
					}
		    	}
		    },
		    {
		    	cn: 'dlg-save-period-future',
		    	'data-tooltip': 'To wydarzenie oraz kolejne wydarzenia zostan� zmienione.',
		    	text: 'Zapisz kolejne wydarzenia',
		    	click: function() {
		    		periodSaveMode = 'future';
		    		$j('[cn=dlg-save]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-save-period-single',
		    	'data-tooltip': 'Wszystkie pozosta�e wydarzenia w serii pozostan� takie same.',
		    	text: 'Zapisz tylko to wydarzenie',
		    	click: function() {
		    		periodSaveMode = 'single';
		    		$j('[cn=dlg-save]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-accept',
		    	text: 'Akceptuj',
		    	click: function() {
		    		var opts = {
						omitPeriodicSelection: true
					};
					
					if(periodAcceptMode != null) {
						if(periodAcceptMode == 'single') {
							opts.periodicDetached = true;
						} else if(periodAcceptMode == 'future') {
							opts.periodicFuture = true;
						}
					}
					confirmEvent(calEvent.id, calEvent.resources, function(){
						var evts = null, i = 0;
						$j('span.accepted, span.rejected').remove();
						$j('[cn=dlg-accept]').before('<span class="accepted">' + MSG_EVENT_CONFIRMED + '</span>').hide();
						$j('[cn=dlg-accept-period-single], [cn=dlg-accept-period-future]').hide();
						if(periodAcceptMode == null) {
							$j('[cn=dlg-reject]').fadeIn();
							$j('#confirmEvent_' + calEvent.id).remove();
						} else {
							$j('span.accepted').addClass('periodic');
							$j('[cn=dlg-reject-periodic-single], [cn=dlg-reject-periodic-future]').fadeIn();
							if(periodAcceptMode == 'single') {
								$j('#confirmEvent_' + calEvent.id).remove();
							} 
						}
						$j('span.accepted').fadeIn();
						/* szukamy spana  z naszym cn i usuwamy zapytnik */
						$j('span.user#' + __currentUser.cn).text($j('span.user#' + __currentUser.cn).text().replace('?', ''));
					}, opts);
				}
		    },
		    {
		    	cn: 'dlg-accept-period-future',
		    	'data-tooltip': 'To wydarzenie oraz kolejne wydarzenia zostan� zaakceptowane.',
		    	text: 'Zaakceptuj kolejne',
		    	click: function() {
		    		periodAcceptMode = 'future';
		    		$j('[cn=dlg-accept]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-reject-period-future',
		    	'data-tooltip': 'To wydarzenie oraz kolejne wydarzenia zostan� odrzucone.',
		    	text: 'Odrzu� kolejne',
		    	click: function() {
		    		periodAcceptMode = 'future';
		    		$j('[cn=dlg-reject]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-accept-period-single',
		    	'data-tooltip': 'Tylko to wydarzenie zostanie zaakceptowane.',
		    	text: 'Zaakceptuj tylko to',
		    	click: function() {
		    		periodAcceptMode = 'single';
		    		$j('[cn=dlg-accept]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-reject-period-single',
		    	'data-tooltip': 'Tylko to wydarzenie zostanie odrzucone.',
		    	text: 'Odrzu� tylko to',
		    	click: function() {
		    		periodAcceptMode = 'single';
		    		$j('[cn=dlg-reject]').trigger('click');
		    	}
		    },
		    {
		    	cn: 'dlg-reject',
		    	text: 'Odrzu�',
		    	click: function() {
		    		var opts = {
						omitPeriodicSelection: true
					};
					
					if(periodAcceptMode != null) {
						if(periodAcceptMode == 'single') {
							opts.periodicDetached = true;
						} else if(periodAcceptMode == 'future') {
							opts.periodicFuture = true;
						}
					}
					showDeleteReasonDialog(calEvent, opts);
				}
		    }		    
		],
		open: function(event, ui) {
			// przywracamy poprzednie wydarzenie (na kt�rym u�ytkownik nie wybra� �adnej opcji)
			if($j('#periodModeCancel').data('event-id') != null && $j('#periodModeCancel').data('event-id') != calEvent.id) {
				$j('#periodModeCancel').trigger('click');
			}
			$j('[cn=dlg-accept]').addClass('accept').hide();
			$j('[cn=dlg-reject]').addClass('reject').hide();
			$j('[cn=dlg-save-period-single]').addClass('dlgSavePeriodSingle').hide();
			$j('[cn=dlg-save-period-future]').addClass('dlgSavePeriodFuture').hide();
			var jqAccept = $j('[cn=dlg-accept]'), jqReject = $j('[cn=dlg-reject]');
			/*jqEventInfo.nextAll('.ui-dialog-buttonset').children().eq(2).addClass('accept').hide();
			jqEventInfo.nextAll('.ui-dialog-buttonset').children().eq(3).addClass('').hide();*/
			
			if(calEvent.periodic === true) {
				jqAccept.hide(); jqReject.hide();
				jqAccept = $j('[cn=dlg-accept-period-single], [cn=dlg-accept-period-future]');
				jqReject = $j('[cn=dlg-reject-period-single], [cn=dlg-reject-period-future]');
				jqAccept.hide(); jqReject.hide();
			} else {
				$j('[cn=dlg-accept-period-single], [cn=dlg-accept-period-future]').hide();
				$j('[cn=dlg-reject-period-single], [cn=dlg-reject-period-future]').hide();
			}
			
			for (i = 0; i < calEvent.resources.length; i++) {
				if (calsList[idCalToIndexList['cal_' + calEvent.calendarId]].permissionType == PERMISSION_EDIT &&
				calEvent.resources[i].cn == __currentUser.cn &&
				calEvent.owner.cn != __currentUser.cn) {
					if (!calEvent.resources[i].confirm) {
						jqAccept.show();
						if(calEvent.periodic === true) {
							$j('[cn=dlg-cancel]').addClass('doubleHeight');
						}
					}
					
					jqReject.show();
					
					break;
				}
			}
			if (calEvent.permissionType != PERMISSION_EDIT) {
				$j('[cn=dlg-cancel]').children('span.ui-button-text').html('Zamknij');
				$j('[cn=dlg-save]').hide();
			} else {
				if(calEvent.periodic === true) {
					$j('[cn=dlg-save-period-single]').show();
					$j('[cn=dlg-save-period-future]').show();
					$j('[cn=dlg-save]').hide();
				}
			}
		},
		create: function(event, ui) {
			var jqButtons = $j(this).parent().find('button'),
				btns = $j(this).dialog('option', 'buttons'), i = 0;
			
			for(i = 0; i < btns.length; i ++) {
				$j(jqButtons[i]).attr('cn', btns[i].cn);
			}
		},
		close: function(){
			__jqCurDiv.removeClass('fc-event-new');
			monitorTimer = true;
			multiEventUpdater();
		}
	});
	
	return jqEventInfo;
}

function getFutureEvents(parentId, fromDate) {
	return $j('#calendar').fullCalendar('clientEvents', function(evt) {
	    if(evt.periodParentId == parentId && evt.start >= fromDate) {
	        return true;
	    }
	});
}

function addPrintButton() {
	if(!$j('table.fc-header tbody tr td.fc-header-left table td:last').hasClass('printButton')) {
		var str = '<td><span class="fc-header-space"></span></td>';
			str += '<td class="printButton"><div class="fc-state-default fc-corner-left fc-corner-right" onclick="printCalendar();"><a><span>';
			str += 'Drukuj</span></a></div></td>';
			
		$j('table.fc-header tbody tr td.fc-header-left table td:last').after(str);
	}
}

function printCalendar() {
	var oldHeight = $j('.fc-agenda-body').css('height');
	$j('.fc-agenda-body').css('height', 'auto');
	window.print();
	$j('.fc-agenda-body').css('height', oldHeight);
}


