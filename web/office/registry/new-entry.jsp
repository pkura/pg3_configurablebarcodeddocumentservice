<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-entry.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<h1><ww:property value="registry.name"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canCreate">
    <form action="<ww:url value='/docusafe/office/registry/new-entry.action'/>" method="post">
        <ww:hidden name="'caseId'" value="caseId"/>
        <ww:hidden name="'documentId'" value="documentId"/>
        <ww:hidden name="'registryId'" value="registryId"/>
        
        <table>
            <ww:if test="registry.regType == 'case'">
            <tr>
                <td>Znak sprawy:</td>
                <td><ww:property value="caseOfficeId"/></td>
            </tr>
            </ww:if>
            <ww:elseif test="registry.regType == 'indocument'">
            <tr>
                <td>Pismo przychodz�ce:</td>
                <td>
                    <ww:if test="inDocument != null">
                        <ww:property value="inDocument.officeNumber"/>
                    </ww:if>
                </td>
            </tr>
            <tr>
                <td>Nadawca pisma:</td>
                <td>
                    <ww:if test="inDocument != null && inDocument.sender != null">
                        <ww:property value="inDocument.sender.shortSummary"/>
                    </ww:if>
                </td>
            </tr>
            <tr>
                <td>Adres nadawcy:</td>
                <td>
                    <ww:if test="inDocument != null && inDocument.sender != null">
                        <ww:property value="inDocument.sender.address"/>
                    </ww:if>
                </td>
            </tr>
            </ww:elseif>
            
            <jsp:include flush="true" page="/common/registry-fields.jsp"/>
            
            <tr>
                <td></td>
                <td><ds:event value="'Utw�rz wpis w rejestrze'" name="'doCreate'" cssClass="'btn'" onclick="'if (!validateForm()) return false;'"/> </td>
            </tr>            
        </table>            
    </form>
</ww:if>

<script type="text/javascript">
    function validateForm()
    {   
        if (!validateRegistry())
            return false;

        return true;
    }
</script>