<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestry</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ww:if test="menuInfo != null">
	<ww:iterator value="menuInfo.registries">
		<p>
			<ww:property value="name"/>
		</p>
		
		<ds:sitemap menu="2" node="node">
			<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
				<p>
					<a href="<ww:url value="href"/>?registryId=<ww:property value="id"/>">
						<ww:property value="label"/>
						<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</p>
			</ww:if>
		</ds:sitemap>  
	</ww:iterator>
</ww:if>

<%--
<h1>Rejestry</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ww:if test="menuInfo != null">
	<ww:iterator value="menuInfo.registries">
		<p><ww:property value="name"/></p>
		
		<ds:sitemap menu="2" node="node">
			<ww:if test="(regType == 'other') || ((regType != 'other') && (param != 'caseRequired'))">
				<p><a href="<ww:url value="href"/>?registryId=<ww:property value="id"/>"><ww:property value="label"/> <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
				</p>
			</ww:if>
		</ds:sitemap>  
		
<%--		 <p>&nbsp;</p>  -->
	</ww:iterator>
</ww:if>
--%>
<!--N koniec main.jsp N-->