<%--T
	Przer�bka layoutu.
	Stan: TC_0_7
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N search-entries.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="registry.name"/> - wyszukiwarka</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.size() == 0">
	<form action="<ww:url value="'/office/registry/search-entries.action'"/>" method="post" onsubmit="disableFormSubmits(this);" id="form" >
		<ww:hidden name="'registryId'" value="registry.id"/>

		<table class="tableMargin">
			<tr>
				<td>
					Numer w rejestrze:
				</td>
				<td>
					Od:
					<ww:textfield name="'sequenceNumberFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/>
					do:
					<ww:textfield name="'sequenceNumberTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/>
					</td>
			</tr>
			<tr>
				<td>
					Data wpisu do rejestru:
				</td>
				<td>
					Od:
					<ww:textfield name="'ctimeFrom'" id="ctimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_ctimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'ctimeTo'" id="ctimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_ctimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>
					U�ytkownik wpisuj�cy:
				</td>
				<td>
					<ww:select multiple="true" name="'creatingUser'" cssClass="'multi_sel'" list="users"
						size="4" listKey="name" listValue="lastname+' '+firstname"/>
				</td>
			</tr>

			<jsp:include page="/common/search-registry-fields.jsp"></jsp:include>
			
			<tr>
				<td>
					Liczba wynik�w na stronie:
				</td>
				<td>
					<ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }" value="limit > 0 ? limit : 50"/>
				</td>
			</tr>
		</table>
		
		<ds:submit-event value="'Szukaj'" name="'doSearch'" disabled="!canRead"/>
	</form>

	<script type="text/javascript">

if($j('#ctimeFrom').length > 0)
{
		Calendar.setup({
			inputField		:	"ctimeFrom",	 // id of the input field
			ifFormat		:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button			:	"calendar_ctimeFrom_trigger",  // trigger for the calendar (button ID)
			align			:	"Tl",		   // alignment (defaults to "Bl")
			singleClick		:	true
		});
}

if($j('#ctimeTo').length > 0)
{
		Calendar.setup({
			inputField		:	"ctimeTo",	 // id of the input field
			ifFormat		:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button			:	"calendar_ctimeTo_trigger",  // trigger for the calendar (button ID)
			align			:	"Tl",		   // alignment (defaults to "Bl")
			singleClick		:	true
		});	   
}
		/*
			Otwieranie okienko wyszukuj�ce osoby (nadawc�w pism).
		*/
		function openPersonPopup()
		{
			openToolWindow('<ww:url value="'/office/common/person.action'"/>?dictionaryType='+'<%= Person.DICTIONARY_SENDER %>');			
		}

		/*
			Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
		*/
		function __accept_person(map, lparam, wparam)
		{
			var txt = document.getElementById('sender');			

			if (!isEmpty(map.lastname))
				txt.value = map.lastname;
			else if (!isEmpty(map.organization))
				txt.value = map.organization;
			else if (!isEmpty(map.firstname))
				txt.value = map.firstname;
		}
	</script>
</ww:if>
<ww:else>
	<table class="search table100p" cellspacing="0" id="mainTable">
		<tr>
			<ww:iterator value="columns" status="status" >
				<th>
					<span class="continuousElement">
						<ww:if test="sortDesc != null">
							<a href="<ww:url value="sortDesc"/>">
								<img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
						</ww:if>
						<ww:property value="title"/>
						<ww:if test="sortAsc != null">
							<a href="<ww:url value="sortAsc"/>">
								<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
						</ww:if>
					</span>
				</th>
				<ww:if test="!#status.last">
					<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
				</ww:if>
				<th>&nbsp;</th>
			</ww:iterator>
		</tr>
		<ww:iterator value="results">
			<ww:set name="result"/>		
			<tr>
				<ww:iterator value="columns" status="status">
					<td>
						<a href="<ww:url value="#result['link']"/>">
							<ww:property value="prettyPrint(#result[property])"/></a>
					</td>
					<ww:if test="!#status.last">
						<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					</ww:if>
					<td>&nbsp;</td>
				</ww:iterator>
			</tr>
		</ww:iterator>
	</table>	

	<p>
		Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<ww:iterator value="registry.printouts">
				<option value="<ww:property value="key"/>">
					<ww:property value="value.name"/>
				</option>
			</ww:iterator>
		</select>
		<input type="button" value="Wydruk" class="btn" onclick="printout();"/>
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<table class="table100p">
		<tr>
			<td class="alignCenter">
				<jsp:include page="/pager-links-include.jsp"/>
			</td>
		</tr>
	</table>

	<script type="text/javascript">
		function printout()
		{
			if (document.getElementById('printSel').value == '')
			{
				alert('Nie wybrano rodzaju wydruku');
				return;
			};
			openToolWindow('<ww:url value="printUrl"/>&printout='+document.getElementById('printSel').value, 'printout');		   
		}

		// pod�wietlanie tabelki na 2 kolorki
		prepareTable(E("mainTable"),0,0);
	</script>
</ww:else>

<%--
<h1><ww:property value="registry.name"/> - wyszukiwarka</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.size() == 0">
	<form action="<ww:url value="'/office/registry/search-entries.action'"/>" method="post"
		onsubmit="disableFormSubmits(this);" id="form" >

		<ww:hidden name="'registryId'" value="registry.id"/>

		<table>
			<tr>
				<td>Numer w rejestrze:</td>
				<td>Od: <ww:textfield name="'sequenceNumberFrom'" id="sequenceidfrom" cssClass="'txt'" size="10" maxlength="84"/> do: <ww:textfield name="'sequenceNumberTo'" id="sequenceidto" cssClass="'txt'" size="10" maxlength="84"/></td>
			</tr>
			<tr>
				<td>Data wpisu do rejestru:</td>
				<td>
					Od:
					<ww:textfield name="'ctimeFrom'" id="ctimeFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_ctimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					do:
					<ww:textfield name="'ctimeTo'" id="ctimeTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_ctimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>U�ytkownik wpisuj�cy:</td>
				<td>
					<ww:select multiple="true" name="'creatingUser'" cssClass="'multi_sel'" list="users"
						size="4" listKey="name" listValue="lastname+' '+firstname"
						/>
				</td>
			</tr>
	 <%--	   <ww:if test="registry.regType == 'case'">
				<tr>
					<td>Znak sprawy:</td>
					<td><ww:textfield name="'caseOfficeId'" id="caseOfficeId" cssClass="'txt'" size="20"/></td>
				</tr>
			</ww:if>
			<ww:elseif test="registry.regType == 'indocument'">
				<tr>
					<td>Nadawca pisma:</td>
					<td><ww:textfield name="'sender'" id="sender" size="30" maxlength="50" cssClass="'txt'"/>
						<input type="button" value="Wybierz nadawc�" onclick="openPersonPopup();" class="btn" ></td>
				</tr>
			</ww:elseif> -->
			
			<jsp:include page="/common/search-registry-fields.jsp"></jsp:include>
			
			<tr>
				<td>Liczba wynik�w na stronie:</td>
				<td><ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }" value="limit > 0 ? limit : 50"/></td>
			</tr>
			<tr>
				<td colspan="2">
					<ds:submit-event value="'Szukaj'" name="'doSearch'" disabled="!canRead"/>
				</td>
			</tr>
		</table>


	</form>

	<script type="text/javascript">
		Calendar.setup({
			inputField	 :	"ctimeFrom",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_ctimeFrom_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
		Calendar.setup({
			inputField	 :	"ctimeTo",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"calendar_ctimeTo_trigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});	   
		
		/*
			Otwieranie okienko wyszukuj�ce osoby (nadawc�w pism).
		*/
		function openPersonPopup()
		{
			openToolWindow('<ww:url value="'/office/common/person.action'"/>?dictionaryType='+'<%= Person.DICTIONARY_SENDER %>');			
		}

		/*
			Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
		*/
		function __accept_person(map, lparam, wparam)
		{
			var txt = document.getElementById('sender');			

			if (!isEmpty(map.lastname))
				txt.value = map.lastname;
			else if (!isEmpty(map.organization))
				txt.value = map.organization;
			else if (!isEmpty(map.firstname))
				txt.value = map.firstname;
		}
	</script>
</ww:if>
<ww:else>
	<table class="search" width="100%" cellspacing="0" id="mainTable">
	<tr>
		<ww:iterator value="columns" status="status" >
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if> <ww:property value="title"/> <ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			<ww:if test="!#status.last">
				<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			</ww:if>
			<th>&nbsp;</th>
		</ww:iterator>
	</tr>
	<ww:iterator value="results">
		<ww:set name="result"/>		
		<tr>
			<ww:iterator value="columns" status="status">
				<td><a href="<ww:url value="#result['link']"/>"><ww:property value="prettyPrint(#result[property])"/></a></td>
				<ww:if test="!#status.last">
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
				</ww:if>
				<td>&nbsp;</td>
			</ww:iterator>
		</tr>
	</ww:iterator>
	
	</table>	

	<p>Wydruk:
		<select class="sel" id="printSel" >
			<option value="">-- wybierz --</option>
			<ww:iterator value="registry.printouts">
				<option value="<ww:property value="key"/>"><ww:property value="value.name"/></option>
			</ww:iterator>
		</select>
		<input type="button" value="Wydruk" class="btn"
			onclick="printout();" />
	</p>

	<ww:set name="pager" scope="request" value="pager"/>
	<table width="100%">
	<tr>
		<td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
	</tr>
	</table>

	<script type="text/javascript">
		function printout()
		{
			if (document.getElementById('printSel').value == '')
			{
				alert('Nie wybrano rodzaju wydruku');
				return;
			};
			openToolWindow('<ww:url value="printUrl"/>&printout='+document.getElementById('printSel').value, 'printout');		   
		}

		// pod�wietlanie tabelki na 2 kolorki
		prepareTable(E("mainTable"),0,0);
	</script>
</ww:else>
--%>
<!--N koniec search-entries.jsp N-->