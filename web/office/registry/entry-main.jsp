<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N entry-main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1>
	<ww:if test="registry != null"><ww:property value="registry.name"/></ww:if>
</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canRead">
    <form action="<ww:url value='/docusafe/office/registry/entry-main.action'/>" method="post">
        
        <table>
            <tr>
                <td>Numer wpisu:</td>
                <td><ww:property value="entryNumberPerYear"/></td>
            </tr>
            <tr>
                <td>Data wpisu:</td>
                <td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
            </tr>
            <tr>
                <td>Autor:</td>
                <td><ww:property value="creator"/></td>
            </tr>
            <ww:if test="registry.regType == 'case'">
            <tr>
                <td>Znak sprawy:</td>
                <td>
                    <ww:if test="cases != null && cases.size() > 0">
                    <table>
                        <ww:iterator value="cases">
                            <tr>
                                <td><a href="<ww:url value="'/office/edit-case.do?id='+key"/>"><ww:property value="value"/></a></td>
                            </tr>
                        </ww:iterator>
                    </table>
                    </ww:if>
                </td>
            </tr>
            </ww:if>
            <ww:elseif test="registry.regType == 'indocument'">
            <tr>
                <td>Pismo przychodzące:</td>
                <td>
                    <ww:if test="inDocument != null">
                        <a href="<ww:url value="'/office/incoming/summary.action?documentId='+inDocument.id"/>"><ww:property value="inDocument.officeNumber"/></a>
                    </ww:if>
                </td>
            </tr>
            <tr>
                <td>Nadawca pisma:</td>
                <td>
                    <ww:if test="inDocument != null && inDocument.sender != null">
                        <ww:property value="inDocument.sender.shortSummary"/>
                    </ww:if>
                </td>
            </tr>
            <tr>
                <td>Adres nadawcy:</td>
                <td>
                    <ww:if test="inDocument != null && inDocument.sender != null">
                        <ww:property value="inDocument.sender.address"/>
                    </ww:if>
                </td>
            </tr>
            </ww:elseif>
                        
            <jsp:include flush="true" page="/common/registry-fields.jsp"/>
        </table>
                
        <ds:event value="'Zapisz zmiany'" name="'doUpdate'" cssClass="'btn'" disabled="!canWrite" onclick="'if (!validateForm()) return false;'"/> 
        
        <p></p>
        
    </form>
    
    <script type="text/javascript">
        function validateForm()
        {   
            if (!validateRegistry())
                return false;
    
            return true;
        }
    </script>
</ww:if>
