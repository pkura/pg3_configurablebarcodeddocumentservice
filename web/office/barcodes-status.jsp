<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N barcodes.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="StatusPosiadanychBarkodow"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p>
	<ds:xmlLink path="Barcodes"/>
</p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/barcodes-status.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
		
		<p></p>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
    			<td colspan="2"><br/></td>	
			</tr>
            <tr>
                <td><ds:lang text="DataZamBarkodow"/></td>
                <td><ww:textfield name="'data_zam'" value="data_zam" cssClass="'txt'" size="30" id="data_zam" /></td>
            </tr>
            <tr>
                <td><ds:lang text="IlZamBarkodow"/></td>
                <td><ww:textfield name="'il_zam'" value="il_zam" cssClass="'txt'" size="30" id="il_zam" /></td>
			</tr>
		    <tr>
                <td><ds:lang text="ZakresZamBarkodow"/></td>
                <td><ww:textfield name="'zakzam_min'" value="zakzam_min" cssClass="'txt'" size="30" id="zakzam_min" /></td>
				<td><ds:lang text="do"/></td>
                <td><ww:textfield name="'zakzam_max'" value="zakzam_max" cssClass="'txt'" size="30" id="zakzam_max" /></td>
            </tr>
			<tr>
    			<td colspan="2"><br/></td>	
			</tr>
			<tr>
                <td><ds:lang text="DataOtrzBarkodow"/></td>
                <td><ww:textfield name="'data_otrz'" value="data_otrz" cssClass="'txt'" size="30" id="data_otrz" /></td>
			</tr>
			<tr>
                <td><ds:lang text="IlOtrzBarkodow"/></td>
                <td><ww:textfield name="'il_otrz'" value="il_otrz" cssClass="'txt'" size="30" id="il_otrz" /></td>
			</tr>
            <tr>
                <td><ds:lang text="ZakresOtrzBarkodow"/></td>
                <td><ww:textfield name="'zakotrz_min'" value="zakotrz_min" cssClass="'txt'" size="30" id="zakotrz_min" /></td>
				<td><ds:lang text="do"/></td>
                <td><ww:textfield name="'zakotrz_max'" value="zakotrz_max" cssClass="'txt'" size="30" id="zakotrz_max" /></td>
            </tr>
			<tr>
    			<td colspan="2"><br/></td>	
			</tr>
			<tr>
                <td><ds:lang text="IlWykBarkodow"/></td>
                <td><ww:textfield name="'il_wyk'" value="il_wyk" cssClass="'txt'" size="30" id="il_wyk" /></td>
			</tr>
            <tr>
                <td><ds:lang text="ZakresWykBarkodow"/></td>
                <td><ww:textfield name="'zakwyk_min'" value="zakwyk_min" cssClass="'txt'" size="30" id="zakwyk_min" /></td>
				<td><ds:lang text="do"/></td>
                <td><ww:textfield name="'zakwyk_max'" value="zakwyk_max" cssClass="'txt'" size="30" id="zakwyk_max" /></td>
            </tr>
			<tr>
    			<td colspan="2"><br/></td>	
			</tr>
			<tr>
                <td><ds:lang text="IlPozBarkodow"/></td>
                <td><ww:textfield name="'il_poz'" value="il_poz" cssClass="'txt'" size="30" id="il_poz" /></td>
			</tr>
            <tr>
                <td><ds:lang text="ZakresPozBarkodow"/></td>
                <td><ww:textfield name="'zakpoz_min'" value="zakpoz_min" cssClass="'txt'" size="30" id="zakpoz_min" /></td>
				<td><ds:lang text="do"/></td>
                <td><ww:textfield name="'zakpoz_max'" value="zakpoz_max" cssClass="'txt'" size="30" id="zakpoz_max" /></td>
            </tr>
        </table>
</form>