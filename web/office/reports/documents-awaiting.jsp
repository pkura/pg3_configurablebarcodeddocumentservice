<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N documents-awaiting.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h3>Pisma oczekujące na odebranie</h3>

<ww:if test="!reportGenerated">

    <form action="<ww:url value="'/office/reports/documents-awaiting.action'"/>" method="post"
        onsubmit="disableFormSubmits(this);">

    <input type="hidden" name="doReport" id="doReport"/>
    <ww:hidden name="'divisionGuid'" value="divisionGuid"/>

    <table width="100%">
        <tr>
            <td><ww:property value="treeHtml" escape="false"/></td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>Referent pisma:</td>
                        <td><ww:select name="'assignedUser'" id="assignedUser"
                            list="users" listKey="name"
                            listValue="asLastnameFirstname()" cssClass="'sel'"
                            value="assignedUser" headerKey="''" headerValue="'-- dowolny --'" /></td>
                    </tr>
                    <tr>
                        <td valign="top">Data dekretacji:</td>
                        <td valign="top">od <ww:textfield name="'fromDate'" size="10" maxlength="10" id="fromDate" cssClass="'txt'" value="fromDate"/>
                                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                                    id="fromDateTrigger" style="cursor: pointer; border: 1px solid red;"
                                    title="Date selector" onmouseover="this.style.background='red';"
                                    onmouseout="this.style.background=''"/>
                            do <ww:textfield name="'toDate'" size="10" maxlength="10" id="toDate" cssClass="'txt'" value="toDate"/>
                                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                                    id="toDateTrigger" style="cursor: pointer; border: 1px solid red;"
                                    title="Date selector" onmouseover="this.style.background='red';"
                                    onmouseout="this.style.background=''"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td><input type="submit" name="doReport" value="Wygeneruj raport" class="btn" onclick="document.getElementById('doReport').value='true';"/></td>
        </tr>
    </table>

    </form>

    <script>
        Calendar.setup({
            inputField     :    "fromDate",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "fromDateTrigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });

        Calendar.setup({
            inputField     :    "toDate",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "toDateTrigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
    </script>

</ww:if>

<ww:else>

    <ww:if test="!userCounts.empty">
        <p>Pisma oczekujące na odebranie przez użytkowników</p>
        <table>
            <tr>
                <th>Nazwa użytkownika</th><th>Liczba pism</th>
            </tr>
            <ww:iterator value="userCounts">
                <tr>
                    <td><ww:property value="key"/></td>
                    <td><ww:property value="value"/></td>
                </tr>
            </ww:iterator>
        </table>
    </ww:if>

    <ww:if test="!divisionCounts.empty">
        <p>Pisma oczekujące na odebranie w wydziale</p>
        <table>
            <tr>
                <th>Nazwa wydziału</th><th>Liczba pism</th>
            </tr>
            <ww:iterator value="divisionCounts">
                <tr>
                    <td><ww:property value="key"/></td>
                    <td><ww:property value="value"/></td>
                </tr>
            </ww:iterator>
        </table>
    </ww:if>

    <input type="button" value="Nowy raport" onclick="document.location.href='<ww:url value="'/office/reports/documents-awaiting.action'"/>';" class="btn"/>

</ww:else>

