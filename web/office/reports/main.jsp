<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Raporty</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<%--
<p><html:link forward="office/reports/documents-root">Rejestr korespondencji</html:link><br>
    Zestawienie pism wchodzących i wychodzących przez Kancelarię Ogólną.</p>

<p><html:link forward="office/reports/documents-division">Pisma w wydziale</html:link><br>
    Zestawienie pism wchodzących, wewnętrznych i wychodzących z wydziału.</p>
--%>

<%--
<p><html:link forward="office/reports/documents-by-assignment">Pisma zadekretowane</html:link><br>
    Zestawienie pism z dekretacjami, które zostały dokonane prez użytkownika.</p>
--%>

<ds:modules test="office">
    <p><a href="<ww:url value="'/office/reports/documents-awaiting.action'"/>">Pisma oczekujące na odebranie <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
        Zestawienie nieodebranych dekretacji
    </p>
</ds:modules>

<%--
<p><a href="<ww:url value="'/office/reports/documents-status.action'"/>">Pisma o określonym statusie <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Zestawienie pism ze względu na status, w jakim się obecnie znajdują
</p>
--%>

<!-- nieistniejące raporty na prezentację -->

<p><a href="#">Pisma otrzymane <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Pisma otrzymane
</p>

<p><a href="#">Pisma przekazane wydziałom<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Pisma przekazane wydziałom
</p>

<p><a href="#">Pisma przekazane użytkownikom<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Pisma przekazane użytkownikom
</p>

<%--
<p><a href="#">Rejestr korespondencji <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Zestawienie pism wchodzących i wychodzących przez KO
</p>

<p><a href="#">Wykaz pism przekazanych do wydziału <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Wykaz pism przyjętych w KO i przekazanych do konkretnego działu
</p>

<p><a href="#">Wykaz pism w wydziale <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Zestawienie pism wchodzących, wew. i wychodzących z wydziału
</p>

<p><a href="#">Wykaz pism zadekretowanych <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Zestawienie pism z dekretacjami, które zostały dokonane przez użytkownika
</p>

<p><a href="#">Wykaz pism przekazanych pracownikom <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Lista pism zarejestrowanych w wydziale
</p>

<p><a href="#">Wykaz pism przeterminowanych <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Wykaz pism, których termin załatwiania upłynął
</p>

<p><a href="#">Wykaz istniejących spraw <img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a> <br>
    Wykaz istniejących spraw
</p>
--%>
