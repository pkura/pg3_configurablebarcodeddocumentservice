<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N documents-status.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h3>Pisma o określonym statusie</h3>

<table>
    <tr>
        <th>Status</th><th>Liczba dokumentów</th>
    </tr>
    <ww:iterator value="status2count">
        <tr>
            <td><ww:property value="key.name"/></td>
            <td align="right"><ww:property value="value"/></td>
        </tr>
    </ww:iterator>
</table>

<p></p>

<a target="_blank" href="<ww:url value="'/office/reports/documents-status.action?pdf=true'"/>">Wersja PDF</a>
