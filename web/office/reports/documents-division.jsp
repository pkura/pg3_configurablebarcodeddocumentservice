<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N documents-division.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ page import="java.util.Calendar"%>

<h3>Pisma w wydziale</h3>

<html:form action="/office/reports/documents-division">

<table width="100%">
<tr>
    <td width="250" valign="top">
        <div style="overflow-x: auto; width: 250px; ">
            <c:out value="${tree}" escapeXml="false" />

            <hr size="1" noshade="noshade"/>
            Legenda: <br/>

            <nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-closed.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Grupa u�ytkownik�w"/> - dzia�</nobr>
            <nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-position.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Stanowisko"/> - stanowisko</nobr>
            <nobr><img src="<c:out value='${pageContext.request.contextPath}'/>/img/folder-group.gif" width="18" height="18" class="zoomImg" class="zoomImg" alt="Grupa u�ytkownik�w"/> - grupa</nobr>
        </div>
    </td>
    <td valign="bottom">

        <table>
        <tr>
            <td><html:select property="month">
                    <html:option value="<%= ""+Calendar.JANUARY %>">stycze�</html:option>
                    <html:option value="<%= ""+Calendar.FEBRUARY %>">luty</html:option>
                    <html:option value="<%= ""+Calendar.MARCH %>">marzec</html:option>
                    <html:option value="<%= ""+Calendar.APRIL %>">kwiecie�</html:option>
                    <html:option value="<%= ""+Calendar.MAY %>">maj</html:option>
                    <html:option value="<%= ""+Calendar.JUNE %>">czerwiec</html:option>
                    <html:option value="<%= ""+Calendar.JULY %>">lipiec</html:option>
                    <html:option value="<%= ""+Calendar.AUGUST %>">sierpie�</html:option>
                    <html:option value="<%= ""+Calendar.SEPTEMBER %>">wrzesie�</html:option>
                    <html:option value="<%= ""+Calendar.OCTOBER %>">pa�dziernik</html:option>
                    <html:option value="<%= ""+Calendar.NOVEMBER %>">listopad</html:option>
                    <html:option value="<%= ""+Calendar.DECEMBER %>">grudzie�</html:option>
                </html:select>
            </td>
        </tr>
        <tr>
            <td><html:select property="year">
                    <html:option value="2004">2004</html:option>
                </html:select>
            </td>
        </tr>
        <tr>
            <td>
                <html:select property="kind">
            <%--
                <% for (int i=0; i < IncomingDocumentKind.kinds.length; i++) { %>
                    <html:option value="<%= IncomingDocumentKind.kinds[i].toInt() %>"><%= IncomingDocumentKind.kinds[i].getDescription() %></html:option>
                <% } %>
            --%>
                </html:select>
            </td>
        </tr>
        <tr>
            <td><html:select property="assignedUser">
                    <html:optionsCollection name="assignedUsers"/>
                </html:select>
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" name="doSearch" value="Poka�">
            </td>
        </tr>
        </table>

    </td>
</tr>
</table>



</html:form>
