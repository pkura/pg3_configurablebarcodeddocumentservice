<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N documents-by-assignment.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<h3>Pisma zadekretowane</h3>

<edm-html:errors />
<edm-html:messages />

<c:if test="${empty results}">
<html:form action="/office/reports/documents-by-assignment">
<table>
    <tr>
        <td>Nadawca pisma:</td>
        <td>
            <html:select property="sourceUser" styleClass="sel_opt">
                <html:option value=" ">-- dowolny --</html:option>
                <html:optionsCollection name="users"/>
            </html:select>
        </td>
    </tr>
    <tr>
        <td>Odbiorca pisma</td>
        <td>
            <html:select property="targetUser" styleClass="sel_opt" >
                <html:option value=" ">-- dowolny --</html:option>
                <html:optionsCollection name="users"/>
            </html:select>
        </td>
    </tr>
    <tr>
        <td>Rodzaj pisma</td>
        <td>
            <html:select property="kind" styleClass="sel_opt">
                <html:option value=" ">-- dowolny --</html:option>
                <%--
                <% for (int i=0; i < IncomingDocumentKind.kinds.length; i++) { %>
                    <html:option value="<%= ""+IncomingDocumentKind.kinds[i].toInt() %>"><%= IncomingDocumentKind.kinds[i].getDescription() %></html:option>
                <% } %>
                --%>
            </html:select>
        </td>
    </tr>
    <tr>
        <td>Data utworzenia (od-do)</td>
        <td>
            <html:text property="fromDate" size="10" maxlength="10" styleId="fromDate" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_fromDate_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            <html:text property="toDate" size="10" maxlength="10" styleId="toDate" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_toDate_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" name="doSearch" class="btn" value="Poka�"/>
        </td>
    </tr>
</table>
</html:form>

<script type="text/javascript">
    Calendar.setup({
        inputField     :    "fromDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_fromDate_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

    Calendar.setup({
        inputField     :    "toDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_toDate_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>
</c:if>

<c:if test="${!empty results}">

    <p>U�ytkownik dekretuj�cy: <c:out value="${sourceUser}"/>,
        odbiorca dekretacji: <c:out value="${targetUser}"/>;
        znaleziono <c:out value="${totalCount}"/> pism</p>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th><nobr>Data przyj�cia pisma
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['incomingDate_desc']}"/>'> &lt; </a>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['incomingDate_asc']}"/>'> &gt; </a>
        </nobr></th>
        <th><nobr>Data dekretacji pisma
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['assignmentCtime_desc']}"/>'> &lt; </a>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['assignmentCtime_asc']}"/>'> &gt; </a>
        </nobr></th>
        <th><nobr>Nadawca
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['sender_desc']}"/>'> &lt; </a>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['sender_asc']}"/>'> &gt; </a>
        </nobr></th>
        <th><nobr>Odbiorca
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['recipient_desc']}"/>'> &lt; </a>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['recipient_asc']}"/>'> &gt; </a>
        </nobr></th>
    </tr>
    <c:forEach items="${results}" var="bean">
        <tr>
        <td>
            <a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><fmt:formatDate value="${bean.incomingDate}" type="both" pattern="dd-MM-yy HH:mm"/></a>
        </td>
        <td>
            <a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><fmt:formatDate value="${bean.assignmentCtime}" type="both" pattern="dd-MM-yy HH:mm"/></a>
        </td>
        <td>
            <a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.sender}"/></a>
        </td>
        <td>
            <a href='<c:out value="${pageContext.request.contextPath}${bean.link}"/>'><c:out value="${bean.recipient}"/></a>
        </td>
        </tr>
    </c:forEach>
    </table>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td colspan="2" align="center">
            <jsp:include page="/pager-links-include.jsp"/>
        <td>
    </tr>
    </table>
</c:if>
