<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<div class="test-div" style="display: none;" id="pageType">office-addIncomingDocument-overall</div>

<style type="text/css">
	select#senderCountry {width:24em;}
	[name=extended] {display:none;}
	#changeViewLink {position:absolute; left:600px;}
</style>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>
<p></p>

<ww:set name="exists" value="documentId != null"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	
	var isSimpleView = <ww:if test="daa">false</ww:if><ww:else>true</ww:else>;
	
	// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
	// jako anonimowego
	var senderFields = new Array(
		'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
		'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
		'senderStreetAddress');
	
	function disableEnableSender(checkbox)
	{
	
		for (var i=0; i < senderFields.length; i++)
		{
			document.getElementById(senderFields[i]).disabled = checkbox.checked;
			//document.getElementById(senderFields[i]).style.background = checkbox.checked ? '#A5967C' : '#BCB09D';
		}
	}
	
	// wype�nia bie��c� dat� i czasem pola informuj�ce o dacie przyj�cia dokumentu
	function fillToday()
	{
		var incomingDate = document.getElementById('incomingDate');
		var incomingTime = document.getElementById('incomingTime');
		var d = new Date();
		var year = d.getYear() >= 1900 ? d.getYear() : 1900 + d.getYear();
	
		incomingDate.value = (d.getDate() < 10 ? '0'+d.getDate() : d.getDate()) +
			'-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1) +
			'-'+year;
		incomingTime.value = (d.getHours() < 10 ? '0'+d.getHours() : d.getHours()) +':'+
			(d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes());
	}
	
	// funkcja wywo�ywana przez okienko z people-directory-results.jsp
	function __people_directory_pick(addresseeType, title, firstname, lastname,
		organization, organizationDivision, street, zip, location, country, email, fax)
	{
		if (addresseeType == 'sender')
		{
			document.getElementById('senderTitle').value = title;
			document.getElementById('senderFirstname').value = firstname;
			document.getElementById('senderLastname').value = lastname;
			document.getElementById('senderOrganization').value = organization;
			document.getElementById('senderStreetAddress').value = street;
			document.getElementById('senderZip').value = zip;
			document.getElementById('senderLocation').value = location;
			document.getElementById('senderCountry').value = country;
			document.getElementById('senderEmail').value = email;
			document.getElementById('senderFax').value = fax;
		}
		else if (addresseeType == 'recipient')
		{
			document.getElementById('recipientTitle').value = title;
			document.getElementById('recipientFirstname').value = firstname;
			document.getElementById('recipientLastname').value = lastname;
			document.getElementById('recipientOrganization').value = organization;
			document.getElementById('recipientDivision').value = organizationDivision;
			document.getElementById('recipientStreetAddress').value = street;
			document.getElementById('recipientZip').value = zip;
			document.getElementById('recipientLocation').value = location;
			//document.getElementById('recipientCountry').value = country;
			document.getElementById('recipientEmail').value = email;
			document.getElementById('recipientFax').value = fax;
		}
	}
	
	// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
	var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
	// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
	// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
	// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
	var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
		'streetAddress', 'zip', 'location', 'country', 'email', 'fax');
	
	// lparam - identyfikator
	function openPersonPopup(lparam, dictionaryType)
	{
		openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true&guid=<ww:property value="personDirectoryDivisionGuid"/>', 'person',700,650);
	}
	
	// lparam - identyfikator
	// wparam - pozycja na li�cie
	function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
	{
		//alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
		var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true&guid=<ww:property value="personDirectoryDivisionGuid"/>';
		//alert('url='+url);
		openToolWindow(url,'person',700,650);
	}
	
	function openGroupPopup()
	{
		openToolWindow('<ww:url value="'/office/common/person-group.action'"/>', 'person',700,650);
	}
	
	function personSummary(map)
	{
		var s = '';
		if (map.title != null && map.title.length > 0)
			s += map.title;
	
		if (map.firstname != null && map.firstname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.firstname;
		}
	
		if (map.lastname != null && map.lastname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.lastname;
		}
	
		if (map.organization != null && map.organization.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.organization;
		}
	
		if (map.street != null && map.street.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.street;
		}
	
		if (map.location != null && map.location.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.location;
		}
	
		return s;
	}
	
		/*
	function __accept_person_duplicate(map, lparam, wparam)
	{
		if (lparam == 'recipient')
		{
			var sel = document.getElementById('odbiorcy');
	
			for (var i=0; i < sel.options.length; i++)
			{
				eval('var smap='+sel.options[i].value);
				if (compareArrays(smap, map, true))
				{
					//alert('Istnieje ju� taki odbiorca');
					return true;
				}
			}
		}
	
		return false;
	}	 */
	
	/*
		Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
	*/
	function __accept_person(map, lparam, wparam)
	{
		if (lparam == 'recipient')
		{
			var sel = document.getElementById('odbiorcy');
	
			//if (isEmpty(map.firstname) && isEmpty(map.lastname) && isEmpty(map.organization))
			//	return;
			//addOption(sel, JSONStringify(map), personSummary(map));
	
			if (wparam != null && (''+wparam).length > 0)
			{
				setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
			}
			else
			{
				addOption(sel, JSONStringify(map), personSummary(map));
			}
		}
		else if (lparam == 'sender')
		{
			document.getElementById('senderTitle').value = map.title;
			document.getElementById('senderFirstname').value = map.firstname;
			document.getElementById('senderLastname').value = map.lastname;
			document.getElementById('senderOrganization').value = map.organization;
			document.getElementById('senderStreetAddress').value = map.street;
			document.getElementById('senderZip').value = map.zip;
			document.getElementById('senderLocation').value = map.location;
			document.getElementById('senderCountry').value = map.country;
			document.getElementById('senderEmail').value = map.email;
			document.getElementById('senderFax').value = map.fax;
		}
	}
	
	function __accept_group(group)
	{
		document.getElementById('doAddGroupToRecipients').value = 'true';
		document.getElementById('group').value = group;
		selectAllOptions(document.getElementById('odbiorcy'));
	
		document.forms[0].submit();
	}
	
	function markReturned()
	{
		document.getElementById('returned').checked = !isNaN(parseInt(document.getElementById('returnReasonId').value));
	}

</script>

<form id="test-overall" action="<ww:url value='&apos;/office/outgoing/main.action&apos;'/>" method="post" enctype="multipart/form-data" onsubmit="selectAllOptions(document.getElementById('odbiorcy')); return; ">
	<ww:hidden name="'filestoken'" value="filestoken"/>
	<input type="hidden" name="doCreate" id="doCreate"/>
	<input type="hidden" name="doSendToEpuap" id="doSendToEpuap"/>
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<input type="hidden" name="doGenerateCSV" id="doGenerateCSV"/>
	<input type="hidden" name="doIntoJournal" id="doIntoJournal"/>
	<input type="hidden" name="doAssignOfficeNumber" id="doAssignOfficeNumber"/>
	<input type="hidden" name="doSplitAndAssignOfficeNumber" id="doSplitAndAssignOfficeNumber"/>
	<input type="hidden" name="doSplit" id="doSplit"/>
	<input type="hidden" name="doAddGroupToRecipients" id="doAddGroupToRecipients"/>
	<input type="hidden" name="group" id="group"/>
	<input type="hidden" name="createMultiReplies" id="createMultiReplies"/>
	<%--	<input type="hidden" name="doCancelDocument" id="doCancelDocument"/>--%>
	<input type="hidden" name="assignOfficeNumber" id="assignOfficeNumber"/>
	<input type="hidden" name="attachCase" id="attachCase"/>
	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'activity'" value="activity"/>
	<ww:hidden name="'inDocumentId'" value="inDocumentId"/>
	<ww:hidden name="'grantRequestId'" value="grantRequestId"/>
	<ww:hidden name="'inActivity'" value="inActivity"/>
	<ww:token />
	
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

<%--	<ww:if test="!daa">
	<a id="changeViewLink" href="javascript:changeView()"><ds:lang text="PokazPelnyWidokFormularza"/></a>
</ww:if>
<br/>	--%>
<ww:if test="#exists">
	<ww:if test="blocked">
		<p><span style="color: red"><ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.</span></p>
	</ww:if>
	
	<p>
		<ww:if test="inDocument != null">
			<ds:lang text="PismoStanowiOdpowiedzNaPismoPrzychodzaceNr"/>
			<a target="_blank" href="<ww:url value="'/office/incoming/summary.action'"/>?documentId=<ww:property value="inDocument.id"/>">
				<ww:property value="inDocument.officeNumber"/> (<ww:property value="inDocument.caseDocumentId"/>)
			</a> <br/>
		</ww:if>
	   <ww:set name="ctime" value="document.ctime" scope="page"/>
		<ds:lang text="Autor"/>: <b><ww:property value="creatingUser"/></b> <br/>
		 	<ds:lang text="dnia"/> <b><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></b><br/>
		<ds:lang text="Dzial"/>: <b><ww:property value="assignedDivision"/></b> <br/>
	 <%--   <ww:set name="officedate" value="document.officedate" scope="page"/>  
		<ds:lang text="NumerKancelaryjny"/>: <b><ww:property value="officeNumber"/></b> 
		<ww:if test="document.officedate!=null">
				<ds:lang text="nadany"/> : <b><fmt:formatDate value="${officedate}" type="both" pattern="dd-MM-yy HH:mm"/> <br/>
		</ww:if>  --%>
		<ds:extras test="!business"><ds:lang text="SymbolWsprawie"/>: <b><ww:property value="caseDocumentId"/></b> <br/> </ds:extras>
		<ww:iterator value="journalMessages">
			<ds:lang text="NumerWdziale"/> '<ww:property value='key'/>': <b><ww:property value='value'/></b> <br/>
		</ww:iterator>
	</p>  
</ww:if>

<ww:if test="currentDayWarning">
	<p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>


<table border="0" class="formTable">
<ww:if test="intoJournalPermission && incomingDivisions != null && incomingDivisions.size() > 0">
<tr>
	<td>
		<ds:lang text="PrzyjmijPismoWwydziale"/>:
	</td>
	<td>
		<ww:select name="'intoJournalId'" list="incomingDivisions" cssClass="'sel'"
			headerKey="''" headerValue="getText('select.wybierz')" />
		<input type="submit" name="doIntoJournal" class="btn" value="<ds:lang text="Przyjmij"/>"
			onclick="document.getElementById('doIntoJournal').value='true'"/>
	</td>
</tr>
</ww:if>
<tr>
	<td><ds:lang text="DataPisma"/>:</td>
	<td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
		<img src="<ww:url value="'/calendar096/img.gif'"/>"
			id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
			title="Date selector" onmouseover="this.style.background='red';"
			onmouseout="this.style.background=''"/>
	</td>
</tr>
<tr>
	<td valign="top"><ds:lang text="OpisPisma"/><span class="star">*</span>:</td>
	<td><ww:textarea name="'description'" rows="3" cols="47" cssClass="'txt'" id="description"/></td>
<%--	<td><ww:textfield name="'description'" size="50" maxlength="80" cssClass="'txt'" id="description"/></td>--%>
</tr>
<tr>
	<td><ds:lang text="SymbolWsprawie"/>:</td>
	<td><ww:textfield name="'caseDocumentId'" size="50" maxlength="100" cssClass="'txt'" id="caseDocumentId"/></td>
</tr>
<tr>
	<td><ds:lang text="ZnakPisma"/>:</td>
	<td><ww:textfield name="'referenceId'" size="50" maxlength="60" cssClass="'txt'" id="referenceId"/></td>
</tr>
<tr name="extended">
	<td><ds:lang text="KodKreskowy"/>:</td>
	<td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" id="barcode"/></td>
</tr>
<tr>
	<td><ds:lang text="StanPisma"/>:</td>
	<td><ww:select name="'prepState'" list="#@java.util.LinkedHashMap@{ @pl.compan.docusafe.core.office.OutOfficeDocument@PREP_STATE_DRAFT: getText('Brudnopis'), @pl.compan.docusafe.core.office.OutOfficeDocument@PREP_STATE_FAIR_COPY: getText('Czystopis') }" cssClass="'sel'" value="prepState" /></td>
</tr>
<ds:available test="ShowReceiveMethod">
<script type="text/javascript">
function hideShowEpuap(val) {
	if ( val!='ePUAP' )  {
		if ( document.all ) {	document.getElementById("MetodaEpuapRow").style.visibility="hidden"; }
		else {	document.getElementById("MetodaEpuapRow").style.display="none";	}
	}else{
		if ( document.all ) {	document.getElementById("MetodaEpuapRow").style.visibility="visible";} 
		else {	document.getElementById("MetodaEpuapRow").style.display="table-row";}
		
	}
}
function hideShowPodpisz(val) {
	if (document.getElementById("btnPodpisz") != null){
			if ( val!='Dor�czenie' ){
				if ( document.all ) {	document.getElementById("btnPodpisz").style.visibility="hidden"; }
				else {	document.getElementById("btnPodpisz").style.display="none";	}
			}else{
			if ( document.all ) {	document.getElementById("btnPodpisz").style.visibility="visible";} 
				else {	document.getElementById("btnPodpisz").style.display="table-row";}
		}
	}
}
</script>
	<tr>
		<td><ds:lang text="SposobOdbioru"/>:</td>
		<td><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
			value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'" onchange="'hideShowEpuap(options[selectedIndex].text);'"/></td>
	</tr>
	<tr id="MetodaEpuapRow" >
		<td><ds:lang text="MetodaEpuap"/>:</td>
		<td><ww:select name="'epuapMethodId'" list="epuapMethods" id="selectEpuapMethodId" cssClass="'sel'" onchange="'hideShowPodpisz(options[selectedIndex].text);'"/></td>
	</tr>
	
	
</ds:available>
<ww:if test="#exists">
	<tr>
		<td><ds:lang text="ReferentPisma"/>:</td>
		<td><ww:select name="'assignedUser'" list="users" listKey="name"
			listValue="asLastnameFirstname()" value="assignedUser"
			headerKey="''" headerValue="getText('brak')"  
			cssClass="'sel'" />
		</td>
	</tr>
</ww:if>
	<tr>
	   	<td><ds:lang text="Waga"/></td>
	   	<td>
	   		<ww:textfield name="'weightKg'" size="10" maxlength="10" cssClass="'txt'" id="weightKg"/>
	   		kg.
			<ww:textfield name="'weightG'" size="10" maxlength="10" cssClass="'txt'" id="weightG"/>
			g.
		</td>
   </tr>
   <tr>
	   	<td><ds:lang text="Priorytet"/></td>
	   	<td><ww:checkbox name="'priority'" fieldValue="true" id="priority" value="priority"/></td>
	</tr>
<tr class="formTableDisabled">
	<td colspan="2"><p><h4><ds:lang text="Nadawca"/></h4></p></td>

</tr>
<tr>
	<td><ds:lang text="TytulStanowisko"/>:</td>
	<td><ww:textfield name="'senderTitle'" size="30" maxlength="30" cssClass="'txt'" id="senderTitle" readonly="readOnly" /></td>
</tr>
<tr>
	<td><ds:lang text="Imie"/>:</td>
	<td><ww:textfield name="'senderFirstname'" size="50" maxlength="50" cssClass="'txt'" id="senderFirstname" readonly="readOnly" /></td>
</tr>
<tr>
	<td><ds:lang text="Nazwisko"/>:</td>
	<td><ww:textfield name="'senderLastname'" size="50" maxlength="50" cssClass="'txt'" id="senderLastname" readonly="readOnly" /></td>
</tr>
<tr>
	<td><ds:lang text="Instytucja"/>:</td>
	<td><ww:textfield name="'senderOrganization'" size="50" maxlength="50" cssClass="'txt'" id="senderOrganization" readonly="readOnly" /></td>
</tr>
<tr>
	<td><ds:lang text="Ulica"/>:</td>
	<td><ww:textfield name="'senderStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="senderStreetAddress" readonly="readOnly" /></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="KodPocztowyImiejscowosc"/>:</td>
	<td><ww:textfield name="'senderZip'" size="7" maxlength="14" cssClass="'txt'" id="senderZip" readonly="readOnly" />
		<ww:textfield name="'senderLocation'" size="38" maxlength="50" cssClass="'txt'" id="senderLocation" readonly="readOnly" />
		<br/><span class="warning" id="senderZipError"> </span></td>
</tr>
<tr name="extended">
	<td><ds:lang text="Kraj"/>:</td>
	<td><ww:select name="'senderCountry'"
		list="@pl.compan.docusafe.util.Countries@COUNTRIES"
		listKey="alpha2" listValue="name" value="'PL'"
		id="senderCountry" cssClass="'sel'" onchange="this.form.senderZip.validator.revalidate();"
		disabled="readOnly"/>
	</td>
</tr>

<tr name="extended">
	<td><ds:lang text="Email"/>:</td>
	<td><ww:textfield name="'senderEmail'" size="50" maxlength="50" cssClass="'txt'" id="senderEmail" readonly="readOnly" /></td>
</tr>
<tr name="extended">
	<td><ds:lang text="Faks"/>:</td>
	<td><ww:textfield name="'senderFax'" size="30" maxlength="30" cssClass="'txt'" id="senderFax" readonly="readOnly" /></td>
</tr>
<tr>
	<td></td>
	<td><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn"></td>
</tr>

<tr class="formTableDisabled">
	<td colspan="2"><p><h4><ds:lang text="Odbiorcy"/> <span class="star">*</span></h4></p></td>
</tr>
<tr class="formTableDisabled">
	<td colspan="2">
		<ww:select name="'recipients'" id="odbiorcy" cssStyle="'width: 500px;'" multiple="true" size="3" cssClass="'multi_sel dontFixWidth'"
			list="recipientsMap" />
		<script type="text/javascript">					
					$j(document).ready(function(event) {
						var sel_el = $j('#odbiorcy');
						var minSize = 3;
						var maxSize = 15;
						
					    sel_el.data("size", $j("#odbiorcy option").size());
					    if(sel_el.children().length > minSize && sel_el.children().length < maxSize)
						    sel_el.attr('size', sel_el.children().length); 
					    

					    $j("#odbiorcy option").livequery(function() {
					        if ($j("#odbiorcy option").size() > sel_el.data("size") && sel_el.data("size") >= minSize) {
					  			sel_el.attr('size', $j("#odbiorcy option").size());
								//alert('dodaje odbiorce: size=' + sel_el.data("size"));
					  			if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
					  		}
					
					        sel_el.data("size", $j("#odbiorcy option").size());
					    }, function() {
							  if ($j("#odbiorcy option").size() < sel_el.data("size")) {
								  sel_el.attr('size', sel_el.data("size")-1);	
								  if(sel_el.data("size") <= minSize)
							  			sel_el.attr('size', minSize);
									
								if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
							  }
					        sel_el.data("size", $j("#odbiorcy option").size());
						 });				    
					});
				</script>
		<ww:if test="!readOnly">
			<br/>
			<input type="button" value="<ds:lang text="DodajOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_SENDER %>');" class="btn">
			<input type="button" value="<ds:lang text="DodajGrupe"/>" onclick="openGroupPopup();" class="btn">
			<input name="editRecipient" type="button" value="<ds:lang text="EdycjaOdbiorcy"/>" onclick="var odb=document.getElementById('odbiorcy'); openEditPersonPopup(odb.options[odb.options.selectedIndex].value, odb.options.selectedIndex, 'recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn">
			<input type="button" value="<ds:lang text="UsunOdbiorcow"/>" onclick="deleteSelectedOptions(document.getElementById('odbiorcy'));" class="btn">
			<ds:available test="main.doGenerateCSV">
				<input type="submit" name="doGenerateCSV" value="<ds:lang text="GenerujCSV"/>" class="btn"
				onclick="document.getElementById('doGenerateCSV').value = 'true';"/>
			</ds:available>
		</ww:if>
	</td>
</tr>
<ds:extras test="!business">
	<tr class="formTableDisabled">
		<td colspan="2"><p><h4><ds:lang text="Inne"/></h4></p></td>
	</tr>
	<tr>
		<td valign="top"><ds:lang text="NrPrzesylkiRejestrowanej"/> (R):</td>
		<td><ww:textfield name="'postalRegNumber'" size="20" maxlength="20" cssClass="'txt'" id="postalRegNumber" />
		</td>
	</tr>
	<tr>
		<td valign="top"><ds:lang text="DataZPO"/>:</td>
		<td><ww:textfield name="'zpoDate'" size="10" maxlength="10" cssClass="'txt'" id="zpoDate" value="zpoDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="zpoDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
		</td>
	</tr>
	<tr>
		<td><ds:lang text="WartoscZnaczka"/>:</td>
		<td><ww:textfield name="'stampFee'" size="6" maxlength="6" cssClass="'txt'" /></td>
	</tr>
	<ww:if test="#exists">
		<tr>
			<td valign="top"><ds:lang text="PismoZwrocone"/>:</td>
			<td><ww:checkbox id="returned" name="'returned'" fieldValue="true" value="document.returned"/></td>
		</tr>
		<tr>
			<td valign="top"><ds:lang text="PrzyczynaZwrotu"/>:</td>
			<td><ww:select name="'returnReasonId'" list="returnReasons" listKey="id" listValue="name"
			value="document.returnReason.id" id="returnReasonId" cssClass="'sel'"
			onchange="'markReturned();'"
			headerKey="''" headerValue="getText('wybierzPrzyczyne')" /></td>
		</tr>
	</ww:if>
</ds:extras>
<ww:else>
	<tr  class="formTableDisabled">
		<td colspan="2"> </td>
	</tr>
</ww:else>

<ww:if test="documentId == null">
	<ww:if test="loadFiles != null && loadFiles.size() > 0">
	<tr class="formTableDisabled">
		<td colspan="1">
			<ds:lang text="PlikiJuzobrane"/>:
		</td>
	</tr>
	</ww:if>
	<ww:iterator value="loadFiles">
		<tr>
			<td colspan="1">
				<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
				<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
			</td>
		</tr>
	</ww:iterator>
	<tr class="formTableDisabled">
		<td colspan="2">
		<table id="kontener">
			<tbody id="tabela_glowna">
				<tr>
					<td><ds:lang text="Plik" /><span class="star">*</span>:</td>
				</tr>
				<tr>
					<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
	<tr class="formTableDisabled">
		<td><a href="javascript:addAtta();"><ds:lang
			text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
	</tr>
</ww:if>

<tr>
	<td></td>
	<td>
		<p></p>
		<ww:if test="documentId == null">
			<input type="submit" name="doCreate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
			onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';"/>

			<input type="submit" name="doCreate" value="<ds:lang text="ZapiszInadajNumerKO"/>" class="btn"
			<ww:if test="!canCreateWithOfficeNumber">disabled="true"</ww:if>
			onclick="if (!validateForm()) return false; document.getElementById('assignOfficeNumber').value = 'true'; document.getElementById('doCreate').value = 'true';"/>
			
			<input type="submit" name="doCreate" value="Zapisz i dodaj do sprawy" class="btn"
				onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';document.getElementById('attachCase').value = 'true';" />
		</ww:if>
		<ww:else>
			<input type="submit" name="doUpdateButton" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" <ww:if test="blocked || save">disabled="disabled"</ww:if>
			onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value = 'true';"/>
		</ww:else>
		<ww:if test="#exists && (officeNumber == null || officeNumber.length() < 1) && canAssignOfficeNumber">
			<input type="submit" name="doAssignOfficeNumber" value="<ds:lang text="NadajNumerKancelaryjny"/>"
			class="btn" onclick="setEvent(this)"/>
			
			
		<%-- narazie wylaczone; zamiast tego jest 'doSplit'
			<input type="submit" name="doSplitAndAssignOfficeNumber" value="Podziel na kilka pism i nadaj numer KO"
			class="btn" onclick="document.getElementById('doSplitAndAssignOfficeNumber').value = 'true';"/>  --%>
			
		<ds:available test="epuap"> 
			<ww:if test="#exists">
				<input type="button" class="btn" value="Podpisz" id="btnPodpisz"
				onclick="javascript:window.location='<ww:url value="'/certificates/sign-xml.action?epuap=true&documentId='+documentId+'&returnUrl='+simpleReturnUrl"/>'"/>
				<input type="submit" name="doSendToEpuap" value="<ds:lang text="WyslijDoEpuap"/>" class="btn"
				onclick="if (!validateFormSendToEpuap()) return false; document.getElementById('doSendToEpuap').value = 'true';"/>
			</ww:if>
		</ds:available>
		
		</ww:if>
		<ww:if test="#exists && ( officeNumber == null || officeNumber.length() < 1 )">
			<input type="submit" name="doSplit" value="<ds:lang text="PodzielNaKilkaPism"/>"
			class="btn" onclick="document.getElementById('doSplit').value = 'true';"/>
		</ww:if>
		
<%--
		<ww:if test="documentId != null">
			&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="submit" name="doCancelDocument" value="Anuluj pismo" class="btn"
				onclick="document.getElementById('doCancelDocument').value = 'true';"/>
		</ww:if>
--%>
	</td>
</tr>

<ww:if test="documentId == null && canCreateMultiReplies">
	<tr class="formTableDisabled">
		<td></td>
		<td>
			<input type="submit" name="doMultiCreate" value="<ds:lang text="ZapiszDlaKazdegoOdbiorcyOsobno"/>" class="btn"
			onclick="if (!validateForm()) return false; document.getElementById('createMultiReplies').value = 'true'; document.getElementById('doCreate').value = 'true';"/>
		</td>
	</tr>
	<tr class="formTableDisabled">
		<td></td>
		<td>
			<input type="submit" name="doMultiCreate" value="<ds:lang text="ZapiszDlaKazdegoOdbiorcyOsobnoInadajNumeryKO"/>" class="btn"
			<ww:if test="!canCreateWithOfficeNumber">disabled="true"</ww:if>
			onclick="if (!validateForm()) return false; document.getElementById('createMultiReplies').value = 'true'; document.getElementById('assignOfficeNumber').value = 'true'; document.getElementById('doCreate').value = 'true';"/>
		</td>
	</tr>
</ww:if>
</table>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script type="text/javascript">
hideShowEpuap(document.getElementById('delivery').options[document.getElementById('delivery').selectedIndex].text);
hideShowPodpisz(""); // ukrycie Podpisz

//disableEnableSender(document.getElementById('senderAnonymous'));
	if($j('#documentDate').length > 0)
	{
		Calendar.setup({
			inputField	 :	"documentDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"documentDateTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}

<ww:if test="!daa">
	if($j('#zpoDate').length > 0)
	{
		Calendar.setup({
			inputField	 :	"zpoDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"zpoDateTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
</ww:if>
<%--
<ww:if test="useBarcodes">
var bi = new BarcodeInput({
	'prefix' : '<ww:property value="barcodePrefix"/>',
	'suffix' : '<ww:property value="barcodeSuffix"/>',
	'inputId' : 'barcode',
	'submit' : false,
	//'submitId' : 'send',
	'tabStarts' : false
});
</ww:if>
--%>

var szv = new Validator(document.getElementById('senderZip'), '##-###');

szv.onOK = function() {
	this.element.style.color = 'black';
	document.getElementById('senderZipError').innerHTML = '';
}

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
	this.element.style.color = 'red';
	if (code == this.ERR_SYNTAX)
		document.getElementById('senderZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp."/> 00-001';
	else if (code == this.ERR_LENGTH)
		document.getElementById('senderZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001)';
}

szv.canValidate = function() {
	var country = document.getElementById('senderCountry');
	var countryCode = country.options[country.options.selectedIndex].value;

	return (countryCode == '' || countryCode == 'PL');
}


// pocz�tkowa walidacja danych
szv.revalidate();

function validateFormSendToEpuap()
{
	var deliveryObj = document.getElementById('delivery');
	var objtext = deliveryObj.options[deliveryObj.options.selectedIndex].text;
	if(objtext != 'ePUAP')
	{
		alert('<ds:lang text="SposobDostarczeniaMusiBycRownyEPUAP"/>');
		return false;
	}
	<%--
	if( document.getElementById('selectEpuapMethodId').children[0].value == '<ww:property value="EpuapDoreczyciel"/>' 
			&& ) //dokument.metoda epuap=Doreczenie && dokument.podpisy.pobierzPodpisEpuap == null)
	{
		alert('<ds:lang text="DokumentMusiBycPodpisanyDoWyslaniaEpuap"/>');
		return false;
	}
	--%>

	return true;
}


function validateForm()
{
	var description = document.getElementById('description').value;
	if (description.trim().length == 0) { alert('Nie podano opisu pisma'); return false; }
	if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNajwyzej80znakow"/>'); return false; }

	var recipients = document.getElementById('odbiorcy');
	if (recipients.options.length == 0)
	{
		alert('<ds:lang text="NieWybranoOdbiorcyPisma"/>');
		return false;
	}

	var returned = document.getElementById('returned');
	var returnReasonId = document.getElementById('returnReasonId');

	if (returned && returned.checked && isNaN(parseInt(returnReasonId.value)))
	{
		alert('<ds:lang text="OznaczonoPismoJakoZwroconeAleNieWybranoPrzyczynyZwrotu"/>');
		return false;
	}

	if (returnReasonId && parseInt(returnReasonId.value) > 0 && !returned.checked)
	{
		alert('<ds:lang text="WybranoPrzyczyneZwrotuPismaAleNieOznaczonoPismaJakoZwroconego"/>');
		return false;
	}

	return true;
}

// zmiana widoku formularz prosty/szczegolowy
/*function changeView()
{
	var elems = document.getElementsByName("extended");
	var i;
	var link = document.getElementById("changeViewLink");
	for (i = 0; i < elems.length; i++)
	{
		if (!isSimpleView) {
			elems[i].style.display = "none";
			link.firstChild.nodeValue = "<ds:lang text="PokazPelnyWidokFormularza"/>";
		}
		else {
			elems[i].style.display = "table-row";
			link.firstChild.nodeValue = "<ds:lang text="PokazUproszczonyWidokFormularza"/>";
		}
	}
	if (isSimpleView)
		isSimpleView = false;
	else
		isSimpleView = true;
}*/

var countAtta = 1;
function addAtta() 
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);
}

function delAtta() 
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;
	/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}

</script>


