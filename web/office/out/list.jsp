<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N list.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h3><ds:lang text="PismaWychodzaceWprzygotowaniu"/></h3>

<ww:if test="!documents.empty">

<table>
    <tr>
        <th><ds:lang text="DataUtworzenia"/></th>
        <th><ds:lang text="Opis"/></th>
    </tr>

<ww:iterator value="documents">

    <tr>
        <ww:set name="ctime" scope="page" value="ctime"/>
        <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy"/></td>
        <td><ww:property value="description"/></td>
    </tr>

</ww:iterator>

</table>

</ww:if>