<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main-reply-created.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<%--<form action="<ww:url value='&apos;/office/outgoing/main.action&apos;'/>" method="post" onsubmit="return disableFormSubmits(this);">--%>

<p></p>

<form action="<ww:url value='&apos;/office/outgoing/main.action&apos;'/>" method="post" enctype="multipart/form-data" onsubmit="selectAllOptions(document.getElementById('odbiorcy')); return disableFormSubmits(this);">
    <input type="hidden" name="doNewDocument" id="doNewDocument" value="true"/>
    <input type="hidden" name="nextReply" value="true"/>
    <ww:hidden name="'inDocumentId'" value="inDocumentId"/>
    <ww:hidden name="'documentDate'" value="documentDate"/>
    <ww:hidden name="'description'" value="description"/>
    <ww:hidden name="'caseDocumentId'" value="caseDocumentId"/>
    <ww:hidden name="'barcode'" value="barcode"/>
    <ww:hidden name="'prepState'" value="prepState"/>
    <ww:hidden name="'deliveryId'" value="deliveryId"/>
    <ww:hidden name="'senderTitle'" value="senderTitle"/>
    <ww:hidden name="'senderFirstname'" value="senderFirstname"/>
    <ww:hidden name="'senderLastname'" value="senderLastname"/>
    <ww:hidden name="'senderOrganization'" value="senderOrganization"/>
    <ww:hidden name="'senderStreetAddress'" value="senderStreetAddress"/>
    <ww:hidden name="'senderLocation'" value="senderLocation"/>
    <ww:hidden name="'senderZip'" value="senderZip"/>
    <ww:hidden name="'senderCountry'" value="senderCountry"/>
    <ww:hidden name="'senderEmail'" value="senderEmail"/>
    <ww:hidden name="'senderFax'" value="senderFax"/>
    <ww:hidden name="'postalRegNumber'" value="postalRegNumber"/>
    <ww:hidden name="'zpoDate'" value="zpoDate"/>
    <ww:hidden name="'stampFee'" value="stampFee"/>
    <ww:hidden name="'returned'" value="returned"/>
    <ww:hidden name="'returnReasonId'" value="returnReasonId"/>



<script type="text/javascript">
    function submitForm()
    {
        document.forms[0].submit();
    }
</script>


<%-- <input type="button" value="Utw�rz kolejn� odpowied� na pismo" class="btn"
    onclick="document.location.href='<ww:url value="'/office/outgoing/main.action?doNewDocument=true&inDocumentId='+inDocumentId"/>';"/>  --%>
<input type="button" name="createdReply" value="<ds:lang text="PrzejdzDoUtworzonegoPisma"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/outgoing/summary.action'"><ww:param name="'documentId'" value="newDocumentId"/></ww:url>';"/>

<input type="button" value="<ds:lang text="UtworzKolejnaOdpowiedzNaPismo"/>" class="btn"
    onclick="javascript:submitForm()"/>

<input type="button" name="incomingDoc" value="<ds:lang text="PowrocDoPismaPrzychodzacego"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/incoming/summary.action'"><ww:param name="'documentId'" value="inDocumentId" /> <ww:param name="'activity'" value="inActivity"/> </ww:url>';"/>

<input type="button" value="<ds:lang text="StronaGlowna"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/'"/>';"/>	
</form>																						
<%--</form>--%>
