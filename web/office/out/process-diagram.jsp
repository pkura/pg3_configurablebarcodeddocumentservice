<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
    </ww:iterator>
</ds:available>

<ds:available test="layout2">
    <div id="middleMenuContainer">
        <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
        <ww:iterator value="tabs" status="status" >
            <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
            <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
        </ww:iterator>
    </div>
</ds:available>

<ds:available test="layout2">
    <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/incoming/process-diagram.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ww:iterator value="diagrams">
        <h3><ww:property value="name"/> (<ww:property value="info"/>)</h3>
        <img id="barcodeImage" src="<ww:property value="imgLink"/>" />
    </ww:iterator>
</form>