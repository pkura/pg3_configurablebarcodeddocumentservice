<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main-split.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="button" value="<ds:lang text="PrzejdzNaListeZadan"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/tasklist/current-user-task-list.action?tab=out'"/>'"/>