<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N ald-outgoing-invoices-import.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="ImportPismKancelaryjnych"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/office/outgoing/ald-outgoing-invoices-import.action'"/>" method="post" enctype="multipart/form-data">
    <table>
		<tr>
			<td><ds:lang text="RodzajImportu"/>:</td>
			<td><ww:select id="typimportu" name="'typimportu'" onchange="'showTiffsBox(this)'"  cssClass="'sel'" list="importTypes" listKey="importCode" listValue="importName" headerKey="''" headerValue="getText('select.wybierz')"/></td>
		</tr>
		<tr id="id_tr_tiff" style="display:none">
            <td><ds:lang text="KatalogZTiffami"/>:</td>
            <td><ww:textfield id="tiffFolder" name="'tiffFolder'" size="67"/></td>
        </tr>
        <tr>
            <td><ds:lang text="PlikZPlatnosciami"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr id="id_tr_journal" style="display:none">
            <td><ds:lang text="Dziennik"/>:</td>
            <td><ww:select id="journalId" name="'journalId'" cssClass="'sel'" list="journals" listKey="id" listValue="description+' '+typeDescription" headerKey="''" headerValue="getText('select.wybierz')"/></td>
        </tr>
        <tr id="id_tr_date" style="display:none">
            <td><ds:lang text="Data"/>:</td>
            <td>
            <ww:textfield id="dataPrzyjecia" name="'dataPrzyjecia'" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>" id="dataPrzyjeciaTrigger" style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
            <script type="text/javascript">
				Calendar.setup({
					inputField	:	"dataPrzyjecia",			// id of the input field
					ifFormat	:	"%d-%m-%Y",				// format of the input field
					button		:	"dataPrzyjeciaTrigger",	// trigger for the calendar (button ID)
					align		:	"Bl",					// alignment (defaults to "Bl")
					singleClick	:	true
				});
			</script>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>

<script type="text/javascript">
	function showTiffsBox(field)
	{
		var imps = new Array();
		var journalImps = new Array();
		var date = new Array();
		<ww:iterator value="importTypes">
			imps['<ww:property value="importCode"/>'] = new Boolean(<ww:property value="needTiffDir"/>);
			journalImps['<ww:property value="importCode"/>'] = new Boolean(<ww:property value="needJournals"/>);
			date['<ww:property value="importCode"/>'] = new Boolean(<ww:property value="needDate"/>);
		</ww:iterator>

		if(imps[field.value] == true)
		{	
			document.getElementById('id_tr_tiff').style.display = "";
		}
		else
		{
			document.getElementById('id_tr_tiff').style.display = "none";
		}

		
		if(journalImps[field.value] == true)
		{	
			document.getElementById('id_tr_journal').style.display = "";
		}
		else
		{
			document.getElementById('id_tr_journal').style.display = "none";
		}

		
		if(date[field.value] == true)
		{	
			document.getElementById('id_tr_date').style.display = "";
		}
		else
		{
			document.getElementById('id_tr_date').style.display = "none";
		}
	}
</script>