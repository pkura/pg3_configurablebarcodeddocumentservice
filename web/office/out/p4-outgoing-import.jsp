<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="ImportPismKancelaryjnych"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/office/outgoing/p4-outgoing-import.action'"/>" method="post" enctype="multipart/form-data">
    <table>
        <tr>
            <td><ds:lang text="Spolka"/>:</td>
            <td><ww:select name="'subJournalId'" list="journals" headerKey="''" headerValue="getText('select.wybierz')" value="subJournalId" id="subJournalId" cssClass="'sel'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Plik"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doImport'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
        </tr>
    </table>
</form>

<script type="text/javascript">

</script>