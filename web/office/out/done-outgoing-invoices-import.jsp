<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N ald-outgoing-invoices-import.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="ImportPismWychodzacych"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />



<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/office/outgoing/ald-outgoing-invoices-import.action'"/>" method="post">

	<ww:hidden name="reportName"/>
	
    <table>
        <tr>
            <ds:lang text="ZakonczonoImport"/>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doShowReport'" value="getText('PokazRaport')" cssClass="'btn'"/></td>
        </tr>
    </table>

</form>
