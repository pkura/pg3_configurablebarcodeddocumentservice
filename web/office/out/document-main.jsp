<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N document-main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" pageEncoding="iso-8859-2"  %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>



<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">

// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
// jako anonimowego
var senderFields = new Array(
    'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
    'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
    'senderStreetAddress');

// wype�nia bie��c� dat� i czasem pola informuj�ce o dacie przyj�cia dokumentu
function fillToday()
{
    var incomingDate = document.getElementById('incomingDate');
    var incomingTime = document.getElementById('incomingTime');
    var d = new Date();
    var year = d.getYear() >= 1900 ? d.getYear() : 1900 + d.getYear();

    incomingDate.value = (d.getDate() < 10 ? '0'+d.getDate() : d.getDate()) +
        '-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1) +
        '-'+year;
    incomingTime.value = (d.getHours() < 10 ? '0'+d.getHours() : d.getHours()) +':'+
        (d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes());
}

// funkcja wywo�ywana przez okienko z people-directory-results.jsp
function __people_directory_pick(addresseeType, title, firstname, lastname,
    organization, organizationDivision, street, zip, location, country, email, fax)
{
    if (addresseeType == 'sender')
    {
        document.getElementById('senderTitle').value = title;
        document.getElementById('senderFirstname').value = firstname;
        document.getElementById('senderLastname').value = lastname;
        document.getElementById('senderOrganization').value = organization;
        document.getElementById('senderDivision').value = organizationDivision;
        document.getElementById('senderStreetAddress').value = street;
        document.getElementById('senderZip').value = zip;
        document.getElementById('senderLocation').value = location;
        document.getElementById('senderCountry').value = country;
        document.getElementById('senderEmail').value = email;
        document.getElementById('senderFax').value = fax;
    }
    else if (addresseeType == 'recipient')
    {
        document.getElementById('recipientTitle').value = title;
        document.getElementById('recipientFirstname').value = firstname;
        document.getElementById('recipientLastname').value = lastname;
        document.getElementById('recipientOrganization').value = organization;
        document.getElementById('recipientDivision').value = organizationDivision;
        document.getElementById('recipientStreetAddress').value = street;
        document.getElementById('recipientZip').value = zip;
        document.getElementById('recipientLocation').value = location;
        //document.getElementById('recipientCountry').value = country;
        document.getElementById('recipientEmail').value = email;
        document.getElementById('recipientFax').value = fax;
    }
}

// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
    'streetAddress', 'zip', 'location', 'country', 'email', 'fax');

// lparam - identyfikator
function openPersonPopup(lparam, dictionaryType)
{
    openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true', 'person', 700,650);
}

// lparam - identyfikator
// wparam - pozycja na li�cie
function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
{
    //alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
    var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
    //alert('url='+url);
    openToolWindow(url, 'person', 700,650);
}

function openGroupPopup()
{
    openToolWindow('<ww:url value="'/office/common/person-group.action'"/>', 'person',700,650);
}

function personSummary(map)
{
    var s = '';
    if (map.title != null && map.title.length > 0)
        s += map.title;

    if (map.firstname != null && map.firstname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.firstname;
    }

    if (map.lastname != null && map.lastname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.lastname;
    }

    if (map.organization != null && map.organization.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.organization;
    }
    if(map.organizationDivision !=null && map.organizationDivision.length>0)
    {
    	if (s.length > 0) s += ' / ';
        s += map.organizationDivision;
    }

    if (map.street != null && map.street.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.street;
    }

    if (map.location != null && map.location.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.location;
    }

    return s;
}

    /*
function __accept_person_duplicate(map, lparam, wparam)
{
    if (lparam == 'recipient')
    {
        var sel = document.getElementById('odbiorcy');

        for (var i=0; i < sel.options.length; i++)
        {
            eval('var smap='+sel.options[i].value);
            if (compareArrays(smap, map, true))
            {
                //alert('Istnieje ju� taki odbiorca');
                return true;
            }
        }
    }

    return false;
}     */

/*
    Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
    if (lparam == 'recipient')
    {
        var sel = document.getElementById('odbiorcy');

        //if (isEmpty(map.firstname) && isEmpty(map.lastname) && isEmpty(map.organization))
        //    return;
        //addOption(sel, JSONStringify(map), personSummary(map));

        if (wparam != null && (''+wparam).length > 0)
        {
            setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
        }
        else
        {
            addOption(sel, JSONStringify(map), personSummary(map));
        }
    }
    else if (lparam == 'sender')
    {
        document.getElementById('senderTitle').value = map.title;
        document.getElementById('senderFirstname').value = map.firstname;
        document.getElementById('senderLastname').value = map.lastname;
        document.getElementById('senderOrganization').value = map.organization;
        document.getElementById('senderStreetAddress').value = map.street;
        document.getElementById('senderZip').value = map.zip;
        document.getElementById('senderLocation').value = map.location;
        document.getElementById('senderCountry').value = map.country;
        document.getElementById('senderEmail').value = map.email;
        document.getElementById('senderFax').value = map.fax;
    }
}

function __accept_group(group)
{
    document.getElementById('doAddGroupToRecipients').value = 'true';
    document.getElementById('group').value = group;
    selectAllOptions(document.getElementById('odbiorcy'));

    document.forms[0].submit();
}

function markReturned()
{
    document.getElementById('returned').checked = !isNaN(parseInt(document.getElementById('returnReasonId').value));
}

</script>

<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>

<form id="form" action="<ww:url value='baseLink'/>" method="post" enctype="multipart/form-data"
    onsubmit="selectAllOptions(document.getElementById('odbiorcy')); return disableFormSubmits(this);">

    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="createNextDocument" id="createNextDocument"/>
    <input type="hidden" name="doAddGroupToRecipients" id="doAddGroupToRecipients"/>
    <input type="hidden" name="group" id="group"/>
    <ww:hidden name="'inDocumentId'"/>
	<ww:hidden name="'boxId'"/>
	<ww:hidden name="'filestoken'" value="filestoken"/>
<table border="0" class="formTable" >
<tr>
    <td><ds:lang text="DataPisma"/>:</td>
    <td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
    </td>
</tr>
<tr>
    <td valign="top"><ds:lang text="OpisPisma"/>:</td>
    <td><ww:textarea name="'description'" rows="2" cols="50" cssClass="'txt'" id="description"/></td>
<%--    <td><ww:textfield name="'description'" size="50" maxlength="80" cssClass="'txt'" id="description"/></td>--%>
</tr>
<%--
<ww:if test="!internal">
    <tr>
        <td>Stan pisma:</td>
        <td><ww:select name="'prepState'" list="#@java.util.LinkedHashMap@{ @pl.compan.docusafe.core.office.OutOfficeDocument@PREP_STATE_DRAFT: getText('Brudnopis'), @pl.compan.docusafe.core.office.OutOfficeDocument@PREP_STATE_FAIR_COPY: getText('Czystopis') }" cssClass="'sel'" value="prepState" /></td>
    </tr>
</ww:if>
--%>
<tr>
    <td><ds:lang text="SposobOdbioru"/>:</td>
    <td><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
        headerKey="''"
        value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/></td>
</tr>


<ww:if test="!internal">

	<tr>
	   	<td><ds:lang text="Waga"/></td>
	   	<td>
	   		<ww:textfield name="'weightKg'" size="10" maxlength="10" cssClass="'txt'" id="weightKg" readonly="readOnly" />
	   		kg.
	    	<ww:textfield name="'weightG'" size="10" maxlength="10" cssClass="'txt'" id="weightG" readonly="readOnly" />
	    	g.
	    </td>
    </tr>
    <tr>
    	<td><ds:lang text="Priorytet"/></td>
    	<td><ww:checkbox name="'priority'"  fieldValue="true" id="priority" value="true"></ww:checkbox></td>

    </tr>
    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="Nadawca"/></h4></td>

    </tr>
    <tr>
        <td><ds:lang text="TytulStanowisko"/>:</td>
        <td><ww:textfield name="'senderTitle'" size="25" maxlength="30" cssClass="'txt'" id="senderTitle" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Imie"/>:</td>
        <td><ww:textfield name="'senderFirstname'" size="50" maxlength="50" cssClass="'txt'" id="senderFirstname" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Nazwisko"/>:</td>
        <td><ww:textfield name="'senderLastname'" size="50" maxlength="50" cssClass="'txt'" id="senderLastname" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Firma/Urzad"/>:</td>
        <td><ww:textfield name="'senderOrganization'" size="50" maxlength="50" cssClass="'txt'" id="senderOrganization" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Ulica"/>:</td>
        <td><ww:textfield name="'senderStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="senderStreetAddress" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td valign="top"><ds:lang text="KodPocztowyImiejscowosc"/>:</td>
        <td><ww:textfield name="'senderZip'" size="7" maxlength="14" cssClass="'txt dontFixWidth'" id="senderZip" readonly="readOnly" />
            <ww:textfield name="'senderLocation'" size="39" maxlength="50" cssClass="'txt dontFixWidth'" id="senderLocation" readonly="readOnly" onchange="'showCode();'" 
            	cssStyle="'width: 231px;'"/>
            <br/><span class="warning" id="senderZipError"> </span></td>
    </tr>
    <tr>
        <td><ds:lang text="Kraj"/> <%--(<c:out value="${incomingDocument.senderCountry}"/>)--%>:</td>
        <td><ww:select name="'senderCountry'"
            list="@pl.compan.docusafe.util.Countries@COUNTRIES"
            listKey="alpha2" listValue="name" value="'PL'"
            id="senderCountry" cssClass="'sel'" onchange="this.form.senderZip.validator.revalidate();"
            disabled="readOnly"/>
        </td>
    </tr>

    <tr>
        <td><ds:lang text="Email"/>:</td>
        <td><ww:textfield name="'senderEmail'" size="50" maxlength="50" cssClass="'txt'" id="senderEmail" readonly="readOnly" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Faks"/>:</td>
        <td><ww:textfield name="'senderFax'" size="30" maxlength="30" cssClass="'txt'" id="senderFax" readonly="readOnly" /></td>
    </tr>
    <tr class="formTableDisabled">
        <td></td>
        <td><ww:if test="!#exists"><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn"></ww:if></td>
    </tr>

    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="Odbiorcy"/> <span class="star">*</span></h4></td>
    </tr>
    <tr  class="formTableDisabled">
        <td colspan="2">
            <ww:select name="'recipients'" id="odbiorcy" cssStyle="'width: 500px;'" multiple="true" size="5" cssClass="'multi_sel dontFixWidth'"
                list="recipientsMap" />
                <script type="text/javascript">
					$j(document).ready(function(event) {
						var sel_el = $j('#odbiorcy');
						var minSize = 5;
						var maxSize = 15;

					    sel_el.data("size", $j("#odbiorcy option").size());
					    if(sel_el.children().length > minSize && sel_el.children().length < maxSize)
						    sel_el.attr('size', sel_el.children().length);

					    $j("#odbiorcy option").livequery(function() {
					        if ($j("#odbiorcy option").size() > sel_el.data("size") && sel_el.data("size") >= minSize) {
					  			sel_el.attr('size', $j("#odbiorcy option").size());
								//alert('dodaje odbiorce: size=' + sel_el.data("size"));
					  			if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
					  		}

					        sel_el.data("size", $j("#odbiorcy option").size());
					    }, function() {
							  if ($j("#odbiorcy option").size() < sel_el.data("size")) {
								  sel_el.attr('size', sel_el.data("size")-1);
								  if(sel_el.data("size") <= minSize)
							  			sel_el.attr('size', minSize);

								if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
							  }
					        sel_el.data("size", $j("#odbiorcy option").size());
						 });
					});
				</script>
            <ww:if test="!readOnly">
                <tr><td colspan="2" class="btnContainer" align="right">
                <input name="addRecipient" type="button" value="<ds:lang text="DodajOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_SENDER %>');" class="btn">
                <input type="button" value="<ds:lang text="DodajGrupe"/>" onclick="openGroupPopup();" class="btn">
                <input type="button" value="<ds:lang text="EdycjaOdbiorcy"/>" onclick="var odb=document.getElementById('odbiorcy'); openEditPersonPopup(odb.options[odb.options.selectedIndex].value, odb.options.selectedIndex, 'recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn">
                <input type="button" value="<ds:lang text="UsunOdbiorcow"/>" onclick="deleteSelectedOptions(document.getElementById('odbiorcy'));" class="btn">
                </td></tr>
            </ww:if>
        </td>
    </tr>
</ww:if>

<ww:if test="!internal">
    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="Inne"/></h4></td>
    </tr>

    <tr>
        <td valign="top"><ds:lang text="NrPrzesylkiRejestrowanej"/> (R):</td>
        <td><ww:textfield name="'postalRegNumber'" size="20" maxlength="20" cssClass="'txt'" id="postalRegNumber" />
        </td>
    </tr>
</ww:if>

<tr>
    <td>
        <ds:lang text="RodzajDokumentuDocusafe"/>:
    </td>
    <td>
        <ds:degradable-select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="cn" listValue="name" value="documentKindCn" cssClass="'sel'"
            onchange="''"/>
            
        <script type="text/javascript">
                $j(function(){
                    $j('#documentKindCn [value=wf]').remove();
                    $j('#documentKindCn [value=absence-request]').remove();
                    $j('#documentKindCn [value=wykaz]').remove();
                    $j('#documentKindCn [value=protokol_br]').remove();
                });
                
        	$j('#documentKindCn').change(function(){
                        documentKindVal = $j('#documentKindCn').val();
        		if(documentKindVal == 'absence-request' || documentKindVal == 'wf') {
					alert('To Pismo mo�e by� tylko Pismem Wewn�trznym!');
					return false;
        		}
                        changeDockind();
           	});
        </script>
    </td>
</tr>
<ww:if test="documentAspects != null">
		<tr>
			<td>
				<ds:lang text="AspektDokumentu"/>:
			</td>
			<td>
				<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
			</td>
		</tr>
</ww:if>


<ww:if test="!normalKind">
    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="DaneArchiwum"/></h4></td>
    </tr>
</ww:if>

<jsp:include page="/common/dockind-fields.jsp"/>

<ww:if test="canAddToRS">
<tr>
    <td><ds:lang text="NumerSzkody"/>:</td>
    <td><ww:textfield name="'NR_SZKODY'" id="NR_SZKODY" size="30" maxlength="30" cssClass="'txt"/></td>
</tr>
</ww:if>
<tr>
<ds:available test="documentMain.int.ArchiwizacjaWOtwartymPudle"  documentKindCn="documentKindCn">
    <td><ds:lang text="ArchiwizacjaWOtwartymPudle"/><ww:if test="boxId != null"> (<ww:property value="boxName"/>)</ww:if>:</td>
    <td><ww:checkbox name="'addToBox'" fieldValue="true" disabled="boxId == null"/></td>
</tr>
</ds:available>
<ww:iterator value="loadFiles">
	<tr>
		<td colspan="1">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
		</td>
	</tr>
</ww:iterator>
<tr class="formTableDisabled">
	<td colspan="2">
	<table id="kontener">
		<tbody id="tabela_glowna">
			<tr>
				<td><ds:lang text="Plik" /><span class="star always">*</span>:</td>
			</tr>
			<tr>
				<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
			</tr>
		</tbody>
	</table>
	</td>
</tr>
<tr class="formTableDisabled">
	<td><a href="javascript:addAtta();"><ds:lang
		text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
</tr>
<jsp:include page="/common/dockind-specific-additions.jsp"/>

<tr class="formTableDisabled">
    <td></td>
    <td>
        <p></p>
        <input type="submit" name="doCreate1" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
        onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';"/>

	<ww:if test="showCreateNext">
	        <input type="submit" name="doCreate2" value="<ds:lang text="ZapiszNext"/>" class="btn saveBtn"
	        onclick="$j('#form').unbind('submit');if (!validateForm()) return false; document.getElementById('doCreate').value = 'true'; document.getElementById('createNextDocument').value='true';" />
	</ww:if>
    </td>
</tr>
</table>

<p><span class="star always">*</span><ds:lang text="PoleObowiazkowe"/></p>

</form>

<script type="text/javascript">
if($j('#documentDate').length > 0)
{
	Calendar.setup({
	    inputField     :    "documentDate",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

<ww:if test="!internal">
var szv = new Validator(document.getElementById('senderZip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('senderZipError').innerHTML = '';
    if (document.getElementById('senderZip').value != "")
    {
		showLocation();
	}
}

//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************ //
		//   |                  |			       |	   //
		//   |			        |			       |       //
		//   |			        |			       |       //
		//  \/			       \/			      \/       //

		var xmlHttp = null;
		var serverName = '<%= request.getServerName() + ":" %>';
		var port = '<%= request.getServerPort() %>';
		var contextPath = '<%= request.getContextPath() %>';
		var protocol = window.location.protocol;

		var polskieZnaki = new Array();
		var zamiana = new Array();

		function showCode()
		{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			var location=document.getElementById('senderLocation').value;

			//Rozwiazanie problemu z polskimi znakami
			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			for(i = 0; i < 18; i++)
			{
				while (location.indexOf(polskieZnaki[i])>-1)
				{
					pos= location.indexOf(polskieZnaki[i]);
					location = "" + (location.substring(0, pos) + zamiana[i] +
					location.substring((pos + polskieZnaki[i].length), location.length));
				}
			}


			if(location==null)
			{
				alert ("No element with id 'location' found on the page");
				return;
			}
			//alert("http://" + serverName + port + contextPath + "/code.ajx?location=" + location);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/code.ajx?location=" + location,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('senderZip');
			};

			xmlHttp.send(null);

		}

		function showLocation()
		{

			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			var zip=document.getElementById('senderZip').value;

			if(zip==null)
			{
				alert ("No element with id 'zip' found on the page");
				return;
			}

			//alert("http://" + serverName + port + contextPath + "/location.ajx?zip=" + zip);
			xmlHttp.open("GET",  protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('senderLocation');
			};

			xmlHttp.send(null);
		}

		function stateChanged(variable)
		{

			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			if (xmlHttp.readyState==4)
			{

				if(xmlHttp.status==200)
				{

					var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];

					if(valid.childNodes[0].nodeValue=='LocNotFound' && variable =='senderZip')
					{
						document.getElementById('senderZipError').innerHTML = "Nie znalaz�em kodu dla tej miejscowo�ci.";
						//document.getElementById(variable).value = '';
						//document.getElementById('senderLocation').value = '';
					}
					else
					{
						var message = valid.childNodes[0].nodeValue;
						for(i = 0; i < 18; i++)
						{
							while (message.indexOf(zamiana[i])>-1)
							{
								pos= message.indexOf(zamiana[i]);
								message = "" + (message.substring(0, pos) + polskieZnaki[i] +
								message.substring((pos + zamiana[i].length), message.length));
							}
						}
					}
					if(valid.childNodes[0].nodeValue=='CodeNotFound' && variable =='senderLocation')
					{
						document.getElementById('senderZipError').innerHTML = "Nie znalaz�em miejscowo�ci dla danego kodu."
						document.getElementById(variable).value='';
					}
					if(message.trim().length ==6)
					{
						document.getElementById(variable).value=message;
					}
					else if (message.trim().length >6 && variable=='senderZip')
					{
						document.getElementById('senderZipError').innerHTML = "Nie ma jednego kodu pocztowego dla danej miejscowo�ci.<br/>Zakres(y) to: " + message + ".";
						document.getElementById('senderZip').value=message.substring(0, 6);
					}
					else if(variable=='senderLocation' && valid.childNodes[0].nodeValue!='CodeNotFound')
					{
						document.getElementById(variable).value=message;
					}


				}
				else
				{
					alert("Error loading page"+ xmlHttp.status +":"+ xmlHttp.statusText);
				}
			}
			else  if (xmlHttp.readyState==0)
			{
				alert("	Object is uninitialized");
			}

		}

		function GetXmlHttpObject()
		{
			var xmlHttp=null;
			try
			{
			// Firefox, Opera 8.0+, Safari
				xmlHttp=new XMLHttpRequest();
			}
			catch (e)
			{
			//Internet Explorer
				try
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e)
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			return xmlHttp;
		}
		//*******************************//

		//   /\                          /\			      /\     //
		//   |			        |			       |     //
		//   |			        |			       |     //
		//   |		                 |			       |     //
		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************//

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp."/> 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp."/> 00-001';
}

szv.canValidate = function() {
    var country = document.getElementById('senderCountry');
    var countryCode = country.options[country.options.selectedIndex].value;

    return (countryCode == '' || countryCode == 'PL');
}


// pocz�tkowa walidacja danych
szv.revalidate();
</ww:if>

function validateForm()
{
    var description = document.getElementById('description').value;
    //if (description.trim().length == 0) { alert('<ds:lang text="NiePodanoOpisuPisma"/>'); return false; }
    if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNajwyzej80znakow"/>'); return false; }

    <ww:if test="!internal">
    var recipients = document.getElementById('odbiorcy');
    if (recipients.options.length == 0)
    {
        alert('<ds:lang text="NieWybranoOdbiorcy"/>');
        return false;
    }
    </ww:if>

    if (!validateDockind())
        return false;

    if (!validateFiles())
        return false;

    return true;
}
var countAtta = 1;
function addAtta()
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);

}

function delAtta()
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;

	/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}

</script>


<%-- JE�LI DOCKIND == 'absence-request' WYPISUJE LISTE URLOP�W PRACOWNIKA / ROZWI�ZANIE TYMCZASOWE --%>
<jsp:include page="/common/employee-absences.jsp" />
