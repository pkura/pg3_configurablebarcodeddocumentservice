<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-orders.jsp N-->

<%@ page import="java.text.DateFormat,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Wyszukiwanie polece�</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
    <form name="formul" action="<ww:url value="'/office/find-orders.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
					<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
        <table>
        <tr>
            <td>Numer polecenia:</td>
            <td>
                <ww:textfield name="'officeNumber'" size="4" maxlength="5" cssClass="'txt'"/>
            </td>
        </tr>
        <tr>
            <td>Rok:</td>
            <td><ww:textfield name="'officeNumberYear'" size="4" maxlength="4" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td colspan="2"><hr/></td>
        </tr>
        <tr>
            <td>Zakres numer�w polecenia:</td>
            <td>
                od <ww:textfield name="'fromOfficeNumber'" size="4" maxlength="5" cssClass="'txt'"/>
                do <ww:textfield name="'toOfficeNumber'" size="4" maxlength="5" cssClass="'txt'"/>
            </td>
        </tr>
        <tr>
            <td>Data polecenia:</td>
            <td>
                <ww:textfield name="'documentDateFrom'" id="documentDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="calendar_documentDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>

                <ww:textfield name="'documentDateTo'" id="documentDateTo" size="10" maxlength="10" cssClass="'txt'"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="calendar_documentDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td>Opis polecenia:</td>
            <td><ww:textfield name="'summary'" size="30" maxlength="80" cssClass="'txt'"/></td>
        </tr>

        <tr>
            <td>Autor:</td>
            <td>
                <ww:select name="'creatingUser'" cssClass="'sel'" list="users"
                    listKey="name" listValue="lastname+' '+firstname"
                    headerKey="''" headerValue="'-- dowolna --'"/>
            </td>
        </tr>
        <tr>
            <td>Referent:</td>
            <td>
                <ww:select name="'clerk'" cssClass="'sel'" list="users"
                    listKey="name" listValue="lastname+' '+firstname"
                    headerKey="''" headerValue="'-- dowolna --'"/>
            </td>
        </tr>
        <tr>
            <td>Status pisma:</td>
            <td><ww:select name="'status'" list="statusMap" 
            	listKey="key" listValue="value" value="status"
                headerKey="''" headerValue="'-- dowolny --'"
                id="status" cssClass="'sel'"/></td>
        </tr>
        <tr>
            <td>Priorytet:</td>
            <td><ww:select name="'priority'" list="priorityMap" 
            	listKey="key" listValue="value" value="priority"
                headerKey="''" headerValue="'-- dowolny --'"
                id="priority" cssClass="'sel'"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <ds:submit-event value="'Szukaj'" name="'doSearch'"/>
				<%--<input type="submit" name="doSearchIn" class="btn" value="Szukaj"/>--%>
				<input type="button" id="clearAllFields" name="clearAllFields" class="btn" value="<ds:lang text='WyczyscWszystkiePola'/>" onclick="clearForm();return false;"/>
            </td>
        </tr>
        </table>
  <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
    </form>

    <script type="text/javascript">


        Calendar.setup({
            inputField     :    "documentDateFrom",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_documentDateFrom_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
        Calendar.setup({
            inputField     :    "documentDateTo",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_documentDateTo_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
        
        function clearForm()
        {
	        $j('input[type=text]').val('');
	        $j('select').val('');
        }

    </script>

</ww:if>

<ww:else>
					<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table width="100%" class="search">
        <tr>
            <th width="18%">
                <a href="<ww:url value="getSortLink('officeNumber', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
                Nr polecenia
                <a href="<ww:url value="getSortLink('officeNumber', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
            </th>
            <th width="18%">
                <a href="<ww:url value="getSortLink('creatingUser', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
                Autor
                <a href="<ww:url value="getSortLink('creatingUser', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
           </th>
            <th width="18%">
                <a href="<ww:url value="getSortLink('summary', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>
                Opis
                <a href="<ww:url value="getSortLink('summary', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
            </th>
            <th width="18%">Status</th>
            <th width="18%">Priorytet</th>
        </tr>
        <ww:iterator value="results">
        	<tr>
        		<td><a href="<ww:url value="'/office/order/summary.action?documentId='+id"/>"><ww:property value="officeNumber"/></a></td>    	
        		<td><a href="<ww:url value="'/office/order/summary.action?documentId='+id"/>"><ww:property value="getUserFirstnameLastname(creatingUser)"/></a></td>        	
        		<td><a href="<ww:url value="'/office/order/summary.action?documentId='+id"/>"><ww:property value="summary"/></a></td>        
        		<td><a href="<ww:url value="'/office/order/summary.action?documentId='+id"/>"><ww:property value="status"/></a></td>
        		<td><a href="<ww:url value="'/office/order/summary.action?documentId='+id"/>"><ww:property value="priority"/></a></td>
        	</tr>        
        </ww:iterator>
    </table>
  <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>


</ww:else>







