<%@ page import="pl.compan.docusafe.core.office.barcode.Package"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="WprowadzonePaczki"/></h1>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="PackageIn"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PackageIn"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
 	
<form action="<ww:url value="'/office/package-verify.action'"/>" method="post">
	<ds:available test="layout2">
		<div id="middleContainer" style="border-top: none;"> <!-- BIG TABLE start -->
		</ds:available>	 	
	<hr size="1" align="left" class="horizontalLine" width="77%" />

<p id="pIEFix">
	<h4><ds:lang text="PaczkiWprowadzonePrzezZalogowanegoUzytkownika"/></h4>
</p>
<ww:hidden name="'packageId'" id="id" value="packageId"/>
<p id="pIEFix">
	<b>Paczka numer :</b> <ww:property value="numer"/> , <b>Status : </b> <ww:property value="packageStatus"/> 
</p>
<ww:include page="/office/package-param.jsp"/>
<ww:if test="status < 2 || status > 3">
	<ds:event name="'doSave'" value="'Zapisz'"/>
	<ds:event name="'doFinish'" value="getText('ZakonczWeryfikacje')"/>
</ww:if>

<table id="titleTable" class="tableMargin">		
		<tr>
			<td align="center">
				<b>Numer pozycji</b>
			</td>
			<td>
				<b>Barkod</b>
			</td>
			<td>
				<b>Status</b>
			</td>
		</tr>
	<ww:iterator value="pac.getPackageElements()">
		<ww:hidden name="'elementIds'" value="id"></ww:hidden>
		<tr>
			<td align="center">
				<ww:property value="kod"/>
			</td>
			<td>			
				<ww:property value="barcode"/>			
			</td>
			<td>			
				<ww:select name="'statusy'" list="statusMap" cssClass="'sel'" value="status"/>
			</td>
		</tr>
	</ww:iterator>
</table>


	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>

<script type="text/javascript">
	prepareTable(E("titleTable"),1,0);

	function checkStatuses() {
        var statusy = document.getElementsByName('statusy');
        var count = 0;
        for (var i=0; i < statusy.length; i++)
        {
            if (statusy[i].selectedIndex == 3) // niezweryfikowana
            {
                count++;
            }
        }        
        if (count > 0) {
           var result  = confirm(count + ' elementy maja status niezweryfikowana. Czy zamienic wpisy na zgodna');
           if (result) {
               for (var i=0; i < statusy.length; i++)
               {
                   if (statusy[i].selectedIndex == 3) // niezweryfikowana
                   {
                	   statusy[i].selectedIndex = 0; // zgodna
                   }
               }
           }
        }
	}

</script>