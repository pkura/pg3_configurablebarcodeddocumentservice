<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-document.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<h3>Nowy dokument</h3>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" enctype="multipart/form-data">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />

    <table>
    <tr>
        <td>Nazwa procesu</td>
        <td><b><ww:property value="jbpmProcessName"/></b></td>
    </tr>
    <jsp:include page="/office/workflow/common/dok-info.jsp"/>
    <ww:if test="!cannotCreateDocument">
        <tr>
            <td>Data</td>
            <td>
                <ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td>List do klienta</td>
            <td><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></td>
        </tr>
        <ww:if test="additionalAttachment">
            <tr>
                <td><ww:property value="additionalAttachmentName"/></td>
                <td><ww:file name="'attFile'" cssClass="'txt'" size="50" id="attFile" /></td>
            </tr>
        </ww:if>
        <tr>
            <td></td>
            <td>
                <ds:event name="'doCreate'" value="'Utw�rz'" onclick="'if (!validateForm()) return false'"/>
            </td>
        </tr>
    </ww:if>
    <ww:else>
        <tr>
            <td></td>
            <td>
                <ds:event name="'doEndTask'" value="endTaskButton != null ? endTaskButton : 'Zako�cz zadanie'"/>
            </td>
        </tr>
    </ww:else>

    <tr>
        <td></td>
        <td>
            <ww:if test="cancelButton">
                <ds:event name="'doCancelDecision'" value="cancelMessage != null ? cancelMessage : 'Anuluj decyzj�'"/>
            </ww:if>
            <ds:event name="'doMoveToInternalWorkflow'" confirm="'Na pewno chcesz przej�� do obiegu r�cznego?'" value="'Przejd� do procesu r�cznego'"/>
        </td>
    </tr>

    <ww:if test="notCreateButton">
        <tr>
            <td colspan="2">Tre�� uwagi:</td>
        </tr>
        <tr>
            <td colspan="2"><ww:textarea name="'content'" id="content" rows="4" cols="90" cssClass="'txt'" value="'List do klienta nie jest konieczny.'"/></td>
        </tr>
        <tr>
            <td><ds:event name="'doNotCreate'" onclick="'if (!validateRemarkForm()) return false'" value="'Nie tw�rz listu'"/></td>
        </tr>
    </ww:if>
    </table>

</form>

<script type="text/javascript">
    <ww:if test="!cannotCreateDocument">
    Calendar.setup({
        inputField     :    "documentDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
    </ww:if>

    function validateForm()
    {
        if (!validateDOK())
            return false;

        var file = document.getElementById('file');
        if (file && file.value.trim().length == 0)
        {
            alert('<ds:lang text="NieWybranoPliku"/>');
            return false;
        }

        return true;
    }
    
    function validateRemarkForm()
    {
        var content = document.getElementById('content').value;
        if (content != null && content.length > 4000) { alert('Tre�� uwagi jest zbyt d�uga. Maksymalna d�ugo�� to 4000 znak�w'); return false; }

        return true;
    }
</script>