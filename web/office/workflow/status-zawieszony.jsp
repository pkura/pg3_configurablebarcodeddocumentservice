<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N status-zawieszony.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />

    <table width="100%">
        <tr>
            <td>Nazwa procesu</td>
            <td><b><ww:property value="jbpmProcessName"/></b></td>
        </tr>
        <jsp:include page="/office/workflow/common/dok-info.jsp"/>
        <tr>
            <td>Status</td>
            <td><b>zawieszony</b></td>
        </tr>
        <tr>
            <td>Przyczyna zawieszenia</td>
            <td><ww:select name="'reason'" id="reason"
                    list="reasons" listKey="key" listValue="value" value="reason"
                    cssClass="'sel'" headerKey="''" headerValue="'-- wybierz --'" onchange=""/>
                <input type="button" value="Wybierz" class="btn" onclick="changeRemark()"/>
            </td>
        </tr>
        <tr>
            <td>Ustaw przypomnienie</td>
            <td>
                <ww:textfield name="'reminderDate'" size="10" maxlength="10" cssClass="'txt'" id="reminderDate"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="reminderDateTrigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
            </td>
        </tr>
    </table>

    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Tre�� uwagi:</td>
        </tr>
        <tr>
            <td><ww:textarea name="'content'" id="content" rows="4" cols="90" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td><ds:event name="'doAddRemark'" onclick="'if (!validateRemarkForm()) return false'" value="'Zapisz'"/></td>
        </tr>
    </table>

    <table>
        <tr>
            <td>
                <ds:event name="'doCancelDecision'" value="'Anuluj decyzj�'"/>
                <ds:event name="'doMoveToInternalWorkflow'" confirm="'Na pewno chcesz przej�� do obiegu r�cznego?'" value="'Przejd� do procesu r�cznego'"/>
            </td>
        </tr>
    </table>
</form>


<script type="text/javascript" >
    Calendar.setup({
        inputField     :    "reminderDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "reminderDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

    function validateRemarkForm()
    {
        if (!validateDOK())
            return false;

        var content = document.getElementById('content').value;
        if (content == null || content.length == 0) { alert('Nie podano tre�ci uwagi.'); return false; }
        if (content.length > 4000) { alert('Tre�� uwagi jest zbyt d�uga. Maksymalna d�ugo�� to 4000 znak�w'); return false; }

        return true;
    }

    function changeRemark()
    {
        var content = document.getElementById('content');
        var reason = document.getElementById('reason');
        if (content != null && reason.selectedIndex > 0)
        {
            content.value = 'przyczyna zawieszenia - '+reason.options[reason.selectedIndex].text;
        }
    }
</script>