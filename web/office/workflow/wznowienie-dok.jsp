<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N wznowienie-dok.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />

    <table width="100%">
        <jsp:include page="/office/workflow/common/main-info.jsp"/>
    </table>

    <jsp:include page="/office/workflow/common/remark_sig_att-info.jsp"/>

    <table>
        <tr>
            <td>
                <ww:if test="canAddNewDocument">
                    <ds:event name="'doInitNewDocument'" value="'Dodaj list do klienta'"/>
                </ww:if>
                <ds:event name="'doEndTask'" value="'Zako�cz zadanie'" confirm="'Na pewno zako�czy� zadanie?'"/>
                <ds:event name="'doMoveToInternalWorkflow'" confirm="'Na pewno chcesz przej�� do obiegu r�cznego?'" value="'Przejd� do procesu r�cznego'"/>
            </td>
        </tr>
    </table>


</form>