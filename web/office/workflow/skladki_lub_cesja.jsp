<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N skladki_lub_cesja.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />

    <table width="100%">
        <jsp:include page="/office/workflow/common/main-info.jsp"/>

        <ww:if test="canChangeReminder">
            <tr>
                <td>Zmie� przypomnienie</td>
                <td>
                    <ww:textfield name="'reminderDate'" size="10" maxlength="10" cssClass="'txt'" id="reminderDate" value="reminderDate"/>
                    <img src="<ww:url value="'/calendar096/img.gif'"/>"
                        id="reminderDateTrigger" style="cursor: pointer; border: 1px solid red;"
                        title="Date selector" onmouseover="this.style.background='red';"
                        onmouseout="this.style.background=''"/>
                    <ds:event name="'doSaveReminder'" value="'Zapisz'"/>
                </td>
            </tr>
        </ww:if>
        <tr>
            <td valign="top">Status</td>
            <td>
                <ww:if test="currentDocumentStatus != null">
                    <b><ww:property value="currentDocumentStatus"/></b><br/>
                </ww:if>
                <ww:select name="'status'" id="status"
                    list="statusMap" listKey="key" listValue="value" value="status"
                    cssClass="'sel'" headerKey="''" headerValue="'-- wybierz --'"/>
                <ds:event name="'doChooseStatus'" value="'Wybierz'" onclick="'if (!validateChooseStatus()) return false'"/>
            </td>
        </tr>
    </table>

    <jsp:include page="/office/workflow/common/remark_sig_att-info.jsp"/>

    <table>
        <tr>
            <td>
                <ds:event name="'doMoveToInternalWorkflow'" confirm="'Na pewno chcesz przej�� do obiegu r�cznego?'" value="'Przejd� do procesu r�cznego'"/>
            </td>
        </tr>
    </table>

</form>

<script type="text/javascript" >
    <ww:if test="canChangeReminder">
        Calendar.setup({
            inputField     :    "reminderDate",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "reminderDateTrigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
    });
    </ww:if>

    function validateChooseStatus()
    {
        var status = document.getElementById('status');
        if (!(status.selectedIndex > 0))
        {
            alert('Nie wybrano statusu');
            return false;
        }

        if (!validateDOK())
            return false;

        return true;
    }

</script>