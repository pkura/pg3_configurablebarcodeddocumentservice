<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dok-info.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.workflow.jbpm.Constants"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<script type="text/javascript">
    function validateDOK()
    {
       /* var wykonanoKonsultacje = document.getElementById('wykonanoKonsultacje');
        if (wykonanoKonsultacje != null)
        {
            alert(wykonanoKonsultacje.value);
            if (wykonanoKonsultacje.value == 'false')
            {
                alert("Nie mo�na wykona� czynno�ci przed konsultacj�  z DOK");
                return false;
            }
        }*/
        <ww:if test="!wykonanoKonsultacje && dokInfo.actionType == 3"> <%-- DOK_ACTION_WYMAGANE_KONSULTACJE_Z_DOK --%>
            alert("Nie mo�na wykona� czynno�ci przed konsultacj�  z DOK");
            return false;
        </ww:if>
        <ww:elseif test="dokInfo.actionType == 5"> <%-- Constants.DOK_ACTION_KONIECZNE_PRZESLANIE_PISMA --%>
            alert("DOK ��da wys�ania zadania.");
            return false;
        </ww:elseif>

        return true;
    }
</script>


<ww:if test="reopenReason != null">      <%-- Wznowienie zadania dla DOK --%>
    <tr>
        <td colspan="2">
            <span style="color:red"><ww:property value="reopenReason"/></span>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <span style="color:red">'<ww:property value="dokMessage"/>'</span>
        </td>
    </tr>
</ww:if>
<ww:if test="dokRealizujeOPS">
    <tr>
        <td></td>
        <td>
            <span style="color:red">Pismo zosta�o przes�ane od OPS w celu dalszej realizacji</span>
        </td>
    </tr>
</ww:if>
<ww:if test="dokInfo != null">
    <tr>
        <td></td>
        <td><span style="color:red"><ww:property value="dokInfo.message"/></span></td>
    </tr>
    <ww:if test="dokInfo.actionType == 3">  <%-- Constants.DOK_ACTION_WYMAGANE_KONSULTACJE_Z_DOK --%>
        <tr>
            <td>Wykonano konsultacje z DOK</td>
            <td>
                <ww:checkbox name="'wykonanoKonsultacje'" fieldValue="true" value="wykonanoKonsultacje" id="wykonanoKonsultacje" disabled="wykonanoKonsultacje"/>
                <ds:submit-event name="'doKonsultacjeZDOK'" value="'Zapisz'" disabled="wykonanoKonsultacje"/>
            </td>
        </tr>
    </ww:if>
    <ww:if test="dokInfo.actionType == 5"> <%-- Constants.DOK_ACTION_KONIECZNE_PRZESLANIE_PISMA --%>
        <tr>
            <td></td>
            <td>
                <ds:submit-event name="'doForwardToDOK'" value="'Przeka� pismo DOK'" />
            </td>
        </tr>
    </ww:if>
</ww:if>
