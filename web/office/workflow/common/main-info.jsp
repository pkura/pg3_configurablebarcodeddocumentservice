<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main-info.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<tr>
    <td><ds:lang text="NazwaProcesu"/></td>
    <td><b><ww:property value="jbpmProcessName"/></b></td>
</tr>
<jsp:include page="/office/workflow/common/dok-info.jsp"/>
<tr>
    <td><ds:lang text="Data"/></td>
    <td><b><ww:property value="documentDate"/></b></td>
</tr>
<tr>
    <td><ds:lang text="NumerPolisy"/></td>
    <td><b><ww:property value="nrPolisy"/></b></td>
</tr>
<tr>
    <td><ds:lang text="TypDokumentu"/></td>
    <td><b><ww:property value="typDokumentu"/></b></td>
</tr>
<tr>
    <td></td>
</tr>
<ww:if test="documents.size > 0">
    <ww:iterator value="documents">
        <tr>
            <td></td>
            <td><a href="<ww:url value="link"/>"><ww:property value="document_description"/></a>
                (<span <ww:if test="isNew">style="color:red"</ww:if>><ww:property value="document_date"/></span>)
            </td>
        </tr>
    </ww:iterator>
</ww:if>
<ww:else>
    <tr>
        <td></td>
        <td><span style="color:red">brakuje podpisanej kopii polisy lub aneksu</span></td>
    </tr>
</ww:else>
<tr>
    <td></td>
    <td><input type="button" value="Poka� wszystkie dokumenty dla danej polisy" class="btn" onclick="openResultsWindow()"/></td>
</tr>

<script type="text/javascript">
    function openResultsWindow()
    {
        openToolWindowX('<ww:url value="nrPolisySearchLink"/>','results',700,650);
    }
</script>