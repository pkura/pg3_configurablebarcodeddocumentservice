<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N remark_sig_att-info.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table border="0" cellspacing="0" cellpadding="0">
    <ww:if test="recentRemark != null">
        <ww:set name="date" scope="page" value="recentRemark.date"/>
        <tr>
            <td valign="top">Ostatnia uwaga</td>
            <td><b><ww:property value="recentRemark.content"/></b></td>
        </tr>
        <tr>
            <td></td>
            <td>dodana przez <b><ww:property value="recentRemark.author"/></b> (<fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>)</td>
        </tr>
    </ww:if>
    <ww:if test="previousRemarks != null && previousRemarks.size > 0">
        <ww:iterator value="previousRemarks" status="status">
            <ww:set name="date" scope="page" value="date"/>
            <tr>
                <td><ww:if test="#status.first">Wcze�niejsze uwagi</ww:if>&nbsp;</td>
                <td>
                    <b><ww:property value="content"/></b> (dodana <fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/> przez <b><ww:property value="author"/></b>)
                </td>
            </tr>
        </ww:iterator>
    </ww:if>
    <tr>
        <td colspan="2">Dodaj uwag�:</td>
    </tr>
    <tr>
        <td colspan="2"><ww:textarea name="'content'" id="content" rows="4" cols="90" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td colspan="2"><ds:event name="'doAddRemark'" onclick="'if (!validateRemarkForm()) return false'" value="'Zapisz'"/></td>
    </tr>
</table>

<ww:if test="signatureSrc != null">
    <h4>Wz�r podpisu</h4>
    <p><a href="<ww:url value="signatureSrc"/>" target="_blank"><img src="<ww:url value="signatureSrc"/>" width="240" height="80" border="0" alt="Wz�r podpisu"/></a></p>
</ww:if>
<ww:if test="signatureDocumentId != null">    
    <ww:if test="signatureSrc == null"><p></ww:if>
    <a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="signatureDocumentId"/></ww:url>">przejd� do wniosku</a>
    <ww:if test="signatureSrc == null"></p></ww:if>
</ww:if>

<ww:if test="attachments.size > 0">
    <h4>Za��czniki</h4>

    <table>
    <tr>
        <th>Tytu�</th>
        <th>Autor</th>
    <%--<th>Rozmiar</th>--%>
        <th>Data</th>
    <%--<th>Wersja</th>--%>
        <!--<th>Kod kreskowy</th>-->
        <th></th>
        <th></th>
    </tr>

    <ww:iterator value="attachments">
        <tr>
            <td><ww:property value="title"/></td>
            <td><ww:property value="userSummary"/></td>
            <ww:if test="mostRecentRevision != null">
                <ww:set name="revision" value="mostRecentRevision"/>
                <ww:set name="size" scope="page" value="#revision.size"/>
                <ww:set name="ctime" scope="page" value="#revision.ctime"/>
            <%--<td><ww:property value="#u.firstname + ' ' + #u.lastname"/></td>--%>
            <%--<td><edm-fmt:format-size size="${size}"/></td>--%>
                <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
            <%--<td><ww:property value="#revision.revision"/></td>--%>
                <!--<td><ww:property value="barcode"/></td>-->
                <td>&nbsp<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Pobierz"/></a></td>
                <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
                    <td valign="top">&nbsp;<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Wy�wietl"/></a></td>
                </ww:if>
            </ww:if>
            <ww:else>
                <td colspan="3">
                    <ww:if test="lparam != null">
                        <b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)"/></i></b>
                    </ww:if>
                    <ww:else>
                        <i>(pusty)</i>
                    </ww:else>
                </td>
            </ww:else>
        </tr>
    </ww:iterator>
    </table>

</ww:if>

<script type="text/javascript">
    function validateRemarkForm()
    {
        var content = document.getElementById('content').value;
        if (content != null && content.length > 4000) { alert('Tre�� uwagi jest zbyt d�uga. Maksymalna d�ugo�� to 4000 znak�w'); return false; }

        return true;
    }
</script>