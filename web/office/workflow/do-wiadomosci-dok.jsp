<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N do-wiadomosci-dok.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />

    <table width="100%">
        <tr>
            <td colspan="2">
                <b>Otrzymanie dokumentu do wiadomości</b>
            </td>
        </tr>
        <jsp:include page="/office/workflow/common/main-info.jsp"/>
    </table>

    <jsp:include page="/office/workflow/common/remark_sig_att-info.jsp"/>

    <table>
        <tr>
            <td>
                <ww:select name="'action'" id="action"
                    list="actionMap" listKey="key" listValue="value" value="action"
                    cssClass="'sel'" /> <%-- moze bedzie tu konieczny wybor wielokrotny multiple="true" size="3"--%>
            </td>
            <td>
                 <ds:event name="'doChooseAction'" value="'Wybierz'" onclick="'if (!validateChooseAction()) return false'"/>
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
    function validateChooseAction()
    {
       /* var status = document.getElementById('status');
        if (!(status.selectedIndex > 0))
        {
            alert('Nie wybrano statusu');
            return false;
        }*/

        return true;
    }
</script>