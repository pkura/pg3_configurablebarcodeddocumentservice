<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-portfolio-main.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>





<ww:if test="portfolio.subFolder">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Podteczka <ww:property value="portfolio.officeId"/></h1>
</ww:if>
<ww:else>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Teczka <ww:property value="portfolio.officeId"/></h1>
</ww:else>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>>
		<ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div>
</ds:available>

<ww:if test="adds">
	<jsp:include page="/parametrization/utp/autocomplete.jsp" />
</ww:if>





<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canView">
	<form action="<ww:url value="'/office/edit-portfolio.action'"/>" method="post" onsubmit="disableFormSubmits(this);" style="*border-top: none; *margin-top: -8px;">
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
		<ww:hidden name="'contractorId'" id="contractorId"/>
		<ww:hidden name="'contractorIdOld'" id="contractorIdOld"/>
		<ww:hidden name="'contractorNameOld'" id="contractorIdOld"/>
		<ds:available test="layout2">
				<div id="middleContainer"> <!-- BIG TABLE start -->
			</ds:available>
		
		<table class="tableMargin">
			<tr>
				<td>
					Znak teczki:
				</td>
				<td>
					<span class="bold"><ww:property value="portfolio.OfficeId"/></span>
				</td>
			</tr>
			
			<tr>
				<td>
					Kategoria RWA teczki:
				</td>
				<td>
					<span class="bold"><ww:property value="prettyRwaCategory"/></span>
				</td>
			</tr>
			
			<tr>
				<td>
					Podtytu� teczki
					<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'name'" size="40" maxlength="126" cssClass="'txt'" value="portfolio.name"/>
				</td>
			</tr>
			
			<tr>
				<td>
					Kod kreskowy:
				</td>
				<td>
					<ww:textfield name="'barcode'" size="40" maxlength="25" cssClass="'txt'" value="portfolio.barcode"/>
				</td>
			</tr>
			<tr>
			   	<td>
			   		Dni na za�atwienie
			   	</td>
			   	<td>
			   		<ww:textfield name="'days'" id="days" size="6" maxlength="5" value="days"/>
			   	</td>
			</tr>
			<tr>
				<ww:if test="portfolio.ctime != null">
					<ww:set name="ctime" value="portfolio.ctime" scope="page"/>
				</ww:if>
				<td>
					Data utworzenia:
				</td>
				<td>
				<span class="bold"><fmt:formatDate value="${ctime}" type="date" pattern="dd-MM-yy"/></span>
				</td>
			</tr>
			<tr>
				<td>
					Referent teczki
					<span class="star">*</span>:
				</td>
				<td>
					<ww:select name="'assignedUser'" list="users" listKey="name" listValue="asLastnameFirstname()" value="portfolio.clerk" cssClass="'sel combox-chosen'" disabled="!canSetClerk" />
				</td>
			</tr>
			<ww:if test="adds">
				<tr>
					<td>Aktualnie przypisany kontrahent:</td>
					<td><span class="bold"><ww:property
								value="contractorNameOld" /></span></td>
				</tr>
				<tr>
					<td><label>Edytuj kontrahenta na:</label> <ds:infoBox
							body="'Autouzupe�nianie po dowolnym fragmencie imienia b�d� nazwiska. W celu zmiany wybierz kontrahenta i nacisnij przycisk \"Zapisz\". \n Je�li zostanie wpisany nieistniej�cy kontrahent, teczka zostanie z nie przypisanym kontrahentem.'"
							header="'Informacja'" /></td>
					<td>
						<div class="ui-widget">
							<select id="combobox" name="'combobox'">
								<ww:iterator value="contractors">
									<option value="<ww:property value="key"/>">
										<ww:property value="value" />
									</option>
								</ww:iterator>

							</select>

						</div> <script>
							document.getElementById("combobox").value=''
						</script>
					</td>
				</tr>
			</ww:if> 
		</table>

		<input type="button" class="btn" value="Wydruk"
			onclick="javascript:void(window.open('<ww:url value="'/office/print-portfolio.action?id='+id"/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));"
			<ww:if test="!canPrint">disabled='true'</ww:if> />	
		
		<ds:submit-event value="'Zapisz'" name="'doUpdate'" disabled="!canModify"/>
		<ds:submit-event value="'Usu� teczk�'" name="'doDelete'" confirm="'Na pewno usun�� teczk�?'" disabled="!canDelete"/>
		<ds:submit-event value="'Zmie� numer teczki'" name="'doChangeNumber'" disabled="!canChangeNumber"/>
		<ww:if test="canMoveToNextYear">
			<ds:submit-event value="'Przenie� na nowy rok'" name="'doMoveToNextYear'" confirm="'Na pewno utworzy� t� teczk� w nowym roku?'"/>
		</ww:if>
		
		<ww:if test="portfolio.parent != null">
			<p>
				<input type="button" value="Teczka nadrz�dna" class="btn"
					onclick="document.location.href='<ww:url value="'/office/edit-portfolio.action?id='+portfolio.parent.id"/>';"/>
			</p>
		</ww:if>
			
		<ww:if test="!portfolio.children.empty">
			<p></p>
			
			<table class="tableMargin">
				<tr>
					<td>
						Znajd� spraw� o numerze:
					</td>
					<td>
						<ww:textfield name="'findCaseNumber'" size="20" maxlength="20" cssClass="'txt'" value="findCaseNumber"/>
					</td>
					<td>
						<ds:submit-event value="'Znajd�'" name="'doSearch'"/>
					</td>
					<ww:if test="casesFound != null">   
						<td>
							<ds:submit-event value="'Poka� wszystkie sprawy i podteczki'" name="'default'"/>
						</td>
					</ww:if>
				</tr>
			</table>
			
			(nale�y poda� fragment numeru sprawy lub pe�ny znak sprawy -  mechanizm wyszukuje po tre�ci znaku sprawy)
			
			
			<ww:if test="casesFound == null">
				<p>Sprawy i podteczki</p>
			</ww:if>
			<ww:else>
				<p>Znalezione sprawy</p>
			</ww:else>
			
			<ww:set name="pager" scope="request" value="pager" />
			<table width="100%">
				<tr>
					<td align="center">
						<jsp:include page="/pager-links-include.jsp"/>
					</td>
				</tr>
			</table>
			
			<table class="tableMargin">
				<tr>
					<th>
						Lp
					</th>
					<th>
						Znak sprawy/podteczki
					</th>
					<th>
						Rok
					</th>
					<th>
						Tytu�
					</th>
					<th>
						Data rozpocz�cia
					</th>
					<th>
						Data zako�czenia
					</th>
					<th>
						Status
					</th>
					<th>
						Opis
					</th>
				</tr>
				
				<ww:if test="casesFound == null">
					<ww:set name="childrenCollection" value="portfolio.children"/>
				</ww:if>
				<ww:else>
					<ww:set name="childrenCollection" value="casesFound"/>
				</ww:else>
				
				<ww:set name="count" value="0"/>
				
				<ww:iterator value="#childrenCollection">
					<ww:if test="(#count >= childrenFrom) && (#count <= childrenTo)"> 
						<ww:if test="case">
							<ww:set name="link" value="'/office/edit-case.do?id='+id"/>
						</ww:if>
						<ww:else>
							<ww:set name="link" value="'/office/edit-portfolio.action?id='+id"/>
						</ww:else>
						
						<tr <ww:if test="author.equals(user)">class="<ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions>"</ww:if>>
							<td><ww:property value="sequenceId"/></td>
							<td><a href="<ww:url value="#link"/>"><ww:property value="officeId"/></a></td>
							<td><ww:property value="year"/></td>
						   
							<ww:if test="case">
					            <td>
					                <ww:property value="title"/>
					            </td>
								
								<ww:set name="openDate" value="openDate" scope="page" />
								<ww:set name="finishDate" value="finishDate" scope="page" />
								
								<td>
									<fmt:formatDate value="${openDate}" type="date" pattern="dd-MM-yy"/>
								</td>
								<td>
									<fmt:formatDate value="${finishDate}" type="date" pattern="dd-MM-yy"/>
								</td>
								<td>
									<ww:property value="status.getName()"/>
								</td>
								<td>
									<ww:property value="description"/>
								</td>
							</ww:if>
							<ww:else>
								<td>
									<ww:property value="name"/>
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td>
									podteczka
								</td>
							</ww:else>
						</tr>
					</ww:if>
					<ww:set name="count" value="#count+1"/>
				</ww:iterator>
			</table>
		</ww:if>
		
		<table width="100%">
			<tr>
				<td align="center">
					<jsp:include page="/pager-links-include.jsp"/>
				</td>
			</tr>
		</table>
		
		<h4>Tworzenie podteczki</h4>

		Tytu� podteczki
		<span class="star">*</span>:
		<ww:textfield name="'subname'" size="40" maxlength="126" cssClass="'txt'"/>

		<br/>
		<ds:submit-event value="'Utw�rz podteczk�'" name="'doPreCreate'" disabled="!canCreate"/>
		
		<input type="button" value="Lista teczek w dziale" class="btn"
	onclick="document.location.href='<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="portfolio.divisionGuid"/></ww:url>';"/>
		
		<p>
			<span class="star">*</span>
			<ds:lang text="PoleObowiazkowe"/>
		</p>
		
	<ds:available test="layout2">
					<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
					<div class="bigTableBottomSpacer">&nbsp;</div>
					</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
				</ds:available>		
	</form>
</ww:if>

<ww:if test="!canView">
<input type="button" value="Lista teczek w dziale" class="btn"
	onclick="document.location.href='<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="portfolio.divisionGuid"/></ww:url>';"/>
</ww:if>

<%--
<ww:if test="portfolio.subFolder">
<h1>Podteczka <ww:property value="portfolio.officeId"/></h1>
</ww:if>
<ww:else>
<h1>Teczka <ww:property value="portfolio.officeId"/></h1>
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canView">

<form action="<ww:url value="'/office/edit-portfolio.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'id'" value="id"/>
<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>

<table>
	<tr>
		<td>Znak teczki:</td>
		<td><b><ww:property value="portfolio.OfficeId"/></b></td>
	</tr>
	
	<tr>
		<td>Kategoria RWA teczki:</td>
		<td><b><ww:property value="prettyRwaCategory"/></b></td>
	</tr>
	
	<tr>
		<td>Podtytu� teczki<span class="star">*</span>:</td>
		<td><ww:textfield name="'name'" size="40" maxlength="126" cssClass="'txt'" value="portfolio.name"/></td>
	</tr>
	
	<tr>
		<td>Kod kreskowy:</td>
		<td><ww:textfield name="'barcode'" size="40" maxlength="25" cssClass="'txt'" value="portfolio.barcode"/></td>
	</tr>
	<tr>
	   	<td>Dni na za�atwienie</td>
	   	<td><ww:textfield name="'days'" id="days" size="6" maxlength="5" value="days"/></td>
	</tr>
	<tr>
		<ww:if test="portfolio.ctime != null"><ww:set name="ctime" value="portfolio.ctime" scope="page"/></ww:if>
		<td>Data utworzenia:</td>
		<td><b><fmt:formatDate value="${ctime}" type="date" pattern="dd-MM-yy"/></b></td>
	</tr>
	<tr>
		<td>Referent teczki<span class="star">*</span>:</td>
		<td>
			<ww:select name="'assignedUser'" list="users" listKey="name" listValue="asLastnameFirstname()" value="portfolio.clerk" cssClass="'sel'" disabled="!canSetClerk" />
		</td>
	</tr>
</table>

<input type="button" class="btn" value="Wydruk"
		onclick="javascript:void(window.open('<ww:url value="'/office/print-portfolio.action?id='+id"/>', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no'));"
		<ww:if test="!canPrint">disabled='true'</ww:if> />	

	<ds:submit-event value="'Zapisz'" name="'doUpdate'" disabled="!canModify"/>
<ds:submit-event value="'Usu� teczk�'" name="'doDelete'" confirm="'Na pewno usun�� teczk�?'" disabled="!canDelete"/>
<ds:submit-event value="'Zmie� numer teczki'" name="'doChangeNumber'" disabled="!canChangeNumber"/>
<ww:if test="canMoveToNextYear"><ds:submit-event value="'Przenie� na nowy rok'" name="'doMoveToNextYear'" confirm="'Na pewno utworzy� t� teczk� w nowym roku?'"/></ww:if>
<%--
<input type="submit" name="doUpdate" value="Zapisz" class="btn" onclick="document.getElementById('doUpdate').value='true';"/>
<input type="submit" name="doDelete" value="Usu� teczk�" onclick="if (!confirm('Na pewno usun��?')) return false; document.getElementById('doDelete').value='true';" class="btn" <ww:if test="!canDelete">disabled="true"</ww:if>/>
-->

<ww:if test="portfolio.parent != null">
	<%--<p><a href="<ww:url value="'/office/edit-portfolio.action?id='+portfolio.parent.id"/>">Teczka nadrz�dna</a></p>-->	
	<p>
	<input type="button" value="Teczka nadrz�dna" class="btn"
		onclick="document.location.href='<ww:url value="'/office/edit-portfolio.action?id='+portfolio.parent.id"/>';"/>
	</p>
</ww:if>
	

<ww:if test="!portfolio.children.empty">

<p></p>

<table>
	<tr>
		<td>Znajd� spraw� o numerze:</td>
		<td><ww:textfield name="'findCaseNumber'" size="20" maxlength="20" cssClass="'txt'" value="findCaseNumber"/></td>
		<td><ds:submit-event value="'Znajd�'" name="'doSearch'"/></td>
		<ww:if test="casesFound != null">   
		<td><ds:submit-event value="'Poka� wszystkie sprawy i podteczki'" name="'default'"/></td>
		</ww:if>
	</tr>
</table>
(nale�y poda� numer kolejny sprawy lub pe�ny znak sprawy)

<ww:if test="casesFound == null"><p>Sprawy i podteczki</p></ww:if>
<ww:else><p>Znalezione sprawy</p></ww:else>

<table>
	<tr>
		<th>Lp</th><th>Znak sprawy/podteczki</th><th>Rok</th><th>Tytu�</th><th>Data rozpocz�cia</th>
		<th>Data zako�czenia</th><th>Status</th><th> Opis</th>
	</tr>
	
	<ww:if test="casesFound == null">
		<ww:set name="childrenCollection" value="portfolio.children"/>
	</ww:if>
	<ww:else>
		<ww:set name="childrenCollection" value="casesFound"/>
	</ww:else>
	
	<ww:set name="count" value="0"/>
	<ww:iterator value="#childrenCollection">
		<ww:if test="(#count >= childrenFrom) && (#count <= childrenTo)"> 
			<ww:if test="case">
				<ww:set name="link" value="'/office/edit-case.do?id='+id"/>
			</ww:if>
			<ww:else>
				<ww:set name="link" value="'/office/edit-portfolio.action?id='+id"/>
			</ww:else>
			<tr <ww:if test="author.equals(user)">class="highlightedText"</ww:if>>
				<td><ww:property value="sequenceId"/></td>
				<td><a href="<ww:url value="#link"/>"><ww:property value="officeId"/></a></td>
				<td><ww:property value="year"/></td>
			   
				<ww:if test="case">
					<td><ww:property value="title"/></td>
					<ww:set name="openDate" value="openDate" scope="page" />
					<ww:set name="finishDate" value="finishDate" scope="page" />
					<td><fmt:formatDate value="${openDate}" type="date" pattern="dd-MM-yy"/></td>
					<td><fmt:formatDate value="${finishDate}" type="date" pattern="dd-MM-yy"/></td>
					<td><ww:property value="status.getName()"/></td>
					<td><ww:property value="description"/></td>
				</ww:if>
				<ww:else>
					<td><ww:property value="name"/></td>
					<td></td>
					<td></td>
					<td>podteczka</td>
				</ww:else>
			</tr>
		</ww:if>
		<ww:set name="count" value="#count+1"/>
	</ww:iterator>
</table>
</ww:if>

<ww:set name="pager" scope="request" value="pager" />
<table width="100%">
	<tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
</table>

<h4>Tworzenie podteczki</h4>
<table>
	<tr>
		<td>Tytu� podteczki<span class="star">*</span>:</td>
		<td><ww:textfield name="'subname'" size="40" maxlength="126" cssClass="'txt'"/></td>
	</tr>
</table>
<ds:submit-event value="'Utw�rz podteczk�'" name="'doPreCreate'" disabled="!canCreate"/>


</form>

</ww:if>

<input type="button" value="Lista teczek w dziale" class="btn"
	onclick="document.location.href='<ww:url value="'/office/portfolios.action'"><ww:param name="'divisionGuid'" value="portfolio.divisionGuid"/></ww:url>';"/>


<ww:if test="canView">
<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
</ww:if>
--%>
<!--N koniec edit-portfolio-main.jsp N-->