<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="ZarzadzanieBarkodami"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="Barcodes"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form action="<ww:url value="'/office/manage-barcodes.action'"/>" method="post" id="form">
<ww:hidden name="'rangeId'" id="rangeId"/>
<br>
<table>
<tr>
	<th>
		<ds:lang text="StopienWykorzystaniaPuli"/>
	</th>
	<td>
		<ww:property value="percentage"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="LiczbaZuzytychBakodow"/>
	</th>
	<td>
		<ww:property value="usedBarcodes"/>
	</td>
</tr>
<tr>
	<th>
		<ds:lang text="PozostaloBakodow"/>
	</th>
	<td>
		<ww:property value="leftBarcodes"/>
	</td>
</tr>
</table>
<br>
<ww:select name="'showstatus'" list="statuses" listKey="value" listValue="key" onchange="[0].submit();" cssClass="'sel'"/>
<ds:submit-event value="'Pokaz'" />
<ww:hidden name="'statusOrig'"/>
<table>
<tr>
	<th>
		<ds:lang text="id"/>
	</th>
	<th>
		<ds:lang text="ctime"/>
	</th>
	<th>
		<ds:lang text="Uzytkownik"/>
	</th>
	<th>
		<ds:lang text="LiczbaBarkodow"/>
	</th>
	<th>
		<ds:lang text="Transport"/>
	</th>
	<th>
		<ds:lang text="Uwagi"/>
	</th>
	<th>
		<ds:lang text="status"/>
	</th>
</tr>
<ww:iterator value="ranges">
<tr>
	<td>
		<ww:property value="id"/>
	</td>
	<td nowrap="nowrap">
		<ds:format-date pattern="dd-MM-yyyy HH:mm" value="ctime"/>
	</td>
	<td>
		<ww:property value="fullUsername"/>
	</td>
	<td>
		<ww:property value="numberOfBarcodes"/>
	</td>
	<td>
		<ww:property value="transportServiceName"/>
	</td>
	<td>
		<ww:property value="remark"/>
	</td>
	<td>
		<ww:property value="statusDesc"/>
	</td>
	<ww:if test="status==1">
	<td>
		<a href="<ww:url value="'/office/manage-barcodes.action'"/>?rangeId=<ww:property value="id"/>&doAccept=true&showstatus=<ww:property value="showstatus"/>"/><ds:lang text="Akceptuj"/></a>
	</td>
	<td>
		<a href="<ww:url value="'/office/assign-from-range.action'"/>?rangeId=<ww:property value="id"/>"/><ds:lang text="PrzydzielZWlasnegoZakresu"/></a>
	</td>
	<td>
		<a href="<ww:url value="'/office/manage-barcodes.action'"/>?rangeId=<ww:property value="id"/>&doReject=true&showstatus=<ww:property value="showstatus"/>"/><ds:lang text="Odrzuc"/></a>
	</td>
	</ww:if>
	<ww:elseif test="status==2">
	<td>
		<a href="<ww:url value="'/office/manage-barcodes.action'"/>?rangeId=<ww:property value="id"/>&doPrint=true&showstatus=<ww:property value="showstatus"/>"/><ds:lang text="WWydruku"/></a>
	</td>
	</ww:elseif>
	<ww:elseif test="status==3">
	<td>
		<a href="<ww:url value="'/office/manage-barcodes.action'"/>?rangeId=<ww:property value="id"/>&doSend=true&showstatus=<ww:property value="showstatus"/>"/><ds:lang text="Wyslane"/></a>
	</td>
	</ww:elseif>
</tr>
</ww:iterator>
</table>
</form>
