<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>

<h1>Wyszukiwanie paczek</h1>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="PackageIn"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PackageIn"/>
	</div>
	<div id="middleContainer" style="border-top: none;">
</ds:available>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="packages == null || packages.size() == 0">
	<form id="form" action="<ww:url value="'/office/package-search.action'"></ww:url>" method="post" >
		<table class="tableMargin">
			<tr>
				<td>Zakres dat utworzenia:</td>
				<td>
					od
					<ww:textfield name="'packageDateFrom'" id="packageDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="packageDateFromTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					do
					<ww:textfield name="'packageDateTo'" id="packageDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="packageDateToTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
				</td>
			</tr>
			<tr>
				<td>Numer paczki:</td>
				<td>
					<ww:textfield name="'packageNb'" id="packageNb" size="25" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
			   	<td>Użytkownik:</td>
			   	<td>
				   	<ww:select name="'creatingUser'" cssClass="'sel'" list="users"
					   	listKey="name" listValue="lastname+' '+firstname"
					   	headerKey="''" headerValue="'-- dowolny --'"/>
				</td>
			</tr>
		</table>
		<ds:submit-event value="'Szukaj'" name="'doSearch'" cssClass="'btn'"/>
	
		<script type="text/javascript">
			Calendar.setup({
				inputField	 :	"packageDateFrom",
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",
				button		 :	"packageDateFromTrigger",
				align		  :	"Tl",
				singleClick	:	true
			});
			
			Calendar.setup({
				inputField	 :	"packageDateTo",
				ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",
				button		 :	"packageDateToTrigger",
				align		  :	"Tl",
				singleClick	:	true
			});
		</script>
	</form>
</ww:if>
<ww:else>
	<table id="titleTable" class="tableMargin">
		<tr>
			<th> Numer paczki: </th>
			<th> Użytkownik: </th>
			<th> Data utworzenia: </th>
		</tr>
		<ww:iterator value="packages">
			<tr>
				<td align="center">
				  <a href="<ww:url value="'/office/package-show.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="numer"/></a>
				</td>
				<td>
					<ww:property value="user"/>
				</td>
				<td>
					<ds:format-date value="ctime" pattern="dd-MM-yy"/>
				</td>				
			</tr>
		</ww:iterator>
	</table>
	<script type="text/javascript">
		prepareTable(E("titleTable"),1,0);
	</script>
</ww:else>

