<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-main.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script language="JavaScript">
</script>

<h3><ds:lang text="Wysylka"/></h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/outgoing/edit-main">
<html:hidden property="exchange" />
<html:hidden property="documentId" />

<p>Dokument w dzienniku <a href="<c:out value="${journalLink}"/>"><c:out value="${journalName}"/></a></p>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>Dotyczy dokumentu przychodz�cego:</td>
    <td><a href="<c:out value="${incomingDocumentLink}"/>">zobacz dokument</a></td>
</tr>
<tr>
    <td>Numer przesy�ki rejestrowanej (R):</td>
    <td><html:text property="postalRegNumber" size="5" maxlength="10" styleClass="txt_opt"/></td>
</tr>
<tr>
    <td><ds:lang text="DataStemplaPocztowego"/>:</td>
    <td><html:text property="stampDate" styleId="stampDate" size="10" maxlength="10" styleClass="txt_opt"/>
        <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
            id="stampDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
    </td>
</tr>
<tr>
    <td colspan="2"><br/><h4>Odbiorca</h4></td>
</tr>
<tr>
    <td>Tytu�/stanowisko:</td>
    <td><c:out value="${incomingDocument.senderTitle}"/></td>
</tr>
<tr>
    <td>Imi�:</td>
    <td><c:out value="${incomingDocument.senderFirstname}"/></td>
</tr>
<tr>
    <td>Nazwisko:</td>
    <td><c:out value="${incomingDocument.senderLastname}"/></td>
</tr>
<tr>
    <td>Instytucja:</td>
    <td><c:out value="${incomingDocument.senderOrganization}"/></td>
</tr>
<tr>
    <td>Adres:</td>
    <td><c:out value="${incomingDocument.senderStreetAddress}"/></td>
</tr>
<tr>
    <td valign="top">Kod pocztowy i miejscowo��:</td>
    <td><c:out value="${incomingDocument.senderZip} ${incomingDocument.senderLocation}"/></td>
</tr>
<tr>
    <td>Kraj:</td>
    <td><c:out value="${incomingDocument.senderCountry}"/></td>
</tr>
<tr>
    <td>Email:</td>
    <td><c:out value="${incomingDocument.senderEmail}"/></td>
</tr>
<tr>
    <td>Faks:</td>
    <td><c:out value="${incomingDocument.senderFax}"/></td>
</tr>
<tr>
    <td colspan="2">
        <input type="submit" name="doUpdate" value="Zapisz" class="btn">
    </td>
</tr>
</table>

</html:form>

<script type="text/javascript">
if($j('#stampDate').length > 0)
{
    Calendar.setup({
        inputField     :    "stampDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "stampDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}
</script>