<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<ww:url value="'/prototype.js'"/>" type="text/javascript"></script>
<script src="<ww:url value="'/effects.js'"/>" type="text/javascript"></script>
<script src="<ww:url value="'/scriptaculous.js'"/>" type="text/javascript"></script>   
    
<h1><ds:lang text="Kontrahent"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<ds:available test="!layout2">
<p>
	<ds:xmlLink path="Kontrahent"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="Kontrahent"/>
	</div>
</ds:available>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/client-contact-list.action'"/>" method="post">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
 		<ww:include page="/crm/include-client-contact-list.jsp"/>
 		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available> 
</form>
