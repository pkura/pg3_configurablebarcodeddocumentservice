<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!--N vat-entry.jsp N-->
<h1>Kwoty VAT</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/office/common/vat-entry.action'"/>" method="post">
		<ww:hidden name="'entry.id'" value="entry.id" id="id" />
		<ww:hidden name="'documentId'" value="documentId" id="documentId" />
        <ww:hidden name="'param'" value="param" id="param"/>
        <ww:hidden id="goToTasklist" name="'goToTasklist'"/>
<table>
	<tr><td>Stawka VAT:</td><td>        <ww:select    id="vatRate"          name="'entry.vatRateString'" list="vatRates" listKey="symbol+'|'+value" listValue="niceString" cssClass="'sel'"/></td></tr>
	<tr><td>Kwota netto:</td><td>       <ww:textfield id="nettoAmount"      name="'entry.nettoAmount'" value="entry.nettoAmount"/></td></tr>
	<tr><td>Kwota VAT:</td><td>         <ww:textfield id="vatAmount"        name="'entry.vatAmount'" value="entry.vatAmount"/></td></tr>
	<tr><td>Kwota netto koszty:</td><td><ww:textfield id="costsNettoAmount" name="'entry.costsNettoAmount'" value="entry.costsNettoAmount"/></td></tr>
	<tr><td>Kwota VAT koszty:</td><td>  <ww:textfield id="costsVatAmount"   name="'entry.costsVatAmount'" value="entry.costsVatAmount"/></td></tr>

<ww:if test="id == null">
    <tr><td><input type="submit" id="doAdd" name="doAdd" value="Dodaj" onclick="if (!validateUpdate()) return false;" class="btn"/></td></tr>
</ww:if><ww:else>
    <tr><td><input type="submit" id="doAdd" name="doAdd" value="Zapisz" onclick="if (!validateAdd()) return false;" class="btn"/></td></tr>
</ww:else>
</table>
<script language="JavaScript">
$j('#vatRate').change(function(){
    vat = $j('#vatRate').val().replace(/.*\|/,'');
	$j('#vatAmount').val(($j('#nettoAmount').val().replace(',','.') * vat / 100).toFixed(2));
    vat = $j('#vatRate').val().replace(/.*\|/,'');
	$j('#costsVatAmount').val(($j('#costsNettoAmount').val().replace(',','.') * vat / 100).toFixed(2));
});

    $j('#nettoAmount').change(function(){
        vat = $j('#vatRate').val().replace(/.*\|/,'');
        $j('#vatAmount').val((this.value.replace(',','.') * vat / 100).toFixed(2));
    });

    $j('#costsNettoAmount').change(function(){
        vat = $j('#vatRate').val().replace(/.*\|/,'');
        $j('#costsVatAmount').val((this.value.replace(',','.') * vat / 100).toFixed(2));
    });

    function validate(){
        if($j('#vatRate').val().length = 0){
            alert("Pole Stawka VAT jest obowiązkowe");
            return false;
        }
        if($j('#nettoAmount').val().length = 0){
            alert("Pole Kwota netto jest obowiązkowe");
            return false;
        }
        if($j('#vatAmount').val().length = 0){
            alert("Pole Kwota VAT jest obowiązkowe");
            return false;
        }
        

		$j('#nettoAmount').val($j('#nettoAmount').val().replace(',','.'));
		$j('#costsNettoAmount').val($j('#costsNettoAmount').val().replace(',','.'));
		$j('#vatAmount').val($j('#vatAmount').val().replace(',','.'));
		$j('#costsVatAmount').val($j('#costsVatAmount').val().replace(',','.'));
		
        return true;
    }

    function validateAdd(){
        return validate();
    }

    function validateUpdate(){
        return validate();
    }

	function initDoc(){
		var el = window.opener.document.getElementById('documentId');
		if(el == null)
			el = window.opener.document.getElementById('id');
		document.getElementById('documentId').value = el.value;
	}
	initDoc();

    function submitStrona() {
        if (!window.opener) {
            alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
            return;
        }

        var ret = new Array();
        var tmp = '';

        ret.investment = 'false';
        ret.id = $j('#id').val();
        ret.vatSymbol = $j('#vatRate').val().replace(/\|.*/,'');
        ret.vat = $j('#vatRate').val().replace(/.*\|/,'');

        ret.vatAmount = $j('#vatAmount').val();
        ret.nettoAmount = $j('#nettoAmount').val();
        ret.costsVatAmount = $j('#costsVatAmount').val();
        ret.costsNettoAmount = $j('#costsNettoAmount').val();

        if (window.opener.accept) {
            window.opener.accept('<ww:property value="param"/>', ret);
        } else {
            alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
        }

        var form = window.opener.document.forms[0];
        var doArchive = window.opener.document.createElement('input');
        doArchive.type = 'hidden';
        doArchive.name = 'doArchive';
        doArchive.value = 'true';
        form.appendChild(doArchive);
        form.submit();
        window.close();
    }

    <ww:if test="added">
        submitStrona();
    </ww:if>

</script>
<!--N koniec vat-entry.jsp N-->
