<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'param'" value="param" id="param"/>

<ww:if test="results == null || results.empty">
	<table class="popupForm">
	<tr class="desc">
		<td colspan="2"><ds:lang text="Nazwa"/><span class="star">*</span></td>
		<td width="10"></td>
		<td width="88">Obr�t</td><td>Data rejestracji</td>
	</tr>
	<tr>
        <td colspan="2"><ww:textfield name="'name'" id="name" size="40" maxlength="50" cssClass="'txt'"/></td>
        <td width="10"></td>
        <td><ww:textfield name="'obrot'" id="obrot" size="18" maxlength="50" cssClass="'txt'"/></td>
 		<td><ww:textfield disabled="true" name="'ctime'" id="ctime" size="18" maxlength="50" cssClass="'txt'"/></td>
   </tr>
   <tr class="desc">
		<td colspan="2"><ds:lang text="NumerKontrahenta"/><span class="star">*</span></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="NumerTelefonu"/></td>
	</tr>
	<tr>
        <td colspan="2"><ww:textfield name="'contractorNo'" id="contractorNo" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
        <td width="10"></td>
        <td colspan="2"><ww:textfield name="'phoneNumber'" id="phoneNumber" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
   </tr>
   <tr class="desc">
		<td colspan="2"><ds:lang text="PelnaNazwa"/></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="NumerTelefonu"/></td>
	</tr>
	<tr>
        <td colspan="2"><ww:textfield  name="'pelnaNazwa'" id="pelnaNazwa" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
		<td width="10"></td>
        <td colspan="2"><ww:textfield name="'phoneNumber2'" id="phoneNumber2" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>		
	</tr>
	<tr class="desc">
		<td colspan="2"><ds:lang text="Ulica"/></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="Faks"/></td>
	</tr>
	<tr>
   		<td colspan="2"><ww:textfield name="'ulica'" id="ulica" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
   		<td width="10"></td>
   		<td colspan="2"><ww:textfield name="'fax'" id="fax" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    </tr>
    <tr class="desc">
	    <td width="88"><ds:lang text="kod"/></td><td><ds:lang text="miasto"/></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="email"/></td>
	</tr>
	<tr>
		<td><ww:textfield name="'kod'" id="kod" size="10" maxlength="14" onchange="'notifyChange();'" cssClass="'txt'"/></td>		
        <td><ww:textfield name="'miasto'" id="miasto" size="25" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
		<td width="10"></td>
		<td  colspan="2"><ww:textfield name="'email'" id="email" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	</tr>
	<tr>
            <td colspan="2"><span class="warning" id="zipError"> </span></td>
    </tr>
    <tr class="desc">
		<td colspan="2"><ds:lang text="Kraj"/></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="Strona www"/></td>
	</tr>
	<tr>
   		<td colspan="2"><ww:textfield name="'kraj'" id="kraj" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
   		<td width="10"></td>
   		<td colspan="2"><ww:textfield name="'www'" id="www" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    </tr>
    <tr class="desc">
		<td colspan="2"><ds:lang text="Uwagi"/></td>
		<td width="10"></td>
		<td colspan="2"><ds:lang text="KRS"/></td>
	</tr>
	<tr>
        <td colspan="2" rowspan="3"><ww:textarea  name="'remarks'" id="remarks" cols="30" rows="4" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    	<td width="10"></td>
    	<td colspan="2"><ww:textfield name="'krs'" id="krs" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    </tr>
    <tr class="desc">
    	<td width="10"></td>
    	<td><ds:lang text="NIP"/></td>
    	<td><ds:lang text="Regon"/></td>
    </tr>
    <tr>
    	<td width="10"></td>
    	<td><ww:textfield name="'nip'" id="nip" size="18" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    	<td><ww:textfield name="'regon'" id="regon" size="18" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
    </tr>
    <tr>
    <!-- 
    <tr>				
		<td colspan="2"><ds:lang text="Region"/>
		<ww:select id="region" name="'region'" headerValue="getText('select.wybierz')" headerKey="''" list="regions" listKey="id" listValue="name"  cssClass="'sel'"/></td>
		<td></td>
		<td colspan="2"><ds:lang text="U�ytkownik"/>
		<ww:select id="username" name="'username'" headerValue="getText('select.wybierz')" headerKey="''" list="users"   cssClass="'sel'"/></td>
	</tr>
	 -->
	
	
	
	
	
	
	
	
	
	<tr>
		<td colspan="6">
		<table align="left">
			<tr>
				<td>Opiekunowie z webLSP</td>
				<td>Opiekunowie z DocuSafe</td>
				
			</tr>
			<tr>
				<td>
					<ww:select name="'selectedWebPatrons'" id="webPatrons" multiple="true" size="5"  
					 cssClass="'multi_sel'" cssStyle="'width: 160px;'" list="webPatrons" disabled="true"/>
				</td>
				<td>
					<ww:select name="'selectedDsPatrons'" id="selectedDsPatrons" multiple="true" size="5" 
					cssClass="'multi_sel'" cssStyle="'width: 160px;'" list="dsPatrons" />
				</td>
				
				<td>
				<input type="button" value="<ds:lang text="Usu� zaznaczonego opiekuna"/>" onclick="delPatron();" class="btn"  <ww:if test="!canDelete">disabled="disabled"</ww:if>/>
				</td>
			</tr>
		</table>	
		</td>
	</tr>
	<tr>
		<td colspan="6">
		<ww:select id="username" name="'username'" headerValue="getText('select.wybierz')" headerKey="''" list="users"   cssClass="'sel'"/> 
		<input type="button" value="<ds:lang text="Dodaj opiekuna"/>" onclick="addPatron();" class="btn" <ww:if test="!canEdit">disabled="disabled"</ww:if> />
		
		</td>
	</tr>	
	<tr>
		<td colspan="5">
			<table>
				<!-- <td><ds:lang text="Role"/></td> -->	
				<ww:set name="numberColumn" value="1"/>
				<ww:iterator value="roles" status="status">
					<ww:if test="#status.first">
						<tr>
					</ww:if>
					<ww:set name="numberColumn" value="#numberColumn + 1"/>
					<ww:if test="#numberColumn > 10">
						<ww:set name="numberColumn" value="1"/>
						</tr>
						<tr>
					</ww:if>
					<td>
						<input type="checkbox" name="checkRoles" value="<ww:property value="id"/>" onchange="changeRole(this,'<ww:property value="name" />')"
							id="<ww:property value="id"/>" <ww:if test="containRole.containsKey(id)">checked="true"</ww:if> />
					</td>
					<td>
						<ww:property value="name" />
					</td>
					<ww:if test="#status.last">
						</tr>
					</ww:if>				
					
				</ww:iterator>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="6">
		<table align="left">
			<tr>
				<td class="markiClass"><ds:lang text="Marki"/> dost�pne</td>
				<td class="markiClass"></td>
				<td class="markiClass"><ds:lang text="Marki"/> kontrahenta</td>
				<td class="machineClass"><ds:lang text="Maszyny/urz�dzenia dost�pne"/></td>
				<td class="machineClass"></td>
				<td class="machineClass"><ds:lang text="Maszyny/urz�dzenia kontrahenta"/></td>
			</tr>
			<tr>
				<td class="alignTop markiClass">
					<ww:select name="'availableMarki'" listKey="id" listValue="name" multiple="true" size="5" 
					cssClass="'multi_sel'" cssStyle="'width: 115px;'" list="availableMarki"/>
				</td>
				<td class="alignMiddle markiClass">
					<input type="button" value=" &gt;&gt; " name="moveRightButton"
						onclick="if (moveOptions(this.form.availableMarki, this.form.selectedMarki) > 0) dirty=true;" class="btn"/>
					<br/>
					<input type="button" value=" &lt;&lt; " name="moveLeftButton"
						onclick="if (moveOptions(this.form.selectedMarki, this.form.availableMarki) > 0) dirty=true;" class="btn"/>
				</td>
				<td class="alignTop markiClass">
					<ww:select name="'selectedMarki'" id="selectedMarki" multiple="true" size="5" listKey="id" 
					listValue="name" cssClass="'multi_sel'" cssStyle="'width: 115px;'" list="contractorMarki"/>
				</td>
				<td class="alignTop machineClass">
					<ww:select name="'availableMachine'" listKey="id" listValue="name" multiple="true" size="5" 
					cssClass="'multi_sel'" cssStyle="'width: 160px;'" list="availableMachine"/>
				</td>
				<td class="alignMiddle machineClass">
					<input type="button" value=" &gt;&gt; " name="moveRightButton"
						onclick="if (moveOptions(this.form.availableMachine, this.form.selectedMachine) > 0) dirty=true;" class="btn"/>
					<br/>
					<input type="button" value=" &lt;&lt; " name="moveLeftButton"
						onclick="if (moveOptions(this.form.selectedMachine, this.form.availableMachine) > 0) dirty=true;" class="btn"/>
				</td>
				<td class="alignTop machineClass">
					<ww:select name="'selectedMachine'" id="selectedMachine" multiple="true" size="5" listKey="id" 
					listValue="name" cssClass="'multi_sel'" cssStyle="'width: 160px;'" list="contractorMachine"/>
				</td>
			</tr>
		</table>	
		</td>
	</tr>
</table>
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <ww:if test="addDuplicate">
            			<input type="submit" id="doAddDuplicate" name="doAddDuplicate" value="<ds:lang text="DodajDoSlownikaDuplikat"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd">disabled="disabled"</ww:if>/>
            		</ww:if>
            		<ww:else>
            			
            			<input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="Dodaj"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id != null">disabled="disabled"</ww:if>/>
            			<input type="submit" id="doAddAndPlace" name="doAddAndPlace" value="<ds:lang text="DodajIumiescWFormularzu"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id != null">disabled="disabled"</ww:if>/>
            		</ww:else>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <ww:if test="popup">
                    	<input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    </ww:if>
                </td>
            </tr>
            <tr>

                 <td>
                 	<input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitForm()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
               		<input type="submit" id="doUpdate" name="doUpdate" value="<ds:lang text="ZapiszZmiany"/>" onclick="selectAllOptions(document.getElementById('selectedDsPatrons'));selectAllOptions(document.getElementById('selectedMarki'));selectAllOptions(document.getElementById('selectedMachine'));" class="btn" <ww:if test="!canEdit || id==null">disabled="disabled"</ww:if>/>
                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                	<span class="star">*</span>&nbsp;<ds:lang text="PoleObowiazkowe"/>
                </td>

            </tr>
        </table>
        	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>        
    </form>
</ww:if>
<ww:else>
    <table width="100%" class="mediumTable search">
        <tr>
            <th><ds:lang text="Nazwa"/></th>
            <th><ds:lang text="Nip"/></th>
            <th><ds:lang text="Regon"/></th>
            <th colspan="2"></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    	<td><ww:property value="name" /></td>
        	    	<td><ww:property value="nip" /></td>
                    <td><ww:property value="regon" /></td>
	               		<td><a href="<ww:url value="baseLink"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>">
	                        <ds:lang text="wybierz"/></a></td>
	                 <ww:if test="popup">
	                    <td><a href="javascript:submitStrona(<ww:property value="id" />,'<ww:property value="name" />','<ww:property value="phoneNumber"/>');">
	                    	<b><ds:lang text="UmiescWformularzu"/></b></a></td>
	                 </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="baseLink"><ww:param name="'param'" value="param"/></ww:url>';"/>
        	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:else>

<script language="JavaScript">

		function addPatron() {
			var jqOption = $j('#username option:selected').clone();
			
			$j('[name=selectedDsPatrons]').append(jqOption);
		}
		
		function delPatron() {
			$j('[name=selectedDsPatrons] option:selected').remove();
		}
        
        function onLoad()
        {
        	boxObjDostawca  = id('7');
        	boxObjDealer = id('8');
        	
        	if(boxObjDostawca.checked == true)
			{
				$j('td.machineClass').show();
			}
			else 
			{
				$j('td.machineClass').hide();
			}
			if(boxObjDealer.checked == true)
			{
				$j('td.markiClass').show();
			}
			else 
			{
				$j('td.markiClass').hide();
			}
        }
        onLoad();
		function changeRole(boxObj,name)
		{
			if(boxObj.checked == true)
			{
				if(name == 'Dostawca')
				{
					$j('td.machineClass').show();
				}
				else if(name == 'Dealer')
				{
					$j('td.markiClass').show();
				}
			}
			else
			{
				if(name == 'Dostawca')
				{
					$j('td.machineClass').hide();	
				}
				else if(name == 'Dealer')
				{
					$j('td.markiClass').hide();
				}
			}
		}
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        // walidacja kodu pocztowego

        <ww:if test="results == null || results.empty">
        var szv = new Validator(document.getElementById('kod'), '##-###');
				
        szv.onOK = function() 
        {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';			
			
        }
        szv.onEmpty = szv.onOK;
		
        szv.onError = function(code) {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001';
        }
        </ww:if>
        
        function notifyChange()
        {
            $j('#doSubmit').attr('disabled', true);
        }
        
        function confirmAdd()
        {
        		selectAllOptions(document.getElementById('selectedMarki'));
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function clearForm()
        {         
        	$j('form input[type=text]').val('');
            
        	$j('#doUpdate').attr('disabled', true);
        	$j('#doSubmit').attr('disabled', true);
			$j('#doAdd').attr('disabled', false);
			$j('#doAddAndPlace').attr('disabled', false);
            var tabRoles = document.getElementsByName('checkRoles');
            if(tabRoles != null)
            {
	            var i;
	            for( i = 0; i < tabRoles.length; i++)
	            {
	            	tabRoles[i].checked = false;
	            }
            }            
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitForm()
        {
        	return submitStrona(id('id').value,id('name').value,id('phoneNumber').value);
        } 
        
        function submitStrona(id,name,phone)
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = {};

            strona.id = id;
            strona.name = name;
            strona.phoneNumber = phone;

			if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>',strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            window.close();
        }
        <ww:if test="insertIntoForm">
        	submitForm();
        </ww:if>
</script>