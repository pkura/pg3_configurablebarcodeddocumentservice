<%-- 
    Document   : insurance-participant
    Created on : 2009-10-08, 09:37:43
    Author     : Micha� Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/common/insurance-participant.action'"/>" method="post">
	<ww:hidden name="'id'" value="id" id="id"/>
	<ww:hidden name="'param'" value="param" id="param"/>
	<table>
		<tr>
			<td colspan="2">Imi�<span class="star">*</span></td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'name'"
									id="name"
									size="40" maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2">Nazwisko</td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'surname'"
									id="surname"
									size="40"
									maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2">Email</td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'email'"
									id="email"
									size="40"
									maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2">PESEL</td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'pesel'"
									id="pesel"
									size="40"
									maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2">Numer polisy OC</td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'oc'"
									id="oc"
									size="40"
									maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td colspan="2">Numer polisy AC</td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'ac'"
									id="ac"
									size="40"
									maxlength="40"
									cssClass="'txt'"/></td>
		</tr>
		<tr><td colspan="2">
			<input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
			<input type="submit" name="doAdd" value="<ds:lang text="Dodaj"/>" class="btn"/>
			<ww:if test="id != null">
				<input type="button" value="<ds:lang text="Wybierz"/>" class="btn" onclick="save()"/>
			</ww:if>
		</td></tr>
	</table>
	<script type="text/javascript">
		function save(){
			 var strona = new Array();
			 strona.id = $j("#id").get(0).value;
			 strona.name = $j("#name").get(0).value;
			 strona.surname = $j("#surname").get(0).value;

			if (window.opener.accept) {
                window.opener.accept('<ww:property value="param"/>', strona);
				window.close();
            } else {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
            }
		}
	</script>
</form>
