<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Umowa</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/umowa.action'"/>" method="post" enctype="multipart/form-data">
<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'kontrahent_id'" value="kontrahent_id" id="kontrahent_id" />
<ww:hidden name="'param'" value="param" id="param"/>

<ww:if test="results == null || results.size() < 1">
<table>
	<tr>
		<td>Maksymalna kwota zam�wienia :</td>
		<td><ww:textfield name="'kwota'" size="20" id="kwota" cssClass="'txt'" value="kwota"/></td>
	</tr>
	<tr>
		<td>Termin obowi�zywania :</td>
        <td><ww:textfield name="'termin'" size="10" maxlength="10"
            cssClass="'txt'" id="termin" />
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="terminTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
	</tr>
    <tr>
		<td><ds:lang text="Dostawca"/><span class="star">*</span></td>
            <td><ww:textfield name="'nazwaKlienta'" id="nazwaKlienta" size="40" maxlength="60" cssClass="'txt'" readonly="readonly"/></td>
    </tr>
    <tr>
        	<td></td>
			<td>
				 <input type="button" id="wybierzK" value="Wybierz dostawc�" onclick="openDictionary_KLIENT()" class="btn" />
			</td>
	</tr>
    <ww:if test="id != null">
	<tr>
    	<td>Za��cznik :</td>
    	<ww:if test="zalacznik == null">
    		<td><ww:file name="'file'" id="file" size="30" cssClass="'txt'" /></td>
    	</ww:if>
    	<ww:else>
    		<td valign="top">
				<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+mostRecentRevision.id"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
				<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mostRecentRevision.mime)">
					&nbsp;
					<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+mostRecentRevision.id"/>"><img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
					&nbsp;
					<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/></a>
				</ww:if>
			</td>
    	</ww:else>
    </tr>
    <ww:if test="zalacznik == null">
    <tr>
    	<td></td>
    	<td><ds:event name="'doAddAtta'" value="'Za��cz'" confirm="'Na pewno za��czy�'"/></td>
    </tr>
    </ww:if>
    </ww:if>
    <tr>
		<td colspan="2">
		<ww:if test="id == null">
			<ds:event name="'doAdd'" value="'Dodaj'" onclick="'if (!validate()) return false;'" confirm="'Na pewno doda� pozycj� ?'"/>
		</ww:if>
		<ww:else>
			<ds:event name="'doUpdate'" value="'Zapisz'"  onclick="'validate();'" confirm="'Zapisa� zmiany ?'"/>
			 <input type="button" id="doSubmit" value="Umie�� w formularzu" 
			 onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
		</ww:else>
		<ds:event name="'doSearch'" value="'Szukaj'"/>
		</td>
	</tr>
</table>
</ww:if>
<ww:else>
	<table>
        <tr>
            <th><ds:lang text="Kwota"/></th>
            <th><ds:lang text="Klient"/></th>
            <th><ds:lang text="Termin"/></th>
            <th><ds:lang text="Zalacznik"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        			<td><ww:property value="kwota"/></td>
        	    	<td><ww:property value="kontrahent.getName()"/></td>        	    	
                    <td><ds:format-date value="termin" pattern="dd-MM-yy HH:mm"/></td>
                    <td>
                    	<ww:if test="zalacznik != null">
                    	<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+zalacznik.getMostRecentRevision().id"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
						<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(zalacznik.getMostRecentRevision().mime)">
							&nbsp;
							<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+zalacznik.getMostRecentRevision().id"/>"><img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
							&nbsp;
							<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="zalacznik.getMostRecentRevision().id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/></a>
						</ww:if>
						</ww:if>
                    </td>
                    <td><a href="<ww:url value="'/office/common/umowa.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/></ww:url>">
                    <ds:lang text="wybierz"/></a></td>
        	</tr>
    	</ww:iterator>
    	</table>
    	<ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>
</ww:else>
</form>
<script language="JavaScript">

Calendar.setup({
    inputField     :    "termin",     // id of the input field
    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
    button         :    "terminTrigger",  // trigger for the calendar (button ID)
    align          :    "Tl",           // alignment (defaults to "Bl")
    singleClick    :    true
});

function id(id)
{
    return document.getElementById(id);
}

function validate() 
{
	
	if(id('kwota').value == null || id('kwota').value.length < 1)
	{
		alert('Kwota nie mo�e by� pusta');
		return false;
	}
	if(id('termin').value == null || id('termin').value.length < 1)
	{
		alert('Termin obowi�zywania nie mo�e by� pusty');
		return false;
	}
	if(id('kontrahent_id').value == null || id('kontrahent_id').value.length < 1)
	{
		alert('Kontrahent musi by� wybrany');
		return false;
	}
	return true;
}

function setKlient()
{
    try
    {
    	if(document.getElementById('kontrahent_id') != null && document.getElementById('kontrahent_id').value == '')
    	{
    		var tmp = window.opener.getId_DOSTAWCA();
    		if(tmp != null && tmp.value != '')
    		{
	    		document.getElementById('kontrahent_id').value = tmp;
	    		document.getElementById('nazwaKlienta').value = window.opener.getdockind_DOSTAWCAname();
	    		document.getElementById('wybierzK').disabled = true;
    		}
    	}
    }        
	catch(err){alert(err);}	
}
setKlient();

function accept(param,map)
{
	document.getElementById('kontrahent_id').value = map.id;
	document.getElementById('nazwaKlienta').value = map.name;
}
	
function openDictionary_KLIENT()
{
	   openToolWindow('<ww:url value="'/office/common/dicinvoice.action'"/>?param=DOSTAWCA&id='+document.getElementById('kontrahent_id').value,'DOSTAWCA', 700, 650);
}

function submitStrona()
{
    if (!window.opener)
    {
        alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
        return;
    }   

    var strona = new Array();

    strona.id = id('id').value;
    strona.kwota = id('kwota').value;
    strona.kontrahent = id('nazwaKlienta').value;
    strona.termin = id('termin').value;

	if (window.opener.accept)
        window.opener.accept('UMOWA', strona);
    else
    {
        alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
        return;
    }
	saveArchive();
    return true;
}

function saveArchive()
{
	 var form = window.opener.document.forms[0];
		
	    var hid = window.opener.document.createElement('INPUT');
	    hid.type = 'HIDDEN';
	    hid.name = 'doArchive';
	    hid.value = 'true';
	    form.appendChild(hid);
	
	    for (var i=0; i < form.elements.length; i++)
	    {
	        var el = form.elements[i];
	        if (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button')
	        {
	            el.disabled = true;
	        }
	    }
	    selectAllOptions(window.opener.document.getElementById('dockind_POZYCJA_ZAMOWIENIA'));
	    form.submit();
		window.close();
}
</script>