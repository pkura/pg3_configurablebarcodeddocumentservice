<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N djPerson.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/office/common/djPersonDictionary.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'param'" value="param" id="param" />

<ww:if test="results == null || results.empty">
		<table>
			<tr>
				<td colspan="2"><ds:lang text="idPracownika"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'idPracownika'" id="idPracownika" size="20" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="imie"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'imie'" id="imie" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="nazwisko"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nazwisko'" id="nazwisko" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="pesel"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'pesel'" id="pesel" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="nrSap"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nrSap'" id="nrSap" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="nrPeoplesoft"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nrPeoplesoft'" id="nrPeoplesoft" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="pracuje"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:select name="'pracuje'" cssClass="'sel'" list="#{ true : 'Tak', false : 'Nie' }" value="pracuje" id="pracuje"/></td>                
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Jednostka"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:select list="sklepy" name="'nrsklepu'" id="nrsklepu" 
                listKey="id" listValue="numer+' '+miasto" cssClass="'sel'" headerKey="''" headerValue="'wybierz'" /></td>
            </tr>
            
            
    </td>
			
		</table>
		
		
		<table>
			<tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="id != null || !canAdd">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" <ww:if test="id == null || !canEdit">disabled="disabled"</ww:if>/>
                </td>
            </tr>
            <tr>
            	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
            <th>Id pracownika</th>
            <th>Imi�</th>
            <th>Nazwisko</th>
            <th>Pesel</th>
            <th><ds:lang text="nrSap"/></th>
            <th><ds:lang text="nrPeoplesoft"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    	<td><ww:property value="idPracownika" /></td>
        	    	<td><ww:property value="imie" /></td>
                    <td><ww:property value="nazwisko"/></td>
                    <td><ww:property value="pesel" /></td>
                    <td><ww:property value="nrSap" /></td>
                    <td><ww:property value="nrPeoplesoft" /></td>
                    <td><a href="<ww:url value="'/office/common/djPersonDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/djPersonDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm(getText('NaPewnoUsunacWpis'))) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/djPersonDictionary.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>
<script language="JavaScript">
        function id(id)
        {
            return document.getElementById(id);
        }

        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('idPracownika').value = '';
            document.getElementById('imie').value = '';
            document.getElementById('nazwisko').value = '';
            document.getElementById('pesel').value = '';
			document.getElementById('nrsklepu').value = '';
			document.getElementById('nrSap').value = '';
			document.getElementById('nrPeoplesoft').value = '';
			document.getElementById('pracuje').value = 'true';
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('id').value;
            strona.idPracownika = id('idPracownika').value;
            strona.imie = id('imie').value;
            strona.nazwisko = id('nazwisko').value;
            strona.pesel = id('pesel').value;
            strona.nrSap = id('nrSap').value;
            strona.nrPeoplesoft = id('nrPeoplesoft').value;
            
            var selectObj = id('nrsklepu');
            var i;
			i = selectObj.selectedIndex;
            if( i== null)
            {
            	alert('Nie wybrano sklepu');
            	return;
            }

			strona.sklep = selectObj.options[i].text;	
            strona.dictionaryDescription = '<ww:property value="dictionaryDescription"/>';

            if (isEmpty(strona.idPracownika)||isEmpty(strona.imie)||isEmpty(strona.nazwisko))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
			if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>