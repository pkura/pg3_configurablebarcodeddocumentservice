<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/dlApplicationDictionary.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'idKlienta'" id="idKlienta" />
		<ww:hidden name="'idDostawcy'" id="idDostawcy" />
        <ww:hidden name="'param'" value="param" id="param"/>

<ww:if test="results == null || results.empty">
		<table>
			<tr>
				<td colspan="1"><ds:lang text="NumerWniosku"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="1"><ww:textfield name="'numerWniosku'" id="numerWniosku" size="20" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
            	<td colspan="2">
	            	<table>
			            <tr>
							<td colspan="1"><ds:lang text="Klient"/><span class="star">*</span></td>
							<td></td>
							<td colspan="1"><ds:lang text="Dostawca"/></td>
						</tr>
						<tr>
			                <td><ww:textfield name="'nazwaKlienta'" id="nazwaKlienta" size="40" maxlength="60" cssClass="'txt'" readonly="readonly"/></td>
			                <td></td>
			                <td><ww:textfield name="'nazwaDostawcy'" id="nazwaDostawcy" size="40" maxlength="60" cssClass="'txt'" readonly="readonly"/></td>
			            </tr>
			            <tr>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzKlienta"/>" onclick="openDictionary_KLIENT()" class="btn" />
			    			</td>
			    			<td></td>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzDostawce"/>" onclick="openDictionary_DOSTAWCA()" class="btn" />
			    			</td>
			    		</tr>
			    	</table>
		    	</td>
		    </tr>
		    <tr>
            	<td colspan="2">
	            	<table>
			            <tr>
							<td colspan="1"><ds:lang text="Umowa"/></td>
						</tr>
						<tr>				
							<td><ww:select id="idUmowy" name="'idUmowy'" list="contractsIdsMap" value="idUmowy" cssStyle="'width: 250px;'" size="5"
								cssClass="'sel'" /></td>
			           </tr>
			            <tr>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzUmowe"/>" onclick="openDictionary_UMOWA()" class="btn" />
			    			</td>
			    		</tr>
			    	</table>
		    	</td>
		    </tr>
            <tr>
				<td colspan="1"><ds:lang text="Wartosc"/></td>
				<td colspan="1"><ds:lang text="WplataWlasna"/></td>
			</tr>
			<tr>
                <td colspan="1"><ww:textfield name="'wartosc'" id="wartosc" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            	 <td colspan="1"><ww:textfield name="'kwota'" id="kwota" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="1"><ds:lang text="LiczbaRat"/></td>
				<td colspan="1"><ds:lang text="WartoscWykupu"/></td>
			</tr>
			<tr>
                <td colspan="1"><ww:textfield name="'liczbaRat'" id="liczbaRat" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            	 <td colspan="1"><ww:textfield name="'wykup'" id="wykup" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
            	<td colspan="1"><ds:lang text="Rodzaj"/></td>
				<td colspan="1"><ds:lang text="Typ"/></td>
            </tr>
            <tr>
				<td colspan="1"><ww:select id="rodzaj" name="'rodzaj'" headerValue="getText('select.wybierz')" headerKey="''" 
				list="rodzajMap" cssClass="'sel'"/></td>
				<td colspan="1"><ww:select id="leasingObjectType" name="'leasingObjectType'" headerValue="getText('select.wybierz')" headerKey="''" 
				list="leasingObjectTypes" listKey="id" listValue="name" cssClass="'sel'"/></td>	
			</tr>
			<tr>
				<td colspan="2"><ds:lang text="opis"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textarea  name="'opis'" id="opis" cols="20" rows="4" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			
		</table>
		<table>
			<tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="id != null || !canAdd">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" <ww:if test="id == null || !canEdit">disabled="disabled"</ww:if>/>
                </td>
            </tr>
            <tr>
            	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
            <th><ds:lang text="NumerWniosku"/></th>
            <th><ds:lang text="IdKlienta"/></th>
            <th><ds:lang text="Opis"/></th>
            <th><ds:lang text="Wartosc"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    	<td><ww:property value="NumerWniosku" /></td>
        	    	<td><ww:property value="IdKlienta" /></td>
                    <td><ww:property value="Opis" /></td>
                    <td><ww:property value="Wartosc" /></td>
                    <td><a href="<ww:url value="'/office/common/dlApplicationDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/dlApplicationDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm(getText('NaPewnoUsunacWpis'))) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dlApplicationDictionary.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>
<script language="JavaScript">
        
        function setKlient()
        {
            try
            {
        		if (document.getElementById('id').value == '')
        		{
            		if(window.opener.document.getElementById('dockind_RODZAJ_KONTRAHENTA') != null)
            		{
		        		if( window.opener.document.getElementById('dockind_RODZAJ_KONTRAHENTA').selectedIndex == 1)
		        		{
	        				document.getElementById('idKlienta').value = window.opener.getId_KLIENT();
	        				document.getElementById('nazwaKlienta').value = window.opener.getdockind_KLIENTname();
	        				document.getElementById('idUmowy').value = window.opener.getId_NUMER_UMOWY();
    						document.getElementById('numerUmowy').value = window.opener.getdockind_NUMER_UMOWYnumerUmowy();
		        		}
		        		else
		        		{
		        			document.getElementById('idDostawcy').value = window.opener.getId_KLIENT();
	        				document.getElementById('nazwaDostawcy').value = window.opener.getdockind_KLIENTname();
	        				document.getElementById('idUmowy').value = window.opener.getId_NUMER_UMOWY();
    						document.getElementById('numerUmowy').value = window.opener.getdockind_NUMER_UMOWYnumerUmowy();
		        		}
            		}
            		else
            		{
            			document.getElementById('idKlienta').value = window.opener.getId_KLIENT();
        				document.getElementById('nazwaKlienta').value = window.opener.getdockind_KLIENTname();
        				document.getElementById('idDostawcy').value = window.opener.getId_DOSTAWCA();
        				document.getElementById('nazwaDostawcy').value = window.opener.getdockind_DOSTAWCAname();
        				document.getElementById('idUmowy').value = window.opener.getId_NUMER_UMOWY();
    					document.getElementById('numerUmowy').value = window.opener.getdockind_NUMER_UMOWYnumerUmowy();
            		}
	        	}
            }        
        	catch(err){}	
        }
        setKlient();
        
        function accept(param,map)
   		{
       		if(param == 'KLIENT')
       		{
       			document.getElementById('idKlienta').value = map.id;
       			document.getElementById('nazwaKlienta').value = map.name;
       		}
       		else if(param == 'DOSTAWCA')
       		{
       			document.getElementById('idDostawcy').value = map.id;
       			document.getElementById('nazwaDostawcy').value = map.name; 
       		}
       		else
       		{
    			//document.getElementById('idUmowy').value = map.id;
    			$j('#idUmowy').append('<option selected="selected" value="' + map.id + '">' + map.numerUmowy + '</option>')
    			//document.getElementById('numerUmowy').value = map.numerUmowy;
    			document.getElementById('idDostawcy').value = map.idDostawcy;
    			document.getElementById('nazwaDostawcy').value = map.nazwaDostawcy;
    			document.getElementById('idKlienta').value = map.idKlienta;
    			document.getElementById('nazwaKlienta').value = map.nazwaKlienta;
       		}
   		}
   		
   		function openDictionary_KLIENT()
    	{
     	   openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=KLIENT&id='+document.getElementById('idKlienta').value,'KLIENT', 700, 650);
  		}

   		function openDictionary_DOSTAWCA()
    	{
     	   openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=DOSTAWCA&id='+document.getElementById('idDostawcy').value,'KLIENT', 700, 650);
  		}

   		function openDictionary_UMOWA()
    	{
     	   openToolWindow('<ww:url value="'/office/common/dlContractDictionary.action'"/>?param=UMOWA&id='+document.getElementById('idUmowy').value,'KLIENT', 700, 650);
  		}
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function clearForm()
        {         
        	//document.getElementById('doUpdate').disabled = 'disabled';
			//document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('numerWniosku').value = '';
            document.getElementById('idKlienta').value = '';
            document.getElementById('idDostawcy').value = '';
            document.getElementById('nazwaDostawcy').value = '';
            document.getElementById('nazwaKlienta').value = '';
            document.getElementById('opis').value = '';
			document.getElementById('wartosc').value = '';
			document.getElementById('kwota').value = '';
			document.getElementById('liczbaRat').value = '';
			document.getElementById('wykup').value = '';
			document.getElementById('rodzaj').value = '';
			document.getElementById('doAdd').disabled = '';
			document.getElementById('idUmowy').value = '';
    		document.getElementById('numerUmowy').value = '';
    		document.getElementById('idDostawcy').value = '';
			document.getElementById('nazwaDostawcy').value = '';
			$j('#idUmowy option').remove();
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('id').value;
            strona.typ = 'Application';
            strona.numerWniosku = id('numerWniosku').value;
            strona.idUmowy = id('idUmowy').value;
            strona.numerUmowy = ($j('#numerUmowy option:selected').html() || '').trim();
            strona.idKlienta = id('idKlienta').value;
            strona.idDostawcy = id('idDostawcy').value;
            strona.nazwaKlienta = id('nazwaKlienta').value;
            strona.nazwaDostawcy = id('nazwaDostawcy').value;
            strona.dictionaryDescription = '<ww:property value="dictionaryDescription"/>';

            if (isEmpty(strona.numerWniosku)||isEmpty(strona.idKlienta))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
            
            
            
			if (window.opener.accept) 
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>