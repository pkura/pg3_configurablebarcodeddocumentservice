<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N agencja.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<ww:if test="closeOnReload">
    <script>window.close();</script>
</ww:if>
<ww:else>

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/agencja.action'"/>" method="post">

        <input type="hidden" name="closeOnReload" id="closeOnReload"/>
        <ww:hidden name="'param'" value="param"/>
        <ww:hidden name="'dictionaryType'" value="dictionaryType"/>
        <ww:hidden name="'canAddAndSubmit'" value="canAddAndSubmit"/>
        <ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'canEdit'" value="canEdit" id="canEdit"/>
        <ww:hidden name="'canDelete'" value="canDelete" id="canDelete"/>
        <ww:hidden name="'searchNazwa'" value="searchNazwa" id="searchNazwa"/>
        <ww:hidden name="'searchNumer'" value="searchNumer" id="searchNumer"/>
        <ww:hidden name="'searchNip'" value="searchNip" id="searchNip"/>
        <ww:hidden name="'forEdit'" value="forEdit" id="forEdit"/>

    <ww:if test="results == null || results.empty">

        <table>
            <tr>
                <td colspan="2"><ds:lang text="Nazwa"/></td>
            </tr>
            <tr>
                <td colspan="2"><ww:textfield name="'nazwa'" id="nazwa" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            </tr>
            <tr><td colspan="2"><ds:lang text="Numer"/></td></tr><tr><td colspan="2"><ww:textfield name="'numer'" id="numer" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
            <tr>
                <td colspan="2"><ds:lang text="NIP"/></td>
            </tr>
            <tr>
                <td colspan="2"><ww:textfield name="'nip'" id="nip" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            </tr>

            <tr><td colspan="2"><ds:lang text="Ulica"/></td></tr><tr><td colspan="2"><ww:textfield name="'ulica'" id="ulica" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
        </table>
        <table>
          <%--  <tr><td>Kod</td></tr><tr><td><ww:textfield name="'kod'" id="kod" size="15" maxlength="15" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
            <tr><td>Miejscowość</td></tr><tr><td><ww:textfield name="'miejscowosc'" id="miejscowosc" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
          --%>  <tr>
                <td><ds:lang text="Kod"/></td><td><ds:lang text="Miejscowosc"/></td>
            </tr>
            <tr>
                <td><ww:textfield name="'kod'" id="kod" size="10" maxlength="14" cssClass="'txt'" onchange="'notifyChange();'"/></td>
                <td><ww:textfield name="'miejscowosc'" id="miejscowosc" size="35" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            </tr>
            <tr>
                <td colspan="2"><span class="warning" id="zipError"> </span></td>
            </tr>
        </table>
        <%--    <tr><td colspan="2">Email</td></tr><tr><td colspan="2"><ww:textfield name="'email'" id="email" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
            <tr><td colspan="2">Faks</td></tr><tr><td colspan="2"><ww:textfield name="'faks'" id="faks" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
         --%>
        <table>
            <tr>
                <td><ds:lang text="Email"/></td><td><ds:lang text="Faks"/></td>
            </tr>
            <tr>
                <td><ww:textfield name="'email'" id="email" size="28" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
                <td><ww:textfield name="'faks'" id="faks" size="17" maxlength="30" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            </tr>


            <tr><td colspan="2"><ds:lang text="Telefon"/></td></tr><tr><td colspan="2"><ww:textfield name="'telefon'" id="telefon" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td></tr>
            <tr><td colspan="2"><ds:lang text="RodzajSieci"/></td></tr>
            <tr><td colspan="2">
                <select name="rodzaj_sieci" id="rodzaj_sieci" class="sel" onchange="notifyChange();">
                    <option>-- wybierz --</option>
                    <option value="1" <ww:if test="rodzaj_sieci == 1"> selected="selected" </ww:if>><ds:lang text="SiecWlasna"/></option>
                    <option value="2" <ww:if test="rodzaj_sieci == 2"> selected="selected" </ww:if>><ds:lang text="SiecZewnetrzna"/></option>
                    <option value="3" <ww:if test="rodzaj_sieci == 3"> selected="selected" </ww:if>><ds:lang text="Banki"/></option>
                </select>
            </td></tr>


        </table>

        <table>
            <tr>
                <td>

                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
                    <ww:if test="id == null">
                        <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAddOrUpdate()) return false;" class="btn" <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
                    </ww:if>
                    <ww:else>
                        <input type="submit" id="doUpdate" name="doUpdate" value="<ds:lang text="ZapiszZmianyWslowniku"/>" onclick="if (!confirmAddOrUpdate()) return false;" class="btn" <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
                    </ww:else>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>

            <tr>
                <td>
                    <ww:if test="!forEdit">
                        <input type="button" id="doSubmit" value="          <ds:lang text="UmiescWformularzu"/>          " onclick="if (!submitAgencja()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if><ww:else></ww:else>/>
                    </ww:if>
                    <ww:if test="afterSearch">
                        <input type="button" value="<ds:lang text="PowrocDoWynikow"/>" onclick="returnToResults();" class="btn"/>
                    </ww:if>
                </td>
            </tr>


        </table>



        <script language="JavaScript">
        function submitAgencja()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            /*if (!window.opener.__accept_agencja)
            {
                alert('W oknie wywołującym nie znaleziono funkcji __accept_agencja');
                return;
            } */

            var agencja = new Array();

            agencja.id = id('id').value;
            agencja.nazwa = id('nazwa').value;
            agencja.numer = id('numer').value;
            agencja.nip = id('nip').value;
            agencja.ulica = id('ulica').value;
            agencja.kod = id('kod').value;
            agencja.miejscowosc = id('miejscowosc').value;
            agencja.email = id('email').value;
            agencja.faks = id('faks').value;
            agencja.telefon = id('telefon').value;


            agencja.rodzaj_sieci = id('rodzaj_sieci').value;

           // alert(isEmpty(agencja.nazwa)+' '+isEmpty(agencja.rodzaj_sieci)+' '+
           //        (parseInt(agencja.rodzaj_sieci) > 1)+' '+(parseInt(agencja.rodzaj_sieci) <= 0));

            if ((isEmpty(agencja.nazwa)) || (isEmpty(agencja.rodzaj_sieci))
            || (parseInt(agencja.rodzaj_sieci) <= 0)
            || (isEmpty(agencja.nip) && (parseInt(agencja.rodzaj_sieci) > 1)))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }

            if (window.opener.__accept_agencja)
                window.opener.__accept_agencja(agencja);
            else if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', agencja);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> __accept_agencja');
                return;
            }

            return true;
        }

        function id(id)
        {
            return document.getElementById(id);
        }

        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = true;
        }

        function clearForm()
        {
            //alert('clear');
            //alert(document.getElementById('id'));
            var iden = document.getElementById('id').value;
            //alert(iden);
            if ((iden != null) && (iden != '')) {
                //alert('tak');
                var button = document.getElementById('doUpdate');
                //alert(button);
                button.setAttribute('id', 'doAdd');
                button.setAttribute('name', 'doAdd');
                button.setAttribute('value', '<ds:lang text="DodajDoSlownika"/>');
            }

            document.getElementById('id').value = '';
            document.getElementById('numer').value = '';
            document.getElementById('nazwa').value = '';
            document.getElementById('nip').value = '';
            document.getElementById('ulica').value = '';
            document.getElementById('kod').value = '';
            document.getElementById('miejscowosc').value = '';
            document.getElementById('email').value = '';
            document.getElementById('faks').value = '';
            document.getElementById('telefon').value = '';
            document.getElementById('rodzaj_sieci').selectedIndex = 0;

            notifyChange();
        }

        function confirmAddOrUpdate()
        {
            if (document.getElementById("doAdd") != null)
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
            else
                return confirm('<ds:lang text="NaPewnoZapisacZmianyWslowniku"/>?');
        }

        function returnToResults()
        {
              document.location.href = '<ww:url value="'/office/common/agencja.action'"/>'+
                '?doSearch=true'+
                '&nazwa=<ww:property value="searchNazwa"/>'+
                '&numer=<ww:property value="searchNumer"/>'+
                '&nip=<ww:property value="searchNip"/>'+
                '&kod=<ww:property value="searchKod"/>'+
                '&miejscowosc=<ww:property value="searchMiejscowosc"/>'+
                '&faks=<ww:property value="searchFaks"/>'+
                '&ulica=<ww:property value="searchUlica"/>'+
                '&telefon=<ww:property value="searchTelefon"/>'+
                '&rodzaj_sieci=<ww:property value="searchRodzaj_sieci"/>'+
                '&forEdit=<ww:property value="forEdit"/>';
        }


        // walidacja

        var szv = new Validator(document.getElementById('kod'), '##-###');

        szv.onOK = function() {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';
        }

        szv.onEmpty = szv.onOK;

        szv.onError = function(code) {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001';
        }

        szv.canValidate = function() {
            return true;
        }

        // aktualizacja formularza z okna wywolujacego
        <ww:if test="id != null">
            if ((window.opener) && ((window.opener.__check_accept_agencja) || (window.opener.checkAccept)))
            {
                var agencja = new Array();

                agencja.id = id('id').value;
                agencja.nazwa = id('nazwa').value;
                agencja.numer = id('numer').value;
                agencja.nip = id('nip').value;
                agencja.ulica = id('ulica').value;
                agencja.kod = id('kod').value;
                agencja.miejscowosc = id('miejscowosc').value;
                agencja.email = id('email').value;
                agencja.faks = id('faks').value;
                agencja.telefon = id('telefon').value;
                agencja.rodzaj_sieci = id('rodzaj_sieci').value;

                if (window.opener.__check_accept_agencja)
                    window.opener.__check_accept_agencja(agencja);
                else if (window.opener.checkAccept)
                    window.opener.checkAccept('<ww:property value="param"/>', agencja);
            }
        </ww:if>

        </script>

    </ww:if>

    <!-- wyniki wyszukiwania -->

    <ww:else>
        <table width="100%">
            <tr>
                <th><ds:lang text="Nazwa"/></th>
                <th><ds:lang text="Numer"/></th>
                <th><ds:lang text="NIP"/></th>
                <th><ds:lang text="Ulica"/></th>
                <th><ds:lang text="Kod"/></th>
                <th><ds:lang text="Miejscowosc"/></th>
                <th></th>
                <ww:if test="canDelete"><th></th></ww:if>
            </tr>
            <ww:iterator value="results">
                <tr>
                    <td><ww:property value="nazwa" /></td>
                    <td><ww:property value="numer" /></td>
                    <td><ww:property value="nip" /></td>
                    <td><ww:property value="ulica" /></td>
                    <td><ww:property value="kod" /></td>
                    <td><ww:property value="miejscowosc" /></td>
                    <td><a href="<ww:url value="'/office/common/agencja.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
                                 <ww:param name="'searchNazwa'" value="searchNazwa"/><ww:param name="'searchNumer'" value="searchNumer"/><ww:param name="'searchNip'" value="searchNip"/><ww:param name="'searchKod'" value="searchKod"/><ww:param name="'searchUlica'" value="searchUlica"/>
                                 <ww:param name="'searchMiejscowosc'" value="searchMiejscowosc"/><ww:param name="'searchRodzaj_sieci'" value="searchRodzaj_sieci"/><ww:param name="'searchTelefon'" value="searchTelefon"/><ww:param name="'searchEmail'" value="searchEmail"/><ww:param name="'afterSearch'" value="afterSearch"/><ww:param name="'forEdit'" value="forEdit"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                        <td><a href="<ww:url value="'/office/common/agencja.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/><ww:param name="'forEdit'" value="forEdit"/></ww:url>" onclick="if (!confirm('<ds:lang text="NaPewnoUsunacWpis"/> <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
                </tr>
            </ww:iterator>
        </table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/agencja.action'"><ww:param name="'param'" value="param"/><ww:param name="'forEdit'" value="forEdit"/></ww:url>';"/>
    </ww:else>

    </form>

    <script>
    var s = document.location.search;
    if (s != null && s.length > 0) s = s.substring(1);

    var ognlMap = null;
    var params = s.split('&');
    for (var i=0; i < params.length; i++)
    {
        var pair = params[i].split('=');
        var name = pair[0];
        var value = pair[1];
        if (name == 'ognlMap')
        {
            ognlMap = unescape(value);
            break;
        }
    }

    if (ognlMap != null)
    {
        eval('var map='+ognlMap);

        document.getElementById('id').value = map.id != null ? map.id : '';
        document.getElementById('nazwa').value = map.nazwa != null ? map.nazwa : '';
        document.getElementById('nip').value = map.nip != null ? map.nip: '';
        document.getElementById('ulica').value = map.ulica != null ? map.ulica : '';
        document.getElementById('kod').value = map.kod != null ? map.kod : '';
        document.getElementById('miejscowosc').value = map.miejscowosc != null ? map.miejscowosc : '';
        document.getElementById('email').value = map.email != null ? map.email : '';
        document.getElementById('faks').value = map.faks != null ? map.faks : '';
        document.getElementById('telefon').value = map.telefon != null ? map.telefon : '';




    }

    window.focus();

    </script>
</ww:else>