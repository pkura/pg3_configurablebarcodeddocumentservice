<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Klienci"/></h1><hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/dicprosika.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'param'" value="param" id="param"/>


<ww:if test="results == null || results.empty">
				<table>
			<tr>
				<td ><ds:lang text="nrEwidencyjny"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td  ><ww:textfield name="'nrEwidencyjny'" id="nrEwidencyjny" size="50" maxlength="14" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td  ><ds:lang text="nazwaKlienta"/></td>
			</tr>
			<tr>
                <td  ><ww:textfield name="'nazwaKlienta'" id="nazwaKlienta" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td  ><ds:lang text="NIP"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td  ><ww:textfield name="'nip'" id="nip" size="50" maxlength="10" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td  ><ds:lang text="regon"/></td>
			</tr>
			<tr>
                <td  ><ww:textfield name="'regon'" id="regon" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			
			<tr>
				<td><ds:lang text="as_miejscowosc"/></td>
				<td><ds:lang text="as_kod"/></td>
			</tr>
			<tr>
                <td><ww:textfield name="'as_miejscowosc'" id="as_miejscowosc" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
				<td><ww:textfield name="'as_kod'" id="as_kod" size="6" maxlength="6" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>

			<tr>
				<td><ds:lang text="as_ulica"/></td>
				<td><ds:lang text="as_nrdomu"/></td>
				<td><ds:lang text="as_nrlokalu"/></td>
			</tr>
			<tr>
                <td><ww:textfield name="'as_ulica'" id="as_ulica" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
				<td><ww:textfield name="'as_nrdomu'" id="as_nrdomu" size="10" maxlength="10" onchange="'notifyChange();'" cssClass="'txt'"/></td>
				<td><ww:textfield name="'as_nrlokalu'" id="as_nrlokalu" size="10" maxlength="10" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			
			
		</table>
		
		
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                </td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
			<th><ds:lang text="nrEwidencyjny"/></th>
            <th><ds:lang text="nazwaKlienta"/></th>
            <!-- 
            <th><ds:lang text="adresSiedziby"/></th>
             -->
            <th><ds:lang text="NIP"/></th>
			<th><ds:lang text="regon"/></th>
			<!--
			<th><ds:lang text="nrTelefonuGlowny"/></th> 
			 -->
    	</tr>
		<ww:iterator value="results">
        	<tr>
        		<td><ww:property value="nrEwidencyjny"/></td>
        	    <td><ww:property value="nazwaKlienta"/></td>
                <td><ww:property value="nip" /></td>
        	    <td><ww:property value="regon" /></td>
        	    <!--
        	    <td><ww:property value="nrTelefonuGlowny" /></td> 
        	     -->
        	    <td><a href="<ww:url value="'/office/common/dicprosika.action'">
        	    				<ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/>
        	    				<ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
								<ww:param name="'searchnrEwidencyjny'" value="searchnrEwidencyjny"/>
								<ww:param name="'searchnazwaKlienta'" value="searchnazwaKlienta"/>
                				<ww:param name="'searchnip'" value="searchnip"/>
								<ww:param name="'searchregon'" value="searchregon"/>
								<ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                <td><a href="<ww:url value="'/office/common/dicprosika.action'">
                	<ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/>
                	<ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Na pewno usun�� wpis <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                   </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dicprosika.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>



<script language="JavaScript">
		function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('nrEwidencyjny').value = '';
            document.getElementById('nazwaKlienta').value = '';
            document.getElementById('nip').value = '';
            document.getElementById('regon').value = '';
            //document.getElementById('nrTelefonuGlowny').value = '';
            document.getElementById('as_ulica').value = '';
            document.getElementById('as_kod').value = '';
            document.getElementById('as_miejscowosc').value = '';
            document.getElementById('as_nrdomu').value = '';
            document.getElementById('as_nrlokalu').value = '';
            //document.getElementById('ai_ulica').value = '';
            //document.getElementById('ai_kod').value = '';
            //document.getElementById('ai_miejscowosc').value = '';
            //document.getElementById('ai_nrdomu').value = '';
            //document.getElementById('ai_nrlokalu').value = '';
            //document.getElementById('ak_ulica').value = '';
            //document.getElementById('ak_kod').value = '';
            //document.getElementById('ak_miejscowosc').value = '';
            //document.getElementById('ak_nrdomu').value = '';
            //document.getElementById('ak_nrlokalu').value = '';
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('id').value;
            strona.nrEwidencyjny = id('nrEwidencyjny').value;
            strona.nazwaKlienta = id('nazwaKlienta').value;
            strona.nip = id('nip').value;
            strona.regon = id('regon').value;
            //strona.nrTelefonuGlowny = id('nrTelefonuGlowny').value;
            strona.as_ulica = id('as_ulica').value;
           	strona.as_kod = id('as_kod').value;
           	strona.as_miejscowosc = id('as_miejscowosc').value; 
           	strona.as_nrdomu = id('as_nrdomu').value;
           	strona.as_nrlokalu = id('as_nrlokalu').value;
           	//strona.ai_ulica = id('ai_ulica').value;
           	//strona.ai_kod = id('ai_kod').value;
           	//strona.ai_miejscowosc = id('ai_miejscowosc').value;
           	//strona.ai_nrdomu = id('ai_nrdomu').value;
           	//strona.ai_nrlokalu = id('ai_nrlokalu').value;
           	//strona.ak_ulica = id('ak_ulica').value;
           	//strona.ak_kod = id('ak_kod').value;
           	//strona.ak_miejscowosc = id('ak_miejscowosc').value;
           	//strona.ak_nrdomu = id('ak_nrdomu').value;
           	//strona.ak_nrlokalu = id('ak_nrlokalu').value;
            
            if (isEmpty(strona.nrEwidencyjny))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
            
            if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>