<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<h1><ds:lang text="Kontrahent"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="Kontrahent"/>
</p>
</ds:available>
<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img"/>
	<ds:xmlLink path="Kontrahent"/>
	</div>
</ds:available>
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form enctype="multipart/form-data" action="<ww:url value="'/office/common/contractor.action'"/>" method="post" >
	<ww:include page="/office/dictionaries/contractorBase.jsp"/>
</form>