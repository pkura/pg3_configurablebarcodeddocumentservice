<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/workerDictionary.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'param'" value="param" id="param" />

<ww:if test="results == null || results.empty">
<ww:hidden name="'doSearch'" id="doSearch"/>
<ww:hidden name="'doAdd'" id="doAdd"/>
		<table>
			<tr>
				<td colspan="2"><ds:lang text="Nr.ewidencyjny"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'idPracownika'" id="idPracownika" size="20" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="imie"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'imie'" id="imie" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="nazwisko"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nazwisko'" id="nazwisko" size="20" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
            	<td colspan="2"><ds:lang text="Instytucja"/><span class="star">*</span></td>
			</tr>
			<tr>
				<td>
					<ww:select name="'instytucja'" id="instytucja" list="instytucje"  cssClass="'sel'" value="instytucja"/>
				</td>
			</tr>
            <tr>
				<td colspan="2"><ds:lang text="pracuje"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:select name="'pracuje'" cssClass="'sel'" list="#{ true : 'Tak', false : 'Nie' }" value="pracuje" id="pracuje"/></td>
            </tr>
		</table>
		<table>
			<tr>
                <td>
                    <input type="submit" value="<ds:lang text="Szukaj"/>" onclick="Search(this);" class="btn searchBtn" <ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="button" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (confirmAdd()) AddPerson(this);" class="btn" <ww:if test="id != null || !canAdd">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    <script language="JavaScript">
						function Search(btn)
						{
							document.getElementById('doSearch').value = true;
							disabledAllBtn(btn);
							btn.form.submit();
							return true;
						}

						function AddPerson(btn)
						{
							document.getElementById('doAdd').value = true;
							disabledAllBtn(btn);
							btn.form.submit();
							return true;
						}
						
					</script >
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" <ww:if test="id == null || !canEdit">disabled="disabled"</ww:if>/>
                </td>
            </tr>
            <tr>
            	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
            </tr>
        </table>
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
            <th>Nr. ewidencyjny</th>
            <th>Imi�</th>
            <th>Nazwisko</th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    	<td><ww:property value="idPracownika" /></td>
        	    	<td><ww:property value="imie" /></td>
                    <td><ww:property value="nazwisko"/></td>
                    <td><a href="<ww:url value="'/office/common/workerDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/workerDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm(getText('NaPewnoUsunacWpis'))) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/workerDictionary.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>
<script language="JavaScript">
        function id(id)
        {
            return document.getElementById(id);
        }


        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }

        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }

        function clearForm()
        {
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('idPracownika').value = '';
            document.getElementById('imie').value = '';
            document.getElementById('nazwisko').value = '';
			document.getElementById('pracuje').value = 'true';
            notifyChange();
        }

        function id(id)
        {
            return document.getElementById(id);
        }

        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('id').value;
            strona.idPracownika = id('idPracownika').value;
            strona.imie = id('imie').value;
            strona.nazwisko = id('nazwisko').value;
            strona.dictionaryDescription = '<ww:property value="dictionaryDescription"/>';

            if (isEmpty(strona.idPracownika)||isEmpty(strona.imie)||isEmpty(strona.nazwisko))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
			if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>