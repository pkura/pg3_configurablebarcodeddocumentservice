<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dcdict.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Cz�onkowie"/></h1><hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/dcdict.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'param'" value="param" id="param"/>

<ww:if test="results == null || results.empty">
		<table>
			<tr>
				<td colspan="2"><ds:lang text="PESEL"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield  name="'pesel'" id="pesel" size="50" maxlength="11" onchange="'notifyChange();'" cssClass="'txt'" /></td>

            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Imie"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'name'" id="name" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Nazwisko"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'surname'" id="surname" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="NumerRachunku"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'accountNumber'" id="accountNumber" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			
		</table>
		
		
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
                    <ww:if test="id==null">
                    	<input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn"/>
                    </ww:if>
                    <ww:else>
                    	<input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" disabled="disabled"/>
                    </ww:else>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" name="doSubmit" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <ww:if test="id==null">
                     	  <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" disabled="disabled"/>
                     </ww:if>
                     <ww:else>
                     	  <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" />
                     </ww:else>
                   
                </td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
	<ww:hidden name="'memberFound'" value="true" id="memberFound"/>
    <table width="100%">
        <tr>
        	<th><ds:lang text="Imie"/></th>
            <th><ds:lang text="Nazwisko"/></th>
            <th><ds:lang text="PESEL"/></th>
            <th><ds:lang text="Numer rachunku"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        		<td><ww:property value="name" /></td>
        	    <td><ww:property value="surname" /></td>
                <td><ww:property value="pesel" /></td>
        	    <td><ww:property value="accountNumber" /></td> 
        	     <td><a href="<ww:url value="'/office/common/dcdict.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/>
                                 
                                 </ww:url>">
                        <ds:lang text="wybierz"/></a></td>
				<td><a href="<ww:url value="'/office/common/dcdict.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Na pewno usun�� wpis <ww:property value="shortSummary"/>?')) return false;">
				<ds:lang text="usun"/></a></td>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dcdict.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>



<script language="JavaScript">
	 	window.resizeTo(1000,700);

        
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = false;
            document.getElementById('id').value = null;
            document.getElementById('accountNumber').value = '';
            document.getElementById('name').value = '';
            document.getElementById('surname').value = '';
            document.getElementById('pesel').value = '';
            notifyChange();
        }
        
        function rewriteNumber()
		{
			if(!window.opener.document.URL.match('search-dockind-documents'))
			{
				var numerKonta = document.getElementById('accountNumber').value;
				window.opener.document.getElementById('dockind_NR_RACHUNKU').value = numerKonta;	
				var name = document.getElementById('name').value;
				window.opener.document.getElementById('dockind_IMIE').value = name;
				var surname = document.getElementById('surname').value;
				window.opener.document.getElementById('dockind_NAZWISKO').value = surname;
				var pesel = document.getElementById('pesel').value;
				window.opener.document.getElementById('dockind_PESEL').value = pesel;

					
			}
		}
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            /*if (!window.opener.__accept_agencja)
            {
                alert('W oknie wywo�uj�cym nie znaleziono funkcji __accept_agencja');
                return;
            } */
			rewriteNumber();
            var strona = new Array();

            strona.id = id('id').value;
            strona.name = id('name').value;
            strona.surname = id('surname').value;
            strona.accountNumber = id('accountNumber').value;
            strona.pesel = id('pesel').value;	
			
            if (isEmpty(strona.name)||isEmpty(strona.pesel)||isEmpty(strona.surname))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
       		if (window.opener.accept){

                window.opener.accept('<ww:property value="param"/>', strona);
            }
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>