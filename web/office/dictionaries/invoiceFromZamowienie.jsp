<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N centrum-kosztow.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<p>
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/invoiceFromZamowienie.action'"/>" method="post" enctype="multipart/form-data">
	<ww:hidden name="'id'" value="id" id="id"/>
	<ww:hidden name="'status'" value="status" id="status"/>
	
	<ww:if test="status == null || status == 0">
		<table>
			<tr><td>Numery zamowien</td><td></td></tr>
			<tr>
				<td>Od : </td>
				<td><ww:textfield name="'nrFrom'" id="nrFrom" size="20" maxlength="50" cssClass="'txt'" cssStyle="'text-align:right'"/></td>
			</tr>
			<tr>
				<td>Do : </td>
				<td><ww:textfield name="'nrTo'" id="nrTo" size="20" maxlength="50" cssClass="'txt'" cssStyle="'text-align:right'"/></td>
			</tr>
			<tr><td><input type="submit" id="doFind" name="doFind" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/></td><td></td></tr>
		</table>
		
	</ww:if>
	
	<ww:if test="status != null && status == 1">
		<table>
			<tr>
				<td></td>
				<td>Centrum</td>
				<td>Konto</td>
				<td>Kwota</td>
				<td>Dokument</td>
			</tr>
			<ww:iterator value="centra">
				<tr>
					<td><ww:checkbox name="'centrumIds'" id="check" fieldValue="id"/></td>
					<td><ww:property value="centrumCode" /></td> 
					<td><ww:property value="accountNumber" /></td> 
					<td><ww:property value="amount" /></td>
					<td><a href="#" onclick="openToolWindow('<ww:url value="'/repository/edit-dockind-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>');"><ww:property value="documentId" /></a></td>
				</tr>
			</ww:iterator>
			
			<tr>
			<td></td>
			<td>Kwota </td>
			<td><ww:textfield name="'kwota'" id="kwota" size="20" maxlength="50" cssClass="'txt'" cssStyle="'text-align:right'" readonly="true"/></td>
			<td></td>
			<td><input type="submit" id="doRewrite" name="doRewrite" value="<ds:lang text="Przepisz"/>" class="btn"/></td>
			</tr>
		</table>
	</ww:if>
	
	<ww:if test="status != null && status == 2">
	
	</ww:if>
	
</form>