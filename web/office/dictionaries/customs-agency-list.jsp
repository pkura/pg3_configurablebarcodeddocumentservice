<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<script type="text/javascript">
	function editRow(id){
		$j("#rowId"+id+" td:eq(1)").html("<input value='" + $j("#rowId"+id+" td:eq(1)").html() + "'>");
		$j("#rowId"+id+" td:eq(2)").html("<a href='javascript:submitEdit("+id+")'>Zapisz</a>");
	}
	function submitEdit(id){
		var name = $j("#rowId"+id+" input").attr("value");
		$j("#ajaxContent").hide("slow", function(){
			$j.post('<ww:url value="'/office/dictionaries/edit-customs-agency.action'"/>',{doEdit:"true",id:id,entryName:name}, function(data) {
				$j("#ajaxContent").html(data).show("slow");
			});
		});
	}
</script>
<table class="tab">
	<tr><th>ID</th><th colspan="2">Nazwa</th></tr>
<ww:iterator value="list"><tr id="rowId<ww:property value="getId()"/>">
	<td><ww:property value="getId()"/></td><td><ww:property value="title"/></td><td><a href="javascript:editRow(<ww:property value="getId()"/>)">Zmie�</a></td>
</tr></ww:iterator>
</table>
