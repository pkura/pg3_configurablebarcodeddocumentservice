<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N rockwellVendor.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/rockwellVendor.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'param'" value="param" id="param"/>


<ww:if test="results == null || results.empty">
		
		<table>
			<tr>
				<td colspan="2"><ds:lang text="Vendor Id"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'vendorId'" id="vendorId" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'" /></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Set Id"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'setId'" id="setId" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="Short Name"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'shortName'" id="shortName" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
			<tr>
				<td colspan="2"><ds:lang text="Name"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'name'" id="name" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="Location"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'location'" id="location" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="Bank Account Number"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'bankAccountNumber'" id="bankAccountNumber" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="Bank ID"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'bankId'" id="bankId" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="IBAN"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'iban'" id="iban" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="Address"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'address'" id="address" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
            <tr>
				<td colspan="2"><ds:lang text="City"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'city'" id="city" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td colspan="2"><ds:lang text="Payment Terms"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'paymentTerms'" id="paymentTerms" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>	
        </table>
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" onclick="if (!search()) return false;" class="btn searchBtn"/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" name="doSubmit" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitVendor()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" <ww:if test=" !canEdit || id==null">disabled="disabled"</ww:if>/>
                     <input type="button" name="doSubmit" id="doSubmit" value="Fill leading zeros" onclick="if (fillZero()) return false;" class="btn"/>																														
                </td>
            </tr>
            <tr>
            	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
<ww:hidden name="'vendorsFound'" value="true" id="vendorsFound"/>
    <table width="100%">
        <tr>
            <th><ds:lang text="Vendor ID"/></th>
            <th><ds:lang text="Set Id"/></th>
            <th><ds:lang text="Short Name"/></th>
            <th><ds:lang text="Name"/></th>
            <th><ds:lang text="Bank Account Number"/></th>
            <th><ds:lang text="Bank ID"/></th>
            
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    <td><ww:property value="vendorId" /></td>
        	    <td><ww:property value="setId" /></td>
                    <td><ww:property value="shortName" /></td>
                    <td><ww:property value="name" /></td>   
                    <td><ww:property value="bankAccountNumber" /></td> 
                    <td><ww:property value="bankId" /></td>              
                    <td><a href="<ww:url value="'/office/common/rockwellVendor.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
                                 <ww:param name="'searchNazwa'" value="searchNazwa"/><ww:param name="'searchNumer'" value="searchNumer"/><ww:param name="'searchNip'" value="searchNip"/><ww:param name="'searchKod'" value="searchKod"/><ww:param name="'searchUlica'" value="searchUlica"/>
                                 <ww:param name="'searchRegon'" value="searchRegon"/>
                                 <ww:param name="'searchMiejscowosc'" value="searchMiejscowosc"/><ww:param name="'searchTelefon'" value="searchTelefon"/><ww:param name="'searchEmail'" value="searchEmail"/><ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/rockwellVendor.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Are you sure to delete <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/rockwellVendor.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>



<script language="JavaScript">
        
        function search()
        {
        	return true;
        }

        
        function confirmAdd()
        {
        	if (isEmpty(id('vendorId').value)||isEmpty(id('setId').value)||isEmpty(id('name').value))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }       		
            return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function setFromOpener()
        {
        		if (document.getElementById('id').value == '')
        		{
        			if(document.getElementById('name')!=null)
        				document.getElementById('name').value = window.opener.getdockind_VENDOR_IDname();
        			if(document.getElementById('vendorId')!=null){
        				document.getElementById('vendorId').value = window.opener.getdockind_VENDOR_IDvendorId();
        			}
        			if(document.getElementById('bankAccountNumber')!=null)
        				document.getElementById('bankAccountNumber').value = window.opener.getdockind_VENDOR_IDbankAccountNumber();
        		}      	
        }
        setFromOpener();
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('vendorId').value = '';
            document.getElementById('setId').value = '';
            document.getElementById('shortName').value = '';
            if(document.getElementById('name')!=null)
           		 document.getElementById('name').value = '';
            document.getElementById('bankAccountNumber').value = '';
            document.getElementById('bankId').value = '';
            document.getElementById('iban').value = '';
            document.getElementById('address').value = '';
			document.getElementById('city').value = '';
			document.getElementById('location').value = '';
			document.getElementById('paymentTerms').value = '';
			notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitVendor()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }


            var vendor = new Array();

            vendor.id = id('id').value;
            vendor.vendorId = id('vendorId').value;
            vendor.setId = id('setId').value;
            vendor.name = id('name').value;
            vendor.shortName = id('shortName').value;
            vendor.bankAccountNumber = id('bankAccountNumber').value;
            
            
			
			if (window.opener.accept)
			{
				//window.opener.accept('<ww:property value="param"/>', vendor);
				//window.opener.accept('Vendor_id', vendor);
				//window.opener.accept_VENDOR_ID(vendor);
				window.opener.accept('<ww:property value="param"/>', vendor);
            }
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
        function fillZero()
        {
        	var vendorIdObj = document.getElementById('vendorId');
        	if(vendorIdObj.value.length < 10 )
        	{
        		var i = 0;
        		var j = 10 - vendorIdObj.value.length;
				for(i=0;i < j;i++)
				{
					vendorIdObj.value = '0'+vendorIdObj.value;
				}
        	}        	
        }
</script>