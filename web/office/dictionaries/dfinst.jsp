<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dfinst.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/dfinst.action'"/>" method="post">
        <ww:hidden name="'kraj'" value="kraj" id="kraj"/>
		<ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'param'" value="param" id="param"/>


<ww:if test="results == null || results.empty">

		<table>
			<tr>
				<td colspan="2"><ds:lang text="Nazwa"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'name'" id="name" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="NIP"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nip'" id="nip" size="50" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="StaraNazwa"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'oldname'" id="oldname" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Regon"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'regon'" id="regon" size="30" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td colspan="2"><ds:lang text="Imie"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'imie'" id="imie" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td colspan="2"><ds:lang text="Nazwisko"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nazwisko'" id="nazwisko" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="Ulica"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'ulica'" id="ulica" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td>
                	<ds:lang text="Kod"/>
               	</td>
               	<td>
               		<ds:lang text="Miejscowosc"/>
               	</td>
            </tr>
            <tr>
                <td>
                	<ww:textfield name="'kod'" id="kod" size="14" maxlength="14" onchange="'notifyChange();'" cssClass="'txt'"/>
                </td>
                <td>
                	<ww:textfield name="'miejscowosc'" id="miejscowosc" size="34" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><span class="warning" id="zipError"> </span></td>
            </tr>            
            <tr>
				<td colspan="2"><ds:lang text="Kraj"/>:</td>
			</tr>
			<tr>
			    <td colspan="2">
			    	<ww:select name="'senderCountry'"
				        list="@pl.compan.docusafe.util.Countries@COUNTRIES"
				        listKey="alpha2" listValue="name" value="kraj"
				        id="senderCountry" cssClass="'sel'" onchange="'this.form.kod.validator.revalidate();'">
				    </ww:select>
 	            </td>
 	        </tr>
 	        
 	        <tr>
 	        	<td>
 	        		<ds:lang text="NumerTelefonu"/>
 	        	</td>
 	        	<td>
 	        		<ds:lang text="NumerFaksu"/>
 	        	</td>
 	        </tr>
			<tr>
                <td>
                	<ww:textfield name="'telefon'" id="telefon" size="14" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/>
               	</td>
                <td>
                	<ww:textfield name="'faks'" id="faks" size="34" maxlength="35" onchange="'notifyChange();'" cssClass="'txt'"/>
               	</td>               	
            </tr>           
            <tr>
            	<td colspan="2">
	            	<ds:lang text="Email"/>
            	</td>
            </tr>
            <tr>
                <td colspan="2">
                	<ww:textfield name="'email'" id="email" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/>
               	</td>            
            </tr>
		</table>
		
		
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                </td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
            <th><ds:lang text="Nazwa"/></th>
            <th><ds:lang text="NIP"/></th>
            <th><ds:lang text="StaraNazwa"/></th>
            <th><ds:lang text="Imie"/></th>
            <th><ds:lang text="Nazwisko"/></th>
            <th><ds:lang text="Regon"/></th>
            <th><ds:lang text="Miejscowosc"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    <td><ww:property value="name" /></td>
        	    <td><ww:property value="nip" /></td>
                    <td><ww:property value="oldname" /></td>
                    <td><ww:property value="imie" /></td>
                    <td><ww:property value="nazwisko" /></td>
                    <td><ww:property value="regon" /></td>
                    <td><ww:property value="kod" /></td>
                    <td><ww:property value="miejscowosc" /></td>
                    <td><a href="<ww:url value="'/office/common/dfinst.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
                                 <ww:param name="'searchNazwa'" value="searchNazwa"/><ww:param name="'searchNumer'" value="searchNumer"/><ww:param name="'searchNip'" value="searchNip"/><ww:param name="'searchKod'" value="searchKod"/><ww:param name="'searchUlica'" value="searchUlica"/>
                                 <ww:param name="'searchRegon'" value="searchRegon"/>
                                 <ww:param name="'searchMiejscowosc'" value="searchMiejscowosc"/><ww:param name="'searchTelefon'" value="searchTelefon"/><ww:param name="'searchEmail'" value="searchEmail"/><ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/dfinst.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Na pewno usun�� wpis <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dfinst.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>



<script language="JavaScript">
	    var szv = new Validator(document.getElementById('kod'), '##-###');

        szv.onOK = function() 
        {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';
        }

        szv.onEmpty = szv.onOK;

        szv.onError = function(code) 
        {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001';
        }

        szv.canValidate = function() 
        {
            return true;
        }
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('imie').value = '';
            document.getElementById('nazwisko').value = '';
            document.getElementById('name').value = '';
			document.getElementById('oldname').value = '';
			document.getElementById('regon').value = '';
			document.getElementById('nip').value = '';
            document.getElementById('ulica').value = '';
            document.getElementById('kod').value = '';
            document.getElementById('miejscowosc').value = '';
            document.getElementById('email').value = '';
            document.getElementById('faks').value = '';
            document.getElementById('telefon').value = '';
            document.getElementById('senderCountry').value = 'PL';
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            /*if (!window.opener.__accept_agencja)
            {
                alert('W oknie wywo�uj�cym nie znaleziono funkcji __accept_agencja');
                return;
            } */

            var strona = new Array();

            strona.id = id('id').value;
            strona.imie = id('imie').value;
            strona.nazwisko = id('nazwisko').value;
            strona.name = id('name').value;
			strona.oldname = id('oldname').value;
			strona.regon = id('regon').value;
			strona.nip = id('nip').value;
            strona.ulica = id('ulica').value;
            strona.kod = id('kod').value;
            strona.miejscowosc = id('miejscowosc').value;
            strona.telefon = id('telefon').value;
            strona.faks = id('faks').value;
            strona.email = id('email').value;
			strona.kraj = id('kraj').value;

			

           // alert(isEmpty(agencja.nazwa)+' '+isEmpty(agencja.rodzaj_sieci)+' '+
           //        (parseInt(agencja.rodzaj_sieci) > 1)+' '+(parseInt(agencja.rodzaj_sieci) <= 0));

            if (isEmpty(strona.name)||isEmpty(strona.nip))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
           /* if (window.opener.__accept_inst)
			{
				window.opener.__accept_inst(strona);
			}
			else if (window.opener.accept_INST) 
			{
				window.opener.accept_INST(strona);
			}
            else*/ if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>