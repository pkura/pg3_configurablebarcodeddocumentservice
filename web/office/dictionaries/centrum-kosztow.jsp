<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N centrum-kosztow.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:if test="afterAction != null">
    <script type="text/javascript">    
    	function finishAction()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }
            <ww:if test="afterAction == 'add'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptAddCentrum');
                    return;
                }
                window.opener.acceptAddCentrum('<ww:property value="id"/>','<ww:property value="description"/>','<ww:property value="amount"/>','<ww:property value="simpleAcceptance"/>');
                window.opener.doSave();
            </ww:if>
            
            <ww:if test="afterAction == 'LoadFile'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptAddCentrum');
                    return;
                }
                                
                <ww:if test="addedFromFile == null || addedFromFile.size() < 1">
                	alert("Nie dodano zadnych wartosci");
                	return;
                </ww:if>
                <ww:iterator value="addedFromFile">
                	window.opener.acceptAddCentrum('<ww:property value="ff_id"/>','<ww:property value="ff_description"/>','<ww:property value="ff_amount"/>'); 
                </ww:iterator>
                window.opener.doSave();
            </ww:if>
            
            <ww:elseif test="afterAction == 'delete'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptDeleteCentrum');
                    return;
                }
                window.opener.acceptDeleteCentrum('<ww:property value="param"/>');
                window.opener.doSave();
            </ww:elseif>
            <ww:elseif test="afterAction == 'update'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptUpdateCentrum');
                    return;
                }
                window.opener.acceptUpdateCentrum('<ww:property value="param"/>','<ww:property value="description"/>','<ww:property value="amount"/>');
                window.opener.doSave();
            </ww:elseif>
            
            window.close();
        }
        
        finishAction();
    </script>
</ww:if>
<ww:else>

    <p><h3><ww:if test="id == null">Dodawanie</ww:if><ww:else>Edycja</ww:else> centrum kosztowego</h3></p>

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/centrum-kosztow.action'"/>" method="post" enctype="multipart/form-data">

        <ww:hidden name="'param'" value="param"/>
        <ww:hidden name="'id'" value="id" id="id"/>
        <ww:hidden name="'documentId'" value="documentId" id="documentId"/>

        <table>
        <tr>
            <td>Kod(nazwa) centrum</td>
        </tr>
        <tr>
            <td>
                <ww:if test="id == null"><ww:select name="'centrumId'" value="centrumId" cssClass="'sel'" list="centra" listKey="id" listValue="symbol"/></ww:if>
                <ww:else><b><ww:property value="code"/></b></ww:else>
            </td>            
        </tr>
        <tr>
            <td><ds:lang text="NumerKonta"/></td>
        </tr>
        <tr>
            <td><ww:select name="'accountNumber'" id="accountNumber" value="accountNumber" list="accountNumbers" listKey="number" listValue="accountFullName" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')"/></td>
        </tr>
        
        <!-- adds -->
        <ww:if test="showDescriptionAmount">
        <tr>
            <td><ds:lang text="Opis"/></td>
        </tr>
        <tr>	
            <td><ww:textarea  name="'descriptionAmount'" id="descriptionAmount" cols="40" rows="4" cssClass="'txt'"/></td>
        </tr>
        </ww:if>
        <!-- adds:end -->
        
        <tr>
            <td><ds:lang text="KwotaCzastkowaBrutto"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'amount'" id="amount" size="20" maxlength="50" cssClass="'txt'" cssStyle="'text-align:right'" readonly="accepted"/></td>
        </tr>                
        </table>

        <table>
            <tr>
                <td>
                    <ww:if test="id == null">
                        <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="Dodaj"/>" onclick="if (!confirmAdd()) return false;" class="btn" />
                    </ww:if>
                    <ww:else>
                         <input type="submit" id="doUpdate" name="doUpdate" value="<ds:lang text="Zapisz"/>" onclick="if (!confirmUpdate()) return false;" class="btn saveBtn" />
                         <input type="submit" id="doDelete" name="doDelete" value="<ds:lang text="Usun"/>" onclick="if (!confirmDelete()) return false;" class="btn" <ww:if test="accepted">disabled="disabled"</ww:if>/>
                    </ww:else>                            
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>                
            </tr>
        </table>
		<ww:if test="id == null">
		<p><h3>Import danych do faktury</h3></p>
		<table>
		<tr>
           	<td>Zaladuj centra i konta z pliku:</td>
        </tr>
        <tr>
           	<td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
        	<td>
        		<input type="submit" id="doLoadFile" name="doLoadFile" value="<ds:lang text="Zaladuj"/>" onclick="if (!confirmMultiAdd()) return false;" class="btn" />
        	</td>
        </tr>
	</table>
	</ww:if>
	</form>

    <script type="text/javascript">
    
    	function confirmMultiAdd()
    	{
    		if (!validate())
                return false;
            return confirm('<ds:lang text="NapewnoZaladowacDaneZPliku"/>?');
    	}
    
        function confirmAdd()
        {
            if (!validate())
                return false;
            return confirm('<ds:lang text="NaPewnoPrzypisacDoFakturyToCentrumKosztowe"/>?');
        }
        
        function confirmUpdate()
        {
            if (!validate())
                return false;
            return confirm('<ds:lang text="NaPewnoZapisacZmiany"/>?');
        }

        function confirmDelete()
        {
            return confirm('<ds:lang text="NaPewnoUsunacZfakturyToCentrum"/>?');
        }
        
        function isNeed()
    	{
    		var ids = new Array();
			
			<ww:iterator value="accountNumbers">
			<ww:set name="result"/>
				<ww:if test="#result.needdescription">
					ids[ids.length] = "<ww:property value="#result.number"/>";
				</ww:if>
			</ww:iterator>
			
			if(ids.length > 0)
			{
				for(i=0;i<ids.length;i++)
				{
					if(ids[i] == document.getElementById('accountNumber').options[document.getElementById('accountNumber').selectedIndex].value)
					{
						return true;
					}					
				}
			}
			return false;			
    	}
        
        function validate()
        {
            var val = document.getElementById('amount').value;
			
			if(val.contains(',') && (val.length - val.lastIndexOf(',')) > 3) 
			{
				alert('<ds:lang text="ZaDuzoCyfrPoPrzecinku"/>');
				return false;
			}

			if(val.contains('.') && (val.length - val.lastIndexOf('.')) > 3) 
			{
				alert('<ds:lang text="ZaDuzoCyfrPoPrzecinku"/>');
				return false;
			}
			
            if (val > <ww:property value="maxAmount"/>)
            {
                alert('<ds:lang text="KwotaCzastkowaNieMozeBycWiekszaNiz"/> <ww:property value="maxAmount"/>!');
                return false;
            }
            if(isNeed())
            {
            	if(document.getElementById('descriptionAmount').value == null || document.getElementById('descriptionAmount').value == "")
            	{
            		alert('Wymagane pole opis'+document.getElementById('descriptionAmount').value);
            		return false;
            	}
            }    
            return true;
        }
    </script>
</ww:else>
