<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/office/common/dlContractDictionary.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'idKlienta'" id="idKlienta" />
		<ww:hidden name="'idDostawcy'" id="idDostawcy" />
		<ww:hidden name="'idWniosku'" id="idWniosku" />
        <ww:hidden name="'param'" value="param" id="param"/>
<ww:if test="results == null || results.empty">
		<table>
			<tr>
				<td colspan="2"><ds:lang text="NumerUmowy"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'numerUmowy'" id="numerUmowy" size="20" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
        	<tr>
            	<td>
	            	<table>
			            <tr>
							<td colspan="1"><ds:lang text="Klient"/><span class="star">*</span></td>
							<td></td>
							<td colspan="1"><ds:lang text="Dostawca"/><span class="star">*</span></td>							
						</tr>
						<tr>
			                <td><ww:textfield name="'nazwaKlienta'" id="nazwaKlienta" size="40" maxlength="60" cssClass="'txt'" readonly="'true'"/></td>
			                <td></td>
			                <td><ww:textfield name="'nazwaDostawcy'" id="nazwaDostawcy" size="40" maxlength="60" cssClass="'txt'" readonly="'true'"/></td>
			            </tr>
			            <tr>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzKlienta"/>" onclick="openDictionary_KLIENT()" class="btn" />
			    			</td>
			    			<td></td>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzDostawce"/>" onclick="openDictionary_DOSTAWCA()" class="btn" />
			    			</td>
			    		</tr>
			    	</table>
		    	</td>
		    </tr>
		    <tr>
            	<td>
	            	<table>
			            <tr>
							<td colspan="2"><ds:lang text="Wniosek"/></td>
						</tr>
						<tr>
			                <td><ww:textfield name="'numerWniosku'" id="numerWniosku" size="40" maxlength="60" cssClass="'txt'" readonly="readonly"/></td>
			           </tr>
			            <tr>
			    			<td>
			    				 <input type="button" value="<ds:lang text="WybierzWniosek"/>" onclick="openDictionary_WNIOSEK()" class="btn" />
			    			</td>
			    		</tr>
			    	</table>
		    	</td>
		    </tr>
            <tr>
				<td colspan="2"><ds:lang text="OpisUmowy"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'opisUmowy'" id="opisUmowy" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td colspan="2"><ds:lang text="WartoscUmowy"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'wartoscUmowy'" id="wartoscUmowy" size="10" maxlength="20" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
            	<td colspan="2"><ds:lang text="DataUmowy"/>
            	<ww:textfield name="'dataUmowy'" size="10" maxlength="10" cssClass="'txt'" id="dataUmowy"/>
       				 <img src="<ww:url value="'/calendar096/img.gif'"/>"
           				 id="dataUmowyTrigger" style="cursor: pointer; border: 1px solid red;"
           				 title="Date selector" onmouseover="this.style.background='red';"
            			onmouseout="this.style.background=''"/>
            	</td>
            	
            </tr>
    		<tr>
				<td colspan="2"><ds:lang text="DataKoncaUmowy"/>
				<ww:textfield name="'dataKoncaUmowy'" size="10" maxlength="10" cssClass="'txt'" id="dataKoncaUmowy"/>
       				 <img src="<ww:url value="'/calendar096/img.gif'"/>"
           				 id="dataKoncaUmowyTrigger" style="cursor: pointer; border: 1px solid red;"
           				 title="Date selector" onmouseover="this.style.background='red';"
            			onmouseout="this.style.background=''"/>
				</td>
            </tr>
			<tr>
				<td colspan="2">
    			</td>
    		</tr>
    		<tr>    		
				<td colspan="2">
    			</td>
			</tr>
			<tr>
				<td colspan="2"><ds:lang text="Typ"/>
				<ww:select id="leasingObjectType" name="'leasingObjectType'" headerValue="getText('select.wybierz')" headerKey="''" 
					list="leasingObjectTypes" listKey="id" listValue="name" cssClass="'sel'"/></td>	
			</tr>
			<tr>
				<td colspan="2"><ds:lang text="Status"/>
				<ww:select id="status" name="'status'" headerValue="getText('select.wybierz')" headerKey="''" list="statusMap"  cssClass="'sel'"/></td>
			</tr>
			<tr>
				<td colspan="2"><ds:lang text="Stan"/>
				<ww:select id="status" name="'stan'" headerValue="getText('select.wybierz')" headerKey="''" list="stanMap"  cssClass="'sel'"/></td>
			</tr>
		</table>
		
		
		<table>
			<tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="id != null || !canAdd">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" <ww:if test="id == null || !canEdit">disabled="disabled"</ww:if>/>
                </td>
            </tr>
            <tr>
            	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
            </tr>
        </table>        
    </form>
    
    <script language="JavaScript">
	
	    Calendar.setup({
	        inputField     :    "dataUmowy",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "dataUmowyTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	        Calendar.setup({
	        inputField     :    "dataKoncaUmowy",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "dataKoncaUmowyTrigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
   
    </script>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
            <th><ds:lang text="NumerUmowy"/></th>
            <th><ds:lang text="IdKlienta"/></th>
            <th><ds:lang text="OpisUmowy"/></th>
            <th><ds:lang text="WartoscUmowy"/></th>
            <th><ds:lang text="DataUmowy"/></th>
            <th><ds:lang text="DataKoncaUmowy"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        	    	<td><ww:property value="NumerUmowy" /></td>
        	    	<td><ww:property value="IdKlienta" /></td>
                    <td><ww:property value="OpisUmowy" /></td>
                    <td><ww:property value="WartoscUmowy" /></td>
                    <td><ww:property value="DataUmowy" /></td>
                    <td><ww:property value="DataKoncaUmowy" /></td>
                    <td><a href="<ww:url value="'/office/common/dlContractDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/dlContractDictionary.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm(getText('NaPewnoUsunacWpis'))) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dlContractDictionary.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>
<script language="JavaScript">
        function id(id)
        {
            return document.getElementById(id);
        }
        
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function clearForm()
        {         
        	//document.getElementById('doUpdate').disabled = 'disabled';
			//document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('numerUmowy').value = '';
            document.getElementById('idKlienta').value = '';
            document.getElementById('nazwaKlienta').value = '';
            document.getElementById('opisUmowy').value = '';
            document.getElementById('status').value = '';
			document.getElementById('wartoscUmowy').value = '';
			document.getElementById('dataUmowy').value = '';
			document.getElementById('dataKoncaUmowy').value = '';
			document.getElementById('idDostawcy').value = '';
			document.getElementById('nazwaDostawcy').value = '';
			document.getElementById('dataKoncaUmowy').value = '';
			document.getElementById('idWniosku').value = '';
    		document.getElementById('numerWniosku').value = '';
            notifyChange();
        }
        
        function setKlient()
        {
            try
            {
        		if (document.getElementById('id').value == '')
        		{
            		if(window.opener.document.getElementById('dockind_RODZAJ_KONTRAHENTA') != null)
            		{
		        		if( window.opener.document.getElementById('dockind_RODZAJ_KONTRAHENTA').selectedIndex == 1)
		        		{
	        				document.getElementById('idKlienta').value = window.opener.getId_KLIENT();
	        				document.getElementById('nazwaKlienta').value = window.opener.getdockind_KLIENTname();
	        				document.getElementById('idWniosku').value = window.opener.getId_NUMER_WNIOSKU();
    						document.getElementById('numerWniosku').value = window.opener.getdockind_NUMER_WNIOSKUnumerWniosku();

		        		}
		        		else
		        		{
		        			document.getElementById('idDostawcy').value = window.opener.getId_KLIENT();
	        				document.getElementById('nazwaDostawcy').value = window.opener.getdockind_KLIENTname();
	        				document.getElementById('idWniosku').value = window.opener.getId_NUMER_WNIOSKU();
    						document.getElementById('numerWniosku').value = window.opener.getdockind_NUMER_WNIOSKUnumerWniosku();
		        		}
            		}
            		else
            		{
            			document.getElementById('idKlienta').value = window.opener.getId_KLIENT();
        				document.getElementById('nazwaKlienta').value = window.opener.getdockind_KLIENTname();
        				document.getElementById('idDostawcy').value = window.opener.getId_DOSTAWCA();
        				document.getElementById('nazwaDostawcy').value = window.opener.getdockind_DOSTAWCAname();
        				document.getElementById('idWniosku').value = window.opener.getId_NUMER_WNIOSKU();
    					document.getElementById('numerWniosku').value = window.opener.getdockind_NUMER_WNIOSKUnumerWniosku();
            		}
	        	}
            }        
        	catch(err){}	
        }
        setKlient();
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();
            strona.id = id('id').value;
            strona.typ = 'Contract';
            strona.numerUmowy = id('numerUmowy').value;
            strona.idWniosku = id('idWniosku').value;
            strona.numerWniosku = id('numerWniosku').value;
            strona.idKlienta = id('idKlienta').value;
            strona.idDostawcy = id('idDostawcy').value;
            strona.nazwaKlienta = id('nazwaKlienta').value;
            strona.nazwaDostawcy = id('nazwaDostawcy').value;
            strona.dictionaryDescription = '<ww:property value="dictionaryDescription"/>';

            if (isEmpty(strona.numerUmowy)||isEmpty(strona.idKlienta))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
            
			if (window.opener.accept)
			{
                window.opener.accept('<ww:property value="param"/>', strona);
			}
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
        
   		function openDictionary_KLIENT()
    	{
     	   openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=KLIENT&id='+document.getElementById('idKlienta').value,'KLIENT', 700, 650);
  		}

   		function openDictionary_DOSTAWCA()
    	{
     	   openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=DOSTAWCA&id='+document.getElementById('idDostawcy').value,'KLIENT', 700, 650);
  		}

   		function openDictionary_WNIOSEK()
    	{
     	   openToolWindow('<ww:url value="'/office/common/dlApplicationDictionary.action'"/>?param=WNIOSEK&id='+document.getElementById('idWniosku').value,'KLIENT', 700, 650);
  		}
    
   	 	function accept(param,map)
		{
    		if(param == 'KLIENT')
    		{
    			document.getElementById('idKlienta').value = map.id;
    			document.getElementById('nazwaKlienta').value = map.name;
    		}
    		else if(param == 'DOSTAWCA')
    		{
    			document.getElementById('idDostawcy').value = map.id;
    			document.getElementById('nazwaDostawcy').value = map.name; 
    		}
    		else
    		{
    			document.getElementById('idWniosku').value = map.id;
    			document.getElementById('numerWniosku').value = map.numerWniosku;
    			document.getElementById('idDostawcy').value = map.idDostawcy;
    			document.getElementById('nazwaDostawcy').value = map.nazwaDostawcy;
    			document.getElementById('idKlienta').value = map.idKlienta;
    			document.getElementById('nazwaKlienta').value = map.nazwaKlienta;
    		}
		}
</script>