<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Pozycja zam�wienia</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/item-order.action'"/>" method="post">
<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'param'" value="param" id="param"/>
<table>
	<tr>
		<td>Konto :</td>
		<td><ww:select id="productGroupId" name="'productGroupId'" list="productGroups"
		 		listKey="id" listValue="name" cssClass="'sel'" value="productGroupId"/></td>
	</tr>
	<tr>
		<td>Opis:</td>
		<td><ww:textarea name="'description'" id="description" cols="50" cssClass="'txt'" rows="4" ></ww:textarea></td>
	</tr>
	<tr>
		<td>Ilo��:</td>
		<td><ww:textfield name="'volume'" size="50" id="volume" cssClass="'txt'" value="volume"/></td>
	</tr>
	<tr>
	<td>Cena:</td>
		<td><ww:textfield name="'amount'" size="50" id="amount" cssClass="'txt'" value="amount"/></td>
	</tr>
	<tr>
		<td colspan="2">
		<ww:if test="id == null">
			<ds:event name="'doAdd'" value="'Dodaj'" onclick="'if (!validate()) return false;'" confirm="'Na pewno doda� pozycj� ?'"/>
		</ww:if>
		<ww:else>
			<ds:event name="'doUpdate'" value="'Zapisz'"  onclick="'validate();'" confirm="'Zapisa� zmiany ?'"/>
		</ww:else>
		</td>
	</tr>
</table>
</form>
<script language="JavaScript">	

function validate() 
{
	if(E('description').value == null || E('description').value.length < 1)
	{
		alert('Opis nie mo�e by� pusty');
		return false;
	}
	if(E('volume').value == null || E('volume').value.length < 1)
	{
		alert('Ilo�� nie mo�e by� pusta');
		return false;
	}
	if(E('amount').value == null || E('amount').value.length < 1)
	{
		alert('Cena nie mo�e by� pusta');
		return false;
	}
	if(E('productGroupId').value == null || E('productGroupId').value.length < 1)
	{
		alert('Musisz wybra� konto');
		return false;
	}
	return true;
}


<ww:if test="documentReassigned">
		if(submitStrona())
		{
		    var form = window.opener.document.forms[0];

		    var hid = window.opener.document.createElement('INPUT');
		    hid.type = 'HIDDEN';
		    hid.name = 'doArchive';
		    hid.value = 'true';
		    form.appendChild(hid);

		    for (var i=0; i < form.elements.length; i++)
		    {
		        var el = form.elements[i];
		        if (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button')
		        {
		            el.disabled = true;
		        }
		    }
		    selectAllOptions(window.opener.document.getElementById('dockind_POZYCJA_ZAMOWIENIA'));
		    form.submit();
			window.close();
		}
</ww:if>
        
     function submitStrona()
     {
         if (!window.opener)
         {
             alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
             return false;
         }          
         <ww:if test="submit">
		        var strona = new Array();
		        strona.id = E('id').value;
		        strona.description = E('description').value;
		        strona.volume = E('volume').value;
		        strona.amount = E('amount').value;
			
				if (window.opener.accept)
				{
		             window.opener.accept('<ww:property value="param"/>', strona);
				}
		        else
		         {
		             alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
		             return false;
		         }
		</ww:if>
         return true;
     }
</script>