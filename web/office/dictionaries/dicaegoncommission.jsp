<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="AgencjaPosrednik"/></h1><hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/dicaegoncommission.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'param'" value="param" id="param"/>


<ww:if test="results == null || results.empty">
		<table>
			<tr>
				<td><ds:lang text="numer_agenta"/></td>
			</tr>
			<tr>
                <td><ww:textfield name="'numer_agenta'" id="numer_agenta" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr>
				<td><ds:lang text="nazwa_posrednik_agencja"/></td>
			</tr>
			<tr>
                <td><ww:textfield name="'nazwa_posrednik_agencja'" id="nazwa_posrednik_agencja" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td><ds:lang text="nip"/></td>
			</tr>
			<tr>
                <td><ww:textfield name="'nip'" id="nip" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
		</table>
		<table>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                    
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                </td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%">
        <tr>
			<th><ds:lang text="numer_agenta"/></th>
            <th><ds:lang text="nazwa_posrednik_agencja"/></th>
            <th><ds:lang text="nip"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
        		<td><ww:property value="numer_agenta"/></td>
        	    <td><ww:property value="nazwa_posrednik_agencja"/></td>
                <td><ww:property value="nip" /></td>
        	    <td><a href="<ww:url value="'/office/common/dicaegoncommission.action'">
        	    				<ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/>
        	    				<ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
								<ww:param name="'search_numer_agenta'" value="search_numer_agenta"/>
								<ww:param name="'search_nip'" value="search_nip"/>
                				<ww:param name="'search_nazwa_posrednik_agencja'" value="search_nazwa_posrednik_agencja"/>								
								<ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                <td><a href="<ww:url value="'/office/common/dicaegoncommission.action'">
                	<ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/>
                	<ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Na pewno usun�� wpis <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                   </ww:if>
        	</tr>
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dicaegoncommission.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
</ww:else>



<script language="JavaScript">
		function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = 'disabled';
            document.getElementById('id').value = '';
            document.getElementById('numer_agenta').value = '';
            document.getElementById('nip').value = '';
            document.getElementById('nazwa_posrednik_agencja').value = '';            
            notifyChange();
        }
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('id').value;
            strona.numer_agenta = id('numer_agenta').value;
            strona.nazwa_posrednik_agencja = id('nazwa_posrednik_agencja').value;
            strona.nip = id('nip').value;
            
            
            if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>