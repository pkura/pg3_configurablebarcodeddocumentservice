<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<h1>Kontakt</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/office/common/crmContactDictionary.action'"/>" method="post">
		<ww:hidden name="'id'" value="id" id="id" />
		<ww:hidden name="'documentId'" value="documentId" id="documentId" />
        <ww:hidden name="'param'" value="param" id="param"/>
        <ww:hidden id="goToTasklist" name="'goToTasklist'"/>
<table>
	<tr>
		<td>Dodany przez :</td><td><ww:property value="createUser"/></td>
	</tr>
	<tr>
		<td>
			Rodzaj kontaktu :
		</td>
		<td>
			<ww:select name="'rodzajKontaktu'" id="rodzajKontaktu" list="rodzajeKontaktu" listKey="cn" headerKey="''" headerValue="'wybierz'"  
				listValue="name" cssClass="'sel'" value="rodzajKontaktu"/>
		</td>
	</tr>
	<tr>
		<td>
			Opis :
		</td>
		<td colspan="5">
			<ww:textarea name="'opis'" id="opis" cols="60" cssClass="'txt'" rows="5" ></ww:textarea>
		</td>
	</tr>
	<tr>
		<td>Data kontaktu :</td>
        <td><ww:textfield name="'dataKontaktu'" size="10" maxlength="10"
            cssClass="'txt'" id="dataKontaktu" />
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="dataKontaktuTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
	</tr>	
	<tr>
	
	<td><br/><br/><br/><br/><br/><br/>
	</td>
	</tr>
	<!--
	<tr>
		<td>
			Materia�y marketingowe : 
		</td>
		<td colspan="3">
		<table>
			<tr>
					<td class="alignTop">
						<ww:select name="'availableLeafle'" listKey="cn" listValue="name" multiple="true" size="5" 
						cssClass="'multi_sel'" cssStyle="'width: 150px;'" list="availableLeafle"/>
					</td>
					<td class="alignMiddle">
						<input type="button" value=" &gt;&gt; " name="moveRightButton"
							onclick="if (moveOptions(this.form.availableLeafle, this.form.selectedLeafle) > 0) dirty=true;" class="btn"/>
						<br/>
						<input type="button" value=" &lt;&lt; " name="moveLeftButton"
							onclick="if (moveOptions(this.form.selectedLeafle, this.form.availableLeafle) > 0) dirty=true;" class="btn"/>
					</td>
					<td class="alignTop">
						<ww:select name="'selectedLeafle'" id="selectedLeafle" multiple="true" size="5" listKey="cn" 
						listValue="name" cssClass="'multi_sel'" cssStyle="'width: 150px;'" list="taskLeafle"/>
					</td>
			</tr>
		</table>	
		</td>
	</tr>
	-->
	<tr bgcolor="#FF9900">
		<td bgcolor="#FF9900"><b>Data nast�pnego kontaktu:</b></td>
        <td bgcolor="#FF9900"  width="125px"><ww:textfield name="'dataNastepnegoKontaktu'" size="10" maxlength="10"
            cssClass="'txt'" id="dataNastepnegoKontaktu" />
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="dataNastepnegoKontaktuTrigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
         </td>
         <td>
         <b>Godzina nast�pnego kontaktu :</b></td><td><ww:textfield name="'godzina'" id="godzina" size="5"  cssClass="'txt'"/>
         </td>
         <td> 
			<b>Rodzaj nast�pnego kontaktu :</b>
			</td><td><ww:select name="'rodzajNastepnegoKontaktu'" id="rodzajNastepnegoKontaktu" list="rodzajeKontaktu" 
				listKey="cn" headerKey="''" headerValue="'wybierz'" listValue="name" cssClass="'sel'" value="rodzajNastepnegoKontaktu"/>
		</td>
	</tr>
	<tr>
		<td>
			Cel kontaktu : 
		</td>
		<td colspan="5">
			<ww:textarea name="'celKontaktu'" id="celKontaktu" cols="60" cssClass="'txt'" rows="5" ></ww:textarea>
		</td>
	</tr>	
	<tr>
		<td>
			Status kontaktu :
		</td>
		<td colspan="5">
			<ww:select name="'statusKontaktu'" id="statusKontaktu" list="statusykontaktu" listKey="id" headerKey="''" headerValue="'wybierz'"  
				listValue="name" cssClass="'sel'" value="statusKontaktu"/>
		</td>
	</tr>
	<tr>
		<ww:if test="id == null">
 			<td><input type="submit" id="doAdd" name="doAdd" value="Dodaj" onclick="if (!validate()) return false;" class="btn"/></td>
 			<td colspan="5"><input type="submit" id="doAdd" name="doAdd" value="Dodaj i przejd� na list� zada�" onclick="if (!validateGoToTasklist()) return false;" class="btn"/></td>
		</ww:if>
		<ww:else>
			<td><input type="submit" id="doUpdate" name="doUpdate" value="Zapisz" onclick="if (!validate()) return false;" class="btn"/></td>
			<td colspan="5"><input type="submit" id="doUpdate" name="doUpdate" value="Zapisz i przejd� na list� zada�" onclick="if (!validateGoToTasklist()) return false;" class="btn"/></td>
		</ww:else>		
	</tr>
</table>
<SCRIPT LANGUAGE="JavaScript">

	function validate()
	{
		if(document.getElementById('statusKontaktu').selectedIndex == null || document.getElementById('statusKontaktu').selectedIndex == 0)
		{
			alert('Status kontaktu jest polem obowi�zkowym.');
			return false;
		}
		if(document.getElementById('rodzajKontaktu').selectedIndex == null || document.getElementById('rodzajKontaktu').selectedIndex == 0)
		{
			alert('Rodzaj kontaktu jest polem obowi�zkowym.');
			return false;
		}
		if(document.getElementById('rodzajNastepnegoKontaktu').selectedIndex == null || document.getElementById('rodzajNastepnegoKontaktu').selectedIndex == 0)
		{
			alert('Rodzaj nast�pnego kontaktu jest polem obowi�zkowym.');
			return false;
		}
		if( (document.getElementById('dataNastepnegoKontaktu').value == '' || document.getElementById('godzina').value == '') 
				&& $j('#statusKontaktu').val() != 20 )
		{
			alert('Data i godzina nast�pnego kontaktu jest polem obowi�zkowym.');
			return false;
		}
		else if(!validateHour(document.getElementById('godzina')) && $j('#statusKontaktu').val() != 20)
		{	
			alert('Wpisano niepoprawn� godzin�');
			return false;
		}
		
		
		selectAllOptions(document.getElementById('selectedLeafle'));
		return true;
	}
	
	function validateHour(element)
	{
		var s = element.value;
	    if (isEmpty(s) || s.length > 5 || s.length == 3)
	        return false;
	
		s = s.trim();
		var re = new RegExp('[0-2][0-9]:[0-5][0-9]');
		if(s.length == 5)
		{
			if(s.charAt(0) == 2)
				re = new RegExp('[0-2][0-4]:[0-5][0-9]');
			else
				re = new RegExp('[0-1][0-9]:[0-5][0-9]');
		}
		if(s.length == 4)
		{
			re = new RegExp('[0-9]:[0-5][0-9]');
		}
		if(s.length == 2)
		{
			re = new RegExp('[0-2][0-9]');
		}
		if(s.length == 1)
		{
			re = new RegExp('[0-9]');
		}
		return element.value.match(re);
	}
	
	function validateGoToTasklist()
	{
		document.getElementById('goToTasklist').value='true';
		return validate();
	}

	function initDoc()
	{
		var el = window.opener.document.getElementById('documentId');
		if(el == null)
			el = window.opener.document.getElementById('id');
		document.getElementById('documentId').value = el.value;
	}
	initDoc();

	Calendar.setup({
	    inputField     :    "dataKontaktu",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "dataKontaktuTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
	
	Calendar.setup({
	    inputField     :    "dataNastepnegoKontaktu",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "dataNastepnegoKontaktuTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
	
<ww:if test="documentReassigned">
		if(submitStrona())
		{
			
		    var form = window.opener.document.forms[0];

		    var hid = window.opener.document.createElement('INPUT');
		    hid.type = 'HIDDEN';
		    <ww:if test="goToTasklist">
		    	hid.name = 'doArchiveGoToTasklist';
		    </ww:if>
		    <ww:else>
		    	hid.name = 'doArchive';
		    </ww:else>
		    hid.value = 'true';
		    form.appendChild(hid);
		    
		    var hid2 = window.opener.document.createElement('INPUT');
		    hid2.type = 'HIDDEN';
		    hid2.name = 'archiveWithoutLabel';
		    hid2.value = 'true';
		    form.appendChild(hid2);
			
		    window.opener.document.getElementById('returnToTasklist').value='true';
		    for (var i=0; i < form.elements.length; i++)
		    {
		        var el = form.elements[i];
		        if (el.type.toLowerCase() == 'submit' || el.type.toLowerCase() == 'button')
		        {
		            el.disabled = true;
		        }
		    }
		    selectAllOptions(window.opener.document.getElementById('dockind_KONTAKT'));
		    form.submit();

			window.close();
		}
</ww:if>
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }          

            var strona = new Array();
			var tmp = '';
			var selectedIndex = id('rodzajKontaktu').selectedIndex;
            strona.id = id('id').value;	
            if(id('rodzajKontaktu') != null && selectedIndex != null)
            {
                tmp += id('rodzajKontaktu').options[selectedIndex].text;
            }
            strona.dataKontaktu =id('dataKontaktu').value;
            strona.nazwaRodzajkontaktu = tmp;
            strona.dataNastepnegoKontaktuAsString =id('dataNastepnegoKontaktu').value;

			if (window.opener.accept)
			{
                window.opener.accept('<ww:property value="param"/>', strona);
			}
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
        
</script>
</form>