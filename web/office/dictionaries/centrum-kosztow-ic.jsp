<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N centrum-kosztow.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:if test="afterAction != null">
    <span id="descriptionSpan" style="display:none;"><ww:property value="description"/></span>
    <script type="text/javascript">    
    	function finishAction()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }
            <ww:if test="afterAction == 'add'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptAddCentrum');
                    return;
                }
                window.opener.acceptAddCentrum('<ww:property value="id"/>',$j('#descriptionSpan').val(),'<ww:property value="amount"/>','<ww:property value="simpleAcceptance"/>');
                window.opener.doSave();
            </ww:if>

            <ww:if test="afterAction == 'fileImport'">
                window.opener.doSave();
            </ww:if>

            <ww:if test="afterAction == 'LoadFile'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptAddCentrum');
                    return;
                }
                                
                <ww:if test="addedFromFile == null || addedFromFile.size() < 1">
                	alert("Nie dodano zadnych wartosci");
                	return;
                </ww:if>
                <ww:iterator value="addedFromFile">
                	window.opener.acceptAddCentrum('<ww:property value="ff_id"/>','<ww:property value="ff_description"/>','<ww:property value="ff_amount"/>'); 
                </ww:iterator>
                window.opener.doSave();
            </ww:if>
            
            <ww:elseif test="afterAction == 'delete'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptDeleteCentrum');
                    return;
                }
                window.opener.acceptDeleteCentrum('<ww:property value="param"/>');
                window.opener.doSave();
            </ww:elseif>
            <ww:elseif test="afterAction == 'update'">
                if (!window.opener.acceptAddCentrum)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> acceptUpdateCentrum');
                    return;
                }
                window.opener.acceptUpdateCentrum('<ww:property value="param"/>',$j('#descriptionSpan').val(),'<ww:property value="amount"/>');
                window.opener.doSave();
            </ww:elseif>
            
            window.close();
        }
        
        finishAction();
    </script>
</ww:if>
<ww:else>

    <p><h3><ww:if test="id == null">Dodawanie</ww:if><ww:else>Edycja</ww:else> centrum kosztowego</h3></p>

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>" method="post" enctype="multipart/form-data">

        <ww:hidden name="'param'" value="param"/>
        <ww:hidden name="'fileOnly'" value="fileOnly"/>
        <ww:hidden name="'id'" value="id" id="id"/>
        <ww:hidden name="'documentId'" value="documentId" id="documentId"/>
		<ww:hidden name="'acceptanceVal'" value="acceptanceVal" id="acceptanceVal" />
<ww:if test="!fileOnly">
        <table>
		<ww:if test="!centrumHidden">
            <tr>
                <td>Kod(nazwa) centrum</td>
            </tr>
            <tr>
                <td>
                    <ww:if test="id == null"><ww:select name="'centrumId'" value="centrumId" cssClass="'sel'" list="centra"/></ww:if>
                    <ww:else><b><ww:property value="code"/></b></ww:else>
                </td>
            </tr>
		</ww:if>
		<tr>
            <td><ds:lang text="NumerKonta"/></td>
        </tr>
        <tr>
            <td><ww:select name="'accountNumber'" id="accountNumber" value="accountNumber" list="accountNumbers" listKey="number" listValue="accountFullName" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')"/></td>
        </tr>
		<ww:if test="!centrumHidden">
		    <tbody id="remarkTbody">
            <tr>
                <td>Komentarz do pozycji</td>
            </tr>
            <tr>
                <td>
					<input type="hidden" id="remarkIdOld" value="<ww:property value="remarkId"/>"/>
				    <select id="remarkId" name="remarkId" class="sel"></select>
                    <%--<select name="remarkId" class="sel">
                      <option value="">-- Wybierz --</option>
                      <option value="1">Komentarz test 1</option>
                      <option value="2">Komentarz test 2</option>
                      <option value="3">Komentarz test 3</option>
                    </select>--%>
                </td>
            </tr>
            <tbody>
        </ww:if>
        <ww:if test="showDescriptionAmount">
            <tr>
                <td><ds:lang text="Opis"/></td>
            </tr>
            <tr>
                <td><ww:textarea  name="'descriptionAmount'" id="descriptionAmount" cols="40" rows="4" cssClass="'txt'"/></td>
            </tr>
        </ww:if>
        <ww:if test="fixedAssets">
			<tr>
				<td>Grupa:</td>
			</tr>
			<tr>
				<td>
					<ww:select
						name="'groupId'"
						id="groupId"
						value="groupId"
						list="groups"
						listKey="id"
						listValue="title"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						onchange="'this.form.submit()'"/>
				</td>
			</tr>
			<tr>
				<td>Podgrupa:</td>
			</tr>
			<tr>
				<td>
					<ww:select
						name="'subgroupId'"
						id="subgroupId"
						value="subgroupId"
						list="subgroups"
						listKey="id"
						listValue="title"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						/>
				</td>
			</tr>

			<tr>
				<td>Miejsce sk�adowania:</td>
			</tr>
			<tr>
				<td><ww:textarea  name="'storagePlace'" id="storagePlace" cols="40" rows="2" cssClass="'txt'"/></td>
			</tr>
		</ww:if>
        <ds:available test="centrum.localization">
        <ww:if test="localizationAvailable">
            <tr>
                <td><ds:lang text="Lokalizacja"/></td>
            </tr>
            <tr>
                 <td><ww:select name="'locationId'" id="locationId" value="locationId" list="locations" listKey="id" listValue="cn + ' - ' + title" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')"/></td>
            </tr>
        </ww:if></ds:available>
        <tr>
            <td><ds:lang text="KwotaCzastkowa"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'amount'" id="amount" size="50" maxlength="50" cssClass="'txt'" cssStyle="'text-align:right'" readonly="!canChangeAmount"/></td>
        </tr>
		<ww:if test="vatFieldsAvailable">
		    <tbody <ww:if test="hideVatFields">style="display:none;"</ww:if> >
            <tr>
                <td>VAT w koszty</td>
            </tr>
            <tr>
                <td>
                    <ww:select
						name="'vatTypeId'"
						id="vatTypeId"
						value="vatTypeId"
						list="vatTypes"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						/>
                </td>
            </tr>
            <tr>
                <td>Klasyfikacja podatkowa w koszty</td>
            </tr>
            <tr>
                <td>
                    <ww:select
						name="'classTypeId'"
						id="classTypeId"
						value="classTypeId"
						list="classTypes"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						/>
                </td>
            </tr>
            <tr>
                <td>Powi�zane/Niepowi�zane</td>
            </tr>
            <tr>
                <td>
                    <ww:select
						name="'connectedTypeId'"
						id="connectedTypeId"
						value="connectedTypeId"
						list="connectedTypes"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						/>
                </td>
            </tr>
            <tr style="display:none;">
                <td>Podatek VAT</td>
            </tr>
            <tr style="display:none;">
                <td>
                    <ww:select
						name="'vatTaxTypeId'"
						id="vatTaxTypeId"
						value="vatTaxTypeId"
						list="vatTaxTypes"
						headerKey="''"
						headerValue="getText('select.wybierz')"
						cssClass="'sel'"
						/>
                </td>
            </tr>
            <ww:if test="analyticsVisible">
                <tr>
                    <td>Dalsza analityka</td>
                </tr>
                <tr>
                    <td>
                        <ww:select
                            name="'analyticsId'"
                            id="analyticsId"
                            value="analyticsId"
                            list="analytics"
                            headerKey="''"
                            headerValue="getText('select.wybierz')"
                            cssClass="'sel'"
                            />
                    </td>
                </tr>
            </ww:if>
            </tbody>
		</ww:if>

		<ww:if test="sad">
			<tr>
				<td>Agencja celna:</td>
			</tr>
			<tr>
				<td>
					<ww:select
						name="'customsAgencyId'"
						id="customsAgencyId"
						value="customsAgencyId"
						list="agencies"
						listKey="id"
						listValue="title"
						cssClass="'sel'"
						/>
				</td>
			</tr>
		</ww:if>

        </table>

        <table>
            <tr>
                <td>
                    <ww:if test="id == null">
                        <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="Dodaj"/>" onclick="if (!confirmAdd()) return false;" class="btn" />
                    </ww:if>
                    <ww:else>
                         <input type="submit" id="doUpdate" name="doUpdate" value="<ds:lang text="Zapisz"/>" onclick="if (!confirmUpdate()) return false;" class="btn saveBtn" />
                         <input type="submit" id="doDelete" name="doDelete" value="<ds:lang text="Usun"/>" onclick="if (!confirmDelete()) return false;" class="btn" <ww:if test="accepted">disabled="disabled"</ww:if>/>
                    </ww:else>                            
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>                
            </tr>
        </table>
    </ww:if> <%-- fileOnly --%>
		<ww:if test="id == null">
		<p><h3>Import danych do faktury</h3></p>
		<table>
		<tr>
           	<td>Zaladuj centra i konta z pliku:</td>
        </tr>
        <tr>
           	<td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
        	<td>
        		<input type="submit" id="doLoadFile" name="doLoadFile" value="<ds:lang text="Zaladuj"/>" onclick="if (!confirmMultiAdd()) return false;" class="btn" />
        	</td>
        </tr>
	</table>
	</ww:if>
	</form>

    <script type="text/javascript">
    	var acceptanceV = window.opener.document.getElementById('dockind_AKCEPTACJA_STALA').value;
		document.getElementById('acceptanceVal').value = acceptanceV;
	
        <ww:if test="accountNumber != null">
        try {
            updateComments($j('#accountNumber').val());
        } catch (e) {}
        </ww:if>

        $j('#accountNumber').change(function(){
            updateComments($j('#accountNumber').val());
        });

        function updateComments(accountNumber){
           var oldId = $j('#remarkIdOld').val();
           var optionsHtml = '<option value="">-- wybierz --</option>';
           $j.getJSON('<ww:url value="'/rest/select/ic/comments'"/>?account-number='+accountNumber+'&remarkIdOld='+oldId, function(data){
               if(data.length > 0){
                   for (var i=0; i<data.length; i++) {
                       var option = data[i];
                       if(option.key == oldId){
                           optionsHtml += '<option value="' + option.key + '" selected="true">' + option.value + '</option>';
                       } else {
                           optionsHtml += '<option value="' + option.key + '">' + option.value + '</option>';
                       }
                   }
                   $j('#remarkTbody').show();
               } else {
                   $j('#remarkTbody').hide();
               }
               $j('#remarkId').html(optionsHtml);
           });
       }
    
    	function confirmMultiAdd()
    	{
    		if (!validate())
                return false;
            return confirm('<ds:lang text="NapewnoZaladowacDaneZPliku"/>?');
    	}
    
        function confirmAdd()
        {
            if (!validate())
                return false;
            return confirm('<ds:lang text="NaPewnoPrzypisacDoFakturyToCentrumKosztowe"/>?');
        }
        
        function confirmUpdate()
        {
            if (!validate())
                return false;
            return confirm('<ds:lang text="NaPewnoZapisacZmiany"/>?');
        }

        function confirmDelete()
        {
            return confirm('<ds:lang text="NaPewnoUsunacZfakturyToCentrum"/>?');
        }
        
        function isNeed()
    	{
    		var ids = new Array();
			
			<ww:iterator value="accountNumbers">
			<ww:set name="result"/>
				<ww:if test="#result.needdescription">
					ids[ids.length] = "<ww:property value="#result.number"/>";
				</ww:if>
			</ww:iterator>
			
			if(ids.length > 0)
			{
				for(i=0;i<ids.length;i++)
				{
					if(ids[i] == document.getElementById('accountNumber').options[document.getElementById('accountNumber').selectedIndex].value)
					{
						return true;
					}					
				}
			}
			return false;			
    	}
        
        function validate()
        {
			var maxAmount = <ww:property value="maxAmount"/>;
            var val = document.getElementById('amount').value;

            if($j("#subgroupId").size() > 0 && $j("#subgroupId").val() === ""){
                alert("Brak wybranej podgrupy");
                return false;
            }

			if(val.lastIndexOf >= 0 && (val.length - val.lastIndexOf('.') ) > 3) {
				alert('<ds:lang text="ZaDuzoCyfrPoPrzecinku"/>');
				return false;
			}

            <ww:if test="!canChangeAmountWithoutLimit">
            if ((Math.abs(val) > Math.abs(maxAmount)) || ((maxAmount > 0) != (val > 0))) /* maxAmount i val nie maj� takiego samego znaku */
            {
                alert('<ds:lang text="KwotaCzastkowaNieMozeBycWiekszaNiz"/> <ww:property value="maxAmount"/>!');
                return false;
            }
            </ww:if>

            if(isNeed())
            {
            	if(document.getElementById('descriptionAmount').value == null || document.getElementById('descriptionAmount').value == "")
            	{
            		alert('Wymagane pole opis'+document.getElementById('descriptionAmount').value);
            		return false;
            	}
            }    
            return true;
        }
    </script>
</ww:else>
