<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N agent.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:if test="closeOnReload">
    <script>window.close();</script>
</ww:if>
<ww:else>
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/agent.action'"/>" method="post">

        <input type="hidden" name="closeOnReload" id="closeOnReload"/>
        <ww:hidden name="'param'" value="param"/>
        <ww:hidden name="'dictionaryType'" value="dictionaryType"/>
        <ww:hidden name="'canAddAndSubmit'" value="canAddAndSubmit"/>
        <ww:hidden name="'id'" value="id" id="id"/>
        <ww:hidden name="'canEdit'" value="canEdit" id="canEdit"/>
        <ww:hidden name="'canDelete'" value="canDelete" id="canDelete"/>

    <ww:if test="results == null || results.empty">

        <table>
        <tr>
            <td><ds:lang text="Imie"/></td><td><ds:lang text="Nazwisko"/></td><td>Komplet dokumentów</td>
        </tr>
        <tr>
            <td><ww:textfield name="'imie'" id="imie" size="20" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            <td><ww:textfield name="'nazwisko'" id="nazwisko" size="25" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
            <td><ww:checkbox name="'setOfDocuments'" id="setOfDocuments" fieldValue="true" value="setOfDocuments" disabled="id != null" /></td>
        </tr>
        </table>
        <table>
        <tr>
            <td colspan="3"><ds:lang text="NumerSpecjalistyOWCA"/></td>
        </tr>
        <tr>
            <td colspan="3"><ww:textfield name="'numer'" id="numer" size="50" maxlength="50" cssClass="'txt'" onchange="'notifyChange();'"/></td>
        </tr>
        <tr>
            <td colspan="3"><ds:lang text="BiuroRegionalneAgencjaBank"/></td>
        </tr>
        <tr>
            <td colspan="3">
            <ww:textfield name="'agencja_nazwa'" id="agencja_nazwa" size="50" maxlength="50" cssClass="'txt'" readonly="true"/>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="button" value="<ds:lang text="WybierzBiuroAgencjeBank"/>" onclick="openAgencjaPopup();" class="btn" >
                <input type="hidden" id="agencja_id" name="agencja_id" value="<ww:property value="agencja_id" />"/>
                <input type="hidden" id="agencja_numer" name="agencja_numer" value="<ww:property value="agencja_numer" />"/>
                <input type="hidden" id="agencja_nip" name="agencja_nip" value="<ww:property value="agencja_nip" />"/>
                <input type="hidden" id="rodzaj_sieci" name="rodzaj_sieci" value="<ww:property value="rodzaj_sieci" />"/>
            </td>
        </tr>
        </table>


        <table>
            <tr>
                <td>

                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>

                    <ww:if test="id == null">
                        <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAddOrUpdate()) return false;" class="btn" <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
                    </ww:if>
                    <ww:else>
                         <input type="submit" id="doUpdate" name="doUpdate" value="<ds:lang text="ZapiszZmianyWslowniku"/>" onclick="if (!confirmAddOrUpdate()) return false;" class="btn" <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
                    </ww:else>

                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" >
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>

            <tr>
                <td>
                    <input type="button" id="doSubmit" value="          <ds:lang text="UmiescWformularzu"/>          " onclick="if (!submitAgent()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if><ww:else></ww:else>/>
                    <ww:if test="afterSearch">
                        <input type="button" value="<ds:lang text="PowrocDoWynikow"/>" onclick="returnToResults();" class="btn"/>
                     </ww:if>
                </td>
            </tr>


        </table>



        <script language="JavaScript">
        function submitAgent()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

           /* if (!window.opener.__accept_agent)
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/>');
                return;
            } */

            var agent = new Array();

            agent.id = id('id').value;
            agent.imie = id('imie').value;
            agent.nazwisko = id('nazwisko').value;
            agent.numer = id('numer').value;
            agent.setOfDocuments = id('setOfDocuments').checked;
            agent.agencja_id = id('agencja_id').value;
            agent.agencja_nazwa = id('agencja_nazwa').value;
            agent.agencja_numer = id('agencja_numer').value;
            agent.agencja_nip = id('agencja_nip').value;
            agent.rodzaj_sieci = id('rodzaj_sieci').value;

            if (isEmpty(agent.imie) || isEmpty(agent.nazwisko))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }

            if (window.opener.__accept_agent)
                window.opener.__accept_agent(agent);
            else if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', agent);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> __accept_agent');
                return;
            }
            return true;
        }

        function id(id)
        {
            return document.getElementById(id);
        }

        function openAgencjaPopup()
        {
            openToolWindowX('<ww:url value="'/office/common/agencja.action'"/>'+
                '?nazwa='+document.getElementById('agencja_nazwa').value+
                '&id='+document.getElementById('agencja_id').value+
                '&canAddAndSubmit=true&forEdit=false', 'Agencja', 950, 650);
        }

        function __check_accept_agencja(map)
        {
              if (parseInt(document.getElementById('agencja_id').value) == parseInt(map.id))
              __accept_agencja(map);
        }
        function __accept_agencja(map, lparam, wparam)
        {
               //alert(map.id+" "+map.nazwa+" "+map.rodzaj_sieci);
               document.getElementById('agencja_id').value = map.id;
               document.getElementById('agencja_nazwa').value = map.nazwa;
               document.getElementById('agencja_numer').value = map.numer;
               document.getElementById('agencja_nip').value = map.nip;
               document.getElementById('rodzaj_sieci').value = map.rodzaj_sieci;

               notifyChange();
        }

        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = true;
        }

        function clearForm()
        {
            var id = document.getElementById('id').value;
            if ((id != null) && (id != '')) {
                var button = document.getElementById('doUpdate');
                button.setAttribute('id', 'doAdd');
                button.setAttribute('name', 'doAdd');
                button.setAttribute('value', '<ds:lang text="DodajDoSlownika"/>');
            }

            document.getElementById('id').value = '';
            document.getElementById('numer').value = '';
            document.getElementById('nazwisko').value = '';
            document.getElementById('imie').value = '';
            document.getElementById('agencja_id').value = '';
            document.getElementById('agencja_numer').value = '';
            document.getElementById('agencja_nazwa').value = '';
            document.getElementById('agencja_nip').value = '';
            document.getElementById('rodzaj_sieci').value = '';

			
			var setOfDocs = document.getElementById('setOfDocuments');
			setOfDocs.disabled = false;
			setOfDocs.checked = false;
			setOfDocs.nextSibling.style.backgroundPosition = "0 0";
			setOfDocs.nextSibling.className = "checkbox";
			setOfDocs.nextSibling.onclick = CheckBox.check;

            notifyChange();
        }

        function confirmAddOrUpdate()
        {
            if (document.getElementById("doAdd") != null)
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
            else
                return confirm('<ds:lang text="NaPewnoZapisacZmianyWslowniku"/>?');
        }

        function returnToResults()
        {
              document.location.href = '<ww:url value="'/office/common/agent.action'"/>'+
                '?doSearch=true'+
                '&nazwisko=<ww:property value="searchNazwisko"/>'+
                '&numer=<ww:property value="searchNumer"/>'+
                '&imie=<ww:property value="searchImie"/>'+
                '&agencja_id=<ww:property value="searchAgencja_id"/>';
        }


        // aktualizacja formularza z okna wywolujacego
        <ww:if test="id != null">
            if ((window.opener) && ((window.opener.__check_accept_agent) || (window.opener.checkAccept)))
            {
                var agent = new Array();

                agent.id = id('id').value;
                agent.imie = id('imie').value;
                agent.nazwisko = id('nazwisko').value;
                agent.numer = id('numer').value;
                agent.agencja_id = id('agencja_id').value;
                agent.agencja_nazwa = id('agencja_nazwa').value;
                agent.agencja_numer = id('agencja_numer').value;
                agent.agencja_nip = id('agencja_nip').value;
                agent.rodzaj_sieci = id('rodzaj_sieci').value;

                if (window.opener.__check_accept_agent)
                    window.opener.__check_accept_agent(agent);
                else if (window.opener.checkAccept)
                    window.opener.checkAccept('<ww:property value="param"/>', agent);
            }
        </ww:if>
        </script>
    </ww:if>

    <!-- wyniki wyszukiwania -->

    <ww:else>
        <table width="100%">
            <tr>
                <th><ds:lang text="ImieInazwisko"/></th>
                <th><ds:lang text="BiuroRegionalneAgencjaBank"/></th>
                <th><ds:lang text="NumerSpecjalistyOWCA"/></th>
                <th></th>
                <ww:if test="canDelete"><th></th></ww:if>
            </tr>
            <ww:iterator value="results">
                <tr>
                    <td><ww:property value="imie"/> <ww:property value="nazwisko"/></td>
                    <td><ww:property value="agencja.nazwa"/></td>
                    <td style="text-align:center"><ww:property value="numer"/></td>
                    <td><a href="<ww:url value="'/office/common/agent.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
                                 <ww:param name="'searchNazwisko'" value="searchNazwisko"/><ww:param name="'searchNumer'" value="searchNumer"/><ww:param name="'searchImie'" value="searchImie"/><ww:param name="'searchAgencja_id'" value="searchAgencja_id"/><ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                        <td><a href="<ww:url value="'/office/common/agent.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'lparam'" value="[1].lparam"/><ww:param name="'wparam'" value="[1].wparam"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>" onclick="if (!confirm('<ds:lang text="NaPewnoUsunacWpis"/> <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
                </tr>
            </ww:iterator>
        </table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/agent.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
    </ww:else>

    </form>

    <script>
    var s = document.location.search;
    if (s != null && s.length > 0) s = s.substring(1);

    var ognlMap = null;
    var params = s.split('&');
    for (var i=0; i < params.length; i++)
    {
        var pair = params[i].split('=');
        var name = pair[0];
        var value = pair[1];
        if (name == 'ognlMap')
        {
            ognlMap = unescape(value);
            break;
        }
    }

    if (ognlMap != null)
    {
        eval('var map='+ognlMap);

        document.getElementById('id').value = map.id != null ? map.id : '';
        document.getElementById('imie').value = map.imie != null ? map.imie : '';
        document.getElementById('nazwisko').value = map.nazwisko != null ? map.nazwisko: '';
        document.getElementById('agencja').value = map.agencja != null ? map.agencja : '';
        document.getElementById('numer').value = map.numer != null ? map.numer: '';

    }

    window.focus();



    CheckBox = {
    		check: function() {
    			element = this.previousSibling;
    			if(element.checked == true) {
    				this.style.backgroundPosition = "0 0";
    				element.checked = false;
    			} else {
    				this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
    				element.checked = true;
    			}
    		}
    	}


    </script>
</ww:else>