<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dicinvoice.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Kontrahenci"/></h1><hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/dicinvoice.action'"/>" method="post">
        <ww:hidden name="'kraj'" value="kraj" id="kraj"/>
		<ww:hidden name="'id'" value="id" id="id" />
        <ww:hidden name="'param'" value="param" id="param"/>


<ww:if test="results == null || results.empty">
		<table class="popupForm">
			<tr class="desc">
				<td colspan="2"><ds:lang text="NumerKontrahenta"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'numerKontrahenta'" id="numerKontrahenta" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <ds:available test="dicinvoice.NumerKontaBankowego">
	            <tr class="desc">
					<td colspan="2"><ds:lang text="NumerKontaBankowego"/></td>
				</tr>
				<tr>
	                <td colspan="2"><ww:textfield name="'numerKontaBankowego'" id="numerKontaBankowego" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	            </tr>
            </ds:available>
			<tr class="desc">
				<td colspan="2"><ds:lang text="DicInvoice_Nazwa"/><span class="star">*</span></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'name'" id="name" size="50" maxlength="200" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr class="desc">
				<td colspan="2"><ds:lang text="NIP"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nip'" id="nip" size="50" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<ds:available test="DicInvoice.show.telefon">
	            <tr>
					<td colspan="2"><ds:lang text="Telefon"/></td>
				</tr>
				<tr>
	                <td colspan="2"><ww:textfield name="'telefon'" id="telefon" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	            </tr>
			</ds:available>
			<ds:available test="dicinvoice.regon">
	            <tr class="desc">
					<td colspan="2"><ds:lang text="Regon"/></td>
				</tr>
				<tr>
	                <td colspan="2"><ww:textfield name="'regon'" id="regon" size="30" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	            </tr>
            </ds:available>
            <%--
            <tr>
				<td colspan="2"><ds:lang text="Imie"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'imie'" id="imie" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
			<tr>
				<td colspan="2"><ds:lang text="Nazwisko"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'nazwisko'" id="nazwisko" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
             --%>
			<tr class="desc">
				<td colspan="2"><ds:lang text="Ulica"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'ulica'" id="ulica" size="50" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            </tr>
            <tr class="desc">
                <td>
                	<ds:lang text="Kod"/>
               	</td>
               	<td>
               		<ds:lang text="Miejscowosc"/>
               	</td>
            </tr>
            <tr>
                <td>
                	<ww:textfield name="'kod'" id="kod" size="14" maxlength="14" onchange="'notifyChange();'" cssClass="'txt'"/>
                </td>
                <td>
                	<ww:textfield name="'miejscowosc'" id="miejscowosc" size="34" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><span class="warning" id="zipError"> </span></td>
            </tr>
            <ds:available test="dicinvoice.numerDomu">
	            <tr class="desc">
					<td colspan="2"><ds:lang text="NumerDomu"/></td>
				</tr>
				<tr>
	                <td colspan="2"><ww:textfield name="'numerDomu'" id="numerDomu" size="50" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	            </tr>     
            </ds:available>
            <ds:available test="dicinvoice.kraj">
	           	 <tr class="desc">
					<td colspan="2"><ds:lang text="Kraj"/>:</td>
				</tr>
				<tr>
				    <td colspan="2">
				    	<ww:select name="'senderCountry'" headerKey="''" headerValue="'-- wszystkie --'"
					        list="@pl.compan.docusafe.util.Countries@COUNTRIES"
					        listKey="alpha2" listValue="name" value="senderCountry"
					        id="senderCountry" cssClass="'sel'" onchange="'this.form.kod.validator.revalidate();'">
					    </ww:select>
	 	            </td>
	 	        </tr>
 	        </ds:available>
 	        <ds:available test="dicinvoice.maxKwota">
	 	        <tr class="desc">
					<td colspan="2"><ds:lang text="Maksymalna kwota zam�wienia bez umowy"/></td>
				</tr>
				<tr>
	                <td colspan="2"><ww:textfield name="'maxKwota'" id="maxKwota" size="30" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	            </tr>
 	        </ds:available>
		</table>
		
		
		<table class="buttons">
            <tr>
                <td width="15%">
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                </td>
                <td width="35%">
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                </td>
                <td width="30%">
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                </td>
                <td width="20%">
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>
            <tr>
                <td colspan="2" width="50%">
                     <input type="button" id="doSubmit" value="     <ds:lang text="UmiescWformularzu"/>     " onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                </td>
                <td colspan="2" width="50%">
                     <input type="submit" name="doUpdate" id="doUpdate" value="      <ds:lang text="ZapiszZmiany"/>      "  class="btn" <ww:if test="!canModify || id==null">disabled="disabled"</ww:if>/>
                </td>
            </tr>
        </table>        
    </form>
</ww:if>
<ww:else>
    <table width="100%" id="mainTable" class="search" cellspacing="0" cellpadding="0">
        <tr>
        	<th><ds:lang text="NumerKontrahenta"/></th>
            <th><ds:lang text="NumerKontaBankowego"/></th>
            <th><ds:lang text="Nazwa"/></th>
            <th><ds:lang text="NIP"/></th>
            <th><ds:lang text="Regon"/></th>
            <th><ds:lang text="Kod"/></th>
            <th><ds:lang text="Miejscowosc"/></th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
    	</tr>
		<ww:iterator value="results" status="stat">
        	<tr <ww:if test="#stat.odd != true">class="oddRows"</ww:if>>
        		<td><ww:property value="numerKontrahenta" /></td>
        	    <td><ww:property value="numerKontaBankowego" /></td>
                <td><ww:property value="name" /></td>
        	    <td><ww:property value="nip" /></td>
        	    <%--
        	        <td><ww:property value="oldname" /></td>
                    <td><ww:property value="imie" /></td>
                    <td><ww:property value="nazwisko" /></td>
                --%>
                    <td><ww:property value="regon" /></td>
                    <td><ww:property value="kod" /></td>
                    <td><ww:property value="miejscowosc" /></td>
                    <td><a href="<ww:url value="'/office/common/dicinvoice.action'"><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/>
                                 <ww:param name="'searchNumerKontrahenta'" value="searchNumerKontrahenta"/><ww:param name="'searchNumerKontaBankowego'" value="searchNumerKontaBankowego"/>
                                 <ww:param name="'searchNazwa'" value="searchNazwa"/><ww:param name="'searchNumer'" value="searchNumer"/><ww:param name="'searchNip'" value="searchNip"/><ww:param name="'searchKod'" value="searchKod"/><ww:param name="'searchUlica'" value="searchUlica"/>
                                 <ww:param name="'searchRegon'" value="searchRegon"/><ww:param name="'param'" value="param"/>
                                 <ww:param name="'searchMiejscowosc'" value="searchMiejscowosc"/><ww:param name="'searchTelefon'" value="searchTelefon"/><ww:param name="'searchEmail'" value="searchEmail"/><ww:param name="'afterSearch'" value="afterSearch"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                	<td><a href="<ww:url value="'/office/common/dicinvoice.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm('Na pewno usun�� wpis <ww:property value="shortSummary"/>?')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	
    	</ww:iterator>
	</table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

		<center>
        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/dicinvoice.action'"><ww:param name="'param'" value="param"/></ww:url>';"/>
        </center>
</ww:else>



<script language="JavaScript">
		<%--document.getElementById('senderCountry').value = 'PL';--%>

	 	window.resizeTo(950,800);
	 	
	    var szv = new Validator(document.getElementById('kod'), '##-###');

        szv.onOK = function() 
        {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';
        }

        szv.onEmpty = szv.onOK;

        szv.onError = function(code) 
        {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001';
        }

        szv.canValidate = function() 
        {
            return true;
        }
        
        function confirmAdd()
        {
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }
        
        function notifyChange()
        {
            document.getElementById('doSubmit').disabled = 'disabled';
        }
        
        function clearForm()
        {         
        	document.getElementById('doUpdate').disabled = 'disabled';
			document.getElementById('doAdd').disabled = '';
            <%--document.getElementById('id').value = '';--%>
			$j('#id').remove();
            document.getElementById('numerKontrahenta').value = '';
            <ds:available test="dicinvoice.NumerKontaBankowego">
            document.getElementById('numerKontaBankowego').value = '';
             </ds:available>
            //document.getElementById('imie').value = '';
            //document.getElementById('nazwisko').value = '';
            document.getElementById('name').value = '';
			//document.getElementById('oldname').value = '';
			<ds:available test="dicinvoice.regon">
				document.getElementById('regon').value = '';
			</ds:available>
			document.getElementById('nip').value = '';
            document.getElementById('ulica').value = '';
            document.getElementById('kod').value = '';
            document.getElementById('miejscowosc').value = '';
            //document.getElementById('email').value = '';
            //document.getElementById('faks').value = '';
            <ds:available test="DicInvoice.show.telefon">
            	document.getElementById('telefon').value = '';
            </ds:available>
            <ds:available test="dicinvoice.numerDomu">
            	document.getElementById('numerDomu').value = '';
            </ds:available>
            <ds:available test="dicinvoice.kraj">
            	document.getElementById('senderCountry').value = '';
            </ds:available>
            notifyChange();
        }
        
        function rewriteNumber()
		{
			if(!window.opener.document.URL.match('search-dockind-documents') && window.opener.document.getElementById('dockind_VAT') == null)
			{
				var numerKonta = document.getElementById('numerKontaBankowego').value;
				window.opener.document.getElementById('dockind_NUMER_RACHUNKU_BANKOWEGO').value = numerKonta;	
			}
		}
        
        function id(id)
        {
            return document.getElementById(id);
        }
        
        function submitStrona()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            /*if (!window.opener.__accept_agencja)
            {
                alert('W oknie wywo�uj�cym nie znaleziono funkcji __accept_agencja');
                return;
            } */
            try
            {
				rewriteNumber();
            }
        	catch(err){}
            var strona = new Array();

            strona.id = id('id').value;
            strona.numerKontrahenta = id('numerKontrahenta').value;
            <ds:available test="dicinvoice.NumerKontaBankowego">
            	strona.numerKontaBankowego = id('numerKontaBankowego').value;
             </ds:available>
            //strona.imie = id('imie').value;
            //strona.nazwisko = id('nazwisko').value;
            strona.name = id('name').value;
			//strona.oldname = id('oldname').value;
			<ds:available test="dicinvoice.regon">
				strona.regon = id('regon').value;
			</ds:available>
			strona.prettyNip = id('nip').value;
            strona.ulica = id('ulica').value;
            strona.kod = id('kod').value;
            strona.miejscowosc = id('miejscowosc').value;
            <ds:available test="DicInvoice.show.telefon">
            	strona.telefon = id('telefon').value;
            </ds:available>
            //strona.faks = id('faks').value;
            //strona.email = id('email').value;
            <ds:available test="dicinvoice.kraj">
				strona.kraj = id('kraj').value;			
			</ds:available>
			<ds:available test="dicinvoice.numerDomu">
            	strona.telefon = id('numerDomu').value;
            </ds:available>


            if (isEmpty(strona.name)||isEmpty(strona.numerKontrahenta))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }
            
            //if (isNaN(parseInt(id('nip').value)) || isNaN(parseInt(id('regon').value))) {
        	//	alert('NIP i Regon musza byc numerami');
            //	return false; 
        	//}
            
           /* if (window.opener.__accept_inst)
			{
				window.opener.__accept_inst(strona);
			}
			else if (window.opener.accept_INST) 
			{
				window.opener.accept_INST(strona);
			}
            else*/ if (window.opener.accept)
                window.opener.accept('<ww:property value="param"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;
        }
</script>