<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<form id="customsAgencyForm" action="/office/dictionaries/customs-agency.action" method="post">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
	<script type="text/javascript">function submitCA(){
		$j("#ajaxContent").hide("slow");
		$j.post('<ww:url value="'/office/dictionaries/create-customs-agency.action'"/>',
		    { doCreate:"true",
		      entryName:$j("#agencyName").attr("value")},
		      function(data) {
			     $j("#ajaxContent").html(data).show("slow");
		});
	}</script>
	Dodaj nowy wpis
	<input class="txt" id="agencyName" value="<ww:property value="entryName"/>"/>
	<input class="btn" type="button" value="Zapisz" onclick="try{submitCA();} finally {return false};"/>
</form>