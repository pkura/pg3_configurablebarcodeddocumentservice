<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<table class="tab">
	<tr><ww:iterator value="headers"><th><ww:property/></th></ww:iterator><th></th></tr>
    <ww:iterator value="list"><tr id="rowId<ww:property value="id"/>">
        <ww:iterator value="objects"><td><ww:property/></td></ww:iterator>
        <td><a href="javascript:editRow(<ww:property value="id"/>,'<ww:property value="dictionaryCn"/>')">Zmie�</a></td>
    </tr></ww:iterator>
    <tr id="rowId"><%-- dla nowych wpis�w --%>
        <td colspan="<ww:property value="columnsSize"/>">
	    	<a href="javascript:editRow('','<ww:property value="dictionaryCn"/>')"><img alt="Dodaj nowy wpis" src="<ww:url value="img/add_icon.gif"/>"/></a>
		</td>
	</tr>
</table>
