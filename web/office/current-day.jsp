<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.DSPermission"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="BiezacyDzien"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

	<ww:if test="NEW_METHOD_SET_OPEN_DAY">
		<% String actualDate = DateUtils.formatJsDate(new java.util.Date()); %>
		<table>
			<tr>
				<th><ds:lang text="Dziennik"/></th>
				<th><ds:lang text="Rodzaj"/></th>
				<th><ds:lang text="OtwartyDzien"/></th>
				<th><ds:lang text="NowyDzien"/></th>
			</tr>
			<ww:if test="mainIncoming != null">
				<tr>
					<td><ds:lang text="GlownyDziennikPismPrzychodzacych"/></td>
					<td><ww:property value="mainIncoming.typeDescription"/></td>
					<td><ww:property value="mainIncoming.day"/></td>
					<td>
						<form action="<ww:url value="'/office/current-day.action?doSetOpenDayNewMethod=true'"/>" method="post">
		               		<input name="day" id="day<ww:property value="mainIncoming.id"/>" class="txt" type="text" size="10" style="width: 100px;" value="<%= actualDate %>">
							<img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger<ww:property value="mainIncoming.id"/>" style="cursor: pointer; border: 1px solid red; display: true;"
							title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
							<input type="hidden" name="id" value="<ww:property value="mainIncoming.id"/>"/>
		               		<ds:submit-event value="getText('OtworzNowyDzien')" name="'doSetOpenDayNewMethod'"/>
						</form>							
					</td>
				</tr>
				<script type="text/javascript">
					Calendar.setup({
						inputField	:	"day<ww:property value="mainIncoming.id"/>",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"dayTrigger<ww:property value="mainIncoming.id"/>",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
				</script>
			</ww:if>
			<ww:if test="mainOutgoing != null">
				<tr>
					<td><ds:lang text="GlownyDziennikPismWychodzacych"/></td>
					<td><ww:property value="mainOutgoing.typeDescription"/></td>
					<td><ww:property value="mainOutgoing.day"/></td>
					<td>
						<form action="<ww:url value="'/office/current-day.action?doSetOpenDayNewMethod=true'"/>" method="post">
		               		<input name="day" id="day<ww:property value="mainOutgoing.id"/>" class="txt" type="text" size="10" style="width: 100px;" value="<%= actualDate %>">
							<img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger<ww:property value="mainOutgoing.id"/>" style="cursor: pointer; border: 1px solid red; display: true;"
							title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
							<input type="hidden" name="id" value="<ww:property value="mainOutgoing.id"/>"/>
		               		<ds:submit-event value="getText('OtworzNowyDzien')" name="'doSetOpenDayNewMethod'"/>
						</form>
					</td>
				</tr>
				<script type="text/javascript">
					Calendar.setup({
						inputField	:	"day<ww:property value="mainOutgoing.id"/>",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"dayTrigger<ww:property value="mainOutgoing.id"/>",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
				</script>
			</ww:if>
			<ww:if test="mainInternal != null">
				<tr>
					<td><ds:lang text="GlownyDziennikPismWewnetrznych"/></td>
					<td><ww:property value="mainInternal.typeDescription"/></td>
					<td><ww:property value="mainInternal.day"/></td>
					<td>
						<form action="<ww:url value="'/office/current-day.action?doSetOpenDayNewMethod=true'"/>" method="post">
		               		<input name="day" id="day<ww:property value="mainInternal.id"/>" class="txt" type="text" size="10" style="width: 100px;" value="<%= actualDate %>">
							<img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger<ww:property value="mainInternal.id"/>" style="cursor: pointer; border: 1px solid red; display: true;"
							title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
							<input type="hidden" name="id" value="<ww:property value="mainInternal.id"/>"/>
							<ds:submit-event value="getText('OtworzNowyDzien')" name="'doSetOpenDayNewMethod'"/>
						</form>
					</td>
				</tr>
				<script type="text/javascript">
					Calendar.setup({
						inputField	:	"day<ww:property value="mainInternal.id"/>",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"dayTrigger<ww:property value="mainInternal.id"/>",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
				</script>
			</ww:if>
			<ww:if test="journals != null && !journals.isEmpty()">
				<ww:iterator value="journals">
					<tr>
						<ww:if test="incoming">
							<td>
								<ww:property value="description"/>
								<ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
							</td>
						</ww:if>
						<ww:elseif test="outgoing">
			            	<td>
			                    <ww:property value="description"/>
			                    <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
			                </td>
			            </ww:elseif>
		               	<ww:elseif test="internal">
		                	<td>
		                        <ww:property value="description"/>
		                        <ww:if test="symbol != null">(<ww:property value="symbol"/>)</ww:if>
		                   </td>
		               	</ww:elseif>
		               	<td><ww:property value="typeDescription"/></td>
		               	<td><ww:property value="day"/></td>
		               	<td>
		               		<form action="<ww:url value="'/office/current-day.action?doSetOpenDayNewMethod=true'"/>" method="post">
			               		<input name="day" id="day<ww:property value="id"/>" class="txt" type="text" size="10" style="width: 100px;" value="<%= actualDate %>">
								<img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger<ww:property value="id"/>" style="cursor: pointer; border: 1px solid red; display: true;"
								title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
								<input type="hidden" name="id" value="<ww:property value="id"/>"/>
								<ds:submit-event value="getText('OtworzNowyDzien')" name="'doSetOpenDayNewMethod'"/>
							</form>
						</td>
					</tr>
					<script type="text/javascript">
						Calendar.setup({
							inputField	:	"day<ww:property value="id"/>",	 // id of the input field
							ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
							button		:	"dayTrigger<ww:property value="id"/>",  // trigger for the calendar (button ID)
							align		:	"Tl",		   // alignment (defaults to "Bl")
							singleClick	:	true
						});
					</script>
				</ww:iterator>
			</ww:if>
		</table>
	</ww:if>
	<ww:else>
		<form action="<ww:url value="'/office/current-day.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
			<p>
			    <ww:iterator value="journals">
		            <ww:property value="description" />
		        </ww:iterator>
		        </br>
		        <ds:lang text="OtwartyDzien"/>:
		        <ww:if test="currentDay == null">
		            <ds:lang text="NieOtwartoDnia"/>
		        </ww:if>
		        <ww:else>
		            <ww:property value="currentDay"/>
		        </ww:else>
			</p>
				
			<ds:submit-event value="getText('OtworzNastepnyDzien')" name="'doCloseDay'" confirm="getText('NaPewnoZamknacDzien')" disabled="currentDay == null || !canCloseDay"/>
			
			<ds:has-permission name="<%= DSPermission.DZIEN_DOWOLNY_OTWARCIE.getName() %>">
				<hr class="shortLine"/>
				
				<p class="italic">
					<ds:lang text="currentDay.mozliweJest..."/>.
				</p>
				
				<p>
					<ds:lang text="InnyDzien"/>:
					<ww:textfield name="'day'" size="10" cssClass="'txt'" id="day"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
				</p>
		
				<ds:submit-event value="getText('OtworzTenDzien')" name="'doSetCurrent'" disabled="!canSetAnyDay" />
				
				<script type="text/javascript">
					Calendar.setup({
						inputField	:	"day",	 // id of the input field
						ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
						button		:	"dayTrigger",  // trigger for the calendar (button ID)
						align		:	"Tl",		   // alignment (defaults to "Bl")
						singleClick	:	true
					});
				</script>
			</ds:has-permission>
		</form>
	</ww:else>