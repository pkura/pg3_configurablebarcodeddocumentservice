<%@ page import="pl.compan.docusafe.util.DateUtils,pl.compan.docusafe.core.office.DSPermission" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script type="text/javascript" src="<ww:url value="'/jquery.chained.js'" />"></script>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestr niezgodno�ci</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/office/reclamation-list.action'"></ww:url>" method="post" >
<table class="mediumTable search userBilling" >
	<tr>
		<td>Status niezgodno�ci:
					<ww:select name="'statusNiezgodnosci'" id="statusNiezgodnosci" list="statusyNiezgodnosci" listKey="key" listValue="value"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		<td>Status realizacji:
					<ww:select name="'statusRealizacji'" id="statusRealizacji" list="statusyRealizacji" listKey="key" listValue="value"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		<td>Data wykrycia od:
		<ww:textfield name="'dataWykryciaOd'" id="dataWykryciaOd" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dataWykryciaOdTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
				<script type="text/javascript">
					if($j('#dataWykryciaOd').length > 0)
					{
						Calendar.setup({
						    inputField  :    "dataWykryciaOd",     // id of the input field
						    ifFormat    :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
						    button      :    "dataWykryciaOdTrigger",  // trigger for the calendar (button ID)
						    align       :    "Tl",           // alignment (defaults to "Bl")
						    singleClick :    true
						});
					}

					
				</script>
		</td>
		<td>Data wykrycia do:
		<ww:textfield name="'dataWykryciaDo'" id="dataWykryciaDo" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dataWykryciaDoTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	            <script type="text/javascript">
	            
	            if($j('#dataWykryciaDo').length > 0)
					{
						Calendar.setup({
						    inputField	:   "dataWykryciaDo",     // id of the input field
						    ifFormat	:   "<%=DateUtils.jsCalendarDateFormat%>", // format of the input field
							button		: 	"dataWykryciaDoTrigger", // trigger for the calendar (button ID)
							align 		: 	"Tl", // alignment (defaults to "Bl")
							singleClick : 	true
						});
					}
	            </script>
		 </td>
		<td>Nr niezgodno�ci:<ww:textfield name="'numerNiezgodnosci'" size="10" cssClass="'txt'"/>
		</td>
		<td>Miejsce wykrycia:<ww:textfield name="'miejsceWykrycia'" size="20" cssClass="'txt'"/>
		</td>
		
	</tr>
	<tr>	
		<td>Rodzaj:
					<ww:select name="'rodzajNiezgodnosci'" id="rodzajNiezgodnosci" list="rodzajeNiezgodnosci" listKey="key" listValue="value"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/> 
		</td>

		 <td>Typ:
		 	<select id="typNiezgodnosci" name="typNiezgodnosci" class="sel"> 
				<option value="">-- wybierz --</option>
				<ww:iterator value="typyNiezgodnosci">
					<option value=<ww:property value="id"/>  class=<ww:property value="refValue"/> <ww:if test="id == typNiezgodnosci">selected="selected"</ww:if>><ww:property value="title"/></option>
				</ww:iterator>
			</select>
			<script charset="utf-8" type="text/javascript">
				$j(function() {
					$j("#typNiezgodnosci").chained("#rodzajNiezgodnosci");
				});
			</script>

		</td>
		
		<td>Dotyczy zak�adu:
					<ww:select name="'dotyczyZakladu'" id="dotyczyZakladu" list="zaklady" listKey="key" listValue="value"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		<td>Autor:
					<ww:select name="'autor'" id="autor" list="autorzy" listKey="id" listValue="asLastnameFirstname()"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		
		<td>Osoba poinformowana:<ww:textfield name="'osobaPoinformowana'" size="20" cssClass="'txt'"/>
		</td>
		<td>Numer zdj�cia:<ww:textfield name="'numerZdjecia'" size="20" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			Zak�ad: <ww:select name="'zaklad'" id="zaklad" list="zaklady" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		<td>
			Typ kosztu: <ww:select name="'typKosztu'" id="typKosztu" list="typyKosztu" listKey="key" listValue="value"
					headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
		<td>
			Koszt jednostkowy: <ww:textfield name="'kosztJednostkowy'" size="20" cssClass="'txt'"/>
		</td>
		<td>
			Ilo��: <ww:textfield name="'ilosc'" size="20" cssClass="'txt'"/>
		</td>
		<td>
			Koszt ca�kowity <ww:textfield name="'kosztCalkowity'" size="20" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
	    <td>
	        <input type="submit" value="Filtruj" name="doSearch" class="btn" onclick="sendEvent(this)"/>
	    </td>
	    <td>
	    	<a href= "<ww:url value="'/office/reclamation-list.action>"/>">
			Wyczy�c pola
			</a>
		</td>	
	</tr>
	<tr>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=statusNiezgodnosci&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Status niezgodno�ci
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=statusNiezgodnosci&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=statusRealizacji&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Status realizacji
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=statusRealizacji&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=nrReklamacji&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Nr niezgodnosci
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=nrReklamacji&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=rodzaj&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Rodzaj
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=rodzaj&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=typ&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Typ
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=typ&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=autor&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>">
 					<img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/>
 			</a>
				Autor
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=autor&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=dataWykrycia&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Data wykrycia
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=dataWykrycia&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=dotyczyZakladu&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Dotyczy zak�adu
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=dotyczyZakladu&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=miejsceWykrycia&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Miejsce wykrycia
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=miejsceWykrycia&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=osobaPoinformowana&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
				Osoba poinformowana
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=osobaPoinformowana&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=numerZdjecia&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Nr zdj�cia
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=numerZdjecia&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Informacje
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'numerZdjecia'" value="numerZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Zak�ad
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Typ kosztu
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Koszt jednostkowy
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Ilo��
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
		<th>
 			<a title="<ds:lang text="SortowanieMalejace"/>" 
 				href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+false">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Koszt ca�kowity
 			<a title="<ds:lang text="SortowanieRosnace"/>" href="
 					<ww:url value="'/office/reclamation-list.action?doSearch=true&sortField=informacje&ascending='+true">
 						<ww:param name="'statusNiezgodnosci'" value="statusNiezgodnosci"/>
 						<ww:param name="'statusRealizacji'" value="statusRealizacji"/>
 						<ww:param name="'nrReklamacji'" value="nrReklamacji"/>
 						<ww:param name="'rodzajNiezgodnosci'" value="rodzajNiezgodnosci"/>
 						<ww:param name="'typNiezgodnosci'" value="typNiezgodnosci"/>
 						<ww:param name="'autor'" value="autor"/>
 						<ww:param name="'dataWykrycia'" value="dataWykrycia"/>
 						<ww:param name="'dotyczyZakladu'" value="dotyczyZakladu"/>
 						<ww:param name="'miejsceWykrycia'" value="miejsceWykrycia"/>
 						<ww:param name="'osobaPoinformowana'" value="osobaPoinformowana"/>
 						<ww:param name="'nrZdjecia'" value="nrZdjecia"/>
 						<ww:param name="'informacje'" value="informacje"/>
 						<ww:param name="'zaklad'" value="zaklad"/>
 						<ww:param name="'typKosztu'" value="typKosztu"/>
 						<ww:param name="'kosztJednostkowy'" value="kosztJednostkowy"/>
 						<ww:param name="'ilosc'" value="ilosc"/>
 						<ww:param name="'kosztCalkowity'" value="kosztCalkowity"/>
 					</ww:url>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a> 
		</th>
	</tr>
	<tr>
	<ww:iterator value="niezgodnosci">
		<tr>
			<td>
				<ww:property value="textStatus()" />
			</td>
	 		<td> 
				<ww:property value="textStatusRealizacji()" />
			</td>
			<td> 
				<ww:property value="nrReklamacji" />
			</td>
			<td> 
				<ww:property value="textRodzaj()" />
			</td>
			<td>
				<ww:property value="textTyp()" />
			</td>
			<td> 
				<ww:property value="textAutor()" />
			</td>
			<td> 
				<ww:property value="getDataWykryciaText()" />
			</td>
			<td> 
				<ww:property value="getDotyczyZakladuString()" />
			</td>
			<td> 
				<ww:property value="miejsceWykrycia" />
			</td>
			<td> 
				<ww:property value="osobaPoinformowana" />
			</td>
			<td> 
				<ww:property value="getNrZdjecia()" />
			</td>
			<td> 
				<ww:property value="informacje" />
			</td>
			<ww:if test="getReclamationCosts() == null || getReclamationCosts().isEmpty()">
				<td>-</td>
				<td>-</td>
				<td>-</td>
				<td>-</td>
				<td>-</td>
			</ww:if>
			<ww:iterator value="reclamationCosts" status="reclamationCostsStatus">
	            <ww:if test="!#reclamationCostsStatus.first">
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            	<td></td>
	            </ww:if>
	            <td>
	                <ww:property value="getWorksString()" />
	            </td>
	            <td>
	            	<ww:property value="getCostTypeString()"/>
	            </td>
	            <td>
	            	<ww:property value="unitCost"/>
	            </td>
	            <td>
	            	<ww:property value="number"/>
	            </td>
	           	<td>
	            	<ww:property value="totalCost"/>
	            </td>
	            <ww:if test="!#reclamationCostsStatus.last">
	            	</tr>
	            </ww:if>
            </ww:iterator>
		<ww:hidden name="'documentId'" value="documentId"/>
		</tr>
	</ww:iterator>	

</table>
		<ww:set name="pager" scope="request" value="pager" />
		<div class="alignCenter line25p">
			<jsp:include page="/pager-links-include.jsp"/>
		</div>
		<input type="submit" value="<ds:lang text="GenerujRaportXLS"/>" name="doGenerateXls" class="btn" onclick="setActionName('doGenerateXls');"/>
</form>