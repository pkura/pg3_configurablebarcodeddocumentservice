<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N bok-submitted.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PismoPrzychodzace"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value='&apos;/office/incoming/main.action&apos;'/>" method="post" onsubmit="return disableFormSubmits(this);">

<p></p>

<input type="button" value="<ds:lang text="PrzyjmijKolejne"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/incoming/bok.action?doNewDocument=true'"/>';"/>
<input type="button" value="<ds:lang text="PrzejdzDoPisma"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/incoming/main.action?documentId='+newDocumentId"/>';"/>
<input type="button" value="<ds:lang text="StronaGlowna"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/find-office-documents.action'"/>';"/>

</form>
