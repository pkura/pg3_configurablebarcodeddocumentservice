<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" pageEncoding="iso-8859-2"%>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person,
				 pl.compan.docusafe.service.ServiceManager,
				 pl.compan.docusafe.service.bip.Bip,
				 pl.compan.docusafe.service.ServiceDriverNotSelectedException"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<ww:set name="exists" value="documentId != null"/>

<div style="display: none;" id="pageType">office-addIncomingDocument-overall</div>

<ww:if test="documentType == 'in'">
	<ww:if test="#exists">
		<h1><ds:lang text="PismoPrzychodzace"/></h1>
	</ww:if>
	<ww:else>
		<h1><ds:lang text="PrzyjmijPismo"/></h1>
	</ww:else>
</ww:if>
<ww:elseif test="documentType == 'out'">
	<h1><ds:lang text="PismoWychodzace"/></h1>
</ww:elseif>
<ww:else>
	<h1><ds:lang text="Pismo"/></h1>
</ww:else>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var isSimpleView = <ww:if test="daa">false</ww:if><ww:else>true</ww:else>;

	// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
	// jako anonimowego
	var senderFields = new Array(
		'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
		'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
		'senderStreetAddress');
	
	function disableEnableSender(checkbox)
	{
		for (var i=0; i < senderFields.length; i++)
		{
			document.getElementById(senderFields[i]).disabled = checkbox.checked;
			//document.getElementById(senderFields[i]).style.background = checkbox.checked ? '#A5967C' : '#BCB09D';
		}
	}
	
	// funkcja wywo�ywana przez okienko z people-directory-results.jsp
	function __people_directory_pick(addresseeType, title, firstname, lastname,
		organization, organizationDivision, street, zip, location, country, email, fax)
	{
		if (addresseeType == 'sender')
		{
			document.getElementById('senderTitle').value = title;
			document.getElementById('senderFirstname').value = firstname;
			document.getElementById('senderLastname').value = lastname;
			document.getElementById('senderOrganization').value = organization;
			document.getElementById('senderStreetAddress').value = street;
			document.getElementById('senderZip').value = zip;
			document.getElementById('senderLocation').value = location;
			document.getElementById('senderCountry').value = country;
			document.getElementById('senderEmail').value = email;
			document.getElementById('senderFax').value = fax;
		}
		else if (addresseeType == 'recipient')
		{
			document.getElementById('recipientTitle').value = title;
			document.getElementById('recipientFirstname').value = firstname;
			document.getElementById('recipientLastname').value = lastname;
			document.getElementById('recipientOrganization').value = organization;
			document.getElementById('recipientDivision').value = organizationDivision;
			document.getElementById('recipientStreetAddress').value = street;
			document.getElementById('recipientZip').value = zip;
			document.getElementById('recipientLocation').value = location;
			//document.getElementById('recipientCountry').value = country;
			document.getElementById('recipientEmail').value = email;
			document.getElementById('recipientFax').value = fax;
		}
	}
	
	// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
	var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
	// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
	// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
	// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
	var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization','organizationDivision',
		'streetAddress', 'zip', 'location', 'country', 'email', 'fax');
	
	// lparam - identyfikator
	function openPersonPopup(lparam, dictionaryType)
	{
		if (lparam == 'sender')
		{
			openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+
				'&title='+document.getElementById('senderTitle').value+
				'&firstname='+document.getElementById('senderFirstname').value+
				'&lastname='+document.getElementById('senderLastname').value+
				'&organization='+document.getElementById('senderOrganization').value+
				'&personId='+document.getElementById('senderId').value+
				'&street='+document.getElementById('senderStreetAddress').value+
				'&zip='+document.getElementById('senderZip').value+
				'&location='+document.getElementById('senderLocation').value+
				'&country='+document.getElementById('senderCountry').value+
				'&email='+document.getElementById('senderEmail').value+
				'&fax='+document.getElementById('senderFax').value+
				'&guid=<ww:property value="personDirectoryDivisionGuid"/>' +
				'&canAddAndSubmit=true', 'person', 700, 650);
		}
		else
		{
			openToolWindowX('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true&guid=<ww:property value="personDirectoryDivisionGuid"/>','person',700,650);
		}
	}
	
	// lparam - identyfikator
	// wparam - pozycja na li�cie
	function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
	{
		//alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
		var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true' +
			'&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+
			'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true'+
			'&guid=<ww:property value="personDirectoryDivisionGuid"/>';
		//alert('url='+url);
		openToolWindowX(url, 'person', 700,650);
	}
	
	function personSummary(map)
	{
		var s = '';
		if (map.title != null && map.title.length > 0)
			s += map.title;
	
		if (map.firstname != null && map.firstname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.firstname;
		}
	
		if (map.lastname != null && map.lastname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.lastname;
		}
	
		if (map.organization != null && map.organization.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.organization;
		}
		if(map.organizationDivision!=null && map.organizationDivision.length>0){
			if (s.length > 0) s += ' / ';
			s += map.organizationDivision;
		}
		if (map.street != null && map.street.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.street;
		}
	
		if (map.location != null && map.location.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.location;
		}
	
		return s;
	}
	
	function __accept_person_duplicate(map, lparam, wparam)
	{
		if (lparam == 'recipient')
		{
			var sel = document.getElementById('odbiorcy');
	
			for (var i=0; i < sel.options.length; i++)
			{
				eval('var smap='+sel.options[i].value);
				if (compareArrays(smap, map, true))
				{
					//alert('Istnieje ju� taki odbiorca');
					return true;
				}
			}
		}
	
		return false;
	}
	
	/*
		Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
	*/
	function __accept_person(map, lparam, wparam)
	{
		//alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);
	
		if (lparam == 'recipient')
		{
			var sel = document.getElementById('odbiorcy');
	
			if (wparam != null && (''+wparam).length > 0)
			{
				setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
			}
			else
			{
				addOption(sel, JSONStringify(map), personSummary(map));
			}
		}
		else if (lparam == 'sender')
		{
			document.getElementById('senderTitle').value = map.title;
			document.getElementById('senderFirstname').value = map.firstname;
			document.getElementById('senderLastname').value = map.lastname;
			document.getElementById('senderOrganization').value = map.organization;
			document.getElementById('senderStreetAddress').value = map.street;
			document.getElementById('senderZip').value = map.zip;
			document.getElementById('senderLocation').value = map.location;
			document.getElementById('senderCountry').value = map.country;
			document.getElementById('senderEmail').value = map.email;
			document.getElementById('senderFax').value = map.fax;
		}
	}
</script>

<%
	boolean hasBip;
	try
	{
		ServiceManager.getService(Bip.NAME);
		hasBip = true;
	}
	catch (ServiceDriverNotSelectedException e)
	{
		hasBip = false;
	}
%>

<form id="test-overall" action="<ww:url value='&apos;/office/incoming/main.action&apos;'/>" method="post" enctype="multipart/form-data" onsubmit="selectAllOptions(document.getElementById('odbiorcy')); return disableFormSubmits(this);">
	<ww:hidden name="'filestoken'" value="filestoken"/>
	<ww:hidden name="'multiToken'"/>
	<ww:hidden name="'inActivity'" value = "inActivity"/>
	<input type="hidden" name="doCreate" id="doCreate"/>
	<input type="hidden" name="doIntoJournal" id="doIntoJournal"/>
	<input type="hidden" name="doCreateOutgoing" id="doCreateOutgoing"/>
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<input type="hidden" name="createNext" id="createNext"/>
	<input type="hidden" name="attachCase" id="attachCase"/>
	<ww:hidden name="'acceptDuplicateReferenceId'" value="acceptDuplicateReferenceId"/>
	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'activity'" value="activity"/>
	<ww:hidden name="'retainedObjectId'" value="retainedObjectId" />
	<ww:hidden name="'messageId'" value="messageId" />
	<%--  <ww:token /> --%>

	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

	<ww:if test="documentId!=null">
		<ds:extras test="!business">
			<input type="button" value="<ds:lang text="PotwierdzeniePrzyjecia"/>" class="btn"
					onclick="openToolWindow('<ww:url value="'/office/incoming/print-receipt.action'"><ww:param name="'documentId'" value="document.id"/></ww:url>');"/>
		</ds:extras>
	</ww:if>
	
	<%--	<ww:if test="!daa">
		<a id="changeViewLink" href="javascript:changeView()"><ds:lang text="PokazPelnyWidokFormularza"/></a>
	</ww:if>	--%>
	
	<ww:if test="#exists">
		<ww:if test="blocked">
			<p><span style="color: red">
				<ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
			</span></p>
		</ww:if>
	
		<p>
			<ww:set name="incomingDate" value="document.incomingDate" scope="page"/>
			<ds:lang text="Autor"/>:
			<b><ww:property value="creatingUser"/></b>
			
			<ds:lang text="dnia"/>
			<b><fmt:formatDate value="${incomingDate}" type="both" pattern="dd-MM-yy HH:mm"/></b>
			<br/>
			
			<ds:lang text="Dzial"/>:
			<b><ww:property value="assignedDivision"/></b>
			<br/>
			
			<% if (hasBip) { %>
					<ds:modules test="office or simpleoffice">
							<ds:lang text="NumerWBIP"/>:
							<b><ww:property value="document.trackingNumber"/></b>
							<br/>
							<ds:lang text="HasloWBIP"/>:
							<b><ww:property value="document.trackingPassword"/></b>
							<br/>
					</ds:modules>
			<% } %>
			
			<ds:lang text="NumerKancelaryjny"/>:
			<b id='documentOfficeNumber'><ww:property value="document.formattedOfficeNumber"/></b>
			<br/>
			
			<ds:modules test="office or simpleoffice">
				<ds:extras test="!business">
					<ds:lang text="SymbolWsprawie"/>:
					<b><ww:property value="caseDocumentId"/></b>
					<br/>
				</ds:extras>
			</ds:modules>
			
			<ww:iterator value="journalMessages">
				<ds:lang text="NumerWdziale"/>
				'<ww:property value='key'/>':
				<b><ww:property value='value'/></b>
				<br/>
			</ww:iterator>

			<ww:if test="document.masterDocument != null">
				<ww:push value="document.masterDocument">
					<ds:lang text="DokumentGlowny"/>:
					<a target="_blank" href="<ww:url value="'/office/incoming/summary.action'"><ww:param name="'documentId'" value="id"/></ww:url>"><ww:property value="formattedOfficeNumber"/> (<ww:property value="caseDocumentId"/>)</a>
					<br/>
				</ww:push>
			</ww:if>
		</p>
	</ww:if>
	
	<%-- pole ustawiane tylko w FillNewDocumentForm, wi�c nie trzeba sprawdza�, czy pismo ju� istnieje --%>
	<ww:if test="currentDayWarning">
		<p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
	</ww:if>
	
	<table border="0" class="formTable">
		<ww:if test="intoJournalPermission && incomingDivisions != null && incomingDivisions.size() > 0">
			<tr>
				<td>
					<ds:lang text="PrzyjmijPismoWwydziale"/>:
				</td>
				<td>
					<ww:select name="'intoJournalId'" list="incomingDivisions" cssClass="'sel'"
						headerKey="''" headerValue="getText('select.wybierz')" />
					<input type="submit" name="doIntoJournal" class="btn" value="<ds:lang text="Przyjmij"/>"
						onclick="document.getElementById('doIntoJournal').value='true'"/>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td>
				<ds:lang text="DataPisma"/>
				<span class="star">*</span>:				
			</td>
			<td>
				<ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
			</td>
		</tr>
		
		<ds:extras test="!business">
			<ds:modules test="office or simpleoffice">
				<tr>
					<td>
						<ds:lang text="DataStemplaPocztowego"/>:
					</td>
					<td>
						<ww:textfield name="'stampDate'" size="10" maxlength="10" cssClass="'txt'" id="stampDate"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="stampDateTrigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
					</td>
				</tr>
			</ds:modules>
		</ds:extras>
		
		<tr>
			<td>
				<ds:lang text="OpisPisma"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textarea name="'description'" rows="3" cols="47" cssClass="'txt'" id="description"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="ZnakPisma"/>:
			</td>
			<td>
				<ww:textfield name="'referenceId'" size="50" maxlength="60" cssClass="'txt'" id="referenceId"/>
			</td>
		</tr>
		<tr name="extended">
			<td>
				<ds:lang text="KodKreskowy"/>:
			</td>
			<td>
				<ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" id="barcode"/>
			</td>
		</tr>
		
		<% if (hasBip) { %>
		<ds:extras test="!business">
			<ds:modules test="office or simpleoffice">
				<tr>
					<td>
						<ds:lang text="WysylajDoBIP"/>:
					</td>
					<ww:if test="canExportToBip">
						<td>
							<ww:checkbox name="'submitToAsiBip'" fieldValue="true" value="submitToAsiBip"/>
						</td>
					</ww:if>
					<ww:else>
						<td>
							<ww:checkbox name="'submitToAsiBip'" fieldValue="true" value="submitToAsiBip" disabled="true" />
						</td>
					</ww:else>
				</tr>
			</ds:modules>
		</ds:extras>
		<% } else {%>
		<ds:extras test="!business">
			<tr>
				<td>
					<ds:lang text="WysylajDoBIP"/>:
				</td>
				<td>
					<ww:checkbox name="'submitToAsiBip'" fieldValue="true" value="submitToAsiBip" disabled="true" />
				</td>
			</tr>
		</ds:extras>
		<% } %>
		
		<tr>
			<td>
				<ds:lang text="StatusPisma"/>:
			</td>
			<td>
				<ww:select name="'statusId'" list="statuses" listKey="id" listValue="name"
				value="#exists ? status.id : statusId" id="status" cssClass="'sel'"
				headerKey="''" headerValue="'-------'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="SposobDostarczenia"/>:
			</td>
			<td>
				<ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
				value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="SposobOdbioru"/>:
			</td>
			<td>
				<ww:select name="'outgoingDeliveryId'" list="outgoingDeliveries" listKey="id" listValue="name"
				value="#exists ? outgoingDelivery.id : outgoingDeliveryId" id="outgoingDelivery" cssClass="'sel'"/>
			</td>
		</tr>
		
		<ds:modules test="office or simpleoffice">
			<ww:if test="#exists">
				<tr>
					<td>
						<ds:lang text="ReferentPisma"/>:
					</td>
					<td>
						<ww:select name="'assignedUser'" list="clerks" listKey="name"
							listValue="asFirstnameLastnameName()" value="assignedUser"
							headerKey="''" headerValue="getText('select.wybierz')"
							cssClass="'sel'" disabled="#exists and !canSetClerk" />
					</td>
				</tr>
			</ww:if>
		</ds:modules>
		
		<tr>
			<td>
				<ds:lang text="PolozeniePisma"/>:
			</td>
			<td>
				<ww:textfield name="'location'" size="50" maxlength="50" cssClass="'txt'" id="location"/>
			</td>
		</tr>
		
		<tr class="formTableDisabled">
			<td colspan="2">
				<p><h4 class="hotfix-tl"><ds:lang text="Nadawca"/></h4></p>
			</td>
		</tr>
		<ww:hidden name="'senderId'" id="senderId" value="senderId"/>
		<tr>
			<td>
				<ds:lang text="Anonim"/>:
			</td>
			<td>
				<ww:checkbox name="'senderAnonymous'" fieldValue="'true'" value="senderAnonymous" onchange="'disableEnableSender(this);'" id="senderAnonymous" />
			</td>	
		</tr>
		
		<!-- readonly="#exists" -->
		<%--cssClass="#exists ? 'txt_ro' : 'txt'"--%>
		<tr>
			<td>
				<ds:lang text="TytulStanowisko"/>:
			</td>
			<td>
				<ww:textfield name="'senderTitle'" size="30" maxlength="30" cssClass="'txt'" id="senderTitle"  />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Imie"/>:
			</td>
			<td>
				<ww:textfield name="'senderFirstname'" size="50" maxlength="50" cssClass="'txt'" id="senderFirstname"  />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nazwisko"/>:
			</td>
			<td>
				<ww:textfield name="'senderLastname'" size="50" maxlength="50" cssClass="'txt'" id="senderLastname"  />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="FirmaUrzad"/>:
			</td>
			<td>
				<ww:textfield name="'senderOrganization'" size="50" maxlength="50" cssClass="'txt'" id="senderOrganization"  />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Ulica"/>:
			</td>
			<td>
				<ww:textfield name="'senderStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="senderStreetAddress"  />
			</td>
		</tr>
		<tr>
			<td valign="top">
				<ds:lang text="KodPocztowyImiejscowosc"/>:
			</td>
			<td>
				<ww:textfield name="'senderZip'" size="7" maxlength="14" cssClass="'txt'" id="senderZip"  />
				<ww:textfield name="'senderLocation'" size="38" maxlength="50" cssClass="'txt dontFixWidth'" 
					id="senderLocation"  onchange="'showCode();'" cssStyle="'width: 194px;'"/>
				<br/>
				<span class="warning" id="senderZipError"> </span>
			</td>
		</tr>
		<tr name="extended">
			<td>
				<ds:lang text="Kraj"/>:
			</td>
			<td>
				<ww:select name="'senderCountry'"
					list="@pl.compan.docusafe.util.Countries@COUNTRIES"
					listKey="alpha2" listValue="name" value="'PL'"
					id="senderCountry" cssClass="'sel'" onchange="this.form.senderZip.validator.revalidate();"/>
			</td>
		</tr>
		<tr name="extended">
			<td>
				<ds:lang text="Email"/>:
			</td>
			<td>
				<ww:textfield name="'senderEmail'" size="50" maxlength="50" cssClass="'txt'" id="senderEmail"  />
			</td>
		</tr>
		<tr name="extended">
			<td>
				<ds:lang text="Faks"/>:
			</td>
			<td>
				<ww:textfield name="'senderFax'" size="30" maxlength="30" cssClass="'txt'" id="senderFax"  />
			</td>
		</tr>
		<tr class="formTableDisabled">
			<td>
			</td>
			<td align="right">
				<input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" >
			</td>
		</tr>
		
		<tr>
			<td colspan="2" class="formTableDisabled">
				<p><h4 class="hotfix-tl"><ds:lang text="Odbiorcy"/></h4></p>
			</td>
		</tr>
		
		<ds:available test="documentMain.chooseCurrentDivision">
			<tr>
			    <td valign="top"><ds:lang text="Dzia�"/>:</td>
			    <td class="leftCell"><ww:select name="'currentAssignmentGuid'" list="divisions" headerKey="''" headerValue="getText('select.wybierz')" listKey="guid" listValue="asNameParentName()" cssClass="'sel'"/></td>
			</tr>
		</ds:available>
		<tr class="formTableDisabled">
			<td colspan="2">
				<ww:select name="'recipients'" id="odbiorcy" cssStyle="'width: 500px;'" multiple="true" size="3" cssClass="'multi_sel'"
					list="recipientsMap" />
				<br/>
				<script type="text/javascript">					
					$j(document).ready(function(event) {
						var sel_el = $j('#odbiorcy');
						var minSize = 3;
						var maxSize = 15;
						
					    sel_el.data("size", $j("#odbiorcy option").size());
					    if(sel_el.children().length > minSize && sel_el.children().length < maxSize)
						    sel_el.attr('size', sel_el.children().length); 

					    $j("#odbiorcy option").livequery(function() {
					        if ($j("#odbiorcy option").size() > sel_el.data("size") && sel_el.data("size") >= minSize) {
					  			sel_el.attr('size', $j("#odbiorcy option").size());
								//alert('dodaje odbiorce: size=' + sel_el.data("size"));
					  			if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
					  		}
					
					        sel_el.data("size", $j("#odbiorcy option").size());
					    }, function() {
							  if ($j("#odbiorcy option").size() < sel_el.data("size")) {
								  sel_el.attr('size', sel_el.data("size")-1);	
								  if(sel_el.data("size") <= minSize)
							  			sel_el.attr('size', minSize);
									
								if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
							  }
					        sel_el.data("size", $j("#odbiorcy option").size());
						 });				    
					});
				</script>
		
				<span class="btnContainer">
					<input type="button" value="<ds:lang text="DodajOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" >
					<input type="button" value="<ds:lang text="EdycjaOdbiorcy"/>" onclick="var odb=document.getElementById('odbiorcy'); openEditPersonPopup(odb.options[odb.options.selectedIndex].value, odb.options.selectedIndex, 'recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn">
					<input type="button" value="<ds:lang text="UsunOdbiorcow"/>" onclick="deleteSelectedOptions(document.getElementById('odbiorcy'));" class="btn">
				</span>
			</td>
		</tr>
		
		<tr class="formTableDisabled">
			<td colspan="2">
				<p><h4 class="hotfix-tl"><ds:lang text="Inne"/></h4></p>
			</td>
		</tr>
		
		<tr>
			<td valign="top">
				<ds:lang text="NrPrzesylkiRejestrowanej"/> (R):
			</td>
			<td>
				<ww:textfield name="'postalRegNumber'" size="20" maxlength="20" cssClass="'txt'" id="postalRegNumber" />
				<p></p>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<ds:lang text="Oryginal"/>:
			</td>
			<p></p>
			<td>
				<ww:checkbox name="'original'"  fieldValue="true" id="original" value="original"></ww:checkbox>
				<p></p>
			</td> 
		</tr>
		<tr>
			<td valign="top">
				<ds:lang text="RodzajPisma"/><span class="star">*</span>:
			</td>
			<td>
				<ww:select name="'kindId'" list="kinds" listKey="id" id="kindId"
					listValue="name + ' ('+getText('Bok.Dni')+': ' + days + ')'" cssClass="'sel'"
					value="#exists ? kind.id : kindId" disabled="#exists && !canChangeKind"
					headerKey="''" headerValue="'-------'" />
				<p></p>
				<ww:if test="#exists && !canChangeKind">
					<ww:hidden name="'kindId'" value="kind.id"/>
				</ww:if>
			</td>
		</tr>
		<%--	<tr>
			<td valign="top">Typ dokumentu:</td>
			<td>
				<ww:select name="'documentExtInf'" cssClass="'sel'" disabled="#exists" />
				<p></p>
			</td>
		</tr>	--%>
		<tr>
			<td>
				<ds:lang text="Dyspozycje"/>:
			</td>
			<td>
				<ww:textarea name="'packageRemarks'" id="packageRemarks" rows="1" cols="47" cssClass="'txt'"/>
			</td>
		</tr>
		<tr name="extended">
			<td>
				<ds:lang text="InneUwagi"/>:
			</td>
			<td>
				<ww:textarea name="'otherRemarks'" id="otherRemarks" rows="4" cols="47" cssClass="'txt'"/>
			</td>
		</tr>
		
		<ww:if test="loadFiles != null && loadFiles.size() > 0">
<tr>
	<td colspan="1">
		<ds:lang text="PlikiJuzobrane"/>:
	</td>
</tr>
</ww:if>
<ww:if test="documentId == null">
	<ww:iterator value="loadFiles">
		<tr>
			<td colspan="1">
				<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
				<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
			</td>
		</tr>
	</ww:iterator>
	<tr>
		<td colspan="2">
		<table id="kontener">
			<tbody id="tabela_glowna">
				<tr>
					<td><ds:lang text="Plik" /><span class="star">*</span>:</td>
				</tr>
				<tr>
					<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td><a href="javascript:addAtta();"><ds:lang
			text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
	</tr>
</ww:if>		
		
			<ww:if test="useScanner">
				<tr>
					<td>Skanowanie</td>
					<td>
						<table cellspacing="0" cellpadding="0" style="border: 1px solid black; padding: 2px">
							<tr>
								<td>
									<script>
										var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
											request.getServerName() + ":" + request.getServerPort() %>';
										// u�yty mo�e by� albo plik jtwain.jar znajduj�cy si� w aplikacji,
										// albo plik systemowy (w JRE_HOME/lib/ext)
										var object =  appletObject('scanapplet',
											'pl.compan.docusafe.applets.scanner.AttachmentsApplet.class',
											'<ww:url value="'/scanner.jar'"/>, <ww:if test="!useSystemJTwain"><ww:url value="'/jtwain-8.2.jar'"/>, </ww:if> <ww:url value="'/jai_core-1.1.2_01.jar'"/>, <ww:url value="'/jai_codec-1.1.2_01.jar'"/>', 700, 55,
											{ 'uploadUrl' : host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>',
											  'reloadPage' : 'false',
											  'multiToken' : '<ww:property value="multiToken"/>',
											  'readFileTokens': 'true' },
											new Array('xMSIE'));
										//alert('object='+object);
										document.write(object);
									</script>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						Po��cz zeskanowane pliki (multitiff):
					</td>
					<td>
						<ww:checkbox name="'multitiff'" fieldValue="true" value="multitiff"/>
					</td>
				</tr>
			</ww:if>		
		
		<ww:if test="documentId == null && instantAssignmentUsers != null && !instantAssignmentUsers.empty">
			<tr>
				<td>
					<ds:lang text="NatychmiastowaDekretacjaNa"/>:
				</td>
				<td>
					<ww:select name="'instantAssignment'" list="instantAssignmentUsers"
						listKey="value" listValue="label" id="instantAssignment"
						cssClass="'sel'" headerKey="''" headerValue="'-------'" />
				</td>
			</tr>
		</ww:if>
		
		<ww:if test="!#exists && incomingJournals.size > 0">
			<tr>
				<td>
					<ds:lang text="DziennikPismPrzychodzacych"/>:
				</td>
				<td>
					<ww:select name="'incomingJournalId'" list="incomingJournals"
						listKey="id" listValue="description" cssClass="'sel'" />
				</td>
			</tr>
		</ww:if>

		<tr class="formTableDisabled">
			<td></td>
			<td>
				
				<ww:if test="!#exists">
					<input type="submit" name="doCreate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
						onclick="if (!validateForm()||!checkSubstitutions()) return false; document.getElementById('doCreate').value = 'true';" <ww:if test="!canCreate || save">disabled="disabled"</ww:if>/>
					<input type="submit" name="doCreate" value="Zapisz i przyjmij nast�pne" class="btn"
						onclick="if (!validateForm()||!checkSubstitutions()) return false; document.getElementById('doCreate').value = 'true';document.getElementById('createNext').value = 'true';" <ww:if test="!canCreate || save">disabled="disabled"</ww:if>/>
					<input type="submit" name="doCreate" value="Zapisz i dodaj do sprawy" class="btn"
						onclick="if (!validateForm()||!checkSubstitutions()) return false; document.getElementById('doCreate').value = 'true';document.getElementById('attachCase').value = 'true';" <ww:if test="!canCreate || save">disabled="disabled"</ww:if>/>
				</ww:if>
				<ww:else>
					<input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" <ww:if test="blocked || save">disabled="disabled"</ww:if>
						onclick="if (!validateForm()||!checkSubstitutions()) return false; document.getElementById('doUpdate').value = 'true';"/>
					<ds:extras test="!business">
						<input type="button" value="<ds:lang text="PotwierdzeniePrzyjecia"/>" class="btn"
							onclick="openToolWindow('<ww:url value="'/office/incoming/print-receipt.action'"><ww:param name="'documentId'" value="document.id"/></ww:url>');"/>
					</ds:extras>
					<ds:available test="ogolne.print.barcode">
						<ds:event name="'printLabel'" value="getText('DrukujEtykiete')"/>
					</ds:available>		
					<ww:if test="useBarcodePrinter">
						<input type="button" value="<ds:lang text="WydrukujKodKreskowy"/>" class="btn"
							onclick="var msg = document.getElementById('barapplet').print('<ww:property value="documentBarcode"/>', '2of5'); if (msg != null) alert(msg);"/>
					</ww:if>
				</ww:else>
			</td>
		</tr>
	</table>
	
	<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script type="text/javascript">
	disableEnableSender(document.getElementById('senderAnonymous'));
	if($j('#documentDate').length > 0)
	{
		Calendar.setup({
			inputField	 :	"documentDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"documentDateTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
<ds:extras test="!business">
<ds:modules test="office or simpleoffice">
	if($j('#stampDate').length > 0)
	{
		Calendar.setup({
			inputField	 :	"stampDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"stampDateTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
</ds:modules>
</ds:extras>
<%--
<ww:if test="useBarcodes">
var bi = new BarcodeInput({
	'prefix' : '<ww:property value="barcodePrefix"/>',
	'suffix' : '<ww:property value="barcodeSuffix"/>',
	'inputId' : 'barcode',
	'submit' : false,
	//'submitId' : 'send',
	'tabStarts' : false
});
</ww:if>
--%>

var szv = new Validator(document.getElementById('senderZip'), '##-###');

szv.onOK = function() {
	this.element.style.color = 'black';
	document.getElementById('senderZipError').innerHTML = '';
	if (document.getElementById('senderZip').value != "") 
    {
		showLocation(); 
	}
}

	//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************ //
		//   |                  |			       |	   //
		//   |			        |			       |       //
		//   |			        |			       |       //
		//  \/			       \/			      \/       //
		
		var xmlHttp = null;
		var serverName = '<%= request.getServerName() + ":" %>';
		var port = '<%= request.getServerPort() %>';
		var contextPath = '<%= request.getContextPath() %>';
		var protocol = window.location.protocol;
		
		var polskieZnaki = new Array();
		var zamiana = new Array();
		function showCode()
		{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			var location=document.getElementById('senderLocation').value;
			
			//Rozwiazanie problemu z polskimi znakami 
			var polskieZnaki = new Array();
			var zamiana = new Array();
			
			
			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";
			
			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";
			
			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";
			
			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";
			
			for(i = 0; i < 18; i++)
			{
				while (location.indexOf(polskieZnaki[i])>-1) 
				{
					pos= location.indexOf(polskieZnaki[i]);
					location = "" + (location.substring(0, pos) + zamiana[i] + 
					location.substring((pos + polskieZnaki[i].length), location.length));
				}
			}
			
			if(location==null)
			{
				alert ("No element with id 'location' found on the page");
				return;
			}
			//alert("http://" + serverName + port + contextPath + "/code.ajx?location=" + location);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/code.ajx?location=" + location,true);
			xmlHttp.onreadystatechange = function() { 
				stateChanged('senderZip');
			};
			
			xmlHttp.send(null);
			
		}
		
		function showLocation()
		{
		
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			
			var zip=document.getElementById('senderZip').value;
			
			if(zip==null)
			{
				alert ("No element with id 'zip' found on the page");
				return;
			}
			
			//alert("http://" + serverName + port + contextPath + "/location.ajx?zip=" + zip);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip,true);
			xmlHttp.onreadystatechange = function() { 
				stateChanged('senderLocation');
			};
			
			xmlHttp.send(null);
		}
		
		function stateChanged(variable) 
		{ 
			
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			
			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";
			
			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";
			
			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";
			
			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";
			
			if (xmlHttp.readyState==4)
			{ 
				
				if(xmlHttp.status==200)
				{
				
					var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];
										
					if(valid.childNodes[0].nodeValue=='LocNotFound' && variable =='senderZip')
					{
						alert("Nie znalaz�em kodu dla tej miejscowo�ci.");
						//document.getElementById(variable).value = '';
						//document.getElementById('senderLocation').value = '';
					}
					else
					{
						var message = valid.childNodes[0].nodeValue;
						for(i = 0; i < 18; i++)
						{
							while (message.indexOf(zamiana[i])>-1) 
							{
								pos= message.indexOf(zamiana[i]);
								message = "" + (message.substring(0, pos) + polskieZnaki[i] + 
								message.substring((pos + zamiana[i].length), message.length));
							}
						}
					}
					if(valid.childNodes[0].nodeValue=='CodeNotFound' && variable =='senderLocation')
					{
						alert("Nie znalaz�em miejscowo�ci dla danego kodu.");
						document.getElementById(variable).value='';
					}
					if(message.trim().length ==6)
					{ 
						document.getElementById(variable).value=message;
					}
					else if (message.trim().length >6 && variable=='senderZip')
					{
						alert("Nie ma jednego kodu pocztowego dla danej miejscowo�ci. Zakres(y) to: " + message + ".");
						document.getElementById('senderZip').value=message.substring(0, 6);
					}
					else if(variable=='senderLocation' && valid.childNodes[0].nodeValue!='CodeNotFound')
					{
						document.getElementById(variable).value=message;
					}
					
	
				}
				else
				{
					alert("Error loading page"+ xmlHttp.status +":"+ xmlHttp.statusText);
				}
			} 
			else  if (xmlHttp.readyState==0)
			{
				alert("	Object is uninitialized");
			}
			
		}
		
		function GetXmlHttpObject()
		{
			var xmlHttp=null;
			try
			{
			// Firefox, Opera 8.0+, Safari
				xmlHttp=new XMLHttpRequest();
			}
			catch (e)
			{
			//Internet Explorer
				try
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e)
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			return xmlHttp;
		}
		//*******************************//
		
		//   /\                          /\			      /\     //
		//   |			        |			       |     //
		//   |			        |			       |     //
		//   |		                 |			       |     //
		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************//


szv.onEmpty = szv.onOK;

szv.onError = function(code) {
	this.element.style.color = 'red';
	if (code == this.ERR_SYNTAX)
		document.getElementById('senderZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp."/> 00-001';
	else if (code == this.ERR_LENGTH)
		document.getElementById('senderZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp."/> 00-001)';
}

szv.canValidate = function() {
	var country = document.getElementById('senderCountry');
	var countryCode = country.options[country.options.selectedIndex].value;

	return (countryCode == '' || countryCode == 'PL');
}


// pocz�tkowa walidacja danych
szv.revalidate();

function validateForm()
{
   /* var file = document.getElementById('file');
	alert(file.value);  */

	var description = document.getElementById('description').value;
	var kindId = document.getElementById('kindId').value;
	var packageRemarks = document.getElementById('packageRemarks').value;
	var otherRemarks = document.getElementById('otherRemarks').value;

	if (description.trim().length == 0) { alert('<ds:lang text="NiePodanoOpisuPisma"/>'); return false; }
	if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNjwyzej80Znakow"/>'); return false; }
	if (kindId.trim().length == 0) { alert('<ds:lang text="NieWybranoRodzajuPisma"/>'); return false; }
	if (packageRemarks != null && packageRemarks.length > 240) { alert('<ds:lang text="WartoscPolaDyspozycjeJestZbytDluga.PoleMozeMiecCoNajwyzej240znakow"/>'); return false; }
	if (otherRemarks != null && otherRemarks.length > 240) { alert('<ds:lang text="WartoscPolaUwagiJestZbytDluga.PoleMozeMiecCoNajwyzej240znakow"/>'); return false; }
	return true;
}

// zmiana widoku formularz prosty/szczegolowy
/*function changeView()
{
	var elems = document.getElementsByTagName("tr");//'extended');
	var i;
	var link = document.getElementById("changeViewLink");
	//alert(elems.length);
	//alert('pzred');
	for (i = 0; i < elems.length; i++)
	{
		//alert(elems[i].getAttribute('name'));
		if (elems[i].getAttribute('name') == 'extended') {
			//alert(i);
			if (!isSimpleView) {
				elems[i].setAttribute('style', 'display:none');//style.display = 'none';
			}
			else {
				elems[i].setAttribute('style', 'display:table-row');//style.display = 'table-row';
			}
		}
	}
	if (isSimpleView) {
		link.firstChild.nodeValue = '<ds:lang text="PokazUproszczonyWidokFormularza"/>';
		isSimpleView = false;
	}
	else {
		link.firstChild.nodeValue = '<ds:lang text="PokazPelnyWidokFormularza"/>';
		isSimpleView = true;
	}
}*/

var countAtta = 1;
function addAtta() 
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);

}

function delAtta() 
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;
		/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}



function checkSubstitutions()
	{
	var count=0;
	var show=0;
	assignments=document.forms[0].assignments;
	var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n";
	var str1="";
	try
	{
		var num=document.forms[0].instantAssignment.selectedIndex;
		if(num>0)
		{
			var naz=document.forms[0].instantAssignment.options[num].text;		
			<ww:iterator value="substitute" status="status">
				if(naz.match("<ww:property value="key"/>")!=null)
				{
					show=1;
					str1=str1+"\n<ds:lang text="UzytkownikJestZastepowanyPrzez"/> <ww:property value="value"/>";
				
				}
			</ww:iterator>
   	 	}
		/*if(show == 0)
		{
			if(!confirm("Na pewno dekretowa�?"))
			return false;
   		}*/
		if(show!=0)
		{
			if(!confirm(str+str1))
			return false;
  		}
  	}catch(err)
  	{
  		return true;
  	}
	
	
	return true;
	
	}
</script>

<ww:if test="useBarcodePrinter">
	<script>
	var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
		request.getServerName() + ":" + request.getServerPort() %>';
	var object =  appletObject('barapplet', 'pl.compan.docusafe.applets.barprinter.BarcodePrinterApplet.class', '<ww:url value="'/barprinter.jar'"/>', 1, 1,
		{ 'device': '<ww:property value="barcodePrinterDevice"/>',
		  'printerType': 'Zebra_TLP2824',
		  'labelWidth': '26mm',
		  'labelHeight': '15mm' },
		new Array('xMSIE'));
	//alert('object='+object);
	document.write(object);
	</script>
</ww:if>

