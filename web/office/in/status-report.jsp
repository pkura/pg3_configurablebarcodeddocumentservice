<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N status-report.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ww:if test="documentType == 'in'">
    <h1><ds:lang text="PismoPrzychodzace"/></h1>
</ww:if>
<ww:elseif test="documentType == 'out'">
    <h1><ds:lang text="PismoWychodzace"/></h1>
</ww:elseif>
<ww:else>
    <h1><ds:lang text="Pismo"/></h1>
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><a href="<ww:url value="'/office/incoming/work-history.action'"><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>"><ds:lang text="PowrotDoHistoriiPisma"/></a></p>

<table>
<tr>
    <th><ds:lang text="Status"/></th>
    <th><ds:lang text="CzasPrzebywania"/></th>
    <th><ds:lang text="PoczatekPrzebywania"/></th>
</tr>
<ww:iterator value="beans">
    <tr>
        <td><ww:property value="name"/></td>
        <td><ds:format-time-period value="period"/></td>
        <td><ds:format-date value="start" pattern="dd-MM-yy HH:mm:ss"/></td>
    </tr>
</ww:iterator>
</table>

