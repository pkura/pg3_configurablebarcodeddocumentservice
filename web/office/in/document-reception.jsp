<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<ww:url value="'/knockout-2.2.0.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/comet.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/functions.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/server-logger.js'" />"></script>

<div id="barcode-container">
	<label for="barcode">Barkod dokumentu:</label>
	<input type="text" id="barcode" autofocus="autofocus" autocomplete="off" class="txt" />
</div>

<div id="lastname-container">
	Odbiorca: <b><ww:property value="firstname"/>&nbsp;<ww:property value="lastname"/></b>
</div>

<table id="mainTable" class="search document-reception" cellspacing="0" width="100%">
	<caption>Liczba wprowadzonych dokument�w: <b data-bind="text: documents().length"></b></caption>
	<thead>
		<tr>
			<th class="empty">Lp</th>
			<th class="empty">Barkod</th>
			<th class="empty">Dostarczenie</th>
			<th class="empty">Nadawca</th>
			<th class="empty">Adresat</th>
			<th class="empty">Data</th>
			<th class="empty"></th>
		</tr>
	</thead>
	<tbody data-bind="if: documents().length < 1">
		<tr><td colspan="7" class="no-barcodes-present">Nie wprowadzono �adnego barkodu</td></tr>
	</tbody>
	<tbody data-bind="foreach: documents">
		<tr>
			<td data-bind="text: $index()+1"></td>
			<td data-bind="text: barcode"></td>
			<td data-bind="text: delivery"></td>
			<td data-bind="text: sender"></td>
			<td data-bind="text: recipient"></td>
			<td data-bind="text: formattedDate"></td>
			<td><input type="button" class="btn" data-bind="click: $root.removeDocument" value="usu�" /></td>
		</tr>
	</tbody>
</table>



<script type="text/javascript">

var ACTION_URL = '<ww:url value="'/office/incoming/document-reception.action'" />';
var fromUrl = '<ww:property value="fromUrl" />';

function checkSession() {
	$j.post(ACTION_URL, {checkSession: true}, function(data) {
		if(data != 'OK') {
			//window.onbeforeunload = null;
			window.onunload = null;
			window.location.href = '<ww:url value="'/'" />';	
		}
	});
}

function Document(documentId, barcode, delivery, sender, recipient, date) {
	var self = this;
	
	self.documentId = documentId;
	self.barcode = barcode;
	self.delivery = delivery;
	self.sender = sender;
	self.recipient = recipient;
	self.date = date;
	self.formattedDate = ko.computed(function() {
		var strDate = '';
		try {
			strDate = (new Date(self.date)).format('dd-mm-yyyy');
		} catch(e) {}
		
		return strDate;
	});
	
}

function DocumentsViewModel() {
	var self = this;
	
	self.documents = ko.observableArray([]);
	
	self.removeDocument = function(doc) {
		$j('#barcode').focus();
		$j.post(ACTION_URL, {removeBarcode: true, barcode: doc.barcode}, function(data) {
			
		});
		self.documents.remove(doc);
	}
	
	self.containsBarcode = function(barcode) {
		var match = ko.utils.arrayFirst(self.documents(), function(item) {
			return item.barcode == barcode;
		});
		
		return match != null;
	}
}

var docViewModel = new DocumentsViewModel();
ko.applyBindings(docViewModel);
var barcodeNotifier = new CometRequest({
	url: '<ww:url value="'/comet-notifier.ajx'" />' + '?MESSAGE_KIND=BARCODE_OFFICE',
	callback: function barcodeCallback(str, params) {
		var strs = str.split('\r\n'), len = strs.length,
		strJson = null, i = 0, messages = [], msg = null;
		for(i = 0; i < len; i ++) {
			strJson = strs[i];
			if(strJson == '') {
				continue;
			}
			
			try {
				messages.push($.parseJSON(strJson));
			} catch(e) {
				serverLogger.error(e,{"name": "jsonParseError","strJson":strJson,"str": str});
				alert('B��d parsowania JSON: ' + str);
				checkSession();
				//console.error(e);
			}
		}
		
		for(i = 0, len = messages.length; i < len; i ++) {
			msg = messages[i];
			switch(msg.barcodeOperation) {
			case 'REMOVE':
			default:
				docViewModel.documents.remove(function(item) {
					return item.barcode == msg.barcode;
				});
			break;
			}
		}
		
		if(params.state == 'complete') {
			barcodeNotifier.init();
			return;
		}
	},
	error: function() {
		checkSession();
	}
});

barcodeNotifier.init();

$j('#barcode').bind('keydown', function(event) {
	if(event.keyCode == 13) {
		$j(this).trigger('barcode');
		return false;
	}
}).bind('barcode', function(event) {
	var barcode = $j(this).val();
	if(barcode != '') {
		$j(this).val('');
		if(docViewModel.containsBarcode(barcode) == false) {
			$j.post(ACTION_URL, {addBarcode: true, barcode: barcode}, function(data) {
				var ret = null, docMetric = null, doc = null, userInfo = null;
				
				//console.log(data);
				try {
					ret = $j.parseJSON(data);
				} catch(e) {
					serverLogger.error(e,{"name": "jsonParseError","data":data});
					//alert('B��d parsowania JSON: ' + data);
					checkSession();
				}
				
				if(ret.success == true) {
					docMetric = ret.documentMetric;
					doc = new Document(docMetric.documentId, docMetric.barcode, docMetric.delivery, docMetric.sender, docMetric.recipient, docMetric.date);
					docViewModel.documents.push(doc);
				} else {
					if(ret.error == 'duplicated reception') {
						docMetric = ret.documentMetric;
						try {
							userInfo = docMetric.recipientUser;
						} catch(e) { 
							userInfo = null;
						}
						ret.errorMessage = 'Pismo o numerze przesy�ki rejestrowane ' + docMetric.barcode.bold() + ' zosta�o ju� wydane ';
						if(userInfo != null) {
							ret.errorMessage += 'u�ytkownikowi ' + userInfo.firstname + ' ' + userInfo.lastname.bold()
								+ ' (' + userInfo.name + ').';
						} else {
							ret.errorMessage += 'nieznanemu u�ytkownikowi.'
						}
					} 
					$j.ambiance({message: ret.errorMessage, type: 'error', timeout: 10, fade: true});
				}
			});
		} else {
			$j.ambiance({
				message: 'Numer ' + barcode + ' zosta� ju� wprowadzony',
				type: 'error',
				timeout: 10, 
				fade: true
			});
		}
	}
});

$j(document).bind('keyup', function(event) {
	var str = String.fromCharCode(event.which);
	
	if($j('#barcode:focus').length == 0) {
		$j('#barcode').val(str);		
	}
	$j('#barcode').focus();
});

function confirmExit() {
	return 'Zamkni�cie tej strony spowoduje anulowanie odbioru przesy�ek';
}

//window.onbeforeunload = confirmExit;

window.onunload = function() {
	console.warn('sending...');
	$j.ajax('<ww:url value="'/office/incoming/document-reception.action'" />?rejectReception=true&login=<ww:property value="login"/>', {
		async: false,
		type: 'GET'
	});
}

</script>

<style>

.document-reception {
	/*table-layout: fixed;*/
}

.document-reception caption {
	font-size: 14px;
}

.document-reception .no-barcodes-present {
	line-height: 60px;
}

.document-reception tr:nth-child(even) td {
	background-color: #CACAE6;
}

#barcode-container, #lastname-container {
	margin-top: 20px;
	display: inline-block;
}

#lastname-container {
	margin-left: 20px;
}

</style>