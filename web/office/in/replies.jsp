<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N replies.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
				 java.util.Date,
				 org.apache.commons.beanutils.DynaBean,
				 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PismoPrzychodzace"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>


<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/incoming/replies.action'"/>" method="post" onsubmit="disableFormSubmits(this);" name="formularz">
<ds:available test="layout2">
			<div id="middleContainer"><!--  BIG TABLE start -->
</ds:available>
<ww:if test="!replyUnnecessary">
	<table>
		<ww:if test="outDocuments.empty">
			<tr>
				<td>
					<p><ds:lang text="NaToPismoNieUdzielonoOdpowiedzi"/>.</p>
					<hr class="highlightedText" style="width:110; height:1; size:1;" align="left"/>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td>
			<ww:if test="canCreateOutgoing && canUpdate">
				<ww:if test="outDocuments.empty">
					<ds:lang text="AbyUtworzycOdp"/>
				</ww:if>
				<ww:else>
					<ds:lang text="AbyUtworzycKolejnaOdp"/>
				</ww:else>
				<a href="<ww:url value="'/office/outgoing/new.action'">
				<ww:param name="'inDocumentId'" value="documentId"/>
				<ww:param name="'inActivity'" value="activity"/></ww:url>" name='newReply'>
				<ds:lang text="KliknijTutaj"/></a>
			</ww:if>
			</td>
		</tr>
	</table>
</ww:if>

<ww:if test="canUpdate">

	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'activity'" value="activity"/>

	<table>
		<tr>
			<td>
				<ww:if test="replyBOK">
					<ds:lang text="PismoOczekujeWBOK"/>
					<a href="javascript:odpowiedzBOKEnd()">
					<ds:lang text="KliknijTutaj"/></a>
				</ww:if>
				<ww:elseif test="replyUnnecessary">
					<ds:lang text="PismoNieWymagaOd.."/>
					<ds:lang text="AbyToZmienic"/>
					<a href="javascript:odpowiedz2()">
					<ds:lang text="KliknijTutaj"/></a>
				</ww:elseif>
				<ww:else>
					<ds:lang text="PismoWymagaOd.."/>
					<ds:lang text="AbyToZmienic"/>
					<a href="javascript:odpowiedz1()">
					<ds:lang text="KliknijTutaj"/></a>
					<ds:additions test="waitingReplies">
						<br/><ds:lang text="JesliOdpowiedzOczekujeNaAdresata"/>
						<a href="javascript:odpowiedzBOK()">
						<ds:lang text="KliknijTutaj"/></a>
					</ds:additions>
				</ww:else>

				<div style="display:none">
					<ww:checkbox id="checkboxik" name="'replyUnnecessary'" fieldValue="true" value="replyUnnecessary"/>
					<ww:checkbox id="replyBOK" name="'replyBOK'" fieldValue="true" value="replyBOK"/>
					<input name="doUpdate" id="doUpdate"/>
				</div>
			</td>
		</tr>
	</table>

</ww:if>

<ww:if test="!outDocuments.empty">
	<hr class="highlightedText" style="width:110; height:1; size:1;" align="left"/>	
	
	<table width="100%">
		<tr>
			<td colspan="4">
					<ds:lang text="AktualneOdpowiedzi"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Id"/>
			</td>
			<td>
				<ds:lang text="NumerKO"/>
			</td>
			<td>
				<ds:lang text="DataDokumentu"/>
			</td>
			<td>
				<ds:lang text="Nadawca"/>
			</td>
			<td>
				<ds:lang text="GlownyOdbiorca"/>
			</td>
		</tr>
		<ww:iterator value="outDocuments">
			<tr>
				<td>
					<a href="<ww:url value="'/office/outgoing/summary.action?documentId='+id"/>"><ww:property value="id"/></a>
				</td>
				<td>
					<a href="<ww:url value="'/office/outgoing/summary.action?documentId='+id"/>"><ww:property value="officeNumber"/></a>
				</td>
				<ww:if test="documentDate != null">
					<ww:set name="documentDate" scope="page" value="documentDate"/>
					<td>
						<a href="<ww:url value="'/office/outgoing/summary.action?documentId='+id"/>"><fmt:formatDate value="${documentDate}" type="both" pattern="dd-MM-yy"/></a>
					</td>
				</ww:if>
				<ww:else>
					<td></td>
				</ww:else>
				<td>
					<a href="<ww:url value="'/office/outgoing/summary.action?documentId='+id"/>"><ww:property value="sender.summary"/></a>
				</td>
				<ww:set name="recipient" value="recipients.{^ true }"/>
				<ww:if test="#recipient.empty">
					<td></td>
				</ww:if>
				<ww:else>
					<td><a href="<ww:url value="'/office/outgoing/summary.action?documentId='+id"/>"><ww:property value="#recipient[0].summary"/></a></td>
				</ww:else>
			</tr>
		</ww:iterator>
	</table>
</ww:if>
		<ds:available test="layout2">
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div>  <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script> 
var formularz = document.forms['formularz'];
function odpowiedz1()
{
	document.getElementById('checkboxik').checked='checked';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
	document.getElementById('ukryj_zalacznik').style.display=
		((document.getElementById('ukryj_zalacznik').style.display=="none")?true:false)?"":"none";
}
function odpowiedzBOK()
{
	document.getElementById('replyBOK').checked='checked';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
}
function odpowiedzBOKEnd()
{
	document.getElementById('replyBOK').checked='';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
}
function odpowiedz2()
{
	document.getElementById('checkboxik').checked='';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
	document.getElementById('ukryj_zalacznik').style.display=
			((document.getElementById('ukryj_zalacznik').style.display=="none")?true:false)?"":"none";
}
</script>