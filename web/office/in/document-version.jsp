<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
<ds:ww-action-errors/>
<ds:ww-action-messages/>
		
<form action="<ww:url value="'/office/incoming/document-version.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="documentVersion.empty">
    	<p><ds:lang text="Brak dokumentow"/></p>
	</ww:if>

	<ww:else>
		<table class="tableMargin">
			<tr>
				<td>Lp.</td>
				<td>Wersja</td>
				<td>Aktualna</td>
				<td>Data</td>
				<td>Opis</td>
			</tr>
			<ww:iterator value="documentVersion">

				<tr>
					<td><ww:property value="lp" /></td>
					<td><a href="<ww:url value="link"/>"><ww:property value="wersja" /></a></td>
					<ww:if test="czyAktywny">
					<td><ww:property value="aktywny" /></td>
					</ww:if>
					<ww:else>
					<td><ww:property value="aktywny" /><a href="<ww:url value="active"/>"> Aktywuj </a></td>
					</ww:else>
					<td><a href="<ww:url value="link"/>"><ww:property value="data" /></a></td>
					<td><a href="<ww:url value="link"/>"><ww:property value="opis" /></a></td>
				</tr>
			</ww:iterator>
		</table>
		<br />
		<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div>
			<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	</ww:else>
</form>	