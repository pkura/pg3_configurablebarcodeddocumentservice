<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ page import="java.text.SimpleDateFormat, java.util.Date, org.apache.commons.beanutils.DynaBean, pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<style type="text/css">
    select#jbpmProcessId {
        width: 22em;
    }
</style>

<h1><ds:lang text="ZarzadzanieUmowa" /></h1>

<hr size="1" align="left" class="highlightedText" width="77%" />

<ds:available test="!layout2">
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
    </ww:iterator>
</ds:available>

<ds:available test="layout2">
    <div id="middleMenuContainer">
        <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif"	class="tab2_img" /> 
        <ww:iterator value="tabs" status="status">
            <a href="<ww:url value='link'/>" title="<ww:property value='title'/>"
               <ww:if test="selected">class="highlightedText"</ww:if>>
                <ww:property value="name" /></a>
            <ww:if test="!#status.last">
                <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10" />
            </ww:if>
        </ww:iterator>
    </div>
</ds:available>

<p></p>

    <ds:available test="layout2">
        <div id="middleContainer"> <!--BIG TABLE start -->
    </ds:available> 

<form id="form" action="<ww:url value='baseLink'/>?documentId=<ww:property value="documentId"/>" 
      method="post" onsubmit="return disableFormSubmits(this);" class="dwr" style="float: left">
    <ww:hidden name="'contractManagment.id'" value="contractManagment.id" id="contractManagmentId" /> 
    <ww:hidden name="'id'" value="id" id="id" /> 
    <ww:hidden name="'offset'" value="offset"/>
    <ww:hidden name="'ascending'" value="ascending"/>
    <ww:hidden name="'sortField'" value="sortField"/>
    <input type="hidden" name="searchContractDate" size="10" maxlength="10" class="txt" id="searchContractDate"
                        value="<ds:format-date value="searchContractDate" pattern="dd-MM-yyyy"/>"/>
    <input type="hidden" name="searchReturnDate" size="10" maxlength="10" class="txt" id="searchReturnDate"
                        value="<ds:format-date value="searchReturnDate" pattern="dd-MM-yyyy"/>"/>
    <input type="hidden" name="searchNote" value="<ww:property value="searchNote"/>" id="searchNote"/>
    <input type="hidden" name="searchId" value="<ww:property value="searchUserId"/>" id="searchUserId"/>
    
    
    <input type="hidden" name="doRemove" id="doRemove"/>
    <input type="hidden" name="doSave" id="doSave"/>
    <input type="hidden" name="doSearch" id="doSearch"/>

    <ds:ww-action-errors />
    <ds:ww-action-messages />

    <table>
        <thead>
            <tr>
                <td>ID: <b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>">
                            <ww:property value="documentId" /></a></b></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><ds:lang text="Zamawiajacy" /><span class="star">*</span>:</td>
                <td>
                    <ww:select name="'contractManagment.userId'" list="users" 
                               id="contractManagmentUserId"
                               listKey="id" 
                               headerKey="''" 
                               headerValue="'----'"  
                               listValue="asFirstnameLastnameName()" 
                               cssClass="'sel'" value="contractManagment.userId"/>
                </td>
                <td rowspan="3" style="vertical-align:top"><ds:lang text="Uwagi"/>:</td>
                <td rowspan="3"><ww:textarea name="'contractManagment.note'" id="contractManagmentNote"
                                 value="contractManagment.note" rows="3" cols="47" cssClass="'txt'"/>
                </td>
        </tr>
        <tr>
            <td><ds:lang text="DataZamowienia"/>:</td>
            <td>
                <input name="contractDate" size="10" maxlength="10" class="txt" id="contractDate"
                        value="<ds:format-date value="contractManagment.contractDate" pattern="dd-MM-yyyy"/>"/>
            </td>
        </tr>
        <tr>
            <td><ds:lang text="DataZwrotu"/>:</td>
            <td> 
                <input name="returnDate" size="10" maxlength="10" class="txt" id="returnDate"
                        value="<ds:format-date value="contractManagment.returnDate" pattern="dd-MM-yyyy"/>"/>
             </td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="button" name="saveButton" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
					   onclick="if(!validateForm())return false;$j('#doSave').attr('value',true);$j('#form').submit()" />
                <input type="button" name="searchButton" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"
					   onclick="$j('#doSearch').attr('value',true);$j('#form').submit()" />
                <ww:if test="contractManagment.id != null">                          
                       <input type="button" name="deleteButton" value="<ds:lang text="Usun"/>" class="btn cancelBtn"
                                           onclick="$j('#doRemove').attr('value',true);$j('#form').submit()" />
                </ww:if>
               <input type="button" name="newButton" value="<ds:lang text="Nowy"/>" class="btn newBtn"/>
            </td>
        </tr>
        </tbody>
    </table>
</form>

<form id="formTableBatch" style="float:left;width:100%" action="<ww:url value='baseLink'/>?documentId=<ww:property value="documentId"/>"
      method="post" onsubmit="return disableFormSubmits(this);">
    <ww:hidden name="'offset'" value="offset"/>
    <ww:hidden name="'ascending'" value="ascending"/>
    <ww:hidden name="'sortField'" value="sortField"/>
    <input type="hidden" name="doRemoveBatch" id="doRemoveBatch"/>
<table class="search" width="100%" cellspacing="0" id="mainTable">
    <thead>
        <tr>
            <th></th>
            <ww:iterator value="columns" status="status">
                <th>
                    <nobr>
                        <a href="<ww:url value="sortDesc"/>" class="nonDroppable" name="<ww:property value="property"/>_descending"
			   id="<ww:property value="property"/>_descending" alt="<ds:lang text="SortowanieMalejace"/>" 
                           title="<ds:lang text="SortowanieMalejace"/>">
			   <ww:if test="sortField == property && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
			   <ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/><ww:param name="'name'" value="ascending"/></a>
                                    <ww:property value="[0].title"/>
                        <a href="<ww:url value="sortAsc"/>" class="nonDroppable" name="<ww:property value="property"/>_ascending"
                           alt="<ds:lang text="SortowanieRosnace"/>" id="<ww:property value="property"/>_ascending"
                           title="<ds:lang text="SortowanieRosnace"/>">
                           <ww:if test="sortField == property && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
                           <ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>

                    </nobr>
                </th>
            </ww:iterator>
        </tr>
    </thead>
    <tbody>
    <ww:iterator value="contracts" status="status">
        <tr <ww:if test="#status.odd != true">class="oddRows"</ww:if>>
        <td>
            <ww:checkbox name="'activityIds'" id="check" fieldValue="[0].id" />
        </td>
        <td>
            <a href="<ww:url value="contract-managment.action">
               <ww:param name="'id'" value="[0].id"/>
                <ww:param name="'documentId'" value="documentId"/>
                </ww:url>">
                <ww:if test="contractManagment.id == [0].id">
                    <b><ww:property value="[0].id"/></b>
                </ww:if>
                <ww:else>
                    <ww:property value="[0].id"/>
                </ww:else>
            </a>
        </td>
        <td>
            <a href="<ww:url value="contract-managment.action">
               <ww:param name="'id'" value="[0].id"/>
                <ww:param name="'documentId'" value="documentId"/>
                </ww:url>">
                <ww:if test="contractManagment.id == [0].id">
                    <b><ww:property value="[0].userName"/></b>
                </ww:if>    
                <ww:else>
                    <ww:property value="[0].userName"/>
                </ww:else>    
            </a>
        </td>
        <td>
            <a href="<ww:url value="contract-managment.action">
               <ww:param name="'id'" value="[0].id"/>
                <ww:param name="'documentId'" value="documentId"/>
                </ww:url>">
                <ww:if test="contractManagment.id == [0].id">
                    <b><ds:format-date value="[0].contractDate" pattern="dd-MM-yyyy"/></b>
                </ww:if>    
                <ww:else>
                    <ds:format-date value="[0].contractDate" pattern="dd-MM-yyyy"/>
                </ww:else>
            </a>
        </td>
        <td>
            <a href="<ww:url value="contract-managment.action">
               <ww:param name="'id'" value="[0].id"/>
                <ww:param name="'documentId'" value="documentId"/>
                </ww:url>">
                <ww:if test="contractManagment.id == [0].id">
                    <b><ds:format-date value="[0].returnDate" pattern="dd-MM-yyyy"/></b>
                </ww:if>   
                <ww:else>
                    <ds:format-date value="[0].returnDate" pattern="dd-MM-yyyy"/>
                </ww:else>
            </a>
        </td>
        <td>
            <a href="<ww:url value="contract-managment.action">
               <ww:param name="'id'" value="[0].id"/>
                <ww:param name="'documentId'" value="documentId"/>
                </ww:url>">
                <ww:if test="contractManagment.id == [0].id">
                    <b><ww:property value="[0].note"/></b>
                </ww:if>
                <ww:else>
                    <ww:property value="[0].note"/>
                </ww:else>
            </a>
        </td>
        </tr>
    </ww:iterator>
</tbody>
</table>
<div>
    <ww:if test="contracts != null">
            <select id="action" name="action" class="sel">
                    <option value="">
                            <ds:lang text="wybierzAkcje"/>
                    </option>
                    <option value="removeBatch">
                        <ds:lang text="UsunZaznaczone"/>
                    </option>
            </select>
            <input type="button" class="btn" value="<ds:lang text="WykonajNaZaznaczonych"/>"
                    onclick="selectAction(document.getElementById('action').value);$j('#formTableBatch').submit()"/>
    </ww:if>
</form>
     <ds:available test="layout2">
        <!--	 Zeby kreska na dole nie byla tuz przy przyciskach -->
        <div class="bigTableBottomSpacer">&nbsp;</div>
        </div>  <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form--> 
    </ds:available>

    <ww:set name="pager" scope="request" value="pager"/>
    <ww:property value="#pager.linkCount"/>
    <table width="100%">
            <tr>
                <td align="center">
                        <jsp:include page="/pager-links-include.jsp"/>
                </td>
            </tr>
    </table>


<script type="text/javascript">
    $j(function(){
        $id = $j('#contractManagmentId');
        $userId = $j('#contractManagmentUserId');
        $contractDate = $j('#contractDate');
        $returnDate = $j('#returnDate');
        $note = $j('#contractManagmentNote');
    
        Calendar.setup({
            inputField:	"contractDate",	 // id of the input field
            ifFormat  :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
            button    :	"contractDateTrigger",  // trigger for the calendar (button ID)
            align     :	"Tl",		   // alignment (defaults to "Bl")
            singleClick	: true
        });
        Calendar.setup({
            inputField	:"returnDate",	 // id of the input field
            ifFormat	:"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
            button      :"returnDateTrigger",  // trigger for the calendar (button ID)
            align	: "Tl",		   // alignment (defaults to "Bl")
            singleClick	: true
        });
        $j('.newBtn').click(function(){
            $id.val(null);
            $userId.val(null);
            $contractDate.val(null);
            $returnDate.val(null);
            $note.val(null);
            jQuery(".cancelBtn").css('display','none');
        });
    });

    function validateForm(){
        $alert = '';
        $ok = true;
        if($userId.val() == '' || $userId.val() == null ){
            $alert += '*<ds:lang text="WybierzUzytkownika"/>\n';
            $ok = false;
        }
        if($contractDate.val() == '' || $contractDate.val() == null ){
            $alert +='\n*<ds:lang text="NiepoprawnaDataZamowienia"/>\n';
            $ok = false;
        }
//            $alert +='\n*<ds:lang text="NiepoprawnaDataZwrotu"/>\n';
//            $ok = false;
//        }
        
    if($returnDate.val() != '' || $returnDate.val() != null )
        if(createDateObject($returnDate.val()) < createDateObject($contractDate.val())){
            $alert +='\n*<ds:lang text="DataZwrotuMusiBycWieksza"/>\n';
            $ok = false;
        }
        
        if(!$ok)
            alert($alert);
        
        return $ok;
    }
    
    
    function createDateObject(string){
        try{
            $dateArray = string.split('-', 3);
            return new Date($dateArray[2], $dateArray[1], $dateArray[0]);
        }catch(e){
            return new Date();
        }
    }
    function selectAction($action) {
        
        switch($action){
            case 'removeBatch' : 
                default:
                    $j("#doRemoveBatch").val(true);
        }
        
    }
</script>