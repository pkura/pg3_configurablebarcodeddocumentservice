<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N bok.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style type="text/css">
	select#senderCountry {width:24em;}
</style>

<ww:if test="documentType == 'in'">
    <h1><ds:lang text="PismoPrzychodzace"/></h1>
</ww:if>
<ww:elseif test="documentType == 'out'">
    <h1><ds:lang text="PismoWychodzace"/></h1>
</ww:elseif>
<ww:else>
    <h1><ds:lang text="Pismo"/></h1> 
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ww:set name="exists" value="documentId != null"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">

// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
// jako anonimowego
var senderFields = new Array(
    'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
    'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
    'senderStreetAddress');

function disableEnableSender(checkbox)
{

    for (var i=0; i < senderFields.length; i++)
    {
        document.getElementById(senderFields[i]).disabled = checkbox.checked;
        //document.getElementById(senderFields[i]).style.background = checkbox.checked ? '#A5967C' : '#BCB09D';
    }
}

// funkcja wywo�ywana przez okienko z people-directory-results.jsp
function __people_directory_pick(addresseeType, title, firstname, lastname,
    organization, organizationDivision, street, zip, location, country, email, fax)
{
    if (addresseeType == 'sender')
    {
        document.getElementById('senderTitle').value = title;
        document.getElementById('senderFirstname').value = firstname;
        document.getElementById('senderLastname').value = lastname;
        document.getElementById('senderOrganization').value = organization;
        document.getElementById('senderStreetAddress').value = street;
        document.getElementById('senderZip').value = zip;
        document.getElementById('senderLocation').value = location;
        document.getElementById('senderCountry').value = country;
        document.getElementById('senderEmail').value = email;
        document.getElementById('senderFax').value = fax;
    }
    else if (addresseeType == 'recipient')
    {
        document.getElementById('recipientTitle').value = title;
        document.getElementById('recipientFirstname').value = firstname;
        document.getElementById('recipientLastname').value = lastname;
        document.getElementById('recipientOrganization').value = organization;
        document.getElementById('recipientDivision').value = organizationDivision;
        document.getElementById('recipientStreetAddress').value = street;
        document.getElementById('recipientZip').value = zip;
        document.getElementById('recipientLocation').value = location;
        //document.getElementById('recipientCountry').value = country;
        document.getElementById('recipientEmail').value = email;
        document.getElementById('recipientFax').value = fax;
    }
}

// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
    'streetAddress', 'zip', 'location', 'country', 'email', 'fax');

// lparam - identyfikator
function openPersonPopup(lparam, dictionaryType)
{
    if (lparam == 'sender')
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+
            '&title='+document.getElementById('senderTitle').value+
            '&firstname='+document.getElementById('senderFirstname').value+
            '&lastname='+document.getElementById('senderLastname').value+
            '&organization='+document.getElementById('senderOrganization').value+
            '&street='+document.getElementById('senderStreetAddress').value+
            '&zip='+document.getElementById('senderZip').value+
            '&location='+document.getElementById('senderLocation').value+
            '&country='+document.getElementById('senderCountry').value+
            '&email='+document.getElementById('senderEmail').value+
            '&fax='+document.getElementById('senderFax').value+
            '&canAddAndSubmit=true');
    }
    else
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true');
    }
}

// lparam - identyfikator
// wparam - pozycja na li�cie
function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
{
    //alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
    var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
    //alert('url='+url);
    openToolWindow(url);
}

function personSummary(map)
{
    var s = '';
    if (map.title != null && map.title.length > 0)
        s += map.title;

    if (map.firstname != null && map.firstname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.firstname;
    }

    if (map.lastname != null && map.lastname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.lastname;
    }

    if (map.organization != null && map.organization.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.organization;
    }

    if (map.street != null && map.street.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.street;
    }

    if (map.location != null && map.location.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.location;
    }

    return s;
}

function __accept_person_duplicate(map, lparam, wparam)
{
    if (lparam == 'recipient')
    {
        var sel = document.getElementById('odbiorcy');

        for (var i=0; i < sel.options.length; i++)
        {
            eval('var smap='+sel.options[i].value);
            if (compareArrays(smap, map, true))
            {
                //alert('Istnieje ju� taki odbiorca');
                return true;
            }
        }
    }

    return false;
}

/*
    Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
    //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

    if (lparam == 'recipient')
    {
        var sel = document.getElementById('odbiorcy');

        if (wparam != null && (''+wparam).length > 0)
        {
            setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
        }
        else
        {
            addOption(sel, JSONStringify(map), personSummary(map));
        }
    }
    else if (lparam == 'sender')
    {
        document.getElementById('senderTitle').value = map.title;
        document.getElementById('senderFirstname').value = map.firstname;
        document.getElementById('senderLastname').value = map.lastname;
        document.getElementById('senderOrganization').value = map.organization;
        document.getElementById('senderStreetAddress').value = map.street;
        document.getElementById('senderZip').value = map.zip;
        document.getElementById('senderLocation').value = map.location;
        document.getElementById('senderCountry').value = map.country;
        document.getElementById('senderEmail').value = map.email;
        document.getElementById('senderFax').value = map.fax;
    }
}

</script>

<ww:if test="#exists">
    <p>
        <ww:set name="incomingDate" value="document.incomingDate" scope="page"/>
        <ds:lang text="PismoPrzyjetePrzezUzytkownika"/>: <b><ww:property value="creatingUser"/></b>
            <ds:lang text="dnia"/> <b><fmt:formatDate value="${incomingDate}" type="both" pattern="dd-MM-yy HH:mm"/></b><br/>
        <ds:lang text="Dzial"/>: <b><ww:property value="assignedDivision"/></b> <br/>
        <ds:extras test="!business">
	        <ds:modules test="office or simpleoffice">
	            <ds:lang text="NumerWBIP"/>: <b><ww:property value="document.trackingNumber"/></b> <br/>
	            <ds:lang text="HasloWBIP"/>: <b><ww:property value="document.trackingPassword"/></b> <br/>
	        </ds:modules>
        </ds:extras>
        <ds:lang text="NumerKancelaryjny"/>: <b><ww:property value="document.formattedOfficeNumber"/></b> <br/>
        <ds:modules test="office or simpleoffice">
            <ds:lang text="NumerWsprawie"/>: <b><ww:property value="caseDocumentId"/></b> <br/>
        </ds:modules>
        <ww:iterator value="journalMessages">
            <ds:lang text="NumerWdziale"/> '<ww:property value='key'/>': <b><ww:property value='value'/></b> <br/>
        </ww:iterator>
        <ww:if test="document.associatedDocuments.size > 0">
            <ds:lang text="DokumentyPowiazane"/>:
            <ww:iterator value="document.associatedDocuments" status="status" >
                <a target="_blank" href="<ww:url value="'/office/incoming/summary.action'"><ww:param name="'documentId'" value="id"/></ww:url>"><ww:property value="formattedOfficeNumber"/> (<ww:property value="caseDocumentId"/>)</a>
                <ww:if test="!#status.last">, </ww:if>
            </ww:iterator>
        </ww:if>
        <ww:if test="document.masterDocument != null">
            <ww:push value="document.masterDocument">
                <ds:lang text="DokumentGlowny"/>: <a target="_blank" href="<ww:url value="'/office/incoming/summary.action'"><ww:param name="'documentId'" value="id"/></ww:url>"><ww:property value="formattedOfficeNumber"/> (<ww:property value="caseDocumentId"/>)</a> <br/>
            </ww:push>
        </ww:if>
    </p>
</ww:if>

<%-- pole ustawiane tylko w FillNewDocumentForm, wi�c nie trzeba sprawdza�,
    czy pismo ju� istnieje --%>
<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>

<form action="<ww:url value='&apos;/office/incoming/bok.action&apos;'/>" method="post" onsubmit="selectAllOptions(document.getElementById('odbiorcy')); return disableFormSubmits(this);">

    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="doCreateOutgoing" id="doCreateOutgoing"/>
    <input type="hidden" name="doUpdate" id="doUpdate"/>
    <input type="hidden" name="doCancelDocument" id="doCancelDocument"/>
    <ww:hidden name="'acceptDuplicateReferenceId'" value="acceptDuplicateReferenceId"/>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:token />


<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td><ds:lang text="DataPisma"/>:</td>
    <td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
    </td>
</tr>
<tr>
    <td><ds:lang text="OpisPisma"/><span class="star">*</span>:</td>
    <td><ww:textarea name="'description'" rows="2" cols="47" cssClass="'txt'" id="description"/></td>
</tr>
<%--
<tr>
    <td>Kod kreskowy:</td>
    <td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" id="barcode"/></td>
</tr>
--%>
<ds:extras test="!business">
	<ds:modules test="office or simpleoffice">
	    <ww:if test="bipEnabled">
	        <tr>
	            <td><ds:lang text="WysylajDoBIP"/>:</td>
	            <ww:if test="canExportToBip">
	                <td><ww:checkbox name="'submitToAsiBip'" fieldValue="true" value="submitToAsiBip"/></td>
	            </ww:if>
	            <ww:else>
	                <td><ww:checkbox name="'submitToAsiBip'" fieldValue="true" value="submitToAsiBip" disabled="true" /></td>
	            </ww:else>
	        </tr>
	    </ww:if>
	</ds:modules>
</ds:extras>

<tr>
    <td colspan="2"><br/><h4><ds:lang text="Nadawca"/></h4></td>

</tr>
<tr>
    <td><ds:lang text="Anonim"/>:</td>
    <td><ww:checkbox name="'senderAnonymous'" fieldValue="'true'" value="senderAnonymous" onchange="'disableEnableSender(this);'" id="senderAnonymous" />
</tr>
<!-- readonly="#exists" -->
<%--cssClass="#exists ? 'txt_ro' : 'txt'"--%>
<tr>
    <td><ds:lang text="Tytul/stanowisko"/>:</td>
    <td><ww:textfield name="'senderTitle'" size="30" maxlength="30" cssClass="'txt'" id="senderTitle"  /></td>
</tr>
<tr>
    <td><ds:lang text="Imie"/>:</td>
    <td><ww:textfield name="'senderFirstname'" size="50" maxlength="50" cssClass="'txt'" id="senderFirstname"  /></td>
</tr>
<tr>
    <td><ds:lang text="Nazwisko"/>:</td>
    <td><ww:textfield name="'senderLastname'" size="50" maxlength="50" cssClass="'txt'" id="senderLastname"  /></td>
</tr>
<tr>
    <td><ds:lang text="Firma/Urzad"/>:</td>
    <td><ww:textfield name="'senderOrganization'" size="50" maxlength="50" cssClass="'txt'" id="senderOrganization"  /></td>
</tr>
<tr>
    <td><ds:lang text="Ulica"/>:</td>
    <td><ww:textfield name="'senderStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="senderStreetAddress"  /></td>
</tr>
<tr>
    <td valign="top"><ds:lang text="KodPocztowyImiejscowosc"/>:</td>
    <td><ww:textfield name="'senderZip'" size="7" maxlength="14" cssClass="'txt'" id="senderZip"  />
        <ww:textfield name="'senderLocation'" size="38" maxlength="50" cssClass="'txt'" id="senderLocation"  />
        <br/><span class="warning" id="senderZipError"> </span></td>
</tr>
<tr>
    <td><ds:lang text="Kraj"/>:</td>
    <td><ww:select name="'senderCountry'"
        list="@pl.compan.docusafe.util.Countries@COUNTRIES"
        listKey="alpha2" listValue="name" value="'PL'"
        id="senderCountry" cssClass="'sel'" onchange="this.form.senderZip.validator.revalidate();"
        /></td>
    <td></td>
</tr>
<tr>
    <td><ds:lang text="Email"/>:</td>
    <td><ww:textfield name="'senderEmail'" size="50" maxlength="50" cssClass="'txt'" id="senderEmail"  /></td>
</tr>
<tr>
    <td><ds:lang text="Faks"/>:</td>
    <td><ww:textfield name="'senderFax'" size="30" maxlength="30" cssClass="'txt'" id="senderFax"  /></td>
</tr>
<tr>
    <td>
    </td>
    <td><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
</tr>

<tr>
    <td colspan="2"><br/><h4><ds:lang text="Odbiorcy"/></td>
</tr>
<tr>
    <td colspan="2">
        <ww:select name="'recipients'" id="odbiorcy" cssStyle="'width: 500px;'" multiple="true" size="5" cssClass="'multi_sel'"
            list="recipientsMap" />
        <br/>
        <script type="text/javascript">					
					$j(document).ready(function(event) {
						var sel_el = $j('#odbiorcy');
						var minSize = 5;
						var maxSize = 15;
						
					    sel_el.data("size", $j("#odbiorcy option").size());
					    if(sel_el.children().length > minSize && sel_el.children().length < maxSize)
						    sel_el.attr('size', sel_el.children().length); 

					    $j("#odbiorcy option").livequery(function() {
					        if ($j("#odbiorcy option").size() > sel_el.data("size") && sel_el.data("size") >= minSize) {
					  			sel_el.attr('size', $j("#odbiorcy option").size());
								//alert('dodaje odbiorce: size=' + sel_el.data("size"));
					  			if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
					  		}
					
					        sel_el.data("size", $j("#odbiorcy option").size());
					    }, function() {
							  if ($j("#odbiorcy option").size() < sel_el.data("size")) {
								  sel_el.attr('size', sel_el.data("size")-1);	
								  if(sel_el.data("size") <= minSize)
							  			sel_el.attr('size', minSize);
									
								if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
							  }
					        sel_el.data("size", $j("#odbiorcy option").size());
						 });				    
					});
				</script>

        <input type="button" value="<ds:lang text="DodajOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" >
        <input type="button" value="<ds:lang text="EdycjaOdbiorcy"/>" onclick="var odb=document.getElementById('odbiorcy'); openEditPersonPopup(odb.options[odb.options.selectedIndex].value, odb.options.selectedIndex, 'recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn">
        <input type="button" value="<ds:lang text="UsunOdbiorcow"/>" onclick="deleteSelectedOptions(document.getElementById('odbiorcy'));" class="btn">
    </td>
</tr>

<tr>
    <td colspan="2"><br/><h4><ds:lang text="Inne"/></h4></td>
</tr>

<tr>
    <td valign="top"><ds:lang text="RodzajPisma"/><span class="star">*</span>:</td>
    <td><ww:select name="'kindId'" list="kinds" listKey="id" id="kindId"
            listValue="name + ' ('+getText('Bok.Dni')+': ' + days + ')'" cssClass="'sel'"
            value="#exists ? kind.id : kindId" disabled="#exists"
            headerKey="''" headerValue="getText('Bok.Brak')" />
        <p></p>
    </td>
</tr>
<tr>
    <td><ds:lang text="SposobOdbioru"/>:</td>
    <td><ww:select name="'outgoingDeliveryId'" list="outgoingDeliveries" listKey="id" listValue="name"
        value="#exists ? outgoingDelivery.id : outgoingDeliveryId" id="outgoingDelivery" cssClass="'sel'"/></td>
</tr>
<tr>
    <td><ds:lang text="InneUwagi"/>:</td>
    <td><ww:textarea name="'otherRemarks'" id="otherRemarks" rows='4' cols="47" cssClass="'txt'"/></td>
</tr>

<%--
<tr>
    <td>Liczba za��cznik�w:</td>
    <td><ww:textfield name="'numAttachments'" size="5" cssClass="'txt'" /></td>
</tr>
--%>

<tr>
    <td></td>
    <td>
        <ww:if test="!#exists">
            <input type="submit" name="doCreate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
            onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';" />
        </ww:if>
        <ww:else>
            <input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
                onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value = 'true';"/>
            <input type="button" value="<ds:lang text="PotwierdzeniePrzyjecia"/>" class="btn"
                onclick="openToolWindow('<ww:url value="'/office/incoming/print-receipt.action'"><ww:param name="'documentId'" value="document.id"/></ww:url>');"/>
        </ww:else>
    </td>
</tr>
</table>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

</form>

<script type="text/javascript">
    disableEnableSender(document.getElementById('senderAnonymous'));

    Calendar.setup({
        inputField     :    "documentDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

var szv = new Validator(document.getElementById('senderZip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('senderZipError').innerHTML = '';
}

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001)';
}

szv.canValidate = function() {
    var country = document.getElementById('senderCountry');
    var countryCode = country.options[country.options.selectedIndex].value;

    return (countryCode == '' || countryCode == 'PL');
}


// pocz�tkowa walidacja danych
szv.revalidate();

function validateForm()
{
    var description = document.getElementById('description').value;
    var kindId = document.getElementById('kindId').value;
    var otherRemarks = document.getElementById('otherRemarks').value;

    if (description.trim().length == 0) { alert('<ds:lang text="NiePodanoOpisuPisma"/>'); return false; }
    if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNajwyzej80Znakow"/>'); return false; }
    if (kindId.trim().length == 0) { alert('<ds:lang text="NieWybranoRodzajuPisma"/>'); return false; }
    if (otherRemarks != null && otherRemarks.length > 240) { alert('<ds:lang text="WartoscPolaUwagiJestZbytDluga.PoleMozeMiecCoNajwyzej240Znakow"/>'); return false; }
    return true;
}

</script>


