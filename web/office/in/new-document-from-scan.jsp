<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PrzyjmijPismo"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/office/incoming/new-scan.action'"/>" method="post" enctype="multipart/form-data">
	
	<ww:if test="BarcodePageFileName != null">
		<ww:hidden name="'TiffFileLocation'" id="TiffFileLocation"/>
		<ww:hidden name="'BarcodeRectangle'" id="BarcodeRectangle"/>
		<ww:hidden name="'assignToIndexing'" id="assignToIndexing"/>
		<script type="text/javascript">  
		window.onload = function () { 
			var ias = new MImageAreaSelection(el('barcodeImage')); 
			ias.onSelectEnd = function () 
			{ 				
				document.getElementById('BarcodeRectangle').value = ias.selectionX1+';'+ias.selectionY1+';'+ias.selectionWidth+';'+ias.selectionHeight;
			}; 
		}; 
		</script> 
		
		<table>
		<tr><td><img id="barcodeImage" src="<ww:property value="BarcodePageFileName"/>" /></td></tr>
		<tr><td><ds:event name="'doManualy'" value="getText('Zaladuj')" cssClass="'btn'"/></td></tr>
		</table>
	</ww:if>
	<ww:else>
		<table>
        <tr>
            <td><ds:lang text="Skan"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
		<tr>
            <td>Dekretuj do zaindeksowania:</td>
            <td><ww:checkbox name="'assignToIndexing'" fieldValue="true"/></td>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>        
    </table>
	</ww:else>
</form>