<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PrzyjmijPlik"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/office/incoming/load-attachment.action'"/>" method="post" enctype="multipart/form-data">
	
	<table>
        <tr>
            <td><ds:lang text="Skan"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
		<tr>
            <td></td>
            <td><ds:event name="'doAdd'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>  
	</table>
	
	<table>
		<tr>
			<th>ID</th>
			<th>Obraz</th>
			<th>Nazwa</th>
			<th>Data</th>
			<th></th>
		</tr>
		<ww:iterator value="atts">
			<tr>
			<td><ww:property value="id"/></td>
			<td>
				<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/><ww:param name="'id'" value="id"/></ww:url>', 'vs', 1000, 750);">
                &nbsp;&nbsp;
                    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="Wyswietl"/>">
                </a>
			</td>
			<td><ww:property value="name"/></td>
			<td><ww:property value="date"/></td>
				<td><a href="<ww:url value="'/office/incoming/dwr-document-main.action'"/>?attId=<ww:property value="id"/>&documentKindCn=fakt_koszt_ifpan"><ds:lang text="Zaewidencjonuj"/></a></td>
			</tr>
		</ww:iterator>		
	</table>	
</form>