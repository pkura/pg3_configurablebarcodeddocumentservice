<%@ page contentType="text/html; charset=iso-8859-2" pageEncoding="iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person,
                 pl.compan.docusafe.service.ServiceManager,
                 pl.compan.docusafe.service.bip.Bip,
                 pl.compan.docusafe.service.ServiceDriverNotSelectedException"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!-- document-main.jsp -->

<h1><ds:lang text="PrzyjmijPismo"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">

// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
// jako anonimowego
var senderFields = new Array(
    'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
    'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
    'senderStreetAddress');

function disableEnableSender(checkbox)
{

    for (var i=0; i < senderFields.length; i++)
    {
        document.getElementById(senderFields[i]).disabled = checkbox.checked;
        //document.getElementById(senderFields[i]).style.background = checkbox.checked ? '#A5967C' : '#BCB09D';
    }
}

// funkcja wywo�ywana przez okienko z people-directory-results.jsp
function __people_directory_pick(addresseeType, title, firstname, lastname,
    organization, organizationDivision, street, zip, location, country, email, fax)
{
    if (addresseeType == 'sender')
    {
        document.getElementById('senderTitle').value = title;
        document.getElementById('senderFirstname').value = firstname;
        document.getElementById('senderLastname').value = lastname;
        document.getElementById('senderOrganization').value = organization;
        document.getElementById('senderStreetAddress').value = street;
        document.getElementById('senderZip').value = zip;
        document.getElementById('senderLocation').value = location;
        document.getElementById('senderCountry').value = country;
        document.getElementById('senderEmail').value = email;
        document.getElementById('senderFax').value = fax;
    }
    else if (addresseeType == 'recipient')
    {
        document.getElementById('recipientTitle').value = title;
        document.getElementById('recipientFirstname').value = firstname;
        document.getElementById('recipientLastname').value = lastname;
        document.getElementById('recipientOrganization').value = organization;
        document.getElementById('recipientDivision').value = organizationDivision;
        document.getElementById('recipientStreetAddress').value = street;
        document.getElementById('recipientZip').value = zip;
        document.getElementById('recipientLocation').value = location;
        //document.getElementById('recipientCountry').value = country;
        document.getElementById('recipientEmail').value = email;
        document.getElementById('recipientFax').value = fax;
    }
}

// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
    'streetAddress', 'zip', 'location', 'country', 'email', 'fax');

// lparam - identyfikator
function openPersonPopup(lparam, dictionaryType)
{
    if (lparam == 'sender')
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+
            '&title='+document.getElementById('senderTitle').value+
            '&firstname='+document.getElementById('senderFirstname').value+
            '&lastname='+document.getElementById('senderLastname').value+
            '&organization='+document.getElementById('senderOrganization').value+
            '&street='+document.getElementById('senderStreetAddress').value+
            '&zip='+document.getElementById('senderZip').value+
            '&location='+document.getElementById('senderLocation').value+
            '&country='+document.getElementById('senderCountry').value+
            '&email='+document.getElementById('senderEmail').value+
            '&fax='+document.getElementById('senderFax').value+
            '&canAddAndSubmit=true', 'person', 700,650);
    }
    else
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true', 'person', 700,650);
    }
}

// lparam - identyfikator
// wparam - pozycja na li�cie
function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
{
    //alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
    var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
    //alert('url='+url);
    openToolWindow(url, 'person', 700,650);
}

function personSummary(map)
{
    var s = '';
    if (map.title != null && map.title.length > 0)
        s += map.title;

    if (map.firstname != null && map.firstname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.firstname;
    }

    if (map.lastname != null && map.lastname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.lastname;
    }

    if (map.organization != null && map.organization.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.organization;
    }
    if(map.organizationDivision !=null && map.organizationDivision.length>0)
    {
    	if (s.length > 0) s += ' / ';
        s += map.organizationDivision;
    }

    if (map.street != null && map.street.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.street;
    }

    if (map.location != null && map.location.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.location;
    }

    return s;
}

function __accept_person_duplicate(map, lparam, wparam)
{
    if (lparam == 'recipient')
    {
        var sel = document.getElementById('recipients');

        for (var i=0; i < sel.options.length; i++)
        {
            eval('var smap='+sel.options[i].value);
            if (compareArrays(smap, map, true))
            {
                //alert('Istnieje ju� taki odbiorca');
                return true;
            }
        }
    }

    return false;
}

/*
    Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
    trace('accept_person map=' + map + ', lparam=' + lparam + ', wparam=' + wparam);

    if (lparam == 'recipient')
    {
        var sel = document.getElementById('recipients');

        if (wparam != null && (''+wparam).length > 0)
        {
            setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
        }
        else
        {
            addOption(sel, JSONStringify(map), personSummary(map));
        }
    }
    else if (lparam == 'sender')
    {
        document.getElementById('senderTitle').value = map.title;
        document.getElementById('senderFirstname').value = map.firstname;
        document.getElementById('senderLastname').value = map.lastname;
        document.getElementById('senderOrganization').value = map.organization;
        document.getElementById('senderStreetAddress').value = map.street;
        document.getElementById('senderZip').value = map.zip;
        document.getElementById('senderLocation').value = map.location;
        document.getElementById('senderCountry').value = map.country;
        document.getElementById('senderEmail').value = map.email;
        document.getElementById('senderFax').value = map.fax;
    }
}

function openOther()
{
	var tabObj = document.getElementById('leftTab');
	var hrefObj = document.getElementById('openHref');

	tabObj.style.display = '';
	hrefObj.style.display = 'none';
}

function closeOther()
{
	var tabObj = document.getElementById('leftTab');
	var hrefObj = document.getElementById('openHref');

	tabObj.style.display = 'none';
	hrefObj.style.display = '';
}

</script>

<%-- pole ustawiane tylko w FillNewDocumentForm, wi�c nie trzeba sprawdza�,
    czy pismo ju� istnieje --%>
<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>

<form id="form"
      action="<ww:url value='&apos;/office/incoming/document-main.action&apos;'/>"
      method="post" enctype="multipart/form-data"
      onsubmit="selectAllOptions(document.getElementById('recipients')); return disableFormSubmits(this);">

<ww:hidden name="'multiToken'"/>
<ww:hidden name="'boxId'"/>
<ww:hidden name="'filestoken'" value="filestoken"/>
<input type="hidden" name="doCreate" id="doCreate"/>
<input type="hidden" name="createNextDocument" id="createNextDocument"/>
<input type="hidden" name="goToDocument" id="goToDocument"/>

<ds:available test="addImService">
	<ds:event name="'doImp'" value="'Dodaj z IM'"></ds:event>
</ds:available>


<ww:if test="documentKindCn == 'invoice-pte'">
	<a id="topSectionControl" href="#">Rozwi� sekcj� korespondencyjn�</a>
	<script type="text/javascript">
		$j('#topSectionControl').toggle(
				function() {
					$j('#leftTab').show();
					$j(this).text('Ukryj sekcj� korespondencyjn�');
					return false;					
				}, 
				function() {
					$j('#leftTab').hide();
					$j(this).text('Rozwi� sekcj� korespondencyjn�');
					return false;
				});
	</script>
</ww:if>
<table id="leftTab" border="0" <ww:if test="documentKindCn == 'invoice-pte'">style="display:none;"</ww:if> <ds:available test="!documentMain.officeDate" documentKindCn="documentKindCn">style="display:none;"</ds:available> class="formTable">
<tr>
    <td><ds:lang text="DataPisma"/>:</td>
    <td class="leftCell"><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
    </td>
</tr>


<tr>
    <td valign="top"><ds:lang text="OpisPisma"/><%--<span class="star">*</span>--%>:</td>
    <td class="leftCell"><ww:textarea cssStyle="'width: 100%;'" name="'summary'" rows="2" cols="50" cssClass="'txt'" id="summary"/></td>
</tr>

<tr>
    <td><ds:lang text="SposobDostarczenia"/>:</td>
    <td class="leftCell"><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
        headerKey="''" headerValue="getText('select.wybierz')"
        value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/></td>
</tr>
<ds:available test="documentMain.sender" documentKindCn="documentKindCn">
<tr class="formTableDisabled">
    <td colspan="2"><br/><h4><ds:lang text="Nadawca"/></h4></td>
</tr>
<!-- readonly="#exists" -->
<%--cssClass="#exists ? 'txt_ro' : 'txt'"--%>


<tr>
    <td width="200"  class="leftCell"><ds:lang text="Anonim"/>:</td>
    <td>
    	<ww:checkbox name="'senderAnonymous'" fieldValue="'true'" value="senderAnonymous" onchange="'disableEnableSender(this);'" id="senderAnonymous" />


    </td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Tytul/stanowisko"/>:</td>
    <td><ww:textfield name="'senderTitle'" size="50" maxlength="50" cssClass="'txt'" id="senderTitle"  cssStyle="'width: 200px;'"/></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Imie"/>:</td>
    <td><ww:textfield name="'senderFirstname'" size="50" maxlength="50" cssClass="'txt'" id="senderFirstname"  cssStyle="'width: 200px;'"/></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Nazwisko"/>:</td>
    <td><ww:textfield name="'senderLastname'" size="50" maxlength="50" cssClass="txt" id="senderLastname" cssStyle="'width: 200px'"/></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Firma/Urzad"/>:</td>
    <td><ww:textfield  name="'senderOrganization'" size="50" maxlength="50" cssClass="'txt'" id="senderOrganization" cssStyle="'width: 200px'"/></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Ulica"/>:</td>
    <td><ww:textfield name="'senderStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="senderStreetAddress" cssStyle="'width: 200px'"/></td>
</tr>
<tr>
    <td valign="top" class="leftCell"><ds:lang text="KodPocztowyImiejscowosc"/>:</td>
    <td><ww:textfield cssStyle="'width: 50px;'" name="'senderZip'" size="7" maxlength="14" cssClass="'txt'" id="senderZip"/>
        <ww:textfield cssStyle="'width: 142px;'" name="'senderLocation'" size="39" maxlength="50" cssClass="'txt'" id="senderLocation"  onchange="'showCode();'" />
        <br/><span class="warning" id="senderZipError"> </span></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Kraj"/>:</td>
    <td><ww:select name="'senderCountry'"
        list="@pl.compan.docusafe.util.Countries@COUNTRIES"
        listKey="alpha2" listValue="name" value="'PL'"
        id="senderCountry" cssClass="'sel'" cssStyle="'width: 206px;'" onchange="this.form.senderZip.validator.revalidate();"
        />
    </td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Email"/>:</td>
    <td><ww:textfield name="'senderEmail'" size="30" maxlength="30" cssClass="'txt'" id="senderEmail"  cssStyle="'width: 200px'"/></td>
</tr>
<tr>
    <td class="leftCell"><ds:lang text="Faks"/>:</td>
    <td><ww:textfield name="'senderFax'" size="30" maxlength="30" cssClass="'txt'" id="senderFax"  cssStyle="'width: 200px'"/></td>
</tr>
<tr class="formTableDisabled">
    <td>
    </td>
    <td><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" style="width: 204px" ></td>
</tr>
</ds:available>

<tr class="formTableDisabled">
    <td colspan="2"><br/><h4><ds:lang text="Odbiorcy"/></h4></td>
</tr>
<tr>
    <td colspan="2">
		<ww:select name="'recipients'" id="recipients" cssStyle="'width: 100%;'" multiple="true" size="5" cssClass="'multi_sel dontFixWidth'"
			list="recipientsMap"/>
		<br/>
		<script type="text/javascript">
					$j(document).ready(function(event) {
						var sel_el = $j('#recipients');
						var minSize = 5;
						var maxSize = 15;

					    sel_el.data("size", $j("#recipients option").size());
					    if(sel_el.children().length > minSize && sel_el.children().length < maxSize)
						    sel_el.attr('size', sel_el.children().length);

					    $j("#recipients option").livequery(function() {
					        if ($j("#recipients option").size() > sel_el.data("size") && sel_el.data("size") >= minSize) {
					  			sel_el.attr('size', $j("#recipients option").size());
								//alert('dodaje odbiorce: size=' + sel_el.data("size"));
					  			if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
					  		}

					        sel_el.data("size", $j("#recipients option").size());
					    }, function() {
							  if ($j("#recipients option").size() < sel_el.data("size")) {
								  sel_el.attr('size', sel_el.data("size")-1);
								  if(sel_el.data("size") <= minSize)
							  			sel_el.attr('size', minSize);

								if(sel_el.attr('size') > maxSize)
						  			sel_el.attr('size', maxSize);
							  }
					        sel_el.data("size", $j("#recipients option").size());
						 });
					});
				</script>
		<span class="btnContainer">
			<input style="width: 132px;" type="button" value="<ds:lang text="DodajOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" >
			<input style="width: 132px;" type="button" value="<ds:lang text="EdycjaOdbiorcy"/>" onclick="var odb=document.getElementById('recipients'); openEditPersonPopup(odb.options[odb.options.selectedIndex].value, odb.options.selectedIndex, 'recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn">
			<input style="width: 132px;" type="button" value="<ds:lang text="UsunOdbiorcow"/>" onclick="deleteSelectedOptions(document.getElementById('recipients'));" class="btn">
		</span>
    </td>
</tr>
<tr class="formTableDisabled">
    <td colspan="2"><br/><h4><ds:lang text="Inne"/></h4></td>
</tr>
<ds:available test="documentMain.postalRegNumber">
<tr>
    <td valign="top" class="leftCell"><ds:lang text="NrPrzesylkiRejestrowanej"/>(R):</td>
    <td><ww:textfield  cssStyle="'width: 200px'" name="'postalRegNumber'" size="20" maxlength="20" cssClass="'txt'" id="postalRegNumber" />
        <p></p>
    </td>
</tr>
</ds:available>
<ds:available test="documentMain.original">
<tr>
    <td valign="top" class="leftCell"><ds:lang text="Oryginal"/>:</td>
    <p></p>
    <td><ww:checkbox name="'original'"  fieldValue="true" id="original" value="true"></ww:checkbox>
        <p></p>
    </td>
</tr>
</ds:available>
</table>



<table border="0" class="formTable">
<tr>
    <td colspan="2">&nbsp;</td>
</tr>
<ww:if test="canChooseKind">
    <tr>
        <td valign="top" class="leftCell"><ds:lang text="RodzajPisma"/><span class="star">*</span>:</td>
        <td><ww:select name="'kindId'" list="kinds" listKey="id" id="kindId"
                listValue="name + ' (dni: ' + days + ')'" cssClass="'sel'"
                value="#exists ? kind.id : kindId" disabled="#exists && !canChangeKind"
                headerKey="''" headerValue="'-- brak --'" />
        </td>
    </tr>
</ww:if>
<ww:else>
    <tr>
        <td colspan="2"><ww:hidden name="'kindId'" id="kindId" value="kindId"/></td>
    </tr>
</ww:else>
</table>



<table border="0" class="formTable">
<tr class="formTableDisabled">
	<td id="openHref" <ds:available test="documentMain.officeDate"> style="display:none;"</ds:available>>
		<a href="javascript:openOther()">
		<ds:lang text="PokazDaneKancelarii"/></a>
	</td>
<tr>
<tr>
    <td width=189 class="leftCell">
        <ds:lang text="RodzajDokumentuDocusafe"/>:
    </td>
    <td>
        <ds:degradable-select cssStyle="'width: 214px;'" id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="cn" listValue="name" value="documentKindCn" cssClass="'sel'"
            onchange="''"/>
        
        <script type="text/javascript">
                $j(function(){
                    //pisma moga byc tylko w pismach wewnetrznych
                    $j('#documentKindCn [value=wf]').remove();
                    $j('#documentKindCn [value=absence-request]').remove();
                    $j('#documentKindCn [value=overtime_app]').remove();
                    $j('#documentKindCn [value=overtime_receive]').remove();
                    $j('#documentKindCn [value=wykaz]').remove();
                    $j('#documentKindCn [value=protokol_br]').remove();
                });
                
        	$j('#documentKindCn').change(function(){
                        documentKindVal = $j('#documentKindCn').val();
        		if(documentKindVal == 'absence-request' || documentKindVal == 'wf') {
					alert('To Pismo mo�e by� tylko Pismem Wewn�trznym!');
					return false;
        		}
				
                        selectAllRec(); 
                        changeDockind();
           	});
        </script>
    </td>
</tr>
<ww:if test="documentAspects != null">
		<tr>
			<td class="leftCell">
				<ds:lang text="AspektDokumentu"/>:
			</td>
			<td>
				<ww:select cssStyle="'width: 105px;'" id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
			</td>
		</tr>
</ww:if>
</table>


<table border="0" class="formTable">

<ww:if test="!normalKind">
    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="DaneArchiwum"/></h4></td>
    </tr>
</ww:if>

<jsp:include page="/common/dockind-fields.jsp"/>

<ww:if test="canAddToRS">
<tr>
    <td class="leftCell"><ds:lang text="NumerSzkody"/>:</td>
    <td><ww:textfield name="'NR_SZKODY'" id="NR_SZKODY" size="30" maxlength="30" cssClass="'txt"/></td>
</tr>
</ww:if>
	
	<ds:available test="documentMain.int.ArchiwizacjaWOtwartymPudle"  documentKindCn="documentKindCn">
	<tr>
	    <td class="leftCell"><ds:lang text="ArchiwizacjaWOtwartymPudle"/><ww:if test="boxId != null"> (<ww:property value="boxName"/>)</ww:if>:</td>
	    <td><ww:checkbox name="'addToBox'" fieldValue="true" disabled="boxId == null"/></td>
	</tr>
	</ds:available>

<ww:if test="loadFiles != null && loadFiles.size() > 0">
<tr>
	<td colspan="1">
		<ds:lang text="PlikiJuzobrane"/>:
	</td>
</tr>
</ww:if>


<ww:iterator value="loadFiles">
	<tr>
		<td colspan="1">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
		</td>
	</tr>
</ww:iterator>

<tr class="formTableDisabled">
	<td colspan="2">
	<table id="kontener">
		<tbody id="tabela_glowna">
			<tr>
				<td><ds:lang text="Plik" /><ds:available test="documentMain.check.attachment"><span class="star always">*</span></ds:available>:</td>
			</tr>
			<tr>
				<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
			</tr>
		</tbody>
	</table>
	</td>
</tr>
<tr class="formTableDisabled">
	<td><a href="javascript:addAtta();"><ds:lang
		text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
</tr>



<jsp:include page="/common/dockind-specific-additions.jsp"/>

<ww:if test="useScanner">
    <tr>
        <td><ds:lang text="Skanowanie"/></td>
        <td>
            <table style="border: 1px solid black; padding: 2px">
                <tr><td>
                    <script>
                    var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
                        request.getServerName() + ":" + request.getServerPort() %>';
                    // u�yty mo�e by� albo plik jtwain.jar znajduj�cy si� w aplikacji,
                    // albo plik systemowy (w JRE_HOME/lib/ext)
                    var object =  appletObject('scanapplet',
                        'pl.compan.docusafe.applets.scanner.AttachmentsApplet.class',
                        '<ww:url value="'/scanner.jar'"/>, <ww:if test="!useSystemJTwain"><ww:url value="'/jtwain-8.2.jar'"/>, </ww:if> <ww:url value="'/jai_core-1.1.2_01.jar'"/>, <ww:url value="'/jai_codec-1.1.2_01.jar'"/>', 700, 55,
                        { 'uploadUrl' : host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>',
                          'reloadPage' : 'false',
                          'multiToken' : '<ww:property value="multiToken"/>',
                          'readFileTokens': 'true' },
                        new Array('xMSIE'));
                    //alert('object='+object);
                    document.write(object);
                    </script>
                </td></tr>
            </table>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="PolaczZeskanowanePliki"/> (multitiff):</td>
        <td><ww:checkbox name="'multitiff'" fieldValue="true" value="multitiff"/></td>
    </tr>
</ww:if>
<ww:hidden name="'duplicateDocId'" value="duplicateDocId" id="duplicateDocId" />
<ww:if test="duplicate">
	<tr class="formTableDisabled">
		<td colspan="2">
			<ds:lang text="ListaDokumentowOtychSamychParametrach"/>
		</td>
	</tr>
		<ww:iterator value="duplicateList">
			<ww:set name="result"/>
			<tr>
				<td>
					<a href="javascript:openToolWindow('<ww:url value="'/repository/edit-simlpe-dockind-document.action'"><ww:param name="'id'" value="#result.id"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);"><ww:property value="id"/></a>
				</td>
			<tr>
		</ww:iterator>
	<tr>
    	<td>
        	<ds:submit-event name="'doCD'" cssStyle="'btn'" value="getText('ZapiszDuplikat')" />
    	</td>
	</tr>
</ww:if>
<ww:else>
	<tr class="formTableDisabled">
	    <td></td>


	    <td>
	        <input type="submit" name="doCreate1" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
	        onclick="$j('#form').unbind('submit');if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';" />
	        
	        <ww:if test="showCreateNext">
	        <input type="submit" name="doCreate2" value="<ds:lang text="ZapiszNext"/>" class="btn saveBtn"
	        onclick="$j('#form').unbind('submit');if (!validateForm()) return false; document.getElementById('doCreate').value = 'true'; document.getElementById('createNextDocument').value='true';" />
	        </ww:if>
	        <ds:available test="documentMain.ZapiszIprzejdzDoDokumentu">
	        	<input type="submit" name="doCreate3" value="<ds:lang text="ZapiszIprzejdzDoDokumentu"/>" class="btn saveBtn"
	       	 	onclick="$j('#form').unbind('submit');if (!validateForm()) return false; document.getElementById('doCreate').value = 'true'; document.getElementById('goToDocument').value='true';" />        
	    	</ds:available>
	    </td>

	</tr>
</ww:else>
</table>


<p><span class="star always">*</span><ds:lang text="PoleObowiazkowe"/></p>



</form>

<script type="text/javascript">
var countAtta = 1;
function addAtta()
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);

}

function delAtta()
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;

	/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}

        /*
    function addFileToken(token)
    {
        if (token == null || token.length == 0)
        {
            alert('Nie otrzymano nazwy pliku z appletu skanuj�cego')
            return false;
        }

        var f = document.createElement('input');
        f.setAttribute('type', 'hidden');
        f.setAttribute('name', 'tempFiles');
        f.setAttribute('value', token);
        document.getElementById('form').appendChild(f);
        hasTempFiles = true;

        // alert('dodano '+token);
    }
        */
	
    	if($j('#documentDate').length > 0)
    	{
		    Calendar.setup({
		        inputField     :    "documentDate",     // id of the input field
		        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		        button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
		        align          :    "Tl",           // alignment (defaults to "Bl")
		        singleClick    :    true
		    });
    	}
<%--
<ww:if test="useBarcodes">
var bi = new BarcodeInput({
    'prefix' : '<ww:property value="barcodePrefix"/>',
    'suffix' : '<ww:property value="barcodeSuffix"/>',
    'inputId' : 'barcode',
    'submit' : false,
    //'submitId' : 'send',
    'tabStarts' : false
});
</ww:if>
--%>

var szv = new Validator(document.getElementById('senderZip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('senderZipError').innerHTML = '';
    if (document.getElementById('senderZip').value != "")
    {
		showLocation();
	}
}

		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************ //
		//   |                  |			       |	   //
		//   |			        |			       |       //
		//   |			        |			       |       //
		//  \/			       \/			      \/       //

		var xmlHttp = null;
		var serverName = '<%= request.getServerName() + ":" %>';
		var port = '<%= request.getServerPort() %>';
		var contextPath = '<%= request.getContextPath() %>';
		var protocol = window.location.protocol;

		var polskieZnaki = new Array();
		var zamiana = new Array();

		function showCode()
		{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			var location=document.getElementById('senderLocation').value;

			//Rozwiazanie problemu z polskimi znakami
			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			for(i = 0; i < 18; i++)
			{
				while (location.indexOf(polskieZnaki[i])>-1)
				{
					pos= location.indexOf(polskieZnaki[i]);
					location = "" + (location.substring(0, pos) + zamiana[i] +
					location.substring((pos + polskieZnaki[i].length), location.length));
				}
			}

			if(location==null)
			{
				alert ("No element with id 'location' found on the page");
				return;
			}
			//alert("http://" + serverName + port + contextPath + "/code.ajx?location=" + location);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/code.ajx?location=" + location,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('senderZip');
			};

			xmlHttp.send(null);

		}

		function showLocation()
		{

			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			var zip=document.getElementById('senderZip').value;

			if(zip==null)
			{
				alert ("No element with id 'zip' found on the page");
				return;
			}

			//alert("http://" + serverName + port + contextPath + "/location.ajx?zip=" + zip);
			xmlHttp.open("GET",  protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('senderLocation');
			};

			xmlHttp.send(null);
		}

		function stateChanged(variable)
		{

			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";


			if (xmlHttp.readyState==4)
			{

				if(xmlHttp.status==200)
				{

					var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];

					if(valid.childNodes[0].nodeValue=='LocNotFound' && variable =='senderZip')
					{
						document.getElementById('senderZipError').innerHTML = "Nie znalaz�em kodu dla tej miejscowo�ci.";
						//document.getElementById(variable).value = '';
						//document.getElementById('senderLocation').value = '';
					}
					else
					{
						var message = valid.childNodes[0].nodeValue;
						for(i = 0; i < 18; i++)
						{
							while (message.indexOf(zamiana[i])>-1)
							{
								pos= message.indexOf(zamiana[i]);
								message = "" + (message.substring(0, pos) + polskieZnaki[i] +
								message.substring((pos + zamiana[i].length), message.length));
							}
						}
					}
					if(valid.childNodes[0].nodeValue=='CodeNotFound' && variable =='senderLocation')
					{
						document.getElementById('senderZipError').innerHTML = "Nie znalaz�em miejscowo�ci dla danego kodu."
						document.getElementById(variable).value='';
					}
					if(message.trim().length ==6)
					{
						document.getElementById(variable).value=message;
					}
					else if (message.trim().length >6 && variable=='senderZip')
					{
						document.getElementById('senderZipError').innerHTML = "Nie ma jednego kodu pocztowego dla danej miejscowo�ci.<br/>Zakres(y) to: " + message + ".";
						document.getElementById('senderZip').value=message.substring(0, 6);
					}
					else if(variable=='senderLocation' && valid.childNodes[0].nodeValue!='CodeNotFound')
					{
						document.getElementById(variable).value=message;
					}


				}
				else
				{
					alert("Error loading page"+ xmlHttp.status +":"+ xmlHttp.statusText);
				}
			}
			else  if (xmlHttp.readyState==0)
			{
				alert("	Object is uninitialized");
			}

		}

		function GetXmlHttpObject()
		{
			var xmlHttp=null;
			try
			{
			// Firefox, Opera 8.0+, Safari
				xmlHttp=new XMLHttpRequest();
			}
			catch (e)
			{
			//Internet Explorer
				try
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e)
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			return xmlHttp;
		}
		//*******************************//

		//   /\                          /\			      /\     //
		//   |			        |			       |     //
		//   |			        |			       |     //
		//   |		                 |			       |     //
		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************//

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp."/> 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('senderZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp."/> (00-001)';
}

szv.canValidate = function() {
    var country = document.getElementById('senderCountry');
    var countryCode = country.options[country.options.selectedIndex].value;

    return (countryCode == '' || countryCode == 'PL');
}


// pocz�tkowa walidacja danych
szv.revalidate();

function validateForm()
{
	    var description = document.getElementById('summary').value;
	    var kindSelect = document.getElementById('kindId');
	    var kindId = kindSelect.value;

	    //if (description.trim().length == 0) { alert('Nie podano opisu pisma'); return false; }
	    if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugiOpisMozeMiecCoNajwyzej80Znakow"/>'); return false; }
	    if (kindId.trim().length == 0) { alert('<ds:lang text="NieWybranoRodzajuPisma"/>'); return false; }

	    if (!validateDockind())
	        return false;
	    <ds:available test="documentMain.check.attachment">
	   		if (!validateFiles())
	            return false;
	   </ds:available>
    return true;
}

function selectAllRec()
{
	selectAllOptions('recipients');
}
</script>

<ww:if test="useBarcodePrinter">
    <script>
    var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
        request.getServerName() + ":" + request.getServerPort() %>';
    var object =  appletObject('barapplet', 'pl.compan.docusafe.applets.barprinter.BarcodePrinterApplet.class', '<ww:url value="'/barprinter.jar'"/>', 1, 1,
        { 'device': '<ww:property value="barcodePrinterDevice"/>',
          'printerType': 'Zebra_TLP2824',
          'labelWidth': '26mm',
          'labelHeight': '15mm' },
        new Array('xMSIE'));
    //alert('object='+object);
    document.write(object);
    </script>
</ww:if>


<script type="text/javascript">
	$j(document).ready(function() {
		var tables = $j('#form > table:visible');
		var maxLen = 0;

		tables.each(function() {
			//$j(this).children().children().children().filter('td.leftCell:first').css('border', '1px solid red');
			var len = $j(this).children().children().children().filter('td.leftCell:first').width();

			if(len > maxLen)
				maxLen = len;
		});

		tables.each(function() {
			$j(this).children().children().children().filter('td.leftCell:first').css('width', maxLen + 'px');
		});
	});
</script>

<%-- JE�LI DOCKIND == 'absence-request' WYPISUJE LISTE URLOP�W PRACOWNIKA / ROZWI�ZANIE TYMCZASOWE --%>
<jsp:include page="/common/employee-absences.jsp" />