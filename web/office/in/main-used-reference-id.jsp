<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main-used-reference-id.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Pismo przychodz�ce</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>Co najmniej jedno pismo o podanym znaku <i><ww:property value="referenceId"/></i>
zosta�o ju� przyj�te. Je�eli chcesz mimo to przyj�� takie pismo ponownie, kliknij
na przycisku <b>Powr��</b> by powr�ci� do przyjmowania pisma. </p>

<form action="<ww:url value='&apos;/office/incoming/main.action&apos;'/>" method="post" onsubmit="return disableFormSubmits(this);">
    <input type="hidden" name="doAcceptDuplicateReferenceId" id="doAcceptDuplicateReferenceId"/>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'acceptDuplicateReferenceId'" value="true"/>
    <ww:token />
    <ww:hidden name="'documentDate'" />
    <ww:hidden name="'stampDate'" />
    <ww:hidden name="'incomingDate'" />
    <ww:hidden name="'incomingTime'" />
    <ww:hidden name="'description'" />
    <ww:hidden name="'referenceId'" />
    <ww:hidden name="'submitToAsiBip'" />
    <ww:hidden name="'statusId'" />
    <ww:hidden name="'deliveryId'" />
    <ww:hidden name="'location'" />
    <ww:hidden name="'senderAnonymous'" />
    <ww:hidden name="'senderTitle'" />
    <ww:hidden name="'senderFirstname'" />
    <ww:hidden name="'senderLastname'" />
    <ww:hidden name="'senderOrganization'" />
    <ww:hidden name="'senderStreetAddress'" />
    <ww:hidden name="'senderZip'" />
    <ww:hidden name="'senderLocation'" />
    <ww:hidden name="'senderCountry'" />
    <ww:hidden name="'senderEmail'" />
    <ww:hidden name="'senderFax'" />
    <ww:hidden name="'postalRegNumber'" />
    <ww:hidden name="'kindId'" />
    <ww:hidden name="'packageRemarks'" />
    <ww:hidden name="'otherRemarks'" />
    <ww:hidden name="'numAttachments'" />
    <ww:hidden name="'instantAssignment'" />
    <ww:iterator value="recipientsMap">
        <ww:hidden name="'recipients'" value="key"/>
    </ww:iterator>

    <input type="submit" name="doAcceptDuplicateReferenceId" class="btn" value="Powr��"
        onclick="document.getElementById('doAcceptDuplicateReferenceId').value='true';"/>
</form>

<p>Znajduj�ce si� w systemie pisma o znaku <ww:property value="referenceId"/>:
<ul>
<ww:iterator value="existingDocuments">
    <li>
        <a href="<ww:url value="'/office/incoming/summary.action'"><ww:param name="'documentId'" value="id"/></ww:url>">
            <ww:property value="officeNumber"/> (<ww:property value="caseDocumentId"/>)
        </a>
    </li>
</ww:iterator>
</ul>