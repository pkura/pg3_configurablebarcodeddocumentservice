<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style type="text/css">
	select#jbpmProcessId {width:22em;}
</style>
<ww:hidden name="'openViewer'" id="openViewer"/>
<script>
openViewer();
function openViewer()
{
	var objViewer = document.getElementById('openViewer');
	if(objViewer.value == 'true')
	{
		<ww:if test="attachments.size > 0">
			openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevisionId"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);
		</ww:if>
		objViewer.value = false;
	}
}
</script>

<ww:if test="documentType == 'in'">
	<h1>
		<ds:lang text="PismoPrzychodzace"/>
	</h1>
</ww:if>

<ww:elseif test="documentType == 'out'">
	<h1>
		<ds:lang text="Pismo wychodz�ce"/>
	</h1>
</ww:elseif>

<ww:else>
	<h1>
		<ds:lang text="Pismo"/>
	</h1>
</ww:else>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status">
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>"<ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name" /></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10" />
	</ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status">
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>"<ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name" /></a>
		<ww:if test="!#status.last">
			<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10" />
		</ww:if>
	</ww:iterator>
</div>
</ds:available>



<p></p>

<form action="<ww:url value='&apos;/office/incoming/summary.action&apos;'/>"
	method="post"><ww:hidden name="'documentId'" /> 
	<ww:hidden name="'activity'" />
	<ww:hidden name="'hUwaga'" value="false" id="idUwaga" />
	<ww:hidden name="'hHistoria'" value="false" id="idHistoria" />
	<ww:hidden name="'hDekretacja'" value="false" id="idDekretacja" />
<ww:hidden name="'zgodnoscZumowa'" value="zgodnoscZumowa" id="dockind_POTWIERDZENIE_ZGODNOSCI"/>
    <ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>

    <ds:ww-action-errors />
    <ds:ww-action-messages />

<table class="summaryTable">
	<ww:if test="document != null and document.incomingDate != null">
		<ww:set name="incomingDate" value="document.incomingDate" scope="page" />
	</ww:if>

	<ww:if test="blocked">
		<tr>
			<td colspan="2">
				<span style="color: red">
					<ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
				</span>
			</td>
		</tr>
	</ww:if>

	<tr>
		<td width="30%" valign="top">
			<span id="id_strzalki1" style="display:">
				<a href="javascript:pokaz_ukryj()" alt="Poka� szczeg�y" title="Poka� szczeg�y"><img class="mini_circle" src="<ww:url value="'/img/circle_mini_down.gif'"/>" border="0"/></a>
			</span>
			<span id="id_strzalki2" style="display:none">
				<a href="javascript:pokaz_ukryj()" alt="Ukryj szczeg�y" title="Ukryj szczeg�y"><img class="mini_circle" src="<ww:url value="'/img/circle_mini_up.gif'"/>" border="0"/></a>
			</span>
			<ds:lang text="NumerKancelaryjny"/>: 
			<b name='numerKO'><ww:property value="document.formattedOfficeNumber"/></b>
		</td>
		

<script type="text/javascript">
$j(document).ready(function() {
pokaz_ukryj();	
});
</script>
		<%--<b><ww:if test="document.masterDocument != null">[</ww:if><ww:property value="document.officeNumber"/><ww:if test="document.masterDocument != null">]</ww:if></b></td>--%>

		<td width="30%" valign="top">
			<ds:extras test="!business">
				<ds:modules test="office or simpleoffice">
					<ds:lang text="SymbolWsprawie"/>: 
					<b><a href="<ww:url value='caseLink'/>">
					<ww:property value="caseDocumentId" /></a></b>
				</ds:modules>
			</ds:extras>
		</td>
	</tr>

	<tr id="id_id" style="display:none">
		<td width="30%">ID: 
			<b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="document.id"/></ww:url>" name='documentId'>
			<ww:property value="document.id" /></a>
			</b>
		</td>
		<td>
			<ww:if test="drsDocs != null">
				<table>
					<ww:iterator value="drsDocs" status="status">
					<tr>
						<td>
							<ww:if test="#status.first"><ds:lang text="NumerySzkod"/>:</ww:if>
						</td>
						<td>
							<a href="<ww:url value="drsLink"/>"><ww:property value="drsNR_SZKODY" /> / <ww:property value="drsNR_KOLEJNY" /></a>
						</td>
					</tr>
					</ww:iterator>
				</table>
			</ww:if>
		</td>
	</tr>

	<ww:if test="deadlineTime != null">
		<tr>
			<td colspan="2">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td width="30%" valign="top">
					<span style="color:red">
				<table>
					<tr>
						<td valign="top">
							<ds:lang text="TerminWykonania"/>:
						</td>
						<td valign="top">
							<b><ds:format-date value="deadlineTime" pattern="dd-MM-yy" /></b>
						</td>
					</tr>
					<tr>
						<td valign="top">
						</td>
						<ds:lang text="ustalonyPrzez"/>
						<td>
						</td>
						<td valign="top">
							<b><ww:property value="deadlineAuthor" /></b>
						</td>
					</tr>
				</table>
				</span>	
			</td>
			<td valign="top">
				<span style="color:red">
					<ds:lang text="Przyczyna"/>: 
					<b><ww:property value="deadlineRemark" /></b>
				</span>
			</td>
		</tr>
	</ww:if>

	<tr id="id_autora" style="display:none">
		<td width="30%" valign="top">
			<table>
				<%--	<tr>
				PIERWOTNA WERSJA
					<td valign="top">
						<ds:lang text="Autor"/>:
					</td>
					<td valign="top">
						<b><ww:property value="creatingUser" /></b>
					</td>
				</tr>
				<tr>
					<td valign="top">
					</td>
					<td valign="top">
						<ds:lang text="dnia"/>
						<b><fmt:formatDate value="${incomingDate}" type="both" pattern="dd-MM-yy HH:mm" /></b>
					</td> 
				</tr> --%>
				<tr>
					<td valign="top">
						<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
							<ds:lang text="Autor"/>:
	                    	<b><ww:property value="creatingUser" /></b>;
							<ds:lang text="dnia"/>: <b><fmt:formatDate value="${incomingDate}" type="both" pattern="dd-MM-yy HH:mm" /></b>
	                    </div>
					</td>
				</tr>
			</table>
		</td>
		<td width="70%" valign="top">
			<table>
				<tr>
					<td valign="top">
						<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
							<ds:lang text="Dzial"/>:
							<b><ww:property value="assignedDivision" /></b>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<ww:set name="printPerson"
	value=":[(#this.firstname != null ? #this.firstname+' ' : '') + (#this.lastname != null ? #this.lastname : '') + (#this.organization != null ? ' / '+#this.organization : '')]" />


	<tr id="id_nadawcy" style="display:none">
		<td width="30%" valign="top">
			<table width="50%">
				<tr>
					<td valign="top">
						<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
							<ds:lang text="Nadawca"/>:
							<b><ww:property value="document.sender.shortSummary" /> 
								<ww:if test= "document.sender.Location != null "> / </ww:if>	
							<ww:property value="document.sender.Location" /> </b>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td width="50%" valign="top">
			<table width=500%">
				<tr>
					<td valign="top">
						<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
							<ds:lang text="Odbiorcy"/>:
							<b> <ww:iterator value="document.recipients" status="status">
								<ww:property value="shortSummary" />
								<ww:if test="!#status.last">, </ww:if>
							</ww:iterator> </b>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>

	<tr id="id_uwag" style="">
		<td valign="top">
			<ww:if test="recentRemark != null">
				<table>
					<tr>
						<td valign="top">
							<div style="white-space: nowrap;">
								<ds:lang text="OstatniaUwaga"/>:
							</div>
						</td>
						<td valign="top">
							<div style="">
								<b><ww:property value="recentRemark.content" /></b>
							</div>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<ds:available test="summary_add_remark">
								
						<div id="newRemarkContainer">
							<ww:textarea name="'remarkText'" id="remarkText" rows="4" cols="25" cssClass="'txt'" />
						</div>
						<div id="newRemarkClose" >
								<a href="#" onclick="closeRemark(this)"><img src= "<c:out value='${pageContext.request.contextPath}'/>/img/minus2.gif" ></a>
						</div>
		
								<span id="spanAddRemark" class="newRemark" onclick="showRemarkInput(this)">dodaj</span>
								<span id="spanSaveRemark" class="newRemark newRemarkSave" onclick="saveRemark()" style="display: none" >zapisz</span>
								<span id="spanWaitRemark" class="newRemarkWait" style="display: none" >czekaj ...</span>
								
								<script type="text/javascript">
									var add_el = $j('#spanAddRemark');
									var save_el = $j('#spanSaveRemark');
								
									
									$j('#newRemarkContainer').hide();
									$j('#newRemarkContainer textarea').keyup(function() {
										var val = $j(this).attr('value');
										if(val != '') {
											if(save_el.css('display') == 'none') {
												add_el.css('display', 'none');
												save_el.css('display', 'block');
											}
										}
									});
								
									function getPos (obj) {
											var output = new Object();
											var mytop=0, myleft=0;
											while( obj) {
												mytop+= obj.offsetTop;
												myleft+= obj.offsetLeft;
												obj= obj.offsetParent;
											}
											output.left = myleft;
											output.top = mytop;
										return output;
									}
									
									function closeRemark() {
										var rc = $j('#newRemarkContainer');
										var rt = $j('#newRemarkContainer textarea');
										var remClose = $j('#newRemarkClose');
										
										rt.attr('value', '');
										rc.hide();
										remClose.css('display', 'none');

										if(save_el.css('display') != 'none') {
											save_el.css('display', 'none');
											add_el.css('display', 'block');
										}
									}
									
									function showRemarkInput(obj) {
										var rc = $j('#newRemarkContainer');
										var rt = $j('#newRemarkContainer textarea');
										rt.attr('value', '');
										var remClose = $j('#newRemarkClose');
										
										
										rc.css('left', getPos(obj).left + 'px');
										rc.css('top', getPos(obj).top + obj.offsetHeight + 5 + 'px');
										rc.show('blind');
										rc.attr('visibility', 'visible');
										remClose.css('left', getPos(obj).left + rc.attr('offsetWidth') - 16 + 'px');
										remClose.css('top', getPos(obj).top + 6 + 5 + 'px');
										remClose.css('display', 'block');										
									}

									function saveRemark() {
										$j('#newRemarkContainer').hide();
										save_el.hide();
										$j('#newRemarkClose').hide();
										$j('#spanWaitRemark').css('display', 'block');
										sendEvent(document.getElementsByName('doAddRemark').item(0));
									}
								</script>
							</ds:available>
							<ds:event value="getText('DodajUwage')"  name="'doAddRemark'" cssClass="'hidden'" disabled="false" />
						</td>
						<td valign="top">
							<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
								<ds:lang text="DodanaPrzez"/>
									<b> <ww:property value="recentRemark.author" /> </b>.
							</div>
						</td>
					</tr>
				</table>
			</ww:if>
		</td>
		<td valign="top">
			<table>
				<tr>
					<td valign="top">
						<ds:lang text="OpisPisma"/>:                    	
					</td>
					<td>
						<b id='shortSummary'> <ww:property value="document.Summary" />
						<ww:if test="daaSummary != null">
							<br/>
							<ww:property value="daaSummary" />
						</ww:if>
						</b>
					</td>
				</tr>
				<ds:available test="summary.document.abstract">
				<tr>
                <td valign="top">
                    <ds:lang text="Abstrakt"/>:
                </td>
                <td>
                    <b><ww:property value="abstrakt" /></b>
                                     </td>
                </tr>
                </ds:available>
				<ww:if test="folderPrettyPath!=null">
					<tr>
						<td valign="top">
							<ds:lang text="Sciezka"/>: 
						</td>
						<td valign="top">
							<ww:property value="folderPrettyPath"/>
						</td>
					</tr>
				</ww:if>
				<ww:if test="documentForm != null">
				<tr>
					<td valign="top">
						<ds:lang text="Forma dokumentu"/>:                    	
					</td>
					<td>
						<b id='documentForm'> 
							<ww:property value="documentForm" />
						</b>
					</td>
				</tr>
				</ww:if>
			</table>
		</td>
	</tr>

	<ww:if test="orders.size > 0">
		<tr id="id_polecenia" style="display:none">
			<td>	
				<table>
					<tr>
						<td>
							<ds:lang text="PoleceniaZwiazaneZpismem"/> :				
							<ww:iterator value="orders">
								<a href="<ww:url value="'/office/order/summary.action'"/>?documentId=<ww:property value="id"/>&activity=<ww:property value="activityId"/>">
								<ww:property value="officeNumber"/> ,
							</ww:iterator>
						</td>
					</tr>		
				</table>
			</td>
		</tr>
	</ww:if>

	<ds:extras test="business">
		<ds:available test="archiwizacja.podsumowanie.pudlo">
			<tr><td colspan="2">
				<table id="boxInnerTable">
					<jsp:include page="/common/box.jsp"/>
				</table>
				</td>
			</tr>
		</ds:available>
	</ds:extras>
	
	
		
			
			 <ww:if test="document.duplicateDoc != null">
			 <tr id="id_duplikatu">
			 <td>
			 	<span class="warning">
				 	<ds:lang text="IdDuplikatu"/>:
				 	<b>
				 	<a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="document.duplicateDoc"/></ww:url>">
					<ww:property value="document.duplicateDoc" /></a>
				 	</b>
				 </span>
			 </td><td>&nbsp;</td>
			 </tr>
			 </ww:if>
		
	

	
	<ww:if test="daaUnassigned">
		<tr id="id_agencji" style="display:none">
			<td style="color:red">
				<span style="font-weight:bold;">!</span>
				<ds:lang text="nieprzypisanyDoAgencji/agenta"/>
			</td>
			<td>
				&nbsp
			</td>
		</tr>
    </ww:if>
</table>

<jsp:include page="/office/common/workflow/jbpm-summary.jsp"/>

	
	<div class="topSpacer">
	<ww:if test="flagsPresent">
		<jsp:include page="/common/flags-present.jsp"/>
	</ww:if>
	</div>
<ds:available test="labels">
		<table>
		<tr>	
			<td>
				<b><ds:lang text="EtykietyModyfikowalne"/>:</b>
				<ww:iterator value="modifiableLabels">
					<div class="inline_flags">
								<a href="<ww:url value="'/office/remove-label.action'">
								<ww:param name="'documentId'" value="documentId"/>
								<ww:param name="'activity'" value="activity"/>
								<ww:param name="'labelId'" value="id"/></ww:url>" 
								title="<ds:lang text="UsunEtykiete"/>"
								class="area">
									<ww:property value="name" /> 
								</a>
					</div>
				</ww:iterator>
				<ww:iterator value="nonModifiableLabels">
					<div class="inline_flags">
						<b><ww:property value="name" />,</b>
					</div>
				</ww:iterator>
			</td>
		</tr>
		<!--<tr>	
			<td>
				<b><ds:lang text="EtykietyNiemodyfikowalne"/>:</b>
				<ww:iterator value="nonModifiableLabels">
					<div class="inline_flags">
						<b><ww:property value="name" />,</b>
					</div>
				</ww:iterator>
			</td>
		</tr>
		--></table>
		</ds:available>
	<ds:modules test="certificate">
		<ww:if test="recentSignature != null">
			<h4><ds:lang text="OstatniaAkceptacjaDokumentu"/></h4>
			
			<table>
				<tr>
					<th>
						<ds:lang text="Autor"/>
					</th>
					<th>
						<ds:lang text="Data"/>
					</th>
					<th colspan="2" style="text-align:center">
						<ds:lang text="pobierz"/>:
					</th>
				</tr>
				<tr>
					<td>
						<ww:property value="recentSignature.author" />
					</td>
					<td>
						<ds:format-date value="recentSignature.ctime" pattern="dd-MM-yy HH:mm" />
					</td>
					<td>
						<a href="<ww:url value="'/office/common/document-signature.action?getDoc=true&signatureId='+recentSignature.id"/>"><ds:lang text="dokument"/></a>
					</td>
					<td>
						<a href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+recentSignature.id"/>"><ds:lang text="podpis"/></a>
					</td>
				</tr>
			</table>
		</ww:if>
		<ww:if test="useSignature">
			<p class="btnContainer">
			<ww:if test="canAccept">
				<input type="button" value="<ds:lang text="AkceptujDokument"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-accept.action?doAcceptFillForm=true&id='+document.id"/>', 'accept', 500, 300);" <ww:if test="!canSign">disabled="disabled"</ww:if>/>
			</ww:if>
			<ww:else>
				<input type="button" value="<ds:lang text="PodpiszDokument"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doSign=true&id='+document.id"/>', 'sign', 400, 300);" <ww:if test="!canSign">disabled="disabled"</ww:if>/>
			</ww:else>
			<input type="button" 
				<ww:if test="canAccept">
					value="<ds:lang text="PostacDokumentuDoAkceptacji"/>"
				</ww:if>
				<ww:else>
					value="<ds:lang text="PostacDokumentuDoPodpisu"/>"
				</ww:else>
				 class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?showDoc=true&id='+document.id"/>', 'sign', 800, 600);"/></p>
		</ww:if>
	</ds:modules>

	<ds:modules test="coreoffice or fax">
		<ww:if test="attachments.size > 0">
			<h4 class="topSpacer"><ds:lang text="Zalaczniki"/></h4>
		
			<table cellspacing="0" cellpadding="3" class="attachs">
				<tr>
					<th>
						<ds:lang text="Tytul"/>
					</th>
					<th>
						<ds:lang text="Autor"/>
					</th>
					<%--	<th>
						Rozmiar
					</th>--%>
					<th>
						<ds:lang text="Data"/>
					</th>
					<%--	<th>
						Wersja
					</th>	--%>
					<%--	<th>
						Kod kreskowy
					</th>	--%>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<ww:iterator value="attachments" status="stat">
					<tr <ww:if test="#stat.odd == true">class="lightOddRows"</ww:if>>
						<td class="attachTitle">
							<ww:property value="title" />
						</td>
						<td>
							<ww:property value="userSummary" />
						</td>
						<ww:if test="mostRecentRevision != null">
							<ww:set name="revision" value="mostRecentRevision" />
							<ww:set name="size" scope="page" value="#revision.size" />
							<ww:set name="ctime" scope="page" value="#revision.ctime" />
							<%--	<td>
								<ww:property value="#u.firstname + ' ' + #u.lastname"/>
							</td>	--%>
							<%--	<td>
								<edm-fmt:format-size size="${size}"/>
							</td>	--%>
							<td>
								<fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm" />
							</td>
							<%--	<td>
								<ww:property value="#revision.revision"/>
							</td>	--%>
							<%--	<td>
								<ww:property value="barcode"/>
							</td>	--%>
							<td width="32">
								&nbsp;
								<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>">
								<img class="zoomImg" src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg"
								title="<ds:lang text="PobierzZalacznik"/>" /></a>
							</td>
							<td valign="top" width="64">
								<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
									&nbsp;
									<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+#revision.id"/>">
									<img class="zoomImg" src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg"
									title="<ds:lang text="PobierzZalacznik"/>" /></a>
									&nbsp;
									<a name="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/><ww:param name ="'documentKindCn'" value="normal"/><ww:param name ="'documentKindCn'" value="'normal'"/></ww:url>', 'vs',screen.width,screen.height);">
									<img class="zoomImg" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
									height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
								</ww:if>
								<ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimePicture(#revision.mime)">
                                	&nbsp;
                                	<a name="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/><ww:param name ="'documentKindCn'" value="normal"/><ww:param name ="'documentKindCn'" value="'norma'l"/></ww:url>', 'vs',screen.width,screen.height);">
                                	<img class="zoomImg" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
                                	height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
                                </ww:elseif>
								<ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(#revision.mime)">
                                    <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ #revision.id"/>')">
                                        <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                                    </a>
                                </ww:elseif>
							</td>
							<td>
								<ww:if test="'nw_compiled_att'.equals(cn) && compilationStatus.newDocuments != null">
									<ww:iterator value="compilationStatus.newDocuments">
										<a style="color:red" href="<ww:url value="'/repository/edit-document.action?id='+key"/>"><ww:property value="value"/></a>
									</ww:iterator>
								</ww:if>
							</td>
						</ww:if>
						<ww:elseif test="'mail'.equals(cn)">
							<td>
								<ds:format-date value="attCtime" pattern="dd-MM-yy HH:mm" />
							</td>
							<td colspan="3">
								&nbsp;
							</td>
						</ww:elseif>
						<ww:else>
							<td colspan="4">
								<ww:if test="lparam != null">
									<b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)" /></i></b>
								</ww:if>
								<ww:else>
									<i>(<ds:lang text="pusty"/>)</i>
								</ww:else>
							</td>
						</ww:else>
					</tr>
				</ww:iterator>
				
					<tr>
					<td>
					<input type="button" value="<ds:lang text="Pobierz pliki z "/>" class="btn"
							onclick="openWith();">
					</td>		
						<td>Uwagi</td><td><ww:checkbox id="uwaga" name="'uwagaName'" fieldValue="true" /></td>
						<td>Historia pisma</td><td><ww:checkbox id="historia" name="'historiaName'" fieldValue="true"/></td>
						<td>Historia dekretacji</td><td><ww:checkbox id="dekretacja" name="'dekretacjaName'" fieldValue="true"/></td>
						<td colspan="6" align="right">
						
						    <!-- W przypadku konieczno��i usuni�cia prosz� o wcze�niejszy kontakt - Maciej Starosz -->
							
							<ww:if test="compilationStatus.canCompile">
								<ds:event name="'doCompileAttachments'" value="getText('KompilujPlikiSprawy')" cssClass="'btn'"/>
							</ww:if>
							<ww:if test="canGenerateDocumentView">
								<input type="button" value="<ds:lang text="PobierzObrazDokumentu"/>" class="btn"
								onclick="openToolWindow('<ww:url value="baseLink"><ww:param name="'doGenerateDocumentView'" value="true"/><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>', 'nwPdf')">
								<%--	<ds:event name="'doGenerateDocumentView'" value="getText('PobierzObrazDokumentu')" cssClass="'btn'"/>	--%>
							</ww:if>
							<ww:if test="canGenerateCsvDocument">
								<input type="button" value="<ds:lang text="PobierzDokumentCsv"/>" class="btn"
								onclick="document.location.href='<ww:url value="baseLink"><ww:param name="'doGenerateCsvDocument'" value="true"/><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>'">
							</ww:if>
						</td>
					</tr>
			</table>
		</ww:if>

		<ww:if test="acceptancesEnabled">
			<table>
				<jsp:include page="/common/summary-acceptances.jsp"/>
			</table>
		</ww:if>
		
		<ww:if test="false">
			<jsp:include page="/common/invoiceFromZamowienie.jsp"/>
		</ww:if>
		
		<ww:if test="enableCollectiveAssignments">
		
		<script type="text/javascript">
				
		
		
			function updateSelectFromLabel(id) {
				var el = document.getElementById(id);

				if(!el.checked) {
					el.checked = 'checked';
					updateSelect(el);
				} else {
					el.checked = 'false';
					updateSelect(el);
				}
			}
		</script>
		
			<h4 class="topSpacer"><ds:lang text="ZbiorczaDekretacja"/></h4>
			<ww:if test="assignmentsMap == null or assignmentsMap.isEmpty">
				<p><i><ds:lang text="ListaZbiorczaDekNieSkonfigurowana1"/>
				<a href="<ww:url value="'/settings/user-collective-assignment.action'"/>"><ds:lang text="tutaj"/></a>.
				<ds:lang text="ListaZbiorczaDekNieSkonfigurowana2"/>. </i></p>
			</ww:if>
			<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
				<table>
					<tr>
						<th></th>
						<th>
							<ds:lang text="Uzytkownik"/>
						</th>
						<th>
							<ds:lang text="RodzajDekretacji"/>
						</th>
						<ww:if test="showObjectives">
							<th>
								<ds:lang text="CelDekretacji"/>
							</th>
						</ww:if>
					</tr>
					
					<ww:iterator value="assignmentsMap">
						<tr>
							<td>
								<input type="checkbox" name='assignments' value="<ww:property value="key"/>"
								id="<ww:property value="key"/>" onchange="updateSelect(this)" />
							</td>
							<td>
									<ww:property value="value" />
							</td>
							<td>
								<select name="assignmentProcess_<ww:property value="key"/>" id='assignmentSelect' class="sel" onchange="updateCheckbox(this);checkSelection()">
									<option value="">
										<ds:lang text="select.wybierz"/>
									</option>
									<ww:iterator value="wfProcesses">
										<option value="<ww:property value="key"/>">
											<ww:property value="value" />
										</option>
									</ww:iterator>
								</select>
							</td>
							<ww:if test="showObjectives">
								<td>
									<ww:select name="'objectiveSel'" list="objectives" cssClass="'sel'" id="objectiveSel"
									listKey="name" listValue="name" headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
								</td>
							</ww:if>
						</tr>
					</ww:iterator>
				</table>
			</ww:if>
		</ww:if>

		<table class="topSpacer">
			<tr>
				<td colspan="3">
                    <ds:available test="print.barcode.create">
                        <ww:if test = "isInOfficeDocument()">
                            <ds:event value="getText('DrukujEtykiete')" name="'doPrint'" cssClass="'btn'"/>
                        </ww:if>
                    </ds:available>
					<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
						<ds:event name="'doAssignments'" value="getText('Dekretuj')" onclick="'if (!validateAssignments()) return false; if(!checkSubstitutions()) return false;'" />
						<%--	<input type="submit" name="doAssignments" value="Dekretuj" class="btn" onclick="if (!validateAssignments() || !confirm('Na pewno dekretowa�?')) return false; E('doAssignments').value='true';"/>	--%>
					</ww:if>
					
					<ww:if test="enableCollectiveAssignments">
						<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
							
						</ww:if>
					</ww:if>
                    <ww:if test="flagsPresent">
                        <ds:event name="'doUpdateFlags'" value="getText('Zapisz')" disabled="blocked" cssClass="'btn saveBtn'"/>
                    </ww:if>
					<ds:modules test="office or fax">
						<ww:if test="externalWorkflow">
							<input type="button" class="btn" value="<ds:lang text="czZadanie"/>"
							onclick="document.location.href='<ww:url value="jbpmLink"/>';" />
                            
						</ww:if>
						<ww:else>
                            <ww:if test="canReopenWf && !doWiadomosci">
    							<ds:event name="'doReopenWf'" value="getText('PrzywrocNaListeZadan')"/> <%-- disabled="!canReopenWf" />  --%>
                            </ww:if>
							
                            <ww:else>
								<ww:if test="canFinish && activity != null">
									<ww:if test="doWiadomosci"><ds:event name="'doManualFinish'" value="getText('PotwierdzamPrzeczytanie')"/></ww:if>
									<ww:else><ds:event name="'doManualFinish'" value="getText('ZakonczPraceZdokumentem')" onclick="'if (!confirmFinish()) return false'" /></ww:else>
								</ww:if>
                            </ww:else>
							<ww:if test="canAssignMe">
								<ds:event name="'doAssignMe'" value="getText('DekretujNaSiebie')"/>
							</ww:if>
						</ww:else>
						<ds:available test="!summaryTab.doWatchExtra.disabled">
							<ds:event name="'doWatch'" value="getText('DodajDoObserwowanych')" />
						</ds:available>
					</ds:modules>

                    <input type="hidden" name="processName" id="processName"/>
                    <input type="hidden" name="processId" id="processId"/>
                    <input type="hidden" name="processAction" id="processAction"/>

					<ds:available test="!summaryTab.processRenderBeans.disabled">
	                    <ww:iterator value="processRenderBeans">
	                        <ds:render template="template"/>
	                    </ww:iterator>
                    </ds:available>

					<ww:if test="canAddToWatched">
                        <ds:event name="'doWatch'" value="getText('DodajDoObserwowanych')" />
					</ww:if> 

					<ds:extras test="!business">
						<ds:available test="klonowanie.podsumowanie">
							<ds:event name="'doClone'" value="getText('Klonuj')" />
						</ds:available>
					</ds:extras>
					
					<ds:extras test="business">
						<ds:available test="klonowanie.podsumowanie">
							<input type="button" class="btn" value="<ds:lang text="Klonuj"/>"
							onclick="document.location.href='<ww:url value="'/repository/clone-dockind-document.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>';" />
						</ds:available>
					</ds:extras>
					
					
					
					<ds:additions test="orders">
						<input type="button" class="btn" value="<ds:lang text="UtworzPolecenie"/>"
						onclick="document.location.href='<ww:url value="'/office/order/new.action'"><ww:param name="'relatedOfficeDocumentId'" value="documentId"/></ww:url>';" />
					</ds:additions>
					<ww:if test="showSendToEva && activity != null">
						<ds:event name="'sendToEva'" value="getText('sendToEva')"/>
					</ww:if>
					<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/office/tasklist/current-user-task-list.action';"/>
					 <td>
					 					  <input type="submit" value="Metryka Dokumentu" name="doDocumentMetrics" class="btn" />
	        <!-- <input type="submit" value="Metryka Dokumentu" name="doDocumentMetrics" class="btn" onclick="sendEvent(this)"/> -->
	       	<ds:available test="PG.wlacz.wydrukKartaZastepcza"> 
	      	  <input type="submit" value="Generuj kart� zast�pcz�" name="doPrintCard" class="btn" />
	      	 </ds:available>
	         <ds:available test="PG.electronicSignature.document">
					<td width="32">
					 
					 <input type="button" class="btn" value="<ds:lang text="Podpisz Certyfikatem"/>"
					class="btn"	onclick="document.location.href='<ww:url value="'/certificates/sign-xml.action?documentId='+document.id +'&returnUrl=/docusafe/office/incoming/summary.action?documentId='+document.id"></ww:url>';"/>
					</td>
             </ds:available>
              <ds:available test="XesLogApi">
							<td width="32">
							 <input type="submit" name="doGetXesLog" value="<ds:lang text="Pobie� XesLog"/>" class="btn"   name="'doGetXesLog'" id="doGetXesLog" />
							</td>
             </ds:available>
               <ds:available test="graphs">
           					    <input type="hidden" name="graphML" id="graphML"/>
           					     <input type="hidden" name="graphDot" id="graphDot"/>
           						 <td>																							  
  
						 <input type="submit" name="doGetGraph" value="<ds:lang text="Graph ML"/>" class="btn"  
						 onclick="document.getElementById('graphML').value = true; document.getElementById('graphDot').value = false"/>
								</td>
									
								<td>																							
						 <input type="submit" name="doGetGraph" value="<ds:lang text="Graph DOT"/>" class="btn"
						   onclick="document.getElementById('graphML').value = false; document.getElementById('graphDot').value = true"/>
							</td>
			 </ds:available>																				
	      
	    	</td>
					<ww:if test="viewQc">
						<ds:event name="'doHotIcr'" value="getText('OkQC')" />
					</ww:if>

					<ds:available test="tasklist.manualToCoordinator">
					    <ww:if test="canAssignToCoordinator">
							<ds:event value="getText('Przeka� do koordynatora')" cssClass="'btn'" name="'doManualPushToCoor'" id="doManualPushToCoor"/>
                        </ww:if>
					</ds:available>

					<ww:if test="evaMessage != null">
						<ww:property value="evaMessage" />
					</ww:if>
					<br/>

					<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>
				</td>
			</tr>
		</table>
	</ds:modules>

 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>
<script>

    //W przypadku konieczno��i usuni�cia prosz� o wcze�niejszy kontakt - Maciej Starosz
	function openWith()
	{
		openToolWindow('<ww:url value="'view-attachments-as-pdf.action'"/>?documentIds=<ww:property value="documentId"/>' +
			'&remarksLayer=true' +	
			'&czyUwagi='+document.getElementById("uwaga").checked +
			'&czyHistoria='+document.getElementById("historia").checked +
			'&czyDekretacja='+document.getElementById("dekretacja").checked, 'nwPdf');
	}
	
	//funkja ukrywaj�ca pole autora, itp
 	function validateForm(){return true;}

	function pokaz_ukryj()
	{
		if($j('#id_strzalki1:visible').length) {
			$j('#id_strzalki1').hide();
			$j('#id_strzalki2').show();
			$j('#id_id, #id_autora, #id_nadawcy, #id_polecenia, #id_agencji').show();
			
		} else {
			$j('#id_strzalki2').hide();
			$j('#id_strzalki1').show();
			$j('#id_id, #id_autora, #id_nadawcy, #id_polecenia, #id_agencji').hide();
		}
		<ds:additions test="tcLayout">
			szerokosc();
			wysokosc();
		</ds:additions>
	}

    
    function updateCheckbox(select)
    {
        var id = select.name.substring(select.name.indexOf('_')+1);
        E(id).checked = (select.value.length > 0);
        /*
         jak wy��czona dekretacja na wiele os�b do realizacji to tutaj w przypadku wybrania 
             'do_realizacji' odpowiednie selecty czyszcze
       <ww:if test="!multiRealizationAssignment">
        if (select.value == 'internal,docusafe_1::obieg_reczny')
        {
            var selects = document.getElementsByTagName('select');
            for (var i=0; i < selects.length; i++)
            {
                if ((select != selects[i]) && (selects[i].value == 'internal,docusafe_1::obieg_reczny'))
                {
                    selects[i].selectedIndex = 0;
                }
            }
        }    
        </ww:if> 
        */
    }

    function countDoRealizacji()
    {
        var selects = document.getElementsByTagName('select');
        var count = 0;
        for (var i=0; i < selects.length; i++)
        {
            //alert(selects[i].value);
            if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
            {
                count++;
            }
        }
        return count;
    }

    function checkSelection()
    {
      /*  var selects = document.getElementsByTagName('select');
        var count = 0;
        for (var i=0; i < selects.length; i++)
        {
            //alert(selects[i].value);
            if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
            {
                count++;
            }
        }*/
        //alert(count);h
        <ww:if test="multiRealizationAssignment">
        if (countDoRealizacji() > 1)
            alert('<ds:lang text="WybranoWiecejNizJednaDekretacjeRealizacja.KlikniecieDekretujSpowodujeDekretacjeDoWiecejNizJednejOsoby"/>.');
        </ww:if>
    }

    function updateSelect(checkbox)
    {

        var id = checkbox.value;
        var sels = document.getElementsByName('assignmentProcess_'+id);
        if (checkbox.checked && sels && sels.length > 0)
        {
            var sel = sels[0];
            if (sel.options && sel.options.length > 0)
                sel.options.selectedIndex = 1;
        } else {
			var sel = sels[0];
            if (sel.options && sel.options.length > 0)
                sel.options.selectedIndex = 0;
		}

    }

    function validateAssignmentsTomek()
    {
        var chks = document.getElementsByName('assignments');
        var checkedCount = 0;
        for (var i=0; i < chks.length; i++)
        {
            if (chks.item(i).checked) checkedCount++;
        }
        
        if (countDoRealizacji() > 1)
        {
            alert('<ds:lang text="MoznaWybracTylkoJednaDekretacjeTypuRealizacja"/>');
           // alert('Mozna wybrac tylko jedna dekretacje typu realizacja"');
            return false;
        }
        
        if (checkedCount == 0)
        {
			alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
            return false;
        }        
        return true;
    }
    
    function validateAssignments()
    {
    	var chks = document.getElementsByName('assignments');
    	var checkedCount = 0;
    	 
    	<ds:extras test="!business">               
        	var selects = document.getElementsByTagName('select');
    
			if(selects.length>chks.length)
			{    
        		for (var i=0; i < selects.length; i++)
        		{
        			if(selects[i].id == 'assignmentSelect')
            		{
        				//alert("chks.item(i/2).checked: " + chks.item(i/2).checked + " i= " + i);
            			if (chks.item(i/2).checked) 
            			{                       	
            				if(selects[i].value == '')
            				{
            					alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            					return false;
            				}
            			}            
            		}          
        		}
        	}
        	else
        	{
        		for (var i=0; i < chks.length; i++)
        		{        		
            		if (chks.item(i).checked) 
            		{                       	
            			if(selects[i].value == '')
            			{
            				alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            				return false;
            			}
            		}                        	         
        		}
        	}
        </ds:extras>
                                      
        for (var i=0; i < chks.length; i++)
        {
            if (chks.item(i).checked) checkedCount++;
        }
        //byl if test dla !multiRealizationAssignment dla tej petli

        if (countDoRealizacji() > 1)
        {
        	alert('<ds:lang text="MoznaWybracTylkoJednaDekretacjeTypuRealizacja"/>');
        	//alert('Mozna wybrac tylko jedna dekretacje typu realizacja');
            return false;
        }

        if (checkedCount == 0)
        {
			alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
            return false;
        }        
        return true;
    }

    function confirmFinish()
    {
    <ww:if test="!finishOrAssignState.canFinish">
        alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
        return false;
    </ww:if>
    <ww:if test="!finishOrAssignState.finishWarnings.empty && !documentFax">
        if (!confirm('<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/>\n\n<ds:lang text="ZakonczycPraceZdokumentem"/>'))
            return false;
    </ww:if>
    <ww:else>
       // if (!confirm('Na pewno zako�czy�?'))
       //     return false;
    </ww:else>
        return true;
    }

    function checkSubstitutionsInTempArray(substitutionsArray, key) {
		var result = false;
		if (substitutionsArray.indexOf(key) > -1)
			 result = true;
		return result;
	}
    
	function checkSubstitutions() {
	    var count=0;
	    var show=0;
	    var assignments = document.getElementsByName('assignments');
	    var str = "<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
	    var str1;
	    var tmpSubstitutions = '<ww:property value="substitutedNames.keySet()" />';

	    for (var i=0; i < assignments.length; i++)
	    { 
	        if (!assignments.item(i).checked)
	            continue; 
	    
		    try {
		    	var asgm = assignments.item(i).value.substring(0, assignments.item(i).value.indexOf(';'));
			    <ww:iterator value="substituted" status="status">
			    	if(checkSubstitutionsInTempArray(tmpSubstitutions, asgm)) {
			    		show=show+1;
			    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
			    		str=str+str1;
			    	}
			    	count=count+1;
			    </ww:iterator>
		     } catch(err) { }
	    }
	    
	    if(show == 0){
	    	if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
	    	return false;
	    }
	    else{
	    	if(!confirm(str))
	    	return false;
	  	}	    
	    return true;   
    }

</script>
