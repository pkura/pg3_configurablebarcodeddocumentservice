<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:property value="treeHtml" escape="false"/>

<script type="text/javascript">
	function pick(id, chooseType)
	{
	    <ds:available test="pg.import.alfresco">
	        if(chooseType == 'DOCUMENT') {
                var url2 ='<ww:url value="'/office/incoming/new.action'"/>';
                window.opener.location= url2 + '?prop='+encodeURIComponent(id)+'&documentKindCn=normal';
                self.close();
           } else {
                window.opener.jQuery('#propUuid').val(id);
                window.opener.jQuery('#sendToExternalRepo').val(true);
                window.opener.jQuery('#doUpdate').val(true);
                window.opener.sendEvent(window.opener);
                self.close();
            }
        </ds:available>
	}
</script>