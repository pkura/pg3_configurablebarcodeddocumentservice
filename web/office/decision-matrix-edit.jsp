<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="Edycja"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/decision-matrix-edit.action'"/>" method="post">
<table>
	<ww:hidden name="'matrixGuid'" id="matrixGuid" value="matrixGuid"/>
	<ww:hidden name="'id'" id="id" value="id"/>
	<tr>
		<td>
			<ds:lang text="Priorytet"/>
		</td>
		<td>
			<ww:textfield name="'priorytet'" value="priorytet"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Segment"/>
		</td>
		<td>
			<ww:select name="'segment'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="dk.getFieldByCn('SEGMENT').getEnumItems()" listKey="cn" listValue="getTitle()"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Typ"/>
		</td>
		<td>
			<ww:select name="'typ'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="dk.getFieldByCn('TYP_DOKUMENTU').getEnumItems()" listKey="cn" listValue="getTitle()"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="PodTyp"/>
		</td>
		<td>
			<ww:select name="'podtyp'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="dk.getFieldByCn('PODTYP_DOKUMENTU').getEnumItems()" listKey="cn" listValue="getTitle()"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="SposobPrzyjecia"/>
		</td>
		<td>
			<ww:select name="'kanalWplywu'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="dk.getFieldByCn('SPOSOB_PRZYJECIA').getEnumItems()" listKey="cn" listValue="getTitle()"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="PNAOd"/>
		</td>
		<td>
			<ww:textfield name="'pnaFrom'" value="pnaFrom"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="PNADo"/>
		</td>
		<td>
			<ww:textfield name="'pnaTo'" value="pnaTo"/>
		</td>
	</tr>
	<tr>
		<ww:hidden name="'division'" id="division" value="division"/>
		<td>
			<ds:lang text="Dzial"/>
		</td>
		<td>
			<input readonly="readonly"  type="text" id="divisionName"
				value="<ww:property value="divisionName"/>" size="30" class="txt"/>
		</td>
	</tr>
	<tr>
		<ww:hidden name="'user'" id="user" value="user"/>
		<td>
			<ds:lang text="Uzytkownik"/>
		</td>
		<td>
			<input readonly="readonly"  type="text" id="userName"
				value="<ww:property value="userName"/>" size="30" class="txt"/>
		</td>
		<td>
			<a name="<ww:property value="cn"/>" href="javascript:void(openAssignmentWindow('rootdivision'))">
				<ds:lang text="wybierz"/></a>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="AutoDekretacja"/>
		</td>
		<td>
			<ww:checkbox name="'auto'" fieldValue="true" value="auto"/>
		</td>
	</tr>
	<tr>
		<td>
			<ww:if test="id ==null">
				<ds:event name="'doCreate'" value="getText('Dodaj')"/>
			</ww:if>
			<ww:else>
				<ds:event name="'doSave'" value="getText('Zapisz')" cssClass="'btn saveBtn'"/>
				<ds:event name="'doDelete'" value="getText('Usun')" confirm="'Czy na pewno usun��'"/>
			</ww:else>
			<ds:button-redirect href="'/office/decision-matrix.action?selectedGuid='+matrixGuid" value="getText('Powrot')"/>
		</td>
	</tr>
</table>
<script type="text/javascript">

   	function openAssignmentWindow(divisionGuid)
	{
   		window.open('<ww:url value="'/admin/division-tree.action'"/>?divisionGuid='+divisionGuid+'&cn=BRAK', 'assignmentwindow', 'width=500,height=400,resizable=yes,scrollbars=yes,status=no,toolbar=no,menu=no');
	}

	function accept(param,cn,name,username)
	{
		var element = document.getElementById('division');
		element.value = param;
		var element2 = document.getElementById('divisionName');
		element2.value = name;
		if(username != null)
		{
			var element3 = document.getElementById('userName');
			element3.value = username;
			var element4 = document.getElementById('user');
			element4.value = username;
		}
		else
		{
			var element3 = document.getElementById('userName');
			element3.value = '';
			var element4 = document.getElementById('user');
			element4.value = '';
		}
	}
</script>
</form>