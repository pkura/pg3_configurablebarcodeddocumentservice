<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N outgoing-package-main.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script language="JavaScript">
</script>

<h3>Wysyłka</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/outgoing-package">
<html:hidden property="documentId" />

<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>Dotyczy dokumentu przychodzącego:</td>
    <td><a href="<c:out value="${incomingDocumentLink}"/>">odnośnik</a></td>
</tr>
<tr>
    <td>Numer przesyłki rejestrowanej (R):</td>
    <td><c:out value="${attrs.postalRegNumber}"/></td>
</tr>
<tr>
    <td><ds:lang text="DataStemplaPocztowego"/>:</td>
    <td><fmt:formatDate value="${attrs.stampDate}" pattern="dd-MM-yyyy" /></td>
</tr>
</table>

</html:form>

