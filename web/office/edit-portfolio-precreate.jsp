<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-portfolio-precreate.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:if test="portfolio.subFolder">
<h1>Podteczka <ww:property value="portfolio.officeId"/></h1>
</ww:if>
<ww:else>
<h1>Teczka <ww:property value="portfolio.officeId"/></h1>
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/edit-portfolio.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'id'" value="id"/>
<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
<ww:hidden name="'subname'" value="subname"/>
<ww:hidden name="'officeIdPrefix'" value="officeIdPrefix"/>
<ww:hidden name="'officeIdMiddle'" value="officeIdMiddle"/>
<ww:hidden name="'officeIdSuffix'" value="officeIdSuffix"/>
<ww:hidden name="'suggestedOfficeId'" value="suggestedOfficeId"/>
<ww:hidden name="'suggestedSequenceId'" value="suggestedSequenceId"/>
<ww:hidden name="'changingNumber'" value="changingNumber"/>   <!-- bool -->
<ww:hidden name="'mainPortfolio'" value="mainPortfolio"/>     <!-- bool -->

<ww:if test="changingNumber == null || !changingNumber">
	<p><br><br>Zamierzasz za�o�y� now� podteczk� o symbolu <ww:property value="suggestedOfficeId"/>
	na pozycji <ww:property value="suggestedSequenceId"/>
	(<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>)</p>
</ww:if>
<ww:else>
	<ww:if test="mainPortfolio == null || !mainPortfolio">
		<p><br><br>Zamierzasz zmieni� numer podteczki na symbol <ww:property value="suggestedOfficeId"/>
		oraz pozycj� <ww:property value="suggestedSequenceId"/>
		(<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>)</p>
	</ww:if>
	<ww:else>
		<p><br><br>Zamierzasz zmieni� numer teczki na symbol <ww:property value="suggestedOfficeId"/>
		(<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>)</p>
	</ww:else>
</ww:else>

<div id="breakdown" style="display: none">
    <ww:hidden name="'customOfficeId'" id="customOfficeId"/> <!-- bool -->
    <ww:if test="mainPortfolio != null && mainPortfolio">
    	<ww:hidden name="'customSequenceId'" value="customSequenceId"/>
    </ww:if>
    <ww:else>
	    Lp: <ww:textfield name="'customSequenceId'" value="customSequenceId" cssClass="'txt'" size="4" maxlength="6"/>.
	    &nbsp;
	</ww:else>
    <ww:textfield name="'customOfficeIdPrefix'" value="customOfficeIdPrefix" cssClass="'txt'" size="6" maxlength="20" />
    <ww:textfield name="'customOfficeIdPrefixSeparator'" value="customOfficeIdPrefixSeparator" cssClass="'txt'" size="2" maxlength="2" />
    <ww:textfield name="'customOfficeIdMiddle'" value="customOfficeIdMiddle" cssClass="'txt'" size="15" maxlength="50" />
    <ww:textfield name="'customOfficeIdSuffixSeparator'" value="customOfficeIdSuffixSeparator" cssClass="'txt'" size="2" maxlength="2" />
    <ww:textfield name="'customOfficeIdSuffix'" value="customOfficeIdSuffix" cssClass="'txt'" size="6" maxlength="20" />
</div>

<p></p>

<ww:if test="changingNumber != null && changingNumber">
	<ds:submit-event value="'Zapisz numer'" name="'doCreate'" cssClass="'btn'" />
</ww:if>
<ww:else>
	<ds:submit-event value="'Utw�rz podteczk�'" name="'doCreate'" cssClass="'btn'" />
</ww:else>
<ds:submit-event value="'Anuluj'" name="'brak akcji'" cssClass="'btn'"/>


</form>

<script language="JavaScript">

function customOfficeId()
{
    toggle_div('breakdown', true);
    document.getElementById('customOfficeId').value = 'true';
}

function toggle_div(id, show)
{
    var div = document.getElementById(id);
    div.style.display = show ? '' : 'none';
}

</script>
