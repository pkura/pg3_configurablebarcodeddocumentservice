<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N edit-rwa.jsp N-->
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rzeczowy Wykaz Akt</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:property value="treeHtml" escape="false"/>

<form action="<ww:url value="'/office/rwa/edit-rwa.action'"/>" method="post" enctype="multipart/form-data">
	<ww:hidden name="'id'" value="id"/>
	<p>Poka� nieobowi�zuj�ce<ww:checkbox name="'showActive'"  onchange="'(document.forms[0].submit())'" value="showActive"  fieldValue="'true'"/></p>
	<ww:if test="rwa != null">
		<h4>Edycja bie��cej kategorii</h4>
		
		<table class="tableMargin table100p">
			<tr>
			    <td>
			    	Kod:
			    </td>
		        <td>
		        	<%	String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
		        		if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){%>
		        		<ww:textfield name="'newRwaValue'" size="10" maxlength="10" cssClass="'txt'" value="rwa.rwa"/>
		        	<%  }else{ %>
		        		<ww:textfield name="'newRwaValue'" size="5" maxlength="5" cssClass="'txt'" value="rwa.rwa"/>
		        	<%  } %>
			        poziom
			        <span class="bold">
			        	<ww:property value="rwa.level"/>
			        </span>			        			       
			    </td>
			</tr>
			<tr>
				<td>
					Obowi�zuj�cy
				</td>
				<td>
					<ww:checkbox name="'isActive'" value="rwa.isActive" fieldValue="'true'"></ww:checkbox>
				</td>
			</tr>
			<tr>
			    <td>
			    	Opis:
			    </td>
			    <td>
			    	<ww:textfield name="'description'" value="rwa.description" size="52" maxlength="300" cssClass="'txt'"/>
			    </td>
			</tr>
			<tr>
			    <td>
			    	Kategoria archiwalna:
			    </td>
			    <td>
			        kom�rki macierzyste:
			        <%	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){%>
				        <ww:textfield name="'acHome'" size="10" maxlength="10" cssClass="'txt'" value="rwa.acHome"/>
				        kom�rki inne:
				        <ww:textfield name="'acOther'" size="10" maxlength="10" cssClass="'txt'" value="rwa.acOther"/>
			        <%  }else{ %>
				        <ww:textfield name="'acHome'" size="5" maxlength="5" cssClass="'txt'" value="rwa.acHome"/>
				        kom�rki inne:
				        <ww:textfield name="'acOther'" size="5" maxlength="5" cssClass="'txt'" value="rwa.acOther"/>
			        <%  } %>
			    </td>
			</tr>
			<tr>
			    <td>
			    	Uwagi:
			    </td>
			    <td>
			        <ww:textarea name="'remarks'" rows="5" cols="52" cssClass="'txt'" value="rwa.remarks"/>
			    </td>
			</tr>
		</table>
		
		<input type="submit" name="doUpdate" value="Zapisz zmiany" class="btn" <ww:if test="!canModify">disabled="true"</ww:if>/>
        <input type="submit" name="doDelete" value="Usu� kategori�" class="btn"/>
	</ww:if>

	<ww:if test="rwa == null || rwa.level < 5">
		<h4>Nowa podkategoria na poziomie <ww:property value="rwa==null ? 1 : rwa.level+1"/></h4>

		<table class="table100p">
			<tr>
			    <td>
			    	Kod:
			    </td>
			    <td>
				    <%	String iloscCyfrNaPoziom = Docusafe.getAdditionProperty("rwa.iloscCyfrNaPoziom");
				    	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){%>
			    			<ww:textfield name="'newRwaCode'" size="10" maxlength="10" cssClass="'txt'" value="newRwaCode == null ? rwa.rwa + 'xx' : newRwaCode"/>
			    	<%} else{    %>
			    			<ww:textfield name="'newRwaCode'" size="5" maxlength="5" cssClass="'txt'" value="newRwaCode == null ? rwa.rwa + 'x' : newRwaCode"/>
			    	<%	} 	 %>
			    </td>
			</tr>
			<tr>
			    <td>
			    	Opis:
			    </td>
			    <td>
			    	<ww:textfield name="'newDescription'" value="newDescription" size="52" maxlength="250" cssClass="'txt'"/>
			    </td>
			</tr>
			<tr>
			    <td>
			    	Kategoria archiwalna:
			    </td>
			    <td>
		        	<%	if(iloscCyfrNaPoziom!=null && iloscCyfrNaPoziom.equals("2")){%>
			        	kom�rki macierzyste:
				        <ww:textfield name="'newAcHome'" size="10" maxlength="10" cssClass="'txt'" value="newAcHome"/>
				        kom�rki inne:
				        <ww:textfield name="'newAcOther'" size="10" maxlength="10" cssClass="'txt'" value="newAcOther"/>
		        	<%  }else{ %>
			        	kom�rki macierzyste:
				        <ww:textfield name="'newAcHome'" size="5" maxlength="5" cssClass="'txt'" value="newAcHome"/>
				        kom�rki inne:
				        <ww:textfield name="'newAcOther'" size="5" maxlength="5" cssClass="'txt'" value="newAcOther"/>
		        	<%  } %>
			        
			    </td>
			</tr>
			<tr>
			    <td>
			    	Uwagi:
			    </td>
			    <td>
			        <ww:textarea name="'newRemarks'" rows="5" cols="52" cssClass="'txt'" value="newRemarks"/>
			    </td>
			</tr>
		</table>
		
		<input type="submit" name="doCreate" value=<ds:lang text="Dodaj"/> class="btn" <ww:if test="!canModify">disabled="true"</ww:if>/>
	</ww:if>

	<ww:if test="rwaUpdateFromFile">
		<h4>Aktualizacja RWA:</h4>
		
		Plik z wykazem akt:
		<ww:file name="'updateRwa'" size="50" cssClass="'txt'"/>
		
		<br/>
		<ds:submit-event value="'Wyslij'" name="'doUpdateRwa'" cssClass="'btn'"/>
	</ww:if>
</form>

<%--
<h1>Rzeczowy Wykaz Akt</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:property value="treeHtml" escape="false" />


<form action="<ww:url value="'/office/rwa/edit-rwa.action'"/>" method="post" enctype="multipart/form-data">
<ww:hidden name="'id'" value="id"/>

<ww:if test="rwa != null">

<h4>Edycja bie��cej kategorii</h4>

<table width="100%">
<tr>
    <td>Kod:</td>
		<!--      <td><b><ww:property value="rwa.rwa"/></b>   -->
        <td><ww:textfield name="'newRwaValue'" size="5" maxlength="5" cssClass="'txt'" value="rwa.rwa"/>		
        &nbsp; poziom <b><ww:property value="rwa.level"/></b>
    </td>
</tr>
<tr>
    <td>Opis:</td>
    <td><ww:textfield name="'description'" value="rwa.description" size="52" maxlength="128" cssClass="'txt'"/></td>
</tr>
<tr>
    <td>Kategoria archiwalna:</td>
    <td>
        kom�rki macierzyste: <ww:textfield name="'acHome'" size="5" maxlength="5" cssClass="'txt'" value="rwa.acHome"/>
        kom�rki inne: <ww:textfield name="'acOther'" size="5" maxlength="5" cssClass="'txt'" value="rwa.acOther"/>
    </td>
</tr>
<tr>
    <td>Uwagi:</td>
    <td>
        <ww:textarea name="'remarks'" rows="5" cols="52" cssClass="'txt'" value="rwa.remarks"/>
    </td>
</tr>
<tr>
    <td></td>
    <td>
        <input type="submit" name="doUpdate" value="Zapisz zmiany" class="btn" <ww:if test="!canModify">disabled="true"</ww:if>/>
        <input type="submit" name="doDelete" value="Usu� kategori�" class="btn"/>
    </td>
</tr>
</table>

</ww:if>

<ww:if test="rwa == null || rwa.level < 5">

<h4>Nowa podkategoria na poziomie <ww:property value="rwa==null ? 1 : rwa.level+1"/></h4>
<table width="100%">
<tr>
    <td>Kod:</td>
    <td><ww:textfield name="'newRwaCode'" size="5" maxlength="5" cssClass="'txt'" value="newRwaCode == null ? rwa.rwa + 'x' : newRwaCode"/></td>
</tr>
<tr>
    <td>Opis:</td>
    <td><ww:textfield name="'newDescription'" value="newDescription" size="52" maxlength="128" cssClass="'txt'"/></td>
</tr>
<tr>
    <td>Kategoria archiwalna:</td>
    <td>
        kom�rki macierzyste: <ww:textfield name="'newAcHome'" size="5" maxlength="5" cssClass="'txt'" value="newAcHome"/>
        kom�rki inne: <ww:textfield name="'newAcOther'" size="5" maxlength="5" cssClass="'txt'" value="newAcOther"/>
    </td>
</tr>
<tr>
    <td>Uwagi:</td>
    <td>
        <ww:textarea name="'newRemarks'" rows="5" cols="52" cssClass="'txt'" value="newRemarks"/>
    </td>
</tr>
<tr>
    <td></td>
    <td>
        <input type="submit" name="doCreate" value=<ds:lang text="Dodaj"/> class="btn" <ww:if test="!canModify">disabled="true"</ww:if>/>
    </td>
</tr>
</table>

</ww:if>

<ww:if test="rwaUpdateFromFile">

<h4>Aktualizacja RWA:</h4>
<table>
    <tr>
        <td>Plik z wykazem akt:</td>
        <td><ww:file name="'updateRwa'" size="50" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td>
            <ds:submit-event value="'Wyslij'" name="'doUpdateRwa'" cssClass="'btn'"/>
        </td>
    </tr>
</table>

</ww:if>
</form>
--%>
<!--N koniec edit-rwa.jsp N-->