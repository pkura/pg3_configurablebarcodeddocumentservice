<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N pick-division.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<input type="button" class="btn" value="<ds:lang text="Wybierz"/>" onclick="pick()" <ww:if test="!canPick">disabled="true"</ww:if> /><br/>

<ww:property value="treeHtml" escape="false" />

<script>
function pick()
{
    if (window.opener.pickDivision)
    {
        if (!window.opener.pickDivision('<ww:property value="reference"/>', '<ww:property value="guid"/>', '<ww:property value="prettyPath" escape="false"/>'))
        {
            
            window.focus();
        }
    }
    else if(window.opener.document.getElementById('id.guid') != null )
    {
        alert('Ustawilem');
        window.opener.document.getElementById('id.guid').value = '<ww:property value="guid"/>';
        window.opener.document.getElementById('id.guid.pretty').value = '<ww:property value="prettyPath" escape="false"/>';
    }
    window.close();
}
</script>
