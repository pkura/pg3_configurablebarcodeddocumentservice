<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N case-precreate.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<b>
    <ds:ww-action-messages />
</b>

<form action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);" id="form">
    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="doPreCreate" id="doPreCreate"/>
    <input type="hidden" name="doAddToCase" id="doAddToCase"/>
    <input type="hidden" name="caseId" id="caseId"/>
    <ww:hidden name="'record'"/>
    <ww:hidden name="'portfolioId'" id="portfolioId"/>
	<ww:hidden name="'description2'" value ="description2"/>
	<ww:hidden name="'caseTitle'" value ="caseTitle"/>
    <ww:hidden name="'caseFinishDate'" value="caseFinishDate"/>
    <ww:hidden name="'clerk'" value="clerk"/>
    <ww:hidden name="'priority'" value="priority"/>

    <ww:hidden name="'officeIdPrefix'" value="officeIdPrefix"/>
    <ww:hidden name="'officeIdMiddle'" value="officeIdMiddle"/>
    <ww:hidden name="'officeIdSuffix'" value="officeIdSuffix"/>
    <ww:hidden name="'suggestedOfficeId'" value="suggestedOfficeId"/>
    <ww:hidden name="'suggestedSequenceId'" value="suggestedSequenceId"/>


    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
    <ww:hidden name="'freeCaseNumbers'" id="freeCaseNumbers" value="freeCaseNumbers"/>
    <ww:token />

<p>Zamierzasz za�o�y� now� spraw� i nada� jej znak sprawy  <ww:property value="suggestedOfficeId"/>
na pozycji <ww:property value="suggestedSequenceId"/>. Je�li chcesz zmieni� automatycznie nadany znak sprawy <a href="javascript:void(customOfficeId())">kliknij tu</a>.</p>

<ww:if test="freeCaseNumbersText != null">
	<ds:lang text="ZostalyWolneNumerySpraw" />: <ww:property value="freeCaseNumbersText"/> 
</ww:if>

<ww:if test="freeCaseNumbers != null">
	<ds:lang text="ZostalyWolneNumerySpraw" />: <ww:property value="freeCaseNumbers"/>
		<script language="JavaScript">
		if(confirm('<ds:lang text="ZostalyWolneNumerySpraw" />: <ww:property value="freeCaseNumbers"/> Czy jeste\u015B pewien, \u017Ce chcesz za\u0142o\u017Cy\u0107 spraw\u0119 na pozycji <ww:property value="customSequenceId"/>?')){
			document.getElementById('doCreate').value = 'true';
		}
		else{
			document.getElementById('doPreCreate').value = 'true';
		}
		form.submit();
	</script>
</ww:if>
<div id="breakdown" style="display: none">
    <ww:hidden name="'customOfficeId'" id="customOfficeId"/> <!-- bool -->
    Lp: <ww:textfield name="'customSequenceId'" value="customSequenceId" cssClass="'txt'" size="4" maxlength="6"/>.
    &nbsp;
    <ww:textfield name="'customOfficeIdPrefix'" value="customOfficeIdPrefix" cssClass="'txt'" size="6" maxlength="20" />
    <ww:textfield name="'customOfficeIdPrefixSeparator'" value="customOfficeIdPrefixSeparator" cssClass="'txt'" size="1" maxlength="1" />
    <ww:textfield name="'customOfficeIdMiddle'" value="customOfficeIdMiddle" cssClass="'txt'" size="15" maxlength="50"/>
    <ww:textfield name="'customOfficeIdSuffixSeparator'" value="customOfficeIdSuffixSeparator" cssClass="'txt'" size="1" maxlength="1" />
    <ww:textfield name="'customOfficeIdSuffix'" value="customOfficeIdSuffix" cssClass="'txt'" size="6" maxlength="20" />
</div>

<p>Je�li akceptujesz znak sprawy naci�nij przycisk "Utw�rz spraw�", a sprawa zostanie utworzona.</p>

<ds:submit-event value="'Utw�rz spraw�'" name="'doCreate'" cssClass="'btn'"  />

</form>

<script language="JavaScript">

function customOfficeId()
{
    toggle_div('breakdown', true);
    document.getElementById('customOfficeId').value = 'true';
}

function toggle_div(id, show)
{
    var div = document.getElementById(id);
    div.style.display = show ? '' : 'none';
}

</script>
