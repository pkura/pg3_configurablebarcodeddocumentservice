<%@ page contentType="application/json; charset=utf-8" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
[<ww:iterator value="divisions" status="status"><ww:if test="!#status.first">,</ww:if>{
	"recipientDivision":"<ww:property value="guid"/>",
 	"recipientDivisionName":"<ww:property value="name"/>" 
}</ww:iterator>]