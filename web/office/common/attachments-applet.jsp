<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<script>
    var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
        request.getServerName() + ":" + request.getServerPort() %>';
    // u�yty mo�e by� albo plik jtwain.jar znajduj�cy si� w aplikacji,
    // albo plik systemowy (w JRE_HOME/lib/ext)
    var object =  appletObject('scanapplet',
        'pl.compan.docusafe.applets.scanner.AttachmentsApplet.class',
        '<ww:url value="'/scanner.jar'"/>, <ww:if test="!useSystemJTwain"><ww:url value="'/jtwain-8.2.jar'"/>, </ww:if> <ww:url value="'/jai_core-1.1.2_01.jar'"/>, <ww:url value="'/jai_codec-1.1.2_01.jar'"/>', 700, 55,
        { 'uploadUrl' : host+'<ww:url value="'/office/incoming/attachments.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>',
          'activity' : '<ww:property value="activity"/>',
          'documentId' : '<ww:property value="documentId"/>' },
        new Array('xMSIE'));
    alert('object='+object);
    document.write(object);

    function closeWindow()
    {
        window.opener.location.href = '<ww:property value="openerUrl"/>';
        window.close();
    }
</script>

<input type="button" value="<ds:lang text="Zamknij"/>" onclick="closeWindow();" class="btn"/>
