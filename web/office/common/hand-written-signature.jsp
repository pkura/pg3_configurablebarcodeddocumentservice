<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-2"/>
<title>Odbi�r przesy�ek</title>
<script type="text/javascript" src="<ww:url value="'/functions.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/comet.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/jquery-1.7.1.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/jquery-ui-1.8.custom.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/knockout-2.2.0.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/sketch.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/server-logger.js'" />"></script>
</head>

<body class="ds-logo">
<button id="fullscreen-button" onclick="document.documentElement.mozRequestFullScreen()">Tryb pe�noekranowy</button>
<div id="step1" class="ds-logo">
	<h1>Odbi�r przesy�ek</h1>
	<div id="container-lastname">
		<input type="hidden" id="login" />
		<button id="clear-lastname-button" disabled="disabled">&#10799;</button>
		<input type="text" id="lastname" name="lastname" autofocus="autofocus" placeholder="wpisz nazwisko odbiorcy" autocomplete="off"/>
		<button id="begin-button" disabled="disabled" >OK</button>
		
		<jsp:include page="/vkeyboard/vkeyboard.jsp"/>
		
	</div>
</div>

<div id="step2" data-bind="css: {'ajax-loader-big': documents().length < 1}">
	<h1><button id="restart">Rozpocznij nowy odbi�r</button><span id="registered-username"></span></h1>
	<table cellspacing="0" width="100%" class="document-reception">
		<%--<caption>Liczba wprowadzonych dokument�w: <b data-bind="text: documents().length"></b></caption> --%>
		<thead>
			<tr>
				<th class="empty ordinal-th">Lp</th>
				<th class="empty barcode-th">Barkod</th>
				<th class="empty type-th">Dostarczenie</th>
				<th class="empty sender-th">Nadawca</th>
				<th class="empty recipient-th">Adresat</th>
				<th class="empty date-th">Data</th>
				<th class="empty remove-th">&nbsp;</th>
			</tr>
		</thead>
		<tbody data-bind="if: documents().length < 1">
			<tr><td colspan="7" class="no-barcodes-present">Nie wprowadzono �adnego barkodu</td></tr>
		</tbody>
		<tbody data-bind="foreach: pagedList">
			<tr>
				<td class="ordinal-th"><div data-bind="text: $index()+1 + $parent.pageSize()*$parent.pageIndex()"></div></td>
				<td class="barcode-th"><div data-bind="text: barcode"></div></td>
				<td class="type-th"><div data-bind="text: delivery"></div></td>
				<td class="sender-th"><div data-bind="text: sender"></div></td>
				<td class="recipient-th"><div data-bind="text: recipient"></div></td>
				<td class="date-th"><div data-bind="text: formattedDate"></div></td>
				<td class="remove-th">
					<!-- <input type="button" class="remove-button" data-bind="click: $root.removeDocument" value="usu�" /> -->
				</td>
			</tr>
		</tbody>
		<tfoot data-bind="if: documents().length > pageSize()">
			<tr>
				<td colspan="7">
					<a href="#" data-bind="css: {disabled: pageIndex() < 1}, click: prevPage">&#65513;</a>
					<select data-bind="options: pageSelect, optionsText: 'label', optionsValue: 'value', value: pageIndex"></select>
					<a href="#" data-bind="css: {disabled: pageIndex() >= maxPageIndex()}, click: nextPage">&#65515;</a>
				</td>
			</tr>
		</tfoot>
	</table>
	<div id="signature-container" data-bind="css: {'empty-canvas': $root.emptySignature()}">
		<button id="clear-button" data-bind="enable: !$root.emptySignature()"><span>Wyczy��</span></button>
		<canvas id="canvas" width="1280" height="260" ></canvas> 
		<button id="confirm-button" data-bind="enable: $root.confirmButtonEnabled()">Potwierd�<br />odbi�r</button>
	</div>
</div>

<div id="confirm-restart-dialog" title="Rozpocznij nowy odbi�r">
	Rozpocz�cie nowego odbioru spowoduje wykasowanie wszystkich wprowadzonych barkod�w. <br />
</div>

<div id="confirm-accept-dialog" title="Oczekiwanie na akceptacj� podpisu" >

</div>


<div id="confirm-info-dialog" title="Wys�ano potwierdzenie odbioru">
	<div id="confirm-info-name"></div>
	<img id="confirm-info-signature-image" src="#" />
</div>

<div id="reject-info-dialog" title="Anulowano odbi�r">
	Odbi�r przesy�ek zosta� anulowany<br/>przez pracownika kancelaryjnego.
</div>

</body>

<link rel="stylesheet" type="text/css" href="<ww:url value="'/jquery-ui-1.8.custom.css'" />" />
<link rel="stylesheet" type="text/css" href="<ww:url value="'/hand-written-signature.css'" />" />

<script type="text/javascript">
var DBG_STEP = null;
var ACTION_URL = '<ww:url value="'/office/common/hand-written-signature.action'" />';
var docViewModel = null,
	notifier = null,
	rejectionNotifier = null,
	acceptNotifier = null,
	emptySignature = true;

var handWrittenSignatureToken = "";

function checkSession() {
	$.post(ACTION_URL, {checkSession: true}, function(data) {
		if(data != 'OK') {
			//window.onbeforeunload = null;
			window.onunload = null;
			window.location.href = '<ww:url value="'/'" />';	
		}
	});
}
	
function clearSignature() {
	var jqCanvas = $('#canvas'),
		ctx = jqCanvas[0].getContext('2d');
	
	ctx.fillStyle = 'white';
	ctx.fillRect(0, 0, jqCanvas[0].width, jqCanvas[0].height);
	ctx.fillStyle = 'black;'
	
	jqCanvas.sketch('actions', []);
	try {
		docViewModel.emptySignature(true);
	} catch(e) {
		
	}
}

//Returns contents of a canvas as a png based data url, with the specified
//background color
function canvasToImage(backgroundColor)
{
	var canvas = $('#canvas')[0],
		context = canvas.getContext('2d');
	//cache height and width		
	var w = canvas.width;
	var h = canvas.height;

	var data;		

	if(backgroundColor)
	{
		//get the current ImageData for the canvas.
		data = context.getImageData(0, 0, w, h);

		//store the current globalCompositeOperation
		var compositeOperation = context.globalCompositeOperation;

		//set to draw behind current content
		context.globalCompositeOperation = "destination-over";

		//set background color
		context.fillStyle = backgroundColor;

		//draw background / rect on entire canvas
		context.fillRect(0,0,w,h);
	}

	//get the image data from the canvas
	var imageData = canvas.toDataURL("image/png");

	if(backgroundColor)
	{
		//clear the canvas
		context.clearRect (0,0,w,h);

		//restore it with original / cached ImageData
		context.putImageData(data, 0,0);		

		//reset the globalCompositeOperation to what it was
		context.globalCompositeOperation = compositeOperation;
	}

	//return the Base64 encoded data url string
	return imageData;
}

function fixFontSize(jqRow) {
	jqRow.find('div').each(function() {
	    var h = $(this).height(),
	        curFontSize = parseFloat($(this).css('font-size')),
	        scaleFactor = 1;
	    
	
	        while(h > 36) {
	            h = $(this).height();
	            curFontSize = parseFloat($(this).css('font-size'));
	            $(this).css('font-size', (curFontSize-1) + 'px');
	        }
	    
	});
}

$(document).ready(function() {
	$('#write').bind('change.vkeyboard', function() {
		$('#lastname').val($(this).html()).autocomplete('search').focus();
		$('#begin-button').attr('disabled', 'disabled');
		
		//console.info($('#lastname').val());
		
		if($('#lastname').val() != '') {
			$('#clear-lastname-button').removeAttr('disabled');
		} else {
			$('#clear-lastname-button').attr('disabled', 'disabled');
		}
	});
	
	$.post(ACTION_URL, {getUserList: true}, function(data) {
		try {
			var ret = $.parseJSON(data);
			if(ret.success == false) {
				alert(ret.errorMessage);
				
				var error = new Error("$.parseJSON(data) == false");
				serverLogger.error(error, {"name":"jsonParseError","errorMessage":ret.errorMessage,"data":data});
				
				return;
			}
			
			var users = ret.users; tmp100 = users;
			var i, len;
			
			for(i = 0, len = users.length; i < len; i ++) {
				users[i].label = users[i].lastname + ' ' + users[i].firstname + ' (' + users[i].login + ')';
				users[i].value = users[i].label;
				users[i].normalizedLabel = users[i].normalizedLastname + ' ' + users[i].normalizedFirstname;
			}
			
			$('#lastname').autocomplete({
				source: function(request, response) {
					var i, len, usr, 
						res = [], 
						regExp = RegExp('^' + $.ui.autocomplete.escapeRegex(request.term), 'i');
					for(i = 0, len = users.length; i < len; i ++) {
						usr = users[i];
						
						if(usr.login.search(regExp) != -1 || usr.normalizedLabel.search(regExp) != -1 || usr.label.search(regExp) != -1) {
							res.push(usr);
						}
					}
					
					response(res);
				},
				select: function(event, ui) {
					$(event.target).val(ui.item.label);
					$('#login').val(ui.item.login);
					$('#write').html(ui.item.label);
					$('#begin-button').removeAttr('disabled');
				},
				close: function(event, ui) {
					if($('#login').val() == '') {
						$('#lastname').autocomplete('search');
					}
				}
			}).bind('keyup.login', function() {
				$('#login').val('');
				$('#begin-button').attr('disabled', 'disabled');
			});
		} catch(e) {
			serverLogger.error(e,{"name":"unknownError"});
			alert('Nieznany b��d');
			checkSession();
		}
	});
	
	$('#clear-lastname-button').click(function() {
		$('#write').html('');
		$('#login').val('');
		$('#lastname').val('');
		$('#begin-button').attr('disabled', 'disabled');
		$(this).attr('disabled', 'disabled');
		$('#lastname').autocomplete('close');
	})
	
	$('#begin-button').click(function() {
		if($('#login').val() == '') {
			alert('Nie podano u�ytkownika');
			return;
		}
		$.post(ACTION_URL, {
			registerRecipient: true,
			login: $('#login').val()
		}, function(data) {
			var res = null;
			try {
				res = JSON.parse(data);
			} catch(e) {
				serverLogger.error(e,{"name":"jsonParseError","data":data});
				alert('B��d parsowania JSON: ' + data);
				checkSession();
				//console.error(e);
				return;
			}
			if(res.success === true) {
				showSignatureForm({lastname: res.lastname, firstname: res.firstname});
				
				// save token
				handWrittenSignatureToken = res.token;
			}
		});
	});
	
	$('#restart').click(function() {
		$('#confirm-restart-dialog').dialog({
			modal: true,
			width: 1200,
			resizable: false,
			buttons: [
			{
				text: 'Rozpocznij nowy odbi�r',
				click: function() {
					$.post(ACTION_URL, {
						cancelReception: true,
						login: $('#login').val()
						
					}, function(data) {
						var ret = null;
						
						try {
							ret = $.parseJSON(data);
						} catch(e) {
							serverLogger.error(e,{"name":"jsonParseError","data":data});
							alert('B��d parsowania JSON: ' + data);
							checkSession();
							return;
						}
						
						if(ret.success === false) {
							var error = new Error("$.parseJSON(data) == false");
							serverLogger.error(error, {"name":"jsonParseError","errorMessage": ret.errorMessage, "data":data});

							alert(ret.errorMessage);
						}
					});
					$(this).dialog('close');
					showWelcomeForm();
				}
			},
			{
				text: 'Anuluj',
				click: function() {
					$(this).dialog('close');
				}
			}
			]
		})
	});
	
	$('#clear-button').click(function() {
		clearSignature();
	});
	
	$('#canvas').sketch();
	$('#canvas').bind('mousedown.drawing', function() {
		docViewModel.emptySignature(false);
	});
	
	$('#confirm-button').click(function() {
		var barcodes = [], i, len;
		var documentIds = [];
		for(i = 0, len = docViewModel.documents().length; i < len; i ++) {
			barcodes.push(docViewModel.documents()[i].barcode);
			documentIds.push(docViewModel.documents()[i].documentId);
		}
		
		var reception = {
			//barcodes: barcodes,
			//documentIds: documentIds,
			login: $('#login').val(),
			token: handWrittenSignatureToken,
			base64img: canvasToImage('white')
		};
		
		$('#confirm-accept-dialog').dialog({
			modal: true,
			width: 1000,
			resizable: false,

			buttons: [
			{
				text: 'ANULUJ ODBI�R',
				click: function()
				{
					$.post(ACTION_URL,
					{
						cancelReception: true,
						login: $('#login').val()
						
					},
					function(data)
					{
						var ret = null;
						
						try
						{
							ret = $.parseJSON(data);
						} catch(e) {
							serverLogger.error(e,{"name":"jsonParseError","data":data});
							alert('B��d parsowania JSON: ' + data);
							checkSession();
							return;
						}
						
						if(ret.success === false)
						{
							var error = new Error("$.parseJSON(data) == false");
							serverLogger.error(error, {"name":"jsonParseError","errorMessage": ret.errorMessage, "data":data});

							alert(ret.errorMessage);
						}
					});
					$(this).dialog('close');
					showWelcomeForm();
				},
				css: { "margin" : "-100px auto 0 auto",
				       "display" : "block",
				       "text-align" : "center",
				       "float" : "none",
				       "background" : "none repeat scroll 0 0 red",
				       "box-shadow" : "none",
				       "font-size" : "0.6em",
				       "height" : "100px",
				       "width" : "500px"}
			}
			]
		});
		
		$.post(ACTION_URL, {
			confirmReception: true,
			json: JSON.stringify(reception)
		}, function(data) {
			var ret = null;
			
			try {
				ret = $.parseJSON(data);
			} catch(e) {
				serverLogger.error(e,{"name": "jsonParseError","data": data});

				//alert('B��d parsowania JSON: ' + data);
				checkSession();
				//return;
			}
			
			if(ret.success === false){
				var error = new Error("$.parseJSON(data) == false");
				serverLogger.error(error, {"name":"jsonParseError","errorMessage":ret.errorMessage,"data":data});

				if(ret.errorMessage == "invalidToken")
				{
					return;
				}
				else
				{
					alert(ret.errorMessage);
				}
			}
			/*
			$('#confirm-info-signature-image').attr('src', reception.base64img);
			$('#confirm-info-name').html($('#registered-username').html());
			
			$('#confirm-info-dialog').dialog({
				modal: true,
				width: 1500,
				height: 730,
				resizable: false,
				buttons: [
				{
					text: 'Rozpocznij nowy odbi�r',
					click: function() {
						$(this).dialog('close');
						showWelcomeForm();
					}
				}
				]
			});*/
		});
	});
	
	
	if(DBG_STEP === 2) {
		showSignatureForm({lastname: 'Kirpsza', firstname: 'Sylwia'});
		$('#login').val('skirpsza');
	}
});

function showWelcomeForm() {
	$('#step1').removeClass('hidden-step1');
	$('#lastname').focus().val('');
	$('#login').val('');
	$('#write').html('').val('');
}

function showSignatureForm(recipient) {
	$('#step1').addClass('hidden-step1');
	$('#registered-username').html(recipient.firstname + ' ' + recipient.lastname);
	$('#begin-button, #clear-lastname-button').attr('disabled', 'disabled');
	clearSignature();
	
	if(docViewModel == null) {
		docViewModel = new DocumentsViewModel();
		ko.applyBindings(docViewModel);
		docViewModel.pageIndex.subscribe(function(newVal) {
			fixFontSize($('.document-reception tbody:last tr'));
		});
		docViewModel.documents.subscribe(function() {
			docViewModel.pageIndex(docViewModel.maxPageIndex());
		});
	} else {
		docViewModel.documents.removeAll();
	}
	
	if(acceptNotifier == null) {
		acceptNotifier = new CometRequest({
			url: '<ww:url value="'/comet-notifier.ajx'" />' + '?MESSAGE_KIND=SIGNATURE_REQUEST',
			callback: function (str, params) {
				if(params.state == 'complete') {
					acceptNotifier.init();
					return;
				}
				
				var strMsgs = str.split('\r\n'),
					message = null,	lastMessage = null, 
					i = strMsgs.length-1, strJson = null,
					notification = null;
				console.info(strMsgs);
				while(i--) {
					strJson = strMsgs[i];
					if(strJson == '') {
						continue;
					}
					try {
						message = $.parseJSON(strJson);
					} catch(e) {
						console.error(e);
						//alert('B��d parsowanie JSON: ' + str);
					}
					
					if(lastMessage == null) {
						lastMessage = message;
					} else {
						switch(message.status) {
						case 'ACCEPT':
							var notification = 'OK';
						break;
						}
						
						//$j.ambiance({message: notification, type: 'success', timeout: 5, fade: true});
					}
				}

				if(lastMessage != null) {
					login = lastMessage.login;
					switch(lastMessage.status) {
					case 'ACCEPT':
						$('#confirm-accept-dialog').dialog('close');
						var reception = {
							//barcodes: barcodes,
							//documentIds: documentIds,
							login: $('#login').val(),
							token: handWrittenSignatureToken,
							base64img: canvasToImage('white')
						};
						
						$.post(ACTION_URL, {
							saveReception: true,
							json: JSON.stringify(reception)
						}, function(data) {
							var ret = null;
							
							try {
								ret = $.parseJSON(data);
							} catch(e) {
								serverLogger.error(e,{"name": "jsonParseError","data": data});

								//alert('B��d parsowania JSON: ' + data);
								checkSession();
								//return;
							}
							
							if(ret.success === false){
								var error = new Error("$.parseJSON(data) == false");
								serverLogger.error(error, {"name":"jsonParseError","errorMessage":ret.errorMessage,"data":data});

								if(ret.errorMessage == "invalidToken")
								{
									return;
								}
								else
								{
									alert(ret.errorMessage);
								}
							}
							
							$('#hand-written-notifier-confirm').dialog('close');
							
							$('#confirm-info-signature-image').attr('src', reception.base64img);
							$('#confirm-info-name').html($('#registered-username').html());
							
							$('#confirm-info-dialog').dialog({
								modal: true,
								width: 1500,
								height: 730,
								resizable: false,
								buttons: [
								{
									text: 'Rozpocznij nowy odbi�r',
									click: function() {
										$(this).dialog('close');
										showWelcomeForm();
									}
								}
								]
							});
						});
					break;
					case 'CORRECT':				
						$('#confirm-accept-dialog').dialog('close');
						clearSignature();
						$.post(ACTION_URL, {
							resetToken: true
						}, function(data) {
							
							
							handWrittenSignatureToken = data;
						}
						);
					break;
					}
				}
				
			},
			error: function() {
				checkSession();
			}
		});
		acceptNotifier.init();
	}
	
	if(notifier == null) {
		notifier = new CometRequest({
			url: '<ww:url value="'/comet-notifier.ajx'" />' + '?MESSAGE_KIND=BARCODE_RECIPIENT',
			callback: function(str, params) {
				if(params.state == 'complete') {
					notifier.init();
					return;
				}
				
				var strs = str.split('\r\n'), len = strs.length,
					strJson = null, i = 0, messages = [], msg = null;
				
				for(i = 0; i < len; i ++) {
					strJson = strs[i];
					if(strJson == '') {
						continue;
					}
					
					try {
						messages.push($.parseJSON(strJson));
					} catch(e) {
						//alert('B��d parsowania JSON ' + str);
						serverLogger.error(e,{"name": "jsonParseError","data": strJson});
						checkSession();
						//console.error(e);
					}
				}
				
				var docMetric = null, doc = null;
				for(i = 0, len = messages.length; i < len; i ++) {
					msg = messages[i];		
					switch(msg.barcodeOperation) {
					case 'ADD':
						docMetric = msg.documentMetric;
						doc = new Document(docMetric.documentId, docMetric.barcode, docMetric.delivery, docMetric.sender, docMetric.recipient, docMetric.date);
						docViewModel.documents.push(doc);
						fixFontSize($('.document-reception tbody:last tr:last'));
					break;
					case 'REMOVE':
					default:
						docViewModel.documents.remove(function(item) {
							return item.barcode == msg.barcode;
						});
					break;
					}
				}
			},
			error: function() {
				checkSession();
			}
		});
	
		notifier.init();
	}
	
	if(rejectionNotifier == null) {
		rejectionNotifier = new CometRequest({
			url: '<ww:url value="'/comet-notifier.ajx'" />' + '?MESSAGE_KIND=SIGNATURE_REQUEST_REJECT',
			autoclose: true,
			callback: function(str, params) {
				var strMsgs = str.split('\r\n'),
					message = null,	lastMessage = null, 
					i = strMsgs.length-1, strJson = null;
				
				while(i--) {
					strJson = strMsgs[i];
					if(strJson == '')
						continue;
					
					try {
						message = $.parseJSON(strJson);
					} catch(e) {
						serverLogger.error(e,{"name": "jsonParseError","data": strJson});
						//console.error(e);
						//alert('B��d parsowanie JSON: ' + str);
					}
					
					if(lastMessage == null) {
						lastMessage = message;
					}
				}
				
				var ret = lastMessage;
				
				if(ret != null && ret.status == 'CANCEL') {
					$('#reject-info-dialog').dialog({
						modal: true,
						resizable: false,
						width: 900,
						buttons: [
						{
							text: 'Rozpocznij nowy odbi�r',
							click: function() {
								$(this).dialog('close');
								showWelcomeForm();
							}
						}
						]
					})
				}
				
				if(params.state == 'complete') {
					rejectionNotifier.init();
					return;
				}
			},
			error: function() {
				checkSession();
			}
		});
		
		rejectionNotifier.init();
	}
	
	if(DBG_STEP == 2) {
		var i = 30, brc;
		
		while((i--) > 20) {
			brc = (new Array(13)).join(Math.random() % 10 * 10 >> 0);
			docViewModel.documents.push(new Document(i, brc + '', 'poczta', 'Andrzej Nowek / Wirecka 34/4 / Chorz�w', 
					'�ukasz Dobrzy�ski / Dzia� IT Shop Rollout and is User Support', (new Date()).getTime()));
			
			fixFontSize($('.document-reception tbody:last tr:last'));
		}
		
		while((i--) > 10) {
			brc = (new Array(13)).join(Math.random() % 10 * 10 >> 0);
			docViewModel.documents.push(new Document(i, brc + '', 'poczta', 'Andrzej Nowek / Wirecka 34/4 / Chorz�w / co�tam / bardzo d�ugi napis m�j w kliku', 
					'�ukasz Dobrzy�ski / Dzia� IT Shop Rollout and is User Support i to jest jeszcze jedna linijka', (new Date()).getTime()));
			
			fixFontSize($('.document-reception tbody:last tr:last'));
		}
		
		while((i--) > 0) {
			brc = (new Array(13)).join(Math.random() % 10 * 10 >> 0);
			docViewModel.documents.push(new Document(i, brc + '', 'poczta', 'Kamilos', 
					'�ukasz Dobrzy�ski / Kr�tki', (new Date()).getTime()));
			
			fixFontSize($('.document-reception tbody:last tr:last'));
		}
	}
}

function Document(documentId, barcode, delivery, sender, recipient, date) {
	var self = this;
	
	self.documentId = documentId;
	self.barcode = barcode;
	self.delivery = delivery;
	self.sender = sender;
	self.recipient = recipient;
	self.date = date;
	self.formattedDate = ko.computed(function() {
		var strDate = '';
		try {
			strDate = (new Date(self.date)).format('dd-mm-yyyy');
		} catch(e) {}
		
		return strDate;
	});
}

function DocumentsViewModel() {
	var self = this;
	
	self.documents = ko.observableArray([]);
	self.pageSize = ko.observable(10);
	self.pageIndex = ko.observable(0);
	self.maxPageIndex = ko.computed(function() {
		return Math.ceil(self.documents().length / self.pageSize()) - 1;
	});
	self.emptySignature = ko.observable(true);
	
	self.pageSelect = ko.computed(function() {
		var i, options = [];
		for(i = 0; i <= self.maxPageIndex(); i ++) {
			options.push({
				label: (i+1),
				value: i
			});
		}
		return options;
	});
	
	self.confirmButtonEnabled = ko.computed(function() {
		return (self.documents().length > 0) && (self.emptySignature() != true);
	});
	
	self.pagedList = ko.computed(function() {
		var size = self.pageSize();
		var start = self.pageIndex() * size;
		
		return self.documents.slice(start, start + size);
	});
	
	self.prevPage = function() {
		if(self.pageIndex() > 0) {
			self.pageIndex(self.pageIndex() - 1);
		}
	}
	
	self.nextPage = function() {
		if(self.pageIndex() < self.maxPageIndex()) {
			self.pageIndex(self.pageIndex() + 1);
		}
	}
	
	self.removeDocument = function(doc) {
		$.post(ACTION_URL, {removeBarcode: true, barcode: doc.barcode}, function(data) {
			
		});
		self.documents.remove(doc);
	}
}

</script>

<style type="text/css">
.ajax-loader-big {
	background-image: url('<ww:url value="'/img/ajax_loader_big.gif'" />');
}

.ds-logo {
	background-image: url('<ww:url value="'/img/logo-play-500.gif'" />');
}
</style>

</html>