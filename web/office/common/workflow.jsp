<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N workflow.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<style type="text/css">
	select#jbpmProcessId {width:22em;}
</style>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<script>
function openAssignmentWindow(divisionGuid, username, hasExecutePlanned, hasManualPush)
{

    window.open('<ww:url value="'/office/common/workflow-assignment.action'"/>?documentId=<ww:property value="documentId"/>&divisionGuid='+divisionGuid+'&hasExecutePlanned='+hasExecutePlanned+'&hasManualPush='+hasManualPush+(username != null ? '&username='+username : ''), 'assignmentwindow', 'width=500,height=400,resizable=yes,scrollbars=auto,status=no,toolbar=no,menu=no');
}

<%-- je�eli push=true, od razu doManualPush --%>
function __accept_assignment(divisionGuid, username, kind, objective, scope, push)
{
    var assignment = divisionGuid+'/'+username+'/'+kind+'/'+scope+'/'+objective;
    if (push)
    {
        document.getElementById('onePlannedAssignment').value = assignment;
        document.getElementById('doManualPush').value = 'true';
    }
    else
    {
        document.getElementById('newPlannedAssignment').value = assignment;
        document.getElementById('doPlanAssignment').value = 'true';
    }
    document.forms[0].submit();
}


</script>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'divisionGuid'" value="divisionGuid"/>
    <input type="hidden" name="newPlannedAssignment" id="newPlannedAssignment"/>

    <ww:hidden name="'onePlannedAssignment'" id="onePlannedAssignment"/>

    <ww:iterator value="plannedAssignments">
        <input type="hidden" name="plannedAssignments" value="<ww:property/>"/>
    </ww:iterator>

    <ww:hidden name="'doManualPush'" id="doManualPush"/>
    <ww:hidden name="'doManualPushToCoor'" id="doManualPushToCoor"/>
    <ww:hidden name="'doManualFinish'" id="doManualFinish"/>
    <ww:hidden name="'doCc'" id="doCc"/>
    <ww:hidden name="'doAcknowledgeCc'" id="doAcknowledgeCc"/>
    <ww:hidden name="'doPlanAssignment'" id="doPlanAssignment"/>
    <ww:hidden name="'doDelete'" id="doDelete"/>

<table width="100%">



<ww:if test="allowAssignment">
	<ds:available test="dekretacja.userlist">
	 <tr>
        <td>
			<div id="showTree">
				<input type="button" class="btn" value="Poka� drzewo" 
					onclick="$j('#organizationTree input').show();$j('#organizationTree').show('slow');$j('#showTree').hide();"
					style="margin-bottom: 10px;">
			</div>
        </td>
    </tr>
    </ds:available>
    <tr>
        <td>
			<div id="organizationTree">
				<ds:available test="dekretacja.userlist">
				<input type="button" class="btn" value="Ukryj drzewo" 
					onclick="$j('#organizationTree input').hide();$j('#organizationTree').hide('slow');$j('#showTree').show();"
					style="margin-bottom: 10px;">
				</ds:available>
				<ww:property value="treeHtml" escape="false"/>
			</div>
			
			<table>
			<ds:available test="dekretacja.userlist">
			<tr>
				<td>
					Lista u�ytkownik�w:
				</td>
			</tr>
			<tr class="formTableDisabled">
				<td>
					<ww:select cssClass="'sel'" name="'unimportantName'" list="allUsers" listKey="name" listValue="lastname+' '+firstname" id="selectedUser" size="10"/>
					<script  type="text/javascript">
						$j('#showTree').hide();
						function assignSelectedUser(){
							select = document.getElementById('selectedUser');
							userString = select.options[select.selectedIndex].value;
							openAssignmentWindow(null, userString, <ww:property value="hasExecutePlanned"/>,<ww:property value="hasManualPush"/> );
						}
		
						$j('document').ready(function() {
							<ww:if test="!plannedAssignmentBeans.empty">
								$j('#dekretuj_tree').attr('disabled', false);
							</ww:if>
							<ww:else>
								$j('#dekretuj_tree').attr('disabled', 'true');
							</ww:else>
							
							$j('#selectedUser').click(function() {
								$j('#dekretuj_list').attr('disabled', false);
							});
						});
					</script><br/>
					<input id="dekretuj_list" disabled="disabled" type="button" class="btn" value="Dekretuj" onclick="assignSelectedUser()" style="width: 204px;"/>
				</td>
			</tr>
			</ds:available>
			<ww:if test="targetDivision != null">
		    <tr>
		        <td>
		            <table width="100%">
		                <tr>
		                    <td width="50%" valign="top">
		                        <ds:lang text="PrzygotowaneDekretacje"/>: <br/>
		
		                        <ww:if test="!plannedAssignmentBeans.empty">
		                            <table>
		                                <ww:iterator value="plannedAssignmentBeans">
		                                    <tr>
		                                        <td valign="top"><ww:checkbox name="'plannedAssignmentIds'" fieldValue="id" /></td>
		                                        <td valign="top">
		                                            <b><ww:if test="user != null"><ww:property value="user.asFirstnameLastname()"/> <ds:lang text="w"/></ww:if>
		                                            <ww:property value="division.prettyPath"/></b>
		                                            <ds:lang text="celem"/> <b><ww:property value="objective"/></b> (<ww:property value="kindDescription"/>).
		                                            <ww:if test="user == null"><ww:property value="scopeDescription"/></ww:if>
		                                        </td>
		                                    </tr>
		                                </ww:iterator>
		                            </table>
		                        </ww:if>
		
		                    </td>
		                </tr>
		            </table>
		        </td>
		    </tr>
		    </ww:if>
		    
		    
			<tr>
			    <td>
			        <ww:if test="requireAcknowledge">
			            <input type="submit" name="doAcknowledgeCc" value="<ds:lang text="PotwierdzamPrzeczytanie"/>" class="btn" onclick="document.getElementById('doAcknowledgeCc').value='true';"/>
			        </ww:if>
			        
			        <div class="btnContainer" style="display: inline;">
					<ww:if test="allowAssignment">
				        <input id="dekretuj_tree" type="submit" name="doManualPush" value="<ds:lang text="Dekretuj"/>" class="btn" onclick="if (!checkSubstitutions()) return false; document.getElementById('doManualPush').value='true';"/>
				       	<ww:if test="pushToCoor">
				            <input type="submit" name="doManualPushToCoor" value="<ds:lang text="DekretujDoKoordynatora"/>" class="btn" onclick="if (!checkSubstitutions()) return false; document.getElementById('doManualPushToCoor').value='true';"/>
						</ww:if>
					</ww:if>
					
					<ww:if test="canReturnAssignment">
						<ds:submit-event value="getText('ZwrocDekretacje')" name="'doReturn'" disabled="!canReturnAssignment" />		
					</ww:if>
					</div>
					<ww:if test="!plannedAssignmentBeans.empty">
						<input type="submit" name="doDelete" class="btn" value="<ds:lang text="UsunZaznaczone"/>" onclick="document.getElementById('doDelete').value='true';"/>
					</ww:if>
					
					<ds:available test="archiwizacja.obserwowane">
					<ds:modules test="office or fax">
						<ds:event name="'doWatch'" value="getText('DodajDoObserwowanych')" />
					</ds:modules>
					</ds:available>
			
					<ww:if test="!requireAcknowledge">
			           <input type="submit" name="doManualFinish" value="<ww:if test="'order'.equals(document.getStringType())"><ds:lang text="ZakonczPraceZpoleceniem"/></ww:if><ww:else><ds:lang text="ZakonczPraceZdokumentem"/></ww:else>" class="btn" onclick="return confirmFinish();" <ww:if test="!canFinish">disabled="disabled"</ww:if>/>
			        	 <!--  <input type="submit" name="doManualFinish" value="<ds:lang text="ZakonczPraceZ"/>" class="btn" onclick="return confirmFinish();" <ww:if test="!canFinish">disabled="disabled"</ww:if>/> -->
			        </ww:if>
			
					<!-- 
					<ds:available test="tasklist.manualToCoordinator">
							<ds:event value="getText('Przeka� do koordynatora')" cssClass="'btn'" name="doManualPushToCoord"/>
					</ds:available>
					 -->
						
			        <ds:modules test="office or fax">
						<ww:if test="!externalWorkflow && canReopenWf">
							<ds:event name="'doReopenWf'" value="getText('PrzywrocNaListeZadan')" disabled="!canReopenWf" />
						</ww:if>
					</ds:modules>
			
			    </td>
			</tr>
		    </table>
        </td>
    </tr>
</ww:if>



</table>

<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>



<script>
    function confirmFinish()
    {
    <ww:if test="!finishOrAssignState.canFinish">
        alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
        return false;
    </ww:if>
    <ww:if test="!finishOrAssignState.finishWarnings.empty">
        if (!confirm('<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/>\n\n <ds:lang text="ZakonczycPraceZdokumentem"/> ?'))
            return false;
    </ww:if>
    <ww:else>
        if (!confirm('<ds:lang text="NaPewnoZakonczyc"/>?'))
            return false;
    </ww:else>

        document.getElementById('doManualFinish').value='true';

        return true;
    }
	 
	function checkSubstitutions()
    {
    var count=0;
    var show=0;
    assignments=document.forms[0].plannedAssignmentIds;
    var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
    var str1;
    try{
    <ww:iterator value="substituted" status="status">
    	
    	if(assignments[count].checked && "<ww:property value="value"/>"!="") {
    		show=show+1;
    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
    		str=str+str1;
    	}
    	count=count+1;
    </ww:iterator>
    }catch(err){
    <ww:iterator value="substituted" status="status">
    	
    	if(assignments.checked && "<ww:property value="value"/>"!="") {
    		show=show+1;
    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
    		str=str+str1;
    	}
    </ww:iterator>
    }
    if(show == 0){
    	if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
    	return false;
    }
    else{
    	if(!confirm(str))
    	return false;
  	}
    
    
    return true;
    
    }

</script>
