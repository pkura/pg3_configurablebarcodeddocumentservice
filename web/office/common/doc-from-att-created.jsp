<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N doc-from-att-created.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<input type="button" value="<ds:lang text="PrzejdzDoUtworzonegoPisma"/>" class="btn"
    onclick="document.location.href='<ww:url value="newDocumentLink"/>';"/>
    
<input type="button" value="<ds:lang text="PowrotDoEkranuZalacznikow"/>" class="btn"
    onclick="document.location.href='<ww:url value="baseLink"><ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>';"/>