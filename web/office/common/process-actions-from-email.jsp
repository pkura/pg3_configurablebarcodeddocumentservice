<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%--
    Fragment wy�wietlaj�cy czynno�ci dost�pne dla procesu
    Wsp�pracuje z klas� ProcessBaseTabsAction.

    W pliku jbpm4-default-process-view.vm jest typowy szablon kt�ry b�dzie wstawiany na tej stronie (bazuje on na jbpm4).

    Fragment musi znajdowa� si� wewn�trz elementu form dodatkowo musz� by� spe�nione nast�puj�ce wymaganie:
     - dost�pna funkcja js validateForm zwracaj�ca true je�li formularz jest prawid�owy
     - dost�pna funkcja js forceNote do wymuszenia dodania uwagi przed wykonaniem akcji (uwaga musi zosta� obs�u�ona
        oddzielnie - ProcessBaseTabsAction jej nie doda)
 --%>

<input type="hidden" name="processName" id="processName"/>
<input type="hidden" name="processId" id="processId"/>
<input type="hidden" name="processAction" id="processAction"/>
<input type="hidden" name="processActionStored" id="processActionStored"/>
<input type="hidden" name="processActionCorrectionInfo" id="processActionCorrectionInfo"/>
<ww:hidden name="'activity'" value="activity" id="processActivity"/>
<form name="processAction"
		action="<ww:url value="'/office/process-action-from-email.action'"/>"
		method="post">
	
<ds:ww-action-errors/>
<ds:ww-action-messages/>
	<br></br><br></br><br></br><br></br>
	<input type="button" class="btn" value="Kontynuuj" onclick="javascript:window.location='<ww:property value="returnUrl"/>'"/>

</form>
<ww:iterator value="processRenderBeans">
        <ds:render template="template"/>
</ww:iterator>