<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script type="text/javascript">
var Uri
function getUrlImage(url){
	if(url.contains("docusafe")){
		Uri =  "/docusafe/img/pobierz.gif"
	} else {
		Uri = "/img/pobierz.gif";
	}
}
</script>

	<table style="width: 100%;" ng-app="documentVersion" ng-controller="versionCtrl" ng-cloak>
		<tr style="width: 100%;">
			<td id="version-lists" style="width: 20%;">
		  		<table>
					<ww:iterator value="versions" status="stat" >
						<ww:set name="result"/>
						<tr id="jackrabbit_link">						
							<a ng-click="fetch('<ww:property value="#result.uuid"/>'); $event.stopPropagation()" href="javascript:void(0)"><ww:property value="#result.path"/></a>
						</tr>
					        <script>
					        	getUrlImage('<ww:url value=""/>');
        					</script>
					</ww:iterator>
	        	</table>
	        </td>
	
			<td style="width: 80%; border=0;">
				<div id="app" style="height: 700px;width: 800px;">
	
					<div class="result">
						<div class="result-header">
							<span ng-if="state=='searching'">
								Szukam...
							</span>
						</div>
						<div class="result-table">
						<table class="result-table">
						</table>
						</div>
					</div>
	
				</div>
			</td>
		</tr>
	</table>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/URI-1.11.2.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/angular/angular-1.2.7.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/lodash.compat-2.4.1.min.js"></script>
<script type="text/javascript">
	var app = angular.module('documentVersion', []);
	var windowQuery = URI.parseQuery(URI.parse(window.location.href)["query"]);
	
	var documentId = windowQuery["documentId"];	

	app.controller('versionCtrl', function($scope,$http) {

	    $scope.documentId = documentId; 
	    $scope.document = null;
	    $scope.state = "waiting";
	    
	    
	    $scope.fetch = function(uuid) {
	    	$scope.state = "fetching";
	    	
			setTimeout(function(){
			    	$http.get('<ww:url value="'/admin/jackrabbit-repos.action'" />?getXmlVersion=true&documentId='+$scope.documentId+'&uuid='+uuid).
		    		success(function(response) {	    		
		    			$scope.document = response.result.document;
		    			traverse($scope.document);
		    			print();
		    			$scope.state = "waiting";
		        	}).
		        	error(function(data, status, headers, config) {
		        		$j('.result-header').val('Wust�pi� B��d (' + status + '). Spr�buj ponownie.');
	       		    });
    		},250);
	    }
	    
	    var tableObj = [];
	    var tableObjKey = [];
	    
	    function traverse(jsonObj) {
	    	if(_.isObject(jsonObj)) {
	    		tableObj.push(jsonObj);
	    		$j.each(jsonObj, function(k,v) {
	    			traverse(v);
	    		});
	    	}
	    }
	    
	    function print(){
		    var r = 0;
		    var table = $j('table.result-table');
		    var row = newRow(table,tableObj);
	    	
	    	// make table based on data json
	    	
	    	$j.each(tableObj, function(k,v) {
	    		$j.each(v, function(ki,vi){
	    			if(primitiveValue(vi)){
	    				if(!rowExisted(r)){
	    					row = newRow(table,tableObj);
	    					table.append(row);
	    				}
	    			}
	    		});
	    		indexCol = indexCol + 1;
	    	});
	    	var colAtta = tableObj.length-1;
	    	var rowAtta = tableObj[colAtta];
	    	rowAtta = ($j.map(rowAtta, function(el) { return el; })).length + 1;
	    	
	    	// fill table based on data json
 		    var indexCol = 0;
	    	var indexRow = 0;
	    	$j.each(tableObj, function(k,v) {
	    		indexRow=2;
	    		$j.each(v, function(ki,vi){
	    			if(primitiveValue(vi)){
	    				if(!(getKey(v,vi)==="content")){
		    				$j("tr."+indexRow).children("."+indexCol).html("<b>"+getKey(v,vi)+"</b>"+": " + vi);
		    				indexRow = indexRow + 1;
	    				} else {
	    					anchorFileDownload(v);
	    				}
	    			}
	    		});
	    		indexCol = indexCol + 1;
	    	});
		    var key1 = null;
		    function traverse2(jsonO, searchedEle) {
		    	$j.each(jsonO, function(k,v) {
		    			if(_.isObject(v)){
			         		  if(v === searchedEle){
			         			 key1= k;
			         		  }else {
			         			 traverse2(v, searchedEle);
			         		  }
		    			}
		    	});
		    }
	    	
	    	indexCol = 0;
	    	for(i=0; i<tableObj.length; i++){
	    		traverse2($scope.document, tableObj[i]);
	    		if(key1 === null){
	    			key1="";
	    		} 
	    		indexRow=1;		
    			$j("tr."+indexRow).children("."+indexCol).html("<b>"+key1+"</b>"+" ");
	    		indexCol = indexCol + 1;
	    	};
	    	
	    	function newRow(table,tableObj){
	    	   	var rowl = $j('<tr/>');
	    		rowl.addClass(""+r);
	    		for(i=0; i<tableObj.length; i++){
	    	        var coll = $j('<td/>');
	    	        coll.addClass(""+i);
	    	        rowl.append(coll);
	    	    } 
	    		r = r + 1;
	       		return rowl;
	    	}
	    	
	    	
	    	function primitiveValue(o){
	    		return !_.isObject(o);
	    	}
	    	
	    	function rowExisted(r){
	    		return $j('tr.'+r).length > 0;
	    	}
	    	
	    	//create link to download attachment
	    	function anchorFileDownload(v){
	    		var download = $j.parseJSON(JSONStringify(v).replace('-', '_' )).original_filename;
				var a = $j('<a class="imgView" id="programatically" download="'+download+'" href="#">Pobierz</a>');
				var url = Uri;
				a.append('<img src="'+url+'" width="18" height="18" class="zoomImg" class="zoomImg" title="Pobierz"/>');
				
				$j("tr."+rowAtta).children("."+colAtta).append(a);
		    	$j("a#programatically").click(function() {
		    	    this.href = "data:"+v.mime+";charset=UTF-8," + encodeURIComponent(atob(v.content));
		    	});
	    	}

	    }
	    
    	function getKey(parent,child){
	    	Object.keys(parent).forEach(function(k){
      		  if(parent[k] === child){
      			  key = k;
      		  }
      		});
	    	return key;
	    }
	});
</script>

