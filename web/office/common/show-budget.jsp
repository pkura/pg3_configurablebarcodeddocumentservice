<%@ page language="Java" contentType="text/html; charset=iso-8859-2" pageEncoding="iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/office/common/show-budget.action'"></ww:url>" method="post" >
<ww:hidden name="'documentId'" value="documentId"/>
<table id="titleTable" style="display:block; float:left;">
	<tr>
		<td>
			Wybierz MPK:
		</td>
		<td>
			<ww:select name="'mpkId'" cssClass="'sel'" list="mpkList" onchange="'document.forms[0].submit();'"/>
		</td>
	</tr>
	
	<tr>
		<th> Typ </th>	
		<th> Kwota </th>		
	</tr>	
	<tr>
		<td>
			Bud�et
		</td>
		<td>
			<ww:textfield name="mpkBudget" value="mpkBudget" size="30" readonly="true" />
		</td>
	</tr>
	<tr>
		<td>
			Suma zam�wie�:
		</td>
		<td>
			<ww:textfield name="sumAllOrders" value="sumAllOrders" size="30" readonly="true" />
		</td>
	</tr>
	<tr>
		<td>
			Pozosta�o:
		</td>
		<td>
			<ww:textfield name="budgetLeft" value="budgetLeft" size="30" readonly="true" />
		</td>
	</tr>
	<tr>
		<td>
			Do wykorzystania:
		</td>
		<td>
			<ww:textfield name="budgetLeftPercent" value="budgetLeftPercent" size="30" readonly="true"/>			
		</td>
	</tr>
	<tr>
		<td>
			Obecne:
		</td>
		<td>
			<ww:textfield name="ordersAmountSum" value="ordersAmountSum" size="30" readonly="true"/>			
		</td>
	</tr>
	<tr>
		<th> Nazwa konta </th>	
		<th> Suma kwot </th>		
	</tr>
	<ww:iterator value="sumAmounts">
		<tr>
			<td> <ww:property value="key"/> </td>
			<td> <ww:property value="value"/> </td>
		</tr>
	</ww:iterator>

</table>
</br>
<img src="<c:out value='${pageContext.request.contextPath}${filename}'/>" style="display: block; float: left;">
</form>