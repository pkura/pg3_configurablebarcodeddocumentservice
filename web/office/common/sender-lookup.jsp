<%-- 
    Document   : sender-lookup
    Created on : 2009-06-24, 14:44:37
    Author     : Michał Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="application/json; charset=utf-8"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
[<ww:iterator value="senders" status="status"><ww:if test="!#status.first">,</ww:if>{
	"personLastname":"<ww:property value="lastname"/>",
	"personFirstname":"<ww:property value="firstname"/>",
	"personOrganization":"<ww:property value="organization"/>",
	"personStreetAddress":"<ww:property value="street"/>",
	"personZip":"<ww:property value="zip"/>",
	"personEmail":"<ww:property value="email"/>",
	"personTitle":"<ww:property value="title"/>",
	"personLocation":"<ww:property value="location"/>",
	"personId":"<ww:property value="id"/>"
}</ww:iterator>]



