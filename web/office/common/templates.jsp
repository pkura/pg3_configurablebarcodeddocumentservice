<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last">
            <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
        </ww:if>
    </ww:iterator>
</ds:available>

<ds:available test="layout2">
    <div id="middleMenuContainer">
        <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
        <ww:iterator value="tabs" status="status" >
            <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        </ww:iterator>
    </div> 
</ds:available>


<style type="text/css">

/*	#snapshot-view-header-table td {
		font-size: 16px;
		font-weight: bold;
		margin-bottom: 5px;
	}
*/

	.field-value {
		width: 250px;
	}

	.field-name {
		width: 250px;
	}

	.snapshot-values {
		margin-top: 10px;
	}
	.snapshot-values td {
		font-size: 14px;
	}
	.snapshot-values caption {
		font-size: 16px;
		border-bottom: 1px black solid;
		margin-bottom: 5px;
		padding-bottom: 5px;
	}

	.read-only td {
		/*font-style: italic;*/
	}

	.last-read-only td {
		/*color: red;*/
		padding-bottom: 10px;
	}

	#snapshots-title {
		font-weight: bold;
		font-size: 16px;
		margin-bottom: 10px	;
	}

	.hide {
		display: none;
	}

	.buttons {
		/*display: inline-block;*/
		/*width: 450px;*/
	}

	.document-list {
		/*display: inline-block;*/
		margin-top: 30px;
		margin-bottom: 10px;
		border: 1px gray solid;
		width: 900px;
		min-height: 300px;
		text-align: center;
	}

	.snapshot-title {
		width: 350px;
	}

	.div-row .div-cell {
		display: inline-block;
		width: 300px;
	}

	.multi-dictionary {
		border: 1px gray solid;
		padding: 5px;
		margin: 5px;
	}

	.multi-dictionary table tr:first-child td {
		border-bottom: 1px gray solid;
		font-weight: bold;
	}

/*	.border-bottom {
		border-bottom: 1px black solid;
	}
*/
</style>

<p></p>

<ds:available test="layout2">
	<div id="middleContainer">
</ds:available>

<div id="messages">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</div>

<!-- <form enctype="multipart/form-data" method="post">

	<input type="hidden" name="uploadFileDocument" value="true">
	<input type="hidden" name="documentId" value="12448">
	<input type="hidden" name="id" value="43">
	<input type="hidden" name="documentName" value="1389009673235_Delegacja_RozliczeniePLNWplataKasa.rtf">

	Nowa wersja: <input type="file" name="file"/>
	<input type="submit" value="submit!">

</form> -->

<div id="ng-app" ng-app="TemplatesApp">

	<!-- Lista snapshot�w -->
	<div id="snapshots-view" class="hide" ng-controller="SnapshotsCtrl">
		<div id="snapshots-title">Snapshoty dokumentu {{documentId}}</div>
		<table id="snapshots-table">
			<tr>
				<td>ID</td>
				<td>Tytu�</td>
				<td>Data utworzenia</td>
				<td>U�ytkownik</td>
			</tr>
			<tr ng-repeat="snapshot in snapshots">
				<td>{{snapshot.id}}</td>
				<td>
					<a href="{{snapshot.href}}<ww:if test="activity!=''">&activity=<ww:property value='activity'/></ww:if>">{{snapshot.title}}</a>
				</td>
				<td>{{snapshot.ctime}}</td>
				<td>{{snapshot.user}}</td>
				<td>
					<button class="remove-snapshot" ng-click="removeSnapshot(snapshot.id)">Usu�</button>
				</td>
			</tr>
		</table>

		<button ng-click="addSnapshot()">Dodaj snapshot</button>
	</div>

	<!-- Widok pojedynczego snapshota -->
	<div id="snapshot-view" class="hide" ng-controller="SnapshotCtrl">

		<!--button ng-click="goBack()">Lista snapshot�w</button-->
		<table class="snapshot-values">
			<caption style="font-weight: bold">
				{{snapshot.title}}
			</caption>
			<tr class="read-only">
				<td>Data utworzenia</td>
				<td colspan="2">
					{{snapshot.ctime}}
				</td>
			</tr>
			<tr class="read-only">
				<td>Data ostatniej modyfikacji</td>
				<td colspan="2">
					{{snapshot.mtime}}
				</td>
			</tr>
			<tr class="read-only">
				<td>Utworzony przez</td>
				<td colspan="2">
					{{snapshot.user}}
				</td>
			</tr>
			<tr class="last-read-only read-only">
				<td>Tytu�</td>
				<td colspan="2">
					<input ng-model="snapshot.title" class="snapshot-title">
				</td>
			</tr>
		</table>

		<div>
			<button ng-click="save()">Zapisz</button>
			<button ng-click="fork()">Utw�rz now� wersj�</button>
		</div>

		<!-- <table class="snapshot-values"> -->
<!-- 			<tr ng-repeat="field in snapshot.fields | orderBy:'cn'">
				<td class="field-name">
					{{field.name}}
				</td>
				<td>
					{{field.cn}}
				</td>
				<td>
					<input type="text" class="field-value" ng-model="field.value">
				</td>
				<td>
					<button ng-click="removeField(field.cn)">Usu�</button>
				</td>
			</tr> -->
		<!-- </table> -->
			<div class="div-row" ng-repeat="field in snapshot.fields | stringFields | orderBy:'cn'">
				<div class="div-cell field-name">
					{{field.name}}
				</div>
				<div class="div-cell">
					{{field.cn}}
				</div>
				<div class="div-cell">
					<input type="text" class="field-value" ng-model="field.value">
				</div>
				<div class="div-cell">
					<button ng-click="removeField(field.cn)">Usu�</button>
				</div>
			</div>
			<div class="div-row multi-dictionary" ng-repeat="field in snapshot.fields | dicFields | orderBy:'cn'">
				<div class="div-cell field-name">
					{{field.name}}
					<button ng-click="removeField(field.cn)">Usu�</button>
				</div>
				<table>
					<tr>
						<td>Nazwa kodowa</td>
						<td>Warto��</td>
					</tr>
					<tr ng-repeat="innerField in field.value | orderBy:'cn'">
						<td>
							{{innerField.cn}}
						</td>
<!-- 						<td>
							{{innerField.name}}
						</td> -->
						<td>
							<input ng-model="innerField.value">
						</td>
					</tr>
				</table>
			</div>
			<div class="div-row multi-dictionary" ng-repeat="field in snapshot.fields | multiDicFields | orderBy:'cn'" ng-if="field.value.length > 0">
				<div class="div-cell field-name">
					{{field.name}}
					<button ng-click="removeField(field.cn)">Usu�</button>
				</div>
<!-- 				<div class="div-cell field-name">
					{{field.cn}}
				</div> -->
				<div>

					<table>
						<!-- Nag��wki do podtabeli -->
						<tr>
							<td ng-repeat="headerField in field.value[0]">
								{{headerField.cn}}
							</td>
						</tr>

						<!-- Iteracja po warto�ciach -->
						<tr ng-repeat="fieldList in field.value">
							<td ng-repeat="innerField in fieldList">
								<input ng-model="innerField.value">
							</td>
						</tr>
					</table>
				</div>
			</div>

<!-- 		<div ng-repeat="field in snapshot.fields | dicFields">
				<h3>
					{{field.cn}}
				</h3>
 -->
<!-- 				<table>
					<tr ng-repeat="f in fields">
						<td class="field-name">
							{{f.name}}
						</td>
						<td>
							{{f.cn}}
						</td>
						<td>
							<input type="text" class="field-value" ng-model="f.value">
						</td>
						<td>
							<button ng-click="removeField(field.cn)">Usu�</button>
						</td>
					</tr>
				</table> -->
		<!-- </div> -->


		<div class="buttons">
			<!--button ng-click="goBack()">Lista snapshot�w</button-->
			<button ng-click="addField()">Dodaj nowe pole</button>
			<button ng-click="save()">Zapisz</button>
			<button ng-click="fork()">Utw�rz now� wersj�</button>
			<br>
			<select id="template-select">
				<option ng-repeat="template in snapshot.templates" value="{{template.name}}">
					{{(template.isUserTemplate ? "(u�ytkownika) " : "") + template.name}}
				</option>
			</select>

			<button ng-click="createFileDocument()">Wygeneruj dokument</button>
		</div>
		<div class="document-list">
			<h3>Lista wygenerowanych dokument�w</h3>
			<table>
				<tr ng-repeat="doc in snapshot.fileDocuments | orderBy:'name'">
					<td>
						<a href="{{getFileDocumentUrl(doc)}}">{{doc.name}}</a>
					</td>
					<td>
						<button ng-click="deleteFileDocument(doc)">Usu�</button>
					</td>
					<td>
						<button ng-click="createPdf(doc)" ng-if="doc.isRtf">PDF</button>
					</td>
					<td>
							Nowa wersja: <input type="file" name="file" ng-file-select="uploadFileDocument($files, doc)"/>
							{{doc.uploadStatus}}
					</td>
				</tr>
			</table>
		</div>
	</div>

</div>

<ds:available test="layout2">
	<div class="bigTableBottomSpacer">&nbsp;</div>
</ds:available>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/URI-1.11.2.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/lodash.compat-2.4.1.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/angular/angular-file-upload/angular-file-upload-html5-shim.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/angular/angular-1.2.7.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/angular/angular-file-upload/angular-file-upload.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/ajaxAction.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/office/common/templatesApp.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript">
	


</script>

<!--N koniec index.jsp N-->
