<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N case.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.record.Records"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<script type="text/javascript">
	jQuery(document).ready(
		function(){
			jQuery('#description2').css('width','239px')
		}		
	)
</script>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages />
<ww:if test="canView">
	<form action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);" id="form">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<ww:if test="document.containingCaseId != null">
	<p>Dokument znajduje si� w sprawie <a href="<ww:url value='caseLink'/>"
										 name="currentCase" 
										 id="<ww:property value="document.containingCase.officeId"/>"
										 target="_blank">
										 	<ww:property value="document.containingCase.officeId"/>
										 </a>.

		<ww:if test="associatedDocumentsIn.size > 0">
			<br>Kopie dokumentu znajduj� si� w sprawach: 
			<ww:iterator value="associatedDocumentsIn" status="status" >
				
				<a href="<ww:url value="'/office/edit-case.do'"> <ww:param name="'id'" value="[0].caseId" /> </ww:url>"
					 target="_blank"					 
				>
					<ww:property value="[0].caseDocumentId"/>
				</a>
					<ww:if test="!#status.last">, </ww:if>
			</ww:iterator>
		</ww:if>
		<ww:if test="associatedDocumentsOut.size > 0">
			<br>Kopie dokumentu znajduj� si� w sprawach :
			<ww:iterator value="associatedDocumentsOut" status="status" >
				<a href="<ww:url value="'/office/edit-case.do'"> <ww:param name="'id'" value="[0].caseId" /> </ww:url>"
					 target="_blank"					 
				>
					<ww:property value="[0].caseDocumentId"/>
				</a>
				
				<ww:if test="!#status.last">, </ww:if>
			</ww:iterator>
		</ww:if>
	</p>
	
	</ww:if>
	    <input type="hidden" name="doAddToCase" id="doAddToCase"/>
	    <input type="hidden" name="doAddToNextCase" id="doAddToNextCase"/>
	    <input type="hidden" name="caseId" id="caseId"/>
	    <input type="hidden" name="caseReopen" id="caseReopen"/>
	    <ww:hidden name="'portfolioId'" id="portfolioId"/>
	
		<ww:hidden name="'normalCaseFinishDate'" id="normalCaseFinishDate" value="caseFinishDate"/>
	    <ww:hidden name="'documentIncomingDate'" value="documentIncomingDate"/>
	    <ww:hidden name="'documentId'" value="documentId"/>
	    <ww:hidden name="'activity'" value="activity"/>
	    <ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
	    <ww:token />
	
	    <ww:if test="!blocked">
	    	<hr class="highlightedText" style="width:10%; height:1; size:1;" align="left"/>
	    	
	    	<span id="ukryj_tekst" style="display:;">
				<ds:lang text="AbyZalozycNowaSprawe"/>
				<a name='newCase' href="javascript:nowa_sprawa()">
				<ds:lang text="KliknijTutaj"/></a>
			</span>

	    	<div id="ukryj_napis" style="display:none;">
	    		<ds:lang text="AbyZalozyc1"/>
	    		<br>
	    		<ds:lang text="AbyZalozyc2"/>
	    	</div>   	

			<table id="ukryj_tabele" style="display:none;">
				<tr>
					<td>
						Teczka<span class="star">*</span>:
					</td>
					<td>
						<ww:textfield name="'portfolioTxt'" id="portfolioTxt" cssClass="'txt'" readonly="true" size="30"/>
						<input type="button" class="btn" value="Wybierz"
						onclick="window.open('<ww:url value="'/office/pick-portfolio.action'"><ww:param name="'documentIncomingDate'" value="documentIncomingDate"/><ww:param name="'divisionGuid'" value="targetDivision"/><ww:param name="'first'" value="true"/></ww:url>', 'pickportfolio', 'width=600,height=600,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes')"/>
					</td>
				</tr>
				<tr>
					<td>
						Tytu� sprawy<span class="star">*</span>:
					</td>
					<td>
						<ww:textfield name="'caseTitle'" id="caseTitle" cssClass="'txt'" value="caseTitle" size="30"/>
					</td>
				</tr>
				<tr>
					<td>
						Opis sprawy:
					</td>
					<td>
						<ww:textarea name="'description2'" rows="3" cols="47" cssClass="'txt'" id="description2" value="document.Summary"/>
					</td>
				</tr>
				<tr>
					<td>
						Spodziewany termin zako�czenia<span class="star">*</span>:
					</td>
					<td>
					<script type="text/javascript">
                        var _caseFinishDate = "caseFinishDate";
                    </script>
						<ww:textfield name="'caseFinishDate'" id="caseFinishDate" size="10" maxlength="10" cssClass="'txt'" onchange="'checkIfSelectedIsBeforeToday(_caseFinishDate)'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="caseFinishDateTrigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					</td>
				</tr>
		
				<ww:if test="canChooseClerk">
					<tr>
						<td>
							Referent sprawy:
						</td>
						<td>
							<ww:select name="'clerk'" list="clerks" listKey="name" listValue="asLastnameFirstname()" cssClass="'sel'" value="clerk" />
						</td>
					</tr>
				</ww:if>
		        
				<tr>
					<td>
						Priorytet sprawy<span class="star">*</span>:
					</td>
					<td>
						<ww:select name="'priority'" list="priorities" listKey="id" listValue="name" value="document.containingCase.priority.id" cssClass="'sel'" />
					</td>
				</tr>
				<tr>
					<td>
						<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
					</td>
				</tr>
				<tr>
					<td>
						<ww:if test="document.containingCaseId!=null">
							<ds:event name="'doPreCreate'"
							value="'Za�� kolejn� spraw�'"
							onclick="'if(!validate()) return false;'"
							confirm="'Na pewno utworzy� now� spraw� dla tego dokumentu?'"
							disabled="!canCreateCase"/>
						</ww:if>
						<ww:else>
							<ds:event name="'doPreCreate'"
							value="'Za�� spraw�'"
							onclick="'if(!validate()) return false;'"
							confirm="'Na pewno utworzy� now� spraw� dla tego dokumentu?'"
							disabled="!canCreateCase"/>
						</ww:else>
					</td>
				<tr>
			</table>
	
			
			<br/>
			<ww:if test="document.containingCaseId!=null">
				<ds:lang text="AbyPrzeniescPismoDoIstSprawy"/>
			</ww:if>
			<ww:else>
				<ds:lang text="AbyDodacPismoDoIstSprawy"/>
			</ww:else>
			<a href="javascript:void(window.open('<ww:url value="'/office/find-cases.action?popup=true'"/>', 'Rwa', 'width=750, height=600, menubar=no, toolbar=no, status=no, location=no, scrollbars=yes, resizable=yes'));" name="addToCase">
			<ds:lang text="KliknijTutaj"/></a><br>
			<ww:if test="document.containingCaseId!=null">
				<ds:lang text="AbyDodacPismoDoIstSprawy"/>
				<a href="javascript:void(window.open('<ww:url value="'/office/find-cases.action?popup=true&mode=1'"/>', 'Rwa', 'width=750, height=600, menubar=no, toolbar=no, status=no, location=no, scrollbars=yes, resizable=yes'));" name="addToCase">
				<ds:lang text="KliknijTutaj"/></a>
				<br>
			</ww:if>			
			<br/>
			<ww:if test="document.containingCaseId!=null">
				<ds:event name="'doRemoveFromCase'"	value="'Usu� pismo ze sprawy'" cssClass="'btn'" />
			</ww:if>
			
	
	            <script language="JavaScript">

			if($j('#caseFinishDate').length > 0)
			{
	            Calendar.setup({
	                inputField     :    "caseFinishDate",     // id of the input field
	                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>", // format of the input field
	                button         :    "caseFinishDateTrigger",  // trigger for the calendar (button ID)
	                align          :    "Tl",           // alignment (defaults to "Bl")
	                singleClick    :    true
	            });
			}
	
	    /*
	                Pomieszane nazwy - w formularzu portfolioId oznacza Portfolio.id,
	                za� portfolio oznacza Portfolio.portfolioId.
	            */
	            
	            function __pick_portfolio(id, portfolioId, name, finishDate)
	            {
	                document.getElementById('portfolioId').value = id;
	                document.getElementById('portfolioTxt').value = portfolioId + ' ('+name+')';
	                //document.getElementById('doSetPortfolio').value = 'true';
	                //document.forms[0].submit();
	                <ww:if test="documentIncomingDate != null">
		                if (finishDate != null) {
		                	//alert('finishDate nie null ');
		                	//alert(finishDate);
		                	document.getElementById('caseFinishDate').value = finishDate;
		                }
		                else {
		                	//alert('finishDate null');
		                	document.getElementById('caseFinishDate').value = document.getElementById('normalCaseFinishDate').value;
		                }
	                </ww:if>
	            }
	            </script>
	    </ww:if>
	
	   <%-- <ww:if test="document.containingCaseId != null and documentType == 'in'"> --%>
	   <ww:if test="(inRecords.size > 0 or availableRecords.size > 0) and (documentType == 'in')">
	        <h3>Rejestr</h3>
	
	        <ww:if test="inRecords.size > 0">
	            <p>Sprawa znajduje si� w nast�puj�cych rejestrach: <br/>
	                <ww:iterator value="inRecords">
	                    <a href="<ww:url value="key"/>"><ww:property value="value"/></a> <br/>
	                </ww:iterator>
	            </p>
	        </ww:if>
	
			<ww:if test="availableRecords.size > 0">
	        <table>
	            <tr>
	                <td>Rejestr:</td>
	                <td>
	                    <ww:select name="'record'" list="availableRecords" cssClass="'sel'" id="record"
	                        headerKey="''" headerValue="'-- wybierz --'" />
	                </td>
	            </tr>
	            <tr>
	                <td colspan="2">
	                    <input type="button" class="btn" value="Utw�rz wpis w rejestrze"
	                        onclick="if (!validateRwa(document.getElementById('record').value)) return false;
	                        if (document.getElementById('record').value.length == 0) { alert('Nie wybrano rejestru'); return false; }; if (!confirm('Na pewno umie�ci� spraw� w rejestrze?')) return false; postCreate(document.getElementById('record').value);"/>
	                </td>
	            </tr>
	        </table>
	        </ww:if>
	
	    </ww:if>
	    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	</form>
</ww:if>

<script language="JavaScript">

	// funkcja wy�wietlania i chowania p�l nowej sprawy
	function nowa_sprawa()
	{
		document.getElementById('ukryj_napis').style.display = '';
		document.getElementById('ukryj_tabele').style.display = '';
		document.getElementById('ukryj_tekst').style.display = 'none';
	}
	 
    // funkcja wywo�ywana przez okienko wyboru RWA
    function pickRwa(rootId, categoryId, rwaString)
    {
        document.getElementById('rwaCategoryId').value = categoryId;
        document.getElementById('prettyRwaCategory').value = rwaString;
    }

    // funkcja wywolywana przez okienko wyboru sprawy
    function pickCase(id, caseId, mode)
    {
        if (!confirm('Na pewno doda� dokument do sprawy '+caseId+'?'))
            return;
        if (confirm('Wznowi� sprawe '+caseId+' je�li jest zamkni�ta?'))
        {
        	//alert('caseReopen -> true');
         	document.getElementById('caseReopen').value = 'true';
        }
        else 
        {	
        	//alert('caseReopen -> false');
        	document.getElementById('caseReopen').value = 'false';
        }  

        if (mode == 0)
       	  document.getElementById('doAddToCase').value = 'true';
        else
          document.getElementById('doAddToNextCase').value = 'true';
        document.getElementById('caseId').value = id;
        document.getElementById('form').submit();
    }
    
    function validateRwa(rwa)
    {
        rwa = rwa.split(";", 2)[1];
        //alert('rwa:' + rwa);
    	if (rwa.length > 0)
    	{
            var caseRwa = '<ww:property value="caseRwa"/>';
            //alert('caseRwa:' + caseRwa);
            if (rwa != caseRwa)
            {
                alert('Sprawa powinna znajdowa� si� w teczce o RWA ' + rwa);
                return false;
            }
        }
        return true;
    }

	function validate(){
		var isOk = true;

		var caseTitle = $j('#caseTitle').val();
		var portfolioTxt = $j('#portfolioTxt').val();

		if(portfolioTxt == null || portfolioTxt == '' ) {
			alert('Wybierz teczk�!');
			isOk = false;
		} else if(caseTitle == null || caseTitle == ''){
			alert('Podaj tytu� sprawy');
			isOk = false;
		} else if(caseTitle.length > 199) {
			alert('Tytu� sprawy jest za d�ugi');
			isOk = false;
		}


		return isOk;
	}

    function postCreate(url)
    {
        url = url.split(";", 2)[0];
        //alert('url:' + url);
        var oForm = document.createElement('FORM');
        oForm.action = url;
        oForm.method = 'POST';
        var oCreate = document.createElement('INPUT');
        oCreate.type = 'HIDDEN';
        oCreate.name = 'doPreCreate';
        oCreate.value = 'true';
        oForm.appendChild(oCreate);
        <ww:if test="document.containingCaseId != null">
        var oCaseId = document.createElement('INPUT');
        oCaseId.type = 'HIDDEN';
        oCaseId.name = 'caseId';
        oCaseId.value = <ww:property value="document.containingCase.id"/>;        
        oForm.appendChild(oCaseId);
        </ww:if>
        var oDocumentId = document.createElement('INPUT');
        oDocumentId.type = 'HIDDEN';
        oDocumentId.name = 'documentId';
        oDocumentId.value = <ww:property value="document.id"/>
        oForm.appendChild(oDocumentId);

        document.body.appendChild(oForm);
        oForm.submit();
    }

</script>
