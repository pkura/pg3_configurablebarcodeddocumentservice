<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<!-- <dwr-document-main.jsp> -->
<jsp:include page="/office/common/inc-title.jsp"/>

<div id="middleMenuContainer" <ww:if test="tabs.size == 0">class="middleMenuContainerEmpty"</ww:if>>
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div>

<hr size="1" align="left" class="horizontalLine" width="100%" />
<p></p>

<div class="error-msg-class">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</div>
<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>
<form id="form" action="<ww:url value="getBaseUrl"/>" method="post" class="dwr" enctype="multipart/form-data" >
 <ww:hidden name="'inDocumentId'" value="inDocumentId"/>
 <ww:hidden name="'messageId'" value="messageId"/>
<ds:available test="layout2">
	<div id="middleContainer" style="clear: both; overflow: hidden; position: relative; top: -13px; *top: -30px; border-top: 0px solid transparent; *padding-bottom: 8px; padding-top: 20px !important;"> <!-- BIG TABLE start -->
</ds:available> 
<jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
<ww:if test="#specialAttachmentLabel==null">
    <fieldset>
        <div class="field multiFiles_part">
        <ww:iterator value="loadFiles">
                <input type="text" value="<ww:property value="value"/>" readonly="readonly"/><br/>
                <input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>"><br/>
        </ww:iterator>
        <ds:available test="!document.attachments.disable" documentKindCn="documentKindCn">
            <label><ds:lang text="Plik"/>:</label>
            <ds:available test="!nfos.style.documentMain"><a href="#" onclick="addAtta(this); return false;" class="addLink" ><ds:lang text="DolaczKolejny" /></a></ds:available>
            <ww:file name="'multiFiles1'" size="30" cssClass="'txt multiFiles'" value="''"/>
            <ds:available test="nfos.style.documentMain"><a href="#" onclick="addAtta(this); return false;" class="addLink" ><ds:lang text="DolaczKolejny" /></a></ds:available>
        </ds:available>
            <!-- label>Tytul:</label>
            <input type="text" class="txt multiFilesTitle" name="multiFilesTitle1" /-->
            <img src="<ww:url value="'/img/minus2.gif'" />" class="removeLink" onclick="delAtta(this);" title="Usu�" style="cursor: pointer; position: relative; top: 3px; left: 3px;">
        </div>
    <script type="text/javascript">
        $j('img.removeLink').hide();
        var attCounter = 2;
        function addAtta(domObj)
        {
            $j('img.removeLink').show();
            var str = '<div class="field multiFiles_part">';
                str += $j('div.multiFiles_part:last').html();
                str += '</div>';

            trace(str);
            $j(domObj).parent().after(str).fadeIn();
            $j('div.multiFiles_part:last').children('input.multiFiles').attr('name', 'multiFiles' + attCounter);
            //$j('div.multiFiles_part:last').children('input.multiFilesTitle').attr('name', 'multiFilesTitle' + attCounter);
            var jqAlmostLast = $j('div.multiFiles_part').eq(-2);
            jqAlmostLast.children('a.addLink').css('visibility', 'hidden');
            jqAlmostLast.children('img.removeLink').show();
            attCounter ++;
        }

        function delAtta(domObj)
        {
            /* usuwamy tylko gdy mamy dwa lub wi�cej plik�w */
            if($j('div.multiFiles_part').length > 1) {
                if ($j('img.removeLink:last').filter(domObj).length > 0) {
                    $j('a.addLink').eq(-2).show();	// pokazujemy przedostatni
                }

                /* ukrywamy ikonk� usu�, je�li zosta�y tylko dwa pliki */
                if($j('div.multiFiles_part').length < 3)
                    $j('img.removeLink').hide();

                $j(domObj).parent('div.multiFiles_part').slideUp(300, function() {
                    $j(this).remove();
                    $j('div.multiFiles_part:last').children('a.addLink').css('visibility', 'visible');
                });
            }
        }

    </script>
    </fieldset>
</ww:if>
<ww:else>
    <label><ww:property value="#specialAttachmentLabel"/>:</label>
    <ww:file name="'multiFiles1'" size="30" cssClass="'txt multiFiles'" value="''"/>
</ww:else>

<div class="dwrDockindButtons clearBoth">
	<ww:if test="documentId != null">
		<input type="submit" value="getText('dwr.document.Zapisz')" name="doUpdate" class="btn" onclick="return fieldsManager.validate()"/>
	</ww:if>
	<ww:else>
		<script type="text/javascript">
    		function createNextDocument()
			{
    			document.getElementById('createNextDoc').value='true';
				return fieldsManager.validate();
			}
			function createNextDocumentForThisUser()
            			{
                			document.getElementById('createNextDocForThisUser').value='true';
            				return fieldsManager.validate();
            			}
		</script>
	    <input type="hidden" name="createNextDoc" id="createNextDoc"/>
	    <input type="hidden" name="createNextDocForThisUser" id="createNextDocForThisUser"/>

        <!-- Blokuje przycisk po kliknieciu -->
	    <ds:available test="saveBtn.onCreate.fromEvent.disabled">
        	 <ww:if test = "#noValidateOnCreate!=true">
                <ds:event value="getText('Zapisz')" id="btnZapisz" name="'doCreate'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" />
            </ww:if>
            <ww:else>
                <ds:event value="getText('Zapisz')" id="btnZapisz" name="'doCreate'" cssClass="'btn saveBtn'"/>
            </ww:else>
        </ds:available>
        <ds:available test="!saveBtn.onCreate.fromEvent.disabled">
	        <input type="submit" value="<ds:lang text="dwr.document.Zapisz"/>" name="doCreate" class="btn saveBtn"
	            <ww:if test="#noValidateOnCreate!=true">onclick="return fieldsManager.validate()"</ww:if>/>
        </ds:available>

		<ds:available test="documentMain.createNext" documentKindCn="documentKindCn">
        	<input type="submit" name="doCreate" value="<ds:lang text="ZapiszNext"/>" class="btn saveBtn"
        	onclick="return createNextDocument();"/>
        </ds:available>
        <ds:available test="documentMain.createNextForThisUser" documentKindCn="documentKindCn">
            <input type="submit" name="doCreate" value="<ds:lang text="ZapiszNextForThisUser"/>" class="btn saveBtn"
                	onclick="return createNextDocumentForThisUser();"/>
        </ds:available>
        <ds:available test="documentMain.ZapiszIprzejdzDoDokumentu" documentKindCn="documentKindCn">
        	<ds:available test="documentMain.validateWhenUpdate">
        		<input type="submit" name="doCreateGoToDocument" value="<ds:lang text="ZapiszIprzejdzDoDokumentu"/>" class="btn saveBtn"
        		onclick="if(!fieldsManager.validate()) return false;"/>
        	</ds:available>
        	<ds:available test="!documentMain.validateWhenUpdate">
        		<input type="submit" name="doCreateGoToDocument" value="<ds:lang text="ZapiszIprzejdzDoDokumentu"/>" class="btn saveBtn" 
        			onclick="if(!fieldsManager.validate()) return false;"/>
        	</ds:available>
    	</ds:available>
    	<ds:available test="documentMain.createAddToCase">
        		<input type="submit" name="doCreateAddToCase" value="<ds:lang text="ZapiszIdodajDoSprawy"/>" class="btn saveBtn" 
        			onclick="if(!fieldsManager.validate()) return false;"/>
        	</ds:available>
        		
        <ds:available test="documentMain.createAndAssignOfficeNumber">
        	 <ww:if test = "outOfficeDocument">	
        	  		 <ww:if test ="canAssignOfficeNumber">
        			<input type="submit" name="doCreateAndAssignOfficeNumber" value="<ds:lang text="ZapiszInadajNumerKO"/>" class="btn saveBtn" 
        				onclick="if(!fieldsManager.validate()) return false;"/>
        			</ww:if>   
        	</ww:if>  
        </ds:available>
        <ds:available test="documentMain.SplitOutOfficeDocument">
        	 <ww:if test = "outOfficeDocument">
        	 
       		    <ds:available test="documentMain.doSplitDocument">
       		    	<input type="submit" name="doSplitDocument" value="<ds:lang text="PodzielPismoDlaKazdegoOdbiorcyOsobno"/>" class="btn saveBtn" 
       		    		onclick="if(!fieldsManager.validate()) return false;"/>
        		</ds:available>
        		
        	    <ds:available test="documentMain.doSplitAndAssingOficeNumber">
        	     <ww:if test = "canAssignOfficeNumber">
        	    	<input type="submit" name="doSplitAndAssingOficeNumber" value="<ds:lang text="PodzielPismoDlaKazdegoOdbiorcyOsobnoNadajNumerKo"/>" class="btn saveBtn"
        	    		onclick="if(!fieldsManager.validate()) return false;" />
				</ww:if>        	   
        	    </ds:available>
			</ww:if>
        	    
        </ds:available>
	</ww:else>
</div>	

	<ww:iterator value="loadFiles">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/><br/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>"><br/>
	</ww:iterator> 
	
	
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

<%-- JE�LI DOCKIND == 'absence-request' WYPISUJE LISTE URLOP�W PRACOWNIKA / ROZWI�ZANIE TYMCZASOWE --%>
<jsp:include page="/common/employee-absences.jsp" />
<!-- </dwr-document-main.jsp> -->
	
</form>
        
 <ds:available test="project.parametrization.ima">
     <ww:set name="typeIma" value="'dwr-document'"/>
    <jsp:include page="/parametrization/ima/left-menu.jsp"/>
 </ds:available>

<ds:available test="dwr.individualJavaScript.enable">
	<script type="text/javascript">
			<ww:property value="individualJavaScript" escape="false"/>
	</script>
</ds:available>

