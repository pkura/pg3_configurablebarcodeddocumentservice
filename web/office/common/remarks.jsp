<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N remarks.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);">
	<input type="hidden" name="doAdd" id="doAdd"/>
	<input type="hidden" name="doDelete" id="doDelete"/>

	<input type="hidden" name="deleteId" id="deleteId"/>

	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'activity'" value="activity"/>
	<ww:token />

	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

	<ww:if test="!blocked">
		<div id="ukryj_tekst" style="display:;">
			<%--
			<ds:lang text="AbyDodacUwage"/>
			<a href="javascript:ukryj()">
			<ds:lang text="KliknijTutaj"/></a>
			--%>
			<input type="button" class="btn" value="<ds:lang text="DodajUwage"/>" onclick="ukryj()" />
		</div>
	</ww:if>

	<div id="ukryj_uwage" style="display:none;">
	<%--	<p><ds:lang text="DodajUwage"/>:</p>	--%>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<ds:lang text="TrescUwagi"/>:
				</td>
			</tr>
			<ds:available test="archiwizacja.uwagi.slownik">
			<tr>
				<td>
					Z listy <ww:select id="contentSmall" name="'contentSmall'" list="noteList" listKey="name" listValue="name" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/> lub z pola
				</td>
			</tr>
			</ds:available>
			<tr class="formTableDisabled">
				<td>
					<ww:textarea name="'content'" id="content" rows="8" cols="90" cssClass="'txt dontFixWidth'" cssStyle="'width: 600px;'"/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" class="btn saveBtn" onclick="if (!validateForm()) return false; document.getElementById('doAdd').value='true';" value="<ds:lang text="Zapisz"/>"/>
				</td>
			</tr>
		</table>
	</div>

	<br/>

	<table width="100%" <ds:available test="layout2"> cellspacing=0 </ds:available> >
		<ww:iterator value="remarks">
			<ww:set name="date" scope="page" value="date"/>
			<tr <ds:available test="layout2"> class=""</ds:available> >
				<td class="historyAuthor">
					<ww:property value="author"/>
				</td>
				<td align="right" class="historyDate" >
					<fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
				</td>
			</tr>
			<tr>
				<td colspan="2" <ds:available test="!layout2"> style="border: 1px solid black;" </ds:available> <ds:available test="layout2"> class="historyContent" </ds:available> >
					<PRE><ww:property value="content"/></PRE>
				</td>
			</tr>
			<ww:if test="canDelete && !blocked">
				<tr <ds:available test="layout2"> class=""</ds:available> >
					<td <ds:available test="layout2"> colspan="2"</ds:available> >
						<%-- <a href="javascript:void(deleteRemark(<ww:property value="remarkId"/>));"><ds:lang text="usunTeUwage"/></a> --%>
						<input class="btn deleteBtn" type="button" value="<ds:lang text="usunTeUwage"/>" onclick="javascript:void(deleteRemark(<ww:property value="remarkId"/>));" />
					</td>
				</tr>
			</ww:if>

		</ww:iterator>
	</table>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script>
function ukryj()
{
	document.getElementById('ukryj_uwage').style.display=
		((document.getElementById('ukryj_uwage').style.display=="none")?true:false)?"":"none";
	document.getElementById('ukryj_tekst').style.display=
		((document.getElementById('ukryj_tekst').style.display=="none")?true:false)?"":"none";
	<ds:additions test="tcLayout">
		rozmiar();
	</ds:additions>
}

function validateForm()
{
    var content = document.getElementById('content').value;
    if (content != null && content.length > 4000) { alert('Tre�� uwagi jest zbyt d�uga. Maksymalna d�ugo�� to 4000 znak�w'); return false; }

    return true;
}

function deleteRemark(remarkId)
{
    if (!confirm('<ds:lang text="NaprawdeUsunacUwage"/>?'))
        return;
    document.getElementById('deleteId').value = remarkId;
    document.getElementById('doDelete').value = 'true';
    document.getElementById('form').submit();
}

/* naprawa wyswietla pola uwagi dla ff */
 var $j = jQuery.noConflict();
 $j(document).ready(function() {
		var ie = document.all && !window.opera ? 1 : 0;
		if(!ie) {
			$j('#ukryj_uwage').css('float', 'left');
		}

	});
</script>