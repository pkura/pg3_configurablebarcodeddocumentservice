<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<form action="<ww:url value="baseLink"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ds:available test="layout2"><div id="middleContainer"></ds:available>

    Podpisy zwi�zane z dokumentem <ww:property value="documentId"/>
    <ww:iterator value="signatures"><br/>
    <ds:format-date pattern="kk:mm:ss dd-MM-yyyy" value="date"/> : <ww:property value="signedObjectType.description"/>
    <a href="<ww:url value="'/certificates/sign-xml.action'"><ww:param name="'signatureId'" value="id"/></ww:url>">Zweryfikuj podpis</a></ww:iterator>

<ds:available test="layout2">
<div class="bigTableBottomSpacer">&nbsp;</div></div>
</ds:available>
</form>

