<%--
	R�czna dekretacja dla JBPM4
--%>
<!--N manual-assignment.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<style type="text/css">
	select#jbpmProcessId {width:22em;}
</style>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<script type="text/javascript">
        function selectUser(username){
            $j('#selectedUsers option[value="'+username+'"]').attr('selected','selected');
        }
        function selectDivision(guid){
            $j('#selectedGuids option[value="'+guid+'"]').attr('selected','selected');
        }
        function expandDivision(guid){
            var sd = $j('#selectedGuid').get(0);
            sd.value = guid;
            sd.form.submit();
        }
</script>

<form action="<ww:url value="baseLink"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>

    <ww:hidden id="selectedGuid" name="'selectedGuid'" value="selectedGuid"/>

    <ww:hidden name="'doCc'" id="doCc"/>
    <ww:hidden name="'doAcknowledgeCc'" id="doAcknowledgeCc"/>
    <ww:hidden name="'doPlanAssignment'" id="doPlanAssignment"/>
    <ww:hidden name="'doDelete'" id="doDelete"/>
    <ww:iterator value="activityIds">
        <ww:hidden name="'activityIds'" value="[0].top"/>
    </ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table width="100%">

    <tr>
        <td>
			<div id="organizationTree">
				<ww:property value="treeHtml" escape="false"/>
			</div>
			

			<table>
			<ds:available test="archiwizacja.dekretacja.reczna.listadzialow">
				<tr>
					<td>
						<ds:lang text="Departament"/>
					</td>
				</tr>
				<tr class="formTableDisabled">
					<td>
						<ww:select cssClass="'sel'" multiple="true" name="'targetGuids'" list="divisions" listKey="guid" listValue="name" id="selectedGuids" size="10"/>
					</td>
				</tr>
			</ds:available>
			<tr>
				<td>
					Lista u�ytkownik�w:
				</td>
			</tr>
			<tr class="formTableDisabled">
				<td>
					<ww:select cssClass="'sel'" multiple="true" name="'targetUsers'" list="users" listKey="name" listValue="lastname+' '+firstname" id="selectedUsers" size="10"/>
				</td>
			</tr>
		    </table>
        </td>
    </tr>
</table>
<ds:event name="'doAssignment'" value="'Przeka� zadanie'"/>
<ww:if test="canCreateReadOnly">
    <ds:event name="'doCreateReadOnly'" value="'Przeka� do wiadomo�ci'"/>
</ww:if>

<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>


