<%-- 
    Document   : recipient-lookup
    Created on : 2009-06-24, 14:44:37
    Author     : Michał Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="application/json; charset=utf-8" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
[<ww:iterator value="users" status="status"><ww:if test="!#status.first">,</ww:if>{
	"recipientLastname":"<ww:property value="lastname"/>",
	"recipientFirstname":"<ww:property value="firstname"/>",
	"recipientName":"<ww:property value="name"/>",
	"recipientDivision":"<ww:property value="getDivisionsWithoutGroupPosition()[0].getGuid()"/>",
	"recipientDivisionName":"<ww:property value="getDivisionsWithoutGroupPosition()[0].getName()"/>"
}</ww:iterator>]
 


