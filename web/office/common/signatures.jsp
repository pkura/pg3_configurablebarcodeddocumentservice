<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

<ww:if test="!signatureBeans.empty">
	
	<table>
	    <tr>
	        <th><ds:lang text="Autor"/></th>
	        <th><ds:lang text="Data"/></th>
	        <th><ds:lang text="Typ"/></th>
	        <th colspan="2" style="text-align:center"><ds:lang text="Pobierz"/>:</th>
	        <th></th>
	        <th></th>
	    </tr>
	    
	    <ww:iterator value="signatureBeans">
	    	<tr>
		    	<td>&nbsp;<ww:property value="author.asFirstnameLastname()"/></td>
		    	<td>&nbsp;<ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
		    	<td>&nbsp;<ww:property value="typeAsString"/></td>
	            <td><a href="<ww:url value="'/office/common/document-signature.action?getDoc=true&signatureId='+id"/>"><ds:lang text="dokument"/></a></td>
	           	<ww:if test="type == null || type == 1">
	           		<td><a href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+id"/>"><ds:lang text="podpis"/></a></td>
	            	<td><a href="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doVerify=true&signatureId='+id+'&id='+documentId"/>', 'sign', 500, 400);"><ds:lang text="weryfikuj"/></a></td>
	            	<td><ww:if test="correctlyVerified != null && !correctlyVerified"><span style="color: red"><ds:lang text="bladWpodpisie"/></span></ww:if></td>
            	</ww:if>
            </tr>
	    </ww:iterator>
	</table>
	
	
</ww:if>
<ww:else>
	&nbsp;
	<br />
</ww:else>

<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>