<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N create-attachment.jsp N-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="<ww:url value="'/tiny_mce/tiny_mce.js'"/>"></script>
<script language="javascript" type="text/javascript">
        // Notice: The simple theme does not use all options some of them are l
        tinyMCE.init({
                mode : "exact",
                elements : "content",
                theme : "advanced",
                plugins : "fullpage,table,searchreplace,paste",
                theme_advanced_buttons1_add : "fontselect,fontsizeselect",
                theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
                theme_advanced_buttons2_add : "separator,forecolor,backcolor",
                theme_advanced_buttons3_add_before : "tablecontrols,separator",
                language : "pl",
                theme_advanced_toolbar_location : "top",
		        theme_advanced_toolbar_align : "left",
                theme_advanced_disable : "image,cleanup,anchor,help"
         //       width: "255",
         //       height: "150"
        });

</script>
<!-- /tinyMCE -->


<form action="<ww:url value="baseLink"/>" method="post">
    <ww:hidden name="'entryId'" value="entryId"/>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'attachmentId'" value="attachmentId"/>
    <ww:hidden name="'revisionId'" value="revisionId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'selectedAttachmentId'" value="selectedAttachmentId"/>
    <ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><ds:lang text="Tytul"/><span class="star">*</span>:</td>
            <td><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="KodKreskowy"/>: </td>
            <td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Tresc"/>: </td>
            <td><ww:textarea name="'content'" id="content" cols="100" rows="25" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <ww:if test="revisionId == null">
                     <ds:event name="'doAddWithEditor'" value="getText('Zapisz')" disabled="readOnly || blocked"
                          onclick="'if (!validateForm()) return false'" cssClass="'btn saveBtn'"/>
                </ww:if>
                <ww:else>
                    <ds:event name="'doEdit'" value="getText('Zapisz')" disabled="readOnly || blocked"
                          onclick="'if (!validateForm()) return false'" cssClass="'btn saveBtn'"/>
                </ww:else>

            </td>
        </tr>
    </table>
      <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>


<script type="text/javascript">
    function validateForm()
    {
        var title = document.getElementById('title').value;
        if (title.trim().length == 0) { alert('<ds:lang text="NiePodanoTytulu"/>'); return false; }
        return true;
    }
</script>