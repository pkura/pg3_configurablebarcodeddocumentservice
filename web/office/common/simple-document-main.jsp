<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:if test="getDocumentType().toString().equals('in')">
	<h1><ds:lang text="PismoPrzychodzace" /></h1>
</ww:if>
<ww:else>
	<h1><ds:lang text="PismoWychodzace"/></h1>
</ww:else>

<style>



</style>

<ds:available test="!dwr.fields">
<script type="text/javascript">
	/** Atrybuty dla mechanizmu ds:part */
	var partAttributes = new Array();
	
	/** Prefixy */
	partAttributes.prefixes = new Array();
</script>

<ds:container name="tabs">
	<ds:part name="tabs">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
	</ww:iterator>
</div>
	</ds:part>
</ds:container>
</ds:available>


<hr size="1" align="left" class="horizontalLine" width="100%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>

<div id="promptDiv" style="overflow-y: auto; overflow-x: hidden; max-height: 200px;"></div>

<ds:available test="!dwr.fields">
<script type="text/javascript">
	var submitPressed = false;
	
	function checkSubmit() {
		//alert('checkSubmit - ' + submitPressed);
		
		return submitPressed;
	}
</script>
</ds:available>

<form id="form" action="<ww:url value="getBaseUrl"/>" method="post" class="dwr" enctype="multipart/form-data" onsubmit="return checkSubmit();">
		<ds:available test="layout2">
			<ww:if test="actionTypeString != 'NEW_DOCUMENT'">
				<div id="middleContainer" style="clear: both; overflow: hidden; position: relative; top: -13px; *top: -30px; border-top: 0px solid transparent; *padding-bottom: 8px;"> <!-- BIG TABLE start -->
			</ww:if>
		</ds:available>		
      <div id="senderAjaxError"></div>
<div id="senderData" style="display: none"></div>
<div id="recipientData" style="display: none"></div>
<ww:hidden name="'filestoken'" value="filestoken"/>
<ww:hidden name="'openViewer'" id="openViewer" value="openViewer"/>

<ds:available test="dwr.fields">
    <ds:lang text="RodzajDokumentuDocusafe"/>:
	<ww:if test="documentKinds.size() > 1">
        <ww:select id="documentKindCn" name="'dockindCn'" list="documentKinds"
                   listKey="cn" listValue="name" value="dockindCn" cssClass="'sel'"
                   onchange="'this.form.submit()'"/>
	</ww:if>
    <ww:else>
    <ww:hidden name="'documentKindCn'" value="documentKindCn"></ww:hidden>
        <ww:property value="documentKindCn"/>
    </ww:else>
    <script type="text/javascript">
        var dwrParams = {
            context:"dockind",
            action:"new",
            dockind:"<ww:property value='dockindCn'/>"
        };
    </script>
    <ds:dwr-fields/>
</ds:available>

<ds:available test="!dwr.fields">
<script type="text/javascript">
	var __dontFixItemsWidth = true;

	

	function fixWidth() {
		/*$j('.partHeader').each(function() {
			var jqParentWidth = $j(this).parent().parent().width();
			$j(this).parent().css('width', jqParentWidth + 'px');
			//trace('newWidth: ' + $j(this).attr('offsetWidth'));
		});*/

		$j('div.group div:first-child').each(function () {
			var parentWidth = $j(this).parent().width();
			$j(this).css('width', parentWidth + 'px');
		});
	}

	function writeUsername2Inputs(src, delimiter, firstnameId, lastnameId) {
		var jqSel = $j(src);

		var selIndex = jqSel.prop('selectedIndex');

		var names  = jqSel.children()[selIndex].text.split(delimiter);

		try {
			$j('#' + lastnameId).val(names[0]);
			$j('#' + firstnameId).val(names[1]);
		} catch(e) {}
	}

	$j(document).ready(function() {
		var ie = $j.browser.msie;
		
		if(1) {
			var maxLen = 0;
			$j('span.label').each(function() {
				//console.warn($j(this).width());
				if($j(this).width() > maxLen)
					maxLen = $j(this).width();
			});
			
			//alert(maxLen);
			$j('span.label').css('width', maxLen + 'px').css('display', 'inline');
		}
			

		sortParts(containerStructure);
		$j('#fastAssignmentSelectUser').bind('change', function() {
			writeUsername2Inputs(this, ' ', 'recipientFirstname', 'recipientLastname');

			$j.post("recipient-lookup.action",
			{
				lastname:getInputValue('recipientLastname').trim(),
				firstname:getInputValue('recipientFirstname').trim()
			},
			function(data){
				$j("#recipientData").html(data.toString());
				//alert(data);
				senders = eval(data);
				<%--trace(senders[0]);--%>
				//$j('#recipientDivision').get(0).value = senders[0].recipientDivisionName;
				setUTFInputVal($j('#senderAjaxError'), $j('#recipientDivision'), senders[0].recipientDivisionName); 

				trace('Selected division: ' + senders[0].recipientDivision);
				$j('#fastAssignmentSelectDivision option[value=' + senders[0].recipientDivision + ']').attr('selected', true);
				$j('#reserveFastAssignmentSelectDivision').val(senders[0].recipientDivision);
			});
		});

		$j('#fastAssignmentSelectDivision').bind('change', function() {
			writeUsername2Inputs(this, null, null, 'recipientDivision');
			<ds:available test="selectDivisionWithoutUser">
				$j('#recipientLastname, #recipientFirstname, #fastAssignmentSelectUser').val('');
			</ds:available>
		});
		
		
		$j('div.group').sortable({ 
			cursor: 'move',
			items: 'div:not(div.part:first-child)',
			start: function(event, ui) {
				ui.item.addClass('dragging');
			},
			stop: function(event, ui) {
				ui.item.removeClass('dragging');
			}
		});
		$j('div.group div:first-child').css('cursor', 'default');
		
		$j(".container").sortable({
			connectWith: '.container',
			start: function(event, ui) {
				ui.item.addClass('dragging');
				$j('div.container').addClass('draggingArea');
			},
			stop: function(event, ui){
				ui.item.removeClass('dragging');
				$j('div.container').removeClass('draggingArea');
				if(ie)
					fixWidth();
			}
		});

		if(ie) {
			fixWidth();
		}
		
		
	});

	
</script>
<style>
	.dragging {
		background: #FFFBC4;
		border: 1px solid gray;
		opacity: 0.8;
	}
	
	.draggingArea {
		border: 1px dotted gray;
	}
</style>

<!--<table id="otherTab" border="0" cellspacing="0" cellpadding="0" <ds:available test="!documentMain.officeDate">style="display:none;"</ds:available>>-->
<!-- Nadawca -->

		
<ds:container name="person">

<ds:part name="populated"><!-- automatycznie generowana tre�� strony --></ds:part>

<%--<ds:part name="senderOrReceiver">
    <span class="partHeader"><ww:if test="getDocumentType().toString().equals('in')">Nadawca</ww:if><ww:else>Odbiorca</ww:else></span>
</ds:part>--%>

<ds:part name="documentDate">
	<span class="label"><ds:lang text="DataPisma"/>:</span>
    <span class="content"><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/></span>
</ds:part>

<ds:part name="delivery">
    <span class="label"><ds:lang text="SposobDostarczenia"/>:</span>
    <span class="content"><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
        headerKey="''" headerValue="getText('select.wybierz')"
        value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/>
        
    </span>
</ds:part>




<ds:part name="javascript">
<!-- readonly="#exists" -->
<%--cssClass="#exists ? 'txt_ro' : 'txt'"--%>
<script type="text/javascript">
Calendar.setup({
    inputField     :    "documentDate",     // id of the input field
    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
    button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
    align          :    "Tl",           // alignment (defaults to "Bl")
    singleClick    :    true
});

//lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
//jako anonimowego
var senderFields = new Array(
 'personTitle', 'personFirstname', 'personLastname', 'personOrganization',
 'personZip', 'personLocation', 'personCountry', 'personEmail', 'personFax',
 'personStreetAddress');

function disableEnableSender(checkbox)
{

 for (var i=0; i < senderFields.length; i++)
 {
	 traceStart('disableEnableSender');
	 try {
     	document.getElementById(senderFields[i]).disabled = checkbox.checked;
		trace('item: ' + senderFields[i]);
	 } catch(e) {}

	 traceEnd();
 }
}

function getQueryStringPart(stringPart, inputId) {
	var el = document.getElementById(inputId);
	if(el)
		return stringPart + "=" + el.value + "&" ;

	return "";
}

function openPersonPopup(lparam, dictionaryType)
{
    if (lparam == 'sender')
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType + "&"
        	+getQueryStringPart('title','personTitle')
        	+getQueryStringPart('firstname','personFirstname')
        	+getQueryStringPart('lastname','personLastname')
        	+getQueryStringPart('organization','personOrganization')
        	+getQueryStringPart('street','personStreetAddress')
        	+getQueryStringPart('zip','personZip')
        	+getQueryStringPart('location','personLocation')
        	+getQueryStringPart('country','personCountry')
        	+getQueryStringPart('email','personEmail')
        	+getQueryStringPart('fax','personFax')
			+'&canAddAndSubmit=true', 'person', 700,650);
           // '&title='+document.getElementById('personTitle').value+
            //'&firstname='+document.getElementById('personFirstname').value+
           // '&lastname='+document.getElementById('personLastname').value+
            //'&organization='+document.getElementById('personOrganization').value+
            //'&street='+document.getElementById('personStreetAddress').value+
            //'&zip='+document.getElementById('personZip').value+
           // '&location='+document.getElementById('personLocation').value+
           // '&country='+document.getElementById('personCountry').value+
           // '&email='+document.getElementById('personEmail').value+
           // '&fax='+document.getElementById('personFax').value+
           // '&canAddAndSubmit=true', 'person', 700,650);
    }
    else
    {
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true', 'person', 700,650);
    }
}


//lparam - identyfikator
//wparam - pozycja na li�cie
function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
{
 //alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
 var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
 //alert('url='+url);
 openToolWindow(url, 'person', 700,650);
}

function personSummary(map)
{
 var s = '';
 if (map.title != null && map.title.length > 0)
     s += map.title;

 if (map.firstname != null && map.firstname.length > 0)
 {
     if (s.length > 0) s += ' ';
     s += map.firstname;
 }

 if (map.lastname != null && map.lastname.length > 0)
 {
     if (s.length > 0) s += ' ';
     s += map.lastname;
 }

 if (map.organization != null && map.organization.length > 0)
 {
     if (s.length > 0) s += ' / ';
     s += map.organization;
 }
 if(map.organizationDivision !=null && map.organizationDivision.length>0)
 {
 	if (s.length > 0) s += ' / ';
     s += map.organizationDivision;
 }

 if (map.street != null && map.street.length > 0)
 {
     if (s.length > 0) s += ' / ';
     s += map.street;
 }

 if (map.location != null && map.location.length > 0)
 {
     if (s.length > 0) s += ' / ';
     s += map.location;
 }

 return s;
}

function __accept_person_duplicate(map, lparam, wparam)
{
 if (lparam == 'recipient')
 {
	 try {
     var sel = document.getElementById('recipients');

     for (var i=0; i < sel.options.length; i++)
     {
         eval('var smap='+sel.options[i].value);
         if (compareArrays(smap, map, true))
         {
             //alert('Istnieje ju� taki odbiorca');
             return true;
         }
     }
	 } catch(e) {}
 }

 return false;
}

/*
 Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
 //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

 if (lparam == 'recipient')
 {
	 
     var sel = document.getElementById('recipients');
     
	if(sel) {
     if (wparam != null && (''+wparam).length > 0)
     {
         setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
     }
     else
     {
         addOption(sel, JSONStringify(map), personSummary(map));
     }
	} else {
		/* jesli nie ma multiselecta #recipients, to znaczy ze mamy pola jak w senderze (tak jest w play-u) */
		/* Robimy wi�c z dupy sztuczk�: (wparam nie ma znaczenia)*/
		/* TODO - to zmieni�, gdy b�d� dwa xmle - dla nadawcy i odbiorcy */
		__accept_person(map, 'sender', wparam)
	}
	
 }
 else if (lparam == 'sender')
 {
     $j('#personTitle').val(map.title);
     $j('#personFirstname').val(map.firstname);
     $j('#personLastname').val(map.lastname);
     $j('#personOrganization').val(map.organization);
     $j('#personStreetAddress').val(map.street);
     $j('#personZip').val(map.zip);
     $j('#personLocation').val(map.location);
     $j('#personCountry').val(map.country);
     $j('#personEmail').val(map.email);
     $j('#personFax').val(map.fax);
 }
}


	$j(document).ready(function(){
		$j("#senderAjaxError").ajaxError(function(event, request, settings){
		   $j(this).append("Error requesting page " + settings.url + '<br>');
		   console.error(event, request, settings);
		});

		/* wylaczamy autocomplete */
		$j('#personLastname').attr('autocomplete', 'off');
		$j('#personOrganization').attr('autocomplete', 'off');
		$j('#recipientLastname').attr('autocomplete', 'off');

		$j(document).click(function() {
			if($j('#promptDiv').css('visibility') == 'visible')
				$j('#promptDiv').css('visibility', 'hidden').css('display', 'none');
		});

		$j('#personLastname').keypress(function(event) {
			if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
				putItem(selectedItem);

				$j('#form').bind('submit', function() {
					return false;
				});
				dontSendForm = true;
				return false;
			}

			if(dontSendForm) {
				dontSendForm = false;
				$j('#form').unbind('submit');
			}

			return true;
		});


		$j('#personOrganization').keypress(function(event) {
			if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
				putItem(selectedItem);

				$j('#form').bind('submit', function() {
					return false;
				});
				dontSendForm = true;
				return false;
			}

			if(dontSendForm) {
				dontSendForm = false;
				$j('#form').unbind('submit');
			}

			return true;
		});


		// odbiorca
		$j('#recipientLastname').keypress(function(event) {
			if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
				putItem(selectedItem);

				$j('#form').bind('submit', function() {
					return false;
				});
				dontSendForm = true;
				return false;
			}

			if(dontSendForm) {
				dontSendForm = false;
				$j('#form').unbind('submit');
			}

			return true;
		});
		
		
		
	});

	/* wyswietla podpowiedzi dla nazwiska - sprawdza czy pole nie jest puste - wtedy podpowiedzi nie wyswietla */
	var selectedItem = 0;
	var KEY_DOWN = 40;
	var KEY_UP = 38;
	var KEY_ENTER = 13;
	var KEY_ESCAPE = 27;
	var pressedKey = 0;
	var itemsLength = 0;
	var senders = null;
	var ie = document.all && !window.opera;
	var dontSendForm = false;

	/* tych pol nie wyswietlamy w podpowiedzi, ale sa dostepne (display: none) */
	var hiddenItems = new Array('recipientDivision', 'recipientName', 'personZip',
			'personEmail', 'senderId');

	/* usuwa poczatkowe spacje */
	String.prototype.trim = function() {
		return this.replace(/^\s*|\s*$/i, '');
	}

	/* poprawia pozycje promptDiv, zeby miescil sie w ekranie przegladarki */
	function fixPromptDivPos(docWidth, pd) {
		traceStart('fixPromptDivPos()');
		
		var rightPos = pd[0].offsetLeft + pd[0].scrollWidth;

		/* mozilla nie liczy scrollbara */
		if($j.browser.mozilla && pd[0].scrollHeight)
			rightPos += 15;

		trace('docWidth: ' + docWidth);
		trace('rightPos: ' + rightPos);

		//alert('posX: ' + docWidth + ', ' + rightPos + ', ' + (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px'); 

		if(rightPos > docWidth) {
			
			trace('Changing leftPos');
			pd.css('left', (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px');
		}

		traceEnd();	
	}


	function showMatchingSender(e, caller){

		pressedKey = e.keyCode;

		var ln = caller;
		var pd = $j('#promptDiv');
		var str = '';

		traceStart('showMatchingSender');
		trace('pressedKey = ' + pressedKey);

		if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
			if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
				trace('Loading ... ');
				
				pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\" />";
				pd.css('left', getPos(ln.get(0)).left  + 'px');
				pd.css('top', getPos(ln.get(0)).top + ln.height() + 'px');
				pd.css('visibility', 'visible').css('display', 'inline');
				
			} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
				trace('Only navigation (without sending ajax request)');
				displayPrompt(senders, ln.attr('id'), hiddenItems);
				traceEnd();
				return;
			}

			trace('Sending ajax request for <<sender-lookup.action>>');
			$j.post("sender-lookup.action",
				 {
				 	lastname:getInputValue('personLastname').trim(),
				 	organization:getInputValue('personOrganization').trim(),
				 	title:getInputValue('personTitle').trim(),
				 	firstname:getInputValue('personFirstname').trim(),
				 	streetAddress:getInputValue('personStreetAddress').trim(),
				 	zip:getInputValue('personZip').trim(),
				 	email:getInputValue('personEmail').trim(),
				 	fax:getInputValue('personFax').trim(),
				 	location:getInputValue('personLocation').trim(),
				 	dictionaryType:<ww:if test="getDocumentType().equals('in')">'SENDER'</ww:if><ww:else>'RECIPIENT'</ww:else>
				 }, function(data){
					var i, len;
					try {
						/*data = data.split('');
						for(i = 0, len = data.length; i < len; i ++) {
							if(data[i] == '') {
								data[i] == '/';
							}
						}
						data = data.join('');
						console.warn(data);*/
						$j("#senderData").html(data.toString());
						senders = eval(data);
					} catch(e) {
						console.error(e);
					}
					var docWidth = $j(document).width();
					displayPrompt(senders, ln.attr('id'), hiddenItems);
					fixPromptDivPos(docWidth, pd);
			}, 'text');
		} else {
			trace('Hidding');
			pd.css('visibility', 'hidden').css('display', 'none');
		}

		traceEnd();
	}


	function highlightItem(id) {
		/* usuwamy stare pod�wietlenie */
		$j('#prompt_item_' + selectedItem).removeClass('selected');

		$j('#' + id).addClass('selected');

		/* wyciagamy numer */
		selectedItem = parseInt((id).substr(12));
	}

	function unHighlightItem(id) {
		$j('#' + id).removeClass('selected');

		/* podswietlenie aktualnego zostaje */
		$j('#prompt_item_' + selectedItem).addClass('selected');
	}

	function putItem() {
		sender = senders[selectedItem];

		if(sender) {
			var c = 0;
			for(property in sender){
				var jq_sel = '#prompt_item_' + selectedItem + ' td.' + property; // wybieramy komorke
				var val = '';

				 // pobieramy tekst z komorki
				if(ie)
					val = $j(jq_sel).prop('innerText');
				else
					val = $j(jq_sel).prop('textContent');

				//zapisujemy do input text
				$j('#' + property).attr('value', val);
				
			}

			$j('#promptDiv').css('visibility', 'hidden').css('display', 'none');

			/* odznaczamy checkbox dodaj do s�ownika */
			if(sender.personLastname) {
				$j('#createPerson').attr('checked', false);
				<ds:available test="layout2">
		        Custom.clear();
		        </ds:available>
			}
			
			/* zapisujemy dzial do multiselecta */
			if(sender.recipientDivision != null) {
				var divInput = $j('#recipientDivision').get(0);
				var divSel = $j('#fastAssignmentSelectDivision').get(0);
				var divisionNamePresent = false; // czy dzial jest w selekcie
	
				for(i = 0; i < divSel.options.length; i ++) {
					if(divSel.options[i].value == sender.recipientDivision) {
						divSel.options[i].selected = true;
	
						divisionNamePresent = true;
						$j('#reserveFastAssignmentSelectDivision').val(sender.recipientDivision);
						setUTFInputVal($j('#senderAjaxError'), $j(divInput), sender.recipientDivisionName); 
						break;
					}
				}
				if(!divisionNamePresent) {
					$j('#recipientDivision').val(sender.recipientDivisionName);
					$j('#fastAssignmentSelectDivision').val('');
					$j('#reserveFastAssignmentSelectDivision').val(sender.recipientDivision);
				}
	
				/* zapisujemy osobe do multiselecta */
				var usrSel = $j('#fastAssignmentSelectUser').get(0);
	
				for(i = 0; i < usrSel.options.length; i ++) {
					if(usrSel.options[i].value == sender.recipientName) {
						usrSel.options[i].selected = true;
						break;
					}
				}
			}

		}

	}

	

		function clearPersonInputs() {
			var form_el = document.forms['form'];

			traceStart('clearpersonInputs()');

			for(i = 0; i < form_el.length; i ++) {
				if((form_el.elements[i].type == 'text') && (form_el.elements[i].id.indexOf('person') != -1)) {
					trace('Clearing item [ID] ' + form_el.elements[i].id);
					form_el.elements[i].value = '';
				}
			}

			traceEnd();
		}

		function clearRecipientInputs() {
			var form_el = document.forms['form'];
			traceStart('clearRecipientInputs()');

			for(i = 0; i < form_el.length; i ++) {
				if((form_el.elements[i].type == 'text') && (form_el.elements[i].id.indexOf('recipient') != -1)) {
					trace('Clearing item [ID] ' + form_el.elements[i].id);
					form_el.elements[i].value = '';
				}	
			}

			/* jeszcze odznaczamy selecty */
			$j('#fastAssignmentSelectDivision').prop('selectedIndex', -1);
			$j('#fastAssignmentSelectUser').prop('selectedIndex', -1);

			traceEnd();
		}
</script>
</ds:part>

<ds:part name="personAnonymous">
    <span class="label"><ds:lang text="Anonim"/>:</span>
    <span class="content">
    	<ww:checkbox name="'personAnonymous'" fieldValue="'true'" value="personAnonymous" onchange="'disableEnableSender(this);'" id="personAnonymous" />
    	&nbsp;
    	<script type="text/javascript">
    		disableEnableSender(document.getElementById('personAnonymous'));
    	</script>
    </span>
</ds:part>

<ds:part name="personTitle">
    <span class="label"><ds:lang text="Tytul/stanowisko"/>:</span>
    <span class="content"><ww:textfield name="'personTitle'" size="50" maxlength="50" cssClass="'txt'" id="personTitle"  cssStyle="'width: 200px;'"/></span>
</ds:part>

<ds:part name="personFirstname">
    <span class="label"><ds:lang text="Imie"/>:</span>
    <span class="content">
    	<ww:textfield name="'personFirstname'" size="50" maxlength="50" cssClass="'txt'" id="personFirstname"  cssStyle="'width: 184px;'"/>
    	<a href="javascript:clearPersonInputs()" title="Wyczy�� pola" class="clearFieldsIco">
    			<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/minus2.gif" />
    		</a>
    </span>
</ds:part>

<ds:part name="personLastname">
    <span class="label"><ds:lang text="Nazwisko"/>:</span>
    <span class="content"><ww:textfield name="'personLastname'" size="50" maxlength="50" cssClass="txt" id="personLastname" cssStyle="'width: 200px'"
    onkeyup="'showMatchingSender(event, $j(this));'" /></span>
</ds:part>

<ds:part name="personOrganization">
    <span class="label"><ds:lang text="Firma/Urzad"/>:</span>
    <span class="content"><ww:textfield  name="'personOrganization'" size="50" maxlength="50" cssClass="'txt'" id="personOrganization" cssStyle="'width: 200px'"
    onkeyup="'showMatchingSender(event, $j(this));'" /></span>
</ds:part>

<ds:part name="personStreetAddress">
    <span class="label"><ds:lang text="Ulica"/>:</span>
    <span class="content"><ww:textfield name="'personStreetAddress'" size="50" maxlength="50" cssClass="'txt'" id="personStreetAddress" cssStyle="'width: 200px'"/></span>
</ds:part>

<ds:part name="personZipAndLocation">
    <span class="label"><ds:lang text="KodPocztowyImiejscowosc"/>:</span>
    <span class="content"><ww:textfield cssStyle="'width: 50px;'" name="'personZip'" size="7" maxlength="14" cssClass="'txt'" id="personZip"/>
        <ww:textfield cssStyle="'width: 142px; margin-left: 2px;'" name="'personLocation'" size="39" maxlength="50" cssClass="'txt'" id="personLocation"  onchange="'showCode();'" />
        <br/><span class="warning" id="personZipError"> </span></span>
</ds:part>

<ds:part name="personCountry">
    <span class="label"><ds:lang text="Kraj"/>:</span>
    <span class="content"><ww:select name="'personCountry'"
        list="@pl.compan.docusafe.util.Countries@COUNTRIES"
        listKey="alpha2" listValue="name" value="'PL'"
        id="personCountry" cssClass="'sel'" cssStyle="'width: 206px;'" onchange="this.form.personZip.validator.revalidate();"
        />
    </span>
</ds:part>

<ds:part name="personEmail">
    <span class="label"><ds:lang text="Email"/>:</span>
    <span class="content"><ww:textfield name="'personEmail'" size="30" maxlength="30" cssClass="'txt'" id="personEmail"  cssStyle="'width: 200px'"/></span>
</ds:part>

<ds:part name="personFax">
    <span class="label"><ds:lang text="Faks"/>:</span>
    <span class="content"><ww:textfield name="'personFax'" size="30" maxlength="30" cssClass="'txt'" id="personFax"  cssStyle="'width: 200px'"/></span>
</ds:part>

<ds:part name="selectPersonButton">
	<ww:if test="getDocumentType().toString().equals('in')">
		<span><input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" style="width: 204px" ></span>
	</ww:if>
	<ww:else>
		<span><input type="button" value="<ds:lang text="WybierzOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" style="width: 204px" ></span>
	</ww:else>
</ds:part>

<ds:part name="createPerson">
    <span class="label">Dodaj do s�ownika:</span>
    <span class="content">
    	<ww:checkbox name="'createPerson'" fieldValue="'true'" value="'true'" id="createPerson" />
		&nbsp;
    </span>
</ds:part>


<ds:part name="Others">
    <span class="label"><ds:lang text="Inne"/></span>
</ds:part>

<ds:part name="postalRegNumber">
    <span class="label"><ds:lang text="NrPrzesylkiRejestrowanej"/>(R):</span>
    <span class="content"><ww:textfield  cssStyle="'width: 200px'" name="'postalRegNumber'" size="25" maxlength="25" cssClass="'txt barcodeInput'" id="postalRegNumber" />
        
    </span>
</ds:part>

<ds:part name="summary"> 
    <span class="label"><ds:lang text="OpisPisma"/>:</span>
    <span class="content"><ww:textarea  rows="'2'" cols="'50'" cssStyle="'width: 200px'" name="'summary'" cssClass="'txt barcodeInput'" id="summary" />
    </span>
</ds:part>


<ds:part name="multiFiles">
	<ww:iterator value="loadFiles">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/><br/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>"><br/>
	</ww:iterator> 
    <span class="label">
    	<ds:lang text="Plik"/><ds:available test="documentMain.check.attachment"><span class="star always">*</span></ds:available>:
    	<a href="#" onclick="addAtta(this); return false;" class="addLink" ><ds:lang text="DolaczKolejny" /></a>	
    </span>
    <span class="content">
    	<ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="''"/>
    </span>
    <span class="contener">
    	<img src="<ww:url value="'/img/minus2.gif'" />" class="removeLink" onclick="delAtta(this);" title="Usu�" style="cursor: pointer; margin-top: 6px;">
    </span>
<script type="text/javascript">
$j('img.removeLink').hide();
function addAtta(domObj)
{
	$j('img.removeLink').show();
	var str = '<div class="multiFiles_part part" style="display:none">';
		str += $j('#multiFiles_part, div.multiFiles_part').last().html();
		str += '</div>';
		
	$j(domObj).parent('span.label').parent('div.part').after(str);
	$j('#multiFiles_part, div.multiFiles_part').last().slideDown(300);
	$j('a.addLink:last').show();
	$j(domObj).fadeOut('fast');
}

function delAtta(domObj)
{
	/* usuwamy tylko gdy mamy dwa lub wi�cej plik�w */
	if($j('#multiFiles_part, div.multiFiles_part').length > 1) {
		if ($j('img.removeLink:last').filter(domObj).length > 0) {
			$j('a.addLink').eq(-2).show();	// pokazujemy przedostatni
		}
		
		/* ukrywamy ikonk� usu�, je�li zosta�y tylko dwa pliki */
		if($j('#multiFiles_part, div.multiFiles_part').length < 3)
			$j('img.removeLink').hide();
		
		$j(domObj).parent('span.contener').parent('div.part').slideUp(300, function() {
			$j(this).remove();
		});
	}
}
</script>
</ds:part>

<ds:part name="original">
    <span class="label"><ds:lang text="Oryginal"/>:</span>
    <span class="content"><ww:checkbox name="'original'"  fieldValue="'true'" id="original" value="true"></ww:checkbox>&nbsp;
    </span>
</ds:part>


</ds:container>



<!-- Odbiorca -->

<ds:container name="user">

			<ds:part name="user-populated"><!-- automatycznie generowana tre�� strony --></ds:part>
	
			<%--<ds:part name="userTitle">
				<span class="partHeader">
					<ww:if test="getDocumentType().toString().equals('in')">Odbiorca</ww:if><ww:else>Nadawca</ww:else>
				</span>
			</ds:part>--%>

			<ds:part name="userFirstname">
				<span class="label">
					Imi�:
				</span>
				<span class="content">
					<ww:textfield id="recipientFirstname" name="'userFirstname'" cssClass="'txt'" cssStyle="'width: 184px'"/>
					<a href="javascript:clearRecipientInputs()" title="Wyczy�� pola" class="clearFieldsIco">
		    			<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/minus2.gif" />
		    		</a>
				</span>
			</ds:part>

			<ds:part name="userLastname">
				<span class="label">
					Nazwisko:
				</span>
				<span class="content">
					<ww:textfield id="recipientLastname" name="'userLastname'" cssClass="'txt'" cssStyle="'width: 200px'"  onkeyup="'showMatchingRecipient(event, $j(this));'" />
				</span>
			</ds:part>

			<ds:part name="userDivision">
				<span class="label">
					<ds:lang text="Dzial"/> 
				</span>
				<span class="content">
					<ww:textfield id="recipientDivision" name="'userDivision'" cssClass="'txt'" cssStyle="'width: 200px'"/>
				</span>
			</ds:part>

			<ds:part name="fastAssignmentSelectUser">
				<span class="label">
					<ww:select id="fastAssignmentSelectUser" name="'fastAssignmentSelectUser'" list="fastAssignmentUser"  listKey="name" listValue="asLastnameFirstname()" 
						size="10" cssClass="'sel'" cssStyle="'width: 153px;'" value="fastAssignmentSelectUser" />
					<script type="text/javascript">
						
					</script>
				</span>
				<span class="content">
					<ww:select name="'fastAssignmentSelectDivision'" id="fastAssignmentSelectDivision" list="fastAssignmentDivision" listKey="guid" listValue="name" 
						size="10" cssClass="'sel'" cssStyle="'width: 203px;'" />
					
				</span>
			</ds:part>
			
			<ww:hidden name="'reserveFastAssignmentSelectDivision'" id="reserveFastAssignmentSelectDivision" value="reserveFastAssignmentSelectDivision"/>
			<ds:part name="javascriptRecipient">
				<script type="text/javascript">
		function showMatchingRecipient(e, caller){
			pressedKey = e.keyCode;

			var ln = caller;
			var pd = $j('#promptDiv');
			var str = '';


			if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
				if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
					pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\"/>";

					traceStart('promptDiv position');
					trace('x: ' + getPos(ln.get(0)).left);
					trace('y: ' + getPos(ln.get(0)).top);
					traceEnd();
					
					
					pd.css('left', getPos(ln.get(0)).left  + 'px');
					pd.css('top', getPos(ln.get(0)).top + ln.height() + 'px');
					pd.css('visibility', 'visible').css('display', 'inline');
				} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
					trace('Only navigation (without sending ajax request)');
					displayPrompt(senders, ln.attr('id'), hiddenItems);
					return;
				}

				trace('Sending request to recipient-lookup.action ...');
				$j.post("recipient-lookup.action",
					{
						lastname:getInputValue('recipientLastname').trim(),
						division:getInputValue('recipientDivision').trim(),
						firstname:getInputValue('recipientFirstname').trim()
					},
					 function(data){
						$j("#recipientData").html(data.toString());
						//alert(data);
						senders = eval(data);
						var docWidth = $j(document).width();
						displayPrompt(senders, ln.attr('id'), hiddenItems);
						fixPromptDivPos(docWidth, pd);
						trace('Request recipient-lookup.action completed and prompt shown');
						traceEnd();
					});
			} else {
				pd.css('visibility', 'hidden').css('display', 'none');
			}


			/*$j.post("recipient-lookup.action", {lastname:$j("#recipientLastname").get(0).value, division:$j('#recipientDivision').get(0).value}, function(data){
				$j("#recipientData").html(data);
				recipients = eval(data);
			});*/
		}

	</script>
			</ds:part>
			
			<ds:part name="saveButtonsPart">
				<span>
			    	<input type="hidden" name="doOldUser" id="doOldUser"/>
					<input type="button" name="doOldUserButton" value="<ds:lang text="Zapisz"/> i dodaj kolejne dla tego u�ytkownika" class="btn"
							onclick="$j('#form').unbind('submit');document.getElementById('doNewUser').value = 'true'; submitPressed = true; $j('#form').submit();" cssStyle="*width: 260px;"/>
				</span>
				<span>
					<input type="hidden" name="doNewUser" id="doNewUser"/>
					<input type="button" name="doNewUserButton" value="<ds:lang text="Zapisz"/> i dodaj kolejne" class="btn"
							onclick="$j('#form').unbind('submit');document.getElementById('doOldUser').value = 'true'; submitPressed = true; $j('#form').submit();"  cssStyle="*width: 130px;"/>
				</span>
			</ds:part>
			<ds:part name="updateButtonsPart">
				<span>
					<ds:event name="'doUpdate'" value="getText('Zapisz')" cssClass="'btn'"/>
				</span>
			</ds:part>
	
			<ds:available test="p4HandWrittenSignature">
				<ds:part name="signaturePdfPart">
					<ww:if test="revisionId != null">
					Podpis potwierdzaj�cy odbi�r: 
					<a href="<ww:url value="'/repository/view-attachment-revision.do'"><ww:param name="'asPdf'" value="true"/><ww:param name="'id'" value="revisionId" /></ww:url>">
                                                   <img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
																title="<ds:lang text="PobierzZalacznik"/>" /></a>
																
					<%--<a class="imgView" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="revisionId"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">
								    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
								</a> --%>																	
					</ww:if>
				</ds:part>
			</ds:available>
	</ds:container>	
	
	<ww:if test="documentId != null">
	<div id="unlockAllDiv">
	<img src="<ww:url value="'/img/klucz.gif'"/>" class="unlockAllIco" onclick="unlockAllInputs(this)" />
	</div>
	</ww:if>
	
	<script type="text/javascript">
		/*	Mapa zawiera id zablokowanych element�w w postaci:
		 *	__blocked[id_container][blocked_input#1, blocked_input#2, ...]
		 */
		var __blocked = new Array();

		

		/* Odblokowuje wszystkie input z klasa .blocked i chowa obrazek
		 * 
		 * objImg		- 	obiekt dom obrazka z k��dk�			
		 */
		function unlockAllInputs(objImg) {
			$j('.blocked').removeClass('blocked').attr('disabled', false);
			objImg.style.display = 'none';
			$j('input[name=doUpdate]').attr('disabled', false);
			$j('img.ui-datepicker-trigger').show();
			$j('a.clearFieldsIco').css('visibility', 'visible');
		}
		

		/* Odblokowuje inputa - po klikni�ciu ikona k��dki znika
		 *
		 * objImg		-	obiekt dom obrazka z k��dk�			
		 * inputsTable	- 	tablica z id poszczeg�lnych input�w
		 * 
		 */
		function unlockInputs(objImg, inputsTable) {
			traceStart('unlockInputs()');
			//var objInput = $j('#' + inputId)[0];

			for(i = 0; i < inputsTable.length; i ++) {
				var item = inputsTable[i];
				trace('Enabling item: ' + item);
				$j('#' + item).attr('disabled', false).removeClass('blocked');
			}

			objImg.style.display = 'none';
			traceEnd();
		}


		/* Blokuje wszystkie inputy kontainer�w i wy�wietla k��dk� obok inputa
		 * do odblokowania go
		 */
		function lockContainersInputs() {
			traceStart('lockContainersInputs()');

			$j('div.container').each(function() {
				var containerId = this.id;
				traceStart(containerId);
				__blocked[containerId] = new Array();

				/* iterujemy po wszystkich widocznych inputach danego containera */
				$j('#' + containerId + ' input:visible:not(.btn), ' 
				+ '#' + containerId + ' select:visible').each(function() {
					var itemId = this.id;
					
					trace('Locking item ' + itemId);
					$j(this).attr('disabled', true).addClass('blocked');
					__blocked[containerId].push(itemId); 
				});

				/* dodajemy ikonk� k��dki (w pierwszym divie danego containera) - sprawdzamy czy div ma jakie� inputy */
				if(__blocked[containerId].length > 0) {
					$j(document.createElement("img")).attr("src","<ww:url value="'/img/klucz.gif'"/>")
						.prependTo($j(this).children('div:first'))
						.click(function() {
								unlockInputs(this, __blocked[containerId]);
							})
						.addClass('unlockIco')
						.attr('alt', 'Odblokuj pola')
						.attr('title', 'Odblokuj pola')
						.hover(function() {
								this.src = this.src.replace('.gif', '_anim.gif');
							}, function() {
								this.src = this.src.replace('_anim.gif', '.gif');
							});
				}

				traceEnd();
			});

			traceEnd();
		}

			$j(document).ready(function() {
				<ww:if test="documentId != null && false">
					//lockContainersInputs();
					$j('input:visible:not(.btn), select:visible')
						.attr('disabled', true)
						.addClass('blocked');
					$j('input[name=doUpdate]').attr('disabled', true);
					$j('img.ui-datepicker-trigger').hide();
					$j('a.clearFieldsIco').css('visibility', 'hidden');
				</ww:if>
			});


			/* Obs�uga wpisywania barcode */
			<ww:if test="documentId == null">
			traceStart('Barcode prefixes');			

			var __keyCount = 0;
			var __firstEnter = false;

			var __prefixes = new Array();
			var __tmpCounter = 0;
			var item;
			
			for(item in partAttributes.prefixes) {
			    trace('Prefix ' + __tmpCounter + ' ' + item + ' = ' + partAttributes.prefixes[item]);

			    __prefixes[__tmpCounter] = new Array();
				var pref = partAttributes.prefixes[item].split(',');

				/* rozkodowujemy dany prefix */
				for(i = 0; i < pref.length; i ++)
					pref[i] = parseInt(pref[i], 10); 

				__prefixes[__tmpCounter].prefix = pref;				
			    __prefixes[__tmpCounter].name = item.toString();
			    __prefixes[__tmpCounter].prefixLength = pref.length;

			    /* wy��czamy wysy�anie formularza po naci�ni�ciu entera */
				$j('#' + __prefixes[__tmpCounter].name).keydown(function(event) {
					if(event.keyCode == 13 && __firstEnter) {
						trace('First enter key was disabled');
						__firstEnter = false;

						/* chowamy podpowiedzi */
						$j('#promptDiv').css('visibility', 'hidden').css('display', 'none');
						
						return false;
					}
				});
			    
			    __tmpCounter ++;
			}

			

			traceEnd();
			

			$j(document).keydown(function(event) {
			    var keyCode = event.keyCode;

			    /* poni�ej 20 mamy szift, enter itp */
			    if(keyCode > 20) {
				trace('keyCode: ' + keyCode + ' charCode: ' + event.charCode);

					/* iterujemy po wszystkich prefiksach */
					for(i = 0; i < __prefixes.length; i ++) {
						var curPrefix = __prefixes[i];

						/* je�li znak w patternie */
						if(keyCode == curPrefix.prefix[__keyCount]) {
							trace(keyCode + ' is matching ' + __keyCount + '. item in ' + curPrefix.name);
							__keyCount ++;
							
							/* prefix pasuje */
							if(__keyCount == curPrefix.prefixLength) {
								trace('FOUND PREFIX!!! ' + curPrefix.name);
								$j('#' + curPrefix.name).val('');
								__firstEnter = true;
								
								$j('#' + curPrefix.name).focus();
							}

							/* nie przetwarzamy dalej - �eby nie wpisywa� prefiksu do inputa,
							 * kt�ry ma obecnie focusa
							 */
							return false;
						} else {
							trace('Clear buffer - item ' + __keyCount + '. doesnt match');						
							__keyCount = 0;
						}
					}									
			    }				
			});
			</ww:if>
	
	</script>
</ds:available>	
	
	 <ds:available test="layout2">
	 	<ww:if test="actionTypeString != 'NEW_DOCUMENT'">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ww:if>
		</ds:available>
		
	 
</form>

<ds:available test="!dwr.fields">
<script type="text/javascript">
/* sprawdza zip code */
var szv = new Validator(document.getElementById('personZip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('personZipError').innerHTML = '';
    if (document.getElementById('personZip').value != "")
    {
		showLocation();
	}
}

		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************ //
		//   |                  |			       |	   //
		//   |			        |			       |       //
		//   |			        |			       |       //
		//  \/			       \/			      \/       //

		var xmlHttp = null;
		var serverName = '<%= request.getServerName() + ":" %>';
		var port = '<%= request.getServerPort() %>';
		var contextPath = '<%= request.getContextPath() %>';
		var protocol = window.location.protocol;

		var polskieZnaki = new Array();
		var zamiana = new Array();

		function showCode()
		{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			var location=document.getElementById('personLocation').value;

			//Rozwiazanie problemu z polskimi znakami
			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			if(location==null)
			{
				alert ("No element with id 'location' found on the page");
				return;
			}
			//alert("http://" + serverName + port + contextPath + "/code.ajx?location=" + location);

			var locationName = null;

			try {
				locationName = $j('#personLocation').val();
			} catch(e) {
				trace('location is null');
			}

			var reqSrv = "code-lookup.action";
			var locationStr = '';
			var encData = null;

			$j.post(reqSrv,
			{
				location: locationName
			},
			function(data) {
				encData = eval(data);

				var zipCode = encData[0].code;
				if(zipCode == 'LocNotFound')
					alert('Nie znalaz�em kodu dla miejscowo�ci ' + locationName);
				else if (zipCode.trim().length >6)
				{
					alert("Nie ma jednego kodu pocztowego miejscowo�ci " + 
							locationName + ". Zakres(y) to: " + zipCode + ".");
					$j('#personZip').val(zipCode.substring(0, 6));
				}
				else
					$j('#personZip').val(encData[0].code);
				//alert(data);
			});

		}

		function showLocation()
		{
			traceStart('showLocation()');
			
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			var zip=document.getElementById('personZip').value;

			if(zip==null)
			{
				alert ("No element with id 'zip' found on the page");
				return;
			}

			//alert("http://" + serverName + port + contextPath + "/location.ajx?zip=" + zip);
			xmlHttp.open("GET",  protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('personLocation');
			};

			xmlHttp.send(null);

			traceEnd();
		}

		function stateChanged(variable)
		{

			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";


			if (xmlHttp.readyState==4)
			{

				if(xmlHttp.status==200)
				{

					var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];
					//alert(valid);

					if(variable =='personZip' && valid.childNodes[0].nodeValue=='LocNotFound')
					{
						alert("Nie znalaz�em kodu dla tej miejscowo�ci.");
					}
					else
					{
						var message = valid.childNodes[0].nodeValue;
					}
					if(valid.childNodes[0].nodeValue=='CodeNotFound' && variable =='personLocation')
					{
						alert("Nie znalaz�em miejscowo�ci dla danego kodu.");
						document.getElementById(variable).value='';
					}
					if(message.trim().length ==6)
					{
						document.getElementById(variable).value=message;
					}
					else if (message.trim().length >6 && variable=='personZip')
					{
						alert("Nie ma jednego kodu pocztowego dla danej miejscowo�ci. Zakres(y) to: " + message + ".");
						document.getElementById('personZip').value=message.substring(0, 6);
					}
					else if(variable=='personLocation' && valid.childNodes[0].nodeValue!='CodeNotFound')
					{
						document.getElementById(variable).value=message;
					}


				}
				else
				{
					trace("Error loading page"+ xmlHttp.status +":"+ xmlHttp.statusText);
				}
			}
			else  if (xmlHttp.readyState==0)
			{
				alert("	Object is uninitialized");
			}

		}

		function GetXmlHttpObject()
		{
			var xmlHttp=null;
			try
			{
			// Firefox, Opera 8.0+, Safari
				xmlHttp=new XMLHttpRequest();
			}
			catch (e)
			{
			//Internet Explorer
				try
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e)
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			return xmlHttp;
		}
		//*******************************//

		//   /\                          /\			      /\     //
		//   |			        |			       |     //
		//   |			        |			       |     //
		//   |		                 |			       |     //
		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************//

    szv.onEmpty = szv.onOK;

    szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('personZipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp."/> 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('personZipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp."/> (00-001)';
	}

	szv.canValidate = function() {
	var country = document.getElementById('personCountry');
	var countryCode = null;

	if(country) {
		countryCode = country.options[country.options.selectedIndex].value;
	
		return (countryCode == '' || countryCode == 'PL');
    }    
         return true;
    }

	function openViewer()
	{
		var objViewer = document.getElementById('openViewer');
		if(objViewer.value == 'true')
		{
				openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="revisionId"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);
			objViewer.value = false;
		}
	}
	openViewer();
	
	document.forms['form'].elements['postalRegNumber'].focus();
</script>
</ds:available>