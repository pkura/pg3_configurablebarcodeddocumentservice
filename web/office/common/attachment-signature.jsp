<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N attachment-signature.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="doSign">
	<object
	   classid="clsid:D484F478-2A45-419C-9CFF-8462A39177D9"
	   ID="MyControl" 
	   VIEWASTEXT>
	</object>

    <object id="SDoc" classid="clsid:38552364-0433-4F7F-910C-7925957Ef3A1" width=20 height=10>
      <param name="UploadScript" value="up.php">
    </object><br>

   <%-- <script type="text/javascript">
        var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
            request.getServerName() + ":" + request.getServerPort() %>';
        var object =  appletObject('SigApplet',
            'pl.compan.docusafe.applets.signature.SignatureApplet.class',
            '<ww:url value="'/signature.jar'"/>', 0, 0,
            { 'uploadUrl' : host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>',
              'downloadUrl' : host+'<ww:url value="'/office/common/document-signature.action'"/>'+';jsessionid=<%= request.getRequestedSessionId() %>'+'?doDownload=true&inFile='+escape('<ww:property value="inFile"/>'),
              'srcFile' : '<ww:property value="inFile"/>'
              },
            new Array('xMSIE'));
        document.write(object);
    </script> --%>

    <p id="signatureError" style="color:red">&nbsp;</p>
	<input type="button" value="<ds:lang text="Zamknij"/>" onclick="window.close();" class="btn"/>
	
	<script>
        function signFile()
           {
              var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
                request.getServerName() + ":" + request.getServerPort() %>';
              var uploadUrl = host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>';
              var downloadUrl = host+'<ww:url value="'/office/common/document-signature.action'"/>'+';jsessionid=<%= request.getRequestedSessionId() %>'+'?doDownload=true&inFile='+escape('<ww:property value="inFile"/>');
               //alert(downloadUrl);
               //alert(uploadUrl);
              var locaFileName;
              localFileName = SDoc.DownloadFile(downloadUrl);//"http://www.k-software.com/test/test.pdf");
              //alert(localFileName);

              var res = MyControl.protectFile(localFileName,localFileName+'.pem',2,'');
               //alert(res);
              if (res == 0) {
                res = SDoc.UploadFile(localFileName+'.pem',uploadUrl,'<ww:property value="outFile"/>');
               //alert(res);

                document.getElementById('doSave').value='true';
				document.forms[0].submit();

                SDoc.DeleteLocalFile(localFileName+'.pem');
              }
              SDoc.DeleteLocalFile(localFileName);

           }
        /*function sign()	{
            applet = document.SigApplet;
            var attFile = applet.download();
            //var res = MyControl.signFileDetach(attFile,attFile+'.sig',0);
            var res = MyControl.protectFile(attFile,attFile+'.pem',2,'');
            if (res == 0) {
                applet.upload(attFile+'.pem','<ww:property value="outFile"/>');
                document.getElementById('doSave').value='true';
				document.forms[0].submit();
			}
			else
				document.getElementById('signatureError').firstChild.nodeValue = 'Podpis za��cznika zako�czony niepowodzeniem (b��d '+res+')';
			//alert(res);
            applet.deleteFiles();
        }             */
	</script>
	
	<form id="signForm" action="<ww:url value="'/office/common/attachment-signature.action'"/>" method="post" onsubmit="document.getElementById('doSave').value='true';">
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'inFile'" value="inFile"/>
		<ww:hidden name="'outFile'" value="outFile"/>
		<input type="hidden" name="doSave" id="doSave"/>
	</form>
	
	<script>
		<%--sign('<ww:property value="inFile"/>','<ww:property value="outFile"/>');--%>
        signFile();
    </script>

</ww:if>
<ww:else>
	<p><ds:lang text="PodpisZalacznikaZakonczylSieSukcesem"/></p>
	<input type="button" value="<ds:lang text="Zamknij"/>" onclick="window.close();" class="btn"/>
</ww:else>