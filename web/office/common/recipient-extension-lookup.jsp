<%-- 
    Document   : recipient-extension-lookup
    Created on : 2013-08-08
    Author     : Maciej Starosz <maciej.starosz@docusafe.pl>
--%>

<%@ page contentType="application/json; charset=utf-8" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
[<ww:iterator value="usersList" status="status"><ww:if test="!#status.first">,</ww:if>{
	"recipientLastname":"<ww:property value="getUserLastName()"/>",
	"recipientFirstname":"<ww:property value="getUserFirstName()"/>",
	"recipientName":"<ww:property value="getUserName()"/>",
	"recipientDivision":"<ww:property value="getUserDivisionGuid()"/>",
	"recipientDivisionName":"<ww:property value="getUserDivisionName()"/>"
}</ww:iterator>]
 


