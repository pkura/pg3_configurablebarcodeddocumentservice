<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N cancel.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><ds:lang text="AbyAnulowacPismoNalezyPodacPowodAnulowania"/>.</p>

<form action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);">
    <ww:hidden name="'documentId'" />
    <ww:hidden name="'activity'" />
    <input type="hidden" name="doConfirmCancel" id="doConfirmCancel"/>

    <table>
        <tr>
            <td><ds:lang text="PrzyczynaAnulowaniaPisma"/>:</td>
            <td><ww:textfield  name="'reason'" size="50" maxlength="80" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="doConfirmCancel" value="<ds:lang text="AnulujPismo"/>" class="btn"
                onclick="document.getElementById('doConfirmCancel').value='true'"/>
                <input type="submit" value="<ds:lang text="Powrot"/>" class="btn"/>
            </td>
        </tr>
    </table>


</form>
