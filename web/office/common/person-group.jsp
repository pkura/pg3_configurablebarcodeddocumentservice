<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N person-group.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<p><h4><ds:lang text="WyszukiwanieGrup"/></h4></p>

<form action="<ww:url value="'/office/common/person-group.action'"/>" method="post">

    <ww:if test="results == null || results.empty">
        <table>
            <tr>
                <td><ds:lang text="NazwaGrupy"/></td>
            </tr>
            <tr>
                <td><ww:textfield name="'group'" id="group" size="50" maxlength="50" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>
        </table>
    </ww:if>

    <!-- wyniki wyszukiwania -->

    <ww:else>
        <table width="100%">
            <tr>
                <th><ds:lang text="NazwaGrupy"/></th>
                <th></th>
            </tr>
            <ww:iterator value="results">
                <tr>
                    <td><ww:property/></td>
                    <td><a href="javascript:submitGroup('<ww:property/>')"><ds:lang text="wybierz"/></a></td>
                </tr>
            </ww:iterator>
        </table>

        <table>
            <tr>
                <td>
                    <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
                        onclick="document.location.href='<ww:url value="'/office/common/person-group.action'"></ww:url>';"/>
                </td>
            </tr>
        </table>

        <script language="JavaScript">
            function submitGroup(group)
            {
                if (!window.opener)
                {
                    alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                    return;
                }

                if (!window.opener.__accept_group)
                {
                    alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> __accept_group');
                    return;
                }

                window.opener.__accept_group(group);

                window.close();
            }
        </script>
    </ww:else>
</form>