<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N signature-verification.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>
    <table>
        <tr>
            <td>
               <ww:property value="verificationInfo"/>
            </td>
        </tr>
        <tr>
            <td>
                <ds:lang text="przez"/>: <b><ww:property value="signatureAuthor"/></b>
            </td>
        </tr>
        <tr>
            <td>
                <ds:lang text="dnia"/>: <b><ww:property value="signatureCtime"/></b>
            </td>
        </tr>
    </table>

</p>

<ww:if test="verificationResult == VERIFY_OK">
	<p><ds:lang text="PodpisZweryfikowanoPoprawnie"/></p>
</ww:if>
<ww:elseif test="verificationResult == VERIFY_PART_OK">
	<p><ds:lang text="PodpisZweryfikowanoNiekompletnie"/></p>
</ww:elseif>
<ww:elseif test="verificationResult == VERIFY_NEG">
	<p style="color:red"><ds:lang text="PodpisZweryfikowanoNegatywniePodpisJestBledny"/></p>
</ww:elseif>
<ww:elseif test="verificationResult == VERIFY_NOT_VER">
	<p style="color:red"><ds:lang text="PodpisZweryfikowanoNegatywnieBrakMozliwosciWeryfikacjiPodpisu"/></p>
</ww:elseif>
<ww:else>
    <p style="color:red"><ds:lang text="WystapilBladPodczasWykonywaniaWeryfikacji"/></p>
</ww:else>

<ww:if test="(verificationResult != VERIFY_ERROR) && (verificationDescription != null) && (!(verificationDescription.equals('brak')))">
    <p><ds:lang text="Wyjasnienie"/>: <ww:property value="verificationDescription"/></p>
</ww:if>

<ww:if test="(verificationWarnings != null) && (verificationWarnings.size() > 0)">
    <p>
    <table>
        <th>
            <ds:lang text="Ostrzezenia"/>
        </th>
        <ww:iterator value="verificationWarnings" >
            <tr>
                <td>
                    - <ww:property/>
                </td>
            </tr>
        </ww:iterator>
    </table>
    </p>
</ww:if>

<script>
	window.opener.location.reload(true);
</script>
<input type="button" value="<ds:lang text="Zamknij"/>" onclick="window.close();" class="btn"/>