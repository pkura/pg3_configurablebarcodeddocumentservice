<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N workflow-assignment.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.workflow.PlannedAssignment"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<script>

var push = false;
function checkSubstitutions()
    {
    var count=0;
    var show=0;
    
    assignments=document.forms[0].plannedAssignmentIds;
    var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n";
    var str1;
    <ww:if test="user.getSubstituteUser()!=null">
    	show=1;
    	str1="\n<ds:lang text="UzytkownikJestZastepowanyPrzez"/> <ww:property value="user.getSubstituteUser().asFirstnameLastname()"/>";
    	str=str+str1;
    </ww:if>
    if(show == 0){
    	if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
    	return false;
    }
    else{
    	if(!confirm(str))
    	return false;
  	}
    
    
    return true;
    
    }
function onSubmit()
{
    var kind = getRadioValue('kind');
   
    if (kind == null)
    {
        alert('<ds:lang text="NieWybranoRodzajuDekretacji"/>');
        return false;
    }


    if (isEmpty(document.getElementById('objectiveSel').value) &&
        isEmpty(document.getElementById('objectiveTxt').value))
    {
        alert('<ds:lang text="NieWybranoCeluDekretacji"/>');
        return false;
    }

    <ww:if test="user == null">
    	var scope = getRadioValue('scope');
	    if (scope == null)
	    {
	        alert('<ds:lang text="NieWybranoZakresuDekretacjiNaDzial"/>');
	        return false;
	    }
	    if(scope == '!' && kind == 'obieg_reczny')
	    {
	    	alert('<ds:lang text="NieMoznaDekretowacZadaniaDoRealizacjiDoWieluDzialow"/>');
	        return false;
	    }
    </ww:if>
    <ww:else>
    var scope = '';
    </ww:else>

    var objective = isEmpty(document.getElementById('objectiveTxt').value) ?
        document.getElementById('objectiveSel').value : document.getElementById('objectiveTxt').value;

    window.opener.__accept_assignment('<ww:property value="divisionGuid"/>', '<ww:property value="username"/>', kind, objective, scope, push);

    window.close();
}

</script>

<p>
<ds:lang text="DekretacjaNa"/>: <br/>

<ww:if test="user != null"><ww:property value="user.asFirstnameLastnameName()"/>, <ds:lang text="pracownikW"/> </ww:if>
    <ww:property value="division.prettyPath"/>

</p>

<form action="<ww:url value="'/office/common/workflow-assignment.action'"/>" method="post" onsubmit="return onSubmit();">
<ww:if test="!blocked">
<ww:hidden name="'divisionGuid'"/>
<ww:hidden name="'username'"/>

<p>

<ds:lang text="RodzajDekretacji"/>: <br/>
<ww:if test="hasManualPush">
    <input checked="true" type="radio" name="kind" value='<%= PlannedAssignment.KIND_EXECUTE %>' /> <ds:lang text="doRealizacji"/> <br/>
</ww:if>
<ww:if test="hasCC">
	<input type="radio" name="kind" value='<%= PlannedAssignment.KIND_CC %>' /> <ds:lang text="doWiadomosci"/> <br/>
</ww:if>
<ww:if test="hasCloneAndConsult">
	<input type="radio" name="kind" value='<%= PlannedAssignment.KIND_SECOND_OPINION %>' /> <ds:lang text="konsultacja"/>
</ww:if>

</p>

<p>
        <ds:lang text="CelDekretacji"/>: <br/>
        <ww:select name="'objectiveSel'" list="objectives" cssClass="'sel'" id="objectiveSel"
            listKey="name" listValue="name"
            headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
        <ds:lang text="lubWpisz"/>:
        <input type="text" name="objectiveTxt" id="objectiveTxt" class="txt"
            size="30" maxlength="50" onkeypress="if (!isEmpty(this.value)) { document.getElementById('objectiveSel').options.selectedIndex=0 };"/>
</p>

<ww:if test="user == null">
<p>

<ds:lang text="ZakresDekretacjiNaDzial"/>: <br/>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_ONEUSER %>" checked="true" /> <ds:lang text="dowolny(jeden)uzytkownikWdziale"/> <br/>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_DIVISION %>"/> <ds:lang text="wszyscyWdziale"/>  <br/>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_SUBDIVISIONS %>"/> <ds:lang text="WszyscyWdzialeIWdzialachPodrzednych"/> <br/>

<%--
<ww:if test="hasCloneAndConsult">
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_ONEUSER %>" checked="true" /> <ds:lang text="dowolny(jeden)uzytkownikWdziale"/> <br/>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_DIVISION %>"/> <ds:lang text="wszyscyWdziale"/>  <br/>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_SUBDIVISIONS %>"/> <ds:lang text="WszyscyWdzialeIWdzialachPodrzednych"/> <br/>
</ww:if>
<ww:else>
<input type="radio" name="scope" value="<%= PlannedAssignment.SCOPE_ONEUSER %>" checked="true" readonly="readonly" /> <ds:lang text="dowolny(jeden)uzytkownikWdziale"/> <br/>
</ww:else>
--%>

</p>
</ww:if>

<input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"/>
<ww:if test="hasManualPush">
    <input type="submit" name="doManualPush" value="<ds:lang text="Dekretuj"/>" class="btn" onclick="if (!checkSubstitutions()) return false; push = true;"/>
</ww:if>
</ww:if>
</form>

