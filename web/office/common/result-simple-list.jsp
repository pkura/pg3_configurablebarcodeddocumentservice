<%-- 
    Document   : insurance-participant-list
    Created on : 2009-10-09, 09:22:39
    Author     : Micha� Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table class="tab tab_onhover">
	<ww:if test="results.hasHeader">
		<thead>
			<tr>
				<ww:iterator value="results.header">
					<th>
						<ww:property/>
					</th>
				</ww:iterator>
			</tr>
		</thead>
	</ww:if>
	<tbody>
		<ww:iterator value="results.iterator()"><%-- Z niewiadomych przyczyn WebWork nie rozpoznaje iterable innych ni� kolekcje --%>
			<tr onclick="javascript:location.href = '<ww:url value="hyperlink"/>?<ww:property value="queryString"/>'">
				<ww:iterator value="iterator()">
					<td>
						<ww:property/>
					</td>
				</ww:iterator>
			</tr>
		</ww:iterator>
	</tbody>
	<script type="text/javascript">
		$j('.tab tbody tr').hover(function() {
			$j(this).addClass('tabHover');
		}, function() {
			$j(this).removeClass('tabHover');
		});
	</script>
</table>
