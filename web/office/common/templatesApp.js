var app = angular.module('TemplatesApp', ['angularFileUpload']);

var windowQuery = URI.parseQuery(URI.parse(window.location.href)["query"]);
var bareUrl = URI(window.location.href).query("");

var documentId = windowQuery["documentId"];
var snapshotId = windowQuery["snapshotId"];

var config = {
    "customFieldPrefix": "POLE"
};

var customFieldRegexp = new RegExp("^" + config["customFieldPrefix"] + "([0-9]+)");

app.service("AjaxAction", ajaxActionFactory);

// app.directive("removeSnapshot", function() {
// 	return {
// 		"restrict": "E",
// 		"template": "<button>Usu�</button>",
// 		"link": function(scope, element, attrs) {
// 			console.log(attrs.snapshot);
// 		}
// 	}
// });

function SnapshotsCtrl($scope, $timeout, $element, AjaxAction) {
    if (documentId && !snapshotId) {
        $element.removeClass("hide");

        $scope.snapshots = [];
        $scope.documentId = documentId;

        $scope.removeSnapshot = function(id) {
            if (!confirm("Potwierd� usuni�cie snapshotu")) {
                return false;
            }

            AjaxAction.post("deleteSnapshot", {
                "id": id
            }).done(function() {
                $scope.snapshots = _.reject($scope.snapshots, function(s) {
                    return s.id === id
                });
            });
        };

        $scope.addSnapshot = function() {
            AjaxAction.post("createSnapshot", {
                "documentId": documentId
            }).done(function(snapshot) {
                addHref(snapshot);
                $scope.snapshots.push(snapshot);
            });
        };

        function addHref(snapshot) {
            snapshot.href = bareUrl.clone().query({
                "snapshotId": snapshot["id"],
                "documentId": snapshot["documentId"]
            }).toString();
        }

        AjaxAction.get("getSnapshots", {
            "documentId": documentId
        }).done(function(snapshots) {
            _.each(snapshots, addHref);
            $scope.snapshots = snapshots;
        });
    }

}

// Zwraca ostatni indeks z pola na formatce.
// np. jesli ostatnie pole na dole to pole123
// (regexp konfigurowalny w obiekcie `config`)
// wtedy funkcja zwroci 123. Je�li brak jest pola z pocz�tkiem `pole`
// wtedy zwracamy 0.
function getLastCustomFieldIndex(fields) {
    var field = _.last(fields);

    var m;
    if (field && (m = field.name.match(customFieldRegexp))) {
        return parseInt(m[1], 10);
    } else {
        return 0;
    }
}

app.filter("stringFields", function() {
    return function(fields) {
        return _.filter(fields, function(elem) {
            return _.isString(elem.value) || _.isNull(elem.value);
        });
    };
});

app.filter("dicFields", function() {
    return function(fields) {
        return _.filter(fields, function(elem) {
            var b = _.isArray(elem.value);
            if(b && elem.value.length > 0) {
            	return !_.isArray(elem.value[0]);
            } else {
            	return false;
            }
        });
    };
});

app.filter("multiDicFields", function() {
    return function(fields) {
        return _.filter(fields, function(elem) {
        	var b = _.isArray(elem.value);
        	if(b && elem.value.length > 0) {
                // console.log("multiDicField", elem.value);
                return _.isArray(elem.value[0]);
        	} else {
        		return false;
        	}
        });
    };
});

function SnapshotCtrl($scope, $element, $upload, AjaxAction) {
    if (snapshotId) {

        $scope.snapshotId = snapshotId;

        $scope.removeField = function(fieldCn) {
            $scope.snapshot.fields = _.reject($scope.snapshot.fields, function(field) {
                return field.cn === fieldCn;
            });
        }

        $scope.customIndex = 0;

        $scope.filterList = function(elem) {
            return elem.cn.indexOf('.') === -1
        }

        $scope.filterListDic = function(elem) {
            return elem.cn.indexOf('.') !== -1
        }

        $scope.addField = function() {
            // var index = $scope.customIndex

            var fieldCn = config["customFieldPrefix"] + $scope.customIndex;
            var fieldName = fieldCn.toLowerCase();

            $scope.snapshot.fields.push({
                "cn": fieldCn,
                "name": fieldName
            });

            $scope.customIndex++;
        };

        $scope.save = function() {
            AjaxAction.post("updateSnapshot", {
                "id": $scope.snapshot.id
            }, {
                "values": JSON.stringify($scope.snapshot.fields),
                "title": $scope.snapshot.title
            });
        };

        // obs�uga przycisku cofni�cia do listy snapshot�w
        $scope.goBack = function() {
            window.location.href = bareUrl.clone().query({
                "documentId": $scope.snapshot.documentId
            });
        };

        $scope.fork = function() {
            AjaxAction.post("createSnapshot", {
                "documentId": $scope.snapshot.documentId
            }, {
                "values": JSON.stringify($scope.snapshot.fields)
            }).done(function(newSnapshot) {
                window.location.href = bareUrl.clone().query({
                    "documentId": $scope.snapshot.documentId,
                    "snapshotId": newSnapshot.id
                });
            });
        };

        $scope.getFileDocumentUrl = function(fileDocument) {
            return bareUrl.clone().query({
                "getFileDocument": "true",
                "documentId": $scope.snapshot.documentId,
                "id": $scope.snapshot.id,
                "documentName": fileDocument.name
            }).toString();
        }

        $scope.createFileDocument = function() {
            var select = $j("#template-select");
            var value = select.val();

            var template = _.find($scope.snapshot.templates, function(t) {
                return t.name === value;
            });

            if (!template) {
                throw "Cannot find template";
            }

            AjaxAction.post("createFileDocument", {
                "id": $scope.snapshot.id,
                "documentId": $scope.snapshot.documentId,
                "templateName": template.name,
                "dockind": $scope.snapshot.dockindCn,
                "isUserTemplate": template.isUserTemplate
            }).done(function(doc) {
                $scope.snapshot.fileDocuments.push(doc);
            });

            // if(value) {
            // 	window.location.href = bareUrl.clone().query(query);
            // }

        };

        $scope.deleteFileDocument = function(fileDocument) {
            if (!confirm("Potwierd� usuni�cie pliku: " + fileDocument.name)) {
                return false;
            }

            AjaxAction.post("deleteFileDocument", {
                "id": $scope.snapshot.id,
                "documentId": $scope.snapshot.documentId,
                "documentName": fileDocument.name
            }).done(function() {
                $scope.snapshot.fileDocuments = _.reject($scope.snapshot.fileDocuments, function(it) {
                    return it.name === fileDocument.name;
                });
            });
        }

        $scope.createPdf = function(fileDocument) {
            AjaxAction.post("createPdf", {
                "id": $scope.snapshot.id,
                "documentId": $scope.snapshot.documentId,
                "documentName": fileDocument.name
            }).done(function(filename) {
                $scope.snapshot.fileDocuments.push({
                    name: filename
                });
            });
        }

        $scope.uploadFileDocument = function($files, fileDocument) {
            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];
                $scope.upload = $upload.upload({
                    url: bareUrl.clone().query({
                        "uploadFileDocument": true,
                        "documentId": $scope.snapshot.documentId,
                        "id": $scope.snapshot.id,
                        "documentName": fileDocument.name
                    }), //upload.php script, node.js route, or servlet url
                    // method: POST or PUT,
                    // headers: {'headerKey': 'headerValue'}, withCredential: true,
                    // data: {myObj: $scope.myModelObj},
                    file: file,
                    // file: $files, //upload multiple files, this feature only works in HTML5 FromData browsers
                    /* set file formData name for 'Content-Desposition' header. Default: 'file' */
                    //fileFormDataName: myFile,
                    /* customize how data is added to formData. See #40#issuecomment-28612000 for example */
                    //formDataAppender: function(formData, key, val){} 
                }).progress(function(evt) {
                    // console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
                    fileDocument.uploadStatus = parseInt(100.0 * evt.loaded / evt.total) + " %";
                }).success(function(data, status, headers, config) {
                    // file is uploaded successfully

                    fileDocument.uploadStatus = "OK";

                    // console.log(data);
                });
                //.error(...)
                //.then(success, error, progress); 

                break;
            }
        }

        // pobierz snapshot
        AjaxAction.get("getSnapshot", {
            "id": snapshotId
        }).done(function(snapshot) {
            $scope.snapshot = snapshot;

            $scope.customIndex = getLastCustomFieldIndex(snapshot.fields) + 1;

            $element.removeClass("hide");
        });

    }
}