<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N school.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:if test="closeOnReload">
    <script>window.close();</script>
</ww:if>
<ww:else>

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/school.action'"/>" method="post">

        <input type="hidden" name="closeOnReload" id="closeOnReload"/>
        <ww:hidden name="'canAddAndSubmit'" value="canAddAndSubmit"/>

    <ww:if test="results == null || results.empty">

        <table>
        <tr>
            <td colspan="2"><ds:lang text="Nazwa"/></td>
        </tr>
        <tr>
            <td colspan="2"><ww:textfield name="'name'" id="name" size="50" maxlength="50" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td colspan="2"><ds:lang text="AdresUlica"/></td>
        </tr>
        <tr>
            <td colspan="2"><ww:textfield name="'street'" id="street" size="50" maxlength="50" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Kod"/></td><td><ds:lang text="Miejscowosc"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'zip'" id="zip" size="10" maxlength="14" cssClass="'txt'"/></td>
            <td><ww:textfield name="'location'" id="location" size="35" maxlength="50" cssClass="'txt'" onchange="'checkCode();'"/></td>
        </tr>
        <tr>
            <td colspan="2"><span class="warning" id="zipError"> </span></td>
        </tr>
        </table>

        <table>
            <tr>
                <td>
                    <input type="button" value="          <ds:lang text="UmiescWformularzu"/>          " onclick="if (!submitSchool()) return false; window.close();" class="btn"/>
                    <ww:if test="canAdd">
                        <input type="submit" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" class="btn"/>
                    </ww:if>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>
            <ww:if test="canAddAndSubmit">
                <tr>
                    <td>
                        <input type="submit" name="doAdd" value="<ds:lang text="UmiescWformularzuIdodajDoSlownika"/>" class="btn"
                        onclick="if (!submitSchool()) return false; document.getElementById('closeOnReload').value = 'true';"/>
                    </td>
                </tr>
            </ww:if>
            
        </table>



        <script language="JavaScript">
        function submitSchool()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            if (!window.opener.__accept_school)
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcji"/> __accept_school');
                return;
            }

            var school = new Array();

            school.name = id('name').value;
            school.street = id('street').value;
            school.zip = id('zip').value;
            school.location = id('location').value;

            if (isEmpty(school.name)) {
                alert('<ds:lang text="NieWpisanoNazwySzkoly"/>');
                return false;
            }

            window.opener.__accept_school(school);

            return true;
        }

        function id(id)
        {
            return document.getElementById(id);
        }

        // walidacja

        var szv = new Validator(document.getElementById('zip'), '##-###');
		var locationElement = document.getElementById('location');
		var zipElement = document.getElementById('zip');
		
		function checkCode() {
	        	showCode(zipElement, locationElement);
	    }
	    
        szv.onOK = function() {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';
            if (zipElement.value != "") 
            {
        		showLocation(zipElement, locationElement); 
        	}
        }

        szv.onEmpty = szv.onOK;

        szv.onError = function(code) {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001)';
        }

        szv.canValidate = function() {
            return true;
        }
        </script>

    </ww:if>

    <!-- wyniki wyszukiwania -->

    <ww:else>
        <table width="100%">
            <tr>
                <th><ds:lang text="Nazwa"/></th>
                <th><ds:lang text="Adres"/></th>
                <th><ds:lang text="Kod"/></th>
                <th><ds:lang text="Miejscowosc"/></th>
                <th></th>
                <ww:if test="canAdd"><th></th></ww:if>
            </tr>
            <ww:iterator value="results">
                <tr>
                    <td><ww:property value="name"/></td>
                    <td><ww:property value="street"/></td>
                    <td><ww:property value="zip"/></td>
                    <td><ww:property value="location"/></td>
                    <td><a href="<ww:url value="'/office/common/school.action'"><ww:param name="'schoolId'" value="id"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>"><ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                        <td><a href="<ww:url value="'/office/common/school.action'"><ww:param name="'schoolId'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>" onclick="if (!confirm('<ds:lang text="NaPewnoUsunacWpis"/> <ww:property value="name"/>')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
                </tr>
            </ww:iterator>
        </table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/school.action'"></ww:url>';"/>
    </ww:else>

    </form>

    <script>
  /*  var s = document.location.search;
    if (s != null && s.length > 0) s = s.substring(1);

    var ognlMap = null;
    var params = s.split('&');
    for (var i=0; i < params.length; i++)
    {
        var pair = params[i].split('=');
        var name = pair[0];
        var value = pair[1];
        if (name == 'ognlMap')
        {
            ognlMap = unescape(value);
            break;
        }
    }

    if (ognlMap != null)
    {
        eval('var map='+ognlMap);

        //alert('parse '+ognlMap);
        //var map = parseOgnlMap(ognlMap);
        //for (var a in map) { alert('arr['+a+']='+map[a]); }

        document.getElementById('title').value = map.title != null ? map.title : '';
        document.getElementById('firstname').value = map.firstname != null ? map.firstname : '';
        document.getElementById('lastname').value = map.lastname != null ? map.lastname : '';
        document.getElementById('organization').value = map.organization != null ? map.organization : '';
        document.getElementById('street').value = map.street != null ? map.street : '';
        document.getElementById('zip').value = map.zip != null ? map.zip : '';
        document.getElementById('location').value = map.location != null ? map.location : '';
        document.getElementById('country').value = map.country != null ? map.country : 'PL';
        document.getElementById('email').value = map.email != null ? map.email : '';
        document.getElementById('fax').value = map.fax != null ? map.fax : '';
        document.getElementById('nip').value = map.nip != null ? map.nip : '';
        document.getElementById('pesel').value = map.pesel != null ? map.pesel : '';
        document.getElementById('regon').value = map.regon != null ? map.regon : '';
    }
*/
    window.focus();

    </script>
</ww:else>