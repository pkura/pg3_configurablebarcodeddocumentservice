<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N assignment-history.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>	
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
<ww:if test="currentAssignmentDivision != null">
    <p>
    <ds:lang text="DokumentZnajdujeSie"/>
    <ww:if test="currentAssignmentUser != null">
        <ds:lang text="uUzytkownika"/> <ww:property value="currentAssignmentUser"/>
    </ww:if>
    <ds:lang text="wDziale"/> <ww:property value="currentAssignmentDivision"/>
	<ww:if test="currentCoor"> (<b>koordynator</b>) </ww:if>
    <ww:if test="currentAssignmentAccepted"><b>(<ds:lang text="przyjety"/>)</b></ww:if>
    <ww:else>(<ds:lang text="oczekujacy"/>)</ww:else>
    </p>
</ww:if>

<ww:if test="participantsDescription != null"><p><ww:property value="participantsDescription"/></p></ww:if>

<p>
<ww:if test="showAll">
    <a href="<ww:url value="getBaseLink()"><ww:param name="'documentId'" value="documentId"/>
    <ww:param name="'activity'" value="activity"/><ww:param name="'showAll'" value="false"/></ww:url>"><ds:lang text="PokazSkroconaHistoriePisma"/></a>
</ww:if>
<ww:else>
    <a href="<ww:url value="getBaseLink()"><ww:param name="'documentId'" value="documentId"/>
    <ww:param name="'activity'" value="activity"/><ww:param name="'showAll'" value="true"/></ww:url>"><ds:lang text="PokazPelnaHistoriePisma"/></a>
</ww:else>
</p>

<table width="100%" cellspacing="0">
<ww:iterator value="assignmentHistory">
    <ww:set name="date" scope="page" value="date"/>
    <tr>
   		<td>
	    	<table width="100%" cellspacing="0">
	    		<tr>
	    			<td align="left" class="historyAuthor" ><ww:property value="user"/><ww:property value="divisions"/><ww:if test="substitute != null"><ww:property value="substitute"/></ww:if></td>
	    			<td align="right" class="historyDate" ><fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/></td>
	    		</tr>
	    	</table>
    	</td>
        
    </tr>
    <tr>
        <td class="historyContent">
            <ww:property value="content"/><ww:property value="status"/>
        </td>
    </tr>
</ww:iterator>
</table>

 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
