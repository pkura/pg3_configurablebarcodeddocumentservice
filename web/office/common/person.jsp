<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N person.jsp N-->

<%@ page language="Java" contentType="text/html; charset=iso-8859-2" pageEncoding="iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
	request.setCharacterEncoding("iso-8859-2");
%>
<%--
	request.setCharacterEncoding("iso-8859-2");
	response.setCharacterEncoding("ISO-8859-2");
--%>

<style type="text/css">
	select#country {width:24em;}
</style>


<ww:if test="closeOnReload">
    <script>window.close();</script>
</ww:if>
<ww:else>

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

    <form action="<ww:url value="'/office/common/person.action'"/>" method="post">

        <input type="hidden" name="closeOnReload" id="closeOnReload"/>
        <ww:hidden name="'lparam'" value="lparam"/>
        <ww:hidden name="'wparam'" value="wparam"/>
        <ww:hidden name="'dictionaryType'" value="dictionaryType"/>
        <ww:hidden name="'canAddAndSubmit'" value="canAddAndSubmit"/>
        <ww:hidden name="'personId'" value="personId"/>

    <ww:if test="results == null || results.empty">

        <table class="popupForm">
        <ds:available test="person.selectGuid">
	        <tr>
	            <td colspan="3">
	                <ds:lang text="WyborSlownika"/>:
	                <ww:select name="'guid'" list="guids" value="guid" cssClass="'sel'"
	                    listKey="key" listValue="value"/>
	            </td>
	        </tr>
        </ds:available>
		<ds:available test="person.title">
        <tr class="desc">
            <td colspan="3"><ds:lang text="Tytul"/>:</td>
        </tr>
        <tr>
            <td colspan="3"><ww:textfield name="'title'" id="title" size="30" maxlength="30" cssClass="'txt'"/></td>
        </tr>
		</ds:available>
        <tr class="desc">
            <td><ds:lang text="Imie"/></td><td><ds:lang text="Nazwisko"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'firstname'" id="firstname" size="20" maxlength="50" cssClass="'txt'"/></td>
            <td><ww:textfield name="'lastname'" id="lastname" size="25" maxlength="50" cssClass="'txt'"/></td>
        </tr>
        <tr class="desc">
            <td colspan="3"><ds:lang text="FirmaUrzad"/></td>
        </tr>
        <tr>
            <td colspan="3"><ww:textfield name="'organization'" id="organization" size="50" cssClass="'txt'"/></td>
        </tr>
        
		<ds:available test="person.dzial">
        <tr class="desc">
            <td colspan="3"><ds:lang text="Dzial"/></td>
        </tr>
        <tr>
            <td colspan="3"><ww:textfield name="'organizationDivision'" id="organizationDivision" size="50" maxlength="50" cssClass="'txt'"/></td>
        </tr>
		</ds:available>
        <tr class="desc">
            <td colspan="3"><ds:lang text="AdresUlica"/></td>
        </tr>
        <tr>
            <td colspan="3"><ww:textfield name="'street'" id="street" size="30" maxlength="50" cssClass="'txt'"/></td>
        </tr>
        </table>

        <table class="popupForm">
        <tr class="desc">
            <td><ds:lang text="Kod"/></td><td><ds:lang text="Miejscowosc"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'zip'" id="zip" size="10" maxlength="14" cssClass="'txt'"/></td>
            <td><ww:textfield name="'location'" id="location" size="35" maxlength="50" cssClass="'txt'"  onchange="'showCode();'"/></td>
        </tr>
        <tr>
            <td colspan="2"><span class="warning" id="zipError"> </span></td>
        </tr>
        </table>
		<ds:available test="person.kraj">
        <table class="popupForm">
        
        <tr class="desc">
            <td><ds:lang text="Kraj"/></td>
        </tr>
        
        <tr>
            <td><ww:select name="'country'" id="country"
                list="@pl.compan.docusafe.util.Countries@COUNTRIES"
                listKey="alpha2" listValue="name" value="country"
                cssClass="'sel'" onchange="this.form.senderZip.validator.revalidate();"/>
            </td>
        </tr>
        </table>
        </ds:available>

		<ds:available test="person.emailFax">
        <table class="popupForm">
        <tr class="desc">
            <td><ds:lang text="Email"/></td><td><ds:lang text="Faks"/></td>
        </tr>
        <tr>
            <td><ww:textfield name="'email'" id="email" size="28" maxlength="50" cssClass="'txt'"/></td>
            <td><ww:textfield name="'fax'" id="fax" size="17" maxlength="30" cssClass="'txt'"/></td>
        </tr>
        </table>
		</ds:available>

		<ds:available test="person.nipPeselRegon">
        <table class="popupForm">
            <tr class="desc">
                <td><ds:lang text="NIP"/></td>
                <td><ds:lang text="PESEL"/></td>
                <td><ds:lang text="REGON"/></td>
            </tr>
            <tr>
                <td><ww:textfield name="'nip'" value="nip" id="nip" size="15" maxlength="15" cssClass="'txt'"/></td>
                <td><ww:textfield name="'pesel'" id="pesel" size="11" maxlength="11" cssClass="'txt'"/></td>
                <td><ww:textfield name="'regon'" id="regon" size="15" maxlength="15" cssClass="'txt'"/></td>
            </tr>
            <ds:available test="person.grupa">
            <tr class="desc">
                <td colspan="3"><ds:lang text="Grupa"/></td>
            </tr>
            <tr>
                <td colspan="3"><ww:textfield name="'group'" id="group" size="50" maxlength="50" cssClass="'txt'"/></td>
            </tr>
            </ds:available>
        </table>
		</ds:available>
		<ds:available test="epuap">
		    <table class="popupForm">
		    	<tr><td><ww:if test="zgodaNaWysylke">Zgoda na odpowied� drog� elektroniczn�</ww:if><ww:else>Brak zgody na odpowied� drog� elektroniczn�</ww:else></td></tr>
		        <tr class="desc">
		            <td><ds:lang text="IdentyfikatorEPUAP"/></td><td><ds:lang text="AdresSkrytkiEPUAP"/></td>
		        </tr>
		        <tr>
		            <td><ww:textfield name="'identifikatorEpuap'" id="identifikatorEpuap" value="identifikatorEpuap" size="20" maxlength="70" cssClass="'txt'"/></td>
		            <td><ww:textfield name="'adresSkrytkiEpuap'" id="adresSkrytkiEpuap" value="adresSkrytkiEpuap" size="30" maxlength="70" cssClass="'txt'"/></td>
		        </tr>
       		</table>
       	</ds:available>

        <table class="buttons" border="0">
            <tr>
                <td width="35%">
                    <input type="button" name = "submitPersonButton" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitPerson()) return false; window.close();" class="btn"/>
                </td>
                <td width="15%">
                    <input type="submit" id="doSearch" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"/>
                </td>
                <td width="35%">
                    <ww:if test="canAdd">
                        <input type="submit" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" class="btn"/>
                    </ww:if>
				</td>
				<td width="15%">
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>
            
            <ww:if test="canAddAndSubmit">
                <tr>
                    <td width="50%" colspan="2">

                        <input type="submit" name="doAdd" value="<ds:lang text="UmiescWformularzuIdodajDoSlownika"/>" class="btn"
                        onclick="if (!submitPerson()) return false; document.getElementById('closeOnReload').value = 'true';"/>
					</td>
					<td width="35%">	
						<input type="submit" name="doClean" value="<ds:lang text="WyczyscPolaFormularza"/>" class="btn"/>
                    </td>
                    <td width="15%">
                    	<input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn" <ww:if test="personId == null">disabled="disabled"</ww:if> />
                    </td>
                </tr>
            </ww:if>

        </table>


        <script language="JavaScript">
        function submitPerson()
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }
            if (!window.opener.__accept_person)
            {
                alert('<ds:lang text="WOknieWywolujacymNieZnalezionoFunkcjiAcceptPerson"/>');
                return;
            }

            // te parametry b�d� przekazane z powrotem do okna wywo�uj�cego
            var lparam = '<ww:property value="lparam"/>';
            var wparam = '<ww:property value="wparam"/>';

            var person = new Array();

            <ds:available test="person.title">
            	person.title = id('title').value;
            </ds:available>
            person.firstname = id('firstname').value;
            person.lastname = id('lastname').value;
            person.organization = id('organization').value;
            <ds:available test="person.dzial">
           		person.organizationDivision = id('organizationDivision').value;
            </ds:available>
            person.street = id('street').value;
            person.zip = id('zip').value;
            person.location = id('location').value;
            <ds:available test="epuap">
            	person.identifikatorEpuap = id('identifikatorEpuap').value;
	            person.adresSkrytkiEpuap = id('adresSkrytkiEpuap').value;
            </ds:available>
            <ds:available test="person.kraj">
            	person.country = getSelectedOption(id('country')).value;
			</ds:available>
			
            <ds:available test="person.emailFax">
	            person.email = id('email').value;
	            person.fax = id('fax').value;
            </ds:available>

            <ds:available test="person.nipPeselRegon">
	            person.nip = id('nip').value;
	            person.pesel = id('pesel').value;
	            person.regon = id('regon').value;
	        </ds:available>
	        <ds:available test="person.grupa">
	        	person.group = id('group').value;
	        </ds:available>
	      
            if (isEmpty(person.title) && isEmpty(person.firstname) &&
                isEmpty(person.lastname) && isEmpty(person.organization))
            {
                alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
                return false;
            }

            if (window.opener.__accept_person_duplicate &&
                window.opener.__accept_person_duplicate(person, lparam, wparam))
            {
                alert('<ds:lang text="IstniejeJuzTakiOdbiorca"/>');
                return false;
            }
            else
            {
                window.opener.__accept_person(person, lparam, wparam);
            }

            return true;
        }

        function id(id)
        {
            return document.getElementById(id);
        }

        // walidacja

        var szv = new Validator(document.getElementById('zip'), '##-###');


        szv.onOK = function() {
            this.element.style.color = 'black';
            document.getElementById('zipError').innerHTML = '';
			if (document.getElementById('zip').value != "") {
				showLocation();
			}

        }

		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************ //
		//   |                  |			       |	   //
		//   |			        |			       |       //
		//   |			        |			       |       //
		//  \/			       \/			      \/       //

		var xmlHttp = null;
		var serverName = '<%= request.getServerName() + ":" %>';
		var port = '<%= request.getServerPort() %>';
		var contextPath = '<%= request.getContextPath() %>';
		var protocol = window.location.protocol;

		var polskieZnaki = new Array();
		var zamiana = new Array();

		function showCode()
		{
			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}
			var location=document.getElementById('location').value;

			//Rozwiazanie problemu z polskimi znakami(niezbyt eleg.)

			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			for(i = 0; i < 18; i++)
			{
				while (location.indexOf(polskieZnaki[i])>-1)
				{
					pos= location.indexOf(polskieZnaki[i]);
					location = "" + (location.substring(0, pos) + zamiana[i] +
					location.substring((pos + polskieZnaki[i].length), location.length));
				}
			}


			if(location==null)
			{
				alert ("No element with id 'location' found on the page");
				return;
			}
			//alert("http://" + serverName + port + contextPath + "/code.ajx?location=" + location);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/code.ajx?location=" + location,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('zip');
			};

			xmlHttp.send(null);

		}

		function showLocation()
		{

			xmlHttp=GetXmlHttpObject();
			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			var zip=document.getElementById('zip').value;

			if(zip==null)
			{
				alert ("No element with id 'zip' found on the page");
				return;
			}
			//alert(protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip);
			xmlHttp.open("GET", protocol + "//" + serverName + port + contextPath + "/location.ajx?zip=" + zip,true);
			xmlHttp.onreadystatechange = function() {
				stateChanged('location');
			};

			xmlHttp.send(null);
		}

		function stateChanged(variable)
		{

			if (xmlHttp==null)
			{
				alert ("Browser does not support HTTP Request");
				return;
			}

			polskieZnaki[0]="�";
			polskieZnaki[1]="�";
			polskieZnaki[2]="�";
			polskieZnaki[3]="�";
			polskieZnaki[4]="�";
			polskieZnaki[5]="�";
			polskieZnaki[6]="�";
			polskieZnaki[7]="�";
			polskieZnaki[8]="�";

			polskieZnaki[9]= "�";
			polskieZnaki[10]="�";
			polskieZnaki[11]="�";
			polskieZnaki[12]="�";
			polskieZnaki[13]="�";
			polskieZnaki[14]="�";
			polskieZnaki[15]="�";
			polskieZnaki[16]="�";
			polskieZnaki[17]="�";

			zamiana[0]="^a";
			zamiana[1]="^e";
			zamiana[2]="^c";
			zamiana[3]="^l";
			zamiana[4]="^o";
			zamiana[5]="^n";
			zamiana[6]="^s";
			zamiana[7]="^x";
			zamiana[8]="^z";

			zamiana[9]= "^A";
			zamiana[10]="^E";
			zamiana[11]="^C";
			zamiana[12]="^L";
			zamiana[13]="^O";
			zamiana[14]="^N";
			zamiana[15]="^S";
			zamiana[16]="^X";
			zamiana[17]="^Z";

			if (xmlHttp.readyState==4)
			{

				if(xmlHttp.status==200)
				{

					var valid = xmlHttp.responseXML.getElementsByTagName("valid")[0];
					//alert(" Message: " + valid.childNodes[0].nodeValue);

					if(valid.childNodes[0].nodeValue=='LocNotFound' && variable =='zip')
					{
						alert("Nie znalaz�em kodu dla tej miejscowo�ci.");
						//document.getElementById(variable).value = '';
						//document.getElementById('location').value = '';
					}
					else
					{
						var message = valid.childNodes[0].nodeValue;
						for(i = 0; i < 18; i++)
						{
							while (message.indexOf(zamiana[i])>-1)
							{
								pos= message.indexOf(zamiana[i]);
								message = "" + (message.substring(0, pos) + polskieZnaki[i] +
								message.substring((pos + zamiana[i].length), message.length));
							}
						}
					}

					if(valid.childNodes[0].nodeValue=='CodeNotFound' && variable =='location')
					{
						alert("Nie znalaz�em miejscowo�ci dla danego kodu." );
						document.getElementById(variable).value='';
					}
					else if(valid.childNodes[0].nodeValue!='CodeNotFound' && variable=='location')
					{
						document.getElementById(variable).value=message;
					}
//				    in this functionality we always have message.trim().length > 6 (iff it is zip)
//					if(message.trim().length == 6)
//					{
//						document.getElementById(variable).value=message;
//					}
//					else if (message.trim().length > 6 && variable=='zip')
//					{
//						alert("Nie ma jednego kodu pocztowego dla danej miejscowo�ci. Zakres(y) to: " + message + ".");
//						document.getElementById('zip').value=message.substring(0, 6);
//					}
					


				}
				else
				{
					alert("Error loading page"+ xmlHttp.status +":"+ xmlHttp.statusText);
				}
			}
			else  if (xmlHttp.readyState==0)
			{
				alert("	Object is uninitialized");
			}
			else if( xmlHttp.readyState == 1 )
  			{
  				//alert("	OnLoading");
  			}
 			else if( xmlHttp.readyState == 2 )
  			{
  				//alert("	OnLoaded");
  			}
  			else if( xmlHttp.readyState == 3 )
  			{
  				//alert("	OnInteractive");
  			}


		}

		function GetXmlHttpObject()
		{
			var xmlHttp=null;
			try
			{
			// Firefox, Opera 8.0+, Safari
				xmlHttp=new XMLHttpRequest();
			}
			catch (e)
			{
			//Internet Explorer
				try
				{
					xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch (e)
				{
					xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
			}
			return xmlHttp;
		}
		//*******************************//

		//   /\                          /\			      /\     //
		//   |			        |			       |     //
		//   |			        |			       |     //
		//   |		                 |			       |     //
		//*************************************************//
		// *****************   AJAX ***********************//
		//************************************************//



        szv.onEmpty = szv.onOK;

        szv.onError = function(code) {
            this.element.style.color = 'red';
            if (code == this.ERR_SYNTAX)
                document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowyPoprawnyKodToNp"/>. 00-001';
            else if (code == this.ERR_LENGTH)
                document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakowNp"/>. 00-001';
        }

        szv.canValidate = function() {
            var country = document.getElementById('country');
            var countryCode = country.value;

            return (countryCode == '' || countryCode == 'PL');
        }


        //ustawianie focusa
        //document.getElementById('title').focus();
        </script>

    </ww:if>

    <!-- wyniki wyszukiwania -->

    <ww:else>
    	<table id="mainTable" class="search" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <th><ds:lang text="Imie"/></th>
                <th><ds:lang text="Nazwisko"/></th>
                <th><ds:lang text="Ulica" /></th>
                <th><ds:lang text="Organizacja"/></th>
                <ds:available test="person.dzial">
                <th><ds:lang text="Dzial"/></th>
                </ds:available>
                <th></th>
                <ww:if test="canAdd"><th></th></ww:if>
            </tr>
            <ww:iterator value="results" status="stat">
                <tr <ww:if test="#stat.odd != true">class="oddRows"</ww:if> >
<%--<td><ww:property value="summary"/></td>--%>
                    <td><ww:property value="(firstname != null ? firstname : '')"/></td>
                    <td><ww:property value="(lastname != null ? lastname : '')"/></td>
                    <td><ww:property value="street"/></td>
                    <td><ww:property value="organization"/></td>
                    <ds:available test="person.dzial">
                    <td><ww:property value="organizationDivision"/></td>
                    </ds:available>
                    <td><a href="<ww:url value="'/office/common/person.action'"><ww:param name="'personId'" value="id"/><ww:param name="'lparam'" value="[1].lparam"/><ww:param name="'wparam'" value="[1].wparam"/><ww:param name="'dictionaryType'" value="dictionaryType"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>" name="<ww:property value="id"/>"><ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                        <td><a href="<ww:url value="'/office/common/person.action'"><ww:param name="'personId'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'lparam'" value="[1].lparam"/><ww:param name="'wparam'" value="[1].wparam"/><ww:param name="'dictionaryType'" value="dictionaryType"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>" onclick="if (!confirm('<ds:lang text="NaPewnoUsunacWpis"/> <ww:property value="shortSummary"/>')) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
                
            </ww:iterator>
        </table>


        <ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>

		<center>
        <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="'/office/common/person.action'"><ww:param name="'lparam'" value="lparam"/><ww:param name="'wparam'" value="wparam"/><ww:param name="'dictionaryType'" value="dictionaryType"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/></ww:url>';"/>
        </center>
    </ww:else>

    </form>

    <script>
    //prepareTable(E("titleTable"),1,0);
    var s = document.location.search;
    if (s != null && s.length > 0) s = s.substring(1);

    var ognlMap = null;
    var params = s.split('&');
    for (var i=0; i < params.length; i++)
    {
        var pair = params[i].split('=');
        var name = pair[0];
        var value = pair[1];
        if (name == 'ognlMap')
        {
            ognlMap = unescape(value);
            break;
        }
    }

    if (ognlMap != null)
    {
        eval('var map='+ognlMap);

        //alert('parse '+ognlMap);
        //var map = parseOgnlMap(ognlMap);
        //for (var a in map) { alert('arr['+a+']='+map[a]); }

        <ds:available test="person.title">
        	document.getElementById('title').value = map.title != null ? map.title : '';
        </ds:available>
        document.getElementById('firstname').value = map.firstname != null ? map.firstname : '';
        document.getElementById('lastname').value = map.lastname != null ? map.lastname : '';
        document.getElementById('organization').value = map.organization != null ? map.organization : '';
        document.getElementById('organizationDivision').value=map.organizationDivision!=null ? map.organizationDivision:'';
        document.getElementById('street').value = map.street != null ? map.street : '';
        document.getElementById('zip').value = map.zip != null ? map.zip : '';
        document.getElementById('location').value = map.location != null ? map.location : '';
        document.getElementById('country').value = map.country != null ? map.country : 'PL';
        <ds:available test="person.emailFax">
	        document.getElementById('email').value = map.email != null ? map.email : '';
	        document.getElementById('fax').value = map.fax != null ? map.fax : '';
	    </ds:available>
	    <ds:available test="person.nipPeselRegon">
	        document.getElementById('nip').value = map.nip != null ? map.nip : '';
	        document.getElementById('pesel').value = map.pesel != null ? map.pesel : '';
	        document.getElementById('regon').value = map.regon != null ? map.regon : '';
	        document.getElementById('group').value = map.group != null ? map.group : '';
	    </ds:available>
	    <ds:available test="epuap">
	    	document.getElementById('adresSkrytkiEpuap').value = map.adresSkrytkiEpuap != null ? map.adresSkrytkiEpuap : '';
	        document.getElementById('identifikatorEpuap').value = map.identifikatorEpuap != null ? map.identifikatorEpuap : '';
	     </ds:available>
    }

//    window.focus();

//    id('search').focus();

    </script>
</ww:else>