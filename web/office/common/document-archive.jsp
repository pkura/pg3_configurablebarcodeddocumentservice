<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
         java.util.Date,
         org.apache.commons.beanutils.DynaBean,
         pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style type="text/css">
    select#jbpmProcessId {width:22em;}

    button.mobile-return {
        /*width: 650px;*/
        width: 100%;
        height: 50px;
        font-size: 24px;
        background: #428bca;
        border-color: #357ebd;
        color: white;

        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-weight: normal;
        line-height: 1.42857143;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
    }
</style>

<ww:if test="mobileView">
    <button class="mobile-return">Powr��</button>
</ww:if>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last">
            <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
        </ww:if>
    </ww:iterator>
</ds:available>

<ww:if test="!mobileView">
    <ds:available test="layout2">
        <div id="middleMenuContainer">
            <img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
            <ww:iterator value="tabs" status="status" >
                <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
            </ww:iterator>
        </div> 
    </ds:available>
</ww:if>

<p></p>

<form id="form" action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);" 
	class="dwr" enctype="multipart/form-data">
    <input type="hidden" name="doManualFinish" id="doManualFinish"/>
    <input type="hidden" name="doManualFinish1" id="doManualFinish1"/>
    <input type="hidden" name="mailNotification" id="mailNotification" />

    <ww:hidden name="'folderId'" id="folderId"/>
    <ww:hidden name="'mobileView'" id="mobileViewInput"/>
    <ww:hidden name="'mobileRedirect'" id="mobileRedirectInput"/>
    <ww:hidden name="'mobileReferrer'" id="mobileReferrerInput"/>

    <ww:hidden name="'documentId'" value="documentId" id="documentId"/>
    <ww:if test="noScheme">
        <ww:hidden name="'noScheme'" value="noScheme" id="noScheme"/>
    </ww:if>
    <ww:hidden name="'doArchiveAssignments'" id="doArchiveAssignments"/>
    <%--ww:hidden name="'activity'" value="activity"/--%>
    <ww:hidden name="'boxId'" id="boxId"/>
    <ww:hidden name="'returnToTasklist'" id="returnToTasklist"/>

    <ww:hidden name="'openViewer'" id="openViewer"/>
    <ww:token />
    <input type="hidden" name="doTmp" id="doTmp"/>

    <ds:available test="layout2">
        <ww:if test="!mobileView">
            <div id="middleContainer">
        </ww:if>
        <ww:else>
            <div>
        </ww:else>
    </ds:available>
    <ww:if test="blocked">
        <p><span style="color: red">
                <ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
            </span></p>
    </ww:if>

    <ds:ww-action-errors/>
    <ds:ww-action-messages/>

    <ds:available test="!dwr">
        <table>
            <tr>
                <td>
				ID: 
                    <b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>">
                            <ww:property value="documentId" /></a></b>
                </td>
            </tr>
            <ds:available test="archiwizacja.officeNumber">
				<ww:if test="document != null && document.getOfficeNumber() != null">
				<tr>
                	<td>
						<b><ds:lang text="Numer KO: "/><ww:property value="document.getOfficeNumber()" /></b>
					</td>
           	 	</tr>
				</ww:if>
       		</ds:available>
            <tr>
                <td>
            <ww:if test="document.getDuplicateDoc() != null">
                <span class="warning">
                    <ds:lang text="IdDuplikatu"/>:
                    <b>
                        <a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="document.getDuplicateDoc()"/></ww:url>">
                            <ww:property value="document.getDuplicateDoc()" /></a>
                    </b>
                </span>
            </ww:if>
            </td>
            </tr>
            <tr>
                <td valign="top" class="archiveLeft <ds:available test="archiwizacja.border.right" documentKindCn="documentKindCn">archiveLeftBorder</ds:available>" >
                    <table class="formTable formTableMain">
                        <ww:if test="canChangeDockind">
                            <tr>
                                <td>
                            <ds:lang text="RodzajDokumentuDocusafe"/>:
                            </td>
                            <td id="dockind-select">
                            <ww:if test="documentKinds.size() > 1">
                                <ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key" listValue="value" value="documentKindCn" cssClass="'sel'"
                                           onchange="'changeDockind();'"/>
                            </ww:if>
                            <ww:else>
                                <ww:hidden name="'documentKindCn'" value="documentKindCn"></ww:hidden>
                                <ww:property value="documentKindCn"/>
                            </ww:else>	
                            </td>
                            </tr>
                        </ww:if>
                        <ww:if test="documentAspects != null">
                            <tr>
                                <td>
                            <ds:lang text="AspektDokumentu"/>:
                            </td>
                            <td>
                            <ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
                            </td>
                            </tr>
                        </ww:if>	


                        <jsp:include page="/common/dockind-fields.jsp"/>
                        <jsp:include page="/common/dockind-specific-additions.jsp"/>

                        <ds:available test="archiwizacja.border.right" documentKindCn="documentKindCn">
                            <script type="text/javascript">
                                $j('.leftAlign').css('margin-left', '-16px');
                            </script>
                        </ds:available>

                        <ds:available test="!ignore.acceptances">
                            <ds:additions test="!archiveAcceptancesRight">
                                <%-- akceptacje dokumentu --%>
                                <ww:if test="acceptancesPage">
                                    <ww:if test="documentKindCn == 'invoice_ic' || documentKindCn == 'sad'">
                                        <jsp:include page="/common/acceptances-ic.jsp"/>
                                    </ww:if>
                                    <ww:else>
                                        <jsp:include page="/common/acceptances.jsp"/>
                                    </ww:else>
                                </ww:if>
                            </ds:additions>
                        </ds:available>
                    </table>
                </td>
                <td valign="top" class="archiveRight">
                    <table>
                        <ds:additions test="archiveAcceptancesRight">
                            <%-- akceptacje dokumentu --%>
                            <ww:if test="acceptancesPage">
                                <ww:if test="documentKindCn == 'invoice_ic' || documentKindCn == 'sad'">
                                    <jsp:include page="/common/acceptances-ic.jsp"/>
                                </ww:if>
                                <ww:else>
                                    <jsp:include page="/common/acceptances.jsp"/>
                                </ww:else>
                            </ww:if>
                        </ds:additions>

                        <ww:if test="fastAssignment">
                            <ww:if test="!assignmentsMap.isEmpty">
                                <tr><td>
                                <ds:event name="'doAssignments'" value="'Przeka� do..'"/>
                                <ww:select name="'assignments'" list="assignmentsMap" cssClass="'sel'"/>
                                </td></tr>
                            </ww:if>
                        </ww:if>

                        <tr>
                            <td>
                        <ds:available test="archiwizacja.zalaczniki" documentKindCn="documentKindCn">
                            <!-- archiwizacja.zalaczniki -->
                            <ww:if test="attachments.size > 0">
                                <span class="bold"><ds:lang text="Zalaczniki"/></span>
                                <table cellspacing="0" cellpadding="3" class="attachs">
                                    <ww:iterator value="attachments" status="stat">
                                        <tr <ww:if test="#stat.odd == true">class="lightOddRows"</ww:if>>
                                        <ww:if test="mostRecentRevision != null">
                                            <td class="attachTitle">
                                            <ww:property value="title" />
                                            </td>
                                            <td>
                                            <ww:property value="userSummary" />
                                            </td>
                                        </ww:if><ww:else>
                                            <ds:available test="!archive.hideEmptyAttachments">
                                                <td class="attachTitle">
                                                <ww:property value="title" />
                                                </td>
                                                <td>
                                                <ww:property value="userSummary" />
                                                </td>
                                            </ds:available>
                                        </ww:else>
                                        <ww:if test="mostRecentRevision != null">
                                            <ww:set name="revision" value="mostRecentRevision" />
                                            <ww:set name="size" scope="page" value="#revision.size" />
                                            <ww:set name="ctime" scope="page" value="#revision.ctime" />
                                            <td>
                                            <fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm" />
                                            </td>
                                            <td>
                                                &nbsp
                                                <a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>">
                                                   <img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
															title="<ds:lang text="PobierzZalacznik"/>" /></a>
                                            </td>
                                            <td valign="top">
                                            <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
                                                &nbsp
                                                <a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+#revision.id"/>">
                                                   <img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
																title="<ds:lang text="PobierzZalacznik"/>" /></a>
                                                &nbsp;
                                                <a id="viewerLink" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
                                                    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
																height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
                                            </ww:if><ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(#revision.mime)">
                                                <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ #revision.id"/>')">
                                                   <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                                                </a>
                                            </ww:elseif>
                                            </td>
                                        </ww:if>
                                        </tr>
                                    </ww:iterator>
                                </table>
                            </ww:if>
                        </ds:available>
                </td>
            </tr>
            <tr>
                <td>
            <ds:available test="archiwizacja.flagi" documentKindCn="documentKindCn">
                <!-- archiwizacja.flagi -->
                <table class="flags topSpacer">
                    <ww:if test="flagsPresent">
                        <tr>
                            <td class="alignTop">
                                <span class="bold"><ds:lang text="FlagiWspolne"/>:</span>
                                <ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiWspolneList">
                                    <ww:iterator value="globalFlags">
                                        <ww:if test="canView">
                                            <li>
                                                <div class="inline_flags">
                                                    <ww:checkbox name="'globalFlag'" fieldValue="id"
                                                                 value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
                                                    <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                                         <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                                         <b><ww:property value="c"/></b>
                                                    <ww:property value="description"/>
                                                </div>
                                            </li>
                                        </ww:if>
                                    </ww:iterator>
                                    <li class="hidden" id="FlagiWspolneNone">
                                        <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                                    </li>
                                </ul>
                                <div class="floatingDiv hidden" id="FlagiWspolneDiv">
                                    <ul class="clearList noMarginPadding floatingMenu" id="FlagiWspolneDivList">
                                    </ul>
                                    <ul class="clearList noMarginPadding">
                                        <li class="hidden" id="FlagiWspolneAll">
                                            <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td class="alignTop">
                                <span class="bold"><ds:lang text="FlagiUzytkownika"/>:</span>
                                <ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiUzytkownikaList">
                                    <ww:iterator value="userFlags">
                                        <li>
                                            <div class="inline_flags">
                                                <ww:checkbox name="'userFlag'" fieldValue="id"
                                                             value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
                                                <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                                     <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                                     <b><ww:property value="c"/></b>
                                                <ww:property value="description"/>
                                            </div>
                                        </li>
                                    </ww:iterator>
                                    <li class="hidden" id="FlagiUzytkownikaNone">
                                        <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                                    </li>
                                </ul>
                                <div class="floatingDiv hidden" id="FlagiUzytkownikaDiv">
                                    <ul class="clearList noMarginPadding floatingMenu" id="FlagiUzytkownikaDivList">
                                    </ul>
                                    <ul class="clearList noMarginPadding">
                                        <li class="hidden" id="FlagiUzytkownikaAll">
                                            <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                    </ww:if>																						
                    <ds:available test="labels" documentKindCn="documentKindCn">
                        <td class="alignTop">
                            <span class="bold"><ds:lang text="EtykietyModyfikowalne"/>:</span>
                            <ul class="clearList noMarginPadding listTofloatingMenu" id="EtykietyModyfikowalneList">
                                <ww:iterator value="nonModifiableLabels">
                                    <li>
                                        <b><ww:property value="name"/></b>
                                    <ww:property value="description"/>
                                    </li>
                                </ww:iterator>
                                <ww:iterator value="hackedLabels"> 
                                    <li>
                                        <div class="inline_flags">
                                            <ww:checkbox name="'label'" value="second" fieldValue="first.id" cssClass="'hidden'"/>
                                            <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                                 <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                                 <b><ww:property value="first.name"/></b>
                                            <ww:property value="first.description"/>
                                        </div>
                                    </li>
                                </ww:iterator>
                                <li class="hidden" id="EtykietyModyfikowalneNone">
                                    <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                                </li>
                            </ul>
                            <div class="floatingDiv hidden" id="EtykietyModyfikowalneDiv">
                                <ul class="clearList noMarginPadding floatingMenu" id="EtykietyModyfikowalneDivList">
                                </ul>
                                <ul class="clearList noMarginPadding">
                                    <li class="hidden" id="EtykietyModyfikowalneAll">
                                        <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                                    </li>
                                </ul>
                            </div>
                        </td>
                    </ds:available>
                    </tr>
                </table>
            </ds:available>
            </td>
            </tr>
            <tr>
                <td>
            <ds:available test="archiwizacja.uwagi" documentKindCn="documentKindCn">
                <!-- archiwizacja.uwagi -->
                <table class="topSpacer">
                    <tr>
                        <td>
                            <span class="bold"><ds:lang text="Uwagi"/>:</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                    <ds:available test="archiwizacja.uwagi.slownik" documentKindCn="documentKindCn">
                        <ww:select id="newNote" name="'newNote'" list="noteList" listKey="name" listValue="name" 
                                   headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" onchange="'writeRemarkToTextarea(this)'" />
                    </ds:available>
                    </td>
                    </tr>
                    <tr>
                        <td valign="middle">
                    <ww:textarea onchange="'remarkChanged=1'" name="'content'" id="content" rows="1" cols="100" cssStyle="width:200px" cssClass="'txt'" value="getText('WpiszTrescUwagi')"/>
                    </td>
                    <td>
                    <ds:available test="archiwizacja.uwagi.dodajBtn" documentKindCn="documentKindCn">
                        <ds:event value="getText('DodajUwage')" name="'doArchiveNote'" cssClass="'btn'" onclick="'if (remarkChanged==0) { noRemarkAlert(); return false; }'" disabled="!canUpdate || blocked"/>		
                    </ds:available>
                    </td>
                    </tr>
                </table>
                <script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.growfield-1.1.js"></script>
                <script type="text/javascript">
                    $j("#content").css({overflow:"hidden", height:"21px"}).addClass("italic");
                    $j("#content").one("click", function() {$j(this).attr("value", "").removeClass("italic")});
                    $j("#content").growfield({min:"21"});

                    function writeRemarkToTextarea(objSelect) {
                        var selIndex = objSelect.selectedIndex;

                        if(selIndex >= 1) {
                            $j('#content').trigger('click');
                            $j('#content').val('').text('');
	
                            trace('Writing text: ' + $j($j(objSelect).children()[selIndex]).text());
											
                            $j('#content').val($j($j(objSelect).children()[selIndex]).text());
                            remarkChanged = 1;
                            $j('#newNote').attr('selectedIndex', 0);
                        }
                    }
                </script>

                <table class="remarksTable tableMargin" id="remarks_table_first" style="margin-left: 3px;">
                    <ww:iterator value="remarks">
                        <ww:set name="date" scope="page" value="date"/>
                        <tr>
                            <td>
                        <ww:property value="author"/>
                        </td>
                        <td class="alignRight">
                        <fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
                        </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="borderBlack1" style="width: 400px;">
                                <pre style="word-wrap: break-word" width="60"><ww:property value="content"/></pre>
                            </td>
                        </tr>
                    </ww:iterator>
                </table>

                <div class="viewserverRemarks">
                    <table class="remarksTable tableMargin hidden" id="remarks_table_second" style="margin-left: 3px;"> 
                    </table>
                    <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
                </div>

                <img id="img" class="hidden" src="<ww:url value="'/img/arrow_down.gif'"/>" alt="<ds:lang text="PokazHistorie"/>" title="<ds:lang text="PokazHistorie"/>">

                <script type="text/javascript">
                    if($j("#remarks_table_first tr").length > 2)
                    {
                        $j("#remarks_table_first tr:gt(1)").appendTo("#remarks_table_second");
                        $j("#img").appendTo("#remarks_table_first td:eq(1)").removeClass("hidden");
                        $j("#img").toggle(
                        function() {
                            $j("#remarks_table_second").removeClass("hidden");
                            $j(this).attr("src", "<ww:url value="'/img/arrow_down_hover.gif'"/>");
                        },
                        function() {
                            $j("#remarks_table_second").addClass("hidden");
                            $j(this).attr("src", "<ww:url value="'/img/arrow_down.gif'"/>");
                        });
										
                        //( ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) > 6)) || !($j.browser.msie) ) ? $j('.viewserverRemarks').attr("style", "opacity: 0.85; filter: alpha(opacity=85)") : true ;
										
                        $j(document).ready(function(){
                            setTimeout("$j('#remarks_table_second').width($j('#remarks_table_first').width() - 1); $j('.viewserverRemarks').width($j('#remarks_table_first').width());", 500);
                        });
                    }
                </script>
            </ds:available>
            </td>
            </tr>
            <tr>
                <td>
            <ds:available test="archiwizacja.historia" documentKindCn="documentKindCn">
                <table>
                    <tr>
                        <td>
                    <ds:lang text="OstatniWpisHistoriPisma"/>:
                    </td>
                    </tr>
                    <ww:set name="date" scope="page" value="lastHistory.ctime"/>
                    <tr>
                        <td>
                    <ww:property value="lastHistoryAutchor"/>
                    </td>
                    <td align="right">
                    <fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
                    </td>
                    </tr>
                    <ww:if test="lastHistory.description != null">
                        <tr>
                            <td colspan="2" style="border: 1px solid black;">
                        <ww:property value="lastHistory.description"/>
                        </td>
                        </tr>
                    </ww:if>
                </table>
            </ds:available>
            </td>
            </tr>
            <tr>
                <td>
            <ds:available test="archiwizacja.dekretacja" documentKindCn="documentKindCn">
                <ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
                    <table>
                        <tr>
                        <h4><ds:lang text="ZbiorczaDekretacja"/></h4>
                        <th>
                        </th>
                        <th>
                        <ds:lang text="Uzytkownik"/>
                        </th>
                        <th>
                        <ds:lang text="RodzajDekretacji"/>
                        </th>
                        <ww:if test="showObjectives">
                            <th>
                            <ds:lang text="CelDekretacji"/>
                            </th>
                        </ww:if>
                        </tr>
                        <ww:iterator value="assignmentsMap">
                            <tr>
                                <td>
                                    <input type="checkbox" name="assignments" value="<ww:property value="key"/>"
														id="<ww:property value="key"/>" onchange="updateSelect(this)" />
                            </td>
                            <td>
                            <ww:property value="value" />
                            </td>
                            <td>
                                <select name="assignmentProcess_<ww:property value="key"/>" class="sel" onchange="updateCheckbox(this);checkSelection()">
                                        <option value="">
                                    <ds:lang text="select.wybierz"/>
                                    </option>
                                    <ww:iterator value="wfProcesses">
                                        <option value="<ww:property value="key"/>">
                                    <ww:property value="value" />
                                    </option>
                                </ww:iterator>
                            </select>
                        </td>
                        <ww:if test="showObjectives">
                            <td>
                                <select name="objectiveSel_<ww:property value="key"/>" class="sel">
                                        <option value="">
                                    <ds:lang text="select.wybierz"/>
                                    </option>
                                    <ww:iterator value="objectives">
                                        <option value="<ww:property value="name"/>">
                                    <ww:property value="name" />
                                    </option>
                                </ww:iterator>
                            </select>
                        </td>
                    </ww:if>
                    </tr>
                </ww:iterator>
                <tr>
                    <td colspan="2">
                <ds:event name="'doAssignments'" value="getText('Dekretuj')" onclick="'return checkSubstitutions(this)'" />
                </td>
                </tr>								
            </table>
        </ww:if>
    </ds:available>
    </td>
    </tr>
    <tr>
        <td>
    <ds:available test="archiwizacja.powiazaneDokumenty" documentKindCn="documentKindCn">
        <h4>Dokumenty Powi�zane</h4>
        <table>
            <tr>
                <th><ds:lang text="IdDokumentu" /></th><th><ds:lang text="Tytul" /></th>
            <th><ds:lang text="PodlagDokumentu" /></th><th><ds:lang text="Zalacznik" /></th>
            </tr>
            <ww:iterator value="relatedDocumentBeans">
                <tr>
                    <td style="padding: 5px;"><a href="<ww:url value="'/repository/edit-document.action?id='+id" />"><ww:property value="id"/></a></td>
                    <td style="padding: 5px;"><a href="<ww:url value="'/repository/edit-document.action?id='+id" />"><ww:property value="title"/></a></td>

                <ww:if test="attachmentId != null">
                    <td style="padding: 5px;">
                    <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(attachmentMime)">
                        <a name="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="attachmentId"/><ww:param name="'activity'" value="docType + ',' + activityKey"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
                            <img class="zoomImg" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
													height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" />
                        </a>
                    </ww:if>
                    <ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(attachmentMime)">
                        <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ attachmentId"/>')">
                           <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                        </a>
                    </ww:elseif>
                    <ww:else>
													Brak podgl�du
                    </ww:else>
                    </td>
                    <td style="padding: 5px;">
                        <a href="<ww:url value="'/repository/view-attachment-revision.do?id=' + attachmentId"/>">
                           <img class="zoomImg" src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg"
													title="<ds:lang text="PobierzZalacznik"/>" />
                        </a>
                    </td>	
                </ww:if>
                <ww:else>
                    <td style="padding: 5px;"><strong>Brak za��cznika</strong></td>
                    <td style="padding: 5px;"><strong>Brak za��cznika</strong></td>
                </ww:else>

                </tr>
            </ww:iterator>
        </table>     
    </ds:available>
    </td>
    </tr>
</table>
</td>
</tr>
</table>
</ds:available>
<ds:available test="dwr">

ID: 
                <b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>">
                      <ww:property value="documentId" /></a></b>
		<ds:available test="kancelaria.wlacz.oznaczJakoCzystopis">
			<br />
           <ds:lang text="Typ dokumentu"/>: <ww:property value="czystopis" />
		</ds:available>
		<br/>
		<ds:available test="archiwizacja.officeNumber">
			<ww:if test="document != null && document.getOfficeNumber() != null"></ww:if>
				<h3><ds:lang text="Numer KO: "/><ww:property value="document.getOfficeNumber()" /></h3> 
        </ds:available>
		<ds:available test="kancelaria.wlacz.LokalizacjaDokumentu">
		<ds:lang text="Aktualna lokalizacja dokumentu papierowego"/>: <ww:property value="location" />
		</ds:available>
		<br/>
		<ds:available test="UTP.mail.wyswietl.sprawy">
		<ds:lang text="Numer pisma w sprawie"/>: <ww:property value="caseName"/>
		</ds:available>
		<script type="text/javascript">function validateDockind(){return fieldsManager.validate();}</script>
    <jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
</ds:available>
<ds:dockinds test="dlbinder">
	<ww:if test="avalilabeTypes != null && avalilabeTypes.size() > 0">
				<table>
				<tr>
					<td class="alignTop">
						<ww:select name="'availableDocument'" listKey="getCn()" listValue="getTitle()" multiple="true" size="5" 
						cssClass="'multi_sel'" cssStyle="'width: 250px;'" list="avalilabeTypes" id="availableDocument"/>
					</td>
					<td class="alignMiddle">
						<input type="button" value=" &gt;&gt; " name="moveRightButton"
							onclick="if (moveOptions(this.form.availableDocument, this.form.newDocument) > 0) dirty=true;" class="btn"/>
						<br/>
						<input type="button" value=" &lt;&lt; " name="moveLeftButton"
							onclick="if (moveOptions(this.form.newDocument, this.form.availableDocument) > 0) dirty=true;" class="btn"/>
					</td>
					<td class="alignTop">
						<ww:select name="'values.DOCLISTnewDocument'" id="newDocument" multiple="true" size="5"
						cssClass="'multi_sel'" cssStyle="'width: 200px;'"/>
					</td>
					<td class="alignMiddle">
					<ww:if test="avalilabeTypes != null && avalilabeTypes.size() > 0">							
						<input type="submit" class="btn" value="Dodaj dokumenty"
								onclick="onclick_dodajdokBtn();"/>							
						<script type="text/javascript">
							function onclick_dodajdokBtn()
							{
								selectAllOptions(document.getElementById('newDocument'));
								document.getElementById('doDockindEvent').value = true;
								document.getElementById('dockindEventValue').value = 'DOCLIST';
							}
						</script>		
					</ww:if>
					</td>
				</tr>
				</table>
		</ww:if>
</ds:dockinds>
<ds:available test="kancelaria.wlacz.wersjeDokumentu">
	<ww:if test="czyUkryc">
		 <ds:lang text="Opis nowej wersji"/>:
	   	 <ww:textfield id="nowyOpis" name="'nowyOpis'" maxlength="100" size="20"  cssClass="'txt'" disabled="czyCzystopis"/>
	</ww:if>
</ds:available>
<table class="formTable formTableConnected" style="margin-left: 3px;">
    <ds:available test="archiwizacja.pudlo" documentKindCn="documentKindCn">
        <jsp:include page="/common/box.jsp"/>
    </ds:available>
    <ds:available test="archiwizacja.wybierzPudlo" documentKindCn="documentKindCn">
        <jsp:include page="/common/chooseBox.jsp"/>
    </ds:available>	
    <ww:if test="drsDocs != null">
        <ww:iterator value="drsDocs" status="status">
            <tr>
                <td>
            <ww:if test="#status.first">
                <ds:lang text="NumerySzkod"/>:
            </ww:if>
            </td>
            <td>
                <a href="<ww:url value="drsLink"/>"><ww:property value="drsNR_SZKODY"/> / <ww:property value="drsNR_KOLEJNY"/></a>
            </td>
            </tr>
        </ww:iterator>
    </ww:if>
</table>
<ds:available test="electronicSignature.document">
    <ww:if test="electronicSignatureAvailable">
        <input type="button" class="btn" value="Podpisz" onclick="javascript:window.location='<ww:url value="'/certificates/sign-xml.action?epuap=true&documentId='+documentId+'&returnUrl='+simpleReturnUrl"/>'"/>
    </ww:if>
</ds:available>
<ds:available test="print.barcode.pool">
                <ww:if test = "documentKindCn == 'barcode_order' && (fm.getKey('STATUS')=='30' || fm.getKey('STATUS')==30)">
                    <ds:event value="getText('DrukujPuleBarcodow')" name="'doPrintBarcodePool'" cssClass="'btn'"/>
                </ww:if>
        </ds:available>
           <!-- brawofilip!-->

<ww:if test="showProcessActions">
    <ds:available test="dockind.process.actions.table">
        <table>
            <tr>
                <td style="padding:10px;"><b>Akcje procesu<b><br />
                    <jsp:include page="/office/common/process-actions.jsp"/>
                </td>
            </tr>
        </table>
    </ds:available>
    <ds:available test="!dockind.process.actions.table">
        <jsp:include page="/office/common/process-actions.jsp"/>
    </ds:available>
    <ds:available test="ifpan.klonowanie">
                            <ww:if test = "documentKindCn == 'zamowienia_umowa_pzp' || documentKindCn == 'zam_umo_pzp' && (fm.getKey('STATUS')=='10' || fm.getKey('STATUS')==10)">
                                <ds:event value="getText('Klonuj')" name="'doClone'" cssClass="'btn'"/>
                            </ww:if>
                    </ds:available>
    <ww:if test="!processesManuallyStarted.empty">
        <ds:submit-event id="doManualStartProcess" name="'doManualStartProcess'" value="getText('RozpocznijProces')" />
        <ww:select name="'startingProcessName'" list="processesManuallyStarted" listKey="name" listValue="name"/>
    </ww:if>
    <ww:if test="('invoice_ic' == documentKindCn || 'sad' == documentKindCn) ">
        <ds:has-permission name="INVOICE_WITHDRAW">
            <ds:event value="getText('Wycofaj')" cssClass="'btn'" name="'doWithdraw'" id="doWithdraw"/>
        </ds:has-permission>
        <ww:if test="generalAcceptances && !fm.getBoolean('AKCEPTACJA_FINALNA') && acceptanceButtonsForCurrentUserVisible">
            <input type="submit" onclick="if(!validateFormToNext(this,null)) return false;"
                   value="Akceptuj" class="btn" name="doFullAcceptance"/>
            <ds:available test="!acceptances.simplified">
                <ww:if test="userToGeneralAcceptance != null && !userToGeneralAcceptance.empty">
                    <input type="submit" onclick="if(!validateFormToUser(this,null)) return false;"
                           value="Akceptuj i przeka� do" class="btn" name="doFullAcceptance"/>
                    <ww:select id="userAcceptance" name="'userAcceptance'" list="userToGeneralAcceptance" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
                </ww:if>
                <%--<input type="submit" onclick="if(!validateFormToNext(this,null)) return false;"
                       value="Akceptuj mo�liwe" class="btn" name="doFullAcceptance"/>--%>
            </ds:available>
        </ww:if>
        <ww:if test="acceptancesCnList.size() > 0">
            <input type="button" id="doDiscardTo" name="doDiscardTo" value="<ww:property value="getText('Odrzu� do..')"/>" onclick="forceNote('doDiscardTo',this);" class="btn"/>
            <ww:if test="acceptancesUserList.size() > 0"><%-- Lista ta mo�e by� pusta --%>
                <ww:select id="discardUserAcceptance" name="'discardUserAcceptance'" list="acceptancesUserList" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
            </ww:if>
            <ww:select id="discardCnAcceptance" name="'discardCnAcceptance'" list="acceptancesCnList" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
        </ww:if>
        <ww:else>
            <ds:available test="!acceptances.simplified">
                <ds:event id="doDiscardToIM" name="'doDiscardToIM'" value="getText('OdrzucdoIM')" onclick="'return selectDiscard(this);'" cssClass="'btn'"/>
            </ds:available>
        </ww:else>
        <ww:if test="fm.getBoolean('AKCEPTACJA_FINALNA')"><%-- zg�oszenie B#1391 --%>
            <input type="button" class="btn" value="Generuj potwierdzenie wp�yni�cia" onclick="document.location.href='<ww:url value="'/repository/confirmation.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>'"/>
        </ww:if>
    </ww:if>
    <ds:available test="tasklist.manualToCoordinator" documentKindCn="documentKindCn">
        <ww:if test="manualToCoordinator">
            <ds:event value="getText('Przeka� do koordynatora')" cssClass="'btn'" name="'doManualToCoordinator'" id="doManualToCoordinator"/>
        </ww:if>
    </ds:available>
</ww:if>

<ds:available test="archiwizacja.klon" documentKindCn="documentKindCn" avtivityId="getActivity()">
	<ww:if test="czyUkryc">
    <input type="button" class="btn" value="<ds:lang text="Klonuj"/>"
			onclick="document.location.href='<ww:url value="'/repository/clone-dockind-document.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>';"/>
	</ww:if>
</ds:available>

<ds:available test="kancelaria.wlacz.oznaczJakoCzystopis" avtivityId="getActivity()">
	<ww:if test="maPrawaDoOznaczaniaJakoCzystopis">
		<ww:if test="czyUkryc">
			<ds:submit-event value="getText('Oznacz jako czystopis')" name="'doCzystopis'" cssClass="'btn'"  disabled="save"/> 
		</ww:if>
	</ww:if>
</ds:available>
 
<ds:available test="documentMain.generateConfirmation">
<ww:if test = "isInOfficeDocument()">
<input type="button" value="<ds:lang text="PotwierdzeniePrzyjecia"/>" class="btn"
					onclick="openToolWindow('<ww:url value="'/office/incoming/print-receipt.action'"><ww:param name="'documentId'" value="document.id"/></ww:url>');"/>
</ww:if>
</ds:available>

<ds:available test="kancelaria.wlacz.podpisWewnetrzny" avtivityId="getActivity()">
	<ww:if test="maPrawaDoPodpisywania">
		<ww:if test="czyUkryc">
			<ww:if test="czyCzystopis">
				<ds:submit-event value="getText('Podpisz')"
					confirm="'Zamierzasz podpisa� dokument oznaczony jako czystopis'"
					name="'doSign'" cssClass="'btn" disabled="sign" />
			</ww:if>
			<ww:else>
				<ds:submit-event value="getText('Podpisz')"
					confirm="'Zamierzasz podpisa� dokument. Ta akcja spowoduje oznaczenie dokumentu jako czystopis i nie b�dziesz mie� mo�liwo�ci jego edycji'"
					name="'doSign'" cssClass="'btn" disabled="sign" />
			</ww:else>
		</ww:if>
	</ww:if>
</ds:available>

	<ww:if test="canAddToRS">
    <input type="button" class="btn" value="<ds:lang text="WpiszDoRejestruSzkod"/>" onclick="document.location.href='<ww:url value="'/nationwide/create-drs.action'"><ww:param name="'sourceId'" value="documentId"/></ww:url>';"/>
</ww:if>
	
<ww:if test="showReject">
    <ds:event id="doReject" name="'doReject'" value="getText('Reject')" onclick="'return selectReject(this);'"/>
</ww:if>

<ds:available test="canAssignMe">
<ww:if test="canAssignMe">
    <ds:event id="doAssignMe" name="'doAssignMe'" value="getText('DekretujNaSiebie')" />
</ww:if>
</ds:available>

<ww:if test="showSendToEva && activity != null">
    <ds:event name="'sendToEva'" value="getText('sendToEva')"/>
</ww:if>


<ww:if test="evaMessage != null">
    <ww:property value="evaMessage"/>
</ww:if>
<br/>

<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>

<table>
    <tr>
    <ds:available test="labels.dol" documentKindCn="documentKindCn">
        <td class="alignTop">
            <span class="bold"><ds:lang text="EtykietyModyfikowalne"/>:</span>
            <ul class="clearList noMarginPadding listTofloatingMenu" id="EtykietyModyfikowalneList">
                <ww:iterator value="nonModifiableLabels">
                    <li>
                        <b><ww:property value="name"/></b>
                    <ww:property value="description"/>
                    </li>
                </ww:iterator>
                <ww:iterator value="hackedLabels"> 
                    <li>
                        <div class="inline_flags">
                            <ww:checkbox name="'label'" value="second" fieldValue="first.id" cssClass="'hidden'"/>
                            <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                 <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                 <b><ww:property value="first.name"/></b>
                            <ww:property value="first.description"/>
                        </div>
                    </li>
                </ww:iterator>
                <li class="hidden" id="EtykietyModyfikowalneNone">
                    <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                </li>
            </ul>
            <div class="floatingDiv hidden" id="EtykietyModyfikowalneDiv">
                <ul class="clearList noMarginPadding floatingMenu" id="EtykietyModyfikowalneDivList">
                </ul>
                <ul class="clearList noMarginPadding">
                    <li class="hidden" id="EtykietyModyfikowalneAll">
                        <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                    </li>
                </ul>
            </div>
        </td>
    </ds:available>
    </tr>
</table>



<ds:available test="archiwizacja.uwagi.dol" documentKindCn="documentKindCn">


<ds:available test="archiwizacja.uwagi.dol.tabela" documentKindCn="documentKindCn">
		<a href="javascript:showRemark()"><ds:lang text="DodajUwage"/></a>
		<table id="tableUwagi"  class="topSpacer" style="display:none">
                <tr>
                    <td>
                        <span class="bold"><ds:lang text="Uwagi"/>:</span>
                </td>
            </tr>
            <tr>
                <td>
            <ds:available test="archiwizacja.uwagi.slownik" documentKindCn="documentKindCn">
                <ww:select id="newNote" name="'newNote'" list="noteList" listKey="name" listValue="name" 
                           headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" onchange="'writeRemarkToTextarea(this)'" />
            </ds:available>
            </td>
            </tr>
            <tr>
                <td valign="middle">
            <ww:textarea onchange="'remarkChanged=1'" name="'content'" id="content" rows="1" cols="100" cssStyle="width:200px" cssClass="'txt dontFixWidth'" value="getText('WpiszTrescUwagi')"/>
            </td>
            <td>
            <ds:available test="archiwizacja.uwagi.dodajBtn" documentKindCn="documentKindCn">
                <ds:event value="getText('DodajUwage')" name="'doArchiveNote'" cssClass="'btn'" onclick="'if (remarkChanged==0) { noRemarkAlert(); return false; }'" disabled="!canUpdate || blocked"/>		
            </ds:available>
            </td>
            </tr>
        </table>
        <script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.growfield-1.1.js"></script>
        <script type="text/javascript">
            $j("#content").css({overflow:"hidden", height:"21px"}).addClass("italic");
            $j("#content").one("click", function() {$j(this).attr("value", "").removeClass("italic")});
            $j("#content").growfield({min:"21"});

            function writeRemarkToTextarea(objSelect) {
                var selIndex = objSelect.selectedIndex;

                if(selIndex >= 1) {
                    $j('#content').trigger('click');
                    $j('#content').val('').text('');

                    trace('Writing text: ' + $j($j(objSelect).children()[selIndex]).text());
			
                    $j('#content').val($j($j(objSelect).children()[selIndex]).text());
                    remarkChanged = 1;
                    $j('#newNote').attr('selectedIndex', 0);
                }
            }
        </script>

        <table class="remarksTable tableMargin" id="remarks_table_first" style="margin-left: 3px;">
            <ww:iterator value="remarks">
                <ww:set name="date" scope="page" value="date"/>
                <tr>
                    <td>
                <ww:property value="author"/>
                </td>
                <td class="alignRight">
                <fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
                </td>
                </tr>
                <tr>
                    <td colspan="2" class="borderBlack1" style="width: 400px;">
                        <pre style="word-wrap: break-word" width="60"><ww:property value="content"/></pre>
                    </td>
                </tr>
            </ww:iterator>
        </table>

        <div class="viewserverRemarks">
            <table class="remarksTable tableMargin hidden" id="remarks_table_second" style="margin-left: 3px;"> 
            </table>
            <!--[if lte IE 6.5]><iframe></iframe><![endif]-->
        </div>

        <img id="img" class="hidden" src="<ww:url value="'/img/arrow_down.gif'"/>" alt="<ds:lang text="PokazHistorie"/>" title="<ds:lang text="PokazHistorie"/>">

        <script type="text/javascript">
            if($j("#remarks_table_first tr").length > 2)
            {
                $j("#remarks_table_first tr:gt(1)").appendTo("#remarks_table_second");
                $j("#img").appendTo("#remarks_table_first td:eq(1)").removeClass("hidden");
                $j("#img").toggle(
                function() {
                    $j("#remarks_table_second").removeClass("hidden");
                    $j(this).attr("src", "<ww:url value="'/img/arrow_down_hover.gif'"/>");
                },
                function() {
                    $j("#remarks_table_second").addClass("hidden");
                    $j(this).attr("src", "<ww:url value="'/img/arrow_down.gif'"/>");
                });
		
                //( ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) > 6)) || !($j.browser.msie) ) ? $j('.viewserverRemarks').attr("style", "opacity: 0.85; filter: alpha(opacity=85)") : true ;
		
                $j(document).ready(function(){
                    setTimeout("$j('#remarks_table_second').width($j('#remarks_table_first').width() - 1); $j('.viewserverRemarks').width($j('#remarks_table_first').width());", 500);
                });
            }
        </script>

</ds:available>
<ds:available test="!archiwizacja.uwagi.dol.tabela" documentKindCn="documentKindCn">
    <!-- archiwizacja.uwagi.dol -->
	<ww:if test="lastRemark.content != null">
	    <table>
	    	<b><ds:lang text="search.documents.column.document_remark"/>:</b>
	        <ww:set name="date" scope="page" value="lastRemark.ctime"/>
	        <tr>
	            <td>
	        		<ww:property value="lastRemarkAutchor"/>
	       		</td>
	        	<td class="alignRight">
	        		<fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
	        	</td>
	        </tr>
	        <tr>
				<td colspan="2" class="borderBlack1"><pre style="word-wrap: break-word; margin:5px;" >
					<ww:property value="lastRemark.content"/>
				</td>
			</tr>
		</table>
	</ww:if>
        <a href="javascript:showRemark()"><ds:lang text="PokazUkryjUwagi"/></a>
        <ds:available test="archiwizacja.uwagi.dol.rozwiniete">
            <table id="tableUwagi">
        </ds:available>
        <ds:available test="!archiwizacja.uwagi.dol.rozwiniete">
            <table id="tableUwagi" style="display:none">
        </ds:available>
        <tr>
            <td>
                <ds:lang text="TrescUwagi"/>:
            </td>
        </tr>
        <tr>
            <td>
                <ds:available test="archiwizacja.uwagi.poszerz" documentKindCn="documentKindCn">
                    <ww:textarea onchange="'remarkChanged=1'" name="'content'" id="content" rows="1" cols="150" cssClass="'txt dontFixWidth'" value="getText('WpiszTrescUwagi')"/>
                </ds:available>
                <ds:available test="!archiwizacja.uwagi.poszerz" documentKindCn="documentKindCn">
                    <ww:textarea onchange="'remarkChanged=1'" name="'content'" id="content" rows="1" cols="50" cssClass="'txt dontFixWidth'" value="getText('WpiszTrescUwagi')"/>
                </ds:available>
            </td>
        </tr>
        <tr>
            <td>
                <ds:event value="getText('DodajUwage')" name="'doArchiveNote'" cssClass="'btn'" onclick="'if (remarkChanged==0) { noRemarkAlert(); return false; }'" disabled="!canUpdate || blocked"/>
            </td>
        </tr>
        <ww:iterator value="remarks">
            <ww:set name="date" scope="page" value="date"/>
            <tr>
                <td>
           			<ww:property value="author"/>
            	</td>
           		<td class="alignRight">
            		<fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
            	</td>
            </tr>
            <tr>
                <td colspan="2" class="borderBlack1"><pre style="word-wrap: break-word; margin:5px;" ><ww:property value="content"/></pre></td>
            </tr>
        </ww:iterator>
    </table>

    <script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.growfield-1.1.js"></script>
    <script type="text/javascript">
        $j("#content").css({overflow:"hidden", height:"21px"}).addClass("italic");
        $j("#content").one("click", function() {$j(this).attr("value", "").removeClass("italic")});
        $j("#content").growfield({min:"21"});
    </script>
    </ds:available>
</ds:available>

<ds:available test="archiwizacja.zalaczniki.dol" documentKindCn="documentKindCn">
    <!-- archiwizacja.zalaczniki.dol -->
    <ww:if test="attachments.size > 0">
        <ww:if test="documentKindCn == 'cok_document'">
            <h4>Za��czniki �r�d�owe</h4>
        </ww:if>
        <ww:else>
            <h4><ds:lang text="Zalaczniki"/></h4>
        </ww:else>
        <table  class="formTable">
            <ww:iterator value="attachments">
                <tr>
                <ww:if test="mostRecentRevision != null">
                    <td>
                    <ww:property value="title" />
                    </td>
                    <td>
                    <ww:property value="userSummary" />
                    </td>
                </ww:if><ww:else>
                    <ds:available test="!archive.hideEmptyAttachments">
                        <td>
                        <ww:property value="title" />
                        </td>
                        <td>
                        <ww:property value="userSummary" />
                        </td>
                    </ds:available>
                </ww:else>

                <ww:if test="mostRecentRevision != null">
                    <ww:set name="revision" value="mostRecentRevision" />
                    <ww:set name="size" scope="page" value="#revision.size" />
                    <ww:set name="ctime" scope="page" value="#revision.ctime" />
                    <td>
                    <fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm" />
                    </td>
                    <td>
                        &nbsp
                        <a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>">
                           <img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
									title="<ds:lang text="PobierzZalacznik"/>" /></a>
                    </td>
                    <td valign="top">
                    <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
                        &nbsp
                        <a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+#revision.id"/>">
                           <img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
										title="<ds:lang text="PobierzZalacznik"/>" /></a>
                        &nbsp;
                        <a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
                            <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
										height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
                    </ww:if><ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(#revision.mime)">
                        <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ #revision.id"/>')">
                           <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                        </a>
                    </ww:elseif>
                    <ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimePicture(#revision.mime)">
					  &nbsp;
					  <a name="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="700"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
					 <img class="zoomImg" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
					 height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
					 </ww:elseif>
					</td>
                </ww:if>
                </tr>
            </ww:iterator>
        </table>
    </ww:if>
</ds:available>

<ds:available test="archiwizacja.flagi.dol" documentKindCn="documentKindCn">
    <!-- archiwizacja.flagi.dol -->

    <ww:if test="flagsPresent">
        <table>
            <tr>
                <td class="alignTop">
                    <span class="bold"><ds:lang text="FlagiWspolne"/>:</span>
                    <ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiWspolneDolList">
                        <ww:iterator value="globalFlags">
                            <ww:if test="canView">
                                <li>
                                    <div class="inline_flags<ww:if test="(value and !canClear) or !canEdit"> inline_flags_disabled</ww:if>" > 
                            <ww:checkbox name="'globalFlag'" fieldValue="id"
                                         value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
                            <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                 <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                 <b><ww:property value="c" /></b>
                            <ww:property value="description" />
                            </div>
                            </li>
                            </ww:if>
                        </ww:iterator>
                        <li class="hidden" id="FlagiWspolneDolNone">
                            <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                        </li>
                    </ul>
                    <div class="floatingDiv hidden" id="FlagiWspolneDolDiv">
                        <ul class="clearList noMarginPadding floatingMenu" id="FlagiWspolneDolDivList">
                        </ul>
                        <ul class="clearList noMarginPadding">
                            <li class="hidden" id="FlagiWspolneDolAll">
                                <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                            </li>
                        </ul>
                    </div>
                </td>
                <td class="alignTop">
                    <span class="bold"><ds:lang text="FlagiUzytkownika"/>:</span>
                    <ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiUzytkownikaDolList">
                        <ww:iterator value="userFlags">
                            <li>
                                <div class="inline_flags">
                                    <ww:checkbox name="'userFlag'" fieldValue="id"
                                                 value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
                                    <img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
                                         <img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
                                         <b><ww:property value="c"/></b>
                                    <ww:property value="description"/>
                                </div>
                            </li>	
                        </ww:iterator>
                        <li class="hidden" id="FlagiUzytkownikaDolNone">
                            <span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
                        </li>
                    </ul>
                    <div class="floatingDiv hidden" id="FlagiUzytkownikaDolDiv">
                        <ul class="clearList noMarginPadding floatingMenu" id="FlagiUzytkownikaDolDivList">
                        </ul>
                        <ul class="clearList noMarginPadding">
                            <li class="hidden" id="FlagiUzytkownikaDolAll">
                                <span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        </table>

        <script type="text/javascript">
            $j(document).ready(function ()
            {
                prepareList($j("#FlagiWspolneDolList"), $j("#FlagiWspolneDolNone"), $j("#FlagiWspolneDolAll"),$j("#FlagiWspolneDolDiv"), $j("#FlagiWspolneDolDivList"), FlagiWspolneStat);
                prepareList($j("#FlagiUzytkownikaDolList"), $j("#FlagiUzytkownikaDolNone"), $j("#FlagiUzytkownikaDolAll"),$j("#FlagiUzytkownikaDolDiv"), $j("#FlagiUzytkownikaDolDivList"), FlagiUzytkownikaStat);
					
                if ($j.browser.msie)
                {											
                    $j("#FlagiWspolneDolList").find("li").css("padding", "3px 0px");
                    $j("#FlagiUzytkownikaDolList").find("li").css("padding", "3px 0px");
                }
										
                    <ds:additions test="tcLayout">
                    rozmiar();
                    </ds:additions>
                });
				
            var FlagiWspolneStat = 0;
            var FlagiUzytkownikaStat = 0;
				
            function checkAllNone (list_jq, none_jq, all_jq)
            {
                list_jq.find("li").filter(":has(input:checked)").length ? none_jq.addClass('hidden') : none_jq.removeClass('hidden');
                list_jq.find("li").filter(":has(input:not(:checked))").length ? all_jq.addClass('hidden') : all_jq.removeClass('hidden');
            }
				
            function hoverFix(list_jq, classa)
            {
                if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7))
                {
                    list_jq.find("li").hover(
                    function() {
                        $j(this).addClass("" + classa);
                    }
                    ,
                    function() {
                        $j(this).removeClass("" + classa);
                    }
                );
                }
            }
				
            function createList(list_jq, div_jq, divList_jq, none_jq, all_jq)
            {
                divList_jq.find("li").remove();
					
                list_jq.find("li").filter(":has(input:not(:checked))").clone().appendTo(divList_jq).removeClass("hidden").removeClass("listTofloatingMenuHover") //IE 6 fix
                .find("div > #plus").removeClass("hidden")
                .end().find("div > #minus").addClass("hidden")
                .end().filter(":has(input:disabled)").find("div > #plus").addClass("hidden")
                .end().find("div > b").css("margin-left", "15px");
					
                hoverFix(divList_jq, "floatingMenuHover");
					
                $j.browser.msie ? divList_jq.find("li > div > input:checked").removeAttr("checked") : true ;
					
                var position = list_jq.position();
					
                position['top'] -= Math.round(div_jq.height() / 2);
                position['left'] += Math.round(list_jq.width() / 2);
					
                if ($j.browser.msie)
                {
                    //position['top'] += $j(window).scrollTop();
                    position['left'] += $j(window).scrollLeft();
                }
				
                //d�
                (position['top'] + div_jq.height()) > ($j(window).scrollTop() + $j(window).height()) ? (position['top'] = $j(window).scrollTop() + $j(window).height() - div_jq.height()) : true ;						
                //g�ra
                position['top'] < $j(window).scrollTop() ? position['top'] = $j(window).scrollTop() : true ;
                //prawo
                (position['left'] + div_jq.width()) > ($j(window).scrollLeft() + $j(window).width()) ? (position['left'] = $j(window).scrollLeft() + $j(window).width() - div_jq.width()) : true ;
                //lewo
                position['left'] < $j(window).scrollLeft() ? position['left'] = $j(window).scrollLeft() : true ;
				
                div_jq.css({top: position['top'], left: position['left']}).removeClass("hidden");
                divList_jq.find("li").click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);});
            }
				
            function updateList(this_jq, list_jq)
            {
                var vall = this_jq.find("div > input:checkbox").attr("value");
					
                list_jq.find("li").filter(":has(input:checkbox[value='" + vall + "'])").removeClass("hidden")
                .find("div > input").attr("checked", "checked")
                .end().find("div > #minus").removeClass("hidden")
                .end().find("div > #plus").addClass("hidden");
                this_jq.remove();
                    <ds:additions test="tcLayout">
                    rozmiar();
                    </ds:additions>
                }
				
            function deleteList(div_jq, divList_jq)
            {
                div_jq.addClass("hidden");
                divList_jq.find("li").remove();
            }
				
            function clickEvent(this_jq, list_jq, div_jq, divList_jq, none_jq, all_jq) //nadawany <li>
            {
                if (this_jq.find("div > input").length)
                {
                    if (this_jq.find("div > input:disabled").length)
                    {//nie mam praw edycji - nie robie nic
                        //alert("jeste� GUPI!!");
                    }
                    else if (this_jq.find("div > input:checked").length)
                    {//zaznaczony - przenosze go na plywajaca liste
                        this_jq.addClass("hidden")
                        .find("div > input").removeAttr("checked")
                        .end().find("div > #minus, div > #plus").addClass("hidden");
                        checkAllNone (list_jq, none_jq, all_jq);
                        createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
                    }
                    else
                    {//niezaznaczony - przywracam do na glownej liscie
                        this_jq.addClass("hidden");
                        updateList(this_jq, list_jq);
                        checkAllNone (list_jq, none_jq, all_jq);
                    }
                }
            }
				
            function prepareList (list_jq, none_jq, all_jq, div_jq, divList_jq, stat)
            {
                hoverFix(list_jq, "listTofloatingMenuHover");
                list_jq.find("li")
                .click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);})
                .filter(":has(input:not(:checked))").addClass('hidden')
                .end().filter(":has(input:checked)").find("div > #minus").removeClass("hidden")
                //tu ju� leci do disabled
                .end().filter(":has(input:disabled)").find("div > #minus").addClass("hidden")
                .end().find("div > b").css("margin-left", "15px");
                checkAllNone (list_jq, none_jq, all_jq);
					
                list_jq.hover(
                function() {
                    if (stat == 0)
                    {
                        createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
                        stat = 1;

                        if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
                            div_jq.children("iframe").width(div_jq.width()+50);
                            div_jq.children("iframe").height(div_jq.height()+50);
                            div_jq.css('filter', 'none');
                        }
                    }
                }
                ,
                function() {
                    setTimeout(function() {												
                        if (stat == 1)
                        {
                            deleteList(div_jq, divList_jq);
                            stat = 0;
                        }
                    }, 100
                );
                }
            );
					
                div_jq.hover(
                function() {
                    stat = 2;
                }
                ,
                function() {
                    deleteList(div_jq, divList_jq);
                    stat = 0;
                }
            );
                /* obejscie dla ie6 */
                if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
                    div_jq.append('<iframe class="iframeMask"></iframe>');
                    div_jq.css('border', 'none');
                }
            }
        </script>
    </ww:if>
</ds:available>

<span class="btnContainer" style="display: block; width: 100%">
    <ds:available test="print.barcode.create">
        <ww:if test = "isInOfficeDocument()">
            <ds:event value="getText('DrukujEtykiete')" name="'doPrint'" cssClass="'btn'"/>
        </ww:if>
    </ds:available>

    <ww:if test="(documentKindCn == 'invoice_ic' || documentKindCn == 'sad') && fm.getValue('STATUS') == 'Nowy'">
        <script type="text/javascript">
            $j(document).ready(function(){
                $j('#archiveConfirm').dialog({autoOpen:false,modal:true, width: 460});
            });
            function archiveConfirm(){
                $j('#archiveConfirm').dialog('open');
                $j('#confirmNoButton').get(0).focus();
            }
        </script>
        <div id="archiveConfirm">
            <p>Czy sprawdziles wszystkie pola wymagajace weryfikacji?</p>
            <p><input type="button" onclick="submitFormEvent('doArchive')" value="Tak"/>
                <input id="confirmNoButton" type="button" onclick="$j('#archiveConfirm').dialog('close');" value="Nie"/></p>
        </div>
        <ds:event value="getText('Zapisz')" id="btnZapisz" name="'doArchive'" cssClass="'btn saveBtn'" onclick="'if (validateForm()) archiveConfirm(); return false;'" disabled="!canUpdate || blocked" />
    </ww:if>

    <ww:else>
        <ds:available test="dwr">

            <ww:if test="!mobileView">
                <ds:available test="kancelaria.szablony.simple">
                    <form id="form" action="<ww:url value='baseLink'/>" method="post" >
                        <ww:select id="templateKind" name="'templateKind'" list="availableTemplates" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
                        <ds:event value="getText('Generuj')" id="btnZapisz" name="'doCreateDocument'" cssClass="'btn saveBtn'"/>
                        <script type="text/javascript">
                            $j(function(){
                                // re-enable button hack
                                var button = $j("[name='doCreateDocument']");
                                button.click(function(){
                                    setTimeout(function(){
                                        $j(".btnContainer input[type='button']").attr("disabled", null);
                                    }, 1000);
                                });
                            });
                        </script>
                    </form>
                </ds:available>
            </ww:if>
        	<!-- IFPAN dokument Zapotrzebowania, dzial dzp, oczekiwanie na podpiecie umowy - wylaczenia validacji podczas zapisu-->
        	<ww:if test="documentKindCn == 'zapotrzeb_ifpan' && ( fm.getKey('STATUS') == 40 || fm.getKey('STATUS') == 50 )">
				<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doUpdate'" cssClass="'btn saveBtn'" disabled="!canUpdate || blocked" />
            </ww:if>
            <ww:else>
            	<ds:available test="documentMain.validateWhenUpdate">
            		<ww:if test="newVersion">
            			<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doSaveNewVersion'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
            		</ww:if>
            		<ww:else>
            			<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doUpdate'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
            		    <ds:event value="getText('Zapisz i przejd� na list� zada�')" id="btnZapisz" name="'doUpdateGoToTasklist'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
            		</ww:else>
            		<ds:available test="documentMain.updateWithoutNewVersion">
	           			<ww:if test="hasOnlyUpdateFields">
	           				<ds:event value="getText('dwr.document.AktualizujBezTworzeniaWersji')" id="btnZapisz" name="'doUpdate'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
	           			</ww:if>
           			</ds:available>
           		</ds:available>
           		<ds:available test="!documentMain.validateWhenUpdate">
           			<ww:if test="newVersion">
           				<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doSaveNewVersion'" cssClass="'btn saveBtn'" disabled="!canUpdate || blocked || save" />
           			</ww:if>
           			<ww:else>
           				<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doUpdate'" cssClass="'btn saveBtn'" disabled="!canUpdate || blocked || save" />
           				<ds:event value="getText('Zapisz i przejd� na list� zada�')" id="btnZapisz" name="'doUpdateGoToTasklist'" cssClass="'btn saveBtn'" disabled="!canUpdate || blocked || save" />
           			</ww:else>
           			<ds:available test="documentMain.updateWithoutNewVersion">
	           			<ww:if test="hasOnlyUpdateFields">
	           				<ds:event value="getText('dwr.document.AktualizujBezTworzeniaWersji')" id="btnZapisz" name="'doUpdate'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
	           			</ww:if>
           			</ds:available>
           		</ds:available>
           		
         		
           		<ds:available test="documentMain.SplitOutOfficeDocument">
           			<ww:if test="isOutOfficeDocument()">
 						 <ds:event id="doSaveAndAssignOfficeNumber" name="'doSaveAndAssignOfficeNumber'" cssClass="'btn saveBtn'" value="getText('ZapiszInadajNumerKO')" disabled="!canAssignOfficeNumber || documentHaveOfficeNumber"/>
       		    		<ds:available test="documentMain.doSplitDocument">
       		    			<ds:submit-event value="getText('PodzielPismoDlaKazdegoOdbiorcyOsobno')" name="'doSplitDocument'" cssClass="'btn saveBtn'" disabled="documentHaveOfficeNumber"/>
        				</ds:available>
        	    		<ds:available test="documentMain.doSplitAndAssingOficeNumber">
        	    			<ds:submit-event value="getText('PodzielPismoDlaKazdegoOdbiorcyOsobnoINadajNumerKo')" name="'doSplitAndAssingOficeNumber'" cssClass="'btn saveBtn'"  disabled="!canAssignOfficeNumber || documentHaveOfficeNumber"/>
        	   		 	</ds:available>
                        <ds:available test="wyslijDoEpuap">
                         <ds:submit-event value="getText('Wy�lij do ePUAP')" name="'doWyslijDoEpuap'" cssClass="'btn saveBtn'" />
                         </ds:available>

					</ww:if>
       			</ds:available>




           		<ww:if test="documentKindCn == 'opzz_cases' ">
	            	<ds:available test="opiniowanie.opzz">
	            		<input type="button" class="btn" value="Opiniuj dokumenty" onclick="javascript:window.location='<ww:url value="'/office/incoming/dwr-document-main.action'"><ww:param name="'documentKindCn'" value="'opzz_doc_opinion'"/><ww:param name="'caseId'" value="documentId"/></ww:url>'"/>
					</ds:available>
            	</ww:if>

                <ds:available test="document.archive.SendToExternalRepo" documentKindCn="documentKindCn">
                    <input type="hidden" name="sendToExternalRepo" id="sendToExternalRepo"/>
                    
                    <input type="hidden" name="propUuid" id="propUuid"/>
                    <input type="button" value="Wy�lij do repozytorium zewn�trznego" id="btnSendToExternalRepo" name="doUpdate" class="btn btnZapisz"
					    onclick="javascript:void(window.open('<ww:url value="'/office/alfresco/pick-alfresco.action'"><ww:param name="'chooseType'" value="'FOLDER'"/></ww:url>', null, 'width=700,height=500,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" />
                </ds:available>

            	<ds:available test="dockind.powiadomienie.enabled" documentKindCn="documentKindCn">
            		<input type="button" value="Wy�lij powiadomienie" id="btnZapisz" name="doUpdate"
							class="btn"
							onclick="if(!fieldsManager.validate()) return false;$j('#mailNotification').val(true);sendEvent(this);"
							<ww:if test="!canUpdate || blocked">disabled="true"</ww:if> 
							/>
				</ds:available>
            </ww:else>
        </ds:available>
        <ds:available test="!dwr">
            <ds:available test="dwr">
                <ds:event value="getText('Zapisz')" id="btnZapisz" name="'doArchive'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked" />
            </ds:available>
            <ds:available test="!dwr">
                <ds:event value="getText('Zapisz')" id="btnZapisz" name="'doArchive'" cssClass="'btn saveBtn'" onclick="'if (!validateForm()) return false;'" disabled="!canUpdate || blocked" />
            </ds:available>
        </ds:available>
    </ww:else>

    <ww:if test="forceSaveButton">
        <input type="button" id="doForceArchive" name="doForceArchive" value="<ww:property value="getText('ZapiszBezSprawdzania')"/>" onclick="forceNote('doForceArchive',this);" class="btn saveBtn"/>
        <ds:event value="'Wy�wietl ukryte pola'" id="btnNoScheme" name="'noScheme'" cssClass="'btn'"/>
    </ww:if>

    <ds:available test="archiwizacja.zakonczPrace" documentKindCn="documentKindCn">

        <input type="submit" value="<ww:if test="doWiadomosci"><ds:lang text="PotwierdzamPrzeczytanie"/></ww:if><ww:else><ds:lang text="ZakonczPraceZdokumentem"/></ww:else>" 
        	name="doManualFinish1" class="btn rightAlignedBtn" onclick="return confirmFinish()"
        <ww:if test="!finishOn">disabled="true"</ww:if>/>
        <ww:if test="canReopenWf">
            <ds:event name="'doReopenWf'" value="getText('PrzywrocNaListeZadan')"/> <%-- disabled="!canReopenWf" />  --%>
        </ww:if>

    </ds:available>
    <ds:available test="archiwizacja.zakonczAwaryjne">
   		<ds:event name="'doClose'" onclick="'if(!closeConfirm()){return false}'" value="'Zamknij awaryjnie'"/>
    </ds:available>
    <!-- <ds:event value="getText('Generuj raport faktury')" id="doDockindEvent" name="'doDockindEvent'"/> -->
    
    <%-- Bialystok - archiwum - wykaz zdawczo-odbiorczy PDF --%>
    <ww:if test="documentKindCn=='wykaz'">
    	<input type="button" id="doArchivedCasesListPDF" name="doArchivedCasesListPDF" value="Generuj PDF" class="btn"
			onclick="document.location.href='<ww:url value="'/office/archiwum.action?doArchivedCasesListPDF=true&documentId='+documentId"/>';"/>
    </ww:if>
    
    <%-- Bialystok - archiwum - protokol brakowania --%>
    <ww:if test="documentKindCn=='protokol_br'">
    	<input type="button" id="doProtokolPDF" name="doProtokolPDF" value="Generuj PDF" class="btn"
			onclick="document.location.href='<ww:url value="'/office/archiwum.action?doProtokolPDF=true&documentId='+documentId"/>';"/>
    </ww:if>
    
    <ww:if test="documentKindCn=='protokol_br'">
    	<input type="button" id="doProtokolBrakuj" name="doProtokolBrakuj" value="Wybrakuj dokumentacj�" class="btn"
			onclick="document.location.href='<ww:url value="'/office/archiwum.action?doProtokolBrakuj=true&documentId='+documentId"/>';"/>
    </ww:if>
    
    <ds:available test="notification.on">
	    <input type="button" id="addNotification" name="addNotification" value="Dodaj powiadomienie" class="btn"
				onclick="javascript:document.location.href='<ww:url  value="'/news/notification-add.action'" >
				<ww:param name="'documentId'" value="documentId" />
			</ww:url>'" 
		/>
    </ds:available>
    <ds:event value="'doAlternativeUpdate'" id="btnAlternativeUpdate" cssStyle="'display: none !important;'" name="'doAlternativeUpdate'"/>

</span>

<ds:available test="layout2">
    <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
    <div class="bigTableBottomSpacer">&nbsp;</div>
    </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

<script type="text/javascript">
    function getBudgetPopupParam() {
        return 'documentId=' + <ww:property value="documentId" /> + '&mpkId=' + document.getElementsByName('values.mpk')[0].value;
    }
	
    var flashes = 0;
    var flash_this;
		
    function flash(kolor, czas, kolor2, czas2)
    {
        if (flashes >= 60) return;
        flashes++;
        flash_this.style.color = kolor;
        setTimeout('flash("' + kolor2 + '",' + czas2 + ',"' + kolor + '",' + czas + ')', czas);
    }
		
    var remarkChanged = 0;

    function noRemarkAlert() {
        alert('<ds:lang text="NieDodanoUwagi"/>');
    }
		
    function showRemark()
    {
        document.getElementById('tableUwagi').style.display=
            ((document.getElementById('tableUwagi').style.display=="none")?true:false)?"":"none";
            <ds:additions test="tcLayout">
            rozmiar();
            </ds:additions>
        }
		
    function selectDiscard(btn){
        try{
            if(safeSelectDiscard(btn)){
                sendEvent(btn);
                return true;
            } else {
                return false;
            }
        } catch (x) {
            return false;
        }
    }

    function safeSelectDiscard(btn)
    {
        var note2 = document.getElementById('content');
        var tmp2 = note2.value;
        if(tmp2.length < 1 || tmp2.toString().indexOf('<ds:lang text="WpiszTrescUwagi"/>') != -1){
            alert('<ds:lang text="UwagaJestWymagana"/>');
            $j("input[name=doArchiveNote]").addClass("hidden");
            $j("#tableUwagi").css("display","");
                <ds:additions test="tcLayout">
                rozmiar();
                </ds:additions>
                flash_this = btn;
            flash("white", 500, "black", 500);
            return false;
        } else {
            return true;
        }
    }
    function selectReject(btn)
    {
        var note = document.getElementById('newNote');
        var note2 = document.getElementById('content');
        var tmp = '';
        var tmp2 = '';
        if(note != null )
            tmp = note.value;
        if(note2 != null)
            tmp2 = note2.value;
			
        if((tmp.length < 1 || tmp == '<ds:lang text="WpiszTrescUwagi"/>') && tmp2.length < 1)
				
        {
            alert('<ds:lang text="UwagaJestWymagana"/>');
            return false;
        }
        sendEvent(btn);
        return true;
    }
	
	
    function updateSelect(checkbox)
    {
            <ds:extras test="business">
            var id = checkbox.value;
        var sels = document.getElementsByName('assignmentProcess_'+id);
        if (checkbox.checked && sels && sels.length > 0)
        {
            var sel = sels[0];
            if (sel.options && sel.options.length > 0)
                sel.options.selectedIndex = 1;
        } else
        {
            var sel = sels[0];
            if (sel.options && sel.options.length > 0)
            {
                sel.options.selectedIndex = 0;
            }
        }
            </ds:extras>
        }
		
    function updateCheckbox(select)
    {
        var id = select.name.substring(select.name.indexOf('_')+1);
        E(id).checked = (select.value.length > 0);
    }
		
    function checkSubstitutions(btn)
    {
        var count=0;
        var show=0;
        assignments = document.forms[0].assignments;
        var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
        var str1;
			
        var chks = document.getElementsByName('assignments');
        var checkedCount = 0;
        for (var i=0; i < chks.length; i++)
        {
            if (chks.item(i).checked) checkedCount++;
        }
        if (checkedCount == 0)
        {
            alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
            return false;
        } 
			
        try
        {
                <ww:iterator value="substituted" status="status">
                if(assignments[count].checked && "<ww:property value="value"/>"!="")
            {
                show=show+1;
                str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
                str=str+str1;
            }
            count=count+1;
                </ww:iterator>
            }
        catch(err)
        {
                <ww:iterator value="substituted" status="status">
                if(assignments.checked && "<ww:property value="value"/>"!="") 
            {
                show=show+1;
                str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
                str=str+str1;
            }
                </ww:iterator>
            }
        if(show == 0)
        {
            if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
                return false;
        }
        else
        {
            if(!confirm(str))
                return false;
        }
        if(isChanges)
        {
            if(confirm('<ds:lang text="ZapisacZmianyPrzedDekretacja"/>'))
            {
                if (!validateForm())
                {
                    return false;
                }
                else
                {
                    document.getElementById('doArchiveAssignments').value='true';
                    E('form').submit();
                }						
            }
            else
            {
                sendEvent(btn);
                return true;
            }
        }
        else
        {
            sendEvent(btn);
            return true;
        }
    }
		
	
    function confirmFinish()
    {
            <ww:if test="doWiadomosci">   
            $j('#doManualFinish').val(true);
            return true;
            </ww:if>
            <ww:if test="!finishOrAssignState.canFinish">	
            alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
        return false;
            </ww:if>
            <ww:if test="!finishOrAssignState.finishWarnings.empty && !documentFax">		
            return confirmDialog('<ds:lang text="ZakonczPraceZdokumentem"/>', '<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/><br/>Zako�czy�?', true,
        function() {
            $j('#doManualFinish').val(true);
        });
            </ww:if>
            <ww:else>
            return confirmDialog('<ds:lang text="ZakonczPraceZdokumentem"/>', '<ds:lang text="NaPewnoZakonczyc"/>?', true,
        function() {
            $j('#doManualFinish').val(true);
        });
            </ww:else>
	
            return true;
    }
	
    function validateForm()
    {
        if (!validateDockind())
            return false;
        return true;
    }
	
    /* wolane z doctype-fields - w edycji nei wybiera sie wzoru umowy */
    /* wiec funkcja nic nie robi									*/
    function __nw_updateAttachmentPattern(typeId)
    {
    }
	
    function openViewer()
    {
        var objViewer = document.getElementById('openViewer');
        if(objViewer.value == 'true')
        {
                <ww:if test="attachments.size > 0">
                <ww:set name="revision" value="attachments.get(0).mostRecentRevision"/>
                openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);
                </ww:if>
                objViewer.value = false;
        }
    }
    
    function openNewDocument()
    {
    	window.location='<ww:url value="'/office/incoming/new.action?caseId='+documentId+'&documentKindCn=opzz_doc_opinion"/>';
    }
    openViewer();
	
</script>
</form>

<div id ="noteForm" style="display:none"><%-- Wy�wietla si� jako dialog --%>
    <ww:textarea onchange="'remarkChanged=1'" name="'content'" id="noteFormArea" rows="3" cols="50" cssClass="'txt dontFixWidth'"/>
    <div><input type="button" class="btn" value="<ww:property value="getText('DodajUwage')"/>" id="noteFormButton" name="doArchiveNote" cssClass="'btn'" onclick="if(checkNoteFormArea()){sendEvent(this)};"/></div>
</div>

<script type="text/javascript">
    $j(document).ready(function(){
        try {
            $j("#noteForm").dialog({autoOpen:false,modal:true, width: 400, title:'Uwaga'});
            $j("#noteFormArea").growfield({min:"21"});
        } catch(e) {}
    });
    function checkNoteFormArea(){
        if($j("#noteFormArea").get(0).value.length > 0){
            $j("#content").remove();<%-- "konkurencyjne" pole z uwag� --%>
            $j("#noteForm").dialog("close");
            $j("#noteForm").hide();
            $j("#noteForm").appendTo("#form");
            return true;
        } else {
            return false;
        }
    }
    <%-- wymusza uwag� po czym submituje formularz --%>
            function forceNote(actionName, button){
                    <ds:available test="archive.popup">
                    $j("#noteFormButton").get(0).name = actionName;
                $j("#noteForm").dialog("open");
                    </ds:available><ds:available test="!archive.popup">
                    var bool = safeSelectDiscard(button);
                if(bool){
                    $j("form").append("<input type='hidden' id='"+actionName+"' name='"+actionName+"' value='true'/>");
                    button.form.submit();
                }
                    </ds:available>
                }
</script>
<script type="text/javascript">
    $j(document).ready(function ()
    {
        prepareList($j("#FlagiWspolneList"), $j("#FlagiWspolneNone"), $j("#FlagiWspolneAll"),$j("#FlagiWspolneDiv"), $j("#FlagiWspolneDivList"), FlagiWspolneStat);
        prepareList($j("#FlagiUzytkownikaList"), $j("#FlagiUzytkownikaNone"), $j("#FlagiUzytkownikaAll"),$j("#FlagiUzytkownikaDiv"), $j("#FlagiUzytkownikaDivList"), FlagiUzytkownikaStat);
        prepareList($j("#EtykietyModyfikowalneList"), $j("#EtykietyModyfikowalneNone"), $j("#EtykietyModyfikowalneAll"),$j("#EtykietyModyfikowalneDiv"), $j("#EtykietyModyfikowalneDivList"), EtykietyModyfikowalneStat);
        if ($j.browser.msie)
        {											
            $j("#FlagiUzytkownikaList").find("li").css("padding", "3px 0px");
            $j("#FlagiWspolneList").find("li").css("padding", "3px 0px");
            $j("#EtykietyModyfikowalneList").find("li").css("padding", "3px 0px");
        }
	
            <ds:additions test="tcLayout">
            rozmiar();
            </ds:additions>
        });

    var FlagiWspolneStat = 0;
    var FlagiUzytkownikaStat = 0;
    var EtykietyModyfikowalneStat = 0;

    function checkAllNone (list_jq, none_jq, all_jq)
    {
        list_jq.find("li").filter(":has(input:checked)").length ? none_jq.addClass('hidden') : none_jq.removeClass('hidden');
        list_jq.find("li").filter(":has(input:not(:checked))").length ? all_jq.addClass('hidden') : all_jq.removeClass('hidden');
    }

    function hoverFix(list_jq, classa)
    {
        if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7))
        {
            list_jq.find("li").hover(
            function() {
                $j(this).addClass("" + classa);
            }
            ,
            function() {
                $j(this).removeClass("" + classa);
            }
        );
        }
    }

    function createList(list_jq, div_jq, divList_jq, none_jq, all_jq)
    {
        divList_jq.find("li").remove();
	
        list_jq.find("li").filter(":has(input:not(:checked))").clone().appendTo(divList_jq).removeClass("hidden").removeClass("listTofloatingMenuHover") //IE 6 fix
        .find("div > #plus").removeClass("hidden")
        .end().find("div > #minus").addClass("hidden")
        .end().filter(":has(input:disabled)").find("div > #plus").addClass("hidden")
        .end().find("div > b").css("margin-left", "15px");
	
        hoverFix(divList_jq, "floatingMenuHover");
	
        $j.browser.msie ? divList_jq.find("li > div > input:checked").removeAttr("checked") : true ;
	
        var position = list_jq.position();
	
        position['top'] -= Math.round(div_jq.height() / 2);
        position['left'] += Math.round(list_jq.width() / 2);
	
        if ($j.browser.msie)
        {
            //position['top'] += $j(window).scrollTop();
            position['left'] += $j(window).scrollLeft();
        }

        //d�
        (position['top'] + div_jq.height()) > ($j(window).scrollTop() + $j(window).height()) ? (position['top'] = $j(window).scrollTop() + $j(window).height() - div_jq.height()) : true ;						
        //g�ra
        position['top'] < $j(window).scrollTop() ? position['top'] = $j(window).scrollTop() : true ;
        //prawo
        (position['left'] + div_jq.width()) > ($j(window).scrollLeft() + $j(window).width()) ? (position['left'] = $j(window).scrollLeft() + $j(window).width() - div_jq.width()) : true ;
        //lewo
        position['left'] < $j(window).scrollLeft() ? position['left'] = $j(window).scrollLeft() : true ;

        div_jq.css({top: position['top'], left: position['left']}).removeClass("hidden");
        divList_jq.find("li").click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);});
    }

    function updateList(this_jq, list_jq)
    {
        var vall = this_jq.find("div > input:checkbox").attr("value");
	
        list_jq.find("li").filter(":has(input:checkbox[value='" + vall + "'])").removeClass("hidden")
        .find("div > input").attr("checked", "checked")
        .end().find("div > #minus").removeClass("hidden")
        .end().find("div > #plus").addClass("hidden");
        this_jq.remove();
            <ds:additions test="tcLayout">
            rozmiar();
            </ds:additions>
        }

    function deleteList(div_jq, divList_jq)
    {
        div_jq.addClass("hidden");
        divList_jq.find("li").remove();
    }

    function clickEvent(this_jq, list_jq, div_jq, divList_jq, none_jq, all_jq) //nadawany <li>
    {
        if (this_jq.find("div > input").length)
        {
            if (this_jq.find("div > input:disabled").length)
            {//nie mam praw edycji - nie robie nic
                //alert("jeste� GUPI!!");
            }
            else if (this_jq.find("div > input:checked").length)
            {//zaznaczony - przenosze go na plywajaca liste
                this_jq.addClass("hidden")
                .find("div > input").removeAttr("checked")
                .end().find("div > #minus, div > #plus").addClass("hidden");
                checkAllNone (list_jq, none_jq, all_jq);
                createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
            }
            else
            {//niezaznaczony - przywracam do na glownej liscie
                this_jq.addClass("hidden");
                updateList(this_jq, list_jq);
                checkAllNone (list_jq, none_jq, all_jq);
            }
        }
    }

    function discardObjectAcceptance(acceptanceCn, objectId, button){
        $j("#objectId").val(objectId);
        $j("#acceptanceCn").val(acceptanceCn);
        forceNote('doDiscardObjectAcceptance', button);
    }

    function prepareList (list_jq, none_jq, all_jq, div_jq, divList_jq, stat)
    {
        hoverFix(list_jq, "listTofloatingMenuHover");
        list_jq.find("li")
        .click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);})
        .filter(":has(input:not(:checked))").addClass('hidden')
        .end().filter(":has(input:checked)").find("div > #minus").removeClass("hidden")
        //tu ju� leci do disabled
        .end().filter(":has(input:disabled)").find("div > #minus").addClass("hidden")
        .end().find("div > b").css("margin-left", "15px");
        checkAllNone (list_jq, none_jq, all_jq);
	
        list_jq.hover(
        function() {
            if (stat == 0)
            {
                createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
                stat = 1;

                if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
                    div_jq.children("iframe").width(div_jq.width()+50);
                    div_jq.children("iframe").height(div_jq.height()+50);
                    div_jq.css('filter', 'none');
                }
            }
        }
        ,
        function() {
            setTimeout(function() {												
                if (stat == 1)
                {
                    deleteList(div_jq, divList_jq);
                    stat = 0;
                }
            }, 100
        );
        }
    );
	
        div_jq.hover(
        function() {
            stat = 2;
        }
        ,
        function() {
            deleteList(div_jq, divList_jq);
            stat = 0;
        }
    );
        /* obejscie dla ie6 */
        if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
            div_jq.append('<iframe class="iframeMask"></iframe>');
            div_jq.css('border', 'none');
        }
    }

    <ds:available test="archiwizacja.uwagi.pokaz">//archiwizacja.uwagi.pokaz
    showRemark();
    </ds:available>
        
    <ds:available test="archiwizacja.dokument.zablokujZmianeRodzaju" documentKindCn="documentKindCn">
        selectedDocumentKind = $j('#documentKindCn option:selected');
        $j('#documentKindCn').remove();

        $j('#dockind-select').append('<input type=\'hidden\' name=\'documentKindCn\' value=\''+ selectedDocumentKind.val()+'\'/><b>'
            + selectedDocumentKind.text() + '</b>');
    </ds:available>
    
	function closeConfirm() {
		return confirm('Czy na pewno zako�czy� prac� z zadaniem?');
	}
</script>

<ds:available test="archiwizacja.dokument.updatujListeUzytkownika" documentKindCn="documentKindCn">
    <jsp:include page="/common/update-users-field.jsp"/>
</ds:available>

            
<!-- JE�LI WNIOSEK URLOPOWY -->
<ww:if test="fm.documentKind.cn == 'absence-request'">
    <jsp:include page="/common/free-days.jsp"/>
</ww:if>

<ww:if test="mobileView">
    <button class="mobile-return">Powr��</button>
</ww:if>

<!--N koniec document-archive.jsp N-->

<script type="text/javascript">
    var MOBILE_VIEW = "<ww:property value="mobileView"/>" === "true";
</script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/document-archive-mobile.js"></script>