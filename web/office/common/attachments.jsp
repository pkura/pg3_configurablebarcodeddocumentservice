<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N attachments.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean,
                 pl.compan.docusafe.core.office.InOfficeDocument"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>


<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="baseLink"/>" method="post" enctype="multipart/form-data">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
	<ww:if test="blocked">
		<p><span style="color: red">
			<ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
		</span></p>
	</ww:if>
	<ww:if test="czyJestPodpisany">
		<p><span style="color: red">
			<ds:lang text="TenDokumentZostalPodpisanyIjestZablokowanaEdycjaZalacznikow"/>.
		</span></p>
	</ww:if>
	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'attachmentId'" value="attachmentId"/>
	<ww:hidden name="'activity'" value="activity"/>
	<ww:hidden name="'selectedAttachmentId'" value="selectedAttachmentId"/>


	<ds:available test="deleteReason">
 	<ww:hidden name="'doDelete'" value="doDelete"/> 
	</ds:available>
	<ww:hidden name="'doDecrypt'" value="doDecrypt"/> 
	<ww:hidden name="'count'" value="count"/> 
	<ww:token />

	<ww:if test="attachment != null">
		<table border="0" cellspacing="0" cellpadding="0">
			<ww:if test="attachment.mostRecentRevision != null">
				<tr class="formTableDisabled">
					<td>
						<ds:lang text="Tytul"/>
                        <ww:if test="attachment.isEditable()"><span class="star">*</span></ww:if>:
					</td>
					<td>
                        <ww:if test="attachment.isEditable()"><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'" value="attachment.title"/></ww:if>
                        <ww:else><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt'" value="attachment.title" disabled="true"/></ww:else>
					</td>
				</tr>
			</ww:if>
			<ww:else>
				<tr>
					<td>
						<ds:lang text="Tytl"/>:
					</td>
					<td>
						<ww:hidden name="'title'" id="title" value="attachment.title"/><ww:property value="attachment.title"/>
					</td>
				</tr>
			</ww:else>
			
			<%--	<tr>
				<td>
					Kod kreskowy
				</td>
				<td>
					<ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" value="attachment.barcode"/>
				</td>
			</tr>	--%>
			<%--	tworz�cy u�ytkownik	--%>
			<tr>
				<td colspan="2">
					<br>
                    <ww:if test="attachment.isEditable()">
                        <ds:event name="'doUpdate'" value="getText('Zapisz')"
                            onclick="'if (!validateForm1()) return false'"
                            disabled="readOnly || blocked || czyJestPodpisany" cssClass="'btn saveBtn'"/>
                    </ww:if>
				</td>
			</tr>
		</table>

		<p></p>

        <%--	lista wersji	--%>
        <ww:if test="!revisionsReversed.empty" >
            <h4><ds:lang text="WersjeZalacznika"/></h4>
            <table>
                <tr>
                    <th>
                        <ds:lang text="Autor"/>
                    </th>
                    <th>
                        <ds:lang text="Rozmiar"/>
                    </th>
                    <th>
                        <ds:lang text="Data"/>
                    </th>
                    <th>
                        <ds:lang text="Wersja"/>
                    </th>
                    <th></th>
                </tr>

                <ww:iterator value="revisionsReversed" status="status">
                    <tr>
                        <td>
                            &nbsp;
                            <ww:property value="author.asFirstnameLastname()"/>
                        </td>
                        <td>
                            &nbsp;
                            <ds:format-size value="#this['size']"/>
                        </td>
                        <td>
                            &nbsp;
                            <ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/>
                        </td>
                        <td align="center">
                            <ww:property value="revision"/>
                        </td>
                        <td>
                            &nbsp;
                            <a <ww:if test="!#status.first">onclick="return confirm('<ds:lang text="ToNieJestNajnowszaWersjaZalacznikaKontynuowac"/>?')"</ww:if> href="<ww:url value="'/repository/view-attachment-revision.do?id='+id"/>"><ds:lang text="pobierzTeWersje"/></a>
                        </td>
                    </tr>

                    <ds:modules test="certificate">
                        <ww:if test="signatures.size() > 0">
                            <tr>
                                <td></td>
                                <td>
                                    <ds:lang text="Podpisy"/>:
                                </td>
                                <td colspan="6">
                                    <table>
                                        <ww:iterator value="signatures">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <ww:property value="author"/>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                    <ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/>
                                                </td>
                                                <td colspan="3">
                                                    <a href="<ww:url value="'/office/common/attachment-signature.action?doView=true&signatureId='+id"/>"><ds:lang text="Pobierz"/></a>
                                                </td>
                                                <td>
                                                    <a href="javascript:openToolWindow('<ww:url value="'/office/common/attachment-signature.action?doVerify=true&signatureId='+id"/>', 'verify', 500, 400);"><ds:lang text="weryfikuj"/></a>
                                                </td>
                                                <td>
                                                    <ww:if test="correctlyVerified != null && !correctlyVerified">
                                                        <span style="color: red">
                                                            <ds:lang text="bladWpodpisie"/>
                                                        </span>
                                                    </ww:if>
                                                </td>
                                            </tr>
                                        </ww:iterator>
                                    </table>
                                </td>
                            </tr>
                        </ww:if>
                    </ds:modules>
                </ww:iterator>
            </table>
        </ww:if>

        <ww:if test="attachment.isEditable()">
            <ww:file name="'revisionFile'" id="revisionFile" size="50" cssClass="'txt dontFixWidth'"/>
            <ds:event name="'doAddRevision'" value="getText('NowaWersjaPliku')"
                onclick="'if (!validateForm3()) return false'"
                disabled="readOnly || blocked || czyJestPodpisany"/>
        </ww:if>

		<p></p>

		<input type="button" value="<ds:lang text="PowrotDoListyZalacznikow"/>" class="btn"
			onclick="document.location.href='<ww:url value="baseLink"><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>';"/>

	</ww:if>

	<ww:elseif test="!attachmentBeans.empty">
		<table id="test" class="bottomSpacer mediumTable search">
		
			<tr>
				<th></th>
				<th>
					<ds:lang text="Tytul"/>
				</th>
				<th>
					<ds:lang text="Autor"/>
				</th>
				<th>
					<ds:lang text="Rozmiar"/>
				</th>
				<th>
					<ds:lang text="Data"/>
				</th>
				<th>
					<ds:lang text="Wersja"/>
				</th>
			<ds:available test="file.encryption">
				<th>Zaszyfrowany</th>
			</ds:available>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>

			<ww:iterator value="attachmentBeans">
				<ww:if test="mostRecentRevision != null">
					<tr>
						<td>
                            <ww:if test="editable">
							    <ww:checkbox name="'attachmentIds'" fieldValue="id" onchange="'countSelectedAtts(this)'" />
                            </ww:if>
						</td>
						<td>
                            <a href="<ww:url value="baseLink"><ww:param name="'attachmentId'" value="id"/><ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>"><ww:property value="title"/></a>
						</td>
						<td>
							&nbsp;
							<ww:property value="author.asFirstnameLastname()"/>
						</td>
						<td>
							&nbsp;
							<ds:format-size value="mostRecentRevision.size"/>
						</td>
						<td>
							&nbsp;
							<ds:format-date value="mostRecentRevision.ctime" pattern="dd-MM-yy HH:mm"/>
						</td>
						<td align="center">
							&nbsp;
							<ww:property value="mostRecentRevision.revision"/>
						</td>
						<ds:available test="file.encryption">
							<td align="center">
							&nbsp;
							 <ww:if test="encrypted">TAK</ww:if>
							 <ww:else>NIE</ww:else>
							</td>
						</ds:available>
						<td valign="top" width="32">
							
							<a class="imgView" href="<ww:url value="'/repository/view-attachment-revision.do?id='+mostRecentRevision.id"/>">
							<img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
						</td>
						<td valign="top" width="64">
							<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mostRecentRevision.mime)">

								<a class="imgView" href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+mostRecentRevision.id"/>"><img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
								&nbsp;
								<a class="imgView" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">
								    <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
								</a>
							</ww:if>
							<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimePicture(mostRecentRevision.mime)">
                            <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">
                                <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                            </a>
                            </ww:if>
							<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(mostRecentRevision.mime)">
                            <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ mostRecentRevision.id"/>')">
						        <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
							</a>
                            </ww:if>
						</td>
					
						<ds:available test="electronicSignature.attachment">
							<td width="32">
                                <a class="xmlView" href="<ww:url value="'/certificates/sign-xml.action?attachmentRevision=' + mostRecentRevision.id + '&returnUrl=' + simpleReturnUrl"/>">
								    <img src="<ww:url value="'/img/podpisz.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PodpiszZalacznik"/>"/>
								</a>
							</td>
                        </ds:available>
						<ds:available test="attachments.send.fax">
							<td width="32">
								<a class="imgView" href="javascript:openToolWindow('<ww:url value="'/office/common/send-fax.action?id='+mostRecentRevision.id"/>', 'verify', 500, 400);"><img src="<ww:url value="'/img/fax.gif'"/>" width="11" height="11" border="0" alt="FAX" title="<ds:lang text="WyslijFax"/>"/></a>
							</td>
						</ds:available>
						
						<ds:available test="webodf.viewer">

						<td width="32">
                        <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeWebOdf(mostRecentRevision.mime)">
						<a class="imgView" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="mostRecentRevision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'editAsWebOdf'" value="true"/></ww:url>', 'vs', 600, 600);">
								    <img src="<ww:url value="'/img/webodf_edit.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="EdytujDokument"/>"/>
						</a>
                        </ww:if>
						</td>
						</ds:available>
						
							<ds:modules test="certificate">
								<ww:if test="useSignature && canSign">
								<td valign="top" width="32">
									&nbsp;
									<a href="javascript:openToolWindow('<ww:url value="'/office/common/attachment-signature.action?id='+mostRecentRevision.id"/>', 'sign', 400, 300);">
									<img src="<ww:url value="'/img/podpisz.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="PodpiszZalacznik"/>"/></a>
								</td>
								</ww:if>
							</ds:modules>
						
						
							<ww:if test="editorOn && mostRecentRevision.editable">
							<td valign="top" width="32">
								&nbsp;
								<a href="<ww:url value="baseLink+'?doInitEdit=true&revisionId='+mostRecentRevision.id+'&documentId='+documentId"/>">
								<img src="<ww:url value="'/img/pencil.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="EdytujZalacznik"/>"/></a>
								</td>
							</ww:if>
						
							<ww:if test="canSplit">
							<td valign="top">
								&nbsp;
								<a href="<ww:url value="baseLink"><ww:param name="'doSplitAttachment'" value="true"/><ww:param name="'attachmentId'" value="id"/>
								<ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>"><ds:lang text="podziel"/></a>
							</td>	
							</ww:if>
											
					</tr>
				</ww:if>
				<ww:elseif test="'mail'.equals(cn)">
					<tr>
						<td>
							<ww:checkbox name="'attachmentIds'" fieldValue="id" />
						</td>
						<td>
							<a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="wparam"/></ww:url>"><ww:property value="title"/></a>
						</td>
						<td>
							&nbsp;
							<ww:property value="author.asFirstnameLastname()"/>
						</td>
						<td align="center">
							-
						</td>
						<td>
							&nbsp;
							<ds:format-date value="attCtime" pattern="dd-MM-yy HH:mm"/>
						</td>
						<td align="center">
							-
						</td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</ww:elseif>
				<ww:else>
					<tr>
						<td></td>
						<td>
							<a href="<ww:url value="baseLink"><ww:param name="'attachmentId'" value="id"/><ww:param name="'documentId'" value="document.id"/><ww:param name="'activity'" value="activity"/></ww:url>"><ww:property value="title"/></a>
						</td>
						<td>
							&nbsp;
							<ww:property value="#u.firstname + ' ' + #u.lastname"/>
						</td>
						<td colspan="3">
							<ww:if test="lparam != null">
								<b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)"/></i></b>
							</ww:if>
							<ww:else>
								<i>(<ds:lang text="pusty"/>)</i>
							</ww:else>
						</td>
						<td>
							&nbsp;
							<ww:property value="barcode"/>
						</td>
						<td>
							<select name="markAttachment[&quot;<ww:property value="id"/>&quot;]" class="sel">
								<option value="">-- wybierz --</option>
								<option value="<%= InOfficeDocument.ATTACHMENT_SEEN %>">przedstawiono do wgl�du</option>
								<option value="<%= InOfficeDocument.ATTACHMENT_FILED %>">posiadany</option>
								<option value="<%= InOfficeDocument.ATTACHMENT_INSPE %>">do okazania/uregulowania przy odbiorze</option>
								<option value="<%= InOfficeDocument.ATTACHMENT_MISSING %>">brak</option>
								<%--	<option value="*">usu� deklaracj�</option>	--%>
							</select>
						</td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</ww:else>
			</ww:iterator>
			<tr>
			
<div id="myDiv" style="display:none">
		<ds:lang text="powodUsuniecia"/>:
        <input type="text" name="powodUsuniecia" id="powodUsuniecia" class="txt" size="30" maxlength="260"/>

</div>
<ds:available test="file.encryption">
<div id="myDiv2" style="display:none">
		<ds:lang text="Podaj Klucz do za�acznika"/>:
        <input type="text" name="decryptKey" id="decryptKey" class="txt" size="30" maxlength="260"/>

</div>
</ds:available>
				<td colspan="8" align="right">
					<ww:if test="showBtnMarkAttachments">
						<ds:event value="getText('OznaczZalaczniki')" name="'doMarkAttachment'" disabled="readOnly"/>
					</ww:if>
					
					<ds:available test="usunZaznaczoneZalaczniki">
						<ds:available test="!deleteReason">
							<ds:event name="'doDelete'"  cssClass="'btn cancelBtn'" value="getText('Usun')" disabled="readOnly || blocked || !canDeleteAttachments"/>
						</ds:available>
					</ds:available>
					<ds:available test="file.encryption"> 
					<input type="button" class="btn" value="<ds:lang text="odszyfruj"/>" id="decrypt" name="decrypt" />
					</ds:available>
					
					<ds:available test="deleteReason">
						<input type="button" class="btn" <ww:if test="czyJestPodpisany">disabled="disabled"</ww:if>  value="<ds:lang text="usun"/>" id="usun" name="usun" />
					</ds:available>
					<ds:available test="zalaczniki.UtworzDokumentZzaznaczonychZalacznikow">
						<ww:if test="canCreateDocFromAtt">
							<ds:event name="'doCreateNewDocument'" value="getText('UtworzDokumentZzaznaczonychZalacznikow')"/>
						</ww:if>
					</ds:available>
					<script type="text/javascript">
					
					
					
						var selectedAtts = 0;
					
						var count = $j('input[name=count]'); 
			
						function updateDeleteButtonText(c) {
							if(c > 0) {
								var oldStr = count.val();
								var newStr = oldStr.replace(/\([0-9]*\)/, '(' + c + ')');
			
								/* jesli wczesniej nie by�o nawias�w */
								if(oldStr == newStr) {
									newStr = oldStr + ' (' + c + ')'; 
								}
			
								count.val(newStr);
							} else if(c == 0) {
								var oldStr = count.val();
								var newStr = oldStr.replace(/ \([0-9]*\)/, '');
							
								count.val(newStr);
								
							}
						}
			
						function incSelectedAtts() {
							selectedAtts ++;
			
							updateDeleteButtonText(selectedAtts);
						}

						function decSelectedAtts() {
							selectedAtts --;

							if(selectedAtts < 0)
								selectedAtts = 0;
							else
								updateDeleteButtonText(selectedAtts);
						}

					
						
						function countSelectedAtts(sender) {
							
							if(sender.checked) 
								incSelectedAtts(); 
							else 
								decSelectedAtts();
						}
						 jQuery(document).ready( function(){ 
							
							 jQuery("#usun").click( showDialog );
								
					            jQuery(":checkbox.styled").change(function(){ countSelectedAtts(this) });
					            
					        	$myWindow = jQuery('#myDiv');
					    	
					        	$myWindow.dialog({ height: 350,
					           		width: 400,
					           		modal: true,
					           		position: 'center',
					           		autoOpen:false,
					           	 buttons:
					                 [
					                     {
					                         text: '<ds:lang text="Usun"/>',
					                         click: function()
					                         {
					                             var tmpObjective;
					                             if($j('#powodUsuniecia').val().length > 0) {
					                                 tmpObjective = $j('#powodUsuniecia').val();
					                             } 
					                            
					                             $j('#test').append('<input type="hidden" name="powodUsuniecia" value="'+$j('#powodUsuniecia').val()+'"/>');  
					                             $j('input[name=doDelete]').val(true); 
					                             $j('form').submit();
					                             $myWindow.dialog('close');
					                         }	
					                     }
					                     
					                 ]
					                 });
						 
						   	}
						
					        );
					   
						 
						 jQuery(document).ready( function(){ 
								
							 jQuery("#decrypt").click( showDialog2 );

					         
					            
					        
					        	$myWindow2 = jQuery('#myDiv2');
					    	
					        	$myWindow2.dialog({ height: 150,
					           		width: 400,
					           		modal: true,
					           		position: 'center',
					           		autoOpen:false,
					           	 buttons:
					                 [
					                     {
					                         text: 'odszyfruj i pobierz',
					                         click: function()
					                         {
					                             var temp;
					                             if($j('#decryptKey').val().length > 0) {
					                            	 temp = $j('#decryptKey').val();
					                             } 
					                            
					                             $j('#test').append('<input type="hidden" name="decryptKey" value="'+$j('#decryptKey').val()+'"/>');  
					                             $j('#doDecrypt').val() ==true;
					                             $j('#doDlete').val() ==false;
					                             $j('input[name=doDecrypt]').val(true);
					                             $j('form').submit();
					                            
					                             $myWindow2.dialog('close');
					                         }	
					                     }
					                     
					                 ]
					                 });
						 
						   	}
						 )
					    var showDialog = function() {
					   
					    	$myWindow.show();
					    	$myWindow.dialog("open");
					    	}

					    var closeDialog = function() {
					    	$myWindow.dialog("close");
					    }
					    var showDialog2 = function() {
							   
					    	$myWindow2.show();
					    	$myWindow2.dialog("open");
					    	}

					    var closeDialog2 = function() {
					    	$myWindow2.dialog("close");
					    }
					</script>
				</td>
			</tr>
		</table>
		<%--	<br>	--%>
	</ww:elseif>

	<hr class="highlightedText" style="width:10%; height:1; size:1;" align="left"/>
	
	<ww:if test="showAddAttachments">
		<span id="ukryj_tekst" style="display:;" class="btnContainer topSpacer">
			<ww:if test="!attachmentBeans.empty">
				<%-- <ds:lang text="AbyDodacKolejnyZalacznik"/> --%>
				<input type="button" class="btn" value="<ds:lang text="DodajKolejnyZalacznik" />" onclick="nowy_zal()" />
			</ww:if>
			<ww:else>
				<%-- <ds:lang text="AbyDodacZalacznik"/> --%>
				<input type="button" class="btn" value="<ds:lang text="DodajZalacznik" />" onclick="nowy_zal()" >
			</ww:else>
			<%--
			<a href="javascript:nowy_zal()">
			<ds:lang text="KliknijTutaj"/></a>
			 --%>
			 
		</span>
	</ww:if>

	<span id="ukryj_zalacznik" style="display:none;">
		<ww:if test="attachment == null">
			<%--	<p></p>	--%>
			<h4><ds:lang text="NowyZalacznik"/></h4>
			<ds:lang text="AbyDodacNowyZalacznik"/>
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><ds:lang text="Tytul"/><span class="star">*</span>:</td>
					<td><ww:textfield name="'title'" id="title" size="50" maxlength="254" cssClass="'txt dontFixWidth'"/></td>
				</tr>
				<%--	<tr>
					<td>Kod kreskowy: </td>
					<td><ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'"/></td>
				</tr>	--%>
				<tr>
					<td><ds:lang text="Plik"/><span class="star">*</span>:</td>
					<td><ww:file name="'file'" id="file" size="50" cssClass="'txt dontFixWidth'"/></td>
				</tr>
				<ds:available test="file.encryption">
				<tr>
					<td><ds:lang text="attachment.encryption"/>:</td>
					<td><ww:checkbox id="encryption" name="'encryption'" fieldValue="true" onchange="'isEncryption()'"/></td> </tr>
				<tr><td><ds:lang text="attachment.publickey"/>:</td>
					<td><ww:textfield name="'publicKey'" id="publicKey" size="50" maxlength="254" cssClass="'txt dontFixWidth'" disabled="true"/></td>
				</tr>
				</ds:available>
				<%--	<c:if test="${!empty requiredAttachments}">
					<tr>
						<td colspan="2">Jest to jeden z wymaganych za��cznik�w:
							<table>
							<c:forEach items="${requiredAttachments}" var="bean">
								<tr>
									<td>
										<c:out value="${bean.title}"/>
									</td>
									<td>
										<c:if test="${!bean.present}"><input type="radio" name="posn" value='<c:out value="${bean.posn}"/>'/></c:if>
										<c:if test="${bean.present}">posiadany</c:if>
									</td>
								</tr>
							</c:forEach>
							</table>
						</td>
					</tr>
				</c:if>	--%>
				<tr>
					<td></td>
					<td>
					<ww:hidden id="attachmentMaxSize" name="'attachmentMaxSize'" value="attachmentMaxSize"/>
						<ds:event name="'doAdd'" value="getText('Utworz')" disabled="readOnly || blocked || czyJestPodpisany"
							onclick="'if (!validateForm2()) return false'"/>

						<ww:if test="useScanner">
							<script type="text/javascript">
								var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
									request.getServerName() + ":" + request.getServerPort() %>';
							</script>
							<input type="button" value="Otw�rz applet skanowania" class="btn" <ww:if test="blocked">disabled="disabled"</ww:if>
								onclick="openToolWindow('<ww:url value="'/office/incoming/attachments-applet.action'"/>?openerUrl='+encodeURIComponent(host+'<ww:url><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>')+'&documentId=<ww:property value="documentId"/>&activity='+encodeURIComponent('<ww:property value="activity"/>'), 'applet', 700, 125)"/>
						</ww:if>
					</td>
				</tr>
				<ww:if test="editorOn">
					<tr>
						<td></td>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<ds:event name="'doInitAddWithEditor'" value="getText('UtworzZaPomocaEdytora')" disabled="readOnly || blocked || czyJestPodpisany"/>
						</td>
					</tr>
				</ww:if>
				<tr>
					<td>
						<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
					</td>
				</tr>
			</table>

			<%--	<ww:if test="useScanner">
				<p></p>
				
				<table cellspacing="0" cellpadding="0" style="border: 1px solid black; padding: 2px">
					<tr>
						<td>
							Skanowanie
						</td>
					</tr>
					<tr>
						<td>
							<script>
								var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
									request.getServerName() + ":" + request.getServerPort() %>';
								// u�yty mo�e by� albo plik jtwain.jar znajduj�cy si� w aplikacji,
								// albo plik systemowy (w JRE_HOME/lib/ext)
								var object =  appletObject('scanapplet',
									'pl.compan.docusafe.applets.scanner.AttachmentsApplet.class',
									'<ww:url value="'/scanner.jar'"/>, <ww:if test="!useSystemJTwain"><ww:url value="'/jtwain-8.2.jar'"/>, </ww:if> <ww:url value="'/jai_core-1.1.2_01.jar'"/>, <ww:url value="'/jai_codec-1.1.2_01.jar'"/>', 700, 55,
									{ 'uploadUrl' : host+'<ww:url value="baseLink"/>;jsessionid=<%= request.getRequestedSessionId() %>',
									'refreshUrl' : host+'<ww:url><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>',
									'reloadPage' : 'true',
									'activity' : '<ww:property value="activity"/>',
									'documentId' : '<ww:property value="documentId"/>' },
									new Array('xMSIE'));
								//alert('object='+object);
								document.write(object);
							</script>
						</td>
					</tr>
				</table>
			</ww:if>	--%>
		</ww:if>
    </span>
    
    <ww:if test="documentType == 'in' and !attachmentBeans.empty and attachment == null">
		<%--	
		<ds:lang text="AbyPrzekonwertowacZalacznikidoTIFF"/>
		<a href="<ww:url value="'attachments-to-tiff.action'"><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>">
		<ds:lang text="KliknijTutaj"/></a>
		--%>
		<ds:available test="zalaczniki.wlacz.PrzekonwertujZalacznikidoTIFF">
			<input type="button" class="btn" value="<ds:lang text="PrzekonwertujZalacznikidoTIFF"/>"
			onclick="location.href='<ww:url value="'attachments-to-tiff.action'"><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>'"
			 />
		</ds:available>
	</ww:if>
    
    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>


<script>

// funkcja wy�wietlania i chowania pola za��cznika
function nowy_zal()
{
	var zalacznik = document.getElementById('ukryj_zalacznik');
	var tekst = document.getElementById('ukryj_tekst');
		
	tekst.style.display = 'none';
	zalacznik.style.display = '';
	
}
	
function validateForm1()
{
    var title = document.getElementById('title').value;
    //var revisionFile = document.getElementById('revisionFile').value;
    if (title.trim().length == 0) { alert('<ds:lang text="NiePodanoTytulu"/>'); return false; }
    //else if (revisionFile.trim().length == 0) { alert('<ds:lang text="NieWybranoPliku"/>'); return false; }
    return true;
}

function validateForm3()
{
    //var title = document.getElementById('title').value;
    var revisionFile = document.getElementById('revisionFile').value;
    //if (title.trim().length == 0) { alert('<ds:lang text="NiePodanoTytulu"/>'); return false; }
    if (revisionFile.trim().length == 0) { alert('<ds:lang text="NieWybranoPliku"/>'); return false; }
    return true;
}

function validateForm2()
{
    <ds:available test="attachment.validate.size">
        var attachmentMaxSize = document.getElementById('attachmentMaxSize');
        var validateFile =  document.getElementById('file');
        var uploadedFile = validateFile.files[0];

        if (uploadedFile.size > attachmentMaxSize.value) { alert('<ds:lang text="MaxSizeFileUpload"/>'); return false; }
    </ds:available>

    var title = document.getElementById('title').value;
    var file = document.getElementById('file').value;

    if (title.trim().length == 0) { alert('<ds:lang text="NiePodanoTytulu"/>'); return false; }
    else if (file.trim().length == 0) { alert('<ds:lang text="NiWybranoPliku"/>'); return false; }

    return true;
}

function isEncryption() {
	if(document.getElementById("encryption").checked)
	{
		document.getElementById("publicKey").disabled = false;
	}
	else {
		document.getElementById("publicKey").disabled = true;
	}
}

<%--
function reloadPage(url)
{
    //alert('reloadPage '+url);
    document.location.href = url;
}
--%>
</script>
