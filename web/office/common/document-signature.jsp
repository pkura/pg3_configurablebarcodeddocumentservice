<%@ page contentType="text/html; charset=cp1250" %>
<%@ page import="pl.compan.docusafe.web.settings.SignatureAction" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="doSign">
	<object
	   classid="clsid:D484F478-2A45-419C-9CFF-8462A39177D9"
	   ID="MyControl" 
	   VIEWASTEXT>
	</object>

   <object id="SDoc" classid="clsid:38552364-0433-4F7F-910C-7925957Ef3A1" width=20 height=10>
      <param name="UploadScript" value="up.php">
   </object><br>

  <%--  <script type="text/javascript">
        var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
            request.getServerName() + ":" + request.getServerPort() %>';
        var object =  appletObject('SigApplet',
            'pl.compan.docusafe.applets.signature.SignatureApplet.class',
            '<ww:url value="'/signature.jar'"/>', 300, 10,
            { 'uploadUrl' : host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>',
              'downloadUrl' : host+'<ww:url value="'/office/common/document-signature.action'"/>'+';jsessionid=<%= request.getRequestedSessionId() %>'+'?doDownload=true&inFile='+escape('<ww:property value="inFile"/>'),
              'srcFile' : '<ww:property value="inFile"/>'
              },
            new Array('xMSIE'));
        document.write(object);
    </script>
      --%>
    <p id="signatureError" style="color:red">&nbsp;</p>
	<input type="button" value="<ds:lang text="Zamknij"/>" onclick="window.close();" class="btn"/>
<%--	<input name="b1" type="button" value="Sign" onclick="signFile()"><br>

    <form action="<ww:url value="'/accept-upload.action'"/>" method="POST" ENCTYPE="multipart/form-data">
   <input type="file" name="file"/><br/>
    <input type="hidden" name="targetFilename" value="C:\\download\\pomoce_michal\\a.tmp"/><br/>
    <input type="hidden" name="doUploadToFile" value="true"/>
   <input type="submit" value="Wy�lij plik"/>
  </form>  --%>

    <script>
        function signFile()
           {
              var host = '<%= (request.isSecure() ? "https://" : "http://" ) +
                request.getServerName() + ":" + request.getServerPort() %>';
              var uploadUrl = host+'<ww:url value="'/accept-upload.action'"/>;jsessionid=<%= request.getRequestedSessionId() %>';
              var downloadUrl = host+'<ww:url value="'/office/common/document-signature.action'"/>'+';jsessionid=<%= request.getRequestedSessionId() %>'+'?doDownload=true&inFile='+escape('<ww:property value="inFile"/>');
               <ww:if test="useSignatureDebuger">
                   alert("JS -> downURL : "+downloadUrl);
                   alert("JS -> upURL : "+uploadUrl);
               </ww:if>
               
              var locaFileName;
              localFileName = SDoc.DownloadFile(downloadUrl);              
              //"http://www.k-software.com/test/test.pdf");
              <ww:if test="useSignatureDebug">
                alert("JS -> sciaga "+localFileName+" z "+downloadUrl);
              </ww:if>  

              var res = MyControl.protectFile(localFileName,localFileName+'.pem',2,'');
              <ww:if test="useSignatureDebug">
               alert("JS -> metoda protect file dla "+localFileName+" daje rezultat : "+res);
              </ww:if>
              if (res == 0) {
                res = SDoc.UploadFile(localFileName+'.pem',uploadUrl,'<ww:property value="outFile"/>');
              <ww:if test="useSignatureDebug">
               alert("JS -> plik "+localFileName+" wyslano na "+uploadUrl+" z nazwa <ww:property value="outFile"/> i rezultatem: "+res);
              </ww:if>
                document.getElementById('doSave').value='true';
				document.forms[0].submit();

                SDoc.DeleteLocalFile(localFileName+'.pem');
              <ww:if test="useSignatureDebug">  
                alert("JS -> usowa plik lokalny "+localFileName+".pem");
              </ww:if>
              }
              SDoc.DeleteLocalFile(localFileName);

           }
        function sign()	{
            applet = document.SigApplet;
            alert("JS -> wywoluje download z apletu");
            var pdfFile = applet.download();
			alert("JS -> podpisujemy "+pdfFile);
            //var res = MyControl.signFileDetach(pdfFile,pdfFile+'.sig',0);
            var res = MyControl.protectFile(pdfFile,pdfFile+'.pem',2,'');
            if (res == 0) {
                applet.upload(pdfFile+'.pem','<ww:property value="outFile"/>');
                <%-- EXCLUDED BY ALERT    --%>
                document.getElementById('doSave').value='true';
				document.forms[0].submit();
				<%-- --%>
                <%-- BEGIN ALERT
                document.getElementById('signTable').style.display = 'none';
                document.getElementById('saveTable').style.display = '';
                 END ALERT --%>

            }
			else {
				document.getElementById('signatureError').firstChild.nodeValue = '<ds:lang text="PodpisDokumentuZakonczonyNiepowodzeniemBlad"/> '+res+')';
                <%-- BEGIN ALERT
                document.getElementById('signTable').style.display = 'none';
                 END ALERT --%>
            }
            applet.deleteFiles();
            <!-- alert(res); -->
		}
	</script>
	
	<form id="signForm" action="<ww:url value="'/office/common/document-signature.action'"/>" method="post" onsubmit="document.getElementById('doSave').value='true';" enctype="multipart/form-data">
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'inFile'" value="inFile"/>
		<ww:hidden name="'outFile'" value="outFile"/>
		<ww:hidden name="'isOffice'" value="isOffice"/>
        <ww:hidden name="'xml'" value="xml"/>
        <input type="hidden" name="doSave" id="doSave"/>

        <%-- BEGIN ALERT
        <table id="signTable">
            <tr>
                <td>Podaj nazw� pliku do podpisu</td>
            </tr>
            <tr>
                <td><ww:textfield name="'filename'" cssClass="'txt'" size="40" id="filename"/></td>
            </tr>
            <tr>
                <td>
                    <input type="button" id="podpisz" value="Podpisz" class="btn" onclick="sign(document.getElementById('filename').value,document.getElementById('filename').value+'.sig');" />
                </td>
            </tr>
        </table>

        <table id="saveTable" style="display:none">
            <tr>
                <td>Podaj wygenerowany podpis</td>
            </tr>
            <tr>
                <td><ww:file name="'signatureFile'" cssClass="'txt'" size="40" id="signatureFile" /></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" id="zapisz" value="Zapisz" class="btn" onclick="document.getElementById('doSave').value='true';"/>
                </td>
            </tr>
        </table>



         END ALERT --%>
    </form>
	
	<script>
		<%-- alert('<ww:property value="inFile"/>');
		alert('<ww:property value="outFile"/>');  --%>

        <%-- EXCLUDED BY ALERT
        sign('<ww:property value="inFile"/>','<ww:property value="outFile"/>');
         --%>
        signFile();
    </script>

</ww:if>
<ww:else>
	<p><ds:lang text="PodpisDokumentuZakonczylSieSukcesem"/></p>
	<script>
		window.opener.location.reload(true);
	</script>
	<input type="button" value="<ds:lang text="Zamknij"/>" onclick="window.close();" class="btn"/>
</ww:else>