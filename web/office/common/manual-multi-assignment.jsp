<%--
	R�czna dekretacja dla JBPM4
--%>
<!--N manual-assignment.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<div id="promptDiv" style="overflow-y: auto; overflow-x: hidden; max-height: 200px;"></div>
<div id="senderData" style="display: none"></div>
<div id="recipientData" style="display: none"></div>
<div id="divisiontData" style="display: none"></div>

<style type="text/css">
	select#jbpmProcessId {width:22em;}
</style>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<div id="assignment_dialog">
<p>
    <ds:lang text="DekretacjaNa"/>: <span  id="dekretacjaNa"></span>
</p>


<p>
    <ds:lang text="RodzajDekretacji"/>: <br/>
    <ww:if test="canAssignToExecute">
    <span><input id="EXECUTE" checked="checked" type="radio" name="kind" value="EXECUTE" /> <ds:lang text="doRealizacji"/></span> <br/>
    </ww:if>
    <span><input id="CC" type="radio" name="kind" value="CC" /> <ds:lang text="doWiadomosci"/></span> <br/>
    <ds:available test="doKonsultacjiDW">
	<span><input id="CCK" type="radio" name="kind" value="CCK" /> <ds:lang text="doKonsultacji"/></span> <br/>
	</ds:available> 
	<ds:available test="doRealizacjiWiodacej">
	<span><input id="CCK" type="radio" name="kind" value="EXECUTE_W" /> <ds:lang text="doRealizacjiWiodacej"/></span> <br/>
	</ds:available> 
    
</p>

<ww:if test="canSetDeadline">
    <p>
        <ds:lang text="Termin"/>:
        <ww:textfield name="'day'" size="10" cssClass="'txt'" id="day"/><img src="<ww:url value="'/calendar096/img.gif'"/>" id="dayTrigger" style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
    </p>
</ww:if>
 <ww:if test="canSetClerk">
	<p>
	    <ds:lang text="Referent"/>:
	    <ww:select name="'clerksSel'" list="clerksList" cssClass="'sel'" id="clerksSel" listKey="name" listValue="asLastnameFirstname()" headerKey="''" headerValue="getText('select.wybierz')" value="clerksSel" />
	</p>
</ww:if>
<p>
    <ds:lang text="CelDekretacji"/>:
    <ww:select name="'objectiveSel'" list="objectivesList" cssClass="'sel'" id="objectiveSel" listKey="name" listValue="name" headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
    <br/>
    <ds:lang text="lubWpisz"/>:
    <ds:available test="dekretacja.wlacz.celDekretacji">
    	<ww:textarea name="'objectiveTxt'" id="objectiveTxt" rows="2" cols="70" cssClass="'txt dontFixWidth'" cssStyle="'sel'"/>
   </ds:available>
   <ds:available test="!dekretacja.wlacz.celDekretacji">
        <input type="text" name="objectiveTxt" id="objectiveTxt" class="txt" size="70" maxlength="200" onkeypress="if (!isEmpty(this.value)) { document.getElementById('objectiveSel').options.selectedIndex=0 };"/>
   </ds:available>
<ds:available test="ITWL">
<p>
    <ds:lang text="Przeka� zadanie i pozostaw Pismo na swojej li�cie zada�"/>: 
    <ww:checkbox name="'stayOnTaskList'" value="stayOnTaskList" fieldValue="true" id="stayOnTaskList"></ww:checkbox>
</p>	
</ds:available>

</p>

</div>


<script type="text/javascript">
        Calendar.setup({
            inputField	:	"day",
            ifFormat	:	"<%= DateUtils.jsCalendarDateFormat %>",
            button		:	"dayTrigger",
            align		:	"Tl",
            singleClick	:	true
        });

        var executeUsedMsg = '<ds:lang text="BlokadaDekretacjiDoRealizacjiNaWieluOdbiorcow"/>';

        <ww:if test="canSetClerk">
        $j('#EXECUTE').click( function( event ) { $j('#clerkSpan').show(); });
        $j('#CC').click( function( event ) { $j('#clerkSpan').hide(); });
        </ww:if>

        $j('#assignment_dialog').dialog({autoOpen:false,modal:true, width: 600, position: 'center',
        buttons:
        [
            {
                text: '<ds:lang text="Dekretuj"/>',
                click: function()
                {
                    <ww:if test="canMultiExec">
                    if($j("input[@name=kind]:checked").attr('value') == 'EXECUTE' && $j('[name=kinds][value*=EXECUTE]').length > 0) {
                        alert(executeUsedMsg);
                        return;
                    }
                    </ww:if>
                    var tmpObjective;
                    if($j('#objectiveTxt').val().length > 0) {
                        tmpObjective = $j('#objectiveTxt').val();
                    } else {
                        tmpObjective = $j('#objectiveSel option:selected').val();
                    }
                    <ww:if test="canSetClerk">
					    var tmpclerk;
					    tmpclerk = $j('#clerksSel option:selected').val();
					</ww:if>
					
                    var radios = document.getElementsByName("kind");
                    var kindValue;
                    for (var i = 0; i < radios.length; i++) {       
                        if (radios[i].checked && radios[i].value != 'true') {
                            kindValue = radios[i].value;
                        }
                    }
                    $j('#planedAssignmentTable').append('<tr><ww:if test="canSetDeadline"><input type="hidden" name="deadlines" value="'+$j('#day').val()+'"/></ww:if><input type="hidden" name="targetsKind" value="'+resourceType+'"/><input type="hidden" name="targets" value="'+resource+'"/><input type="hidden" name="kinds" value="'+kindValue+'"/><input type="hidden" name="objectives" value="'+tmpObjective+'"/><ww:if test="canSetClerk"><input type="hidden" name="clerks" value="'+tmpclerk+'"/></ww:if><td>'+$j('#dekretacjaNa').text()+'</td><td>'+$j("input[@name=kind]:checked").parent().text()+'</td><td>'+tmpObjective+'</td><ww:if test="canSetClerk"><td>'+tmpclerk+'</td></ww:if><ww:if test="canSetDeadline"><td>'+$j('#day').val()+'</td></ww:if><td><a href="#" onClick="lockAssignmentButton(this); "><ds:lang text="usun"/></a></td></tr>');
                    <ww:if test="canSetClerk">
                    $j('#clerk').val($j('#clerkSel option:selected').val());
                    </ww:if>
                    $j('input[name=doAssignButton]')[0].disabled=false;
                    $j('#doLastone').val(true);
                    $j('form').submit();
                }
            }
            ,
            {
                text: '<ds:lang text="Zapisz"/>',
                click: function()
                {
                    <ww:if test="canMultiExec">
                    if($j("input[@name=kind]:checked").attr('value') == 'EXECUTE' && $j('[name=kinds][value*=EXECUTE]').length > 0) {
                        alert(executeUsedMsg);
                        return;
                    }
                    </ww:if>
                    var tmpObjective;
                    if($j('#objectiveTxt').val().length > 0) {
                        tmpObjective = $j('#objectiveTxt').val();
                    } else {
                        tmpObjective = $j('#objectiveSel option:selected').val();
                    }
                    <ww:if test="canSetClerk">
	                    var tmpclerk;
	                    tmpclerk = $j('#clerksSel option:selected').val();
	                </ww:if>
                    <ww:if test="canSetClerk">
                    $j('#clerk').val($j('#clerkSel option:selected').val());
                    </ww:if>
                    
                    var radios = document.getElementsByName("kind");
                    var kindValue;
                    for (var i = 0; i < radios.length; i++) {       
                        if (radios[i].checked && radios[i].value != 'true') {
                            kindValue = radios[i].value;
                        }
                    }
                    $j('#planedAssignmentTable').append('<tr><ww:if test="canSetDeadline"><input type="hidden" name="deadlines" value="'+$j('#day').val()+'"/></ww:if><input type="hidden" name="targetsKind" value="'+resourceType+'"/><input type="hidden" name="targets" value="'+resource+'"/><input type="hidden" name="kinds" value="'+kindValue+'"/><input type="hidden" name="objectives" value="'+tmpObjective+'"/><ww:if test="canSetClerk"><input type="hidden" name="clerks" value="'+tmpclerk+'"/></ww:if><td>'+$j('#dekretacjaNa').text()+'</td><td>'+$j("input[@name=kind]:checked").parent().text()+'</td><td>'+tmpObjective+'</td><ww:if test="canSetClerk"><td>'+tmpclerk+'</td></ww:if><ww:if test="canSetDeadline"><td>'+$j('#day').val()+'</td></ww:if><td><a href="#" onClick="lockAssignmentButton(this); "><ds:lang text="usun"/></a></td></tr>');
                    $j('#assignment_dialog').dialog('close');
                    $j('input[name=doAssignButton]')[0].disabled=false;
                    //czyszczenie zawartosci okienek
                    clearRecipientInputs();
                }
            }
        ]
        });

        var resource;
        var resourceType;

        function openDialog(name, res, type)
        {
            resource = res;
            resourceType = type;
            $j('#dekretacjaNa').text(name);
            $j('#assignment_dialog').dialog('open');
        }

        function openDialogForUsr()
    	{
        	sender = senders[selectedItem];

        	// omini�cie problem litery "�"
        	jqTmpDiv = $j(document.createElement("div"));
            jqTmpDiv.html(sender.recipientLastname);
            var lastname = jqTmpDiv.html();
            jqTmpDiv.html(sender.recipientFirstname);
            var firstname = jqTmpDiv.html();
            jqTmpDiv.remove();

		    openDialog(firstname + " " + lastname, sender.recipientName , 'U');
    	}
        
        function openDialogForDiv()
    	{
        	//var textDiv = document.getElementById('recipientDivision');
            sender = senders[selectedItem];

            // omini�cie problem litery "�"
            jqTmpDiv = $j(document.createElement("div"));
            jqTmpDiv.html(sender.recipientDivisionName);
            var divisionName = jqTmpDiv.html();
            jqTmpDiv.remove();

    		openDialog(divisionName, sender.recipientDivision , 'D');
    	}
        
        
        
        function selectUser(username){
            $j('#selectedUsers option[value="'+username+'"]').attr('selected','selected');
        }
        function selectDivision(guid){
            $j('#selectedGuids option[value="'+guid+'"]').attr('selected','selected');
        }
        function expandDivision(guid){

            var sd = $j('#selectedGuid').get(0);
            sd.value = guid;
            sd.form.submit();
        }

        $j(document).ready(function() {

            var elem = document.getElementById("recipientDivision");
            var selectedDivisionOnTree = document.getElementById("selectedDivisionOnTree");
            elem.value = selectedDivisionOnTree.value;

            if($j('#planedAssignmentTable tr').size() === 1) {
                $j('input[name=doAssignButton]')[0].disabled=true;
            }

			$j('#recipientLastname').attr('autocomplete', 'off');

			$j('#recipientLastname').keypress(function(event) {
				if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
					putItem(selectedItem);
					openDialogForUsr();
					$j('#form').bind('submit', function() {
						return false;
					});
					dontSendForm = true;
					return false;
				}

				if(dontSendForm) {
					dontSendForm = false;
					$j('#form').unbind('submit');
				}

				return true;
			});

			$j('#recipientDivision').keypress(function(event) {
				if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
					putItem(selectedItem);
					openDialogForDiv();
					$j('#form').bind('submit', function() {
						return false;
					});
					dontSendForm = false;

					return false;
				}

				if(dontSendForm) {
					dontSendForm = false;
					$j('#form').unbind('submit');
				}

				return true;
			});
		});

        function writeUsername2Inputs(src, delimiter, firstnameId, lastnameId) {

			var jqSel = $j(src);

			var selIndex = jqSel.attr('selectedIndex');

			var names  = jqSel.children()[selIndex].text.split(delimiter);

			try {
				$j('#' + lastnameId).val(names[0]);
				$j('#' + firstnameId).val(names[1]);
			} catch(e) {}
		}

        function showMatchingRecipient(e, caller){
			pressedKey = e.keyCode;

			var ln = caller;
			var pd = $j('#promptDiv');
			var str = '';


			if((ln.attr('value').trim().length >= 1) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
				if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
					pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\"/>";

					traceStart('promptDiv position');
					trace('x: ' + getPos(ln.get(0)).left);
					trace('y: ' + getPos(ln.get(0)).top);
					traceEnd();


					pd.css('left', getPos(ln.get(0)).left  + 'px');
					pd.css('top', getPos(ln.get(0)).top + ln.attr('offsetHeight') + 'px');
					pd.css('visibility', 'visible').css('display', 'inline');
				} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
					trace('Only navigation (without sending ajax request)');
					displayPrompt(senders, ln.attr('id'), hiddenItems);
					return;
				}

				trace('Sending request to recipient-lookup.action ...');
				$j.post("recipient-extension-lookup.action",
					{
						lastname:getInputValue('recipientLastname').trim(),
						firstname:getInputValue('recipientFirstname').trim(),
						division:getInputValue('recipientDivision').trim()
						
					},
					 function(data){

						$j("#recipientData").html(data.toString());
						//alert(data);
						senders = eval(data);
						var docWidth = $j(document).width();
						displayPrompt(senders, ln.attr('id'), hiddenItems);
						fixPromptDivPos(docWidth, pd);
						trace('Request recipient-lookup.action completed and prompt shown');
						traceEnd();

					});
			} else {
				pd.css('visibility', 'hidden').css('display', 'none');
			}

		}

        function showMatchingDivision(e, caller){
			pressedKey = e.keyCode;

			var ln = caller;
			var pd = $j('#promptDiv');
			var str = '';


			if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
				if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
					pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\"/>";

					traceStart('promptDiv position');
					trace('x: ' + getPos(ln.get(0)).left);
					trace('y: ' + getPos(ln.get(0)).top);
					traceEnd();


					pd.css('left', getPos(ln.get(0)).left  + 'px');
					pd.css('top', getPos(ln.get(0)).top + ln.attr('offsetHeight') + 'px');
					pd.css('visibility', 'visible').css('display', 'inline');
				} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
					trace('Only navigation (without sending ajax request)');
					displayPrompt(senders, ln.attr('id'), hiddenItems);
					return;
				}

				trace('Sending request to division-lookup.action ...');
				$j.post("division-lookup.action",
					{
						
						name:getInputValue('recipientDivision').trim(),
						firstname:getInputValue('recipientFirstname').trim(),
						lastname:getInputValue('recipientLastname').trim(),
					},
					 function(data){

						$j("#divisiontData").html(data.toString());
						//alert(data);
						senders = eval(data);
						var docWidth = $j(document).width();
						displayPrompt(senders, ln.attr('id'), hiddenItems);
						fixPromptDivPos(docWidth, pd);
						trace('Request division-lookup.action completed and prompt shown');
						traceEnd();

					});
			} else {
				pd.css('visibility', 'hidden').css('display', 'none');
			}

		}

        function clearRecipientInputs() {
			var form_el = document.forms[0];
			traceStart('clearRecipientInputs()');

			for(i = 0; i < form_el.length; i ++) {
				if((form_el.elements[i].type == 'text') && (form_el.elements[i].id.indexOf('recipient') != -1)) {
					trace('Clearing item [ID] ' + form_el.elements[i].id);
					form_el.elements[i].value = '';
				}
			}

			traceEnd();
		}

        /* tych pol nie wyswietlamy w podpowiedzi, ale sa dostepne (display: none) */
		var hiddenItems = new Array('recipientDivision', 'recipientName', 'personZip',
				'personEmail', 'senderId');

		function highlightItem(id) {
			/* usuwamy stare pod�wietlenie */
			$j('#prompt_item_' + selectedItem).removeClass('selected');

			$j('#' + id).addClass('selected');

			/* wyciagamy numer */
			selectedItem = parseInt((id).substr(12));
		}

		function unHighlightItem(id) {
			$j('#' + id).removeClass('selected');

			/* podswietlenie aktualnego zostaje */
			$j('#prompt_item_' + selectedItem).addClass('selected');
		}

		/* poprawia pozycje promptDiv, zeby miescil sie w ekranie przegladarki */
		function fixPromptDivPos(docWidth, pd) {
			traceStart('fixPromptDivPos()');

			var rightPos = pd[0].offsetLeft + pd[0].scrollWidth;

			/* mozilla nie liczy scrollbara */
			if($j.browser.mozilla && pd[0].scrollHeight)
				rightPos += 15;

			trace('docWidth: ' + docWidth);
			trace('rightPos: ' + rightPos);

			//alert('posX: ' + docWidth + ', ' + rightPos + ', ' + (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px');

			if(rightPos > docWidth) {

				trace('Changing leftPos');
				pd.css('left', (pd[0].offsetLeft - (rightPos - docWidth + 5)) + 'px');
			}

			traceEnd();
		}

		/* wyswietla podpowiedzi dla nazwiska - sprawdza czy pole nie jest puste - wtedy podpowiedzi nie wyswietla */
		var selectedItem = 0;
		var KEY_DOWN = 40;
		var KEY_UP = 38;
		var KEY_ENTER = 13;
		var KEY_ESCAPE = 27;
		var pressedKey = 0;
		var itemsLength = 0;
		var senders = null;
		var ie = document.all && !window.opera;
		var dontSendForm = false;

		function putItem() {
			sender = senders[selectedItem];

			if(sender) {
				var c = 0;
				for(property in sender){
					var jq_sel = '#prompt_item_' + selectedItem + ' td.' + property; // wybieramy komorke
					var val = '';

					 // pobieramy tekst z komorki
					val = $j(jq_sel).text();



					//zapisujemy do input text
					$j('#' + property).val(val);

				}

				$j('#promptDiv').css('visibility', 'hidden').css('display', 'none');

				/* odznaczamy checkbox dodaj do s�ownika */
				if(sender.personLastname) {
					$j('#createPerson').attr('checked', false);
					<ds:available test="layout2">
			        Custom.clear();
			        </ds:available>
				}

				/* zapisujemy dzial do multiselecta */
				var divInput = $j('#recipientDivision').get(0);



				setUTFInputVal($j('#senderAjaxError'), $j(divInput), sender.recipientDivisionName);


			}

		}

		function openDialogForUsr2(){
			var selectedUser = jQuery("#selectedUsers").val();

			if(selectedUser != null){
				var selectedUserName = jQuery("#selectedUsers option:selected").text();

			    openDialog(selectedUserName,selectedUser,'U');
			}
		}
</script>

<form action="<ww:url value="baseLink"/>" method="post" onsubmit="disableFormSubmits(this);" name="AssignmentForm">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'clerk'" value="clerk" id="clerk"/>
    <ww:hidden id="selectedGuid" name="'selectedGuid'" value="selectedGuid"/>

    <ww:hidden name="'doCc'" id="doCc"/>
    <ww:hidden name="'doAcknowledgeCc'" id="doAcknowledgeCc"/>
    <ww:hidden name="'doPlanAssignment'" id="doPlanAssignment"/>
    <ww:hidden name="'doDelete'" id="doDelete"/>
    <ww:hidden name="'doSave'" id="doSave"/>
    <ww:hidden name="'selectedDivisionOnTree'" id="selectedDivisionOnTree"/>
    <ww:iterator value="activityIds">
        <ww:hidden name="'activityIds'" value="[0].top"/>
    </ww:iterator>
    <ww:hidden name="'doLastone'" id="doLastone"/>
    
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table width="100%">

    <tr>
        <td>
			<div id="organizationTree">
				<ww:property value="treeHtml" escape="false"/>
			</div>
        </td>
    </tr>
</table>
<ds:available test="searchInManualMultiAssignment">
<table>
	<tr class="formTableDisabled">
		<td>Imi�:</td>
		<td>
			<ww:textfield id="recipientFirstname" name="'userFirstname'" cssClass="'txt dontFixWidth'" cssStyle="'width: 184px'"
			onkeyup="'showMatchingRecipient(event, $j(this));'" />
			<a href="javascript:clearRecipientInputs()" title="Wyczy�� pola" class="clearFieldsIco">
				<img src="<ww:url value="'/img/minus2.gif'"/>" />
			</a>
		</td>
	</tr>
	<tr class="formTableDisabled">
		<td>Nazwisko:</td>
		<td>
		<div>
			<ww:textfield id="recipientLastname" name="'userLastname'" cssClass="'txt dontFixWidth'" cssStyle="'width: 200px'"  
				onkeyup="'showMatchingRecipient(event, $j(this));'" />
			<a href="javascript:openDialogForUsr()" title="Dekretuj na u�ytkownika">
				<img src="<ww:url value="'/img/document-move.gif'"/>" />
			</a>
		</div>
		</td>
	</tr>
	<tr class="formTableDisabled">
		<td>Dzia�:</td>
		<td>
		<div>
			<ww:textfield id="recipientDivision" name="'userDivision'" cssClass="'txt dontFixWidth'" cssStyle="'width: 200px'"
				onkeyup="'showMatchingDivision(event, $j(this));'" />
			<a href="javascript:openDialogForDiv()" title="Dekretuj na dzia�">
				<img src="<ww:url value="'/img/document-move.gif'"/>" />
			</a>
		</div>
	</td>
	</tr>
	<ds:available test="listInManualMultiAssignment">
		<tr>
		    <td>
		        Lista u�ytkownik�w:
		    </td>
		</tr>
		<tr class="formTableDisabled">
		    <td>
		        <ww:select cssClass="'sel'" multiple="false" name="'selectedUsers'" list="usersList" listKey="name" listValue="asLastnameFirstname()" value="selectedUsers" id="selectedUsers" size="10"/>
		        <a href="javascript:openDialogForUsr2()" title="Przygotuj dekretacje">Przygotuj
		    		<img src="<ww:url value="'/img/document-move.gif'"/>" />
				</a>
		    </td>
		</tr>
	</ds:available>
</table>
</ds:available>
<table id="planedAssignmentTable">
    <tr>
        <th><ds:lang text="DekretacjaNa"/></th>
        <th><ds:lang text="RodzajDekretacji"/></th>
        <th><ds:lang text="CelDekretacji"/></th>
        <ww:if test="canSetClerk">
        	<th><ds:lang text="Referent"/></th>
        </ww:if>
        <ww:if test="canSetDeadline">
            <th><ds:lang text="Termin"/></th>
        </ww:if>
        <th></th>
    </tr>
    <ww:iterator value="planedAssignments">
        <tr>
            <ww:hidden name="'targetsKind'" value="targetKind"/>
            <ww:hidden name="'targets'" value="target"/>
            <ww:hidden name="'kinds'" value="kind"/>
            <ww:hidden name="'objectives'" value="objective"/>
            <ww:if test="canSetClerk">
            	<ww:hidden name="'clerks'" value="clerk"/>
            </ww:if>
            <ww:if test="canSetDeadline">
                <ww:hidden name="'deadlines'" value="deadline"/>
            </ww:if>
            <td><ww:property value="targetName"/></td>
            <td><ww:property value="kindName"/></td>
            <td><ww:property value="objective"/></td>
            <ww:if test="canSetClerk">
            	<td><ww:property value="clerk"/></td>
            </ww:if>
            <ww:if test="canSetDeadline">
                <td><ww:property value="deadline"/></td>
            </ww:if>
            <td><a href="#" onClick="lockAssignmentButton(this); "><ds:lang text="usun"/></a></td>
        </tr>
    </ww:iterator>
</table>
<ds:available test="mma.startNewProcess">
<p>
    <ds:lang text="PrzekazujacZadanieDoRealizacjiPozostawJeNaMojejLiscieRozpocznijNowyProcesRealizacji"/>:
    <ww:checkbox name="'startNewProcess'" fieldValue="true" value="startNewProcess"  />
</p>
</ds:available>
<ds:available test="saveAssignment">
    <ww:select name="'choosenPlanedAssignmentName'" id="savedPlanedAssignmentNameID" list="savedPLanedAssignments" value="choosenPlanedAssignmentName" cssClass="'sel'" headerValue="getText('select.wybierz')" headerKey="''" onchange="'document.forms.AssignmentForm.submit();'" />
</ds:available>
<ds:submit-event name="'doAssign'" value="'Wykonaj'"/>
<ds:available test="saveAssignment">
    <ds:lang text="savedPlanedAssignmentNameText"/>:
    <ww:textfield name="'savedPlanedAssignmentName'" size="20" maxlength="20" cssClass="'txt'" value="savedPlanedAssignmentName"/>
    <input type="submit" class="btn saveBtn" onclick="document.getElementById('doSave').value='true';" value="<ds:lang text="Zapisz"/>"/>
</ds:available>

<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

    <script type="text/javascript">

        function lockAssignmentButton(place) {
            $j(place).parent().parent().remove();

            if($j('#planedAssignmentTable tr').size() === 1) {
                $j('input[name=doAssignButton]')[0].disabled=true;
            }
        }

    </script>

</form>


