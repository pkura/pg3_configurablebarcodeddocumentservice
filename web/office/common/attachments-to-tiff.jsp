<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N attachments-to-tiff.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>
	

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" id="form" >

    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    
    <ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
	<p><a href="<ww:url value="'attachments.action'"><ww:param name="'documentId'" value="documentId"/><ww:param name="'activity'" value="activity"/></ww:url>"><ds:lang text="PowrotDoListyZalacznikow"/></a></p>

    <h3><ds:lang text="LaczeniePlikowGraficznychDoPlikuTIFF"/></h3>

    <p><ds:lang text="totiff.NaLiscieUmieszczono.."/>.</p>

    <table>
        <tr>
            <td rowspan="2">
                <ww:select name="''" id="attachmentsMap"
                           list="attachmentsMap" multiple="true" size="5"
                    cssClass="'multi_sel'"/>
            </td>
            <td valign="top"><a href="javascript:void(moveOption('attachmentsMap', -1, -1))"><img src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="11" height="11" border="0"/></a></td>
        </tr>
        <tr>
            <td valign="bottom"><a href="javascript:void(moveOption('attachmentsMap', -1, 1))"><img src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="11" height="11" border="0"/></a></td>
        </tr>
        <tr>
            <td colspan="2"><a href="javascript:void(removeOptions('attachmentsMap'))"><ds:lang text="usunZaznaczone"/></a>
                <br/><i><ds:lang text="naLisciePokazanoTylkoZalacznikiGraficzne"/></i>
            </td>
        </tr>
    </table>

    <p><input type="checkbox" name="deleteSources" value="true"> <ds:lang text="UsunUzyteZalacznikiPoPolaczeniu"/></p>

    <ds:event name="'doConcatenate'" value="getText('PolaczWybraneZalacznikiDoTIFF')"
        onclick="'if (!validateForm()) return false;'"/>
        
    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script type="text/javascript">
    function validateForm()
    {
        var form = E('form');
        var sel = E('attachmentsMap');
        if (sel.length == 0)
        {
            alert('<ds:lang text="ListaZalacznikowJestPusta"/>');
            return false;
        }
        for (var i=0; i < sel.length; i++)
        {
            var opt = sel.options[i].value;
            var hid = document.createElement('INPUT');
            hid.type = 'HIDDEN';
            hid.name = 'revisionIds';
            hid.value = sel.options[i].value;
            form.appendChild(hid);
        }
        return true;
        /*
        disableFormSubmits(form);
        E('doConcatenate').value = 'true';
        form.submit();
        */
    }
</script>
