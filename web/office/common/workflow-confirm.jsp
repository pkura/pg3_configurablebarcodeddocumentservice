<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="button" value="<ds:lang text="ListaZadan"/>" class="btn"
    onclick="document.location.href='<ww:url value="'/office/tasklist/current-user-task-list.action?tab='+documentType"/>';"
    style="margin-top: 30px;"/>
