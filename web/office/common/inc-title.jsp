<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N inc-title.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<ds:available test="document.title">
	<ww:if test="documentType == 'in'">
	    <h1><ds:lang text="PismoPrzychodzace" /></h1>
	</ww:if>
	<ww:elseif test="documentType == 'out'" >
	    <h1><ds:lang text="PismoWychodzace" /></h1>
	</ww:elseif>
	<ww:elseif test="documentType == 'internal'">
	    <h1><ds:lang text="PismoWewnetrzne" /></h1>
	</ww:elseif>
	<ww:elseif test="documentType == 'order'">
	    <h1><ds:lang text="Polecenie" /></h1>
	</ww:elseif>
	<ww:else>
	    <h1><ds:lang text="Pismo" /></h1>
	</ww:else>
</ds:available>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
