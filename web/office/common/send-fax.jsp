<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="WyslijFax"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/common/send-fax.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'id'" id="id" value="id"/>
	<table>
		<tr>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumerFaksu"/>
			</td>
		</tr>
		<tr>
			<td>
				<ww:textfield name="'numerTelefonu'" id="numerTelefonu" size="50" maxlength="50" cssClass="'txt'" value="numerTelefonu"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Tresc"/>
			</td>
		</tr>
		<tr>
			<td>
				<ww:textarea name="'opis'" id="opis" value="opis" cols="75" cssClass="'txt'" rows="5"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:event name="'doSend'" value="'Wyslij'"/>
			</td>
		</tr>
	</table>
</form>