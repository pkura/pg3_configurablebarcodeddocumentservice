<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="AkceptujDokument"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/office/common/document-accept.action'"/>" method="post">
<ww:hidden name="'id'" id="id" value="id"/>

<ww:if test="!signatureBeans.empty">
	<table>
		<tr><th><ds:lang text="Podpisy"/></th></tr>
	    <tr>
	        <th><ds:lang text="Autor"/></th>
	        <th><ds:lang text="Data"/></th>
	        <th><ds:lang text="Typ"/></th>
	        <th colspan="2" style="text-align:center"><ds:lang text="Pobierz"/>:</th>
	        <th></th>
	        <th></th>
	    </tr>
	    
	    <ww:iterator value="signatureBeans">
	    	<tr>
		    	<td>&nbsp;<ww:property value="author.asFirstnameLastname()"/></td>
		    	<td>&nbsp;<ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
		    	<td>&nbsp;<ww:property value="typeAsString"/></td>
	           	<ww:if test="type == null || type == 1">
	           		<td><a href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+id"/>"><ds:lang text="podpis"/></a></td>
	            	<td><a href="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doVerify=true&signatureId='+id+'&id='+documentId"/>', 'sign', 500, 400);"><ds:lang text="weryfikuj"/></a></td>
	            	<td><ww:if test="correctlyVerified != null && !correctlyVerified"><span style="color: red"><ds:lang text="bladWpodpisie"/></span></ww:if></td>
            	</ww:if>
            </tr>
	    </ww:iterator>
	</table>
</ww:if>
<table>
	<ww:if test="!accept">
	<tr>
		<td colspan="2">
			<ds:lang text="CzyChceszZaakceptowacDokument"/>
		</td>
	</tr>
	<tr>
		<td><ds:event name="'doAccept'" value="getText('Akceptuj')" confirm="getText('NaPewnoZaakceptowac')"/></td>
		<td><input type="button" name="close" value="<ds:lang text="Anuluj"/>" onclick="javascript:void(window.close());" class="btn cancelBtn"/></td>
	</tr>
	</ww:if>
	<ww:else>
		<tr>
			<td><input type="button" name="close" value="<ds:lang text="Zamknij"/>" onclick="doClose()" class="btn"/></td>
		</tr>
	</ww:else>
</table>
<script type="text/javascript">
	function doClose()
	{
		window.opener.document.forms[0].submit(); 
		window.close() ;
	}
</script>
</form>