<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N jbpm-summary.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<ww:if test="jbpmSummary != null && jbpmSummary.size() > 0">
	<table>
		<ww:iterator value="jbpmSummary">
			<tr>
				<td colspan="2">
					<b><ds:lang text="DokumentZnajdujeSieWprocesieWorkflow"/>:
					<ww:property value="processName"/></b>
				</td>
			</tr>
			<ww:iterator value="activeTasks" status="status">
				<tr>
					<td>
						<ww:if test="#status.first">
							<ds:lang text="AktywneZadania"/>:
						</ww:if>
					</td>
					<td>
						<b><ww:property value="key+' ('+value+')'"/></b>
					</td>
				</tr>
			</ww:iterator>
			<ww:if test="DOKAction != null">
				<tr>
					<td>
						<ds:lang text="Uwagi"/>:
					</td>
					<td>
						<b><ds:lang text="DOKzakonczylZadanieWybierajacOpcje"/>
						'<ww:property value="DOKAction"/>'</b>
					</td>
				</tr>
			</ww:if>
		</ww:iterator>
	</table>
</ww:if>
