<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N init-jbpm-process.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<ds:modules test="workflow">
    <ww:if test="!externalWorkflow && canInitJbpmProcess">
        <br/>
        <ww:select name="'jbpmProcessId'" list="jbpmProcesses"
            cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierzProces')"
            id="jbpmProcessId" value="jbpmProcessId"/>
        <input type="button" class="btn" value="<ds:lang text="InicjujProces"/>"
            onclick="initJbpmProcess()" />
    </ww:if>
    
    <script type="text/javascript">
        function initJbpmProcess()
        {
            var jbpmProcessId = document.getElementById('jbpmProcessId');
            if (jbpmProcessId && (!(parseInt(jbpmProcessId.value) > 0)))
            {
                alert('<ds:lang text="NieWybranoProcesuDoZainicjowania"/>');
                return false;
            }
            // alert(jbpmProcessId.value);
    
            var link = '<ww:url value="initJbpmLink"/>&jbpmProcessId='+jbpmProcessId.value;
    
            document.location.href = link;
        }
    </script>
</ds:modules>
