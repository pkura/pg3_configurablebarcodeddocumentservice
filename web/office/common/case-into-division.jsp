<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N case-into-division.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<%--NIEU�YWANE--%>

<jsp:include page="/office/common/inc-title.jsp"/>

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages />

<form action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);" id="form">
<ww:hidden name="'portfolioId'" />
<ww:hidden name="'portfolioTxt'" />
<ww:hidden name="'caseFinishDate'"/>
<ww:hidden name="'clerk'"/>
<ww:hidden name="'arbitraryCaseId'"/>
<ww:hidden name="'priority'"/>
<ww:hidden name="'documentId'"/>
<ww:hidden name="'activity'"/>

<p>
<ww:if test="intoJournalPermission && incomingDivisions != null && incomingDivisions.size > 0">
    <ds:lang text="PrzyjmijPismoWwydziale"/>
    <br/>
    <ww:select name="'intoJournalId'" list="incomingDivisions" cssClass="'sel'"
        headerKey="''" headerValue="'-- wybierz dziennik --'" />
    <ds:submit-event value="'Przyjmij w tym dziale'" name="'doIntoJournal'"/>
</ww:if>
    <ds:submit-event value="'Wr��'" name="'doDefault'"/>
</p>

</form>

