<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N people-directory.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Nadawcy/odbiorcy</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/people-directory">
<html:hidden property="addresseeType" styleId="addresseeType" />
<html:hidden property="style"/>

<table>
<tr>
    <td>Tytu�</td><td>Imi�<span class="star">*</span></td><td>Nazwisko<span class="star">*</span></td>
</tr>
<tr>
    <td><html:text property="title" styleId="title" size="10" maxlength="30" styleClass="txt_opt"/></td>
    <td><html:text property="firstname" styleId="firstname" size="40" maxlength="50" styleClass="txt"/></td>
    <td><html:text property="lastname" styleId="lastname" size="40" maxlength="70" styleClass="txt"/></td>
</tr>
<tr>
    <td colspan="3">Firma/urz�d</td>
</tr>
<tr>
    <td colspan="3"><html:text property="organization" styleId="organization" size="91" maxlength="100" styleClass="txt_opt"/></td>
</tr>
<tr>
    <td colspan="3">Wydzia�</td>
</tr>
<tr>
    <td colspan="3"><html:text property="organizationDivision" styleId="organizationDivision" size="91" maxlength="100" styleClass="txt_opt"/></td>
</tr>
<tr>
    <td colspan="3">Adres/ulica</td>
</tr>
<tr>
    <td colspan="3"><html:text property="street" styleId="street" size="91" maxlength="100" styleClass="txt_opt"/></td>
</tr>
</table>

<table>
<tr>
    <td>Kod pocztowy</td><td>Miejscowo��</td><td>Kraj</td>
</tr>
<tr>
    <td><html:text property="zip" styleId="zip" size="8" maxlength="14" styleClass="txt_opt"/></td>
    <td><html:text property="location" styleId="location" size="30" maxlength="70" styleClass="txt_opt"/></td>
    <%--<td><html:text property="country" size="2" maxlength="2" styleClass="txt_opt"/></td>--%>
    <td><html:select property="country" styleId="country" styleClass="sel_opt" onchange="this.form.zip.validator.revalidate();">
            <html:option value="  ">-- wybierz kraj --</html:option>
            <html:option value="US">United States</html:option>
            <html:option value="GB">United Kingdom</html:option>
            <html:option value="CA">Canada</html:option>

            <html:option value="AU">Australia</html:option>
            <html:option value="AF">Afghanistan</html:option>
            <html:option value="AL">Albania</html:option>
            <html:option value="DZ">Algeria</html:option>

            <html:option value="AD">Andorra</html:option>
            <html:option value="AO">Angola</html:option>
            <html:option value="AI">Anguilla</html:option>
            <html:option value="AG">Antigua and Barbuda</html:option>
            <html:option value="AR">Argentina</html:option>

            <html:option value="AM">Armenia</html:option>
            <html:option value="AW">Aruba</html:option>
            <html:option value="AU">Australia</html:option>
            <html:option value="AT">Austria</html:option>
            <html:option value="AZ">Azerbaijan</html:option>
            <html:option value="BS">Bahamas</html:option>

            <html:option value="BH">Bahrain</html:option>
            <html:option value="BD">Bangladesh</html:option>
            <html:option value="BB">Barbados</html:option>
            <html:option value="BY">Belarus</html:option>
            <html:option value="BE">Belgium</html:option>
            <html:option value="BZ">Belize</html:option>

            <html:option value="BJ">Benin</html:option>
            <html:option value="BM">Bermuda</html:option>
            <html:option value="BT">Bhutan</html:option>
            <html:option value="BO">Bolivia</html:option>
            <html:option value="BA">Bosnia and Herzegovina</html:option>
            <html:option value="BW">Botswana</html:option>

            <html:option value="BR">Brazil</html:option>
            <html:option value="BN">Brunei</html:option>
            <html:option value="BG">Bulgaria</html:option>
            <html:option value="BF">Burkina Faso</html:option>
            <html:option value="MM">Burma (Myanmar)</html:option>

            <html:option value="BI">Burundi</html:option>
            <html:option value="KH">Cambodia</html:option>
            <html:option value="CM">Cameroon</html:option>
            <html:option value="CA">Canada</html:option>
            <html:option value="CV">Cape Verde</html:option>
            <html:option value="KY">Cayman Islands</html:option>

            <html:option value="CF">Central African Republic</html:option>
            <html:option value="TD">Chad</html:option>
            <html:option value="CL">Chile</html:option>
            <html:option value="CN">China</html:option>

            <html:option value="CO">Colombia</html:option>
            <html:option value="KM">Comoros</html:option>
            <html:option value="CD">Congo, Democratic Republic of the</html:option>
            <html:option value="CG">Congo, Republic of the</html:option>
            <html:option value="CR">Costa Rica</html:option>

            <html:option value="HR">Croatia</html:option>
            <html:option value="CY">Cyprus</html:option>
            <html:option value="CZ">Czech Republic</html:option>
            <html:option value="DK">Denmark</html:option>
            <html:option value="DJ">Djibouti</html:option>
            <html:option value="DM">Dominica</html:option>

            <html:option value="DO">Dominican Republic</html:option>
            <html:option value="TL">East Timor</html:option>
            <html:option value="EC">Ecuador</html:option>
            <html:option value="EG">Egypt</html:option>
            <html:option value="SV">El Salvador</html:option>
            <html:option value="GQ">Equatorial Guinea</html:option>

            <html:option value="ER">Eritrea</html:option>
            <html:option value="EE">Estonia</html:option>
            <html:option value="ET">Ethiopia</html:option>
            <html:option value="FK">Falkland Islands (Malvinas)</html:option>
            <html:option value="FJ">Fiji</html:option>

            <html:option value="FI">Finland</html:option>
            <html:option value="FR">France</html:option>
            <html:option value="GF">French Guiana</html:option>
            <html:option value="PF">French Polynesia</html:option>
            <html:option value="GA">Gabon</html:option>
            <html:option value="GM">Gambia</html:option>

            <html:option value="GE">Georgia</html:option>
            <html:option value="DE">Germany</html:option>
            <html:option value="GH">Ghana</html:option>
            <html:option value="GI">Gibraltar</html:option>
            <html:option value="GR">Greece</html:option>
            <html:option value="GL">Greenland</html:option>

            <html:option value="GD">Grenada</html:option>
            <html:option value="GP">Guadeloupe</html:option>
            <html:option value="GT">Guatemala</html:option>
            <html:option value="GN">Guinea</html:option>
            <html:option value="GW">Guinea-Bissau</html:option>

            <html:option value="GY">Guyana</html:option>
            <html:option value="HT">Haiti</html:option>
            <html:option value="HN">Honduras</html:option>
            <html:option value="HK">Hong Kong</html:option>
            <html:option value="HU">Hungary</html:option>
            <html:option value="IS">Iceland</html:option>

            <html:option value="IN">India</html:option>
            <html:option value="ID">Indonesia</html:option>
            <html:option value="IQ">Iraq</html:option>
            <html:option value="IE">Ireland</html:option>
            <html:option value="IL">Israel</html:option>
            <html:option value="IT">Italy</html:option>

            <html:option value="CI">Ivory Coast (C�te d'Ivoire)</html:option>
            <html:option value="JM">Jamaica</html:option>
            <html:option value="JP">Japan</html:option>
            <html:option value="JO">Jordan</html:option>
            <html:option value="KZ">Kazakhstan</html:option>
            <html:option value="KE">Kenya</html:option>

            <html:option value="KI">Kiribati</html:option>
            <html:option value="KR">Korea, South</html:option>
            <html:option value="KW">Kuwait</html:option>
            <html:option value="KG">Kyrgyzstan</html:option>
            <html:option value="LA">Laos</html:option>
            <html:option value="LV">Latvia</html:option>

            <html:option value="LB">Lebanon</html:option>
            <html:option value="LS">Lesotho</html:option>
            <html:option value="LR">Liberia</html:option>
            <html:option value="LI">Liechtenstein</html:option>
            <html:option value="LT">Lithuania</html:option>
            <html:option value="LU">Luxembourg</html:option>

            <html:option value="MK">Macedonia, Republic of</html:option>
            <html:option value="MG">Madagascar</html:option>
            <html:option value="MW">Malawi</html:option>
            <html:option value="MY">Malaysia</html:option>

            <html:option value="ML">Mali</html:option>
            <html:option value="MT">Malta</html:option>
            <html:option value="MQ">Martinique</html:option>
            <html:option value="MR">Mauritania</html:option>
            <html:option value="MU">Mauritius</html:option>

            <html:option value="MX">Mexico</html:option>
            <html:option value="FM">Micronesia</html:option>
            <html:option value="MD">Moldova</html:option>
            <html:option value="MC">Monaco</html:option>
            <html:option value="MN">Mongolia</html:option>

            <html:option value="MA">Morocco</html:option>
            <html:option value="MZ">Mozambique</html:option>
            <html:option value="NA">Namibia</html:option>
            <html:option value="NR">Nauru</html:option>
            <html:option value="NP">Nepal</html:option>

            <html:option value="NL">Netherlands</html:option>
            <html:option value="AN">Netherlands Antilles</html:option>
            <html:option value="NC">New Caledonia</html:option>
            <html:option value="NZ">New Zealand</html:option>
            <html:option value="NI">Nicaragua</html:option>
            <html:option value="NE">Niger</html:option>

            <html:option value="NG">Nigeria</html:option>
            <html:option value="NO">Norway</html:option>
            <html:option value="OM">Oman</html:option>

            <html:option value="PK">Pakistan</html:option>
            <html:option value="PS">Palestinian Territory</html:option>
            <html:option value="PA">Panama</html:option>
            <html:option value="PG">Papua New Guinea</html:option>
            <html:option value="PY">Paraguay</html:option>

            <html:option value="PE">Peru</html:option>
            <html:option value="PH">Philippines</html:option>
            <html:option value="PL">Polska</html:option>
            <html:option value="PT">Portugal</html:option>
            <html:option value="PR">Puerto Rico</html:option>

            <html:option value="QA">Qatar</html:option>
            <html:option value="RE">R�union</html:option>
            <html:option value="RO">Romania</html:option>
            <html:option value="RU">Russia</html:option>
            <html:option value="RW">Rwanda</html:option>
            <html:option value="SH">Saint Helena</html:option>

            <html:option value="KN">Saint Kitts and Nevis</html:option>
            <html:option value="LC">Saint Lucia</html:option>
            <html:option value="VC">Saint Vincent and the Grenadines</html:option>
            <html:option value="SM">San Marino</html:option>

            <html:option value="ST">Sa~o Tome and Principe</html:option>
            <html:option value="SA">Saudi Arabia</html:option>
            <html:option value="SN">Senegal</html:option>
            <html:option value="CS">Serbia and Montenegro</html:option>
            <html:option value="SC">Seychelles</html:option>
            <html:option value="SL">Sierra Leone</html:option>

            <html:option value="SG">Singapore</html:option>
            <html:option value="SK">Slovakia</html:option>
            <html:option value="SI">Slovenia</html:option>
            <html:option value="SO">Somalia</html:option>
            <html:option value="ZA">South Africa</html:option>

            <html:option value="ES">Spain</html:option>
            <html:option value="LK">Sri Lanka</html:option>
            <html:option value="SR">Suriname</html:option>
            <html:option value="SZ">Swaziland</html:option>

            <html:option value="SE">Sweden</html:option>
            <html:option value="CH">Switzerland</html:option>
            <html:option value="TW">Taiwan</html:option>
            <html:option value="TJ">Tajikistan</html:option>
            <html:option value="TZ">Tanzania</html:option>
            <html:option value="TH">Thailand</html:option>

            <html:option value="TG">Togo</html:option>
            <html:option value="TT">Trinidad and Tobago</html:option>
            <html:option value="TN">Tunisia</html:option>
            <html:option value="TR">Turkey</html:option>

            <html:option value="TM">Turkmenistan</html:option>
            <html:option value="UG">Uganda</html:option>
            <html:option value="UA">Ukraine</html:option>
            <html:option value="AE">United Arab Emirates</html:option>

            <html:option value="GB">United Kingdom</html:option>
            <html:option value="US">United States</html:option>
            <html:option value="UM">United States Minor Outlying Islands</html:option>
            <html:option value="UY">Uruguay</html:option>
            <html:option value="UZ">Uzbekistan</html:option>
            <html:option value="VU">Vanuatu</html:option>

            <html:option value="VA">Vatican</html:option>
            <html:option value="VE">Venezuela</html:option>
            <html:option value="VN">Vietnam</html:option>
            <html:option value="VG">Virgin Islands, British</html:option>
            <html:option value="VI">Virgin Islands, U. S.</html:option>

            <html:option value="EH">Western Sahara</html:option>
            <html:option value="YE">Yemen</html:option>
            <html:option value="ZM">Zambia</html:option>
            <html:option value="ZW">Zimbabwe</html:option>
        </html:select>
</tr>
<tr>
    <td colspan="3"><span class="warning" id="zipError"> </span></td>
</tr>
</table>

<table>
<tr>
    <td>Email</td><td>Faks</td>
</tr>
<tr>
    <td><html:text property="email" styleId="email" size="30" maxlength="50" styleClass="txt_opt"/></td>
    <td><html:text property="fax" styleId="fax" size="30" maxlength="30" styleClass="txt_opt"/></td>
</tr>
</table>

<input type="submit" name="doSearch" value="Szukaj" class="btn"/>
<input type="submit" name="doAdd" value="Dodaj do s�ownika" class="btn"/>

</html:form>

<p><small><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></small></p>

<script language="JavaScript">
var szv = new Validator(document.getElementById('zip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('zipError').innerHTML = '';
}

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('zipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('zipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
}

szv.canValidate = function() {
    var country = document.getElementById('country');
    var countryCode = country.options[country.options.selectedIndex].value;

    return (countryCode == '' || countryCode == 'PL');
}
</script>

<c:if test="${pickThis}">
    <script language="JavaScript">
    if (window.opener.__people_directory_pick)
    {
        window.opener.__people_directory_pick(
            document.getElementById('addresseeType').value,
            document.getElementById('title').value,
            document.getElementById('firstname').value,
            document.getElementById('lastname').value,
            document.getElementById('organization').value,
            document.getElementById('organizationDivision').value,
            document.getElementById('street').value,
            document.getElementById('zip').value,
            document.getElementById('location').value,
            document.getElementById('country').value,
            document.getElementById('email').value,
            document.getElementById('fax').value);
        window.close();
    }
    else
    {
        alert('W oknie wywo�uj�cym nie zdefiniowano funkcji __people_directory_pick.\nSkontaktuj si� z administratorem.');
    }
    </script>
</c:if>

