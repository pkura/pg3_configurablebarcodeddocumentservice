<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N portfolios-precreate.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Teczki</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="baseLink"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'rwaCategoryId'" id="rwaCategoryId" value="rwaCategoryId"/>
<ww:hidden name="'divisionGuid'" value="divisionGuid"/>
<ww:hidden name="'officeIdPrefix'" value="officeIdPrefix"/>
<ww:hidden name="'officeIdMiddle'" value="officeIdMiddle"/>
<ww:hidden name="'officeIdSuffix'" value="officeIdSuffix"/>
<ww:hidden name="'suggestedOfficeId'" value="suggestedOfficeId"/>
<ww:hidden name="'name'" value="name"/>
<ww:hidden name="'barcode'" value="barcode"/>
<ww:hidden name="'days'" value="days"/>
<ww:hidden name="'rok'" value="rok" />
<!-- ponizsze na potrzeby PickPortfolioAction-->
<ww:hidden name="'documentIncomingDate'" value="documentIncomingDate"/>

<table width="100%">
<tr>
    <td>
        <ww:property value="treeHtml" escape="false"/>
    </td>
</tr>
<tr>
    <td valign="top">
        <p><br><br>Zamierzasz za�o�y� now� teczk� o symbolu <ww:property value="suggestedOfficeId"/>
        (<a href="javascript:void(customOfficeId())">zmodyfikuj numer</a>)</p>

        <div id="breakdown" style="display: none">
            <ww:hidden name="'customOfficeId'" id="customOfficeId"/> <!-- bool -->
            <ww:textfield name="'customOfficeIdPrefix'" value="customOfficeIdPrefix" cssClass="'txt'" size="6" maxlength="20" />
            <ww:textfield name="'customOfficeIdPrefixSeparator'" value="customOfficeIdPrefixSeparator" cssClass="'txt'" size="1" maxlength="1" />
            <ww:textfield name="'customOfficeIdMiddle'" value="customOfficeIdMiddle" cssClass="'txt'" size="15" maxlength="50" />
            <ww:textfield name="'customOfficeIdSuffixSeparator'" value="customOfficeIdSuffixSeparator" cssClass="'txt'" size="1" maxlength="1" />
            <ww:textfield name="'customOfficeIdSuffix'" value="customOfficeIdSuffix" cssClass="'txt'" size="6" maxlength="20" />
        </div>

        <p></p>

        <ds:submit-event value="'Utw�rz teczk�'" name="'doCreate'" cssClass="'btn'" />
        <ds:submit-event value="'Anuluj'" name="'brak akcji'" cssClass="'btn'"/>
    </td>
</tr>
</table>

</form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<script language="JavaScript">
// funkcja wywo�ywana przez okienko wyboru RWA
function pickRwa(rootId, categoryId, rwaString)
{
    document.getElementById('rwaCategoryId').value = categoryId;
    document.getElementById('prettyRwaCategory').value = rwaString;
    document.getElementById('name').value = rwaString.substring(rwaString.indexOf(': ')+2);
}

function customOfficeId()
{
    toggle_div('breakdown', true);
    document.getElementById('customOfficeId').value = 'true';
}

function toggle_div(id, show)
{
    var div = document.getElementById(id);
    div.style.display = show ? '' : 'none';
}

</script>
