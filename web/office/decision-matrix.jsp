<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="Matrix"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ds:xmlLink path="decisionMatrix"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/decision-matrix.action'"/>" method="post">
<table>
	<ww:hidden name="'toMyDivision'" id="toMyDivision" value="toMyDivision"/>
	<tr>
		<td><ww:select cssClass="'sel'" name="'selectedGuid'" id="selectedGuid" list="divisions" 
		listValue="value.getName()"
		onchange="'document.forms[0].submit();'"/></td>
	</tr>
</table>
<table> 
	<tr>
			<th><nobr>
				<ds:lang text="Priorytet"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="Segment"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="Typ"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="PodTyp"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="KanalWplywu"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="PNAOd"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="PNADo"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="Dzial"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="Uzytkownik"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
				<ds:lang text="Auto"/>
			</nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<th><nobr>
			</nobr></th>
			<th><nobr>
			</nobr></th>
	</tr>
	
	<ww:iterator value="decisionRules">
		<tr>
			<td>
				<ww:property value="priority"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property  value="getValueAsString('SEGMENT',segment)"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property value="getValueAsString('TYP_DOKUMENTU',typ)"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property value="getValueAsString('PODTYP_DOKUMENTU',podTyp)"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property value="getValueAsString('SPOSOB_PRZYJECIA',kanalWplywu)"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property value="pnaGroupFrom"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<td>
				<ww:property value="pnaGroupTo"/>
			</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			<ww:if test="!toMyDivision">
				<td>
					<ww:property value="getGuidName(targetGuid)"/>
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="getUserAsString(targetUser)"/>
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="getAutoAsString(autoAssign)"/>			
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<a name="<ww:property value="cn"/>" href="<ww:url value="'/office/decision-matrix-edit.action'"><ww:param name="'id'" value="id"/><ww:param name="'matrixGuid'" value="selectedGuid"/></ww:url>">
						<ds:lang text="edytuj"/></a>
				</td>
			</ww:if>
			<ww:else>
				<td>
					<ww:property value="getGuidName(matrixGuid)"/>
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="getUserAsString(targetUser)"/>
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="getAutoAsString(autoAssign)"/>			
				</td><td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			</ww:else>
		</tr>
	</ww:iterator>
	<ww:if test="!toMyDivision">
	<tr>
		<td>
			<ds:button-redirect href="'/office/decision-matrix-edit.action?matrixGuid='+selectedGuid" value="getText('Dodaj')"/>
		</td>
	</tr>
	</ww:if>
</table>
<script type="text/javascript">

   	function openAssignmentWindow(id)
	{
		window.open('<ww:url value="'/office/decision-matrix-edit.action'"/>?id='+id, 'assignmentwindow', 'width=800,height=600,resizable=yes,scrollbars=yes,status=no,toolbar=no,menu=no');
	}
</script>
</form>