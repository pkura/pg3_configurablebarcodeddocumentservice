<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 04.08.08
	Poprzednia zmiana: 04.08.08
C--%>
<!--N edit-role-permissions.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript">

	function DSPermission(id, name, category)
	{
		this.getId = function() { return id; }
		this.getName = function() { return name; }
		this.getCategory = function() { return category; }
	}
	
	// lista uprawnie� mo�liwych do dodania
	var freePermissions = new Array();
	<ww:iterator value="freePermissions" status="status" >
		freePermissions[<ww:property value="#status.index"/>] = new DSPermission('<ww:property value="name" escape="false" />', '<ww:property value="description" escape="false" />', '<ww:property value="category.sid"/>');
	</ww:iterator>
	
	// lista uprawnie� jakie rola posiada
	var boundPermissions = new Array();
	<ww:iterator value="boundPermissions" status="status" >
		boundPermissions[<ww:property value="#status.index"/>] = new DSPermission('<ww:property value="name" escape="false" />', '<ww:property value="description" escape="false" />', '<ww:property value="category.sid"/>');
	</ww:iterator>
	
	/**
		Przesuwa zaznaczone elementy z jednego elementu SELECT do drugiego
		oraz ten sam element mi�dzy kolekcjami freePermissions i boundPermissions.
		Kierunek przesuwania okre�la parametr direction, mog�cy przyjmowa�
		warto�ci 'toBound' lub 'toFree'.
	*/
	function move(direction)
	{
		if (direction == 'toBound')
		{
			var select1 = document.getElementById('freePermissions');
			var select2 = document.getElementById('boundPermissions');
			var collection1 = freePermissions;
			var collection2 = boundPermissions;
		}
		else if (direction == 'toFree')
		{
			var select1 = document.getElementById('boundPermissions');
			var select2 = document.getElementById('freePermissions');
			var collection1 = boundPermissions;
			var collection2 = freePermissions;
		}
		var i=0;
		while (i < select1.options.length)
		{
			if (select1.options[i].selected)
			{
				var opt = select1.options[i];
				select2.options[select2.options.length] = new Option(opt.text, opt.value);
				select1.options[i] = null;
	
				// przesuwanie elementu z kolekcji 1 do 2
				for (var c=0; c < collection1.length; c++)
				{
					if (collection1[c].getId() == opt.value)
					{
						collection2[collection2.length] = collection1[c];
						for (var j=c; j < collection1.length-1; j++)
						{
							collection1[j] = collection1[j+1];
						}
						collection1.length = collection1.length - 1;
					}
				}
			}
			else
			{
				i++;
			}
		}
	}
	
	/**
		Generuje elementy INPUT TYPE=HIDDEN na podstawie
		kolekcji boundPermissions.
	*/
	function generateHiddenInputs()
	{
		var f = document.getElementById("form"); // formularz
	
		for (var i=0; i < boundPermissions.length; i++)
		{
			var inp = document.createElement("INPUT");
			inp.setAttribute("type", "hidden");
			inp.setAttribute("name", "selectedPermissions");
			inp.setAttribute("value", boundPermissions[i].getId());
			f.appendChild(inp);
		}
	}
	
	/**
		Wype�nia element SELECT nazwami uprawnie�. Pomija uprawnienia,
		dla kt�rych funkcja accept zwraca false.
	*/
	function fill(sel, perms, accept)
	{
		for (var i=0; i < perms.length; i++)
		{
			if (accept(perms[i]))
				sel.options[sel.options.length] = new Option(perms[i].getName(), perms[i].getId());
		}
	}
	
	function refill()
	{
		var freePermissionsSelect = document.getElementById('freePermissions');
		var boundPermissionsSelect = document.getElementById('boundPermissions');
	
		freePermissionsSelect.options.length = 0;
		boundPermissionsSelect.options.length = 0;
	
		// funkcja akceptuj�ca tylko uprawnienia zgodne z bie��c� kategori�
		var accept = function(perm) { return perm.getCategory() == document.getElementById('categories').value; }
	
		fill(freePermissionsSelect, freePermissions, accept);
		fill(boundPermissionsSelect, boundPermissions, accept);
	}
</script>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Rola"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/admin/edit-role-permissions.action'"/>" id="form" method="post" onsubmit="generateHiddenInputs();">
	<ww:hidden name="'id'" value="id"/>
	
	<ds:lang text="Nazwa"/>:
	<ww:textfield name="'roleName'" size="30" cssClass="'txt'"/>
	
	<br/>
	<ds:lang text="Kategoria"/>:
	<ww:select name="''" cssClass="'sel'" onchange="'refill();'" id="categories" list="categories" listKey="sid" listValue="name" />
	
	<br/>
	<br/>
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepneUprawnienia"/>:
			</th>
			<td></td>
			<th>
				<ds:lang text="UprawnieniaRoli"/>:
			</th>
		</tr>
		<tr>
			<td>
				<select id="freePermissions" multiple="true" size="10" class="multi_sel" style="min-width: 300px;"/>
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " onclick="move('toBound');" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " onclick="move('toFree');" class="btn"/>
			</td>
			<td>
				<select id="boundPermissions" multiple="true" size="10" class="multi_sel" style="min-width: 300px;"/>
			</td>
		</tr>
	</table>
	
	<input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"/>
	<input type="submit" name="doCancel" value="<ds:lang text="Powrot"/>" class="btn"/>
	
</form>
<%--
<h1><ds:lang text="Rola"/></h1>
<hr size="1" align="left" class="highlightedText" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/admin/edit-role-permissions.action'"/>" id="form" method="post" onsubmit="generateHiddenInputs();">
	<ww:hidden name="'id'" value="id"/>
	
	<table>
	<tr>
		<td><ds:lang text="Nazwa"/>:</td>
		<td><ww:textfield name="'roleName'" size="30" cssClass="'txt'"/></td>
	</tr>
	</table>
	
	<table>
		<tr>
			<td>
				<ds:lang text="Kategoria"/>:
			</td>
			<td>
				<ww:select name="''" cssClass="'sel'" onchange="'refill();'"
				id="categories" list="categories" listKey="sid" listValue="name" />
			</td>
		</tr>
	</table>
	
	<table>
	<tr>
		<th><ds:lang text="DostepneUprawnienia"/></th>
		<th></th>
		<th><ds:lang text="UprawnieniaRoli"/></th>
	</tr>
	<tr>
		<td valign="top">
			<select id="freePermissions" multiple="true" size="10" class="multi_sel" style="width: 300px;">
			</select>
		</td>
		<td valign="middle">
			<input type="button" value=" &gt;&gt; "
				onclick="move('toBound');"
				class="btn"/>
			<br/>
			<input type="button" value=" &lt;&lt; "
				onclick="move('toFree');"
				class="btn"/>
		</td>
		<td valign="top">
			<select id="boundPermissions" multiple="true" size="10" class="multi_sel" style="width: 300px;">
			</select>
		</td>
	</tr>
	</table>
	
	<input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn"/>
	<input type="submit" name="doCancel" value="<ds:lang text="Powrot"/>" class="btn"/>

</form>
--%>
<script> refill(); </script>

<!--N koniec edit-role-permissions.jsp N-->