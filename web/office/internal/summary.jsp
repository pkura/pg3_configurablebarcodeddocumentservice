<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style type="text/css">
	select#jbpmProcessId {width:22em;}
</style>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last">
		<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	</ww:if>
</ww:iterator>
</div>
</ds:available>

<p></p>

<form action="<ww:url value="'/office/internal/summary.action'"/>" method="post">

	<ww:hidden name="'documentId'"/>
	<ww:hidden name="'activity'"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>

    <ds:ww-action-errors/>
    <ds:ww-action-messages/>

	<table class="summaryTable">
		<ww:if test="blocked">
			<tr>
				<td colspan="2">
					<span style="color: red">
						<ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
					</span>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td width="30%" valign="top">
				<span id="id_strzalki1" style="display:">
					<a href="javascript:pokaz_ukryj()" alt="Poka� szczeg�y" title="Poka� szczeg�y"><img class="mini_circle" src="<ww:url value="'/img/circle_mini_down.gif'"/>" border="0"/></a>
				</span>
				<span id="id_strzalki2" style="display:none">
					<a href="javascript:pokaz_ukryj()" alt="Ukryj szczeg�y" title="Ukryj szczeg�y"><img class="mini_circle" src="<ww:url value="'/img/circle_mini_up.gif'"/>" border="0"/></a>
				</span>
				<ds:lang text="NumerKancelaryjny"/>: 
				<b><ww:property value="document.formattedOfficeNumber"/></b>
			</td>
<script type="text/javascript">
$j(document).ready(function() {
pokaz_ukryj();	
});
</script>
			<%--	<b><ww:if test="document.masterDocument != null">
				[
			</ww:if>
			<ww:property value="document.officeNumber"/>
			<ww:if test="document.masterDocument != null">
				]
			</ww:if></b>
			</td>	--%>
			<td width="30%" valign="top">
				<ds:extras test="!business">
					<ds:lang text="SymbolWsprawie"/>: 
					<b><a href="<ww:url value='caseLink'/>"><ww:property value="caseDocumentId"/></a></b>
				</ds:extras>
			</td>
		</tr>
		<tr id="id_id" style="display:none">
			<td width="30%">ID: 
				<b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="document.id"/></ww:url>"><ww:property value="document.id"/></a></b>
			</td>
			<td></td>
		</tr>
		<tr id="id_autora" style="display:none">
			<ww:if test="document != null and document.ctime != null">
				<ww:set name="ctime" value="document.ctime" scope="page"/>
			</ww:if>
			<td width="30%" valign="top">
				<table>
					<tr>
						<td valign="top">
							<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
								<ds:lang text="Autor"/>: 
								<b><ww:property value="creatingUser"/></b>;
								<ds:lang text="dnia"/>: 
								<b><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></b>
							</div>
						</td>
					</tr>
				</table>
			</td>
			<td width="70%" valign="top">
				<table>
					<tr>
						<td valign="top">
							<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
								<ds:lang text="Dzial"/>:
								<b><ww:property value="assignedDivision"/></b>
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<ww:set name="printPerson" value=":[(#this.firstname != null ? #this.firstname+' ' : '') + (#this.lastname != null ? #this.lastname : '') + (#this.organization != null ? ' / '+#this.organization : '')]"/>
		<ww:if test="document.repliedDocumentId != null">
			<tr>
				<ds:lang text="PismoJestOdpowiedziaNaDokument"/> KO:<a href="<ww:url value="'/office/internal/summary.action'"><ww:param name="'documentId'" value="document.repliedDocumentId"/></ww:url>"> <ww:property value="repliedDocument.officeNumber"/> </a>
			</tr>
		</ww:if>
		<tr id="id_uwag" style="display:">
			<td valign="top">
				<ww:if test="document.remarks.size > 0">
					<table>
						<tr>
							<td valign="top">
								<div style="white-space: nowrap;">
									<ds:lang text="OstatniaUwaga"/>:
								</div>
							</td>
							<td valign="top">
								<div style="overflow: hidden; text-overflow: ellipsis;">
									<b><ww:property value="document.remarks[document.remarks.size-1].content"/></b>
								</div>
							</td>
						</tr>
						<tr>
							<td valign="top">
								&nbsp
							</td>
							<td valign="top">
								<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
									<ds:lang text="dodanaPrzez"/>
									<b><ww:property value="recentRemark.author"/></b>.
								</div>
							</td>
						</tr>
					</table>
				</ww:if>
			</td>

			<td valign="top">
			<table>
				<tr>
					<td valign="top">
						<div style="overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
							<ds:lang text="OpisPisma"/>:
							<b><ww:property value="document.summary"/>
							<ww:if test="daaSummary != null">
								<br/>
								<ww:property value="daaSummary"/>
							</ww:if></b>
						</div>
					</td>
				</tr>
				<ww:if test="documentForm != null">
				<tr>
					<td valign="top">
						<ds:lang text="Forma dokumentu"/>:                    	
					</td>
					<td>
						<b id='documentForm'> 
							<ww:property value="documentForm" />
						</b>
					</td>
				</tr>
				</ww:if>
			</table>
		</td>
		</tr>
		<ww:if test="orders.size > 0">
			<tr id="id_polecenia" style="display:none">
				<td>	
					<table>
						<tr>
							<td>
								<ds:lang text="PoleceniaZwiazaneZpismem"/> :
								<ww:iterator value="orders">
									<a href="<ww:url value="'/office/order/summary.action'"/>?documentId=<ww:property value="id"/>">
									<ww:property value="officeNumber"/>
									<ww:if test="!#status.last">,</ww:if>
								</ww:iterator>
							</td>
						</tr>		
					</table>
				</td>
			</tr>
		</ww:if>

		<ds:extras test="business">
			<tr><td colspan="2">
			<table id="boxInnerTable">
			<jsp:include page="/common/box.jsp"/>
			</table>
			</td></tr>
		</ds:extras>

		<ww:if test="daaUnassigned">
			<tr id="id_agencji" style="display:none">
				<td></td>
				<td style="color:red">
					<span style="font-weight:bold;">!</span>
					<ds:lang text="nieprzypisanyDoAgencjiAgenta"/>
				</td>
			</tr>
		</ww:if>
	</table>

	<jsp:include page="/office/common/workflow/jbpm-summary.jsp"/>
		 
	<ww:if test="flagsPresent">
	<!-- <h4><ds:lang text="Flagi"/></h4> -->
		<table class="tableMargin">
		<tr>
			<td class="alignTop">
				<span class="bold"><ds:lang text="FlagiWspolne"/>:</span>
				<ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiWspolneList">
					<ww:iterator value="globalFlags">
						<ww:if test="canView">
							<li>
								<div class="inline_flags">
									<ww:checkbox name="'globalFlag'" fieldValue="id"
										value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
									<img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
									<img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
									<b><ww:property value="c"/></b>
									<ww:property value="description"/>
								</div>
							</li>
						</ww:if>
					</ww:iterator>
					<li class="hidden" id="FlagiWspolneNone">
						<span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
					</li>
				</ul>
				<div class="floatingDiv hidden" id="FlagiWspolneDiv">
					<ul class="clearList noMarginPadding floatingMenu" id="FlagiWspolneDivList">
					</ul>
					<ul class="clearList noMarginPadding">
						<li class="hidden" id="FlagiWspolneAll">
							<span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
						</li>
					</ul>
				</div>
			</td>
			<td class="alignTop">
				<span class="bold"><ds:lang text="FlagiUzytkownika"/>:</span>
				<ul class="clearList noMarginPadding listTofloatingMenu" id="FlagiUzytkownikaList">
					<ww:iterator value="userFlags">
						<li>
							<div class="inline_flags">
								<ww:checkbox name="'userFlag'" fieldValue="id"
									value="value" disabled="(value and !canClear) or !canEdit" cssClass="'hidden'"/>
								<img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden" id="plus"/>
								<img src="<ww:url value="'/img/list-minus.gif'"/>" class="hidden" id="minus"/>
								<b><ww:property value="c"/></b>
								<ww:property value="description"/>
							</div>
						</li>
					</ww:iterator>
					<li class="hidden" id="FlagiUzytkownikaNone">
						<span class="italic italicFlags"><ds:lang text="NieWybrano"/></span>
					</li>
				</ul>
				<div class="floatingDiv hidden" id="FlagiUzytkownikaDiv">
					<ul class="clearList noMarginPadding floatingMenu" id="FlagiUzytkownikaDivList">
					</ul>
					<ul class="clearList noMarginPadding">
						<li class="hidden" id="FlagiUzytkownikaAll">
							<span class="italic continuousElement"><ds:lang text="WybranoWszystkie"/></span>
						</li>
					</ul>
				</div>
			</td>
		</tr>
	</table>
	<script type="text/javascript">
		$j(document).ready(function ()
		{
			prepareList($j("#FlagiWspolneList"), $j("#FlagiWspolneNone"), $j("#FlagiWspolneAll"),$j("#FlagiWspolneDiv"), $j("#FlagiWspolneDivList"), FlagiWspolneStat);
			prepareList($j("#FlagiUzytkownikaList"), $j("#FlagiUzytkownikaNone"), $j("#FlagiUzytkownikaAll"),$j("#FlagiUzytkownikaDiv"), $j("#FlagiUzytkownikaDivList"), FlagiUzytkownikaStat);

			if ($j.browser.msie)
			{											
				$j("#FlagiUzytkownikaList").find("li").css("padding", "3px 0px");
				$j("#FlagiWspolneList").find("li").css("padding", "3px 0px");
			}
			
			<ds:additions test="tcLayout">
				rozmiar();
			</ds:additions>
		});
		
		var FlagiWspolneStat = 0;
		var FlagiUzytkownikaStat = 0;
		
		function checkAllNone (list_jq, none_jq, all_jq)
		{
			list_jq.find("li").filter(":has(input:checked)").length ? none_jq.addClass('hidden') : none_jq.removeClass('hidden');
			list_jq.find("li").filter(":has(input:not(:checked))").length ? all_jq.addClass('hidden') : all_jq.removeClass('hidden');
		}
		
		function hoverFix(list_jq, classa)
		{
			if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7))
			{
				list_jq.find("li").hover(
					function() {
						$j(this).addClass("" + classa);
					}
					,
					function() {
						$j(this).removeClass("" + classa);
					}
				);
			}
		}
		
		function createList(list_jq, div_jq, divList_jq, none_jq, all_jq)
		{
			divList_jq.find("li").remove();
			
			list_jq.find("li").filter(":has(input:not(:checked))").clone().appendTo(divList_jq).removeClass("hidden").removeClass("listTofloatingMenuHover") //IE 6 fix
				.find("div > #plus").removeClass("hidden")
				.end().find("div > #minus").addClass("hidden")
				.end().filter(":has(input:disabled)").find("div > #plus").addClass("hidden")
				.end().find("div > b").css("margin-left", "15px");
			
			hoverFix(divList_jq, "floatingMenuHover");
			
			$j.browser.msie ? divList_jq.find("li > div > input:checked").removeAttr("checked") : true ;
			
			var position = list_jq.position();
			
			position['top'] -= Math.round(div_jq.height() / 2);
			position['left'] += Math.round(list_jq.width() / 2);
			
			if ($j.browser.msie)
			{
				//position['top'] += $j(window).scrollTop();
				position['left'] += $j(window).scrollLeft();
			}
		
			//d�
			(position['top'] + div_jq.height()) > ($j(window).scrollTop() + $j(window).height()) ? (position['top'] = $j(window).scrollTop() + $j(window).height() - div_jq.height()) : true ;						
			//g�ra
			position['top'] < $j(window).scrollTop() ? position['top'] = $j(window).scrollTop() : true ;
			//prawo
			(position['left'] + div_jq.width()) > ($j(window).scrollLeft() + $j(window).width()) ? (position['left'] = $j(window).scrollLeft() + $j(window).width() - div_jq.width()) : true ;
			//lewo
			position['left'] < $j(window).scrollLeft() ? position['left'] = $j(window).scrollLeft() : true ;
		
			div_jq.css({top: position['top'], left: position['left']}).removeClass("hidden");
			divList_jq.find("li").click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);});
		}
		
		function updateList(this_jq, list_jq)
		{
			var vall = this_jq.find("div > input:checkbox").attr("value");
			
			list_jq.find("li").filter(":has(input:checkbox[value='" + vall + "'])").removeClass("hidden")
				.find("div > input").attr("checked", "checked")
				.end().find("div > #minus").removeClass("hidden")
				.end().find("div > #plus").addClass("hidden");
			this_jq.remove();
			<ds:additions test="tcLayout">
				rozmiar();
			</ds:additions>
		}
		
		function deleteList(div_jq, divList_jq)
		{
			div_jq.addClass("hidden");
			divList_jq.find("li").remove();
		}
		
		function clickEvent(this_jq, list_jq, div_jq, divList_jq, none_jq, all_jq) //nadawany <li>
		{
			if (this_jq.find("div > input").length)
			{
				if (this_jq.find("div > input:disabled").length)
				{//nie mam praw edycji - nie robie nic
					//alert("jeste� GUPI!!");
				}
				else if (this_jq.find("div > input:checked").length)
				{//zaznaczony - przenosze go na plywajaca liste
					this_jq.addClass("hidden")
						.find("div > input").removeAttr("checked")
						.end().find("div > #minus, div > #plus").addClass("hidden");
					checkAllNone (list_jq, none_jq, all_jq);
					createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
				}
				else
				{//niezaznaczony - przywracam do na glownej liscie
					this_jq.addClass("hidden");
					updateList(this_jq, list_jq);
					checkAllNone (list_jq, none_jq, all_jq);
				}
			}
		}
		
		function prepareList (list_jq, none_jq, all_jq, div_jq, divList_jq, stat)
		{
			hoverFix(list_jq, "listTofloatingMenuHover");
			list_jq.find("li")
				.click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);})
				.filter(":has(input:not(:checked))").addClass('hidden')
				.end().filter(":has(input:checked)").find("div > #minus").removeClass("hidden")
				//tu ju� leci do disabled
				.end().filter(":has(input:disabled)").find("div > #minus").addClass("hidden")
				.end().find("div > b").css("margin-left", "15px");
			checkAllNone (list_jq, none_jq, all_jq);
			
			list_jq.hover(
				function() {
					if (stat == 0)
					{
						createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
						stat = 1;
						
						if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
							div_jq.children("iframe").width(div_jq.width()+50);
							div_jq.children("iframe").height(div_jq.height()+50);
							div_jq.css('filter', 'none');
						}
					}
				}
				,
				function() {
					setTimeout(function() {												
						if (stat == 1)
							{
								deleteList(div_jq, divList_jq);
								stat = 0;
							}
						}, 100
					);
				}
			);
			
			div_jq.hover(
				function() {
					stat = 2;
				}
				,
				function() {
					deleteList(div_jq, divList_jq);
					stat = 0;
				}
			);
			
			/* obejscie dla ie6 */
			if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
				div_jq.append('<iframe class="iframeMask"></iframe>');
				div_jq.css('border', 'none');
			}
		}
	</script>
		</ww:if>
		<ds:available test="labels">
		<table>
		<tr>	
			<td>
				<b><ds:lang text="EtykietyModyfikowalne"/>:</b>
				<ww:iterator value="modifiableLabels">
					<div class="inline_flags">
								<a href="<ww:url value="'/office/remove-label.action'">
								<ww:param name="'documentId'" value="documentId"/>
								<ww:param name="'activity'" value="activity"/>
								<ww:param name="'labelId'" value="id"/></ww:url>" 
								title="<ds:lang text="UsunEtykiete"/>"
								class="area">
									<ww:property value="name" /> 
								</a>
					</div>
				</ww:iterator>
				<ww:iterator value="nonModifiableLabels">
					<div class="inline_flags">
						<b><ww:property value="name" />,</b>
					</div>
				</ww:iterator>
			</td>
		</tr>
		<!--<tr>	
			<td>
				<b><ds:lang text="EtykietyNiemodyfikowalne"/>:</b>
				<ww:iterator value="nonModifiableLabels">
					<div class="inline_flags">
						<b><ww:property value="name" />,</b>
					</div>
				</ww:iterator>
			</td>
		</tr>
		--></table>
		</ds:available>
		
	

	<ds:modules test="certificate">
		<ww:if test="recentSignature != null">
			<h4><ds:lang text="OstatniaAkceptacjaDokumentu"/></h4>
			<table>
				<tr>
					<th>
						<ds:lang text="Autor"/>
					</th>
					<th>
						<ds:lang text="Data"/>
					</th>
					<th colspan="2" style="text-align:center">
						<ds:lang text="pobierz"/>:
					</th>
				</tr>
				<tr>
					<td>
						<ww:property value="recentSignature.author"/>
					</td>
					<td>
						<ds:format-date value="recentSignature.ctime" pattern="dd-MM-yy HH:mm"/>
					</td>
					<td>
						<a href="<ww:url value="'/office/common/document-signature.action?getDoc=true&signatureId='+recentSignature.id"/>"><ds:lang text="dokument"/></a>
					</td>
					<ww:if test="recentSignature.type == null || recentSignature.type == 1">
						<td>
							<a href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+recentSignature.id"/>"><ds:lang text="podpis"/></a>
						</td>
					</ww:if>
				</tr>
			</table>
		</ww:if>
		
		<ww:if test="useSignature">
			<p class="btnContainer">
			<ww:if test="canAccept">
				<input type="button" value="<ds:lang text="AkceptujDokument"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-accept.action?doAcceptFillForm=true&id='+document.id"/>', 'accept', 500, 300);" <ww:if test="!canSign">disabled="disabled"</ww:if>/>
			</ww:if>
			<ww:else>
				<input type="button" value="<ds:lang text="PodpiszDokument"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doSign=true&id='+document.id"/>', 'sign', 400, 300);" <ww:if test="!canSign">disabled="disabled"</ww:if>/>
			</ww:else>
			<input type="button" 
				<ww:if test="canAccept">
					value="<ds:lang text="PostacDokumentuDoAkceptacji"/>"
				</ww:if>
				<ww:else>
					value="<ds:lang text="PostacDokumentuDoPodpisu"/>"
				</ww:else>
				 class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?showDoc=true&id='+document.id"/>', 'sign', 800, 600);"/></p>
		</ww:if>
	</ds:modules>

	<ds:modules test="office">
		<ww:if test="attachments.size > 0">
			<h4><ds:lang text="Zalaczniki"/></h4>
			<table>
				<tr>
					<th>
						<ds:lang text="Tytul"/>
					</th>
					<th>
						<ds:lang text="Autor"/>
					</th>
					<%--	<th>
						Rozmiar
					</th>	--%>
					<th>
						<ds:lang text="Data"/>
					</th>
					<%--	<th>
						Wersja
					</th>	--%>
					<%--	<th>
						Kod kreskowy
					</th>	--%>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				<ww:iterator value="attachments">
					<tr>
						<td>
							<ww:property value="title"/>
						</td>
						<td>
							<ww:property value="userSummary"/>
						</td>
						<ww:if test="mostRecentRevision != null">
							<ww:set name="revision" value="mostRecentRevision"/>
							<ww:set name="size" scope="page" value="#revision.size"/>
							<ww:set name="ctime" scope="page" value="#revision.ctime"/>
							<%--	<td>
								<ww:property value="#u.firstname + ' ' + #u.lastname"/>
							</td>	--%>
							<%--	<td>
								<edm-fmt:format-size size="${size}"/>
							</td>	--%>
							<td>
								<fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/>
							</td>
							<%--	<td>
								<ww:property value="#revision.revision"/>
							</td>	--%>
							<%--	<td>
								<ww:property value="barcode"/>
							</td>	--%>
							<td width="32">
								&nbsp;
								<a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>">
								<img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>"/></a>
							</td>
							<td valign="top" width="64">
								<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#revision.mime)">
									&nbsp
									<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+#revision.id"/>">
									<img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
									title="<ds:lang text="PobierzZalacznik"/>" /></a>
									&nbsp;
									<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'">
									<ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/>
									<ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/>
									<ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">
									<img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>"/></a>
								</ww:if>
								<ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimePicture(#revision.mime)">
                                                                	&nbsp;
                                                                	<a name="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#revision.id"/><ww:param name="'activity'" value="activity"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
                                                                	<img class="zoomImg" src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
                                                                	height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
                                                                </ww:elseif><ww:elseif test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(#revision.mime)">
                                    <a class="xmlView" href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ #revision.id"/>')">
                                        <img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/>
                                    </a>
                                </ww:elseif>
							</td>
							<td>
								<ww:if test="'nw_compiled_att'.equals(cn) && compilationStatus.newDocuments != null">
									<ww:iterator value="compilationStatus.newDocuments">
										<a style="color:red" href="<ww:url value="'/repository/edit-document.action?id='+key"/>"><ww:property value="value"/></a>
									</ww:iterator>
								</ww:if>
							</td>
						</ww:if>
						<ww:else>
							<td colspan="4">
								<ww:if test="lparam != null">
									<b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)"/></i></b>
								</ww:if>
								<ww:else>
								<i>(<ds:lang text="pusty"/>)</i>
								</ww:else>
							</td>
						</ww:else>
					</tr>
					<tr>
					<td>
					<input type="button" value="<ds:lang text="Pobierz pliki z "/>" class="btn"
							onclick="openWith();">
					</td>
						<td>Uwagi</td><td><ww:checkbox id="uwaga" name="'uwagaName'" fieldValue="true" /></td>
						<td>Historia pisma</td><td><ww:checkbox id="historia" name="'historiaName'" fieldValue="true"/></td>
						<td>Historia dekretacji</td><td><ww:checkbox id="dekretacja" name="'dekretacjaName'" fieldValue="true"/></td>
						<td colspan="6" align="right">
						
						    <!-- W przypadku konieczno��i usuni�cia prosz� o wcze�niejszy kontakt - Maciej Starosz -->
							<%-- <input type="button" value="<ds:lang text="PobierzZuwagami"/>" class="btn"
							onclick="openWith();"> --%>
						</tr>
				</ww:iterator>
			</table>

			<ds:extras test="business">
				<ww:if test="compilationStatus.canCompile">
					<ds:event name="'doCompileAttachments'" value="getText('KompilujPlikiSprawy')" cssClass="'btn'"/>    
				</ww:if>    
				<ww:if test="canGenerateDocumentView">
					<input type="button" value="<ds:lang text="PobierzObrazDokumentu"/>" class="btn"
						onclick="openToolWindow('<ww:url value="baseLink"><ww:param name="'doGenerateDocumentView'" value="true"/><ww:param name="'activity'" value="activity"/><ww:param name="'documentId'" value="documentId"/></ww:url>', 'nwPdf')">
				</ww:if>
			</ds:extras>
		</ww:if>

		<ww:if test="acceptancesEnabled">
			<table>
				<jsp:include page="/common/summary-acceptances.jsp"/>
			</table>
		</ww:if>

		<ww:if test="enableCollectiveAssignments">	<%-- --%>
			<h4><ds:lang text="ZbiorczaDekretacja"/></h4>
			<ww:if test="assignmentsMap == null or assignmentsMap.isEmpty"> <%--	<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">	--%>
				<p><i><ds:lang text="ListaZbiorczaDekNieSkonfigurowana1"/>
				<a href="<ww:url value="'/settings/user-collective-assignment.action'"/>"><ds:lang text="tutaj"/></a>.
				<ds:lang text="ListaZbiorczaDekNieSkonfigurowana2"/>. </i></p>
			</ww:if>
			<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
				<table>
					<tr>
						<th></th>
						<th>
							<ds:lang text="Uzytkownik"/>
						</th>
						<th>
							<ds:lang text="RodzajDekretacji"/>
						</th>
						<ww:if test="showObjectives">
							<th>
								<ds:lang text="CelDekretacji"/>
							</th>
						</ww:if>
					</tr>
					
					<ww:iterator value="assignmentsMap" >
						<tr>
							<td>
								<input type="checkbox" name="assignments" value="<ww:property value="key"/>"
									id="<ww:property value="key"/>"	onchange="updateSelect(this);"/>
							</td>
							<td>
								<ww:property value="value"/>
							</td>
							<td>
								<select name="assignmentProcess_<ww:property value="key"/>" id='assignmentSelect' class="sel" onchange="updateCheckbox(this);checkSelection()">
									<option value="">
										<ds:lang text="select.wybierz"/>
									</option>
									<ww:iterator value="wfProcesses">
										<option value="<ww:property value="key"/>">
											<ww:property value="value"/>
										</option>
									</ww:iterator>
								</select>
							</td>
							<ww:if test="showObjectives">
								<td>
									<ww:select name="'objectiveSel'" list="objectives" cssClass="'sel'" id="objectiveSel"
									listKey="name" listValue="name"	headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
								</td>
							</ww:if>
						</tr>
					</ww:iterator>
				</table>
			</ww:if>
		</ww:if>

		<table>
			<tr>
				<td colspan="3">
                    <ds:available test="!dwr">
                        <ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
                            <ds:event name="'doAssignments'" value="getText('Dekretuj')" onclick="'if (!validateAssignments()) return false; if (!checkSubstitutions()) return false;'"/>
                        </ww:if>

                        <ds:event name="'doUpdateFlags'" value="getText('Zapisz')" disabled="blocked" cssClass="'btn saveBtn'"/>

                        <ww:if test="externalWorkflow">
                            <input type="button" class="btn" value="<ds:lang text="ZakonczZadanie"/>" onclick="document.location.href='<ww:url value="jbpmLink"/>';"/>
                        </ww:if>
                        <ww:else>
                                <ww:if test="canReopenWf && !doWiadomosci">
                                   <ds:event name="'doReopenWf'" value="getText('PrzywrocNaListeZadan')"/> <%-- disabled="!canReopenWf" />  --%>
                               </ww:if>
                               <ww:elseif test="activity != null">
                                   <ww:if test="doWiadomosci"><ds:event name="'doManualFinish'" value="getText('PotwierdzamPrzeczytanie')"/></ww:if>
                                   <ww:else><ds:event name="'doManualFinish'" value="getText('ZakonczPraceZdokumentem')" onclick="'if (!confirmFinish()) return false;'" /></ww:else>
                               </ww:elseif>
                        </ww:else>

                        <ds:extras test="!business">
                            <ds:available test="klonowanie.podsumowanie">
                                <ds:event name="'doClone'" value="getText('Klonuj')" />
                            </ds:available>
                        </ds:extras>
					</ds:available>

					<input type="hidden" name="processName" id="processName"/>
                    <input type="hidden" name="processId" id="processId"/>
                    <input type="hidden" name="processAction" id="processAction"/>

                    <ds:available test="!summaryTab.processRenderBeans.disabled">
	                    <ww:iterator value="processRenderBeans">
	                        <ds:render template="template"/>
	                    </ww:iterator>
                    </ds:available>

					<ww:if test="canAddToWatched">
                        <ds:event name="'doWatch'" value="getText('DodajDoObserwowanych')" />
                    </ww:if>

					<ds:extras test="business">  <%--  and !nationwide --%>
						<ds:available test="klonowanie.podsumowanie">
							<input type="button" class="btn" value="<ds:lang text="Klonuj"/>"onclick="document.location.href='<ww:url value="'/repository/clone-dockind-document.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>';" />
						</ds:available>
					</ds:extras>

					<ds:additions test="orders">
						<input type="button" class="btn" value="<ds:lang text="UtworzPolecenie"/>"
						onclick="document.location.href='<ww:url value="'/office/order/new.action'"><ww:param name="'relatedOfficeDocumentId'" value="documentId"/></ww:url>';"/>
					</ds:additions>
					<ww:if test="showSendToEva && activity != null">
						<ds:event name="'sendToEva'" value="getText('sendToEva')"/>
					</ww:if>

					<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/office/tasklist/current-user-task-list.action';"/>
					<td>
	        		<input type="submit" value="Metryka Dokumentu" name="doDocumentMetrics" class="btn" onclick="sendEvent(this)"/>
	    			</td>
	    			<td>
	    			<ds:available test="PG.wlacz.wydrukKartaZastepcza"> 
	    			<input type="submit" value="Karta zastepcza" name="doPrintCard" class="btn" onclick="sendEvent(this)"/>
	    			</ds:available>
	    			<ds:available test="PG.electronicSignature.document">
							<td width="32">
							 <input type="button" class="btn" value="<ds:lang text="Podpisz Certyfikatem"/>"
							class="btn"	onclick="document.location.href='<ww:url value="'/certificates/sign-xml.action?documentId='+document.id +'&returnUrl=/docusafe/office/internal/summary.action?documentId='+document.id"></ww:url>';"/>
							</td>
            		 </ds:available>
             		 <ds:available test="XesLogApi">
							<td width="32">
							 <input type="submit" name="doGetXesLog" value="<ds:lang text="Pobie� XesLog"/>" class="btn"   name="'doGetXesLog'" id="doGetXesLog" />
							</td>
             		</ds:available>
	    			</td>
					<ww:if test="viewQc">
						<ds:event name="'doHotIcr'" value="getText('OkQC')" />
					</ww:if>
					
					<ww:if test="evaMessage != null">
						<ww:property value="evaMessage" />
					</ww:if>
				</td>
			</tr>
		</table>

	    <br/>
		<jsp:include page="/office/common/workflow/init-jbpm-process.jsp"/>
	</ds:modules>
	
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script>

	//W przypadku konieczno��i usuni�cia prosz� o wcze�niejszy kontakt - Maciej Starosz
	function openWith()
	{
		openToolWindow('<ww:url value="'view-attachments-as-pdf.action'"/>?documentIds=<ww:property value="documentId"/>' +
			'&remarksLayer=true' +	
			'&czyUwagi='+document.getElementById("uwaga").checked +
			'&czyHistoria='+document.getElementById("historia").checked +
			'&czyDekretacja='+document.getElementById("dekretacja").checked, 'nwPdf');
	}

 	function validateForm(){return true;}
 	//funkja ukrywaj�ca pole autora, itp
	function pokaz_ukryj()
	{
		document.getElementById('id_strzalki1').style.display=
			((document.getElementById('id_strzalki1').style.display=="none")?true:false)?"":"none";
		document.getElementById('id_strzalki2').style.display=
			((document.getElementById('id_strzalki1').style.display=="none")?true:false)?"":"none";

		document.getElementById('id_id').style.display=
			((document.getElementById('id_id').style.display=="none")?true:false)?"":"none";
		document.getElementById('id_autora').style.display=
			((document.getElementById('id_autora').style.display=="none")?true:false)?"":"none";
		if (document.getElementById('id_polecenia') != null)
			document.getElementById('id_polecenia').style.display=
				((document.getElementById('id_polecenia').style.display=="none")?true:false)?"":"none";
		//if (document.getElementById('id_pudla') != null)
		//	document.getElementById('id_pudla').style.display=
		//		((document.getElementById('id_pudla').style.display=="none")?true:false)?"":"none";
		if (document.getElementById('id_agencji') != null)
			document.getElementById('id_agencji').style.display=
				((document.getElementById('id_agencji').style.display=="none")?true:false)?"":"none";
		<ds:additions test="tcLayout">
			szerokosc();
			wysokosc();
		</ds:additions>
	}

    function confirmFinish()
    {
    <ww:if test="!finishOrAssignState.canFinish">
        alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
        return false;
    </ww:if>
    <ww:if test="!finishOrAssignState.finishWarnings.empty && !documentFax">
        if (!confirm('<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/>\n\n<ds:lang text="ZakonczycPraceZdokumentem"/>'))
            return false;
    </ww:if>
    <ww:else>
        if (!confirm('<ds:lang text="NaPewnoZakonczyc"/>?'))
            return false;
    </ww:else>

        document.getElementById('doManualFinish').value='true';

        return true;
    }

function forAll(select)
{
    var sels = document.getElementsByTagName('SELECT');
    for (var i=0; i < sels.length; i++)
    {
        if (sels[i].name.startsWith('userAssignment_'))
        {
            sels[i].value = select.value;
            //sels[i].fireEvent('onchange');
            updateCheckbox(sels[i]);
        }
    }
}


 function validateAssignments()
    {
    	var chks = document.getElementsByName('assignments');
    	var checkedCount = 0;
    	 
    	<ds:extras test="!business">               
        	var selects = document.getElementsByTagName('SELECT');
    
			if(selects.length>chks.length)
			{    
        		for (var i=0; i < selects.length; i++)
        		{
        			if(selects[i].id == 'assignmentSelect')
            		{
        				//alert("chks.item(i/2).checked: " + chks.item(i/2).checked + " i= " + i);
            			if (chks.item(i/2).checked) 
            			{                       	
            				if(selects[i].value == '')
            				{
            					alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            					return false;
            				}
            			}            
            		}          
        		}
        	}
        	else
        	{
        		for (var i=0; i < chks.length; i++)
        		{        		
            		if (chks.item(i).checked) 
            		{                       	
            			if(selects[i].value == '')
            			{
            				alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            				return false;
            			}
            		}                        	         
        		}
        	}
        </ds:extras>
                                      
        for (var i=0; i < chks.length; i++)
        {
            if (chks.item(i).checked) checkedCount++;
        }
        //byl if test dla !multiRealizationAssignment dla tej petli

        if (countDoRealizacji() > 1)
        {
        	alert('<ds:lang text="MoznaWybracTylkoJednaDekretacjeTypuRealizacja"/>');
        	//alert('Mozna wybrac tylko jedna dekretacje typu realizacja');
            return false;
        }

        if (checkedCount == 0)
        {
			alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
            return false;
        }        
        return true;
    }

function updateCheckbox(select)
{
    var id = select.name.substring(select.name.indexOf('_')+1);
    E(id).checked = (select.value.length > 0);
}

function countDoRealizacji()
{
    var selects = document.getElementsByTagName('select');
    var count = 0;
    for (var i=0; i < selects.length; i++)
    {
        //alert(selects[i].value);
        if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
        {
            count++;
        }
    }
    return count;
}
    
function checkSelection()
{
 /*   var selects = document.getElementsByTagName('select');
    var count = 0;
    for (var i=0; i < selects.length; i++)
    {
        //alert(selects[i].value);
        if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
        {
            count++;
        }
    }*/
    //alert(count);
    <ww:if test="multiRealizationAssignment">
        if (countDoRealizacji() > 1)
            alert('<ds:lang text="WybranoWiecejNizJednaDekretacjeRealizacja.KlikniecieDekretujSpowodujeDekretacjeDoWiecejNizJednejOsoby"/>.');
    </ww:if>}

function updateSelect(checkbox)
{

    var id = checkbox.value;
    var sels = document.getElementsByName('assignmentProcess_'+id);
    if (checkbox.checked && sels && sels.length > 0)
    {
        var sel = sels[0];
        if (sel.options && sel.options.length > 0)
            sel.options.selectedIndex = 1;
    } else
	{
		var sel = sels[0];
		if (sel.options && sel.options.length > 0)
		{
			sel.options.selectedIndex = 0;
		}
	}

}
 function checkSubstitutions()
    {
    var count=0;
    var show=0;
    assignments=document.forms[0].assignments;
    var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
    var str1;
    try{
    <ww:iterator value="substituted" status="status">
    	
    	if(assignments[count].checked && "<ww:property value="value"/>"!="") {
    		show=show+1;
    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyPrzez"/> <ww:property value="value"/>";
    		str=str+str1;
    	}
    	count=count+1;
    </ww:iterator>
     }catch(err){
    <ww:iterator value="substituted" status="status">
    	
    	if(assignments.checked && "<ww:property value="value"/>"!="") {
    		show=show+1;
    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyPrzez"/> <ww:property value="value"/>";
    		str=str+str1;
    	}
    </ww:iterator>
    }
    if(show == 0){
    	if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
    	return false;
    }
    else{
    	if(!confirm(str))
    	return false;
  	}
    
    
    return true;
    
    }

</script>
<!--N koniec summary.jsp N-->