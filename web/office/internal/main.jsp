<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<div class="test-div" style="display: none;" id="pageType">office-addIncomingDocument-overall</div>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" id="<ww:property value='name'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</div>
</ds:available>

<p></p>

<ww:set name="exists" value="documentId != null"/>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="replyDocumentId!=null">
	<ww:if test="replyDocument!=null">
		
		<ds:lang text="OdpowiadaszNaPismo"/> 
		<ww:if test="replyDocument.officeNumber != null">
			KO:<a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="replyDocumentId"/></ww:url>"> <ww:property value="replyDocument.officeNumber"/> </a>
		</ww:if>
		<ww:else>
			ID:<a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="replyDocumentId"/></ww:url>"> <ww:property value="replyDocumentId"/> </a>
		</ww:else>	
		<ds:lang text="odUzytkownika"/> <ww:property value='replyAuthor'/>
	</ww:if>
</ww:if>

<script language="JavaScript">
	
	// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
	// jako anonimowego
	var senderFields = new Array(
		'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
		'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
		'senderStreetAddress');
	
	function disableEnableSender(checkbox)
	{
	
		for (var i=0; i < senderFields.length; i++)
		{
			document.getElementById(senderFields[i]).disabled = checkbox.checked;
			//document.getElementById(senderFields[i]).style.background = checkbox.checked ? '#A5967C' : '#BCB09D';
		}
	}
	
	// wype�nia bie��c� dat� i czasem pola informuj�ce o dacie przyj�cia dokumentu
	function fillToday()
	{
		var incomingDate = document.getElementById('incomingDate');
		var incomingTime = document.getElementById('incomingTime');
		var d = new Date();
		var year = d.getYear() >= 1900 ? d.getYear() : 1900 + d.getYear();
	
		incomingDate.value = (d.getDate() < 10 ? '0'+d.getDate() : d.getDate()) +
			'-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1) +
			'-'+year;
		incomingTime.value = (d.getHours() < 10 ? '0'+d.getHours() : d.getHours()) +':'+
			(d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes());
	}
	
	// funkcja wywo�ywana przez okienko z people-directory-results.jsp
	function __people_directory_pick(addresseeType, title, firstname, lastname,
		organization, organizationDivision, street, zip, location, country, email, fax)
	{
		if (addresseeType == 'sender')
		{
			document.getElementById('senderTitle').value = title;
			document.getElementById('senderFirstname').value = firstname;
			document.getElementById('senderLastname').value = lastname;
			document.getElementById('senderOrganization').value = organization;
			document.getElementById('senderStreetAddress').value = street;
			document.getElementById('senderZip').value = zip;
			document.getElementById('senderLocation').value = location;
			document.getElementById('senderCountry').value = country;
			document.getElementById('senderEmail').value = email;
			document.getElementById('senderFax').value = fax;
		}
		else if (addresseeType == 'recipient')
		{
			document.getElementById('recipientTitle').value = title;
			document.getElementById('recipientFirstname').value = firstname;
			document.getElementById('recipientLastname').value = lastname;
			document.getElementById('recipientOrganization').value = organization;
			document.getElementById('recipientDivision').value = organizationDivision;
			document.getElementById('recipientStreetAddress').value = street;
			document.getElementById('recipientZip').value = zip;
			document.getElementById('recipientLocation').value = location;
			//document.getElementById('recipientCountry').value = country;
			document.getElementById('recipientEmail').value = email;
			document.getElementById('recipientFax').value = fax;
		}
	}
	
	// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
	var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
	// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
	// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
	// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
	var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
		'streetAddress', 'zip', 'location', 'country', 'email', 'fax');
	
	// lparam - identyfikator
	function openPersonPopup(lparam, dictionaryType)
	{
		openToolWindowX('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true','person',700,650);
	}
	
	// lparam - identyfikator
	// wparam - pozycja na li�cie
	function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
	{
		//alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
		var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
		//alert('url='+url);
		openToolWindowX(url,'person',700,650);
	}
	
	function personSummary(map)
	{
		var s = '';
		if (map.title != null && map.title.length > 0)
			s += map.title;
	
		if (map.firstname != null && map.firstname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.firstname;
		}
	
		if (map.lastname != null && map.lastname.length > 0)
		{
			if (s.length > 0) s += ' ';
			s += map.lastname;
		}
	
		if (map.organization != null && map.organization.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.organization;
		}
	
		if (map.street != null && map.street.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.street;
		}
	
		if (map.location != null && map.location.length > 0)
		{
			if (s.length > 0) s += ' / ';
			s += map.location;
		}
	
		return s;
	}
	
	/*
		Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
	*/
	function __accept_person(map, lparam, wparam)
	{
		if (lparam == 'recipient')
		{
			var sel = document.getElementById('odbiorcy');
	
			//if (isEmpty(map.firstname) && isEmpty(map.lastname) && isEmpty(map.organization))
			//	return;
			//addOption(sel, JSONStringify(map), personSummary(map));
	
			if (wparam != null && (''+wparam).length > 0)
			{
				setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
			}
			else
			{
				addOption(sel, JSONStringify(map), personSummary(map));
			}
		}
		else if (lparam == 'sender')
		{
			document.getElementById('senderTitle').value = map.title;
			document.getElementById('senderFirstname').value = map.firstname;
			document.getElementById('senderLastname').value = map.lastname;
			document.getElementById('senderOrganization').value = map.organization;
			document.getElementById('senderStreetAddress').value = map.street;
			document.getElementById('senderZip').value = map.zip;
			document.getElementById('senderLocation').value = map.location;
			document.getElementById('senderCountry').value = map.country;
			document.getElementById('senderEmail').value = map.email;
			document.getElementById('senderFax').value = map.fax;
		}
	}
	
	function markReturned()
	{
		document.getElementById('returned').checked = !isNaN(parseInt(document.getElementById('returnReasonId').value));
	}

</script>

<ww:set name="blocked" scope="request" value="blocked" />

<form id="test-overall" action="<ww:url value='&apos;/office/internal/main.action&apos;'/>" method="post" enctype="multipart/form-data" onsubmit="return disableFormSubmits(this);">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<ww:hidden name="'filestoken'" value="filestoken"/>
	<ww:hidden id="replyDocumentId" name="'replyDocumentId'" value="replyDocumentId"/>
	<input type="hidden" name="doCreate" id="doCreate"/>
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<input type="hidden" name="doIntoJournal" id="doIntoJournal"/>
	<input type="hidden" name="doAssignOfficeNumber" id="doAssignOfficeNumber"/>
	<input type="hidden" name="attachCase" id="attachCase"/>
	<%--	<input type="hidden" name="doCancelDocument" id="doCancelDocument"/>--%>
	<input type="hidden" name="assignOfficeNumber" id="assignOfficeNumber"/>
	<ww:hidden name="'documentId'" value="documentId"/>
	<ww:hidden name="'activity'" value="activity"/>
	<ww:hidden name="'inDocumentId'" value="inDocumentId"/>
	<!-- akcja update w wypadku wyst�pienia b��d�w pomija FillDocumentForm, a wi�c ustawianie pola internal. -->
	<ww:hidden name="'internal'"/>
	<ww:token />

	<ww:if test="#exists">
		<ww:if test="blocked">
			<p><span style="color: red">
				<ds:lang text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja"/>.
			</span></p>
		</ww:if>
		<p>
			<ds:lang text="Autor"/>:
			<b><ww:property value="creatingUser"/></b>
			<br/>
			<ds:lang text="Dzial"/>:
			<b><ww:property value="assignedDivision"/></b>
			<br/>
			<ds:lang text="NumerKancelaryjny"/>:
			<b><ww:property value="officeNumber"/></b>
			<br/>
			<ww:if test="repliedDocument!=null"> 
				<ds:lang text="PismoJestOdpowiedziaNaDokument"/> KO:<a href="<ww:url value="'/office/internal/summary.action'"><ww:param name="'documentId'" value="document.repliedDocumentId"/></ww:url>"> <ww:property value="repliedDocument.officeNumber"/> </a>
				<br/>
			</ww:if>
			<ds:extras test="!business">
				<ds:lang text="SymbolWsprawie"/>:
				<b><ww:property value="caseDocumentId"/></b>
				<br/>
			</ds:extras>
			<ww:iterator value="journalMessages">
				<ds:lang text="NumerWdziale"/> '<ww:property value='key'/>':
				<b><ww:property value='value'/></b>
				<br/>
			</ww:iterator>
		</p>
	</ww:if>
	
	<ww:if test="currentDayWarning">
		<p class="warning">
		<ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.
		</p>
	</ww:if>
	
	
	<table border="0" cellspacing="0" cellpadding="0">
		<ww:if test="intoJournalPermission && incomingDivisions != null && incomingDivisions.size() > 0">
			<tr>
				<td>
					<ds:lang text="PrzyjmijPismoWwydziale"/>:
				</td>
				<td>
					<ww:select name="'intoJournalId'" list="incomingDivisions" cssClass="'sel'"
						headerKey="''" headerValue="getText('select.wybierz')" />
					<input type="submit" name="doIntoJournal" class="btn" value="<ds:lang text="Przyjmij"/>"
						onclick="document.getElementById('doIntoJournal').value='true'"/>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td>
				<ds:lang text="DataPisma"/>:
			</td>
			<td>
				<ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
			</td>
		</tr>
		<tr>
			<td valign="top">
				<ds:lang text="OpisPisma"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textarea name="'summary'" rows="3" cols="47" cssClass="'txt'" id="summary"/>
			</td>
			<%--	<td><ww:textfield name="'summary'" size="50" maxlength="80" cssClass="'txt'" id="summary"/></td>--%>
		</tr>
		<ds:extras test="!business">
			<tr>
				<td>
					<ds:lang text="ZnakPisma"/>:
				</td>
				<td>
					<ww:textfield name="'referenceId'" size="50" maxlength="60" cssClass="'txt'" id="referenceId"/>
				</td>
			</tr>
		</ds:extras>
		
		<tr>
			<td>
				<ds:lang text="KodKreskowy"/>:
			</td>
			<td>
				<ww:textfield name="'barcode'" size="50" maxlength="25" cssClass="'txt'" id="barcode"/>
			</td>
		</tr>
		<ww:if test="#exists">
			<tr>
				<td>
					<ds:lang text="ReferentPisma"/>:
				</td>
				<td>
					<ww:select name="'clerk'" list="users" listKey="name"
						listValue="asLastnameFirstname()" value="clerk"
						headerKey="''" headerValue="getText('brak')"  
						cssClass="'sel'" />
				</td>
			</tr>
		</ww:if>

<ww:if test="documentId == null">
	<ww:if test="loadFiles != null && loadFiles.size() > 0">
	<tr>
		<td colspan="1">
			<ds:lang text="PlikiJuzobrane"/>:
		</td>
	</tr>
	</ww:if>
	<ww:iterator value="loadFiles">
		<tr>
			<td colspan="1">
				<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
				<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
			</td>
		</tr>
	</ww:iterator>
	<tr>
		<td colspan="2">
		<table id="kontener">
			<tbody id="tabela_glowna">
				<tr>
					<td><ds:lang text="Plik" /><span class="star">*</span>:</td>
				</tr>
				<tr>
					<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
				</tr>
			</tbody>
		</table>
		</td>
	</tr>
	<tr>
		<td><a href="javascript:addAtta();"><ds:lang
			text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
	</tr>
</ww:if>

		<tr>
			<td></td>
			<td>
				<p></p>
				<ww:if test="documentId == null">
					<input type="submit" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" 
						onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';"/>
					<input type="submit" name="doCreate" value="Zapisz i dodaj do sprawy" class="btn"
						onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';document.getElementById('attachCase').value = 'true';" />
				</ww:if>
				<ww:else>
					<input type="submit" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" <ww:if test="blocked || save">disabled="disabled"</ww:if>
						onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value = 'true';"/>
				</ww:else>
			</td>
		</tr>
	</table>
	
	<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script type="text/javascript">
	//disableEnableSender(document.getElementById('senderAnonymous'));
	if($j('#documentDate').length > 0)
	{
		Calendar.setup({
			inputField	 :	"documentDate",	 // id of the input field
			ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
			button		 :	"documentDateTrigger",  // trigger for the calendar (button ID)
			align		  :	"Tl",		   // alignment (defaults to "Bl")
			singleClick	:	true
		});
	}
	<%--	<ww:if test="useBarcodes">
	var bi = new BarcodeInput({
		'prefix' : '<ww:property value="barcodePrefix"/>',
		'suffix' : '<ww:property value="barcodeSuffix"/>',
		'inputId' : 'barcode',
		'submit' : false,
		//'submitId' : 'send',
		'tabStarts' : false
	});
	</ww:if>	--%>
	
	function validateForm()
	{
		var summary = document.getElementById('summary').value;
		if (summary.trim().length == 0) { alert('<ds:lang text="NiePodanoOpisuPisma"/>'); return false; }
		if (summary.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNajwyzej80znakow"/>'); return false; }
	
		return true;
	}
	
	var countAtta = 1;
	function addAtta() 
	{
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
		countAtta++;
		tr.appendChild(td);
		var kontener = document.getElementById('tabela_glowna');
		kontener.appendChild(tr);

	}

	function delAtta() 
	{
		var x = document.getElementById('tabela_glowna');
		var y = x.rows.length;
/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
	}

</script>
