<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N replies.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
				 java.util.Date,
				 org.apache.commons.beanutils.DynaBean,
				 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="PismoWewnetrzne"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>


<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>


	<table>
		<ww:if test="replies==null||replies.empty">
			<tr>
				<td>
					<p><ds:lang text="NaToPismoNieUdzielonoOdpowiedzi"/>.</p>
					<hr class="highlightedText" style="width:110; height:1; size:1;" align="left"/>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td>			
				<ww:if test="replies==null||replies.empty">
					<ds:lang text="AbyUtworzycOdp"/>
				</ww:if>
				<ww:else>
					<ds:lang text="AbyUtworzycKolejnaOdp"/>
				</ww:else>
				<a href="<ww:url value="'/office/internal/new.action'">
				<ww:param name="'replyDocumentId'" value="documentId"/>
				<ww:param name="'inActivity'" value="activity"/></ww:url>" name='newReply'>
				<ds:lang text="KliknijTutaj"/></a>			
			</td>
		</tr>
	</table>

<ww:if test="replies != null && !replies.empty"> 
	<hr class="highlightedText" style="width:110; height:1; size:1;" align="left"/>	
	
	<table width="100%">
		<tr>
			<td colspan="4">
					<ds:lang text="AktualneOdpowiedzi"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="NumerKO"/>
			</td>
			<td>
				<ds:lang text="DataDokumentu"/>
			</td>
			<td>
				<ds:lang text="Opis"/>
			</td>
		</tr>
		<ww:iterator value="replies">
			<tr>
				<td>
					<a href="<ww:url value="'/office/internal/summary.action?documentId='+id"/>"><ww:property value="officeNumber"/></a>
				</td>
				<ww:if test="documentDate != null">
					<ww:set name="documentDate" scope="page" value="documentDate"/>
					<td>
						<a href="<ww:url value="'/office/internal/summary.action?documentId='+id"/>"><fmt:formatDate value="${documentDate}" type="both" pattern="dd-MM-yy"/></a>
					</td>
				</ww:if>
				<ww:else>
					<td></td>
				</ww:else>
				<td>
					<a href="<ww:url value="'/office/internal/summary.action?documentId='+id"/>"><ww:property value="summary"/></a> 
				</td>		
			</tr>
		</ww:iterator>
	</table>
</ww:if> 

<script>
function odpowiedz1()
{
	document.getElementById('checkboxik').checked='checked';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
	document.getElementById('ukryj_zalacznik').style.display=
		((document.getElementById('ukryj_zalacznik').style.display=="none")?true:false)?"":"none";
}
function odpowiedz2()
{
	document.getElementById('checkboxik').checked='';
	document.getElementById('doUpdate').value='true';
	formularz.submit();
	document.getElementById('ukryj_zalacznik').style.display=
			((document.getElementById('ukryj_zalacznik').style.display=="none")?true:false)?"":"none";
}
</script>