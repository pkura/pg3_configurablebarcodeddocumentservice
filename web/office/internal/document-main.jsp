<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N document-main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:include page="/office/common/inc-title.jsp"/>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">

// lista p�l, kt�re powinny by� zablokowane po oznaczeniu nadawcy
// jako anonimowego
var senderFields = new Array(
    'senderTitle', 'senderFirstname', 'senderLastname', 'senderOrganization',
    'senderZip', 'senderLocation', 'senderCountry', 'senderEmail', 'senderFax',
    'senderStreetAddress');

// wype�nia bie��c� dat� i czasem pola informuj�ce o dacie przyj�cia dokumentu
function fillToday()
{
    var incomingDate = document.getElementById('incomingDate');
    var incomingTime = document.getElementById('incomingTime');
    var d = new Date();
    var year = d.getYear() >= 1900 ? d.getYear() : 1900 + d.getYear();

    incomingDate.value = (d.getDate() < 10 ? '0'+d.getDate() : d.getDate()) +
        '-'+(d.getMonth()+1 < 10 ? '0'+(d.getMonth()+1) : d.getMonth()+1) +
        '-'+year;
    incomingTime.value = (d.getHours() < 10 ? '0'+d.getHours() : d.getHours()) +':'+
        (d.getMinutes() < 10 ? '0'+d.getMinutes() : d.getMinutes());
}

// funkcja wywo�ywana przez okienko z people-directory-results.jsp
function __people_directory_pick(addresseeType, title, firstname, lastname,
    organization, organizationDivision, street, zip, location, country, email, fax)
{
    if (addresseeType == 'sender')
    {
        document.getElementById('senderTitle').value = title;
        document.getElementById('senderFirstname').value = firstname;
        document.getElementById('senderLastname').value = lastname;
        document.getElementById('senderOrganization').value = organization;
        document.getElementById('senderStreetAddress').value = street;
        document.getElementById('senderZip').value = zip;
        document.getElementById('senderLocation').value = location;
        document.getElementById('senderCountry').value = country;
        document.getElementById('senderEmail').value = email;
        document.getElementById('senderFax').value = fax;
    }
    else if (addresseeType == 'recipient')
    {
        document.getElementById('recipientTitle').value = title;
        document.getElementById('recipientFirstname').value = firstname;
        document.getElementById('recipientLastname').value = lastname;
        document.getElementById('recipientOrganization').value = organization;
        document.getElementById('recipientDivision').value = organizationDivision;
        document.getElementById('recipientStreetAddress').value = street;
        document.getElementById('recipientZip').value = zip;
        document.getElementById('recipientLocation').value = location;
        //document.getElementById('recipientCountry').value = country;
        document.getElementById('recipientEmail').value = email;
        document.getElementById('recipientFax').value = fax;
    }
}

// link bazowy do PeopleDirectoryAction, zawiera ju� parametry doSearch i style
var peopleDirectoryLink = '<c:out value="${pageContext.request.contextPath}${peopleDirectoryLink}" escapeXml="false" />';
// identyfikatory p�l formularza, z kt�rych nale�y pobra� dane do wywo�ania
// PeopleDirectoryAction - nazwy musz� by� poprzedzone nazw� typu osoby
// np: "senderTitle" lub "recipientTitle" przed odwo�aniem si� do nich
var addresseeFields = new Array('title', 'firstname', 'lastname', 'organization',
    'streetAddress', 'zip', 'location', 'country', 'email', 'fax');

// lparam - identyfikator
function openPersonPopup(lparam, dictionaryType)
{
    openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true','person',700,650);
}

// lparam - identyfikator
// wparam - pozycja na li�cie
function openEditPersonPopup(ognlMap, wparam, lparam, dictionaryType)
{
    //alert('ognlMap='+ognlMap+', wparam='+wparam+', lparam='+lparam);
    var url = '<ww:url value="'/office/common/person.action'"/>?doEdit=true&ognlMap='+escape(ognlMap)+'&wparam='+wparam+'&lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true';
    //alert('url='+url);
    openToolWindow(url,'person',700,650);
}

function personSummary(map)
{
    var s = '';
    if (map.title != null && map.title.length > 0)
        s += map.title;

    if (map.firstname != null && map.firstname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.firstname;
    }

    if (map.lastname != null && map.lastname.length > 0)
    {
        if (s.length > 0) s += ' ';
        s += map.lastname;
    }

    if (map.organization != null && map.organization.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.organization;
    }

    if (map.street != null && map.street.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.street;
    }

    if (map.location != null && map.location.length > 0)
    {
        if (s.length > 0) s += ' / ';
        s += map.location;
    }

    return s;
}

/*
    Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
*/
function __accept_person(map, lparam, wparam)
{
    if (lparam == 'recipient')
    {
        var sel = document.getElementById('odbiorcy');

        //if (isEmpty(map.firstname) && isEmpty(map.lastname) && isEmpty(map.organization))
        //    return;
        //addOption(sel, JSONStringify(map), personSummary(map));

        if (wparam != null && (''+wparam).length > 0)
        {
            setOption(sel, parseInt(wparam), JSONStringify(map), personSummary(map));
        }
        else
        {
            addOption(sel, JSONStringify(map), personSummary(map));
        }
    }
    else if (lparam == 'sender')
    {
        document.getElementById('senderTitle').value = map.title;
        document.getElementById('senderFirstname').value = map.firstname;
        document.getElementById('senderLastname').value = map.lastname;
        document.getElementById('senderOrganization').value = map.organization;
        document.getElementById('senderStreetAddress').value = map.street;
        document.getElementById('senderZip').value = map.zip;
        document.getElementById('senderLocation').value = map.location;
        document.getElementById('senderCountry').value = map.country;
        document.getElementById('senderEmail').value = map.email;
        document.getElementById('senderFax').value = map.fax;
    }
}

function markReturned()
{
    document.getElementById('returned').checked = !isNaN(parseInt(document.getElementById('returnReasonId').value));
}

</script>

<ww:if test="currentDayWarning">
    <p class="warning"><ds:lang text="DzienOtwartyWaplikacjiNieJestZgodnyZbiezacaData"/>.</p>
</ww:if>

<form id="form" action="<ww:url value='baseLink'/>" method="post" enctype="multipart/form-data"
    onsubmit="return disableFormSubmits(this);">

    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="createNextDocument" id="createNextDocument"/>
    <ww:hidden name="'inDocumentId'"/>
	<ww:hidden name="'boxId'"/>
	<ww:hidden name="'filestoken'" value="filestoken"/>
<table border="0" class="formTable">
<ds:available test="documentMain.int.officeDate"  documentKindCn="documentKindCn">
<tr>
    <td><ds:lang text="DataPisma"/>:</td>
    <td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
    </td>
</tr>
<tr>
    <td valign="top"><ds:lang text="OpisPisma"/>:</td>
    <td><ww:textarea name="'description'" rows="2" cols="50" cssClass="'txt'" id="description"/></td>
<%--    <td><ww:textfield name="'description'" size="50" maxlength="80" cssClass="'txt'" id="description"/></td>--%>
</tr>
</ds:available>
<tr>
    <td>
        <ds:lang text="RodzajDokumentuDocusafe"/>:
    </td>
    <td>
        <ds:degradable-select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="cn" listValue="name" value="documentKindCn" cssClass="'sel'"
            onchange="'changeDockind();'"/>
    </td>
</tr>
<ww:if test="documentAspects != null">
		<tr>
			<td>
				<ds:lang text="AspektDokumentu"/>:
			</td>
			<td>
				<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
			</td>
		</tr>
</ww:if>


<ww:if test="!normalKind">
    <tr class="formTableDisabled">
        <td colspan="2"><br/><h4><ds:lang text="DaneArchiwum"/></h4></td>
    </tr>
</ww:if>

<jsp:include page="/common/dockind-fields.jsp"/>

<ww:if test="canAddToRS">
<tr>
    <td><ds:lang text="NumerSzkody"/>:</td>
    <td><ww:textfield name="'NR_SZKODY'" id="NR_SZKODY" size="30" maxlength="30" cssClass="'txt"/></td>
</tr>
</ww:if>
<ds:available test="documentMain.int.ArchiwizacjaWOtwartymPudle"  documentKindCn="documentKindCn">
	<tr>
	    <td><ds:lang text="ArchiwizacjaWOtwartymPudle"/><ww:if test="boxId != null"> (<ww:property value="boxName"/>)</ww:if>:</td>
	    <td><ww:checkbox name="'addToBox'" fieldValue="true" disabled="boxId == null"/></td>
	</tr>
</ds:available>
<ww:iterator value="loadFiles">
	<tr>
		<td colspan="1">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
		</td>
	</tr>
</ww:iterator>
    <tr class="formTableDisabled" id="file-tr">
            <td colspan="2">
            <table id="kontener">
                    <tbody id="tabela_glowna">
                            <tr>
                                    <td><ds:lang text="Plik " /><ds:available test="documentMain.check.attachment"><span class="star always">*</span></ds:available>:</td>
                            </tr>
                            <tr>
                                    <td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
                            </tr>
                    </tbody>
            </table>
            </td>
    </tr>
<tr class="formTableDisabled" id="file-tr-button">
	<td><a href="javascript:addAtta();"><ds:lang
		text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
</tr>
<jsp:include page="/common/dockind-specific-additions.jsp"/>

<tr class="formTableDisabled">
    <td></td>
    <td>
        <p></p>
        <input type="submit" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
        onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';"/>

		<ww:if test="showCreateNext">
	        <input type="submit" name="doCreate2" value="<ds:lang text="ZapiszNext"/>" class="btn saveBtn"
	        onclick="$j('#form').unbind('submit');if (!validateForm()) return false; document.getElementById('doCreate').value = 'true'; document.getElementById('createNextDocument').value='true';" />
	    </ww:if>
    </td>
</tr>
</table>

<p><span class="star always">*</span><ds:lang text="PoleObowiazkowe"/></p>

</form>

<script type="text/javascript">
if($j('#documentDate').length > 0)
{
	Calendar.setup({
	    inputField     :    "documentDate",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
}

    <ds:available test="documentMain.attachment.ukryj" documentKindCn="documentKindCn">
        $j('#file-tr').css('display','none');
        $j('#file-tr-button').css('display','none');
    </ds:available>

function validateForm()
{
<ds:available test="documentMain.int.officeDate" documentKindCn="documentKindCn" >
    var description = document.getElementById('description').value;
    //if (description.trim().length == 0) { alert('<ds:lang text="NiePodanoOpisuPisma"/>'); return false; }
    if (description.length > 80) { alert('<ds:lang text="PodanyOpisPismaJestZbytDlugi.OpisMozeMiecCoNajwyzej80znakow"/>'); return false; }
</ds:available>
    if (!validateDockind())
        return false;
<ds:available test="documentMain.check.attachment">
    if (!validateFiles())
        return false;
</ds:available>
    return true;
}

var countAtta = 1;
function addAtta()
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);

}

function delAtta()
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;

	/*
		dwa obowiazkowe wiersze:
	
		Plik: *
		[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}

</script>

<ds:available test="archiwizacja.dokument.updatujListeUzytkownika" documentKindCn="documentKindCn">
    <jsp:include page="/common/update-users-field.jsp"/>
</ds:available>

<%-- JE�LI DOCKIND == 'absence-request' WYPISUJE LISTE URLOP�W PRACOWNIKA / ROZWI�ZANIE TYMCZASOWE --%>
<jsp:include page="/common/employee-absences.jsp" />

<%-- JE�LI DOCKIND == 'overtime_app' WYPLISUJE KARTE NADGODZIN  --%>
<jsp:include page="/common/employee-overtime-card.jsp"/>
