<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %> 	
		
	<table>	
	<tr>
		<td>
				<ds:lang text="TypDokumentow"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'typDokumentu'" id="typDokumentu" list="typyDokumentow" headerKey="''" headerValue="'wybierz'"  
				cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Lokalizacja"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'lokalizacja'" id="lokalizacja" list="locations" listKey="id" headerKey="''" headerValue="'wybierz'"  
				listValue="name+' '+zip+' '+city" cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			Odbiorca paczki<span class="star">*</span>
		</td>
		<td>
			<ww:select name="'odbiorcaLokalizacja'" id="odbiorcaLokalizacja" list="locations" listKey="id" headerKey="''" headerValue="'wybierz'"  
				listValue="name+' '+zip+' '+city" cssClass="'sel'"/>
		</td>
	</tr>
<!-- 	 
	<tr>	
		<td>
			<ds:lang text="RodzajPrzesylki"/>
		</td>
		<td>
			<ww:select name="'rodzajPrzesylki'" id="rodzajPrzesylki" list="rodzajePrzesylki" listKey="ordinal()"
				listValue="name()" cssClass="'sel'"/>
		</td>
	</tr>
-->
	<tr>
		<td>
			<ds:lang text="Opis"/>
		</td>
		<td>
			<ww:textarea name="'opis'" id="opis" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
	
	<tr>
		<td>
			<ds:lang text="transportServiceName"/>
		</td>
		<td>
			<ww:textarea name="'transportServiceName'" id="transportServiceName" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
</table>