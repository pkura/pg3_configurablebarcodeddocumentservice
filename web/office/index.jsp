<%@ page import="pl.compan.docusafe.core.cfg.Configuration,
                 pl.compan.docusafe.common.Modules"%>
<%
    if (Configuration.moduleAvailable(Modules.MODULE_OFFICE) ||
        Configuration.moduleAvailable(Modules.MODULE_FAX))
    {
        response.sendRedirect(request.getContextPath()+"/office/tasklist/current-user-task-list.action"); 
    }
    else
    {
        response.sendRedirect(request.getContextPath()+"/office/find-office-documents.action"); 
    }
%>