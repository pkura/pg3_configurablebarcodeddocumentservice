<%@ page import="pl.compan.docusafe.util.DateUtils,pl.compan.docusafe.core.office.DSPermission" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Potwierdzenie wykupu bilet�w"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/office/confirm-tickets-purchase.action'"></ww:url>" method="post" >
<table class="mediumTable search userBilling" >

	<tr>
		<td colspan="2">Numer zestawienia:
			<ww:textfield name="'nrZestawienia'" id="nrZestawienia" size="10" cssClass="''"/>
	        <input type="submit" value="Znajd�" name="doSearch" class="btn"/>
		</td>
	</tr>
	
	<tr>
		<th>
				Numer rezerwacji
		</th>
		<th>
				Pracownik delegowany
		</th>
		<th>
				Cel podr�y
		</th>
		<th>
				Data wyjazdu
		</th>
		<th>
				Data powrotu
		</th>
		<th>
				Termin wykupienia biletu
		</th>
	</tr>

		<ww:iterator value="bilety">

			<tr>
				<td><ww:property value="nrRezerwacji" /></td>
				<td><ww:property value="workerName" /></td>
				<td><ww:property value="tripPurpose" /></td>
				<td><ds:format-date value="startDate" pattern="dd-MM-yy" /></td>
				<td><ds:format-date value="finishDate" pattern="dd-MM-yy" /></td>
				<td><ds:format-date value="purchaseDate" pattern="dd-MM-yy" />
				</td>
			</tr>
		</ww:iterator>



		<tr>
			<td colspan="1">
				<ww:if test="showGenerujButton">
					<ds:submit-event value="getText('Potwierd� wykupienie bilet�w z zestawienia')" name="'doConfirm'" />
				</ww:if>
			</td>
	</tr>
</table>
</form>