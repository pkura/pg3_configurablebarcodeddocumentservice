<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>

<script type="text/javascript" src="<ww:url value="'/jquery.expander.js'" />" ></script>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
	</div>
</ds:available>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/office/mass-returns.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>


<input name="postalRegNumberChain" id="postalRegNumberChain"  class="txt" hidden="true"></input>


<h1>Masowe przyjmowanie zwrotek</h4>

<table id="tableList">
	<tr align="left">
		<td align="left">
			<ds:lang text="Data zwrotu:" />
		</td>
		<td>
			<ww:textfield name="'returnsDate'"  size="10" maxlength="10" cssClass="'txt'" id="returnsDate"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer listu poleconego:" />
		</td>
		<td>
			<input name="'inputValue'" id="inputValue" class="txt" />
		</td>
		<td>
			<input type="button" class="btn" onClick="addElement();" value="<ds:lang text="Dodaj"/>"/>
		</td>
	</tr>


<tr>
<div id="msg" style="color:red"></div>
</tr>

</table>

<table>
	<tr>
		<td>
			<ds:event name="'doSave'" value="'Zapisz'" cssClass="'btn saveBtn'" />
		</td>
	</tr>
</table>


<ds:available test="layout2">
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>

<script language="javascript">

var counter = 0;
var chainArray = new Array();

function addElement(){
	
	var inputValue = document.getElementById('inputValue').value;

	if(inputValue==""){

		document.getElementById('msg').style.display="block";
		document.getElementById('msg').innerHTML = "Nie wprowadzono numeru przesy�ki rejestrowanej";
	}else{
		document.getElementById('msg').innerHTML = "";
		var tr = document.createElement('tr');
		tr.setAttribute('id','tr_' + counter);
		
		var td1 = document.createElement('td');
		var td2 = document.createElement('td');
		var td3 = document.createElement('td');
		
		var container = document.getElementById('tableList');
		container.insertBefore(tr, container.nextSlibling);
		
		var dispText = document.createTextNode("");
		
		var textField = document.createElement('input');
		textField.setAttribute('id','text_' + counter);
		textField.setAttribute('name','text_' + counter);
		textField.setAttribute('class','txt');
		textField.setAttribute('style','width: 200px;');
		textField.setAttribute('value',inputValue);
		
		var deleteButton= document.createElement('input');
		deleteButton.setAttribute('type','button');
		deleteButton.setAttribute('name','btn_' + counter);
		deleteButton.setAttribute('class','btn deleteBtn');
		deleteButton.setAttribute('value','<ds:lang text="Usu� "/>');
		deleteButton.setAttribute('onclick','removeRow(tr_' + counter +', ' + counter +');');
		
		
		tr.insertBefore(td3, tr.firstChild);
		td3.insertBefore(deleteButton, td3.firstChild);
		tr.insertBefore(td2, tr.firstChild);
		td2.insertBefore(textField, td2.firstChild);
		tr.insertBefore(td1, tr.firstChild);
		
		document.getElementById('inputValue').value="";
		
		chainArray[counter] = document.getElementById('text_' + counter);
		counter = counter + 1;
		
		concatPostalRegNumbers()
	}
}

function removeRow(variable, counter){
	document.getElementById(variable.id).remove();
	chainArray.splice(counter,1);
	concatPostalRegNumbers()
}

function concatPostalRegNumbers() {
	var chain = document.getElementById('postalRegNumberChain');
	chain.value = "";
	for (var i = 0, len = chainArray.length; i < len ; i++) {
		chain.value = chain.value + ',' +  chainArray[i].value;
	}
	
}

$j('#inputValue').keypress(function(event) {
	if (event.which===13) {
		addElement();
	}
	return true;
});

Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = 1, len = this.length; i < len; i++) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}


function clearProjectEntry() {
	$j('#ProjectEntry').children('tbody').children('tr:not(.buttonsRow)').each(function() {
		$j(this).children('td').children('input, select, textarea').val('');
	});
	$j('#entryId').val('');
}

if($j('#returnsDate').length > 0)
{
    Calendar.setup({
        inputField     :    "returnsDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "startDateTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
}
    

    
</script>
