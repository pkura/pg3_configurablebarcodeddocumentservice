<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N barcodes.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZamowBarkody"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p>
	<ds:xmlLink path="Barcodes"/>
</p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/barcodes-ordering.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

		<p></p>
		<table>
            <tr>
 				<td><ds:lang text="IloscBarkodow"/>:</td>
				<td><ww:textfield name="'ilbar'" size="20" cssClass="'txt'" id="ilbar"/></td>
            <tr>
            	<td><ds:lang text="OpisBarkodow"/>:</td>
				<td><ww:textfield name="'opbar'" size="52" cssClass="'txt'" id="opbar"/></td>
            </tr>
            <tr>
                <td></td>
                <td><ds:event name="'doOrder'" value="getText('Zam�wBarkody')"/></td>
            </tr>
        </table>
        
</form>