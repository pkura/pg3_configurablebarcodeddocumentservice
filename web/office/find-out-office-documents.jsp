<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-out-office-documents.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>

<ww:if test="!internal">
	<h1><ds:lang text="WyszukiwaniePismWychodzacych"/></h1>	
</ww:if>
<ww:else>
	<h1><ds:lang text="WyszukiwaniePismWewnetrznych"/></h1>
</ww:else>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="results == null || results.count() == 0">
	<form name="formul" action="<ww:url value="'/office/find-office-documents.action'"/>" method="post">
		<ww:hidden name="'tab'"/>
		<ww:hidden name="'internal'"/>
					<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
		<table>
			<tr>
				<td>
					<ds:lang text="NumerKO"/>:
				</td>
				<td>
					<ww:textfield name="'officeNumber'" size="5" maxlength="6" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Rok"/>:
				</td>
				<td>
					<ww:textfield name="'officeNumberYear'" size="5" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="ZakresNumerowKO"/>:
				</td>
				<td>
					<ds:lang text="od"/>
					<ww:textfield name="'fromOfficeNumber'" size="4" maxlength="5" cssClass="'txt'"/>
					<ds:lang text="do"/>
					<ww:textfield name="'toOfficeNumber'" size="4" maxlength="5" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="DataPisma"/>:
				</td>
				<td>
					<ww:textfield name="'documentDateFrom'" id="documentDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_documentDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
					<ww:textfield name="'documentDateTo'" id="documentDateTo" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_documentDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/>
				</td>
			</tr>
			
			<ww:if test="!internal">
				<tr>
					<td>
						<ds:lang text="DataZPO"/>:
					</td>
					<td>
						<ww:textfield name="'zpoDateFrom'" id="zpoDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="calendar_zpoDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
						<ww:textfield name="'zpoDateTo'" id="zpoDateTo" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>"
							id="calendar_zpoDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
							title="Date selector" onmouseover="this.style.background='red';"
							onmouseout="this.style.background=''"/>
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="NumerPrzesylkiRejestrowanej"/> (R):
					</td>
					<td>
						<ww:textfield name="'postalRegNumber'" size="20" maxlength="20" cssClass="'txt'" id="postalRegNumber" />
					</td>
				</tr>
			</ww:if>
			
			<ds:available test="kodKreskowy.wyszukiwarka">
				<tr>
					<td>
						<ds:lang text="KodKreskowy"/>:
					</td>
					<td>
						<ww:textfield name="'barcode'" id="barcode" size="30" maxlength="25" cssClass="'txt'"/>
					</td>
				</tr>
			</ds:available>
			
			<tr>
				<td>
					<ds:lang text="OpisPisma"/>:
				</td>
				<td>
					<ww:textfield name="'summary'" size="30" maxlength="80" cssClass="'txt'"/>
				</td>
			</tr>
			
			<ds:extras test="!business">
				<tr>
					<td>
						<ds:lang text="ZnakPisma"/>:
					</td>
					<td>
						<ww:textfield name="'referenceId'" size="30" maxlength="60" cssClass="'txt'"/>
					</td>
				</tr>
			</ds:extras>
			
			<ww:if test="!internal">
				<tr>
					<td>
						<ds:lang text="Nadawca"/>:
					</td>
					<td>
						<ww:textfield name="'sender'" id="sender" size="30" maxlength="50" cssClass="'txt'"/>
						<input type="button" value="<ds:lang text="WybierzNadawce"/>"
							onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" >
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="Odbiorca"/>
					</td>
					<td>
						<ww:textfield name="'recipient'" id="recipient" size="30" maxlength="50" cssClass="'txt'"/>
						<input type="button" value="<ds:lang text="WybierzOdbiorce"/>"
							onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_SENDER %>');" class="btn" >
					</td>
				</tr>
			</ww:if>
			
			<ds:available test="dziennik.wyszukiwarka">
				<tr>
					<td>
						Poka� dzienniki z roku:
					</td>
					<td>
						<ww:select name="'selectedYear'" list="yearsList" headerKey="''" cssClass="'sel'" onchange="'changeJournal()'"/>																														
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="Dziennik"/>:
					</td>
					<td>
						<ww:select name="'journalId'" list="journals" cssClass="'sel'"
							headerKey="''" headerValue="getText('select.wszystkie')"/>
					</td>
				</tr>
			</ds:available>
			
			<ww:if test="!internal">
				<tr>
					<td>
						<ds:lang text="SposobOdbioru"/>:
					</td>
					<td>
						<ww:select name="'deliveryId'" list="outDeliveries" listKey="id" listValue="name"
							headerKey="''" headerValue="getText('select.wybierz')"
							id="delivery" cssClass="'sel'"/>
					</td>
				</tr>
			</ww:if>
			
			<%--	<tr>
				<td>
					Nadawca:
				</td>
				<td>
					<ww:textfield name="'sender'" size="30" maxlength="60" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					Rodzaj pisma:
				</td>
				<td>
					<ww:select name="'kind'" list="kinds"
						listKey="id" listValue="name" cssClass="'sel'"
						headerKey="''" headerValue="'-- dowolny --'"/>
				</td>
				<p></p>
			</td>
			</tr>	--%>
			
			<ds:extras test="!business">
				<tr>
					<td>
						<ds:lang text="OsobaPrzyjmujaca"/>:
					</td>
					<td>
						<ww:select name="'creatingUser'" cssClass="'sel'" list="accessedByUsers"
							listKey="name" listValue="lastname+' '+firstname"
							headerKey="''" headerValue="getText('select.wybierz')"/>
					</td>
				</tr>
			</ds:extras>
			
			<ds:available test="referent.wyszukiwarka">
				<tr>
					<td>
						<ds:lang text="ReferentPisma"/>:
					</td>
					<td>
						<ww:select name="'assignedUser'" cssClass="'sel'" list="accessedByUsers"
							listKey="name" listValue="lastname+' '+firstname"
							headerKey="''" headerValue="getText('select.wybierz')"/>
					</td>
				</tr>
			</ds:available>
			
			<ds:available test="uzytkownikPracujacyNaDokumencie.wyszukiwarka">
				<tr>
					<td valign="top">
						<ds:lang text="UzytkownikPracujacyNaDokumencie"/>:
					</td>
					<td>
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top">
									<ww:select name="'accessedBy'" list="accessedByUsers" cssClass="'sel'"
										listKey="name" listValue="asLastnameFirstname()"
										headerKey="''" headerValue="getText('select.wybierz')" />
								</td>
								<td valign="top">
									<ww:select id="accessedAs" name="'accessedAs'" size="4" multiple="true" cssClass="'multi_sel'"
										list="@pl.compan.docusafe.core.base.DocumentChangelog@all()"
										listKey="what" listValue="description"/>
									<br/>
									<a href="javascript:void(select(document.getElementById('accessedAs'), true))"><ds:lang text="zaznacz"/></a>
									/
									<a href="javascript:void(select(document.getElementById('accessedAs'), false))"><ds:lang text="odznacz"/></a>
									<ds:lang text="wszystkie"/>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</ds:available>
			<tr>
					<td>
						<ds:lang text="LiczbaWynikowNaStronie"/>:
					</td>
					<td>
						<ww:select name="'limit'" id="limit" cssClass="'sel'" list="#{ 10: 10, 50: 50, 100: 100 }"
							value="limit > 0 ? limit : 50"/>
					</td>
				</tr>
			
			<ds:available test="flagi.wyszukiwarka">
				<ww:if test="internal && flagsOn">
					<tr>
						<td valign="top">
							<ds:lang text="Flagi"/>:
						</td>
						<td>
							<nobr>
								<ww:iterator value="flags">
									<ww:checkbox  name="'flagIds'" fieldValue="id" value="'false'"/>
									<span title="<ww:property value="description"/>"><ww:property value="c"/></span>
								</ww:iterator>
							</nobr>
							<br/>
							
							<ds:lang text="nadaneOd"/>
							<ww:textfield name="'flagsDateFrom'" id="flagsDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_flagsDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
							<ds:lang text="do"/>
							<ww:textfield name="'flagsDateTo'" id="flagsDateTo" size="10" maxlength="10" cssClass="'txt'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_flagsDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
						</td>
					</tr>
					<tr>
						<td valign="top">
							<ds:lang text="FlagiUzytkownika"/>:
						</td>
						<td>
							<nobr>
								<ww:iterator value="userFlags">
									<ww:checkbox  name="'userFlagIds'" fieldValue="id" value="'false'"/>
									<span title="<ww:property value="description"/>"><ww:property value="c"/></span>
								</ww:iterator>
							</nobr>
							<br/>
							<ds:lang text="nadaneOd"/>
							<ww:textfield name="'userFlagsDateFrom'" id="userFlagsDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_userFlagsDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
							<ds:lang text="do"/>
							<ww:textfield name="'userFlagsDateTo'" id="userFlagsDateTo" size="10" maxlength="10" cssClass="'txt'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_userFlagsDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
						</td>
					</tr>
				</ww:if>
			</ds:available>
			
			<%--	<tr>
				<td>
					Czas wykonania up�ywa za:
				</td>
				<td>
					<html:text property="expiresIn" size="3" styleClass="txt_opt"/>
					<html:select property="expiresInMod" styleClass="sel_opt">
						<html:option value="1">lub wi�cej dni</html:option>
						<html:option value="-1">lub mniej dni</html:option>
						<html:option value="0">dni</html:option>
					</html:select>
					albo up�yn��
					<html:text property="expiredBy" size="3" styleClass="txt_opt"/>
					<html:select property="expiredByMod" styleClass="sel_opt">
						<html:option value="1">lub wi�cej dni</html:option>
						<html:option value="-1">lub mniej dni</html:option>
						<html:option value="0">dni</html:option>
					</html:select>
					temu
				</td>
			</tr>
			<tr>
				<td>
					Liczba wynik�w na stronie:
				</td>
				<td>
					<html:select property="limit" styleClass="sel" >
						<html:option value="10">10</html:option>
						<html:option value="25">25</html:option>
						<html:option value="50">50</html:option>
					</html:select>
				</td>
			</tr>	--%>
			
			<tr>
				<td colspan="2">
					<ds:submit-event name="'doSearchOut'" value="getText('Szukaj')" cssClass="'btn searchBtn'"/>
					<%--	<input type="submit" name="doSearchOut" class="btn" value="Szukaj"/>	--%>
					<ds:submit-event name="'doClearFields'" cssClass="btn" value="getText('WyczyscWszystkiePola')"/>
				</td>
			</tr>
		</table>
				    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	</form>
	
	<script type="text/javascript">
		function changeJournal()
		{			
			document.forms[0].submit();			
		}
		
		Calendar.setup({
			inputField     :    "documentDateFrom",     // id of the input field
			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
			button         :    "calendar_documentDateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "documentDateTo",     // id of the input field
			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
			button         :    "calendar_documentDateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		<ww:if test="internal && flagsOn">
			<ds:available test="flagi.wyszukiwarka">
				Calendar.setup({
					inputField     :    "flagsDateFrom",     // id of the input field
					ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
					button         :    "calendar_flagsDateFrom_trigger",  // trigger for the calendar (button ID)
					align          :    "Tl",           // alignment (defaults to "Bl")
					singleClick    :    true
				});
				Calendar.setup({
					inputField     :    "flagsDateTo",     // id of the input field
					ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
					button         :    "calendar_flagsDateTo_trigger",  // trigger for the calendar (button ID)
					align          :    "Tl",           // alignment (defaults to "Bl")
					singleClick    :    true
				});
				Calendar.setup({
					inputField     :    "userFlagsDateFrom",     // id of the input field
					ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
					button         :    "calendar_userFlagsDateFrom_trigger",  // trigger for the calendar (button ID)
					align          :    "Tl",           // alignment (defaults to "Bl")
					singleClick    :    true
				});
				Calendar.setup({
					inputField     :    "userFlagsDateTo",     // id of the input field
					ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
					button         :    "calendar_userFlagsDateTo_trigger",  // trigger for the calendar (button ID)
					align          :    "Tl",           // alignment (defaults to "Bl")
					singleClick    :    true
				});
			</ds:available>
		</ww:if>
		
        <ww:if test="!internal">
			Calendar.setup({
				inputField     :    "zpoDateFrom",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_zpoDateFrom_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
			Calendar.setup({
				inputField     :    "zpoDateTo",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_zpoDateTo_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
		</ww:if>
		
		<ds:available test="uzytkownikPracujacyNaDokumencie.wyszukiwarka">
		</ds:available>

		// lparam - identyfikator
		function openPersonPopup(lparam, dictionaryType)
		{
			if (lparam == 'sender')
			{
				openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true');
			}
			else
			{
			openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true');
			}
		}

		/*
			Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
		*/
		function __accept_person(map, lparam, wparam)
		{
			//alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);
			var txt;

			if (lparam == 'recipient')
			{
				txt = document.getElementById('recipient');
			}
			else if (lparam == 'sender')
			{
				txt = document.getElementById('sender');
			}

			if (!isEmpty(map.lastname))
				txt.value += map.lastname + " ";
			if (!isEmpty(map.organization))
				txt.value += map.organization + " ";
			if (!isEmpty(map.firstname))
				txt.value += map.firstname + " ";
			if (!isEmpty(map.street))
				txt.value += map.street + " ";
			if (!isEmpty(map.zip))
				txt.value += map.zip
		}
	</script>
</ww:if>

<ww:else>
	<ww:hidden name="'limit'" id="limit"/>
				<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table width="100%" class="search">
		<tr>
			<th>
				<nobr>
					<%--	<a href="<ww:url value="getSortLink('officeNumber', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('officeNumber', false)"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
						<ww:if test="(sortField == 'officeNumber' && ascending == false) || sortField == null"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
						<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>					
					<ds:lang text="NumerKO"/>
					<a href="<ww:url value="getSortLink('officeNumber', true)"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
						<ww:if test="sortField == 'officeNumber' && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
						<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
					<%--	<a href="<ww:url value="getSortLink('officeNumber', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr>
			</th>
			<th>
				<nobr>
					<%--	<a href="<ww:url value="getSortLink('documentDate', false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
					<a href="<ww:url value="getSortLink('documentDate', false)"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
						<ww:if test="sortField == 'documentDate' && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
						<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>					
					<ds:lang text="DataDokumentu"/>
					<a href="<ww:url value="getSortLink('documentDate', true)"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
						<ww:if test="sortField == 'documentDate' && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
						<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
					<%--	<a href="<ww:url value="getSortLink('documentDate', true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
				</nobr>
			</th>
			
			<ww:if test="!internal">
				<th>
					<ds:lang text="Nadawca"/>
				</th>
				<th>
					<ds:lang text="GlownyOdbiorca"/>
				</th>
			</ww:if>
			
			<th>
				<ds:lang text="OpisPisma"/>
			</th>
			<%--	<th>
				Typ
			</th>	--%>
		</tr>
		
		<ww:set name="linkBase" value="(internal ? '/office/internal' : '/office/outgoing') + '/summary.action?documentId='"/>
		<ww:iterator value="results">
			<tr>
				<td>
					<a href="<ww:url value="#linkBase + id"/>"><ww:property value="formattedOfficeNumber"/></a>
				</td>
				
				<ww:if test="documentDate != null">
					<ww:set name="documentDate" scope="page" value="documentDate"/>
					<td>
						<a href="<ww:url value="#linkBase + id"/>"><fmt:formatDate value="${documentDate}" type="both" pattern="dd-MM-yy"/></a>
					</td>
				</ww:if>
				<ww:else>
					<td></td>
				</ww:else>
				
				<ww:if test="!internal">
					<td>
						<a href="<ww:url value="#linkBase + id"/>"><ww:property value="sender.summary"/></a>
					</td>
				</ww:if>
				
				<ww:if test="!internal">
					<ww:set name="recipient" value="recipients.{^ true }"/>
					<ww:if test="#recipient.empty">
						<td></td>
					</ww:if>
					<ww:else>
						<td>
							<a href="<ww:url value="#linkBase + id"/>"><ww:property value="#recipient[0].summary"/></a>
						</td>
					</ww:else>
				</ww:if>
				
				<td>
					<a href="<ww:url value="#linkBase + id"/>"><ww:property value="title"/></a>
				</td>
				
				<!-- typ -->
				<!--	<td>
					<a href="<ww:url value="#linkBase + id"/>">
						<ww:if test="doctype != null"><ww:property value="doctype.name"/></ww:if>
						<ww:else></ww:else></a>
				</td>	-->
			</tr>
		</ww:iterator>
	</table>
	
	<ww:set name="pager" scope="request" value="pager"/>
	<table width="100%">
		<tr>
			<td align="center">
				<jsp:include page="/pager-links-include.jsp"/>
			</td>
		</tr>
	</table>
	
	<ds:extras test="business">
		<input type="button" class="btn" value="<ds:lang text="PobierzZalacznikiJakoPDF"/>"
			onclick="openToolWindow('<ww:url value="attachmentsAsPdfUrl"/>', 'asPdf')"/>
	</ds:extras>
	
	<input type="button" class="btn" value="<ds:lang text="PobierzRaportXLS"/>"
		onclick="document.location='<ww:url value="getSortLink('officeNumber', false)"/>&generateXls=true'"/>
			    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:else>