<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 04.08.08
	Poprzednia zmiana: 04.08.08
C--%>
<!--N roles.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Role"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/admin/roles">
	<c:if test="${!empty roles}">
		<ds:lang text="Nazwa"/>
		<c:forEach items="${roles}" var="role">
			<br/>
			<input type="checkbox" name="roleIds" value="<c:out value='${role.id}'/>"/>
			<a href='<c:out value="${pageContext.request.contextPath}"/>/office/admin/edit-role-permissions.action?id=<c:out value='${role.id}'/>'>
				<c:out value="${role.name}"/></a>
		</c:forEach>
		
		<br/>
		<input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn"/>
	</c:if>
		
	<h4><ds:lang text="NowaRola"/></h4>
	<ds:lang text="Nazwa"/>:
	<input type="text" name="roleName" size="30" class="txt"/>
	<input type="submit" name="doCreate" value="<ds:lang text="NowaRola"/>" class="btn"/>
</html:form>

<%--
<h1><ds:lang text="Role"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/admin/roles">

<c:if test="${!empty roles}">
	<table>
	<tr>
		<th></th>
		<th><ds:lang text="Nazwa"/></th>
	</tr>
	<c:forEach items="${roles}" var="role">
		<tr>
			<td><input type="checkbox" name="roleIds" value="<c:out value='${role.id}'/>"/></td>
<%--			<td><a href='<c:out value="${pageContext.request.contextPath}${role.editLink}"/>'><c:out value="${role.name}"/></a></td>-->
			<td><a href='<c:out value="${pageContext.request.contextPath}"/>/office/admin/edit-role-permissions.action?id=<c:out value='${role.id}'/>'><c:out value="${role.name}"/></a></td>
		</tr>
	</c:forEach>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn"/>
		</td>
	</tr>
	</table>
</c:if>

<h4><ds:lang text="NowaRola"/></h4>

<table>
<tr>
	<td><ds:lang text="Nazwa"/>:</td>
	<td><input type="text" name="roleName" size="30" class="txt"/></td>
	<%--<td><html:text property="roleName" size="30" styleClass="txt"/></td>-->
	<td><input type="submit" name="doCreate" value="<ds:lang text="NowaRola"/>" class="btn"/></td>
</tr>
</table>

</html:form>
--%>

<!--N koniec roles.jsp N-->