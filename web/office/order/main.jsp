<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils,
                 pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<p></p>

<ww:set name="exists" value="documentId != null"/>


<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value='&apos;/office/order/main.action&apos;'/>" method="post" enctype="multipart/form-data" onsubmit="return disableFormSubmits(this);">
    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="doUpdate" id="doUpdate"/>
    <input type="hidden" name="doChangeStatus" id="doChangeStatus"/>
    <ww:hidden name="'documentId'" value="documentId"/>
    <ww:hidden name="'activity'" value="activity"/>
    <ww:hidden name="'relatedOfficeDocumentId'" value="relatedOfficeDocumentId"/>
    <ww:hidden name="'filestoken'" value="filestoken"/>
    <ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    

    <ww:if test="#exists">
        <p>
            Polecenie przyj�te przez u�ytkownika: <b><ww:property value="creatingUser"/></b> <br/>
            Dzia�: <b><ww:property value="assignedDivision"/></b> <br/>
            Numer polecenia: <b><ww:property value="officeNumber"/></b> <br/>
        </p>
    </ww:if>

    <ww:if test="currentDayWarning">
        <p class="warning">Dzie� otwarty w aplikacji nie jest zgodny z bie��c� dat�.</p>
    </ww:if>

    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Termin wykonania:</td>
            <td><ww:textfield name="'finishDate'" size="10" maxlength="10" cssClass="'txt'" id="finishDate"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="finishDateTrigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
            </td>
        </tr>
        <tr>
            <td>Opis polecenia<span class="star">*</span>:</td>
            <td><ww:textarea name="'description'" rows="3" cols="47" cssClass="'txt'" id="description"/></td>
        </tr>
        <tr>
            <td>Szczeg�owa tre��<span class="star">*</span>:</td>
            <td><ww:textarea name="'detailedDescription'" rows="5" cols="47" cssClass="'txt'" id="detailedDescription"/></td>
        </tr>
        <tr>
            <td>Priorytet polecenia:</td>
            <td><ww:select name="'priority'" id="priority"
                list="priorities" listKey="key" listValue="value" value="priority"
                cssClass="'sel'"/>
            </td>
        </tr>
        <ww:if test="#exists">
            <tr>
                <td>Status polecenia:</td>
                <td><ww:select name="'status'" id="status"
                    list="statusMap" listKey="key" listValue="value" value="status"
                    cssClass="'sel'"/>
                    <ww:if test="canChangeStatus">
                        &nbsp;<input type="submit" name="doChangeStatus" value="Zmie� status" class="btn"
                            onclick="document.getElementById('doChangeStatus').value = 'true';"/>
                    </ww:if>
                </td>
            </tr>
            <tr>
                <td>Referent:</td>
                <td><ww:select name="'assignedUser'" list="users" listKey="name"
                    listValue="asLastnameFirstname()" value="assignedUser"
                    headerKey="''" headerValue="'-- brak --'"
                    cssClass="'sel'" />
                </td>
            </tr>
        </ww:if>

<ww:if test="loadFiles != null && loadFiles.size() > 0">
<tr>
	<td colspan="1">
		<ds:lang text="PlikiJuzobrane"/>:
	</td>
</tr>
</ww:if>
<ww:iterator value="loadFiles">
	<tr>
		<td colspan="1">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>">
		</td>
	</tr>
</ww:iterator>
<tr>
	<td colspan="2">
	<table id="kontener">
		<tbody id="tabela_glowna">
			<tr>
				<td><ds:lang text="Plik" /><span class="star">*</span>:</td>
			</tr>
			<tr>
				<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" value="'C:\Program Files\Apache Software Foundation\Tomcat 6.0\work\Catalina\localhost\docusafe\1-5.TIF'"/></td>
			</tr>
		</tbody>
	</table>
	</td>
</tr>
<tr>
	<td><a href="javascript:addAtta();"><ds:lang
		text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
</tr>		
        <tr>
            <td></td>
            <td>
                <p></p>
                <ww:if test="documentId == null">
                    <input type="submit" name="doCreate" value="Zapisz" class="btn" 
                    onclick="if (!validateForm()) return false; document.getElementById('doCreate').value = 'true';"/>

          <%--          <input type="submit" name="doCreate" value="Zapisz i nadaj numer KO" class="btn"
                    <ww:if test="!canCreateWithOfficeNumber">disabled="true"</ww:if>
                    onclick="if (!validateForm()) return false; document.getElementById('assignOfficeNumber').value = 'true'; document.getElementById('doCreate').value = 'true';"/> --%>
                </ww:if>
                <ww:else>
                    <input type="submit" name="doUpdate" value="Zapisz" class="btn" <ww:if test="!canEdit">disabled=disabled</ww:if>
                    onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value = 'true';"/>
                </ww:else>
           <%--     <ww:if test="#exists && officeNumber == null && canAssignOfficeNumber">
                    <input type="submit" name="doAssignOfficeNumber" value="Nadaj numer kancelaryjny"
                    class="btn" onclick="setEvent(this)"/>
                </ww:if>   --%>
            </td>
        </tr>
    </table>
     <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script type="text/javascript">
//disableEnableSender(document.getElementById('senderAnonymous'));
	if($j('#finishDate').length > 0)
	{
		Calendar.setup({
		    inputField     :    "finishDate",     // id of the input field
		    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    button         :    "finishDateTrigger",  // trigger for the calendar (button ID)
		    align          :    "Tl",           // alignment (defaults to "Bl")
		    singleClick    :    true
		});
	}
	
function validateForm()
{
    var description = document.getElementById('description').value;
    if (description.trim().length == 0) { alert('Nie podano opisu polecenia'); return false; }
    if (description.length > 80) { alert('Podany opis polecenia jest zbyt d�ugi. Opis mo�e mie� co najwy�ej 80 znak�w'); return false; }

    var detailedDescription = document.getElementById('detailedDescription').value;
    if (detailedDescription.trim().length == 0) { alert('Nie podano szczeg�owej tre�ci polecenia'); return false; }
    if (detailedDescription.length > 4000) { alert('Podana szczeg�owa tre�� polecenia jest zbyt d�uga. Tre�� mo�e mie� co najwy�ej 4000 znak�w'); return false; }

    return true;
}

var countAtta = 1;
function addAtta() 
{
	var tr = document.createElement("tr");
	var td = document.createElement("td");
	td.innerHTML = '<input type="file" name="multiFiles' + countAtta + '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
	countAtta++;
	tr.appendChild(td);
	var kontener = document.getElementById('tabela_glowna');
	kontener.appendChild(tr);

}

function delAtta() 
{
	var x = document.getElementById('tabela_glowna');
	var y = x.rows.length;
	/*
	dwa obowiazkowe wiersze:

	Plik: *
	[______________] Przegladaj
	*/
	if(y > 2)
		x.deleteRow(y - 1);
}
</script>