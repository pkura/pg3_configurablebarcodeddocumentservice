<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N summary.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1>Polecenie</h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
	<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
		<ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
	</ww:iterator>
</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form action="<ww:url value="'/office/order/summary.action'"/>" method="post">

<ww:hidden name="'documentId'"/>
<ww:hidden name="'activity'"/>
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

<table width="100%">
    <tr>
        <td width="50%">Numer polecenia: <b><ww:property value="document.officeNumber"/></b></td>
        <td></td>
    </tr>
    <tr>
        <td width="50%">ID: <b><ww:property value="document.id"/></b></td>
        <td>
        </td>
    </tr>
    <tr>
        <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
        <ww:if test="document != null and document.ctime != null">
            <ww:set name="ctime" value="document.ctime" scope="page"/>
        </ww:if>
        <td width="50%" valign="top">Przyj�te przez: <b><ww:property value="creatingUser"/></b>
            dnia <b><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></b>
        </td>
        <td width="50%" valign="top">Dzia�: <b><ww:property value="assignedDivision"/></b></td>
    </tr>
    <tr>
        <td>
        <ww:if test="relatedOfficeDocument != null">
            <ww:if test="relatedOfficeDocument.officeNumber != null">
                Polecenie zwi�zane jest z pismem <ww:property value="relatedDocumentName"/> o nr
                <a href="<ww:url value="'/office/'+relatedDocumentKind+'/summary.action'"/>?documentId=<ww:property value="relatedOfficeDocument.id"/>">
                    <ww:property value="relatedOfficeDocument.officeNumber"/> (<ww:property value="relatedOfficeDocument.caseDocumentId"/>)
                </a> <br/>
            </ww:if>
            <ww:else>
                Polecenie zwi�zane jest z pismem <ww:property value="relatedDocumentName"/> o ID
                <a href="<ww:url value="'/office/'+relatedDocumentKind+'/summary.action'"/>?documentId=<ww:property value="relatedOfficeDocument.id"/>">
                    <ww:property value="relatedOfficeDocument.id"/> (<ww:property value="relatedOfficeDocument.caseDocumentId"/>)
                </a> <br/>
            </ww:else>
        </ww:if>
        </td>
        <td></td>
    </tr>
    <tr>
        <td valign="top">
            <ww:if test="document.remarks.size > 0">
                Ostatnia uwaga: <b><ww:property value="document.remarks[document.remarks.size-1].content"/></b>
                dodana przez <b><ww:property value="recentRemark.author"/></b>.
            </ww:if>
        </td>
        <td valign="top">
            Tre��: <b><ww:property value="document.summary"/>
        </td>
    </tr>
    <tr>
        <td valign="top" colspan="2">
            Szczeg�owa tre�� polecenia: <br /><b><ww:property value="document.detailedDescription"/></b>
        </td>
    </tr>
</table>

<ds:modules test="office">
    <ww:if test="attachments.size > 0">
        <h4>Za��czniki</h4>

        <table>
        <tr>
            <th>Tytu�</th>
            <th>Autor</th>
            <%--<th>Rozmiar</th>--%>
            <th>Data</th>
            <%--<th>Wersja</th>--%>
            <th>Kod kreskowy</th>
            <th></th>
        </tr>

        <ww:iterator value="attachments">
            <tr>
                <td><ww:property value="title"/></td>
                <td><ww:property value="userSummary"/></td>
                <ww:if test="mostRecentRevision != null">
                    <ww:set name="revision" value="mostRecentRevision"/>
                    <ww:set name="size" scope="page" value="#revision.size"/>
                    <ww:set name="ctime" scope="page" value="#revision.ctime"/>
                    <%--<td><ww:property value="#u.firstname + ' ' + #u.lastname"/></td>--%>
                    <%--<td><edm-fmt:format-size size="${size}"/></td>--%>
                    <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
                    <%--<td><ww:property value="#revision.revision"/></td>--%>
                    <td><ww:property value="barcode"/></td>
                    <td><a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#revision.id"/>">pobierz najnowsz� wersj�</a></td>
                </ww:if>
                <ww:else>
                    <td colspan="3">
                        <ww:if test="lparam != null">
                            <b><i><ww:property value="@pl.compan.docusafe.core.office.InOfficeDocument@getAttachmentLparamDescription(lparam)"/></i></b>
                        </ww:if>
                        <ww:else>
                            <i>(pusty)</i>
                        </ww:else>
                    </td>
                </ww:else>
            </tr>
        </ww:iterator>
        </table>

    </ww:if>

	<ww:if test="enableCollectiveAssignments">

        <h4>Zbiorcza dekretacja</h4>

        <ww:if test="assignmentsMap == null or assignmentsMap.isEmpty">
            <p><i>Administrator systemu nie skonfigurowa� domy�lnej listy
            zbiorczej dekretacji, ale mo�esz zrobi� to sam w swoich ustawieniach.
            Aby skonfigurowa� swoje w�asne ustawienia zbiorczej dekretacji
            kliknij <a href="<ww:url value="'/settings/user-collective-assignment.action'"/>">tutaj</a>.
            Do tych ustawie� b�dziesz m�g� p�niej przej�� klikaj�c na odno�niku
            "Ustawienia" po lewej stronie ekranu, a nast�pnie na "Zbiorcza dekretacja -
            ustawienia u�ytkownika".
            </i></p>
        </ww:if>
        <ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
            <table>
                <ww:iterator value="assignmentsMap" >
                    <tr>
                        <td><input type="checkbox" name="assignments" value="<ww:property value="key"/>" id="<ww:property value="key"/>"
                            onchange="updateSelect(this);"/></td>
                        <td><ww:property value="value"/></td>
                        <td>
                            <select name="assignmentProcess_<ww:property value="key"/>" id='assignmentSelect'
                                class="sel" onchange="updateCheckbox(this);checkSelection()">
                                <option value="">-- wybierz --</option>
                                <ww:iterator value="wfProcesses">
                                    <option value="<ww:property value="key"/>"><ww:property value="value"/></option>
                                </ww:iterator>
                            </select>
                        </td>
                    </tr>
                </ww:iterator>
                <tr>
                    <td colspan="3">
                        <%--<ds:event name="'doAssignments'" value="'Dekretuj'" confirm="'Na pewno dekretowa�?'"/>   tak bylo, nie wiem dlaczego? --%>
                        <ds:event name="'doAssignments'" value="getText('Dekretuj')" onclick="'if (!validateAssignments()) return false; if(!checkSubstitutions()) return false;'" />
                        <%--<ds:event name="'doManualFinish'" value="'Zako�cz prac� z dokumentem'" disabled="!canFinish"/>--%>
                    </td>
                </tr>
            </table>
        </ww:if>

    </ww:if>

 <%--   <ds:event name="'doReopenWf'" value="'Przywr�� na list� zada�'" disabled="!canReopenWf"/>  --%>
 <ww:if test="activity != null">
    <ds:event name="'doManualFinish'" value="'Zako�cz prac� z poleceniem'" onclick="'if (!confirmFinish()) return false'" disabled="!canFinish"/>
 </ww:if>
 <ds:event name="'doWatch'" value="'Dodaj do Obserwowanych'"/>
    <%--   <ds:extras test="!nationwide">
        <ds:event name="'doClone'" value="'Klonuj'"/>
    </ds:extras>
</ds:modules>

 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script>

    function updateCheckbox(select)
	{
	    var id = select.name.substring(select.name.indexOf('_')+1);
	    E(id).checked = (select.value.length > 0);
	}

	function checkSelection()
    {
        var selects = document.getElementsByTagName('select');
        var count = 0;
        for (var i=0; i < selects.length; i++)
        {
            //alert(selects[i].value);
            if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
            {
                count++;
            }
        }
        //alert(count);
        if (count > 1)
            alert('Wybrano wi�cej ni� jedn� dekretacj� \'realizacja\'. Klikni�cie \'dekretuj\' spowoduje dekretacj� do wi�cej ni� jednej osoby.');
    }

    function updateSelect(checkbox)
    {
       
        var id = checkbox.value;
        var sels = document.getElementsByName('assignmentProcess_'+id);
        if (checkbox.checked && sels && sels.length > 0)
        {
            var sel = sels[0];
            if (sel.options && sel.options.length > 0)
                sel.options.selectedIndex = 1;
        } else
		{
			var sel = sels[0];
			if (sel.options && sel.options.length > 0)
			{
				sel.options.selectedIndex = 0;
			}
		}
       
    }

    function countDoRealizacji()
    {
        var selects = document.getElementsByTagName('select');
        var count = 0;
        for (var i=0; i < selects.length; i++)
        {
            //alert(selects[i].value);
            if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
            {
                count++;
            }
        }
        return count;
    }

    function validateAssignments()
    {
    	var chks = document.getElementsByName('assignments');
    	var checkedCount = 0;
    	 
    	<ds:extras test="!business">               
        	var selects = document.getElementsByTagName('select');
    
			if(selects.length>chks.length)
			{    
        		for (var i=0; i < selects.length; i++)
        		{
        			if(selects[i].id == 'assignmentSelect')
            		{
        				//alert("chks.item(i/2).checked: " + chks.item(i/2).checked + " i= " + i);
            			if (chks.item(i/2).checked) 
            			{                       	
            				if(selects[i].value == '')
            				{
            					alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            					return false;
            				}
            			}            
            		}          
        		}
        	}
        	else
        	{
        		for (var i=0; i < chks.length; i++)
        		{        		
            		if (chks.item(i).checked) 
            		{                       	
            			if(selects[i].value == '')
            			{
            				alert('<ds:lang text="Nie wybrano rodzaju dekretacji"/>');
            				return false;
            			}
            		}                        	         
        		}
        	}
        </ds:extras>
                                      
        for (var i=0; i < chks.length; i++)
        {
            if (chks.item(i).checked) checkedCount++;
        }
        //byl if test dla !multiRealizationAssignment dla tej petli

        if (countDoRealizacji() > 1)
        {
        	alert('<ds:lang text="MoznaWybracTylkoJednaDekretacjeTypuRealizacja"/>');
        	//alert('Mozna wybrac tylko jedna dekretacje typu realizacja');
            return false;
        }

        if (checkedCount == 0)
        {
			alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
            return false;
        }        
        return true;
    }

    function checkSubstitutionsInTempArray(substitutionsArray, key) {
		var result = false;
		if (substitutionsArray.indexOf(key) > -1)
			 result = true;
		return result;
	}
    
    function checkSubstitutions() {
	    var count=0;
	    var show=0;
	    var assignments = document.getElementsByName('assignments');
	    var str = "<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
	    var str1;
	    var tmpSubstitutions = '<ww:property value="substitutedNames.keySet()" />';

	    for (var i=0; i < assignments.length; i++)
	    { 
	        if (!assignments.item(i).checked)
	            continue;
	    
		    try {
		    	var asgm = assignments.item(i).value.substring(0, assignments.item(i).value.indexOf(';'));
			    <ww:iterator value="substituted" status="status">
			    	if(checkSubstitutionsInTempArray(tmpSubstitutions, asgm)) {
			    		show=show+1;
			    		str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowanyAPrzez"/> <ww:property value="value"/>";
			    		str=str+str1;
			    	}
			    	count=count+1;
			    </ww:iterator>
		     } catch(err) { }
	    }
	    
	    if(show == 0){
	    	if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
	    	return false;
	    }
	    else{
	    	if(!confirm(str))
	    	return false;
	  	}
	    return true;
    }

    function confirmFinish()
    {
        <ww:if test="!finishOrAssignState.canFinish">
            alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
            return false;
        </ww:if>
        <ww:if test="!finishOrAssignState.finishWarnings.empty && !documentFax">
            if (!confirm('<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/>\n\n<ds:lang text="ZakonczycPraceZdokumentem"/>'))
                return false;
        </ww:if>
        <ww:else>
           // if (!confirm('Na pewno zako�czy�?'))
           //     return false;
        </ww:else>

      //  document.getElementById('doManualFinish').value='true';

        return true;
    }

</script>
<!--N koniec summary.jsp N-->