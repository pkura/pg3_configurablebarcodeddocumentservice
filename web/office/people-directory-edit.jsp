<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N people-directory-edit.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Edycja danych osoby</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/people-directory">
<html:hidden property="addresseeType"/>
<html:hidden property="style"/>
<html:hidden property="id"/>
<html:hidden property="returnUrl"/>

<table>
<tr>
    <td>Tytu�</td><td>Imi�<span class="star">*</span></td><td>Nazwisko<span class="star">*</span></td>
</tr>
<tr>
    <td><html:text property="title" size="10" maxlength="30" styleClass="txt_opt"/></td>
    <td><html:text property="firstname" size="40" maxlength="50" styleClass="txt"/></td>
    <td><html:text property="lastname" size="40" maxlength="70" styleClass="txt"/></td>
</tr>
<tr>
    <td colspan="3">Firma/urz�d</td>
</tr>
<tr>
    <td colspan="3"><html:text property="organization" size="91" maxlength="100" styleClass="txt_opt"/></td>
</tr>
<tr>
    <td colspan="3">Adres/ulica</td>
</tr>
<tr>
    <td colspan="3"><html:text property="street" size="91" maxlength="100" styleClass="txt_opt"/></td>
</tr>
</table>

<table>
<tr>
    <td>Kod pocztowy</td><td>Miejscowo��</td><td><!--Kraj--></td>
</tr>
<tr>
    <td><html:text property="zip" styleId="zip" size="8" maxlength="14" styleClass="txt_opt"/></td>
    <td><html:text property="location" size="30" maxlength="70" styleClass="txt_opt"/></td>
    <td><%--<html:text property="country" styleId="country" size="2" maxlength="2" styleClass="txt_opt"/>--%>
        <html:hidden property="country" styleId="country" value="PL" /></td>
</tr>
<tr>
    <td colspan="3"><span class="warning" id="zipError"> </span></td>
</tr>
</table>

<table>
<tr>
    <td>Email</td><td>Faks</td>
</tr>
<tr>
    <td><html:text property="email" size="30" maxlength="50" styleClass="txt_opt"/></td>
    <td><html:text property="fax" size="30" maxlength="30" styleClass="txt_opt"/></td>
</tr>
</table>

<input type="submit" name="doUpdate" value="Zapisz" class="btn"/>
<input type="submit" name="doDelete" value="Usu�" class="btn"/>
<input type="submit" name="doReturn" value="Anuluj" class="btn"/>

</html:form>

<p><small><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></small></p>

<script language="JavaScript">
var szv = new Validator(document.getElementById('zip'), '##-###');

szv.onOK = function() {
    this.element.style.color = 'black';
    document.getElementById('zipError').innerHTML = '';
}

szv.onEmpty = szv.onOK;

szv.onError = function(code) {
    this.element.style.color = 'red';
    if (code == this.ERR_SYNTAX)
        document.getElementById('zipError').innerHTML = 'Niepoprawny kod pocztowy, poprawny kod to np. 00-001';
    else if (code == this.ERR_LENGTH)
        document.getElementById('zipError').innerHTML = 'Kod pocztowy musi mie� sze�� znak�w (np. 00-001)';
}

szv.canValidate = function() {
    var country = document.getElementById('country');
    var countryCode = country.value;

    return (countryCode == '' || countryCode == 'PL');
}
</script>
