<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="PrzyjmijPaczkeNaPodstawieBarkodow"/></h1>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="PackageIn"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PackageIn"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form action="<ww:url value="'/office/package-in.action'"/>" method="post" onsubmit="if(!submm()) return false;"> 
	<ds:available test="layout2">
		<div id="middleContainer" style="border-top: none;"> <!-- BIG TABLE start -->
	</ds:available>
	<hr size="1" align="left" class="horizontalLine" width="77%" />
	<i><ds:lang text="WprowadzanieDoSystemuBarkodowZPaczkiDokumentow"/></i>
	<p>
		<ww:include page="/office/package-param.jsp"/>
 		<table id="selectTable">	
 			<tr>
 				<td><ds:lang text="WybierzSposobWprowadzaniaBarkodow"/>:</td>
				<td><select class="sel" onChange="if(!changeSelect()) return false;">
						<option id="wpr0">-- wybierz --</option>
						<option id="wpr1">wprowadzanie czytnikiem</option>
						<option id="wpr2">wprowadzanie z klawiatury</option>
					</select>
				</td>
 			</tr>
 		</table>
	</p>

	<p>
 		<table id="fieldsTable">	
 			<tr>
 				<td><ds:lang text="PodajNrWczytywanejPaczki"/>:<span class="star">*</span></td>
				<td><input name="nrpaczki" class="txt" type="text" size="30" id="nrpaczki"/></td>
				<td><input name="zmiennrpaczki" class="btn" value="<ds:lang text='GenerujNumerPaczki'/>" cssClass="btn" type="button" id="zmiennrpaczki" onclick="generujNumerPaczki();"/></td>
 			</tr>
 			<tr>
 				<td><ds:lang text="NrWczytBarkodu"/>:<span class="star">*</span></td>
				<td><input name="nrbar" class="txt" type="text" size="30" id="nrbar" onchange="if (!validate()) return false;"/></td>
				<td><input name="wpr_klik" class="btn" value="<ds:lang text='WprowadzBarkod'/>" cssClass="btn" type="button" id="wpr_klik" onclick="if (!validate()) return false;"/></td>
 			</tr>
 		</table>
 	</p>

	<p>
		<ww:hidden name="'czyIstnieje'" value="czyIstnieje" id="czyIstnieje"/>
	</p>

	<div id="divbar"></div>	

	<p id="tyt">		
	</p>

	<p id="po">
	</p>	

	<p>
		<table id="barcodeTable" class="tableMargin">
			<tr>		
				<th>
					<ds:lang text='NrWczytywanejPaczki'/>:
				</th>
				<td>
					<b><ww:property value="nr_paczki"/></b>
				</td>
			</tr>
			<tr></tr>
			<tr>		
				<th>
					<ds:lang text="WczytaneBarkody"/>:
				</th>
			</tr>
			<ww:iterator value="packages">
			<tr>	
				<td>
					<ww:textfield name="'barkod'" size="30" cssClass="'txt'" maxlength="30"/>
				</td>	
			</tr>
			</ww:iterator>
		</table>
 	</p>

	<p>
 		<table>	
 			<tr>
 				<td><input class="btn" type="submit" value="<ds:lang text='WprowadzNumery'/>" name="doCreate" id="doCreate" cssClass="btn" onclick="click2_();"/></td>
 				<td><input class="btn" type="submit" value="<ds:lang text='UtworzPisma'/>" name="doAdd" id="doAdd" cssClass="btn" onclick="click_();"/></td>
				<td><input class="btn" name="delete_all" value="<ds:lang text='UsunCalaListe'/>" cssClass="btn" type="button" id="delete_all" onClick="deleteAll();"/></td>
 			</tr>
 		</table>
 	</p>

	<input type="hidden" id="nr_paczki" name="nr_paczki"/>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>

</form>

<script type="text/javascript">

var ilosc=0;
var ktory=0;
var prob1=null;
var bool=false;
var interval_nr_paczki;
var interval;

	$j(document).ready(function ()
	{
		$j("#nrpaczki").attr("readonly", true).addClass("grey");
	//	$j("#nrbar").attr("readonly", true).addClass("grey");
		//$j("#zmiennrpaczki").hide();
		$j("#wpr_klik").hide();
		$j("#doAdd").attr("disabled","disabled");
		
		$j("#barcodeTable").hide();
		
		var liczba_barkodow = $j("#barcodeTable input").length;
		
		if (liczba_barkodow != 0)
		{
			var czy_istnieje = $j("#czyIstnieje").attr("value");
			if (czy_istnieje != 1)
			{
				$j("#barcodeTable").show();
				$j("#doAdd").removeAttr("disabled");
			}	
			$j("#doCreate").attr("disabled","disabled");
			$j("#delete_all").attr("disabled","disabled");
			
			$j("#fieldsTable").hide();
			$j("#selectTable").hide();
			
			$j("#barcodeTable input").each(function()
			{
				$j(this).attr("readonly", true).addClass("grey");
			});
		}
		else{}	
		
    });

	function changeSelect()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		
		if($j("#wpr0").is(":selected"))
		{
			$j("#nrpaczki").attr("readonly", true).addClass("grey");
		//	$j("#nrbar").attr("readonly", true).addClass("grey");
			//$j("#zmiennrpaczki").hide();
			$j("#wpr_klik").hide();
		}				
       	else if ($j("#wpr1").is(":selected"))
       	{	     
			if(nrpaczki == null || nrpaczki.trim().length == 0) 
			{
			//	$j("#zmiennrpaczki").hide();
				$j("#wpr_klik").hide();
				$j("#nrpaczki").attr("readonly", false).removeClass("grey");
				//$j("#nrbar").attr("readonly", false).removeClass("grey");
				setCursor("p1");
				interval_nr_paczki = setInterval("sprawdz('nrpaczki');", 200);	
			}
			else
			{  		
				//$j("#zmiennrpaczki").show();
				$j("#wpr_klik").hide();
				setCursor("p2");
   				interval = setInterval("sprawdz('nrbarkodu');", 200);
       		}
   		}
   		else if ($j("#wpr2").is(":selected"))
       	{
       		if(nrpaczki == null || nrpaczki.trim().length == 0) 
			{
				clearInterval(interval_nr_paczki);
				//$j("#zmiennrpaczki").hide();
				$j("#wpr_klik").show();
				$j("#nrpaczki").attr("readonly", false).removeClass("grey");
				$j("#nrbar").attr("readonly", false).removeClass("grey");
				setCursor("p1");
			}
			else
			{  	
				clearInterval(interval);
			//	$j("#zmiennrpaczki").show();
				$j("#wpr_klik").show();
				setCursor("p2");
       		}   	
   		}
	}

	function setCursor(pole)
	{	
		if(pole == "p1")
		{
			$j("#nrpaczki").focus();
		}
		if(pole == "p2")
		{
			$j("#nrbar").focus();
		}
		
		return true;
	}

	function sprawdz(warunek)
	{
		var nrbar = $j("#nrbar").attr("value");
		var nrpaczki = $j("#nrpaczki").attr("value");
		var sprawdzenie;
		
		if(warunek == "nrbarkodu")
		{
			sprawdzenie = nrbar;
			
		}
		else
		{
			sprawdzenie = nrpaczki;
		}

		if (sprawdzenie == null || sprawdzenie.trim().length == 0) 
		{ 	
			return false;
		}
		else
		{
			if (prob1 != sprawdzenie)
	   		{
   				prob1 = sprawdzenie;
   			}
   			else
   			{
				if(warunek == "nrbarkodu")
				{
					validate();
					prob1= null;
				}
				else
				{
					clearInterval(interval_nr_paczki);
					setCursor("p2");
					prob1= null;
					interval = setInterval("sprawdz('nrbarkodu');", 200);
				}
   			}
		}
	}

	function validate()
	{		
		var nrbar = $j("#nrbar").attr("value");
		var test = true;
		var text;
		
		$j("#nrbar").attr("value", "");
		
		$j("#nrpaczki").attr("readonly", true).addClass("grey");
	//	$j("#zmiennrpaczki").show();
	
		if (nrbar == null || nrbar.trim().length == 0) 
		{ 	
			//window.alert("<ds:lang text='NieWczytanoBarkodu'/>");
			setCursor("p2");
			return false;
		}
		else if(nrbar.length != 12)
		{
			window.alert("<ds:lang text='NieprawidlowaDlugoscBarkodu'/>" + ": " + nrbar); 
			setCursor("p2");
			return false;
		}
		else
		{		 
			//$j("#selbar > option").each(function()
			$j("#divbar > input").each(function()
	  		{
	 			//text = $j(this).text();
	 			text = $j(this).val();
	
				if (nrbar == text) 
				{ 
					window.alert("<ds:lang text='TenBarkodZostalJuzWczytany'/>" + ": " + text); 
					test = false;
				}
	  		});
	  
	  		//if (test && (nrbar != null))
	  		if (test)
	  		{		
	  			ilosc=ilosc+1;	
				//$j("#selbar").html($j("#selbar").html() + "<option id=\"opt_nr" + ilosc + "\">" + nrbar + "</option>");
				$j("#divbar").html($j("#divbar").html() + "<input name=\"kody\" value=\"" + nrbar + "\" id=\"opt_nr" + ilosc + "\" type=\"hidden\" />");
				
				createTable();
	  		}
			
			setCursor("p2");
			
			return true;
		}	
	}	
	
	function createTable()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		var text;
		var id_;
		var nr_lp = 0;
	
		$j("#tyt > div").remove();
		$j("#po > table").remove();

		$j("#po").html($j("#po").html() + "<table id=\"to\">");

  		$j("#divbar > input").each(function()
  		{
  			//text = $j(this).text();
  			text = $j(this).val();
  			id_ = $j(this).attr("id");			

  			if (nr_lp == 0)
  			{
  				$j("#tyt").html($j("#tyt").html() + "<div class=\"bold\"><ds:lang text='NrWczytywanejPaczki'/>: " + "<span class=\"warning\">" +nrpaczki + "</span></div>");
  				$j("#tyt").html($j("#tyt").html() + "<div class=\"bold\"><ds:lang text='WczytaneBarkody'/>:</div>");
  				$j("#to").html($j("#to").html() + "<tr> <td class=\"alignCenter\"><span class=\"bold\"><ds:lang text='lp'/></span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td class=\"alignCenter\"><span class=\"bold\"><ds:lang text='Barkod'/></span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td></td> </tr>");
  			}
  			nr_lp = nr_lp+1;
			$j("#to").html($j("#to").html() + "<tr> <td style=\"width: 3em\" class=\"alignCenter\">" + nr_lp + "</td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td style=\"padding: 0px 5px\"><span id=" + id_ + " style=\"white-space:nowrap\">" + text + "</span></td> <td background=\"/docusafe/img/pionowa-linia.gif\" class=\"s\"></td> <td style=\"width: 3em\" class=\"alignCenter bold\"> <a class=\"barcodeLink\" onClick=\"javascript:deleteLink('" + id_ + "')\">usu�</a></td> </tr>");
  		});	 
  		
  		$j("#po").html($j("#po").html() + "</table>");

		$j("#to tr:odd").addClass("oddRows");
		  		
  		if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 8))
			$j("#to tr").hover(
				function() 
				{
					$j(this).addClass("toHover");
				}, 
				function() 
				{
					$j(this).removeClass("toHover");
				}
			); 
  		
  		return true;
	}
	
	function deleteLink(id_ha)
	{	
	
		if (confirm("<ds:lang text='CzyNaPewnoUsunacBarkod'/>"))
		{			
			//$j("#selbar > option[id='" + id_ha + "']").remove();
			$j("#divbar > input[id='" + id_ha + "']").remove();
		
  			createTable();
		} 
		else 
		{
		}	

  		setCursor("p2");
		
		return true;
	}
	
	function deleteAll()
	{	
		var id_number;
		
		if (confirm("<ds:lang text='CzyNaPewnoUsunacCalaLista'/>"))
		{			
			$j("#divbar > input").each(function()
			{
				//$j("#selbar > option[id='" + id_ha + "']").remove();
				id_number = $j(this).attr("id");
				$j("#divbar > input[id='" + id_number + "']").remove();
			})
  			createTable();
		} 
		else 
		{
		}	

  		setCursor("p2");
		
		return true;
	}
	
	function click2_()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		var nr_paczki;
	
		if (nrpaczki == null || nrpaczki.trim().length == 0)
		{ 	
			window.alert("<ds:lang text='PolePodajNrWczytywanejPaczkiNieMozeBycPuste'/>");	
			setCursor("p1");
		}
		else
		{
			$j("#nr_paczki").val(nrpaczki);
			nr_paczki = $j("#nr_paczki").attr("value");
			bool = true;
		}
	}
	
	function click_()
	{
		var nrpaczki = $j("#nrpaczki").attr("value");
		var nr_paczki;
		
		$j("#nr_paczki").val(nrpaczki);
		nr_paczki = $j("#nr_paczki").attr("value");
		
		bool = true;
	}
	
	function submm()
	{		
		if(bool)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function odblokuj()
	{
	//	$j("#zmiennrpaczki").hide();
		$j("#nrpaczki").attr("readonly", false).removeClass("grey"); 
	}

	function generujNumerPaczki()
	{
		var d = new Date();
		
		document.getElementById('nrpaczki').value = d.getTime();;
	}

</script>