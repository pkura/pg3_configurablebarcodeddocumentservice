<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N people-directory-results.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script language="JavaScript">
function pick(addresseeType, title, firstname, lastname, organization,
    organizationDivision, street, zip, location, country, email, fax)
{
    if (window.opener.__people_directory_pick)
    {
        window.opener.__people_directory_pick(addresseeType, title, firstname, lastname,
            organization, organizationDivision, street, zip, location, country, email, fax);
        window.close();
    }
    else
    {
        alert('W oknie wywo�uj�cym nie zdefiniowano funkcji __people_directory_pick.\nSkontaktuj si� z administratorem.');
    }
}
</script>

<h3>Wyniki wyszukiwania</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/people-directory">
<html:hidden property="addresseeType"/>
<html:hidden property="style"/>

<c:if test="${!empty results}">
    <table width="100%">
    <tr>
        <th>Imi�</th><th>Nazwisko</th><th>Organizacja</th><th>Miejscowo��</th>
        <th></th>
        <c:if test="${peopleDirectory.style == 'popup'}">
            <th></th>
        </c:if>
    </tr>
    <c:forEach items="${results}" var="bean">
        <tr>
            <td><c:out value="${bean.firstname}"/></td>
            <td><c:out value="${bean.lastname}"/></td>
            <td><c:out value="${bean.organization}"/></td>
            <td><c:out value="${bean.location}"/></td>
            <td><a href='<c:out value="${pageContext.request.contextPath}${bean.editLink}"/>'>edycja</a></td>
            <c:if test="${peopleDirectory.style == 'popup'}">
                <td>
                    &nbsp;&nbsp;
                    <a href="javascript:void(pick(
                        '<c:out value='${peopleDirectory.addresseeType}'/>',
                        '<c:out value='${bean.title}'/>',
                        '<c:out value='${bean.firstname}'/>',
                        '<c:out value='${bean.lastname}'/>',
                        '<c:out value='${bean.organization}'/>',
                        '<c:out value='${bean.organizationDivision}'/>',
                        '<c:out value='${bean.street}'/>',
                        '<c:out value='${bean.zip}'/>',
                        '<c:out value='${bean.location}'/>',
                        '<c:out value='${bean.country}'/>',
                        '<c:out value='${bean.email}'/>',
                        '<c:out value='${bean.fax}'/>'));">wybierz</a>
                </td>
            </c:if>
        </tr>
    </c:forEach>
    </table>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50%"><c:if test="${!empty prevPageLink}">
            <a href='<c:out value="${prevPageLink}"/>'>poprzednia strona</a>
        </c:if></td>
        <td width="50%" align="right"><c:if test="${!empty nextPageLink}">
            <a href='<c:out value="${nextPageLink}"/>'>nast�pna strona</a>
        </c:if></td>
    </tr>
    </table>

</c:if>

</html:form>

<a href='<c:out value="${pageContext.request.contextPath}${newSearchLink}"/>'>Nowe wyszukiwanie</a>

