<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="ZamawianieBarkodow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="Barcodes"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
<tr>
	<th>
		<ds:lang text="id"/>
	</th>
	<th>
		<ds:lang text="ctime"/>
	</th>
	<th>
		<ds:lang text="LiczbaBarkodow"/>
	</th>
	<th>
		<ds:lang text="LiczbaPrzydzielonychBarkodow"/>
	</th>
	<th>
		<ds:lang text="PoczatekZakresu"/>
	</th>
	<th>
		<ds:lang text="KoniecZakresu"/>
	</th>
	<th>
		<ds:lang text="status"/>
	</th>
	<th>
		<ds:lang text="Lokalizacja"/>
	</th>
	<th>
		<ds:lang text="Transport"/>
	</th>
	<th>
		<ds:lang text="Uwagi"/>
	</th>
</tr>
<ww:iterator value="ranges">
<tr>
	<td>
		<ww:property value="id"/>
	</td>
	<td nowrap="nowrap">
		<ds:format-date pattern="dd-MM-yyyy HH:mm" value="ctime"/>
	</td>
	<td>
		<ww:property value="numberOfBarcodes"/>
	</td>
	<td>
		<ww:property value="assignedNumberOfBarcodes"/>
	</td>
	<td>
		<ww:property value="startString"/>
	</td>
	<td>
		<ww:property value="endString"/>
	</td>
	<td>
		<ww:property value="statusDesc"/>
	</td>
	<td>
		<ww:property value="getLocationName()"/>
	</td>
	<td>
		<ww:property value="transportServiceName"/>
	</td>
	<td>
		<ww:property value="remark"/>
	</td>
	<ww:if test="status==4">
		<td>
			<a href="<ww:url value="'/office/barcodes-order.action'"/>?rangeId=<ww:property value="id"/>&doConfirm=true"/><ds:lang text="Odebrano"/></a>
		</td>
	</ww:if>
	<ww:if test="status==5">
		<td>
			<a href="<ww:url value="'/office/barcodes-order.action'"/>?rangeId=<ww:property value="id"/>&doUsed=true"/><ds:lang text="Wykorzystane"/></a>
		</td>
	</ww:if>
	<ww:if test="status==1">
		<td>
			<a href="<ww:url value="'/office/barcodes-order.action'"/>?rangeId=<ww:property value="id"/>&doCancel=true"/><ds:lang text="Anuluj"/></a>
		</td>
	</ww:if>
	<ww:if test="status==4 || status==5">
		<td>
			<a href="<ww:url value="'/office/barcodes-order.action'"/>?rangeId=<ww:property value="id"/>&doAdvertise=true"/><ds:lang text="Zareklamuj"/></a>
		</td>
	</ww:if>
</tr>
</ww:iterator>
</table>


<form action="<ww:url value="'/office/barcodes-order.action'"/>" method="post">
<table>
	<tr>
		<td>
			<ds:lang text="liczbaBarkodow"/><span class="star">*</span>
		</td>
		<td>
			<ww:textfield name="'number'" size="5" maxlength="4" cssClass="'txt'"/>x1000
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Lokalizacja"/><span class="star">*</span>
		</td>
		<td>
			<ww:select id="locationId" name="'locationId'" cssClass="'sel'" list="locations" listKey="id" listValue="getDictionaryDescription()" headerKey="''" headerValue="getText('select.wybierz')" onchange="'clearAddress()'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Uwagi"/>
		</td>
		<td>
			<ww:textarea name="'remark'" id="remark" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
		<tr>
		<td>
			<ds:lang text="NazwaUslugiTransportowej"/>
		</td>
		<td>
			<ww:textarea name="'transportServiceName'" id="transportServiceName" cols="30" cssClass="'txt'" ></ww:textarea>
		</td>
	</tr>
	
	
</table>
<ds:submit-event value="'Zamow'" name="'doOrder'" cssClass="'btn'"/>
</form>


<script>
function clearAddress()
{
	document.getElementById('city').value = "";
	document.getElementById('street').value = "";
	document.getElementById('zip').value = "";
	document.getElementById('name').value = "";
}

function clearLocation()
{
	document.getElementById('locationId').value = "";
}

</script>