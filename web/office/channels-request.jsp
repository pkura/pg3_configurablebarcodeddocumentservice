<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ page import="pl.compan.docusafe.core.office.ChannelRequestBean.ChannelRequestStatus" %>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<form action="<ww:url value="'/office/channels-request.action'"/>"  method="post" >
	<p><ds:xmlLink path="ChannelsView"/></p>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZgloszeniaModyfikacji"/></h1>
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
	<p></p>
	<ds:ww-action-errors/>
	<div style="padding: 20px;">
		<ww:if test="requestBeans.size() > 0">
			<table border="1px" cellpadding="3px" style="border-collapse:collapse;">
				<tr>
					<th>Data nadej�cia</th>
					<th>Tre��</th>
					<th>Status</th>
					<ww:if test="channelAdmin">
						<th>Wybierz</th>
					</ww:if>
					<th>Data przetworzenia</th>
					<th>Komentarz</th>
					
				</tr>
			<ww:iterator value="requestBeans">
				<tr <ww:if test="status.isInvalid()">style="background-color: #FDE8D7"</ww:if>
					<ww:elseIf test="status.isProcessed()">style="background-color: #D8F3C9"</ww:elseIf>
						>
					<td><ds:format-date pattern="dd-MM-yy kk:mm:ss" value="incomingDate"/></td>
				 	<td><ww:property value="content"/></td>
					<td><ww:property value="status.print()"/></td>
					<ww:if test="channelAdmin">
						<td><input type="radio" name="requestToProcess" value="<ww:property value="id"/>"
							<ww:if test="!status.isOpen()">disabled="true"</ww:if>/> </td>
					</ww:if>
					
					<td><ds:format-date pattern="dd-MM-yy kk:mm:ss" value="processDate"/></td>
					<td><ww:property value="comment"/></td>
				</tr>
			</ww:iterator>
			</table>
		</ww:if>
		<ww:else>
			Brak zg�osze�<br>
		</ww:else>
		<p></p>
		<ww:if test="channelAdmin">
			<ww:if test="requestBeans.size > 0">
				<table>
				<tr>
				<td>Nowy status:</td>
				<td><select name="operation">
					<option value="process" label="Przetworzone">Przetworzone</option>
					<option value="invalid" label="Nieprawid�owe">Nieprawid�owe</option>
				</select></td></tr>
				<tr>
				<td>Komentarz:</td>
				<td><ww:textarea name="'comment'" cols="50" rows="3"/></td>
				</tr>
				</table>
				<ds:event name="'doProcess'" value="'Zako�cz zlecenie'"/>
			</ww:if>
		</ww:if>
		<ww:else>
			<div>
				Dodaj zg�oszenie (do 500. znak�w): <br>
				<ww:textarea name="'newRequestContent'" cols="50" rows="3"/><br>
				<ds:event name="'doSubmit'" value="'Dodaj'"/>
			</div>
		</ww:else>
	</div>
</form>