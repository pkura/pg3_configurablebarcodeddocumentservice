<%@ page import="pl.compan.docusafe.core.office.barcode.Package"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="WprowadzonePaczki"/></h1>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="PackageIn"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PackageIn"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
 	
<form action="<ww:url value="'/office/package-show.action'"/>" method="post">
	<ds:available test="layout2">
		<div id="middleContainer" style="border-top: none;"> <!-- BIG TABLE start -->
		</ds:available>	 	
	<hr size="1" align="left" class="horizontalLine" width="77%" />

<p id="pIEFix">
	<h4><ds:lang text="PaczkiWprowadzonePrzezZalogowanegoUzytkownika"/></h4>
</p>
<ww:hidden name="'id'" id="id" value="id"/>
<p id="pIEFix">
	<b>Paczka numer :</b> <ww:property value="numer"/> , <b>Status : </b> <ww:property value="packageStatus"/> 
</p>
<table>	
	<tr>
		<td>
				<ds:lang text="TypDokumentow"/>
		</td>
		<td>
			<ww:select name="'typDokumentu'" id="typDokumentu" list="typyDokumentow" headerKey="''" headerValue="'wybierz'"  
				cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Lokalizacja"/>
		</td>
		<td>
			<ww:select name="'lokalizacja'" id="lokalizacja" list="locations" listKey="id" headerKey="''" headerValue="'wybierz'"  
				listValue="name+' '+zip+' '+city" cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			Odbiorca paczki
		</td>
		<td>
			<ww:select name="'odbiorcaLokalizacja'" id="odbiorcaLokalizacja" list="locations" listKey="id" headerKey="''" headerValue="'wybierz'"  
				listValue="name+' '+zip+' '+city" cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Opis"/>
		</td>
		<td>
			<ww:textarea name="'opis'" id="opis" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="transportServiceName"/>
		</td>
		<td>
			<ww:textarea name="'transportServiceName'" id="transportServiceName" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
</table>

<ww:if test="status < 2 || status > 3">
<ds:event name="'doSave'" value="'Zapisz'"/>
</ww:if>
<ww:if test="status != 2">
	<ds:event name="'doSend'" value="'Wy�lij'"/>
</ww:if>

<ww:if test="pac!=null">
<input type="submit" class="btn" value="Generuj PDF" name="generatePdfElem" style="btn" />
<ds:has-permission name="PACZKI_WERYFIKACJA">
	<a href="<ww:url value="'/office/package-verify.action'"/>?packageId=<ww:property value="id"/>"><ds:lang text="Weryfikuj"/></a>
</ds:has-permission>
<table id="titleTable" class="tableMargin">		
		<tr>
			<td align="center">
				<b>Numer pozycji</b>
			</td>
			<td>
				<b>Barkod</b>
			</td>
		</tr>
	<ww:iterator value="pac.getPackageElements()">
		<tr>
			<td align="center">
				<ww:property value="kod"/>
			</td>
			<td>			
				<ww:property value="barcode"/>			
			</td>
		</tr>
	</ww:iterator>
</table>
</ww:if>
<ww:else>
<input type="submit" class="btn" value="Generuj PDF" name="generatePdf" style="btn" />
<table id="titleTable" class="tableMargin">		
		<tr>
			<td align="center">
				<b>Numer Paczki</b>
			</td>
			<td>
				<b>Status</b>
			</td>
		</tr>
	<ww:iterator value="packages">
		<ww:set name="stat" value="status"/>
		<tr>
			<td align="center"><a href="<ww:url value="'/office/package-show.action'"><ww:param name="'id'" value="id"/></ww:url>">
				<ww:property value="numer"/></a>
			</td>
			<td>			
				<ww:property value="getStatusAsString(#stat)"/>			
			</td>
		</tr>
	</ww:iterator>
</table>
</ww:else>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</form>

<script type="text/javascript">
	prepareTable(E("titleTable"),1,0);
</script>