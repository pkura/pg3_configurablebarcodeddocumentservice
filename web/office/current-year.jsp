<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N current-year.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="BiezacyRok"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/current-year.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<p class="marginBottom2">
		<ds:lang text="OtwartyRok"/>:
		<ww:if test="currentYear == null">
			<ds:lang text="NieOtwartoRoku"/>
		</ww:if>
		<ww:else>
			<ww:property value="currentYear"/>
		</ww:else>
	</p>
	<ds:submit-event value="getText('ZamknijRok')" name="'doCloseYearPrep'" confirm="getText('NaPewnoZamknacRok')" disabled="!canCloseYear" />
	
	<ww:if test="currentYear == null">
		<hr class="shortLine"/>
		<p class="marginBottom2">
			<ds:lang text="OtworzRok"/>:
			<ww:select name="'newYear'" list="years" cssClass="'sel'" />
		</p>
		<ds:submit-event value="getText('OtworzRok')" name="'doOpenAnyYear'" confirm="getText('NaPewnoOtworzycTenRok')" disabled="!canOpenAnyYear"/>
	</ww:if>
</form>

<%--
	<h1><ds:lang text="BiezacyRok"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/current-year.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table>
<tr>
    <td><ds:lang text="OtwartyRok"/>:</td>
    <td><ww:if test="currentYear == null"><ds:lang text="NieOtwartoRoku"/></ww:if>
        <ww:else><ww:property value="currentYear"/></ww:else>
    </td>
</tr>
</table>
<ds:submit-event value="getText('ZamknijRok')" name="'doCloseYearPrep'" confirm="getText('NaPewnoZamknacRok')" disabled="!canCloseYear" />

<ww:if test="currentYear == null">
    <hr/>

    <table>
    <tr>
        <td><ds:lang text="OtworzRok"/>:</td>
        <td><ww:select name="'newYear'" list="years" cssClass="'sel'" /></td>
    </tr>
    </table>
    <ds:submit-event value="getText('OtworzRok')" name="'doOpenAnyYear'" confirm="getText('NaPewnoOtworzycTenRok')" disabled="!canOpenAnyYear"/>
</ww:if>
</form>	--%>

<!--N koniec current-year.jsp N-->