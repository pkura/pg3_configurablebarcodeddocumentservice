<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N case.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.record.Records"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="createNewCase"/></h1>

<ds:ww-action-errors/>
<ds:ww-action-messages />

<ww:if test="canView">
	<form action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);" id="form">
	
	    <input type="hidden" name="doCreate" id="doCreate"/>
	    <input type="hidden" name="caseId" id="caseId"/>

	    <ww:hidden name="'portfolioId'" id="portfolioId"/>
	    <ww:token />

		<table>
			<tr>
				<td>
					Teczka<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'portfolioTxt'" id="portfolioTxt" cssClass="'txt'" readonly="true" size="30"/>
					<input type="button" class="btn" value="Wybierz"
					onclick="window.open('<ww:url value="'/office/pick-portfolio.action'"><ww:param name="'divisionGuid'" value="targetDivision"/><ww:param name="'first'" value="true"/></ww:url>', 'pickportfolio', 'width=600,height=600,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes')"/>
				</td>
			</tr>
			<tr>
				<td>
					Tytu� sprawy<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'caseTitle'" id="caseTitle" cssClass="'txt'" value="caseTitle" size="30"/>
				</td>
			</tr>
			<tr>
				<td>
					Opis sprawy:
				</td>
				<td>
					<ww:textarea name="'description2'" rows="4" cols="80" cssClass="'txt-200p'" cssStyle="width: 183px; height: 71px;"  id="description2"/>
				</td>
			</tr>
			<tr>
				<td>
					Spodziewany termin zako�czenia<span class="star">*</span>:
				</td>
				<td>
					<ww:textfield name="'caseFinishDate'" id="caseFinishDate" size="10" maxlength="10" cssClass="'txt'"/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="caseFinishDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
				</td>
			</tr>
	
			<ww:if test="canChooseClerk">
				<tr>
					<td>
						Referent sprawy:
					</td>
					<td>
						<ww:select name="'clerk'" list="clerks" listKey="name" listValue="asLastnameFirstname()" cssClass="'sel'" value="clerk" />
					</td>
				</tr>
			</ww:if>
	        
			<tr>
				<td>
					Priorytet sprawy<span class="star">*</span>:
				</td>
				<td>
					<ww:select name="'priority'" list="priorities" listKey="id" listValue="name" cssClass="'sel'" />
				</td>
			</tr>
			<tr>
				<td>
					<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
				</td>
			</tr>
			<tr>
				<td>
					<ds:event name="'doPreCreate'" value="'Za�� spraw�'" confirm="'Na pewno utworzy� now� spraw�?'" />
				</td>
			<tr>
		</table>
		<br/>
		<script language="JavaScript">
			if($j('#caseFinishDate').length > 0)
			{
	            Calendar.setup({
	                inputField     :    "caseFinishDate",     // id of the input field
	                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>", // format of the input field
	                button         :    "caseFinishDateTrigger",  // trigger for the calendar (button ID)
	                align          :    "Tl",           // alignment (defaults to "Bl")
	                singleClick    :    true
	            });
			}
			
           function __pick_portfolio(id, portfolioId, name, finishDate)
           {
               document.getElementById('portfolioId').value = id;
               document.getElementById('portfolioTxt').value = portfolioId + ' ('+name+')';
           }
		</script>
	</form>
</ww:if>
