<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="PrzydzielBarkodyNumeromDokumentow"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%"/>
<i><ds:lang text="PrzydzielanieWSystemieBarkodowDoWczytanychWczesniejNumerowDokumentow"/></i>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/office/package-in-nr-to-bar.action'"/>" method="post"> 	
 		<table>
		 	<tr>
		 		<th>
					<ds:lang text="NumerPaczki"/><span class="star">*</span>
				</th>
				<td>
					<ww:textfield name="'nrPaczki'" id="nrPaczki" size="30" cssClass="'txt'" maxlength="30"/>
				</td>
				<td>
					<ww:select name="'numerPaczki'" id="numerPaczki" list="packages"listKey="numer" headerKey="''" headerValue="'wybierz'"  
						listValue="numer" cssClass="'sel'" value="numerPaczki"/>
 				</td>
				<td>
					<ds:event name="'doSearchNumber'" value="'Zatwierd� wyb�r'"/>
				</td>
			</tr>	
		</table>
</form>
<form action="<ww:url value="'/office/package-in-nr-to-bar.action'"/>" method="post" onsubmit="if (!validate()) return false;"> 
<ww:hidden name="'doCreate'" id="doCreate"/>
<ww:hidden name="'id'" id="id" value="id"/>
<ww:if test="elements.size() > 0">
	
<ww:include page="/office/package-param.jsp"/>
<ds:event name="'doSave'" value="'Zapisz'"/>
<ds:event name="'doReturn'" value="'Zg�o� b��d'"/>
 		<table id="selectTable">	
 			<tr>
 				<td><ds:lang text="WybierzSposobWprowadzaniaBarkodow"/>:</td>
				<td><select class="sel" onChange="if(!changeSelect()) return false;">
						<option id="wpr0">-- wybierz --</option>
						<option id="wpr1">wprowadzanie czytnikiem</option>
						<option id="wpr2">wprowadzanie z klawiatury</option>
					</select>
				</td>
 			</tr>
 		</table>
		
		<table id="barcodeTable" class="tableMargin">
			<tr>		
				<th>
					<ds:lang text="Kod"/>
				</th>
				<th>
					<ds:lang text="Barkod"/>
				</th>
			</tr>
			<ww:iterator value="elements">
			<tr>	
				<td>
					<ww:property value="kod"/>	
					<ww:hidden name="'elementId'" value="id"/>			
				</td>		
				<td>
					<ww:textfield name="'barkodes'" value="barcode" size="30" cssClass="'txt'" maxlength="30"/>
				</td>	
			</tr>
			</ww:iterator>
		</table>	
		<table id="buttonTable">
 			<tr>
				<td><input type="button" class="btn" value="<ds:lang text='Przypisz barkody'/>" 
				onclick="validateTrue();" name="Create" /></td>
 			</tr>
 		</table>
</ww:if>
</form>

<script type="text/javascript">

var onsubmit = 0;
var interval_shift;
var interval_catch;
var prob=null;
var nr_pola = 0;
var bool=false;

	function validateTrue()
	{
		onsubmit = 1;
		document.getElementById('doCreate').value='true';
		document.forms[1].submit();
	}

	 function unblockNumber()
	    {
	    	$j("#nrPaczki").attr("readonly", false).removeClass("grey");
	    	$j("#selectList").removeAttr("disabled");
	    	$j("#doSearchNumber").removeAttr("disabled");
	    	$j("#zmiennrpaczki").hide();
	    }
	    
	    function changePackageSelect()
	    {
	    	var liczba = $j('#selectList :selected').text();
	    	$j("#nrPaczki").attr("value", liczba);
	    }
	    
	    function removeClass_inputs()
		{
			var liczba = $j("#barcodeTable input").length;
			
			if (liczba != 0)
			{
				$j("#barcodeTable input").each(function()
				{
					$j(this).attr("readonly", false).removeClass("grey");
				});
			}
			else{}
		}

		function changeSelect()
		{		
			if($j("#wpr0").is(":selected"))
			{
				clearInterval(interval_shift);
				clearInterval(interval_catch);
				
				var liczba = $j("#barcodeTable input").length;
			
				if (liczba != 0)
				{
					$j("#barcodeTable input").each(function()
		  			{
		  				$j(this).attr("readonly", true).addClass("grey");
		  			});
				}
				else{}	
			}
	       	else if ($j("#wpr1").is(":selected"))
	       	{	     
	       		interval_shift = setInterval("sprawdz();", 200);
				
				removeClass_inputs();
	   		}
	   		else if ($j("#wpr2").is(":selected"))
	       	{
				clearInterval(interval_shift);
				clearInterval(interval_catch);
				
				removeClass_inputs();	
	   		}
		}	

		function sprawdz()
		{		
			var liczba = $j("#barcodeTable input").length;
			var warunek =0;
			var licznik = 0;
			
			if (liczba != 0)
			{
				clearInterval(interval_shift);
			
				$j("#barcodeTable input").each(function()
		  		{
		  			licznik += 1;
		  			
		  			if(warunek == 0)
		  			{
						var tekst = $j(this).val();
					
						if (tekst == null || tekst.trim().length == 0)
						{
							$j(this).focus();
							warunek = 1;
							nr_pola = licznik;
							
							interval_catch = setInterval("catch_barcode();", 200);
						}
						else{}
					}
		  		});
			}
			else{}
		}
		
		function catch_barcode()
		{
			var liczba = $j("#barcodeTable input").length;
			var iteracja = 0;
			
			$j("#barcodeTable input").each(function()
		  	{
				iteracja += 1;		
				if (iteracja == nr_pola)
				{
					var tekst = $j(this).val();
					
					if (tekst != 0)
					{
						if (prob != tekst)
		   				{
		   					prob = tekst;
	 	  				}
	   					else
	   					{   						
	   						clearInterval(interval_catch);
							prob = null;
						
							if (liczba == iteracja)
							{
								//$j("#doCreate").focus();
							}
							else
							{
								sprawdz();
							}
	   					}
					}
					else{}	
				}
			});
		}

		function validate()
		{
			if(onsubmit < 1)
			{
				return false;
			}
				
			var text;
			var text2;
			var licznik_powtorzen = 0;
			var czy_powtarza = 0;
			var text3;
			
			var liczba = $j("#nrPaczki").attr("value");
	    	$j("#numerPaczki").attr("value", liczba);
			
			$j("#barcodeTable input").each(function()
		  	{
		 		text = $j(this).val();
		
				$j("#barcodeTable input").each(function()
		  		{
		 			text2 = $j(this).val();
		 			
		 			if (text == text2)
		 			{
		 				$j(this).attr("value", "");		
		 			}
				});	
				
				$j(this).attr("value", text);
	 		});	
	 			
	 		$j("#barcodeTable input").each(function()
		  	{
		 		text3 = $j(this).attr("value");
		 		if (text3 == null || text3.trim().length == 0)
		 		{
					czy_powtarza += 1;
				}
			});	
	 				
	 		if ($j("#chceZapisac").is(":checked"))
	     	{
				bool = true;
	 		}
		   	else 
	   		{		
				bool = false;
	   		}

	 		if (czy_powtarza == 0 || bool)
	 		{
	 			return true; 
	 		}	
	 		else
	 		{
	 			window.alert("<ds:lang text='PozostalyPustePolaLubTezZostalyOneWyczyszczonePoniewazWpisaneWNichBarkodyPowtarzalySie'/>");
	 			return false;		
	 		}
	 		 
		}	
</script>