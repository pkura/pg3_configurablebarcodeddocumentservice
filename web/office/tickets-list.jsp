<%@ page import="pl.compan.docusafe.util.DateUtils,pl.compan.docusafe.core.office.DSPermission" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Rejestr bilet�w"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form" action="<ww:url value="'/office/tickets-list.action'"></ww:url>" method="post" >
<table class="mediumTable search userBilling" >
	<tr colspan="3">
		<td>Status:<ww:select name="'accepted'" id="accepted" cssClass="'sel'"
					list="filtr"
					headerKey="''" headerValue="'-- wybierz --'"/>
		</td>	
		<td>Termin wyjazdu od:<ww:textfield name="'dateTripFrom'" id="dateTripFrom" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dateTripFromTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
				<script type="text/javascript">
					if($j('#dateTripFrom').length > 0)
					{
						Calendar.setup({
						    inputField  :    "dateTripFrom",     // id of the input field
						    ifFormat    :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
						    button      :    "dateTripFromTrigger",  // trigger for the calendar (button ID)
						    align       :    "Tl",           // alignment (defaults to "Bl")
						    singleClick :    true
						});
					}

					
				</script>
		</td>
		<td>Termin wyjazdu do:<ww:textfield name="'dateTripTo'" id="dateTripTo" size="10" maxlength="10" cssClass="'txt'"/>
	           	<img src="<ww:url value="'/calendar096/img.gif'"/>"
	            id="dateTripToTrigger" style="cursor: pointer; border: 1px solid red;"
	            title="Date selector" onmouseover="this.style.background='red';"
	            onmouseout="this.style.background=''"/>
	            <script type="text/javascript">
	            
	            if($j('#dateTripTo').length > 0)
					{
						Calendar.setup({
						    inputField	:   "dateTripTo",     // id of the input field
						    ifFormat	:   "<%=DateUtils.jsCalendarDateFormat%>", // format of the input field
							button		: 	"dateTripToTrigger", // trigger for the calendar (button ID)
							align 		: 	"Tl", // alignment (defaults to "Bl")
							singleClick : 	true
						});
					}
	            </script>
		 </td>
		
	</tr>
	<tr  colspan="3">
		<td>Pracownik delegowany:<ww:select name="'workerId'" id="workerId"
	            	list="users" listKey="id" listValue="asLastnameFirstname()"
	            	headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
		</td>
	    <td>Cel podr�y:<ww:textfield name="'tripPurpose'" size="100" cssClass="'txt'"/>
		</td>
		<td>Trasa:<ww:textfield name="'trasa'" size="100" cssClass="'txt'"/>
		</td>
	</tr>
	
	<tr  colspan="2">
	    <td>
	        <input type="submit" value="Znajd�" name="doSearch" class="btn"/>
	    </td>
	    <td>
	    	<a href= "<ww:url value="'/office/tickets-list.action>"/>">
			Wyczy�c pola
			</a>
		</td>
	</tr>
	<tr>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=nr_rezerwacji&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Numer rezerwacji
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=nr_rezerwacji&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Pracownik delegowany
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=trip_purpose&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Cel podr�y
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=trip_purpose&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=trasa=t&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Trasa
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=trasa&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieRosnace"/>"  href="<ww:url value="'/office/tickets-list.action?sortField=start_date&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Data wyjazdu
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=start_date&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=finish_date&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Data powrotu
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=finish_date&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=purchase_date&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Termin wykupienia biletu
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=purchase_date&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
		<th>
			<a title="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=nr_zestawienia&ascending='+false"/>"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
				Numer zestawienia
			<a title="<ds:lang text="SortowanieRosnace"/>" href="<ww:url value="'/office/tickets-list.action?sortField=nr_zestawienia&ascending='+true"/>"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>" width="11" height="11" border="0"/></a>
		</th>
	</tr>
	<ww:iterator value="bilety">
		<ww:hidden name="'id'" value="documentId"/>
		<tr>
<!-- 			<td> -->
<%-- 				<ww:checkbox name="doZestawienia" fieldValue="true"/> --%>
<!-- 			</td> -->
<!-- 		<td> 
 				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
 					<ww:property value="documentId" /> 
 				</a> 
 			</td> --> 
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ww:property value="nrRezerwacji" />
				</a>
			</td>
	 		<td> 
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ww:property value="workerName" />
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ww:property value="tripPurpose"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ww:property value="trasa"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ds:format-date value="startDate" pattern="dd-MM-yy"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ds:format-date value="finishDate" pattern="dd-MM-yy"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ds:format-date value="purchaseDate" pattern="dd-MM-yy"/>
				</a>
			</td>
			<td>
				<a href="<ww:url value="'/office/tickets-list.action?documentId='+id"/>">
					<ww:property value="nrZestawienia"/>
				</a>
			</td>
		</tr>
	</ww:iterator>
	<tr>
		<td colspan="2">
			<ww:if test="showGenerujButton">
				<input type="submit" value="Generuj Zestawienie" class="btn" name="doGenerateZestawienie" id="doGenerateZestawienie"/>
			</ww:if>
		</td>
	</tr>
</table>
</form>

