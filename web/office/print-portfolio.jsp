<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N print-portfolio.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils,
				 pl.compan.docusafe.web.office.PrintPortfolioAction"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h2>Kryteria wydruku spraw</h2>

<form action="<ww:url value="'/office/print-portfolio.action'"/>" method="post">
<ww:hidden name="'id'" value="id"/>

<table cellspacing="0" cellpadding="0" style="margin-left:10px">
<tr><td colspan="2">Wydrukuj sprawy za�o�one:</td></tr>
<tr>
	<td><input type="radio" name="searchType" value="<%= PrintPortfolioAction.SEARCH_DAY %>" id="searchType_day" <ww:if test="searchType == 'day'">checked="true"</ww:if>/>
    dnia <ww:textfield name="'day'" id="day" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_day&quot;).checked = true'" />
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_day" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
    </td>
</tr>
<tr>
	<td><input type="radio" name="searchType" value="<%= PrintPortfolioAction.SEARCH_DAYRANGE %>" id="searchType_dayrange" <ww:if test="searchType == 'dayrange'">checked="true"</ww:if>/>
    od dnia <ww:textfield name="'fromDay'" id="fromDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_fromDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
        do dnia <ww:textfield name="'toDay'" id="toDay" cssClass="'txt'" size="10" maxlength="10" onchange="'document.getElementById(&quot;searchType_dayrange&quot;).checked = true'"/>
        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_toDay" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
    </td>
<tr>
<tr><td>&nbsp;</td></tr>
</tr>
	<td><input type="radio" name="searchType" value="<%= PrintPortfolioAction.SEARCH_NUMRANGE %>" id="searchType_numrange" <ww:if test="searchType == 'numrange'">checked="true"</ww:if>/>
    Numery od <input type="text" name="fromNum" id="fromNum" size="6" maxlength="10" class="txt" onkeypress="document.getElementById('searchType_numrange').checked = true" value="<ww:property value="fromNum"/>"/>
                do <input type="text" name="toNum" id="toNum" size="6" maxlength="10" class="txt" onkeypress="document.getElementById('searchType_numrange').checked = true" value="<ww:property value="toNum"/>"/>
    </td>

</tr>
<tr><td>&nbsp;</td></tr>
<tr>
	<td>
	<input type="submit" name="doPrint" value="Stw�rz wydruk" class="btn"/>
	</td>
</tr>
</table>
</form>

<script type="text/javascript">
	if($j('#day').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "day",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_day",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
	if($j('#fromDay').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "fromDay",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_fromDay",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
	if($j('#toDay').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "toDay",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_toDay",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
 </script>
 