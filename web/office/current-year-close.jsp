<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N current-year-close.jsp N-->

<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="BiezacyRok"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><b><ds:lang text="current.year.close.PrzedZamknieciemRoku.."/> 
.</b></p>

<form action="<ww:url value="'/office/current-year.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<h3><ds:lang text="DziennikiDoZamkniecia"/></h3>

    <p><ds:lang text="current.year.close.ZaznaczoneDziennikiZostana..."/> .</p>

<table>
    <ww:iterator value="journals">
        <tr>
            <td><input name="journalIds" type="checkbox" checked="true" value="<ww:property value="key"/>"/></td>
            <td><ww:property value="value"/></td>
        </tr>
    </ww:iterator>
</table>

<%--
<h3>Teczki do zamkni�cia</h3>

<table>
    <ww:iterator value="portfolios">
        <tr>
            <td><input type="checkbox" checked="true"/></td>
            <td><ww:property value="value"/></td>
        </tr>
    </ww:iterator>
</table>
--%>

<ds:submit-event value="getText('ZamknijRok')" name="'doCloseYear'" confirm="getText('NaPewnoZamknacRok')"/>

</form>

