<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-case-status-dictionary.jsp N-->

<%@ page import="pl.compan.docusafe.core.office.Journal"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>S�ownik status�w spraw</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/office/admin/edit-case-status-dictionary">

<table>
<c:forEach items="${statuses}" var="bean">
    <tr>
        <td><input type="checkbox" name="ids" value="<c:out value='${bean.id}'/>"/></td>
        <td><c:out value="${bean.description}"/></td>
    </tr>
</c:forEach>
<c:if test="${!empty statuses}">
    <tr>
        <td colspan='2'><input type="submit" name="doDelete" value="Usu� zaznaczone" class="btn"/></td>
    </tr>
</c:if>
</table>

<p></p>

<p>Nowy status<span class="star">*</span>: <html:text property="description" size="30" maxlength="80" styleClass="txt"/>
<input type="submit" name="doAdd" value="Dodaj status" class="btn"/></p>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

</html:form>
