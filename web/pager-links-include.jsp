<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N pager-links-include.jsp N-->

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<%--
	Wy�wietlanie listy odno�nik�w do kolejnych stron wynik�w
	w postaci ci�gu "1 2 3 4" gdzie cyfry s� odno�nikami do
	odpowiednich stron.

	Strona spodziewa sie atrybutu requestu, kt�ry jest instancj�
	klasy pl.compan.docusafe.web.Pager.

	Strony JSP korzystaj�ce w WebWork przed w3�czeniem tego pliku powinny
	umie�cia tag <ww:set name="pager" scope="request" value="pager"/>
--%>

<span class="pager">
<c:if test="${pager.linkCount > 1}">
	<!-- pierwsza strona -->
	<c:choose>
		<c:when test="${!empty pager.firstPageLink}">
			<a href='<c:if test="${!pager.jsLinks}"><c:out value="${pageContext.request.contextPath}"/></c:if><c:out value="${pager.firstPageLink}"/>'>&laquo;&nbsp;Pierwsza</a>
		</c:when>
		<c:otherwise>
			<a href="#" class="disabledPage">&laquo;&nbsp;Pierwsza</a>
		</c:otherwise>
	</c:choose>
	
	<!-- poprzednia strona -->
	<c:choose>
		<c:when test="${!empty pager.prevPageLink}">
			<a href='<c:if test="${!pager.jsLinks}"><c:out value="${pageContext.request.contextPath}"/></c:if><c:out value="${pager.prevPageLink}"/>'>&lsaquo;&nbsp;Poprzednia</a>
		</c:when>
		<c:otherwise>
			<a href="#" class="disabledPage">&lsaquo;&nbsp;Poprzednia</a>
		</c:otherwise>
	</c:choose>
	<!-- odno�niki do stron -->
	
	<c:forEach begin="${pager.beginOffset}" end="${pager.endOffset}" step="${pager.limit}" var="off" varStatus="status" >
		<c:choose>
			<c:when test="${pager.offset == off}">
				<a href="#" class="currentPage"><c:out value="${status.count + pager.startPage - 1}"/></a>
			</c:when>
			<c:otherwise>
				<a href='<c:if test="${!pager.jsLinks}"><c:out value="${pageContext.request.contextPath}"/></c:if><c:out value="${pager.link[off]}"/>'><c:out value="${status.count + pager.startPage - 1}"/></a>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	
	<!-- kolejna strona -->
	<c:choose>
		<c:when test="${!empty pager.nextPageLink}">
			<a href='<c:if test="${!pager.jsLinks}"><c:out value="${pageContext.request.contextPath}"/></c:if><c:out value="${pager.nextPageLink}"/>'><ds:lang text="Nastepna"/>&nbsp;&rsaquo;</a>
		</c:when>
		<c:otherwise>
			<a href="#" class="disabledPage"><ds:lang text="Nastepna"/>&nbsp;&rsaquo;</a>
		</c:otherwise>
	</c:choose>
	
	<!-- ostatnia strona -->
	<c:choose>
		<c:when test="${!empty pager.lastPageLink}">
			<a href='<c:if test="${!pager.jsLinks}"><c:out value="${pageContext.request.contextPath}"/></c:if><c:out value="${pager.lastPageLink}"/>'>Ostatnia&nbsp;&raquo;</a>
		</c:when>
		<c:otherwise>
			<a href="#" class="disabledPage">Ostatnia&nbsp;&raquo;</a>
		</c:otherwise>
	</c:choose>
</c:if>

</span>
<!--N koniec pager-links-include.jsp N-->