<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N registry-fields.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<script type="text/javascript">
    /**
     * Tablica (mapa) funkcji, kt�re b�d� wykorzystane przez poszczeg�lne s�owniki do
     * przekazania wybrane przez u�ytkownika dane. Kluczem jest 'cn' a warto�ci�
     * funkcja.
     */
    var functions = new Array();

    /**
     * Otrzymuje dane od s�ownika i 'cn' pola, aby m�c rozpozna�, kt�r� funkcj�
     * akceptuj�c� wywo�a�,
     */
    function accept(cn, map)
    {
        functions[cn](map);
    }

    /**
     * Sprawdza czy potrzebna aktualizacja - je�li tak to wywo�uje funkcj� akceptuj�c�.
     * Wo�ane, gdy w s�owniku w�a�nie zapisano zmiany i nie umieszczono ich w tym formularzu.
     */
    function checkAccept(cn, map)
    {
        if (parseInt(document.getElementById('registry_'+cn).value) == parseInt(map.id))
            accept(cn,map);
    }
</script>

<ww:iterator value="rm.fields">
<ww:if test="!hidden">
    <ww:set name="prefix" value="'registry_'"/>
    <ww:set name="tagName" value="'values.'+cn"/>
    <ww:set name="tagId" value="#prefix+cn"/>
    <ww:set name="showName" value="(type != 'enum') || ((type == 'enum') && (refStyle != 'all-visible'))"/>
    <tr id="<ww:property value="'tr_'+#tagId"/>" >
        <td valign="top">
            <ww:if test="#showName"><ww:property value="name"/>:<ww:if test="required"><span id="<ww:property value="'required_'+cn"/>" class="star">*</span></ww:if></ww:if>
        </td>
        <td>
            <ww:if test="multiple">
                <!-- TODO: obecnie dzia�a tylko dla p�l typu integer,long,float i string -->
                <table>
                    <ww:if test="rm.getKey(cn) != null">
                    <ww:iterator value="rm.getKey(cn)" status="status">
                        <ww:set name="result"/>
                        <tr id="<ww:property value="'attTr_'+cn+#status.index"/>">
                            <td>
                                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
                                    value="<ww:property value="#result"/>" size="30" maxlength="<ww:property value="length"/>" class="txt" />
                            </td>
                            <td>
                                <input type="button" value="Usu�" class="btn" onclick="removeFieldValue_<ww:property value="cn+'('+#status.index+')'"/>"/>
                            </td>
                        </tr>
                    </ww:iterator>
                    </ww:if>
                    <tr id="<ww:property value="'attCreateTr_'+cn"/>">                        
                        <td colspan="2">
                            <input type="button" value=<ds:lang text="dodaj"/> class="btn" onclick="addFieldValue_<ww:property value="cn"/>()"/>
                        </td>
                    </tr>
                </table>
                
                <script type="text/javascript">
                    var fieldsSize_<ww:property value="cn"/> = <ww:property value="rm.getKey(cn) != null ? rm.getKey(cn).size() : 0"/>;
                    
                    function addFieldValue_<ww:property value="cn"/>()
                    {                
                        var option;
                
                        fieldsSize_<ww:property value="cn"/>++;
                
                        //alert('bylo '+attachmentsCount);
                        var attCreateTr = document.getElementById('attCreateTr_<ww:property value="cn"/>');                
                        var tr = document.createElement('tr');                
                        tr.setAttribute('id', '<ww:property value="'attTr_'+cn"/>'+fieldsSize_<ww:property value="cn"/>);                
                        // tworzymy input z nazw�
                        var input = document.createElement('input');
                        input.setAttribute('type', 'text');          
                        input.setAttribute('size', 30);            
                        input.setAttribute('name', '<ww:property value="#tagName"/>');                
                        input.setAttribute('class', 'txt');  
                        input.className = 'txt';             
                        input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>);               
                        tr.appendChild(createCell(input,true));                 
                
                        // tworzymy przycisk "usu�"                
                        input = document.createElement('input');                
                        input.setAttribute('type', 'button');                
                        input.setAttribute('value', 'Usu�');                
                        input.setAttribute('class', 'btn');                
                        input.className = 'btn';
                        //input.setAttribute('onclick', 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')');                
                        // trzeba robic obejscie jak ponizej bo wpp nie dziela pod IE
                        var func = 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')';
                        input.onclick = function() { var temp = new Function(func); temp(); };
                        tr.appendChild(createCell(input,false));                                                                
                        
                        attCreateTr.parentNode.insertBefore(tr, attCreateTr);                
                    }
                
                    function removeFieldValue_<ww:property value="cn"/>(no)                
                    {                
                        var tr = document.getElementById('<ww:property value="'attTr_'+cn"/>'+no);   
                        if (tr != null)             
                            tr.parentNode.removeChild(tr);                                                                      
                    }
                                                    
                    function createTextCell(value)                
                    {                
                        return createCell(document.createTextNode(value));                
                    }                                    
                
                    function createCell(elem)                
                    {                
                        var cell = document.createElement('td');                
                        cell.appendChild(elem);
                        return cell;                
                    }
                    
                </script>  
                
            </ww:if> 
            <ww:elseif test="type == 'string'">
                <%--<ww:textfield name="#tagName" id="#tagId" value="rm.getKey(cn)" cssClass="'txt'" size="30" maxlength="length"/>--%>
                <ww:if test="length <= 80">
                    <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                        value="<ww:property value="rm.getKey(cn)"/>" size="30" maxlength="<ww:property value="length"/>" class="txt" 
                        />
                </ww:if>
                <ww:else>
                    <textarea name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" rows="3" cols="30" class="txt"><ww:property value="rm.getKey(cn)"/></textarea>
                </ww:else>
            </ww:elseif>
            <ww:elseif test="type == 'float'">                
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="rm.getKey(cn)"/>" size="30" maxlength="30" class="txt" />
            </ww:elseif>
            <ww:elseif test="type == 'integer' || type == 'long'">
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="rm.getKey(cn)"/>" size="10" maxlength="10" class="txt" />
            </ww:elseif>
            <ww:elseif test="type == 'enum'">               
                <select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
                        onchange="<ww:property value="rm.mainFieldCn == cn ? 'onchange_main_field(this)' : ''"/>"
                        >
                    <ww:if test="rm.mainFieldCn != cn"><option value="">-- wybierz --</option></ww:if>
                    <ww:set name="fieldCn" value="cn"/>
                    <option value="">-- wybierz --</option>
                    <ww:iterator value="enumItems">
                        <option value="<ww:property value="id"/>" <ww:if test="rm.getKey(#fieldCn) == id">selected="selected"</ww:if>>
                            <ww:property value="title"/>
                        </option>
                    </ww:iterator>
                </select>              
            </ww:elseif>
            <ww:elseif test="type == 'bool'">              
                <input type="checkbox" name="<ww:property value="#tagName"/>" value="true" id="<ww:property value="#tagId"/>"
                    <ww:if test="rm.getKey(cn)">checked="true"</ww:if> <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
                <%-- gdy brak uprawnien do edycji (pole jest "disabled") --%>
            </ww:elseif>
            <ww:elseif test="type == 'date'">
                <input type="text" size="10" maxlength="10" class="txt"
                    name="<ww:property value="#tagName"/>"
                    id="<ww:property value="#tagId"/>"
                    value="<ds:format-date value="rm.getKey(cn)" pattern="dd-MM-yyyy"/>"
                    />
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="datefield_trigger_<ww:property value="#tagId"/>" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
                <script type="text/javascript">
                    Calendar.setup({
                        inputField     :    "<ww:property value="#tagId"/>",     // id of the input field
                        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                        button         :    "datefield_trigger_<ww:property value="#tagId"/>",  // trigger for the calendar (button ID)
                        align          :    "Tl",           // alignment (defaults to "Bl")
                        singleClick    :    true
                    });                   
                </script>
            </ww:elseif>            
            <ww:elseif test="type == 'class'">
                <input type="hidden" name="<ww:property value="#tagName"/>"
                        id="<ww:property value="#tagId"/>" value="<ww:property value="rm.getKey(cn)"/>"/>

                <table>
                    <ww:iterator value="dictionaryAttributes">
                        <tr>
                            <td><ww:property value="value"/></td>
                            <td><input type="text" id="<ww:property value="#tagId+key"/>" value="<ww:property value="rm.getValue(cn)[key]"/>"
                                size="50" class="txt" readonly="readonly"/>
                            </td>
                        </tr>
                    </ww:iterator>
                    <tr>
                        <td colspan="2">
                            <input type="button" value="Wybierz" onclick="openDictionary_<ww:property value="cn"/>()" class="btn" <%--<ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>--%>/>
                            <input type="button" value="Wyczy��" onclick="clear_<ww:property value="cn"/>()" class="btn" <%--<ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>--%>/>
                        </td>
                    </tr>
                </table>

               <script type="text/javascript">
                    function openDictionary_<ww:property value="cn"/>()
                    {
                        openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+
                                       '&id='+document.getElementById('<ww:property value="#tagId"/>').value,'<ww:property value="cn"/>', 700, 610);
                    }

                    function clear_<ww:property value="cn"/>()
                    {
                        document.getElementById('<ww:property value="#tagId"/>').value = '';
                        <ww:iterator value="dictionaryAttributes">
                            document.getElementById('<ww:property value="#tagId+key"/>').value = '';
                        </ww:iterator>
                    }

                    function accept_<ww:property value="cn"/>(map)
                    {
                        document.getElementById('<ww:property value="#tagId"/>').value = map.id;
                        <ww:iterator value="dictionaryAttributes">
                            document.getElementById('<ww:property value="#tagId+key"/>').value = map.<ww:property value="key"/>;
                        </ww:iterator>
                    }

                    functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
                </script>
            </ww:elseif>
            <ww:else>
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="rm.getKey(cn)"/>" class="txt" <ww:if test="!canEdit">readonly="readonly"</ww:if>/>
            </ww:else>
        </td>
    </tr>
</ww:if>
</ww:iterator>

<script type="text/javascript">

    function validateRegistry()
    {
        var field, val;
        <%-- sprawdzanie obligatoryjno�ci p�l --%>
        <ww:iterator value="rm.fields">
            <ww:set name="prefix" value="'registry_'"/>
            <ww:set name="tagId" value="#prefix+cn"/>
            <%-- [<ww:property value="rm.mainFieldCn"/>] [<ww:property value="cn"/>] [<ww:property value="required"/>] --%>
            <ww:if test="required">
                field = document.getElementById('<ww:property value="#tagId"/>');
                if (field != null)
                {
                    if (emptyField(field))
                    {
                        alert('Pole <ww:property value="name"/> jest obowi�zkowe!');
                        return false;
                    }
                }
            </ww:if>
            <%-- sprawdzanie czy nie wpisano za d�ugich warto�ci --%>
            <ww:if test="type == 'string'">
                field = document.getElementById('<ww:property value="#tagId"/>');
                if (field != null)
                {
                    if (field.value.trim().length > <ww:property value="length"/>)
                    {
                        alert('Warto�� pola <ww:property value="name"/> nie mo�e by� d�u�sza ni� <ww:property value="length"/>!');
                        return false;
                    }    
                }
            </ww:if>
        </ww:iterator>
        
        <%-- pozosta�a walidacja p�l --%>
        <ww:if test="rm.registry.validationRules != null">
            <ww:iterator value="rm.registry.validationRules">
                <ww:if test="type == 'min-length'">
                    val = document.getElementById('registry_<ww:property value="fieldCn"/>').value;
                    if ((val != null) && (val.trim().length < <ww:property value="value"/>))
                    {
                        alert('Warto�� pola <ww:property value="rm.getField(fieldCn).name"/> musi mie� conajmniej <ww:property value="value"/> znak�w');
                        return false;
                    }
                </ww:if>
                <ww:if test="type == 'min-length-no-spaces'">
                    val = document.getElementById('registry_<ww:property value="fieldCn"/>').value;
                    if ((val != null) && (val.replaceAll(' ', '').length < <ww:property value="value"/>))
                    {
                        alert('Warto�� pola <ww:property value="rm.getField(fieldCn).name"/> musi mie� conajmniej <ww:property value="value"/> znak�w z wy��czeniem spacji');
                        return false;
                    }
                </ww:if>
            </ww:iterator>
        </ww:if>
                
        return true;
    }   
</script>
