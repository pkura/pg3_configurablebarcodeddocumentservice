<%-- 
    Document   : recipient-lookup
    Created on : 2009-06-24, 14:44:37
    Author     : Micha� Sankowski <michal.sankowski@docusafe.pl>
--%>
<%@ page contentType="application/json; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
[<ww:iterator value="users" status="status"><ww:if test="!#status.first">,</ww:if>{
	"recipientLastname":"<ww:property value="lastname"/>",
	"recipientFirstname":"<ww:property value="firstname"/>",
	"recipientName":"<ww:property value="name"/>",
	"recipientDivision":"<ww:property value="getDivisions()[0].getGuid()"/>",
	"recipientDivisionName":"<ww:property value="getDivisions()[0].getName()"/>"
}</ww:iterator>]
 


