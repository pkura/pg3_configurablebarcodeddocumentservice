<!--N box.jsp POCZATEK N-->
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
	<ds:available test="addition.box.available">
	<tr>
		<ww:if test="pudloModify">
		<td>
			<ds:lang text="PudloArchiwalne"/>:
		</td>
		<td>
			<ww:select name="'boxId'" id="archiveBox" list="availableArchiveBoxes" listKey="key" listValue="value" cssClass="'sel dontFixWidth'" />
			<script type="text/javascript">
				$j('#archiveBox').change(function(){
					$j('#boxId').val($j(this).val());
				});
			</script>
		</td>
		</ww:if>
		<ww:else>
			<ww:hidden name="'boxNumber'" value="boxNumber"></ww:hidden>
	        <td><ds:lang text="PudloArchiwalne"/>:</td>
			<td><ww:property value="boxNumber"/></td>
		
		</ww:else>					
	</tr>
	<ww:if test="pudloModify">
	<tr>
		<td></td>
		<td>
		<input style="width: 200px;" name="setBox" type="button" class="btn btnLeft" value="<ds:lang text="NadajNumerOtwartegoPudla"/>"
				onclick="document.getElementById('archiveBox').value = <ww:property value="currentBoxId"/>;
				<ww:if test="currentBoxId != null">$j('#boxId').val('<ww:property value="currentBoxId"/>');</ww:if>"
				<ww:if test="currentBoxNumber == null">disabled="true"</ww:if> />
			<input style="width: 103px;" type="button" class="btn<ww:if test="canUpdate && blocked"> btnMiddle</ww:if><ww:else> btnRight</ww:else>" 
				value="<ds:lang text="AnulujWybor"/>"
				onclick="document.getElementById('archiveBox').value = 0; document.getElementById('boxId').value = null"
				<ww:if test="currentBoxNumber == null">disabled="true"</ww:if> title="<ds:lang text="AnulujWyborPudlo"/>"/>
			<ww:if test="canUpdate && blocked">
				<ds:submit-event cssClass="btn btnRight" value="getText('ZapiszPudlo')" name="'doUpdateBox'"/>
			</ww:if>
		</td>
	</tr>
	</ww:if>
	<ww:if test="needsNotBox">
		<tr>
			<td></td>
			<td>
				<ds:lang text="DokumentNieWymagaUmieszczeniaWpudleArchiwalnym"/>
			</td>
		</tr>
	</ww:if>
	</ds:available>
<!--N box.jsp KONIEC N-->