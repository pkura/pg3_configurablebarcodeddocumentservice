<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!-- poczatek search-dockind-fields.jsp -->

<script type="text/javascript">
    /**
     * Tablica (mapa) funkcji, kt�re b�d� wykorzystane przez poszczeg�lne s�owniki do
     * przekazania wybrane przez u�ytkownika dane. Kluczem jest 'cn' a warto�ci�
     * funkcja.
     */
    var functions = new Array();

    /**
     * Otrzymuje dane od s�ownika i 'cn' pola, aby m�c rozpozna�, kt�r� funkcj�
     * akceptuj�c� wywo�a�,
     */
    function accept(cn, map)
    {
        functions[cn](map);
    }

	function changeDisplayRange(idname)
	{
		var element_from = document.getElementById(idname+'_from');
		var element_to = document.getElementById(idname+'_to');
	    var element2 = document.getElementById('in'+idname);
	    var element_od = document.getElementById('od:'+idname);
	    var element_do = document.getElementById('do:'+idname);

	    if(element_from.style.display == 'none')
	    {
	    	element_from.style.display = '';
	    	element_to.style.display = '';
	    	element_od.style.display = '';
	    	element_do.style.display = '';
	    	element2.style.display = 'none';
	    	document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif';	
	    }
	    else
	    {
	    	element_from.style.display = 'none';
	    	element_to.style.display = 'none';
	    	element_od.style.display = 'none';
	    	element_do.style.display = 'none';
	    	element2.style.display = '';
	    	document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/gora.gif';		    
	    }
	}
    
    function changeDisplay(name,idname)
    {
       var element = document.getElementById(idname);
       var element2 = document.getElementById('in'+idname);
       if(element.style.display == 'none')
       {
        element.style.display = '';
        element2.style.display = 'none';
        document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif';								
		}
		else
		{
			element.style.display = 'none';
            element2.style.display = '';
            document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/gora.gif';
		}						
     }

    function changeDisplayClass(name,idname,attrName)
    {
       var element2 = document.getElementById('td_area'+idname);
       var element3 = document.getElementById(idname+'_attrName');
       var calssTBody = document.getElementById('class_'+idname);
	   element3.value = attrName;


	   
	   try {
	   		$j(calssTBody).parents('table.leftAlign').css('left', '-3px');
	   } catch(e) {}
	   
       if(calssTBody.style.display == 'none')
       {
        calssTBody.style.display = '';
        element2.style.display = 'none';
        document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif';								
		}
		else
		{
			calssTBody.style.display = 'none';
            element2.style.display = '';
            document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/gora.gif';
		}						
     }

    /**
     * Sprawdza czy potrzebna aktualizacja - je�li tak to wywo�uje funkcj� akceptuj�c�.
     * Wo�ane, gdy w s�owniku w�a�nie zapisano zmiany i nie umieszczono ich w tym formularzu.
     */
    function checkAccept(cn, map)
    {
        if (parseInt(document.getElementById('dockind_'+cn).value) == parseInt(map.id))
            accept(cn,map);
    }
    
    function selectOPS()
	{
		var ids = new Array();
		<ww:if test="opsParts == null">
			alert('Nie wybrano �adnego dokumentu dla OPS\nUstwaienia -> OPS');
		</ww:if>
		<ww:elseif test="opsParts != null">
			<ww:iterator value="opsParts">
			<ww:set name="result"/>
				ids[ids.length] = "<ww:property value="#result"/>";
			</ww:iterator>
		</ww:elseif>
		if(ids.length > 0)
		{
			for(i=0; i<getElementByName('values.RODZAJ').options.length; i++)
			{
				for(j=0; j<ids.length; j++)
				{
					if(getElementByName('values.RODZAJ').options[i].value == ids[j])
					{
						getElementByName('values.RODZAJ').options[i].selected = true;
					}
				}	
			}	
		}
		
		//getElementByName('values.RODZAJ').options[1].selected = true;
		//getElementByName('values.RODZAJ').options[9].selected = true;
		
	}

	function refreshVisibleFields(caller){
		//alert("refreshVisibleFields");
		var updated = false;
		$j(".enum_ref").show();
		$j(".refreshable_field").hide();
		$j(".enum_ref div").hide();
		$j("select:visible").each(function(){
			try{
			if(this.selectedIndex >= 0 && this.selectedIndex < this.options.length){
				if(this.cn == null){
					this.cn = this.id.replace(/dockind_/,'');
				}
				var classname = ".if_" + this.cn + "_" + this.options[this.selectedIndex].value;
				updated |= $j(classname).css("display","").size() > 0;
			}
			} catch(x){}//ignore exception
		});

		$j("input[type='hidden']").each(function(){
			if(this.cn == null){
				this.cn = this.id.replace(/dockind_/,'');
			}
			var classname = ".if_" + this.cn + "_" + this.value;
			updated |= $j(classname).css("display","").size() > 0;
		});

		$j("tr:hidden select option").each(function(){
			this.selected = false;
		});

		$j("div:hidden select option").each(function(){
			this.selected = false;
		});

		$j("tr:hidden :text").each(function(){
			this.value = "";
		});
		
		if(updated){
			$j(caller).after($j("#updateMessage"));
			$j("#updateMessage").show();
			$j("#updateMessage").fadeOut("slow", function(){
				$j("#updateMessage").fadeIn("slow");
				$j("#updateMessage").hide();
			});
		}
	}

	$j(document).ready(function(){
		if($j("#searchStyle").get(0).value == "only-related"){
			$j(".refreshable_field").hide();
			$j("tr.dockind_field select").change(function(){
				refreshVisibleFields(this);
			});
		}
	});
    
</script>
<input type="hidden" name="rozwiniete" id="rozwiniete" value="true"/>

<span style="display:none" id="updateMessage"> <ds:lang text="uaktualnionoPola"/></span>

<ww:iterator value="kind.getFieldsByAspect(documentAspectCn)">
<ww:if test=" searchShow || (!hidden && !searchHidden)">
    <ww:set name="prefix" value="'dockind_'"/>
    <ww:set name="tagName" value="'values.'+cn"/>
    <ww:set name="tagId" value="#prefix+cn"/>
    
    <tr id="<ww:property value="'tr_'+#tagId"/>"
		class="dockind_field <ww:if test="availableWhen.size()>0">refreshable_field </ww:if><ww:iterator value="availableWhen">if_<ww:property value="fieldCn"/>_<ww:property value="fieldValue"/> </ww:iterator><ww:if test="type == 'enum-ref'"> enum_ref</ww:if>">
        <td valign="top"><ww:property value="name"/>:</td>
        <td>
            <ww:include page="/common/search-dockind-field.jsp"/>
        </td>
    </tr>
</ww:if>
</ww:iterator><%-- fields iterator --%>
<script type="text/javascript">	
<ww:if test="#searchStyle == 'only-related'">



    function onchange_main_field(mainField)
    {
		<%--
        var tr, field, span;
        var value = mainField != null ? mainField.value : null;
        <ww:iterator value="kind.fields">
            <ww:set name="prefix" value="'dockind_'"/>
            <ww:set name="tagId" value="#prefix+cn"/>
            <ww:if test="kind.mainFieldCn != cn">
                <ww:if test="availableWhen != null && availableWhen.size() > 0">
                    tr = document.getElementById('<ww:property value="'tr_'+#tagId"/>');
                    field = document.getElementById('<ww:property value="#tagId"/>');
                    if (tr != null)
                    {
                        <ww:iterator value="availableWhen" status="status">
                            <ww:if test="!#status.first">else </ww:if>
                            <ww:if test="type == 'main'">
                                if (value == <ww:property value="fieldValue"/>)
                                    tr.style.display = "";
                            </ww:if>
                            <ww:else>
                                if (document.getElementById('dockind_<ww:property value="fieldCn"/>').value == <ww:property value="fieldValue"/>)
                                    tr.style.display = "";
                            </ww:else>
                        </ww:iterator>
                        else
                        {
                            tr.style.display = "none";
                            <ww:if test="type == 'date'">
                                document.getElementById('<ww:property value="#tagId"/>_from').value = '';
                                document.getElementById('<ww:property value="#tagId"/>_to').value = '';
                            </ww:if>                                
                            <ww:else>
                                if (field.tagName.toUpperCase() == 'INPUT')
                                {
                                    <ww:if test="type == 'class'">
                                        clear_<ww:property value="cn"/>();                                    
                                    </ww:if>
                                    <ww:else>
                                        field.value = '';
                                    </ww:else>
                                }
                                else if (field.tagName.toUpperCase() == 'SELECT')
                                {
                                    //alert('<ww:property value="cn"/>'+field.multiple);
                                    if (field.multiple)
                                        select(getElementByName('values.<ww:property value="cn"/>'), false);
                                    else
                                        field.options.selectedIndex = 0;                                        
                                }
                            </ww:else>
                        }
                    }
                </ww:if>
            </ww:if>
        </ww:iterator> 
        
       if ((mainField != null) && (additional_onchange_main_field))
        {
            additional_onchange_main_field(mainField);
        } --%>
    }
    
    // pocz�tkowa inicjalizacja p�l na podstawie warto�ci g��wnego pola
    onchange_main_field(document.getElementById('<ww:property value="'dockind_'+kind.mainFieldCn"/>'));        
   
</ww:if>
<ww:else>
    function onchange_main_field(mainField)
    {
		
        var tr, field, span;
        var value = mainField != null ? mainField.value : null;
        <ww:iterator value="kind.fields">
            <ww:set name="prefix" value="'dockind_'"/>
            <ww:set name="tagId" value="#prefix+cn"/>
			
			<ww:if test="type == 'dataBase'">
            	var mainFieldId = mainField.id.substring(8);
            	if(mainField.id != '<ww:property value="#tagId"/>' && '<ww:property value="refField"/>' == mainFieldId)
            	{
            		<ww:property value="'update_database'+#tagId"/>();
            	}
            </ww:if>
            
        </ww:iterator> 
    }
</ww:else>
</script> 
<script type="text/javascript">
    //ustawianie focusa
    <ww:if test="kind.properties['search-focus'] != null">
    document.getElementById('dockind_<ww:property value="kind.properties['search-focus']"/>').focus();
    </ww:if>


    $j(document).ready(function() {
	    //alert('onchanges');
		/* wymuszamy zdarzenie onchange na widocznych selectach */
		$j("tr.dockind_field select:visible").each(function() {
			var selIndex = this.selectedIndex;

			if(selIndex != -1) {
				$j(this).trigger('change');
			}
		});
    });
    
</script>


<!-- koniec search-dockind-fields.jsp -->
