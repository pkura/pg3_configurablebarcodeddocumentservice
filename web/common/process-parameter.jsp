<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Proces"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form action="<ww:url value="'/common/process-parameter.action'"/>" method="post">
<%--
<table>
<ww:iterator value="beans">
	<ww:property value="processName"/>
	<ww:iterator value="checkpoints">
		<tr>
			<td><ww:property value="getCheckPointName()"/></td>
			<td><ww:property value="getCheckpointScore()"/></td>
			<td><ww:property value="isCheckpointFinal()"/></td>
			<td><ww:property value="isFulfilled()"/></td>
			<td><ww:property value="isCurrentCheckpoint()"/></td>
		</tr>
	</ww:iterator>
	<ww:iterator value="alerts">
		<tr>
			<td><ww:property value="getAlertName()"/></td>
			<td><ww:property value="getAlertCode()"/></td>
			<td><ww:property value="isAlertPresent()"/></td>
			<td><ww:property value="getAlertScore()"/></td>
			<td><ww:property value="getAlertType()"/></td>
		</tr>
	</ww:iterator>
</ww:iterator>
</table>
--%>

<ww:iterator value="beans">
<fieldset class="procParam">
<legend><ww:property value="processName"/></legend>

<table class="procParam" border=0 cellpadding=0 cellspacing=0>
	<tbody>
	<tr class="circle">
		<ww:iterator value="checkpoints" status="status">
			<td>
				<div class="procSpacerShort <ww:if test="#status.first">procSpacerHidden</ww:if>">&nbsp;</div>
				<ww:if test="isFulfilled()">
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/circle1.gif" />
				</ww:if>
				<ww:if test="isCurrentCheckpoint()">
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/circle2.gif" />
				</ww:if>
				<ww:if test="!isFulfilled() && !isCurrentCheckpoint()">
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/circle3.gif" />
				</ww:if>
				
				<div class="procSpacerShort <ww:if test="#status.last">procSpacerHidden</ww:if>" >&nbsp;</div>
			</td>
		</ww:iterator>
	</tr>
	
	<tr class="desc">
		<ww:iterator value="checkpoints">
			<td class="<ww:if test="isFulfilled()">green</ww:if><ww:if test="isCurrentCheckpoint()">orange</ww:if><ww:if test="!isFulfilled() && !isCurrentCheckpoint()">red</ww:if>" >
				<ww:property value="getCheckPointName()" />	
			</td>
		</ww:iterator>
	</tr>
	</tbody>
</table>
</fieldset>
</ww:iterator>

</form>