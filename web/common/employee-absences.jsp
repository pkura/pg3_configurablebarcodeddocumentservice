<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>


<ds:available test="documentmain.emplAbsences" documentKindCn="documentKindCn">
	<a href="<ww:url value="'/pdf-absences.action'">
				 	<ww:param name="'empCardName'" value="empCardName" />
				 	<ww:param name="'currentYear'" value="currentYear" />
				 	<ww:param name="'doPdf'" value="'true'" />
				 </ww:url>">
			<img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>" />
			<ds:lang text="Pobierz karte PDF" />
		</a>
<!-- LISTA URLOP�W PRACOWNIKA -->
<table class="search mediumTable table100p">
<thead>
	<tr>
		<th class="empty">
			<ds:lang text="Lp"/>	
		</th>
		<th class="empty">
			<ds:lang text="DataOd"/>
		</th>
		<th class="empty">
			<ds:lang text="DataDo"/>
		</th>
		<th class="empty">
			<ds:lang text="LiczbaDni"/>
		</th>
		<th class="empty">
			<ds:lang text="TypUrlopu"/>
		</th>
		<th class="empty">
			<ds:lang text="PozostaloDniUrlopu"/>
		</th>
		<th class="empty">
			<ds:lang text="DodatkoweUwagi"/>
		</th>
	</tr>
</thead>
<tbody>
<ww:iterator value="emplAbsences">
	<tr>
		<td>
			<ww:property value="lp"/>
		</td>
		<td>
			<ds:format-date pattern="yyyy-MM-dd" value="absence.startDate"/>
		</td>
		<td>
			<ds:format-date pattern="yyyy-MM-dd" value="absence.endDate"/>
		</td>
		<td>
			<ww:property value="absence.daysNum"/>
		</td>
		<td>
			<ww:property value="absence.absenceType.name"/>
		</td>
		<td>
			<ww:property value="restOfDays"/>
		</td>
		<td>
			<ww:property value="absence.info"/>
		</td>
	</tr>
</ww:iterator>
</tbody>
</table>
<!-- END / LISTA URLOP�W PRACOWNIKA -->

<script type="text/javascript">
<ww:iterator value="freeDays">
	addFreeDay('<ds:format-date pattern="dd-MM-yyyy" value="date" />');
</ww:iterator>
</script>
</ds:available>
