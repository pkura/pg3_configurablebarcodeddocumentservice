<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--N poczatek dockind-fields.jsp N-->
<!-- list-value -->
<script type="text/javascript">
	var pressedKey = 0;
	var ajaxResult = null;
	var prefix = '';
	/* usuwa poczatkowe spacje */
	String.prototype.trim = function() {
		return this.replace(/^\s*|\s*$/i, '');
	}
	function putItem() {
		var itm = ajaxResult[selectedItem];
		if(itm) {
			var c = 0;
			for(property in itm){
				var jq_sel = '#prompt_item_' + selectedItem + ' td.' + property; // wybieramy komorke
				var val = '';

				 // pobieramy tekst z komorki
				if(ie)
					val = $j(jq_sel).attr('innerText');
				else
					val = $j(jq_sel).attr('textContent');

				// nic nie wstawiamy je�li to nag��wek tabeli
				if (val == 'ID') {
					 $j('#promptDiv').css('visibility', 'hidden');
					 return;
				}
				//zapisujemy do input text
				trace('Writing ' + val + ' to ' + '#' +  'dockind_' + property + '  JQUERY: ' + $j('#' +  'dockind_' + property).attr('id'));
				$j('#' +  'dockind_' + property).attr('value', val);
			}
			$j('#promptDiv').css('visibility', 'hidden');
		}
	}

	/**
	 * Tablica (mapa) funkcji, kt�re b�d� wykorzystane przez poszczeg�lne s�owniki do
	 * przekazania wybrane przez u�ytkownika dane. Kluczem jest 'cn' a warto�ci�
	 * funkcja.
	 */
	var functions = new Array();
	var functionsUpdate = new Array();

	/**
	 * Otrzymuje dane od s�ownika i 'cn' pola, aby m�c rozpozna�, kt�r� funkcj�
	 * akceptuj�c� wywo�a�,
	 */
	function accept(cn, map)
	{
		functions[cn](map);
	}

	/**
	 * Otrzymuje dane od s�ownika i 'cn' pola, aby m�c rozpozna�, kt�r� funkcj�
	 * akceptuj�c� aktualizujaca wywo�a�,
	 */
	function acceptUpdate(cn, map)
	{
		functionsUpdate[cn](map);
	}

	/**
	 * Sprawdza czy potrzebna aktualizacja - je�li tak to wywo�uje funkcj� akceptuj�c�.
	 * Wo�ane, gdy w s�owniku w�a�nie zapisano zmiany i nie umieszczono ich w tym formularzu.
	 */
	function checkAccept(cn, map)
	{
		if (parseInt(document.getElementById('dockind_'+cn).value) == parseInt(map.id))
			accept(cn,map);
	}
	
	/**
	 * Tablica (mapa) funkcji, kt�re b�d� wywo�ane podczas submit-owania formularza.
	 */
	var onsubmitFunctions = new Array();
	
	/**
	 * Wywo�uje wszystkie zarejestrowane funkcje 'onsubmit'.
	 */
	function onsubmitFunction()
	{
		for (func in onsubmitFunctions)
		{
			onsubmitFunctions[func]();
		}
	}
	var isChanges = false;
	var nulek = null;

	/*
		Funkcja wywo�ywuje si� bardzo d�ugo w ie (oko�o 500ms), 
		wi�c wywo�ujemy cia�o z timera, �eby ie nie zamiera� na wybranym selekcie.
	*/
	function refreshVisibleFields(caller) {
		traceStart('refreshVisibleFields');
		<ww:if test="'dlbinder'.equals(fm.getDocumentKind().getCn())">
			return;
		</ww:if>

		/* ie slow js */
		setTimeout(function() {
		
		timeStart();	
		var updated = false;
		$j("tr.enum_ref").show();
		$j("tr.refreshable_field").addClass("autoHidden").hide();
		$j("tr.enum_ref tr:not(.dontHide)").hide();
		$j("span.star:not(.always)").hide();
		//$j("select:visible").each(function(){
		$j("tbody[id=tbodyDockindFields] select").each(function(){
			try{
				if(this.selectedIndex >= 0 && this.selectedIndex < this.options.length && this.options[this.selectedIndex].value != null && this.options[this.selectedIndex].value != ''){
					if(this.cn == null){
						this.cn = this.id.replace(/dockind_/,'');
					}
					var classname = "if_" + this.cn + "_" + this.options[this.selectedIndex].value;
					updated |= $j('tr.' + classname).css("display","").size() > 0;

					//trace(classname);

					classname = "required_" + this.cn + "_" + this.value + " .star";
					updated |= $j('tr.' + classname).css("display","").size() > 0;
				}
			} catch(x){}//ignore exception
		});
		
		$j("tbody[id=tbodyDockindFields] input[type='hidden']").each(function(){
			if(this.cn == null){
				this.cn = this.id.replace(/dockind_/,'');
			}
			
			var classname = "if_" + this.cn + "_" + this.value;
			updated |= $j('tr.' + classname).css("display","").size() > 0;

			classname = "required_" + this.cn + "_" + this.value + " .star";
			updated |= $j('tr.' + classname).css("display","").size() > 0;
		});

		$j("tbody[id=tbodyDockindFields] input[type='checkbox']").each(function(){
			if(this.cn == null){
				this.cn = this.id.replace(/dockind_/,'');
			}
			var classname = "if_" + this.cn + "_" + (this.checked?"1":"0");
			updated |= $j('tr.' + classname).css("display","").size() > 0;

			classname = "required_" + this.cn + "_" + (this.checked?"1":"0") + " .star";
			updated |= $j('tr.' + classname).css("display","").size() > 0;
		});

		$j("tbody[id=tbodyDockindFields] tr.autoHidden:hidden select").each(function(){
			this.selectedIndex = 0;
		});

		$j("tbody[id=tbodyDockindFields] div.autoHidden:hidden select").each(function(){
			this.selectedIndex = 0;
		});

		$j("tbody[id=tbodyDockindFields] tr.autoHidden:hidden :text").each(function(){
			this.value = "";
		});

		$j("tbody[id=tbodyDockindFields] tr.autoHidden:hidden :checkbox").each(function(){
        	this.checked = false;
        });

		onchange_fields();
		additional_onchange_main_field(document.getElementById('<ww:property value="'dockind_'+fm.mainFieldCn"/>'));

		$j('select.dockindWide:visible').each(function() {
			traceStart('Id of select: ' + this.id);

			var jqSel = $j(this);
			var len = jqSel.children('option:first').width();

			trace(len);

			<%-- �eby nie popsu� wcze�niejszych --%>
			if((ie6 || ie7) && $j(this).hasClass('enum-ref')) {
				$j(this).css('left', $j(this).parent('td').position().left + 'px');
			}

			<%-- je�li len = 0, to jest to oczywi�cie ie6/ie7 --%>
			if(len > 400 || len == 0) {
				jqSel.unbind('change.showTxt');
				jqSel.bind('change.showTxt', function() {
					traceStart('OnChange fired for select: ' + this.id);
						
					var selIndex = this.selectedIndex;
					trace('Selected option: ' + selIndex);

					var selFullId = this.id + '_fullTxt';
					if((ie7 || ie6) && $j(this).parent().hasClass('dockindWide')) {
						trace('Setting width to 304px: ' + $j(this).attr('id'));
						$j(this).css('width', '304px');
					}
					/* pierwsza opcja to 'wybierz' */
					if(selIndex > 0) {
						var selFullTxt = $j(this).children()[selIndex].text;
						
						trace(selFullId + ': ' + selFullTxt);

						$j('#' + selFullId).text(selFullTxt).css('width', '245px'); // 260 bo mniejsza czcionka i <option> ma padding na strza�k�

						/* je�li tylko jedna linijka, to nie wy�wietlamy (bo i tak zmie�ci si� w selectie) */
						if($j('#' + selFullId).height() <= 11) {
							trace('only one line');
							$j('#' + selFullId).text('');
						} else {
							trace('multiline');
							$j('#' + selFullId).css('width', '300px')
						}
					} else {
						trace('selIndex <= 0');
						$j('#' + selFullId).text('');
					}
						
					traceEnd();
				});
			}

			$j(this).trigger('change.showTxt');
			traceEnd();
		}); // end of $j('select.dockindWide:visible').each(function()

		/* specjalnie dla ie6/ie7 - pokazujemy select na ca�� d�ugo��, bo inaczej opcje s� zbyt kr�tkie */
		var __currentSelect = null;
		if(ie7 || ie6) {
			$j('select.dockindWide').each(function() {
				$j(this).css('width', 'auto');
				/*if($j(this).width() <= 304) {
					//$j(this).removeClass('dockindWide');
				}
				else*/
				$j(this).css('width', '304px');
			});
			
			$j('select.dockindWide').unbind('mousedown.setCur');
			$j('select.dockindWide').unbind('mouseover.toExpand');
			$j('select.dockindWide').unbind('mouseout.toExpand');

			$j('select.dockindWide').bind('mousedown.setCur', function() {
				__currentSelect = $j(this).attr('id');
			});

			$j('select.dockindWide').bind('mouseover.toExpand', function() {
				$j(this).css('width', 'auto');
				if($j(this).width() < 304) {
					$j(this).css('width', '304px');
				}
				__currentSelect = null;
			});

			$j('select.dockindWide').bind('mouseout.toExpand', function() {
				if(__currentSelect != $j(this).attr('id')) {
					__currentSelect = null;
					trace('Setting width to 304px');
					$j(this).css('width', '304px');
				}
			});
		}
		
		/* end of fix */		
		fixItemsWidth();
		timeEnd('timeDiv');
		}, 0);

		traceEnd();
	}

	$j(document).ready(function(){
		$j(".refreshable_field").hide();
		$j("tr.dockind_field select:not(.multi_sel)").change(function(){
			refreshVisibleFields(this); 
		});
		refreshVisibleFields(null);
	});
	
	function onchange_fields() {
		<%-- jezeli zbyt brzydkie rozwiazanie to mozna pobawic sie przez dodanie do xmla dockindu dla enum-item atrybutu (tablicy) defaultFor --%>
		<ww:if test="fm.getDocumentKind().cn == 'sad' || fm.getDocumentKind().cn =='invoice_ic'">
		try{
            if ( document.getElementById('dockind_WALUTA').selectedIndex == 0){
                if (document.getElementById('dockind_RODZAJ_FAKTURY').value == '774'){ // zakup UE => EURO
                    $j("#dockind_WALUTA option[value='10']").attr("selected","selected");
                }
                else if (document.getElementById('dockind_RODZAJ_FAKTURY').value == '773'){ // faktura krajowa => PLN
                    $j("#dockind_WALUTA option[value='290']").attr("selected","selected");
                }
            }
		} catch (e) {}
		</ww:if>
	}
</script>

<ww:hidden name="'values.dockindEvent'" id="dockind_dockindEvent"/>
<span style="display:none" id="updateMessage"> <%--<ds:lang text="uaktualnionoPola"/>--%></span>
<div id="promptDiv"></div><div id="ajaxData" style="display: none"></div>
<div id="ajaxError"></div><div style="display:none;" id="timeDiv"></div>

<ww:hidden name="'doDockindEvent'" id="doDockindEvent"/>
<ww:hidden name="'dockindEventValue'" id="dockindEventValue"/>
<ww:hidden name="'doExecuteDroolsRule'" id="doExecuteDroolsRule"/>
<ww:hidden name="'droolsRuleCn'" id="droolsRuleCn"/>

<tbody id="tbodyDockindFields">
<ww:iterator value="fm.getFieldsByAspect(documentAspectCn)">
	<ww:set name="fieldResult"/>
	<ww:set name="prefix" value="'dockind_'"/>
	<ww:set name="status_cn" value="'STATUS'"/>
	<ww:set name="tagName" value="'values.'+cn"/>
	<ww:set name="tagId" value="#prefix+cn"/>
	<ww:set name="dictionaryClass" value="classname"/>
	<ww:set name="fieldName" value="name"/>
	<ww:set name="showName" value="((type != 'enum') || ((type == 'enum') && (refStyle != 'all-visible'))) && type != 'dockindButton'"/>
	<ww:if test="!newDocumentShow && isNewDocumentAction() != null && isNewDocumentAction()">
	</ww:if>
	<ww:elseif test="!hidden">
		<tr id="<ww:property value="'tr_'+#tagId"/>"  <%--  wyrzucilem ten dziubek --%>
		    <ww:if test="!fm.getShemeVisible(cn)" > <%-- Je�li schema okre�la pole jako ukryte to pomi� je przy stylowaniu i okre�laniu wymagalno�ci --%>
		        style="display:none;">
		    </ww:if><ww:else>
                class="dockind_field <ww:if test="requiredWhen != null && requiredWhen.size() > 0"><ww:iterator value="requiredWhen">required_<ww:property value="fieldCn"/>_<ww:property value="fieldValue"/> </ww:iterator></ww:if> <ww:if test="availableWhen.size()>0">refreshable_field </ww:if><ww:iterator value="availableWhen">if_<ww:property value="fieldCn"/>_<ww:property value="fieldValue"/> </ww:iterator><ww:if test="type == 'enum-ref'"> enum_ref</ww:if>"
                <ww:if test="fm.getEnableSheme(cn)">
                    style="display:<ww:property value="fm.getShemeDisplay(cn)"/>;"
                </ww:if>   >
			</ww:else>
			<ww:if test="fm.getDisabled(cn)
                &&  !((type == 'dataBase' && !canedit)
                      ||(type == 'list-value')
                      ||(type == 'string')
                      ||(type == 'enum')
                      ||(type == 'enum-ref')
                      ||(type == 'class' && !multiple)
                      ||(type == 'date')
                      ||(type == 'bool') || (type == 'dsuser'))">
				<input type="hidden" name="<ww:property value="#tagName"/>"
					id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>                                
			</ww:if>
            <ww:if test="(type == 'dsuser' && fm.getDisabled(cn) )">
                <ww:if test="multiple">
                    <div class="hidden_user_multiple" style="display: none">
                        <select class="multi_sel dontFixWidth" size="10" multiple="multiple"
                                                id="<ww:property value="#tagId"/>" <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
                                                name="<ww:property value="#tagName"/>"
                                                onchange="<ww:property value="fm.mainFieldCn == cn ? 'onchange_main_field(this)' : ''"/>">

                                                <ww:if test="fm.mainFieldCn != cn && !required">
                                                        <option value="">
                                                                <ds:lang text="select.wybierz"/>
                                                        </option>
                                                </ww:if>
                                                <ww:set name="fieldCn" value="cn"/>
                                                <ww:iterator value="enumItems">
                                                <ww:if test="available || fm.getKey(#fieldCn).contains(id)">
                                <option value='<ww:property value="id"/>' <ww:if test="fm.getKey(#fieldCn).contains(id)">selected="selected"</ww:if>>
                                <ww:property value="title"/>
                                </option>
                                                        </ww:if>
                                                </ww:iterator>
                        </select>
                    </div>
                </ww:if>
                <ww:else>
                    <input type="hidden" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>_hidden" value="<ww:property value="fm.getKey(cn)"/>"/>
                </ww:else>
            </ww:if>
            <ww:if test="(fm.documentKind.cn == 'overtime_app' || fm.documentKind.cn == 'overtime_receive' || fm.documentKind.cn == 'wf') && fm.getDisabled(cn)">
                <ww:if test="(type == 'string')">
                    <input type="hidden" name="<ww:property value="#tagName"/>"
                        id="<ww:property value="#tagId"/>_hidden" value="<ww:property value="fm.getKey(cn)"/>"/>
                </ww:if>
                <ww:if test="type == 'date'">
                    <input type="hidden" name="<ww:property value="#tagName"/>"
                        id="<ww:property value="#tagId"/>_hidden" value="<ds:format-date value="fm.getKey(cn)" pattern="dd-MM-yyyy"/>"/>
                </ww:if>
            </ww:if>
                            
			<td class="alignTop leftCell">
				<ww:if test="#showName">
					<ww:property value="name"/>:
					<ww:if test="required">
						<ww:if test="requiredWhen != null && requiredWhen.size() > 0">
							<span id="<ww:property value="'required_'+cn"/>" class="star">*</span>
						</ww:if><ww:else>
							<span class="star always">*</span>
						</ww:else>
					</ww:if>
				</ww:if>
			</td>
			<td align="left">
				<ww:if test="specific">
					<jsp:include page="/common/dockind-specific-fields.jsp"/>
				</ww:if>
				<ww:elseif test="asText">
					<input type="hidden" name="<ww:property value="#tagName"/>"	id="<ww:property value="#tagId"/>"
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
						value="<ww:property value="fm.getKey(cn)"/>"/>
						<ww:property value="fm.getValue(cn)"/>
				</ww:elseif>
				<!-- pola wielowarto�ciowe -->
				<ww:elseif test="multiple">
					<!-- 369 multiple -->
					<ww:if test="type == 'centrum-koszt'">
						<!-- pole typu Field.CENTRUM_KOSZT //-->
						<ww:if test="dockindAction == 'edit'">
							<!-- centr�w kosztowych nie mo�na ustala� przy tworzeniu, poniewa� nie znamy wtedy jeszcze id dokumentu!  --> 
							<table class="tableMargin">	
							<tbody>					
								<ww:if test="fm.getValue(cn) != null">
									<ww:iterator value="fm.getValue(cn)" status="status">
										<ww:set name="result"/>
										<tr id="<ww:property value="'attTr_'+cn+#status.index"/>">
											<td>
												<input type="hidden" name="<ww:property value="#tagName"/>"
													id="<ww:property value="#tagId+#status.index+'h'"/>"
													value="<ww:property value="#result.id"/>"/>
												    <input type="text" name="<ww:property value="#tagId+#status.index"/>"
												        id="<ww:property value="#tagId+#status.index"/>"
												        <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
													    amount="<ww:property value="#result.amount"/>"
													    value="<ww:property value="#result.description"/>"
													    size="35" maxlength="<ww:property value="length"/>" class="txt" readonly="readonly"/>
											        <input id="Edytuj" type="button" value="Edytuj" class="btn"
											        <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
													onclick="updateFieldValue_<ww:property value="cn+'('+#status.index+')'"/>"
													<ww:if test="!canUpdate || fm.getKey('AKCEPTACJA_FINALNA')">disabled="disabled"</ww:if>/>
											</td>
										</tr>
									</ww:iterator>
								</ww:if>
								<tr id="<ww:property value="'attCreateTr_'+cn"/>">						
									<td>
										<input type="button" id="dodajCentrum" value="<ds:lang text="dodajCentrum"/>" class="btn" 
											<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
											onclick="addFieldValue_<ww:property value="cn"/>()" 
											<ww:if test="!canUpdate || fm.getKey('AKCEPTACJA_FINALNA')">disabled="disabled"</ww:if>
											/>
										<ds:available test="centrum.addFromFile">
											<input type="button" value="<ds:lang text="dodajCentrumZPliku"/>" class="btn"
											    <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
											    onclick="openToolWindow('<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>'+
												   '?documentId=<ww:property value="fm.documentId"/>&fileOnly=true','<ww:property value="cn"/>', 700, 350);"/>
										</ds:available>
									</td>
								</tr>
								<ww:if test="fm.acceptancesDefinition.finalAcceptance.exactAmountFieldCn != null">
									<tr>
										<td>
											<table class="tableMargin">
												<tr>
													<td>
														Aktualna suma kwot wybranych centr�w:
													</td>
													<td>
														<input type="text" id="currentCentrumAmount" class="txt" readonly="readonly" style="text-align:right"/>
													</td>
												</tr>
												<tr>
													<td>
														Nieprzydzielona kwota:
													</td>
													<td>
														<input type="text" id="remainingAmount" class="txt" readonly="readonly" style="text-align:right"/>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</ww:if>
							</tbody>
							</table>
							
							<script type="text/javascript">
								var amountVerification = <ww:if test="fm.acceptancesDefinition.finalAcceptance.exactAmountFieldCn != null">true</ww:if><ww:else>false</ww:else>;
								
								<ww:if test="fm.getKey(cn) != null">
									var fieldsSize_<ww:property value="cn"/> = <ww:property value="fm.getKey(cn).size()"/>;
								</ww:if>
								<ww:else>
									var fieldsSize_<ww:property value="cn"/> = 0;
								</ww:else>

								var fieldsSize_<ww:property value="cn"/> = 0;

								function addFieldValue_<ww:property value="cn"/>()
								{
									//bug 368 [wymagane pole opis towaru przy przypisywaniu centrum kosztowego]
									var test=new Boolean(); 
									test = false;
									if(document.getElementById("dockind_OPIS_TOWARU") == null) //if dla backoffice ktory nie ma pola [opis towaru]
									{
										test = true;
									} 
									else 
									{
										tmp = document.getElementById("dockind_OPIS_TOWARU").value;
										if( tmp == "" || tmp.length==0 || tmp == null)
										{
											alert("<ds:lang text="UzupelnijOpisTowaru" />");
										} else
										{
											if($j('#dockind_POTWIERDZENIE_ZGODNOSCI option:selected').val()=='')
											{
												alert('<ds:lang text="UzupelnijPotwierdzenieZgodnosci"/>');
											}
											else
											{
												if($j('#dockind_POTWIERDZENIE_ZGODNOSCI option:selected').val()=='10' && $j('#dockind_UMOWA').val()=='')
												{
													alert('<ds:lang text="UzupelnijUmowe"/>');
												}
												else
												{
													test = true;
												}
											}
										}																				
									}
									if(test)
									{								
											<ww:if test="documentKindCn == 'invoice_ic'">
												var objDzial = null;
												try { objDzial = document.getElementById('dockind_DZIAL').value } catch (e) {};
												openToolWindow('<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>'+
												   '?documentId=<ww:property value="fm.documentId"/>&centrum='+objDzial,'<ww:property value="cn"/>');
											</ww:if><ww:elseif test="documentKindCn == 'sad'">
												openToolWindow('<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>'+
												   '?documentId=<ww:property value="fm.documentId"/>','<ww:property value="cn"/>');
											</ww:elseif><ww:else>
												openToolWindow('<ww:url value="'/office/common/centrum-kosztow.action'"/>'+
												   '?documentId=<ww:property value="fm.documentId"/>','<ww:property value="cn"/>');
											</ww:else>
									}
								}
								
								function doSave()
								{
									$j('#doTmp').val('true');<%-- ustawiane tylko gdy istnieje --%>
									document.forms[0].submit();								
								}
															 
								function acceptAddCentrum(id,description,amount,simple)
								{		 
									var option;
							
									fieldsSize_<ww:property value="cn"/>++;
							
									//alert('bylo '+attachmentsCount);
									var attCreateTr = document.getElementById('attCreateTr_<ww:property value="cn"/>');				
									var tr = document.createElement('tr');				
									tr.setAttribute('id', '<ww:property value="'attTr_'+cn"/>'+fieldsSize_<ww:property value="cn"/>);				
									
									// tworzymy input 'hidden' z id
									var input = document.createElement('input');
									input.setAttribute('type', 'hidden'); 
									input.setAttribute('name', '<ww:property value="#tagName"/>');	  
									input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>+'h');   
									input.setAttribute('value', id);
									var td = createCell(input,true);			
									
									// tworzymy input z nazw�
									input = document.createElement('input');
									input.setAttribute('type', 'text');		  
									input.setAttribute('size', 35);						 
									input.setAttribute('class', 'txt');  
									input.setAttribute('readonly', 'readonly');
									input.setAttribute('value', description);
									input.setAttribute('amount', amount);
									input.className = 'txt';			 
									input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>);			   
									td.appendChild(input);
									tr.appendChild(td);				 
							
									// tworzymy przycisk "edytuj"				
									input = document.createElement('input');				
									input.setAttribute('type', 'button');				
									input.setAttribute('value', 'Edytuj');				
									input.setAttribute('class', 'btn');				
									input.className = 'btn';
									//input.setAttribute('onclick', 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')');				
									// trzeba robic obejscie jak ponizej bo wpp nie dziela pod IE
									var func = 'updateFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')';
									input.onclick = function() { var temp = new Function(func); temp(); };
									tr.appendChild(createCell(input,false));																			
									
									attCreateTr.parentNode.insertBefore(tr, attCreateTr);			   
									
									if (amountVerification)
									{
										document.getElementById('currentCentrumAmount').value = (parseFloat(document.getElementById('currentCentrumAmount').value) + parseFloat(amount)).toFixed(2);
										document.getElementById('remainingAmount').value = (parseFloat(document.getElementById('remainingAmount').value) - parseFloat(amount)).toFixed(2);
									}			
									
									//alert(simple);
									if(simple == 'true' && document.getElementById('dockind_SIMPLE_ACCEPTANCE') != null)
									{
										document.getElementById('dockind_SIMPLE_ACCEPTANCE').checked = 'true';
									}
								}
							
								function updateFieldValue_<ww:property value="cn"/>(no)				
								{
									<ww:if test="documentKindCn == 'invoice_ic'">
									openToolWindow('<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>'+'?param='+no+
												   '&id='+document.getElementById('<ww:property value="#tagId"/>'+no+'h').value,'<ww:property value="cn"/>');
									</ww:if><ww:elseif test="documentKindCn == 'sad'">
									openToolWindow('<ww:url value="'/office/common/centrum-kosztow-ic.action'"/>'+'?param='+no+
												   '&id='+document.getElementById('<ww:property value="#tagId"/>'+no+'h').value,'<ww:property value="cn"/>');
									</ww:elseif><ww:else>
									openToolWindow('<ww:url value="'/office/common/centrum-kosztow.action'"/>'+'?param='+no+
												   '&id='+document.getElementById('<ww:property value="#tagId"/>'+no+'h').value,'<ww:property value="cn"/>');
									</ww:else>
								}
								
								function acceptUpdateCentrum(no,description,amount)
								{			   
									// zmieniamy wiersz z edycj� tego centrum
									var cInput = document.getElementById('<ww:property value="#tagId"/>'+no);   
									if (cInput != null)			 
										cInput.value = description;		   
									// zmieniamy wiersz z akceptacjami dla tego centrum
									var input = document.getElementById('<ww:property value="'centrum_'+cn"/>'+no);   
									if (input != null)			 
										input.value = description;   
																				
										
									if (amountVerification)
									{	
										document.getElementById('currentCentrumAmount').value = (parseFloat(document.getElementById('currentCentrumAmount').value) + parseFloat(amount) - parseFloat(cInput.getAttribute('amount'))).toFixed(2);
										document.getElementById('remainingAmount').value = (parseFloat(document.getElementById('remainingAmount').value) - parseFloat(amount) + parseFloat(cInput.getAttribute('amount'))).toFixed(2);
										cInput.setAttribute('amount', amount);
									}	
								}
								
								function acceptDeleteCentrum(no)
								{			
									var cInput = document.getElementById('<ww:property value="#tagId"/>'+no); 
									if (amountVerification)
									{	
										document.getElementById('currentCentrumAmount').value = (parseFloat(document.getElementById('currentCentrumAmount').value) - parseFloat(cInput.getAttribute('amount'))).toFixed(2);
										document.getElementById('remainingAmount').value = (parseFloat(document.getElementById('remainingAmount').value) + parseFloat(cInput.getAttribute('amount'))).toFixed(2);								
									} 
								   
								   
									// usuwamy wiersz z edycj� tego centrum
									var tr = document.getElementById('<ww:property value="'attTr_'+cn"/>'+no);   
									if (tr != null)			 
										tr.parentNode.removeChild(tr);   
									// usuwamy wiersz z akceptacjami dla tego centrum
									tr = document.getElementById('<ww:property value="'acceptanceTr_'+cn"/>'+no);	   
									if (tr != null)			 
										tr.parentNode.removeChild(tr);																																 
								}	
								
								function initAmountFields()
								{
									if (amountVerification)
									{
										<ww:set name="tmpAmount" value="0"/>
										<ww:set name="fullAmount" value="fm.getKey(fm.acceptancesDefinition.finalAcceptance.exactAmountFieldCn)"/>
										<ww:if test="#fullAmount == null">
											<ww:set name="fullAmount" value="0"/>
										</ww:if>
										<ww:if test="fm.getValue(cn) != null">
										<ww:iterator value="fm.getValue(cn)" status="status">
											<ww:set name="tmpAmount" value="#tmpAmount + amount"/>
										</ww:iterator>							
										</ww:if>
										document.getElementById('currentCentrumAmount').value = (<ww:property value="#tmpAmount"/>).toFixed(2);
										document.getElementById('remainingAmount').value = (<ww:property value="#fullAmount - #tmpAmount"/>).toFixed(2);
										<ww:if test="documentKindCn == 'invoice_ic'||documentKindCn == 'sad'">
										if (parseInt(document.getElementById('remainingAmount').value) > 0) {
											document.getElementById('dodajCentrum').style.backgroundColor = '#FC9494';
										}
										</ww:if>
									}
								}
								initAmountFields();																						 
							</script>  
						</ww:if> 
					</ww:if>					 
					<ww:elseif test="type == 'class'"><!-- typy z�o�one -->
						<ww:if test="asTable">
							<table id="asTable_<ww:property value="#tagId"/>" class="tab">
									<thead>
										<tr>
											<th class="disabled"></th>
											<ww:iterator value="dictionaryAttributes">
												<th><ww:property value="value"/></th>
											</ww:iterator>
										</tr>
									</thead>
									<tbody>
								<ww:iterator value="fm.getValue(cn)" status="dicStatus">
									<ww:set name="dicResult"/>
									<tr class="row_<ww:property value="#tagId"/><ww:if test="#dicStatus.odd == true"> oddRows</ww:if>">
										<td class="disabled">
											<input type="hidden" name="<ww:property value="#tagName"/>"	id="<ww:property value="#tagId+'_'+#dicStatus.index"/>" value="<ww:property value="#dicResult.id"/>"/>											
										</td>
										
										<ww:iterator value="dictionaryAttributes">
											<td>
											<ww:if test="isBoxDescriptionAvailable()">
												<span id="boxA_<ww:property value="#tagId"/>" title="offsetx=[15] offsety=[15] delay=[500] header=[<ww:property value="#fieldName"/>] body=[<ww:property value="getBoxDescription()" escape="false"/>]">
											</ww:if>
											<ww:property value="#dicResult[key]"/>
											<ww:if test="isBoxDescriptionAvailable()">
												</span>
											</ww:if>
											</td>
										</ww:iterator>
										
									</tr>
								</ww:iterator>
								
								<tr>
									<td class="disabled"></td>
									<td colspan="3" align="right" class="btnContainer">
										<input type="button" value="<ds:lang text="Dodaj"/>" onclick="openAddDictionary_<ww:property value="cn"/>()" 
											class="btn" 
											<ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
										<input type="button" value="<ds:lang text="Usun"/>" onclick="deleteSelectedRows('asTable_<ww:property value="#tagId"/>');" 
											class="btn" 
											<ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
										<input type="button" value="<ds:lang text="wybierz"/>" onclick="openDictionaryRows_<ww:property value="cn"/>()" 
											class="btn" 
											<ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
									</td>
								</tr>
								</tbody>
								</table>
							<ww:if test="fm.getShemeVisible(cn)"><script type="text/javascript">
								var index_<ww:property value="#tagId"/> = 0;
								
								var $j = jQuery.noConflict();

								$j('.tab tbody tr:not(:last)').click(function() {
									if($j(this).hasClass('highlightedRows'))
										$j(this).removeClass('highlightedRows').children().css('font-weight', 'normal');
									else {
										//$j('.tab tbody tr').removeClass('highlightedRows'); jesli odkomentujemy to mozna zaznaczac tylko jeden wiersz
										$j(this).addClass('highlightedRows').children().css('font-weight', 'bold');
									}
								}).hover(function() {
									$j(this).addClass('tabHover');
								}, function() {
									$j(this).removeClass('tabHover');
								});
							
								function openAddDictionary_<ww:property value="cn"/>()
								{
									openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>',
												   '<ww:property value="cn"/>', 700, 650);
								}
								function openDictionary_<ww:property value="cn"/>()
								{
									var selObj = document.getElementById('<ww:property value="#tagId"/>');
									var selectedIndex = selObj.selectedIndex;
									if(selectedIndex == null)
									{
										return;
									}
									var idSel = selObj.options[selectedIndex].value;
									openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+'&id='+idSel,
												   '<ww:property value="cn"/>', 700, 650);
								}

								function openDictionaryRows_<ww:property value="cn"/>() {
									var selObj = $j('#asTable_<ww:property value="#tagId"/> tbody tr.highlightedRows:first :input');
									var idSel = selObj.attr('value');
									if(!idSel) 
										return false;
									//alert(idSel);
									openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+'&id='+idSel,
											   '<ww:property value="cn"/>', 700, 650);
								}
								
								function getId_<ww:property value="cn"/>()
								{
									return document.getElementById('dockind_<ww:property value="cn"/>').value;
								}

								function accept_update_<ww:property value="cn"/>(map)
								{
									traceStart('acceptUpdate' + map);
									
									var tabelka = $j('#asTable_<ww:property value="#tagId"/> tbody .row_<ww:property value="#tagId"/> td');

									<ww:iterator value="dictionaryAttributes" status="stat">
										tabelka.get(<ww:property value="#stat.index"/>).innerHTML = map.<ww:property value="key" />;
									</ww:iterator>

									traceEnd();		  
								}
			
								function accept_<ww:property value="cn"/>(map)
								{
									traceStart('accept');

									/* szukamy pod�wietlonego wiersza - bierzemy pierwszy */
									var jqSelRow = $j('#asTable_<ww:property value="#tagId"/> tbody tr.highlightedRows:first');

									if(jqSelRow.length) {
										var i = 1; // bo pierwsza kom�rka pusta
										//alert('znalazlem: ' + jqSelRow.children('td').children('input').attr('id') + ' = ' + jqSelRow.children('td').children('input').val());

										/* sprawdzamy czy zgadzaj� nam si� id wpis�w - je�li nie to dodajemy nowy wpis, czyli funkcja biegnie dalej */
										var tableRowId = jqSelRow.children().children('input:hidden:first').val();
										if(tableRowId == map.id) {
											<ww:iterator value="dictionaryAttributes">
												jqSelRow.children('td')[i].innerHTML = map.<ww:property value="key"/>;
												//alert('zmienilem map.<ww:property value="key"/> na ' + map.<ww:property value="key"/>);
	
												i ++;
											</ww:iterator>
	
											//alert('koniec zmian');
											traceEnd();
											return;
										}
									}
									
									<ww:property value="dictionaryAccept"/>;
									/*var sel = document.getElementById('<ww:property value="#tagId"/>');
									alert('accept' + sel + map.id + map.dictionaryDescription);
									addOption(sel, map.id, map.dictionaryDescription);*/
									var idIndex = ('#asTable_<ww:property value="#tagId"/> tbody tr').length + 1;
									$j('#asTable_<ww:property value="#tagId"/> tbody').append('<tr><td class="disabled"><input type="hidden" name="<ww:property value="#tagName"/>" id=<ww:property value="#tagId"/>_' + idIndex + ' value="'+map.id+'"/></td></tr>');
									
									//alert('<tr><input type="hidden" name="<ww:property value="#tagName"/>" id=<ww:property value="#tagId"/>_' + idIndex + ' value="'+map.id+'"/></tr>');
									<ww:iterator value="dictionaryAttributes">
										//<ww:property value="key"/>
										//map.<ww:property value="key"/>
										$j('#asTable_<ww:property value="#tagId"/> tbody tr:last').append('<td>' + map.<ww:property value="key"/> + '</td>');
									</ww:iterator>
									$j('#asTable_<ww:property value="#tagId"/> tr.oddRows').removeClass('oddRows');
									$j('#asTable_<ww:property value="#tagId"/> tr:odd').addClass('oddRows');	

									traceEnd();				  
								}

								function deleteSelectedRows(tableId) {
									$j('#' + tableId + ' tr.highlightedRows').remove();
									$j('#' + tableId + ' tr.oddRows').removeClass('oddRows');
									$j('#' + tableId + ' tr:odd').addClass('oddRows');

									/* je�li skasowali�my wszystkie, to musimy doda� pustego hiddena (do pierwszej kom�rki nag��wka) */
									if($j('#' + tableId + ' tr').length == 2) {
										//alert('dodajemy puste');
										$j('#' + tableId + ' tr th:first').append('<input type="hidden" name="<ww:property value="#tagName"/>" id=<ww:property value="#tagId"/>_0 value=""/>');
									}
								}
			
								function onsubmit_<ww:property value="cn"/>()
								{
									selectAllOptions(document.getElementById('<ww:property value="#tagId"/>'));
								}
			
								functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
								functionsUpdate['<ww:property value="cn"/>'] = accept_update_<ww:property value="cn"/>;
								onsubmitFunctions['<ww:property value="cn"/>'] = onsubmit_<ww:property value="cn"/>;
							</script></ww:if>
						</ww:if>
						<ww:else>
							<table class="tableMargin">
								<tr>
									<td>
										<select class="sel"  <ww:property value="fm.getStyle(cn,canEdit)"  escape="false"/>
											id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" 
											multiple="multiple" size="3" style="width:400px" <ww:if test="!canEdit">disabled="disabled"</ww:if>>
											<ww:iterator value="fm.getValue(cn)">
												<option value="<ww:property value="id"/>">
													<ww:property value="dictionaryDescription"/>
												</option>
											</ww:iterator>
										</select>
									</td>
								</tr>		
								<tr>
									<td colspan="2" align="right" class="btnContainer">
										<input type="button" value="<ds:lang text="Dodaj"/>" onclick="openAddDictionary_<ww:property value="cn"/>()" 
											class="btn" <ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
										<input type="button" value="<ds:lang text="Usun"/>" onclick="deleteSelectedOptions(document.getElementById('<ww:property value="#tagId"/>'));" 
											class="btn" <ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
										<input type="button" value="<ds:lang text="wybierz"/>" onclick="openDictionary_<ww:property value="cn"/>()" 
											class="btn" <ww:if test="(!canEdit) || ((!canReadDictionaries) && (dockindAction != 'create'))">disabled="disabled"</ww:if>/>
									</td>
								</tr>
							</table>	
							<ww:if test="fm.getShemeVisible(cn)"><script type="text/javascript">
								function openAddDictionary_<ww:property value="cn"/>()
								{
									openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>',
												   '<ww:property value="cn"/>', 700, 650);
								}
								function openDictionary_<ww:property value="cn"/>()
								{
									var selObj = document.getElementById('<ww:property value="#tagId"/>');
									var selectedIndex = selObj.selectedIndex;
									if(selectedIndex == null)
									{
										return;
									}
									var idSel = selObj.options[selectedIndex].value;
									openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+'&id='+idSel,
												   '<ww:property value="cn"/>', 700, 650);
								}
								
								function getId_<ww:property value="cn"/>()
								{
									return document.getElementById('dockind_<ww:property value="cn"/>').value;
								}
			
								function accept_<ww:property value="cn"/>(map)
								{
									<ww:property value="dictionaryAccept"/>;
									var sel = document.getElementById('<ww:property value="#tagId"/>');
									addOption(sel, map.id, map.dictionaryDescription);						  
								}
			
								function onsubmit_<ww:property value="cn"/>()
								{
									selectAllOptions(document.getElementById('<ww:property value="#tagId"/>'));
								}
			
								functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
								//functionsUpdate['<ww:property value="cn"/>'] = accept_update_<ww:property value="cn"/>;
								onsubmitFunctions['<ww:property value="cn"/>'] = onsubmit_<ww:property value="cn"/>;
							</script></ww:if>
						</ww:else>	
					</ww:elseif>
					<ww:elseif test="type == 'enum'">
						<!-- multi enum //-->
						<select class="multi_sel dontFixWidth" size="10" multiple="multiple" id="<ww:property value="#tagId"/>"  name="<ww:property value="#tagName"/>" 
								<ww:if test="fm.mainFieldCn == cn">onchange="<ww:property value="'onchange_main_field(this)'"/>"</ww:if>
								<ww:else>onchange="<ww:property value="onchange"/>"</ww:else>
								<ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if>>
						
						<ww:if test="fm.mainFieldCn != cn && !required">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
						</ww:if>						
						<ww:set name="fieldCn" value="cn"/>
						<ww:iterator value="getEnumItemsByAspect(documentAspectCn,fm)">
							<option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn).contains(id)">selected="selected"</ww:if>>
								<ww:property value="title"/>
							</option>
						</ww:iterator>						
						</select>
						<script type="text/javascript">
							fixMultiItemWidth(document.getElementById('<ww:property value="#tagId"/>'));
						</script>
					</ww:elseif>
					<ww:elseif test="type == 'dsuser'">
						<!-- multi enum -->
						<select class="multi_sel dontFixWidth" size="10" multiple="multiple" 
                                                        id="<ww:property value="#tagId"/>" <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
                                                        name="<ww:property value="#tagName"/>" 
                                                        onchange="<ww:property value="fm.mainFieldCn == cn ? 'onchange_main_field(this)' : ''"/>">
						
							<ww:if test="fm.mainFieldCn != cn && !required">
								<option value="">
									<ds:lang text="select.wybierz"/>
								</option>
							</ww:if>						
							<ww:set name="fieldCn" value="cn"/>
							<ww:iterator value="enumItems">
						    	<ww:if test="available || fm.getKey(#fieldCn).contains(id)">
                                	<option value='<ww:property value="id"/>' <ww:if test="fm.getKey(#fieldCn).contains(id)">selected="selected"</ww:if>>
                                    	<ww:property value="title"/>
                                	</option>
								</ww:if>
							</ww:iterator>						
						</select>
						<script type="text/javascript">
							fixMultiItemWidth(document.getElementById('<ww:property value="#tagId"/>'));
						</script>
					</ww:elseif>
					<ww:elseif test="type == 'dataBase'">
						<!-- multi enum -->
						<select class="multi_sel dontFixWidth" size="10" multiple="multiple" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" onchange="<ww:property value="fm.mainFieldCn == cn ? 'onchange_main_field(this)' : ''"/>">
						
						<ww:if test="fm.mainFieldCn != cn && !required">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
						</ww:if>						
						<ww:set name="fieldCn" value="cn"/>
						<ww:iterator value="enumItems">
						    <ww:if test="available || fm.getKey(#fieldCn).contains(id)">
                                <option value='<ww:property value="id"/>' <ww:if test="fm.getKey(#fieldCn).contains(id)">selected="selected"</ww:if>>
                                    <ds:field mode="default-formatting" object="[0].top"/>
                                </option>
							</ww:if>
						</ww:iterator>						
						</select>
						<script type="text/javascript">
							fixMultiItemWidth(document.getElementById('<ww:property value="#tagId"/>'));
						</script>
					</ww:elseif>	
					<ww:elseif test="type == 'list-value'">
					<!-- list-value -->
						<table class="tableMargin">
							<ww:if test="fm.getKey(cn) != null && fm.getKey(cn).size() > 0">
								<ww:iterator value="fm.getKey(cn)" status="status">
									<ww:set name="result"/>
									<tr id="<ww:property value="'attTr_'+cn+#status.index"/>">
										<td>
											<select name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
												class="sel" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if>>
												<ww:if test="fm.mainFieldCn != cn && !required">
													<option value="">
														<ds:lang text="select.wybierz"/>
													</option>
												</ww:if>						
												<ww:set name="fieldCn" value="cn"/>
												<ww:iterator value="enumItems">
						    						<ww:if test="available">
                                						<option value='<ww:property value="id"/>' <ww:if test="id.toString().equals(#result.toString())">selected="selected"</ww:if>>
                                   							<ww:property value="title"/>
                                						</option>
													</ww:if>
												</ww:iterator>	
											
											<%-- 
											<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
												value="<ww:property value="#result"/>" size="21" maxlength="<ww:property value="length"/>" class="txt"
												<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
											--%>
						 				</td>
										<td>
											<ww:if test="#status.index > 0">
												<input type="button" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if> value=<ds:lang text="Usun"/> class="btn" onclick="removeFieldValue_<ww:property value="cn+'('+#status.index+')'"/>"/>
											</ww:if>
										</td>
									</tr>
								</ww:iterator>
							</ww:if>
							<ww:else>
								<tr id="<ww:property value="'attTr_'+cn+'1'"/>">
										<td>
											<select name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
												class="sel" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if>>
												<ww:if test="fm.mainFieldCn != cn && !required">
													<option value="">
														<ds:lang text="select.wybierz"/>
													</option>
												</ww:if>						
												<ww:set name="fieldCn" value="cn"/>
												<ww:iterator value="enumItems">
						    						<ww:if test="available">
                                						<option value='<ww:property value="id"/>'>
                                   							<ww:property value="title"/>
                                						</option>
													</ww:if>
												</ww:iterator>		
											</select>
										</td>
										<td>
										</td>
									</tr>
							</ww:else>
							<tr id="<ww:property value="'attCreateTr_'+cn"/>">						
								<td colspan="2">
									<input type="button" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if> value=<ds:lang text="dodaj"/> class="btn" onclick="addFieldValue_<ww:property value="cn"/>()"/>
								</td>
							</tr>
						</table>
						<script type="text/javascript">
							var fieldsSize_<ww:property value="cn"/> = <ww:property value="fm.getKey(cn) != null ? fm.getKey(cn).size() : 1"/>;
							
							function addFieldValue_<ww:property value="cn"/>()
							{				
								var option;
						
								fieldsSize_<ww:property value="cn"/>++;
						
								//alert('bylo '+attachmentsCount);
								var attCreateTr = document.getElementById('attCreateTr_<ww:property value="cn"/>');				
								var tr = document.createElement('tr');				
								tr.setAttribute('id', '<ww:property value="'attTr_'+cn"/>'+fieldsSize_<ww:property value="cn"/>);				
								// tworzymy select z nazw�
								var input = document.createElement('select');
								input.setAttribute('name', '<ww:property value="#tagName"/>');				
								input.setAttribute('class', 'txt');  
								input.className = 'sel';			 
								input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>);	

								//dodajemy opcje z poprzedniego selekta		   
								$j(input).html($j('#<ww:property value="#tagId"/>0').html()).css('width', $j('#<ww:property value="#tagId"/>0').width() + 'px');
								
								tr.appendChild(createCell(input,true));				 
						
								// tworzymy przycisk "usu�"				
								input = document.createElement('input');				
								input.setAttribute('type', 'button');				
								input.setAttribute('value', '<ds:lang text="Usun"/>');				
								input.setAttribute('class', 'btn');				
								input.className = 'btn';
								//input.setAttribute('onclick', 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')');				
								// trzeba robic obejscie jak ponizej bo wpp nie dziela pod IE
								var func = 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')';
								input.onclick = function() { var temp = new Function(func); temp(); };
								tr.appendChild(createCell(input,false));																
								
								attCreateTr.parentNode.insertBefore(tr, attCreateTr);				
							}
						
							function removeFieldValue_<ww:property value="cn"/>(no)				
							{				
								var tr = document.getElementById('<ww:property value="'attTr_'+cn"/>'+no);   
								if (tr != null)			 
									tr.parentNode.removeChild(tr);																	  
							}																									   
						</script>
					</ww:elseif>		
					<ww:else>
						<!-- pozosta�e typy -->
						<%-- TODO: obecnie dzia�a tylko dla p�l typu integer,long,float i string --%>
						<table class="tableMargin">
							<ww:if test="fm.getKey(cn) != null && fm.getKey(cn).size() > 0">
								<ww:iterator value="fm.getKey(cn)" status="status">
									<ww:set name="result"/>
									<tr id="<ww:property value="'attTr_'+cn+#status.index"/>">
										<td>
											<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
												value="<ww:property value="#result"/>" size="21" maxlength="<ww:property value="length"/>" class="txt"
												<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
						 				</td>
										<td>
											<input type="button" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if> value=<ds:lang text="Usun"/> class="btn" onclick="removeFieldValue_<ww:property value="cn+'('+#status.index+')'"/>"/>
										</td>
									</tr>
								</ww:iterator>
							</ww:if>
							<ww:else>
								<tr id="<ww:property value="'attTr_'+cn+'1'"/>">
										<td>
											<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>0"
												size="21" maxlength="<ww:property value="length"/>" class="txt"
												<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
										</td>
										<td>
											<input type="button" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if> value=<ds:lang text="Usun"/> class="btn" onclick="removeFieldValue_<ww:property value="cn+'('+#status.index+')'"/>"/>
										</td>
									</tr>
							</ww:else>
							<tr id="<ww:property value="'attCreateTr_'+cn"/>">						
								<td colspan="2">
									<input type="button" <ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if> value=<ds:lang text="dodaj"/> class="btn" onclick="addFieldValue_<ww:property value="cn"/>()"/>
								</td>
							</tr>
						</table>
						
						<script type="text/javascript">
							var fieldsSize_<ww:property value="cn"/> = <ww:property value="fm.getKey(cn) != null ? fm.getKey(cn).size() : 1"/>;
							
							function addFieldValue_<ww:property value="cn"/>()
							{				
								var option;
						
								fieldsSize_<ww:property value="cn"/>++;
						
								//alert('bylo '+attachmentsCount);
								var attCreateTr = document.getElementById('attCreateTr_<ww:property value="cn"/>');				
								var tr = document.createElement('tr');				
								tr.setAttribute('id', '<ww:property value="'attTr_'+cn"/>'+fieldsSize_<ww:property value="cn"/>);				
								// tworzymy input z nazw�
								var input = document.createElement('input');
								input.setAttribute('type', 'text');		  
								input.setAttribute('size', 21);			
								input.setAttribute('name', '<ww:property value="#tagName"/>');				
								input.setAttribute('class', 'txt');  
								input.className = 'txt';			 
								input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>);			   
								tr.appendChild(createCell(input,true));				 
						
								// tworzymy przycisk "usu�"				
								input = document.createElement('input');				
								input.setAttribute('type', 'button');				
								input.setAttribute('value', '<ds:lang text="Usun"/>');				
								input.setAttribute('class', 'btn');				
								input.className = 'btn';
								//input.setAttribute('onclick', 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')');				
								// trzeba robic obejscie jak ponizej bo wpp nie dziela pod IE
								var func = 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')';
								input.onclick = function() { var temp = new Function(func); temp(); };
								tr.appendChild(createCell(input,false));																
								
								attCreateTr.parentNode.insertBefore(tr, attCreateTr);				
							}
						
							function removeFieldValue_<ww:property value="cn"/>(no)				
							{				
								var tr = document.getElementById('<ww:property value="'attTr_'+cn"/>'+no);   
								if (tr != null)			 
									tr.parentNode.removeChild(tr);																	  
							}																									   
						</script>  
					</ww:else>	
				</ww:elseif>
				<!-- pola jednowarto�ciowe -->
				<ww:elseif test="type == 'doclist'">
				<!-- doclist -->
						<ww:if test="!isNewDocumentAction()">
							<jsp:include page="/common/doclist.jsp"/>	
						</ww:if>				
				</ww:elseif>
				<ww:elseif test="type == 'document'">
				<!-- document -->
						<ww:if test="!isNewDocumentAction()">
							<b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="fm.getKey(cn)"/></ww:url>">
								<ww:property value="fm.getValue(cn)"/></a></b>
						</ww:if>				
				</ww:elseif>
				<ww:elseif test="type == 'drools-button'">
				<!-- drools-button -->
				    <ww:if test="!newDocumentAction">
				        <input type="submit" class="btn" id="<ww:property value="#tagId"/>" value="<ww:property value="name"/>"
                                onclick="onclick_<ww:property value="cn"/>Btn();"/>
                            <script type="text/javascript">
                            function onclick_<ww:property value="cn"/>Btn() {
                                document.getElementById('doExecuteDroolsRule').value = true;
                                document.getElementById('droolsRuleCn').value = '<ww:property value="cn"/>';
                            }
                            </script>
				    </ww:if>
                </ww:elseif>
				<ww:elseif test="type == 'dockindButton'">
				<ww:set name="place" value="getPlace()"/>
				<!-- dockindButton -->
					<ww:if test="!isNewDocumentAction() && isButtonAvailable(#place)">
					
							<ww:if test="getActions() != null && getActions().size() > 1">
							<%--<td colspan="3">--%>
								<table>
									<tr>
										<td>							
										<input type="button" class="btn" id="<ww:property value="#tagId"/>" value="<ww:property value="name"/>"
											onclick="onclick_<ww:property value="cn"/>Btn();"/>
										<select class="sel"	name="''" id="<ww:property value="#tagId"/>_sel">
											<option value="">
												<ds:lang text="select.wybierz"/>
											</option>
											<ww:iterator value="getActions()">
													<option value="<ww:property value="key"/>">
														<ww:property value="value"/> 
													</option>
											</ww:iterator>
										</select>
										<script type="text/javascript">
										function onclick_<ww:property value="cn"/>Btn() 
										{
											<ww:property value="fm.getOnclick(cn)"/>
											document.getElementById('doDockindEvent').value = true;
											var selectObj = document.getElementById('<ww:property value="#tagId"/>_sel');
											document.getElementById('dockindEventValue').value = selectObj.options[selectObj.options.selectedIndex].value;
											document.forms[0].submit();
										}
										</script>
										</td>
									</tr>
								</table>
							<%--</td>--%>
							</ww:if>
							<ww:else>
								<%--<td colspan="3">--%>
								<table class="formTableDisabled">
										<tr>
											<td>							
											<input type="button" class="btn" id="<ww:property value="#tagId"/>" value="<ww:property value="name"/>"
												onclick="onclick_<ww:property value="cn"/>Btn();"/>
											<script type="text/javascript">
											function onclick_<ww:property value="cn"/>Btn() 
											{
												<ww:property value="fm.getOnclick(cn)"/>
												document.getElementById('doDockindEvent').value = true;
												document.getElementById('dockindEventValue').value = '<ww:property value="cn"/>';
                                                                                                document.forms[0].submit();
											}
											</script>
										</td>
									</tr>
								</table>	
								<%--</td>--%>
							</ww:else>
					</ww:if>	
				</ww:elseif>
				<ww:elseif test="type == 'string'">
					<!-- string -->
				
					<%--<ww:textfield name="#tagName" id="#tagId" value="fm.getKey(cn)" cssClass="'txt'" size="21" maxlength="length"/>--%>
					<ww:if test="(fm.getShowAs(cn).equalsIgnoreCase('plaintext') && fm.getDisabled(cn))">
						<ww:property value="fm.getKey(cn)"/>
					</ww:if>
					<ww:elseif test="length <= 80">
						<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" 
							value="<ww:property value="fm.getKey(cn)"/>" size="<ww:property value="size != null ? size : 26"/>" maxlength="<ww:property value="length"/>" class="txt"
							<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/> />
					</ww:elseif>
					<ww:elseif test="length <= 600">
						<textarea name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" rows="3" cols="<ww:property value="size != null ? size : 21"/>" class="txt"<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>><ww:property value="fm.getKey(cn)"/></textarea>
					</ww:elseif>
					<ww:else>
						<textarea name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" rows="6" cols="<ww:property value="size != null ? size : 35"/>" class="txt"<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>><ww:property value="fm.getKey(cn)"/></textarea>
					</ww:else>
				
				</ww:elseif>

				<ww:elseif test="type == 'float'">
				<!-- float -->				
					<input type="text" 
						name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
						value="<ww:property value="fm.getKey(cn)"/>" size="21" maxlength="21" class="txt"
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
				</ww:elseif>

				<ww:elseif test="type == 'integer' || type == 'long' || type == 'double'">
				<!-- integer, etc -->	
					<input type="text" 
						name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
						value="<ww:property value="fm.getKey(cn)"/>" size="10" maxlength="10" class="txt"
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
				</ww:elseif>
				<ww:elseif test="type == 'money'">
				<!-- money -->
					<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" 
						value="<ww:property value="fm.getValue(cn)"/>" size="20" maxlength="20" class="txt"
							<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
                                </ww:elseif>
				<ww:elseif test="type == 'dsuser'">
					 <select class="sel" id="<ww:property value="#tagId"/>" <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/> 
                                                 name="<ww:property value="#tagName"/>">
                                                 <ww:if test="(fm.mainFieldCn != cn && !required) || (fm.documentKind.properties['add-empty-to-user-field'] == 'true')">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
						</ww:if>						
						<ww:set name="fieldCn" value="cn"/>
						<ww:iterator value="enumItems">
						    <ww:if test="available || fm.getKey(#fieldCn) == id">
                                                         <option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn) == id">selected="selected"</ww:if>>
                                                                <ww:property value="title"/>
                                                        </option>
							</ww:if>
						</ww:iterator>								
                        </select>
				</ww:elseif>
				<ww:elseif test="type == 'dataBase'"><!-- dataBase -->
					<ww:set name="refFieldCn" value="refField"/>
					<select class="sel"
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
						<ww:if test="canEdit">id="<ww:property value="#tagId"/>"</ww:if>
						name="<ww:property value="#tagName"/>" onchange="onchange_main_field(this);">

						<%-- <ww:if test="fm.mainFieldCn != cn"><option value="''"><ds:lang text="select.wybierz"/></option></ww:if> --%>
						<ww:set name="fieldCn" value="cn"/>
						<ww:if test="fm.mainFieldCn != cn">
							<option value="">
								<ds:lang text="select.wybierz"/>
							</option>
						</ww:if> 
						<ww:if test="!fm.getEnumItem(#fieldCn).available"> <%-- Je�li nie jest dost�pny to nie pojawi si� w kolekcji poni�ej, wi�c trzeba go doda� ju� teraz --%>
							<option selected="selected" value='<ww:property value="fm.getEnumItem(#fieldCn).id"/>'> <ds:field mode="default-formatting" object="fm.getEnumItem(#fieldCn)"/></option>
						</ww:if>
						<ww:iterator value="availableItems">
							<option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn) == id">selected="selected"</ww:if>><ds:field mode="default-formatting" object="[0].top"/></option>
						</ww:iterator>
					</select>
					
					<%-- gdy brak uprawnien do edycji (pole jest "disabled") --%>
					<ww:if test="!canEdit">
						<input type="hidden" name="<ww:property value="#tagName"/>"
							id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
					</ww:if>
					
					<script type="text/javascript">
					 	function <ww:property value="'update_database'+#tagId"/>()
					 	{
					 		<ww:if test="refField != null">
					 			var selObj = document.getElementById('<ww:property value="#tagId"/>');
					 			var refValue = document.getElementById('<ww:property value="'dockind_'+refField"/>').value;
					 			//alert(refValue);
					 			var a = 0;
			   					var b = selObj.options.length;
			   					for(a=0; a < b; a++)
			   					{
			   						selObj.remove(selObj.options.length-1);			   						
			   					}
			   					var elOptNew = document.createElement('option');
								elOptNew.text = '<ds:lang text="select.wybierz"/>';
								elOptNew.value = '';

								try
								 {
									selObj.add(elOptNew, null);
								 }
								 catch(ex)
								 {
									selObj.add(elOptNew); // IE only
								 }

								<ww:if test="!fm.getEnumItem(#fieldCn).available">
								    <%-- Je�li nie jest dost�pny to nie pojawi si� w kolekcji poni�ej (availableItems)--%>
                                                                        var elOptNew = document.createElement('option');

									var s = '<ds:field mode="default-formatting" object="fm.getEnumItem(#fieldCn)"/>';
									elOptNew.text = s.replace(/&#xF3;/g,'�');
									elOptNew.value = <ww:property value="fm.getEnumItem(#fieldCn).id"/>;
			   						elOptNew.selected = true;
			   						if(<ww:property value="fm.getEnumItem(#fieldCn).refValue"/> == refValue){
				   						 try { selObj.add(elOptNew, null); }
										 catch(ex) {
											selObj.add(elOptNew); <%-- IE only --%>
										 }
					 				}
						        </ww:if>
								 
			   					<ww:iterator value="availableItems">
			   						var elOptNew = document.createElement('option');
									
									var s = '<ds:field mode="default-formatting" object="[0].top"/>';
									elOptNew.text = s.replace(/&#xF3;/g,'�');
									elOptNew.value = <ww:property value="id"/>;
									<ww:if test="fm.getKey(#fieldCn) == id">
			   							elOptNew.selected = true;
			   						</ww:if> 
			   						if(<ww:property value="refValue"/> == refValue){
				   						 try { selObj.add(elOptNew, null); }
										 catch(ex) {
											selObj.add(elOptNew); <%-- IE only --%>
										 }
					 				}
			   						
			   					</ww:iterator>
					 		</ww:if>
					 		<ww:if test="getAssoField() != null">
					 			<ww:property value="'update_databasedockind_'+getAssoField()"/>();
					 		</ww:if>
					 	}
					</script>
				</ww:elseif>
				
				<ww:elseif test="type == 'enum'"><!-- enum -->
					<ww:if test="refStyle == 'all-visible'">
						<input type="hidden" name="<ww:property value="#tagName"/>"
							id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"
							<ww:if test="fm.mainFieldCn == cn"> onchange="onchange_main_field(this);"</ww:if>/>
					</ww:if>
					
					<ww:elseif test="refStyle == 'chosen-visible'">					
						<select class="sel"
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
							<ww:if test="canEdit">id="<ww:property value="#tagId"/>"</ww:if> name="<ww:property value="#tagName"/>" 
							onchange="if (window.<ww:property value="'onchange_'+#tagId"/> ){<ww:property value="'onchange_'+#tagId+'(this);'"/>}  <ww:property value="(fm.mainFieldCn == cn ? 'onchange_main_field(this)' : '')"/>"
							<ww:if test="!canEdit">disabled="disabled"</ww:if>
							<ww:elseif test="getEnumItemsByAspect(documentAspectCn,fm).size() == 1">disabled="disabled" style="background-color:#ffffc4;"</ww:elseif>>

							<ww:if test="getEnumItemsByAspect(documentAspectCn,fm).size()>1">
								<ww:if test="fm.mainFieldCn != cn">
									<option value="">
										<ds:lang text="select.wybierz"/>
									</option>
								</ww:if>
								<ww:elseif test="specialBool != null">
									<option value=""><ds:lang text="select.wybierz"/></option>
								</ww:elseif>
							</ww:if>
							<ww:set name="fieldCn" value="cn"/>
							<ww:if test="!fm.getEnumItem(#fieldCn).available"> <%-- Jesli nie jest dost�pny to nie pojawi si� w kolekcji poni�ej --%>
								<option selected="selected" value='<ww:property value="fm.getEnumItem(#fieldCn).id"/>'> 
	                                <ww:property value="fm.getEnumItem(#fieldCn).title"/>
								</option>
							</ww:if>
							<ww:iterator value="getEnumItemsByAspect(documentAspectCn,fm)">
								<option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn) == id ">selected="selected"</ww:if>>
									<ww:property value="title"/>
								</option>
							</ww:iterator>
						</select>

						<%-- gdy brak uprawnien do edycji (pole jest "disabled") --%>
						<ww:if test="!canEdit">
							<input type="hidden" name="<ww:property value="#tagName"/>"
								id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
						</ww:if>
					</ww:elseif>
					
					<ww:else><!-- else -->
						<ww:set name="remarkField" value="(fm.documentKind.properties['remark-field'] == cn) && (!fm.getEnumItem(cn).forceRemark || (remark != null)) && (dockindAction == 'edit') && (document.stringType != 'plain')"/>
						<select class="sel" 
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
							<ww:if test="canEdit">id="<ww:property value="#tagId"/>"</ww:if> name="<ww:property value="#tagName"/>" 
							onchange="<ww:property value="#remarkField ? 'onchange_remark_field_'+#tagId+'(this);' : ''"/> <ww:property value="fm.mainFieldCn == cn ? 'onchange_main_field(this)' : ''"/>"
							<ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if>
							<ww:if test="getEnumItemsByAspect(documentAspectCn,fm).size() == 1">disabled="disabled" style="background-color:#ffffc4;" </ww:if>>
							<ww:if test="getEnumItemsByAspect(documentAspectCn,fm).size()>1">
								<ww:if test="fm.mainFieldCn != cn">
									<option value="">
										<ds:lang text="select.wybierz"/>
									</option>
								</ww:if>
								<ww:elseif test="specialBool != null">
										<option value=""><ds:lang text="select.wybierz"/></option>
								</ww:elseif>
							</ww:if>
							
							<ww:set name="fieldCn" value="cn"/>
							<ww:if test="!fm.getEnumItem(#fieldCn).available"> <%-- Jesli nie jest dost�pny to nie pojawi si� w kolekcji poni�ej --%>
								<option selected="selected" value='<ww:property value="fm.getEnumItem(#fieldCn).id"/>'> 
                                	<ww:property value="fm.getEnumItem(#fieldCn).title"/>
								</option>
							</ww:if>
							<ww:iterator value="getEnumItemsByAspect(documentAspectCn,fm)">
								<option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn) == id">selected="selected"</ww:if>>
									<ww:property value="title"/>
								</option>
							</ww:iterator>
							
						</select>
									
						<%-- gdy brak uprawnien do edycji (pole jest "disabled") --%>
						<ww:if test="!canEdit || fm.getDisabled(cn)">
							<input type="hidden" name="<ww:property value="#tagName"/>"
								id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
						</ww:if>
						
						<%-- zawsze na pocz�tku pole jest ukryte (chyba, �e remark != null co oznacza, �e 
							 wcze�niej kto� musia� poda� uwag�, ale akcja zako�czy�a si� b��dem, wi�c teraz te� j� wy�wietlamy --%>
						<ww:hidden id="checkRemark" name="'checkRemark'" value="(remark != null)"/>
						<ww:if test="#remarkField">
							
									</td>
								</tr>
									<tr id="remarkDiv" <ww:if test="remark == null">style="display:none"</ww:if>>>
										<td valign="center">
											Przyczyna odrzucenia:
											<span class="star">*</span>
										</td>											
										<td>
											<textarea name="remark" id="remark" cols="40" rows="3" class="txt"><ww:property value="remark"/></textarea>
										</td>
								</tr>
								<tr>
									<td>

						</ww:if>

						<script type="text/javascript">
							function onchange_remark_field_<ww:property value="#tagId"/>(field)
							{
								var checkRemark = document.getElementById('checkRemark').value;
								<ww:iterator value="enumItems">
									<ww:if test="forceRemark">
									if ((field.value == <ww:property value="id"/>) && (checkRemark == 'false'))
									{
										//alert('show');
										document.getElementById('checkRemark').value = true;
										document.getElementById('remarkDiv').style.display = '';
									}
									</ww:if>
									<ww:else>
									if ((field.value == <ww:property value="id"/>) && (checkRemark == 'true'))
									{
										//alert('hide');
										document.getElementById('checkRemark').value = 'false';
										document.getElementById('remarkDiv').style.display = 'none';
										//document.getElementById('remark').style.display = 'none';
									}
									</ww:else>
								</ww:iterator>
							}
						</script>
					</ww:else>
				</ww:elseif>
				
				<ww:elseif test="type == 'bool' || type == 'document-out-zpo' || type == 'document-out-additional-zpo'">
					<ww:if test="specialBool != null">
						<select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>">
                    		<option value=""><ds:lang text="select.wybierz"/></option>
                    		<option value="true"><ds:lang text="tak"/></option>
                    		<option value="false"><ds:lang text="nie"/></option>
                		</select>
					</ww:if>			  
					<ww:else>
						<input type="hidden" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
						<input id="bool_id_<ww:property value="cn"/>" type="checkbox" onchange="javascript:void(change_bool_<ww:property value="cn"/>(this))"
						        <ww:if test="fm.getKey(cn)">checked="true"</ww:if>
						        <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
						        <ww:if test="!canEdit">disabled="disabled"</ww:if>/>
						<script type="text/javascript">
							$j('#bool_id_<ww:property value="cn"/>').get(0).cn = '<ww:property value="cn"/>';
							function change_bool_<ww:property value="cn"/>(obj)
							{
								obj.cn = '<ww:property value="cn"/>';
								document.getElementById('<ww:property value="#tagId"/>').value = obj.checked;
								refreshVisibleFields(obj);
								<ww:property value="onchange"/>
							}
						</script>
					</ww:else>
				</ww:elseif>
				<ww:elseif test="type == 'date' || type == 'document-date'" ><!--  type == date -->
					<input type="text" size="10" maxlength="10" class="txt" <ww:if test="fm.getDisabled(cn)">disabled="disabled"</ww:if>
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
						name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" onchange="<ww:property value="onchange"/>"
						value="<ds:format-date value="fm.getKey(cn)" pattern="dd-MM-yyyy"/>" <ww:if test="!canEdit">readonly="readonly"</ww:if>/>
					<ww:if test="canEdit && !fm.getDisabled(cn)">		
						<img src="<ww:url value="'/calendar096/img.gif'"/>" id="datefield_trigger_<ww:property value="#tagId"/>"
							style="cursor: pointer; border: 1px solid red;<ww:if test="!canEdit">display:none;</ww:if>"
							title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
						
						<script type="text/javascript">
							Calendar.setup({
								inputField	 :	"<ww:property value="#tagId"/>",	 // id of the input field
								ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
								button		 :	"datefield_trigger_<ww:property value="#tagId"/>",  // trigger for the calendar (button ID)
								align		  :	"Tl",		   // alignment (defaults to "Bl")
								singleClick	:	true
							});				   
						</script>
					</ww:if>
				</ww:elseif>
				<ww:elseif test="type == 'enum-ref'"><!-- type = enum-ref -->
					<input type="hidden" name="<ww:property value="#tagName"/>"
						id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
	
					<ww:if test="refStyle == 'all-visible'">
						<table class="tableMargin leftAlign">
						<tbody class="leftAlignInner">
							<ww:iterator value="enumItemsList">
								<tr class="dontHide">
									<td valign="top">
										<ww:property value="fm.getField(fieldRefCn).getEnumItem(key).title"/>
									</td>
									<td>
										<select class="sel <ww:property value="#tagId+'_clear'"/> dontFixWidth dockindWide enum-ref"
											<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
											id="<ww:property value="#tagId+key"/>" onchange="update_<ww:property value="#tagId"/>(this)" <ww:if test="!canEdit">disabled="disabled"</ww:if>>
											<option value="">
												<ds:lang text="select.wybierz"/>
											</option>
											<ww:set name="fieldCn" value="cn"/>
											<ww:if test="!fm.getEnumItem(#fieldCn).available"> <%-- Jesli nie jest dost�pny to nie pojawi si� w kolekcji poni�ej --%>
												<option selected="selected" value='<ww:property value="fm.getEnumItem(#fieldCn).id"/>'> 
					                                <ds:field mode="default-formatting" object="fm.getEnumItem(#fieldCn)"/>
												</option>
											</ww:if>
											<ww:iterator value="value">
												<option value="<ww:property value="key"/>" <ww:if test="fm.getKey(cn) == key">selected="selected"</ww:if>>
													<ww:property value="value.title"/>
												</option>
											</ww:iterator>
										</select>
										<!-- Pe�en tekst selecta, je�li jest bardzo d�ugi -->
										<div id="<ww:property value="#tagId+key"/>_fullTxt" class="dockindWideTxt">
										</div>
										<script type="text/javascript">
											$j("#<ww:property value="#tagId+key"/>").get(0).cn = "<ww:property value="cn"/>";
											$j("#<ww:property value="#tagId+key"/>").change(function(){
												var me = this;
												if (typeof additional_update_<ww:property value="#tagId"/> == 'function') {
													additional_update_<ww:property value="#tagId"/>(this);
												}
												$j("#dockind_<ww:property value="fieldRefCn"/>").get(0).value = <ww:property value="key"/>;
												$j("#<ww:property value="#tagId"/>").get(0).value = this.options[this.selectedIndex].value;
												$j(".<ww:property value="#tagId+'_clear'"/>").each(function(){if(this != me)this.selectedIndex = 0;});
											});
										</script>
									</td>
								</tr>
							</ww:iterator>
						</tbody>
						</table>
					</ww:if>

					<ww:else>	<!-- refStyle == 'chosen-visible' -->
					<table cellspacing=0 cellpadding=0 border=0>
						<ww:iterator value="enumItemsList">
							<tr  id="<ww:property value="'div_'+#tagId+key"/>" class="if_<ww:property value="fieldRefCn+'_'+key"/> formTableDisabled" style="display:none">
							<td>
								<select class="sel dontFixWidth dockindWide"
									<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>
									id="<ww:property value="#tagId+key"/>" name="<ww:property value="#tagId+key"/>" onchange="update_<ww:property value="#tagId"/>(this);" <ww:if test="!canEdit">disabled="disabled"</ww:if>>
									<option value="">
										<ds:lang text="select.wybierz"/>
									</option>
									<ww:iterator value="value">
										<option value="<ww:property value="key"/>" <ww:if test="fm.getKey(cn) == key">selected="selected"</ww:if>>
											<ww:property value="value.title"/>
										</option>
									</ww:iterator>
								</select>
								
								<!-- Pe�en tekst selecta, je�li jest bardzo d�ugi -->
								<div id="<ww:property value="#tagId+key"/>_fullTxt" class="dockindWideTxt">
								</div>
							
							<script type="text/javascript">
								$j("#<ww:property value="#tagId+key"/>").get(0).cn = "<ww:property value="cn"/>"
								$j("#<ww:property value="#tagId+key"/>").change(function(){
									$j("#<ww:property value="#tagId"/>").get(0).value = this.options[this.selectedIndex].value;
									if (typeof additional_update_<ww:property value="#tagId"/> == 'function') {
										additional_update_<ww:property value="#tagId"/>(this);
									}
								});
							</script>
							</td>
							</tr>
						</ww:iterator>
					</table>
					
					</ww:else>
	
					<script type="text/javascript">
						function update_<ww:property value="#tagId"/>(select){}
						function onchange_<ww:property value="#prefix+fieldRefCn"/>(select){}
						function init_<ww:property value="#tagId"/>(){}
					</script>
				</ww:elseif>
				<ww:elseif test="type == 'class'"><!--  type = class nie multi -->
					<ww:set name="boolFalse" value="false" />
					<input type="hidden" name="<ww:property value="#tagName"/>"
						id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>	
					<table class="tableMargin leftAlign">
					<tbody class="leftAlignInner">
						<ww:iterator value="dictionaryAttributes"><!-- dictionaryAttributes -->
							<tr class="dontHide">
								<td><ww:property value="value"/></td>
								<td>
									<ww:if test="isAjaxAvailable()">
									<script type="text/javascript">
										$j(document).ready(function() {
											$j('#<ww:property value="#tagId+key"/>').attr('autocomplete', 'off').keyup(function(event) {
												//trace('#<ww:property value="#tagId+key"/>  <ww:property value="#tagName"/>');
												showMatchingData_<ww:property value="#tagId" />(event, $j(this));
												prefix = '<ww:property value="#prefix"/>';
	
												return true;
											}).keypress(function(event) {
												prefix = '<ww:property value="#tagId" />';
												if((event.keyCode == KEY_ENTER) && ($j('#promptDiv').css('visibility') == 'visible')) {
													putItem(selectedItem);

													$j('#form').bind('submit', function() {
														return false;
													});
													//dontSendForm = true;
													return false;
												}

												return true;
											});
										}); 
									</script>
									</ww:if>
									<ww:if test="isBoxDescriptionAvailable()">
										<span id="'boxA_'<ww:property value="#tagId"/>" title="offsetx=[15] offsety=[15] delay=[500] header=[Opis] body=[<ww:property value="fm.getValue(cn).getBoxDescription()" escape="false"/>]" style="vertical-align:middle;font-family:arial;font-size:20px;font-weight:bold;HEIGHT:300px;color:#ABABAB;cursor:pointer">
									</ww:if>
									<input type="text" id="<ww:property value="#tagId+key"/>" readonly="readonly" value="<ww:property value="fm.getValue(cn)[key]"/>" size="35" class="txt" />
									<ww:if test="isBoxDescriptionAvailable()">
										</span>
									</ww:if>
								</td>
							</tr>
						</ww:iterator>
						<tr>
							<td colspan="2" class="alignLeftBtns btnContainer" align="right">
								<input type="button" name="dictButton_<ww:property value="cn"/>" value=<ds:lang text="Wybierz"/> onclick="openDictionary_<ww:property value="cn"/>()" class="btn" <ww:if test="(!canEdit) || fm.getDisabled(cn) || ((!canReadDictionaries) && (dockindAction != 'create'))">style="display:none"</ww:if>/>
								<input type="button" value=<ds:lang text="wyczysc"/> onclick="clear_<ww:property value="cn"/>()" class="btn" <ww:if test="(!canEdit) || fm.getDisabled(cn) || ((!canReadDictionaries) && (dockindAction != 'create'))">style="display:none"</ww:if>/>
							</td>
						</tr>
					</tbody>
					</table>	
					<script type="text/javascript">
							function showMatchingData_<ww:property value="#tagId" />(e, caller)
							{
								var ln = caller;
								var pd = $j('#promptDiv');
								var str = '';
								var qstr = ''; 
								pressedKey = e.keyCode;
								<ww:iterator value="dictionaryAttributes">qstr += $j('#<ww:property value="#tagId+key"/>').val() + ',';</ww:iterator>
								<ww:iterator value="additionalSearchParam()">
							 		<ww:set name="resultadds"/>
							 		try { qstr += <ww:property value="#resultadds"/> + ',';} catch(e) {} 
					 			</ww:iterator>
					 			qstr = qstr.slice(0, qstr.length-1);
								
								traceStart('showMatchingData_<ww:property value="#tagId" />');
								trace('<ww:property value="#tagId" /> className: <ww:property value="#dictionaryClass"/>');

								if((ln.attr('value').trim().length > 2) && ln.attr('value').trim() != "" && pressedKey != KEY_ESCAPE) {
									if(pressedKey != KEY_DOWN && pressedKey != KEY_UP) {
										pd.get(0).innerHTML = "<img src= \"<c:out value='${pageContext.request.contextPath}'/>/img/ajax_loader.gif\" />";
										pd.css('visibility', 'visible');
									} else if((pressedKey == KEY_DOWN || pressedKey == KEY_UP || pressedKey == KEY_ENTER) && pd.css('visibility') == 'visible') {
										trace('Only navigation (without sending ajax request)');
										displayPrompt(ajaxResult, ln.attr('id'), '');

										traceEnd();
										return;
									}


									$j.post("<ww:url value="'/office/tasklist/document-kind-dictionary.action'"/>",
										 {
										 	params: qstr,
										 	className: '<ww:property value="#dictionaryClass"/>'
										 	
										 }, function(data){
											trace('Ajax query success');	
											trace('DATA: ' + data);											
											$j("#ajaxData").html(data);
											ajaxResult = eval(data);
											trace('DATA evaled: ' + ajaxResult);

											if(data) {
												trace('Calling displayPrompt...');
												displayPrompt(ajaxResult, ln.attr('id'), '');
											} else {
												trace('Nothing to display');
												pd.css('visibility', 'hidden');
											}
										});
								} else {
									pd.css('visibility', 'hidden');
								}
						
								trace(qstr);
								traceEnd();
							}
		
				   
						function openDictionary_<ww:property value="cn"/>()
						{
							openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+
										   '&id='+document.getElementById('<ww:property value="#tagId"/>').value,'<ww:property value="cn"/>',
										   <ww:if test="width > 0"><ww:property value="width"/>,<ww:property value="height"/></ww:if>
										   <ww:else>700, 650</ww:else>);
						}						
						function getId_<ww:property value="cn"/>()
						{
							return document.getElementById('dockind_<ww:property value="cn"/>').value;
						}
						<ww:iterator value="dictionaryAttributes">
							function get<ww:property value="#tagId+key"/>()
							{
								return document.getElementById('<ww:property value="#tagId+key"/>').value;
							}
						</ww:iterator>	
						function clear_<ww:property value="cn"/>()
						{
							document.getElementById('<ww:property value="#tagId"/>').value = '';
							<ww:iterator value="dictionaryAttributes">
								document.getElementById('<ww:property value="#tagId+key"/>').value = '';
							</ww:iterator>
						}	
						function accept_<ww:property value="cn"/>(map)
						{
						<ww:property value="dictionaryAccept"/>
							document.getElementById('<ww:property value="#tagId"/>').value = map.id;
							<ww:iterator value="dictionaryAttributes">
								document.getElementById('<ww:property value="#tagId+key"/>').value = map.<ww:property value="key"/>;
							</ww:iterator>
						}	
						functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
					</script>
				</ww:elseif>
				<ww:elseif test="type == 'list-value'">
					<ww:set name="result" value="fm.getKey(cn)"/>
					<select name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
							class="sel" <ww:property value="fm.getStyle(cn,canEdit)" /> >
							<ww:if test="fm.mainFieldCn != cn && !required">
								<option value="">
									<ds:lang text="select.wybierz"/>
								</option>
							</ww:if>						
							<ww:iterator value="enumItems">
	    						<ww:if test="available">
               						<option value='<ww:property value="id"/>' <ww:if test="id.toString().equals(#result.toString())">selected="selected"</ww:if>>
                    					<ww:property value="title"/>
                                	</option>
								</ww:if>
							</ww:iterator>	
					</select>
				</ww:elseif>
				<ww:else>
					<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" 
						value="<ww:property value="fm.getKey(cn)"/>" class="txt" <ww:if test="fm.getDisabled(cn)">disabled="disabled"</ww:if>
						<ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
				</ww:else>
			</td>
			<ww:if test="getInfoBox() != null">
				<td><ds:infoBox body="infoBox" header="infoBox" dockind="true"/></td>
			</ww:if>
		</tr>
	</ww:elseif>
	<ww:else>
		<input type="hidden" name="<ww:property value="#tagName"/>"
			id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
	</ww:else>
</ww:iterator>
</tbody>

	<tr class="formTableDisabled">
		<td>
			 <ww:if test="fm.getDocumentKind().properties['process-parameter-action'] != null">
			 	<ds:available test="!pokazProcesIco">
		    	<input type="button" value="<ds:lang text="PokazProces"/>" class="btn"
		    	onclick="openToolWindow('<ww:url value="fm.getDocumentKind().properties['process-parameter-action']"><ww:param name="'documentId'" value="documentId"/></ww:url>','Proces', 800, 400); return;"/>
		    	</ds:available>
		    </ww:if>
		</td>
		<ds:available test="pokazProcesIco">
		<td align="right">
		    		<img id="pokazProcesIco" border="0" src= "<c:out value='${pageContext.request.contextPath}'/>/img/pokazProces.gif" style="cursor:pointer; padding-right: 5px;" title="<ds:lang text="PokazProces"/>" onclick="openToolWindow('<ww:url value="fm.getDocumentKind().properties['process-parameter-action']"><ww:param name="'documentId'" value="documentId"/></ww:url>','Proces', 800, 400); return;" />
		    		
		    		
		    		</td>
		    	</ds:available>
	</tr>
<input type="hidden" name="doChangeDockind" id="doChangeDockind"/>

<script type="text/javascript">
	/**
	 * zmienia aktualny rodzaj dokumentu - prze�adowuj�c stron� 
	 */
	function changeDockind()
	{
		var form = document.getElementById('form');
		form.setAttribute('action', form.getAttribute('action')+'#documentKindCn');
		document.getElementById('doChangeDockind').value = 'true';
		document.forms[0].submit();
	}

	function validateDockind(){
	    try {
	        return _validateDockind();
	    } catch (ex) {
	        alert(ex);
	        return false;
	    }
	}

	// dla o i O z kreska
 	function convertCharO(text) {
		var pos = text.indexOf('&');
		if (pos == -1) {
			return text;
		} else {
			var temp_text = text.substring(0, pos);
			while (pos != -1) {
				if (text.length >= (pos + 6)) {
					if (text.substring(pos + 3, pos + 5) == 'D3') temp_text += '�';
					if (text.substring(pos + 3, pos + 5) == 'F3') temp_text += '�';
				}
				var tt= text.substring(temp_text.length + 1);
				pos = tt.indexOf('&');
			}
		    return temp_text += text.substring(temp_text.length + 5);
		}
	}

	function _validateDockind() {

		onsubmitFunction();
	
		var field, val;
		<ww:if test="fm.mainFieldCn != null">
		var mainValue = document.getElementById('<ww:property value="'dockind_'+fm.mainFieldCn"/>').value;
		</ww:if>
		<%-- sprawdzanie obligatoryjno�ci p�l --%>
		<ww:iterator value="fm.getFieldsByAspect(documentAspectCn)">
			<ww:set name="prefix" value="'dockind_'"/>
			<ww:set name="tagId" value="#prefix+cn"/>
			<%-- [<ww:property value="fm.mainFieldCn"/>] [<ww:property value="cn"/>] [<ww:property value="required"/>] --%>
			<ww:if test="fm.mainFieldCn != cn && fm.getShemeVisible(cn) && required">
				field = document.getElementById('<ww:property value="#tagId"/>');
				if (field != null) {
					<ww:if test="requiredWhen != null && requiredWhen.size() > 0">
						<ww:iterator value="requiredWhen" status="status">
							<ww:if test="#status.first">if ((</ww:if><ww:else> || </ww:else>
							<ww:if test="type == 'main'">
								(mainValue == <ww:property value="fieldValue"/>)
							</ww:if>
							<ww:else>
								(document.getElementById('dockind_<ww:property value="fieldCn"/>').value == <ww:property value="fieldValue"/>)   
							</ww:else>
						</ww:iterator>
						) && (emptyField(field)))
					</ww:if> <ww:else>if (emptyField(field)
								<ww:if test="type != 'class' && type != 'enum-ref'">
									&&	field.type != 'hidden' </ww:if>
								&& field.disabled != true) </ww:else>
					{
							alert('<ds:lang text="Pole"/>'+ convertCharO(' <ww:property value="name"/> ') +'<ds:lang text="JestObowiazkowe"/>');
							return false;
					}
				}
			</ww:if>
		</ww:iterator>
		
		<%-- pozosta�a walidacja p�l --%>
		<ww:if test="fm.documentKind.validationRules != null">
			<ww:iterator value="fm.documentKind.validationRules">
				<ww:if test="type == 'min-length'">
					val = document.getElementById('dockind_<ww:property value="fieldCn"/>').value;
					if ((val != null) && (val.trim().length < <ww:property value="value"/>))
					{
						alert('<ds:lang text="DlugoscPola.dockind.fields"/>'+' <ww:property value="fm.getField(fieldCn).name"/> '+'<ds:lang text="MusiMiecPrzynajmniej"/>'+' <ww:property value="value"/> '+'<ds:lang text="Znakow"/>');
						return false;
					}
				</ww:if>
				<ww:if test="type == 'min-length-no-spaces'">
					val = document.getElementById('dockind_<ww:property value="fieldCn"/>').value;
					if ((val != null) && (val.replaceAll(' ', '').length < <ww:property value="value"/>))
					{
						alert('<ds:lang text="Dlugosc.dockind.fields"/>'+' <ww:property value="fm.getField(fieldCn).name"/> '+'<ds:lang text="MusiMiecPrzynajmniej"/>'+' <ww:property value="value"/> '+'<ds:lang text="ZnakowZwylaczeniemSpacji"/>');
						return false;
					}
				</ww:if>
			</ww:iterator>
		</ww:if>
		
		<%-- sprawdzanie obecno�ci uwagi --%>
		if ((document.getElementById('checkRemark') != null) && (document.getElementById('checkRemark').value == 'true'))
		{
			var remarkField = document.getElementById('remark');
			if ((remarkField != null) && emptyField(remarkField))
			{
				alert('<ds:lang text="PolePrzyczynaOdrzuceniaJestObowiazkowe"/>');
				return false;				
			}
		}
		
		if (additional_validateDockind)
		{
			if (!additional_validateDockind())
				return false;
		}

		<ww:if test="fm.documentKind.cn == 'invoice_ic' || fm.documentKind.cn == 'sad'">
		try{
		if(document.getElementById('dockind_NUMER_RACHUNKU_BANKOWEGO').value.length != 26 && document.getElementById('dockind_NUMER_RACHUNKU_BANKOWEGO').value.length > 0)
		{
			alert('Numer rachunku powinien miec 26 cyfr '+document.getElementById('dockind_NUMER_RACHUNKU_BANKOWEGO').value.length);
			return false;
		}
		} catch (e) {
			
		}
		
		</ww:if>
		return true;
	} /* End of validateDockind() */

	function onchange_main_field(mainField) {

		var tr, field, span;
		var value = mainField != null ? mainField.value : null;
		<ww:iterator value="fm.getFieldsByAspect(documentAspectCn)">
			<ww:set name="prefix" value="'dockind_'"/>
			<ww:set name="tagId" value="#prefix+cn"/>

			try { <ww:if test="type == 'dataBase'">
				var mainFieldId = mainField.id.substring(8);
				if(mainField.id != '<ww:property value="#tagId"/>' && '<ww:property value="refField"/>' == mainFieldId) {
					<ww:property value="'update_database'+#tagId"/>();
				}
			</ww:if> } catch(e) {}
			
			<ww:if test="fm.mainFieldCn != cn && fm.getShemeVisible(cn)">
				<ww:if test="availableWhen != null && availableWhen.size() > 0">
					tr = document.getElementById('<ww:property value="'tr_'+#tagId"/>');
					field = document.getElementById('<ww:property value="#tagId"/>');
					if (tr != null) {
						<ww:iterator value="availableWhen" status="status">
							<ww:if test="!#status.first">else </ww:if>
							<ww:if test="type == 'main'">
								if (value == <ww:property value="fieldValue"/>)
									tr.style.display = "";
							</ww:if><ww:else>
								if (document.getElementById('dockind_<ww:property value="fieldCn"/>').value == <ww:property value="fieldValue"/>)
									tr.style.display = "";
							</ww:else>
						</ww:iterator>
						else {
							tr.style.display = "none";
							<ww:if test="!multiple">
							if (field.tagName.toUpperCase() == 'INPUT') {
							 	if (field.type.toUpperCase() == 'CHECKBOX') { } else {
									<ww:if test="type == 'class'">
										//clear_<ww:property value="cn"/>();
									</ww:if>
									<ww:else>
										/* nie mozna wyczyscic, bo tracimy wartosci
										alert('1971');
										field.value = '';
										*/
									</ww:else>
								}
							} else if (field.tagName.toUpperCase() == 'SELECT') {
								/* nie mozna wyczyscic, bo tracimy wartosci
								alert('1978');
								field.options.selectedIndex = 0;
								*/
							} </ww:if>
						}
					}
				</ww:if>
				<ww:if test="requiredWhen != null && requiredWhen.size() > 0">
					span = document.getElementById('<ww:property value="'required_'+cn"/>');
					if (span != null) {
						<ww:iterator value="requiredWhen" status="status">
							<ww:if test="!#status.first">else </ww:if>
							<ww:if test="type == 'main'">
								if (value == <ww:property value="fieldValue"/>)
									span.style.display = "";
							</ww:if><ww:else>
								if (document.getElementById('dockind_<ww:property value="fieldCn"/>').value == <ww:property value="fieldValue"/>)
									span.style.display = "";
							</ww:else>
						</ww:iterator>
						else {
							span.style.display = "none";
						}
					} </ww:if></ww:if>
		</ww:iterator>
		
		if ((mainField != null) && (additional_onchange_main_field))
		{
			additional_onchange_main_field(mainField);
		}
	}

	function createTextCell(value)				
	{				
		return createCell(document.createTextNode(value));				
	}									

	function createCell(elem)				
	{				
		var cell = document.createElement('td');				
		cell.appendChild(elem);
		return cell;				
	} 
        
        function setTimeFormat($value){
            if($value == null )
                return;
            if($value != null && $value.length > 5)
              return $value.substr(0,5).replace(/[^0-9\:]/g,'');
            else 
                return $value.substr(0,5).replace(/[^0-9\:]/g,'');
        }
        
</script>
<!--N koniec dockind-fields.jsp N-->
