<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include_js (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>
<!--N dockind-specific-additions.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%--
	Umieszczone s� tutaj funkcj� specyficzne dla poszczeg�lnych typ�w, ale tak�e niekt�re wsp�lne rzeczy,
	kt�re zale�� w pewnym stopniu od tych funkcji jak np. walidacja za��cznik�w czy pocz�tkowe wywo�anie
	funkcji onchange_main_field (gdy� dopiero tutaj s� definiowane dodatkowe rzeczy).
--%>

<ww:if test="(fm.documentKind.cn == 'daa') && (dockindAction == 'create')">
	<tr>
		<td>Dodaj wz�r umowy:</td>
		<td>
			<ww:select name="'attachmentPattern'" list="patterns" listKey="id" listValue="filename"
				headerKey="''" headerValue="getText('select.wybierz')"
				value="attachmentPattern" id="attachmentPattern" cssClass="'sel'"/>
		</td>
	</tr>
	<script type="text/javascript">
		/**
		 * Specyficzny update dla pola o cn='TYP_DOKUMENTU' dla rodzaju 'daa' - wzory um�w.
		 */
		function additional_update_dockind_TYP_DOKUMENTU(select)
		{
			var typeId = select.value;
			var patternId;
			<ww:iterator value="patterns">
				if (parseInt(typeId) == <ww:property value="typeId"/>)
					patternId = <ww:property value="id"/>;
				else
			</ww:iterator>
			patternId = 0;

			document.getElementById('attachmentPattern').selectedIndex = patternId;
		}
	</script>
</ww:if>


<script type="text/javascript">
	/**
	 * Walidacja wyboru za��cznika.
	 */

	$j(document).ready(function ()
	{
		$j("#ifStar").show();
    });


	function validateFiles()
	{
		<ds:available test="dontaskforattachment" documentKindCn="fm.documentKind.cn">
			return true;
		</ds:available>
		
		<ww:if test="fm.documentKind.cn == 'daa'">
			var file = document.getElementsByName('file');
			var files = document.getElementsByName('multiFiles');
			var returnLoadFiles = document.getElementsByName('returnLoadFiles');
			var patternFile = document.getElementById('attachmentPattern');
			if (/*patternFile.selectedIndex == 0 &&*/!isCheckFile(file) && !isCheckFile(files) && !isCheckFile(returnLoadFiles))
			{
				if (!confirm('<ds:lang text="NieWybranoZalacznikaCzyNaPewnoChceszUtworzycDokument"/>'))
					return false;
			}
		</ww:if>
		<ww:else>
			var file = document.getElementsByName('file');<%-- do poprawy - wynika z formatki new document w archiwym (najpierw poprawi� tam p�niej usun�� t� linijk�)  --%>
			var files = document.getElementsByName('multiFiles');
			var returnLoadFiles = document.getElementsByName('returnLoadFiles');
			if (!isCheckFile(files) && !isCheckFile(file) && !isCheckFile(returnLoadFiles))
			{
				<ww:if test="fm.documentKind.attachmentRequired">
					if ((typeof hasTempFiles == 'undefined') || !hasTempFiles)
					{
						alert('<ds:lang text="NieWybranoPliku"/>');
						return false;
					}
				</ww:if>
				<ww:else>
					if (!confirm('<ds:lang text="NieWybranoZalacznikaCzyNaPewnoChceszUtworzycDokument"/>'))
						return false;
				</ww:else>
			}
		</ww:else>
		return true;
	}

	function isCheckFile(files)
	{
		if(files == null || files.length == 0)
		{
			return false;
		}
		for (var i=0; i < files.length; i++)
		{
			if(files[i].value.trim().length == 0)
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * Funkcja wykonuj�ca specyficzne dla 'daa' zmiany w momencie zmiany warto�ci g��wnego pola.
	 */
	function additional_onchange_main_field(mainField)
	{
		<ww:if test="fm.documentKind.cn == 'daa'">
		if (mainField.value != 10)
		{
			// wybrana jest inna kategoria ni� AGENT (pola agenta sa niewidoczne) - w��czamy przyciski
			// do wyboru agencji (o ile mamy uprawnienie do odczytu s�ownik�w)
			<ww:if test="(canReadDictionaries) || (dockindAction == 'create')">
			document.getElementById('clear_AGENCJA_button').disabled = '';
			document.getElementById('open_AGENCJA_button').disabled = '';
			</ww:if>
		}
		else
		{
			// wybrana jest kategoria 'RODZAJ_AGENT' - wy��czamy przycisk do wyboru agencji
			document.getElementById('clear_AGENCJA_button').disabled = "disabled";
			document.getElementById('open_AGENCJA_button').disabled = "disabled";
		}
		</ww:if>
		<ww:elseif test="documentKindCn == 'zamowienie'">
			chCentrum(document.getElementById('centar_mpk'));
		</ww:elseif>
		//<ww:property value="documentKindCn"/>
	}

	/**
	 * Dodatkowa walidacja - uzale�niona od danego rodzaju dokumentu.
	 */
	function additional_validateDockind()
	{
		<ww:if test="(fm.documentKind.cn == 'daa') && (dockindAction == 'edit')">
			var kategoria = document.getElementById('dockind_KATEGORIA');
			var rodzajSieci = document.getElementById('dockind_RODZAJ_SIECI');
			var agent = document.getElementById('dockind_AGENT');
			var agencja = document.getElementById('dockind_AGENCJA')
			if (!(kategoria.value == 30) && (parseInt(rodzajSieci.value) == 1))
			{
				//alert(document.getElementById('RODZAJ_AGENCJA').options.selectedIndex+" "+parseInt(document.getElementById('__nw_AGENCJA').value));
				if ((kategoria.value == 20) && (!(parseInt(agencja.value) > 0)))
				{
					alert('Nie wybrano biura/agencji/banku');
					return false;
				}
				//alert(document.getElementById('RODZAJ_AGENT').options.selectedIndex+" "+parseInt(document.getElementById('__nw_AGENT').value));
				if ((kategoria.value == 10) && (!(parseInt(agent.value) > 0)))
				{
					alert('Nie wybrano specjalisty/OWCA');
					return false;
				}
			}

			<ww:if test="daaAssigned">
				if (!(parseInt(agencja.value) > 0) && !(parseInt(agent.value) > 0) && !(kategoria.value == 30))
				{
					alert('Dokument by� ju� przypisany, a nie wybrano specjalisty/OWCA ani biura/agencji/banku');
					return false;
				}
			</ww:if>
		</ww:if>

		return true;
	}

	// pocz�tkowa inicjalizacja p�l na podstawie warto�ci g��wnego pola

	try {
		onchange_main_field(document.getElementById('<ww:property value="'dockind_'+fm.mainFieldCn"/>'));
	} catch(e) {

	}
</script>
<!--N koniec dockind-specific-additions.jsp N-->