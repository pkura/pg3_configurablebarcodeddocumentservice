<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<h1>Dodaj za��cznik</h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
<ds:ww-action-errors /> 
<ds:ww-action-messages />
</p>

<form action="<ww:url value="'/office/common/add-attachment-from-pages.action'"/>" enctype="multipart/form-data" method="post">

<ww:hidden name="'attachmentId'" id="attachmentId"/>
<ww:hidden name="'documentId'" id="documentId"/>
<ww:hidden name="'binderId'" id="binderId"/>
<table>
	<ww:if test="files == null || files.size() < 1">
		<ww:if test="close">
			<script language="JavaScript">
				window.opener.document.forms[0].submit();
			</script>
		</ww:if>
		<ww:else>
			<tr>
				<td><ds:lang text="Plik" /><span class="star">*</span>:</td>
			</tr>
			<tr>
				<td><ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'" /></td>
			</tr>
			<tr>
				<td>
				<table id="kontener">
					<tbody id="tabela_glowna">
					</tbody>
				</table>
				</td>
			</tr>
			<tr>
				<td><a href="javascript:addAtta();"><ds:lang
					text="DolaczKolejny" /></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun" /></a></td>
			</tr>
			<tr>
				<td><ds:event value="'Pobierz'" name="'doNew'" cssClass="'btn'"/></td>
			</tr>
		</ww:else>
	</ww:if>
	<ww:else>
		<tr>
			<td valign="top"><ww:select id="selectPages" name="'selectPages'"
				multiple="true" size="10" cssClass="'multi_sel'"
				cssStyle="'width: 150px;'" list="files" onchange="'showImage(this)'"/>
				<br/>
				Do��cz za��cznik tylko do tej teczki <ww:checkbox name="'attachToOtherDocument'" fieldValue="true" /> 
				<br/>
				Zapisz jako PDF <ww:checkbox name="'asPdf'" fieldValue="true"/> 
				<br/>
				Do��cz do istniej�cewgo <ww:checkbox name="'attachFiles'" fieldValue="true"/> 
				<br/>
				<ds:event name="'doSave'" value="'Zapisz'"/>
			</td>
			<td>
				<img id="fileimage" ondblclick="openImage();" name="fileimage" 
				src="<ww:url value="'/img/loadPage.gif'"/>" alt="Wybierz stron� by wyswietli� zawarto��" style="border: 2px solid black;" 
				height="606" width="430"/>
				<script language="javascript">
    				var img_el = document.getElementById('fileimage');
    				img_el.style.height = document.body.clientHeight - 90 + 'px';
					img_el.style.width = 'auto';
				</script>
			</td>
		</tr>
	</ww:else>
</table>
</form>
<script language="JavaScript">
	var countAtta = 1;
	function addAtta() {
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.innerHTML = '<input type="file" name="multiFiles' + countAtta
				+ '" size="30" id="multiFiles' + countAtta + '" class="txt"/>';
		countAtta++;
		tr.appendChild(td);
		var kontener = document.getElementById('tabela_glowna');
		kontener.appendChild(tr);

	}

	function delAtta() {
		var x = document.getElementById('tabela_glowna');
		var y = x.rows.length;
		x.deleteRow(y - 1)
	}
	
	function showImage(select)
	{
		var fileimageObj = E('fileimage');
		var sel = typeof select == 'string' ? E(select) : select;
		for (var i=0; i < sel.length; i++)
		{
        	if( sel.options[i].selected == true)
       		{
        		var tmp = sel.options[i].value;
       			var tab = tmp.split('\\');
       			
       			var tmp = '<c:out value='${pageContext.request.contextPath}'/>/tempPng/'+tab[tab.length - 1];
       			
       			tmp += '.png';
       			fileimageObj.src = tmp;	
       		}
        }
	}
	
	function openImage()
	{
		var fileimageObj = E('fileimage');
		var nowe_okno = window.open();
		nowe_okno.document.write('<HTML><HEAD></HEAD><BODY><IMG SRC="'+fileimageObj.src+'" width="100%"></BODY></HTML>');

		
	}
</script>