<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%-- 
    Description: plik bedzie wyswietla� karte nadgodzin pracowika zatwierdzonych
    Document   : employee-overtime-card
    Created on : 2011-07-01, 15:47:29
    Author     : �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
--%>
<ww:if test="fm.documentKind.cn == 'overtime_receive'">
    <a href="<ww:url value="'/pdf-overtimes.action'">
				 	<ww:param name="'currentYear'" value="currentYear" />
				 	<ww:param name="'doPdf'" value="'true'" />
				 </ww:url>">
			<img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>" />
			<ds:lang text="Pobierz karte PDF" />
    </a>
    <a href="<ww:url value="'/pdf-overtimes.action'">
				 	<ww:param name="'currentYear'" value="currentYear" />
				 	<ww:param name="'doXls'" value="'true'" />
				 </ww:url>">
			<img src="<ww:url value="'/img/report.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="PobierzZalacznik"/>" />
			<ds:lang text="Pobierz karte XLS" />
    </a>
<table class="search mediumTable table100p">
<thead>
	<tr>
		<th class="empty">
			<ds:lang text="Lp"/>	
		</th>
		<th class="empty">
			<ds:lang text="DataOd"/>
		</th>
		<th class="empty">
			<ds:lang text="DataDo"/>
		</th>
		<th class="empty">
			<ds:lang text="LiczbaWypracowanychNadgodzin"/>
		</th>
		<th class="empty">
			<ds:lang text="LiczbaOdebranychNadgodzin"/>
		</th>
		<th class="empty">
			<ds:lang text="PozostaleNadgodziny"/>
		</th>
		<th class="empty">
			<ds:lang text="Szczegoly"/>
		</th>
	</tr>
</thead>
<tbody>
<ww:iterator value="empOvertimes" status="status">
    <tr>
        <td>
            <ww:property value="month"/>
        </td>
        <td>
            <ww:property value="firstDayOfMonth"/>
        </td>
        <td>
            <ww:property value="lastDayOfMonth"/>
        </td>
        <td>
            <ww:property value="sumHour"/>:<ww:property value="sumMinute"/>
        </td>
        <td>
            <ww:property value="sumHourReceive"/>:<ww:property value="sumMinuteReceive"/>
        <td>
            <ww:property value="leftHour"/>:<ww:property value="leftMinute"/>
        </td>
        <td>
            <a href="#" onclick="openToolWindowX('<ww:url value="'/overtime-card/overtime-monthly.action'"><ww:param name="'popup'" value="true" /><ww:param name="'month'" value="month"/><ww:param name="'currentYear'" value="currentYear"/></ww:url>', 
                                'xx', 1000, 700)"><ww:property value="overtimeMonth"/></a>
        </td>
    </tr>
</ww:iterator>
</tbody>
<tfoot>
    <tr>
        <td colspan="3"><b><ds:lang text="PODSUMOWANIE"/></b></td>
        <td><b><ww:property value="empOvertimesAllHour"/></b></td>
        <td><b><ww:property value="empOvertimesAllHourReceive"/></b></td>
        <td><b><ww:property value="empOvertimesAllHourLeft"/></b></td>
    </tr>
</tfoot>
</table>
</ww:if>