<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N acceptances-ic.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<ww:if test="fm.acceptancesDefinition != null"> 
<tr class="formTableDisabled">
	<td colspan="2">
        <input type="hidden" name="doGiveAcceptance" id="doGiveAcceptance"/>        
        <input type="hidden" name="doSendToDecretation" id="doSendToDecretation"/>        
        <input type="hidden" name="doWithdrawAcceptance" id="doWithdrawAcceptance"/>
        <input type="hidden" name="acceptanceCn" id="acceptanceCn"/>
        <input type="hidden" name="objectId" id="objectId"/>
        <input type="hidden" name="centrumToAcceptanceId" id="centrumToAcceptanceId"/>
        <input type="hidden" name="acceptanceToUser" id="acceptanceToUser"/>
        <h4><ds:lang text="akceptacjeCentrumKosztowego"/></h4>                   
        <ww:set name="numberRows" value="0"/>
		<ww:set name="centrumCn" value="fm.acceptancesDefinition.centrumKosztFieldCn"/>
        <ww:if test="fm.getValue(#centrumCn) != null && fm.getValue(#centrumCn).size() > 0">
        	
		<ww:iterator value="fm.getValue(#centrumCn)" status="status">
			<ww:set name="result"/>
	        <ww:set name="isAcc" value="1"/>
			<table id="acceptanceTable" style="margin-top:5px; border: 2px solid #C3C4C6" width="500px" class="formTable">
				<tr>
					<td colspan="5" style="white-space: normal">
						<ww:property value="#result.getTextToJsp()"/> <br/>
						<ww:if test="showFullAccount">
						    Zbudowane konto : <ww:property value="#result.fullAccountNumber"/>
						</ww:if>
					</td>
				</tr>
				<tr>
					<ww:set name="firstAcceptance" value="fm.acceptancesState.getFieldAcceptance('zwykla',#result.id)"/>
					<ww:if test="centrum == null || !centrum.intercarsPowered">
						<ww:set name="modes" value="#result.possibleAcceptanceModes"/>
						<ww:set name="centrumId" value="centrumId"/>
					</ww:if>
					<ww:else>
						<ww:set name="modes" value="centrum.availableAcceptanceModes"/>
						<ww:set name="centrumId" value="centrum.id"/>
					</ww:else>
					<ww:if test="#firstAcceptance == null">
						<script type="text/javascript">
							function refreshAcceptances(){
								sendEvent($j("input[name='doArchive']:first").get(0));
							}
						</script>
						<td>
							<ww:if test="acceptanceModeInfo == null">
								Tryb akceptacji :
								
								<ww:if test="fm.getBoolean('AKCEPTACJA_STALA')">
								    Akceptacja sta�a<ww:hidden name="'centrum' + id" value="'3'"/>
									<%--<ds:degradable-select name="'centrum' + id"
										   list="#modes"
										   listValue="acceptanceNames.get(#centrumId + '.' + id)"
										   listKey="id"
										   onchange="'refreshAcceptances();'" disabled="true" />--%>
								</ww:if>
								<ww:else>
									<ds:degradable-select name="'centrum' + id"
										   list="#modes"
										   listValue="acceptanceNames.get(#centrumId + '.' + id)"
										   listKey="id"
										   onchange="'refreshAcceptances();'" disabled="false" />
								</ww:else>
							</ww:if>
							<ww:else>
								Tryb akceptacji :
								<ww:if test="fm.getBoolean('AKCEPTACJA_STALA')">
								    Akceptacja sta�a<ww:hidden name="'centrum' + id" value="'3'"/>
									<%--<ds:degradable-select name="'centrum' + id"
										   list="#modes"
										   headerKey="acceptanceModeInfo.id"
										   headerValue="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id)"
										   listValue="acceptanceNames.get(#centrumId + '.' + id)"
										   listKey="id"
										   onchange="'refreshAcceptances();'" disabled="true"/>--%>
								</ww:if>
								<ww:else>
									<ds:degradable-select name="'centrum' + id"
										   list="#modes"
										   headerKey="acceptanceModeInfo.id"
										   headerValue="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id)"
										   listValue="acceptanceNames.get(#centrumId + '.' + id)"
										   listKey="id"
										   onchange="'refreshAcceptances();'" disabled="false" />
								</ww:else>
							</ww:else>
						</td>
					</ww:if>
					<ww:else>
						<td colspan="5">
						    Tryb akceptacji :
							 <ww:if test="fm.getBoolean('AKCEPTACJA_STALA')"> Akceptacja sta�a
							 </ww:if>
							 <ww:else>
							<ww:if test="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id) != null"><ww:property value="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id)"/></ww:if><ww:else><ww:property value="acceptanceModeInfo.name"/></ww:else>
							</ww:else>
						</td>
					</ww:else>
				</tr>

				<ww:if test="acceptanceModeInfo == null && #firstAcceptance == null">
					<tr>
						<td>Zwyk�a</td>
						<ww:set name="isAcc" value="2" />
						<td></td>
						<td></td>
					</tr>
				</ww:if>
				<ww:else>
					<ww:iterator value="fm.acceptancesDefinition.fieldAcceptances">
						<ww:if test="acceptanceModeInfo.isAcceptanceNeeded(value.cn)">
							<tr>
								<td><ww:property value="value.name" /></td>
								<ww:set name="acceptance"
									value="fm.acceptancesState.getFieldAcceptance(key,#result.id)" />
								<ww:if test="#acceptance != null">
									<td><ww:property value="#acceptance.asFirstnameLastname" />
									</td>
									<td><ds:format-date value="#acceptance.acceptanceTime"
										pattern="dd-MM-yy HH:mm:ss" />
										<ww:if test="rejectObjectAcceptanceAvailable"><input type="button" value="Odrzu� akceptacj�" onclick="discardObjectAcceptance('<ww:property value="key"/>','<ww:property value="#result.id"/>', this)"/></ww:if>
									</td>
								</ww:if>
								<ww:else>
									<ww:set name="isAcc" value="2" />
									<td></td>
									<td></td>
								</ww:else>
							</tr>
						</ww:if>
					</ww:iterator>
				</ww:else>
				<%--
				<ds:available test="!acceptances.simplified">
				<ww:if test="activity != null && #isAcc == 2">
					<tr>
						<td colspan="4">
							<input type="submit"
								onclick="if(!validateFormToNext(this,<ww:property value="#result.id"/>)) return false;"
								value="Akceptuj" class="btn" name="doNextDecretation"/>
							<input type="submit"
								onclick="if(!validateFormToUser(this,<ww:property value="#result.id"/>)) return false;"
								value="Akceptuj i przeka� do" class="btn" name="doNextDecretation"/>
							<ww:select id="userAcceptance" name="'userAcceptance'"
								list="userToNextAcceptance.get(#result.id)" headerKey="''"
								headerValue="getText('select.wybierz')" cssClass="'sel'"/>
						</td>
					</tr>
				</ww:if>
				</ds:available>
				--%>
			</table>
		</ww:iterator>
	       	
	       	
		<table width="500px" class="formTable">
			<ww:iterator value="fm.getAcceptancesDefinition().getGeneralAcceptances()">
				<ww:if test="cnsNeeded.contains(value.cn)">
					<tr>
						<td>
							<ww:property value="value.name"/>
						</td>
						<ww:set name="acceptance" value="fm.acceptancesState.getGeneralAcceptancesAsMap().get(value.cn)"/>
						<ww:if test="#acceptance != null">
							<td>
								<ww:property value="#acceptance.asFirstnameLastname"/>
							</td>
							<td>
								<ds:format-date value="#acceptance.acceptanceTime" pattern="dd-MM-yy HH:mm:ss"/>
							</td>
						</ww:if>
						<ww:else>
							<td></td>
							<td></td>
						</ww:else>
					</tr>
				</ww:if>
			</ww:iterator>
		</table>
      </ww:if>
    </td>
</tr>
</ww:if>
<script>
	function validateFormToNext(btn,centrumId) {
		if (!validateDockind())
			return false;
		var obj = document.getElementById('acceptanceToUser');
		obj.value = false;
		var obj2 = document.getElementById('centrumToAcceptanceId');
		obj2.value = centrumId;
		sendEvent(btn);
		return true;
	}

	function validateFormToUser(btn,centrumId) {
		if (!validateDockind()){
			return false;
		}

		if($j("#userAcceptance").val().length==0){
		    alert('Musisz wybra� u�ytkownika');
            return false;
		} else {
		    sendEvent(btn);
		    return true;
		}
	}
</script>
<!--N acceptances-ic.jsp KONIEC N-->