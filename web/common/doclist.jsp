<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<!--N poczatek doclist.jsp N-->
<tr>
	<td colspan="3">
			<table>
				<tr>
					<td id="teczkaCell">	
						<ww:set name="result" value="fm.getKey(cn)"/>  
						<ww:property value="#result.getDocListHTML(#fieldResult)" escape="false"/>
					</td>
				</tr>
			</table>
			<ww:if test="brakeEnumItems != null && brakeEnumItems.size() > 0">
				<table>
				<tr>
					<td class="alignTop">
						<ww:select name="'availableDocument'" listKey="getCn()" listValue="getTitle()" multiple="true" size="5" 
						cssClass="'multi_sel'" cssStyle="'width: 250px;'" list="brakeEnumItems" id="availableDocument"/>
					</td>
					<td class="alignMiddle">
						<input type="button" value=" &gt;&gt; " name="moveRightButton"
							onclick="if (moveOptions(this.form.availableDocument, this.form.newDocument) > 0) dirty=true;" class="btn"/>
						<br/>
						<input type="button" value=" &lt;&lt; " name="moveLeftButton"
							onclick="if (moveOptions(this.form.newDocument, this.form.availableDocument) > 0) dirty=true;" class="btn"/>
					</td>
					<td class="alignTop">
						<ww:select name="#tagName+'newDocument'" id="newDocument" multiple="true" size="5"
						cssClass="'multi_sel'" cssStyle="'width: 200px;'"/>
					</td>
					<td class="alignMiddle">
					<ww:if test="brakeEnumItems != null && brakeEnumItems.size() > 0">						
						Do��cz dokument do teczek zwi�zanych z wnioskiem: <ww:checkbox name="#tagName+'attachToOtherBinder'" id="attachToOtherBinder" fieldValue="'true'"/> 
						<br/>
						<ww:select cssClass="'sel'" name="#tagName+'newDocumentKind'" list="getNewDocumentKinds()" id="newDocumentKind"/>	
						<br/>		
						<input type="submit" class="btn" value="Dodaj dokumenty"
								onclick="onclick_<ww:property value="cn"/>Btn();"/>							
						<script type="text/javascript">
							function onclick_<ww:property value="cn"/>Btn() 
							{
								selectAllOptions(document.getElementById('newDocument'));
								document.getElementById('doDockindEvent').value = true;
								document.getElementById('dockindEventValue').value = '<ww:property value="cn"/>';
							}
						</script>		
					</ww:if>
					</td>
				</tr>
				</table>
			</ww:if>
			<table>
			
			<ds:dockinds test="dlbinder">
			<tr><td>
			<input type="button" class="btn" value="<ds:lang text="Do��cz dokumenty"/>" onClick="openNew()"/>
              <script type="text/javascript">
              		function openNew()
              		{
              			openToolWindow('<ww:url value="'/repository/new-portfolios-for-doc.action'"><ww:param name="'id'" value="fm.getKey('KLIENT')"/>
                      			<ww:param name="'dostawcaId'" value="fm.getKey('DOSTAWCA')"/>
              					<ww:param name="'contractId'" value="fm.getKey('NUMER_UMOWY')"/>
              					<ww:param name="'applicationId'" value="fm.getKey('NUMER_WNIOSKU')"/>
              					<ww:param name="'doNew'" value="'true'"/>
              					<ww:param name="'binderId'" value="documentId"/></ww:url>','vs', 800, 600);
              		}
              </script>
            </td></tr> 
            </ds:dockinds>
		</table>
    </td>
</tr>
<script type="text/javascript">

function showText(textId)
{
	var textObj = 	document.getElementById(textId);
	var boxObj = document.getElementById(textId+'_box');
	var attaObj = document.getElementById(textId+'_atta');
	if(textObj.style.display == 'none')
	{
		textObj.style.display = '';	
		attaObj.style.display = 'none';
	}
	else
	{
		textObj.style.display = 'none';
		attaObj.style.display = '';
		boxObj.boBDY = textObj.value;
	}
}
function changeStatus(docId) 
{
	var obj = document.getElementById('DOCLIST_EF_STATUS_DOKUMENTU'+docId); 
	if(obj.tagName == 'SELECT')
		obj.options[2].selected = true;
	else
		obj.value = 20;
	return true;
}

function changeStatusDocs(docsId) 
{
	for (var i=0; i < docsId.length; i++)
	{
		var obj = document.getElementById('DOCLIST_EF_STATUS_DOKUMENTU'+docsId[i].value); 
		if(obj.tagName == 'SELECT')
			obj.options[2].selected = true;
		else
			obj.value = 20;
	}
}

function changeShow(textId,link) 
{
	var spanObj = document.getElementById(textId);
	var linkObj = document.getElementById(link);
	if(spanObj.style.display == 'none')
	{
		spanObj.style.display = '';	
		linkObj.style.color = 'red';
	}
	else
	{
		spanObj.style.display = 'none';
		linkObj.style.color = 'black'
	}
}

function aTarget(el, name)
{
	var wnd = window.open(typeof el == 'string' ? el : el.href, typeof name != 'undefined' ? name : '', 'menubar=yes,toolbar=yes,location=yes,directories=no,status=yes,scrollbars=yes,resizable=yes');
	if (!wnd) return false;
	wnd.focus();
	return true; 
}
</script> 

<!--N koniec doclist.jsp N-->