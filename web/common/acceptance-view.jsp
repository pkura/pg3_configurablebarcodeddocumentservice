<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<ww:if test="fm.acceptancesDefinition != null">
<tr>
	<td colspan="2">
        <h4><ds:lang text="akceptacjeCentrumKosztowego"/></h4>
        <ww:set name="numberRows" value="0"/>
		<ww:set name="centrumCn" value="fm.acceptancesDefinition.centrumKosztFieldCn"/>
        <ww:if test="fm.getValue(#centrumCn) != null && fm.getValue(#centrumCn).size() > 0">

		<ww:iterator value="fm.getValue(#centrumCn)" status="status">
			<ww:set name="result"/>
	        <ww:set name="isAcc" value="1"/>
	        <table id="acceptanceTable" style="margin-top:5px; border: 2px solid #C3C4C6">
				<tr>
					<td colspan="5">
						<ww:property value="#result.getTextToJsp()"/>
					</td>
				</tr>
				<tr>
					<ww:set name="firstAcceptance" value="fm.acceptancesState.getFieldAcceptance('zwykla',#result.id)"/>
					<ww:if test="centrum == null || !centrum.intercarsPowered">
						<ww:set name="modes" value="#result.possibleAcceptanceModes"/>
						<ww:set name="centrumId" value="centrumId"/>
					</ww:if>
					<ww:else>
						<ww:set name="modes" value="centrum.availableAcceptanceModes"/>
						<ww:set name="centrumId" value="centrum.id"/>
					</ww:else>
					<ww:if test="#firstAcceptance == null">
						<td>
						</td>
					</ww:if>
					<ww:else>
						<td colspan="5">
							Tryb akceptacji : <ww:if test="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id) != null"><ww:property value="acceptanceNames.get(#centrumId + '.' + acceptanceModeInfo.id)"/></ww:if><ww:else><ww:property value="acceptanceModeInfo.name"/></ww:else>
						</td>
					</ww:else>
				</tr>

				<ww:if test="acceptanceModeInfo == null && #firstAcceptance == null">
					<tr>
						<td>Zwyk�a</td>
						<ww:set name="isAcc" value="2" />
						<td></td>
						<td></td>
					</tr>
				</ww:if>
				<ww:else>
					<ww:iterator value="fm.acceptancesDefinition.fieldAcceptances">
						<ww:if test="acceptanceModeInfo.isAcceptanceNeeded(value.cn)">
							<tr>
								<td><ww:property value="value.name" /></td>
								<ww:set name="acceptance"
									value="fm.acceptancesState.getFieldAcceptance(key,#result.id)" />
								<ww:if test="#acceptance != null">
									<td><ww:property value="#acceptance.asFirstnameLastname" />
									</td>
									<td><ds:format-date value="#acceptance.acceptanceTime"
										pattern="dd-MM-yy HH:mm:ss" /></td>
								</ww:if>
								<ww:else>
									<ww:set name="isAcc" value="2" />
									<td></td>
									<td></td>
								</ww:else>
							</tr>
						</ww:if>
					</ww:iterator>
				</ww:else>
				<ww:if test="activity != null && #isAcc == 2">
					<tr>
						<td colspan="4">
						</td>
					</tr>
				</ww:if>
			</table>
		</ww:iterator>


		<table width="500px">
			<ww:iterator value="fm.getAcceptancesDefinition().getGeneralAcceptances()">
				<ww:if test="cnsNeeded.contains(value.cn)">
					<tr>
						<td>
							<ww:property value="value.name"/>
						</td>
						<ww:set name="acceptance" value="fm.acceptancesState.getGeneralAcceptancesAsMap().get(value.cn)"/>
						<ww:if test="#acceptance != null">
							<td>
								<ww:property value="#acceptance.asFirstnameLastname"/>
							</td>
							<td>
								<ds:format-date value="#acceptance.acceptanceTime" pattern="dd-MM-yy HH:mm:ss"/>
							</td>
						</ww:if>
						<ww:else>
							<td></td>
							<td></td>
						</ww:else>
					</tr>
				</ww:if>
			</ww:iterator>
		</table>
      </ww:if>
    </td>
</tr>
</ww:if>
