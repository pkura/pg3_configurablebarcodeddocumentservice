<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N summary-acceptances.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<jsp:include page="/common/acceptances.jsp"/>
<ww:if test="(fm.acceptancesDefinition.finalAcceptance != null) && (fm.acceptancesDefinition.finalAcceptance.fieldCn != null)">
    <ww:set name="finalFieldCn" value="fm.acceptancesDefinition.finalAcceptance.fieldCn"/> 
    <tr>
        <td colspan="2"><b><ww:property value="fm.getField(#finalFieldCn).name"/></b><%--</td>
        <td>--%>&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="checkbox" name="<ww:property value="#tagName"/>" value="true" 
                    <ww:if test="fm.getKey(#finalFieldCn)">checked="true"</ww:if> disabled="disabled"/>
        </td>
    </tr>
</ww:if>