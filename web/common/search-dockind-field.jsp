<%@ page import="pl.compan.docusafe.util.DateUtils,
 			pl.compan.docusafe.core.office.Person"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<ww:if test="searchByRange && (type == 'string' || type == 'integer' || type == 'long' || type == 'float' || type == 'double' || type == 'money')">
    <table class="leftAlign oneLine">
    <tbody class="leftAlignInner oneLine">
    <tr>
    <td><span id="od:<ww:property value="#tagId"/>"><ds:lang text="od"/></span></td><td><input type="text" size="13" maxlength="<ww:property value="type == 'string' ? length : 10"/>" class="txt"
        name="<ww:property value="#tagName"/>_from"
        id="<ww:property value="#tagId"/>_from"
        value="<ww:property value="values[cn+'_from']"/>"/></td>
    <td><span id="do:<ww:property value="#tagId"/>"><ds:lang text="do"/></span></td><td><input type="text" size="13" maxlength="<ww:property value="type == 'string' ? length : 10"/>" class="txt"
        name="<ww:property value="#tagName"/>_to"
        id="<ww:property value="#tagId"/>_to"
        value="<ww:property value="values[cn+'_to']"/>"
        onchange="compareNumbers('<ww:property value="#tagId"/>_from', '<ww:property value="#tagId"/>_to');"/>  </td>
    </tr>
    </tbody>
    </table>

    <ww:if test="type == 'string'">
    <textarea name="in<ww:property value="#tagName"/>" id="in<ww:property value="#tagId"/>"
         cols="30" rows="3" style="display:none;" class="txt"></textarea>
   <a href="javascript:void(changeDisplayRange('<ww:property value="#tagId"/>'))">
        <img id="img<ww:property value="#tagId"/>" src="<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif" width="11" height="11" border="0"/>
   </a>
    </ww:if>
</ww:if>
<ww:elseif test="type == 'centrum-koszt'">
            <ww:select headerKey="''" headerValue="'-- wybierz --'" name="'values.CENTRUM_KOSZTOWE'" id="dockind_CENTRUM_KOSZTOWE" cssClass="'sel'" list="centra" listKey="id" listValue="SearchTitle" />
        <td>
    </tr>
    <tr id="<ww:property value="'tr_KONTA_KOSZTOWE'"/>">
        <ds:available test="!acceptances.simplified">
        <td valign="top"><ds:lang text="KontoKosztowe"/></td>
        <td>

            <ww:select headerKey="''" headerValue="'-- wybierz --'" name="'values.KONTA_KOSZTOWE'" id="dockind_KONTA_KOSZTOWE" cssClass="'sel'" list="konta" listKey="number" listValue="number+' - '+name" />
        </ds:available>
    <ds:dockinds test="invoice_ic">
    <tr id="<ww:property value="'tr_LOKALIZACJA'"/>">
        <ds:available test="!acceptances.simplified">
        <td valign="top"><ds:lang text="Lokalizacja"/></td>
        <td>

            <ww:select headerKey="''" headerValue="'-- wybierz --'" name="'values.CENTRUM_LOKALIZACJA'" id="dockind_CENTRUM_LOKALIZACJA" cssClass="'sel'" list="locations" listKey="id" listValue="cn + ' - ' + title" />
        </ds:available>
    </tr>
    </ds:dockinds>
</ww:elseif>

<ww:elseif test="type == 'string' || type='document-reference-id' || type='document-office-case-symbol'">
    <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
        value="<ww:property value="values[cn]"/>" size="30" maxlength="<ww:property value="length"/>" class="txt" />

   <textarea name="in<ww:property value="#tagName"/>" id="in<ww:property value="#tagId"/>"
         cols="30" rows="3" style="display:none;" class="txt"></textarea>
   <a href="javascript:void(changeDisplay('<ww:property value="#tagName"/>','<ww:property value="#tagId"/>'))">
        <img id="img<ww:property value="#tagId"/>" src="<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif" width="11" height="11" border="0"/>
   </a>


</ww:elseif>
<ww:elseif test="type == 'float' || type == 'money'">
    <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
        value="<ww:property value="values[cn]"/>" size="30" maxlength="30" class="txt" />
</ww:elseif>

<ww:elseif test="type == 'list-value'">
    <ww:if test="multiple">
        <ww:hidden name="'in' + #tagName" id="valuesString" value="''" />
        <table class="tableMargin">
        <tr id="<ww:property value="'attTr_' + cn + '1'"/>">
            <td colspan="2">
                <select id="<ww:property value="#tagId"/>0" class="sel listValue" <ww:property value="fm.getStyle(cn,canEdit)" /> >
                    <option value="">
                        <ds:lang text="select.wybierz"/>
                    </option>
                    <ww:iterator value="enumItems">
                        <ww:if test="available">
                            <option value='<ww:property value="id"/>'>
                                <ww:property value="title"/>
                            </option>
                        </ww:if>
                    </ww:iterator>
                </select>
            </td>
        </tr>
        <tr id="<ww:property value="'attCreateTr_'+cn"/>">
            <td colspan="2">
                <input type="button" value=<ds:lang text="dodaj"/> class="btn" onclick="addFieldValue_<ww:property value="cn"/>()"/>
            </td>
        </tr>
        </table>
        <script type="text/javascript">
                var fieldsSize_<ww:property value="cn"/> = 1;
                var jqHiddenIn = $j('#valuesString');
                var hiddenValues = new Array();

                $j('select.listValue').bind('change.list', getAllSelectValues);


                function addFieldValue_<ww:property value="cn"/>()
                {
                    var option;

                    fieldsSize_<ww:property value="cn"/>++;
                    var attCreateTr = document.getElementById('attCreateTr_<ww:property value="cn"/>');
                    var tr = document.createElement('tr');
                    tr.setAttribute('id', '<ww:property value="'attTr_'+cn"/>'+fieldsSize_<ww:property value="cn"/>);
                    // tworzymy select bez nazwy
                    var input = document.createElement('select');
                    //input.setAttribute('name', '<ww:property value="#tagName"/>');
                    input.setAttribute('class', 'txt');
                    input.className = 'sel listValue';
                    input.setAttribute('id', '<ww:property value="#tagId"/>'+fieldsSize_<ww:property value="cn"/>);

                    //dodajemy opcje z poprzedniego selekta
                    $j(input).html($j('#<ww:property value="#tagId"/>0').html()).css('width', $j('#<ww:property value="#tagId"/>0').width() + 'px');

                    tr.appendChild(createCell(input,true));

                    // tworzymy przycisk "usu�"
                    input = document.createElement('input');
                    input.setAttribute('type', 'button');
                    input.setAttribute('value', '<ds:lang text="Usun"/>');
                    input.setAttribute('class', 'btn');
                    input.className = 'btn';
                    //input.setAttribute('onclick', 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')');
                    // trzeba robic obejscie jak ponizej bo wpp nie dziela pod IE
                    var func = 'removeFieldValue_<ww:property value="cn"/>('+fieldsSize_<ww:property value="cn"/>+')';
                    input.onclick = function() { var temp = new Function(func); temp(); };
                    tr.appendChild(createCell(input,false));

                    attCreateTr.parentNode.insertBefore(tr, attCreateTr);
                    $j('select.listValue:last').bind('change.list', getAllSelectValues);
                    $j(input).bind('click.list', function() {
                        removeSelectValue($j(this).parent('td').prev('td').children('select').val());
                    });
                }

                function createCell(elem)
                {
                    var cell = document.createElement('td');
                    cell.appendChild(elem);
                    return cell;
                }

                function removeFieldValue_<ww:property value="cn"/>(no)
                {
                    var tr = document.getElementById('<ww:property value="'attTr_'+cn"/>'+no);
                    if (tr != null)
                        tr.parentNode.removeChild(tr);
                }

                function getAllSelectValues() {
                    hiddenValues = new Array();
                    $j('select.listValue').each(function() {
                        if(this.value && this.value != '') {
                            var valPresent = false;
                            for(i = 0; i < hiddenValues.length; i ++) {
                                if(hiddenValues[i] == this.value) {
                                    valPresent = true;
                                    break;
                                }
                            }
                            if(!valPresent)
                                hiddenValues.push(this.value);
                        }
                    });
                    jqHiddenIn.val(hiddenValues.toString());
                }

                function removeSelectValue(val) {
                    for(i = 0; i < hiddenValues.length; i ++) {
                        if(hiddenValues[i] == val) {
                            hiddenValues.splice(i, 1);
                            break;
                        }
                    }
                    jqHiddenIn.val(hiddenValues.toString());
                }
            </script>
    </ww:if>
    <ww:else>
        <select name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" class="sel" <ww:property value="fm.getStyle(cn,canEdit)" /> >
            <option value="">
                <ds:lang text="select.wybierz"/>
            </option>
            <ww:iterator value="enumItems">
                <ww:if test="available">
                    <option value='<ww:property value="id"/>'>
                        <ww:property value="title"/>
                    </option>
                </ww:if>
            </ww:iterator>
        </select>
    </ww:else>
</ww:elseif>
<ww:elseif test="type == 'dsuser' || type == 'document-office-clerk'">
    <ww:if test="multiple">
    <select class="multi_sel" size="5" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" multiple="multiple">
            <ww:set name="fieldCn" value="cn"/>
             <option value=""><ds:lang text="select.wybierz"/></option>
            <ww:iterator value="enumItems">
                <option value="<ww:property value="id"/>">
                    <ww:property value="title"/>
                </option>
            </ww:iterator>
        </select>

        <br/>
        <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), true))"><ds:lang text="zaznacz"/></a>
        / <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), false))"><ds:lang text="odznacz"/></a>
        <ds:lang text="wszystkie"/>
   </ww:if>
   <ww:else>
            <select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>">
                <ww:set name="fieldCn" value="cn"/>
    <option value=""><ds:lang text="select.wybierz"/></option>
    <ww:iterator value="enumItems">
                        <option value="<ww:property value="id"/>">
                                <ww:property value="title"/>
                        </option>
                </ww:iterator>
        </select>
   </ww:else>

</ww:elseif>
<ww:elseif test="type == 'dataBase'">
    <ww:set name="refFieldCn" value="refField"/>
    <select class="sel"
             id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
             onchange="onchange_main_field(this)"
            <ww:set name="fieldCn" value="cn"/>>
            <option value=""><ds:lang text="select.wybierz"/></option>
            <ww:iterator value="getSortedEnumItems()">
                    <option value="<ww:property value="id"/>">
                            <ds:field mode="default-formatting" object="[0].top"/>
                    </option>
            </ww:iterator>
        </select>
        <%-- teraz to nie mo�e dzia�a�, przy czym blokuje wyszukiwanie

        gdy brak uprawnien do edycji (pole jest "disabled")
        <ww:if test="!canEdit">
            <input type="hidden" name="<ww:property value="#tagName"/>"
            id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"
            />
         </ww:if>
         --%>
         <script type="text/javascript">
            function <ww:property value="'update_database'+#tagId"/>()
            {
                <ww:if test="refField != null && !searchStyle.equals('all-visible')">
                    //<ww:property value="searchStyle"/>
                    var selObj = document.getElementById('<ww:property value="#tagId"/>');
                    var refValue = document.getElementById('<ww:property value="'dockind_'+refField"/>').value;
                    //alert(refValue);
                    var a = 0;
                    var b = selObj.options.length;
                    for(a=0; a < b; a++)
                    {
                        //alert('usuwam '+a);
                        selObj.remove(selObj.options.length-1);
                    }
                    var elOptNew = document.createElement('option');
                    var s = '<ds:lang text="select.wybierz"/>';
                    elOptNew.text = s.replace(/&#xF3;/g,'�');
                    elOptNew.value = '';

                    try {
                        selObj.add(elOptNew, null);
                     }
                     catch(ex)
                     {
                        selObj.add(elOptNew); // IE only
                     }

                    <ww:if test="true">
                    <ww:iterator value="getSortedEnumItems()">
                        var elOptNew = document.createElement('option');
                        var s = '<ds:field mode="default-formatting" object="[0].top"/>';
                        elOptNew.text = s.replace(/&#xF3;/g,'�');
                        elOptNew.value = <ww:property value="id"/>;
                        <ww:if test="fm.getKey(#fieldCn) == id">
                            elOptNew.selected = true;
                        </ww:if>

                         try
                         {
                            selObj.add(elOptNew, null);
                         }
                         catch(ex)
                         {
                            selObj.add(elOptNew); // IE only
                         }

                    </ww:iterator>

                    <ww:iterator value="getSortedEnumItems()">
                        // por�wnuje jako string, bo property refValue mo�e by� puste
                        if('<ww:property value="refValue"/>' != (refValue + ''))
                        {
                            //alert('<ww:property value="refValue"/>   - '+refValue);
                            var i = 0;
                            var j = selObj.options.length;
                            for(i=0; i<selObj.options.length;i++)
                            {
                                if(selObj.options[i].value == <ww:property value="id"/>)
                                {
                                    //alert('D <ww:property value="id"/>'+i);
                                    selObj.remove(i);
                                }
                            }
                        }

                    </ww:iterator>
                    </ww:if>
                </ww:if>
            }

         </script>
</ww:elseif>
<ww:elseif test="type == 'document-office-delivery' || type == 'document-in-office-kind' || type == 'document-in-invoice-kind' || type == 'document-in-office-status'">
			<select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
                    onchange="try{<ww:property value="'onchange_'+#tagId+'(this);onchange_main_field(this)'"/>}catch(e){}<ww:property value="'onchange_main_field(this)'"/>">
                <ww:set name="fieldCn" value="cn"/>
                <option value=""><ds:lang text="select.wybierz"/></option>
                <ww:iterator value="getAllEnumItemsByAspect(documentAspectCn,kind)">
                    <option value="<ww:property value="id"/>" <ww:if test="values[#fieldCn] == id">selected="selected"</ww:if>>
                        <ww:property value="title"/>
                    </option>
                </ww:iterator>
            </select>
</ww:elseif>

<ww:elseif test="type == 'dsdivision'">
			<select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
                    onchange="try{<ww:property value="'onchange_'+#tagId+'(this);onchange_main_field(this)'"/>}catch(e){}">
                <ww:set name="fieldCn" value="cn"/>
                <option value=""><ds:lang text="select.wybierz"/></option>
                <ww:iterator value="getAllEnumItemsByAspect(documentAspectCn,kind)">
                    <option value="<ww:property value="id"/>" <ww:if test="values[#fieldCn] == id">selected="selected"</ww:if>>
                        <ww:property value="title"/>
                    </option>
                </ww:iterator>
            </select>
</ww:elseif>
<ww:elseif test="type == 'integer' || type == 'long'">
    <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
        value="<ww:property value="values[cn]"/>" size="10" maxlength="10" class="txt" />
</ww:elseif>
<ww:elseif test="type == 'enum'">
    <ww:if test="cn == kind.mainFieldCn && (#searchStyle == 'only-related')">
        <%-- PRZYPADEK: nie wszystkie pola wyszukiwania maj� by� widoczne --%>
        <ww:if test="refStyle == 'all-visible'">
            <input type="hidden" name="<ww:property value="#tagName"/>"
                id="<ww:property value="#tagId"/>" value="<ww:property value="values[cn]"/>"
                onchange="onchange_main_field(this)"
                />
        </ww:if>
        <ww:else> <%-- refStyle == 'chosen-visible' --%>
            <select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
                    onchange="try{<ww:property value="'onchange_'+#tagId+'(this);onchange_main_field(this)'"/>}catch(e){}">
                <ww:set name="fieldCn" value="cn"/>
                <option value=""><ds:lang text="select.wybierz"/></option>
                <ww:iterator value="getAllEnumItemsByAspect(documentAspectCn,kind)">
                    <option value="<ww:property value="id"/>" <ww:if test="values[#fieldCn] == id">selected="selected"</ww:if>>
                        <ww:property value="title"/>
                    </option>
                </ww:iterator>
            </select>
        </ww:else>
    </ww:if>
    <ww:else>
        <%-- PRZYPADEK: zawsze pokazujemy wszystkie pola dockind-a --%>

        <select class="multi_sel" size="5" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" multiple="multiple">
            <ww:set name="fieldCn" value="cn"/>
             <option value=""><ds:lang text="select.wybierz"/></option>
            <ww:iterator value="getAllEnumItemsByAspect(documentAspectCn,kind)">
                <option value="<ww:property value="id"/>" <ww:if test="values[#fieldCn] == id">selected="selected"</ww:if>>
                    <ww:property value="title"/>
                </option>
            </ww:iterator>
        </select>

        <br/>
        <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), true))"><ds:lang text="zaznacz"/></a>
        / <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), false))"><ds:lang text="odznacz"/></a>
        <ds:lang text="wszystkie"/>
    </ww:else>
</ww:elseif>
<ww:elseif test="type == 'enum-ref'">
    <ww:if test="#searchStyle == 'only-related'">
        <%-- PRZYPADEK: nie wszystkie pola wyszukiwania maj� by� widoczne --%>


        <input type="hidden" name="<ww:property value="#tagName"/>"
            id="<ww:property  value="#tagId"/>" value="<ww:property value="values[cn]"/>"/>

        <ww:if test="refStyle == 'all-visible'">
            <table class="line404"> <!-- debug class -->
                <ww:iterator value="enumItemsList">
                <tr>
                    <td><ww:property value="kind.getFieldByCn(fieldRefCn).getEnumItem(key).title"/></td>
                    <td>
                        <select class="sel" id="<ww:property value="#tagId+key"/>" onchange="update_<ww:property value="#tagId"/>(this);">
                        <option value=""><ds:lang text="select.wybierz"/></option>
                        <ww:iterator value="value">
                            <option value="<ww:property value="key"/>" <ww:if test="values[cn] == key">selected="selected"</ww:if>>
                                <ww:property value="value.title"/>
                            </option>
                        </ww:iterator>
                        </select>
                        <script type="text/javascript">
                                $j("#<ww:property value="#tagId+key"/>").get(0).cn = "<ww:property value="cn"/>";
                                $j("#<ww:property value="#tagId+key"/>").change(function(){
                                    var me = this;
                                    if (typeof additional_update_<ww:property value="#tagId"/> == 'function') {
                                        additional_update_<ww:property value="#tagId"/>(this);
                                    }
                                    $j("#dockind_<ww:property value="fieldRefCn"/>").get(0).value = <ww:property value="key"/>;
                                    $j("#<ww:property value="#tagId"/>").get(0).value = this.options[this.selectedIndex].value;
                                    $j(".<ww:property value="#tagId+'_clear'"/>").each(function(){if(this != me)this.selectedIndex = 0;});
                                });
                        </script>
                    </td>
                </tr>
                </ww:iterator>
            </table>

        </ww:if>
        <ww:else>    <%-- refStyle == 'chosen-visible' --%>
            <ww:iterator value="enumItemsList">
            <div id="<ww:property value="'div_'+#tagId+key"/>" class="if_<ww:property value="kind.mainFieldCn+'_'+key"/>" style="display:none">
                <select class="sel" id="<ww:property value="#tagId+key"/>" onchange="update_<ww:property value="#tagId"/>(this)">
                    <option value=""><ds:lang text="select.wybierz"/></option>
                    <ww:iterator value="value">
                        <option value="<ww:property value="key"/>" <ww:if test="values[cn] == key">selected="selected"</ww:if>>
                            <ww:property value="value.title"/>
                        </option>
                    </ww:iterator>
                </select><script type="text/javascript">
                    $j("#<ww:property value="#tagId+key"/>").get(0).cn = "<ww:property value="cn"/>"
                    $j("#<ww:property value="#tagId+key"/>").change(function(){
                        $j("#<ww:property value="#tagId"/>").get(0).value = this.options[this.selectedIndex].value;
                        if (typeof additional_update_<ww:property value="#tagId"/> == 'function') {
                            additional_update_<ww:property value="#tagId"/>(this);
                        }
                    });
                </script>
            </div>
            </ww:iterator>
        </ww:else>

        <script type="text/javascript">


            function update_<ww:property value="#tagId"/>(select)
            {	}

            function onchange_<ww:property value="#prefix+fieldRefCn"/>(select)
            {
            }

            function init_<ww:property value="#tagId"/>()
            {
               <%-- var div;
                <ww:if test="refStyle == 'chosen-visible'">
                    <ww:set name="firstKey" value="0"/>
                    <ww:iterator value="enumItemsList" status="enumStatus">
                        <ww:set name="refKey" value="((values[fieldRefCn] != null) && (values[fieldRefCn] != '')) ? values[fieldRefCn] : kind.getFieldByCn(fieldRefCn).enumItems[#firstKey].id"/>
                        <ww:property value="'/* '+#refKey+' '+key+' '+(#refKey == key)+' */'"/>
                        <ww:if test="#refKey.equals(key)">
                            div = document.getElementById('<ww:property value="'div_'+#tagId+key"/>');
                            div.style.display = "";
                        </ww:if>
                    </ww:iterator>
                </ww:if> --%>
            }

            init_<ww:property value="#tagId"/>();
        </script>


    </ww:if>
    <ww:else>
        <%-- PRZYPADEK: zawsze pokazujemy wszystkie pola dockind-a --%>
        <ww:set name="mainField" value="kind.getFieldByCn(fieldRefCn)"/>
        <ww:select name="#tagName" list="sortedEnumItems"
            listKey="id" listValue="title+(withRefCodes ? (' ['+#mainField.getEnumItem(tmp).getArg(1)+']') : '')" value="values[cn]"
            cssClass="'multi_sel dontFixWidth'" size="5" multiple="true" />
        <br/>
        <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), true))">zaznacz</a>
        / <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), false))">odznacz</a>
        wszystkie


    </ww:else>
</ww:elseif>
<ww:elseif test="type == 'bool' || type == 'document-out-zpo' || type == 'document-out-additional-zpo' || type == 'document-in-original' || type == 'document-in-submit-to-bip' ">

    <select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>">
        <option value=""><ds:lang text="select.wybierz"/></option>
        <option value="true"><ds:lang text="tak"/></option>
        <option value="false"><ds:lang text="nie"/></option>
    </select>
</ww:elseif>
<ww:elseif test="type == 'date' || type == 'document-out-zpo-date'  || type=='document-in-stamp-date'">

    <table class="leftAlign oneLine">
    <tbody class="leftAlignInner">
    <tr>
    <td><ds:lang text="od"/>:&nbsp;</td><td><input type="text" size="10" maxlength="10" class="txt"
        name="<ww:property value="#tagName"/>_from"
        id="<ww:property value="#tagId"/>_from"
        value="<ww:property value="values[cn+'_from']"/>" onchange="checkDate('<ww:property value="#tagId"/>_from','<ww:property value="#tagId"/>_to',1)"/>
    <img src="<ww:url value="'/calendar096/img.gif'"/>"
        id="datefield_trigger_<ww:property value="#tagId"/>_from" style="cursor: pointer; border: 1px solid red;"
        title="Date selector" onmouseover="this.style.background='red';"
        onmouseout="this.style.background=''"/></td>
    <td><ds:lang text="do"/>:&nbsp;</td><td><input type="text" size="10" maxlength="10" class="txt"
        name="<ww:property value="#tagName"/>_to"
        id="<ww:property value="#tagId"/>_to"
        value="<ww:property value="values[cn+'_to']"/>" onchange="checkDate('<ww:property value="#tagId"/>_from','<ww:property value="#tagId"/>_to',2)"/>
    <img src="<ww:url value="'/calendar096/img.gif'"/>"
        id="datefield_trigger_<ww:property value="#tagId"/>_to" style="cursor: pointer; border: 1px solid red;"
        title="Date selector" onmouseover="this.style.background='red';"
        onmouseout="this.style.background=''"/>
    <script>
        Calendar.setup({
            inputField     :    "<ww:property value="#tagId"/>_from",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "datefield_trigger_<ww:property value="#tagId"/>_from",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
        Calendar.setup({
            inputField     :    "<ww:property value="#tagId"/>_to",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "datefield_trigger_<ww:property value="#tagId"/>_to",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
    </script>
    </td>
    </tr>
    </tbody>
    </table>
</ww:elseif>
<ww:elseif test="type == 'dictionary' || type == 'document-field'">
    <table>
        <ww:set name="parentCn" value="cn"/>
        <ww:iterator value="dockindSearchFields">
            <ww:set name="tagName" value="'values.'+#parentCn+'_'+cn"/>
            <ww:set name="tagId" value="'dockind_'+#parentCn+'_'+cn"/>
            <tr>
                <td valign="top"><ww:property value="name"/>:</td>
                <td><ww:include page="/common/search-dockind-field.jsp"/></td>
            </tr>
        </ww:iterator>
    </table>
</ww:elseif>
<ww:elseif test="type == 'class'">
    <table class="leftAlign"> <!-- debug class -->
         <tr>
            <td colspan="2">
                <input type="hidden" name="ret_<ww:property value="#tagId"/>" id="ret_<ww:property value="#tagId"/>"/>
                <input type="hidden" name="<ww:property value="#tagName+'_attrName'"/>" id="<ww:property value="#tagId+'_attrName'"/>"/>
                <table class="line556"> <!-- debug inner class -->
                    <tbody id="desc_<ww:property value="#tagId"/>"  style="display:none;" >
                    <tr>
                        <td>
                            <select name="<ww:property value="#tagName"/>" class="sel" id="select_<ww:property value="#tagId"/>" multiple="true">
                            </select>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="2"><a href="javascript:void(removeOptions('select_<ww:property value="#tagId"/>'))"><ds:lang text="usuZaznaczone"/></a></td>
                     </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td id="<ww:property value="'td_area'+#tagId"/>"  style="display:none;">
               <textarea name="in<ww:property value="#tagName"/>" id="in<ww:property value="#tagId"/>"
                     cols="30" rows="3" class="txt"></textarea>
                <a href="javascript:void(changeDisplayClass('<ww:property value="#tagName"/>','<ww:property value="#tagId"/>','0'))">
                    <img id="img<ww:property value="#tagId"/>" src="<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif" width="11" height="11" border="0"/>
                </a>
            </td>
        </tr>
        <tbody id="<ww:property value="'class_'+#tagId"/>" class="leftAlignInner" >
        <ww:iterator value="dictionaryAttributes">
            <tr>
                <td id="td_name_<ww:property value="#tagId+key"/>"><ww:property value="value"/></td>
                <td id="td_value_<ww:property value="#tagId+key"/>">
                    <ww:if test="key == 'setOfDocuments'">
                        <input id="<ww:property value="#tagId+key"/>" type="checkbox" name="<ww:property value="#tagName+'_'+key"/>" <ww:if test="values[cn+'_'+key] == 'on'">checked="checked"</ww:if> />
                    </ww:if>
                    <ww:else>
                        <input type="text" id="<ww:property value="#tagId+key"/>"
                            name="<ww:property value="#tagName+'_'+key"/>" value="<ww:property value="values[cn+'_'+key]"/>"
                            size="50" class="txt" onchange="clear_<ww:property value="cn"/>(true)"/>
                    </ww:else>
                    <a href="javascript:void(changeDisplayClass('<ww:property value="#tagName"/>','<ww:property value="#tagId"/>','<ww:property value="key"/>'))">
                        <img id="img<ww:property value="#tagId"/>" src="<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif" width="11" height="11" border="0"/>
                    </a>
                </td>
            </tr>
        </ww:iterator>
            <tr>
                <td colspan="2" class="alignLeftBtns">
                    <input id="btn_wybierz_<ww:property value="#tagId"/>" type="button" value="<ds:lang text="wybierz"/>" onclick="openDictionary_<ww:property value="cn"/>()" class="btn" />
                    <input id="btn_wyczysc_<ww:property value="#tagId"/>" type="button" value="<ds:lang text="wyczysc"/>" onclick="clear_<ww:property value="cn"/>(false)" class="btn" />
                    <input id="btn_dodaj_<ww:property value="#tagId"/>" type="button" value="<ds:lang text="dodajKolejny"/>" onclick="add_next_<ww:property value="cn"/>()" class="btn" style="display:none;"/>
                </td>
            </tr>
        </tbody>
    </table>

    <script type="text/javascript">
   

        function openDictionary_<ww:property value="cn"/>()
        {
            var addId;
            var selObj = E('select_<ww:property value="#tagId"/>');
            if(selObj.length > 0)
            {
                addId = '&id=' + selObj.options[0].value;
            }
            else
            {
                addId = '';
            }
            openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'
                            +addId
                           <ww:iterator value="dictionaryAttributes">
                                +'&<ww:property value="key"/>='+document.getElementById('<ww:property value="#tagId+key"/>').value
                           </ww:iterator>
                           ,'<ww:property value="cn"/>',
                             <ww:if test="width > 0"><ww:property value="width"/>,<ww:property value="height"/></ww:if>
                               <ww:else>700, 650</ww:else>);
                           isAddNext = 0;
        }
        var isAddNext = 0;
        function add_next_<ww:property value="cn"/>()
        {
            openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>');
                           isAddNext = 1;
        }

        <ww:iterator value="dictionaryAttributes">
            function get<ww:property value="#tagId+key"/>()
            {
                return document.getElementById('<ww:property value="#tagId+key"/>').value;
            }
        </ww:iterator>

        function clear_<ww:property value="cn"/>(onlyId)
        {
            var sel = E('select_<ww:property value="#tagId"/>');
            var j  = sel.length;
            for (var i=0; i < j; i++)
            {
                sel.options[i] = null;
            }

            if (!onlyId)
            {
            <ww:iterator value="dictionaryAttributes">
                document.getElementById('<ww:property value="#tagId+key"/>').value = '';
            </ww:iterator>
            }
             document.getElementById('btn_dodaj_<ww:property value="#tagId"/>').style.display = 'none';
        }
       
        function accept_<ww:property value="cn"/>(map)
        {
            var description = '';
            <ww:iterator value="dictionaryAttributes">
                <ww:if test="key == 'setOfDocuments'">

                    if (map.setOfDocuments)
                        document.getElementById('<ww:property value="#tagId+key"/>').previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
                    else
                        document.getElementById('<ww:property value="#tagId+key"/>').previousSibling.style.backgroundPosition = "0 0";

                    document.getElementById('<ww:property value="#tagId+key"/>').checked = map.<ww:property value="key"/>;
                </ww:if>
                <ww:else>
                    document.getElementById('<ww:property value="#tagId+key"/>').value = map.<ww:property value="key"/>;
                    try
                    {
                        document.getElementById('<ww:property value="#tagId+key"/>').focus();
                    }
                    catch (e)
                    {}
                    description = description + map.<ww:property value="key"/>+ ' | ';
                </ww:else>

            </ww:iterator>
            document.getElementById('btn_dodaj_<ww:property value="#tagId"/>').style.display = '';

            var sel = E('select_<ww:property value="#tagId"/>');
           
            

            if(isAddNext < 1)
            {
                if(sel.length > 0)
                    sel.options[sel.options.length-1] = new Option(description, map.id);
                else
                    sel.options[sel.options.length] = new Option(description, map.id);

            }
            else
            {
                sel.options[sel.options.length] = new Option(description, map.id);
                <ww:iterator value="dictionaryAttributes">
                    document.getElementById('td_value_<ww:property value="#tagId+key"/>').style.display = 'none';
                    document.getElementById('td_name_<ww:property value="#tagId+key"/>').style.display = 'none';
                </ww:iterator>
                document.getElementById('btn_wybierz_<ww:property value="#tagId"/>').style.display = 'none';
                document.getElementById('btn_wyczysc_<ww:property value="#tagId"/>').style.display = 'none';
                document.getElementById('btn_dodaj_<ww:property value="#tagId"/>').style.display = '';
                document.getElementById('desc_<ww:property value="#tagId"/>').style.display = '';
            }
            selectAllOptions('select_<ww:property value="#tagId"/>');
        }

        functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
    </script>
</ww:elseif>
<ww:elseif test="type == 'document-date'">
    <table class="leftAlign oneLine">
    <tbody class="leftAlignInner">
    <tr>
    <td><ds:lang text="od"/>:&nbsp;</td><td><input type="text" size="10" maxlength="10" class="txt"
        name="documentDateFrom"
        id="<ww:property value="#tagId"/>_from"
        value="<ww:property value="values[cn+'_from']"/>" onchange="checkDate('<ww:property value="#tagId"/>_from','<ww:property value="#tagId"/>_to',1)"/>
    <img src="<ww:url value="'/calendar096/img.gif'"/>"
        id="datefield_trigger_<ww:property value="#tagId"/>_from" style="cursor: pointer; border: 1px solid red;"
        title="Date selector" onmouseover="this.style.background='red';"
        onmouseout="this.style.background=''"/></td>
    <td><ds:lang text="do"/>:&nbsp;</td><td><input type="text" size="10" maxlength="10" class="txt"
        name="documentDateTo"
        id="<ww:property value="#tagId"/>_to"
        value="<ww:property value="values[cn+'_to']"/>" onchange="checkDate('<ww:property value="#tagId"/>_from','<ww:property value="#tagId"/>_to',2)"/>
    <img src="<ww:url value="'/calendar096/img.gif'"/>"
        id="datefield_trigger_<ww:property value="#tagId"/>_to" style="cursor: pointer; border: 1px solid red;"
        title="Date selector" onmouseover="this.style.background='red';"
        onmouseout="this.style.background=''"/>
    <script>
        Calendar.setup({
            inputField     :    "<ww:property value="#tagId"/>_from",
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",
            button         :    "datefield_trigger_<ww:property value="#tagId"/>_from",
            align          :    "Tl",
            singleClick    :    true
        });
        Calendar.setup({
            inputField     :    "<ww:property value="#tagId"/>_to",
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",
            button         :    "datefield_trigger_<ww:property value="#tagId"/>_to",
            align          :    "Tl",
            singleClick    :    true
        });
    </script>
    </td>
    </tr>
    </tbody>
    </table>
</ww:elseif>
<ww:elseif test="type == 'document-description'">
    <ww:textfield name="'description'"/>
</ww:elseif>
<ww:elseif test="type == 'document-abstract-description'">
    <ww:textfield name="'abstractDescription'"/>
</ww:elseif>
<ww:elseif test="type == 'document-officenumber'"/>
<ww:elseif test="type == 'document-journal'">
<ww:select name="'journalId'" list="journals" listKey="id" listValue="description" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
</ww:elseif>
<ww:elseif test="type == 'document-postal-reg-nr'">
    <ww:textfield name="'postalRegNumber'"/>
</ww:elseif>
<ww:elseif test="type == 'document-person'">
    <table>
        <ww:if test="senderField">
            <tr><td><ds:lang text="Imie"/></td><td><ww:textfield name="'senderFirstame'"/></td></tr>
            <tr><td><ds:lang text="Nazwisko"/></td><td><ww:textfield name="'senderLastname'"/></td></tr>
            <tr><td><ds:lang text="Firma/Urz�d"/></td><td><ww:textfield name="'senderOrganization'"/></td></tr>
			<tr><td><ds:lang text="Kod Pocztowy"/></td><td><ww:textfield name="'senderZip'"/></td></tr>
			<tr><td><ds:lang text="Miejscowo��"/></td><td><ww:textfield name="'senderLocation'"/></td></tr>
			<tr><td><ds:lang text="NIP"/></td><td><ww:textfield name="'senderNip'"/></td></tr>
        </ww:if>
        <ww:else>
            <tr><td><ds:lang text="Imie"/></td><td><ww:textfield name="'recipientFirstname'"/></td></tr>
            <tr><td><ds:lang text="Nazwisko"/></td><td><ww:textfield name="'recipientLastname'"/></td></tr>
            <tr><td><ds:lang text="Firma/Urz�d"/></td><td><ww:textfield name="'recipientOrganization'"/></td></tr>
			<tr><td><ds:lang text="Kod Pocztowy"/></td><td><ww:textfield name="'recipientZip'"/></td></tr>
			<tr><td><ds:lang text="Miejscowo��"/></td><td><ww:textfield name="'recipientLocation'"/></td></tr>
			<ds:available test="addStreetToRecipientSearch">
				<tr><td><ds:lang text="Ulica"/></td><td><ww:textfield name="'recipientStreet'"/></td></tr>
			</ds:available>
			<tr><td><ds:lang text="NIP"/></td><td><ww:textfield name="'recipientNip'"/></td></tr>
        </ww:else>
        
<%-- 		<input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" > --%>
        
    </table>
</ww:elseif>
<ww:elseif test="type == 'document-user-division-second'">
    <table>
    <ww:if test="senderField">
        <tr><td><ds:lang text="Imie"/></td><td><ww:textfield name="'senderFirstame'"/></td></tr>
        <tr><td><ds:lang text="Nazwisko"/></td><td><ww:textfield name="'senderLastname'"/></td></tr>
        <tr><td><ds:lang text="Dzial"/></td><td><ww:textfield name="'senderOrganizationDivision'"/></td></tr>
    </ww:if>
    <ww:else>
        <tr><td><ds:lang text="Imie"/></td><td><ww:textfield name="'recipientFirstname'"/></td></tr>
        <tr><td><ds:lang text="Nazwisko"/></td><td><ww:textfield name="'recipientLastname'"/></td></tr>
        <tr><td><ds:lang text="Dzial"/></td><td><ww:textfield name="'recipientOrganizationDivision'"/></td></tr>
    </ww:else>
    </table>
</ww:elseif>
<ww:elseif test="type == 'dockindButton' || type == 'doclist'"><%//Nie pokazujemy%></ww:elseif>
   <ww:elseif test="type == 'html' || type == 'document-field'">

    </ww:elseif>
<ww:else>
    <!-- <p>TYPE_CN = <ww:property value="type"/></p> -->
    <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
        value="<ww:property value="values[cn]"/>" size="30" class="txt" />
</ww:else>

 <!--  <script type="text/javascript">
 function openPersonPopup(lparam, dictionaryType)
    {
       
        openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType+'&canAddAndSubmit=true','person', 700,650);
    }
 function __accept_person(map, lparam, wparam)
	{
		//alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);
		var txt;
		if (lparam == 'recipient')
		{
			txt = document.getElementById('recipient');
		}
		else if (lparam == 'sender')
		{
			txt = document.getElementById('sender');
		}
	//	alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam, +"sdfsdf"+txt) ;
		if (!isEmpty(map.lastname))
			txt.value += map.lastname + " ";
		if (!isEmpty(map.organization))
			txt.value += map.organization + " ";
		if (!isEmpty(map.firstname))
			txt.value += map.firstname + " ";
		if (!isEmpty(map.street))
			txt.value += map.street + " ";
		if (!isEmpty(map.zip))
			txt.value += map.zip
	}
    </script> -->