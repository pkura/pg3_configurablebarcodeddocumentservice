<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!--N flags-present.jsp N-->

<table class="tableMargin">
	<tr>
		<td class="alignTop" style="padding-right: 20px;"><span class="bold" style="padding-left: 5px;"><ds:lang
			text="FlagiWspolne" />:</span>
		<ul class="clearList noMarginPadding listTofloatingMenu"
			id="FlagiWspolneList">
			<ww:iterator value="globalFlags">
				<ww:if test="canView">
					<li>
					<div class="inline_flags<ww:if test="(value and !canClear) or !canEdit"> inline_flags_disabled</ww:if>"><ww:checkbox name="'globalFlag'"
						fieldValue="id" value="value"
						disabled="(value and !canClear) or !canEdit" cssClass="'hidden'" />
					<img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden"
						id="plus" /> <img src="<ww:url value="'/img/list-minus.gif'"/>"
						class="hidden" id="minus" /> <b><ww:property value="c" /></b> <ww:property
						value="description" /></div>
					</li>
				</ww:if>
			</ww:iterator>
			<li class="hidden" id="FlagiWspolneNone"><span class="italic italicFlags"><nobr><ds:lang
				text="NieWybrano" /></nobr></span></li>
		</ul>
		
		<!-- poprawa wyswietlania w ie6, bo selecty zakrywaja flagi */ -->
		<!-- <div class="select-free" id="dd3">  -->
			<div class="floatingDiv hidden" id="FlagiWspolneDiv">
			<ul class="clearList noMarginPadding floatingMenu"
				id="FlagiWspolneDivList">
			</ul>
			<ul class="clearList noMarginPadding">
				<li class="hidden" id="FlagiWspolneAll"><span
					class="italic continuousElement"><ds:lang
					text="WybranoWszystkie" /></span></li>
			</ul>
			
			</div>
			
		
		</td>
		<td class="alignTop"><span class="bold"><ds:lang
			text="FlagiUzytkownika" />:</span>
		<ul class="clearList noMarginPadding listTofloatingMenu"
			id="FlagiUzytkownikaList">
			<ww:iterator value="userFlags">
				<li>
				<div class="inline_flags"><ww:checkbox name="'userFlag'"
					fieldValue="id" value="value"
					disabled="(value and !canClear) or !canEdit" cssClass="'hidden'" />
				<img src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden"
					id="plus" /> <img src="<ww:url value="'/img/list-minus.gif'"/>"
					class="hidden" id="minus" /> <b><ww:property value="c" /></b> <ww:property
					value="description" /></div>
				</li>
			</ww:iterator>
			<li class="hidden" id="FlagiUzytkownikaNone"><span
				class="italic italicFlags"><nobr><ds:lang text="NieWybrano" /></nobr></span></li>
		</ul>
		<div class="floatingDiv hidden" id="FlagiUzytkownikaDiv">
		<ul class="clearList noMarginPadding floatingMenu"
			id="FlagiUzytkownikaDivList">
		</ul>
		<ul class="clearList noMarginPadding">
			<li class="hidden" id="FlagiUzytkownikaAll"><span
				class="italic continuousElement"><ds:lang
				text="WybranoWszystkie" /></span></li>
		</ul>
		</div>
		</td>
		<ds:available test="labels">
			<td class="alignTop"><span class="bold"><ds:lang
				text="EtykietyModyfikowalne" />:</span>
			<ul class="clearList noMarginPadding listTofloatingMenu"
				id="EtykietyModyfikowalneList">
				<ww:iterator value="nonModifiableLabels">
					<li><b><ww:property value="name" /></b> <ww:property
						value="description" /></li>
				</ww:iterator>

				<ww:iterator value="hackedLabels">
					<li>
					<div class="inline_flags"><ww:checkbox name="'label'"
						value="second" fieldValue="first.id" cssClass="'hidden'" /> <img
						src="<ww:url value="'/img/list-plus.gif'"/>" class="hidden"
						id="plus" /> <img src="<ww:url value="'/img/list-minus.gif'"/>"
						class="hidden" id="minus" /> <b><ww:property
						value="first.name" /></b> <ww:property value="first.description" /></div>
					</li>
				</ww:iterator>
				<li class="hidden" id="EtykietyModyfikowalneNone"><span
					class="italic"><nobr><ds:lang text="NieWybrano" /></nobr></span></li>
			</ul>
			<div class="floatingDiv hidden" id="EtykietyModyfikowalneDiv">
			<ul class="clearList noMarginPadding floatingMenu"
				id="EtykietyModyfikowalneDivList">
			</ul>
			<ul class="clearList noMarginPadding">
				<li class="hidden" id="EtykietyModyfikowalneAll"><span
					class="italic continuousElement"><ds:lang
					text="WybranoWszystkie" /></span></li>
			</ul>
			</div>
			</td>
		</ds:available>
	</tr>
</table>
<script type="text/javascript">
		$j(document).ready(function ()
		{
			prepareList($j("#FlagiWspolneList"), $j("#FlagiWspolneNone"), $j("#FlagiWspolneAll"),$j("#FlagiWspolneDiv"), $j("#FlagiWspolneDivList"), FlagiWspolneStat);
			prepareList($j("#FlagiUzytkownikaList"), $j("#FlagiUzytkownikaNone"), $j("#FlagiUzytkownikaAll"),$j("#FlagiUzytkownikaDiv"), $j("#FlagiUzytkownikaDivList"), FlagiUzytkownikaStat);
			prepareList($j("#EtykietyModyfikowalneList"), $j("#EtykietyModyfikowalneNone"), $j("#EtykietyModyfikowalneAll"),$j("#EtykietyModyfikowalneDiv"), $j("#EtykietyModyfikowalneDivList"), EtykietyModyfikowalneStat);
			
			if ($j.browser.msie)
			{											
				$j("#FlagiUzytkownikaList").find("li").css("padding", "3px 0px");
				$j("#FlagiWspolneList").find("li").css("padding", "3px 0px");
				$j("#EtykietyModyfikowalneList").find("li").css("padding", "3px 0px");
			}
			
			<ds:additions test="tcLayout">
				//rozmiar();
			</ds:additions>
		});
		
		var FlagiWspolneStat = 0;
		var FlagiUzytkownikaStat = 0;
		var EtykietyModyfikowalneStat = 0;
		
		function checkAllNone (list_jq, none_jq, all_jq)
		{
			list_jq.find("li").filter(":has(input:checked)").length ? none_jq.addClass('hidden') : none_jq.removeClass('hidden');
			list_jq.find("li").filter(":has(input:not(:checked))").length ? all_jq.addClass('hidden') : all_jq.removeClass('hidden');
		}
		
		function hoverFix(list_jq, classa)
		{
			if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7))
			{
				list_jq.find("li").hover(
					function() {
						$j(this).addClass("" + classa);
					}
					,
					function() {
						$j(this).removeClass("" + classa);
					}
				);
			}
		}
		
		function createList(list_jq, div_jq, divList_jq, none_jq, all_jq)
		{
			divList_jq.find("li").remove();
			
			list_jq.find("li").filter(":has(input:not(:checked))").clone().appendTo(divList_jq).removeClass("hidden").removeClass("listTofloatingMenuHover") //IE 6 fix
				.find("div > #plus").removeClass("hidden")
				.end().find("div > #minus").addClass("hidden")
				.end().filter(":has(input:disabled)").find("div > #plus").addClass("hidden")
				.end().find("div > b").css("margin-left", "15px");
			
			hoverFix(divList_jq, "floatingMenuHover");
			
			$j.browser.msie ? divList_jq.find("li > div > input:checked").removeAttr("checked") : true ;
			
			var position = list_jq.position();
			
			position['top'] -= Math.round(div_jq.height() / 2);
			position['left'] += Math.round(list_jq.width() / 2);
			
			if ($j.browser.msie)
			{
				//position['top'] += $j(window).scrollTop();
				position['left'] += $j(window).scrollLeft();
			}
		
			//d�
			(position['top'] + div_jq.height()) > ($j(window).scrollTop() + $j(window).height()) ? (position['top'] = $j(window).scrollTop() + $j(window).height() - div_jq.height()) : true ;						
			//g�ra
			position['top'] < $j(window).scrollTop() ? position['top'] = $j(window).scrollTop() : true ;
			//prawo
			(position['left'] + div_jq.width()) > ($j(window).scrollLeft() + $j(window).width()) ? (position['left'] = $j(window).scrollLeft() + $j(window).width() - div_jq.width()) : true ;
			//lewo
			position['left'] < $j(window).scrollLeft() ? position['left'] = $j(window).scrollLeft() : true ;
		
			div_jq.css({top: position['top'], left: position['left']}).removeClass("hidden");
			divList_jq.find("li").click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);});
		}
		
		function updateList(this_jq, list_jq)
		{
			var vall = this_jq.find("div > input:checkbox").attr("value");
			
			list_jq.find("li").filter(":has(input:checkbox[value='" + vall + "'])").removeClass("hidden")
				.find("div > input").attr("checked", "checked")
				.end().find("div > #minus").removeClass("hidden")
				.end().find("div > #plus").addClass("hidden");
			this_jq.remove();
			<ds:additions test="tcLayout">
				//rozmiar();
			</ds:additions>
		}
		
		function deleteList(div_jq, divList_jq)
		{
			div_jq.addClass("hidden");
			divList_jq.find("li").remove();
		}
		
		function clickEvent(this_jq, list_jq, div_jq, divList_jq, none_jq, all_jq) //nadawany <li>
		{
			if (this_jq.find("div > input").length)
			{
				if (this_jq.find("div > input:disabled").length)
				{//nie mam praw edycji - nie robie nic
					//alert("jeste� GUPI!!");
				}
				else if (this_jq.find("div > input:checked").length)
				{//zaznaczony - przenosze go na plywajaca liste
					this_jq.addClass("hidden")
						.find("div > input").removeAttr("checked")
						.end().find("div > #minus, div > #plus").addClass("hidden");
					checkAllNone (list_jq, none_jq, all_jq);
					createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
				}
				else
				{//niezaznaczony - przywracam do na glownej liscie
					this_jq.addClass("hidden");
					updateList(this_jq, list_jq);
					checkAllNone (list_jq, none_jq, all_jq);
				}
			}

			/* je�li jeste�my we viewserverze, to pr�bujemy przeskalowa� obrazek */
			try {
				windowResize();
			} catch(e) {}
		}
		
		function prepareList (list_jq, none_jq, all_jq, div_jq, divList_jq, stat)
		{
			hoverFix(list_jq, "listTofloatingMenuHover");
			list_jq.find("li")
				.click(function() {clickEvent($j(this), list_jq, div_jq, divList_jq, none_jq, all_jq);})
				.filter(":has(input:not(:checked))").addClass('hidden')
				.end().filter(":has(input:checked)").find("div > #minus").removeClass("hidden")
				//tu ju� leci do disabled
				.end().filter(":has(input:disabled)").find("div > #minus").addClass("hidden")
				.end().find("div > b").css("margin-left", "15px");
			checkAllNone (list_jq, none_jq, all_jq);
			
			list_jq.hover(
				function() {
					if (stat == 0)
					{
						createList(list_jq, div_jq, divList_jq, none_jq, all_jq);
						stat = 1;

						if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
							div_jq.children("iframe").width(div_jq.width()+50);
							div_jq.children("iframe").height(div_jq.height()+50);
							div_jq.css('filter', 'none');
						}
					}
				}
				,
				function() {
					setTimeout(function() {												
						if (stat == 1)
							{
								deleteList(div_jq, divList_jq);
								stat = 0;
							}
						}, 100
					);
				}
			);
			
			div_jq.hover(
				function() {
					stat = 2;
				}
				,
				function() {
					deleteList(div_jq, divList_jq);
					stat = 0;
				}
			);

			/* obejscie dla ie6 */
			if ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) < 7)) {
				div_jq.append('<iframe class="iframeMask"></iframe>');
				div_jq.css('border', 'none');
			}
		}
	</script>
<!--N koniec flags-present.jsp N-->