<!--N acceptances.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>

<ww:if test="fm.acceptancesDefinition != null">
<tr>
    <td>        
        
        <ww:if test="fm.documentKind.properties['remark-field-button'] == 'true'">
             <ww:if test="(generalAcceptancesToGo != null && generalAcceptancesToGo.size() > 0) || canRemarkDocument == true">
                <input class="btn" id="remarkButton" type="button" value="<ds:lang text="OdrzucDokument" />" onclick="force_remark_on_button()"/>
                <script type="text/javascript">
                    remarkBtnPressed = false;

                    function force_remark_on_button()
                    {
                        if(!remarkBtnPressed)
                        {
                            $j('#checkRemark').val(true);
                            $j('#remarkDiv').css('display','');
                            $j('#remarkButton').val('<ds:lang text="CofnijOdrzucenie"/>');
                            remarkBtnPressed = true;
                        }
                        else
                        {
                            $j('#checkRemark').val(false);
                            $j('#remarkDiv').css('display','none');
                            $j('#remarkButton').val('<ds:lang text="OdrzucDokument"/>');
                            remarkBtnPressed = false;
                        }
                    }
                </script>
            </ww:if>
        </ww:if>
    </td>
</tr>
<tr>
    <td colspan="2">
        <input type="hidden" name="doGiveAcceptance" id="doGiveAcceptance"/>        
        <input type="hidden" name="doSendToDecretation" id="doSendToDecretation"/>        
        <input type="hidden" name="doWithdrawAcceptance" id="doWithdrawAcceptance"/>
        <input type="hidden" name="acceptanceCn" id="acceptanceCn"/>
        <input type="hidden" name="objectId" id="objectId"/>
        
        <ds:available test="acceptances.titles"  documentKindCn="documentKindCn">
        <h4><ds:lang text="akceptacjeCentrumKosztowego"/></h4>                   
        </ds:available>
        
        <ww:set name="numberRows" value="0"/>
		<ww:set name="centrumCn" value="fm.acceptancesDefinition.centrumKosztFieldCn"/>
        <ww:if test="fm.getValue(#centrumCn) != null && fm.getValue(#centrumCn).size() > 0">
            <table>
                <tr>
					<td>MPK, konto ksiegowe, kwota</td>                                    
                    <ww:iterator value="fm.acceptancesDefinition.fieldAcceptances">
                        <td align="center"><ww:property value="value.name"/></td>
                    </ww:iterator>
                    <td colspan="2"></td>
                </tr>
                <ww:iterator value="fm.getValue(#centrumCn)" status="status">
                <ww:set name="result"/>
                
                <tr id="<ww:property value="'acceptanceTr_'+#centrumCn+#status.index"/>">
                    <td>
                        <textarea cols="50" rows="3" class="txt" readonly="readonly" id="<ww:property value="'centrum_'+#centrumCn+#status.index"/>"><ww:property value="#result.textdesc"/></textarea>
                    </td>
                    <ww:iterator value="fm.acceptancesDefinition.fieldAcceptances">
                        <td>
                            <ww:set name="acceptance" value="fm.acceptancesState.getFieldAcceptance(key,#result.id)"/>
                            <ww:if test="#acceptance != null">
                                <ww:property value="#acceptance.descriptionWithoutCn"/>
                            </ww:if>
                        </td>
                    </ww:iterator>
                    <td> <ww:set name="numberRows" value="#numberRows + 1"/>
                        <select class="sel" id="<ww:property value="'centrumAcceptance_'+#status.index"/>" name="centrumAcceptanceCn[<ww:property value="#result.id"/>]">
                            <option value="">-- wybierz akceptacj� --</option>
                            <ww:iterator value="fm.acceptancesState.getFieldAcceptancesDef(#centrumCn,#result.id)">
                                <option value="<ww:property value="key"/>" <ww:if test="key == 'szefa_centrum'">selected="selected"</ww:if> > <%-- domy�lne ustawianie na 'szefa-centrum' ze wzgl�du na Aegon --%>
                                    <ww:property value="value.name"/>
                                </option>
                            </ww:iterator>
                        </select>
                    </td>
                    <td>
                        <ww:if test="fm.acceptancesState.isCentrumAccepted(#result.id)">
                            <%-- aktualny uzytkownik dokonal juz akceptacji dla tego centrum --%>
                            <input type="button" value=<ds:lang text="cofnijAkceptacje"/> class="btn" onclick="withdrawCentrumAcceptance('<ww:property value="#result.id"/>')"/>               
                        </ww:if>                       
                    </td>                
                </tr>
                </ww:iterator>
                <tr>
                    <td>
                    </td>
                    <ww:iterator value="fm.acceptancesDefinition.fieldAcceptances">
                        <td>                    
                        </td>
                    </ww:iterator>
                    <td>
                        <input type="button" value="Daj akceptacj�" class="btn" onclick="isChosen() && giveCentrumAcceptance()"/>
                    </td>
                    <td></td>
                </tr>                
            <ww:if test="(fm.acceptancesDefinition.finalAcceptance != null) && (fm.acceptancesDefinition.finalAcceptance.fieldCn != null)">
	            <ww:set name="finalFieldCn" value="fm.acceptancesDefinition.finalAcceptance.fieldCn"/>
		            <!-- (dockindAction == 'summary') &&  -->
		            <ww:if test="('invoice' == fm.documentKind.cn || 'invoice_ic' == fm.documentKind.cn || 'invoice-pte' == fm.documentKind.cn ) && !(fm.getKey(#finalFieldCn)) && activity != null">
			            <tr><td></td><td></td>
			            <td colspan="2">
			            	<input type="button" value="<ds:lang text="DekretujDoDalszychAkceptacji"/>" class="btn" onclick="sendToDecretation()" 
			            			<ww:if test="sendDecretation == false">disabled="disabled"</ww:if> />
			            </td>
			            </tr>
		            </ww:if>
            </ww:if>
            </table>            
        </ww:if>

        <%-- 
             specyficzne pole dla "invoice" - w przysz�o�ci przerzuci� to jako property w XMLu 
             okre�laj�ce, kt�re pola wyswietla� na podsumowaniu razem z akceptacjami
        --%>
        <ww:if test="(dockindAction == 'summary') && ('invoice' == fm.documentKind.cn || 'invoice-pte' == fm.documentKind.cn)">
        <table>
            <tr>
                <td><b><ww:property value="fm.getField('OPIS_TOWARU').name"/>:</b></td>
                <td><ww:property value="fm.getDescription('OPIS_TOWARU')"/></td>
            </tr>
        </table>
        </ww:if>
		
		<ds:available test="acceptances.titles"  documentKindCn="documentKindCn">
        <h4><ds:lang text="akceptacjeOgolne"/></h4>
        </ds:available>
        <table>
            <tr>
                <td>
                    <select class="sel dontFixWidht" multiple="multiple" size="3">
                        <ww:iterator value="fm.acceptancesState.generalAcceptances">
                            <option value="<ww:property value="id"/>">
                                <ww:property value="descriptionWithoutCn"/> (<ww:property value="fm.documentKind.getAcceptancesDefinition().getAcceptanceName(acceptanceCn)"/>)
                            </option>
                        </ww:iterator>
                    </select>
                </td>
            </tr>            
            <ww:if test="fm.acceptancesState.givenGeneralAcceptancesDef != null && fm.acceptancesState.givenGeneralAcceptancesDef.size() > 0">
                <tr>
                    <td>
                        <select class="sel" id="givenAcceptances">
                        <ww:iterator value="fm.acceptancesState.givenGeneralAcceptancesDef">
                            <option value="<ww:property value="cn"/>">
                                <ww:property value="name"/>
                            </option>
                        </ww:iterator>
                        </select>
                        <input type="button" value=<ds:lang text="cofnijAkceptacje"/> class="btn" onclick="withdrawAcceptance()"/>                     
                    </td>                                
                </tr>    
            </ww:if>
            
             <ww:if test="generalAcceptancesToGo != null && generalAcceptancesToGo.size() > 0">
            	<tr>                
                    <td>
                        <select class="sel" id="acceptances">
                        <ww:iterator value="generalAcceptancesToGo">
                            <option value="<ww:property value="key"/>">
                                <ww:property value="value.name"/>
                            </option>
                        </ww:iterator>
                        </select>
                        <input type="button" value="daj akceptacj�" class="btn" onclick="testKsiegowa() && giveAcceptance()"/>
                    </td>                                
                </tr>
            </ww:if> 
            
            <ww:if test="(fm.acceptancesDefinition.finalAcceptance != null) && (fm.acceptancesDefinition.finalAcceptance.fieldCn != null)">
	            <ww:set name="finalFieldCn" value="fm.acceptancesDefinition.finalAcceptance.fieldCn"/>
		            <!-- (dockindAction == 'summary') &&  -->
		            <ww:if test="(!(fm.getKey(#finalFieldCn)) && activity != null) || (acceptancesMap.size() > 0)">
			            <tr>
			            <td colspan="2">
			            	<ww:if test="acceptancesMap.size() > 0 && 
                                               ('absence-request' == fm.documentKind.cn || 'overtime_app' == fm.documentKind.cn || 'overtime_receive' == fm.documentKind.cn || 'wf' == fm.documentKind.cn)">
				            	<input type="button" value="<ds:lang text="Dekretuj do akceptacji"/>" class="btn" onclick="sendToDecretation()"/>
				            	<ww:select name="'userName'" list="acceptancesMap" listKey="key" listValue="value" cssClass="'sel'" />
			            	</ww:if>
			            	<ww:else>
			            		<input type="button" value="<ds:lang text="Dekretuj do akceptacji"/>" class="btn" onclick="sendToDecretation()" 
			            			   <ww:if test="sendDecretation == false">disabled="disabled"</ww:if>/>
			            	</ww:else>
			            </td>
			            </tr>
		            </ww:if>
            </ww:if>
            </table>
            
            <script type="text/javascript" charset="iso-8859-2">
            
            
            function testKsiegowa()
            {
            	//przy akceptacji ksiegowej: konto kosztowe, mpk, opis towaru
            	var test = new Boolean();
            	var text = new String();
            	text = "Brak:";
            	test = true;
            	//alert(document.getElementById('centrum_CENTRUM_KOSZTOWE'+0));
            	if(document.getElementById('acceptances').value == 'ksiegowa') //tylko dla akceptacji ksiegowej
            	{
            		for (i=0;i<=<ww:property value="#numberRows"/>-1;i++) //konto
            		{
	            		if((document.getElementById('centrum_CENTRUM_KOSZTOWE'+i) != null) && (document.getElementById('centrum_CENTRUM_KOSZTOWE'+i).value.match("brak konta")))
	            		{
	            			text += "\n konta ksiegowego dla: "+document.getElementById('centrum_CENTRUM_KOSZTOWE'+i).value;
	            			test = false;
	            		}
            		}
            		if(document.getElementById('dockind_OPIS_TOWARU') != null) {
	            		if(document.getElementById('dockind_OPIS_TOWARU').value == "") //opis towaru
		            	{
		            		text += "\n opisu towaru";
		            		test = false;
		            	}
	            	}
	            	if(document.getElementById('remainingAmount') != null) {
		            	if(document.getElementById('remainingAmount').value != 0) //mpk
		            	{
		            		text += "\n przydzielenia calej kwoty";
		            		test = false;
		            	}
	            	}
	            	if(document.getElementById('dockind_POTWIERDZENIE_ZGODNOSCI').value == '' || document.getElementById('dockind_POTWIERDZENIE_ZGODNOSCI').value == null)
	        		{
	            		text +="\n potwierdzenia zgodnosci";
	            		test = false;
	        		}
	            	if(!test)
	            	{
	            		alert(text);
	            	}
	            }
            	return test;
            }    
                
                        
        	function isChosen() //sprawdza opis towaru, czy wybrano rodzaj akceptacji
        	{
        		var test = new Boolean();
        		var testCentrum = new Boolean();
        		var text = new String();
        		test = true;
        		testCentrum = false;
        		text = "";
        		for (i=0;i<=<ww:property value="#numberRows"/>-1;i++)
        		{
        			if(document.getElementById('centrumAcceptance_'+i).value != "")
        			{	        				
        				testCentrum = true;
        			}
        		}
        		if(document.getElementById('dockind_OPIS_TOWARU') != null) {
        		if(document.getElementById('dockind_OPIS_TOWARU').value == "") //opis towaru
        		{
        			text += "\n Brak opisu towaru";
        			test = false;
        		}
        		}
        		if(!testCentrum)
        		{
        			text += "\n Brak uprawnie� do akceptacji tego MPK lub nie wybrano �adnego rodzaju akceptacji";
        			test = false;
        		}
        		if(document.getElementById('dockind_POTWIERDZENIE_ZGODNOSCI').value == '' || document.getElementById('dockind_POTWIERDZENIE_ZGODNOSCI').value == null)
        		{
            		text +="\n Brak potwierdzenia zgodnosci";
            		test = false;
        		}
        		if(!test)
        		{
            		alert(text);
        		}
        		return test;
        	}

            function sendToDecretation()
            {
            	document.getElementById('doSendToDecretation').value = 'true';
            	document.forms[0].submit();
            }
            
            function giveAcceptance()
            {   
                if ((typeof validateForm == 'function') && !validateForm())
                    return false;
            
                var cn = document.getElementById('acceptances').value;
                <%--  STARY SPOSOB
                
                var fieldValue, field;                
                <ww:iterator value="fm.acceptancesDefinition.acceptances">
                <ww:if test="!value.canAccept">
                    <ww:if test="value.fieldCn != null">
                    field = document.getElementById('dockind_<ww:property value="value.fieldCn"/>');
                    fieldValue = (typeof field == 'undefined' || field == null) ? '<ww:property value="value.currentFieldValue"/>' : field.value;
                    </ww:if>                    
                    if (cn == '<ww:property value="key"/>') 
                    {                    
                    <ww:if test="value.fieldCn != null && value.conditions != null && value.conditions.size() > 0">
                        <ww:iterator value="value.conditions" status="status">
                            <ww:if test="#status.first">if (</ww:if><ww:else> && </ww:else>
                            (fieldValue != '<ww:property/>')
                        </ww:iterator>) {
                       // alert('Wykonanie akceptacji tego typu nie jest mo�liwe dla aktualnej warto�ci pola <ww:property value="fm.getField(value.fieldCn).name"/>');
                        alert('Aby mo�liwe by�o wykonanie akceptacji tego typu, warto�� pola <ww:property value="fm.getField(value.fieldCn).name"/> musi by� '
                        <ww:set name="enum" value="(fm.getField(value.fieldCn).type == 'enum') || (fm.getField(value.fieldCn).type == 'enum-ref')"/>
                        <ww:iterator value="value.conditions" status="status">
                            <ww:if test="#status.first">+' r�wna '</ww:if>
                            <ww:else>+' lub '</ww:else>
                            +'<ww:if test="#enum"><ww:property value="fm.getField(value.fieldCn).getEnumItem(top).title"/></ww:if><ww:else><ww:property/></ww:else>'
                        </ww:iterator>
                        );
                        return;
                        }
                    </ww:if>
                    <ww:else>
                        alert('Nie masz praw do wykonania akceptacji tego typu');
                        return;
                    </ww:else>
                    }
                </ww:if>
                </ww:iterator>   --%>

                <%--
                <ww:iterator value="fm.acceptancesDefinition.generalAcceptances">
                <ww:if test="!fm.acceptancesState.getPermission(key).canAccept">
                if (cn == '<ww:property value="key"/>') 
                {    
                    alert('Nie masz praw do wykonania akceptacji tego typu');
                    return;
                }       
                </ww:if>
                </ww:iterator>
                --%>
                               
                document.getElementById('doGiveAcceptance').value = 'true';
                document.getElementById('acceptanceCn').value = cn;
                document.forms[0].submit();
            }
            
            function withdrawAcceptance()
            {   
                if ((typeof validateForm == 'function') && !validateForm())
                    return false;
            
                var cn = document.getElementById('givenAcceptances').value;
            
                document.getElementById('doWithdrawAcceptance').value = 'true';
                document.getElementById('acceptanceCn').value = cn;
                document.forms[0].submit();
            }
            
            function withdrawCentrumAcceptance(objectId)
            {   
                if ((typeof validateForm == 'function') && !validateForm())
                    return false;
            
                document.getElementById('doWithdrawAcceptance').value = 'true';
                document.getElementById('objectId').value = objectId;
                document.forms[0].submit();
            }
            
            /**
             * Zlecenie wykonania akceptacji dokumentu.
             */
            function giveCentrumAcceptance()
            {
                if ((typeof validateForm == 'function') && !validateForm())
                    return false;
                    
                document.getElementById('doGiveAcceptance').value = 'true';
                document.forms[0].submit();    
            }  
        </script>
    </td>
</tr>
</ww:if>
<!--N acceptances.jsp:end N-->