<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N search-registry-fields.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<script type="text/javascript">
    /**
     * Tablica (mapa) funkcji, kt�re b�d� wykorzystane przez poszczeg�lne s�owniki do
     * przekazania wybrane przez u�ytkownika dane. Kluczem jest 'cn' a warto�ci�
     * funkcja.
     */
    var functions = new Array();

    /**
     * Otrzymuje dane od s�ownika i 'cn' pola, aby m�c rozpozna�, kt�r� funkcj�
     * akceptuj�c� wywo�a�,
     */
    function accept(cn, map)
    {
        functions[cn](map);
    }

    /**
     * Sprawdza czy potrzebna aktualizacja - je�li tak to wywo�uje funkcj� akceptuj�c�.
     * Wo�ane, gdy w s�owniku w�a�nie zapisano zmiany i nie umieszczono ich w tym formularzu.
     */
    function checkAccept(cn, map)
    {
        if (parseInt(document.getElementById('registry_'+cn).value) == parseInt(map.id))
            accept(cn,map);
    }
</script>

<ww:iterator value="registry.fields">
<ww:if test="!hidden && !searchHidden">
    <ww:set name="prefix" value="'registry_'"/>
    <ww:set name="tagName" value="'values.'+cn"/>
    <ww:set name="tagId" value="#prefix+cn"/>   
    <tr id="<ww:property value="'tr_'+#tagId"/>">
        <td valign="top"><ww:property value="name"/>:</td>
        <td>
            <ww:if test="searchByRange && (type == 'string' || type == 'integer' || type == 'long' || type == 'float')">
                od <input type="text" size="13" maxlength="<ww:property value="type == 'string' ? length : 10"/>" class="txt"
                    name="<ww:property value="#tagName"/>_from"
                    id="<ww:property value="#tagId"/>:from"
                    value="<ww:property value="values[cn+'_from']"/>"/>
                do <input type="text" size="13" maxlength="<ww:property value="type == 'string' ? length : 10"/>" class="txt"
                    name="<ww:property value="#tagName"/>_to"
                    id="<ww:property value="#tagId"/>:to"
                    value="<ww:property value="values[cn+'_to']"/>"/>            
            </ww:if>
            <ww:elseif test="type == 'string'">
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="values[cn]"/>" size="30" maxlength="<ww:property value="length"/>" class="txt" />
            </ww:elseif>
            <ww:elseif test="type == 'float'">
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="values[cn]"/>" size="30" maxlength="30" class="txt" />
            </ww:elseif>
            <ww:elseif test="type == 'integer' || type == 'long'">                
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="values[cn]"/>" size="10" maxlength="10" class="txt" />
            </ww:elseif>
            <ww:elseif test="type == 'enum'">     
                <select class="multi_sel" size="5" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" multiple="multiple">
                    <ww:set name="fieldCn" value="cn"/>
                    <ww:iterator value="enumItems">
                        <option value="<ww:property value="id"/>" <ww:if test="values[#fieldCn] == id">selected="selected"</ww:if>>
                            <ww:property value="title"/>
                        </option>
                    </ww:iterator>
                </select>
                
                <br/>
                <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), true))">zaznacz</a>
                / <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), false))">odznacz</a>
                wszystkie    
            </ww:elseif>            
            <ww:elseif test="type == 'bool'">                            
                <select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>">
                    <option value="">-- wybierz --</option>
                    <option value="true">tak</option>
                    <option value="false">nie</option>
                </select>           
            </ww:elseif>
            <ww:elseif test="type == 'date'">
                od <input type="text" size="10" maxlength="10" class="txt"
                    name="<ww:property value="#tagName"/>_from"
                    id="<ww:property value="#tagId"/>:from"
                    value="<ww:property value="values[cn+'_from']"/>"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="datefield_trigger_<ww:property value="#tagId"/>:from" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
                do <input type="text" size="10" maxlength="10" class="txt"
                    name="<ww:property value="#tagName"/>_to"
                    id="<ww:property value="#tagId"/>:to"
                    value="<ww:property value="values[cn+'_to']"/>"/>
                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                    id="datefield_trigger_<ww:property value="#tagId"/>:to" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>
                <script>
                    Calendar.setup({
                        inputField     :    "<ww:property value="#tagId"/>:from",     // id of the input field
                        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                        button         :    "datefield_trigger_<ww:property value="#tagId"/>:from",  // trigger for the calendar (button ID)
                        align          :    "Tl",           // alignment (defaults to "Bl")
                        singleClick    :    true
                    });
                    Calendar.setup({
                        inputField     :    "<ww:property value="#tagId"/>:to",     // id of the input field
                        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                        button         :    "datefield_trigger_<ww:property value="#tagId"/>:to",  // trigger for the calendar (button ID)
                        align          :    "Tl",           // alignment (defaults to "Bl")
                        singleClick    :    true
                    });
                </script>
            </ww:elseif>
            <ww:elseif test="type == 'class'">
                <input type="hidden" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="values[cn]"/>"/>
                    
                <table>
                    <ww:iterator value="dictionaryAttributes">
                        <tr>
                            <td><ww:property value="value"/></td>
                            <td>
                                <input type="text" id="<ww:property value="#tagId+key"/>" 
                                    name="<ww:property value="#tagName+'_'+key"/>" value="<ww:property value="values[cn+'_'+key]"/>"
                                    size="50" class="txt" onchange="clear_<ww:property value="cn"/>(true)"/>
                            </td>
                        </tr>
                    </ww:iterator>
                    <tr>
                        <td colspan="2">
                            <input type="button" value="Wybierz" onclick="openDictionary_<ww:property value="cn"/>()" class="btn" />
                            <input type="button" value="Wyczy��" onclick="clear_<ww:property value="cn"/>(false)" class="btn" />
                        </td>
                    </tr>
                </table>    
                
                <script type="text/javascript">
                    function openDictionary_<ww:property value="cn"/>()
                    {
                        openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+
                                       '&id='+document.getElementById('<ww:property value="#tagId"/>').value,'<ww:property value="cn"/>');
                    }

                    function clear_<ww:property value="cn"/>(onlyId)
                    {
                        document.getElementById('<ww:property value="#tagId"/>').value = '';
                        if (!onlyId)
                        {
                        <ww:iterator value="dictionaryAttributes">
                            document.getElementById('<ww:property value="#tagId+key"/>').value = '';
                        </ww:iterator>
                        }
                    }

                    function accept_<ww:property value="cn"/>(map)
                    {
                        document.getElementById('<ww:property value="#tagId"/>').value = map.id;
                        <ww:iterator value="dictionaryAttributes">
                            document.getElementById('<ww:property value="#tagId+key"/>').value = map.<ww:property value="key"/>;
                        </ww:iterator>
                    }

                    functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
                </script>
            </ww:elseif>
            <ww:else>
                <input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
                    value="<ww:property value="values[cn]"/>" size="30" class="txt" />
            </ww:else>
        </td>
    </tr>
</ww:if>
</ww:iterator>


<script type="text/javascript">
    //ustawianie focusa
    <ww:if test="registry.properties['search-focus'] != null">
    document.getElementById('registry_<ww:property value="registry.properties['search-focus']"/>').focus();
    </ww:if>
</script>
