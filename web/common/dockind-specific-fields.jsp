<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!-- Poczatek dockind-specific-field.jsp -->
<%-- Dokument archiwum agenta --%>
<ww:if test="(cn == 'AGENT') || (cn == 'AGENCJA')">
	<input type="hidden" name="<ww:property value="#tagName"/>"
		id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
						
	<table>
		<ww:iterator value="dictionaryAttributes">
			<tr>
				<td>
					<ww:property value="value"/>
				</td>
				<td>
					<ww:if test="key == 'setOfDocuments'">
						<input id="<ww:property value="#tagId+key"/>" type="checkbox" <ww:if test="fm.getValue(cn)[key] == true">checked="checked"</ww:if> disabled="disabled" />
					</ww:if>
					<ww:else>
						<input type="text" id="<ww:property value="#tagId+key"/>"
							value="<ww:property value="fm.getValue(cn)[key]"/>"
							size="50" class="txt" readonly="readonly"/>
					</ww:else>
				</td>
			</tr>
		</ww:iterator>
		<tr>
			<td class="btnContainer" colspan="2">
				<input type="button" id="open_<ww:property value="cn"/>_button" value="Wybierz" 
					onclick="openDictionary_<ww:property value="cn"/>()" class="btn" <ww:if test="(!canReadDictionaries) && (dockindAction != 'create')">disabled="disabled"</ww:if>/>
				<input type="button" id="clear_<ww:property value="cn"/>_button" value="Wyczy��" 
					onclick="clear_<ww:property value="cn"/>();<ww:if test="cn == 'AGENT'">clear_AGENCJA()</ww:if>" class="btn"
					<ww:if test="(!canReadDictionaries) && (dockindAction != 'create')">disabled="disabled"</ww:if>/>
			</td>
		</tr>
	</table>

	<script type="text/javascript">
		function openDictionary_<ww:property value="cn"/>()
		{
			openToolWindow('<ww:url value="dictionaryAction"><ww:param name="'param'" value="cn"/></ww:url>'+
							'&id='+document.getElementById('<ww:property value="#tagId"/>').value,'<ww:property value="cn"/>');
		}

		function clear_<ww:property value="cn"/>()
		{
			document.getElementById('<ww:property value="#tagId"/>').value = '';
			<ww:iterator value="dictionaryAttributes">
				document.getElementById('<ww:property value="#tagId+key"/>').value = '';
			</ww:iterator>

			document.getElementById('dockind_AGENTsetOfDocuments').nextSibling.style.backgroundPosition = "0 0";
			
			<%--	<ww:if test="cn == 'AGENT'">
				// czy�cimy tak�e agencj�
				document.getElementById('dockind_AGENCJA').value = '';
				document.getElementById('dockind_AGENCJAnazwa').value = '';
				document.getElementById('dockind_AGENCJAnumer').value = '';
				document.getElementById('dockind_AGENCJAnip').value = '';
			</ww:if>	--%>
		}

		function accept_<ww:property value="cn"/>(map)
		{
			document.getElementById('<ww:property value="#tagId"/>').value = map.id;
			<ww:iterator value="dictionaryAttributes">
				document.getElementById('<ww:property value="#tagId+key"/>').value = map.<ww:property value="key"/>;
			</ww:iterator>

			//document.getElementById('dockind_AGENTsetOfDocuments').checked = map.setOfDocuments;
			//alert(map.setOfDocuments);
			if (map.setOfDocuments)
				document.getElementById('dockind_AGENTsetOfDocuments').nextSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			else
				document.getElementById('dockind_AGENTsetOfDocuments').nextSibling.style.backgroundPosition = "0 0";
			
			<ww:if test="cn == 'AGENT'">
				document.getElementById('dockind_AGENCJA').value = map.agencja_id;
				document.getElementById('dockind_AGENCJAnazwa').value = map.agencja_nazwa;
				document.getElementById('dockind_AGENCJAnumer').value = map.agencja_numer;
				document.getElementById('dockind_AGENCJAnip').value = map.agencja_nip;
				document.getElementById('dockind_RODZAJ_SIECI').selectedIndex = map.rodzaj_sieci;
			</ww:if>
			<ww:elseif test="cn == 'AGENCJA'">
				document.getElementById('dockind_RODZAJ_SIECI').selectedIndex = map.rodzaj_sieci;

				//wyczyszczenie pola agenta
				document.getElementById('dockind_AGENT').value = '';
				document.getElementById('dockind_AGENTimie').value = '';
				document.getElementById('dockind_AGENTnumer').value = '';
				document.getElementById('dockind_AGENTnazwisko').value = '';
			</ww:elseif>
		}

		functions['<ww:property value="cn"/>'] = accept_<ww:property value="cn"/>;
	</script>												
</ww:if>


<ww:elseif test="cn == 'ISGE'">
	<input type="checkbox" name="<ww:property value="#tagName"/>" value="true" id="<ww:property value="#tagId"/>"<ww:if test="fm.getKey(cn)">checked="true"</ww:if> onchange="set_NR_POLISY(this);"/>
	
	<script type="text/javascript">
		function set_NR_POLISY(field)
		{
			if(field.checked)
			{
				//alert(document.getElementById('required_NR_POLISY').value+'\n'+document.getElementById('required_NR_POLISY'));
				document.getElementById('dockind_NR_POLISY').value = 'GE MONEY';
			}
			else
			{
				//alert(document.getElementById('required_NR_POLISY').value+'\n'+document.getElementById('required_NR_POLISY'));
				document.getElementById('dockind_NR_POLISY').value = '';
			}
		}
	</script>
</ww:elseif>


<ww:elseif test="cn == 'NR_REJESTRACYJNY_HISTORIA'">
	<textarea name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" rows="5" cols="40" class="txt"<ww:if test="fm.getEnableSheme(cn)"> style="background-color:<ww:property value="fm.getShemeColor(cn)"/>"</ww:if><ww:if test="!canEdit">readonly="readonly"</ww:if>><ww:property value="fm.getKey(cn)"/></textarea>
</ww:elseif>

<ww:elseif test="cn == 'KWOTA_BRUTTO'">
<input type="text" name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>" size="10" maxlength="10" class="txt formatNumber" <ww:property value="fm.getStyle(cn,canEdit)" escape="false"/>/>
</ww:elseif>

<ww:elseif test="cn == 'RODZAJ_SIECI'">
	<select class="sel" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>"
			onchange="clear_RODZAJ_SIECI()"/>
		<option value="">
			<ds:lang text="select.wybierz"/>
		</option>
		<ww:set name="fieldCn" value="cn"/>
		<ww:iterator value="enumItems">
			<option value="<ww:property value="id"/>" <ww:if test="fm.getKey(#fieldCn) == id">selected="selected"</ww:if>>
				<ww:property value="title"/>
			</option>
		</ww:iterator>
	</select>

	<script type="text/javascript">
		function clear_RODZAJ_SIECI() 
		{
			document.getElementById('dockind_AGENT').value = '';
			document.getElementById('dockind_AGENTimie').value = '';
			document.getElementById('dockind_AGENTnumer').value = '';
			document.getElementById('dockind_AGENTnazwisko').value = '';

			document.getElementById('dockind_AGENCJA').value = '';
			document.getElementById('dockind_AGENCJAnazwa').value = '';
			document.getElementById('dockind_AGENCJAnumer').value = '';
			document.getElementById('dockind_AGENCJAnip').value = '';
		}		
	</script>
</ww:elseif>


<%-- Dokument ALD oraz Faktura kosztowa --%>
<ww:elseif test="(cn == 'ZAPLACONY') || (cn == 'ZAPLACONO')">
	<ww:set name="relatedCn" value="cn == 'ZAPLACONY' ? 'DATA_PLATNOSCI' : 'DATA_ZAPLATY'"/>
	<input type="checkbox" name="<ww:property value="#tagName"/>" value="true"
		<ww:if test="canEdit">id="<ww:property value="#tagId"/>"</ww:if>
		<ww:if test="fm.getKey(cn)">checked="true"</ww:if> onchange="onchange_<ww:property value="cn"/>(this);"
		<ww:if test="!canEdit || fm.getDisabled(cn)">disabled="disabled"</ww:if>/>
	
	<%-- gdy brak uprawnien do edycji (pole jest "disabled") --%>
	<ww:if test="!canEdit">
		<input type="hidden" name="<ww:property value="#tagName"/>"
			id="<ww:property value="#tagId"/>" value="<ww:property value="fm.getKey(cn)"/>"/>
	</ww:if> 
					
	<script type="text/javascript">
		function onchange_<ww:property value="cn"/>(field)
		{
			var test = new Boolean(false);
			var tmp = '<ww:property value="cn"/>';
			if(tmp.match('ZAPLACONO'))
			{	
				var data2 = document.getElementById('dockind_DATA_ZAKSIEGOWANIA');
				test = true;
			}
			
			var data = document.getElementById('dockind_<ww:property value="#relatedCn"/>');
			
			if (field.checked)
			{				
				if (emptyField(data))
				{
					var currentTime = new Date();
					var month = currentTime.getMonth() + 1;
					var day = currentTime.getDate();
					var year = currentTime.getFullYear();
					data.value = day + '-' + (month < 10 ? '0' : '') + month + '-' + year;
					if(test && (data2.value == null || data2.value == ''))
					{
						data2.value = data.value;
					}
				}
			}
			else
			{
				data.value = '';
			}
		}
	</script>
</ww:elseif>

<ds:dockinds test="invoice">
	<ww:elseif test="(cn == 'DATA_WYSTAWIENIA')">
		
		<input type="text" size="10" maxlength="10" class="txt" onchange="rewriteDateToDataZaksiegowania(this)"
			<ww:if test="fm.getEnableSheme(cn)"> 
			style="background-color:<ww:property value="fm.getShemeColor(cn)"/>;
			display:<ww:property value="fm.getShemeDisplay(cn)"/>" 
			</ww:if>
			<ww:if test="fm.getEnableSheme(cn) && fm.getDisabled(cn)"> 
				disabled="disabled"
			</ww:if>
			name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
			value="<ds:format-date value="fm.getKey(cn)" pattern="dd-MM-yyyy"/>" <ww:if test="!canEdit">readonly="readonly"</ww:if>/>
			
			<img src="<ww:url value="'/calendar096/img.gif'"/>" id="datefield_trigger_<ww:property value="#tagId"/>"
				style="cursor: pointer; border: 1px solid red;<ww:if test="!canEdit">display:none;</ww:if>"
				title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					
			<script type="text/javascript">
				Calendar.setup({
					inputField	 :	"<ww:property value="#tagId"/>",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"datefield_trigger_<ww:property value="#tagId"/>",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});				   
			</script>
		
			<script type="text/javascript">
				function rewriteDateToDataZaksiegowania(field)
				{
					if(document.URL.match('document-main') == 'document-main')
						document.getElementById('dockind_DATA_ZAKSIEGOWANIA').value = field.value;
					return;					
				}
			</script>
			
	</ww:elseif>
</ds:dockinds>


<ww:elseif test="(cn == 'DATA_PLATNOSCI') || (cn == 'DATA_ZAPLATY')">
	<ww:set name="relatedCn" value="cn == 'DATA_PLATNOSCI' ? 'ZAPLACONY' : 'ZAPLACONO'"/>
	<input type="text" size="10" maxlength="10" class="txt" <ww:if test="fm.getDisabled(cn)">disabled="disabled"</ww:if>
		name="<ww:property value="#tagName"/>" id="<ww:property value="#tagId"/>"
		value="<ds:format-date value="fm.getKey(cn)" pattern="dd-MM-yyyy"/>"
		onchange="onchange_<ww:property value="cn"/>(this);" <ww:if test="!canEdit">readonly="readonly"</ww:if>/>
	<ww:if test="!fm.getDisabled(cn)">		
		<img src="<ww:url value="'/calendar096/img.gif'"/>" id="datefield_trigger_<ww:property value="#tagId"/>"
			 style="cursor: pointer; border: 1px solid red;<ww:if test="!canEdit">display:none;</ww:if>"
			 title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
	
	<script type="text/javascript">
		Calendar.setup({
			inputField	 :	"<ww:property value="#tagId"/>",	 // id of the input field
			ifFormat		:	"<%= DateUtils.jsCalendarDateFormat %>",		// format of the input field
			button		 :	"datefield_trigger_<ww:property value="#tagId"/>",	// trigger for the calendar (button ID)
			align			:	"Tl",			// alignment (defaults to "Bl")
			singleClick	:	true
		});
		
		function onchange_<ww:property value="cn"/>(field)
		{
			document.getElementById('dockind_<ww:property value="#relatedCn"/>').checked = !emptyField(field);
		}
	</script>
	</ww:if>
</ww:elseif>
<ww:elseif test="(cn == 'mpk')">
	<ww:select name="#tagName" id="centar_mpk" list="centra" onchange="'chCentrum(this)'" listKey="id" listValue="name" value="fm.getKey(cn)" cssClass="'sel'"/>
	<ww:iterator value="centraToRachunek">
		<input type="hidden" id="chCentrum_<ww:property value="key"/>" value="<ww:property value="value"/>"/>
	</ww:iterator>
	
	<script type="text/javascript">
		function chCentrum(obj)
		{			
			try {
			document.getElementById('dockind_NR_RACHUNKU').value = document.getElementById('chCentrum_'+obj.options[obj.options.selectedIndex].value).value;
			} catch(e) {}
		}
		

		$j(document).ready(function()
		{
			chCentrum(document.getElementById('centar_mpk')); 
		});
	</script>
</ww:elseif>
<!-- Koniec dockind-specific-field.jsp -->