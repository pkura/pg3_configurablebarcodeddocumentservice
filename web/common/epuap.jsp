<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<ww:if test="epuapDocument != null">
	<tr>
		<th colspan="2"><ds:lang text="DaneEpuap"/></th>
	</tr>
	<tr>
		<td colspan="1">
			<ds:lang text="DataNadania"/>
		</td>
		<td colspan="1">
			<ds:format-date pattern="dd-MM-yyyy HH:mm:ss" value="epuapDocument.dataNadania"/>
		<td>
	</tr>
	<tr>
		<td colspan="1">
			<ds:lang text="NazwaSkrytki"/>
		</td>
		<td colspan="1">
			<ww:property value="epuapDocument.nazwaSkrytki"/>
		<td>
	</tr>
	<tr>
		<td colspan="1">
			<ds:lang text="ZgodaNaOtrzymanieOdp"/>
		</td>
		<td colspan="2">
			<ds:format-bool value="epuapDocument.podmiot.zgoda"/>
		<td>
	</tr>
</ww:if>