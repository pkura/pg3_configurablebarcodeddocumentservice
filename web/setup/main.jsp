<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<h3>Wyb�r katalogu domowego</h3>

<p>Aplikacja DocuSafe zosta�a zainstalowana w serwerze aplikacyjnym, ale
wymaga konfiguracji.</p>

<p><b>Uwaga</b>: je�eli aplikacja zosta�a ju� wcze�niej skonfigurowana, a mimo to
widzisz ten ekran, prawdopobnie z katalogu domowego (<i><%= Configuration.getHome() %></i>)
zosta�y usuni�te pliki, lub nazwa katalogu domowego jest niepoprawna.
Katalog domowy okre�la si� w konfiguracji serwera aplikacyjnego przy pomocy
parametru <i><%= Configuration.HOME %></i>.</p>

<p>Wybrany katalog domowy: <%= Configuration.getHome() %> </p>

<p>Licencja: <html:file property="licenseFile"/></p>

<p> Dane o u�ytkownikach i organizacji w: <br/>
    <input type="radio" name="userFactory" value="sql" checked="true"> Bazie danych SQL <br/>
    <input type="radio" name="userFactory" value="ldap" > LDAP <br/>
</p>
