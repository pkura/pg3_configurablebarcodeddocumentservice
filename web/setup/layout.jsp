<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N layout.jsp N-->

<html>
<HEAD>
    <title><ds:lang text="TytulStrony"/></title>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; CHARSET=iso-8859-2">
    <style type="text/css">
    body, table {
        background: #ffffff;
        color: #474747;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 10pt;
        /*
        margin-left: 5px;
        margin-right: 5px;
        */
    }

    th {
        text-align: left;
    }

    a {
        color: #7FA0B3;
        text-decoration: none;
    }

    a:hover {
        color: #7FA0B3;
        text-decoration: underline;
    }

    td.popup-header {
        background: #7FA0B3;
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 10pt;
        text-align: right;
    }

    td.popup-header a {
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 10pt;
    }

    td.popup-header a:hover {
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 10pt;
    }

    td.hdr1 {
        background: #7FA0B3;
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 12pt;
        text-align: center;
    }

    td.hdr1 a {
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 12pt;
        text-align: center;
        text-decoration: none;
    }

    td.hdr1 a:hover {
        color: #FFE8B5;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 12pt;
        text-align: center;
        text-decoration: underline;
    }


    td.hdr1s {
        background: #FFE8B5;
        text-align: center;
    }

    td.hdr1s a {
        background: #FFE8B5;
        color: #7FA0B3;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 12pt;
        text-align: center;
        text-decoration: none;
    }

    td.hdr1s a:hover {
        background: #FFE8B5;
        color: #7FA0B3;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 12pt;
        text-align: center;
        text-decoration: underline;
    }

    td.hdr2 {
        background: #B6E5FF;
    }

    td.footer {
        background: #7FA0B3;
        color: #FFE8B5;
        font-weight: normal;
        font-size: 10pt;
        padding-left: 5px;
        padding-right: 5px;
        padding-top: 1px;
        padding-bottom: 1px;
    }

    td.footer a {
        color: #FFE8B5;
        text-decoration: none;
    }

    td.footer a:hover {
        color: #FFE8B5;
        text-decoration: underline;
    }

    td.body {
        padding-left: 5px;
        padding-bottom: 2px;
    }

    /* menu w lewej kolumnie */
    td.menu {
        background: #B6E5FF;
        padding-left: 5px;
        /*
        color: #7FA0B3;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-weight: bold;
        font-size: 10pt;
        */
    }

    td.menu a {
        color: #7FA0B3;
        font-family: Tahoma, 'Trebuchet MS';
        font-weight: bold;
        font-size: 10pt;
        text-decoration: none;
    }

    td.menu a:hover {
        color: #7FA0B3;
        font-family: Tahoma, 'Trebuchet MS';
        font-weight: bold;
        font-size: 10pt;
        text-decoration: underline;
    }

    h1 {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 14pt;
        font-weight: bold;
        color: #474747;
    }

    h2 {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 13pt;
        font-weight: bold;
        color: #474747;
    }

    h3 {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 12pt;
        font-weight: bold;
        color: #474747;
    }

    h4 {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 11pt;
        font-weight: bold;
        color: #474747;
    }

    input.txt, select.sel, select.multi_sel, textarea.txt {
        border-style: solid;
        border-width: 1px;
        border-color: black;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        background: #FFE8B5; /*#C9C9C9;*/
        font-size: 10pt;
    }

    input.txt_opt, select.sel_opt, select.multi_sel_opt, textarea.txt_opt {
        border-style: solid;
        border-width: 1px;
        border-color: black;
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        background: #f0f0f0;
        font-size: 10pt;
    }

    input.txt, select.sel, select.sel_opt {
        height: 14pt;
    }

    input.btn {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 8pt;
        height: 16pt;
        background: #FFE8B5; /*#C9C9C9;*/
        border-width: 1px;
        border-style: ridge;
        border-right-color: black;
        border-left-color: #b3a37f;
        border-top-color: #b3a37f;
        border-bottom-color: black;
    }

    input.cancel_btn {
        font-family: Tahoma, Arial, Helvetica, sans-serif;
        font-size: 8pt;
        height: 16pt;
        background: #b3a37f; /*#C9C9C9;*/
        border-width: 1px;
        border-style: ridge;
        border-right-color: black;
        border-left-color: #a3936f;
        border-top-color: #a3936f;
        border-bottom-color: black;
    }

    table.login {
        border-style: solid;
        border-width: 1px;
        border-color: black;
        background: #D6CFC4;
        color: white;
    }
    td.login {
        font-family: Tahoma;
        font-weight: bold;
        font-size: 10pt;
    }
    input.loginbtn {
        border-style: ridge;
        border-width: 1px;
        background: #FFE8B5;
        color: black;
        font-family: Tahoma;
        font-weight: normal;
        font-size: 8pt;
    }
    input.logintxt {
        border-style: solid;
        border-width: 1px;
        border-color: black;
        font-family: Tahoma;
        font-size: 10pt;
        background: white;
        height: 14pt;
    }

    span.star {
        color: red;
        font-weight: bold;
    }

    p.division-path {
        font-weight: bold;
    }

    .warning {
        color: red;
        font-weight: bold;
    }

    </style>
</HEAD>
<body leftMargin=0 topMargin=0 marginwidth="0" marginheight="0">

  <% if (request.getAttribute("include.location") != null) { %>
    <jsp:include page="<%= (String) request.getAttribute("include.location") %>"/>
  <% } %>

  <hr width="100%" size="1" noshade="true"/>

  <table width="100%" cellspacing="0" cellpadding="0">
  <tr>
      <td class="footer">
          Konfiguracja aplikacji Docusafe
      </td>
      <td class="footer" align="right">
          Copyright (c) 2003-2005 COM-PAN
      </td>
  </tr>
  </table>

</body>
</html>
