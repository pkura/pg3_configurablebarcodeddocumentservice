<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N clone-dockind-document.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<%@ taglib uri="webwork" prefix="ww" %>

<h1><ds:lang text="KlonowanieDokumentu"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="p4CloneDocument">
<form action="<ww:url value="'/repository/clone-dockind-document.action'"/>" class="dwr" method="post" onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'documentId'"/>
    <ds:available test="dwr">
        <jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
    </ds:available>
    <table>
        <tr>
            <td><ds:lang text="Tytul"/>:</td>
            <td><ww:property value="title"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Opis"/>:</td>
            <td><ww:property value="description"/></td>
        </tr>
        <ds:available test="!dwr">
            <jsp:include page="/common/dockind-fields.jsp"/>
           <jsp:include page="/common/dockind-specific-additions.jsp"/>
        </ds:available>
      
	



        <tr>
            <td valign="top"><ds:lang text="Zalaczniki"/>:</td>
            <td>
                <table>
                <ww:set name="canModify" value="canModify"/>
                <ww:iterator value="attachments">
                    <tr>
                        <td><b><ww:property value="title"/></b></td>
                        <td><nobr>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" checked="true" value="copy" /> <ds:lang text="kopiuj"/>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="move" <ww:if test="!#canModify || blocked">disabled="true"</ww:if> /> <ds:lang text="przenies"/>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="none" /> <ds:lang text="niePrzenos"/>
                        </nobr></td>
                    </tr>
                </ww:iterator>
                </table>
            </td>
        </tr>
        
        
         <ds:available test="kancelaria.wlacz.wersjeDokumentu">
        	<tr>
				<td><ds:lang text="Nowa wersja pisma"/>:</td>
				<td>
					<ww:checkbox name="'nowa_wersja'" fieldValue="true" value ="true" />
				</td>
			</tr>
			<tr>
				<td><ds:lang text="Opis nowej wersji"/>:</td>
				<td>
				<ww:textfield name="'nowy_opis'" maxlength="600" size="60"/></td>
           		<!--  <input type="text" name="'nowy_opis'" maxlength="510" class="txt"/></td>  -->
			</tr>
	    </ds:available>
	    

    </table>
	
    <ds:event value="getText('Klonuj')" name="'doClone'" cssClass="'btn'" onclick="return fieldsManager.validate()"/>

</form>
</ds:available>
<ds:available test="!p4CloneDocument">
<form action="<ww:url value="'/repository/clone-dockind-document.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'documentId'"/> 
	
	
    <table>
        <tr>
            <td><ds:lang text="Tytul"/>:</td>
            <td><ww:property value="title"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Opis"/>:</td>
            <td><ww:property value="description"/></td>
        </tr>
		
        <jsp:include page="/common/dockind-fields.jsp"/>
        <jsp:include page="/common/dockind-specific-additions.jsp"/>
        
        	
        <tr>
            <td valign="top"><ds:lang text="Zalaczniki"/>:</td>
            <td>
                <table>
                <ww:set name="canModify" value="canModify"/>
                <ww:iterator value="attachments">
                    <tr>
                        <td><b><ww:property value="title"/></b></td>
                        <td><nobr>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" checked="true" value="copy" /> <ds:lang text="kopiuj"/>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="move" <ww:if test="!#canModify || blocked">disabled="true"</ww:if> /> <ds:lang text="przenies"/>
                            <input type="radio" name="copyAttachments[<ww:property value="id"/>]" value="none" /> <ds:lang text="niePrzenos"/>
                        </nobr></td>
                    </tr>
                </ww:iterator>
                </table>
            </td>
        </tr>
         <ds:available test="kancelaria.wlacz.wersjeDokumentu">
         	<tr>
				<td><ds:lang text="Nowa wersja pisma"/>:</td>
				<ww:if test="wyslano">
				<td>
					<ww:checkbox name="'nowa_wersja'" fieldValue="true" value ="false"  disabled="true"/>
				</td>
				</ww:if>
				<ww:else>
				<td>
					<ww:checkbox name="'nowa_wersja'" fieldValue="true" value ="true" />
				</td>
				</ww:else>
			</tr>
			<tr>
				<td><ds:lang text="Opis nowej wersji"/>:</td>
				<td><nobr>
           		<ww:textfield id="'nowy_opis'" name="'nowy_opis'" maxlength="60" size="60"/></td>
           		<!-- <input type="text" name="'nowy_opis'" maxlength="510" class="txt"/></td> -->
			</tr>
			
	   </ds:available>

    </table>

    <ds:event value="getText('Klonuj')" name="'doClone'" cssClass="'btn'" onclick="'if (!validateForm()) return false'"/>



<script type="text/javascript">
    function validateForm()
    {
        if (!validateDockind())
            return false;
        return true;
    }

    </script>
    <ds:available test="utp.niePokazojPolSlownikowychNaFormatceKlonowania">
    <script>
	$j('#tr_dockind_LINKS, #tr_dockind_NUMBER_DICT,#tr_dockind_SENDER,#tr_dockind_RECIPIENT,#tr_dockind_SENDER_HERE,#tr_dockind_RECIPIENT_HERE').hide();
	$j('#tr_dockind_JOURNAL,#tr_dockind_DOC_DELIVERY,#tr_dockind_datazwrotki,#tr_dockind_DOC_FORM,#tr_dockind_WAGA').hide();
	$j('#tr_dockind_PRACOWNIK,#tr_dockind_STUDENT,#tr_dockind_KANDYDAT,#tr_dockind_KONTRAHENT,#tr_dockind_typ_slownika').hide();
	$j('#tr_dockind_PRACOWNIK_KOPIA,#tr_dockind_STUDENT_KOPIA,#tr_dockind_KANDYDAT_KOPIA,#tr_dockind_KONTRAHENT_KOPIA,#tr_dockind_SENDER_KOPIA,#tr_dockind_typ_slownika_kopia').hide();
	$j('#tr_dockind_PRACOWNIK_UKRYTA_KOPIA,#tr_dockind_STUDENT_UKRYTA_KOPIA,#tr_dockind_KANDYDAT_UKRYTA_KOPIA,#tr_dockind_KONTRAHENT_UKRYTA_KOPIA,#tr_dockind_SENDER_UKRYTA_KOPIA,#tr_dockind_typ_slownika_ukrytaKopia').hide();
	$j('#tr_dockind_DOSTEP').hide();
	</script>
	</ds:available >
</form>
</ds:available>