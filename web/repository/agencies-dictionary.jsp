<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N agencies-dictionary.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1><ds:lang text="SlownikAgencji"/></h1>

<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
<ww:iterator value="tabs" status="status" >
	
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</div>
</ds:available>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/agencies-dictionary.action'"/>" method="post" enctype="multipart/form-data">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

    <table>
        <tr>
            <th colspan="2"><ds:lang text="EksportSlownikaAgencjiDoPlikuCSV"/></th>
        </tr>
        <tr>
            <td><ds:lang text="Plik"/>:</td>
            <td><ww:textfield name="'exportFilename'" value="exportFilename" cssClass="'txt'" size="50" id="exportFilename" /></td>
        </tr>
        <tr>
            <td><ds:event name="'doExport'" value="getText('Eksportuj')"/></td>
            <td></td>
        </tr>
        <tr>
            <td><input type="button" value="<ds:lang text="EdycjaSlownikaAgencji"/>" onclick="openAgencjaPopup();" class="btn" ></td>
            <td></td>
        </tr>
    </table>
    
    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>

<script language="javascript">

        function openAgencjaPopup()
        {
            openToolWindowX('<ww:url value="'/office/common/agencja.action'"/>'+
                '?canAddAndSubmit=true&forEdit=true', 'Agencja', 950, 650);
        }

        
</script>
