<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="ListaPudel"/></h1>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form action="<ww:url value="'/repository/search-box.action'"/>" method="post">
<ds:available test="layout2">
  <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
<ww:hidden name="'priorityKind'" id="priorityKind"/>
<ww:hidden name="'boxId'" id="boxId"/>
<ww:hidden name="'doOrder'" id="doOrder"/>

<table class="mediumTable search">
  <tr>
    <td colspan="4">
      <ds:lang text="OpisPudla"/>:<ww:textfield name="'description'" size="100" maxlength="300" cssClass="'txt'"/>
    </td>
  </tr>
  <tr>
    <td><ds:event name="'doSearch'" value="getText('Szukaj')"/></td>
  </tr>
  <tr>
    <th>
      ID
    </th>
    <th>
      Numer
    </th>
    <th>
      Opis
    </th>
    <th>
      Rodzaj zam�wienia
    </th>
    <th>
      Status
    </th>
    <th>
      Zamawiaj�cy
    </th>
    <th>
      Data zam�wienia
    </th>
    <th>
    </th>
    <th>
    </th>
  </tr>
  <ww:iterator value="boxBeans">
    <tr>
      <td>
        <ww:property value="box.id"/>
      </td>
      <td>
        <ww:property value="box.name"/>
      </td>
      <td>
        <ww:property value="box.description"/>
      </td>
      <td>
        <ww:select name="'priorityKindSel'+box.id" id="priorityKind" list="priorityMap" headerKey="''" headerValue="getText('select.wybierz')"
          cssClass="'sel'" />
      </td>
      <td>
        <ww:property value="status"/>
      </td>
      <td>
        <ww:property value="user"/>
      </td>
      <td>
        <ww:property value="orderDate"/>
      </td>
      <td>
      	<a href="javascript:zamow(<ww:property value="box.id"/>);">zam�w</a>
        <a href="<ww:url value="'/repository/search-box.action'"><ww:param name="'doOrder'" value="'true'"/><ww:param name="'boxId'" value="box.id"/></ww:url>" >zam�w</a>
      </td>
      <td>
        <a href="<ww:url value="'/repository/search-box.action'"><ww:param name="'doReturn'" value="'true'"/><ww:param name="'boxId'" value="box.id"/></ww:url>" >potwierd� zwrot</a>
      </td>
    </tr>
  </ww:iterator>
</table>
<script type="text/javascript">
	function zamow(boxID)
	{
		var selElement = document.getElementsByName('priorityKindSel'+boxID)[0];
		if(selElement.selectedIndex == null || selElement.selectedIndex == 0)
		{
			alert('Wybierz rodzaj zam�wienia');
			return;
	 	}
		document.getElementById('priorityKind').value = selElement.options[selElement.selectedIndex].value ;
		document.getElementById('boxId').value = boxID;
		document.getElementById('doOrder').value = 'true';
		document.forms[0].submit();
	}
</script>
<ds:available test="layout2">
  <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
  <div class="bigTableBottomSpacer">&nbsp;</div>
  </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>