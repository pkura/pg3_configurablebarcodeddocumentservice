<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N bookmarks.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Ulubione"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/bookmarks.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="bookmarks.empty">
    	<p><ds:lang text="BrakUlubionych"/></p>
	</ww:if>

	<ww:else>
	    <table class="tableMargin">
	         <ww:iterator value="bookmarks">
	            <tr>
	                <td>
	                	<ww:checkbox name="'deleteKeys'" fieldValue="key"/>
	                </td>
	                <td>
	                	<a href="<ww:url value="link"/>"><ww:property value="title"/></a>
	                </td>
	            </tr>
	        </ww:iterator> 
	    </table>
	    
	    <ds:submit-event value="getText('UsunZaznaczone')" name="'doDelete'" cssClass="'btn'"/> 
	</ww:else>
</form>

<%--
<h1><ds:lang text="Ulubione"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/bookmarks.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<ww:if test="bookmarks.empty">
    <p><ds:lang text="BrakUlubionych"/></p>
</ww:if>
<ww:else>
    <table>
        <ww:iterator value="bookmarks">
            <tr>
                <td><ww:checkbox name="'deleteKeys'" fieldValue="key"/></td>
                <td><a href="<ww:url value="link"/>"><ww:property value="title"/></a></td>
            </tr>
        </ww:iterator>
    </table>
    <ds:submit-event value="getText('UsunZaznaczone')" name="'doDelete'" cssClass="'btn'"/> 
</ww:else>

</form>
--%>
<!--N koniec bookmarks.jsp N-->