<%@ page import="pl.compan.docusafe.web.archive.repository.EditAttachmentAction"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h1><edm-html:message key="editAttachment.h1"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/repository/edit-attachment" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);" >
<html:hidden property="id" />
<input type="hidden" name="doUpdate" id="doUpdate"/>

<table>
<tr>
    <td><ds:lang text="attachment.title"/><span class="star">*</span>:</td>
    <td><html:text property="title" size="30" maxlength="254" styleClass="txt" /></td>
</tr>
<tr>
    <td><ds:lang text="attachment.description"/><span class="star">*</span>:</td>
    <td><html:text property="description" size="30" maxlength="510" styleClass="txt" /></td>
</tr>
<tr>
    <td><ds:lang text="attachment.file"/>:</td>
    <td><html:file property="file" styleClass="txt_opt" /></td>
</tr>
<tr>
    <td></td>
    <td><input type="submit" value="<ds:lang text="Aktualizuj"/>" class="btn"
            onclick="document.getElementById('doUpdate').value='true';"/>
        <br/>
        <small><ds:lang text="attachment.anyChangeCreatesNewRevision"/></small>
    </td>
</tr>
</table>

<c:if test="${!empty documentLink}">
    <input type="button" value='<ds:lang text="attachment.backToDocument"/>' onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${documentLink}'/>';" class="btn">
</c:if>

<h3><ds:lang text="attachment.availableRevisions"/></h3>

<table>
<tr>
<!-- 
    <th><edm-html:message key="attachmentRevision.revision"/></th>
    <th><edm-html:message key="attachmentRevision.date"/></th>
    <th><edm-html:message key="attachmentRevision.size"/></th>
    <th colspan="2"><edm-html:message key="attachmentRevision.originalFilename"/></th>
    <th><edm-html:message key="attachmentRevision.author"/></th>
-->
    <th><ds:lang text="attachmentRevision.revision"/></th>
    <th><ds:lang text="attachmentRevision.date"/></th>
    <th><ds:lang text="attachmentRevision.size"/></th>
    <th colspan="2"><ds:lang text="attachmentRevision.originalFilename"/></th>
    <th><ds:lang text="attachmentRevision.author"/></th>
    <ds:available test="app.documentconverter">
    <th><ds:lang text="attachmentRevision.documentconverter"/></th>
    </ds:available>
</tr>

<c:forEach items="${revisionBeans}" var="bean" varStatus="status">
<tr>
    <td align="right">
        <c:out value="${bean.revision}"/>
    </td>
    <td>
        <fmt:formatDate value="${bean.ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/>
    </td>
    <td align="right">
        <edm-fmt:format-size size="${bean.size}"/>
    </td>
    <td align="right"><img src='<c:out value="${pageContext.request.contextPath}${bean.icon}"/>'></td>
    <td>
        <c:choose>
            <c:when test="${canReadAttachments}">
                <c:if test="${status.first}">
                	<a href="<c:out value="${pageContext.request.contextPath}"/>/repository/view-attachment-revision.do?id=<c:out value="${bean.id}"/>&binderId=<c:out value="${binderId}"/>">
                	<c:out value="${bean.originalFilename}"/></a>
                </c:if>
                <c:if test="${!status.first}">
                	<a onclick="return conf();" href="<c:out value="${pageContext.request.contextPath}"/>/repository/view-attachment-revision.do?id=<c:out value="${bean.id}"/>&binderId=<c:out value="${binderId}"/>">
                	<c:out value="${bean.originalFilename}"/></a>
                </c:if>
            </c:when>
            <c:otherwise>
                <c:out value="${bean.originalFilename}"/>
            </c:otherwise>
        </c:choose>
    </td>
    <td>
        <c:out value="${bean.author}"/>
    </td>
    <ds:available test="app.documentconverter">
    <td class="documentconverter" data-revision-id="<c:out value="${bean.id}"/>">
        <select></select>
        <button>Pobierz</button>
    </td>
    </ds:available>
</tr>
<ds:modules test="certificate">
    <c:if test="${!empty bean.signatures}">
        <tr><td></td><td><ds:lang text="Podpisy"/>:</td><td colspan="5">
            <table>
            <c:forEach items="${bean.signatures}" var="sig">
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;<c:out value="${sig.author}"/></td>
                    <td>&nbsp;<fmt:formatDate value="${sig.ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/></td>
                    <td colspan="3"><a href="<c:out value="${pageContext.request.contextPath}"/>/office/common/attachment-signature.action?doView=true&signatureId=<c:out value="${sig.id}"/>"><ds:lang text="Pobierz"/></a></td>
                    <td><a href="javascript:openToolWindow('<c:out value="${pageContext.request.contextPath}"/>/office/common/attachment-signature.action?doVerify=true&signatureId=<c:out value="${sig.id}"/>', 'verify', 500, 400);"><ds:lang text="weryfikuj"/></a></td>
                    <td><c:if test="${sig.correctlyVerified ne null && !sig.correctlyVerified}"><span style="color: red"><ds:lang text="bladWpodpisie"/></span></c:if></td>
                </tr>
            </c:forEach>
            </table>
        </td></tr>
    </c:if>
</ds:modules>
</c:forEach>
</table>

    <c:if test="${!empty linkShowAll}">
        <p><ds:lang text="PokazywanychJestTylko"/> <%= EditAttachmentAction.SHOW_REVISIONS %>
        <ds:lang text="najnowszychWersjiZalacznika.AbyZobaczycWszystkieWersjeKliknij"/>
        <a href="<c:out value="${linkShowAll}"/>"><ds:lang text="Tutaj"/></a>.</p>
    </c:if>

</html:form>
<script type="text/javascript">
	function conf()
	{
		return confirm('<ds:lang text="ToNieJestNajnowszaWersjaZalacznikaKontynuowac"/>');
	}
    var APPLICATION_URL = "<c:out value="${pageContext.request.contextPath}"/>";
</script>
<script src="<c:out value="${pageContext.request.contextPath}"/>/js/editAttachments.js"></script>