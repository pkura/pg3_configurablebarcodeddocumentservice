<%@ page
	import="org.apache.commons.beanutils.DynaBean,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<h1>
	<ds:lang text="SzczegulyDockumentu" />
</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<script language="JavaScript">
function pickFolder(id, path)
{
    document.getElementById('folderId').value = id;
    document.getElementById('folderPath').value = path;
    // ustawiam info, �e folder by� r�cznie wybrany - zatem nie b�dzie archiwizacji zgodnej z dockind-em
    document.getElementById('manualFolder').value = 'true';
}

function promptLockExpiration()
{
    var exp = prompt('<ds:lang text="DoKiedyZablokowac"/>?', document.getElementById('lockExpiration').value);
    if (exp == null || exp == "")
    {
        return false;
    }
    else
    {
        document.getElementById('lockExpiration').value = exp;
        return true;
    }
}

function promptEmail()
{
    var email = prompt('<ds:lang text="PodajAdresEmailOdbiorcy"/>');
    if (email == null || email == "")
    {
        return false;
    }
    else
    {
        document.getElementById('email').value = email;
        return true;
    }
}
<ds:available test="!dwr">
function validateForm()
{
    var title = document.getElementById('title').value;
    if (title == null || title.length == 0) { alert('<ds:lang text="NiePodanoTytuluDokumentu"/>'); return false; }

    var description = document.getElementById('description').value;
    if (description == null || description.length == 0) { alert('<ds:lang text="NiePodanoOpisuDokumentu"/>'); return false; }
    if (description != null && description.length > 510) { alert('<ds:lang text="OpisDokumentuJestZbytDlugi.MaksymalnaLiczbaZnakowWOpisieTo510"/>'); return false; }

    if (!validateDockind())
        return false;
 
    return true;
}
</ds:available>

<%--
/* wolane z doctype-fields - w edycji nei wybiera sie wzoru umowy */
/* wiec funkcja nic nie robi                                      */
function __nw_updateAttachmentPattern(typeId)
{
}
    --%>

</script>
<ww:if test="!popup">
	<ds:available test="!layout2">
		<p>
			<ds:xmlLink path="editDockind" />
		</p>
	</ds:available>

	<ds:available test="layout2">
		<div id="middleMenuContainer">
			<img
				src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif"
				class="tab2_img" />
			<ds:xmlLink path="editDockind" />
		</div>
	</ds:available>
</ww:if>
<ww:if test="popup">
	<form id="form" class="dwr"
		action="<ww:url value="'/repository/edit-dockind-document-popup.action'"/>"
		method="post" onsubmit="disableFormSubmits(this)"
		enctype="multipart/form-data">
</ww:if>
<ww:else>
	<form name="EditDockindForm" id="form" class="dwr"
		action="<ww:url value="'/repository/edit-dockind-document.action'"/>"
		method="post" onsubmit="disableFormSubmits(this)"
		enctype="multipart/form-data">
</ww:else>
<ds:available test="layout2">
	<div id="middleContainer">
		<!-- BIG TABLE start -->
</ds:available>
<ds:ww-action-errors />
<ds:ww-action-messages />
<ww:if test="canRead">

	<ww:if test="!hist">
		<ww:hidden name="'id'" />
		<ww:hidden name="'binderId'" value="binderId" />
		<ww:hidden name="'popup'" value="popup" />
		<ww:hidden name="'email'" id="email" />
		<ww:hidden name="'folderId'" id="folderId" />
		<ww:hidden name="'queryLink'" />
		<ww:hidden name="'lockExpiration'" id="lockExpiration" />
		<ww:hidden name="'boxId'" id="boxId" />
		<ww:hidden name="'manualFolder'" id="manualFolder" value="false" />
		<ww:hidden name="'daa'" id="daa" value="daa" />

		<ww:if test="!queryLink.empty">
			<p>
				<b><a href='<ww:url value="queryLink"/>'> <ds:lang
							text="PowrotDoWyszukiwania" />
				</a></b>
			</p>
		</ww:if>

		<table class="formTable">
			<ww:if test="blocked">
				<tr>
					<td colspan="2"><span style="color: red"><ds:lang
								text="TenDokumentZostalZaakceptowanyIjestZablokowanyPrzedEdycja" />.</span></td>
				</tr>
			</ww:if>
			<ww:if test="lockedThru != null">
				<ww:set name="lockedThru" value="lockedThru" scope="page" />
				<tr>
					<td><span style="color: red"><ds:lang
								text="ZablokowanyDo" /></span>:</td>
					<td><fmt:formatDate value="${lockedThru}" type="both"
							pattern="dd-MM-yy HH:mm" /> <ds:lang text="przez" /> <ww:property
							value="lockedBy" /></td>
				</tr>
			</ww:if>
			<tr>
				<td><ds:lang text="Identyfikator" />:</td>
				<td id="identyfikator_td"><ww:property value="id" /></td>
			</tr>
			<ww:if test="document.ctime != null">
				<ww:set name="ctime" value="document.ctime" scope="page" />
			</ww:if>
			<tr>
				<td><ds:lang text="DataUtworzenia" />:</td>
				<td><fmt:formatDate value="${ctime}" type="both"
						pattern="dd-MM-yy HH:mm" /></td>
			</tr>
			<ww:if test="document.mtime != null">
				<ww:set name="mtime" value="document.mtime" scope="page" />
			</ww:if>
			<tr>
				<td><ds:lang text="DataOstatniejModyfikacji" />:</td>
				<td><fmt:formatDate value="${mtime}" type="both"
						pattern="dd-MM-yy HH:mm" /></td>
			</tr>
			<tr>
				<td><ds:lang text="Autor" />:</td>
				<td><ww:property value="documentAuthor" /></td>
			</tr>
			<ds:available test="browsedocument.officedocument.visible">
				<ww:if test="officeLink != null && !disabledAllButtons && officeLinksEnabled">
					<td><ds:lang text="DokumentKancelaryjny" />:</td>
					<ww:if test="officeTask != null && officeTask.size() > 0">

						<ww:iterator value="officeTask">
							<tr>
								<td></td>
								<td><a
									href="<ww:url value="officeLink"/>&activity=<ww:property value="getWorkflowName()"/>,<ww:property value="getActivityKey()"/>">
										<ds:lang text="NumerKO" /> <ww:property
											value="getDocumentOfficeNumber()" />
								</a></td>
							</tr>
						</ww:iterator>

					</ww:if>
					<ww:else>
						<td><a href="<ww:url value="officeLink"/>"><ds:lang
									text="przejdz" /></a> | <a href="<ww:url value="przywrocLink"/>"><ds:lang
									text="przywroc" /></a></td>
					</ww:else>
				</ww:if>
			</ds:available>
			<ds:available test="!dwr">
				<tr>
					<td><ds:lang text="attachment.title" /><span class="star">*</span>:</td>
					<td><ww:textfield name="'title'" id="title" size="50"
							maxlength="128" cssClass="'txt'" /></td>
				</tr>

				<tr
					<ds:available test="repository.description.hide">style="display:none"</ds:available>>
					<td><ds:lang text="Opis" /><span class="star">*</span>:</td>
					<td><ww:textarea name="'description'" id="description"
							cssClass="'txt'" rows="4" cols="50" /></td>
				</tr>
			</ds:available>
			<ds:available test="!repository.folderpath">
				<tr>
					<td><ww:if test="folderLink != null">
							<a href="<ww:url value="folderLink"/>"><ds:lang text="Folder" /></a>:
            </ww:if> <ww:else>
							<ds:lang text="Folder" />:</ww:else></td>
					<td><ww:textarea name="'folderPrettyPath'"
							cssClass="'txt_opt'" rows="3" cols="70" readonly="true"
							id="folderPath" /> <input type="button"
						value="<ds:lang text="WybierzFolder"/>"
						onclick="javascript:void(window.open('folders-tree-popup.jsp', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"
						class="btn" disabled="disabledAllButtons"  id="chooseFolder"></td>
				</tr>
			</ds:available>
			<ds:available test="repository.pudlo">
				<jsp:include page="/common/box.jsp" />
			</ds:available>

			<ww:if test="flagsPresent">
				<tr>
					<td><ds:lang text="Flagi" />:</td>
					<td><jsp:include page="/common/flags-present.jsp" /></td>
				</tr>

			</ww:if>
			<ds:available test="!dwr">
				<ww:if test="canChangeDockind">
					<tr
						<ds:available test="dockind.select.hide">style="display:none"</ds:available>>
						<td><ds:lang text="RodzajDokumentuDocusafe" />:</td>
						<td id="dockind-select"><ww:if
								test="documentKinds.size() > 1">
								<ww:select id="documentKindCn" name="'documentKindCn'"
									list="documentKinds" listKey="key" listValue="value"
									value="documentKindCn" cssClass="'sel'"
									onchange="'changeDockind();'" />
							</ww:if> <ww:else>
								<ww:hidden name="'documentKindCn'" value="documentKindCn"></ww:hidden>
								<ww:property value="documentKindCn" />
							</ww:else></td>
					</tr>
				</ww:if>
				<ww:if test="documentAspects != null">
					<tr>
						<td><ds:lang text="AspektDokumentu" />:</td>
						<td><ww:select id="documentAspectCn"
								name="'documentAspectCn'" list="documentAspects"
								cssClass="'sel'" listKey="cn" listValue="name"
								onchange="'changeDockind();'" /></td>
					</tr>
				</ww:if>

				<jsp:include page="/common/dockind-fields.jsp" />

				<jsp:include page="/common/dockind-specific-additions.jsp" />
			</ds:available>

			<ds:available test="dwr">
		</table>
		<input type="hidden" name="processName" id="processName" />
		<input type="hidden" name="processId" id="processId" />
		<input type="hidden" name="processAction" id="processAction" />
		<script type="text/javascript">
            function validateDockind(){return fieldsManager.validate();}
            function validateForm(){return validateDockind();}
        </script>

		<jsp:include page="/dwrdockind/dwr-document-base.jsp" />

		<table>
			</ds:available>

			<tr class="formTableDisabled">
				<td colspan="2"><span class="btnContainer"> <ds:available
							test="dwr">
						<!-- 	<ww:if test="importedDocument">
							<ds:submit-event value="getText('Przyjmij  zaimportowany dokument')"
								name="'doSelfAssignImportedDocument'" cssClass="'btn saveBtn'" disabled="takePackageConfirmation"
								validate="validateForm()"/>
							</ww:if>-->
							<ds:available test="menu.left.repository.paczki.dokumentow">
							<ww:if test="packageKind">
							<ds:submit-event value="getText('Przyjmij paczk�/przesy�ke')"
								name="'doConfirmTakePackage'" cssClass="'btn saveBtn'" disabled="takePackageConfirmation"
								validate="validateForm()"/>
								</ww:if> 
						</ds:available>
							<ds:event value="getText('ZapiszZmiany')" id="btnZapisz"
								type="'submit'" name="'doUpdateDWR'" cssClass="'btn saveBtn'"
								onclick="'if(!fieldsManager.validate()) return false;'"
								disabled="!canUpdate || blocked" />
						</ds:available> <ds:available test="!dwr">
							<ds:submit-event value="getText('ZapiszZmiany')"
								name="'doUpdate'" cssClass="'btn saveBtn'"
								validate="validateForm()" disabled="!canUpdate || blocked" />
						</ds:available>
						
						 <ds:submit-event value="getText('Usun')" name="'doDelete'"
							cssClass="'btn cancelBtn'" disabled="!canDelete"  /><!--onClick="delete_confirm()" -->
				</span> <ww:if test="canUndelete">
						<ds:submit-event value="getText('CofnijUsuniecie')"
							name="'doUndelete'" cssClass="'btn'" />
					</ww:if> <ds:available test="!repository.permissions.hide">
						<ww:if test="canSetPermissions">
							<ww:set name="permissionsLink" value="permissionsLink"
								scope="page" />
							<edm-html:window-open-button windowHeight="300" windowWidth="400"
								href="${pageContext.request.contextPath}${permissionsLink}"
								styleClass="btn"><ds:lang text="Uprawnienia" /></edm-html:window-open-button>
						</ww:if>
						<ww:else>
							<input type="button" value="<ds:lang text="Uprawnienia"/>"
								class="btn" disabled="true" />
						</ww:else>
					</ds:available> <ww:if
						test="document.getDoctype() != null and !'daa'.equals(document.getDoctype().getCn())">
						<ww:if test="canLock">
							<!--
                    <ww:set name="lockDocumentLink" value="lockDocumentLink" scope="page"/> 
                    <edm-html:window-open-button windowHeight="400" windowWidth="400" href="${pageContext.request.contextPath}${lockDocumentLink}" value="getOpis()" styleClass="btn"></edm-html:window-open-button>
                     -->
							<input type="button" class="btn"
								value="<ds:lang text="Zablokuj"/>" onClick="openLock()" />
							<script type="text/javascript">
                       		function openLock()
                       		{
                           		//repository/lock-document.do?documentId=147424
                       			openToolWindow('<ww:url value="'/repository/lock-document.do'"><ww:param name="'documentId'" value="id"/></ww:url>','600', 600, 500);
                       		}
                       </script>
						</ww:if>
						<ww:else>
							<input type="button" value="<ds:lang text="Zablokuj"/>"
								class="btn" disabled="true" />
						</ww:else>

						<ww:if test="canUnlock">
							<ds:submit-event value="getText('Odblokuj')" name="'doUnlock'"
								cssClass="'btn'" />
						</ww:if>
						<ww:else>
							<input type="button" value="<ds:lang text="Odblokuj"/>"
								class="btn" disabled="true" />
						</ww:else>
					</ww:if> <span class="btnContainer"> <ds:available
							test="archiwizacja.obserwowane">
							<ds:submit-event value="getText('DoObserwowanych')"
								name="'doAddToFavourites'" cssClass="'btn'"  disabled="disabledAllButtons" />
						</ds:available> <ds:available test="archiwizacja.ulubione">
							<ds:submit-event value="getText('DoUlubionych')"
								name="'doAddToBookmarks'" cssClass="'btn'" disabled="disabledAllButtons" />
						</ds:available>
				</span> <ds:available test="archwizacja.wyslijLink && !disabledAllButtons">
						<ww:set name="sendLinkLink" value="sendLinkLink" scope="page" />
						<edm-html:window-open-button windowHeight="130" windowWidth="450"
							href="${pageContext.request.contextPath}${sendLinkLink}"
							styleClass="btn"><ds:lang text="WyslijOdnosnik"   /></edm-html:window-open-button>
					</ds:available> <ds:available test="archiwizacja.klon">
						<input type="button" class="btn" value="<ds:lang text="Klonuj"/>"
							onclick="document.location.href='<ww:url value="'/repository/clone-dockind-document.action'"><ww:param name="'documentId'" value="id"/></ww:url>';"
							<ww:if test="!canClone">disabled="true"</ww:if> />
					</ds:available> <ds:available test="archiwum.cofnijarchiwizacje">
						<ds:submit-event value="getText('CofniecieArchiwizacji')"
							cssClass="'btn'" name="'doUnarchive'"
							disabled="!canUnarchive || !canUpdate || blocked" />
					</ds:available> <ww:if test="canAddCorrectingNotes">
						<input type="button" class="btn" value="<ds:lang text="Koryguj"/>"
							onclick="document.location.href='<ww:url value="'/repository/correcting-note.action'"><ww:param name="'documentId'" value="id"/></ww:url>';" />
					</ww:if> <ds:available test="document.confirmation">
						<input type="button" class="btn" disabled="disabledAllButtons" 
							value="<ds:lang text="Potwierdz"/>"
							onclick="document.location.href='<ww:url value="'/repository/confirmation.action'"><ww:param name="'documentId'" value="id"/></ww:url>';" />
					</ds:available> <ds:available test="electronicSignature.document">
						<ww:if test="electronicSignatureAvailable">
							<input type="button" class="btn" value="Podpisz" disabled="disabledAllButtons" 
								onclick="javascript:window.location='<ww:url value="'/certificates/sign-xml.action?documentId='+documentId+'&returnUrl='+simpleReturnUrl"/>'" />
						</ww:if>
					</ds:available>
					<ds:available test="document.OCR">
                        <input type="button" class="btn" value="<ds:lang text="Wy�lij do OCR"/>"
                    		onclick="document.location.href='<ww:url value="'/repository/ocr.action'"><ww:param name="'documentId'" value="id"/></ww:url>';" />
                    </ds:available>
                    
                    <ds:available test="showButtonsToPrints">
		                <ww:if test="'accepted'.equals(status)">
			                Wydruki: <ww:select list="templates" name="'selectedTemplate'" cssClass="'sel'"/>
			                <input type="button" class="btn" value="Generuj wybrany wydruk" onclick="createUrl('<ww:property value="id"/>')"/>
			                <script type="text/javascript">
			                	function createUrl(id) {
			                		var template = $j('[name=selectedTemplate] option:selected').val();
			                		return window.location.href='docusafe/download-dockind-template.action?special=true&documentId=' + id + '&templateName=' + template;
			                	}
			                </script>
		                </ww:if>
                    </ds:available>
                    
					<input type="button" class="btn" value="<ds:lang text="Powrot"/>"
					onClick="history.back()" /> <ds:extras test="business">
					</ds:extras> <input id="sendInfoButton" type="button" class="btn"
					value="<ds:lang text="UmiescWformularzu"/>" onClick="sendInfo()" />
					<script type="text/javascript">
	            $j(document).ready(function() {
	            	if(!window.opener || !window.opener.accept_UMOWA) 
		            {
	            		$j('#sendInfoButton').hide();
	            	}
	            });
            	
           		function sendInfo()
           		{
					var map = new Array();

					map.id = $j('#identyfikator_td').text();
					map.numerUmowy = document.getElementById('title').value;
        			window.opener.accept_UMOWA(map);
        			window.close();
           		}
            </script></td>
			</tr>
		</table>

		<ds:modules test="certificate">
			<ww:if test="!signatureBeans.empty">
				<h4>
					<ds:lang text="AkceptacjeDokumentu" />
				</h4>

				<table>
					<tr>
						<th><ds:lang text="Autor" /></th>
						<th><ds:lang text="Data" /></th>
						<th colspan="2" style="text-align: center"><ds:lang
								text="Pobierz" />:</th>
						<th></th>
						<th></th>
					</tr>

					<ww:iterator value="signatureBeans">
						<tr>
							<td>&nbsp;<ww:property value="author.asFirstnameLastname()" /></td>
							<td>&nbsp;<ds:format-date value="ctime"
									pattern="dd-MM-yy HH:mm" /></td>
							<td><a
								href="<ww:url value="'/office/common/document-signature.action?getDoc=true&signatureId='+id"/>"><ds:lang
										text="dokument" /></a></td>
							<td><a
								href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+id"/>"><ds:lang
										text="podpis" /></a></td>
							<td><a
								href="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doVerify=true&signatureId='+id+'&id='+document.id"/>', 'verify', 500, 400);"><ds:lang
										text="Weryfikuj" /></a></td>
							<td><ww:if
									test="correctlyVerified != null && !correctlyVerified">
									<span style="color: red"><ds:lang text="bladWPodpisie" /></span>
								</ww:if></td>
						</tr>
					</ww:iterator>
				</table>
			</ww:if>

			<ww:if test="useSignature">
				<p>
					<input type="button" value="<ds:lang text="PodpiszDokument"/>"
						class="btn"
						onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doSign=true&id='+document.id"/>', 'sign', 400, 300);"
						<ww:if test="!canSign">disabled="disabled"</ww:if> /> <input
						type="button" value="<ds:lang text="PostacDokumentuDoPodpisu"/>"
						class="btn"
						onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?showDoc=true&id='+document.id"/>', 'sign', 800, 600);" />
				</p>
			</ww:if>
		</ds:modules>


		<h3>
			<ds:lang text="Zalaczniki" />
		</h3>

		<input type="button" value="<ds:lang text="NowyZalacznik"/>"
			onclick="document.location.href='<ww:url value="addAttachmentLink"/>';"
			class="btn"
			<ww:if test="!canModifyAttachments || blocked">disabled="true"</ww:if> />

		<ww:if test="!attachments.empty">
			<table>
				<tr>
					<th></th>
					<th></th>
					<th><ds:lang text="attachment.title" /></th>
					<th><ds:lang text="attachment.newestRevision" /></th>
					<th><ds:lang text="Autor" /></th>
					<th><ds:lang text="Rozmiar" /></th>
					<th><ds:lang text="Data" /></th>
					<th><ds:lang text="Wersja" /></th>
				</tr>

				<ww:iterator value="attachments">
					<tr>
						<ww:if test="'mail'.equals(cn)">
							<td><ww:checkbox name="'ids'" fieldValue="id" /></td>
							<td></td>
							<td><a
								href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="wparam"/></ww:url>"><ww:property
										value="title" /></a></td>
							<td></td>
							<td><ww:property value="author" /></td>
							<td></td>
							<td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm" /></td>
							<td></td>
							<td></td>
						</ww:if>
						<ww:else>
							<td><ww:if test="editable"><ww:checkbox name="'ids'" fieldValue="id" /></ww:if></td>
							<td><img src="<ww:url value="icon"/>" /></td>
							<td>
								<ww:if test="canReadOrginalAttachments">
									<a href="<ww:url value="editLink"/>">
										<ww:property value="title"/>
									</a>
								</ww:if>
								<ww:else>
									<ww:property value="title"/>
								</ww:else>
							</td>
							<td>
								<ww:if test="canReadAttachments">
									<ww:if test="canReadOrginalAttachments">
										<a href="<ww:url value="recentContentLink"/>"> <img
											src="<ww:url value="'/img/pobierz.gif'"/>" width="18"
											height="18" class="zoomImg" class="zoomImg"
											title="<ds:lang text="Pobierz"/>"></a>
									</ww:if>
									<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mime)">
										<a href="<ww:url value="recentContentLink+'&asPdf=true'"/>">
											<img src="<ww:url value="'/img/pdf.gif'"/>" width="18"
											height="18" class="zoomImg" class="zoomImg"
											title="<ds:lang text="Pobierz"/>" />
										</a>
										&nbsp;
										<ww:if test="canReadOrginalAttachments && @pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptableWithoutOnViewer(mime)">
											<a
												href="javascript:openToolWindow('<ww:url value="showRecentContentLink"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/><ww:param name="'binderId'" value="binderId"/></ww:url>', 'vs', 1000, 750);">
												&nbsp;&nbsp; <img
												src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
												height="18" class="zoomImg" class="zoomImg"
												title="<ds:lang text="Wyswietl"/>">
											</a>
										</ww:if>
									</ww:if>
									<ww:elseif
										test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeXml(mime) && canReadOrginalAttachments">
										<a class="xmlView"
											href="javascript:openToolWindow('<ww:url value="'/viewserver/xml/'+ revisionId"/>')">
											<img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
											height="18" class="zoomImg"
											title="<ds:lang text="WyswietlZalacznik"/>" />
										</a>
									</ww:elseif>
								</ww:if>
								<ds:modules test="certificate">
									<ww:if test="useSignature && canSign">
										<a
											href="javascript:openToolWindow('<ww:url value="'/office/common/attachment-signature.action?id='+revisionId"/>', 'sign', 400, 300);"><img
											src="<ww:url value="'/img/podpisz.gif'"/>" width="18"
											height="18" class="zoomImg"
											title="<ds:lang text="PodpiszZalacznik"/>" /></a>
									</ww:if>
								</ds:modules>
							</td>
							<td><ww:property value="author" /></td>
							<td align="right"><ds:format-size value="#this['size']" /></td>
							<td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm" /></td>
							<td align="right"><ww:property value="revision" /></td>
							<td><ww:if
									test="'nw_compiled_att'.equals(cn) && compilationStatus.newDocuments != null">
									<ww:iterator value="compilationStatus.newDocuments">
										<a style="color: red"
											href="<ww:url value="'/repository/edit-document.action?id='+key"/>"><ww:property
												value="value" /></a>
									</ww:iterator>
								</ww:if></td>
						</ww:else>
					</tr>
				</ww:iterator>
			</table>
			<!-- AFTER TABLE -->

			<span class="btnContainer"> <ww:if
					test="usunZaznaczoneZalaczniki">
					<ds:submit-event value="getText('UsunZaznaczoneZalaczniki')"
						name="'doDeleteAttachments'" cssClass="'btn'"
						disabled="!canModifyAttachments || blocked || !canDeleteAttachments" />

					<ds:available test="deleteAttachmentAndAddRemark">
						<ww:hidden id="CreateRemark" name="'createRemark'" />
						<input id="DeleteAndAddRemark" type="button" class="btn deleteBtn"
							value="Usu� zaznaczone za��czniki i dodaj uwag�"
							<ww:if test="!canModifyAttachments || blocked || !canDeleteAttachments">disabled="disabled"</ww:if> />
						<script type="text/javascript">
		        	$j('#DeleteAndAddRemark').click(function(){ 
			        	$j('#CreateRemark').val('true');
			        	$j('#doDeleteAttachments').val('true');
			        	document.forms.EditDockindForm.submit();
		        	});
		        </script>
					</ds:available>
				</ww:if> <ww:if test="trwaleUsunZaznaczoneZalaczniki">
					<ds:submit-event value="getText('TrwaleUsunZaznaczoneZalaczniki')"
						name="'doDeleteAttachmentsPermanently'" cssClass="'btn'"
						disabled="!canModifyAttachments || blocked || !canDeleteAttachments" />
				</ww:if>
			</span>

			<ww:if test="compilationStatus.canCompile">
				<ds:submit-event name="'doCompileAttachments'"
					value="getText('KompilujPlikiSprawy')" cssClass="'btn'" />
			</ww:if>

			<ww:if test="canGenerateDocumentView">
				<input type="button" value="<ds:lang text="PobierzObrazDokumentu"/>"
					class="btn"
					onclick="openToolWindow('<ww:url value="'/repository/edit-dockind-document.action'"><ww:param name="'id'" value="id"/><ww:param name="'doGenerateDocumentView'" value="true"/></ww:url>', 'nwPdf')">
			</ww:if>
		</ww:if>
	</ww:if>
</ww:if>
<ww:elseif test="hist">
	<table width="100%">
		<ww:iterator value="orderHistList">
			<tr>
				<td><ww:property value="KTO" /></td>
				<td align="right"><ww:property value="DATA_ZMIANY" /></td>
			</tr>
			<tr>
				<td colspan="2" style="border: 1px solid black;"><ww:property
						value="TXT" /></td>
			</tr>
		</ww:iterator>
	</table>
</ww:elseif>
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div>
	<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>
<script type="text/javascript">
    function checkFolderSize()
    {
        /* zwiekszam wielkosc pola folder jesli konieczne (czyli nazwa folderu bardzo d�uga) */
        var element = document.getElementById('folderPath');

        try {
	        if ((element.value != null) && (element.value != undefined) && (parseInt(element.value.length) > parseInt(element.getAttribute('size'))))
	        {
	            element.setAttribute('size', element.value.length);
	        }
        } catch(e) {}
    }

    checkFolderSize();
    function disp_confirm()
    {
        var r=confirm("<ds:lang text="CzyNaPewnoChceszZamowic"/>");
        if (r==true)
        {
            document.getElementById('doZamow').value='true';
            document.forms[0].submit();
        }
        else
        {
            document.forms[0].reset();
        }
    }
    function delete_confirm()
    {
        var r=confirm("<ds:lang text="CzyNaPewnoChceszUsunac"/>");
        if (r==true)
        {
            document.getElementById('doDelete').value='true';
            document.forms[0].submit();
        }
        else
        {
            document.forms[0].reset();
        }
    }
    <ds:available test="archiwizacja.dokument.zablokujZmianeRodzaju" documentKindCn="documentKindCn">
        selectedDocumentKind = $j('#documentKindCn option:selected');
        $j('#documentKindCn').remove();

        $j('#dockind-select').append('<input type=\'hidden\' name=\'documentKindCn\' value=\''+ selectedDocumentKind.val()+'\'/><b>'
            + selectedDocumentKind.text() + '</b>');
    </ds:available>
    
</script>

<ds:available test="archiwizacja.dokument.updatujListeUzytkownika"
	documentKindCn="documentKindCn">
	<jsp:include page="/common/update-users-field.jsp" />
</ds:available>
