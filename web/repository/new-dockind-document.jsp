<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<ds:extras test="!nationwide">
<ds:available test="!new_dockind_document_hide_title">
		<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NowyDokument"/></h1>
	</ds:available>
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
</ds:extras>
<p></p>




<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/repository/new-dockind-document.action'"/>" 
      method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);" 
      class="dwr <ww:property value="documentKindCn"/>">

	<input type="hidden" name="doCreate" id="doCreate"/>
	<input type="hidden" name="createNext" id="createNext"/>
	<ww:hidden name="'boxId'"/>
	<ww:hidden name="'folderId'" value="folderId" id="folderId"/>
	<ww:hidden name="'manualFolder'" id="manualFolder" value="false"/>

	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

	<ds:available test="dwr">
	<jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
	<fieldset>
	<div class="field multiFiles_part">
	<ww:iterator value="loadFiles">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/><br/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>"><br/>
	</ww:iterator> 
		<label><ds:lang text="Plik"/>:</label> 
    	<a href="#" onclick="addAtta(this); return false;" class="addLink" ><ds:lang text="DolaczKolejny" /></a>
		<ww:file name="'multiFiles1'" size="30" cssClass="'txt multiFiles'" value="''"/>
		<!-- label>Tytul:</label> 
		<input type="text" class="txt multiFilesTitle" name="multiFilesTitle1" /-->
		<img src="<ww:url value="'/img/minus2.gif'" />" class="removeLink" onclick="delAtta(this);" title="Usu�" style="cursor: pointer; position: relative; top: 3px; left: 3px;">
	</div>
	<script type="text/javascript">
		$j('img.removeLink').hide();
		var attCounter = 2;
		function addAtta(domObj)
		{
			$j('img.removeLink').show();
			var str = '<div class="field multiFiles_part">';
				str += $j('div.multiFiles_part:last').html();
				str += '</div>';
				
			trace(str);
			$j(domObj).parent().after(str).fadeIn();
			$j('div.multiFiles_part:last').children('input.multiFiles').attr('name', 'multiFiles' + attCounter);
			//$j('div.multiFiles_part:last').children('input.multiFilesTitle').attr('name', 'multiFilesTitle' + attCounter);
			var jqAlmostLast = $j('div.multiFiles_part').eq(-2);
			jqAlmostLast.children('a.addLink').css('visibility', 'hidden');
			jqAlmostLast.children('img.removeLink').show();
			attCounter ++;
		}
		
		function delAtta(domObj)
		{
			/* usuwamy tylko gdy mamy dwa lub wi�cej plik�w */
			if($j('div.multiFiles_part').length > 1) {
				if ($j('img.removeLink:last').filter(domObj).length > 0) {
					$j('a.addLink').eq(-2).show();	// pokazujemy przedostatni
				}
				
				/* ukrywamy ikonk� usu�, je�li zosta�y tylko dwa pliki */
				if($j('div.multiFiles_part').length < 3)
					$j('img.removeLink').hide();
				
				$j(domObj).parent('div.multiFiles_part').slideUp(300, function() {
					$j(this).remove();
					$j('div.multiFiles_part:last').children('a.addLink').css('visibility', 'visible');
				});
			}
		}
	</script>
	</fieldset>
	
	</ds:available>
	<table id="bt" class="tableMargin formTable">
		<ds:available test="!dwr">
		<tr>
			<td>
				<ds:lang text="Tytul"/>:
			</td>
			<td>
				<ww:textfield name="'title'" size="50" maxlength="254" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Opis"/>:
			</td>
			<td>
				<ww:textarea name="'description'" rows="4" cols="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr <ds:available test="dockind.select.hide">style="display:none"</ds:available> >
			<td>
				<ds:lang text="RodzajDokumentuDocusafe"/>:
			</td>
			<td>
				<ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key"
					listValue="value" value="documentKindCn" cssClass="'sel'" onchange="''"/>
				<script type="text/javascript">
                                $j(function(){
                                    //pisma moga byc tylko w pismach wewnetrznych
                                    $j('#documentKindCn [value=wf]').remove();
                                    $j('#documentKindCn [value=absence-request]').remove();
                                    $j('#documentKindCn [value=overtime_app]').remove();
                                    $j('#documentKindCn [value=overtime_receive]').remove();
                                    $j('#documentKindCn [value=wykaz]').remove();
                                    $j('#documentKindCn [value=protokol_br]').remove();
                                });
                                
		        	$j('#documentKindCn').change(function(){
                                        
		        		if($j('#documentKindCn').val() == 'absence-request') {
							alert('Wniosek urlopowy mo�na wype�ni� tylko jako Pismo Wewn�trzne!');
							return false;
		        		}
                                        if($j('#documentKindCn').val() == 'wf' || $j('#documentKindCn').val() == 'overtime_app' || $j('#documentKindCn').val() == 'overtime_receive') {
							alert('Dokument mo�na wype�ni� tylko jako Pismo Wewn�trzne!');
							return false;
		        		}
						
						changeDockind();
		           	});
		        </script>
			</td>
		</tr>
		<ww:if test="documentAspects != null">
		<tr>
			<td>
				<ds:lang text="AspektDokumentu"/>:
			</td>
			<td>
				<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
			</td>
		</tr>
		</ww:if>
		
		<jsp:include page="/common/dockind-fields.jsp"/>
		
		<ww:if test="canAddToRS">
			<tr>
				<td>
					<ds:lang text="NumerSzkody"/>:
				</td>
				<td>
					<ww:textfield name="'NR_SZKODY'" id="NR_SZKODY" size="30" maxlength="30" cssClass="'txt"/>
				</td>
			</tr>
		</ww:if>
		</ds:available>
		<tr>
			<ds:available test="repository.newdockinddocumen.chooseFolder">
		 		<td>
			  		<ds:lang text="Folder"/>:
				</td>
				<td>
					<ww:textarea name="'folderPrettyPath'" cssClass="'txt_opt'" rows="3" cols="70" readonly="true" id="folderPath"/>
					<input type="button" value="<ds:lang text="WybierzFolder"/>" onclick="javascript:void(window.open('folders-tree-popup.jsp', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn" id="chooseFolder">
				</td>
			</ds:available>
		</tr>
		<ds:available test="!dwr">
		<tr>
			<td>
				<ds:lang text="Zalacznik"/>:<span class="star always">*</span>
			</td>
			<td>
				<ww:file name="'file'" cssClass="'txt'" size="50" id="file"/>
			</td>
		</tr>
		</ds:available>

		<jsp:include page="/common/dockind-specific-additions.jsp"/>
		<ds:available test="documentMain.int.ArchiwizacjaWOtwartymPudle"  documentKindCn="documentKindCn">
		<tr>
			<td>
				<ds:lang text="ArchiwizacjaWOtwartymPudle"/><ww:if test="boxId != null"> (<ww:property value="boxName"/>)</ww:if>:
			</td>
			<td>
				<ww:checkbox name="'addToBox'" fieldValue="true" disabled="boxId == null"/>
			</td>
		</tr>
		</ds:available>
		<tr class="formTableDisabled">
			<td></td>
			<td>
				<ds:available test="!dwr">
					<input name = "createDocument" type="submit" value="<ds:lang text="Utworz"/>" class="btn"
						onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true';"/>
				
					<ww:if test="showCreateNext">
					<input name = "createDocumentNext" type="submit" value="<ds:lang text="UtworzNext"/>" class="btn"
						onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true'; document.getElementById('createNext').value='true';"/>
					</ww:if>
				</ds:available>
				<ds:available test="dwr">
					<input name = "createDocument" type="submit" value="<ds:lang text="Utworz"/>" class="btn"
					onclick="if (!fieldsManager.validate()) return false; document.getElementById('doCreate').value='true';"/>
				
					<ww:if test="showCreateNext">
					<input name = "createDocumentNext" type="submit" value="<ds:lang text="UtworzNext"/>" class="btn"
						onclick="if (!fieldsManager.validate()) return false; document.getElementById('doCreate').value='true'; document.getElementById('createNext').value='true';"/>
					</ww:if>
				</ds:available>
			</td>
		</tr>
	</table>
	<p>
	<span class="star always">*</span><ds:lang text="PoleObowiazkowe"/>
	</p>
	<ww:iterator value="loadFiles">
			<input type="text" value="<ww:property value="value"/>" readonly="readonly"/><br/>
			<input type="hidden" name="returnLoadFiles" value="<ww:property value="key"/>"><br/>
	</ww:iterator> 
	 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
	
</form>
<script>
function pickFolder(id, path)
{
	document.getElementById('folderId').value = id;
	document.getElementById('folderPath').value = path;
	// ustawiam info, �e folder by� r�cznie wybrany - zatem nie b�dzie archiwizacji zgodnej z dockind-em
	document.getElementById('manualFolder').value = 'true';
}

	function validateForm()
	{
		
		if (!validateDockind())
			return false;
		
		if (!validateFiles())
			return false; 
	
		return true;
	}

	//ustawianie focusa
	<ww:if test="fm.documentKind.properties['newdocument-focus'] != null">
	document.getElementById('dockind_<ww:property value="fm.documentKind.properties['newdocument-focus']"/>').focus();
	</ww:if>
</script>
        
<ds:available test="archiwizacja.dokument.updatujListeUzytkownika" documentKindCn="documentKindCn">
    <jsp:include page="/common/update-users-field.jsp"/>
</ds:available>

<jsp:include page="/repository/new-dockind-partial.jsp"/>

<!--N koniec new-dockind-document.jsp N-->

 <ds:available test="project.parametrization.ima">
     <ww:set name="typeIma" value="'repository'"/>
    <jsp:include page="/parametrization/ima/left-menu.jsp"/>
 </ds:available>