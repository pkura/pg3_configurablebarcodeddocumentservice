<%@ page import="pl.compan.docusafe.web.archive.repository.CreateNewFolderAction" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Nowy Folder"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/create-new-folder.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:property value="folderPath"/>

    <table class="tableMargin">
        <tr>
            <td>
                <ds:lang text="Tytul"/>
                <span class="star">*</span>:
            </td>
            <td>
                <ww:textfield id="newFolderName" name="'newFolderName'" required="true" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;
            </td>
            <td align="left">
                <ds:event name="'doCreate'" value="getText('doCreate')"/>
                <ds:button-redirect href="'/repository/explore-documents.do#'" value="getText('doCancel')"/>
            </td>
        </tr>
     </table>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
</form>