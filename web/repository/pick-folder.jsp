<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N pick-folder.jsp N-->

<%@ page import="java.io.IOException,
                 java.util.*,
                 org.apache.commons.logging.Log,
                 org.apache.commons.logging.LogFactory,
                 pl.compan.docusafe.util.XmlUtils,
                 pl.compan.docusafe.core.users.auth.AuthUtil,
                 pl.compan.docusafe.core.security.AccessDeniedException"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<html>
<head>
    <title><ds:lang text="TytulStrony"/></title>
    <script language="JavaScript">
    function pickFolder(id, prettyPath, lparam)
    {
        window.opener.pickFolder(id, prettyPath, lparam);
        window.close();
    }
    </script>
<style>
body {
	background-color: #D6CFC4;
}
a {
	color:black;
	text-decoration: none;
    vertical-align: middle;
	font: bold 12px/15px Verdana, Geneva, Arial, Helvetica, sans-serif;
	line-height: 15px;
	height: 15px;
}
</style>
</head>
<body>
<c:out value="${tree}" escapeXml="false"/>
</body>
</html>
