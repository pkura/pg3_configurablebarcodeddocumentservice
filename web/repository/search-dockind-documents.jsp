<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<%--
<a href="<ww:url value="'/repository/search-documents.action'"/>" ><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>" class="highlightedText"><ds:lang text="WyszukiwanieZaawansowane"/></a>

<ds:dockinds test="ald">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-payment-verification.action'"/>"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>"><ds:lang text="WeryfikacjaRejestruVAT"/></a>    
</ds:dockinds>

<ds:dockinds test="dl">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>"><ds:lang text="InformacjeOkliencie"/></a>
</ds:dockinds>

<ds:dockinds test="invoice">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>
--%>

<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>

<p></p>
<ww:if test="results == null or results.empty">
	<form name="formul" id="form" action="<ww:url value="'/repository/search-dockind-documents.action'"/>" method="post">
		<ww:hidden name="'doChangeDockind'" id="doChangeDockind"/>
		<ww:hidden name="'doChangeStyle'" id="doChangeStyle"/>
		<ww:hidden name="'resultsInPopup'" id="resultsInPopup" value="resultsInPopup"/>
		<ww:hidden name="'newSearchStyle'" id="newSearchStyle"/>
		<ww:hidden name="'searchStyle'" id="searchStyle" value="searchStyle"/>
		
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

        <ds:ww-action-errors/>
        <ds:ww-action-messages/>

		<table id="bt" class="formTable">
			<ds:available test="wyszukiwarka.IDString">
				<tr>
					<td><ds:lang text="Identyfikator" />:</td>
					<td><ww:textfield id="docId" name="'documentIdString'"
							size="10" maxlength="10" cssClass="'txt'" /></td>
				</tr>
			</ds:available>
			<ds:available test="!wyszukiwarka.IDString">
				<tr>
					<td><ds:lang text="IdentyfikatorID" />:</td>
					<td><ww:textfield id="docId" name="'documentId'" size="10"
							maxlength="10" cssClass="'txt'" /></td>
				</tr>
			</ds:available>
			<tr>
				<td><ds:lang text="officenumber" />:</td>
				<td><ww:textfield id="officeNumber" name="'officeNumber'"
						size="10" maxlength="10" cssClass="'txt'" /></td>
			</tr>
			<ds:available test="SearchDockindDocument.year">
			<tr>
				<td>Rok:</td>
				<td><ww:textfield id="officeNumberYear" name="'officeNumberYear'" size="10" maxlength="4" cssClass="'txt'"/>
				</td>
			</tr>
			</ds:available>
			<ds:available test="SearchDockindDocument.documentOfficeCaseSymbol">
			<tr>
				<td>Znak sprawy:</td>
				<td><ww:textfield id="documentOfficeCaseSymbol" name="'documentOfficeCaseSymbol'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			
			<%-- <tr>
				<td>Symbol w sprawie;</td>
				<td><ww:textfield id="documentReferenceId" name="'documentReferenceId'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr> --%>
			
			</ds:available>
			
			
			 <ds:available test="searchDockindDocument.officeNumberFromTo">
			<tr>
				<td>
					<ds:lang text="ZakresNumerowKO"/>:
				</td>
				<td>
					<table class="leftAlign oneLine" cellspacing=0 cellpadding=0>
					<tbody class="leftAlignInner">
					<tr>
					<td><ds:lang text="od"/></td>
					<td><ww:textfield id="officeNumberFrom" name="'officeNumberFrom'" size="10" maxlength="10" cssClass="'txt'"/></td>
					<td><ds:lang text="do"/></td>
					<td><ww:textfield id="officeNumberTo" name="'officeNumberTo'" size="10" maxlength="10" cssClass="'txt'"/></td>
					</tr>
					</tbody>
					</table>
				</td>
			</tr>
			</ds:available>
			
			
				<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr <ds:available test="dockind.select.hide">style="display:none"</ds:available>  >
				<td>
					<ds:lang text="RodzajDokumentuDocusafe"/>:
				</td>
				<td>
					<ww:if test="documentKinds.size() > 1">
					<ww:select name="'documentKindCn'" list="documentKinds" listKey="key" listValue="value"
						cssClass="'sel'" id="documentKindCn" onchange="'dockindChanged(event);'" />
					</ww:if>
					<ww:else>
						<ww:hidden name="'documentKindCn'" value="documentKindCn"></ww:hidden>
    					<ww:property value="documentKindCn"/>
					</ww:else>
					<ww:set name="searchStyle" value="searchStyle == 'only-related' ? 'only-related' : null"/>
					<ww:if test="kind.properties['can-change-style'] == 'true'">
						<a href="javascript:changeSearchStyle()"><ww:property value="(#searchStyle == 'only-related') ? getText('WidokPelny') : getText('WidokUproszczony')"/></a>
					</ww:if>
				</td>
			</tr>
			<ww:if test="documentKindCn == 'allDockinds'">
				<tr>
					<td>Wyszukiwanie pe�no kontekstowe:</td><td><ww:textfield name="'fullTextSearch'" size="50" maxlength="200" cssClass="'txt'"/></td>
				<tr>
			</ww:if>
			<ww:if test="documentAspects != null">
			<tr>
				<td>
					<ds:lang text="AspektDokumentu"/>:
				</td>
				<td>
					<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'dockindChanged(event);'"/>
				</td>
			</tr>
			</ww:if>			

			<jsp:include page="/common/search-dockind-fields.jsp"/>
			
			<ds:available test="flagi.wyszukiwarka" documentKindCn="documentKindCn">
				<ww:if test="flagsOn">
					<ww:if test="flags.size() > 0">
					<tr>
						<td valign="top">
							<ds:lang text="FlagiWspolne"/>:
						</td>
						<td>
							
							<nobr>
								<ww:iterator value="flags">
									<ww:checkbox name="'flagIds'" fieldValue="id" value="'false'"/>
									<span title="<ww:property value="description"/>">
										<ww:property value="c"/>
									</span>
								</ww:iterator>
							</nobr>
							<br>

							<table class="leftAlign oneLine">
							<tbody class="leftAlignInner">
							<tr>
							<td><ds:lang text="nadane"/>&nbsp;<ds:lang text="od"/>
							<script type="text/javascript">
								var _flagsDateFrom = "flagsDateFrom";
								var _flagsDateTo = "flagsDateTo";
							</script></td>
							
							<td><ww:textfield name="'flagsDateFrom'" id="flagsDateFrom" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_flagsDateFrom,_flagsDateTo,1)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_flagsDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/></td>
							<td><ds:lang text="do"/></td>
							<td><ww:textfield name="'flagsDateTo'" id="flagsDateTo" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_flagsDateFrom,_flagsDateTo,2)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_flagsDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/></td>
							</tr>
							</tbody>
							</table>
						</td>
						<!-- TRZECIA POPRAWA -->
						<script type="text/javascript">
							Calendar.setup({
								inputField     :    "flagsDateFrom",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_flagsDateFrom_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
							Calendar.setup({
								inputField     :    "flagsDateTo",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_flagsDateTo_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
						</script>
					</tr>
					</ww:if>
					<ww:if test="userFlags.size() > 0">
					<tr>
						<td valign="top">
							<ds:lang text="FlagiUzytkownika"/>:
						</td>
						<td>
							<nobr>
								<ww:iterator value="userFlags">
									<ww:checkbox name="'userFlagIds'" fieldValue="id" value="'false'"/>
									<span title="<ww:property value="description"/>">
										<ww:property value="c"/>
									</span>
								</ww:iterator>
							</nobr>
							<br>
							
							<table class="leftAlign oneLine">
							<tbody class="leftAlignInner">
							<tr>
							<tr>
							<td>
							<ds:lang text="nadane"/>&nbsp;<ds:lang text="od"/>
							<script type="text/javascript">
								var _userFlagsDateFrom = "userFlagsDateFrom";
								var _userFlagsDateTo = "userFlagsDateTo";
							</script></td>
							<td>
							<ww:textfield name="'userFlagsDateFrom'" id="userFlagsDateFrom" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_userFlagsDateFrom,_userFlagsDateTo,1)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_userFlagsDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/></td>
								
							<td><ds:lang text="do"/></td>
							<td><ww:textfield name="'userFlagsDateTo'" id="userFlagsDateTo" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_userFlagsDateFrom,_userFlagsDateTo,2)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_userFlagsDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/></td>
							</tr>
							</tbody>
							</table>
						</td>
						<!-- JEDNA POPRAWA -->
						<script type="text/javascript">
							Calendar.setup({
								inputField     :    "userFlagsDateFrom",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_userFlagsDateFrom_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
							Calendar.setup({
								inputField     :    "userFlagsDateTo",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_userFlagsDateTo_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
						</script>
					</tr>
					</ww:if>
				</ww:if>
			</ds:available>
			<ds:available test="repository.ctime">
			<tr>
				<td>
					<ds:lang text="DataRejestracji"/>:
				</td>
				<script type="text/javascript">
								var _ctimeFrom = "ctimeFrom";
								var _ctimeTo = "ctimeTo";
							</script>
				<td>
					od : <ww:textfield name="'ctimeFrom'" id="ctimeFrom" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_ctimeFrom,_ctimeTo,1)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_ctimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
					do : <ww:textfield name="'ctimeTo'" id="ctimeTo" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_ctimeFrom,_ctimeTo,1)'"/>
							<img src="<ww:url value="'/calendar096/img.gif'"/>"
								id="calendar_ctimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
								title="Date selector" onmouseover="this.style.background='red';"
								onmouseout="this.style.background=''"/>
				</td>
				<script type="text/javascript">
							Calendar.setup({
								inputField     :    "ctimeFrom",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_ctimeFrom_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
							Calendar.setup({
								inputField     :    "ctimeTo",     // id of the input field
								ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
								button         :    "calendar_ctimeTo_trigger",  // trigger for the calendar (button ID)
								align          :    "Tl",           // alignment (defaults to "Bl")
								singleClick    :    true
							});
						</script>
			</tr>
			</ds:available>
			<ds:available test="!repository.search.hideBoxName" documentKindCn="documentKindCn">
			<tr>
				<td>
					<ds:lang text="PudloArchiwalne"/>:
				</td>
				<td>
					<ww:textfield name="'boxName'" size="20" maxlength="100" cssClass="'txt'" />
				</td>
			</tr>
		    </ds:available>
			<tr>
				<td>
					<ds:lang text="Autor"/>:
				</td>
				<td>
					<ww:select name="'author'" list="accessedByUsers" cssClass="'sel combox-chosen'" headerKey="''"
					headerValue="getText('select.dowolny')" listKey="name" listValue="asLastnameFirstnameName()" />
				</td>
			</tr>
			<ds:available test="repository.search.uzytkownikPracujacyNaDokumencie">
			<tr>
				<td valign="top">
					<ds:lang text="UzytkownikPracujacyNaDokumencie"/>:
				</td>

							<td valign="top">
								<ww:select name="'accessedBy'" list="accessedByUsers" cssClass="'sel combox-chosen'"
									headerKey="''" headerValue="getText('select.dowolny')"
									listKey="name" listValue="asLastnameFirstnameName()" />
							</td>
							<td valign="top">
								<ww:select id="accessedAs" name="'accessedAs'" size="4" multiple="true" cssClass="'multi_sel'"
									list="@pl.compan.docusafe.core.base.DocumentChangelog@used()"
									listKey="what" listValue="description"/>
								<br/>
								<a href="javascript:void(select(document.getElementById('accessedAs'), true))"><ds:lang text="zaznacz"/></a>
								/
								<a href="javascript:void(select(document.getElementById('accessedAs'), false))"><ds:lang text="odznacz"/></a>
								<ds:lang text="wszystkie"/>
							</td>

			</tr>

			</ds:available>
			<ds:available test="repository.search.acceptance" documentKindCn="documentKindCn">
				<tr>
					<td><ds:lang text="UzytkownikAkceptujacyDokument"/></td>
					<td><ww:select name="'acceptedBy'" list="accessedByUsers" cssClass="'sel'"
									headerKey="''" headerValue="getText('select.dowolny')"
									listKey="name" listValue="asLastnameFirstnameName()" /></td>
				</tr>
			</ds:available>
            <ds:available test="repository.search.documentView">
                        				<script type="text/javascript">
                        								var _viewtimeFrom = "viewtimeFrom";
                        								var _viewtimeTo = "viewtimeTo";

                                                        //shit, to nie dziala!
                        								$j("#notViewed").click(function() {
                                                            if ($j("#notViewed").is(':checked')) {
                                                                $j("#viewtimeFrom").attr("disabled", "disabled");
                                                                $j("#viewtimeTo").attr("disabled", "disabled");
                                                            } else {
                                                                $j("#viewtimeFrom").removeAttr("disabled");
                                                                $j("#viewtimeTo").removeAttr("disabled");
                                                            }
                                                        });
                        							</script>
                                    <tr>
                                                    <td>
                                                         <ds:lang text="Dokumenty odczytane/nieodczytane"/>:
                                    				</td>
                                    				<td valign="top">
                                    				Nieodczytane <ww:checkbox id="notViewed" name="'notViewed'" fieldValue="'true'" /> przez:<br/>
                                    				<ww:select id="viewUsers" name="'viewUsers'" size="4" multiple="true" cssClass="'multi_sel'"
                                                    	list="viewedByUsers"
                                                    	listKey="name" listValue="asLastnameFirstnameName()" headerKey="-1" headerValue="test"/>
                                                    	od : <ww:textfield name="'viewtimeFrom'" id="viewtimeFrom" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_viewtimeFrom,_viewtimeTo,1)'"/>
                                                                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                                                                    id="calendar_viewtimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                                                                    title="Date selector" onmouseover="this.style.background='red';"
                                                                    onmouseout="this.style.background=''"/>
                                                        do : <ww:textfield name="'viewtimeTo'" id="viewtimeTo" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_viewtimeFrom,_viewtimeTo,1)'"/>
                                                                <img src="<ww:url value="'/calendar096/img.gif'"/>"
                                                                    id="calendar_viewtimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
                                                                    title="Date selector" onmouseover="this.style.background='red';"
                                                                    onmouseout="this.style.background=''"/>
                                                    <br/>
                                                    <a href="javascript:void(select(document.getElementById('viewUsers'), true))"><ds:lang text="zaznacz"/></a>
                                                    /
                                                    <a href="javascript:void(select(document.getElementById('viewUsers'), false))"><ds:lang text="odznacz"/></a>
                                                    <ds:lang text="wszystkie"/>

                                                    </td>
                                    </tr>
                                    <script type="text/javascript">
                                                                                        Calendar.setup({
                                                                                            inputField     :    "viewtimeFrom",     // id of the input field
                                                                                            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                                                                            button         :    "calendar_viewtimeFrom_trigger",  // trigger for the calendar (button ID)
                                                                                            align          :    "Tl",           // alignment (defaults to "Bl")
                                                                                            singleClick    :    true
                                                                                        });
                                                                                        Calendar.setup({
                                                                                            inputField     :    "viewtimeTo",     // id of the input field
                                                                                            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                                                                            button         :    "calendar_viewtimeTo_trigger",  // trigger for the calendar (button ID)
                                                                                            align          :    "Tl",           // alignment (defaults to "Bl")
                                                                                            singleClick    :    true
                                                                                        });


                                                                                    </script>

            </ds:available>
			<tr>
				<td>
					<ds:lang text="LiczbaWynikowNaStronie"/>:
				</td>
				<td>
					<ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }" value="limit > 0 ? limit : 50"/>
				</td>
			</tr>
            <ds:available test="saveSearch">
            <tr>
                <td>
                    <ds:lang text="saveSearchNameSzukaj"/>:
                </td>
                <td>
                    <ww:select name="'saveSearchName'" id="saveSearchNameID" list="saveSearchNames" value="saveSearchName" cssClass="'sel'" headerValue="getText('select.wybierz')" headerKey="''"/>
                </td>
            </tr>
            </ds:available>
		</table>
		
		<ds:submit-event value="getText('Znajdz')" name="'doSearch'" cssClass="'btn searchBtn'" disabled="documentKindCn == null" />
		<ds:submit-event name="'doClear'" cssClass="btn" value="getText('WyczyscWszystkiePola')"/>
<%--  
		<ww:if test="adminAccess && !documentKindCn.equals('normal')">
			<ds:submit-event value="getText('ZapiszPonownie')" name="'doArchive'" cssClass="'btn'"/>
		
		</ww:if>
--%>
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
		
		<script type="text/javascript">
			function dockindChanged(event)
			{
				var src = event.srcElement ? event.srcElement : event.target;
				var id = src.value;
				if (id == null || id == '') return true;

				document.getElementById('doChangeDockind').value = 'true';
				document.getElementById('form').submit();
			}

			function changeSearchStyle()
			{
				<ww:if test="#searchStyle == 'only-related'">
					document.getElementById('newSearchStyle').value = 'normal';
				</ww:if>
				<ww:else>
					document.getElementById('newSearchStyle').value = 'only-related';
				</ww:else>

				document.getElementById('doChangeStyle').value = 'true';
				document.getElementById('form').submit();
			}
		</script>
	</form>
</ww:if>

<ww:if test="!results.empty">
	<form id="form" action="<ww:url value="addDocsForCompilationUrl"/>" method="post">
		<ww:hidden name="'documentAspectCn'" id="documentAspectCn" value="documentAspectCn"/>
		
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

        <ds:ww-action-errors/>
        <ds:ww-action-messages/>

		<table>
		<tr>
		<td><b><ds:lang text="odnalezionoDokumentow"/></b>:</td>
		<td>
		<ww:property value="count"/>
		</td>
		</tr>
		</table>
		
		<ww:set name="pager" scope="request" value="pager"/>
		<ww:if test="!omitLimit">
			<table width="100%">
				<tr>
					<td <ds:available test="search.pager.center">align="center"</ds:available>>
						<jsp:include page="/pager-links-include.jsp"/>
					</td>
				</tr>
			</table>
		</ww:if>
		
		<table class="search" width="100%" cellspacing="0" id="mainTable">
			<tr>
				<%--	<ww:if test="canCompileAttachments">	--%>
				<th>
					&nbsp;
				</th>
				<%--	</ww:if>	--%>
				<ww:iterator value="columns" status="status" >
					<th>
						<nobr>
							<ww:if test="sortDesc != null">
								<%--	<a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
								<a name="<ds:lang text="SortowanieMalejace"/>" href="<ww:url value="sortDesc"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
									<ww:if test="((property == sortField) || (property == ('document_' + sortField))) && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
									<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
							</ww:if>
							<ww:property value="title"/>
							<ww:if test="sortAsc != null">
								<%--	<a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
								<a href="<ww:url value="sortAsc"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
									<ww:if test="((property == sortField) || (property == ('document_' + sortField))) && ascending == true"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
									<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
							</ww:if>
						</nobr>
					</th>
				</ww:iterator>
			</tr>
			
			<ww:iterator value="results" status="stat">
				<ww:set name="result"/>
				<tr <ww:if test="#stat.odd != true">class="oddRows"</ww:if>>
					<td>
						<ww:checkbox name="'documentIds'" fieldValue="document_id" id="check" /> 
					</td>

					<ww:iterator value="columns" status="status">
						<ww:if test="property == 'attachment_link1' and #result[property] != null">
							<td>
								<ww:if test="#result['attachment_link_vs'] != null && #result.canReadOrginalAttachments">
									<a title="<ds:lang text="Wyswietl"/>" href="javascript:openToolWindow('<ww:url value="#result['attachment_link_vs']"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'nwPdf', 1000, 750)">
										<img src="<ww:url value="'/img/wyswietl.gif'"/>" style="width: 17px; height: 17px;"/></a>
								</ww:if>
								<ww:if test="#result.canReadOrginalAttachments">
								<a title="<ds:lang text="Pobierz"/>" href="<ww:url value="#result[property]"/>">
									<img src="<ww:url value="'/img/pobierz.gif'"/>" style="width: 17px; height: 17px;"/></a>
								</ww:if>
							</td>
						</ww:if>
						<ww:elseif test="property == 'current_workflow_location'">
							<td>
								<a href="<ww:url value="#result['current_workflow_location_link']"/>"><ww:property value="prettyPrint(#result[property])"/></a>
							</td>
						</ww:elseif>
						<ww:else>
							<td>
								<a href="<ww:url value="#result['link']"/>">
									<ww:if test="(prettyPrint(#result[property])) == 'getText('nie');'">
										<span style="color:red;">
									</ww:if>
									<ww:if test="(prettyPrint(#result[property])) == 'getText('tak');'">
										<span style="color:green;">
									</ww:if>
									<ww:property value="prettyPrint(#result[property])"/>
									<ww:if test="((prettyPrint(#result[property])) == 'getText('tak');' || (prettyPrint(#result[property])) == 'getText('nie');')">
										</span>
									</ww:if>
								</a>
							</td>
						</ww:else>
						
					</ww:iterator>
				</tr>
			</ww:iterator>
			
			<tr height="10"></tr>
			<tr>
				<td style="text-align: left" colspan="<ww:property value="columns.size*2  + 1"/>">
					<ds:available test="!archiwum.disable.wyszukiwaniezaawansowane.zaznaczwszystko">
						<input type="checkbox" onclick="selectAll(this)"/>
						<ds:lang text="ZaznaczWszystkie/cofnijZaznaczenie"/>
					</ds:available>
				</td>
			</tr>
		</table>
		
		<ww:if test="!omitLimit">
			<table width="100%">
				<tr>
					<td <ds:available test="search.pager.center">align="center"</ds:available>>
						<jsp:include page="/pager-links-include.jsp"/>
					</td>
				</tr>
			</table>
		</ww:if>
		<ds:available test="addition.boxorder.available">
			<ww:if test="canOrder">
			<div id="ordersParam" style="display:none;">
				<table>
					<tr>
						<td>
							<h2>Zam�wienie</h2>
						</td>
					</tr>
				</table>
			</div>		
			<input id="orderBtn" type="button" value="Zam�w zaznaczone" onclick="orderDocs(); " class="btn"/>
			</ww:if>
		</ds:available>


		<ds:available test="canGenerateReports">
		<input type="hidden" name="doGenerateXls" id="doGenerateXls"/>
    		<input type="hidden" name="doGenerateXlsFromChecked" id="doGenerateXlsFromChecked"/>

		<input type="hidden" name="doGenerateCsv" id="doGenerateCsv"/>
       		<input type="hidden" name="doGenerateCsvFromChecked" id="doGenerateCsvFromChecked"/>

        <input type="hidden" name="doGenerateXml" id="doGenerateXml"/>
       		<input type="hidden" name="doGenerateXmlFromChecked" id="doGenerateXmlFromChecked"/>



		<select id="Action" name="action" class="sel">
        					<option value="">
        						<ds:lang text="wybierzAkcje"/>
        					</option>

                            <ww:if test="canGenerateXlsReport">
        					<ds:available test="tasklist.watches.generateXlsAll">
        							<option value="doGenerateXls">
        							<ds:lang text="GenerujZbiorczyRaportXLS"/>
        							</option>
                             </ds:available>	<ds:available test="tasklist.watches.generateXls">
        							<option value="doGenerateXlsFromChecked">
                                    <ds:lang text="GenerujRaportXLSzZaznaczonych"/>
                                    </option>

        					</ds:available>
                            </ww:if>

                            <ww:if test="canGenerateCsvReport">
        					<ds:available test="tasklist.watches.generateCsvAll">
        							<option value="doGenerateCsv">
        							<ds:lang text="GenerujZbiorczyRaportCSV"/>
        							</option>
                             </ds:available>	<ds:available test="tasklist.watches.generateCsv">

        							<option value="doGenerateCsvFromChecked">
                                    <ds:lang text="GenerujRaportCSVzZaznaczonych"/>
                                    </option>
        					</ds:available>
                            </ww:if>

                            <ww:if test="canGenerateXmlReport">
        					<ds:available test="tasklist.watches.generateXmlAll">
        							<option value="doGenerateXml">
        							<ds:lang text="GenerujZbiorczyRaportXML"/>
        							</option>
                             </ds:available>	<ds:available test="tasklist.watches.generateXml">
        							<option value="doGenerateXmlFromChecked">
                                    <ds:lang text="GenerujRaportXMLzZaznaczonych"/>
                                    </option>
        					</ds:available>
                            </ww:if>

        				</select>
        				<input type="submit" class="btn" value="<ds:lang text="Wykonaj"/>" onclick="selectActionNoCheck(this, document.getElementById('Action').value)"/>
        </ds:available>

		<ds:extras test="business">
		<%-- 
			<input type="button" class="btn" value="<ds:lang text="PobierzZalacznikiJakoPDF"/>"
				onclick="openToolWindow('<ww:url value="attachmentsAsPdfUrl"/>', 'asPdf')"/>
			 <input type="button" class="btn" value="<ds:lang text="PobierzZalacznikiZaznaczonychJakoPdf"/>"
				onclick="generatePdfPartially()"/>--%>
			<ds:available test="!archiwum.disable.wyszukiwaniezaawansowane.archiwizacjaGlobalna">
				<input type="hidden" name="moveToArchive" id="moveToArchive"/>
				<input type="hidden" name="removeDocument" id="removeDocument"/>
				<select id="actionGlobal" name="action" class="sel">
						<option value="">
							<ds:lang text="wybierzAkcje"/>
						</option>
						<ds:has-permission name="DOKUMENT_PRZENIES_DO_ARCHIWUM_GLOBALNA">
							<option value="moveToArchive">
								<ds:lang text="PrzeniesienieDoArchwium"/>
							</option>
						</ds:has-permission>
						<ds:has-permission name="DOKUMENT_USUN_GLOBALNA">
							<option value="removeDocument">
								<ds:lang text="UsuniecieDokumentu"/>
							</option>
						</ds:has-permission>
				</select>
				<input type="submit" value="<ds:lang text="Wyslij"/>" name="Wyslij" class="btn" onclick="if(!selectAction(this, document.getElementById('actionGlobal').value)) return false;"/>
			</ds:available>
			<!-- <input type="submit" value="<ds:lang text="PobierzZalacznikiJakoPDF"/>" name="doGeneratePdf" class="btn" onclick="setActionName('doGeneratePdf');"/>  -->
			<ds:available test="!archiwum.disable.wyszukiwaniezaawansowane.pobierzzalaczniki">
				<input type="submit" onclick="return generatePdfPartially();" value="<ds:lang text="PobierzZalacznikiZaznaczonychJakoPdf"/>" name="doGeneratePdfPartially" class="btn"/>
			</ds:available>	
			
			
		</ds:extras>



		<ww:if test="canGenerateXlsReport">
			<ds:available test="searchdockind.generateXlsFromAll">
			
				<ww:if test="documentKindCn == 'ald'">
					<input type="submit" value="<ds:lang text="GenerujRaportXLS"/>" name="doGenerateAldXls" class="btn" onclick="setActionName('doGenerateAldXls');"/>
				</ww:if>
				<ww:else>
					<input type="submit" value="<ds:lang text="GenerujRaportXLS"/>" name="doGenerateXls" class="btn" onclick="setActionName('doGenerateXls');"/>
				</ww:else>			 	 
			

			
			<!-- 
				<input type="submit" value="<ds:lang text="GenerujRaportXLSzZaznaczonych"/>" name="doGenerateXlsFromChecked" class="btn" onclick="setActionName('doGenerateXlsFromChecked');"/>
			 -->			
			
			<input type="submit" value="<ds:lang text="GenerujRaportXLSzZaznaczonych"/>" name="doGenerateXlsFromChecked" class="btn" onclick="if (!check()) return false; setActionName('doGenerateXlsFromChecked');"/>
		</ds:available>
		</ww:if>

		<ww:if test="canGenerateCsvReport">
			<ds:available test="searchdockind.generateCsvFromAll">
			
			<input type="submit" value="<ds:lang text="GenerujRaportCSV"/>" name="doGenerateCsv" class="btn" onclick="setActionName('doGenerateCsv');"/>
			
			<input type="submit" value="<ds:lang text="GenerujRaportCSVzZaznaczonych"/>" name="doGenerateCsvFromChecked" class="btn" onclick="if (!check()) return false; setActionName('doGenerateCsvFromChecked');"/>
		</ds:available>
		</ww:if>
		
			<ww:if test="canGenerateXmlReport">
			<ds:available test="searchdockind.generateXmlFromAll">
			
			<input type="submit" value="<ds:lang text="GenerujRaportXML"/>" name="doGenerateXml" class="btn" onclick="setActionName('doGenerateXml');"/>
			
			<input type="submit" value="<ds:lang text="GenerujRaportXMLzZaznaczonych"/>" name="doGenerateXmlFromChecked" class="btn" onclick="if (!check()) return false; setActionName('doGenerateXmlFromChecked');"/>
		</ds:available>
		</ww:if>



		<!-- 
		<ds:available test="multiChangeAttributes.wyszukiwarka">
			<ds:event name="'doMultiChangeAttributes'" value="getText('doMultiChangeAttributes')" cssClass="'btn'" onclick="'if (!check()) return false;'"/>
		</ds:available>
		-->

        <ds:available test="saveSearch">
            <ds:lang text="saveSearchName"/>:
            <ww:textfield name="'saveSearchName'" size="20" maxlength="20" cssClass="'txt'" value="saveSearchName"/>
            <input type="submit" value="<ds:lang text="SaveSearch"/>" name="doSaveSearch" class="btn" onclick="setActionName('doSaveSearch');"/>
        </ds:available>
		
		<ww:if test="isCanMultichange()">
			<ds:event name="'doMultiChangeAttributes'" value="getText('doMultiChangeAttributes')" cssClass="'btn'" onclick="'if (!check()) return false;'"/>
		</ww:if>
		
		
		<input type="hidden" name="doAction" id="doAction"/>
		<input type="hidden" name="actionName" id="actionName"/>
		<input type="hidden" name="documentIdsString" id="documentIdsString"/>
		
		<br /> <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
		<script type="text/javascript">
			//prepareTable(E("mainTable"),1,1);
			
			var prefix = "check";
			tableId="mainTable";
			
			prepareCheckboxes();
		</script>
		
	</form>
</ww:if>

<script>

	$j("#docId").blur(function() {
	    if ($j(this).val() != '')
	        $j("#officeNumber").attr("disabled", "disabled");
	    else
	        $j("#officeNumber").removeAttr("disabled");
	});
		
	function orderDocs()
	{
		var tabObj = document.getElementById('ordersParam');
		var btnObj = document.getElementById('orderBtn');			
		tabObj.style.display = '';
		btnObj.style.display = 'none';
	}

	function check()
	{
		var x = document.getElementsByName('documentIds');
		var selected=false;
		for(i=0;i<x.length;i++ )
		{
			if(x.item(i).checked)
			{
				selected=true;
			}
		}
		if(!selected)
		{
			alert('<ds:lang text="NieWybranoDokumentow"/>');
			return false;
		}
		return true;		
	}
	
	function generatePdfPartially()
	{
		document.getElementById('doAction').value = false;
		var x = document.getElementsByName('documentIds');
		var i=0;
		var selected=false;
		var str = '';
		for(i=0;i<x.length;i++ )
		{
			if(x.item(i).checked)
			{
				str = str + x.item(i).value + ",";
				selected=true;
			}
		}
		
		if(!selected){
			alert('<ds:lang text="NieWybranoDokumentow"/>');
			return false;
		}
		else{
			document.getElementById('documentIdsString').value=str;
			return true;
		}
	}
	
	 function selectAction(btn, act)
	 {
		 if(!check())
			 return false;
		 
		 setActionName(act);
		 return true;
	 }

    function selectActionNoCheck(btn, act)
    	 {

    		 setActionName(act);
    		 return true;
    	 }

	
	function setActionName(an)
	{
		document.getElementById('doAction').value = true;
		document.getElementById('actionName').value = an;
	}
	function selectAll(checkbox)
	{
		<ww:if test="results != null">
			var boxes = document.getElementsByName('documentIds');
		</ww:if>
		if (boxes.length > 0)
		{
			for (var i=0; i < boxes.length; i++)
			{
				boxes.item(i).checked = checkbox.checked;
			}
		}

		<ds:available test="layout2">
        Custom.clear();
        </ds:available>
	}

</script>

 <ds:available test="project.parametrization.ima">
     <ww:set name="typeIma" value="'search-dockind'"/>
    <jsp:include page="/parametrization/ima/left-menu.jsp"/>
 </ds:available>

<ds:available test="searchDockindDocuments.ilpl">
<script type="text/javascript">

$j(document).ready(function() {
    var filterOptions = (function() {
        var allOptions = [];
        $j('#dockind_ILPOL_KIND option').each(function() {
            allOptions.push({
                value: Number($j(this).val()),
                title: $j(this).text()
            });
        });

        var dockind = $j('#documentKindCn').val();
        var clientValues, nonClientValues;

        if(dockind == 'normal') {
        	clientValues = [0,2,6,10,14,16,18,22,26,28,30,32,34,36,38,40,42,44,46,48,50,54,55];
        	nonClientValues = [0,4,8,12,15,20,24,35,52,55];
        } else if(dockind == 'normal_out') {
        	clientValues = [0,1,2,3,4,35,6,7,8,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,40,41,42,43,44,45,46,48,51,52,53,54,60,61];
        	nonClientValues = [0,36,37,38,39,47,49,50,55,56,57,58,59,60];
        }


        return function(bClient) {
        	$j('#dockind_ILPOL_KIND option').remove();

        	var jqIlpolKind = $j('#dockind_ILPOL_KIND');
        	var i, len, curOption, validOption = false;
        	for(i = 0, len = allOptions.length; i < len; i ++) {
        		curOption = allOptions[i];
        		validOption = false;

        		if(bClient === true && ($j.inArray(curOption.value, clientValues) != -1)) {
        			validOption = true;
        		} else if(bClient === false && ($j.inArray(curOption.value, nonClientValues) != -1)) {
        			validOption = true;
        		} else if(bClient === null) {
        			validOption = true;
        		}

        		if(validOption) {
        			//console.info('adding', curOption.title);
        			jqIlpolKind.append('<option value="' + curOption.value + '">' + curOption.title + '</option>');
        		}
        	}

            /*console.info(bClient);
            console.dir(allOptions);*/
        }
    })();

    $j('#dockind_ILPL_CLIENT').bind('change', function() {
    	// console.info('change');
        var bClient = null,
            val = $j(this).val();
        if(val == 'true')
            bClient = true;
        else if(val == 'false')
            bClient = false;

        filterOptions(bClient);
    });
});

</script>
</ds:available>
