<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'binderId'" id="binderId"/>
<ww:hidden name="'contractId'" id="contractId"/>
<ww:hidden name="'dostawcaId'" id="dostawcaId" value="dostawcaId"/>
<ww:hidden name="'applicationId'" id="applicationId"/>
<ww:hidden name="'customerName'" value="customerName" id="customerName"/>
<ww:hidden name="'oldType'" id="oldType" />
<ww:hidden name="'idUmowyPrzekazane2'" id="idUmowyPrzekazane2" value="idUmowyPrzekazane"/>
<ww:if test="showPage != 2">
	<ww:hidden name="'contractorKind'" value="contractorKind"/>
</ww:if>
<ww:if test="results != null || showPage == 1">
    <table>
        <tr>H
            <td colspan="2">
                <ds:lang text="Nazwa"/>
            </td>
            <td colspan="2">
                <ds:lang text="NIP"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ww:textfield name="'nazwa'" id="nazwa" size="40" maxlength="50" cssClass="'txt'"/>
            </td>
            <td colspan="2">
                <ww:textfield name="'nip'" id="nip" size="10" maxlength="10" cssClass="'txt'"/>
            </td>
            <td>
                <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
            </td>
        </tr>
    </table>
    <table class="search" width="800" cellspacing="0" id="mainTable">
        
        <tr>
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'name'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Nazwa"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'name'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'nip'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Nip"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'nip'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>
			
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'city'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Miasto"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'city'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

    		<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'street'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Ulica"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'street'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>
			
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Telefon"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>

    	</tr>
        <ww:iterator value="results">
            <tr>
                <td>
                    <a href="<ww:url value='&apos;/crm/crmContactor.action&apos;'/>?id=<ww:property value="id"/>"><ww:property value="name"/></a>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
                <td>
                    <ww:property value="nip"/>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
                <td>
                    <ww:property value="city"/>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="street"/>
				</td>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="phoneNumber"/>
				</td>
				<td>
					<a href="<ww:url value="'/repository/new-portfolios.action'"><ww:param name="'id'" value="id"/><ww:param name="'doNew'" value="true"/></ww:url>">
                        <ds:lang text="wybierz"/></a>
                </td>
        	</tr>
    	</ww:iterator>
	</table>

   <ww:set name="pager" scope="request" value="pager"/>
   <table width="100%">
      <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
   </table>
</ww:if>
<ww:elseif test="showPage == 2">
<table>
            <tr>
				<td>
					<ds:lang text="AbyDodacNowyZestawDokumentowProszeDodacNumerUmowyLubWnioskuAnastepnieWybracPlikTiff"/>
				</td>
			</tr>
</table>
<table>
            <tr>
				<td>
					<ds:lang text="NumerUmowy"/>
				</td>
				<td>
					<ds:lang text="NumerWniosku"/>
				</td>
			</tr>
			<tr>
				<td>
					<ww:select id="idUmowy" name="'idUmowy'" list="contractsIdsMap" value="idUmowy"  multiple="true"
					cssStyle="'width: 250px;'" size="5"	cssClass="'sel'" />
				</td>
				<td>
					<ww:textfield name="'applicationName'" id="application" />
				</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
			  	<td>
					<!--<input type="button" value="<ds:lang text="Dodaj"/>" onclick="openDictionary_contract()" class="btn"/>-->
				</td>
				<td>
					<input type="button" value="<ds:lang text="Dodaj"/>" onclick="openDictionary_application()" class="btn"/>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					Rodzaj kontrahenta : <ww:select name="'contractorKind'" list="contractorkinds" listKey="id" listValue="title" cssClass="'sel'"/>
				</td>
			</tr>
</table>
<table >
		<tr>
            <td>
            	<ds:lang text="Plik"/><span class="star">*</span>:
          	</td>
       </tr>
       <tr>
               <td>
             		<ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'"/>
             	</td>
          </tr>
          <tr>
          	<td>
          		<table id="kontener">
					<tbody id="tabela_glowna"> 
					</tbody>           
				</table>
          	</td>            	
          </tr>
          <tr>
		<td>
			<a href="javascript:addAtta();"><ds:lang text="DolaczKolejny"/></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun"/></a>
		</td>
	</tr>
          <tr>
          	<td>
          		<ds:event value="'Pobierz'" name="'doNew'" cssClass="'btn'" onclick="'return checkSubmit(this);'"/>
          	</td>
          </tr>         
</table>
<script type="text/javascript">
	var countAtta = 1;
	selectAllOptions(document.getElementById('idUmowy'));
	function addAtta()
	{
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.innerHTML = '<input type="file" name="multiFiles'+countAtta+'" size="30" id="multiFiles'+countAtta+'" class="txt"/>';
		countAtta++;
		tr.appendChild(td);	
		var kontener = document.getElementById('tabela_glowna');
		kontener.appendChild(tr);
		
	}
	
	function delAtta()
	{
		var x = document.getElementById('tabela_glowna');
		var y = x.rows.length;
  		x.deleteRow(y-1) 
	}
	
	
	function checkSubmit(btn)
	{
		if(document.getElementById('contractId').value == '' && document.getElementById('applicationId').value == '')
		{
			alert('<ds:lang text="NalezyWybracUmoweLubWniosek"/>');
			return false;
		}
		sendEvent(btn);
		return true;
	}

    function openDictionary_contract()
    {
    
        openToolWindow('<ww:url value="dictionaryContractAction"></ww:url>?numerUmowy='+document.getElementById('contract').value,
                       '<ww:property value="cn"/>', 700, 650);
    }
    
    function openDictionary_application()
    {
        openToolWindow('<ww:url value="dictionaryApplicationAction"></ww:url>?numerWniosku='+document.getElementById('application').value,
                       '<ww:property value="cn"/>', 700, 650);
    }
    
    function accept(param,map)
    {
    	if(map.typ == 'Contract')
    	{
    		document.getElementById('contract').value = map.numerUmowy;
    		document.getElementById('contractId').value = map.id;
    	}
    	else
    	{
       		document.getElementById('application').value = map.numerWniosku;
       		document.getElementById('applicationId').value = map.id;
       		
       	}
    }
    
    function getId_KLIENT()
    {
    	return document.getElementById('id').value;
    }
    
   	function getdockind_KLIENTname()
   	{
   		return document.getElementById('customerName').value;
   	}
    
</script>
</ww:elseif>
<ww:elseif test="showPage == 3">
<ww:iterator value="types">
	<input type="hidden" name="<ww:property value="cn"/>" id="<ww:property value="cn"/>"/>
</ww:iterator>
  <table>
	<tr>
		<td>
			<ww:property value="contractor.name"/>
			<ww:property value="contractor.nip"/>
			<ww:property value="contractor.contractorno"/>
			<ww:property value="contractor.city"/>
			<ww:property value="contractor.street"/>
		</td>
	</tr>
	<tr>
		<td>
		Numer umowy/wniosku : 
		<ww:property value="contractName"/>
		</td>
	</tr>
</table>
<table>
	<tr>
		<td colspan="4">
			<ww:select id="type" name="'type'" list="types" value="titel"
			listKey="cn" listValue="title" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" onchange="'changeTypes();'" />
		</td>
	</tr>
		<tr>
            <td valign="top">
                <ww:select id="filesS" name="'filesS'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 100px;'"
                    list="files" onchange="'showImage(this)'"/>
            </td>
			<ww:iterator value="types">
	            <td valign="middle" id="button_<ww:property value="cn"/>" style="display:none;">
	                <input type="button" value=" &gt;&gt; "
	                    onclick="selectOptions(this.form.filesS, this.form.file_<ww:property value="cn"/>,'<ww:property value="cn"/>');"
	                    class="btn"/>
	                <br/>
	                <input type="button" value=" &lt;&lt; "
	                    onclick="selectOptions(this.form.file_<ww:property value="cn"/>, this.form.filesS,'<ww:property value="cn"/>');"
	                    class="btn"/>
	                <br/>
	            </td>
	            <td valign="top" id="sel_<ww:property value="cn"/>" style="display:none;">
	                <table width="100%">
	                    <tr>
	                        <td rowspan="2">
								<select name="file_<ww:property value="cn"/>" size="10" id="file_<ww:property value="cn"/>"
								 multiple="multiple" class="multi_sel" style="width: 100px;" onclick="showImage(this);">
								</select>
	                        </td>
	                    </tr>
	                </table>
	            </td>
			</ww:iterator>
			<td rowspan="3">
        			<img id="fileimage" ondblclick="openImage();" name="fileimage" src="<ww:url value="'/img/loadPage.gif'"/>" alt="Wybierz stron� by wyswietli� zawarto��" style="border: 2px solid black;"   height="606" width="430"/> 
        	</td>
		</tr>
	    <tr>
           	<td>
           		<ww:hidden name="'doSave'" id="doSave"/>
           		<input type="button" onclick="zapisz(this);" value="Zapisz" class="btn" id="doSaveBTN"/>
           	</td>
        </tr>
        <tr height="400">
        	<td></td>
        </tr>
  </table>
<script language="JavaScript">


	function changeTypes()
    {
    	var element;
    	var oldTypeObj = document.getElementById('oldType');
    	var selectTypeObj = document.getElementById('type');
    	
    	if(oldTypeObj == null || oldTypeObj.value.length < 1 )
    	{
    		element = document.getElementById('button_'+selectTypeObj.value);
			element.style.display = '';
			element = document.getElementById('sel_'+selectTypeObj.value);
			element.style.display = '';
			
			oldTypeObj.value = selectTypeObj.value;
    	}
    	else
    	{
    		
    		element = document.getElementById('button_'+selectTypeObj.value);
    		element.style.display = ''
    		element = document.getElementById('sel_'+selectTypeObj.value);
    		element.style.display = '';
    		
    		element = document.getElementById('button_'+oldTypeObj.value);
    		element.style.display = 'none';
    		element = document.getElementById('sel_'+oldTypeObj.value);
    		element.style.display = 'none';
    		
    		oldTypeObj.value = selectTypeObj.value;
    		
    	}
    	
    }

	function selectOptions(select1,select2,cn)
	{
		var count = moveOptions(select1,select2);
		var ilosc;
		if(select1.name == 'filesS')
		{
			ilosc = select2.length;
		}
		else
		{
			ilosc = select1.length;
		}
		if(ilosc > 0)
		{
			var selObj = document.getElementById('type');
			for (var i=0; i < selObj.length; i++)
	    	{
	        	if( selObj.options[i].value == cn)
	        	{
	        		var tmp = selObj.options[i].text;
	        		var pozycja = tmp.indexOf('*')
	        		if( pozycja < 0 )
	        		{
	        			selObj.options[i].text = tmp + '*';
	        		}
	        	}
	    	}
	    }
	    else
	    {
	    	var selObj = document.getElementById('type');
			for (var i=0; i < selObj.length; i++)
	    	{
	        	if( selObj.options[i].value == cn)
	        	{
	        		var tmp = selObj.options[i].text;
	        		var pozycja = tmp.indexOf('*')
	        		if( pozycja > 0 )
	        		{
	        			tmp = tmp.substring(0,pozycja);
	        			selObj.options[i].text = tmp;
	        		}
	        	}
	    	}
	    }
	}

	function showImage(select)
	{
		var fileimageObj = E('fileimage');
		var sel = typeof select == 'string' ? E(select) : select;
		for (var i=0; i < sel.length; i++)
		{
        	if( sel.options[i].selected == true)
       		{
       			var tmp = sel.options[i].value;
       			var tab = tmp.split('\\');
       			var tmp = '<c:out value='${pageContext.request.contextPath}'/>/tempPng/'+tab[tab.length - 1];
       			tmp += '.png';
       			fileimageObj.src = tmp;	
       		}
        }
	}
	
	function openImage()
	{

		var fileimageObj = E('fileimage');
		var nowe_okno = window.open();
		nowe_okno.document.write('<HTML><HEAD></HEAD><BODY>ad <IMG SRC="'+fileimageObj.src+'" width="100%"></BODY></HTML>');
	}
	
	function selectFirst()
	{
		var fileimageObj = E('fileimage');
		var sel = E('filesS');
		var sel2 = document.getElementsByName('filesS');
		var tmp = sel.options[1].value;
		var tab = tmp.split('\\');
		var tmp = tab[tab.length - 1];
		tmp += '.png';
		fileimageObj.src = tmp;	
	}
	selectFirst();
    
    function zapisz(btn)
    {
		var el = document.getElementById('doSaveBTN');
        el.disabled = true;        
        document.getElementById('doSave').value = true;
    	<ww:iterator value="types">
    		  var txtSelectedValuesObj<ww:property value="cn"/> = document.getElementById('<ww:property value="cn"/>');
			  var selectedArray<ww:property value="cn"/> = new Array();
			  var selObj<ww:property value="cn"/> = document.getElementById('file_<ww:property value="cn"/>');
			  var i<ww:property value="cn"/>;
			  var count<ww:property value="cn"/> = 0;
			  for (i=0; i<selObj<ww:property value="cn"/>.options.length; i++)
			  {
			  //  if (selObj<ww:property value="cn"/>.options[i].selected) 
			    {
			      selectedArray<ww:property value="cn"/>[count<ww:property value="cn"/>] = selObj<ww:property value="cn"/>.options[i].value;
			      count<ww:property value="cn"/>++;
			    }
			  }
			  txtSelectedValuesObj<ww:property value="cn"/>.value = selectedArray<ww:property value="cn"/>;	
		</ww:iterator>

		document.forms[0].submit();
        return true;
    }
    
</script>
</ww:elseif>
<ww:elseif test="showPage == 4">
<script language="JavaScript">
window.opener.document.forms[0].submit();
</script>
</ww:elseif>
<script language="JavaScript">

    prepareTable(E("mainTable"),2,0);
    
</script>


