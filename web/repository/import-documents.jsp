<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N import-documents.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1><ds:lang text="ImportDokumentow"/></h1>


<ww:if test="!tabs.empty">
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:available test="!layout2">
<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
<ww:iterator value="tabs" status="status" >
	
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
</div>
</ds:available>

</ww:if>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="canImport">
    <form action="<ww:url value="'/repository/import-documents.action'"/>" method="post" enctype="multipart/form-data">
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
        <table class="formTable">
            <tr>
                <td><ds:lang text="TypImportu"/></td>
                <td>
                    <ww:select id="importType" name="'importType'" list="importTypes" cssClass="'sel'" />
                </td>
            </tr>
            <tr>
                <td><ds:lang text="PlikZdanymiOimporcie"/></td>
                <td><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></td>
            </tr>
            <tr>
                <td><ds:lang text="KatalogNaSerwerzeZplikamiTif"/></td>
                <td><ww:textfield name="'tiffDirectory'" value="tiffDirectory" cssClass="'txt'" size="50" id="tiffDirectory" /></td>
            </tr>
            <tr class="formTableDisabled">
                <td></td>
                <td><ds:event name="'doAddFile'" value="getText('DodajPlik')"/></td>
            </tr>
        </table>

        <p>&nbsp;</p>

        <ww:if test="importedFiles != null && importedFiles.size() > 0">
            <table class="formTable">
                <tr class="formTableDisabled">
                    <th><ds:lang text="NazwaPliku"/></th>
                    <th><ds:lang text="DataWczytania"/></th>
                    <th><ds:lang text="DataPrzetworzenia"/></th>
                    <th><ds:lang text="Status"/></th>
                    <th><ds:lang text="LiczbaDokumentow"/></th>
                    <th><ds:lang text="Przetworzono"/></th>
                    <th><ds:lang text="Blednych"/></th>
                    <th></th>
                </tr>
                <ww:iterator value="importedFiles">
                <ww:if test="iteratorValue < 20">
                    <ww:set name="loadTime" value="ctime" scope="page"/>
                    <ww:if test="endTime != null">
                         <ww:set name="endTime" value="endTime" scope="page"/>
                    </ww:if>
                    <ww:else>
                        <ww:set name="endTime" value="''" scope="page"/>
                    </ww:else>
                    <tr>
                        <td><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <ww:property value="fileName"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <fmt:formatDate value="${loadTime}" type="both" pattern="dd-MM-yy HH:mm"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <fmt:formatDate value="${endTime}" type="both" pattern="dd-MM-yy HH:mm"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <ww:property value="status"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td align="center"><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <ww:property value="documentsNo"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td align="center"><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <ww:property value="processedDocumentsNo"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td align="center"><ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                        <ww:property value="badDocumentsNo"/>
                        <ww:if test="badDocumentsNo > 0"></span></ww:if></td>
                        <td>
                        	<ww:if test="badDocumentsNo > 0"><span class="warning"></ww:if>
                            <ww:if test="canStartImport">
                                &nbsp;
                                <%-- link znika, poniewa� kilkukrotne klikni�cie powoduje b��d --%>
                                <div id="startImportDiv_<ww:property value="id"/>" style="display: block">
                                    <a href="<ww:url value="'/repository/import-documents.action'"><ww:param name="'doStartImport'" value="true"/><ww:param name="'importedFileId'" value="id"/></ww:url>"
                                    onclick="document.getElementById('startImportDiv_<ww:property value="id"/>').style.display='none';"><ds:lang text="Importuj"/></a>
                                </div>
                            </ww:if>
                            <ww:if test="canGenerateReport"><a href="<ww:url value="'/repository/import-documents.action'"><ww:param name="'doGenerateReport'" value="true"/><ww:param name="'importedFileId'" value="id"/></ww:url>"><ds:lang text="PobierzRaport"/></a></ww:if>
                            <ww:if test="badDocumentsNo > 0"></span></ww:if>
                        </td>
                    </tr>
                </ww:if>
                </ww:iterator>
            </table>
        </ww:if>
        
        <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
    </form>
</ww:if>