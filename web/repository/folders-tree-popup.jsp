<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N folders-tree-popup.jsp N-->

<%@ page import="java.io.IOException,
                 java.util.*,
                 org.apache.commons.logging.Log,
                 org.apache.commons.logging.LogFactory,
                 pl.compan.docusafe.util.XmlUtils,
                 pl.compan.docusafe.core.users.auth.AuthUtil,
                 pl.compan.docusafe.core.security.AccessDeniedException,
                 pl.compan.docusafe.core.base.Folder,
                 pl.compan.docusafe.core.base.FolderNotFoundException,
                 pl.compan.docusafe.core.EdmException,
                 pl.compan.docusafe.core.security.AccessDeniedException,
                 pl.compan.docusafe.core.DSContext,
                 pl.compan.docusafe.util.StringManager,
                 pl.compan.docusafe.core.GlobalPreferences,
                 pl.compan.docusafe.core.DSApi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<html>
<head>
    <title><ds:lang text="TytulStrony"/></title>
    <script language="JavaScript">
    function pickFolder(id, prettyPath, lparam)
    {
        window.opener.pickFolder(id, prettyPath, lparam);
        window.close();
    }
    /*
    function escapeXml(s)
    {
        var xml = '';
        for (var i=0; i < s.length; i++)
        {
            var c = s.charAt(i);
            switch(c)
            {
                case '\'': xml += '&apos;'; break;
                case '"': xml += '&quot;'; break;
                case '<': xml += '&lt;'; break;
                case '>': xml += '&gt;'; break;
                case '&': xml += '&amp;'; break;
                default: xml += c;
            }
        }
        return xml;
    }
    */
    </script>
<style>
body {
	/*background-color: #D6CFC4;*/
	background-color: white;
}
a {
	color: black;
	text-decoration: none;
    vertical-align: middle;
	font: bold 12px/15px Verdana, Geneva, Arial, Helvetica, sans-serif;
	line-height: 15px;
	height: 15px;
}
</style>
</head>
<body>
<%
    Log log = LogFactory.getLog("jsp.repository.folders-tree-popup");

    String lparam = request.getParameter("lparam");

    // identyfikator aktualnie wybranego (rozwini�tego folderu)
    Long id = null;
    try
    {
        id = new Long(request.getParameter("id"));
    }
    catch (Exception e)
    {
    }

    DSContext ctx = null;
    try
    {
        ctx = DSApi.open(AuthUtil.getSubject(request));

        Folder target;
        try
        {
            if (id != null)
                target = Folder.find(id);
            else
                target = Folder.getRootFolder();
        }
        catch (FolderNotFoundException e)
        {
            target = Folder.getRootFolder();
        }

        //request.setAttribute("targetFolder", target);

        // �cie�ka prowadz�ca do wybranego folderu
        List expanded = new LinkedList();
        Folder folder = target;
        //Folder splitter = null;
        do
        {
            expanded.add(folder);
/*
            if (folder.parent().isRoot())
                splitter = folder;
*/
            folder = folder.findFirstReadableAncestor();
        }
        while (folder != null);
        //Collections.reverse(expanded);

/*
        request.setAttribute("expanded", expanded);
        request.setAttribute("splitter", splitter);
        request.setAttribute("root", ctx.getDocumentHelper().getFolder(new Long(0)));
*/

        Folder root = Folder.getRootFolder();

        out.print("<img src='"+request.getContextPath()+"/img/folder-open.gif' width='16' height='13'>");
        out.print("&nbsp;");
        out.print("<a href='"+request.getContextPath()+"/repository/folders-tree-popup.jsp?id="+root.getId()+"&lparam="+(lparam != null ? lparam : "")+"'>");
        out.print(root.getTitle());
        out.print("</a>");
        out.print("<br>");

        printFolders(out, request.getContextPath(), root.getChildFolders(),
            id, expanded, 20 /*margin*/, lparam);
    }
    catch (AccessDeniedException e)
    {
    	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
        String tekst = sm.getString("BrakPrawaOdczytuGlownegoFolderu");
        out.print("<p>" + tekst + "</p>");
    }
    catch (EdmException e)
    {
        log.error("", e);
        request.setAttribute("exception", e);
    }
    finally
    {
        DSApi._close();
    }

    /*
        wypisywanie folder�w przez printFolders(Collection, expanded)

        printFolders(root.getPropertChildren(), expanded)

        out.print("<img src=folder.png>");
        out.print(folder.getTitle());
        if (expanded.contains(folder))
            printFolders(folder.getChildren(), expanded)
    */

%>
<%!
    /**
     * Rekurencyjnie wypisuje drzewo folder�w. Rozwijane s� tylko foldery,
     * kt�re znajduj� si� na li�cie expanded.
     * @param out
     * @param contextPath
     * @param folders Lista folder�w do wypisania.
     * @param expanded Lista folder�w, kt�re powinny zosta� rozwini�te.
     * @param margin Lewy margines w pikselach, jaki powinna mie� lista folder�w.
     * @throws IOException
     */
    private void printFolders(JspWriter out, String contextPath, Collection folders,
                              Long chosenId, List expanded, int margin, String lparam)
    throws IOException, EdmException
    {
    	StringManager sm = GlobalPreferences.loadPropertiesFile("",null);
        out.print("<div style='margin-left: "+margin+"px;'>");
        for (Iterator iter=folders.iterator(); iter.hasNext(); )
        {
            Folder child = (Folder) iter.next();
            boolean open = expanded.contains(child);

            out.print("<nobr>");
            if (open)
            {
                out.print("<img src='"+contextPath+"/img/folder-open.gif' width='16' height='13'>");
            }
            else
            {
                out.print("<img src='"+contextPath+"/img/folder-closed.gif' width='15' height='13'>");
            }
            out.print("&nbsp;");
            out.print("<a href='"+contextPath+"/repository/folders-tree-popup.jsp?id="+child.getId()+"&lparam="+(lparam != null ? lparam : "")+"'>");
            if (child.getId().equals(chosenId)) out.print("<b>");
            out.print(child.getTitle());
            if (child.getId().equals(chosenId)) out.print("</b>");
            out.print("</a>");
            if (child.canCreate())
                out.print("&nbsp;<small>(<a href=\"javascript:void(pickFolder("+child.getId()+
                    ", '"+XmlUtils.escapeXml(child.getPrettyPath())+"', '"+XmlUtils.escapeXml(lparam != null ? lparam : "")+"'))\">"+sm.getString("wybierz")+"</a>)</small>");
            out.print("</nobr>");
            out.print("<br>");

            if (open && child.hasChildFolders())
            {
                printFolders(out, contextPath, child.getChildFolders(), chosenId, expanded, margin, lparam);
            }
        }
        out.print("</div>");
    }
%>
</body>
</html>
