<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'binderId'" id="binderId"/>
<ww:hidden name="'contractId'" id="contractId"/>
<ww:hidden name="'dostawcaId'" id="dostawcaId" value="dostawcaId"/>
<ww:hidden name="'applicationId'" id="applicationId"/>
<ww:hidden name="'customerName'" value="customerName" id="customerName"/>
<ww:hidden name="'oldType'" id="oldType" />
<ww:hidden name="'idUmowyPrzekazane2'" id="idUmowyPrzekazane2" value="idUmowyPrzekazane"/>

<h1><ds:lang text="ModyfikacjaZalacznikow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form enctype="multipart/form-data" action="<ww:url value="'/repository/split-tiff.action'"/>" method="post" >

<ww:if test="showPage == 1">
<table>
		<tr>
            <td>
            	<ds:lang text="Plik"/><span class="star">*</span>:
          	</td>
       </tr>
       <tr>
               <td>
             		<ww:file name="'multiFiles'" id="multiFiles" size="30" cssClass="'txt'"/>
             	</td>
          </tr>
          <tr>
          	<td>
          		<table id="kontener">
					<tbody id="tabela_glowna"> 
					</tbody>           
				</table>
          	</td>            	
          </tr>
          <tr>
		<td>
			<a href="javascript:addAtta();"><ds:lang text="DolaczKolejny"/></a>&nbsp;/&nbsp;<a href="javascript:delAtta();"><ds:lang text="Usun"/></a>
		</td>
	</tr>
          <tr>
          	<td>
          		<ds:event value="'Pobierz'" name="'doSplit'" cssClass="'btn'" onclick="'return checkSubmit(this);'"/>
          	</td>
          </tr>         
</table>
<script type="text/javascript">
	var countAtta = 1;
	selectAllOptions(document.getElementById('idUmowy'));
	function addAtta()
	{
		var tr = document.createElement("tr");
		var td = document.createElement("td");
		td.innerHTML = '<input type="file" name="multiFiles'+countAtta+'" size="30" id="multiFiles'+countAtta+'" class="txt"/>';
		countAtta++;
		tr.appendChild(td);	
		var kontener = document.getElementById('tabela_glowna');
		kontener.appendChild(tr);
		
	}
	
	function delAtta()
	{
		var x = document.getElementById('tabela_glowna');
		var y = x.rows.length;
  		x.deleteRow(y-1) 
	}
	
	
	function checkSubmit(btn)
	{
	    var filePresent = false;
	    $j('[id^=multiFiles]').each(function() {
            if($j(this).val() != '') {
                filePresent = true;
            }
        });
        if(filePresent == false) {
            alert('<ds:lang text="NalezyZaladowacPrzynajmniejJedenPlik" />');
            return false;
        }
		sendEvent(btn);
		return true;
	}

    function openDictionary_contract()
    {
    
        openToolWindow('<ww:url value="dictionaryContractAction"></ww:url>?numerUmowy='+document.getElementById('contract').value,
                       '<ww:property value="cn"/>', 700, 650);
    }
    
    function openDictionary_application()
    {
        openToolWindow('<ww:url value="dictionaryApplicationAction"></ww:url>?numerWniosku='+document.getElementById('application').value,
                       '<ww:property value="cn"/>', 700, 650);
    }
    
    function accept(param,map)
    {
    	if(map.typ == 'Contract')
    	{
    		document.getElementById('contract').value = map.numerUmowy;
    		document.getElementById('contractId').value = map.id;
    	}
    	else
    	{
       		document.getElementById('application').value = map.numerWniosku;
       		document.getElementById('applicationId').value = map.id;
       		
       	}
    }
    
    function getId_KLIENT()
    {
    	return document.getElementById('id').value;
    }
    
   	function getdockind_KLIENTname()
   	{
   		return document.getElementById('customerName').value;
   	}
    
</script>
</ww:if>
<ww:elseif test="showPage == 2">
<table>
    <tr>
        <th>Lista stron:</th>
        <th></th>
        <th>Wybrane strony:</th>
        <th></th>
    </tr>
    <tr>
        <td valign="top">
            <ww:select id="allFiles" name="'filesS'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 100px;'"
                list="files" onchange="'showImage(this)'"/>
        </td>
        <td class="alignMiddle">
            <input type="button" value=" &gt;&gt; " name="moveRightButton"
                onclick="if (moveOptions($j('#allFiles')[0], $j('#selectedFiles')[0]) > 0) dirty=true;" class="btn"/>
            <br/>
            <input type="button" value=" &lt;&lt; " name="moveLeftButton"
                onclick="if (moveOptions($j('#selectedFiles')[0], $j('#allFiles')[0]) > 0) dirty=true;" class="btn"/>
        </td>
        <td  valign="top" >
             <ww:select id="selectedFiles" name="'selectedFiles'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 100px;'"
                            list="selectedFiles" onchange="'showImage(this)'"/>
        </td>
        <td rowspan="3">
                <img id="fileimage" ondblclick="openImage();" name="fileimage" src="<ww:url value="'/img/loadPage.gif'"/>" alt="Wybierz stron� by wyswietli� zawarto��" style="border: 2px solid black;"   height="606" width="430"/>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <ww:hidden name="'doSave'" id="doSave"/>
            <input type="button" onclick="zapisz(this);" value="Zapisz" class="btn" id="doSaveBTN"/>
        </td>
    </tr>
    <tr height="400">
        <td colspan="4"></td>
    </tr>
  </table>
</form>
<script language="JavaScript">


	function changeTypes()
    {
    	var element;
    	var oldTypeObj = document.getElementById('oldType');
    	var selectTypeObj = document.getElementById('type');
    	
    	if(oldTypeObj == null || oldTypeObj.value.length < 1 )
    	{
    		element = document.getElementById('button_'+selectTypeObj.value);
			element.style.display = '';
			element = document.getElementById('sel_'+selectTypeObj.value);
			element.style.display = '';
			
			oldTypeObj.value = selectTypeObj.value;
    	}
    	else
    	{
    		
    		element = document.getElementById('button_'+selectTypeObj.value);
    		element.style.display = ''
    		element = document.getElementById('sel_'+selectTypeObj.value);
    		element.style.display = '';
    		
    		element = document.getElementById('button_'+oldTypeObj.value);
    		element.style.display = 'none';
    		element = document.getElementById('sel_'+oldTypeObj.value);
    		element.style.display = 'none';
    		
    		oldTypeObj.value = selectTypeObj.value;
    		
    	}
    	
    }

	function selectOptions(select1,select2,cn)
	{
		var count = moveOptions(select1,select2);
		var ilosc;
		if(select1.name == 'filesS')
		{
			ilosc = select2.length;
		}
		else
		{
			ilosc = select1.length;
		}
		if(ilosc > 0)
		{
			var selObj = document.getElementById('type');
			for (var i=0; i < selObj.length; i++)
	    	{
	        	if( selObj.options[i].value == cn)
	        	{
	        		var tmp = selObj.options[i].text;
	        		var pozycja = tmp.indexOf('*')
	        		if( pozycja < 0 )
	        		{
	        			selObj.options[i].text = tmp + '*';
	        		}
	        	}
	    	}
	    }
	    else
	    {
	    	var selObj = document.getElementById('type');
			for (var i=0; i < selObj.length; i++)
	    	{
	        	if( selObj.options[i].value == cn)
	        	{
	        		var tmp = selObj.options[i].text;
	        		var pozycja = tmp.indexOf('*')
	        		if( pozycja > 0 )
	        		{
	        			tmp = tmp.substring(0,pozycja);
	        			selObj.options[i].text = tmp;
	        		}
	        	}
	    	}
	    }
	}

	function showImage(select)
	{
		var fileimageObj = E('fileimage');
		var sel = typeof select == 'string' ? E(select) : select;
		for (var i=0; i < sel.length; i++)
		{
        	if( sel.options[i].selected == true)
       		{
       			var tmp = sel.options[i].value;
       			var tab = tmp.split('\\');
       			var tmp = '<c:out value='${pageContext.request.contextPath}'/>/tempPng/'+tab[tab.length - 1];
       			tmp += '.png';
       			fileimageObj.src = tmp;	
       		}
        }
	}
	
	function openImage()
	{

		var fileimageObj = E('fileimage');
		var nowe_okno = window.open();
		nowe_okno.document.write('<HTML><HEAD></HEAD><BODY>ad <IMG SRC="'+fileimageObj.src+'" width="100%"></BODY></HTML>');
	}
	
	function selectFirst()
	{
		var fileimageObj = E('fileimage');
		var sel = E('filesS');
		var sel2 = document.getElementsByName('filesS');
		var tmp = sel.options[1].value;
		var tab = tmp.split('\\');
		var tmp = tab[tab.length - 1];
		tmp += '.png';
		fileimageObj.src = tmp;	
	}
	selectFirst();
    
    function zapisz(btn)
    {
        if($j('#selectedFiles option').length == 0) {
            alert('<ds:lang text="NiewybranoStronyZalacznika" />');
            return false;
        } else {
            $j('#selectedFiles option').attr('selected', 'selected');
            var el = document.getElementById('doSaveBTN');
            el.disabled = true;
            document.getElementById('doSave').value = true;

            document.forms[0].submit();
            return true;
        }
    }
    
</script>
</ww:elseif>
<ww:elseif test="showPage == 4">
<script language="JavaScript">
$j(document).ready(function() {
    window.opener.document.forms[0].submit();
});
</script>
</ww:elseif>
<script language="JavaScript">
    window.opener.accept('ILPOL_SPLIT_TIFF', {DWR_SPLIT_MESSAGE: $j('#messageList li').text()});
    prepareTable(E("mainTable"),2,0);
    
</script>


