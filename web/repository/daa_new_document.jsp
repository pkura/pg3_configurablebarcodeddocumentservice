<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N daa_new_document.jsp N-->

<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<%--<h3>Nowy dokument</h3>--%>
<%--
    Nie ma tytu�u, bo Mieczkowska sobie nie �yczy (mail z Thursday, July 28, 2005 3:03 PM)
--%>

<p> </p>

<% if (Docusafe.hasExtra("daa")) { %>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/daa_new_document.action'" />" title="<ds:lang text="NowyDokumentAgenta"/>" class="highlightedText"><ds:lang text="NowyDokumentAgenta"/></a>
<% } %>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/daa_new_document.action'"/>" method="post" enctype="multipart/form-data"
    onsubmit="disableFormSubmits(this);">

    <input type="hidden" name="doCreate" id="doCreate"/>
    <input type="hidden" name="folderId" id="folderId" />
    <ww:hidden name="'boxId'"/>

<table>
<%--<tr>
    <td>Tytu�:</td>
    <td><ww:textfield name="'title'" size="50" maxlength="254" cssClass="'txt'"/></td>
</tr>
<tr>
    <td>Opis:</td>
    <td><ww:textarea name="'description'" rows="4" cols="50" cssClass="'txt'"/></td>
</tr>--%>

<tr>
    <td><ds:lang text="Zalacznik"/>:</td>
    <td><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></td>
</tr>

<tr>
    <td><ds:lang text="DodajWzorUmowy"/>:</td>
    <td>
        <ww:select name="'attachmentPattern'" list="patterns" listKey="id" listValue="filename"
        headerKey="''" headerValue="'-- wybierz --'"
        value="attachmentPattern" id="attachmentPattern" cssClass="'sel'"/>
    </td>
</tr>

<tr>
    <td><ds:lang text="ArchiwizacjaWOtwartymPudle"/><ww:if test="boxId != null"> (<ww:property value="boxName"/>)</ww:if>:</td>
    <td><ww:checkbox name="'addToBox'" fieldValue="true" disabled="boxId == null"/></td>
</tr>

<tr>
    <td></td>
    <td>
        <input type="submit" value="getText('Utworz')" class="btn"
            onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true';"/>
    </td>
</tr>






</table>

</form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<script>
function validateForm()
{
    if (!__nw_validate()) return false;

    var rodzaj_sieci = document.getElementById('__nw_RODZAJ_SIECI');
    if (rodzaj_sieci && (!(parseInt(rodzaj_sieci.value) > 0)))
    {
        alert('Nie wybrano rodzaju sieci');
        return false;
    }

    var typ_dokumentu = document.getElementById('__nw_TYP_DOKUMENTU');
    if(typ_dokumentu && (!(parseInt(typ_dokumentu.value) > 0))) {
        alert('<ds:lang text="NieWybranoTypuDokumentu"/>');
        return false;
    }

    if ((document.getElementById('RODZAJ_RAPORTY').options.selectedIndex > 0)
    && (document.getElementById('__nw_KLASA_RAPORTU').options.selectedIndex == 0))
    {
        alert('<ds:lang text="NieWybranoKlasyRaportu"/>');
        return false;
    }

    var file = document.getElementById('file');
    var patternFile = document.getElementById('attachmentPattern');
    if (patternFile.selectedIndex == 0 && file.value.trim().length == 0)
    {
        if (!confirm(<ds:lang text="NieWybranoZalacznikaCzyNaPewnoChceszUtworzycDokument"/>))
            return false;
    }

   // alert('przekazano: agentid = '+document.getElementById('agent_id').value+' agencjaid = '+document.getElementById('agencja_id').value);
    return true;
}

/* wolane z doctype-fields */
function __nw_updateAttachmentPattern(typeId)
{
    var patternId;
    <ww:iterator value="patterns">
       if (parseInt(typeId) == <ww:property value="typeId"/>)
           patternId = <ww:property value="id"/>;
       else
    </ww:iterator>
    patternId = 0;

    document.getElementById('attachmentPattern').selectedIndex = patternId;
}






</script>