<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<h1><ds:lang text="Zamowienie"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form id="form" action="<ww:url value="'/repository/box-order-multi-doc.action'"/>" method="post">
<ww:hidden name="'orderId'" value="orderId"/>
<ww:if test="documents != null && documents.size() > 0">	
	<table>
		<tr>
			<td><ds:lang text="Dokumenty :"/></td>
		</tr>
		<ww:iterator value="documents">
			<tr>
				<td></td>
				<td align="left"><a href="<ww:url value="'/repository/edit-dockind-document.action'"><ww:param name="'id'" value="id"/><ww:param name="'orderId'" value="orderId"/></ww:url>">
				<ww:property value="title"/> ID : <ww:property value="id"/></a>
				</td>
				<ww:if test="orderAdmin">
				<td align="left"> 
				 pud�o: <ww:property value="boxNumber"/>
				</td>
				</ww:if>
				
			</tr>
		</ww:iterator>
	</table>
</ww:if>
	<table>
		<tr>
			<td><b><ds:lang text="Uwagi"/> : </b></td>
			<td align="left"><ww:property value="remark" /></td>
		</tr>
	</table>
	<table width="100%">
		<ww:iterator value="orderHistory">
		    <ww:set name="date" scope="page" value="date"/>
		    <tr>
		        <td><ww:property value="author"/></td>
		        <td align="right"><fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/></td>
		    </tr>
		    <tr>
		        <td colspan="2" style="border: 1px solid black;">
		            <ww:property value="content"/>
		        </td>
		    </tr>
		</ww:iterator>
	</table>
</form>