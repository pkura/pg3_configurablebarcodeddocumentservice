<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<!-- 
<a href="<ww:url value="'/repository/search-documents.action'"/>" class="highlightedText"><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>"><ds:lang text="WyszukiwanieZaawansowane"/></a>    

<ds:dockinds test="ald">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-payment-verification.action'"/>"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>"><ds:lang text="WeryfikacjaRejestruVAT"/></a>
</ds:dockinds>

<ds:dockinds test="dl">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>"><ds:lang text="InformacjeOkliencie"/></a>
</ds:dockinds>

<ds:dockinds test="invoice">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>
-->

<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form name="formul" action="<ww:url value="'/repository/search-documents.action'"/>" method="post">
	<ww:if test="results == null or results.empty">
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		<table id="bt" class="formTable">
		<ds:available test="menu.left.repository.PrzegladajFiltryWyszukiwania">
			<tr>
				<td>
					<ds:infoBox body="'Nazwa wykorzystywana przy zapisaniu filtra'" header="'Uwaga'"/>
					<ds:lang text="Nazwa filtra"/>:
				</td>
				<td>
					<ww:textfield name="'filterName'" size="10" maxlength="10" cssClass="'txt'"/>
				</td>		
			</tr>
		</ds:available>
			<ds:available test="wyszukiwarka.IDString">
				<tr>
					<td><ds:lang text="Identyfikator" />:</td>
					<td><ww:textfield id="docId" name="'documentIdString'"
							size="10" maxlength="10" cssClass="'txt'" /></td>
				</tr>
			</ds:available>
				<tr>
					<td><ds:lang text="Identyfikator dokumentu" />:</td>
					<td><ww:textfield id="docBarcode" name="'documentBarcode'"
							size="10" maxlength="20" cssClass="'txt'" /></td>
				</tr>
			<ds:available test="!wyszukiwarka.IDString">
				<tr>
					<td><ds:lang text="IdentyfikatorID" />:</td>
					<td><ww:textfield id="docId" name="'documentId'" size="10"
							maxlength="10" cssClass="'txt'" /></td>
				</tr>
			</ds:available>
			<tr>
				<td colspan="2">
					<hr/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Tytul"/>:
				</td>
				<td>
					<ww:textfield name="'title'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Opis"/>:
				</td>
				<td>
					<ww:textfield name="'description'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<%--	<tr>
				<td>
					<ds:lang text="Atrybuty:
				</td>
				<td>
					<ww:textfield name="'attribute'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>	--%>
			<ww:if test="!doctypes.empty">
				<tr>
					<td>
						<ds:lang text="Typy"/>:
					</td>
					<td>
						<ww:select name="'doctypeIds'" cssClass="'sel'" multiple="true" size="4"
							list="doctypes" listKey="id" listValue="name"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="!documentKinds.empty">
				<tr>
					<td>
						<ds:lang text="RodzajeDokumentowDocusafe"/>:
					</td>
					<td>
						<ww:select name="'documentKindIds'" cssClass="'sel dontFixWidth'" multiple="true" size="10"
							list="documentKinds" listKey="id" listValue="name"/>
					</td>
				</tr>
			</ww:if>
			<!-- 
			<ds:dockinds test="!prosika">
			<tr>
				<td>
					<ds:lang text="KodKreskowy"/>:
				</td>
				<td>
					<ww:textfield name="'attachmentBarcode'" id="attachmentBarcode" size="50" maxlength="25" cssClass="'txt'" />
				</td>
			</tr>
			</ds:dockinds>
			 -->
			<tr>
				<td>
					<ds:lang text="ZakresDatUtworzenia"/>:
				</td>
				<td>
					<table class="leftAlign oneLine">
					<tbody class="leftAlignInner">
					<tr>
					<td>
					<script type="text/javascript">
						var startCtimeT = "startCtime";
						var endCtimeT = "endCtime";
					</script>
					<ds:lang text="Od"/>:&nbsp;</td>
					
					<td><ww:textfield  name="'startCtime'" id="startCtime" size="10" maxlength="10" cssClass="'txt'"
					onchange="'checkDate(startCtimeT,endCtimeT,1)'"
					/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_startCtime_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"
						
						/></td>
					<td><ds:lang text="do"/>:&nbsp</td>
					
					<td><ww:textfield  name="'endCtime'" id="endCtime" size="10" maxlength="10" cssClass="'txt'"
					onchange="'checkDate(startCtimeT,endCtimeT,2)'"
					/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_endCtime_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/></td>
					</tr>
					</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="ZakresDatMmodyfikacji"/>:
				</td>
				<td>
					<table class="leftAlign oneLine">
					<tbody class="leftAlignInner">
					<tr>
					<td>
					<script type="text/javascript">
						var startMtimeT = "startMtime";
						var endMtimeT = "endMtime";
					</script>
					<ds:lang text="Od"/>:&nbsp;</td>
					<td><ww:textfield name="'startMtime'" id="startMtime" size="10" maxlength="10" cssClass="'txt'"
					onchange="'checkDate(startMtimeT,endMtimeT,1)'"
					/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_startMtime_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/></td>
					<td><ds:lang text="do"/>:&nbsp;</td>
					<td><ww:textfield name="'endMtime'" id="endMtime" size="10" maxlength="10" cssClass="'txt'"
					onchange="'checkDate(startMtimeT,endMtimeT,2)'"
					/>
					<img src="<ww:url value="'/calendar096/img.gif'"/>"
						id="calendar_endMtime_trigger" style="cursor: pointer; border: 1px solid red;"
						title="Date selector" onmouseover="this.style.background='red';"
						onmouseout="this.style.background=''"/></td>
					</tr>
					</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="Autor"/>:
				</td>
				<td valign="top">
				    <table border="0" cellspacing="0" cellpadding="0">
				    <tr><td>
					<ww:select name="'author'" cssClass="'sel combox-chosen'" list="users"
						listKey="name" listValue="asLastnameFirstname()"
						headerKey="''" headerValue="''"/>
						</td>
						</tr>
						</table>
				</td>

			</tr>
			<tr>
				<td valign="top">
					<ds:lang text="UzytkownikPracujacyNaDokumencie"/>:
				</td>
				<td>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td valign="top">
								<ww:select name="'accessedBy'" list="accessedByUsers" cssClass="'sel combox-chosen'"
									headerKey="''" headerValue="getText('select.dowolny')"
									listKey="name" listValue="asLastnameFirstname()" />
							</td>
							<td valign="top">
								<ww:select id="accessedAs" name="'accessedAs'" size="4" multiple="true"
									cssClass="'multi_sel'" list="documentChangeLogAll"
									listKey="what" listValue="description"/>
								<br/>
								<a href="javascript:void(select(document.getElementById('accessedAs'), true))"><ds:lang text="zaznacz"/></a>
								/
								<a href="javascript:void(select(document.getElementById('accessedAs'), false))"><ds:lang text="odznacz"/></a>
								<ds:lang text="wszystkie"/>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="OstatniaUwaga"/>:
				</td>
				<td>
					<ww:textfield name="'lastRemark'" size="50" maxlength="50" cssClass="'txt'"/>
				</td>
			</tr>
			<%--	<tr>
				<td>
					Pud�o archiwalne:
				</td>
				<td>
					<ww:select name="'boxId'" list="boxList" listKey="id" listValue="name"
						headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'" />
				</td>
			</tr>	--%>
			<tr>
				<td>
					<ds:lang text="LiczbaWynikowNaStronie"/>:
				</td>
				<td>
					<ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }"
						value="limit > 0 ? limit : 50"/>
				</td>
			</tr>
		</table>

		<ds:submit-event value="getText('Znajdz')" name="'doSearch'" cssClass="'btn searchBtn'"/>
		<input type="button" id="clearAllFields" name="clearAllFields" class="btn" value="<ds:lang text='WyczyscWszystkiePola'/>" onclick="javascript:document.formul.reset();return false;"/>
		<ds:available test="menu.left.repository.PrzegladajFiltryWyszukiwania">
		<%-- <ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="bth searchBtn" disabled="filterName != null" /> --%>
		<ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="bth searchBtn" />
		</ds:available>
		<script type="text/javascript">
	

	if($j('#startCtime').length > 0)
	{
			Calendar.setup({
				inputField     :    "startCtime",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_startCtime_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
	}

	if($j('#endCtime').length > 0)
	{
			Calendar.setup({
				inputField     :    "endCtime",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_endCtime_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
	}

	if($j('#startMtime').length > 0)
	{
			Calendar.setup({
				inputField     :    "startMtime",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_startMtime_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
	}

	if($j('#endMtime').length > 0)
	{
			Calendar.setup({
				inputField     :    "endMtime",     // id of the input field
				ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
				button         :    "calendar_endMtime_trigger",  // trigger for the calendar (button ID)
				align          :    "Tl",           // alignment (defaults to "Bl")
				singleClick    :    true
			});
	}

			<%--	<ww:if test="useBarcodes">
				var bi = new BarcodeInput({
					'prefix' : '<ww:property value="barcodePrefix"/>',
					'suffix' : '<ww:property value="barcodeSuffix"/>',
					'inputId' : 'attachmentBarcode',
					'submit' : false,
					//'submitId' : 'send',
					'tabStarts' : false
				});
			</ww:if>	--%>
		</script>
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end -->
		</ds:available>
	</ww:if>
</form>

<ww:if test="!results.empty">
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	<table class="search" width="100%" cellspacing="0" id="mainTable">
		<tr>
			<ww:iterator value="columns" status="status" >
				<th>
					<nobr>
						<ww:if test="sortDesc != null">
							<%--	<a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortDesc"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
								<ww:if test="property == ('document_' + sortField) && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
						<ww:property value="title"/>
						<ww:if test="sortAsc != null">
							<%--	<a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortAsc"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
								<ww:if test="(sortField == null && property == 'document_title') || (property == ('document_' + sortField) && ascending == true)"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
					</nobr>
				</th>
				<ww:if test="!#status.last">
					<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
				</ww:if>
			</ww:iterator>
		</tr>
		<ww:iterator value="results">
			<ww:set name="result"/>
			<tr>
				<ww:iterator value="columns" status="status">
					<ww:set name="result_property" scope="page" value="prettyPrint(#result[property])"/>
					
					 <ww:if test="property == 'attachment_link1' and #result[property] != null">
						<td>
							<ww:if test="#result.canReadAttachments">
								<a href="<ww:url value="#result[property]"/>"><img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Pobierz"/> (wer. <ww:property value="#result['attachment_rev1']"/>)</a>
							</ww:if>
						</td>
					</ww:if>
					<ww:else>
						<td>
							<a href="<ww:url value="#result['link']"/>"><c:out value="${result_property}" escapeXml="no"/></a>
						</td>
					</ww:else>
					<ww:if test="!#status.last">
						<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					</ww:if> 
				</ww:iterator>
			</tr>
		</ww:iterator>
		<tr>
			<td>
				<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="history.back();"/>
			</td>
		</tr>
	</table>
	<ds:available test="layout2">
		</div> <!-- BIG TABLE end -->
	</ds:available>
</ww:if>	

<script type="text/javascript">
	prepareTable(E("mainTable"),1,0);
</script>

<ww:set name="pager" scope="request" value="pager"/>
<table width="100%">
	<tr>
		<td align="center">
			<jsp:include page="/pager-links-include.jsp"/>
		</td>
	</tr>
</table>
