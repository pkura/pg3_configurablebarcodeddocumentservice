<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N explore-documents.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.repository.ExploreDocumentsAction,pl.compan.docusafe.boot.Docusafe" %>

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<script type="text/javascript">

function isEncryption() {
	if(document.getElementById("encryption").checked)
	{
		document.getElementById("publicKey").disabled = false;
	}
	else {
		document.getElementById("publicKey").disabled = true;
	}
}

Calendar.setup({
    inputField     :    "fileDate",     // id of the input field
    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
    button         :    "fileDate",  // trigger for the calendar (button ID)
    align          :    "Tl",           // alignment (defaults to "Bl")
    singleClick    :    true
});

	var $j = jQuery.noConflict();
	
	$j(document).ready(function ()
	{
		var top = $j(".tree_target").position();
		$j("#documentsList").css("margin-top", (top['top'] - 163));
		<ds:available test="nfos.style.exploreDocuments">
		$j("#documentsList").css("margin-top",0);
		$j("#treeDiv").css("width","auto");
		</ds:available>

		// funkcja wykorzystywana tylko w tclayout
		<ds:available test="tcLayout">
		wysokosc();
		</ds:available>

		/* sprawdzamy czy mamy kotwice w adresie - okre�la ona kt�ry folder jest otwarty i jak daleko przewin�� stron� */
		if(document.location.toString().search(/\#[0-9]*/) == -1) {
			document.location += '#' + $j('input[name=folderId]').val();
		}

		/* IE7 wyswietla za male pole na drzewko - trzeba je zwiekszyc recznie */
		/*var ie7 = (navigator.appVersion.indexOf('MSIE 7.')== -1) ? false : true;
		if(ie7) {
			document.getElementById('treeDiv').style.height = document.getElementById('treeDiv').offsetHeight + 20 + 'px';
		}*/
	});
	
	     function submitStrona(selectDocID,docTitle)
        {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = selectDocID;
            strona.dictionaryDescription =  docTitle;
            strona.nazwa = docTitle;
            
			if (window.opener.accept)
                window.opener.accept('<c:out value="${dicParam}"/>', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            window.close();
            return true;
        }
</script>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h1><ds:lang text="PrzegladajDokumenty"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<edm-html:errors />

<table id="exploreDocumentsToolbar">
<tr>
<ds:available test="menu.left.repository.nowydokument">
<!-- Nowy dokument -->
<td>&nbsp;
<c:if test="${!empty newDocumentLink}">
	<input type="image" class="zoomImg" name="newDocument" src="<c:out value='${pageContext.request.contextPath}'/>/img/new-document.gif" width="24" height="24" title="<ds:lang text="NowyDokument"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${newDocumentLink}'/>';">
</c:if>
<c:if test="${empty newDocumentLink}">
	<input type="image" class="zoomImg zoomDisabled" name="newDocument" src="<c:out value='${pageContext.request.contextPath}'/>/img/new-document_g.gif" width="24" height="24" title="<ds:lang text="NowyDokument"/>" disabled="true">
</c:if>
</td>

<!-- Nowy folder -->
<td>&nbsp;
<c:if test="${!empty newFolderLink}">
    <ds:available test="old.new.folder">
	    <input type="image" class="zoomImg" name="newFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/new-folder.gif" width="24" height="24" title="<ds:lang text="NowyFolder"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${newFolderLink}'/>';">
	</ds:available>
	<ds:available test="new.new.folder">
        <a href="<%= request.getContextPath() + ExploreDocumentsAction.FOLDERURL %><c:out value='${exploreDocuments.folderId}'/>">
	        <input type="image" class="zoomImg" name="newFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/new-folder.gif" width="24" height="24" title="<ds:lang text="NowyFolder"/>">
	    </a>
	</ds:available>
</c:if>
<c:if test="${empty newFolderLink}">
	<input type="image" class="zoomImg zoomDisabled" name="newFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/new-folder_g.gif" width="24" height="24" title"=<ds:lang text="NowyFolder"/>" disabled="true">
</c:if>
</td>

</ds:available>

<!-- Zmiana nazwy folderu -->
<td>&nbsp;
<c:if test="${!empty renameFolderLink}">
	<input type="image" class="zoomImg" name="renameFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/edit-folder.gif" width="24" height="24" title="<ds:lang text="ZmianaNazwyFolderu"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${renameFolderLink}'/>';">
</c:if>
<c:if test="${empty renameFolderLink}">
	<input type="image" class="zoomImg zoomDisabled" name="renameFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/edit-folder_g.gif" width="24" height="24" title="<ds:lang text="ZmianaNazwyFolderu"/>" disabled="true">
</c:if>
</td>

<!-- Usuwanie folderu -->
<td>&nbsp;
<c:if test="${!empty deleteFolderLink}">
	<input type="image" class="zoomImg" name="deleteFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/delete-folder.gif" width="24" height="24" title="<ds:lang text="Usunfolder"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${deleteFolderLink}'/>';">
</c:if>
<c:if test="${empty deleteFolderLink}">
	<input type="image" class="zoomImg zoomDisabled" name="deleteFolder" src="<c:out value='${pageContext.request.contextPath}'/>/img/delete-folder_g.gif" width="24" height="24" title="<ds:lang text="UsunFolder"/>" disabled="true">
</c:if>
</td>


<!-- Uprawnienia do folderu -->
<c:if test="${!empty editFolderPermissionsLink}">
	<td>&nbsp;
	<input type="image" class="zoomImg" src="<c:out value='${pageContext.request.contextPath}'/>/img/lock-folder.gif" width="24" height="24" title="<ds:lang text="EdycjaUprawnienFolderu"/>" onclick="window.open('<c:out value='${pageContext.request.contextPath}${editFolderPermissionsLink}'/>',null,'width=320,height=320,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes')" />
	</td>
</c:if>

<c:if test="${!empty printToPdf}">
	<td>&nbsp;
	<input type="image" class="zoomImg" src="<c:out value='${pageContext.request.contextPath}'/>/img/pdf.gif" width="24" height="24" title="<ds:lang text="DrukowanieDoPdf"/>" onclick="document.location.href='<c:out value='${pageContext.request.contextPath}${printToPdf}'/>';" />
	</td>
</c:if>

</tr>
</table>

<html:form action="/repository/explore-documents">
	<html:hidden property="folderId" styleId="folderId" />
	<html:hidden property="popupwin" />
	<!-- wype�niane automatycznie po klikni�ciu ikony usuwania dokumentu -->
	<html:hidden property="documentId" />
	<html:hidden property="doDeleteDocument" />
	<html:hidden property="param" />
	<html:hidden property="showCtime" />
	<!-- ##################################################################### -->
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="230" valign="top">
				<%--	<div style="overflow-x: auto; width: 450px; ">	--%>
				<div id="treeDiv"
					style="width: 450px; padding-left: 5px; height: auto;">
					<c:out value="${treeHtml}" escapeXml="false"></c:out>
				</div>
			</td>
			<td valign="top">
				<%--	<p><b><c:out value="${folder.prettyPath}"/></b></p>	--%>
				<table id="documentsList" width="100%" border="0" cellspacing="0"
					cellpadding="0" class="search">
					<tr>
						<!--<th></th>-->
						<th><nobr>
								<a href='<c:out value="${title_up}"/>'
									alt="<ds:lang text="SortowanieRosnace"/>"
									title="<ds:lang text="SortowanieRosnace"/>"><img
									src="<c:out value="${pageContext.request.contextPath}/img/strzalka-gora-lista.gif"/>"
									width="11" height="11" border="0" /></a>
								<ds:lang text="Tytul" />
								<a href='<c:out value="${title_down}"/>'
									alt="<ds:lang text="SortowanieMalejace"/>"
									title="<ds:lang text="SortowanieMalejace"/>"><img
									src="<c:out value="${pageContext.request.contextPath}/img/strzalka-dol-lista.gif"/>"
									width="11" height="11" border="0" /></a>
							</nobr></th>
						<c:if test="${showCtime}">
							<th><nobr>
									<a href='<c:out value="${ctime_up}"/>'
										alt="<ds:lang text="SortowanieRosnace"/>"
										title="<ds:lang text="SortowanieRosnace"/>"><img
										src="<c:out value="${pageContext.request.contextPath}/img/strzalka-gora-lista.gif"/>"
										width="11" height="11" border="0" /></a>
									<ds:lang text="DataUtworzenia" />
									<%--	<ds:lang text="Tytul"/>	--%>
									<a href='<c:out value="${ctime_down}"/>' id="ctime_down"
										name="ctime_down" alt="<ds:lang text="SortowanieMalejace"/>"
										title="<ds:lang text="SortowanieMalejace"/>"><img
										src="<c:out value="${pageContext.request.contextPath}/img/strzalka-dol-lista.gif"/>"
										width="11" height="11" border="0" /></a>
								</nobr></th>
						</c:if>
						<th></th>
					</tr>
					<c:forEach items="${documentBeans}" var="bean" varStatus="stat">
						<tr
							<ds:available test="nfos.style.exploreDocuments">
							<c:if test="${stat.count % 2 == 0}">style="background-color:#DBDBFF"</c:if>
						</ds:available>>

							<td valign="top"
								<ds:available test="nfos.style.exploreDocuments">style="text-align:left"</ds:available>>
								<a href='<c:out value="${bean.editLink}"/>'> <c:out
										value='${bean.title}' /></a>&nbsp;
							</td>
							<c:if test="${showCtime}">
								<td valign="top"><fmt:formatDate value="${bean.ctime}"
										type="both" pattern="dd-MM-yy HH:mm" /> &nbsp;</td>
							</c:if>
							<c:if test="${dicParam != null}">
								<td><a
									href="javascript:submitStrona('<c:out value='${bean.id}'/>','<c:out value='${bean.title}'/>')"><ds:lang
											text="wybierz" /></a></td>
							</c:if>

							<%--	<td valign="top">
							<fmt:formatDate value="${bean.mtime}" type="both" pattern="dd-MM-yy HH:mm"/>
							&nbsp;
						</td>	--%>
							<%--	<td valign="top">
							<c:out value="${bean.author}"/>
						</td>	--%>
							<ds:available test="!nfos.style.exploreDocuments">
								<td valign="top"><c:if test="${bean.canReadAttachments}">
										<table class="attachTable">
											<c:forEach items="${bean.attachments}" var="att">
												<tr>
													<td><c:out value='${att.title}' /></td>
													<td width="32">&nbsp; <a
														href="<c:out value='${att.revisionLink}'/>"> <img
															src="<c:out value='${pageContext.request.contextPath}'/>/img/pobierz.gif"
															width="18" height="18" class="zoomImg"
															title=<ds:lang text="Pobierz"/> />
													</a>
													</td>
													<c:if test="${!empty att.viewerLink}">
														<td width="64">&nbsp; <a
															href="<c:out value='${att.revisionLink}&asPdf=true'/>">
																<img
																src="<c:out value='${pageContext.request.contextPath}'/>/img/pdf.gif"
																width="18" height="18" class="zoomImg"
																title=<ds:lang text="Pobierz"/> />
														</a> &nbsp; <a href="<c:out value='${att.viewerLink}'/>">
																<img
																src="<c:out value='${pageContext.request.contextPath}'/>/img/wyswietl.gif"
																width="18" height="18" class="zoomImg"
																title=<ds:lang text="WyswietlZalacznik"/> />
														</a>
														</td>
													</c:if>

												</tr>
											</c:forEach>
										</table>
									</c:if> <c:if test="${bean.isDeleted}">
										<input type="image"
											src="<c:out value='${pageContext.request.contextPath}'/>/img/delete-document.gif"
											width="24" height="24" title="Usun dokument"
											onclick="if (!confirm('Na pewno usun�� ?')) return false; this.form.documentId.value = <c:out value='${bean.id}'/>; this.form.doDeleteDocument.value = 'true';" />
									</c:if></td>
							</ds:available>
						</tr>
					</c:forEach>
				</table>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2" align="center"><jsp:include
								page="/pager-links-include.jsp" />
						<td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</html:form>

<ds:available test="draganddrop">
	<div id=dragdrop>

				<h4><ds:lang text="NowyZalacznik"/></h4>
                			<ds:lang text="AbyDodacNowyZalacznik"/>
                			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
				    <td><ds:lang text="Tytul"/><span class="star">*</span>:</td>
				    <td><ww:textfield id="fileDescription" name="'fileDescription'" size="40" maxlength="40" cssClass="'txt dontFixWidth'" /></td>
                </tr>

			    <tr>
            		<td><ds:lang text="Data"/><span class="star">*</span>:</td>
            		<td><ww:textfield id="fileDate" name="'fileDate'" size="10" maxlength="10" cssClass="'txt dontFixWidth'" /></td>
                </tr>

				<tr>
					<td><ds:lang text="Plik"/><span class="star">*</span>:</td>
				<td>
				   <ww:file name="'fileselect'" id="fileselect" cssClass="'txt dontFixWidth'" size="40" />
				</td>
				</tr>

			<ds:available test="file.encryption.draganddrop">
                <tr>
					<td><ds:lang text="attachment.encryption"/>:</td><td><ww:checkbox id="encryption" name="'encryption'" fieldValue="true" onchange="'isEncryption()'"/></td>
				</tr>
				<tr>
					<td><ds:lang text="attachment.publickey"/>:</td><td><ww:textfield name="'publicKey'" id="publicKey" size="40" maxlength="254" cssClass="'txt dontFixWidth'" disabled="true"/></td>
			    </tr>
			</ds:available>

                <tr>
                <td>
                <tr>
                <td></td>
				<td>	<button id="submitbutton" cssClass="btn"><ds:lang text="Utworz"/></button>
				</td></tr>
				<tr>
				<td>
				<span class="star">*</span><ds:lang text="PoleObowiazkowe"/>
				</td>
				</tr>

</table>


	</div>
		<div id="messages"></div>

	</div>
</ds:available>
<script type="text/javascript">


	function $id(id) {
		return document.getElementById(id);
	}

	if (window.File) {
		Init();
	}

	function Init() {

		var submitbutton = $id("submitbutton");

		submitbutton.addEventListener("click", FileSelectHandler, false);

		var xhr = new XMLHttpRequest();
		if (xhr.upload) {
		}

	}

	function FileSelectHandler(e) {

        var fileselect = $id("fileselect");

		FileDragHover(e);

		var files = fileselect.files
		for (var i = 0, f; f = files[i]; i++) {
		    if(f.size > 104857600)
		    {
		         alert('<ds:lang text="MaxSizeFileUpload"/>');
		    }
            else {
			UploadFile(f);
			}
		}

	}

	function Output(msg) {
		var m = $id("messages");
		m.innerHTML = msg + m.innerHTML;
	}

	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}

	function UploadFile(file) {

		if($j('#folderId').val() != '') {

		if(($j('#fileDescription').val() != '') && ($j('#fileDate').val() != '')) {
		var folderId = $j('#folderId').val();
		var fileDescription = $j('#fileDescription').val();
		var fileDate = $j('#fileDate').val();
		var publicKey = $j('#publicKey').val();
		var encryption = $j('#encryption').val();


		console.log(folderId);
		console.log(fileDescription);
		console.log(fileDate);

		var data = new FormData();
		data.append("file", file);

		$j.ajax({
			type: "POST",
		    data: data,
		    url: "<%=Docusafe.getBaseUrl()%>/repository/uploadDnDAjax.action?folderId=" + folderId + "&fileDescription=" + fileDescription + "&fileDate=" + fileDate + "&publicKey=" + publicKey + "&encryption=" + encryption,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function(data) {

		    	$j('#messages').html("<p>Dodano: <strong>" + file.name + "</strong> rozmiar: <strong>" + file.size + "</strong> bajt�w </p>");
		    },
		    error: function(data){

		    	$j('#messages').html("<p>Wyst�pi� b��d podczas dodawania pliku.</p>");
		    }

		});

		}
		else {
			alert("Wype�nij wszystkie pola i dodaj plik ponownie.");
		}
		} else {
		alert("Wybierz folder.")
		}
	}



</script>


