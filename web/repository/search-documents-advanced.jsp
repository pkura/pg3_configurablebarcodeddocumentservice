<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08 
C--%>
<!--N search-documents-advanced.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Szukaj dokument�w</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<a href="<ww:url value="'/repository/search-documents.action'"/>" >Wyszukiwanie proste</a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<ds:extras test="business">
	<a href="<ww:url value="'/repository/search-documents-advanced.action'"/>">Wyszukiwanie zaawansowane</a>
</ds:extras>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form name="formul" id="form" action="<ww:url value="'/repository/search-documents-advanced.action'"/>" method="post">
<ww:hidden name="'doChangeDoctype'" id="doChangeDoctype"/>
<ww:hidden name="'resultsInPopup'" id="resultsInPopup" value="resultsInPopup"/>

<ww:if test="results == null or results.empty">
    <table>
    <tr>
        <td>Identyfikator:</td>
        <td><ww:textfield name="'documentId'" size="10" maxlength="10" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td>Typ dokumentu Docusafe:</td>
        <td>
            <ww:select name="'doctypeId'" list="doctypes" listKey="id" listValue="name"
                cssClass="'sel'" id="doctypeId" onchange="'doctypeChanged(event);'"
                headerKey="''" headerValue="'-- wybierz --'" />
        </td>
    </tr>
    <ww:if test="doctypeFields.size > 0">
        <ww:iterator value="doctypeFields">
            <ww:if test="!hidden">
            <tr>
                <td valign="top"><ww:property value="name"/>:</td>
                <td>
                    <ww:if test="type == 'date'">
                        od <input type="text" size="10" maxlength="10" class="txt"
                            name="field_<ww:property value="id"/>:from"
                            id="datefield_<ww:property value="id"/>:from"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="datefield_trigger_<ww:property value="id"/>:from" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                        do <input type="text" size="10" maxlength="10" class="txt"
                            name="field_<ww:property value="id"/>:to"
                            id="datefield_<ww:property value="id"/>:to"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="datefield_trigger_<ww:property value="id"/>:to" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                        <script>
                            Calendar.setup({
                                inputField     :    "datefield_<ww:property value="id"/>:from",     // id of the input field
                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                button         :    "datefield_trigger_<ww:property value="id"/>:from",  // trigger for the calendar (button ID)
                                align          :    "Tl",           // alignment (defaults to "Bl")
                                singleClick    :    true
                            });
                            Calendar.setup({
                                inputField     :    "datefield_<ww:property value="id"/>:to",     // id of the input field
                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                button         :    "datefield_trigger_<ww:property value="id"/>:to",  // trigger for the calendar (button ID)
                                align          :    "Tl",           // alignment (defaults to "Bl")
                                singleClick    :    true
                            });
                        </script>
                    </ww:if>
                    <ww:if test="type == 'string'">
                        <ww:textfield name="'field_'+id" size="length > 30 ? 30 : length" maxlength="length" cssClass="'txt'" />
                    </ww:if>
                    <ww:if test="type == 'bool'">
                        <ww:select name="'field_'+id" headerKey="''" headerValue="'-- wybierz --'"
                            cssClass="'sel'"
                            list="#@java.util.LinkedHashMap@{ true : 'tak', false: 'nie' }"/>
<%--                                <ww:checkbox fieldValue="true" name="'field_'+id" />--%>
                    </ww:if>
                    <ww:if test="type == 'enum'">
                        <ww:if test="cn == 'TYP_DOKUMENTU'" >


                            <input type="hidden" name="field_<ww:property value="id" />" id="field_<ww:property value="id" />" />
                             <!-- zmienna okre�laj�ca aktualn� warto�� pola TYP_DOKUMENTU -->
                             <ww:set name="fieldid" value="@java.lang.Integer@valueOf(@pl.compan.docusafe.util.HttpUtils@valueOrNull(doctypeFields[id]))"/>
                             <table>
                             <tr>
                                 <td>Dokumenty specjalisty/OWCA:</td>
                                 <td>
                                     <!-- og�lne -->

                                     <select class="sel" id="RODZAJ_AGENT" onchange="__nw_updateRODZAJ(this);">
                                         <option value="">-- wybierz --</option>
                                         <ww:iterator value="enumItems">
                                             <ww:if test="arg1 == 'RODZAJ_AGENT'">
                                                 <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                                     <ww:property value="title"/>
                                                 </option>
                                             </ww:if>
                                         </ww:iterator>
                                     </select>

                                 </td>
                             </tr>
                             <tr>
                                 <td>Dokumenty biura/agencji/banku:</td>
                                 <td>
                                     <!-- �wiadczeniowe -->
                                     <select class="sel" id="RODZAJ_AGENCJA" onchange="__nw_updateRODZAJ(this);">
                                         <option value="">-- wybierz --</option>
                                         <ww:iterator value="enumItems">
                                             <ww:if test="arg1 == 'RODZAJ_AGENCJA'">
                                                 <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                                     <ww:property value="title"/>
                                                 </option>
                                             </ww:if>
                                         </ww:iterator>
                                     </select>
                                 </td>
                             </tr>
                             <tr>
                                 <td>Raporty:</td>
                                 <td>
                                     <!-- raporty -->
                                     <select class="sel" id="RODZAJ_RAPORTY" onchange="__nw_updateRODZAJ(this);">
                                         <option value="">-- wybierz --</option>
                                         <ww:iterator value="enumItems">
                                             <ww:if test="arg1 == 'RODZAJ_RAPORT'">
                                                 <option value="<ww:property value="id"/>" <ww:if test="#fieldid == id">selected="selected"</ww:if> >
                                                     <ww:property value="title"/>
                                                 </option>
                                             </ww:if>
                                         </ww:iterator>
                                     </select>
                                 </td>
                             </tr>

                             </table>

                             <script>
                             // po wybraniu warto�ci w jednej z list usuwa wyb�r z pozosta�ych
                             function __nw_updateRODZAJ(select)
                             {
                                 var id = select.id;
                                 if (id != 'RODZAJ_AGENT') document.getElementById('RODZAJ_AGENT').options.selectedIndex = 0;
                                 if (id != 'RODZAJ_AGENCJA') document.getElementById('RODZAJ_AGENCJA').options.selectedIndex = 0;
                                 if (id != 'RODZAJ_RAPORTY') document.getElementById('RODZAJ_RAPORTY').options.selectedIndex = 0;
                                 var hid = document.getElementById('field_<ww:property value="id"/>');
                                 hid.value = select.value;
                                 
                             }

                             </script>
                         </ww:if>
                         <ww:else>
                        <ww:select name="'field_'+id" list="enumItems"
                            listKey="id" listValue="title"
                            cssClass="'multi_sel'" size="5" multiple="true" /><br/><a href="javascript:void(select(getElementByName('field_<ww:property value="id"/>'), true))">zaznacz</a>
                        / <a href="javascript:void(select(getElementByName('field_<ww:property value="id"/>'), false))">odznacz</a>
                        wszystkie
                                  </ww:else>
                    </ww:if>
                    <ww:if test="type == 'integer'">




                        <ww:textfield name="'field_'+id" cssClass="'txt'"
                            size="6" maxlength="8"/>
                    </ww:if>
                    <ww:if test="type == 'long'">
                        <ww:if test="cn == 'AGENT'">
                            <input
                                    type="hidden"
                                    name="agent_id"
                                    id="agent_id" />                         
                            <table>
                                <tr><td>Imi�: </td><td><ww:textfield name="'agent_imie'" id="agent_imie" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agent_imie" onchange="'clearAgent(true);'"/></td></tr>
                                <tr><td>Nazwisko: </td><td><ww:textfield name="'agent_nazwisko'" id="agent_nazwisko" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agent_nazwisko" onchange="'clearAgent(true);'"/></td></tr>
                                <tr><td>Numer: </td><td><ww:textfield name="'agent_numer'" id="agent_numer" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agent_numer" onchange="'clearAgent(true);'"/></td></tr>
                            </table>
                            <input type="button" value="Wybierz specjalist�/OWCA" onclick="openAgentPopup();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            <input type="button" value="Wyczy��" onclick="clearAgent(false);" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            <script type="text/javascript" >
                                    function __check_accept_agent(map)
                                    {
                                          if (parseInt(document.getElementById('agent_id').value) == parseInt(map.id))
                                          __accept_agent(map);
                                    }
                                    function __accept_agent(map, lparam, wparam)
                                    {
                                        document.getElementById('agent_id').value = map.id;
                                        document.getElementById('agent_imie').value = map.imie;
                                        document.getElementById('agent_nazwisko').value = map.nazwisko;
                                        document.getElementById('agent_numer').value = map.numer;
                                    }
                                    function openAgentPopup()
                                    {
                                        openToolWindow('<ww:url value="'/office/common/agent.action'"/>'+
                                            '?imie='+document.getElementById('agent_imie').value+
                                           '&nazwisko='+document.getElementById('agent_nazwisko').value+
                                           '&id='+document.getElementById('agent_id').value+
                                            '&canAddAndSubmit=true');
                                    }
                                    function clearAgent(onlyId)
                                    {
                                        document.getElementById('agent_id').value = '';
                                        if (!onlyId) {
                                             document.getElementById('agent_nazwisko').value = '';
                                             document.getElementById('agent_imie').value = '';
                                             document.getElementById('agent_numer').value = '';
                                        }
                                    }
                            </script>
                        </ww:if>

                        <ww:if test="cn == 'AGENCJA'">
                            <input
                                    type="hidden"
                                    name="agencja_id"
                                    id="agencja_id"
                                    />
                            <table>
                                <tr><td>Nazwa: </td><td><ww:textfield name="'agencja_nazwa'" id="agencja_nazwa" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agencja_nazwa" onchange="'clearAgencja(true);'"/></td></tr>
                                <tr><td>Numer: </td><td><ww:textfield name="'agencja_numer'" id="agencja_numer" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agencja_numer" onchange="'clearAgencja(true);'"/></td></tr>
                                <tr><td>NIP: </td><td><ww:textfield name="'agencja_nip'" id="agencja_nip" size="50" maxlength="254" cssClass="'txt'" readonly="false" value="agencja_nip" onchange="'clearAgencja(true);'"/></td></tr>
                            </table>
                            <input type="button" value="Wybierz biuro/agencj�/bank" onclick="openAgencjaPopup();" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            <input type="button" value="Wyczy��" onclick="clearAgencja(false);" class="btn" <ww:if test="!canReadDAASlowniki">disabled="disabled"</ww:if>>
                            <script type="text/javascript" >
                                    function __check_accept_agencja(map)
                                    {
                                          if (parseInt(document.getElementById('agencja_id').value) == parseInt(map.id))
                                          __accept_agencja(map);
                                    }
                                    function __accept_agencja(map, lparam, wparam)
                                    {

                                       document.getElementById('agencja_id').value = map.id;
                                       document.getElementById('agencja_nazwa').value = map.nazwa;
                                       document.getElementById('agencja_numer').value = map.numer;
                                       document.getElementById('agencja_nip').value = map.nip;

                                    }
                                    function openAgencjaPopup()
                                    {
                                        openToolWindowX('<ww:url value="'/office/common/agencja.action'"/>'+
                                            '?nazwa='+document.getElementById('agencja_nazwa').value+
                                            '&id='+document.getElementById('agencja_id').value+
                                            '&nip='+document.getElementById('agencja_nip').value+
                                            '&canAddAndSubmit=true', 'Agencja', 950, 650);
                                    }
                                    function clearAgencja(onlyId)
                                    {
                                        document.getElementById('agencja_id').value = '';
                                        if (!onlyId) {
                                             document.getElementById('agencja_nazwa').value = '';
                                             document.getElementById('agencja_nip').value = '';
                                             document.getElementById('agencja_numer').value = '';
                                        }
                                    }
                            </script>

                        </ww:if>

                        <ww:if test="cn != 'AGENT' and cn != 'AGENCJA'">
    
                            <ww:textfield name="'field_'+id" cssClass="'txt'"
                                size="6" maxlength="8"/>
                        </ww:if>
                    </ww:if>
                </td>
            </tr>
            </ww:if>
        </ww:iterator>
    </ww:if>
    <tr>
        <td>Pud�o archiwalne:</td>
        <td><ww:textfield name="'boxName'" size="20" maxlength="100" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td valign="top">U�ytkownik pracuj�cy na dokumencie:</td>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <ww:select name="'accessedBy'" list="accessedByUsers" cssClass="'sel'"
                        headerKey="''" headerValue="'-- wybierz --'"
                        listKey="name" listValue="asFirstnameLastnameName()" />
                    </td>
                    <td valign="top">
                        <ww:select id="accessedAs" name="'accessedAs'" size="4" multiple="true" cssClass="'multi_sel'"
                        list="@pl.compan.docusafe.core.base.DocumentChangelog@all()"
                        listKey="what" listValue="description"/><br/><a href="javascript:void(select(document.getElementById('accessedAs'), true))">zaznacz</a>
                            / <a href="javascript:void(select(document.getElementById('accessedAs'), false))">odznacz</a>
                            wszystkie
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>Liczba wynik�w na stronie:</td>
        <td><ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }" value="limit > 0 ? limit : 50"/></td>
    </tr>
<%--
    <tr>
        <td>Pud�o archiwalne:</td>
        <td><ww:textfield name="'boxNumber'" size="30" maxlength="30" cssClass="'txt'"/></td>
    </tr>
--%>
    </table>

    <ds:submit-event value="'Znajd�'" name="'doSearch'" cssClass="'btn'" disabled="doctypeId == null" />
	<input type="button" id="clearAllFields" name="clearAllFields" class="btn" value="<ds:lang text='WyczyscWszystkiePola'/>" onclick="javascript:document.formul.reset();return false;"/>
	
</ww:if>

</form>


<ww:if test="!results.empty">
<table class="search" width="100%" cellspacing="0" id="mainTable">
<tr>
    <ww:iterator value="columns" status="status" >
        <th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if> <ww:property value="title"/> <ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
        <ww:if test="!#status.last">
            <th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
        </ww:if>
        <th>&nbsp;</th>
    </ww:iterator>
</tr>
<ww:iterator value="results">
    <ww:set name="result"/>
    <tr>
        <ww:iterator value="columns" status="status">
            <ww:if test="property == 'attachment_link1' and #result[property] != null">
                <td><ww:if test="#result.canReadAttachments">
                    <ww:if test="#result['attachment_link_vs'] != null">
                        <a title="Wy�wietl" href="javascript:openToolWindow('<ww:url value="#result['attachment_link_vs']"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'nwPdf', 1000, 750)">W</a> /
                    </ww:if>
                    <a title="Pobierz" href="<ww:url value="#result[property]"/>">P</a>
                </td>
            </ww:if>
            <ww:else>
                <td><a href="<ww:url value="#result['link']"/>"><ww:property value="prettyPrint(#result[property])"/></a></td>
            </ww:else>
            <ww:if test="!#status.last">
                <td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
            </ww:if>
            <td>&nbsp;</td>
        </ww:iterator>
    </tr>
</ww:iterator>

    <ww:set name="pager" scope="request" value="pager"/>
    <table width="100%">
    <tr>
        <td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
    </tr>
    </table>
</ww:if>
</table>


<script>
prepareTable(E("mainTable"),0,0);

function doctypeChanged(event)
{
    var src = event.srcElement ? event.srcElement : event.target;
    var id = src.value;
    if (id == null || id == '') return true;

    document.getElementById('doChangeDoctype').value = 'true';
    document.getElementById('form').submit();
}

</script>