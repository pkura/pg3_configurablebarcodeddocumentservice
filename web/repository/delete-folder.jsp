<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N delete-folder.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><ds:lang text="UsuwanieFolderu"/> </h3>

<p><c:out value="${deleteFolder.prettyPath}"/></p>

<edm-html:errors />

<html:form action="/repository/delete-folder">
<html:hidden property="folderId"/>
<html:hidden property="prettyPath"/>

<html:submit property="doDelete" styleClass="btn"><edm-html:message key="doDelete" global="true" /></html:submit>
<html:submit property="doCancel" styleClass="btn"><edm-html:message key="doCancel" global="true" /></html:submit>

</html:form>

