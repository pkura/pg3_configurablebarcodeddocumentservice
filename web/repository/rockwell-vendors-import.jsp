<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N rockwell-vendors-import.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1>Vendors import</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/repository/rockwell-vendors-import.action'"/>" method="post"  enctype="multipart/form-data">

<ww:file name="'file'" id="file" size="30" cssClass="'txt'"/>
<br/>
<ds:event value="getText('Importuj')" name="'doImport'" cssClass="'btn'"/>

</form>