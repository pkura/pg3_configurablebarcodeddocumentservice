<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="Lista dokumentów"/></h1>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>
 <form action="<ww:url value="'/repository/search-example.action'"/>" method="post">
<ds:available test="layout2">
  <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
<!--<ww:hidden name="'priorityKind'" id="priorityKind"/>
<ww:hidden name="'boxId'" id="boxId"/>
<ww:hidden name="'doOrder'" id="doOrder"/>

-->
<table class="mediumTable search">
  <tr>
    <td colspan="4">
      <ds:lang text="Nazwa dokumentu"/>:<ww:textfield name="'name'" size="100" maxlength="300" cssClass="'txt'"/>
    </td>
  </tr>
  <tr>
    <td><ds:event name="'doSearch'" value="getText('Szukaj')"/></td>
  </tr>
  <tr>
    <th>
      ID
    </th>
    <th>
      Nazwa
    </th>
    <th>
      Data dokumentu
    </th>    
  </tr>
  <ww:iterator value="docs">
    <tr>
      <td>
        <ww:property value="id"/>
      </td>
      <td>
        <ww:property value="name"/>
      </td>
      <td>
        <ww:property value="creation_date"/>
      </td>     
    </tr>
  </ww:iterator>
</table>
<ds:available test="layout2">
  <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
  <div class="bigTableBottomSpacer">&nbsp;</div>
  </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>