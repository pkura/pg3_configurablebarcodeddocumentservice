<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N impuls-leasing-contract.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="Umowa"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/repository/impuls-leasing-contract.action'"/>" method="post" enctype="multipart/form-data">


<table>
	<tr ALIGN=center>
        <th><ds:lang text="NumerUmowy"/></th>
        <th><ds:lang text="OpisUmowy"/></th>
        <th><ds:lang text="WartoscUmowy"/></th>
        <th><ds:lang text="DataUmowy"/></th>
        <th><ds:lang text="DataKoncaUmowy"/></th>
    </tr>
    <tr>
		<td><ww:property value="dlC.numerUmowy" /></td>
        <td><ww:property value="dlC.opisUmowy" /></td>
        <td><ww:property value="dlC.wartoscUmowy" /></td>
        <td><ww:property value="dlC.dataUmowy" /></td>
        <td><ww:property value="dlC.dataKoncaUmowy" /></td>
    </tr>

</table>

<table>
        <tr>
    		<hr size="1" align="left" class="horizontalLine" width="77%" />
    	</tr>
   		<tr>
   			<th class="highlightedText"><nobr><ds:lang text="DokumentyZwiazaneZUmowa"/>:</nobr></th>
    	</tr>
</table>
<table>
      	<tr>
    		<th><nobr>ID</nobr></th>

    		<th><nobr><ds:lang text="RodzajDokumentu"/></nobr></th>

    		<th><nobr><ds:lang text="TypDokumentu"/></nobr></th>

    		<th><nobr><ds:lang text="DataDokumentu"/></nobr></th>

    		&nbsp
    	</tr>
        <ww:iterator value="documents">
        	<ww:set name="document"/>
            	<tr>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['id']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['rodzajDokumentu']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['typDokumentu']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['data']"/></td>
				</tr>
        </ww:iterator>
    	<tr><td>&nbsp</td></tr>
</table>        
</form>