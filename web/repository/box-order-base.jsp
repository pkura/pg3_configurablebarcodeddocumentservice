<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<table>	
	<tr>
		<td>
			<ds:lang text="Lokalizacja"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'location'" id="location" list="locations" listKey="id" headerKey="''" headerValue="getText('select.wybierz')"  
				listValue="getShortDescription()" cssClass="'sel'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Uwagi"/>
		</td>
		<td>
			<ww:textarea name="'remark'" id="remark" cols="30" cssClass="'txt'" rows="4" ></ww:textarea>
		</td>
	</tr>
	<ww:if test="archiveDocumentId != null">
	<%--
	<tr>
		<td>
			<ds:lang text="Numer dokumentu"/><span class="star">*</span>
		</td>
		<td>
			<ww:textfield name="'nrDoc'" id="nrDoc" cssClass="'txt'"/>
		</td>
	</tr>
	--%>
	</ww:if>
	<tr>
		<td>
			<ds:lang text="Priorytet"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'priority'" id="priority" list="priorityMap" headerKey="''" headerValue="getText('select.wybierz')"  
				cssClass="'sel'" value="'2'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="RodzajZamowienia"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'orderKind'" id="orderKind" list="orderKindMap" headerKey="''" headerValue="getText('select.wybierz')"  
				cssClass="'sel'" value="'1'" onchange="'changeOrder(this);'"/>
		</td>
	</tr>
	<tr id="deliveryKindtr" style="display:none;">
		<td>
			<ds:lang text="SposobDostarczenia"/><span class="star">*</span>
		</td>
		<td>
			<ww:select name="'deliveryKind'" id="deliveryKind" list="deliveryKindMap" headerKey="''" headerValue="getText('select.wybierz')"  
				cssClass="'sel'" value="'10'"/>
		</td>
	</tr>
	<tr>
		<ww:if test="orderId == null">
			<td>
				<ww:hidden name="'doOrder'" id="doOrder"/>
				<input type="button" value="<ds:lang text="Zamow"/>" onclick="order(this);" class="btn"/>
			</td>
		</ww:if>
		<ww:else>
			<td>
				<ww:hidden name="'doSave'" id="doSave"/>
				<input type="button" value="<ds:lang text="Zapisz"/>" onclick="save(this);" class="btn saveBtn"/>
			</td>
		</ww:else>
	</tr>
</table>
<script type="text/javascript">
function order(btn)
{
	if(confirm('Czy na pewno zam�wi� dokument ?'))
	{
		if(document.getElementById('location').selectedIndex == 0)
		{
			alert('Nie wybrano lokalizacji');
			return false;
		}
		if(document.getElementById('priority').selectedIndex == 0)
		{
			alert('Nie wybrano priorytetu');
			return false;
		}
		if(document.getElementById('orderKind').selectedIndex == 0)
		{
			alert('Nie wybrano rodzaju zam�wienia');
			return false;
		}
		if(document.getElementById('orderKind').options[document.getElementById('orderKind').selectedIndex].value > 1 && 
				document.getElementById('deliveryKind').selectedIndex == 0)
		{
			alert('Nie wybrano sposobu dostarczenia');
			return false;
		}
		document.getElementById('doOrder').value = 'true';
		disabledAllBtn(btn);
		btn.form.submit();
		return true;
	}
	else
	{
		return false
	}
	return true;
}

function changeOrder(selObj) 
{
	if(selObj.options[selObj.selectedIndex].value == 1)
	{
		document.getElementById('deliveryKindtr').style.display = 'none';
	}
	else
	{
		document.getElementById('deliveryKindtr').style.display = '';
	}
}
function save(btn)
{
	if(confirm('Czy na pewno zapisa� zmiany ?'))
	{
		document.getElementById('doSave').value = 'true';
		disabledAllBtn(btn);
		btn.form.submit();
		return true;
	}
	return false;
}
</script>