<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N send-link.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<script>
<c:if test="${close}">
    window.close();
</c:if>
</script>

<edm-html:errors />

<html:form action="/repository/send-link">
    <html:hidden property="documentId"/>
    <b><ds:lang text="WysylanieOdnosnikaDoDokumentu"/></b>
    <p><html:select property="username" styleClass="sel">
        <option value=""><ds:lang text="select.wybierzUzytkownika"/></option>
        <html:optionsCollection name="users"/>
       </html:select>
       <ds:lang text="lubWpiszEmail"/>
       <html:text property="email" size="20" maxlength="128" styleClass="txt"/>
       <br />
       <input type="submit" name="doSendLink" value="<ds:lang text="Wyslij"/>" class="btn"/>
    </p>
</html:form>
