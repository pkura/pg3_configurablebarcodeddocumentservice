<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<ds:available test="!layout2">
<p>
<ds:xmlLink path="editDockind"/>
</p>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="editDockind"/>
</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="layout2">
        <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available><ww:if test="currentAssignmentDivision != null">
    <p>
    <ds:lang text="DokumentZnajdujeSie"/>
    <ww:if test="currentAssignmentUser != null">
        <ds:lang text="uUzytkownika"/> <ww:property value="currentAssignmentUser"/>
    </ww:if>
    <ds:lang text="wDziale"/> <ww:property value="currentAssignmentDivision"/>
	<ww:if test="currentCoor"> (<b>koordynator</b>) </ww:if>
    <ww:if test="currentAssignmentAccepted"><b>(<ds:lang text="przyjety"/>)</b></ww:if>
    <ww:else>(<ds:lang text="oczekujacy"/>)</ww:else>
    </p>
</ww:if>

<ww:if test="participantsDescription != null"><p><ww:property value="participantsDescription"/></p></ww:if>

<table width="100%" cellspacing="0">
<ww:iterator value="assignmentHistory">
    <ww:set name="date" scope="page" value="date"/>
    <tr>
   		<td>
	    	<table width="100%" cellspacing="0">
	    		<tr>
	    			<td align="left" class="historyAuthor" ><ww:property value="user"/><ww:property value="divisions"/></td>
	    			<td align="right" class="historyDate" ><fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/></td>
	    		</tr>
	    	</table>
    	</td>
        
    </tr>
    <tr>
        <td class="historyContent">
            <ww:property value="content"/><ww:property value="status"/>
        </td>
    </tr>
</ww:iterator>
</table>

 <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
