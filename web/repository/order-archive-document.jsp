<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<h1><ds:lang text="ZamowDokumentArchiwalny"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form id="form" action="<ww:url value="'/repository/order-archive-document.action'"/>" method="post">

<script type="text/javascript">
			function departmentChange()
			{
				document.getElementById('doReload').value = 'true';
				document.getElementById('form').submit();
			}
</script>
<ww:if test="archiveDocumentId == null">
<ww:hidden name="'doReload'" id="doReload"/>
<table>
	<tr>
		<td>
			<ds:lang text="Departament:"/>
		</td>
		<td>
		    <ww:select name="'department'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="departments" onchange="'departmentChange();'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Wydzia�:"/>
		</td>
		<td>
			<ww:select name="'wydzial'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="wydzialy"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer Oracle:"/>
		</td>
		<td>
			<ww:textfield name="'oracleNo'" id="oracleNo" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Nazwa dokumentu:"/>
		</td>
		<td>
			<ww:textfield name="'name'" id="name" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Znak akt:"/>
		</td>
		<td>
			<ww:textfield name="'znakAkt'" id="znakAkt" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer rejestru FKX:"/>
		</td>
		<td>
			<ww:textfield name="'fkxNo'" id="fkxNo" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Tytu� teczki lub tomu:"/>
		</td>
		<td>
			<ww:textfield name="'title'" id="title" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Rok:"/>
		</td>
		<td>
			<ww:select name="'rok'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="lata"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Zakres lat:"/>
		</td>
		<td>
			<ww:select name="'rokOd'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="lata"/> - <ww:select name="'rokDo'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="lata"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer segregatora:"/>
		</td>
		<td>
			<ww:textfield name="'numerSegregatora'" id="numerSegregatora" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer pude�ka:"/>
		</td>
		<td>
			<ww:textfield name="'boxNumber'" id="boxNumber" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Opis:"/>
		</td>
		<td>
			<ww:textfield name="'description'" id="description" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:event name="'doSearch'" value="'Szukaj'"/>
		</td>
	</tr>
</table>
	<ww:if test="archiveDocuments != null">
		<table id="archiveDocumentsTable" class="tableMargin">
			<tr>
				<th><nobr>
					<ds:lang text="ID"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Departament"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Wydzia�"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Numer Oracle"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Numer FKX"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Nazwa dokumentu"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Znak akt"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Tytu� teczki lub tomu"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Rok"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Numer segregatora"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Numer pude�ka archiwalnego"/>
				</nobr></th>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<th><nobr>
					<ds:lang text="Opis"/>
				</nobr></th>
			</tr>
			<ww:iterator value="archiveDocuments">
				<tr>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="id"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="department"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="wydzial"/></a>
					</td>		
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="oracleNo"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="fkxNo"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="name"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="znakAkt"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="title"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="rok"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="numerSegregatora"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="boxNumber"/></a>
					</td>
					<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
					<td align="center"><a href="<ww:url value="'/repository/order-archive-document.action'"><ww:param name="'archiveDocumentId'" value="id"/></ww:url>">
					<ww:property value="description"/></a>
					</td>
				</tr>	
			</ww:iterator>
		</table>
	</ww:if>
</ww:if>
<ww:else>
<table>
	<tr>
		<td>
			<ds:lang text="Departament"/>:
		</td>
		<td>
			<b><ww:property value="department"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Wydzia�"/>:
		</td>
		<td>
			<b><ww:property value="wydzial"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer Oracle"/>:
		</td>
		<td>
			<b><ww:property value="oracleNo"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer FKX"/>:
		</td>
		<td>
			<b><ww:property value="fkxNo"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Nazwa dokumentu"/>:
		</td>
		<td>
			<b><ww:property value="name"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Znak akt"/>:
		</td>
		<td>
			<b><ww:property value="znakAkt"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Tytu� dokumentu"/>:
		</td>
		<td>
			<b><ww:property value="title"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Rok"/>:
		</td>
		<td>
			<b><ww:property value="rok"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer segergatora"/>:
		</td>
		<td>
			<b><ww:property value="numerSegregatora"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Numer pude�ka archiwalnego"/>:
		</td>
		<td>
			<b><ww:property value="boxNumber"/></b>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Opis"/>:
		</td>
		<td>
			<b><ww:property value="description"/></b>
		</td>
	</tr>
</table>
	<ww:if test="orders != null && orders.size() > 0">
		<table>
			<tr>
				<td>
					<b><ds:lang text="Zamowienia"/></b>
				</td>
			</tr>
			<ww:iterator value="orders">
				<tr>
					<td>
						Zam�wienie : <ww:property value="orderName"/>
					</td>
				</tr>
				<ww:if test="documents != null && documents.size() > 0">
						<tr>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<b>Dokumenty :</b>
							</td>
						</tr>
					<ww:iterator value="documents">
						<tr>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="id"/></ww:url>">
								 <ww:property value="description"/></a>	 
								 <a href="<ww:url value="'/repository/view-attachment-revision.do?id='+revisionId"/>">
									<img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
									title="<ds:lang text="PobierzZalacznik"/>" /></a>
								<ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mime)">
									&nbsp;
									<a href="<ww:url value="'/repository/view-attachment-revision.do?asPdf=true&id='+revisionId"/>">
										<img src="<ww:url value="'/img/pdf.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
										title="<ds:lang text="PobierzZalacznik"/>" /></a>
									&nbsp;
									<a id="viewerLink" href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="revisionId"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);">
										<img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
										height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
								</ww:if>
								 
								 
								 
								 
							</td>
						</tr>
					</ww:iterator>
				</ww:if>
			</ww:iterator>
		</table>
	</ww:if>

</ww:else>
<script type="text/javascript">
function order(btn)
{
	if(confirm('Czy na pewno zam�wi� dokument ?'))
	{
		sendEvent(btn);
		return true;
	}
	return false;
}

function save(btn)
{
	if(confirm('Czy na pewno zapisa� zmiany ?'))
	{
		sendEvent(btn);
		return true;
	}
	return false;
}
</script>
</form>