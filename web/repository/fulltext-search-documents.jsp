<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>


<form name="formul" action="<ww:url value="'/repository/fulltext-search-documents.action'"/>" method="post">

		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
		<table id="bt" class="formTable">
			<tr>
				<td>
					<ds:lang text="SzukanyTekst"/>:
				</td>
				<td>
					<ww:textfield name="'searchText'" size="50" maxlength="250" cssClass="'txt'"/>
					<script type="text/javascript">
						$j('input[name=searchText]').focus();
					</script>
				</td>
			</tr>
		    <ds:available test="fulltextSearch.filetype">
                <tr>
                    <td>
                        <ds:lang text="RozszerzeniePliku"/>:
                    </td>
                    <td>
                        <ww:select name="'extension'" cssClass="'sel'" list="extensionsList"/>
                    </td>
                </tr>
		    </ds:available>
			<tr>
				<td>
					<ds:lang text="LiczbaWynikowNaStronie"/>:
				</td>
				<td>
					<ww:select name="'searchLimit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }"
						value="searchLimit > 0 ? searchLimit : 50"/>
				</td>
			</tr>
		</table>

		<input type="submit" name="doSearch" value="<ds:lang text="Znajdz"/>" class="btn searchBtn"/>
		<input type="text" name="none" style="display: none"/>
		<br />
		
		<ww:if test="luceneResult != null">
			<h2><ds:lang text="WynikiWyszukiwania" /></h2>
			<table id="bt" class="formTable">
				<tr>
					<th><ds:lang text="Id" /></th>
					<th><ds:lang text="Tytul" /></th>
					<th><ds:lang text="NazwaPliku" /></th>
				</tr>
				<ww:iterator value="luceneResult" >
					<tr>
						<td><a href="<ww:url value="'/repository/edit-document.action'" >
							<ww:param name="'id'" value="id" />
						</ww:url>" ><ww:property value="id" /></a></td>
						<td><a href="<ww:url value="'/repository/edit-document.action'" >
							<ww:param name="'id'" value="id" />
						</ww:url>" ><ww:property value="title" /></a></td>
						<td><a href="<ww:url value="'/repository/edit-document.action'" >
							<ww:param name="'id'" value="id" />
						</ww:url>" ><ww:property value="originalFilename" /></a></td>
					</tr>
				</ww:iterator>
			</table>
		</ww:if>
		
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end -->
		</ds:available>
</form>