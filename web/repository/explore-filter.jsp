<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Rejestr filtr�w wyszukiwania"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />


 <ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="SearchFilter"/>
	</div>
</ds:available>

<p></p>

		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
		
	<form action="<ww:url value="'/repository/explore-filter.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="searchFilter.empty">
    	<p><ds:lang text="Brak zapisanych filtr�w"/></p>
	</ww:if>

	<ww:else>
	    <table class="tableMargin">
	        <ww:iterator value="searchFilter">
	            <tr>
	                 <td>
	                	<ww:checkbox name="'deleteKeys'" fieldValue="key"/>
	                </td> 
	                <td>
	                	<a href="<ww:url value="link"/>"><ww:property value="filterName"/></a>
	                </td>
	            </tr>
	        </ww:iterator>
	    </table>
	    
	    <br /> <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
		 
	    <ds:submit-event value="getText('UsunZaznaczone')" name="'doDelete'" cssClass="'btn'"/> 
	</ww:else>
</form>	
	





