
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>

<h1><ds:lang text="OCR"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/repository/ocr.action'"/>" method="post">
	<ww:hidden name="'generated'" id="generated"/>
	<ww:hidden name="'documentId'" id="documentId"/>
	<ww:if test="!generated">
		<ds:lang text="confirmSendToOcr"/>
		<br/>
		<ds:submit-event value="getText('Tak')" name="'doSendToOcr'" cssClass="'btn'" />
		<input type="button" class="btn" value="<ds:lang text="Nie"/>" onClick="history.back()"/>
	</ww:if>
	<ww:else>
		<input type="button" class="btn" value="<ds:lang text="PowrotDoDokumentu"/>"
  			onclick="document.location.href='<ww:url value="'/repository/edit-dockind-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>';"/>
	</ww:else>
</form>
