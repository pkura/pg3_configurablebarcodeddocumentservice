<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N ald-payment-verification.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<!-- 
<a href="<ww:url value="'/repository/search-documents.action'"/>" ><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>"><ds:lang text="WyszukiwanieZaawansowane"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/ald-payment-verification.action'"/>" class="highlightedText"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>"><ds:lang text="WeryfikacjaRejestruVAT"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>"><ds:lang text="InformacjeOkliencie"/></a>
<ds:dockinds test="invoice">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>
-->
<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/repository/ald-payment-verification.action'"/>" method="post" enctype="multipart/form-data">

<input type="hidden" name="pdfUrl" id="pdfUrl"/>
<input type="hidden" name="doExecutePayments" id="doExecutePayments"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    <table>
        <tr>
            <td><ds:lang text="PlikZPlatnosciami"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>
    </table>
    
    <ww:if test="invoiceBeans != null && invoiceBeans.size() > 0">
        <table id="table" class="search" width="100%" >
            <tr>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('supplierNumber',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a><ds:lang text="NrDostawcy"/> <a href="<ww:url value="getSortLink('supplierNumber',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('lineNumber',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a><ds:lang text="NrLinii"/> <a href="<ww:url value="getSortLink('lineNumber',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>  
                </th>
            <%--    <th>
                    <a href="<ww:url value="getSortLink('nrKsiegowania',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>                    
                    <ds:lang text="NrKsiegowania"/>
                    <a href="<ww:url value="getSortLink('nrKsiegowania',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
                </th> --%>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('bookCode',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a><ds:lang text="BookCode"/> <a href="<ww:url value="getSortLink('bookCode',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('invoiceNumber',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="NrFaktury"/> <a href="<ww:url value="getSortLink('invoiceNumber',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('amount',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="Kwota"/> <a href="<ww:url value="getSortLink('amount',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
              <%--  <th>
                    <nobr><a href="<ww:url value="getSortLink('nip',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="Nip"/> <a href="<ww:url value="getSortLink('nip',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th> --%>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('status',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="Status"/> <a href="<ww:url value="getSortLink('status',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>                
                <th><ds:lang text="Zaplacony"/></th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('paymentDate',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="DataZaplaty"/> <a href="<ww:url value="getSortLink('paymentDate',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th></th>
                <th></th>
            </tr>
            <ww:iterator value="invoiceBeans">
                <tr <ww:if test="status == 10">style="color:green"</ww:if><ww:elseif test="status != 1">style="color:red"</ww:elseif>>
                    <td><ww:property value="supplierNumber"/></td>
                    <td><ww:property value="lineNumber"/></td>
                    <td><ww:property value="bookCode"/> <ww:property value="nrKsiegowania"/></td>                    
                    <td><ww:property value="invoiceNumber"/></td>
                    <td><ww:property value="amount"/></td>
                   <%--<td><ww:property value="nip"/></td>--%>
                    <td><ww:property value="statusDescription"/></td>
                    <td>
                        <ww:if test="documentId != null">
                            <ww:if test="paid"><ds:lang text="Tak"/></ww:if>
                            <ww:else>
                                <input id="check" type="checkbox" name="<ww:property value="'payments.doc'+documentId+'_'+id"/>" value="true" payment_status="<ww:property value="status"/>" amount="<ww:property value="origAmount"/>"/>
                            </ww:else>                          
                        </ww:if>
                    </td>
                    <td><ds:format-date value="paymentDate" pattern="dd-MM-yyyy"/></td>
                    <td>
                        <ww:if test="documentId != null">
                            <a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>"><ds:lang text="dokument"/></a>
                        </ww:if>
                    </td>
                    <td>
                        <ww:if test="viewerLink != null">
                            <a href="javascript:openToolWindow('<ww:url value="viewerLink"/>', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznik"/>"/></a>
                        </ww:if>
                    </td>
                </tr>
            </ww:iterator>  
            <tr><td></td>
                <td colspan="7"></td>
                <td colspan="2"><a href="javascript:selectAll()"><ds:lang text="zaznacz"/></a>/<a href="javascript:unselectAll()"><ds:lang text="odznacz"/></a> <ds:lang text="wszystkie"/></td>
                <td></td>
            </tr>
            <tr>
                
                <ww:if test="pdfUrl!=null">
                    
                    <td><input type="submit" class="btn" value="<ds:lang text="PobierzRaportPDF"/>" onclick="javascript:window.open('<ww:url value="/repository/ald-payment-verification.action"/>?streamPdf=true&pdfUrl=<ww:property value="pdfUrl"/>');"/></td>
                    <td colspan="6"></td>
                </ww:if>
                <ww:else>
                    <td colspan="7"></td>
                </ww:else>
                <td><input type="submit" value="<ds:lang text="ZaplaconyWDniu"/>" class="btn" onclick="onExecute();"/></td>
                
                <td>
                    <ww:textfield name="'date'" id="date" size="10" maxlength="10" cssClass="'txt'"/>
                    <img src="<ww:url value="'/calendar096/img.gif'"/>"
                        id="dateButton" style="cursor: pointer; border: 1px solid red;"
                        title="Date selector" onmouseover="this.style.background='red';"
                        onmouseout="this.style.background=''"/>
                </td>
            </tr>          
            </table>
            <script type="text/javascript" defer="true">
                Calendar.setup({
                inputField     :    "date",     // id of the input field
                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                button         :    "dateButton",  // trigger for the calendar (button ID)
                align          :    "Bl",           // alignment (defaults to "Bl")
                singleClick    :    true
            });
            </script>
        
    </ww:if>
    
    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script type="text/javascript">
    

    var prefix = "check";
    tableId="table";
    prepareCheckboxes();
        

    function selectAll()
    {
        var form = document.forms[0];
        for (var i=0; i < form.elements.length; i++)
        {
            var el = form.elements[i];
            if ((el.type.toLowerCase() == 'checkbox') && (el.getAttribute('payment_status') == 1))
            {
                el.checked = true;
            }
        }
    }
    
    function unselectAll()
    {
        var form = document.forms[0];
        for (var i=0; i < form.elements.length; i++)
        {
            var el = form.elements[i];
            if (el.type.toLowerCase() == 'checkbox')
            {
                el.checked = false;
            }
        }
    }
    
    function onExecute()
    {
        var form = document.forms[0];
        var suma = 0;
        var count = 0;
        for (var i=0; i < form.elements.length; i++)
        {
            var el = form.elements[i];
            if (el.type.toLowerCase() == 'checkbox' && el.checked)

            {
                count++;
                suma = parseFloat(suma) + parseFloat(el.getAttribute('amount'))*100;
            }
        }
        suma = suma / 100;
        var ret = confirm('<ds:lang text="ZamierzaszZaplacic"/> '+count+' <ds:lang text="faktur(zaznaczoneNaRaporcie)NaLacznaKwoteBrutto(sumeTychFaktur)"/>: '+suma);
        if(ret)
            document.getElementById('doExecutePayments').value='true';
    }    
</script>