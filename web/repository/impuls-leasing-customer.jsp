<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N impuls-leasing-customer.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.crm.Contractor" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1><ds:lang text="InformacjeOkliencie"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<!-- 
<a href="<ww:url value="'/repository/search-documents.action'"/>" ><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>"><ds:lang text="WyszukiwanieZaawansowane"/></a>
<ds:dockinds test="ald">
    <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
    <a href="<ww:url value="'/repository/ald-payment-verification.action'"/>"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
    <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
    <a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>"><ds:lang text="WeryfikacjaRejestruVAT"/></a>    
</ds:dockinds>
<ds:dockinds test="dl">
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>" class="highlightedText"><ds:lang text="InformacjeOkliencie"/></a>
</ds:dockinds>
<ds:dockinds test="invoice">
    <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
    <a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>
-->
<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/repository/impuls-leasing-customer.action'"/>" method="post" enctype="multipart/form-data">

<input type="hidden" name="pdfUrl" id="pdfUrl"/>
<input type="hidden" name="id" id="id"/>
<input type="hidden" name="doExecutePayments" id="doExecutePayments"/>
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

    <table>
    	<tr>
    		<td>
    			 <input type="button" value="<ds:lang text="WybierzKlienta"/>" onclick="openDictionary_KLIENT()" class="btn" />
    		</td>
    	</tr>
    
    
        <tr>
            <td><ds:lang text="NumerKlienta"/>:</td>
            <td><ww:textfield name="'klient'" size="10" maxlength="10" cssClass="'txt'" id="klient"  /></td>
            <td></td>
            <td><ds:event name="'doSearch'" value="'Poka�'" cssClass="'btn'"/></td>
        </tr>
    </table>
<ww:if test="documents.size()>0"> 	
    <table>
   		<tr>
    	<th class="highlightedText"><nobr><ds:lang text="Dokumenty"/>:</nobr></th>
    	</tr>
        <tr>
    		<hr size="1" align="left" class="horizontalLine" width="77%" />
    	</tr>
      	<TR ALIGN=center>
    		<th><nobr>ID</nobr></th>

    		<th><nobr>Rodzaj dokumentu</nobr></th>

    		<th><nobr>Typ documentu</nobr></th>

    		<th><nobr>Data dokumentu</nobr></th>

    		&nbsp
    	</TR>
        <ww:iterator value="documents">
        	<ww:set name="document"/>
            	<tr>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['id']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['rodzajDokumentu']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['typDokumentu']"/>&nbsp</td>
             	   <td><a href="<ww:url value="#document['link']"/>"><ww:property value="#document['data']"/></td>
           	 	</tr>
        </ww:iterator>
    	<tr><td>&nbsp</td></tr>
    </table>

</ww:if>
    
	
 <ww:if test="results.size()>0">   
    <table>
        <tr><td>&nbsp</td></tr>
    	<tr>
    		<th class="highlightedText"><nobr>Umowy :</nobr></th>
    	</tr>
    	<tr>
    		<hr size="1" align="left" class="horizontalLine" width="77%" />
    	</tr>
    	<tr ALIGN=center>
            <th>Numer umowy</th>
            <th>Opis umowy</th>
            <th>Warto�� umowy</th>
            <th>Data umowy</th>
            <th>Data ko�ca umowy</th>
        </tr>
    	<ww:iterator value="results">
        	<tr>
        		<td><a href="<ww:url value="'/repository/impuls-leasing-contract.action'"/>?id=<ww:property value="id"/>"><ww:property value="NumerUmowy" /></td>
                <td><a href="<ww:url value="'/repository/impuls-leasing-contract.action'"/>?id=<ww:property value="id"/>"><ww:property value="OpisUmowy" /></td>
                <td><a href="<ww:url value="'/repository/impuls-leasing-contract.action'"/>?id=<ww:property value="id"/>"><ww:property value="WartoscUmowy" /></td>
                <td><a href="<ww:url value="'/repository/impuls-leasing-contract.action'"/>?id=<ww:property value="id"/>"><ww:property value="DataUmowy" /></td>
                <td><a href="<ww:url value="'/repository/impuls-leasing-contract.action'"/>?id=<ww:property value="id"/>"><ww:property value="DataKoncaUmowy" /></td>
        	</tr>
    	</ww:iterator>
    </table>
</ww:if>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
<script type="text/javascript">
   function openDictionary_KLIENT()
    {
        openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=KLIENT','KLIENT', 700, 650);
    }
    
   function accept(param,map)
   {
       document.getElementById('klient').value = map.id;
   }
    
</script>  
        
</form>