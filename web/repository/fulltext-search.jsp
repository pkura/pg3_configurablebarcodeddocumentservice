<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>

<ds:available test="layout2">
	<div id="middleContainer">
</ds:available>

<div id="messages">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</div>

<style type="text/css">
	.app {
		/*text-align: center;*/
	}

	.search-query {
		width: 600px !important;
		font-size: 20px;
	}

	.search-button {
		font-size: 20px;
	}

	.row {
		display: table-cell;
		width: 100px;
	}

	.rows-header {
		font-weight: bold;
		display: table-header-group;
	}

	.result-table {
		font-size: 16px;
		display: table;
	}

	.row {
		padding: 5px;
		overflow-wrap: break-word;
	}
	
	.row em{
		color: red;
	}
</style>

<div class="app" ng-app="FullTextSearchApp" ng-controller="SearchCtrl">

	<form ng-submit="search(query)">
		<input class="search-query" ng-model="query" size="50" autofocus="">
		<input type="submit" class="search-button" value="Wyszukaj">
	</form>

	<div class="result">
		<div class="result-header">
			<span ng-if="state=='waiting'">
				Znaleziono {{documents.length}} {{documents.length == 1 ? 'wynik' : documents.length == 2 ? 'wyniki' : 'wyników'}}.
			</span>
			<span ng-if="state=='searching'">
				Szukam...
			</span>
		</div>
		<div class="result-table">
			<div class="rows-header">
				<div class="row">
					Identyfikator
				</div>
				<div class="row">
					Numer KO
				</div>
				<div class="row">
					Autor
				</div>
				<div class="row">
					Tytuł/Opis
				</div>
				<div class="row">
					Barcode
				</div>
				<div class="row">
					Data utworzenia (Dockind)
				</div>
				<div class="row">
					Data utworzenia (Ctime)
				</div>
				<div class="row">
					Sprawa
				</div>
				<div class="row">
					Aktualnie przyporządkowana osoba
				</div>
				<div class="row">
					Nr przesyłki rejestrowanej (R)
				</div>
			</div>
			<div ng-repeat="doc in documents" style="display:table-row;">
				<div class="row">
					<a href="{{getDocumentLink(doc.id)}}" ng-bind-html="getValue(doc.id) | highlight:query | unsafe">
					</a>
				</div>
				<div class="row" ng-bind-html="getField(doc.fields, 'DOC_OFFICENUMBER') | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getValue(doc.authorName) | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getField(doc.fields, 'DOC_DESCRIPTION') | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getField(doc.fields, 'DOC_BARCODE') | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getField(doc.fields, 'DOC_DATE') | date:'yyyy-MM-dd' | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getValue(doc.ctime) | date:'yyyy-MM-dd' | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getValue(doc.caseDocumentId) | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getValue(doc.currentAssignmentUsername) | highlight:query | unsafe">
				</div>
				<div class="row" ng-bind-html="getField(doc.fields, 'POSTAL_REG_NR') | highlight:query | unsafe">
				</div>
			</div>
		</div>
	</div>

</div>

<ds:available test="layout2">
	</div>
</ds:available>

<script type="text/javascript">
	var CONTEXT_PATH = "<c:out value='${pageContext.request.contextPath}'/>";
</script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/URI-1.11.2.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/lodash.compat-2.4.1.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/angular/angular-1.2.7.min.js"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/js/ajaxAction.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/repository/fulltext-search-app.js?v=<%= Docusafe.getDistTimestamp() %>"></script>