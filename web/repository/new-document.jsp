<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-document.jsp N-->

<%@ page import="org.apache.struts.action.DynaActionForm"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script language="JavaScript">
function pickFolder(id, path)
{
    document.getElementById('folderId').value = id;
    document.getElementById('folderPath').value = path;
}
</script>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><edm-html:message key="newDocument.h1"></edm-html:message></h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/repository/new-document" onsubmit="disableFormSubmits(this);" >
<html:hidden property="folderId" styleId="folderId" />
<input type="hidden" name="doCreate" id="doCreate"/>
<%--<input type="hidden" id="folderId" name="folderId"/>--%>
<%--
<bean:define id="folderId" name="newDocument" property="folderId" type="java.lang.Long"/>
<script language="JavaScript">
var folderId = parseLong('<%= ((DynaActionForm) pageContext.findAttribute("newDocument")).get("folderId") %>');
if (folderId == 0)
    folderId = parseLong(document.getElementById('folderId').value);
</script>
--%>



<table>
<tr>
    <td><edm-html:message key="document.title"/><span class="star">*</span>:</td>
    <td><html:text property="title" styleId="title" size="50" maxlength="254" styleClass="txt" /></td>
</tr>
<tr>
    <td><edm-html:message key="document.description"/><span class="star">*</span>:</td>
    <td>
        <html:textarea property="description" styleId="description" styleClass="txt" rows="4" cols="50"></html:textarea>
    </td>
</tr>
<tr>
    <td><ds:lang text="TypDokumentu"/>:</td>
    <td><html:select property="doctypeId" styleClass="sel">
        <html:option value=""><ds:lang text="select.wybierz"/></html:option>
        <html:optionsCollection name="doctypes"/>
        </html:select>
    </td>
</tr>
<%--
<tr>
    <td><edm-html:message key="document.externalId"/>:</td>
    <td><html:text property="externalId" size="50" maxlength="128" styleClass="txt_opt" /></td>
</tr>
--%>
<%--
<tr>
    <td><edm-html:message key="document.type"/>:</td>
    <td>
        <html:select property="type" styleClass="sel_opt" >
            <html:option value=" "><edm-html:message key="document.chooseType.option"/></html:option>
        </html:select>
    </td>
</tr>
--%>
<tr>
    <td><edm-html:message key="document.folder"/>:</td>
    <td><c:out value="${folderPrettyPath}"/></td>
</tr>
<tr>
    <td></td>
    <td>
        <input type="submit" value="Utw�rz" class="btn"
            onclick="if (!validateForm()) return false; document.getElementById('doCreate').value='true';"/>
        <input type="button" value="Rezygnuj" class="btn"
            onclick="document.location.href='<c:out value="${pageContext.request.contextPath}/repository/explore-documents.do"/>';" />
    </td>
</tr>
</table>

</html:form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<script>
function validateForm()
{
    var title = document.getElementById('title').value;
    if (title == null || title.length == 0) { alert('<ds:lang text="NiePodanoTytuluDokumentu"/>'); return false; }

    var description = document.getElementById('description').value;
    if (description == null || description.length == 0) { alert('<ds:lang text="NiePodanoOpisuDokumentu"/>'); return false; }
    if (description != null && description.length > 510) { alert('<ds:lang text="OpisDokumentuJestZbytDlugi.MaksymalnaLiczbaZnakowWOpisieTo510"/>'); return false; }
    return true;
}
</script>