<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N find-document.jsp N-->

<%@ page import="java.text.DateFormat,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h1><edm-html:message key="findDocument.h1"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<edm-html:errors />
<edm-html:messages />

<html:form action="/repository/find-document">

<c:if test="${empty results}">
    <table>
    <tr>
        <td><ds:lang text="Identyfikator"/>:</td>
        <td><html:text property="documentId" size="10" maxlength="10" styleClass="txt" /></td>
    </tr>
    <tr>
        <td colspan="2"><hr/></td>
    </tr>
    <tr>
        <td><ds:lang text="Tytul"/>:</td>
        <td><html:text property="title" size="50" maxlength="50" styleClass="txt" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Opis"/>:</td>
        <td><html:text property="description" size="50" maxlength="50" styleClass="txt" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Atrybuty"/>:</td>
        <td><html:text property="attribute" size="50" maxlength="50" styleClass="txt"/></td>
    </tr>
    <tr>
        <td><ds:lang text="Typy"/>:</td>
        <td>
            <html:select property="doctypeIds" styleClass="sel" multiple="true" size="4" >
                <html:optionsCollection name="doctypes"/>    
            </html:select>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="KodKreskowy"/>:</td>
        <td><html:text property="attachmentBarcode" styleId="attachmentBarcode" size="50" maxlength="25" styleClass="txt" /></td>
    </tr>
    <tr>
        <td><ds:lang text="ZakresDatUtworzenia"/>:</td>
        <td>
            <edm-html:message key="findDocument.fromDate" />
            <html:text property="startCtime" styleId="startCtime" size="10" maxlength="10" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_startCtime_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            <edm-html:message key="findDocument.toDate" />
            <html:text property="endCtime" styleId="endCtime" size="10" maxlength="10" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_endCtime_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="ZakresDatModyfikacji"/>:</td>
        <td>
            <edm-html:message key="findDocument.fromDate" />
            <html:text property="startMtime" styleId="startMtime" size="10" maxlength="10" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_startMtime_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            <edm-html:message key="findDocument.toDate" />
            <html:text property="endMtime" styleId="endMtime" size="10" maxlength="10" styleClass="txt_opt" />
            <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                id="calendar_endMtime_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="Autor"/>:</td>
        <td>
            <html:select property="author" styleClass="sel" >
                <html:option value=" ">-- <ds:lang text="WszyscyUzytkownicy"/> --</html:option>
                <html:optionsCollection name="userOptions"/>>
            </html:select>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="LiczbaWynikowNaStronie"/>:</td>
        <td><html:select property="limit" styleClass="sel" >
                <html:option value="10">10</html:option>
                <html:option value="25">25</html:option>
                <html:option value="50">50</html:option>
            </html:select>
        </td>
    </tr>
    </table>

    <html:submit property="doSearch" styleClass="btn"><edm-html:message key="doSearch" global="true" /></html:submit>

    <script type="text/javascript">
        Calendar.setup({
            inputField     :    "startCtime",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_startCtime_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });

        Calendar.setup({
            inputField     :    "endCtime",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_endCtime_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
        Calendar.setup({
            inputField     :    "startMtime",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_startMtime_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });

        Calendar.setup({
            inputField     :    "endMtime",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_endMtime_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });

<%--
        <c:if test="${useBarcodes}">
        var bi = new BarcodeInput({
            'prefix' : '<c:out value="${barcodePrefix}"/>',
            'suffix' : '<c:out value="${barcodeSuffix}"/>',
            'inputId' : 'attachmentBarcode',
            'submit' : false,
            //'submitId' : 'send',
            'tabStarts' : false
        });
        </c:if>
--%>

    </script>

</c:if>

</html:form>



<c:if test="${!empty results}">

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="search">
    <tr>
        <th><nobr>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['id_desc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-lewo.gif"/>" width="11" height="11" border="0"/></a>
            ID
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['id_asc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-prawo.gif"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['title_desc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-lewo.gif"/>" width="11" height="11" border="0"/></a>
            <ds:lang text="Tytul"/>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['title_asc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-prawo.gif"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['ctime_desc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-lewo.gif"/>" width="11" height="11" border="0"/></a>
            <ds:lang text="DataUtworzenia"/>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['ctime_asc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-prawo.gif"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['mtime_desc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-lewo.gif"/>" width="11" height="11" border="0"/></a>
            <ds:lang text="DataModyfikacji"/>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['mtime_asc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-prawo.gif"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['author_desc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-lewo.gif"/>" width="11" height="11" border="0"/></a>
            <ds:lang text="Autor"/>
            <a href='<c:out value="${pageContext.request.contextPath}${sorting['author_asc']}"/>'><img src="<c:out value="${pageContext.request.contextPath}/img/strzalka-prawo.gif"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
    </tr>
    <c:forEach items="${results}" var="bean">
        <tr>
        <td>
            <c:out value="${bean.id}"/>
        </td>
        <td>
            <a href='<c:out value="${bean.editLink}"/>'>
                <c:out value='${bean.title}'/>
            </a>
        </td>
        <td>
            <fmt:formatDate value="${bean.ctime}" type="both" pattern="dd-MM-yy HH:mm"/>
        </td>
        <td>
            <fmt:formatDate value="${bean.mtime}" type="both" pattern="dd-MM-yy HH:mm"/>
        </td>
        <td>
            <c:out value="${bean.author}"/>
        </td>
        </tr>
    </c:forEach>
    </table>

    <table width="100%">
    <tr>
        <td align="center"><jsp:include page="/pager-links-include.jsp"/></td>
    </tr>
    </table>

    <%--
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="50%"><c:if test="${!empty prevPageLink}">
            <a href='<c:out value="${prevPageLink}"/>'>poprzednia strona</a>
        </c:if></td>
        <td width="50%" align="right"><c:if test="${!empty nextPageLink}">
            <a href='<c:out value="${nextPageLink}"/>'>nast�pna strona</a>
        </c:if></td>
    </tr>
    </table>
    --%>
</c:if>