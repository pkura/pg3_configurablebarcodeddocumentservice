<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="GropowaZmianaAtrybutow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/repository/multi-change-attributes.action'"/>" method="post">
<ww:hidden name="'documentKindCn'" id="documentKindCn"/>
<ww:if test="documentKindName != null">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><ds:lang text="LiczbaDokumentow"/>:</td>
	<td><ww:property value="count"/></td>
</tr>
<tr>
	<td><ds:lang text="RodzajDokumentuDocusafe"/>:</td>
	<td><ww:property value="documentKindName"/></td>
</tr>
<ww:if test="documentAspects != null">
		<tr>
			<td>
				<ds:lang text="AspektDokumentu"/>:
			</td>
			<td>
				<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'document.forms[0].submit();'"/>
			</td>
		</tr>
</ww:if>
<tr>
	<td>
			<jsp:include page="/common/dockind-fields.jsp"/>
			<jsp:include page="/common/dockind-specific-additions.jsp"/>
	</td>
</tr>
<tr>
	<td>
		<input type="submit" name="doChangeAttributes" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" onclick="return countConfirm()"/>
	</td>
	<td></td>
</tr>
</table>
</ww:if>
<ww:else>
	<a href="<ww:url value='redirectUrl'/>" >raport</a>
</ww:else>


</form>

<script type="text/javascript">
	if(document.getElementById('tr_dockind_BARCODE') != null)
	{
		document.getElementById('tr_dockind_PNA').style.display = "none";
		document.getElementById('tr_dockind_BARCODE').style.display = "none";
	}
	function countConfirm()
	{
		return confirm('Czy napewno chcesz zmieni� warto�ci w <ww:property value="count"/> dokumentach?');
	}
</script>