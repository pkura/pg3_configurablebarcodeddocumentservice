<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-document-locked.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1><ds:lang text="EdycjaDokumentu"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<form action="<ww:url value="'/repository/edit-document.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
<ww:hidden name="'id'"/>
<ww:hidden name="'folderId'"/>
<ww:hidden name="'queryLink'"/>

<p><ds:lang text="DokumentZostalZablokowanyPrzezUzytkownika"/> <ww:property value="lockedBy"/></p>

<p><ds:lang text="WyslijProsbeOodblokowanieDokumentu"/>:</p>

<table>
<tr>
    <td valign="top"><ds:lang text="PowodProsby"/> <span class="star">*</span>:</td>
    <td><ww:textarea name="'unlockReason'" cols="50" rows="4" cssClass="'txt'"/></td>
</tr>
<tr>
    <td></td>
    <td>
        <ds:submit-event value="getText('WyslijProsbe')" name="'doSendUnlockRequest'" cssClass="'btn'"/>
    </td>
</tr>
</table>

</form>
