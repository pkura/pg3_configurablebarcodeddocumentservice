var app = angular.module('FullTextSearchApp', []);

app.service("AjaxAction", ajaxActionFactory);
app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });
app.filter('highlight', 
		function() { 
			return function(input, query){
				if(input==="undefined" || input == "null" || input == null)
					return "";
				
				splittedQuery = query.split(" ");
				
				var index, len;

				for(index = 0, len = splittedQuery.length; index < len; ++index){
					input = input.replace(new RegExp( "(" + splittedQuery[index] + ")" , 'gi' ), '<em>$&</em>');
				}

				return input;
			}; 
		}
	);

function SearchCtrl($scope, AjaxAction) {
	$scope.query = "";

	$scope.state = "waiting";

	$scope.documents = [];

	$scope.search = function(query) {

		var tQuery = $j.trim(query);

		if(tQuery === "") {
			return false;
		} else if(tQuery[0] === "?" || tQuery[0] === "*") {
			return false;
		}

		$scope.state = "searching";

		setTimeout(function(){
			AjaxAction.post("search", {}, {query: query}).done(function(result){
				$scope.documents = result;
				$scope.state = "waiting";
			});
		},250);
	}

	$scope.getValue = function(field){
		return field+"";
	} 
	
	$scope.getField = function(fields, cn) {
		var ret = _.find(fields, function(field) {
			return field.cn === cn;
		});

		return _.isUndefined(ret) ? "" : ret.value;
	}
	
	$scope.getDocDescription = function(fields) {
		var ret = _.find(fields, function(field) {
			return field.cn === "DOC_DESCRIPTION";
		});

		return _.isUndefined(ret) ? "" : ret;
	}

	$scope.getDocumentLink = function(id) {
		var uri = URI(CONTEXT_PATH);
		return uri
			.directory(uri.directory() + "/repository")
			.segment([uri.filename(), "repository", "edit-dockind-document.action"])
			.search({id: id})
			.toString();
	}
	
}