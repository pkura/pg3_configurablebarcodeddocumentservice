<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N aegon-invoice-reports.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<!--
<a href="<ww:url value="'/repository/search-documents.action'"/>" ><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>" ><ds:lang text="WyszukiwanieZaawansowane"/></a>
<ds:dockinds test="ald">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-payment-verification.action'"/>"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>"><ds:lang text="WeryfikacjaRejestruVAT"/></a>
</ds:dockinds>
<ds:dockinds test="dl">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>"><ds:lang text="InformacjeOkliencie"/></a>
</ds:dockinds>
<ds:dockinds test="invoice">
    <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
    <a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>" style="color:#813526"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>

-->
<br/>
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<br/>


<form id="form" action="<ww:url value="'/repository/aegon-invoice-reports.action'"/>" method="post">

<input type="hidden" name="doGenerate" id="doGenerate"/>
<input type="hidden" name="doPay" id="doPay"/>
<ww:hidden name="'listUnpaidInvoices'" value="listUnpaidInvoices"/>
<ww:hidden name="'maxReportNumber'" value="maxReportNumber"/>


    <ww:if test="!listUnpaidInvoices">
		<ww:set name="pager" scope="request" value="pager"/>
        <table width="100%">
            <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        </table>
        <table>
            <th>
                <ds:lang text="NumerZestawienia"/>
            </th>
            <th>
                <ds:lang text="IloscFakturWZestawieniu"/>
            </th>
            <th>
                <ds:lang text="Data"/>
            </th>
            <th>
            </th>
            <tr/><tr/>
            <ww:iterator value="reports">
				<tr>
                    <td>
                        <ww:property value="id"/>
                    </td>
                    <td>
                        <ww:property value="count"/>
                    </td>
                    <td>
                        <ww:property value="StrWhen"/>
                    </td>
                    <td>
                        <a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>?download=<ww:property value="id"/>"><ds:lang text="Pobierz"/></a>
                    </td>
                </tr>
            </ww:iterator>
        </table>
		
        <br/><br/>
            <ds:lang text="IloscFakturNieBedacychWZadnymZestawieniu"/> : <ww:property value="unreportedInvoices"/>
        <br/><br/>

        <ww:if test="unreportedInvoices>0">
        <ww:if test="canListUnpaidInvoices">
            <input type="submit" value="Generuj nowe zestawienie" class="btn" onclick="if (!confirm('Na pewno?')) return false; document.getElementById('doGenerate').value='true'"/>
        </ww:if>
        <ww:else>
        	<ds:lang text="NieMaszUprawnienDoGenerowaniaZestawienia"/>
        </ww:else>    
        </ww:if>

        <br/><br/>

        <ww:if test="canListUnpaidInvoices">
            <a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>?listUnpaidInvoices=true"><ds:lang text="ListaFakturNiezaplaconychZnajdujaychSieWZestawieniach"/></a>
        </ww:if>

    </ww:if>
    <ww:else>
        <ww:if test="reports.size()>0">
            <table>
            <th/>
            <th>
                <ds:lang text="NumerZestawienia"/>
            </th>
            <th>
                <ds:lang text="KwotaBrutto"/>
            </th>
            <th>
                <ds:lang text="Dostawca"/>
            </th>
            <th>
                <ds:lang text="DataPlatnosci"/>
            </th>
            <ww:iterator value="reports">
                <tr>
                    <td>
                        <ww:checkbox name="'Ids'" fieldValue="id"/>
                    </td>
                    <td>
                        <ww:property value="listingNo"/>
                    </td>
                    <td>
                        <ww:property value="grossAmount"/>
                    </td>
                    <td>
                        <ww:property value="supplyer"/>
                    </td>
                    <td>
                        <ww:property value="paymentDate"/>
                    </td>
                </tr>
            </ww:iterator>
        </table>

        <br/>
        <input type="submit" value="Zap�a� w dniu: " class="btn" onclick="if (!confirm('Na pewno?')) return false; document.getElementById('doPay').value='true'"/>
        &nbsp&nbsp&nbsp&nbsp
        <ww:textfield name="'date'" id="date" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_date_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
    </ww:if>
    <ww:if test="reports.size()==0">
        <h4>
            <ds:lang text="WszystkieFakturyZnajdujaceSieWZestawieniachSaZaplacone"/>
        </h4>
    </ww:if>
    <br/><br/>
        <a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="Powrot"/></a>
    </ww:else>

</form>
<ww:if test="listUnpaidInvoices && reports.size()>0">
<script type="text/javascript">
    Calendar.setup({
        inputField     :    "date",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_date_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</ww:if>
</script>