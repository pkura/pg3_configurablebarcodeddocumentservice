<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ExportDokumentow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form name="ExportDocumentsForm" action="<ww:url value="'/repository/export-documents.action'"/>" method="post">
	<table>
		<tr <ds:available test="dockind.select.hide">style="display:none"</ds:available> >
			<td>
				<ds:lang text="RodzajDokumentuDocusafe"/>:
			</td>
			<td>
				<ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key"
					listValue="value" value="documentKindCn" cssClass="'sel'"/>
				<script type="text/javascript">
					$j('#documentKindCn').change(function(){
						document.forms.ExportDocumentsForm.submit();
					});
				</script>
			</td>
		</tr>
		<ww:if test="docTypes.size() > 0">
		<tr>
			<td></td>
			<td>
				<ww:select id="docTypeId" name="'docTypeId'" list="docTypes" listKey="key"
					listValue="value" value="documentKindCn" cssClass="'sel'" headerKey="''" headerValue="getText('wszystkie')" />
			</td>
		</tr>
		</ww:if>
		<tr>
			<td>
				<ds:lang text="SciezkaNaSerwerze"/>:
			</td>
			<td>
				<ww:textfield name="'path'" value="path" cssClass="'txt'" size="50" ></ww:textfield>
			</td>
		</tr>
		<tr>
			<td>
				<ds:submit-event value="'Eksportuj'" name="'doExport'"  />
			</td>
		</tr>
	</table>
</form>