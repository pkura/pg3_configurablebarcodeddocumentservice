<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<h1><ds:lang text="NowaTeczka"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form enctype="multipart/form-data" action="<ww:url value="'/repository/new-portfolios-for-doc.action'"/>" method="post" >
	<ww:include page="/repository/new-portfolios-base.jsp"/>
		<ww:if test="showPage == 4">
			<ww:iterator value="docsId">
				<ww:set name="result"/>
				<ww:hidden name="'documentIDS'" value="#result"/>
			</ww:iterator>
			<script language="JavaScript">
				window.opener.changeStatusDocs(document.getElementsByName('documentIDS'));
			</script>
		</ww:if>
</form>
