<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N favourites.jsp N-->

<%@ page import="pl.compan.docusafe.core.base.Attachment,
				 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-html-el" prefix="html-el" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %> 
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-security" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ObserwowaneDokumenty"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<c:forEach items="${tabs}" var="tab" varStatus="status" >
	<a class="bigger <c:if test="${tab.selected}"><ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">highlightedText</ds:additions></c:if>" href="<c:out value="${pageContext.request.contextPath}${tab.link}"/>"><c:out value="${tab.title}"/></a>
	<c:if test="${!status.last}"><img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/></c:if>
</c:forEach>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<c:forEach items="${tabs}" var="tab" varStatus="status" >
		<a class="middleMenuLink<c:if test="${tab.selected}"> middleMenuLinkHover middleMenuLinkSelected</c:if>" href="<c:out value="${pageContext.request.contextPath}${tab.link}"/>" <c:if test="${tab.selected}">id="tab2_selected"</c:if>>
			<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left<c:if test="${tab.selected}">_pushed</c:if>.gif" class="tab2_img" />
			<c:out value="${tab.title}"/>
		</a>
	</c:forEach>
</div>
</ds:available>

<edm-html:errors/>
<edm-html:messages/>

<html:form action="/repository/favourites" >
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
	<html:hidden property="tab"/>
	<html:hidden property="sortField"/>

	<table id="mainTable" class="search" cellspacing="0">
		<tr>
			<th></th>
			<th>
				<span class="continuousElement">
					<a name="docIDDesc" href='<c:out value="${pageContext.request.contextPath}${sorting['documentId_desc']}"/>'>
						<ww:if test="sortField == property && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" /></ww:if>
						<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></ww:else>
					</a>
					<ds:lang text="ID"/>
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentId_asc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
		 	</th>
			<th>
				<span class="continuousElement">
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentTitle_desc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					<ds:lang text="Tytul"/>
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentTitle_asc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['folderTitle_desc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					<ds:lang text="Lokalizacja"/>
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['folderTitle_asc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a name="modifDateDesc" href='<c:out value="${pageContext.request.contextPath}${sorting['documentMtime_desc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					<ds:lang text="DataModyfikacji"/>
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentMtime_asc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
			<th>
				<span class="continuousElement">
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['lastname_desc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
					<ds:lang text="Autor"/>
					<a href='<c:out value="${pageContext.request.contextPath}${sorting['lastname_asc']}"/>'>
						<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
				</span>
			</th>
		</tr>
		
		<c:forEach items="${watches}" var="watch">
			<c:choose>
				<c:when test="${watch.documentInaccessible}">
					<tr>
						<td>
							<html-el:multibox title="${watch.documentTitle}" property="watchIds" value="${watch.id}"/>
						</td>
						<td>
							<c:out value="${watch.documentId}"/>
						</td>
						<td>
							<c:out value="${watch.documentTitle}"/>
						</td>
						<td>
							<c:out value="${watch.folderTitle}"/>
						</td>
						<td>
							<fmt:formatDate  value="${watch.documentCtime}" type="both" pattern="dd-MM-yy HH:mm"/>
						</td>
						<td>
							<c:out value="${watch.user}"/>
						</td>
					</tr>
				</c:when>
				
				<c:otherwise>
					<tr>
						<td>
							<html-el:multibox title="${watch.documentTitle}" property="watchIds" value="${watch.id}"/>
						</td>
						<td>
							<a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.documentId}"/></a>
						</td>
						<td>
							<a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.documentTitle}"/></a>
						</td>
						<td>
							<a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.folderTitle}"/></a>
						</td>
						<td>
							<a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><fmt:formatDate  value="${watch.documentMtime}" type="both" pattern="dd-MM-yy HH:mm"/></a>
						</td>
						<td>
							<a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.user}"/></a>
						</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</table>
	
	<c:if test="${!empty watches}">
		<input type="submit" name="doRemove" value="<ds:lang text="UsunZObserwowanych"/>"  class="btn"/><br/>
	</c:if>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
	</ds:available>
</html:form>

<%--
<h1><ds:lang text="ObserwowaneDokumenty"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<c:forEach items="${tabs}" var="tab" varStatus="status" >
	<a class="bigger" <c:if test="${tab.selected}">class="highlightedText"</c:if> href="<c:out value="${pageContext.request.contextPath}${tab.link}"/>"><c:out value="${tab.title}"/></a>
	<c:if test="${!status.last}"><img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/></c:if>
</c:forEach>

<p></p>

<edm-html:errors />
<edm-html:messages />

<html:form action="/repository/favourites" >
<html:hidden property="tab"/>
<html:hidden property="sortField"/>

<table width="100%" class="search">
	<tr>
		<th></th>
        <th><nobr>
			<a name="docIDDesc" href='<c:out value="${pageContext.request.contextPath}${sorting['documentId_desc']}"/>'><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="ID"/>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentId_asc']}"/>'><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentTitle_desc']}"/>'><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Tytul"/>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentTitle_asc']}"/>'><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['folderTitle_desc']}"/>'><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Lokalizacja"/>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['folderTitle_asc']}"/>'><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
			<a name="modifDateDesc" href='<c:out value="${pageContext.request.contextPath}${sorting['documentMtime_desc']}"/>'><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="DataModyfikacji"/>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['documentMtime_asc']}"/>'><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
        <th><nobr>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['lastname_desc']}"/>'><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/></a>
			<ds:lang text="Autor"/>
			<a href='<c:out value="${pageContext.request.contextPath}${sorting['lastname_asc']}"/>'><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/></a>
        </nobr></th>
	</tr>
	<c:forEach items="${watches}" var="watch">
		<c:choose>
			<c:when test="${watch.documentInaccessible}">
				<tr>
					<td><html-el:multibox title="${watch.documentTitle}" property="watchIds" value="${watch.id}"/></td>
					<td><c:out value="${watch.documentId}"/></td>
					<td><c:out value="${watch.documentTitle}"/></td>
					<td><c:out value="${watch.folderTitle}"/></td>
					<td><fmt:formatDate  value="${watch.documentCtime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
					<td><c:out value="${watch.user}"/></td>
				</tr>
			</c:when>
			<c:otherwise>
				<tr>
					<td><html-el:multibox title="${watch.documentTitle}" property="watchIds" value="${watch.id}"/></td>
					<td><a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.documentId}"/></a></td>
					<td><a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.documentTitle}"/></a></td>
					<td><a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.folderTitle}"/></a></td>
					<td><a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><fmt:formatDate  value="${watch.documentMtime}" type="both" pattern="dd-MM-yy HH:mm"/></a></td>
					<td><a href='<c:out value="${pageContext.request.contextPath}${watch.link}"/>'><c:out value="${watch.user}"/></a></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</c:forEach>
</table>
<c:if test="${!empty watches}">
	<!--	<ds:submit-event value="getText('UsunZObserwowanych')" name="'doRemove'"  cssClass="'btn'"/>--> 
	<input type="submit" name="doRemove" value="<ds:lang text="UsunZObserwowanych"/>"  class="btn"/><br/>
</c:if>

</html:form>
--%>
<!--N koniec favourites.jsp N-->