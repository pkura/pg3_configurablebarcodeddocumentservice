<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N ald-invoice-verification.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<!-- 
<a href="<ww:url value="'/repository/search-documents.action'"/>" ><ds:lang text="WyszukiwanieProste"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/search-dockind-documents.action'"/>"><ds:lang text="WyszukiwanieZaawansowane"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/ald-payment-verification.action'"/>"><ds:lang text="WeryfikacjaPlatnosciFaktur"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/ald-invoice-verification.action'"/>" class="highlightedText"><ds:lang text="WeryfikacjaRejestruVAT"/></a>
<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
<a href="<ww:url value="'/repository/impuls-leasing-customer.action'"/>"><ds:lang text="InformacjeOkliencie"/></a>
<ds:dockinds test="invoice">
	<img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	<a href="<ww:url value="'/repository/aegon-invoice-reports.action'"/>"><ds:lang text="RaportyFakturKosztowych"/></a>
</ds:dockinds>
-->

<ds:available test="!layout2">
<p>
<ds:xmlLink path="WyszukiwarkaDokumentow"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/repository/invoice-verification.action'"/>" method="post" enctype="multipart/form-data">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
    <table>
		<ww:if test="rejestrWybor">
		<tr>
            <td><ds:lang text="RodzajRejestru"/>:</td>
            <td>
				<select name="invoiceType" class="sel">
					<option value="1" <ww:if test="invoiceType==1">selected="selected"</ww:if>><ds:lang text="Kosztowy"/></option>
					<option value="2" <ww:if test="invoiceType==2">selected="selected"</ww:if>><ds:lang text="Zakupowy"/></option>
				</select>
			</td>
        </tr>
		</ww:if>
        <tr>
            <td><ds:lang text="PlikZRejestremVAT"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
            <td></td>
            <td>
            	<ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/>
				<ww:if test="rejestrWybor">
				<ds:event name="'default'" value="getText('Pokaz')" cssClass="'btn'"/>
				</ww:if>
			</td>
        </tr>
    </table>
    
    <ww:if test="invoiceBeans != null && invoiceBeans.size() > 0">
        <table class="search" width="100%" id="table">
            <tr>
                <th>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('lineNumber',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="NrLinii"/> <a href="<ww:url value="getSortLink('lineNumber',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>  
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('invoiceNumber',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="NrFaktury"/> <a href="<ww:url value="getSortLink('invoiceNumber',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('amountBrutto',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="KwotaBrutto"/> <a href="<ww:url value="getSortLink('amountBrutto',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>
                <th>
                    <nobr><a href="<ww:url value="getSortLink('status',false)"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a> <ds:lang text="Status"/> <a href="<ww:url value="getSortLink('status',true)"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></nobr>
                </th>                                
                <th></th>
                <th></th>
            </tr>
            <ww:iterator value="invoiceBeans">
                <tr style="<ww:if test="processStatus == 2">color:gray</ww:if><ww:elseif test="status != 1">color:red</ww:elseif>; ">
					
                    <td><ww:checkbox name="'documentIds'" fieldValue="id" id="check" value="'false'"/></td>
                    <td><ww:property value="lineNumber"/></td>             
                    <td><ww:property value="invoiceNumber"/></td>
                    <td><ww:property value="amount"/></td>
                    <td><ww:property value="statusDescription"/></td>
                    <td>
                        <ww:if test="documentId != null">
                            <a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>"><ds:lang text="dokument"/></a>
                        </ww:if>
                    </td>
                    <td>
                        <ww:if test="viewerLink != null">
                            <a href="javascript:openToolWindow('<ww:url value="viewerLink"/>&preventRefresh=true', 'vs', 1000, 750);"><img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Wy�wietl za��cznik"/></a>
                        </ww:if>
                    </td>
                </tr>
            </ww:iterator>
			<tr>
				<td>
					
				</td>
			</tr>
			
        </table>
		<input type="checkbox" onclick="selectAll(this)"/>
		<ds:lang text="ZaznaczWszystkie/cofnijZaznaczenie"/>
		<br/>
		<ds:submit-event name="'doDelete'" value="getText('UsunZaznaczone')" cssClass="'btn'"/>
		<ds:submit-event name="'doGetCSV'" value="getText('PobierzWszystkieJakoCSV')" cssClass="'btn'"/>
		<ds:submit-event name="'doChangeProcessStatus'" value="getText('OznaczJakoSprawdzone')" cssClass="'btn'"/>
        <!-- <input type="submit" value="Generuj raport z zaznaczonych" class="btn" onclick="generatePdf() "/>-->
    </ww:if>
    <ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script type="text/javascript">
function selectAll(checkbox)
{
	
    var boxes = document.getElementsByName('documentIds');
    if (boxes.length > 0)
    {
        for (var i=0; i < boxes.length; i++)
        {
            boxes.item(i).checked = checkbox.checked;
        }
    }
}
function generatePdf()
{
    var x = document.getElementsByName('documentIds');
    var i=0;
    var selected=false;
    var str = '';
    for(i=0;i<x.length;i++ )
    {
        if(x.item(i).checked)
        {
            str = str+'&documentIds='+x.item(i).value;
            selected=true;
        }
    }
    if(!selected)
        alert("Nie wybrano dokument�w");
    else
        openToolWindow(document.URL + '?streamPdf=true' + str)
}
    var prefix = "check";
    tableId="table";

    prepareCheckboxes();
</script>
