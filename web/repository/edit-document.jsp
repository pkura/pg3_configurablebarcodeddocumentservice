<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-document.jsp N-->

<%@ page import="org.apache.commons.beanutils.DynaBean,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>

<h1><ds:lang text="EdycjaDokumentu"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<p></p>

<script language="JavaScript">
function pickFolder(id, path)
{
    document.getElementById('folderId').value = id;
    document.getElementById('folderPath').value = path;
}

function promptLockExpiration()
{
    var exp = prompt('<ds:lang text="DoKiedyZablokowac"/> ?', document.getElementById('lockExpiration').value);
    if (exp == null || exp == "")
    {
        return false;
    }
    else
    {
        document.getElementById('lockExpiration').value = exp;
        return true;
    }
}

function promptEmail()
{
    var email = prompt('<ds:lang text="PodajAdresEmailOdbiorcy"/>');
    if (email == null || email == "")
    {
        return false;
    }
    else
    {
        document.getElementById('email').value = email;
        return true;
    }
}

function validateForm()
{
    var title = document.getElementById('title').value;
    if (title == null || title.length == 0) { alert('<ds:lang text="NiePodanoTytuluDokumentu"/>'); return false; }

    var description = document.getElementById('description').value;
    if (description == null || description.length == 0) { alert('<ds:lang text="NiePodanoOpisuDokumentu"/>'); return false; }
    if (description != null && description.length > 510) { alert('<ds:lang text="OpisDokumentuJestZbytDlugi.MaksymalnaLiczbaZnakowWOpisieTo510"/>'); return false; }

    if (document.getElementById('daa').value == "true") {
        var rodzaj_sieci = document.getElementById('__nw_RODZAJ_SIECI');
        if (rodzaj_sieci && (!(parseInt(rodzaj_sieci.value) > 0)))
        {
            alert('<ds:lang text="NieWybranoRodzajuSieci"/>');
            return false;
        }
        var typ_dokumentu = document.getElementById('__nw_TYP_DOKUMENTU');
        if(typ_dokumentu && (!(parseInt(typ_dokumentu.value) > 0))) {
            alert('<ds:lang text="NieWybranoTypuDokumentu"/>');
            return false;
        }
        //alert(document.getElementById('RODZAJ_RAPORTY').options.selectedIndex+" "+parseInt(rodzaj_sieci.value));
        if (!(document.getElementById('RODZAJ_RAPORTY').options.selectedIndex > 0)
        && (parseInt(rodzaj_sieci.value) == 1)) {
            //alert(document.getElementById('RODZAJ_AGENCJA').options.selectedIndex+" "+parseInt(document.getElementById('__nw_AGENCJA').value));
            if ((document.getElementById('RODZAJ_AGENCJA').options.selectedIndex > 0)
            && (!(parseInt(document.getElementById('__nw_AGENCJA').value) > 0))) {
                alert('<ds:lang text="NieWybranoBiura/agencji/banku"/>');
                return false;
            }
            //alert(document.getElementById('RODZAJ_AGENT').options.selectedIndex+" "+parseInt(document.getElementById('__nw_AGENT').value));
            if ((document.getElementById('RODZAJ_AGENT').options.selectedIndex > 0)
            && (!(parseInt(document.getElementById('__nw_AGENT').value) > 0))) {
                alert('<ds:lang text="NieWybranoSpecjalisty/OWCA"/>');
                return false;
            }
        }

        if ((document.getElementById('RODZAJ_RAPORTY').options.selectedIndex > 0)
        && (document.getElementById('__nw_KLASA_RAPORTU').options.selectedIndex == 0))
        {
            alert('Nie wybrano klasy raportu');
            return false;
        }

        <ww:if test="daaAssigned">
            if (!(parseInt(document.getElementById('__nw_AGENCJA').value) > 0) && !(parseInt(document.getElementById('__nw_AGENT').value) > 0)
            && (document.getElementById('RODZAJ_RAPORTY').options.selectedIndex == 0))
            {
                alert('<ds:lang text="DokumentBylJuzPrzypisany,ANieWybranoSpecjalisty/OWCAAniBiura/agencji/banku"/>');
                return false;
            }
        </ww:if>
    }

    return true;
}

function doctypeChanged(event)
{
    var src = event.srcElement ? event.srcElement : event.target;
    var id = src.value;
    if (id == null || id == '') return true;

    document.getElementById('doChangeDoctype').value = 'true';
    document.getElementById('form').submit();
}


/* wolane z doctype-fields - w edycji nei wybiera sie wzoru umowy */
/* wiec funkcja nic nie robi                                      */
function __nw_updateAttachmentPattern(typeId)
{
}

</script>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:extras test="business">
<ds:available test="addition.boxorder.available">
	<ds:available test="!layout2">
    <ww:if test="!hist">
        <a style="color:#813526"><ds:lang text="Podsumowanie"/></a>
        <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
        <a href="<ww:url value="'/repository/edit-doctype-document.action'"><ww:param name="'id'" value="id"/><ww:param name="'hist'" value="true"/></ww:url>"><ds:lang text="HistoriaZamowien"/></a>
    </ww:if>
    <ww:else>
    	<a href="<ww:url value="'/repository/edit-doctype-document.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="Podsumowanie"/></a>
        <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
        <a style="color:#813526"><ds:lang text="HistoriaZamowien"/></a>
    </ww:else>
    </ds:available>
    
    <ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
		<ww:if test="!hist">
	        <a class="highlightedText"><ds:lang text="Podsumowanie"/></a>
	        <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	        <a href="<ww:url value="'/repository/edit-doctype-document.action'"><ww:param name="'id'" value="id"/><ww:param name="'hist'" value="true"/></ww:url>" >
	        	<ds:lang text="HistoriaZamowien"/>
	        </a>
	    </ww:if>
	    <ww:else>
	    	<a href="<ww:url value="'/repository/edit-doctype-document.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="Podsumowanie"/></a>
	        <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/>
	        <a class="highlightedText"><ds:lang text="HistoriaZamowien"/></a>
	    </ww:else>		
	</div>
	</ds:available>
    
</ds:available>
</ds:extras>
<!--<ww:if test="hist">-->
        
<!-- </ww:if>-->
<form id="form" action="<ww:url value="'/repository/edit-doctype-document.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
<ww:if test="!hist">
	<ww:hidden name="'id'"/>
    <ww:hidden name="'email'" id="email"/>
    <ww:hidden name="'folderId'" id="folderId"/>
    <ww:hidden name="'queryLink'"/>
    <ww:hidden name="'lockExpiration'" id="lockExpiration"/>
    <ww:hidden name="'boxId'" id="boxId"/>
    
    <ww:hidden name="'daa'" id="daa" value="daa" />
    
    <ww:if test="!queryLink.empty">
        <p><b><a href='<ww:url value="queryLink"/>'>
            <ds:lang text="PowrotDoWyszukiwania"/>
        </a></b></p>
    </ww:if>
    
    <table class="formTable">
    <ww:if test="blocked">
    	<tr class="formTableDisabled">
    		<td colspan="2"><span style="color: red"><ds:lang text="TenDokumentZostaZaakceptowanyIJestZablokowanyPrzedEdycja"/>.</span></td>
    	</tr>
    </ww:if>
    <ww:if test="lockedThru != null">
        <ww:set name="lockedThru" value="lockedThru" scope="page"/>
        <tr class="formTableDisabled">
            <td><span style="color: red"><ds:lang text="ZablokowanyDo"/></span>:</td>
            <td><fmt:formatDate value="${lockedThru}" type="both" pattern="dd-MM-yy HH:mm"/>
                <ds:lang text="przez"/> <ww:property value="lockedBy"/></td>
        </tr>
    </ww:if>
    <tr class="formTableDisabled">
        <td><ds:lang text="Identyfikator"/>:</td>
        <td><ww:property value="id"/></td>
    </tr>
    <ww:if test="document.ctime != null"><ww:set name="ctime" value="document.ctime" scope="page"/></ww:if>
    <tr class="formTableDisabled">
        <td><ds:lang text="DataUtworzenia"/>:</td>
        <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
    </tr>
    <ww:if test="document.mtime != null"><ww:set name="mtime" value="document.mtime" scope="page"/></ww:if>
    <tr class="formTableDisabled">
        <td><ds:lang text="DataOstatniejModyfikacji"/>:</td>
        <td><fmt:formatDate value="${mtime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
    </tr>
    <tr class="formTableDisabled">
        <td><ds:lang text="Autor"/>:</td>
        <td><ww:property value="documentAuthor"/></td>
    </tr>
    <ww:if test="officeLink != null">
        <tr class="formTableDisabled">
            <td><ds:lang text="DokumentKancelaryjny"/>:</td>
            <td><a href="<ww:url value="officeLink"/>"><ds:lang text="przejdz"/></a></td>
        </tr>
    </ww:if>
    <tr>
        <td><ds:lang text="Tytul"/><span class="star">*</span>:</td>
        <td><ww:textfield name="'title'" id="title" size="50" maxlength="128" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td><ds:lang text="Opis"/><span class="star">*</span>:</td>
        <td>
            <ww:textarea name="'description'" id="description" cssClass="'txt'" rows="4" cols="50"/>
        </td>
    </tr>
    <tr>
        <td>
            <ww:if test="folderLink != null">
                <a href="<ww:url value="folderLink"/>"><ds:lang text="Folder"/></a>:
            </ww:if>
            <ww:else><ds:lang text="Folder"/>:</ww:else>
        </td>
        <td>
            <ww:textfield name="'folderPrettyPath'" cssClass="'txt_opt'" size="80" readonly="true" id="folderPath"/>
            <input type="button" value="<ds:lang text="WybierzFolder"/>" onclick="javascript:void(window.open('folders-tree-popup.jsp', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));" class="btn" id="chooseFolder">
        </td>
    </tr>
    
    <tr>
        <td><ds:lang text="PudloArchiwalne"/>:</td>
        <td><ww:textfield name="'boxNumber'" size="30" maxlength="30" cssClass="'txt'" id="boxNumber" readonly="boxNumberReadonly" />
            <input type="button" class="btn" value="<ds:lang text="NadajNumerOtwartegoPudla"/>"
            onclick="document.getElementById('boxNumber').value = '<ww:property value="currentBoxNumber"/>'; <ww:if test="currentBoxId != null">document.getElementById('boxId').value = <ww:property value="currentBoxId"/>; </ww:if>"
            <ww:if test="currentBoxId == null">disabled="true"</ww:if> />
            <input type="button" class="btn" value="<ds:lang text="AnulujWybor"/>"
            onclick="document.getElementById('boxNumber').value = ''; document.getElementById('boxId').value = null"
            <ww:if test="currentBoxId == null">disabled="true"</ww:if> />
            <ww:if test="canUpdate && blocked">
                <ds:submit-event cssClass="btn saveBtn" value="getText('Zapisz')" name="'doUpdateBox'"/>
            </ww:if>
        </td>
    </tr>
    <ww:if test="canChangeDoctype">
    <tr>
        <td>
            <ds:lang text="RodzajDokumentu"/>
        </td>
        <td>
            <ww:select name="'rodzaj_dokumentu'" list="rodzaje_dokumentow" listKey="cn" listValue="nazwa" value="rodzaj_dokumentu" cssClass="'sel'"/>
            <ds:submit-event value="getText('Zmien')" name="'doChangeDoctype'" cssClass="'btn'" confirm="NaPewnoZmienicRodzajDokumentu"/>
        </td>
    </tr>
    </ww:if>

    <ds:extras test="!nationwide">
        <!--TODO: zrobi� pola dla typu dokumentu-->
        <!--<tr><td colspan="2">Nie okre�lono widoku dla tego typu dokumentu</td></tr>-->
        <%--
        <tr>
            <td>Typ dokumentu:</td>
            <td><ww:select name="'doctypeId'" list="doctypes" listKey="key" listValue="value"
                headerKey="''" headerValue="'-- brak --'" cssClass="'sel'"
                onchange="'doctypeChanged(event);'" /></td>
        </tr>
        --%>
    </ds:extras>
    
    
    
    
    
    <tr class="formTableDisabled">
        <td></td>
        <td>
            <ds:submit-event value="getText('ZapiszZmiany')" name="'doUpdate'" cssClass="'btn'" validate="validateForm()" disabled="!canUpdate || blocked" />
            <ds:submit-event value="getText('Usun')" name="'doDelete'" cssClass="'btn'" disabled="!canDelete" />
            <ww:if test="canUndelete">
                <ds:submit-event value="getText('CofnijUsuniecie')" name="'doUndelete'" cssClass="'btn'"/>
            </ww:if>
    <%--        <input type="submit" value="Zapisz zmiany" class="btn"--%>
    <%--            onclick="if (!validateForm()) return false; document.getElementById('doUpdate').value='true';"/>--%>
    <%--        <input type="submit" value="Usu�" class="btn"--%>
    <%--            onclick="document.getElementById('doDelete').value='true';"/>--%>
            <input type="button" value="<ds:lang text="Powrot"/>" class="btn"
                onclick="document.location.href='<ww:url value="folderLink"/>';"/>
            <ww:if test="canSetPermissions">
                <ww:set name="permissionsLink" value="permissionsLink" scope="page"/>
                <edm-html:window-open-button windowHeight="300" windowWidth="400" href="${pageContext.request.contextPath}${permissionsLink}"  styleClass="btn"><ds:lang text="Uprawnienia"/></edm-html:window-open-button>
            </ww:if>
            <ww:else>
                <input type="button" value="<ds:lang text="Uprawnienia"/>" class="btn" disabled="true"/>
            </ww:else>
    
            <ww:if test="document.getDoctype() != null and !'daa'.equals(document.getDoctype().getCn())">
                <ww:if test="canLock">
                    <ww:set name="lockDocumentLink" value="lockDocumentLink" scope="page"/>
                    <edm-html:window-open-button windowHeight="250" windowWidth="400" href="${pageContext.request.contextPath}${lockDocumentLink}" styleClass="btn"><ds:lang text="ZablokujDokument"/></edm-html:window-open-button>
                </ww:if>
                <ww:else>
                    <input type="button" value="<ds:lang text="Zablokuj"/>" class="btn" disabled="true"/>
                </ww:else>
    
    
                <ww:if test="canUnlock">
                    <ds:submit-event value="getText('Odblokuj')" name="'doUnlock'" cssClass="'btn'"/>
        <%--            <input type="submit" value="Odblokuj" class="btn"--%>
        <%--                onclick="document.getElementById('doUnlock').value='true';"/>--%>
                </ww:if>
                <ww:else>
                    <input type="button" value="<ds:lang text="Odblokuj"/>" class="btn" disabled="true"/>
                </ww:else>
            </ww:if>
            <ds:submit-event value="getText('DoObserwowanych')" name="'doAddToFavourites'" cssClass="'btn'"/>
    <%--        <input type="submit" value="Do obserwowanych" class="btn"--%>
    <%--            onclick="document.getElementById('doAddToFavourites').value='true';"/>--%>
    
            <ds:submit-event value="getText('DoUlubionych')" name="'doAddToBookmarks'" cssClass="'btn'"/>
    
            <ww:set name="sendLinkLink" value="sendLinkLink" scope="page"/>
            <edm-html:window-open-button windowHeight="130" windowWidth="450" href="${pageContext.request.contextPath}${sendLinkLink}"  styleClass="btn"><ds:lang text="WyslijOdnosnik"/></edm-html:window-open-button>
            <%--<html:submit property="doSendLink" value="Wy�lij odno�nik" styleClass="btn" onclick="return promptEmail();" />--%>
    

            <!--
            <ds:extras test="!nationwide">    
            	<ds:available test="klonowanie.podsumowanie">               	    	
                	<ds:event name="'doClone'" value="'Klonuj'"/>
				 </ds:available>              
            </ds:extras>
            -->
            <ds:extras test="!nationwide"> 
						<ds:available test="klonowanie.podsumowanie">
							<input type="button" class="btn" value="<ds:lang text="Klonuj"/>"onclick="document.location.href='<ww:url value="'/repository/clone-dockind-document.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>';" />
						</ds:available>
			</ds:extras>
            <ds:extras test="business">
            </ds:extras>
        </td>
    </tr>
    </table>
    
    <ds:modules test="certificate">
        <ww:if test="!signatureBeans.empty">
            <h4><ds:lang text="AkceptacjeDokumentu"/></h4>
    
    		<table>
    		    <tr>
    		        <th><ds:lang text="Autor"/></th>
    		        <th><ds:lang text="Data"/></th>
    		        <th colspan="2" style="text-align:center"><ds:lang text="Pobierz"/>:</th>
    		        <th></th>
    		        <th></th>
    		    </tr>
    
    		    <ww:iterator value="signatureBeans">
    		    	<tr>
    			    	<td>&nbsp;<ww:property value="author.asFirstnameLastname()"/></td>
    			    	<td>&nbsp;<ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
    		            <td><a href="<ww:url value="'/office/common/document-signature.action?getDoc=true&signatureId='+id"/>"><ds:lang text="dokument"/></a></td>
    		            <td><a href="<ww:url value="'/office/common/document-signature.action?getSig=true&signatureId='+id"/>"><ds:lang text="podpis"/></a></td>
    		            <td><a href="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doVerify=true&signatureId='+id+'&id='+document.id"/>', 'verify', 500, 400);"><ds:lang text="Weryfikuj"/></a></td>
    		            <td><ww:if test="correctlyVerified != null && !correctlyVerified"><span style="color: red"><ds:lang text="bladWPodpisie"/></span></ww:if></td>
    	            </tr>
    		    </ww:iterator>
    		</table>
    	</ww:if>
    
        <ww:if test="useSignature">
        	<p><input type="button" value="<ds:lang text="PodpiszDokument"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?doSign=true&id='+document.id"/>', 'sign', 400, 300);" <ww:if test="!canSign">disabled="disabled"</ww:if>/>
        	<input type="button" value="<ds:lang text="PostaDokumentuDoPodpisu"/>" class="btn" onclick="javascript:openToolWindow('<ww:url value="'/office/common/document-signature.action?showDoc=true&id='+document.id"/>', 'sign', 800, 600);"/></p>
        </ww:if>
    </ds:modules>
    
    
    <h3><ds:lang text="Zalaczniki"/></h3>
    
    <input type="button" value="<ds:lang text="NowyZalacznik"/>" onclick="document.location.href='<ww:url value="addAttachmentLink"/>';"
        class="btn" <ww:if test="!canModifyAttachments">disabled="true"</ww:if> />
    
    <ww:if test="!attachments.empty">
        <table>
        <tr>
            <th></th>
            <th></th>
            <th><edm-html:message key="attachment.title"/></th>
            <th><edm-html:message key="attachment.newestRevision"/></th>
            <th><ds:lang text="Autor"/></th>
            <th><ds:lang text="Rozmiar"/></th>
            <th><ds:lang text="Data"/></th>
            <th><ds:lang text="Wersja"/></th>
        </tr>
    
        <ww:iterator value="attachments">
            <tr>
                <ww:if test="'mail'.equals(cn)">
                    <td><ww:checkbox name="'ids'" fieldValue="id" /></td>
                    <td></td>
                    <td><a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="wparam"/></ww:url>"><ww:property value="title"/></a></td>
                    <td></td>
                    <td><ww:property value="author"/></td>
                    <td></td>
                    <td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
                    <td></td>
                </ww:if>
                <ww:elseif test="noContent">
                    <td><ww:checkbox name="'ids'" fieldValue="id" /></td>
                    <td></td>
                    <td><a href="<ww:url value="editLink"/>"><ww:property value="title"/></a></td>
                    <td colspan="5"><i>(<ds:lang text="BrakTresci"/>)</i></td>
                </ww:elseif>
                <ww:else>
                    <td><ww:checkbox name="'ids'" fieldValue="id" /></td>
                    <td><img src="<ww:url value="icon"/>"/></td>
                    <td><a href="<ww:url value="editLink"/>"><ww:property value="title"/></a></td>
                    <td>
                        <ww:if test="canReadAttachments">
                            <a name="Pobierz" href="<ww:url value="recentContentLink"/>">
    						<img src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" id="Pobierz "title="Pobierz" name="Pobierz" ></a>
    
                            <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(mime)">
                                <a href="javascript:openToolWindow('<ww:url value="showRecentContentLink"><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs', 1000, 750);">&nbsp;&nbsp;<img src="<ww:url value="'/img/wyswietl.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Wy�wietl"></a>
                            </ww:if>
                        </ww:if>
    
                        <ds:modules test="certificate">
                            <ww:if test="useSignature && canSign">
                                <a href="javascript:openToolWindow('<ww:url value="'/office/common/attachment-signature.action?id='+revisionId"/>', 'sign', 400, 300);"><img src="<ww:url value="'/img/podpisz.gif'"/>" width="18" height="18" class="zoomImg" title="PodpiszZa�acznik"/></a>
                            </ww:if>
                        </ds:modules>
    
                    </td>
                    <td><ww:property value="author"/></td>
                    <td align="right"><ds:format-size value="#this['size']"/></td>
                    <td><ds:format-date value="ctime" pattern="dd-MM-yy HH:mm"/></td>
                    <td align="right"><ww:property value="revision"/></td>
                </ww:else>
            </tr>
        </ww:iterator>
    
        </table>
    
    	<span class="btnContainer">
	        <ds:submit-event value="getText('UsunZaznaczoneZalaczniki')" name="'doDeleteAttachments'"
	            cssClass="'btn'" disabled="!canModifyAttachments || blocked"/>
	    
	        <ds:submit-event value="getText('TrwaleUsunZaznaczoneZalaczniki')" name="'doDeleteAttachmentsPermanently'"
	            cssClass="'btn'" disabled="!canModifyAttachments || blocked"/>
            
        </span>
    </ww:if>
</ww:if>
<ww:elseif test="hist">
    <table width="100%">
<ww:iterator value="orderHistList">
    <tr>
        <td><ww:property value="KTO"/></td>
        <td align="right"><ww:property value="DATA_ZMIANY"/></td>
    </tr>
    <tr>
        <td colspan="2" style="border: 1px solid black;">
            <ww:property value="TXT"/>
        </td>
    </tr>
</ww:iterator>
</table>
</ww:elseif>

<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>

</form>
<script type="text/javascript">
    function checkFolderSize()
    {
         /* zwiekszam wielkosc pola folder jesli konieczne */
        var element = document.getElementById('folderPath');
        if ((element.value != null) && (element.value != undefined) && (parseInt(element.value.length) > parseInt(element.getAttribute('size'))))
        {
            element.setAttribute('size', element.value.length);
        }
    }

    checkFolderSize();
    function disp_confirm()
    {
        var r=confirm("<ds:lang text="CzyNaPewnoChceszZamowic"/>");
        if (r==true)
        {
            document.getElementById('doZamow').value='true';
            document.forms[0].submit();
        }
        else
        {
            document.forms[0].reset();
        }
    }
</script>

