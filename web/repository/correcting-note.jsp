<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N correcting-note.jsp N-->

<%@ page import="org.apache.commons.beanutils.DynaBean,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt" %>

<h1>Dodawania noty koryguj�cej</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<form id="form" action="<ww:url value="'/repository/correcting-note.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
<table>
	<ww:hidden name="'documentId'"/>
	<tr>
		<td>
			NIP dostawcy:
		</td>
		<td>
			<ww:property value="nipDostawcy"/>
		</td>
	</tr>
	<tr>
		<td>
			Numer faktury:
		</td>
		<td>
			<ww:property value="invoiceNo"/>
		</td>
	</tr>
	<tr>
		<td>
			Nazwa klienta:
		</td>
		<td>
			<ww:property value="nazwaKlienta"/>
		</td>
	</tr>
	<tr>
		<td>
			Numer VIN:
		</td>
		<td>
			<ww:property value="vinNo"/>
		</td>
	</tr>
	<tr>
		<td>
			Tre�� korygowana:
		</td>
		<td>
			<ww:textarea name="'trescKorygowana'" id="trescKorygowana" cssClass="'txt'" rows="4" cols="50"/>
		</td>
	</tr>
	<tr>
		<td>
			Tre�� poprawna:
		</td>
		<td>
			<ww:textarea name="'trescPoprawna'" id="trescPoprawna" cssClass="'txt'" rows="4" cols="50"/>
		</td>
	</tr>
	<tr>
		<td>
			Data wystawienia:
		</td>
		<td>
			<ww:textfield name="'dataWystawienia'" id="dataWystawienia" size="10" maxlength="10" cssClass="'txt'"/>
			 <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="dataWystawieniaTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
		</td>
	</tr>
	<tr>
		<td>
			Data sprzeda�y:
		</td>
		<td>
			<ww:textfield name="'dataSprzedazy'" id="dataSprzedazy" size="10" maxlength="10" cssClass="'txt'"/>
			 <img src="<ww:url value="'/calendar096/img.gif'"/>"
            id="dataSprzedazyTrigger" style="cursor: pointer; border: 1px solid red;"
            title="Date selector" onmouseover="this.style.background='red';"
            onmouseout="this.style.background=''"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:submit-event value="getText('Utworz')" name="'doAddCorrectingNote'"
            cssClass="'btn'"/>
		</td>
		<td>
			
		</td>
	</tr>
	
	




</table>
</form>
<script type="text/javascript">
	 Calendar.setup({
        inputField     :    "dataWystawienia",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "dataWystawieniaTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
    Calendar.setup({
        inputField     :    "dataSprzedazy",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "dataSprzedazyTrigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>