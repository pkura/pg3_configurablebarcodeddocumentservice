<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N lock-document.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<script>
    // window.opener.location.href = '<c:out value="${pageContext.request.contextPath}/${documentLink}"/>';
<c:if test="${close}">
    window.opener.location.href = '<c:out value="${pageContext.request.contextPath}${documentLink}"/>';
    window.close();
</c:if>

<ds:available test="jQueryCalendar">
/* Obejscie starego kalendarza na jquery */


var $j = jQuery.noConflict();

Calendar = function() {
	
}

Calendar.setup = function(params) {
	/*$j('#' + params['button']).click(function() {
		alert('klik');
	});*/
	$j(function() {
	//jQuery.fx.off = true;

	$j('#' + params['inputField']).datepicker({showOn: 'button', buttonImage: '<ww:url value="'/calendar096/img.gif'"/>', 
		buttonImageOnly: true, dateFormat: 'dd-mm-yy', duration: 0,
		changeMonth: true, changeYear: true});
	});
	$j('#' + params['button']).hide();
}
</ds:available>

</script>

<edm-html:errors />

<html:form action="/repository/lock-document">
    <html:hidden property="documentId"/>

    <table width="100%" height="100%">    
    <tr>
        <td width="100%" valign="bottom">
            <b><ds:lang text="BlokowanieDokumentu"/></b>
            <p><ds:lang text="DataIczasWygasnieciaBlokady"/>:
                <html:text property="date" size="10" maxlength="10" styleClass="txt" styleId="date"/>
                <img src="<c:out value='${pageContext.request.contextPath}'/>/calendar096/img.gif"
                    id="calendar_date_trigger" style="cursor: pointer; border: 1px solid red;"
                    title="Date selector" onmouseover="this.style.background='red';"
                    onmouseout="this.style.background=''"/>

                <html:text property="date" size="5" maxlength="5" styleClass="txt" styleId="time"/>
               <br />
               <input type="submit" name="doLock" value="<ds:lang text="Zablokuj"/>" class="btn"/>
            </p>
        </td>
    </tr>
    </table>
</html:form>

    <script type="text/javascript">
        Calendar.setup({
            inputField     :    "date",     // id of the input field
            ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
            button         :    "calendar_date_trigger",  // trigger for the calendar (button ID)
            align          :    "Tl",           // alignment (defaults to "Bl")
            singleClick    :    true
        });
    </script>