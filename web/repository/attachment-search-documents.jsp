<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page pageEncoding="ISO-8859-2" %>
<%@ page language="java" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzukajDokumentow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ds:available test="!layout2">
<p>
<ds:xmlLink path="Wyszukiwaniewzalacznikach"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="WyszukiwarkaDokumentow"/>
	</div>
</ds:available>


<form name="formul" action="<ww:url value="'/repository/attachment-search-documents.action'"/>" method="post">
		<ww:if test="results == null or results.empty">
		<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
		<table id="bt" class="formTable">
			<tr>
				<td>
					<ds:lang text="SzukanyTekst"/>:
				</td>
				<td>
					<td>
					<ww:textfield name="'klucz'" size="10" maxlength="10" cssClass="'txt'"/>
				</td>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="LiczbaWynikowNaStronie"/>:
				</td>
				<td>
					<ww:select name="'limit'" cssClass="'sel'" list="#{ 10: 10, 25: 25, 50: 50 }"
						value="limit > 0 ? limit : 50"/>
				</td>
			</tr>
		</table>

		<input type="submit" name="doSearch" value="<ds:lang text="Znajdz"/>" class="btn searchBtn"/>
		<input type="text" name="none" style="display: none"/>
		<br />
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end -->
		</ds:available>
	</ww:if>
	</form>
		
			<ww:if test="!results.empty">
	<ds:available test="layout2">
		<div id="middleContainer"> <!-- BIG TABLE start -->
	</ds:available>
		<table class="search" width="100%" cellspacing="0" id="mainTable">
		<tr>
			<ww:iterator value="columns" status="status" >
				<th>
					<nobr>
						<ww:if test="sortDesc != null">
							<%--	<a href="<ww:url value="sortDesc"/>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortDesc"/>" alt="<ds:lang text="SortowanieMalejace"/>" title="<ds:lang text="SortowanieMalejace"/>">
								<ww:if test="property == ('document_' + sortField) && ascending == false"><img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
						<ww:property value="title"/>
						<ww:if test="sortAsc != null">
							<%--	<a href="<ww:url value="sortAsc"/>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>	--%>
							<a href="<ww:url value="sortAsc"/>" alt="<ds:lang text="SortowanieRosnace"/>" title="<ds:lang text="SortowanieRosnace"/>">
								<ww:if test="(sortField == null && property == 'document_title') || (property == ('document_' + sortField) && ascending == true)"><img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"</ww:if>
								<ww:else><img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"</ww:else> width="11" height="11" border="0"/></a>
						</ww:if>
					</nobr>
				</th>
				<ww:if test="!#status.last">
					<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
				</ww:if>
			</ww:iterator>
		</tr>
		<ww:iterator value="results">
			<ww:set name="result"/>
			<tr>
				<ww:iterator value="columns" status="status">
					
						<td>
							<a href="<ww:url value="#result['link']"/>"><ww:property value="prettyPrint(#result[property])"/></a>
						</td>
					
					<ww:if test="!#status.last">
						<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					</ww:if> 
				</ww:iterator>
			</tr>
		</ww:iterator>
		<tr>
			<td>
				<input type="button" class="btn" value="<ds:lang text="Powrot"/>" onclick="history.back();"/>
			</td>
		</tr>
	</table>
	<ds:available test="layout2">
		</div> <!-- BIG TABLE end -->
	</ds:available>
	
	<ww:set name="pager" scope="request" value="pager"/>
<table width="100%">
	<tr>
		<td align="center">
			<jsp:include page="/pager-links-include.jsp"/>
		</td>
	</tr>
</table>
	</ww:if>	 
