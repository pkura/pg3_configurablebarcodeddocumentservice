<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-folder.jsp N-->

<%@ page import="org.apache.struts.action.DynaActionForm"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><ds:lang text="newFolder.h1"/></h3>

<edm-html:errors />

<html:form action="/repository/new-folder" onsubmit="disableFormSubmits(this);" >
<html:hidden property="folderId" styleId="folderId" />
<input type="hidden" name="doCreate" id="doCreate"/>

<p><b><c:out value="${folderPrettyPath}"/></b></p>

<table>
<tr>
    <td><ds:lang text="Tytul"/><span class="star">*</span>:</td>
    <td><html:text property="title" size="30" maxlength="128" styleClass="txt" /></td>
</tr>
<tr>
    <td></td>
    <td>
        <input type="submit" value="<ds:lang text="Utworz"/>" class="btn"
            onclick="document.getElementById('doCreate').value='true';"/>
        <input type="button" value="<ds:lang text="Rezygnuj"/>" class="btn"
            onclick="document.location.href='<c:out value="${pageContext.request.contextPath}${returnLink}"/>';"/>
    </td>
</tr>
</table>

</html:form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>
