<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N folders.jsp N-->

<%@ page import="java.util.HashMap,
                 java.util.Map,
                 pl.compan.docusafe.core.base.Folder,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><edm-html:message key="folders.h1"/></h3>

<edm-html:errors />

<html:form action="/repository/folders">
<html:hidden property="id"/>

<%
    Map params = new HashMap();
%>

<c:forEach items="${path}" var="folder" varStatus="status">
    <html:link forward="repository/folders" paramId="id" paramName="folder" paramProperty="id">
        <c:out value="${folder.title}"/>
    </html:link>
    <c:if test="${!status.last}"> / </c:if>
    <c:if test="${status.last}">
        <%
            params.put("id", ((Folder) pageContext.findAttribute("folder")).getId());
            params.put("parentId", ((Folder) pageContext.findAttribute("root")).getId());
            pageContext.setAttribute("params", params);
        %>
        <small>(<html:link forward="repository/edit-folder" name="params">
            <edm-html:message key="folders.edit"/>
        </html:link>)</small>
        <small>(<a href="javascript:void(window.open('<c:out value='${pageContext.request.contextPath}'/>/security/set-permissions.do?objectType=folder&objectId=<c:out value='${folder.id}'/>', '_blank', 'width=400,height=260,menubar=no,toolbar=no,location=no,status=no'));"><ds:lang text="Uprawnienia"/></a>)</small>
    </c:if>
</c:forEach>

<p style="margin-left: 10px;">
    <c:forEach items="${folderBeans}" var="bean">
        <html:multibox property="folderIds" value='<%= ((DynaBean) pageContext.findAttribute("bean")).get("id").toString() %>'/>
        <a href='<bean:write name="bean" property="enterLink"/>'><bean:write name="bean" property="title"/></a>
        <small>(<a href='<bean:write name="bean" property="editLink"/>'><edm-html:message key="folders.edit"/></a>)</small>
        <small>(<a href="javascript:void(window.open('<c:out value='${pageContext.request.contextPath}'/>/security/set-permissions.do?objectType=folder&objectId=<c:out value='${bean.id}'/>', '_blank', 'width=400,height=260,menubar=no,toolbar=no,location=no,status=no'));"><ds:lang text="Uprawnienia"/></a>)</small>
        <br/>
    </c:forEach>

<%--
    <c:forEach items="${root.properChildren}" var="folder" varStatus="status">
        <html:multibox property="folderIds" value='<%= ((Folder) pageContext.findAttribute("folder")).getId() %>'/>
        <input type="checkbox"/>
        <%
            params.put("id", ((Folder) pageContext.findAttribute("folder")).getId());
            params.put("parentId", ((Folder) pageContext.findAttribute("root")).getId());
            pageContext.setAttribute("params", params);
        %>
        <html:link forward="repository/folders" paramId="id" paramName="folder" paramProperty="id">
            <c:out value="${folder.title}"/>
        </html:link>

        <small>(<html:link forward="repository/edit-folder" name="params">
            <edm-html:message key="folders.edit"/>
        </html:link>)</small> <br/>
    </c:forEach>
--%>
</p>

<p><html:submit property="doDelete" styleClass="btn" ><edm-html:message key="folders.doDelete"/></html:submit></p>

<html:text property="title" size="30" maxlength="128" styleClass="txt" />
<html:submit property="doAdd" styleClass="btn"><edm-html:message key="folders.doAdd"/></html:submit>

</html:form>

