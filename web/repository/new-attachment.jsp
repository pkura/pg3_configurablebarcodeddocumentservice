<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-attachment.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://struts.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><ds:lang text="newAttachment.h1"/></h3>

<edm-html:errors />

<html:form action="/repository/new-attachment" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">
<html:hidden property="documentId" />
<input type="hidden" name="doCreate" id="doCreate"/>

<table>
<tr>
    <td><ds:lang text="attachment.title"/><span class="star">*</span>:</td>
    <td><html:text property="title" size="30" maxlength="128" styleClass="txt" /></td>
</tr>
<tr>
    <td><ds:lang text="attachment.description"/><span class="star">*</span>:</td>
    <td><html:text property="description" size="30" maxlength="128" styleClass="txt" /></td>
</tr>
<tr>
    <td><ds:lang text="attachment.file"/><span class="star">*</span>:</td>
    <td><html:file property="file" styleClass="txt" /></td>
</tr>
<ds:available test="file.encryption">
<tr>
	<td><ds:lang text="attachment.encryption"/>:</td>
	<td><ww:checkbox id="encryption" name="'encryption'" fieldValue="true" onchange="'isEncryption()'"/></td>
</tr>
<tr>	
	<td><ds:lang text="attachment.publickey"/>:</td>
	<td><ww:textfield name="'publicKey'" id="publicKey" size="45" maxlength="254" cssClass="'txt dontFixWidth'" disabled="true"/></td>
</tr>
</ds:available>
<tr>
    <td></td>
    <td>
        <input type="submit" value="<ds:lang text="Utworz"/>" class="btn"
            onclick="document.getElementById('doCreate').value='true';"/>
        <input type="button" value="<ds:lang text="Rezygnuj"/>" class="btn"
            onclick="document.location.href='<c:out value="${pageContext.request.contextPath}${documentLink}"/>';"/>
</tr>
</table>

<%--<p><input type="button" value='<edm-html:message key="attachment.backToDocument"/>' onclick="document.location.href = '<c:out value='${pageContext.request.contextPath}${documentLink}'/>';" class="btn"></p>--%>

</html:form>

<p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p>

<script type="text/javascript">
function isEncryption() {
	if(document.getElementById("encryption").checked)
	{
		document.getElementById("publicKey").disabled = false;
	}
	else {
		document.getElementById("publicKey").disabled = true;
	}
}
</script>
