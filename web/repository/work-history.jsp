<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="SzczegulyDockumentu"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:available test="!layout2">
<p>
<ds:xmlLink path="editDockind"/>
</p>
</ds:available>

<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="editDockind"/>
</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:available test="layout2">
        <div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<p>

<ww:if test="showAll">
    <a href="<ww:url value="getBaseLink()"><ww:param name="'documentId'" value="documentId"/>
    <ww:param name="'activity'" value="activity"/><ww:param name="'showAll'" value="false"/></ww:url>"><ds:lang text="PokazSkroconaHistoriePisma"/></a>
</ww:if>
<ww:else>
    <a href="<ww:url value="getBaseLink()"><ww:param name="'documentId'" value="documentId"/>
    <ww:param name="'activity'" value="activity"/><ww:param name="'showAll'" value="true"/></ww:url>"><ds:lang text="PokazPelnaHistoriePisma"/></a>
</ww:else>

<table width="100%">
<ww:iterator value="workHistory">
    <ww:set name="date" scope="page" value="date"/>
    <tr>
        <td class="historyAuthor"><ww:property value="author"/></td>
        <td align="right" class="historyDate"><fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/></td>
    </tr>
    <tr>
        <td colspan="2" class="historyContent">
            <ww:property value="content"/>
        </td>
    </tr>
</ww:iterator>
</table>

 <ds:available test="layout2">
        <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
        <div class="bigTableBottomSpacer">&nbsp;</div>
        </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
