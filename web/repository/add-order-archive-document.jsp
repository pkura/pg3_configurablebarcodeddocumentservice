<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="PrzyjmijPismo"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/repository/add-order-archive-document.action'"/>" method="post" enctype="multipart/form-data">
<ww:hidden name="'docId'" id="docId"/>
<ww:hidden name="'orderId'" id="orderId"/>
<ww:if test="docId == null">    
    <table>
        <tr>
            <td><ds:lang text="Skan"/>:</td>
            <td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
        </tr>
        <tr>
            <td></td>
            <td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
        </tr>
        <tr>
            <td></td>
            <td><ds:lang text="ImportowanyPlikMusiBycWformacie"/> : tiff</td>
        </tr>
    </table>
</ww:if>
<ww:else>
<table border="0" cellspacing="0" cellpadding="0">
<ww:hidden name="'doArchive'" id="doArchive"/>
	<tr>
		<td>
			<jsp:include page="/common/dockind-fields.jsp"/>
			<jsp:include page="/common/dockind-specific-additions.jsp"/>
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="doArchive" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
	        	onclick="if (!validateForm()) return false;document.getElementById('doArchive').value = 'true';"/>
		</td>
	</tr>
	<script type="text/javascript">
		function validateForm()
		{
			if (!validateDockind())
				return false;
			return true;
		}
	</script>
</table>
</ww:else>
</form>