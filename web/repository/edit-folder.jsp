<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-folder.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />

<h3><edm-html:message key="editFolder.h1"/></h3>

<edm-html:errors />

<html:form action="/repository/edit-folder">
<html:hidden property="id"/>
<html:hidden property="parentId"/>

<table>
<tr>
    <td><edm-html:message key="DataUtworzenia"/>:</td>
    <td><fmt:formatDate value="${folder.ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/></td>
</tr>
<tr>
    <td><edm-html:message key="folder.title"/>:</td>
    <td><html:text property="title" size="30" maxlength="128" styleClass="txt" /></td>
</tr>
<tr>
    <td></td>
    <td>
        <html:submit property="doUpdate" styleClass="btn"><edm-html:message key="doUpdate" global="true" /></html:submit>
        <html:cancel styleClass="cancel_btn"><edm-html:message key="doCancel" global="true" /></html:cancel>
    </td>
</tr>
</table>

</html:form>

