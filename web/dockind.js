
function accept_contract(map)
{
	setKlient(map);
	var rodzajObj = document.getElementById('dockind_RODZAJ_KONTRAHENTA');
	if(rodzajObj == null)
	{
		setDostawca(map);
		document.getElementById('dockind_NUMER_WNIOSKU').value = map.idWniosku;
		document.getElementById('dockind_NUMER_WNIOSKUnumerWniosku').value = map.numerWniosku;
	}
	else
	{
		var sel = document.getElementById('dockind_NUMER_UMOWY');
		for (var i=0; i < sel.options.length; i++)
		{
			if (sel.options[i].value == map.id)
			{
				sel.options[i] = null;
				i--;
			}
		}
	}
}

function accept_application(map)
{
	setKlient(map);
	var rodzajObj = document.getElementById('dockind_RODZAJ_KONTRAHENTA');
	if(rodzajObj == null)
	{
		setDostawca(map);
		document.getElementById('dockind_NUMER_UMOWY').value = map.idUmowy;
		document.getElementById('dockind_NUMER_WNIOSKUnumerWniosku').value = map.numerUmowy;
	}
	else
	{
		try
		{
			var sel = document.getElementById('dockind_NUMER_WNIOSKU');
			for (var i=0; i < sel.options.length; i++)
			{
				if (sel.options[i].value == map.id)
				{
					sel.options[i] = null;
					i--;
				}
			}
		}
		catch(e){}
	}
}

function setKlient(map)
{
	var rodzajObj = document.getElementById('dockind_RODZAJ_KONTRAHENTA');
	if (document.getElementById('dockind_KLIENT').value == '' || document.getElementById('dockind_KLIENT').value ==null )
	{
		if(rodzajObj != null && rodzajObj.selectedIndex == 2)
		{
			document.getElementById('dockind_KLIENTname').value = map.nazwaDostawcy;
			document.getElementById('dockind_KLIENT').value = map.idDostawcy;
		}
		else
		{
			document.getElementById('dockind_KLIENTname').value = map.nazwaKlienta;
			document.getElementById('dockind_KLIENT').value = map.idKlienta;
		}
	}
	else if(rodzajObj != null && rodzajObj.selectedIndex == 2 && document.getElementById('dockind_KLIENT').value != map.idDostawcy)
	{
			alert('Wybrano instancje zwi�zan� z innym dostawc�.');
			document.getElementById('dockind_KLIENTname').value = map.nazwaDostawcy;
			document.getElementById('dockind_KLIENT').value = map.idDostawcy;
			clearUmowaWniosek();
	}
	else if(rodzajObj != null && rodzajObj.selectedIndex == 1 && document.getElementById('dockind_KLIENT').value != map.idKlienta)
	{
		alert('Wybrano instancje zwi�zan� z innym klientem.');
		document.getElementById('dockind_KLIENTname').value = map.nazwaKlienta;
		document.getElementById('dockind_KLIENT').value = map.idKlienta;
		clearUmowaWniosek();
	}
}

function setDostawca(map)
{
	if (document.getElementById('dockind_DOSTAWCA') != null && document.getElementById('dockind_DOSTAWCA').value == '' || document.getElementById('dockind_DOSTAWCA').value == null )
	{
		document.getElementById('dockind_DOSTAWCAname').value = map.nazwaDostawcy;		
		document.getElementById('dockind_DOSTAWCA').value = map.idDostawcy;
	}
	else if (document.getElementById('dockind_DOSTAWCA') != null && document.getElementById('dockind_DOSTAWCA').value != map.idDostawcy)
	{
		alert('Wybrano instancje zwi�zan� z innym dostawc�.');
		document.getElementById('dockind_DOSTAWCAname').value = map.nazwaDostawcy;		
		document.getElementById('dockind_DOSTAWCA').value = map.idDostawcy;
		clearUmowaWniosek();
	}
}

function clearUmowaWniosek()
{
	try
	{
		if(typeof E('dockind_NUMER_UMOWY') == 'select')
		{
			selectAllOptions(document.getElementById('dockind_NUMER_UMOWY'));
			deleteSelectedOptions(document.getElementById('dockind_NUMER_UMOWY'));
			selectAllOptions(document.getElementById('dockind_NUMER_WNIOSKU'));
			deleteSelectedOptions(document.getElementById('dockind_NUMER_WNIOSKU'));
		}
		else
		{
			document.getElementById('dockind_NUMER_WNIOSKU').value = '';			
			document.getElementById('dockind_NUMER_WNIOSKUnumerWniosku').value = '';
			document.getElementById('dockind_NUMER_UMOWY').value = '';			
			document.getElementById('dockind_NUMER_UMOWYnumerUmowy').value = '';
		}
	}
	catch(e)
	{
		document.getElementById('dockind_NUMER_WNIOSKU').value = '';			
		document.getElementById('dockind_NUMER_WNIOSKUnumerWniosku').value = '';
		document.getElementById('dockind_NUMER_UMOWY').value = '';			
		document.getElementById('dockind_NUMER_UMOWYnumerUmowy').value = '';
	}
}

function accept_contractor(map)
{
	try
	{
		if (document.getElementById('dockind_KLIENT') != null)
		{
			if (document.getElementById('dockind_KLIENT').value != map.id)
			{
				document.getElementById('dockind_KLIENTname').value = map.nazwa;
				selectAllOptions(document.getElementById('dockind_NUMER_UMOWY'));
				deleteSelectedOptions(document.getElementById('dockind_NUMER_UMOWY'));
				selectAllOptions(document.getElementById('dockind_NUMER_WNIOSKU'));
				deleteSelectedOptions(document.getElementById('dockind_NUMER_WNIOSKU'));		
			}
		}
		else if (document.getElementById('dockind_NUMER_KONTRAHENTA') != null && document.getElementById('dockind_NUMER_PRACOWNIKA') != null )
		{
			if(document.getElementById('dockind_NUMER_KONTRAHENTA').value != map.id )
			{
				document.getElementById('dockind_NUMER_PRACOWNIKA').value = '';
				document.getElementById('dockind_NUMER_PRACOWNIKAfirstname').value = '';
				document.getElementById('dockind_NUMER_PRACOWNIKAlastname').value = '';
				document.getElementById('dockind_NUMER_PRACOWNIKAphoneNumber').value = '';
			}
		}
	}
	catch(e){}
}	

function accept_contact(map)
{
//	var sel = document.getElementById('dockind_KONTAKT');
	//for (var i=0; i < sel.options.length; i++)
	{
	//	if (sel.options[i].value == map.id)
		{
	//		sel.options[i] = null;
	//		i--;
		}
	}	
}

function accept_contractor_w(map)
{
	if (document.getElementById('dockind_NUMER_KONTRAHENTA').value == '' ||
	 document.getElementById('dockind_NUMER_KONTRAHENTA').value ==null )
	{
		document.getElementById('dockind_NUMER_KONTRAHENTAname').value = map.nazwaKontrahenta;
		//document.getElementById('dockind_NUMER_KONTRAHENTAphoneNumber').value = map.phoneNumber;
		document.getElementById('dockind_NUMER_KONTRAHENTA').value = map.NUMER_KONTRAHENTA;
	}
	else if (document.getElementById('dockind_NUMER_KONTRAHENTA').value != map.NUMER_KONTRAHENTA )
	{
		alert('Wybrano osob� zwi�zan� z innym kontrahentem.');
		document.getElementById('dockind_NUMER_KONTRAHENTAname').value = map.nazwaKontrahenta;
		//document.getElementById('dockind_NUMER_KONTRAHENTAphoneNumber').value = map.phoneNumber;
		document.getElementById('dockind_NUMER_KONTRAHENTA').value = map.NUMER_KONTRAHENTA;
	}
}

function setSegNumber(cnS,cnW)
{
	var el  = document.getElementById('dockind_'+cnS);
	var el2 = document.getElementById('dockind_'+cnW);
	if(el.value == null || el.value == '' )
	{
		el.value = document.getElementById('dockind_NR_SEG').value;
		//document.getElementById('dockind_NR_SEG').value = parseInt(document.getElementById('dockind_NR_SEG').value,10)+1;
	}
	if(el2.value == null || el2.value == '' )
	{
		el2.value = document.getElementById('dockind_NR_W_SEG').value;
		document.getElementById('dockind_NR_W_SEG').value = parseInt(document.getElementById('dockind_NR_W_SEG').value,10)+1;
	}
}

var freeDays = new Array();
var indexFD = 0;
function addFreeDay(sDate)
{
	var arr = sDate.split('-');
	var date = new Date(arr[2], arr[1] - 1, arr[0]);
	freeDays[indexFD++] = date;
}

function setNumberOfDays(cnFrom, cnTo, cnNum, cnAvailable, cnKind)
{
	
	$j('#dockind_' + cnKind).change(function(){
		setNumberOfDays(cnFrom, cnTo, cnNum, cnAvailable, cnKind);
	});
	var specialKindIDs = [100];
	var kind = document.getElementById('dockind_' + cnKind);
	var kindValue = 70;
	if(kind != null)
	{
		var kindValue = kind.options[kind.selectedIndex].value;	
	}
	
	
//	if (kindValue == 40)
//	{
//		alert("Ta opcja jest w tej chwili niedost�pna!");
//		kind.selectedIndex = 0;
//		return;
//	}
	
	var aDate1 = document.getElementById('dockind_' + cnFrom).value.split('-');
	var aDate2 = document.getElementById('dockind_' + cnTo).value.split('-');

	if (aDate1 == '' || aDate2 == '')
	{
		return;
	}
	
	var oDate1 = new Date(aDate1[2], aDate1[1] - 1, aDate1[0]);
	var oDate2 = new Date(aDate2[2], aDate2[1] - 1, aDate2[0]);
	
	trace(oDate1 + ', ' + oDate2);
	
	if (oDate1 > oDate2)
	{
		alert("Daty sa niepoprawne!");
		document.getElementById('dockind_' + cnTo).value = '';
		return;
	}
	
	var availableDays = document.getElementById('dockind_' + cnAvailable).value;
	var days = ((oDate2.getTime() + (oDate1.getTimezoneOffset() * 60 * 1000) - (oDate1.getTime() + (oDate2.getTimezoneOffset() * 60 * 1000))) / 1000 / 60 / 60 / 24) + 1;
	days = Math.floor(days);
	
	var toMinus = 0;
	
	// odj�cie dni wolnych 
	for (i = 0; i < freeDays.length; i++)
	{
		if (freeDays[i] >= oDate1 && freeDays[i] <= oDate2)
			toMinus++;
	}
	
	// sprawdzenie czy jest to rodzaj urlopu kt�remu liczone s� wszystkie dni kalendarzowe
	var specialKind = false;
	for (i = 0; i < specialKindIDs.length; i++)
		if (specialKindIDs[i] == kindValue)
			specialKind = true;
	// odj�cie weekend�w
	while (oDate1 <= oDate2 && !specialKind)
	{
		if (oDate1.getDay() == 0 || oDate1.getDay() == 6)
			toMinus++;
		oDate1.setDate(oDate1.getDate()+1);
	}
	
	days -= toMinus;
	if (availableDays < days && ( kindValue != 60 && kindValue != 50 && kindValue != 70 && kindValue != 80 && kindValue != 90 && kindValue != 40  && kindValue != 100)) // sprawdzenie ilosci dni i czy urlop bezplatny
	{
		document.getElementById('dockind_' + cnNum).value = '';
		alert('Ilosc dostepnych dni jest zbyt mala!')
		days = '';
	}
	
	document.getElementById('dockind_' + cnNum).value = days;
}


// wyodrebnienie powyzszej metody dla AEGON
function setNumberOfDaysForAEGON(cnFrom, cnTo, cnNum, cnAvailable, cnKind)
{
	$j('#dockind_' + cnKind).change(function(){
		setNumberOfDaysForAEGON(cnFrom, cnTo, cnNum, cnAvailable, cnKind);
	});
	var specialKindIDs = [100];
	var kind = document.getElementById('dockind_' + cnKind);
	var kindValue = kind.options[kind.selectedIndex].value;
	
	if (kindValue == 40)
	{
		alert("Ta opcja jest w tej chwili niedost�pna!");
		kind.selectedIndex = 0;
		return;
	}
	
	var aDate1 = document.getElementById('dockind_' + cnFrom).value.split('-');
	var aDate2 = document.getElementById('dockind_' + cnTo).value.split('-');

	if (aDate1 == '' || aDate2 == '')
	{
		return;
	}
	
	var oDate1 = new Date(aDate1[2], aDate1[1] - 1, aDate1[0]);
	var oDate2 = new Date(aDate2[2], aDate2[1] - 1, aDate2[0]);
	
	trace(oDate1 + ', ' + oDate2);
	
	if (oDate1 > oDate2)
	{
		alert("Daty sa niepoprawne!");
		document.getElementById('dockind_' + cnTo).value = '';
		return;
	}
	
	var availableDays = document.getElementById('dockind_' + cnAvailable).value;
	var days = ((oDate2.getTime() + (oDate1.getTimezoneOffset() * 60 * 1000) - (oDate1.getTime() + (oDate2.getTimezoneOffset() * 60 * 1000))) / 1000 / 60 / 60 / 24) + 1;
	days = Math.floor(days);
	
	var toMinus = 0;
	
	// odj�cie dni wolnych 
	for (i = 0; i < freeDays.length; i++)
	{
		if (freeDays[i] >= oDate1 && freeDays[i] <= oDate2)
			toMinus++;
	}
	
	// sprawdzenie czy jest to rodzaj urlopu kt�remu liczone s� wszystkie dni kalendarzowe
	var specialKind = false;
	for (i = 0; i < specialKindIDs.length; i++)
		if (specialKindIDs[i] == kindValue)
			specialKind = true;
	// odj�cie weekend�w
	while (oDate1 <= oDate2 && !specialKind)
	{
		if (oDate1.getDay() == 0 || oDate1.getDay() == 6)
			toMinus++;
		oDate1.setDate(oDate1.getDate()+1);
	}
	
	days -= toMinus;
	
	if (availableDays < days && kindValue != 60) // sprawdzenie ilosci dni i czy urlop bezplatny
	{
		document.getElementById('dockind_' + cnNum).value = '';
		alert('Ilosc dostepnych dni jest zbyt mala!')
		return;
	}
	
	document.getElementById('dockind_' + cnNum).value = days;
}

//kopia powyzszej metody dla PAA
function setNumberOfDaysForPAA(cnFrom, cnTo, cnNum, cnAvailable, cnKind)
{
	$j('#dockind_' + cnKind).change(function(){
		setNumberOfDaysForPAA(cnFrom, cnTo, cnNum, cnAvailable, cnKind);
	});
	var specialKindIDs = [100];
	var kind = document.getElementById('dockind_' + cnKind);
	var kindValue = kind.options[kind.selectedIndex].value;
	
	if (kindValue == 40)
	{
		alert("Ta opcja jest w tej chwili niedost�pna!");
		kind.selectedIndex = 0;
		return;
	}
	
	var aDate1 = document.getElementById('dockind_' + cnFrom).value.split('-');
	var aDate2 = document.getElementById('dockind_' + cnTo).value.split('-');

	if (aDate1 == '' || aDate2 == '')
	{
		return;
	}
	
	var oDate1 = new Date(aDate1[2], aDate1[1] - 1, aDate1[0]);
	var oDate2 = new Date(aDate2[2], aDate2[1] - 1, aDate2[0]);
	
	trace(oDate1 + ', ' + oDate2);
	
	if (oDate1 > oDate2)
	{
		alert("Daty sa niepoprawne!");
		document.getElementById('dockind_' + cnTo).value = '';
		return;
	}
	
	var availableDays = document.getElementById('dockind_' + cnAvailable).value;
	var days = ((oDate2.getTime() + (oDate1.getTimezoneOffset() * 60 * 1000) - (oDate1.getTime() + (oDate2.getTimezoneOffset() * 60 * 1000))) / 1000 / 60 / 60 / 24) + 1;
	days = Math.floor(days);
	
	var toMinus = 0;
	
	// odj�cie dni wolnych 
	for (i = 0; i < freeDays.length; i++)
	{
		if (freeDays[i] >= oDate1 && freeDays[i] <= oDate2)
			toMinus++;
	}
	
	// sprawdzenie czy jest to rodzaj urlopu kt�remu liczone s� wszystkie dni kalendarzowe
	var specialKind = false;
	for (i = 0; i < specialKindIDs.length; i++)
		if (specialKindIDs[i] == kindValue)
			specialKind = true;
	// odj�cie weekend�w
	while (oDate1 <= oDate2 && !specialKind)
	{
		if (oDate1.getDay() == 0 || oDate1.getDay() == 6)
			toMinus++;
		oDate1.setDate(oDate1.getDate()+1);
	}
	
	days -= toMinus;
	
	if (availableDays < days && kindValue != 60) // sprawdzenie ilosci dni i czy urlop bezplatny
	{
		document.getElementById('dockind_' + cnNum).value = '';
		alert('Ilosc dostepnych dni jest zbyt mala!')
		return;
	}
	
	document.getElementById('dockind_' + cnNum).value = days;
}