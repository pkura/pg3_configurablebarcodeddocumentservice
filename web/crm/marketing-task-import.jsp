<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1>Import zada� marketingowych</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/crm/marketing-task-import.action'"/>" method="post"  enctype="multipart/form-data">
<table>
	<tr>
		<td>
			<ww:file name="'file'" id="file" size="60" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
	 		<ds:lang text="CelDekretacji"/>: <br/>
		
	        <ww:select name="'objectiveSel'" list="objectives" cssClass="'sel'" id="objectiveSel"
	            listKey="name" listValue="name"
	            headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
	        <ds:lang text="lubWpisz"/>:
		</td>
	</tr>
	<tr>
		<td>
	        <input type="text" name="objectiveTxt" id="objectiveTxt" class="txt"
	            size="70" maxlength="300" onkeypress="if (!isEmpty(this.value)) { document.getElementById('objectiveSel').options.selectedIndex=0 };"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:event value="'Importuj'" name="'doImport'" cssClass="'btn'" confirm="'Na pewno importowa�?'"/>
			<ds:event name="'doTMP'" value="'Nadpisac informacje'"></ds:event>
		</td>
	</tr>
</table>
</form>