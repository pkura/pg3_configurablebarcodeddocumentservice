<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page import="pl.compan.docusafe.core.crm.Competition" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<ww:hidden name="'contractorId'" value="contractorId" id="contractorId" />
<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'selectedCompetition'" value="selectedCompetition" id="selectedCompetition" />
<table>
	<tr>
		<td>
			<ww:property value="crmPage"/>Rodzaj Konkurencji :
		</td>
		<td>
			<ww:select name="'competitionKind'" id="competitionKind" list="competitionKinds" listKey="cn" headerKey="''" headerValue="'wybierz'"  
				listValue="name" cssClass="'sel'" value="competitionKind"/>
		</td>
	</tr>
	<tr>
		<td>
			Nazwa Konkurencji :
		</td>
		<td>
			<ww:select name="'competitionName'" id="competitionName" list="competitionNames" listKey="cn" headerKey="''" headerValue="'wybierz'"  
				listValue="name" cssClass="'sel'" value="competitionName"/>
		</td>
	</tr>
	<tr>
		<td>
			Oficjalna :
		</td>
		<td>
			 <ww:checkbox name="'oficjalna'" fieldValue="true"/>
		</td>
	</tr>
	<tr>
		<td>Mar�a konkurencji od :</td>
		<td><ww:textfield name="'marzaOd'" id="marzaOd" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Mar�a konkurencji do :</td>
		<td><ww:textfield name="'marzaDo'" id="marzaDo" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Prowizja salonu :</td>
		<td><ww:textfield name="'prowizjaSalon'" id="prowizjaSalon" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Prowizja handlowca salonu :</td>
		<td><ww:textfield name="'prowizjaHandlowiec'" id="prowizjaHandlowiec" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Nagroda handlowca salonu :</td>
		<td><ww:textfield name="'nagrodaHandlowiec'" id="nagrodaHandlowiec" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Wysoko�� sprzeda�y :</td>
		<td>
			<ww:textfield name="'wysokoscSprzedazy'" id="wysokoscSprzedazy" size="18" maxlength="50" cssClass="'txt'"/>
			Okrest : <ww:select name="'okres'" id="okres" list="okresy" headerKey="''" headerValue="'wybierz'"  
				 cssClass="'sel'" value="okres"/>
		</td>
	</tr>
	<tr>
		<td>
			Opis : 
		</td>
		<td colspan="3">
			<ww:textarea name="'opis'" id="opis" cols="50" cssClass="'txt'" rows="6" ></ww:textarea>
		</td>
	</tr>


	<tr>
 		<td>
			<input type="submit" id="doAdd" name="doAdd" value="Dodaj" onclick="" class="btn"/>
			<ww:if test="selectedCompetition != null">
				<input type="submit" id="doUpdate" name="doUpdate" value="Zapisz" onclick="" class="btn"/>
			</ww:if>
		</td>
	</tr>
</table>
<table>
	<ww:if test="competitions.size() > 0">
		<tr>
			<th align="center">Rodzaj konkurencji</th>
			<th align="center">Nazwa konkurencji</th>
			<th align="center">Mar�a od</th>
			<th align="center">Mar�a do</th>
			<th align="center">Prowizja salon</th>
			<th align="center">Prowizja Handlowiec</th>
			<th align="center">Nagroda</th>
			<th align="center">Wysoko�� sprzeda�</th>
			<th align="center">Data dodania</th>
		</tr>
	</ww:if>
	<ww:iterator value="competitions">
		<tr>                      
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="competitionKind.name"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="competitionName.name"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="marzaOd"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="marzaDo"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="prowizjaSalon"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="prowizjaHandlowiec"/></a>
			</td> 
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="nagrodaHandlowiec"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="wysokoscSprzedazy"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCompetition.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCompetition'" value="id"/></ww:url>">
				<ww:property value="dataDodania"/></a>
			</td>
		</tr>
	</ww:iterator>
</table>