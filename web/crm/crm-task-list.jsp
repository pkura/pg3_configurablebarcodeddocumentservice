<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N crm-task-list.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Kontrahenci"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/crm/contractor-list.action'"/>" method="post" name="MainForm">

<ww:hidden name="'id'" value="id" id="id" />

    <table>
        <tr>
            <td colspan="2">
                <ds:lang text="Nazwa"/>
            </td>
            <td colspan="2">
                <ds:lang text="NIP"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <ww:textfield name="'nazwa'" id="nazwa" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/>
            </td>
            <td colspan="2">
                <ww:textfield name="'nip'" id="nip" size="10" maxlength="10" onchange="'notifyChange();'" cssClass="'txt'"/>
            </td>
            <td>
                <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
            </td>
            <td>
                <input type="button" value="<ds:lang text="Dodaj"/>" class="btn" onclick="document.location.href='<ww:url value='&apos;/crm/crmContactor.action&apos;'/>';"<ww:if test="!canAdd">disabled="disabled"</ww:if>/>
            </td>
        </tr>
    </table>

<ww:if test="results!=null">
    <table class="search" width="600" cellspacing="0" id="mainTable">
        
        <tr>
            <th/>
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'name'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Nazwa"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'name'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'nip'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Nip"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'nip'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

    		<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'regon'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Regon"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'regon'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>

    	</tr>
        <ww:iterator value="results">
            <tr>
                <td>
                    <input type="radio" name="contractors" id="<ww:property value="id"/>radio"/>
                </td>
                <td>
                    <a href="<ww:url value='&apos;/crm/crmContactor.action&apos;'/>?id=<ww:property value="id"/>"><ww:property value="name"/></a>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
                <td>
                    <ww:property value="nip"/>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td><ww:property value="regon"/></td>
        	</tr>
    	</ww:iterator>
	</table>
   <ww:set name="pager" scope="request" value="pager"/>
   <table width="100%">
      <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
   </table>

   <select id="selectAction"  class="sel" >
        <option>Wybierz akcj�</option>
        <option>Nowe zg�oszenie</option>
        <option>Nowe zadanie</option>
        <option>Usu�</option>
   </select>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="button" class="btn" value="Wykonaj" onClick="doAction()"/>
   <br/>
</ww:if>
</form>


<script language="JavaScript">

    function doAction()
    {
        var val = 'elo';

        for( i = 0; i < document.MainForm.contractors.length; i++ )
        {
            if( document.MainForm.contractors[i].checked == true )
            val = document.MainForm.contractors[i].id;
        }
        var ID = val.substring(0,val.length-5);
        var sel = E("selectAction");

        if(ID.length==0)
        {
            alert('Nie wybrano kontrahenta');
            return;
        }
        if(sel.selectedIndex==0)
            alert('Nie wybrano akcji !');
        else if(sel.selectedIndex==1)
            document.location = '<ww:url value="'/crm/new-application.action'"/>?contractorId='+ID;
        else if(sel.selectedIndex==2)
            document.location = '<ww:url value="'/crm/new-task.action'"/>?contractorId='+ID;
        else if(sel.selectedIndex==3)
            document.location = '<ww:url value="'/crm/contractor-list.action'"/>?id='+ID+'&doDelete=true';
    }

    prepareTable(E("mainTable"),2,0);

</script>