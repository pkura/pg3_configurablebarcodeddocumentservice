<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N crm-fields.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>

<ww:hidden name="'categoryTitle'" value="categoryTitle"/>
<ww:hidden name="'kindTitle'" value="kindTitle"/>


 <tr>
            <td>
                    <b>Kategoria</b>
            </td>
            <td>
                <select id="mainSelect" onChange="changeMainSelect()" class="sel">
                    <ww:iterator value="tasksKinds" >
                        <option value="<ww:property value="key"/>">
                            <ww:property value="key"/>
                        </option>
                    </ww:iterator>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                    <b>Rodzaj</b>
            </td>
            <td>
                <div id="refSelects">
                    <ww:iterator value="tasksKinds" status="status">
                        <select  class="sel" name="taskKind" <ww:if test="!#status.first">style="display:none;"</ww:if>>
                            <ww:iterator value="value">
                                <option>
                                    <ww:property/>
                                </option>
                            </ww:iterator>
                        </select>
                    </ww:iterator>
                 </div>
            </td>
        </tr>

<script type="text/javascript">

    function create()
    {
        var category = document.getElementById('categoryTitle');
        var kind = document.getElementById('kindTitle');
        var mainSelect = document.getElementById('mainSelect');
        var elements = document.getElementsByName('taskKind');
        var i, selectedIndex = mainSelect.selectedIndex;

        //ustawienie kategorii
        category.value = mainSelect.options[mainSelect.selectedIndex].value;

        for(i=0;i<elements.length;i++)
        {
            if(i==selectedIndex)
            {
                kind.value = elements[i].options[elements[i].selectedIndex].text;
//                alert(elements[i].options[elements[i].selectedIndex].value);
//				alert(elements[i].options[elements[i].selectedIndex].text);
//				alert(elements[i]);

             }
        }
//		alert(i);
//		alert(selectedIndex);
        
        document.getElementById('doCreate').value="'true'";
    }

    function changeMainSelect()
    {
        //var div = document.getElementById('refSelects');
        var elements = document.getElementsByName('taskKind');
        var i, selectedIndex = document.getElementById('mainSelect').selectedIndex;
        
        for(i=0;i<elements.length;i++)
        {
            elements[i].style.display='none';
            if(i==selectedIndex)
                elements[i].style.display='';
        }
    }
</script>


<!--
        <tr>
            <td>
                    <b>Kategoria</b>
            </td>
            <td>
                <select id="taskCategory" onChange="changeTaskCategory(this)" class="sel">
                    <ww:iterator value="tasksKinds" >
                        <option value="<ww:property value="key"/>">
                            <ww:property value="key"/>
                        </option>
                    </ww:iterator>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                    <b>Rodzaj</b>
            </td>
            <td>
                <ww:iterator value="tasksKinds" status="status">
                    <select id="<ww:property value="key"/>" class="sel" name="taskKind" <ww:if test="!#status.first">style="display:none;</ww:if>">
                        <ww:iterator value="value">
                            <option>
                                <ww:property/>
                            </option>
                        </ww:iterator>
                    </select>
                </ww:iterator>
            </td>
        </tr>

<script type="text/javascript">

    function create()
    {
        document.getElementById('doCreate').value="'true'";
        
        var category = document.getElementById('categoryTitle');
        var kind = document.getElementById('kindTitle');
        var x = document.getElementById('newTaskTable');
        var i;
        
        x = x.getElementsByTagName('select');
        for(i=0;i<x.length;i++)
            if(x[i].style.display!='none')
            {
                var ctrl = x[i];
                kind.value = ctrl.options[ctrl.selectedIndex].text;
                alert(ctrl.options[ctrl.selectedIndex].text);
            }
        x = document.getElementById('taskCategory');
        document.getElementById('categoryTitle').value = x.options[x.selectedIndex].value;
    }

    function changeTaskCategory(sel)
    {
        var x = document.getElementsByName('taskKind');
        var i;
        for(i=0;i<x.length;i++)
            x.item(i).style.display='none';

        var selected = sel.options[sel.selectedIndex].value;
        var ctrl = document.getElementById(selected);
        //var ctrl = document.getElementById(sel.selectedIndex);
        ctrl.style.display='';
    }
</script>
-->