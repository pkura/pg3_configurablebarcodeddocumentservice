<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N contractor-import.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1>Import wezwa� do zap�aty</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/crm/contractor-import.action'"/>" method="post"  enctype="multipart/form-data">
<table>
	<tr>
		<td>
			<ww:file name="'file'" id="file" size="60" cssClass="'txt'"/>
		</td>
	</tr>
	<tr>
		<td>
	 		<ds:lang text="CelDekretacji"/>: <br/>
		
	        <ww:select name="'objectiveSel'" list="objectives" cssClass="'sel'" id="objectiveSel"
	            listKey="name" listValue="name"
	            headerKey="''" headerValue="getText('select.wybierz')" value="objectiveSel"/>
	        <ds:lang text="lubWpisz"/>:
		</td>
	</tr>
	<tr>
		<td>
	        <input type="text" name="objectiveTxt" id="objectiveTxt" class="txt"
	            size="70" maxlength="300" onkeypress="if (!isEmpty(this.value)) { document.getElementById('objectiveSel').options.selectedIndex=0 };"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:event value="'Importuj'" name="'doImport'" cssClass="'btn'" confirm="'Na pewno importowa�?'"/>
		</td>
	</tr>
</table>
</form>