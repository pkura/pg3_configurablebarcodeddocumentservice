<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page import="pl.compan.docusafe.core.crm.Competition" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<ww:hidden name="'contractorId'" value="contractorId" id="contractorId" />
<ww:hidden name="'id'" value="id" id="id" />
<ww:hidden name="'selectedCooperation'" value="selectedCooperation" id="selectedCooperation" />
<table>
	<tr>
		<td>Mar�a sugerowana od :</td>
		<td><ww:textfield name="'marzaOd'" id="marzaOd" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Mar�a sugerowana do :</td>
		<td><ww:textfield name="'marzaDo'" id="marzaDo" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Prowizja sugerowana salonu od :</td>
		<td><ww:textfield name="'prowizjaSalonOd'" id="prowizjaSalonOd" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
	<td>Prowizja sugerowana salonu do :</td>
		<td><ww:textfield name="'prowizjaSalonDo'" id="prowizjaSalonDo" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Prowizja handlowca salonu :</td>
		<td><ww:textfield name="'prowizjaHandlowiec'" id="prowizjaHandlowiec" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Nagroda handlowca salonu :</td>
		<td><ww:textfield name="'nagrodaHandlowiec'" id="nagrodaHandlowiec" size="18" maxlength="50" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>
			Opis : 
		</td>
		<td colspan="3">
			<ww:textarea name="'opis'" id="opis" cols="50" cssClass="'txt'" rows="6" ></ww:textarea>
		</td>
	</tr>
	<tr>
 		<td>
			<input type="submit" id="doAdd" name="doAdd" value="Dodaj" onclick="" class="btn"/>
			<ww:if test="selectedCooperation != null">
				<input type="submit" id="doUpdate" name="doUpdate" value="Zapisz" onclick="" class="btn"/>
			</ww:if>
		</td>
	</tr>
</table>
<table>
	<ww:if test="cooperationTerms.size() > 0">
		<tr>
			<th align="center">Mar�a od</th>
			<th align="center">Mar�a do</th>
			<th align="center">Prowizja salon od</th>
			<th align="center">Prowizja salon do</th>
			<th align="center">Prowizja Handlowiec</th>
			<th align="center">Nagroda</th>
			<th align="center">Data dodania</th>
		</tr>
	</ww:if>
	<ww:iterator value="cooperationTerms">
		<tr>                   
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="marzaOd"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="marzaDo"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="prowizjaSalonOd"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="prowizjaSalonDo"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="prowizjaHandlowiec"/></a>
			</td> 
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="nagrodaHandlowiec"/></a>
			</td>
			<td align="center"><a href="<ww:url value="'crmCooperationTerms.action'"><ww:param name="'contractorId'" value="contractorId"/><ww:param name="'param'" value="param"/><ww:param name="'selectedCooperation'" value="id"/></ww:url>">
				<ww:property value="dataDodania"/></a>
			</td>
		</tr>
	</ww:iterator>
</table>