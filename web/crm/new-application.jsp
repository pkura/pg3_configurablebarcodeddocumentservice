<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N new-application.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!--
    <script src="<c:out value='${pageContext.request.contextPath}'/>/prototype.js" type="text/javascript"></script>
    <script src="<c:out value='${pageContext.request.contextPath}'/>/effects.js" type="text/javascript"></script>
    <script src="<c:out value='${pageContext.request.contextPath}'/>/scriptaculous.js?load=effects" type="text/javascript"></script>
    -->
    <script src="<ww:url value="'/prototype.js'"/>" type="text/javascript"></script>
    <script src="<ww:url value="'/effects.js'"/>" type="text/javascript"></script>
    <script src="<ww:url value="'/scriptaculous.js'"/>" type="text/javascript"></script>   
    
<h1>Nowe Zgłoszenie</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />

    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form action="<ww:url value="'/crm/new-application.action'"/>" method="post">


<ww:hidden name="'contractorId'" value="contractorId" id="id" />

<ww:if test="applicationArchived">
    <input type="button" value="Nowe Zadanie" class="btn" onClick="document.location='<ww:url value="'/crm/new-task.action'"/>';">
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    <input type="button" value="Nowe Zgłoszenie" class="btn" onClick="document.location='<ww:url value="'/crm/new-application.action'"/>';">
</ww:if>
<ww:if test="!applicationArchived">
	    <input type="button" class="btn" value="wybierz pracownika" onClick="window.location='<ww:url value="'/crm/contractor-worker-list.action'"/>'"/>
	    &nbsp&nbsp&nbsp&nbsp
	    <input type="button" class="btn" value="wybierz kontrahenta" onClick="window.location='<ww:url value="'/crm/contractor-list.action'"/>'"/>
	    <br/>
	    <table>
	        <tr>
	            <td>
	                    <b>Kontrahent/pracownik</b>
	            </td>
	            <td>
	                    <ww:property value="contractorName"/>
	            </td>
	        </tr>
	        <jsp:include page="/common/dockind-fields.jsp"/>
			<jsp:include page="/common/dockind-specific-additions.jsp"/>
			<ww:if test="contractorName != null">
				<tr>
            		<td colspan="2">
            		<input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn"
	        		onclick="if (!validateForm()) return false;document.getElementById('doSave').value = 'true';" />
            	</td>
            </ww:if>
        </tr>
	    </table>

</ww:if>
<script type="text/javascript">
	function validateForm()
    {
        if (!validateDockind())
            return false;

        return true;
    }
</script>
 
</form>
