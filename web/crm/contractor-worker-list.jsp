<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Osoby"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form action="<ww:url value="'/crm/contractor-worker-list.action'"/>" method="post" name="MainForm">
<ww:hidden name="'id'" value="id" id="id" />
		<table>
			<tr>
				<td colspan="2"><ds:lang text="Imie"/></td>
				<td colspan="2"><ds:lang text="Nazwisko"/></td>
			</tr>
			<tr>
                <td colspan="2"><ww:textfield name="'firstname'" id="firstname" size="20" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
                <td colspan="2"><ww:textfield name="'lastname'" id="lastname" size="20" maxlength="30" onchange="'notifyChange();'" cssClass="'txt'"/></td>
            	<td><input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/></td>
            	<td><input type="button" value="<ds:lang text="Dodaj"/>" class="btn"
    				onclick="document.location.href='<ww:url value='&apos;/crm/new-contractor-worker.action&apos;'/>';"
    				<ww:if test="!canAdd">disabled="disabled"</ww:if> />
    			</td>
            </tr>
		</table>        

<ww:if test="results!=null">
    <table class="search" width="800" cellspacing="0" id="mainTable">
        
        <tr>
           
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'firstname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Imie"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'firstname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'lastname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="Nazwisko"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'lastname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

    		<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="NumerTelefonu"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>
			
			<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'email'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
    		 	<ds:lang text="email"/>
			<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'email'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			
    	</tr>
        <ww:iterator value="results">
            <tr>
            <!-- 
                <td width="10">
                    <input type="radio" name="contractors" id="<ww:property value="id"/>radio"/>
                </td>
             -->
                <td>
                    <a href="<ww:url value='&apos;/crm/new-contractor-worker-tab.action&apos;'/>?thisWorkerId=<ww:property value="id"/>&id=<ww:property value="contractorId"/>"><ww:property value="firstname"/></a>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
                <td>
                    <ww:property value="lastname"/>
                </td>
                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="phoneNumber"/>
				</td>
				<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
				<td>
					<ww:property value="email"/>
				</td>
        	</tr>
    	</ww:iterator>
    	<!-- Wstawiony poniewaz ja kjest jeden radioButton to nie widzi tego obiektu -->
    	<tr style="display:none;">
    		<td >
                    <input type="radio" name="contractors" id="hiddenRadio" checked="false"/>
            </td>
    	</tr>
	</table>

   <ww:set name="pager" scope="request" value="pager"/>
   <table width="100%">
      <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
   </table>
<!-- 
   <select id="selectAction"  class="sel" >
        <option>Wybierz akcj�</option>
        <option>Nowe zg�oszenie</option>
        <option>Nowe zadanie</option>
        <option>Usu�</option>
   </select>
   &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input type="button" class="btn" value="Wykonaj" onClick="doAction()"/>
   <br/> -->
</ww:if>
</form>
<script language="JavaScript">
 function doAction()
    {
        var val = 'elo';
        for( i = 0; i < document.MainForm.contractors.length; i++ )
        {
            if( document.MainForm.contractors[i].checked == true )
            val = document.MainForm.contractors[i].id;
        }
        var ID = val.substring(0,val.length-5);
        var sel = E("selectAction");

        if(ID.length==0)
        {
            alert('Nie wybrano kontrahenta');
            return;
        }
        if(sel.selectedIndex==0)
            alert('Nie wybrano akcji !');
        else if(sel.selectedIndex==1)
            document.location = '<ww:url value="'/crm/new-application.action'"/>?contractorWorkerId='+ID;
        else if(sel.selectedIndex==2)
            document.location = '<ww:url value="'/crm/new-task.action'"/>?contractorWorkerId='+ID;
        else if(sel.selectedIndex==3)
            document.location = '<ww:url value="'/crm/contractor-worker-list.action'"/>?id='+ID+'&doDelete=true';
    }

        prepareTable(E("mainTable"),2,0);
        
</script>