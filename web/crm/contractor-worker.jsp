<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h1><ds:lang text="Osoby"/></h1>
<hr size="1" align="left" class="horizontalLine" width="100%" />

<ww:if test="isCrm()">
		<ds:available test="!layout2">
<p>
	<ds:xmlLink path="KontrahentCRM"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="KontrahentCRM"/>
	</div>
</ds:available>
</ww:if>
<ww:else>
<ds:available test="!layout2">
<p>
	<ds:xmlLink path="Kontrahent"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="Kontrahent"/>
	</div>
</ds:available>
</ww:else>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form id="form" action="<ww:url value='baseLink'/>" method="post" onsubmit="return disableFormSubmits(this);">

<ww:hidden name="'id'" value="id" id="id"/>
<ww:hidden name="'doAdd'" id="doAdd"/>
<ww:hidden name="'doUpdate'" id="doUpdate"/>
<ww:hidden name="'doDelete'" id="doDelete"/>
<ww:hidden name="'thisWorkerId'" id="thisWorkerId" value="thisWorkerId"/>
<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>


<ww:if test="results == null || results.empty">
	<table>
		<tr>
			<td colspan="2"><ds:lang text="Imie"/><span class="star">*</span></td>
		</tr>
		<tr>
	        <td colspan="2" ><ww:textfield name="'firstname'" id="firstname" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="Nazwisko"/><span class="star">*</span></td>
		</tr>
		<tr>
			<td colspan="2"><ww:textfield name="'lastname'" id="lastname" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="NumerTelefonu"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'phoneNumber'" id="phoneNumber" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="NumerTelefonu"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'phoneNumber2'" id="phoneNumber2" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	<!--
	    <tr>
			<td colspan="2"><ds:lang text="Ulica"/></td>
		</tr>
		<tr>
	         <td colspan="2"><ww:textfield name="'street'" id="street" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
		<tr>
			<td><ds:lang text="kod"/></td><td><ds:lang text="miasto"/></td>
	    </tr>
	    <tr>
	      	<td><ww:textfield name="'zip'" id="zip" size="10" maxlength="10" cssClass="'txt'"/></td>
	       	<td><ww:textfield name="'location'" id="location" size="25" maxlength="30" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="kraj"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'country'" id="country" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	   	<tr>
			<td colspan="2"><ds:lang text="Pesel"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'pesel'" id="pesel" size="40" maxlength="50" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="Nip"/></td>
		</tr>

		<tr>
	         <td colspan="2"><ww:textfield name="'nip'" id="nip" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	     -->
		<tr>
			<td colspan="2"><ds:lang text="email"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'email'" id="email" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	   	<tr>
			<td colspan="2"><ds:lang text="fax"/></td>
		</tr>
		<tr>
	        <td colspan="2"><ww:textfield name="'fax'" id="fax" size="40" maxlength="40" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
			<td colspan="2"><ds:lang text="Uwagi"/></td>
		</tr>
		<tr>
	    	<td colspan="2"><ww:textarea name="'remarks'" id="remarks" cols="38" rows="4" onchange="'notifyChange();'" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
   			 <td>Kontrahent</td>
   			 <td><ww:textfield name="'contractorName'" id="contractorName" size="20" maxlength="40"  cssClass="'txt'"/></td>
	   	</tr>
	   	<tr>
	   			 <td colspan="2"> <input type="button" value="Do��cz do kontrahenta" onclick="openDictionary_KLIENT()" class="btn" /></td>
	    </tr>
	</table>
	<table>
       		<tr>
		        <td colspan="3">
		            <input type="submit" name="doAdd" value="<ds:lang text="Dodaj"/>" onclick="document.getElementById('doAdd').value = 'true';if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd">disabled="disabled"</ww:if>/>
		            <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
		        	<input type="submit"  name="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" onclick="document.getElementById('doUpdate').value = 'true';" <ww:if test="thisWorkerId == null || !canEdit">disabled="disabled"</ww:if>/>
		    	</td>
		    </tr>
		    <tr>
   				<td>
					<ds:event name="'doSearch'" value="'Szukaj'"/>
					<ww:if test="!isCrm()">
						<input type="button" id="doSubmit" value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="thisWorkerId == null">disabled="disabled"</ww:if>/>
       				</ww:if>
       				<input type="submit" name="doDelete" id="doDelete" value="<ds:lang text="Usun"/>" onclick="document.getElementById('doDelete').value = 'true';" class="btn" <ww:if test="thisWorkerId == null || !canDelete">disabled="disabled"</ww:if>/>
       			</td>
   			</tr>
			<tr>
		    	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
		    </tr>
			<ww:elseif test="workers != null && workers.size() > 0">

				<table class="search" width="700" cellspacing="0" id="mainTable">
			        <tr align="left">
			        	<td align="left"> <b>Pracownicy :</b></td>
			        </tr>
			        <tr>
						<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'firstname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
			    		 	<ds:lang text="Imie"/>
						<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'firstname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

						<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'lastname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
			    		 	<ds:lang text="Nazwisko"/>
						<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'lastname'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
			    		<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>

			    		<th><nobr><ww:if test="sortDesc != null"><a href="<ww:url value="sortDesc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="11" height="11" border="0"/></a></ww:if>
			    		 	<ds:lang text="NumerTelefonu"/>
						<ww:if test="sortAsc != null"><a href="<ww:url value="sortAsc"><ww:param name="'sortField'" value="'phoneNumber'"/></ww:url>"><img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a></ww:if></nobr></th>
						<td class="s" background="/docusafe/img/pionowa-linia.gif"></td>
			    	</tr>
			        <ww:iterator value="workers">
			            <tr>
			                <td align="left"><a href="<ww:url value="baseLink"><ww:param name="'thisWorkerId'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'id'" value="contractorId"/></ww:url>">
								<ww:property value="firstname"/></a>
							</td>
			                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
			                <td>
			                    <ww:property value="lastname"/>
			                </td>
			                <td class="s" background="/docusafe/img/pionowa-linia.gif"/>
							<td>
								<ww:property value="phoneNumber"/>
							</td>
							<td class="s" background="/docusafe/img/pionowa-linia.gif"/>
							<ww:if test="defaultWorkerId == id">
							<td>Domy�lny</td>
							</ww:if>
							<ww:else>
								<td align="center"><a href="<ww:url value="baseLink"><ww:param name="'thisWorkerId'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'doChangeDefault'" value="'true'"/><ww:param name="'id'" value="contractorId"/></ww:url>">
									Ustaw jako domy�lny</a>
								</td>
							</ww:else>
			        	</tr>
			    	</ww:iterator>
			    	<tr>
				    	<td><p><span class="star">*</span><ds:lang text="PoleObowiazkowe"/></p></td>
				    </tr>
				</table>
			</ww:elseif>
	</table>
</ww:if>
<ww:else>
	<table width="100%">
        <tr>
            <th><ds:lang text="Imie"/></th>
            <th><ds:lang text="Nazwisko"/></th>
            <th><ds:lang text="Telefon"/></th>
    	</tr>
		<ww:iterator value="results">
        	<tr>
       	    	<td><ww:property value="firstname" /></td>
       	    	<td><ww:property value="lastname" /></td>
                   <td><ww:property value="phoneNumber" /></td>
                    <td><a href="<ww:url value='baseLink'><ww:param name="'id'" value="id"/><ww:param name="'param'" value="param"/><ww:param name="'canAddAndSubmit'" value="canAddAndSubmit"/><ww:param name="'dictionaryAction'" value="true"/></ww:url>">
                        <ds:lang text="wybierz"/></a></td>
                    <ww:if test="canDelete">
                		<td><a href="<ww:url value='baseLink'><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/><ww:param name="'param'" value="param"/></ww:url>" onclick="if (!confirm(getText('NaPewnoUsunacWpis'))) return false;"><ds:lang text="usun"/></a></td>
                    </ww:if>
        	</tr>
    	</ww:iterator>
	</table>
    <ww:set name="pager" scope="request" value="pager"/>
    <table width="100%">
        <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
    </table>

    <input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
        onclick="document.location.href='<ww:url value='baseLink'><ww:param name="'param'" value="param"/></ww:url>';"/>





</ww:else>
<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<script language="JavaScript">
        function id(id)
        {
            return document.getElementById(id);
        }

        function notifyChange()
        {

        }

        function confirmAdd()
        {
        	 document.getElementById('id').valu
                return confirm('<ds:lang text="NaPewnoDodacDoSlownika"/>?');
        }

        function clearForm()
        {
            document.getElementById('id').value = '';
            document.getElementById('firstname').value = '';
            document.getElementById('lastname').value = '';
//			document.getElementById('street').value = '';
//			document.getElementById('zip').value = '';
//			document.getElementById('location').value = '';
//			document.getElementById('country').value = '';
//			document.getElementById('pesel').value = '';
//			document.getElementById('nip').value = '';
			document.getElementById('email').value = '';
			document.getElementById('fax').value = '';
			document.getElementById('remarks').value = '';
			document.getElementById('phoneNumber').value = '';
			document.getElementById('phoneNumber2').value = '';
			document.getElementById('contractorName').value = '';
			document.getElementById('thisWorkerId').value = '';

        }

	    function openDictionary_KLIENT()
	    {
	        openToolWindow('<ww:url value="'/office/common/contractor.action'"/>?param=KLIENT','KLIENT', 700, 650);
	    }

	   function accept(param,map)
	   {
	      document.getElementById('id').value = map.id;
	      document.getElementById('contractorName').value = map.name;
	   }

	   function submitStrona()
       {
            if (!window.opener)
            {
                alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
                return;
            }

            var strona = new Array();

            strona.id = id('thisWorkerId').value;
            strona.imieNazwisko = id('firstname').value + ' '+id('lastname').value;
            strona.firstname = id('firstname').value;
            strona.lastname = id('lastname').value;
            strona.phoneNumber = id('phoneNumber').value;
            strona.nazwaKontrahenta = id('contractorName').value;
            strona.NUMER_KONTRAHENTA = id('id').value;
            strona.dictionaryDescription = '<ww:property value="dictionaryDescription"/>';


			if (window.opener.accept)
                window.opener.accept('NUMER_PRACOWNIKA', strona);
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
			window.close();
            return true;
       }

        function setKlient()
        {
        	if (!window.opener.accept_NUMER_PRACOWNIKA)
        	{
        		if(document.getElementById('doSubmit') != null)
        		{
        			document.getElementById('doSubmit').style.display = 'none';
        		}
        	}

        	if(document.getElementById('id').value == null || document.getElementById('id').value == '' )
        	{
        			var tmp = window.opener.getId_NUMER_KONTRAHENTA();
        			document.getElementById('id').value = tmp;
        			document.getElementById('contractorName').value = window.opener.getdockind_NUMER_KONTRAHENTAname();
        	}
        }
        setKlient();
	    prepareTable(E("mainTable"),2,1);

</script>