<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>

<h1><ww:property value="kind.getReport(reportCn).name"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/mootools.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/timePicker/timePicker.js?v=<%= Docusafe.getDistTimestamp() %>"></script>
<h3><ww:property value="title"/></h3>
<form action="<ww:url value="'/reports/specify-report-params.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);" enctype="multipart/form-data">
    <ww:hidden name="'documentKindCn'" value="documentKindCn"/>
    <ww:hidden name="'reportCn'" value="reportCn"/>
    
    <table>
   		<tr>
			<th>
				<ds:lang text="Tytul"/>:
			</th>
			<td>
				<ww:textfield name="'title'" size="30" maxlength="100" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<th>
				<ds:lang text="Opis"/>:
			</th>
			<td>
				<ww:textfield name="'description'" size="40" maxlength="300" cssClass="'txt'"/>
			</td>
		</tr>
		<ww:if test="shareable">
		<tr>
			<th>
				<ds:lang text="Grupa"/>:
			</th>
			<td>
				<select class="multi_sel" size="5" id="guid" name="guid" multiple="multiple">                    
                         <ww:iterator value="groups">
                         	<option value="<ww:property value="guid"/>">
                                 <ww:property value="name"/>
                            </option>
                         </ww:iterator>
                </select>
			</td>
		</tr>
		</ww:if>
   </table>
	<table>
    	<tr>
    <ww:iterator value="params">
		<ww:if test="type == 'break-line'">
			<td><ww:property value="label"/></td><td></td></tr><tr>
		</ww:if>
		<ww:else>
            <td><ww:property value="label"/><ww:if test="required"><span class="star">*</span></ww:if>:</td>                
            <td>
				<ww:set name="id" value="fieldCn"/>
				<ww:set name="name" value="'values.'+fieldCn"/>				
				<ww:if test="type=='text'">
					 <input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" size="10" maxlength="50" class="txt"/>
				</ww:if>
                <ww:elseif test="type=='bool'">
                   <input type="checkbox" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" />
               	</ww:elseif>
				<ww:elseif test="type == 'date'">            
                        <input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" size="10" maxlength="10" class="txt"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="<ww:property value="#id"/>_trigger" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                            
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField     :    "<ww:property value="fieldCn"/>",     // id of the input field
                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                button         :    "<ww:property value="#id"/>_trigger",  // trigger for the calendar (button ID)
                                align          :    "Tl",           // alignment (defaults to "Bl")
                                singleClick    :    true
                            });
                        </script>    
                                         
                 </ww:elseif>
				 <ww:elseif test="type == 'hour'">     
				 		<script type="text/javascript">
							var tp_<ww:property value="#id"/> = null;
							/* ustawiamy odpowiednie pola po wczytaniu dokumentu */
							$j(document).ready( function() {
									tp_<ww:property value="#id"/> = new TimePicker('<ww:property value="#id"/>_picker', '<ww:property value="#id"/>', '<ww:property value="#id"/>_toggler', {format24:true, imagesPath:"<ww:url value="'/timePicker/images'"/>",
										offset:{x:0, y:10}});
								});
						
						</script>  
						<ds:lang text="godz" /> : <input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" size="2" maxlength="20" class="txt"/>
						<a href="#" id="<ww:property value="#id"/>_toggler" title="<ds:lang text="oGodzinie" />" style="line-height: 22px; vertical-align: middle">
							<img src="<ww:url value="'/img/clock.gif'"/>" style="margin-bottom: -6px; _margin-bottom: 0px;" />
						</a>
						<div id="<ww:property value="#id"/>_picker" class="time_picker_div"></div>                     
                 </ww:elseif>
                 <ww:elseif test="type == 'division'">
					
					<select class="multi_sel" size="5" id="<ww:property value="#id"/>" name="<ww:property value="#name"/>" multiple="multiple">                    
                         <ww:iterator value="divisions">
                         	<option value="<ww:property value="guid"/>">
                                 <ww:property value="name"/>
                             </option>
                         </ww:iterator>
                     </select>
                     <br/>
                     <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), true))"><ds:lang text="zaznacz"/></a>
                     / <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), false))"><ds:lang text="odznacz"/></a>
                     <ds:lang text="wszystkie"/>
                     
                 </ww:elseif>
				
                 <ww:elseif test="type == 'user'">
                     <select class="multi_sel" size="5" id="<ww:property value="#id"/>" name="<ww:property value="#name"/>" multiple="multiple">                    
                         <ww:iterator value="users">
                             <option value="<ww:property value="name"/>">
                                 <ww:property value="lastname+' '+firstname"/>
                             </option>
                         </ww:iterator>
                     </select>                    
                     <br/>
                     <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), true))"><ds:lang text="zaznacz"/></a>
                     / <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), false))"><ds:lang text="odznacz"/></a>
                     <ds:lang text="wszystkie"/> 
                 </ww:elseif>
				 <ww:elseif test="type == 'select'">
                    <%--<ww:select name="#name" id="<ww:property value="#id"/>"
                               cssClass="'multi_sel'"
                               size="5"
                               multiple="multiple"
                               list="availableValues"
                               listKey="key"
                               listValue="value">
                    </ww:select> --%>
                    <select class="multi_sel" size="5"
                            id="<ww:property value="#id"/>"
                            name="<ww:property value="#name"/>"
                            multiple="multiple">
                         <ww:iterator value="availableValues">
                             <option value="<ww:property value="key"/>">
                                 <ww:property value="value"/>
                             </option>
                         </ww:iterator>
                     </select>
					<ww:if test="multiple">
					<br/>
					<a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), true))"><ds:lang text="zaznacz"/></a>
                     / <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), false))"><ds:lang text="odznacz"/></a>
                     <ds:lang text="wszystkie"/> 
					</ww:if>
                 </ww:elseif>
                <ww:elseif test="type == 'file'">
                    <input type="file" id="file" name="file" class="txt" size="50"/>
                </ww:elseif>
                <ww:elseif test="type == 'dockind'">
                    <ww:set name="field" value="kind.getFieldByCn(dockindCn)"/>
                    <ww:if test="#field.isMultiple()">
                    	<ww:if test="#field.type == 'money' || #field.type == 'string' || #field.type == 'float' || #field.type == 'long' || #field.type == 'double' || #field.type == 'integer'">
							<input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>"
			                    size="30" maxlength="<ww:property value="length"/>" class="txt" /> 			                                   
			               <textarea name="in<ww:property value="#name"/>" id="in<ww:property value="#id"/>"
			               		 cols="30" rows="3" style="display:none;" class="txt"></textarea>
			               <a href="javascript:void(changeDisplay('<ww:property value="#name"/>','<ww:property value="#id"/>'))">
			               		<img id="img<ww:property value="#id"/>" src="<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif" width="11" height="11" border="0"/>
			               </a>
						</ww:if>
                    </ww:if>
                    <ww:else>
	                    <ww:if test="#field.type == 'enum' || #field.type == 'enum-ref' || #field.type == 'dataBase'">
	                        <select class="multi_sel" size="5" id="<ww:property value="#id"/>" name="<ww:property value="#name"/>" <ww:if test="multiple">multiple="multiple"</ww:if>>                    
	                            <ww:iterator value="#field.sortedEnumItems">
	                                <option value="<ww:property value="id"/>" <ww:if test="values[fieldCn] == id">selected="selected"</ww:if>>
	                                    <ww:property value="title"/>
	                                </option>
	                            </ww:iterator>
	                        </select>                    
	                        <br/>
	                        <ww:if test="multiple">
								<a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), true))"><ds:lang text="zaznacz"/></a>
			                     / <a href="javascript:void(select(getElementByName('<ww:property value="#name"/>'), false))"><ds:lang text="odznacz"/></a>
			                     <ds:lang text="wszystkie"/> 
							</ww:if>
	                    </ww:if>
						<ww:elseif test="#field.type == 'date' || #field.type == 'timestamp'">
							<input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" size="10" maxlength="10" class="txt"/>
	                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
	                            id="<ww:property value="#id"/>_trigger" style="cursor: pointer; border: 1px solid red;"
	                            title="Date selector" onmouseover="this.style.background='red';"
	                            onmouseout="this.style.background=''"/>
	                            
	                        <script type="text/javascript">
	                            Calendar.setup({
	                                inputField     :    "<ww:property value="fieldCn"/>",     // id of the input field
	                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	                                button         :    "<ww:property value="#id"/>_trigger",  // trigger for the calendar (button ID)
	                                align          :    "Tl",           // alignment (defaults to "Bl")
	                                singleClick    :    true
	                            });
	                        </script>    
						</ww:elseif>
						<ww:elseif test="#field.type == 'money' || #field.type == 'string' || #field.type == 'float' || #field.type == 'long' || #field.type == 'double' || #field.type == 'integer'">
							<input type="text" name="<ww:property value="#name"/>" id="<ww:property value="#id"/>" size="10" maxlength="50" class="txt"/>
						</ww:elseif>
					</ww:else>
                </ww:elseif>
            </td>   
        </ww:else>
    </ww:iterator>
    </tr>
	<ww:if test="periodicalCapable">
	<tr>
		<td>
			<ds:lang text="RaportOkresowy"/>
		</td>
		<td>
			<ww:checkbox name="'periodical'" fieldValue="'true'"/>
		</td>
	</tr>
	<tr>
		<td>
			<ds:lang text="Okres"/>
		</td>
		<td>
			<ww:textfield name="'interval'" size="3"></ww:textfield><ds:lang text="dni"/>
		</td>
	</tr>
	</ww:if>
    </table>
    
    <ds:event value="getText('ZamowRaport')" name="'doRegisterReport'" disabled="false" onclick="'if (!validate()) return false;'"/>	
    <input type="button" class="btn" value="<ds:lang text="PrzejdzDoSwoichRaportow"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/reports/list.action';"/>
</form>

<script type="text/javascript">

	function changeDisplay(name,idname)
	{
	   var element = document.getElementById(idname);
	   var element2 = document.getElementById('in'+idname);
	   if(element.style.display == 'none')
	   {
	    element.style.display = '';
	    element2.style.display = 'none';
	    document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/dol.gif';								
		}
		else
		{
			element.style.display = 'none';
	        element2.style.display = '';
	        document.getElementById('img'+idname).src = '<c:out value='${pageContext.request.contextPath}'/>/img/gora.gif';
		}						
	}
	
    function emptySearchField(field)
    {
        if (field.tagName.toUpperCase() == 'SELECT' && field.multiple)
        {
            return field.options.selectedIndex < 0;
        }
        else
            return emptyField(field);
    }

    function validate()
    {
        var field,field2;
        <ww:iterator value="kind.getReport(reportCn).criteria.values()">
            <ww:if test="required">
                <ww:if test="type == 'standard'">
                    <ww:if test="fieldCn == 'date'">
                        field = document.getElementById('dateFrom');
                        field2 = document.getElementById('dateTo');
                        if (emptySearchField(field) || emptySearchField(field2))
                            return alertError('<ww:property value="name"/>');
                    </ww:if>
                    <ww:else>
                        field = document.getElementById('<ww:property value="fieldCn"/>');
                        if (emptySearchField(field))
                            return alertError('<ww:property value="name"/>');
                    </ww:else>
                </ww:if>
                <ww:else>
                    field = document.getElementById('dockind_<ww:property value="fieldCn"/>');
                    if (emptySearchField(field))
                        return alertError('<ww:property value="name"/>');
                </ww:else>
            </ww:if>
        </ww:iterator>
        <ww:iterator value="params">
            <ww:if test="required">
                var jqField_<ww:property value="fieldCn"/> = $j('#<ww:property value="fieldCn"/>');
                if(jqField_<ww:property value="fieldCn"/>.val() == null
                    || (jqField_<ww:property value="fieldCn"/>.val().trim &&
                        jqField_<ww:property value="fieldCn"/>.val().trim().length == 0)){
                    return alertError('<ww:property value="label"/>');
                }
            </ww:if>
        </ww:iterator>
        return true;
    }
    
    function alertError(fieldName)
    {
        alert('Pole '+fieldName+' jest obowiązkowe!');
        return false;    
    }
</script>
