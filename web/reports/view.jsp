<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N view.jsp N-->

<%@ page import="java.io.*"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jstl/xml" prefix="x" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ww:property value="report.title"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-messages/>
<ds:ww-action-errors/>

<ww:if test="report.ctime != null">
    <ww:set name="ctime" value="report.ctime" scope="page" />
</ww:if>

<ww:if test="!report.done">
    <p><ds:lang text="RaportNieZostalJeszczeUtworzony"/>.</p>
</ww:if>

<table>
    <tr>
        <td><ds:lang text="Tytul"/>:</td>
        <td><ww:property value="report.title"/></td>
    </tr>
    <tr>
        <td><ds:lang text="UtworzonyPrzez"/>:</td>
        <td><ww:property value="author"/></td>
    </tr>
    <tr>
        <td><ds:lang text="DataUtworzenia"/>:</td>
        <td><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/></td>
    </tr>
</table>

<ww:if test="report.done">

<ww:if test="pdfLink != null">
    <input type="button" class="btn" value="<ds:lang text="WersjaPDF"/>"
        onclick="javascript:void(window.open('<ww:url value='pdfLink'/>&amp;', null, 'width=750,height=450,menubar=no,toolbar=no,status=no,location=no, resizable=yes'));" />
</ww:if>
<ww:if test="csvLink != null">
    <input type="button" class="btn" value="<ds:lang text="WersjaCSV"/>"
        onclick="document.location.href='<ww:url value="csvLink"/>';" />
</ww:if>
<%--
<input type="button" class="btn" value="Poka� raport"
    onclick="javascript:document.location.href = '<ww:url><ww:param name="'show'" value="true"/></ww:url>';"/>
--%>

    <ww:property value="html" escape="false"/>
</ww:if>

