<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N not-ready.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="Raport"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<p><ds:lang text="RaportNieJestJeszczeDostepny.PoWygenerowaniuRaportuBedzieMoznaGoObejrzecNa"/>
	 <a href="<ww:url value="'/reports/list.action'"/>"><ds:lang text="TejStronie"/></a>.</p>

<input type="button" value="<ds:lang text="ListaRaportow"/>" class="btn"
    onclick="document.location.href = '<ww:url value="'/reports/list.action'"/>';"/>
