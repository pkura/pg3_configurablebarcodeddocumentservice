<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dockind-report.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ww:property value="kind.getReport(reportCn).name"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/dockind/dockind-report.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'documentKindCn'" value="documentKindCn"/>
    <ww:hidden name="'reportCn'" value="reportCn"/>
    
    <table>
    <ww:if test = "reportCn=='raport1' && documentKindCn =='ald'">
    <tr>
            <td><ds:lang text="LiczbaDniOdWplyniecia"/></td>
            <td><ww:textfield name="'days'" id="days" size="3" maxlength="3" cssClass="'txt'"/></td>
    </tr>
    </ww:if>

    <ww:iterator value="kind.getReport(reportCn).criteria.values()">
        <tr>
            <td><ww:if test = "!(fieldCn == 'diff' || fieldCn == 'diffUser' || fieldCn == 'officeNumber')"><ww:property value="name"/><ww:if test="required"><span class="star">*</span></ww:if>:</ww:if></td>                
            <td>
                <ww:if test="type == 'standard'">
                    <ww:if test="fieldCn == 'date'">            
                        <ds:lang text="od"/>
                        <ww:textfield name="'dateFrom'" id="dateFrom" size="10" maxlength="10" cssClass="'txt'"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_dateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                        <ds:lang text="do"/>
                        <ww:textfield name="'dateTo'" id="dateTo" size="10" maxlength="10" cssClass="'txt'"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_dateTo_trigger" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                            
                        <script type="text/javascript">
                	
	                	if($j('#dateTo').length > 0)
	                	{
	                            Calendar.setup({
	                                inputField     :    "dateFrom",     // id of the input field
	                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	                                button         :    "calendar_dateFrom_trigger",  // trigger for the calendar (button ID)
	                                align          :    "Tl",           // alignment (defaults to "Bl")
	                                singleClick    :    true
	                            });
	                	}
	                	if($j('#dateTo').length > 0)
	                	{
	                            Calendar.setup({
	                                inputField     :    "dateTo",     // id of the input field
	                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	                                button         :    "calendar_dateTo_trigger",  // trigger for the calendar (button ID)
	                                align          :    "Tl",           // alignment (defaults to "Bl")
	                                singleClick    :    true
	                            });
	                	}
                        </script>    
                                         
                    </ww:if>
					<ww:elseif test="fieldCn == 'invoiceDate'">            
                        <ds:lang text="od"/>
                        <ww:textfield name="'invoiceDateFrom'" id="invoiceDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_invoiceDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                        <ds:lang text="do"/>
                        <ww:textfield name="'invoiceDateTo'" id="invoiceDateTo" size="10" maxlength="10" cssClass="'txt'"/>
                        <img src="<ww:url value="'/calendar096/img.gif'"/>"
                            id="calendar_invoiceDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
                            title="Date selector" onmouseover="this.style.background='red';"
                            onmouseout="this.style.background=''"/>
                            
                        <script type="text/javascript">
                            Calendar.setup({
                                inputField     :    "invoiceDateFrom",     // id of the input field
                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                button         :    "calendar_invoiceDateFrom_trigger",  // trigger for the calendar (button ID)
                                align          :    "Tl",           // alignment (defaults to "Bl")
                                singleClick    :    true
                            });
                            Calendar.setup({
                                inputField     :    "invoiceDateTo",     // id of the input field
                                ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                                button         :    "calendar_invoiceDateTo_trigger",  // trigger for the calendar (button ID)
                                align          :    "Tl",           // alignment (defaults to "Bl")
                                singleClick    :    true
                            });
                        </script>    
                                         
                    </ww:elseif>
                    <ww:elseif test="fieldCn == 'division'">

						<select class="multi_sel" size="5" id="division" name="divisionGuid" multiple="multiple">                    
                            <ww:iterator value="divisions">
                            	<option value="<ww:property value="guid"/>">
                                    <ww:property value="name"/>
                                </option>
                            </ww:iterator>
                        </select>
                        <br/>
                        <a href="javascript:void(select(getElementByName('divisionGuid'), true))"><ds:lang text="zaznacz"/></a>
                        / <a href="javascript:void(select(getElementByName('divisionGuid'), false))"><ds:lang text="odznacz"/></a>
                        <ds:lang text="wszystkie"/>
                        
                    </ww:elseif>
                    <ww:elseif test="fieldCn == 'user'">
                        <select class="multi_sel" size="5" id="user" name="username" multiple="multiple">                    
                            <ww:iterator value="users">
                                <option value="<ww:property value="name"/>">
                                    <ww:property value="lastname+' '+firstname"/>
                                </option>
                            </ww:iterator>
                        </select>                    
                        <br/>
                        <a href="javascript:void(select(getElementByName('username'), true))"><ds:lang text="zaznacz"/></a>
                        / <a href="javascript:void(select(getElementByName('username'), false))"><ds:lang text="odznacz"/></a>
                        <ds:lang text="wszystkie"/> 
                    </ww:elseif>
                    	 
                    
                </ww:if>
                <ww:elseif test="type == 'dockind'">
                    <ww:set name="field" value="kind.getFieldByCn(fieldCn)"/>
                    <ww:set name="prefix" value="'dockind_'"/>
                    <ww:set name="tagName" value="'values.'+fieldCn"/>
                    <ww:set name="tagId" value="#prefix+fieldCn"/>
                    <ww:if test="#field.type == 'enum' || #field.type == 'enum-ref'">
                        <select class="multi_sel" size="5" id="<ww:property value="#tagId"/>" name="<ww:property value="#tagName"/>" multiple="multiple">                    
                            <ww:iterator value="#field.sortedEnumItems">
                                <option value="<ww:property value="id"/>" <ww:if test="values[fieldCn] == id">selected="selected"</ww:if>>
                                    <ww:property value="title"/>
                                </option>
                            </ww:iterator>
                        </select>                    
                        <br/>
                        <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), true))"><ds:lang text="zaznacz"/></a>
                        / <a href="javascript:void(select(getElementByName('<ww:property value="#tagName"/>'), false))"><ds:lang text="odznacz"/></a>
                        <ds:lang text="wszystkie"/>  
                    </ww:if>
                </ww:elseif>
            </td>   
        </tr>
    </ww:iterator>
    
    </table>
    
    <ds:event value="getText('GenerujRaport')" name="'doReport'" disabled="false" onclick="'if (!validate()) return false;'"/>
    
</form>

<script type="text/javascript">
    function emptySearchField(field)
    {
        if (field.tagName.toUpperCase() == 'SELECT' && field.multiple)
        {
            return field.options.selectedIndex < 0;
        }
        else
            return emptyField(field);
    }

    function validate()
    {
        var field,field2;
        <ww:iterator value="kind.getReport(reportCn).criteria.values()">
        <ww:if test="required">
            <ww:if test="type == 'standard'">
                <ww:if test="fieldCn == 'date'">
                    field = document.getElementById('dateFrom');
                    field2 = document.getElementById('dateTo');
                    if (emptySearchField(field) || emptySearchField(field2))
                        return alertError('<ww:property value="name"/>');
                </ww:if>
                <ww:else>
                    field = document.getElementById('<ww:property value="fieldCn"/>');
                    if (emptySearchField(field))
                        return alertError('<ww:property value="name"/>');
                </ww:else>
            </ww:if>
            <ww:else>
                field = document.getElementById('dockind_<ww:property value="fieldCn"/>');
                if (emptySearchField(field))
                    return alertError('<ww:property value="name"/>');
            </ww:else>
        </ww:if>
        </ww:iterator>
        return true;
    }
    
    function alertError(fieldName)
    {
        alert('Pole '+fieldName+' jest obowiązkowe!');
        return false;    
    }
</script>