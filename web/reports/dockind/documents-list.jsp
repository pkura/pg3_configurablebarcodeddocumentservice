<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N documents-list.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ww:property value="report.name"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-messages/>
<ds:ww-action-errors/>

<table>
    <tr>
        <td><b><ds:lang text="DataWygenerowania"/>:</b></td>
        <td><ww:property value="currentDate"/></td>
    </tr>
    <tr><td colspan="2"><ds:lang text="KryteriaWyboru"/>:</td></tr>
    <ww:iterator value="criteriaInfo">
    <tr>
        <td><b><ww:property value="key"/>:</b></td>
        <td><ww:property value="value"/></td>
    </tr>
    </ww:iterator>        
</table>

<table>
    <tr>
        <ww:iterator value="columns">
            <td><b><ww:property value="value"/></b></td>
        </ww:iterator>
    </tr>
    <ww:iterator value="documents">
        <ww:set name="result"/>
        <tr>
        <ww:iterator value="columns">
            <td align="center">
                <ww:if test="key == 'standard.attachment'">
                    <ww:if test="#result[key] != null">
                        <a href="<ww:url value="'/repository/view-attachment-revision.do?id='+#result[key].id"/>"><img
                                src="<ww:url value="'/img/pobierz.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg"
                        title="<ds:lang text="PobierzZalacznik"/>" /></a>
                        <ww:if test="@pl.compan.docusafe.web.viewserver.ViewServer@mimeAcceptable(#result[key].mime)">
                            &nbsp;<a href="javascript:openToolWindow('<ww:url value="'/viewserver/viewer.action'"><ww:param name="'id'" value="#result[key].id"/><ww:param name="'fax'" value="false"/><ww:param name="'width'" value="1000"/><ww:param name="'height'" value="750"/></ww:url>', 'vs',screen.width,screen.height);"><img
                                src="<ww:url value="'/img/wyswietl.gif'"/>" width="18"
                            height="18" class="zoomImg" class="zoomImg" title="<ds:lang text="WyswietlZalacznikWprzegladarce"/>" /></a>
                        </ww:if>                
                    </ww:if>
                </ww:if>
                <ww:elseif test="key == 'standard.description'">
                    <a href="<ww:url value="'/repository/edit-document.action?id='+#result['id']"/>"><ww:property value="#result[key]"/></a>
                </ww:elseif>
                <ww:else>
                    <ww:property value="#result[key]"/>
                </ww:else>                
            </td>
        </ww:iterator>
        </tr>
    </ww:iterator>
</table>