<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N jasper-reports.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="System Raportowy"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/jasper-reports.action'"/>" method="post">
    <table>
        <tr>
        <script type="text/javascript">
            var _ctimeFrom = "startDate";
            var _ctimeTo = "koniecDate";
        </script>
            <td class="alignTop">
                <ds:lang text="Data od"/>:
            </td>
            <td>
                <ww:textfield name="'startDate'" id="startDate" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_ctimeFrom,_ctimeTo,1)'"/>
                    <img src="<ww:url value="'/calendar096/img.gif'"/>"
                        id="calendar_ctimeFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                        title="Date selector" onmouseover="this.style.background='red';"
                        onmouseout="this.style.background=''"/>            </td>
        </tr>
        <tr>
            <td class="alignTop">
                <ds:lang text="Data do"/>:
            </td>
            <td>
                <ww:textfield name="'koniecDate'" id="koniecDate" size="10" maxlength="10" cssClass="'txt'" onchange="'checkDate(_ctimeFrom,_ctimeTo,1)'"/>
                    <img src="<ww:url value="'/calendar096/img.gif'"/>"
                        id="calendar_ctimeTo_trigger" style="cursor: pointer; border: 1px solid red;"
                        title="Date selector" onmouseover="this.style.background='red';"
                        onmouseout="this.style.background=''"/>           </td>
            <script type="text/javascript">
                Calendar.setup({
                    inputField     :    "startDate",     // id of the input field
                    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                    button         :    "calendar_ctimeFrom_trigger",  // trigger for the calendar (button ID)
                    align          :    "Tl",           // alignment (defaults to "Bl")
                    singleClick    :    true
                });
                Calendar.setup({
                    inputField     :    "koniecDate",     // id of the input field
                    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
                    button         :    "calendar_ctimeTo_trigger",  // trigger for the calendar (button ID)
                    align          :    "Tl",           // alignment (defaults to "Bl")
                    singleClick    :    true
                });
            </script>
        </tr>
        <tr>
            <td class="alignTop">
                <ds:lang text="Raport"/>:
            </td>
            <td>
                <ww:select name="'report'" list="listOfReport" id="report"
                    listKey="key" listValue="value" cssClass="'sel'"
                    value="report"/>            </td>
        </tr>
        <tr>
            <td class="alignTop">
                <ds:lang text="Format zwracany"/>:
            </td>
            <td>
                <ww:select name="'reportType'" list="listOfReportType" id="reportType"
                    listKey="key" listValue="value" cssClass="'sel'"
                    value="reportType"/>            </td>
        </tr>
        <ds:available test="jasper.select.user">
        <ww:if test="isSupervisor">
        <tr>
            <td class="alignTop">
                <ds:lang text="Uzytkownik"/>:
            </td>
            <td>
            <ww:select name="'user'" list="users" 
                                           listKey="name" 
                                           headerKey="''" 
                                           headerValue="'----'"  
                                           listValue="asFirstnameLastnameName()" 
                                           cssClass="'sel'" value="user"/>
           </td>
        </tr>
        </ww:if>
        </ds:available>
        <tr>
            <td></td>
            <td><input class="btn" type="button" name="doReport" value="Generuj raport" onclick="document.getElementById('downloadInfo').style.display='block'; sendEvent(this)"></td>
        </tr>
        <tr>
            <td></td>
            <td><div id="downloadInfo" style="display:none;"><img src="/reports/jasper/loading.gif" height=20px width=20px>Za chwil� rozpocznie si� pobieranie raportu...</div></td>
        </tr>
    </table>


</form>

