<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N list.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Raporty"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/list.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="search tableMargin table100p" cellpadding="0" cellspacing="0">
		<tr>
			<th></th>
			<th>
				<ds:lang text="Tytul"/>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<ds:lang text="DataUtworzenia"/>
			</th>
			<th class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></th>
			<th>
				<ds:lang text="Status"/>
			</th>
		</tr>
	
		<ww:iterator value="reports">
			<ww:set name="ctime" value="ctime" scope="page"/>
			<tr>
				<ww:if test="done">
					<td>
						<ww:checkbox name="'reportIds'" fieldValue="id"/>
					</td>
					<td>
						<a href="<ww:url value="'/reports/view.action?id='+id"/>">
							<ww:property value="title"/></a>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<a href="<ww:url value="'/reports/view.action?id='+id"/>">
							<fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/></a>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<ds:lang text="gotowy"/>
						<ww:if test="failed">(<ds:lang text="wystapilyBledy"/>)</ww:if>
					</td>
				</ww:if>
				<ww:else>
					<td></td>
					<td>
						<ww:property value="title"/>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<ds:lang text="wTrakcieTworzenia"/>
					</td>
				</ww:else>
			</tr>
		</ww:iterator>
		<ww:iterator value="newReports">
			<ww:set name="ctime" value="ctime" scope="page"/>
			<tr>
					<td>
						<ww:checkbox name="'newReportIds'" fieldValue="id"/>
					</td>
					
					<td>
						<a href="<ww:url value="'/reports/view-specified-report.action'">
							<ww:param name="'reportId'" value="id"/>
							</ww:url>"><ww:property value="title"/>
						</a>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<a href="<ww:url value="'/reports/view-specified-report.action'">
							<ww:param name="'reportId'" value="id"/>
							</ww:url>"><fmt:formatDate value="${ctime}" type="both" pattern="dd-MM-yyyy HH:mm"/>
						</a>
					</td>
					<td class="s" background="<ww:url value="'/img/pionowa-linia.gif'"/>"></td>
					<td>
						<a href="<ww:url value="'/reports/view-specified-report.action'">
							<ww:param name="'reportId'" value="id"/>
							</ww:url>"><ww:property value="getTextStatus()"/>
						</a>
					</td>
				
			</tr>
		</ww:iterator>
	</table>


	<ds:submit-event value="getText('UsunRaporty')" name="'doDelete'"/>
</form>
<!--N koniec list.jsp N-->