<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ds:lang text="Raport"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ds:xmlLink path="ViewSpecifiedReport"/>
	<br/>
<img src="<ww:url value="'/reports/report-chart.action'"><ww:param name="'reportId'" value="reportId"/></ww:url>"/>