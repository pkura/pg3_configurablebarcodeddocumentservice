<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1><ds:lang text="ReportTemplateToEmail"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/report-to-email.action'"/>" method="post">
<ww:hidden name="'templatesList'" id="templatesList"></ww:hidden>
<table border="0" cellspacing="5" cellpadding="0">
	<tr>
		<th><ds:lang text="Szablon"/></th>
		<th><ds:lang text="Godzina"/></th>
		<th><ds:lang text="Data"/></th>
		<th><ds:lang text="Dni"/></th>
		<th><ds:lang text="Uruchomiony"/></th>
	</tr>
	
	<ww:iterator value="templatesList" status="status">
		<tr>
			<td><input type="text" value="<ww:property value="reportName"/>" name="name" class="txt" readonly="readonly"/></td>
			<td>
				<ww:select id="hours" name="'hours'" cssClass="'sel'" list="avHours"/>:<ww:select id="minutes" name="'minutes'" cssClass="'sel'" list="avMinutes"/>
			</td>
			<td>
				<input type="text" id="<ww:property value="getRandomId()"/>_field" value="<ww:property value="getStringDate()"/>" name="date" class="txt" size="10"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>" id="<ww:property value="getRandomId()"/>_trigger" style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
				<script type="text/javascript">
				Calendar.setup({
					inputField	 :	"<ww:property value="getRandomId()"/>_field",	 // id of the input field
					ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
					button		 :	"<ww:property value="getRandomId()"/>_trigger",  // trigger for the calendar (button ID)
					align		  :	"Tl",		   // alignment (defaults to "Bl")
					singleClick	:	true
				});
				</script>
			</td>
			<td><input type="text" value="<ww:property value="reportDays"/>" name="days" class="txt" size="5"/></td>
			<td>
				<ww:select id="run" name="'run'" cssClass="'sel'" list="#{ 0: 'Nie', 1: 'Tak' }"/>
			</td>
		</tr>
	</ww:iterator>
	
	<tr>
		<td><input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" /></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
</table>
</form>