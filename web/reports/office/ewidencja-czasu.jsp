<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N in-documents.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.service.reports.InDocumentsReport"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="RejestrAktywnosci"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/ewidencja-czasu.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table>
    <tr>
        <td><ds:lang text="RodzajDokumentu"/>:</td>
        <td><ww:select name="'documentKind'" list="documentKinds"
            listKey="id" listValue="name" cssClass="'sel'"
            headerKey="''"/>
            <p></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <ds:submit-event value="getText('GenerujRaport')" name="'doReport'" disabled="!canCreateReports" />
        </td>
    </tr>
</table>

</form>
