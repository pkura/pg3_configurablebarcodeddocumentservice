<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N overdue-documents.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.service.reports.OverdueDocumentsReport"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="<%= OverdueDocumentsReport.TITLE %>"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/overdue-documents.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table>
    <tr>
        <td><ds:lang text="DataPrzyjecia"/>:</td>
        <td>
            <ds:lang text="od"/>
            <ww:textfield name="'incomingDateFrom'" id="incomingDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_incomingDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            <ds:lang text="do"/>
            <ww:textfield name="'incomingDateTo'" id="incomingDateTo" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_incomingDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
    	<td>
    		<ds:lang text="ZOstatnichDni"/>:
    	</td>
    	<td>
    		<ww:textfield name="'lastDays'" id="lastDays" size="10" maxlength="10" cssClass="'txt'"/>
    	</td>
    </tr>
    <tr>
        <td><span title="Dziennik, w kt�rym ostatnio przyj�to pismo"><ds:lang text="Dziennik"/></span>:</td>
        <td><ww:select id="'journalId'" name="'journalId'"  list="journals" cssClass="'sel'"
             headerKey="''" headerValue="getText('select.wszystkie')" onchange="'onJournalUpdate(this)'"/>
			 <script language="JavaScript" type="text/javascript">
		function compareOptionText(a,b) {
			if((a.text.charAt(0) == '(' && b.text.charAt(0) == '(')|| (a.text.charAt(0) != '(' && b.text.charAt(0) != '(')){
				return a.text!=b.text ? a.text<b.text ? -1 : 1 : 0;
			} else {
				return a.text.charAt(0) == '('? 1: -1;
			}
		}

		<%-- funkcja sortuje selecta, nie rusza pierwszego elementu --%>
		function sortOptions(list) {
			var items = list.options.length;
			var tmpArray = new Array(items - 1);
			for ( i=0; i<items - 1; i++ ) <%-- Nie sortuje pierwszego elementu ("wybierz") --%>
				tmpArray[i] = new
			Option(list.options[i+1].text,list.options[i+1].value);
			tmpArray.sort(compareOptionText);
			for ( i=0; i<items - 1; i++ )
				list.options[i+1] = new Option(tmpArray[i].text,tmpArray[i].value);
		}

			sortOptions(document.getElementById('journalId'));
	</script>
        </td>
    </tr>
    <tr>
        <td><span title="<ds:lang text="DzialNaKtoryOstatnioZadekretowanoPismo"/>"><ds:lang text="Dzial"/></span>:</td>
        <td>
            <input type="hidden" name="divisionGuid" id="divisionGuid" value="<ww:property value='divisionGuid'/>"/>

            <input type="text" size="50" maxlength="100" class="txt" readonly="true"
                id="divisionGuid.pretty" value="<ww:property value='divisionName'/>" />

            <input type="button" value="<ds:lang text="Wybierz"/>" class="btn" onclick="javascript:void(window.open('<ww:url value="'/office/pick-division.action'"/>?reference=divisionGuid', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"
                />
            <input type="button" value="<ds:lang text="Wyczysc"/>" class="btn" onclick="document.getElementById('divisionGuid').value=''; document.getElementById('divisionGuid.pretty').value=''"
                />
        </td>
    </tr>
    <tr>
        <td valign="top"><ds:lang text="RodzajPisma"/>:</td>
        <td><ww:select name="'kindIds'" list="kinds" size="5" multiple="true"
            listKey="id" listValue="name+' (dni: '+days+')'" cssClass="'sel'" />
            <p></p>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="OsobaPrzyjmujaca"/>:</td>
        <td>
            <ww:select name="'author'" cssClass="'sel combox-chosen'" list="users"
                listKey="name" listValue="lastname+' '+firstname"
                headerKey="''" headerValue="getText('select.wszystkie')"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="SposobDostarczenia"/>:</td>
        <td><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
            headerKey="''" headerValue="getText('select.wszystkie')"
            value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/></td>
    </tr>
    <tr>
        <td><ds:lang text="SposobOdbioru"/>:</td>
        <td><ww:select name="'outgoingDeliveryId'" list="outgoingDeliveries" listKey="id" listValue="name"
            headerKey="''" headerValue="getText('select.wszystkie')"
            value="#exists ? outgoingDelivery.id : outgoingDeliveryId" id="outgoingDelivery" cssClass="'sel'"/></td>
    </tr>
    <tr>
        <td><ds:lang text="Nadawca"/>:</td>
        <td><ww:textfield name="'sender'" id="sender" size="30" maxlength="50" cssClass="'txt'"/>
            <input type="button" value="<ds:lang text="WybierzNadawce"/>" onclick="openPersonPopup('sender', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
    </tr>
    <tr>
        <td><ds:lang text="Adresat"/>:</td>
        <td><ww:textfield name="'recipient'" id="recipient" size="30" maxlength="50" cssClass="'txt'"/>
            <input type="button" value="<ds:lang text="WybierzOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_RECIPIENT %>');" class="btn" ></td>
    </tr>
    <tr>
       	<td><ds:lang text="SzukanieNadawcyIAdresataZawezajace"/>:</td>
       	<td><ww:checkbox name="'forceAnd'" fieldValue="true" value="true"/></td>
    </tr>
    <tr>
        <td><ds:lang text="PismaZblizajaceSieDoPrzeterminowania"/>:</td>
        <td><ww:checkbox name="'useWarnDays'" fieldValue="true"/>
            <ds:lang text="DniDoPrzeterminowania"/>:<ww:textfield name="'warnDays'" cssClass="'txt'" size="2" maxlength="2" /></td>
    </tr>
    <tr>
        <td><ds:lang text="PismaJuzPrzeterminowane"/>:</td>
        <td><ww:checkbox name="'showOverdues'" fieldValue="true" value="true"/></td>
    </tr>
    <tr>
        <td><ds:lang text="KryteriumSortowania"/>:</td>
        <td><ww:select name="'sortField'" cssClass="'sel'" list="sortFields" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <ds:submit-event value="getText('GenerujRaport')" name="'doReport'" disabled="!canCreateReports" />
        </td>
    </tr>
    <tr>
	    <td colspan="2">
    	        <br/><br/><br/>    
        </td>
    </tr>
    
	<tr>
        <td>
        	<ww:textfield name="'reportTemplateName'" id="reportTemplateName" size="20" maxlength="20" cssClass="'txt'"/>
			&nbsp&nbsp&nbsp&nbsp
			<ds:submit-event value="getText('ZapiszRaport')" name="'doSaveReport'"/>
        <td/>
	</tr>
    <tr> 
        <td>
			<ds:lang text="NazwaRaportu"/>
	    <td/>
    </tr> 
</table>

</form>

<script type="text/javascript">
    Calendar.setup({
        inputField     :    "incomingDateFrom",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_incomingDateFrom_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
    Calendar.setup({
        inputField     :    "incomingDateTo",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_incomingDateTo_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });

    // lparam - identyfikator
    function openPersonPopup(lparam, dictionaryType)
    {
        if (lparam == 'sender')
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType);
        }
        else
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType);
        }
    }

    /*
        Funkcja wywo�ywana z okienka person.jsp po wybraniu osoby.
    */
    function __accept_person(map, lparam, wparam)
    {
        //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

        var txt;

        if (lparam == 'recipient')
        {
            txt = document.getElementById('recipient');
        }
        else if (lparam == 'sender')
        {
            txt = document.getElementById('sender');
        }

	    if (!isEmpty(map.lastname)) {
			if(!isEmpty(map.firstname)){
				txt.value = map.firstname + " " + map.lastname;
			} else {
				txt.value = map.lastname;
			}
        } else if (!isEmpty(map.organization)) {
            txt.value = map.organization;
        } else if (!isEmpty(map.firstname)) {
            txt.value = map.firstname;
		}
    }

    function pickDivision(reference, guid, prettyPath)
    {
        if (reference != null && guid != null)
        {
            document.getElementById(reference).value = guid;
            document.getElementById(reference+'.pretty').value = prettyPath;
            document.getElementById('journalId').value = '';
        }
    }

    function onJournalUpdate(sel)
    {
        if (!isEmpty(document.getElementById('journalId').value))
        {
            <ww:if test='canChooseDivision'>
            document.getElementById('divisionGuid').value = '';
            document.getElementById('divisionGuid.pretty').value = '';
            </ww:if>
        }
    }
</script>
