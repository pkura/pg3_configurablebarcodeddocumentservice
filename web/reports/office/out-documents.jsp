<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N out-documents.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.service.reports.OutDocumentsReport"%>
<%@ page import="pl.compan.docusafe.service.reports.IntDocumentsReport"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:if test="internal">
    <h1><ds:lang text="<%= IntDocumentsReport.TITLE %>"/></h1>
</ww:if>
<ww:else>
    <h1><ds:lang text="<%= OutDocumentsReport.TITLE %>"/></h1>
</ww:else>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/out-documents.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
<ww:hidden name="'internal'"/>

<table>
    <tr>
        <td><ds:lang text="Dzial"/><span class="star">*</span>:</td>
        <td><ww:select id="journalId" name="'journalId'" list="journals"
                headerKey="''" headerValue="getText('select.wybierz')"
                cssClass="'sel'" />
		<script language="JavaScript" type="text/javascript">
		function compareOptionText(a,b) {
			if((a.text.charAt(0) == '(' && b.text.charAt(0) == '(')|| (a.text.charAt(0) != '(' && b.text.charAt(0) != '(')){
				return a.text!=b.text ? a.text<b.text ? -1 : 1 : 0;
			} else {
				return a.text.charAt(0) == '('? 1: -1;
			}
		}

		<%-- funkcja sortuje selecta, nie rusza pierwszego elementu --%>
		function sortOptions(list) {
			var items = list.options.length;
			var tmpArray = new Array(items - 1);
			for ( i=0; i<items - 1; i++ ) <%-- Nie sortuje pierwszego elementu ("wybierz") --%>
				tmpArray[i] = new
			Option(list.options[i+1].text,list.options[i+1].value);
			tmpArray.sort(compareOptionText);
			for ( i=0; i<items - 1; i++ )
				list.options[i+1] = new Option(tmpArray[i].text,tmpArray[i].value);
		}

			sortOptions(document.getElementById('journalId'));
	</script>
</td>
    </tr>
    <tr>
        <td><ds:lang text="DataPrzyjeciaPisma"/><span class="star">*</span>:</td>
        <td>
            <ds:lang text="od"/>
            <ww:textfield name="'documentDateFrom'" id="documentDateFrom" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_documentDateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            <ds:lang text="do"/>
            <ww:textfield name="'documentDateTo'" id="documentDateTo" size="10" maxlength="10" cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_documentDateTo_trigger" style="cursor: pointer; border: 1px solid red;"
                title="Date selector" onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
        </td>
    </tr>
    <tr>
    	<td>
    		<ds:lang text="ZostatnichDni"/>:
    	</td>
    	<td>
    		<ww:textfield name="'lastDays'" id="lastDays" size="10" maxlength="10" cssClass="'txt'"/>
    	</td>
    </tr>
<ds:extras test="!business">
    <tr>
        <td><ds:lang text="OsobaPrzyjmujaca"/>:</td>
        <td>
            <ww:select name="'author'" cssClass="'sel combox-chosen'" list="users"
                listKey="name" listValue="firstname+' '+lastname"
                headerKey="''" headerValue="getText('select.dowolna')"/>
        </td>
    </tr>
</ds:extras>
<ds:available test="inDocumentFineReport.autor">
  <tr>
        <td><ds:lang text="OsobaPrzyjmujaca"/>:</td>
        <td>
            <ww:select name="'author'" cssClass="'sel combox-chosen'" list="users"
                listKey="name" listValue="firstname+' '+lastname"
                headerKey="''" headerValue="getText('select.dowolna')"/>
        </td>
  </tr>
</ds:available>
    <ww:if test="!internal">
        <tr>
            <td><ds:lang text="SposobDostarczenia"/>:</td>
            <td><ww:select name="'deliveryId'" list="deliveries" listKey="id" listValue="name"
                headerKey="''" headerValue="getText('select.dowolny')"
                value="#exists ? delivery.id : deliveryId" id="delivery" cssClass="'sel'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="Odbiorca"/>:</td>
            <td><ww:textfield name="'recipient'" id="recipient" size="30" maxlength="50" cssClass="'txt'"/>
                <input type="button" value="<ds:lang text="WybierzOdbiorce"/>" onclick="openPersonPopup('recipient', '<%= Person.DICTIONARY_SENDER %>');" class="btn" ></td>
        </tr>
        <tr>
        	<td><ds:lang text="SzukanieOdbiorcyZawezajace"/>:</td>
        	<td><ww:checkbox name="'forceAnd'" fieldValue="true" value="true"/></td>
    	</tr>
    </ww:if>
    <tr>
        <td colspan="2">
            <ds:submit-event value="getText('GenerujRaport')" name="'doReport'" disabled="!canCreateReports"/>
        </td>
    </tr>
	<tr>
	    <td colspan="2">
    	        <br/><br/><br/>    
        </td>
    </tr>
    
	<tr>
        <td>
        	<ww:textfield name="'reportTemplateName'" id="reportTemplateName" size="20" maxlength="20" cssClass="'txt'"/>
			&nbsp&nbsp&nbsp&nbsp
			<ds:submit-event value="getText('ZapiszRaport')" name="'doSaveReport'"/>
        <td/>
	</tr>
    <tr> 
        <td>
			<ds:lang text="NazwaRaportu"/>
	    <td/>
    </tr> 
</table>

</form>

<script type="text/javascript">

	if($j('#documentDateFrom').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "documentDateFrom",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_documentDateFrom_trigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}
	
	if($j('#documentDateTo').length > 0)
	{
	    Calendar.setup({
	        inputField     :    "documentDateTo",     // id of the input field
	        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	        button         :    "calendar_documentDateTo_trigger",  // trigger for the calendar (button ID)
	        align          :    "Tl",           // alignment (defaults to "Bl")
	        singleClick    :    true
	    });
	}

    // lparam - identyfikator
    function openPersonPopup(lparam, dictionaryType)
    {
        if (lparam == 'sender')
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType);
        }
        else
        {
            openToolWindow('<ww:url value="'/office/common/person.action'"/>?lparam='+lparam+'&dictionaryType='+dictionaryType);
        }
    }

    /*
        Funkcja wywoływana z okienka person.jsp po wybraniu osoby.
    */
    function __accept_person(map, lparam, wparam)
    {
        //alert('accept_person map='+map+', lparam='+lparam+', wparam='+wparam);

        var txt;

        if (lparam == 'recipient')
        {
            txt = document.getElementById('recipient');
        }
        else if (lparam == 'sender')
        {
            txt = document.getElementById('sender');
        }
		
		if (!isEmpty(map.lastname)) {
			if(!isEmpty(map.firstname)){
				txt.value = map.firstname + " " + map.lastname;
			} else {
				txt.value = map.lastname;
			}
        } else if (!isEmpty(map.organization)) {
            txt.value = map.organization;
        } else if (!isEmpty(map.firstname)) {
            txt.value = map.firstname;
		}
    }

</script>
