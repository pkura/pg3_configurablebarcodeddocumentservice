<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N templates.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:if test="templateDeleted || showTemplate">
	<h1><ds:lang text="Szablon"/> <ww:property value="templateName"/></h1>	
</ww:if> 
<ww:else>
	<h1><ds:lang text="SzablonyRaportow"/></h1>
</ww:else>
 
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/templates-report.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
    
	<table>
		<ww:if test="showTemplate">
			<th>
				<ds:lang text="RodzajAtrybutu"/>
				&nbsp&nbsp&nbsp&nbsp
			</th>
			<th>
				<ds:lang text="WartoscAtrybutu"/>
				&nbsp&nbsp&nbsp&nbsp
			</th>
			<tr>
				<td colspan="2">
					<br/><br/>
				</td>
			</tr>
			<ww:if test="incomingDateFrom!=null">
				<tr>
					<td>
						<ds:lang text="PoczatkowaData"/>:
					</td>
					<td>
						<ww:property value="incomingDateFrom"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="incomingDateTo!=null">
				<tr>
					<td>
						<ds:lang text="KoncowaData"/>:
					</td>
					<td>
						<ww:property value="incomingDateTo"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="lastDays!=null">
				<tr>
					<td>
						<ds:lang text="ZostatnichDni"/>:
					</td>
					<td>
						<ww:property value="lastDays"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="division!=null">
				<tr>
					<td>
						<ds:lang text="Dzial"/>:
					</td>
					<td>
						<ww:property value="division"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="journal!=null">
				<tr>
					<td>
						<ds:lang text="Dziennik"/>:
					</td>
					<td>
						<ww:property value="journal"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="sender!=null">
				<tr>
					<td>
						<ds:lang text="Nadawca"/>;
					</td>
					<td>
						<ww:property value="sender"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="recipient!=null">
				<tr>
					<td>
						<ds:lang text="Odbiorca"/>:
					</td>
					<td>
						<ww:property value="recipient"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="author!=null">
				<tr>
					<td>
						<ds:lang text="Autor"/>
					</td>
					<td>
						<ww:property value="author"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="warnDays!=null">
				<tr>
					<td>
						<ds:lang text="IloscDniDoPrzeterminowaniaPisma"/>:
					</td>
					<td>
						<ww:property value="warnDays"/>
					</td>
				</tr>
			</ww:if>
			<ww:if test="showOverdues">
				<tr>
					<td>
						<ds:lang text="PismaPrzeterminowaneBedaPokazywane"/>
					</td>
				</tr>
			</ww:if>
		</ww:if>
		<ww:if test="!templateDeleted && !showTemplate">
			<th>
				<ds:lang text="NazwaRaportu"/> 
				&nbsp&nbsp&nbsp&nbsp
			</th>
			<th>
				<ds:lang text="DataUtworzenia"/>
				&nbsp&nbsp&nbsp&nbsp
			</th>
			<th>
				<ds:lang text="TypRaportu"/>
			</th>
			<tr>
				<td colspan="2">
					<br/><br/>
				</td>
			</tr>
			<ww:iterator value="templates">
				<tr>
					<td>
						<a href="templates-report.action?action=show&templateName=<ww:property value="name"/>">
							<ww:property value="name"/>
						</a>
					</td>
					<td>
						<ww:property value="createDate"/>
					</td>
					<td>
						<ww:property value="type"/>
					</td>
					<td>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp					
						<a href="templates-report.action?action=delete&templateName=<ww:property value="name"/>">
							<ds:lang text="Usun"/>
						</a>
					</td>
					<td>
						&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp					
						<a href="templates-report.action?action=generate&templateName=<ww:property value="name"/>">
							<ds:lang text="Generuj"/>
						</a>
					</td>		
				</tr>
			</ww:iterator>
		</ww:if>
		<ww:else>
			<tr>
				<td>
					<br/	>
					<a href="templates-report.action">
						<ds:lang text="PowrotNaListeSzablonow"/>
					</a>
				</td>
			</tr>
		</ww:else>
	</table>
	<br/><br/>
	<ds:available test="templetes.report.auto">
	<table>
	<tr>
		<th><ds:lang text="AutomatycznyRaport"/></th>
		<th></th>
		<th></th>
	</tr>
	<tr>
		<th><ds:lang text="Nazwa"/></th>
		<th><ds:lang text="Uruchomiono"/></th>
		<th><ds:lang text="LiczbaDniR"/></th>
	</tr>
	<tr>	
		<ww:iterator value="templates">
			<td><input type="text" value="<ww:property value="name"/>" name="nazwa" class="txt" readonly="readonly"/></td>
			<td><ww:select id="run" name="'run'" cssClass="'sel'" list="#{ 0: 'Nie', 1: 'Tak' }"/></td>
			<td><input type="text" name="liczbaDni" class="txt" size="5"/></td>
		</ww:iterator>
	</tr>
	<tr>
		<td><input type="submit" name="doAutoRaportSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" /></td>
		<td></td>
		<td></td>
	</tr>	
	</table>
	</ds:available>
</form>