<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.service.reports.ProcessReport"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<script type="text/javascript" src="<ww:url value="'/jquery-1.7.1.min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/jquery.chained.js'" />"></script>

<h1><ds:lang text="<%= ProcessReport.TITLE %>"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/reports/office/process-report.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table>

    <tr>
        <td><ds:lang text="prfnDocumentId"/>:</td>
        <td><ww:textfield name="'documentId'" cssClass="'txt'" size="10"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:lang text="prfnDocumentIdInfo"/></td>
    </tr>


    <tr>
        <td><ds:lang text="prfnDivision"/>:</td>
        <td><ww:select
            name="'division'"
            id="division"
            list="divisionMap"
            listKey="key"
            listValue="value"
            headerKey="''"
            headerValue="getText('dowolny')"
            cssClass="'sel combox-chosen'"/></td>
    </tr>


    <tr>
        <td><ds:lang text="prfnUser"/>:</td>
        <td><ww:select
            name="'user'"
            id="user"
            list="userMap"
            listKey="key"
            listValue="value"
            headerKey="''"
            headerValue="getText('dowolny')"
            cssClass="'sel combox-chosen'"/></td>
    </tr>


    <tr>
        <td><ds:lang text="prfnInterval"/></td>
        <td><ds:lang text="prfnFrom"/>:


            <ww:textfield
            name="'intervalFrom'"
            id="intervalFrom"
            size="10"
            maxlength="10"
            cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_intervalFrom_trigger"
                style="cursor: pointer; border: 1px solid red;"
                title="Date selector"
                onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            (<ds:lang text="prfnDateFormat"/>)
        </td>
    </tr>
    <tr>
        <td></td>
        <td><ds:lang text="prfnTo"/>:
            <ww:textfield
                name="'intervalTo'"
                id="intervalTo"
                size="10"
                maxlength="10"
                cssClass="'txt'"/>
            <img src="<ww:url value="'/calendar096/img.gif'"/>"
                id="calendar_intervalTo_trigger"
                style="cursor: pointer; border: 1px solid red;"
                title="Date selector"
                onmouseover="this.style.background='red';"
                onmouseout="this.style.background=''"/>
            (<ds:lang text="prfnDateFormat"/>)
            , <ds:lang text="prfnOnlyNotCompleted"/>:<ww:checkbox name="'onlyNotCompleted'" fieldValue="true" value="false"/>
        </td>
    </tr>
    <tr>
        <td></td>
        <td><ds:lang text="prfnIntervalsInfo1"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:lang text="prfnIntervalsInfo2"/></td>
    </tr>


    <tr>
        <td><ds:lang text="prfnStatus"/>:</td>
        <td><ds:lang text="prfnDocumentKind"/>:
            <ww:select
                name="'documentKind'"
                id="documentKind"
                list="documentKindMap"
                listKey="Key"
                listValue="Value"
                headerKey="''"
                headerValue="getText('dowolny')"
                cssClass="'sel'" />
        </td>
    </tr>


    <tr>
        <td></td>
        <td><select
                name="status"
                id="status"
                class="multi_sel"
                multiple="multiple">
                <ww:iterator value="statusList">
					<option value=<ww:property value="id"/> class=<ww:property value="refValue"/>><ww:property value="title"/></option>
                </ww:iterator>
            </select>
            <script charset="utf-8" type="text/javascript">
                $(function() {
                    $("#status").chained("#documentKind");
                });
            </script>
        </td>
    </tr>


    <tr>
        <td><ds:lang text="prfnDuration"/>:</td>
        <td><ww:textfield name="'durationDays'"/><ds:lang text="prfnDays"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ww:textfield name="'durationHours'"/><ds:lang text="prfnHours"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ww:textfield name="'durationMinutes'"/><ds:lang text="prfnMinutes"/></td>
    </tr>


    <tr>
        <td><ds:lang text="prfnDelay"/>:</td>
        <td><ww:textfield name="'delayDays'"/><ds:lang text="prfnDays"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ww:textfield name="'delayHours'"/><ds:lang text="prfnHours"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ww:textfield name="'delayMinutes'"/><ds:lang text="prfnMinutes"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:lang text="prfnOnlyDelayed"/>:<ww:checkbox name="'onlyDelayed'" fieldValue="true" value="false"/></td>
    </tr>


    <tr>
        <td colspan="2">
            <ds:submit-event value="getText('GenerujRaport')" name="'doReport'" disabled="!canCreateReports" />
        </td>
    </tr>
</table>

</form>

<script type="text/javascript">
    Calendar.setup({
        inputField     :    "intervalFrom",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_intervalFrom_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
    Calendar.setup({
        inputField     :    "intervalTo",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_intervalTo_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });
</script>
