<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N dockind-report.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1><ww:property value="title"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ww:if test="chartEnabled">
	<ds:xmlLink path="ViewSpecifiedReport"/>
	<br/>
</ww:if>
	<!-- <b><ww:property value="title"/></b> -->
	<br/>
	<ww:property value="description"/>
	<br/>

    <table>
    <tr>
		<th><ds:lang text="OsobaZamawiajaca"/></th>
		<td><ww:property value="orderUser"/></td>
	</tr>
	<tr>
		<th><ds:lang text="Status"/></th>
		<td><ww:property value="status"/></td>
	</tr>
	<tr>
		<th><ds:lang text="Klucz"/></th>
		<td><ww:property value="remoteKey"/></td>
	</tr>
	<tr>
		<br/>
		<td colspan="2"><ds:lang text="Parametry"/></td>
	</tr>
    <ww:iterator value="printableParams">
		<tr>
			<th><ww:property value="key"/></th>
			<td><ww:property value="value"/></td>
		</tr>
    </ww:iterator>

    </table>
	<br/>

	<h4><ds:lang text="Pliki"/>:</h4>
	<br/>
	<ww:set name="reportChart" value="0"/>
	<ww:iterator value="files">
		<ww:set name="result"/>
		<ww:if test="!#result.equals('raport.jpg')">
			<img src="<ww:url value="'/img/report.gif'"/>" width="32" height="32"/>
			<a href="<ww:url value="'/reports/view-specified-report.action'">
				<ww:param name="'doGetFile'" value="'true'"/>
				<ww:param name="'fileName'" value="#result"/>
				<ww:param name="'reportId'" value="reportId"/>
				</ww:url>"><ww:property value="#result"/>
			</a>
			<br/>
		</ww:if>
	</ww:iterator>
	<br />
	<br />
    <input type="button" class="btn" value="<ds:lang text="PrzejdzDoSwoichRaportow"/>" onclick="document.location.href='<c:out value="${pageContext.request.contextPath}"/>/reports/list.action';"/>
	<br />