<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="RaportZObslugiUmow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p><!DOCTYPE html>

<ds:available test="!layout2">
    <ww:iterator value="tabs" status="status" >
        <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
        <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
    </ww:iterator>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id ="form" action="<ww:url value='baseLink'/>" method="post"
      onsubmit="disableFormSubmits(this);" style="float:left">
    <input type="hidden" name="doReport" id="doReport"/>
    <table>
        <tr>
            <td><ds:lang text="DataOd"/>:</td>
        <td>
            <input name="dateFrom" size="10" maxlength="10" class="txt" id="dateFrom"
                   value="<ds:format-date value="dtDateFrom" pattern="dd-MM-yyyy"/>"/>
        </td>
        <td><ds:lang text="DataDo"/>:</td>
        <td> 
            <input name="dateTo" size="10" maxlength="10" class="txt" id="dateTo"
                   value="<ds:format-date value="dtDateTo" pattern="dd-MM-yyyy"/>"/>
        </td>
        </tr>
        <tr>
            <td>
                 <input type="button" name="saveButton" value="<ds:lang text="GenerujRaport"/>" class="btn"
					   onclick="if(!validateForm())return false;$j('#doReport').attr('value',true);$j('#form').submit()" />

            </td>
        </tr>
    </table>  
</form>

<script type="text/javascript">
    $j(function(){
        $dateFrom = $j('#dateFrom');
        $dateTo = $j('#dateTo');
    	if($j('#dateFrom').length > 0)
    	{
	        Calendar.setup({
	            inputField:	"dateFrom",	 // id of the input field
	            ifFormat  :	"<%= DateUtils.jsCalendarDateFormat%>",	  // format of the input field
	            button    :	"dateFromTrigger",  // trigger for the calendar (button ID)
	            align     :	"Tl",		   // alignment (defaults to "Bl")
	            singleClick	: true
	        });
    	}
    	if($j('#dateTo').length > 0)
    	{
	        Calendar.setup({
	            inputField	:"dateTo",	 // id of the input field
	            ifFormat	:"<%= DateUtils.jsCalendarDateFormat%>",	  // format of the input field
	            button      :"dateToTrigger",  // trigger for the calendar (button ID)
	            align	: "Tl",		   // alignment (defaults to "Bl")
	            singleClick	: true
	        });
    	}
    });
    
    function validateForm(){
        $alert = '';
        $ok = true;
      
//        if($dateFrom.val() == '' || $dateFrom.val() == null ){
//            $alert +='\n*<ds:lang text="NiepoprawnaDataOd"/>\n';
//            $ok = false;
//        }
//        
//        if($dateTo.val() == '' || $dateTo.val() == null ){
//            $alert +='\n*<ds:lang text="NiepoprawnaDataDo"/>\n';
//            $ok = false;
//        }
//        
//        if(createDateObject($dateTo.val()) < createDateObject($dateFrom.val())){
//                $alert +='\n*<ds:lang text="DataOdMusiBycWieksza"/>\n';
//                $ok = false;
//        }
//        
//        if(!$ok)
//            alert($alert);
        
        return $ok;
        
    }
    function createDateObject(string){
        try{
            $dateArray = string.split('-', 3);
            return new Date($dateArray[2], $dateArray[1], $dateArray[0]);
        }catch(e){
            return new Date();
        }
    }
</script>
