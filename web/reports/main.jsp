<%@ page import="pl.compan.docusafe.service.reports.*"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Raporty"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
</p>

<p>
	<a href="<ww:url value="'/reports/list.action'"/>">
		<ds:lang text="TwojeRaporty"/></a>
</p>

<hr class="shortLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="jasperreports.on">
    <p>
        <a href="<ww:url value="'/reports/jasper-reports.action'"/>">
            <ds:lang text="System Raportowy"/></a>
        <br>
        <ds:lang text="Raporty z Systemu Raportowgeo"/>.
    </p>
</ds:available>

<ww:iterator value="dockindReports">
	<p>
		<a href="<ww:url value="'/reports/dockind/dockind-report.action?reportCn='+cn+'&documentKindCn='+dockindCn"/>">
			<ww:property value="name"/></a>
		<br>
		<ww:property value="description"/>.
	</p>
</ww:iterator>

<ww:iterator value="newReports">
	<p>
		<a href="<ww:url value="'/reports/specify-report-params.action?reportCn='+cn"/>">
			<ww:property value="name"/></a>
		<br>
		<ww:property value="description"/>.
	</p>
</ww:iterator>

<ds:available test="standard.reports">
	 <ww:if test="UTPReport">
    	 <ds:available test="reports.wlacz.UTPReport">
    		 <p>
      	  <a href="<ww:url value="'/reports/office/in-documentsUTP-fine.action'"/>">
            <ds:lang text="<%= InDocumentsFineReportUTP.TITLE %>"/></a>
      	  <br>
       	 <ds:lang text="ZestawienieSzczegolowePismPrzychodzacych"/>.
   		</p>	
   		 </ds:available>
    </ww:if>
    <ww:else>
   		 <p>
    	    <a href="<ww:url value="'/reports/office/in-documents-fine.action'"/>">
            <ds:lang text="<%= InDocumentsFineReport.TITLE %>"/></a>
     	   <br>
     	   <ds:lang text="ZestawienieSzczegolowePismPrzychodzacych"/>.
   		 </p>	
    </ww:else>
    <p>
        <a href="<ww:url value="'/reports/office/int-documents-fine.action'"/>">
            <ds:lang text="<%= IntDocumentsFineReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawienieSzczegolowePismWewnetrznych"/>.
    </p>
    <p>
        <a href="<ww:url value="'/reports/office/out-documents-fine.action'"/>">
            <ds:lang text="<%= OutDocumentsFineReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawienieSzczegolowePismWychodzacych"/>.
    </p>
    <p>
        <a href="<ww:url value="'/reports/office/in-documents.action'"/>">
            <ds:lang text="<%= InDocumentsReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawienieZbiorczePismPrzychodzacych"/>.
    </p>
    <p>
        <a href="<ww:url value="'/reports/office/out-documents.action'"><ww:param name="'internal'" value="true"/></ww:url>">
            <ds:lang text="<%= IntDocumentsReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawienieZbiorczePismWewnetrznych"/>.
    </p>

    <p>
        <a href="<ww:url value="'/reports/office/out-documents.action'"/>">
            <ds:lang text="<%= OutDocumentsReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawienieZbiorczePismWychodzacych"/>.
    </p>

    <ds:available test="reports.OverdueDocuments">
    <p>
        <a href="<ww:url value="'/reports/office/overdue-documents.action'"/>">
            <ds:lang text="<%= OverdueDocumentsReport.TITLE %>"/></a>
        <br>
        <ds:lang text="ZestawieniePismPrzeterminowanych"/>.
    </p>
    </ds:available>

    <ds:available test="reports.TemplatesReport">
    <p>
        <a href="<ww:url value="'/reports/office/templates-report.action'"/>">
            <ds:lang text="SzablonyRaportow"/></a>
        <br>
        <ds:lang text="ZestawienieSzablonowRaportow"/>.
    </p>
    </ds:available>

    <ds:available test="reports.PaaTaskReport">
        <p>
            <a href="<ww:url value="'/reports/office/paatask-documents.action'"/>">
                <ds:lang text="<%= PaaTaskReport.TITLE %>"/></a>
            <br>
            <ds:lang text="Raportzadan"/>.
            </p>
     </ds:available>

    <ds:available test="reports.ProcessReport">
        <p>
            <a href="<ww:url value="'/reports/office/process-report.action'"/>">
                <ds:lang text="<%= ProcessReport.TITLE %>"/></a>
            <br>
            <ds:lang text="ProcessReportDescription"/>.
            </p>
     </ds:available>

    <ds:available test="reports.email">
        <p>
            <a href="<ww:url value="'/reports/office/report-to-email.action'"/>">
                <ds:lang text="GenerowanieRaportuNaEmail"/></a>
            <br>
            <ds:lang text="GenerowanieRaportuNaEmailOpis"/>.
        </p>
    </ds:available>

    <ds:available test="reports.ewidencjaCzasu">
        <p>
            <a href="<ww:url value="'/reports/office/ewidencja-czasu.action'"/>">
                <ds:lang text="RejestrAktywnosci"/></a>
            <br>
            <ds:lang text="ZestawienieAktywnosciObslugiKorespondencji"/>.
        </p>
    </ds:available>
</ds:available>
<!--N koniec main.jsp N-->