function E(id)
{
    return document.getElementById(id);
}
    var tableId;
    var startCheck = -1;
    var endCheck = -1;
    var startUnCheck = -1;
    var endUnCheck = -1;

    function prepareCheckboxes()
    {
        if(!tableId)
            return;
        var children = document.getElementById(tableId).getElementsByTagName('*');
        var len = children.length;
        var count=0;
        for(i=0;i<len;i++)
            if(children[i].id==prefix)
            {
                count++;
                children[i].id = prefix+count;
                children[i].onmousedown = function(){mouseDownOnCheckbox(event)};
            }
    }

    function mouseDownOnCheckbox(e)
    {
        var evtobj=window.event? event : e;
        var t = (evtobj.target) ? evtobj.target : evtobj.srcElement;

        var id = t.id.substring(prefix.length);
        if(!evtobj.ctrlKey)
        {
            startCheck = -1;
            startUnCheck = -1;
            return;
        }
        if(!t.checked){

            if(startCheck==-1) {
                startCheck=id;
            }
            else {
                endCheck = id;
                setCheckboxes();
                t.checked=false;
            }
        }
        else{
            if(startUnCheck==-1) {
                startUnCheck=id;
            }
            else {
                endUnCheck = id;
                unsetCheckboxes();
                t.checked=true;
            }
        }
    }

    function setCheckboxes()
    {
        var i;
        var first = (startCheck>endCheck) ? endCheck:startCheck;
        var second = (startCheck<endCheck) ? endCheck:startCheck;
        for(i=first;i<=second;i++)
            document.getElementById(prefix+i).checked=true;
        startCheck = endCheck = -1;
    }

    function unsetCheckboxes()
    {
        var first = (startUnCheck>endUnCheck) ? endUnCheck:startUnCheck;
        var second = (startUnCheck<endUnCheck) ? endUnCheck:startUnCheck;
        var i;
        for(i=first;i<=second;i++)
            document.getElementById(prefix+i).checked=false;
        startUnCheck = endUnCheck = -1;
    }

    prepareCheckboxes();

/**
	przypisuje co drugiemu wierszowi w tabeli styl  
	oddRows generowanyw CssService
*/
function prepareTable(table, startOffset, endOffset )
{
    if(!table)return;
	var i,tr = table.getElementsByTagName("tr");
	for(i=startOffset;i<tr.length-endOffset;i++)
	{ 
            tr[i].onmouseover = highlightRow;
            tr[i].onmouseout = unhighlightRow;
		if((i%2)==0 && i>1)
                    {
			tr[i].className='oddRows';
                    }
	}
}

function prepareTableNoHighlight(table, startOffset, endOffset )
{
    if(!table)return;
	var i,tr = table.getElementsByTagName("tr");
	for(i=startOffset;i<tr.length-endOffset;i++)
	{ 
		if((i%2)==0 && i>1)
                    {
			tr[i].className='oddRows';
                    }
	}
}

function getParentTr(ctrl)
{
    while(true)
    {
        if(ctrl.nodeName=='TR')
            return ctrl;
        ctrl = ctrl.parentNode;
    }
}

function highlightRow(evnt)
{
    if(!evnt) var evnt = window.event;
    var target = (evnt.target)?evnt.target:evnt.srcElement;
    target = getParentTr(target);
    target.old = {style:target.className};
    getParentTr(target).className='highlightedRows';
}

function unhighlightRow(evnt)
{
    if(!evnt) var evnt = window.event;
    var target = (evnt.target)?evnt.target:evnt.srcElement;
    target = getParentTr(target);
    target.className=target.old.style;
}

function ensureElement(element, tagName)
{
    var tagUpper = (''+tagName).toUpperCase();
    if (element == null || !element.tagName ||
        element.tagName.toUpperCase() != tagUpper)
    {
      //  alert('expected a '+tagUpper+', got a '+(element != null ? typeof element : null));
        return false;
    }
    return true;
}

/**
    Przesuwa opcj? we wskazanym elemencie Select o podan? liczb? pozycji.
    TODO: wsparcie dla wielu element?w zaznaczonych jednocze?nie?

    @param select Element typu Select lub identyfikator elementu w elemencie (string)
    @param position Pozycja przesuwanego elementu lub -1, je?eli chodzi o aktualnie
        zaznaczony element.
    @param count Liczba pozycji, o jak? nale?y przesun?? element (liczba dodatnia
        dla przesuni?cia w d??, ujemna dla przesuni?cia w g?r?).
    @return Liczba pozycji, o jak? przesuni?to element.  0, je?eli z jakiego?
        powodu przesuni?cie nie by?o mo?liwe (b??d, lub element nie m?g? by?
        przesuni?ty dalej we wskazanym kierunku).
*/
function moveOption(select, position, count)
{
    var sel = typeof select == 'string' ? E(select) : select;

    if (!ensureElement(sel, 'select'))
        return 0;

    if (count == 0)
        return 0;

    if (position < 0)
    {
        position = sel.options.selectedIndex;
        if (position < 0)
            return 0;
    }

    if (position >= sel.options.length)
    {
        alert('moveOption: position invalid, position='+position+', options.length='+sel.options.length);
        return 0;
    }

    if ((position == sel.options.length-1 && count > 0) || (position == 0 && count < 0))
    {
        return 0;
    }


    // new index of a selected element
    var nidx = position + count;
    if (nidx < 0) nidx = 0;
    else if (nidx >= sel.options.length) nidx = sel.options.length - 1;

    var result = Math.abs(nidx - position);

    var tmp = sel.options[nidx];
    sel.options[nidx] = new Option(sel.options[position].text, sel.options[position].value, false, true);
    sel.options[position] = new Option(tmp.text, tmp.value);

    return result;
}

/**
    Przenosi wskazane opcje z listy pierwszej do drugiej.
    @param select1 Element typu Select lub jego identyfikator (string).
        Z tego elementu przenoszone b?d? opcje.
    @param select2 Element typu Select lub jego identyfikator (string).
        Do tego elementu przenoszone b?d? opcje.
    @param positions (opcjonalny) Tablica pozycji, kt?re maj? zosta?
        przeniesione, np. [1, 3, 5].  Je?eli parametr nie zostanie podany,
        przeniesione zostan? zaznaczone elementy.
    @return Liczba przeniesionych element?w.
*/
function moveOptions(select1, select2 /*, positions */)
{
    var positions = null; // tablica pozycji, kt?re nale?y przesun?? z select1 do select2
    if (arguments.length > 2)
        positions = arguments[2];
    //alert('positions='+positions);

    var sel1 = typeof select1 == 'string' ? E(select1) : select1;
    var sel2 = typeof select2 == 'string' ? E(select2) : select2;

    if (!ensureElement(sel1, 'select') || !ensureElement(sel2, 'select'))
        return 0;

    if (sel1.selectedIndex < 0 && positions == null)
        return 0;

    if (positions == null)
    {
        positions = new Array();
        for (var i=0; i < sel1.length; i++)
        {
            if (sel1.options[i].selected)
                positions.push(i);
        }
    }

    if (positions.length == 0)
        return 0;

    for (var i=0; i < sel2.length; i++)
    {
        sel2.options[i].selected = false;
    }

    //alert('p='+positions);
    // licznik przeniesionych element?w
    var count = 0;
    // modyfikator indeksu tablicy ?r?d?owej (po przeniesieniu elementu
    // indeksy element?w le??cych dalej zmieniaj? si?)
    var shift = 0;
    for (var i=0; i < positions.length; i++)
    {
        var pos = positions[i] - shift;
        if (pos >= sel1.options.length)
            break;
        var opt = sel1.options[pos];
        var newOpt = new Option(opt.text, opt.value, false, true);
        sel1.options[pos] = null;
        sel2.options[sel2.options.length] = newOpt;
        shift++;
        count++;
    }

    return count;
}

function removeOptions(select)
{
    var positions = null;
    if (arguments.length > 1)
        positions = arguments[1];

    var sel = typeof select == 'string' ? E(select) : select;

    if (!ensureElement(sel, 'select'))
        return 0;

    if (positions == null)
    {
        positions = new Array();
        for (var i=0; i < sel.length; i++)
        {
            if (sel.options[i].selected)
                positions.push(i);
        }
    }

    if (positions.length == 0)
        return 0;

    var count = 0;
    var shift = 0;

    for (var i=0; i < positions.length; i++)
    {
        var pos = positions[i] - shift;
        if (pos >= sel.options.length)
            break;
        sel.options[pos] = null;
        shift++;
        count++;
    }

    return count;
}

function selectAllOptions(select)
{
    var sel = typeof select == 'string' ? E(select) : select;

    if (!ensureElement(sel, 'select'))
        return false;

    for (var i=0; i < sel.length; i++)
        sel.options[i].selected = true;

    return true;
}

