<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- Poczatek image.jsp -->
<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.cookie.min.js"></script>

<ds:available test="barcodeHighlight">
<div id="selectedBarcode">
</ds:available>

<ds:available test="viewer.narrow">
<style>

#toolbarTable {
	/*width: 500px !important;*/
	float: left;
	<ww:if test="viewserverDataSide">
		float: right;
	</ww:if>
}

#busDiv {
	margin-top: -58px;
}

#busDiv br {
	display: none;
}

#toolBar td a img {
	width: 40px;
}
	
</style>
</ds:available>

</div>

<ww:if test="documentReassigned">
	<script type="text/javascript">
		<ww:if test="!preventRefresh">
			window.opener.document.location.href = '<ww:url value="reloadLink"/>';
		</ww:if>
		window.close();
	</script>
</ww:if>

<ww:else>
	<script type="text/javascript">
		<ww:if test="isReloadOpener">
			<ds:available test="!dwr.viewserver">
			try
			{
				if(window.opener.document.URL.match('document-archive') || 
						window.opener.document.URL.match('summary'))
				{
					window.opener.document.forms[0].submit();
				}
			}
			catch(e) 
			{}
			</ds:available>
		</ww:if>
	</script>
	<ww:if test="!tiffyApplet">
	
	<table width="100%" id="toolbarTable">
		<tr id="toolBar">
			<td>
			<ds:available test="!viewserver2">
				<a href="javascript:void(null)" onclick="mainImage.actualSize()">
					<img src="../img/dopasuj2.gif" border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.fitToWindow(1, 1)">
					<img src="../img/dopasuj.gif" border="0"/>	
				</a> 
				
				<ds:available test="viewserverAutoHeightImage">
				<a href="javascript:void(null)" onclick="mainImage.fitVertical()">
					<img src="../img/dopasuj3.gif" border="0"/>	
				</a>
				</ds:available>
				
				<a href="javascript:void(null)" onclick="mainImage.rotateCircular(-1)">
					<img src="../img/obrot.gif"  border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.rotateCircular(1)">
					<img src="../img/obrot2.gif" w border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.rotate(0)">
					<img src="../img/normal.gif" border="0"/>		
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.zoomOut()">
					<img src="../img/minus.gif" border="0"/>
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.zoomIn()">
						<img src="../img/plus.gif" border="0"/>
				</a> 	
			</ds:available>
			<ds:available test="viewserver2">
				<a href="javascript:void(null)" onclick="mainImage.actualSize()">
					<img src="../img/dopasuj2.gif" border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.fitVertic()">
					<img src="../img/dopasuj3.gif" border="0"/>	
				</a>
				<a href="javascript:void(null)" onclick="mainImage.fitHoriz()">
					<img src="../img/dopasuj.gif" border="0"/>	
				</a>
				<a href="javascript:void(null)" onclick="mainImage.rotate(-1)">
					<img src="../img/obrot.gif"  border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.rotate(1)">
					<img src="../img/obrot2.gif" w border="0"/>	
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.rotate(0)">
					<img src="../img/normal.gif" border="0"/>		
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.zoomOut()">
					<img src="../img/minus.gif" border="0"/>
				</a> 
				<a href="javascript:void(null)" onclick="mainImage.zoomIn()">
						<img src="../img/plus.gif" border="0"/>
				</a> 					
			</ds:available>
			</td>
			<td align="center">
				<ww:if test="pageCount > 1">
					<ww:set name="pager" scope="request" value="pager" />
					<table width="100%">
						<tr>
							<td align="center">
								<jsp:include page="/pager-links-include.jsp"/>
								<ds:available test="viewer.narrow">
									<script type="text/javascript">
									$j('span.pager').children('a').each(function() {
									    var newHtml = $j(this).html();
									    if(isNaN(newHtml)) {
									    newHtml = newHtml.replace('Pierwsza', '').replace('Poprzednia', '')
									        .replace('Nast�pna', '').replace('Ostatnia', '');
									    $j(this).html(newHtml);
									    }
									});
									</script>
								</ds:available>
							</td>
						</tr>
					</table>
				</ww:if>
			</td>
		</tr>
	</table>
	</ww:if>
	
	
	<form id="form" action="<ww:url value="'/viewserver/viewer.action'"/>" method="post" onsubmit="return disableFormSubmits(this);"
	    <ds:available test="dwr.viewserver">class="dwr"</ds:available>
	>
	<ww:hidden name="'preventRefresh'" value="preventRefresh"/>
	<ww:hidden name="'binderId'" id="binderId" value="binderId"/>
	<ww:hidden name="imgWidth" />
	<ww:hidden name="imgHeight" />
	
	<ds:available test="viewserver2">
				<jsp:include page="/viewserver/content.jsp"/>
				<jsp:include page="/viewserver/business.jsp"/>
	</ds:available>
	
	<ds:available test="!viewserver2">
		<ww:if test="viewserverDataSide">
				<jsp:include page="/viewserver/content.jsp"/>
				<jsp:include page="/viewserver/business.jsp"/>
			</ww:if>
			<ww:else>
				<jsp:include page="/viewserver/business.jsp"/>		
				<jsp:include page="/viewserver/content.jsp"/>			
			</ww:else>
	</ds:available>
	
	</form>
	
	<ds:available test="archive.popup">
	<div id ="noteForm" style="display:none"><%-- Wy�wietla si� jako dialog --%>
		<ww:textarea onchange="'remarkChanged=1'" name="'content'" id="noteFormArea" rows="3" cols="40" cssClass="'txt'"/>
		<div><input type="button" class="btn" value="<ww:property value="getText('DodajUwage')"/>" id="noteFormButton" name="doArchiveNote" cssClass="'btn'" onclick="if(checkNoteFormArea()){sendEvent(this)};"/></div>
	</div></ds:available>


	<script type="text/javascript">

	function traceStart(str) {
		try {
			console.group(str);
		} catch(e) {}
	}
	
	function trace(str) {
		try {
			console.log(str);
		} catch(e) {}
	}

	function traceEnd() {
		try {
			console.groupEnd();
		} catch(e) {}
	}
	
	var mainImage = null;
	var remarkChanged = 0;

	<ds:available test="viewserver2">

	var $j = jQuery.noConflict();
	var windowWidth = 0;
	var windowHeight = 0;
	var isResizing = false;			// true jesli akurat zmieniamy rozmiar
	var fitH = true;
	var fitV = false;

	<ds:available test="!viewserverAutoHeightImage">
		fitH = true;
		fitV = false;
	</ds:available>
	<ds:available test="viewserverAutoHeightImage">
		fitH = false;
		fitV = true;
	</ds:available>

	<ww:if test="tiffyApplet">
		var fitH = true;
		var fitV = false;
	</ww:if>

	var scrollbarWidth = (document.all && !window.opera) ? 0 : 20;
	var minSize = 400;
	var availWidth, availHeight, aspectRatio;
	
	window.onresize = windowResize;
	
	function windowResize() {
		if(!isResizing) {
			
			resize(); /*resize();*/
			
		}
	}
	
	function resize() {
		if(mainImage != null) {
			mainImage.fitArea(fitH, fitV);
			mainImage.fitImage(fitH, fitV);
			<ds:available test="dwr.viewserver">
			mainImage.fitForm();
			</ds:available>
		}
	}
	
	function setWindowSize() 
	{
		if( typeof( window.innerWidth ) == 'number' ) {
			//Non-IE
			windowWidth = window.innerWidth;
			windowHeight = window.innerHeight;
		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
			//IE 6+ in 'standards compliant mode'		
			windowWidth = document.documentElement.clientWidth;
			windowHeight = document.documentElement.clientHeight;
		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
			//IE 4 compatible		
			windowWidth = document.body.clientWidth;
			windowHeight = document.body.clientHeight;
		}		

		//alert(windowWidth);
	}
	
	function Image(id, areaId, areaInnerId) {
		var img = document.getElementById(id);

		<ww:if test="tiffyApplet">
			img = document.getElementById('TiffyApplet');
		</ww:if>
		
		var area = document.getElementById(areaId);
		var areaInner = document.getElementById(areaInnerId);
		var spacer = 8;		// suma marginesow, paddingow etc dla area
		var maxWidth = 0;	// maksymalna szerokosc
		var bus = document.getElementById('busDiv');
		var zoom = 0;
		var orgWidth = parseInt(document.getElementById('imgWidth').value, 10);
		var orgHeight = parseInt(document.getElementById('imgHeight').value, 10);
		var orgDocWidth = document.body.offsetWidth;
		var rotation = 0;
		var cookieRotation = 0;

		var pageWithMemory = false;
		
		//barCode info - #todo
		var barCodeHeight = 263;
		var barCodeWidth = 427;
		var barCodeTop = 250;
		var barCodeLeft = 250;

		<ds:available test="viewer.memory">	
		pageWithMemory = true;	
		var pageNb = '<ww:property value="pager.offset"/>';
		var pagesSize = '<ww:property value="pageCount"/>'	
		var imagesParam = '<ww:property value="getPagesParam()" />'.split(',');
		var imageSizeInWindow = 0;
		var imageRotatePosition = 0;
		var imageZoom = 0;	
		if (imagesParam != null) {
			imageSizeInWindow = imagesParam[(pageNb * 3)];
			imageRotatePosition = imagesParam[(pageNb * 3) + 1];
			imageZoom = imagesParam[(pageNb * 3) + 2];
		}

		traceStart('ViewServer memory');
		trace('pageNb: ' + pageNb);
		trace('pagesSize ' + pagesSize);
		trace('imagesParam ' + imagesParam);
		
		traceEnd();
		</ds:available>
		
		aspectRatio = orgWidth / orgHeight;

		trace('AspectRatio: ' + aspectRatio);

		/* zwraca cookieRotation */
		this.getCookieRot = function() {
			return cookieRotation;
		}

		this.getRotation = function() {
			return rotation;
		}

		/* zwraca obiekt area */
		this.getArea = function() {
			return area;
		}

		/* wczytuje wymiary arrea z ciasteczka */
		this.loadAreaSize = function() {
			trace('Trying to retrieve areaWidth from cookie ...');
			if($j.cookie('areaWidth') && ($j.cookie('areaWidth') != -1)) {
				area.style.width = $j.cookie('areaWidth') + 'px';
				trace('Trying to retrieve areaWidth from cookie ... [OK]');
			} else {
				trace('Trying to retrieve areaWidth from cookie ... [FAIL]');
			}

			trace('Trying to retrieve areaHeight from cookie ...');
			if($j.cookie('areaHeight') && ($j.cookie('areaHeight') != -1)) {
				area.style.height = $j.cookie('areaHeight') + 'px';
				trace('Trying to retrieve areaHeight from cookie ... [OK]');
			} else {
				trace('Trying to retrieve areaHeight from cookie ... [FAIL]');
			}

			trace('Trying to retrieve image rotation from cookie ...');
			if($j.cookie('rotation')) {
				cookieRotation = $j.cookie('rotation');
				trace('Trying to retrieve image rotatation from cookie ... [OK]');
			} else {
				cookieRotation = 0;
				trace('Trying to retrieve image rotatation from cookie ... [FAIL]');
			}

			//alert(cookieRotation);
		}
		
		/* rozciaga area maksymalnie */
		this.fitArea = function(h, v) {
			setWindowSize();
			availWidth = windowWidth - bus.offsetWidth - spacer - scrollbarWidth;			
			availHeight = windowHeight - document.getElementById('close').offsetHeight - 
				<ww:if test="!tiffyApplet">document.getElementById('toolBar').offsetHeight - </ww:if> 
				20 - spacer;

			traceStart('fitArea');
			trace('availWidth: ' + availWidth + '  availHeight: ' + availHeight);

			if(availWidth < minSize)
				availWidth = minSize;
			if(availHeight < minSize)
				availHeight = minSize;
			
			trace('availWidth: ' + availWidth + '  availHeight: ' + availHeight);

			
			//window.status = availWidth;
			
			<ds:available test="viewserverScroll">
			if(zoom > 0) {
				availWidth = img.width;
				availHeight = img.height;
				document.body.style.width = img.offsetWidth + 20 + bus.offsetWidth + 'px';
			} else if(zoom < 0)
				document.body.style.width = orgDocWidth + 'px';
			</ds:available>
			
			/* dopasowuje na szerokosc */
			if(h) {
				area.style.width = availWidth + 'px';
				area.style.height = availWidth / aspectRatio + 'px';
				trace('Fitting width');
			} else if(v) { 		// dopasowuje na wysokosc
				area.style.height = availHeight + 'px';
				area.style.width = availHeight * aspectRatio + 'px';
				trace('Fitting Height');
			}

			trace('areaWidth: ' + area.offsetWidth + '  areaHeight: ' + area.offsetHeight);
			
			traceEnd();

			<ds:available test="viewserverScroll">
			if(zoom != 0)
				return;
			</ds:available>
			
			if(availWidth <= minSize || availHeight <= minSize) 
				return;
			
			/* mozilla zle wylicza szerokosc okna - dodaje pasek przewijania */
			if(bus.offsetTop > area.offsetTop) {
			//	if(!document.all)
					scrollbarWidth = 20;
				
			//	if(h)
			//		this.fitArea(h, v);
			//	else if(v)
			//		this.fitArea(!h, !v);
			} 
		}
		
		/* dopasowuje area do obrazka, jesli obrazek mniejszy od area lub area zmiesci sie na ekranie */
		this.fitAreaToImage = function() {
			var imgWidth = img.width;
			var imgHeight = img.height;
			
			if(rotation == 1 || rotation == 3) {
				var tmp = imgWidth;
				imgWidth = imgHeight;
				imgHeight = tmp;
			}
			
			if(imgWidth < areaInner.offsetWidth || imgWidth + spacer < availWidth) {
				areaInner.style.width = imgWidth + 'px';
				area.style.width = imgWidth + spacer + 'px';
			} else {
				areaInner.style.width = availWidth - spacer + 'px'
				area.style.width = availWidth + 'px';
			}
				
			if(imgHeight < areaInner.offsetHeight || imgHeight < availHeight) {
				areaInner.style.height = imgHeight + 'px';
				area.style.height = imgHeight + spacer + 'px';
			} else {
				areaInner.style.height = availHeight - spacer + 'px'
				area.style.height = availHeight + 'px';
			}
			<ds:available test="barcodeHighlight">
			this.fitBarcodeHighlight();
			</ds:available>
		}

		this.fitForm = function() {
			var width = $j('#imageDiv').outerWidth() + $j('#busDiv').outerWidth();
			$j('#form').width(width);
		}

		this.updateZoomInfo = function() {
			var zoomPercent = Math.floor(img.width / orgWidth * 100);
			var title = window.document.title + '';
			title = title.replace(/\(\d+\%\)\s/, '');
			title = '(' + zoomPercent + '%) ' + title;
			window.document.title = title;
		}
		
		this.zoomIn = function()
		{
			this.resetImagePos();
			img.width = Math.round(img.width * 1.15);
			img.height = Math.round(img.height * 1.15);
	
			zoom ++;

			traceStart('ZoomIn');
			trace('img.width: ' + img.width);
			trace('img.height: ' + img.height);
			trace('zoom: ' + zoom);
			traceEnd();
			<ds:available test="viewserverScroll">
				this.fitArea(fitH, fitV);
			</ds:available>
			
			this.fitAreaToImage();	
			this.updateZoomInfo();
		}
		
		this.zoomOut = function()
		{
			img.width = Math.round(img.width * (100 / 115));
			img.height = Math.round(img.height * (100 / 115));
			
			zoom --;
		
			traceStart('ZoomOut');
			trace('img.width: ' + img.width);
			trace('img.height: ' + img.height);
			trace('zoom: ' + zoom);

			traceEnd();
			
			<ds:available test="viewserverScroll">
				this.fitArea(fitH, fitV);
			</ds:available>
			this.fitAreaToImage();
			this.updateZoomInfo();
		}
		
		this.superZoom = function() {
			$j('#zoomWindow').css('display', 'block');
			var zoom_el = document.getElementById('zoomWindow');
			var oldWidth, oldHeight;
			
			function moveZoom(obj, e) {
				obj.style.left = e.pageX - 50 + 'px';
				obj.style.top = e.pageY - 50 + 'px';
				
				if(obj.offsetLeft < 0)
						obj.style.left = 0 + 'px';
					else if(obj.offsetLeft + 100 > areaInner.offsetWidth)
						obj.style.left = areaInner.offsetWidth - 100 + 'px';
						
					if(obj.offsetTop < 0)
						obj.style.top = 0 + 'px';
					else if(obj.offsetTop + 100 > areaInner.offsetHeight)
						obj.style.top = areaInner.offsetHeight - 100 + 'px';
			}
			
			function customZoom(scale, x, y) {
				oldWidth = img.width;
				oldHeight = img.height;
				
				//alert(scale);
			
				img.width *= scale;
				img.height *= scale;
				
				
				img.style.left = Math.floor((50 - x) * scale) + 'px';
				img.style.top = Math.floor((50 - y) * scale) + 'px';
				
				
				zoom ++;
				
				window.status = x + ' x ' + y;
			}
			
			function customUnzoom() {
				img.style.top = 0;
				img.style.left = 0;
				img.width = oldWidth;
				img.height = oldHeight;
				
				zoom --;
			}
			
			$j('#zoomWindow').bind('mousemove', function(e) {
				moveZoom(zoom_el, e);
			}).bind('click', function(e) {
				
				customZoom(2, e.pageX, e.pageY);
				zoom_el.style.display = 'none';
				$j('#zoomWindow').unbind('mousemove').unbind('click');
				$j('#imgAtt').unbind('mousemove');
				
				$j('#imgAtt').bind('click', function(e) {
					customUnzoom();
					
					$j(this).unbind('click');
				});
			});
			
			$j('#imgAtt').bind('mousemove', function(e) {
				$j('#zoomWindow').css('left', e.pageX - 50).css('top', e.pageY - 50);
			});
			
		}
		
		this.fitHoriz = function() {
			zoom = 0;
			fitH = true;
			fitV = false;
			this.resetImagePos();
			resize();
		}
		
		this.fitVertic = function() {
			zoom = 0;
			fitH = false;
			fitV = true;
			this.resetImagePos();
			resize();
		}
		
		this.actualSize = function() {
			zoom = 0;
			img.width = orgWidth;
			img.height = orgHeight;
			this.fitAreaToImage();
			this.updateZoomInfo();
		}
		
		this.resetImagePos = function() {
			img.style.top = 0;
			img.style.left = 0;
		}
		
		this.rotate = function(arg) {
			if (arg > 0) 
				rotation += 1;
			else if(arg == 0)
				rotation = 0;
			else 
				rotation += 3;
			rotation = rotation % 4;
			
			img.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + rotation + ')';
			window.status = rotation;
			aspectRatio = 1 / aspectRatio;
			
			this.fitAreaToImage();
			this.fitBarcodeHighlight();
		}

		this.rotateCustom = function(rot) {
			//alert('RotateCustom: ' + rot);
			if(rot != -1) {
				while(rot > 0) {
					this.rotate(1);
					rot --;
				}
			}
		}
		
		this.calculateOrigin = function(top, left) {
			var imgWidth = img.width;
			var imgHeight = img.height;
			
			if(rotation == 1 || rotation == 3) {
				var tmp = imgWidth;
				imgWidth = imgHeight;
				imgHeight = tmp;
			}
		
			/* jesli obrazek jest wiekszy niz area */
			if(imgWidth >= areaInner.offsetWidth) {
			if(left > 0) 
				left = 0;
			else if(left + imgWidth < areaInner.offsetWidth) {
				left = parseInt(areaInner.offsetWidth - imgWidth);
			}
			} else {
				if(left < 0)
					left = 0;
				else if(left + imgWidth > areaInner.offsetWidth) {
					left = parseInt(areaInner.offsetWidth - imgWidth);
				}
			}

			if(imgHeight >= areaInner.offsetHeight) {
			if(top > 0)
				top = 0;
			else if(top + imgHeight < areaInner.offsetHeight) {
				top = parseInt(areaInner.offsetHeight - imgHeight);
			}
			} else {
				if(top < 0)
					top = 0;
				else if(top + imgHeight > areaInner.offsetHeight) {
					top = parseInt(areaInner.offsetHeight - imgHeight);
				}
			}
			return {top: top, left: left};
		}
		
		this.drag = function(pos) {
			var p = this.calculateOrigin(pos.top, pos.left);
			pos.top = p.top;
			pos.left = p.left;
		
			<ds:available test="barcodeHighlight">
			mainImage.fitBarcodeHighlight(pos.top, pos.left);
			</ds:available>

		}
		
		
		//funkcja scary
		<ds:available test="barcodeHighlight">
		this.dragBarcode = function(position, top, left) {
			var p = this.calculateOrigin(top, left);
		
			$j('#imgAtt').css("top", p.top + "px");
			$j('#imgAtt').css("left", p.left + "px");

			mainImage.fitBarcodeHighlight(p.top, p.left, position);
		}
		</ds:available>
		
		/* dopasowuje obrazek */
		this.fitImage = function(h, v) {
			areaInner.style.width = parseInt(area.style.width) - spacer + 'px';
			areaInner.style.height = parseInt(area.style.height) - spacer + 'px';
			
			if(zoom == 0) {
				img.width = areaInner.offsetWidth;
				img.height = areaInner.offsetHeight;
				
				if(rotation == 1 || rotation == 3) {
					var tmp = img.width;
					img.width = img.height;
					img.height = tmp;
				}
			}
			<ds:available test="barcodeHighlight">
			this.fitBarcodeHighlight();
			</ds:available>
			<ds:available test="viewer.narrow">
			$j('#toolbarTable').width($j('#imageDiv').width());
			</ds:available>
			this.updateZoomInfo();
		}
		
		this.fitBarcodeHighlight = function(x, y, position) {
			var scaleW = img.width / orgWidth;
			var scaleH = img.height / orgHeight;
			
			var imgTop = 0;
			var imgLeft = 0;

			if (typeof(x) != 'undefined')
				imgTop = x;
			else {
				if (img.style.top != '') 
					imgTop =  parseInt(img.style.top.replace("px", ""), 10);
			}
			
			if (typeof(y) != 'undefined')
				imgLeft = y;
			else {
				if (img.style.left != '') 
					imgLeft =  parseInt(img.style.left.replace("px", ""), 10);
			}
			
			//do zmiany potem
			barCodeLeft = <ww:property value="barCodeX" />;
			barCodeTop = <ww:property value="barCodeY" />;

			var strNotVisible = ""; //wart ozn, ze nie ma barcodu

			if ( '<ww:property value="barCode" />' == strNotVisible) {
				$j("#selectedBarcode").hide();
			}
			else {
				$j("#selectedBarcode").show();

			
				var browserName=navigator.appName; 

				var leftOffset = spacer;
				var topOffset = document.getElementById('close').offsetHeight + 
				<ww:if test="!tiffyApplet">document.getElementById('toolBar').offsetHeight + </ww:if> 
				+ spacer - 2;
				var browserName=navigator.appName; 
			
				if (browserName=="Microsoft Internet Explorer") topOffset += 18;
		
				var scaledHeight = Math.round(barCodeHeight * scaleH);
				var scaledWidth =  Math.round(barCodeWidth * scaleW);
				var scaledLeft = Math.round(barCodeLeft * scaleW);
				var scaledTop = Math.round(barCodeTop * scaleH);
				
				trace("scaledLeft: " + scaledLeft + " scaledTop: " + scaledTop);
			
				this.barcodeRotate(scaledTop, scaledLeft, scaledWidth, scaledHeight, topOffset, imgLeft, imgTop, position);
			}
		
		}
		
		this.barcodeClip = function(t, l, w, h, topOffset, position) {
			//obcinanie wystaj�cych kawalkow barcodu poza divem z obrazkiem
			var maxHeight = $j("#imageInnerDiv").height();
			var maxWidth = $j("#imageInnerDiv").width();
			
			if ((l < maxWidth) && (t < maxHeight + topOffset) && ((t + h) > topOffset)) {
				$j("#selectedBarcode").show();	
				h = Math.round(Math.min((t + h), (maxHeight + topOffset)) - Math.max(topOffset, t));
				$j("#selectedBarcode").css("height", h + "px");
				$j("#selectedBarcode").css("width", Math.min((maxWidth - l + 1), w)+"px");
				$j("#selectedBarcode").css("left", l + "px");
				$j("#selectedBarcode").css("top", Math.max(t, topOffset) + "px");
				
				if (typeof(position) != 'undefined') {
					position.top = Math.max(t, topOffset);
					position.left = l;
				}
				
			}
			
			else {
				$j("#selectedBarcode").hide();	
			}
		}
		
		
		this.barcodeRotate = function(t, l, w, h, topOffset, imgLeft, imgTop, position) {
			var leftOffset = 2; //magic
			var divH = img.height;
			var divW = img.width;
			
			var rotatedTop, rotatedLeft, rotatedHeight, rotatedWidth;
		
			switch (rotation) {
				case 0:
				rotatedHeight = h;
				rotatedWidth = w;
				rotatedTop = t + topOffset;
				rotatedLeft = l;
				break
				case 1:
					rotatedHeight = w;
					rotatedWidth = h;
					rotatedTop = l + topOffset;
					rotatedLeft = divH - h - t;
				break;
				case 2:
					rotatedHeight = h;
					rotatedWidth = w;
					rotatedTop = divH - h - t + topOffset;
					rotatedLeft = divW - w - l;
				break;
				case 3:
					rotatedHeight = w;
					rotatedWidth = h;
					rotatedTop = divW - l - w + topOffset;
					rotatedLeft = t;
				break;
			}
			this.barcodeClip(rotatedTop + imgTop, rotatedLeft + imgLeft + leftOffset, rotatedWidth, rotatedHeight, topOffset, position);

		}
	}

	$j(document).ready(function() {
					
		//alert(document.getElementById('imgWidth').value + 'x' + document.getElementById('imgHeight').value);
		mainImage = new Image('imgAtt', 'imageDiv', 'imageInnerDiv');	

		<ds:available test="dwr.viewserver">
		try {
			var reloadOpener = <ww:property value="isReloadOpener" />;
			if(reloadOpener === true) {
				// przeladowujemy formatke z np.: archiwizacja
				// window.history.go(0) - nie zawsze od�wie�a cache
			 	window.opener.location.href = window.opener.location.href + '';				
			 	// alert(window.opener.location.href);
			}
		} catch(e) {
			console.error(e);
		}
		</ds:available>
		
		
		windowResize();
		<ds:available test="viewer.memory">
			mainImage.loadAreaSize();
			mainImage.fitImage(true, false);
			//alert('po windowResize: ' + mainImage.getCookieRot());
			mainImage.rotateCustom(mainImage.getCookieRot());
		</ds:available>
		
		
		$j('#imageDiv').resizable({resize: function(event, ui) {
			mainImage.fitImage(fitH, fitV);
		}, start: function(event, ui) {
			isResizing = true;
			$j('#imageDiv').resizable('option', 'maxWidth', availWidth - scrollbarWidth);
			$j('#imageDiv').resizable('option', 'maxHeight', availWidth * aspectRatio);
		}, stop: function(event, ui) {
			isResizing = false;
		}, aspectRatio: true
		});
		
		$j('#imgAtt').draggable({cursor: 'move', snap: 'true' 			
			/*, drag: function(event, ui) {

				mainImage.drag(ui.position);
			}*/			
		});
		
		$j('#imgAtt').bind('drag', function(event, ui) {
			mainImage.drag(ui.position);
		});
		
		$j('#selectedBarcode').draggable({cursor: 'move',
			drag: function(event, ui) {
				var imgLeft = parseInt($j('#imgAtt').css("left").replace("px", ""), 10);
				var imgTop = parseInt($j('#imgAtt').css("top").replace("px", ""), 10);
				
				var bcLeft = parseInt($j('#selectedBarcode').css("left").replace("px", ""), 10);
				var bcTop = parseInt($j('#selectedBarcode').css("top").replace("px", ""), 10);			
				trace("bcLeft: " + bcLeft + ", bcTop: " + bcTop);
				imgTop = imgTop - bcTop + ui.offset.top;
				imgLeft = imgLeft - bcLeft + ui.offset.left;
				mainImage.dragBarcode(ui.position, imgTop, imgLeft);
			}
	
		});

		<ds:available test="viewserverMouseWheel">
		
		$j('#imgAtt').bind('mousewheel', function(event, delta) {
			if(delta > 0)
				mainImage.zoomIn();
			else
				mainImage.zoomOut();

			return false;
		});
		</ds:available>
		
	
		/*$j(window).unload(function() {
			mainImage.resetAreaCookie();
			alert('EXIT');
		});*/
		
		//});
	});
	
	</ds:available>

	<ds:available test="!viewserver2">
	var top;
	var left;
	var dragStartTop;
	var dragStartLeft;
	var dragging = false;
	var viewportWidth;
	var viewportHeight;
	var windowWidth;
	var windowHeight;
	</ds:available>
	
	var pageWithMemory = false;
	<ds:available test="viewer.memory">	
	pageWithMemory = true;	
	var pageNb = '<ww:property value="pager.offset"/>';
	var pagesSize = '<ww:property value="pageCount"/>'	
	var imagesParam = '<ww:property value="getPagesParam()" />'.split(',');
	var imageSizeInWindow = 0;
	var imageRotatePosition = 0;
	var imageZoom = 0;	
	if (imagesParam != null) {
		imageSizeInWindow = imagesParam[(pageNb * 3)];
		imageRotatePosition = imagesParam[(pageNb * 3) + 1];
		imageZoom = imagesParam[(pageNb * 3) + 2];
	}
	</ds:available>


	<ds:available test="!viewserver2">
	var outerDiv = document.getElementById("outerDiv");
	var innerDiv = document.getElementById("innerDiv");
			 
	window.onresize = prep;	
	window.onload = prep;	 
	</ds:available>

	//setTimeout('init()',100);


		<ds:available test="archive.popup">
	$j(document).ready(function(){
		try {
			$j("#noteForm").dialog({autoOpen:false,modal:true, width: 460, title:'Uwaga'});
			$j("#noteFormArea").growfield({min:"21"});
		} catch(e) {}
	});
	function checkNoteFormArea(){
		if($j("#noteFormArea").get(0).value.length > 0){
			$j("#content").remove();<%-- "konkurencyjne" pole z uwag� --%>
			$j("#noteForm").dialog("close");
			$j("#noteForm").hide();
			$j("#noteForm").appendTo("#form");
			return true;
		} else {
			return false;
		}
	}</ds:available>
	 
	function selectDiscard(btn)
	{
		if(safeSelectDiscard(btn)){
			sendEvent(btn);
			return true;
		} else {
			return false
		}
	}

	function safeSelectDiscard(btn)
	{
		var note2 = document.getElementById('content');
		var tmp2 = note2.value;
		if(tmp2.length < 1 || tmp2.toString().indexOf('<ds:lang text="WpiszTrescUwagi"/>') != -1){
			alert('<ds:lang text="UwagaJestWymagana"/>');
			return false;
		}
		return true;
	}

	function forceNote(actionName, button){
		<ds:available test="archive.popup">
		$j("#noteFormButton").get(0).name = actionName;
		$j("#noteForm").dialog("open");
		</ds:available><ds:available test="!archive.popup">
		var bool = safeSelectDiscard(button);
		if(bool){
			$j("form").append("<input type='hidden' id='"+actionName+"' name='"+actionName+"' value='true'/>");
			button.form.submit();
		}
		</ds:available>
	}
	 
	function selectReject(btn,next)
	{
		var combobox = document.getElementById('selectForAdnotations');
		var adnotation = document.getElementById('content');
		var selected = combobox.selectedIndex;
		if(selected!=0)
			adnotation.value = combobox[selected].value + " " + adnotation.value;
		var note = document.getElementById('content');
		var tmp = note.value;
	
		if(tmp.length < 1)
		{
			alert('<ds:lang text="UwagaJestWymagana"/>');
			return false;
		}
		if(next)
		{
			document.getElementById('next').value='true';
		}
		sendEvent(btn);
		return true;
	}
	
	function selectEva(btn,next)
	{
		if(next)
		{
			document.getElementById('next').value='true';
		}
		sendEvent(btn);
		return true;
	}
	 
	function confirmFinish()
	{
	
		<ww:if test="!finishOrAssignState.canFinish">	
			alert('<ww:property value="joinCollection(finishOrAssignState.cantFinishReasons)" escape="false"/>');
			return false;
		</ww:if>
		<ww:if test="!finishOrAssignState.finishWarnings.empty && !documentFax">
			if (!confirm('<ww:property value="joinCollection(finishOrAssignState.finishWarnings)" escape="false"/>\n\n<ds:lang text="ZakonczycPraceZdokumentem"/>'))
				return false;
		</ww:if>
		<ww:else>
			if (!confirm('<ds:lang text="NaPewnoZakonczyc"/>?'))
				return false;
		</ww:else>
		document.getElementById('doManualFinish').value='true';
	
	return true;
	}

	function fillRemark()
	{
		var uwaga = document.getElementById('content');
		var uwagaSelect = document.getElementById('selectForAdnotations');
		var selOpt = uwagaSelect.options[uwagaSelect.selectedIndex];
		if (uwaga.value == '<ds:lang text="WpiszTrescUwagi"/>') {
			uwaga.value = "";
		}
		if (selOpt.value != -1) {
			if (uwaga.value != "") {
				uwaga.value += " ";
			}
			uwaga.value += selOpt.value;
			remarkChanged = 1;
		}
		$j("#content").unbind('click').removeClass("italic");
	}
	 
	function archiveAction()
	{
		var uwaga = document.getElementById('content');
		var uwagaSelect = document.getElementById('selectForAdnotations');
		
		if (remarkChanged == 0 && uwagaSelect.value == -1) {
			uwaga.value = "";
		}
		
		return validateDockind();
	}
	
	function init()
	{
		prep();
		<ww:if test="viewserverWithData && !viewserverWholePage">
			mainImage.zoomIn();mainImage.zoomIn();mainImage.zoomIn();
		</ww:if>	
		applySavedPosition();
	}

	// dwr wywolywuje przy zmianie rozmiaru
	function fixResize() {
		windowResize();
	}


	<ds:available test="!viewserver2">
	/* stary vieserver */
	function setWindowSize() 
	{
		if( typeof( window.innerWidth ) == 'number' ) {
			//Non-IE
			windowWidth = window.innerWidth;
			windowHeight = window.innerHeight;
		} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
			//IE 6+ in 'standards compliant mode'		
			windowWidth = document.documentElement.clientWidth;
			windowHeight = document.documentElement.clientHeight;
		} else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
			//IE 4 compatible		
			windowWidth = document.body.clientWidth;
			windowHeight = document.body.clientHeight;
		}		 
	}
				
	function prep()
	{
		setWindowSize();
		<ww:if test="viewserverWithData">
			viewportWidth = Math.round((windowWidth - $j("#businessDataCell").get(0).clientWidth) * 0.90);
			viewportHeight = Math.round(windowHeight * 0.82);
		</ww:if>
		<ww:else>
			viewportWidth = Math.round(windowWidth * 0.93);
			viewportHeight = Math.round(windowHeight * 0.82);
		</ww:else>
	
		outerDiv.onmousedown = startMove;
		outerDiv.onmousemove = processMove;
		outerDiv.onmouseup = stopMove;
		// konieczne dla IE do umo�liwienia przeci�gania
		outerDiv.ondragstart = function() { return false; }   
	   
		outerDiv.style.width = viewportWidth;
		outerDiv.style.height = viewportHeight;

		mainImage = new Image('i');
		
		if (pageWithMemory == true) {
			if (imageSizeInWindow != null && imageSizeInWindow == 1)
				mainImage.actualSize();
			else
				mainImage.fitToWindow(1,1);			
			if (imageRotatePosition!=null && imageRotatePosition != 0)
				mainImage.rotate(imageRotatePosition);
			if (imageZoom != null && imageZoom != 0)
				mainImage.zoom(imageZoom);
		} else {
			mainImage.fitToWindow(1,1);
		}

		<ds:available test="viewserverAutoHeightImage">
			mainImage.fitVertical();
		</ds:available>


		// fix dla ie
		innerDiv.style.width = parseInt(outerDiv.style.width) - 4 + 'px'; // 2 piksle zajmuje border (chyba)

		//alert('outerDiv: ' + outerDiv.offsetWidth + ' xx innerDiv: ' + innerDiv.offsetWidth);
	}
	
	function startMove(event) 
	{
		if (!event) event = window.event; // dla IE
		dragStartLeft = event.clientX;
		dragStartTop = event.clientY;
	
		innerDiv.style.cursor = "move";//"-moz-grab";
	
		top = stripPx(innerDiv.style.top);
		left = stripPx(innerDiv.style.left);
	
		dragging = true;
		return false;
	}
	
	function processMove(event) 
	{
		if (!event) event = window.event; // dla IE
		if (dragging) 
		{
			var newTop = parseFloat(top) + (event.clientY - dragStartTop);
			var newLeft = parseFloat(left) + (event.clientX - dragStartLeft);
			innerDiv.style.top = getTop(newTop);
			innerDiv.style.left = getLeft(newLeft);
			checkAvailableMoves()
		}
	}
	
	function hideElem(id)
	{
		try {
		document.getElementById(id).src = '<ww:url value="'/img/blank.gif'"/>';
		document.getElementById(id).style.cursor = 'default';
		} catch(e) {}
	}
	
	function showElem(id,dir)
	{
		document.getElementById(id).src = '<ww:url value="'/img/strzalka-'"/>'+dir+'.gif';
		document.getElementById(id).style.cursor = 'pointer';
	}
	
	function checkAvailableMoves()
	{
		var outX = stripPx(outerDiv.style.width);
		var outY = stripPx(outerDiv.style.height);
		var inX = stripPx(innerDiv.style.width);
		var inY = stripPx(innerDiv.style.height);
		var left = stripPx(innerDiv.style.left);
		var top = stripPx(innerDiv.style.top);
		
		if (left < 0)
			showElem('moveLeft', 'lewo');
		else
			hideElem('moveLeft');
		if (top < 0)
			showElem('moveUp', 'gora');
		else
			hideElem('moveUp'); 
		if (left > -(inX - outX))
			showElem('moveRight', 'prawo');
		else
			hideElem('moveRight');		
		if (top > -(inY - outY))
			showElem('moveDown', 'dol');
		else
			hideElem('moveDown');			  
	}
	
	function check(val,inV,outV)
	{
		var tmp = inV - outV;
		if (tmp >= 0)
		{
			if (val < -tmp)
				val = -tmp;
		}
		else
		{
			if (val < 0)
				val = 0;
		}
		return val;		
	}
	
	function getTop(top)
	{
		var outY = stripPx(outerDiv.style.height);
		var inY = stripPx(innerDiv.style.height);
		if (top > 0)
			top = 0;
		//if (top < -(inY - outY))
		return check(top,inY,outY);
		//	top = -(inY - outY);
		//return top;	
	}
	
	function getLeft(left)
	{
		var outX = stripPx(outerDiv.style.width);
		var inX = stripPx(innerDiv.style.width);
		if (left > 0)
			left = 0;
		//if (left < -(inX - outX))
		return check(left,inX,outX);
		//	left = -(inX - outX);
		//return left;	
	}
	
	function stopMove() 
	{
		innerDiv.style.cursor = "";
		dragging = false;
	}
	
	function stripPx(value) 
	{
		if (value == "") return 0;
		return parseFloat(value.substring(0, value.length - 2));
	}
	
	function setInnerDivSize(width, height) 
	{
		innerDiv.style.width = width;
		innerDiv.style.height = height;

		innerDiv.style.width = width;
		innerDiv.style.height = height;
	}

	function setOuterDivSize(width, height) 
	{
		outerDiv.style.width = width;
		outerDiv.style.height = height;
	}

	function applySavedPosition() {
		var img = document.getElementById('i');
		if (img == null) {
			return;
		}
		var rot = document.getElementById('rotate').value;
		mainImage.rotate(rot);
	}
	
	function Image(id)
	{
		var img = document.getElementById(id);
		<ww:if test="tiffyApplet">
			img = document.getElementById('TiffyApplet');
		</ww:if>
		var origWidth = img.width;
		var origHeight = img.height;
		var rotation = 0;
		var self = this;
	
		var fitting = false;
		var fitAvailWidth = 1;
		var fitAvailHeight = 1;
	
		this.fitToWindow = function()
		{
			if (visibleOrigWidth() == 0 || visibleOrigHeight() == 0)
				return false;
	
			if (arguments.length > 1)
			{
				fitAvailWidth = arguments[0];
				fitAvailHeight = arguments[1];
			}
	
			var widthRatio = (fitAvailWidth * viewportWidth) / visibleOrigWidth();
		   // var heightRatio = (fitAvailHeight * viewportHeight) / visibleOrigHeight();
		   // var ratio = Math.min(widthRatio, heightRatio);
			ratio = widthRatio;
			
			img.width = Math.round(origWidth * ratio);
			img.height = Math.round(origHeight * ratio);

			<ds:available test="viewserverAutoHeightImage">
				img.style.width = Math.round(origWidth * ratio) + 'px';
				img.style.height = Math.round(origHeight * ratio) + 'px';
			</ds:available>
			
			fitting = true;
			setInnerDivSize(img.width,img.height);
			
			self.center(); 
			<ww:if test="viewserverWholePage">
				mainImage.viewWholePage();
			</ww:if>
			checkAvailableMoves();
			if (pageWithMemory == true)
				imageSizeInWindow = 0;
		}

		this.fitVertical = function() {
			var topHeight = 120;			// rozmiar paska u gory z ikonkami etc
			var oldWidth = img.width;

			setWindowSize();
			
			img.style.width = 'auto';
			img.style.height = windowHeight - topHeight;		
			
			setInnerDivSize(img.width,img.height);
			setOuterDivSize(img.width,img.height);
		}
	
		this.viewWholePage = function()
		{
			viewportHeight = img.height;
			outerDiv.style.height = viewportHeight;
		}
	
		this.actualSize = function()
		{
			img.width = origWidth;
			img.height = origHeight;

			<ds:available test="viewserverAutoHeightImage">
				img.style.width = origWidth + 'px';
				img.style.height = origHeight + 'px';
			</ds:available>
			
			setInnerDivSize(img.width,img.height);
	
			fitting = false;
			fitAvailWidth = 1;
			fitAvailHeight = 1;
			
			self.center(); 
			checkAvailableMoves();
			if (pageWithMemory == true)
				imageSizeInWindow = 1;			
		}
	
		this.zoomIn = function()
		{
			//alert(img.width);
			img.width = Math.round(img.width * 1.15);
			img.height = Math.round(img.height * 1.15);

			<ds:available test="viewserverAutoHeightImage">
				img.style.width = Math.round(img.width * 1.15) + 'px';
				img.style.height = Math.round(img.height * 1.15) + 'px';
			</ds:available>
			//alert(img.width);
			
			setInnerDivSize(img.width,img.height);
			checkAvailableMoves();
			if (pageWithMemory == true)
				++imageZoom;
		}
		
		this.zoomOut = function()
		{
			img.width = Math.round(img.width * (100 / 115));
			img.height = Math.round(img.height * (100 / 115));

			<ds:available test="viewserverAutoHeightImage">
				img.style.width = Math.round(img.width * (100 / 115)) + 'px';
				img.style.height = Math.round(img.height * (100 / 115)) + 'px';
			</ds:available>
			
			setInnerDivSize(img.width,img.height);
			checkAvailableMoves();
			if (pageWithMemory == true)
				--imageZoom;
		}

		this.zoom = function(arg) {
			for (i = 0; i < Math.abs(arg); i++)
				if (arg < 0) {
					img.width = Math.round(img.width * (100 / 115));
					img.height = Math.round(img.height * (100 / 115));
				} else {
					img.width = Math.round(img.width * 1.15 * arg);
					img.height = Math.round(img.height * 1.15 * arg);
				}
			setInnerDivSize(img.width,img.height);
			checkAvailableMoves();
			imageZoom = arg;
		}
	
		this.move = function(arg)
		{
			var newTop = stripPx(innerDiv.style.top);
			var newLeft = stripPx(innerDiv.style.left);
			if (arg == 'down')
				newTop = newTop - 100;
			else if (arg == 'up')
				newTop = newTop + 100;
			else if (arg == 'right')
				newLeft = newLeft - 100;
			else if (arg == 'left')
				newLeft = newLeft + 100;	
				
			innerDiv.style.left = getLeft(newLeft);	
			innerDiv.style.top = getTop(newTop);  
			checkAvailableMoves();
		}
	
		this.rotateCircular = function(arg)
		{
			if (arg > 0) rotation += 1;
			else rotation += 3;
			rotation = rotation % 4;
			if (pageWithMemory == true)
				imageRotatePosition = rotation;
			img.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+rotation+')';
			if (fitting)
				self.fitToWindow();
			else
				setInnerDivSize(img.width,img.height);  
				
			self.center();	   
			checkAvailableMoves();
			document.getElementById('rotate').value = rotation;
		}
	
		this.rotate = function(arg)
		{
			rotation = arg % 4;
			if (pageWithMemory == true)
				imageRotatePosition = rotation;
			img.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation='+rotation+')';
			if (fitting)
				self.fitToWindow();
			else	
				setInnerDivSize(img.width,img.height);   
				
			self.center(); 
			checkAvailableMoves();
			document.getElementById('rotate').value = rotation;
		}
	
		var visibleOrigWidth = function()
		{
			return (rotation % 2 > 0) ? origHeight : origWidth;
		}
	
		var visibleOrigHeight = function()
		{
			return (rotation % 2 > 0) ? origWidth : origHeight;
		}
	
		var visibleWidth = function()
		{
			return (rotation % 2 > 0) ? img.height : img.width;
		}
	
		var visibleHeight = function()
		{
			return (rotation % 2 > 0) ? img.width : img.height;
		}
		
		/**
			Wycentrowuje obrazek w oknie lub dosuwa go do lewej kraw�dzi, je�eli
			szeroko�� obrazka przekracza szeroko�� okna.
		*/
		this.center = function()
		{		
		   // if (stripPx(innerDiv.style.width) >= stripPx(outerDiv.style.width))
		   // {
				innerDiv.style.left = 0;
				innerDiv.style.top = 0;
		   // }
		   // else
		   // {
		   //	 //alert(outerDiv.style.width+' '+innerDiv.style.width);
		   //	 innerDiv.style.left = Math.round((stripPx(outerDiv.style.width) - stripPx(innerDiv.style.width)) / 2) + 'px';
		   //	 innerDiv.style.top = Math.round((stripPx(outerDiv.style.height) - stripPx(innerDiv.style.height)) / 2) + 'px';
		   // }	   
		}
	}

	/* koniec starego vieservera */
	</ds:available>
	
	function validateForm()
	{
		var content = document.getElementById('content').value;
		if (content != null && content.length > 4000) { alert('<ds:lang text="TrescUwagiJestZbytDluga.MaksymalnaDlugoscTo4000Znakow"/>'); return false; }
	
		return true;
	}
	
	// RZECZY ZWI�ZANE Z DEKRETACJ�
	function updateCheckbox(select)
	{
		var id = select.name.substring(select.name.indexOf('_')+1);
		document.getElementById(id).checked = (select.value.length > 0);
		<ds:available test="layout2">
			Custom.clear();
		</ds:available>
	}
	
	function countDoRealizacji()
	{
		var selects = document.getElementsByTagName('select');
		var count = 0;
		for (var i=0; i < selects.length; i++)
		{
			//alert(selects[i].value);
			if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
			{
				count++;
			}
		}
		return count;
	}
	
	function checkSelection()
	{
	   /* var selects = document.getElementsByTagName('select');
		var count = 0;
		for (var i=0; i < selects.length; i++)
		{
			//alert(selects[i].value);
			if (selects[i].value == 'internal,docusafe_1::obieg_reczny')
			{
				count++;
			}
		}*/
		//alert(count);
		<ww:if test="multiRealizationAssignment">
		if (countDoRealizacji() > 1)
			alert('<ds:lang text="WybranoWiecejNizJednaDekretacjeRealizacja.KlikniecieDekretujSpowodujeDekretacjeDoWiecejNizJednejOsoby"/>.');
		</ww:if>	
	}
	
	function updateSelect(checkbox)
	{
		<ds:extras test="business">
		var id = checkbox.value;
		var sels = document.getElementsByName('assignmentProcess_'+id);
		if (checkbox.checked && sels && sels.length > 0)
		{
			var sel = sels[0];
			if (sel.options && sel.options.length > 0)
			{
				sel.options.selectedIndex = 1;
			}
		}
		else
		{
			var sel = sels[0];
			if (sel.options && sel.options.length > 0)
			{
				sel.options.selectedIndex = 0;
			}
		}
		</ds:extras>
	}
	
	function validateAssignments()
	{
		var chks = document.getElementsByName('assignments');
		var checkedCount = 0;
		for (var i=0; i < chks.length; i++)
		{
			if (chks.item(i).checked) checkedCount++;
		}
	
		<ww:if test="!multiRealizationAssignment">
			if (countDoRealizacji() > 1)
			{
				alert('<ds:lang text="MoznaWybracTylkoJednaDekretacjeTypuRealizacja"/>');
				return false;
			}
		</ww:if>
		if (checkedCount == 0)
		{
			alert('<ds:lang text="NieWybranoUzytkownikowDoDekretacji"/>');
			return false;
		}		
		return true;
	}
	
	function checkSubstitutions()
	{
		var count=0;
		var show=0;
		assignments=document.forms[0].assignments;
		var str="<ds:lang text="ChceszDekretowacNaOsobyKtoreSaZastepowane"/>:\n"
		var str1;
		try{
		<ww:iterator value="substituted" status="status">
			
			if(assignments[count].checked && "<ww:property value="value"/>"!="") {
				show=show+1;
				str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowany.aPrzez"/> <ww:property value="value"/>";
				str=str+str1;
			}
			count=count+1;
		</ww:iterator>
		 }catch(err){
		<ww:iterator value="substituted" status="status">
			
			if(assignments.checked && "<ww:property value="value"/>"!="") {
				show=show+1;
				str1="\n<ww:property value="key"/> <ds:lang text="jestZastepowany.aPrzez"/> <ww:property value="value"/>";
				str=str+str1;
			}
		</ww:iterator>
		}
		if(show == 0){
			if(!confirm("<ds:lang text="NaPewnoDekretowac"/>?"))
			return false;
		}
		else{
			if(!confirm(str))
			return false;
		}
		
		var combobox = document.getElementById('selectForAdnotations');
		var adnotation = document.getElementById('content');
		var selected = combobox.selectedIndex;
		if(selected!=0)
			adnotation.value = combobox[selected].value + " " + adnotation.value;
		
		return true;
	
	}

	function setImageProperties() {
	    if (imagesParam == null) {
			for (i = 0; i < pagesSize * 3; i++) {
				imagesParam[i] = 0;
			}			
		}
		imagesParam[(pageNb * 3)] = imageSizeInWindow;
		imagesParam[(pageNb * 3)+1] = imageRotatePosition;
		imagesParam[(pageNb * 3)+2] = imageZoom;
	}
	
	function changePage(page)
	{
		if (pageWithMemory == true) {
			setImageProperties();
			document.getElementById('pagesParam').value = imagesParam;
		} 
		document.getElementById('page').value = page;
		document.getElementById('doChangePage').value = 'true';

		<ds:available test="viewer.memory">
		trace('Saving areaWidth to cookie ...');
		$j.cookie('areaWidth', mainImage.getArea().offsetWidth, { path: '/' });
		trace('Saving areaWidth to cookie ... [OK]');

		trace('Saving areaHeight to cookie ...');
		$j.cookie('areaHeight', mainImage.getArea().offsetHeight, { path: '/' });
		trace('Saving areaHeight to cookie ... [OK]');

		trace('Saving rotation to cookie ...');
		$j.cookie('rotation', mainImage.getRotation(), { path: '/' });
		trace('Saving rotation to cookie ... [OK]');
		</ds:available>

		//alert('pageChange');
		
		document.forms[0].submit();
	}
	
	</script>
</ww:else>

<style>
#selectedBarcode {
	background-color: yellow;
	opacity: 0.5;
	filter:alpha(opacity=50);
	position: absolute;
	z-index: 9999;
	width:428px;
	height:262px;
}
</style>

<!--N koniec image.jsp N-->