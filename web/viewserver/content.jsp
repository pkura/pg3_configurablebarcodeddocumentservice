<!--N content.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww"%>

<ds:available test="!viewserver2">
<td valign="top" id="imageCell" align="center" width=70%>
	<ww:if test="tiffyApplet">
		<ww:hidden name="'annotations'" value="annotations" id="annotations"/>
		<ww:hidden name="'pageCount'" id="pageCount" value="pageCount"/>
		<table>
			<tr>
				<td>
					<!--ds:event name="'doSaveAnnotation'" value="getText('Zapisz komentarz')" onclick="'saveAnnotation(this);'"/-->
				</td>
			</tr>
			<tr>
				<td>
					<div id="outerDiv">
					<div id="innerDiv" style="z-index: 0;">
					<applet code="TiffyApplet" archive="<ww:url value="'/Tiffy.jar,TiffyAppletDoc.jar'"/>"
						 id="TiffyApplet" width="100" height="100">
						<param name="options" value ="/nothumbnails /fh">
						<param name="cabbase" value="TiffyApplets.cab">
						<param name="filename" value="<ww:url value="'/viewserver/'"/>tiffApplet/<ww:property value="tiffyID"/>.tiffy">				
					</applet>
					</div>
					</div>
				</td>
			</tr>
			<tr style="display:none">
				<td>
					<div id="outerDiv"></div> 
				</td>
			</tr>
		</table>

	</ww:if>
	<ww:else>
	
	<table>
		<tr>
			<td colspan="3" align="center">
				<a href="javascript:mainImage.move('up')"><img id="moveUp"
					src="<ww:url value="'/img/strzalka-gora.gif'"/>" width="9" height="9"/></a>
			</td> 
		</tr>
		<tr>
			<td>
				<a href="javascript:mainImage.move('left')"><img id="moveLeft"
					src="<ww:url value="'/img/strzalka-lewo.gif'"/>" width="9" height="9"/></a>
			</td>
			<td align="center">
				<div id="outerDiv">
					<div id="innerDiv" style="z-index: 0;">
						<img src="<ww:url value="src"/>" width="<ww:property value="contentWidth"/>"
							height="<ww:property value="contentHeight"/>" alt="" id="i" />
					</div>
				</div>
			</td>
			<td>
				<a href="javascript:mainImage.move('right')"><img id="moveRight"
					src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="9" height="9"/></a>
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<a href="javascript:mainImage.move('down')"><img id="moveDown"
					src="<ww:url value="'/img/strzalka-dol.gif'"/>" width="9" height="9"/></a>
			</td>
		</tr>
	</table>
	
	
	
	</ww:else>
	
</td>
</ds:available>

<ds:available test="viewserver2">
	
	<div id="imageDiv" <ww:if test="viewserverDataSide"> style="float:right;<ww:if test="!viewserverWithData"> float: none;</ww:if>"> </ww:if> >
		<div id="imageInnerDiv">
			<ww:if test="tiffyApplet">
			<ww:hidden name="'annotations'" value="annotations" id="annotations"/>
			<ww:hidden name="'pageCount'" id="pageCount" value="pageCount"/>
					
					<applet code="TiffyApplet" archive="<ww:url value="'/Tiffy.jar,TiffyAppletDoc.jar'"/>"
						 id="TiffyApplet" width="800" height="800">
						<param name="options" value ="/nothumbnails /fh">
						<param name="cabbase" value="TiffyApplets.cab">
						<param name="filename" value="<ww:url value="'/viewserver/'"/>tiffApplet/<ww:property value="tiffyID"/>.tiffy">				
					</applet>
			<script type="text/javascript">
				function saveAnnotation(btn)
				{		
					document.getElementById('annotations').value = document.TiffyApplet.getAnnotations(true);
					sendEvent(btn);
					return true;
				}
			</script>
			</ww:if>
			
			<ww:else>
			<img src="<ww:url value="src"/>" alt="" id="imgAtt" />
			
			</ww:else>
			<div id="zoomWindow"></div>
		</div>
	</div>
	<!-- ds:event name="'doSaveAnnotation'" value="getText('Zapisz komentarz')" onclick="'saveAnnotation(this);'"/-->
</ds:available>
<script type="text/javascript">
	function saveAnnotation(btn)
	{		
		document.getElementById('annotations').value = document.TiffyApplet.getAnnotations(true);
		sendEvent(btn);
		return true;
	}
</script>
<!--N koniec content.jsp N-->