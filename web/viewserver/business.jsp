<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<ds:available test="!viewserver2">
<td id="businessDataCell" class="alignTop" width=30%>
</ds:available>

<ds:available test="viewserver2">
	<div id="busDiv" <ww:if test="!viewserverWithData">style="width: 1px; max-width: 1px; height: 20px; max-height: 20px;"</ww:if> >
	<table id="busTable" border=0 class="wrapper">
	<tr>
	<td>
	<script type="text/javascript">
	    function discardObjectAcceptance(acceptanceCn, objectId, button){
            $j("#objectId").val(objectId);
            $j("#acceptanceCn").val(acceptanceCn);
            forceNote('doDiscardObjectAcceptance', button);
        }
		var remarkChanged = 0;
		$j(document).ready(function() {
			$j('fieldset legend').click(function() {
				var ch = $j(this).parent().children().filter(':not(legend, script, .legendFix)');
				var thisId = $j(this).attr('id');
				trace('clicked');
				trace(thisId);
				
				if(!$j(this).prop('open')) {
					trace('hide');
					ch.hide();
					$j(this).prop('open', 'open');
					$j(this).addClass('downArrow');
					$j.cookie(thisId, 'hidden', { path: '/', expires: 360 });
				} else {
					trace('show');
					ch.show();
					$j(this).removeClass('downArrow');
					$j(this).removeProp('open', '');
					$j.cookie(thisId, 'visible', { path: '/', expires: 360 });
				}

				windowResize();
				
			});

			/* przywracamy ustawienia z ciasteczek */
			trace('Loading fieldset cookies ...');
			$j('fieldset legend').each(function(i) {
				
				var ch = $j(this).parent().children().filter(':not(legend, script, .legendFix)');
				var thisId = $j(this).attr('id');
				var cookieState = $j.cookie(thisId);

				if(cookieState == 'hidden') {
					ch.hide();
					$j(this).addClass('downArrow');
					$j(this).attr('open', 'open');
				} 


				return true;
			});
		});
	</script>
</ds:available>

	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
		<ww:hidden name="'fax'" value="fax"/>
		<ww:hidden name="'id'" value="id"/>
		<ww:hidden name="'documentId'" id="documentId" value="documentId"/>
		<ww:hidden name="'activity'" value="activity"/>
		<ww:hidden name="'width'" value="width"/>
		<ww:hidden name="'height'" value="height"/>
		<ww:hidden id="page" name="'page'" value="page"/>
		<ww:hidden id="doChangePage" name="'doChangePage'"/>
		<ww:hidden id="next" name="'next'"/>
		<ww:hidden id="rotate" name="'rotate'"/>
		<ww:hidden id="imgWidth" name="'imgWidth'"/>
		<ww:hidden id="imgHeight" name="'imgHeight'"/>
		<ww:hidden id="documentKindCn" name="'documentKindCn'"/>
		<ww:hidden id="documentKinds" name="'documentKinds'"/>
		<ww:hidden id="document" name="'document'"/>
		<ww:hidden id="pagesParam" name="'pagesParam'"/>
		<ww:hidden id="viewserverDataSize" name="'viewserverDataSize'" />
		<ww:hidden name="'docType'" value="docType" />
		<input type="hidden" name="doTmp" id="doTmp"/>
		
		<script type="text/javascript">
			$j('#busTable').attr('width', $j('#viewserverDataSize').val());
		</script>
		
		<ww:if test="viewserverWithData">
			<ds:available test="dwr.viewserver">
				<jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
				<span class="btnContainer" style="display: block; width: 100%">
					<ds:event value="getText('dwr.document.Aktualizuj')" id="btnZapisz" name="'doUpdateDwr'" cssClass="'btn saveBtn'" onclick="'if(!fieldsManager.validate()) return false;'" disabled="!canUpdate || blocked || save" />
				</span>
			</ds:available>
			<ds:available test="!dwr.viewserver">
			<fieldset>
			<legend id="vsDokumentZwiazany"><ds:lang text="DokumentZwiazanyZtymZalacznikiem"/></legend>
			<!-- div o wysoko�ci 0px, �eby fieldset mia� odpowiedni� szeroko�� (ie6 & ie7) -->
			<div class="legendFix"><nobr>&nbsp;&nbsp;<ds:lang text="DokumentZwiazanyZtymZalacznikiem"/>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></div>
			<table class="tableMargin">
<!--				<tr>-->
<!--					<td colspan="2">-->
<!--						<span class="bold"><ds:lang text="DokumentZwiazanyZtymZalacznikiem"/>:</span>-->
<!--					</td>-->
<!--				</tr>-->
				<tr>
					<td>
						ID:
					</td>
					<td>
						<span class="bold"><ww:property value="documentId"/></span>
					</td>
				</tr>
				<ww:if test="officeNumber != null">
					<tr>
						<td>
							<ds:lang text="NumerKO"/>:
						</td>
						<td>
							<span class="bold"><ww:property value="officeNumber"/></span>
						</td>
					</tr>
				</ww:if>
				<ww:if test="documentAspects != null">
				<tr>
					<td>
						<ds:lang text="AspektDokumentu"/>:
					</td>
					<td>
						<ww:select id="documentAspectCn" name="'documentAspectCn'" list="documentAspects" cssClass="'sel'" listKey="cn" listValue="name" onchange="'changeDockind();'"/>
					</td>
				</tr>
				</ww:if>
				
				<jsp:include page="/common/dockind-fields.jsp"/>
				
				<%-- akceptacje dokumentu --%>
				<ww:if test="acceptancesPage">
					<ww:if test="'invoice_ic' == fm.documentKind.cn || 'sad' == fm.documentKind.cn">
						<jsp:include page="/common/acceptances-ic.jsp"/>
					</ww:if>
					<ww:else>
						<jsp:include page="/common/acceptances.jsp"/>
					</ww:else>
				</ww:if>
					
				<jsp:include page="/common/dockind-specific-additions.jsp"/>


				
				<tr>
					<td colspan="2">
						<ww:if test="fastAssignment">
							<ww:if test="!assignmentsMap.isEmpty">
								<ds:event name="'doAssignments'" value="'Przeka� do...'"/>
								<ww:select name="'assignments'" list="assignmentsMap" cssClass="'sel'"/>
							</ww:if>
						</ww:if>

						<ww:if test="activity != null">
							<input type="hidden" name="processName" id="processName"/>
							<input type="hidden" name="processId" id="processId"/>
							<input type="hidden" name="processAction" id="processAction"/>
							<ww:iterator value="processRenderBeans">
								<ds:render template="template"/>
							</ww:iterator>

							<ww:if test="('invoice_ic' == fm.documentKind.cn || 'sad' == fm.documentKind.cn) ">
								<ds:has-permission name="INVOICE_WITHDRAW">
									<ds:event value="getText('Wycofaj')" cssClass="'btn'" name="'doWithdraw'" id="doWithdraw"/>
								</ds:has-permission>
								<ww:if test="generalAcceptances && acceptanceButtonsForCurrentUserVisible && !fm.getBoolean('AKCEPTACJA_FINALNA')">
									<input type="submit" onclick="if(!validateFormToNext(this,null)) return false;"
									  value="Akceptuj" class="btn" name="doFullAcceptance"/>
									<ds:available test="!acceptances.simplified">
									<input type="submit" onclick="if(!validateFormToUser(this,null)) return false;"
									  value="Akceptuj i przeka� do" class="btn" name="doFullAcceptance"/>
										<ww:select id="userAcceptance" name="'userAcceptance'" list="userToGeneralAcceptance" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
									</ds:available>
									<br/>
								</ww:if>
								<ww:if test="acceptancesCnList.size() > 0 && acceptanceButtonsForCurrentUserVisible">
									<input type="button" id="doDiscardTo" name="doDiscardTo" value="<ww:property value="getText('Odrzu� do..')"/>" onclick="forceNote('doDiscardTo',this);" class="btn"/>
									<ww:if test="acceptancesUserList.size() > 0"><%-- Lista ta mo�e by� pusta --%>
										<ww:select id="discardUserAcceptance" name="'discardUserAcceptance'" list="acceptancesUserList" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
									</ww:if>
									<ds:available test="!acceptances.simplified">
										<ww:select id="discardCnAcceptance" name="'discardCnAcceptance'" list="acceptancesCnList" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
									</ds:available>
								</ww:if>	<ww:else>
									<ds:available test="!acceptances.simplified">
									<%--<ds:event id="doDiscard" name="'doDiscard'" value="getText('Odrzu�')" onclick="'return selectDiscard(this);'" cssClass="'btn'"/>--%>
										<ds:dockinds test="invoice_ic">
											<ds:event id="doDiscardToIM" name="'doDiscardToIM'" value="getText('OdrzucDoIM')" onclick="'return selectDiscard(this);'" cssClass="'btn'"/>
										</ds:dockinds>
									</ds:available>
								</ww:else>

								<ww:if test="fm.getBoolean('AKCEPTACJA_FINALNA')"><%-- zg�oszenie B#1391 --%>
								    <script>
								        function toTheConfirmation() {
								            if(window.opener){
								                window.opener.document.location.href='<ww:url value="'/repository/confirmation.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>';
								                window.opener.focus();
								            } else {
								                document.location.href='<ww:url value="'/repository/confirmation.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>'
								            }
								        }
								    </script>
		                            <input type="button" class="btn" value="Generuj potwierdzenie wp�yni�cia" onclick="toTheConfirmation()"/>
                		        </ww:if>
							</ww:if>
						</ww:if>
					</td>
				</tr>
				<ds:available test="viewer.pudlo">
					<jsp:include page="/common/box.jsp"/>
				</ds:available>		 
			</table>
			</fieldset>
			
			<ww:if test="flagsPresent">
				<br/>
				<fieldset>
				<legend id="vsFlagi"><ds:lang text="Flagi"/></legend>
				<!-- div o wysoko�ci 0px, �eby fieldset mia� odpowiedni� szeroko�� (ie6 & ie7) -->
				<div class="legendFix"><nobr>&nbsp;&nbsp;<ds:lang text="Flagi"/>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></div>
				<jsp:include page="/common/flags-present.jsp"/>
				</fieldset>
			</ww:if>
 
			<br/>
			
			<fieldset>
			<legend id="vsUwagi"><ds:lang text="Uwagi"/></legend>
			<!-- div o wysoko�ci 0px, �eby fieldset mia� odpowiedni� szeroko�� (ie6 & ie7) -->
			<div class="legendFix"><nobr>&nbsp;&nbsp;<ds:lang text="Uwagi"/>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></div>
			
			<table width="100%">
			<tr>
			<td><ds:lang text="TrescUwagi"/>:&nbsp;</td>
			
			<td width="70%">
				<select name="selectForAdnotations" id="selectForAdnotations" class="sel" onchange="writeRemarkToTextarea(this)" style="width: 99%;">
					<option value="-1">
						<ds:lang text="select.wybierz"/>
					</option>
					<ww:iterator value="adnotations">
						<option value="<ww:property value="name"/>">
							<ww:property value="name"/>
						</option>
					</ww:iterator>
				</select>
				<script type="text/javascript">
					/*function setRemark(select){
						var textarea = $j("#content").get(0);
						textarea.value = select.options[select.selectedIndex].value;
						$j("#content").unbind('click').removeClass("italic");
					}*/

					
				</script>
			</td>
			</tr>
			
			
			<tr>
			<td colspan="2">
				<table width="100%">
				<tr>
				<td>
					<ww:textarea  onchange="'remarkChanged=1'" name="'content'" id="content" rows="1" cols="30" cssClass="'txt'" value="getText('WpiszTrescUwagi')"/>
					<script type="text/javascript" src="<%= request.getContextPath() %>/jquery.growfield-1.1.js"></script>
					<script type="text/javascript">
						try {
							var changed = 0;
							$j("#content").css({overflow:"hidden", height:"21px"}).addClass("italic");
							$j("#content").one("click", function() {$j(this).attr("value", "").removeClass("italic"); changed = 1;});
							$j("#content").growfield({min:"21"});
							$j("form").submit(function() {
								if (!changed) $j("#content").attr("value", "");
								return true;
							});
						} catch(e) {}

						function writeRemarkToTextarea(objSelect) {
							var selIndex = objSelect.selectedIndex;

							if(selIndex >= 1) {
								$j('#content').trigger('click');
								$j('#content').val('').text('');

								trace('Writing text: ' + $j($j(objSelect).children()[selIndex]).text());
								
								$j('#content').val($j($j(objSelect).children()[selIndex]).text().trimAll());
								remarkChanged = 1;
								$j('#newNote').attr('selectedIndex', 0);
							}
						}
						
					</script>
				</td>
			
				<td align="right">
					<ds:event value="getText('Zapisz')" name="'doArchive'" cssClass="'btn saveBtn'" onclick="'if (!archiveAction()) return false;'" disabled="!canUpdate || blocked" />
				</td>
				</tr>
				</table>			
			</td>
			
			</tr>
			
			<tr>
			<td colspan="2">
				<table class="remarksTable tableMargin" id="pierwsza">
					<ww:iterator value="remarks">
						<ww:set name="date" scope="page" value="date"/>
						<tr>
							<td>
								<ww:property value="author"/>
							</td>
							<td class="alignRight">
								<fmt:formatDate value="${date}" type="both" pattern="dd-MM-yy HH:mm"/>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="borderBlack1" style="width: 500px;">
								<ww:property value="content"/>
							</td>
						</tr>
					</ww:iterator>
				</table>
	
				<div class="viewserverRemarks">
					<table class="remarksTable tableMargin hidden" id="druga">
					</table>
					<!--[if lte IE 6.5]><iframe></iframe><![endif]-->
				</div>
				
				<img id="img" class="hidden" src="<ww:url value="'/img/arrow_down.gif'"/>" alt="<ds:lang text="PokazHistorie"/>" title="<ds:lang text="PokazHistorie"/>">
				
				<script type="text/javascript">
					if($j("#pierwsza tr").length > 2)
					{
						$j("#pierwsza tr:gt(1)").appendTo("#druga");
						$j("#img").appendTo("#pierwsza td:eq(1)").removeClass("hidden");
						
						/* Teraz chowamy/pokazujemy klikajac (wczesniej byl na hover)*/
						var drugaIsVisible = false;
						$j('#img').click(function() {
							if(!drugaIsVisible) {
								$j('#druga').removeClass('hidden');
								<ds:available test="layout2">
									$j('#druga').css('border', '1px solid #C0C0C0');
								</ds:available>
								$j(this).attr("src", "<ww:url value="'/img/arrow_down_hover.gif'"/>");
							} else {
								$j('#druga').addClass('hidden');
								$j(this).attr("src", "<ww:url value="'/img/arrow_down.gif'"/>");
							}
							drugaIsVisible = !drugaIsVisible;
						});
						
						
						
						
						( ($j.browser.msie && (parseInt($j.browser.version.substr(0, 1)) > 6)) || !($j.browser.msie) ) ? $j('.viewserverRemarks').attr("style", "opacity: 0.85; filter: alpha(opacity=85)") : true ;
						
						$j(document).ready(function(){
							//setTimeout("$j('#druga').width($j('#pierwsza').width() - 1); $j('.viewserverRemarks').width($j('#pierwsza').width());", 500);
							$j('#druga').width($j('#pierwsza').width() - 4 + 'px');
						});
					}
				</script>
			</td>
			</tr>
			</table>
			</fieldset>		
			</ds:available>	
									
			<ds:available test="!archiwizacja.szybkaDekretacja">
			<ww:if test="enableCollectiveAssignments">
				<!-- <h4><ds:lang text="ZbiorczaDekretacja"/></h4> //-->
				
				<br/>
				<fieldset>
				<legend id="vsZbiorczaDekretacja"><ds:lang text="ZbiorczaDekretacja"/></legend>
				<!-- div o wysoko�ci 0px, �eby fieldset mia� odpowiedni� szeroko�� (ie6 & ie7) -->
				<div class="legendFix"><nobr>&nbsp;&nbsp;<ds:lang text="ZbiorczaDekretacja"/>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></div>
				<ww:if test="assignmentsMap != null and !assignmentsMap.isEmpty">
					<table class="tableMargin">
						<ww:iterator value="assignmentsMap">
							<tr>
								<td>
									<input type="checkbox" name="assignments" value="<ww:property value="key"/>"
										id="<ww:property value="key"/>" onchange="updateSelect(this)" />
								</td>
								<td>
									<ww:property value="value" />
								</td>
								<td>
									<select id = "<ww:property value="value" />" name="assignmentProcess_<ww:property value="key"/>"
										class="sel" onchange="updateCheckbox(this);checkSelection()">
										<option value="">
											<ds:lang text="select.wybierz"/>
										</option>
										<ww:iterator value="wfProcesses">
											<option value="<ww:property value="key"/>">
												<ww:property value="value"/>
											</option>
										</ww:iterator>
									</select>
								</td>
							</tr>
						</ww:iterator>
						<tr>
							<td colspan="3">
								<ds:event name="'doAssignments'" value="getText('Dekretuj')"
									onclick="'if (!validateAssignments()) return false;if (!checkSubstitutions()) return false;'" />
								<!--<input type="submit" name="doAssignments" value="Dekretuj" class="btn" onclick="if (!validateAssignments() || !confirm('Na pewno dekretowa�?')) return false; E('doAssignments').value='true';"/>-->
							</td>
						</tr>
					</table>
				</ww:if>
				</fieldset>
			</ww:if>
			</ds:available>
			<ds:available test="barcodeHighlight">
				<br/>
				<fieldset>
				<legend id="vsTest"><ds:lang text="Test"/></legend>
				<div class="legendFix"><nobr>&nbsp;&nbsp;<ds:lang text="Flagi"/>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></div>
				<ww:iterator value="barCodes">
				<a href="<ww:url value="'/viewserver/view.action">
				<ww:param name="'barCode'" value="barCodeString"/>
				<ww:param name="'barCodeX'" value="x"/>
				<ww:param name="'barCodeY'" value="y"/>
				<ww:param name="'barCodePage'" value="pageNr"/>
				<ww:param name="'id'" value="id"/>

				<ww:if test="pageNr < pageCount">
					<ww:param name="'doChangePage'" value="true"/>
					<ww:param name="'page'" value="pageNr"/>	
				</ww:if>
				</ww:url>">
				<ww:property value='pageCount'/>
				<ww:property value="barCodeString"/>, x: <ww:property value="x"/>, y: <ww:property value="y"/>, str: <ww:property value="pageNr"/>
				<br />
				</a>
				</ww:iterator>
				<!-- div o(ie6 & ie7) -->
				</fieldset>
				
			</ds:available>
			
			<br/>
			<input type="hidden" name="doManualFinish" id="doManualFinish"/>
			
			<ww:if test="!externalWorkflow && activity!=null">
				<ds:available test="viewer.ManualFinish">
					<table border=0>
					<tr>
					
					<td>
					<input style="width: 200px" type="submit" value="<ds:lang text="ZakonczPraceZdokumentem"/>" name="doManualFinish" class="btn"
						onclick="if (!confirmFinish()) return false; document.getElementById('doManualFinish').value='true';" <ww:if test="!manualFinishPossible">disabled="true"</ww:if>/>
					<ds:available test="!acceptances.simplified">
						<input style="width: 250px" type="submit" value="<ds:lang text="ZakonczPraceZdokumentemGoToNext"/>" name="doManualFinish" class="btn"
							onclick="if (!confirmFinish()) return false; document.getElementById('doManualFinish').value='true';document.getElementById('next').value='true';" <ww:if test="!manualFinishPossible || !finishAndGoNextPossible">disabled="true"</ww:if>/>
					</ds:available>
					</td>
					
					</tr>
					</table>
				</ds:available>
			</ww:if>
			<ww:if test="showReject">
				<ds:event name="'doReject'" value="getText('Reject')" onclick="'return selectReject(this,null);'"/>
				<ds:event name="'doReject'" value="getText('RejectGoToNext')" onclick="'return selectReject(this,true);'"/>
			</ww:if>
			<ww:if test="showSendToEva && activity != null">
				<ds:event name="'sendToEva'" value="getText('sendToEva')"/>
				<ds:event name="'sendToEva'" value="getText('sendToEvaGoToNext')" onclick="'return selectEva(this,true);'"/>
			</ww:if>
			<ww:if test="evaMessage != null">
				<ww:property value="evaMessage"/>
			</ww:if>
		</ww:if>

<ds:available test="viewserver2">
	</td></tr>
	</div>
</ds:available>

<ds:available test="!viewserver2">
</td>
</ds:available>
<!--N koniec business.jsp N-->