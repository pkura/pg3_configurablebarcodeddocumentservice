<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KalendarzDniWolnych"/> <ww:property value="currentYear"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<!-- DODANIE WPISU -->
<form action="<ww:url value="'/absences/free-days-calendar.action'"/>" method="post" enctype="application/x-www-form-urlencoded">

<table class="search mediumTable">
<thead>
	<tr>
		<th class="empty">
			<label for="dayDate"><ds:lang text="Data dnia wolnego"/></label><span class="star">*</span>
		</th>
		<th class="empty">
			<label for="freeDay.info"><ds:lang text="Opis"/></label>
		</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td>
			<ww:textfield name="'dayDate'"  size="15" maxlength="10" cssClass="'txt'" id="startDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "startDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
		</td>
		<td>
			<ww:textarea name="'freeDay.info'" cssClass="'txt'" cssStyle="'width: 300px; height: 80px;'" />
		</td>
	</tr>
</tbody>
</table>
<div>
	<ds:submit-event value="'Dodaj wpis'" name="'addFreeDay'" cssClass="'btn btnBanner'"/>
</div>

<div class="clearBoth" style="height: 30px"></div>
<!-- END / WYSZUKIWARKA -->



<!-- TABELA Z DANYMI -->
<table class="search mediumTable">
<thead>
<tr>
	<th class="empty">&nbsp;</th>
	<th class="empty">
		<ds:lang text="Data dnia wolnego"/>
	</th>
	<th class="empty">
		<ds:lang text="Opis"/>
	</th>
</tr>
</thead>
<tbody>
<ww:iterator value="freeDays">
<tr>
	<td>
		<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
	</td>
	<td>
		<ds:format-date pattern="dd-MM-yyyy" value="date" />
	</td>
	<td>
		<ww:property value="info" />
	</td>
</tr>
</ww:iterator>
</tbody>
</table>
<!-- END / TABELA Z DANYMI -->

<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane dni wolne?'" name="'deleteFreeDays'" cssClass="'btn'"/>

</form>