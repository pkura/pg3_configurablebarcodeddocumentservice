<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KartyPracownikow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- WYSZUKIWARKA -->
<form id="SearchEmployeeCardsForm" name="SearchEmployeeCardsForm" action="<ww:url value="'/absences/employee-card-list.action'"/>" method="post"
	enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'sortField'" id="sortField" />
	<ww:hidden name="'ascending'" id="ascending" />
	<ww:hidden name="'pageNumber'" id="pageNumber" />

<fieldset class="container">
	<p class="simpleSearch">
		<label for="empCard.user.lastname"><ds:lang text="NazwiskoPracownika"/></label>
		<ww:textfield id="empCard.user.lastname" name="'empCard.user.lastname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.user.firstname"><ds:lang text="ImiePracownika"/></label>
		<ww:textfield id="empCard.user.firstname" name="'empCard.user.firstname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	
	<p>
		<label for="empCard.KPX"><ds:lang text="NumerEwidencyjny" /></label>
		<ww:textfield id="empCard.KPX" name="'empCard.KPX'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p class="simpleSearch">
			<label for="maxResults"><ds:lang text="LiczbaRezultatowNaStronie" /></label>
			<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
	</p>
</fieldset>

<ds:available test="employeeCard.extendedFields">
<fieldset class="container">
	<p>
		<label for="empCard.department"><ds:lang text="Departament"/></label>
		<ww:textfield id="empCard.department" name="'empCard.department'" maxlength="100" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.company"><ds:lang text="NazwaSpolki"/></label>
		<ww:textfield id="empCard.company" name="'empCard.company'" maxlength="100" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.position"><ds:lang text="NazwaStanowiska"/></label>
		<ww:textfield id="empCard.position" name="'empCard.position'" maxlength="255" size="15" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>
</ds:available>
<fieldset class="container auto left">
	<p class="simpleSearchNotVisible">
		<label for="onlyEmployed"><ds:lang text="TylkoZatrudnieni" /></label>
		<ww:checkbox id="onlyEmployedCheckbox" name="'onlyEmployedCheckbox'" value="onlyEmployed" fieldValue="'true'"  />
		<ww:hidden name="'onlyEmployed'" id="onlyEmployed" />
		<script type="text/javascript">
		$j(document).ready(function(){
			$j('#onlyEmployedCheckbox').prev().click(function(){
				var val = document.getElementById("onlyEmployedCheckbox").checked;
				$j('#onlyEmployed').val(val);	
			});
		});	
		</script>
	</p>
	<p class="connect1 topLeft topRight">
		<label class="connectTitle" for=""><ds:lang text="OkresZatrudnienia" /></label>
	</p>
	<p class="connect1 bottomLeft">
		<label for="employmentStartDate"><ds:lang text="OkresZatrudnieniaOd" /></label>
		<ww:textfield id="employmentStartDate" name="'employmentStartDate'"  size="10" maxlength="10" cssClass="'txt date dontFixWidth'" />
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
				<script type="text/javascript">
					Calendar.setup({
		    			inputField     :    "employmentStartDate",     // id of the input field
		    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
		    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
		    			align          :    "Tl",           // alignment (defaults to "Bl")
		    			singleClick    :    true
					});
				</script>
	</p>
</fieldset>

<fieldset class="container auto right">
	<p></p>
	<p></p>
	<p class="connect1 bottomRight topRight">
		<label for="employmentEndDate"><ds:lang text="OkresZatrudnieniaDo" /></label>
		<ww:textfield id="employmentEndDate" name="'employmentEndDate'"  size="10" maxlength="10" cssClass="'txt date dontFixWidth'" />
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="endDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "employmentEndDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "endDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
	</p>	
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Szukaj kart pracownik�w'" name="'doFilter'" cssClass="'btn btnBanner'" cssStyle="'*width: 180px;'"/>
	<a id="searchSwitcher" href="#" extendedSearch="false" >Wyszukiwanie zaawansowane</a>
	<a id="clearFields" href="#" >Wyczy��</a>
</div>

<div class="clearBoth" style="height: 30px"></div>

</form>
<!-- END / WYSZUKIWARKA -->


<!-- TABELA Z DANYMI -->
<form action="<ww:url value="'/absences/employee-card-list.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
<table id="mainTable" class="search" cellspacing="0">
<thead>
	<tr>
		<th class="empty">
			
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="lastName" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="NazwiskoPracownika"/>
			<a href="#" class="sortBy" name="lastName" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="firstName" ascending="true">
				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11"/>
			</a>
			<ds:lang text="ImiePracownika"/>
			<a href="#" class="sortBy" name="firstName" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11"/>
			</a>
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="KPX" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="NumerEwidencyjny"/>
			<a href="#" class="sortBy" name="KPX" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		<ds:available test="employeeCard.extendedFields">
		<th class="empty">
			<a href="#" class="sortBy" name="company" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="NazwaSpolki"/>
			<a href="#" class="sortBy" name="company" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="position" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="NazwaStanowiska"/>
			<a href="#" class="sortBy" name="position" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="department" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="Departament"/>
			<a href="#" class="sortBy" name="department" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		<th class="empty">
			<a href="#" class="sortBy" name="division" ascending="true">
					<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
			<ds:lang text="Dzial"/>
			<a href="#" class="sortBy" name="division" ascending="false">
				<img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>" width="11" height="11" border="0"/>
			</a>
		</th>
		</ds:available>
		<th class="empty">
			<a href="#" class="sortBy" name="employmentStartDate" ascending="true">
				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11"/>
			</a>
			<ds:lang text="DataZatrudnienia"/>
			<a href="#" class="sortBy" name="employmentStartDate" ascending="false">
				<img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>" width="11" height="11"/>
			</a>
		</th>
		<th class="empty">
			<ds:lang text="DataZwolnienia"/>
		</th>
	</tr>
</thead>
<tbody>
<ww:iterator value="employeeCards">
<tr>
	<td>
		<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
	</td>
	
	<ww:if test="externalUser != null">
	
	<td>
		<a href="<ww:url value="'/absences/employee-card.action'"/>?empCard.id=<ww:property value="id"/>">
			<ww:property value="lastName"/>
		</a>
	</td>
	<td>
		<a href="<ww:url value="'/absences/employee-card.action'"/>?empCard.id=<ww:property value="id"/>">
			<ww:property value="firstName"/>
		</a>
	</td>
	
	</ww:if>
	<ww:else>
	
	<td>
		<a href="<ww:url value="'/absences/employee-card.action'"/>?empCard.id=<ww:property value="id"/>">
			<strong><ds:lang text="BrakPrzypisania"/></strong>
		</a>
	</td>
	<td>
		<a href="<ww:url value="'/absences/employee-card.action'"/>?empCard.id=<ww:property value="id"/>">
			<strong><ds:lang text="BrakPrzypisania"/></strong>
		</a>
	</td>
	
	</ww:else>
	<td>
		<a href="<ww:url value="'/absences/employee-card.action'"/>?empCard.id=<ww:property value="id"/>">
			<ww:property value="KPX"/>
		</a>
	</td>
	<ds:available test="employeeCard.extendedFields">
	<td>
		<ww:property value="company"/>
	</td>
	<td>
		<ww:property value="positionName"/>
	</td>
	<td>
		<ww:property value="departmentName"/>
	</td>
	<td>
		<ww:property value="divisionName"/>
	</td>
	</ds:available> 
	<td>
		<ds:format-date pattern="dd-MM-yyyy" value="employmentStartDate"/>
	</td>
	<td>
		<ds:format-date pattern="dd-MM-yyyy" value="employmentEndDate"/>
	</td>
</tr>
</ww:iterator>
<tr> 
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<ds:submit-event value="'Usu� zaznaczone'" confirm="'Zostan� utracone wszystkie dane powi�zane z wybranymi kartami. Czy napewno usun��?'" name="'deleteEmployeeCards'" cssClass="'btn'"/>
	</td>
	<td>
		<input type="button" value="Dodaj now� kart�" class="btn" 
			onclick="document.location.href='<ww:url value="'/absences/employee-card.action'" />'" />
	</td>	
</tr>
</tbody>
</table>
<!-- END / TABELA Z DANYMI -->

<!-- PAGER -->
<table class="table100p">
	<tr>
		<td align="center">
			<ww:set name="pager" scope="request" value="pager"/>
				<jsp:include page="/pager-links-include.jsp"/>
				<script type="text/javascript">
					$j('.pager a').click(function() {
						var page = $j(this).attr('href');
						if (page == '#')
							return false;
						$j('#pageNumber').attr('value', page);
						document.forms.SearchEmployeeCardsForm.submit();
						return false;
					});
				</script>
		</td>
	</tr>
</table>
<!-- END PAGER -->
	<!-- SORTOWANIE DANYCH i inne -->
	<script type="text/javascript">
		showSortArrow($j('#sortField').val());

		$j('.sortBy').click(function(){
				$j('#sortField').attr('value', $j(this).attr('name'));
				$j('#ascending').attr('value', $j(this).attr('ascending'));
				document.forms.SearchEmployeeCardsForm.submit();
				return false;
			});

		bindLabelDecoration();
		initExtendedSearch();
	</script>
</form>