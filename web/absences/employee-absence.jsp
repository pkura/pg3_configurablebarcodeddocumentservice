<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="UrlopPracownika"/>
	<ww:property value="absence.year"/>
	- <ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />	
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/absences/employee-absence.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<h2>
	<input type="button" class="btn" value="<ds:lang text="WrocDoListyUrlopow" />" 
		onclick="location.href='<ww:url value="'/absences/employee-absences.action'">
		 	<ww:param name="'empCard.id'" value="empCard.id" />
	 	<ww:param name="'currentYear'" value="absence.year" />
	 </ww:url>'"/>
</h2>

<ww:hidden name="'absence.id'" />
<ww:hidden name="'absence.year'" />
<ww:hidden name="'empCard.id'" />

<table class="tableMargin">
<tr>
	<td>
		<ds:lang text="IloscDniUrlopu"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'absence.daysNum'" maxlength="3" size="15" />
	</td>
</tr>

<tr>
		<td>
			<ds:lang text="DataOd"/>
			<span class="star">*</span>
		</td>
		<td>
			<ww:textfield name="'timePeriod.startDate'"  size="15" maxlength="10" cssClass="'txt'" id="startDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "startDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
		</td>
</tr>
<tr>
		<td>
			<ds:lang text="DataDo"/>
			<span class="star">*</span>
		</td>
		<td>
			<ww:textfield name="'timePeriod.endDate'"  size="15" maxlength="10" cssClass="'txt'" id="endDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="endDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "endDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "endDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
		</td>
</tr>
<tr>
	<td style="width: 200px;">
		<ds:lang text="WybierzTypUrlopu"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:select name="'selectedAbsenceTypeCode'" list="absencesTypeMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 300px;'" />
	</td>
</tr>

<ds:available test="absences.changeEditor">
<tr>
	<td style="width: 200px;">
		<ds:lang text="ZewnetrznaOsobaAkceptujaca"/>
	</td>
	<td>
		<ww:textfield name="'externalUser'" maxlength="62" size="50"/>
	</td>
</tr>
<tr>
	<td style="width: 200px;">
		<ds:lang text="OsobaAkceptujaca"/>
	</td>
	<td>
		<ww:select size="'10'" multiple="'false'" name="'selectedUserName'" list="usersMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 300px;'" />
	</td>
</tr>
</ds:available>

<tr>
	<td>
		<ds:lang text="UwagiDodatkowe"/>
	</td>
	<td>
		<ww:textarea name="'absence.info'" cssClass="'txt'" cssStyle="'width: 300px; height: 80px;'" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<p>
		<ds:lang text="PoleObowiazkowe"/>
		<span class="star">*</span>
		</p>
	</td>
</tr>
<tr>
	<td colspan="1">
		<ds:submit-event value="'Zapisz urlop'" name="'saveEmployeeAbsence'" cssClass="'btn'"/>
	</td>	
</tr>
</table>

</form>
