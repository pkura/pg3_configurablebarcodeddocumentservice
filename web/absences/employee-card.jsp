<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KartaPracownika"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form name="SaveEmployeeCardForm" action="<ww:url value="'/absences/employee-card.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<h2>
	<input type="button" value="<ds:lang text="WrocDoListy" />" class="btn"
		onclick="document.location.href='<ww:url value="'/absences/employee-card-list.action'"/>'"/>
	
</h2>

<ww:hidden name="'empCard.id'" />
<ww:hidden name="'doChangeDepartment'" id="doChangeDepartment" />

<table class="tableMargin">
<tr>
	<td>
		<ds:lang text="NumerEwidencyjny"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'empCard.KPX'" maxlength="50" size="20" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="NazwaSpolki"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'empCard.company'" maxlength="100" size="50"/>
	</td>
</tr>
<ds:available test="employeeCard.extendedFields">

<tr>
	<td>
		<ds:lang text="NazwaStanowiska"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:select name="'empCard.position'" list="positionsMap" listKey="key"
			listValue="value" cssClass="'sel'" cssStyle="'width: 250px;'" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Departament"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:select name="'empCard.department'" id="selectDepartment" list="departmentsMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 250px;'" />
		<script type="text/javascript">
			/*$j('#selectDepartment').change(function(){
					$j('#doChangeDepartment').attr('value', 'true');
					document.forms.SaveEmployeeCardForm.submit();
				});*/
		</script>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Dzial"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:select name="'empCard.division'" list="divisionsMap" listKey="key"
			listValue="value" cssClass="'sel'" cssStyle="'width: 250px;'" />
	</td>
</tr>
</ds:available>
<tr>
		<td>
			<ds:lang text="DataZatrudnienia"/>
			<span class="star">*</span>
		</td>
		<td>
			<ww:textfield name="'employmentStartDate'"  size="20" maxlength="10" cssClass="'txt dontFixWidth'" id="employmentStartDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="startDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "employmentStartDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "startDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
		</td>
</tr>
<tr>
		<td>
			<ds:lang text="DataZwolnienia"/>
		</td>
		<td>
			<ww:textfield name="'employmentEndDate'"  size="20" maxlength="10" cssClass="'txt dontFixWidth'" id="employmentEndDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="endDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "employmentEndDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "endDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
		</td>
</tr>
<ds:available test="employeeCard.extendedFields">
<tr>
	<td style="width: 200px;">
		<ds:lang text="UzytkownikZewnetrzny"/>
	</td>
	<td>
		<ww:if test="empCard != null && empCard.user != null">
			<ww:textfield name="'empCard.externalUser'" value="" maxlength="62" size="50"/>
		</ww:if>
		<ww:else>
			<ww:textfield name="'empCard.externalUser'" maxlength="62" size="50"/>
		</ww:else>
	</td>
</tr>
</ds:available>
<tr>
	<td style="width: 200px;">
		<ds:lang text="WybierzUzytkownika"/>
	</td>
	<td>
		<ww:select size="'10'" multiple="'false'" name="'selectedUserName'" list="usersMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 300px;'" />
	</td>
</tr>
<ds:available test="employeeCard.hasSpecialAbsences">
<tr>
	<td>
		<ds:lang text="Urlop 188Kp"/>
	</td>
	<td>
		<table>
			<tr>
				<td>
					<ww:checkbox name="'empCard.T188KP'" fieldValue="true" value="empCard.T188KP"/>
				</td>
				<td>
					<ds:lang text="Liczba dni"/>
					<ww:textfield name="'empCard.t188kpDaysNum'"  size="10" maxlength="10" cssClass="'txt dontFixWidth'" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Urlop ojcowski UJ"/>
	</td>
	<td>
		<table>
			<tr>
				<td>
					<ww:checkbox name="'empCard.TUJ'" fieldValue="true" value="empCard.TUJ"/>
				</td>
				<td>
					<ds:lang text="Liczba dni"/>
					<ww:textfield name="'empCard.tujDaysNum'"  size="10" maxlength="10" cssClass="'txt dontFixWidth'" />
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Urlop inwalidzki INW"/>
	</td>
	<td>
		<table>
			<tr>
				<td>
					<ww:checkbox name="'empCard.TINW'" fieldValue="true" value="empCard.TINW"/>
				</td>
				<td>
					<ds:lang text="Liczba dni"/>
					<ww:textfield name="'empCard.invalidAbsenceDaysNum'"  size="10" maxlength="10" cssClass="'txt dontFixWidth'" />
				</td>
			</tr>
		</table>
	</td>
</tr>
</ds:available>
<tr>
	<td>
		<ds:lang text="UwagiDodatkowe"/>
	</td>
	<td>
		<ww:textarea name="'empCard.info'" cssClass="'txt'" cssStyle="'width: 300px; height: 80px;'" />
	</td>
</tr>

<ds:available test="employeeCard.multipleCards">
<ww:if test="empCard == null || empCard.id == null">
	<tr>
		<td>
			<ds:lang text="TransferUrlopowZPoprzedniejKarty"/>
		</td>
		<td>
			<ww:checkbox name="'transferAbsences'" fieldValue="true" value="transferAbsences"/>
		</td>
	</tr>
</ww:if>
</ds:available>

<tr>
	<td colspan="2">
		<p>
		<ds:lang text="PoleObowiazkowe"/>
		<span class="star">*</span>
		</p>
	</td>
</tr>
<tr>
	<td colspan="1">
		<ds:submit-event value="'Zapisz kart�'" name="'doSave'" cssClass="'btn'"/>
	</td>	
</tr>
</table>

</form>
