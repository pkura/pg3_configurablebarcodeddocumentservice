<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:if test="empCard != null">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
	<ds:lang text="RaportZWykorzystaniaUrlopu"/> <ww:property value="currentYear"/>
	- <ww:if test="empCard.user != null">
	  	<ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />
	  </ww:if> 	
	  <ww:else>
	  	<ww:property value="empCard.externalUser" />
	  </ww:else>
	</h1>
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions>
	<ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
</ww:if>
<ww:else>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
		<ds:lang text="UtworzNowyWpisWymiaruUrlopowego"/> 	
	</h1>
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions>
	<ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
</ww:else>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>


<ww:if test="empCard != null">

<h2>
	<input type="button" class="btn btnBanner" value="<ds:lang text="WrocDoRaportuZWykorzystaniaUrlopu" />"
		onclick="location.href='<ww:url value="'/absences/absences-list.action'">
		 	<ww:param name="'currentYear'" value="currentYear" />
		 </ww:url>'" />
</h2>


<form action="<ww:url value="'/absences/employee-absences.action'"/>" method="post" enctype="application/x-www-form-urlencoded">

<ds:submit-event value="'Generuj raport XLS'" name="'generateReport'" cssClass="'btn btnBanner'"/>

<ww:hidden name="'empCard.id'" />
<ww:hidden name="'currentYear'" />

<!-- LISTA URLOP�W PRACOWNIKA -->
<table class="search bottomSpacer table100p" id="mainTable" cellspacing="0">
<thead>
	<tr>
		<th class="empty">
			<ds:lang text="Edytuj"/>
		</th>
		<th class="empty">
			<ds:lang text="Lp"/>	
		</th>
		<th class="empty">
			<ds:lang text="NazwiskoImiePracownika"/>	
		</th>
		<th class="empty">
			<ds:lang text="DataOd"/>
		</th>
		<th class="empty">
			<ds:lang text="DataDo"/>
		</th>
		<th class="empty">
			<ds:lang text="LiczbaDni"/>
		</th>
		<th class="empty">
			<ds:lang text="TypUrlopu"/>
		</th>
		<th class="empty">
			<ds:lang text="PozostaloDniUrlopu"/>
		</th>
		<th class="empty">
			<ds:lang text="DodatkoweUwagi"/>
		</th>		
	</tr>
</thead>
<tbody>
<ww:iterator value="emplAbsences">
	<tr>
		<td>
			<a href="<ww:url value="'/absences/employee-absence.action'">
						<ww:param name="'empCard.id'" value="empCard.id" />
						<ww:param name="'absence.year'" value="currentYear" />
					 	<ww:param name="'absence.id'" value="absence.id" />
					 </ww:url>">
				<ds:lang text="edytuj" />
			</a>
		</td>
		<td>
			<ww:property value="lp"/>
			<ww:checkbox name="'deleteIds'" fieldValue="absence.id" value="false" />
		</td>
		<td>
			<ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />
		</td>
		<td>
			<ww:property value="absence.startDate" />
		</td>
		<td>
			<ww:property value="absence.endDate" />
		</td>
		<td>
			<ww:property value="absence.daysNum"/>
		</td>
		<td>
			<ww:property value="absence.absenceType.name"/>
		</td>
		<td>
			<ww:property value="restOfDays"/>
		</td>
		<td>
			<ww:property value="absence.info"/>
		</td>		
	</tr>
</ww:iterator>
</tbody>
</table>
<!-- END / LISTA URLOP�W PRACOWNIKA -->
<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane urlopy?'" name="'deleteAbsences'" cssClass="'btn cancelBtn'"/>
<input type="button" value="Dodaj nowy urlop" class="btn" 
	onclick="document.location.href='<ww:url value="'/absences/employee-absence.action'">
										<ww:param name="'empCard.id'" value="empCard.id" />
									 	<ww:param name="'absence.year'" value="currentYear" />
									 </ww:url>'" />
</form>

</ww:if>
<ww:else>

<!-- FORMULARZ TWORZENIA NOWEGO WPISU WYMIARU URLOPOWEGO DLA WYBRANEGO PRACOWNIKA -->
<form name="CreateNewAbsencesForm" action="<ww:url value="'/absences/employee-absences.action'"/>" method="post" enctype="application/x-www-form-urlencoded">

<table class="tableMargin">
<tr>
	<td>
		<ds:lang text="WybierzRok" />
		<span class="star">*</span>
	</td>
	<td colspan="5">
		<ww:select name="'currentYear'" list="yearsMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 100px;'" 
			onchange="'document.forms.CreateNewAbsencesForm.submit();'" />
	</td>
</tr>
<tr>
	<td width="200">
		<ds:lang text="WybierzKartePracownika" />
		<span class="star">*</span>
	</td>
	<td>
		<ww:select size="'10'" multiple="'false'" name="'selectedEmployeeCardId'" list="employeeCardsMap" listKey="key" 
			listValue="value" cssClass="'sel'" cssStyle="'width: 250px;'" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="WymiarUrlopuNalezny" />
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'reportEntry.dueDays'" maxlength="10" size="5" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="WymiarUrlopuZalegly" />
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'reportEntry.overdueDays'" maxlength="10" size="5" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="DodatkoweUwagi" />
	</td>
	<td>
		<ww:textarea name="'reportEntry.info'" cssStyle="'width: 300px;'" cssClass="'txt'" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<ds:submit-event value="'Utw�rz nowy wpis'" name="'createNewAbsences'" cssClass="'btn'"/>
	</td>
</tr>
</table>
</form>
<!-- END / FORMULARZ TWORZENIA NOWEGO WPISU WYMIARU URLOPOWEGO DLA WYBRANEGO PRACOWNIKA -->

</ww:else>
