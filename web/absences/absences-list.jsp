<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="RaportZWykorzystaniaUrlopu"/> <ww:property value="currentYear"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<!-- WYSZUKIWARKA -->
<form id="SearchAbsencesForm" name="SearchAbsencesForm" action="<ww:url value="'/absences/absences-list.action'"/>" method="post"
	enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'pageNumber'" id="pageNumber" />
	
<fieldset class="container">
	<p class="simpleSearch">
		<label for="empCard.user.lastname"><ds:lang text="NazwiskoPracownika"/></label>
		<ww:textfield id="empCard.user.lastname" name="'empCard.user.lastname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.user.firstname"><ds:lang text="ImiePracownika"/></label>
		<ww:textfield id="empCard.user.firstname" name="'empCard.user.firstname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.KPX"><ds:lang text="NumerEwidencyjny" /></label>
		<ww:textfield id="empCard.KPX" name="'empCard.KPX'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p class="simpleSearch">
		<label for="currentYear"><ds:lang text="WybierzRok" /></label>
		<ww:select id="currentYear" name="'currentYear'" list="yearsMap" listKey="key" 
			listValue="value" cssClass="'sel dontFixWidth'" cssStyle="'width: 100px;'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="empCard.company"><ds:lang text="NazwaSpolki" /></label>
		<ww:textfield id="empCard.company" name="'empCard.company'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.department"><ds:lang text="NazwaDepartamentu"/></label>
		<ww:textfield id="empCard.department" name="'empCard.department'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.position"><ds:lang text="NazwaStanowiska"/></label>
		<ww:textfield id="empCard.position" name="'empCard.position'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p class="simpleSearch">
			<label for="maxResults"><ds:lang text="LiczbaRezultatowNaStronie" /></label>
			<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
	</p>
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Szukaj wpis�w'" name="'goSearch'" cssClass="'btn btnBanner'"/>
	<ds:submit-event value="'Generuj raport XLS'" name="'generateReport'" cssClass="'btn btnBanner'"/>
	<a id="searchSwitcher" href="#" extendedSearch="false" >Wyszukiwanie zaawansowane</a>
	<a id="clearFields" href="#" >Wyczy��</a>
</div>

<div class="clearBoth" style="height: 30px"></div>

</form>
<!-- END / WYSZUKIWARKA -->



<!-- TABELA Z DANYMI -->
<form action="<ww:url value="'/absences/absences-list.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
<ds:submit-event value="'Nalicz urlopy na bie��cy rok'" name="'chargeAbsences'" cssClass="'btn'" disabled="!chargeAbsencesEnabled" />
<table class="search bottomSpacer" id="mainTable" cellspacing="0">
<thead>
<tr>
	<th class="empty">
		<ds:lang text="PodgladEdycja" />
	</th>
	<th class="empty">
		<ds:lang text="Karta" />
	</th>
	<th class="empty">
		<ds:lang text="Lp"/>
	</th>
	<th class="empty">
		<ds:lang text="NumerEwidencyjny"/>
	</th>
	<th class="empty">
		<ds:lang text="NazwiskoImiePracownika"/>
	</th>
	<th class="empty">
		<ds:lang text="WymiarUrlopuNalezny"/>
	</th>
	<th class="empty">
		<ds:lang text="WymiarUrlopuZalegly"/>
	</th>
	<th class="empty">
		<ds:lang text="WymiarUrlopuDodatkowy"/>
	</th>
	<th class="empty">
		<ds:lang text="WymiarUrlopuWykorzystany"/>
	</th>
	<th class="empty">
		<ds:lang text="WymiarUrlopuPozostalego"/>
	</th>
</tr>
</thead>
<tbody>
<ww:iterator value="absences">
<tr>
	<td>
		<a href="<ww:url value="'/absences/employee-absences.action'">
				 	<ww:param name="'empCard.id'" value="empCardId" />
				 	<ww:param name="'currentYear'" value="currentYear" />
				 </ww:url>">
			<ds:lang text="PodgladEdycja" />
		</a>
	</td>
	<td>
		<a href="<ww:url value="'/absences/absences-list.action'">
				 	<ww:param name="'empCard.externalUser'" value="login" />
				 	<ww:param name="'currentYear'" value="currentYear" />
				 	<ww:param name="'doPdf'" value="'true'" />
				 </ww:url>">
			<ds:lang text="pdf" />
		</a>
	</td>
	<td>
		<ww:property value="lp" />
		<ww:checkbox name="'deleteIds'" fieldValue="empCardId" value="false" />
	</td>
	<td>
		<ww:property value="KPX" />
	</td>
	<td>
		<ww:property value="name" />
	</td>
	<td>
		<ww:property value="dueDays" />
	</td>
	<td>
		<ww:property value="overdueDays" />
	</td>
	<td>
		<ww:property value="additionalDays" />
	</td>
	<td>
		<ww:property value="executedDays" />
	</td>
	<td>
		<ww:property value="restOfDays" />
	</td>
</tr>
</ww:iterator>
</tbody>
</table>
<!-- END / TABELA Z DANYMI -->

<!-- PAGER -->
<table class="table100p">
	<tr>
		<td align="center">
			<ww:set name="pager" scope="request" value="pager"/>
				<jsp:include page="/pager-links-include.jsp"/>
				<script type="text/javascript">
					$j('.pager a').click(function() {
						var page = $j(this).attr('href');
						if (page == '#')
							return false;
						$j('#pageNumber').attr('value', page);
						$j('#generateReport').attr('value', '');
						document.forms.SearchAbsencesForm.submit();
						return false;
					});
				</script>
		</td>
	</tr>
</table>

<ww:hidden name="'currentYear'" />
<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane urlopy?'" name="'deleteEmployeeAbsences'" cssClass="'btn'"/>
<input type="button" value="Dodaj nowy wpis" class="btn" 
			onclick="document.location.href='<ww:url value="'/absences/employee-absences.action'">
											 	<ww:param name="'currentYear'" value="currentYear" />
											 </ww:url>'" />

</form>
<script type="text/javascript">
	bindLabelDecoration();
	initExtendedSearch();
</script>