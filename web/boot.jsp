<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: layout (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.08.08
	Poprzednia zmiana: 01.08.08
C--%>
<!--N boot.jsp N-->

<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="java.io.PrintWriter"%>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2005-09-09
  Time: 13:57:59
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=iso-8859-2" language="java" %>
<html>
	<head>
		<title><ds:lang text="TytulStrony"/> - uruchamianie</title>
		
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
		<meta http-equiv="refresh" content="2; url=<%= request.getContextPath() %>/">
	</head>
	
	<body style="font-family: Verdana, sans-serif; font-size: 10pt; text-align: center;">
		<div style="width: 50%; margin: 20px auto; text-align: center;">
			<p>Aplikacja Docusafe jest uruchamiana. Prosz� czeka�.</p>
			
			<% int rl = Docusafe.getRunlevel(); %>
			
			<p>Aktualny poziom uruchamiania: <%= rl + " (" + Docusafe.getRunlevelName(rl) + ")" %>.</p>
			
			<% if (Docusafe.failed()) { %>
				<p>
					Wyst�pi� b��d podczas uruchamiania aplikacji:
					<br/>
					<pre><%= Docusafe.getThrowable().toString() %></pre>
				</p>
				<code>
					<pre>
						<% Docusafe.getThrowable().printStackTrace(new PrintWriter(out)); %>
					</pre>
				</code>
			
			<% } else if (rl == Docusafe.RL_RESTART) { %>
				<p>Serwer aplikacyjny wymaga restartu po aktualizacji wersji.</p>
			
			<% } else { %>
				<!-- wska�nik post�pu uruchamiania -->
				<div>
					<div style="float: left; text-align: left;">
						0%
					</div>
					<div style="float: right; text-align: right;">
						100%
					</div>
					<div>
						50%
					</div>
					<div style="clear: both; height: 16px;  border: 1px solid gray; background: -webkit-gradient(linear, left top, left bottom, from(#eee), to(#e2e2e2)); background: -moz-linear-gradient(center top , #EEEEEE, #E2E2E2); *background-color: #eee;">
						<div style="float: left; background: -webkit-gradient(linear, left top, left bottom, from(blue), to(navy));  background: -moz-linear-gradient(center top , blue, navy); *background-color: navy; width:<%= rl > 0 ? (int) (100 * rl / (double) Docusafe.RL_RUNNING) : 0 %>%">
							&nbsp;
						</div>
					</div>
				</div>
			<% } %>
		</div> <!-- main -->
	</body>
</html>

<!--N koniec boot.jsp N-->