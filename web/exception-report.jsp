<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N exception-report.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.io.StringWriter"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%
    Object object = request.getAttribute("exception");
    Exception exception = object instanceof Exception ? (Exception) object : null;
%>

<script language="JavaScript">
function toggle_div(id)
{
    var div = document.getElementById(id);
    if (div.style.display == 'none')
    {
        div.style.display = '';
    }
    else
    {
        div.style.display = 'none';
    }
}
</script>

<% if (exception == null) { %>
<h2><edm-html:message key="exception.unknown"/></h2>
<% } else { %>
<h2>Wyst�pi� b��d: <%= exception.getMessage() != null ? exception.getMessage() : exception.getClass().getName() %></h2>

<p>W celu zg�oszenia b��du wy�lij t� stron� poczt� elektroniczn� na adres
qa@docusafe.pl.  Stron� mo�na wys�a� poczt� wybieraj�c z menu Plik opcj�
"Wy�lij", a nast�pnie "Strona poczt� email...".</p>

<a href="javascript:void(toggle_div('trace'));">szczeg�y</a>
<div id="trace" style="display: none; border: 1px solid gray;">
<pre id="content">
<%
    Throwable t = exception;
    PrintWriter wrappedOut = new PrintWriter(out);
    exception.printStackTrace(wrappedOut);

    while (t != null && t.getCause() != null)
    {
        out.print("Caused by: ");
        t = t.getCause();
        t.printStackTrace(wrappedOut);
    }
%>
</pre>
</div>
<% } %>
