var gmenuFilters = [];
var gmenuForms = [];
var gmenuButtons = [gmenuFilters];
var gmenuButtonsId = [];
var gmenuFiltersScroll = [];
var gmenuNextTab = [];
var ie = document.all && !window.opera ? 1 : 0;
var found = -1;	// index of found filter
var gmenuArrowSrc = '';

var gmenuDefaultPatterns = [];
var gmenuDefaultButtons = [];

var gmenuCurrentButton = -1;
var gmenuIsUp = false;

var gmenuMaxFilters = 6;
var gmenuMaxHeight = 100;
var gmenuIsShow = false;
var gmenuSelectedItem = 0;
var gmenuSelectByKey = false;
var gmenuFoundList = new Array();
var __gmenuAuto = '__gmenuAuto';

/* by nie trzeba by�o podawa� id*/
var gmenuIdGenerator = 0;

/* specjalnie dla ie */
function GetElementPosition(xElement){

  var selectedPosX = 0;
  var selectedPosY = 0;
  var theElement = document.getElementById(xElement);
  var ret = [];
              
  while(theElement != null){
    selectedPosX += theElement.offsetLeft;
    selectedPosY += theElement.offsetTop;
    theElement = theElement.offsetParent;
  }
  
  ret[0] = selectedPosX;
  ret[1] = selectedPosY;
                        		      		      
  /*return selectedPosX + "," + selectedPosY*/
  return ret;

}

function gmenuFixSearchWidth(btnIndex) {
	var forScroll = 0;
	var marginsEtc = 33;
	
	if(!ie)
		marginsEtc = 34;
	
	if(gmenuFiltersScroll[btnIndex]) {
		forScroll = 4;
	}
	
	document.getElementById('gmenu_searcher').style.width = document.getElementById(gmenuButtonsId[btnIndex]).offsetWidth - 
		marginsEtc - forScroll + 'px';
}

function gmenuChangeButtonState(btnIndex, isPushed) {
	var btn_el = document.getElementById(gmenuButtonsId[btnIndex]);
	var str = gmenuButtonsId[btnIndex];
	str += '_img';
	//window.status = str;
	var img_el = document.getElementById(str);

	if(isPushed) {
		/*btn_el.style.backgroundImage = "url('btn_back_pushed.gif')";
		btn_el.style.color = 'white';*/
		btn_el.className = 'gmenuButtonPushed';
		img_el.src = gmenuArrowSrc.replace('down', 'up');
	}
	else {
		/*btn_el.style.backgroundImage = "url('btn_back.gif')";
		btn_el.style.color = 'black';*/
		btn_el.className = 'gmenuButton';
		img_el.src = gmenuArrowSrc;
	}
}

function gmenuButtonOver(id, isOver) {
	if(isOver)
		id.style.borderColor = '#888888';
	else
		id.style.borderColor = '#C0C0C0';
}


function btn_dbg(btn, isOver) {
	var el = document.getElementById(btn.id);
	var dbg_el = document.getElementById('gmenu_debug');
	
	dbg_el.innerHTML = '';
	if(!isOver)
		return;
	
	dbg_el.innerHTML += 'Left: ' + el.offsetLeft + '<br>';
	dbg_el.innerHTML += 'Top: ' + el.offsetTop + '<br>';
	dbg_el.innerHTML += 'pixelLeft: ' + el.style.pixelLeft + '<br>';
	dbg_el.innerHTML += 'pixelTop: ' + el.style.pixelTop + '<br>';
	dbg_el.innerHTML += 'offsetWidth: ' + el.offsetWidth + '<br>';
	dbg_el.innerHTML += 'offsetHeight: ' + el.offsetHeight + '<br>';
	dbg_el.innerHTML += 'clientWidth: ' + el.clientWidth + '<br>';
	dbg_el.innerHTML += 'clientHeight: ' + el.clientHeight + '<br>';
	dbg_el.innerHTML += 'IE Position X: ' + GetElementPosition(btn.id)[0] + '<br>';
	dbg_el.innerHTML += 'IE Position Y: ' + GetElementPosition(btn.id)[1] + '<br>';
}

function gmenuAddFilter(btnIndex, filterName) {
	gmenuButtons[btnIndex].push(filterName);
}

var printArray = function(x, idx) {
	document.writeln(x);
}

function flt_dbg(index) {
	gmenuButtons[index].forEach(printArray);
}

function replaceClass(id, str) {
		id.className = str;
}

function gmenuRefreshActiveFilter(formId, btnIndex) {
	var btn_el = document.getElementById(gmenuButtonsId[btnIndex]);
	var form_el = document.getElementById(formId);
	var length = form_el.length;
	for(i = 0; i < length; i ++) {
		if(form_el.options[i].selected) {
		str = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />';
		if(gmenuDefaultPatterns[btnIndex] == form_el.options[i].text)
			str += gmenuDefaultButtons[btnIndex];
		else
			str += form_el.options[i].text;	
													
		btn_el.innerHTML = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />' +
		form_el.options[i].text;
		btn_el.innerHTML = str;
		}
	}
	gmenuChangeButtonState(btnIndex, false);	
}

function gmenuFixButtonLength(btn_el, flt_el, btnIndex) {
	traceStart('gmenuFixButtonLength');
	
	if(btn_el.offsetWidth > 280)
		btn_el.style.width = '280px';
	
	trace('sprawdzam wysokosc przycisku filtrow: ', btn_el);
	/* jedna literka zajmuje jakies 7 pikseli (20px padding) */
	var maxTxtLength = Math.floor((btn_el.offsetWidth - 20) / 7,5) - 4;	// 7,5 dla bezpieczenstwa, 4 - zeby zmiesci dwukropek
	if(btn_el.offsetHeight > 22) {
		trace('wiecej niz 22 piksele');
		trace('zamieniam ' + flt_el.text + ' na ' + flt_el.text.slice(0, maxTxtLength));
		btn_el.innerHTML = btn_el.innerHTML.replace(flt_el.text, flt_el.text.slice(0, maxTxtLength)) + ' ...';
		
		$j('#' + gmenuButtonsId[btnIndex]).attr('title', flt_el.text).wTooltip({style: false, className: "dsTooltip", 
		offsetY: 20, fadeIn: 400, delay: 400});
		
		/* trzeba przywrocic onmouseover, bo wtooltip usunal */
		function mo(event) {
			traceStart('gmenu.js --> gmenuSelectFilter --> mo');
			trace('Over z funkcji mo, isOver: ' + event.data.isOver);
			
			gmenuButtonOver(event.data.obj, event.data.isOver);
			
			traceEnd();
		}
		$j(btn_el).unbind('mouseover', mo);
		$j(btn_el).bind('mouseover', {obj: btn_el, isOver:true}, mo);
		$j(btn_el).unbind('mouseout', mo);
		$j(btn_el).bind('mouseout', {obj: btn_el, isOver:false}, mo);
		
		//alert();
	} else {
		$j('#' + gmenuButtonsId[btnIndex]).attr('title', '');
	}
	
	traceEnd();
}

function gmenuSelectFilter(name, btnIndex) {
	traceStart('gmenu.js --> gmenuSelectFilter for button ' + btnIndex);
	
	var str = name.id;
	var form_el = document.getElementById(str); 
	var btn_el = document.getElementById(gmenuButtonsId[btnIndex]);
	var num;
	str = str.replace('td_filter_', '');
	num = parseInt(str);
	found = num;
	//alert("btnIndex = " + btnIndex+" "+ gmenuForms[btnIndex] + " " + document.getElementById(gmenuForms[btnIndex]));
	var flt_el = document.getElementById(gmenuForms[btnIndex]).options[found];

	flt_el.selected = true;
	
	gmenuHideFilters();
	str = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />';
	if(gmenuDefaultPatterns[btnIndex] == flt_el.text)
		str += gmenuDefaultButtons[btnIndex];
	else
		str += flt_el.text;	
		
	btn_el.innerHTML = str;
	gmenuChangeButtonState(btnIndex, false);
	
	gmenuFixButtonLength(btn_el, flt_el, btnIndex);
	
	trace('calling gmenuSwitchNextTab: ' + gmenuSelectByKey);
	
	if(gmenuSelectByKey)
		gmenuSwitchNextTab(btnIndex);
	traceEnd();
}

function tc()
{
	document.getElementById('tc').innerHTML = gmenuSelectByKey;
	t=setTimeout("tc()",10);
}

function gmenuSelectFilterChb(name, btnIndex) {
	var str = name.id; 
	var btn_el = document.getElementById(gmenuButtonsId[btnIndex]);
	var num;
	str = str.replace('td_filter_', '');
	num = parseInt(str);
	found = num;
	//var flt_el = document.getElementById(gmenuForms[btnIndex]).options[found];
	//alert(gmenuForms[btnIndex]);
	//alert(str);
	
	var jForm_el = $j('#' + gmenuForms[btnIndex]);
	var chb_el = $j('#' + 'td_filter_' + found).children().get(1);
	var span_el = $j('#' + 'td_filter_' + found).children().get(0);
	
	if(jForm_el.children()[found].checked) {
		jForm_el.children()[found].checked = false;
		chb_el.checked = false;
		CustomDynamic.clear();
		//span_el.onmouseup;
	} else {
		jForm_el.children()[found].checked = true;
		chb_el.checked = true;
		CustomDynamic.clear();
	}
	
	// TUTEJ poprawic grafike
	
	//jChb_el.checked = jForm_el.children()[found].checked;
	//jChb_el.handleEvent();
	
	/*gmenuHideFilters();
	str = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />';
	if(gmenuDefaultPatterns[btnIndex] == flt_el.text)
		str += gmenuDefaultButtons[btnIndex];
	else
		str += flt_el.text;	
		
	btn_el.innerHTML = str;
	gmenuChangeButtonState(btnIndex, false);*/
}

function gmenuSwitchNextTab(btnIndex) {
	traceStart('gmenuSwitchNextTab');
	
	
	trace(gmenuSelectByKey);
	trace($j('#focus_btn_' + btnIndex).attr('id'));
	
	gmenuHideFilters();
	
	var nextTab = parseInt($j('#focus_btn_' + btnIndex).attr('tabindex')) + 1;
	if(nextTab > __tabIndexCount)
		nextTab = 0;
		
	if(gmenuSelectByKey)
		$j('[tabindex=' + nextTab + ']').focus();
	
	traceEnd();
}

/* buduje filtry spelniajacymi pattern */
function buildFiltersDiv(btnIndex, pattern) {
	traceStart('buildFiltersDiv');

	var container_el = document.getElementById('gmenu_container');
	var shadow_el = document.getElementById('gmenu_shadow');
	var isScroll = gmenuFiltersScroll[btnIndex];
	var s = '';
	var fi = 0;	// found index
	var fs = '';	// found string
	
	/* czyscimy wszystko */
	container_el.innerHTML = '';
	
	/* wy?ietlamy wyszukiwarke */
	s += '<table cellspacing=0 cellpadding=0 class="filter"><tbody>';
	s += '<tr class="filter"><td class="filter_left"></td>';
	s += '<td class="filter"><input type="text" class="filter" autocomplete="off"  id="gmenu_searcher" name="gmenu_search"';
	
	if(gmenuNextTab[btnIndex] != '') {
		trace('nextTab: ' + gmenuNextTab[btnIndex]);
		s+= ' onkeydown="return keyDown(event, ' + btnIndex + ', ' + gmenuNextTab[btnIndex] + ');"';
	} 
	s+= ' onkeyup="return keyUp(event, ' + btnIndex + ');"></td></tr>';
	
	if(isScroll) {
		s += '</tbody></table>';
		s += '<div id="gmenu_scroll" style="display: block; float: left; overflow: auto; overflow-x: hidden; height: ' + 'auto' + ';">';
		s+= '<table cellspacing=0 cellpadding=0 class="filter" id="gmenu_scroll_table"><tbody>';
	}
	
	found = -1;	
	
	//traceInfo('reseting gmenuFoundList');
	gmenuFoundList = [];
	for(i = 0; i < gmenuButtons[btnIndex].length; i ++) {
		
		if(pattern == '') {
			s += '<tr class="filter"><td class="filter_left"></td><td id="td_filter_' + i + '" class="filter" onmouseover="replaceClass(this, \'over\');" 	onmouseout="replaceClass(this, \'filter\')" onclick="gmenuSelectFilter(this,' + btnIndex + ')">';
			s += gmenuButtons[btnIndex][i];
			s += '</tr></td>';
			gmenuFoundList.push('td_filter_' + i);
		}
		else {
			var pat = new RegExp(pattern, 'i');
	
			if((fs=gmenuButtons[btnIndex][i].match(pat)) != null) {
				if(found == -1) {
					found = i;
				}
				var str = gmenuButtons[btnIndex][i];
				var boldPattern = '<b class="found">' + fs + '</b>';
				str = str.replace(fs, boldPattern);
				s += '<tr class="filter"><td class="filter_left"></td><td id="td_filter_' + i + '" class="filter" onmouseover="replaceClass(this, \'over\')" 	onmouseout="replaceClass(this, \'filter\')" onclick="gmenuSelectFilter(this,' + btnIndex + ')">';
				s += str;
				s += '</tr></td>';
				//trace(gmenuFoundList.push('td_filter_' + i));
				gmenuFoundList.push('td_filter_' + i);
			}
			
			
		}
		
		
		
	}
	traceInfo('reseting gmenuSelectedItem');
	gmenuSelectedItem = 0;
	
	s += '</table>';
	
	if(isScroll)
		s += '</div>';
	
	container_el.innerHTML = s;
	if(found != -1) {
		document.getElementById('td_filter_' + found).className = 'over';
	} else {
		try {
			document.getElementById('td_filter_0').className = 'over';
		} catch(e) {}
	}
	document.getElementById('gmenu_searcher').focus();
	if(ie)
		document.getElementById('gmenu_searcher').select();	// zeby wrocilo na koniec wpisywanego tekstu
	
	var search_el = document.getElementById('gmenu_searcher');
	var scroll_el;
	var scroll_tab_el;
	
	if(isScroll) {
		scroll_el = document.getElementById('gmenu_scroll');
		scroll_tab_el = document.getElementById('gmenu_scroll_table');
	}
	
	gmenuFixSearchWidth(btnIndex);
	search_el.style.width = container_el.offsetWidth - 33 + 'px';
	
	if(isScroll) {
		scroll_tab_el.style.width = container_el.offsetWidth + 'px';
		scroll_el.style.width = container_el.offsetWidth + 'px';
	}
	
	if(isScroll) {
		if(scroll_el.offsetHeight > gmenuMaxHeight)
			scroll_el.style.height = gmenuMaxHeight + 'px';
		else
			scroll_el.style.height = 'auto';
	}
	
	shadow_el.style.width = container_el.offsetWidth + 'px';
	shadow_el.style.height = container_el.offsetHeight + 'px';
	
	traceEnd();
}

/* odswieza zaznaczenia czekboks?*/
function gmenuUpdateLabels(btnIndex, isChecked) {
	var formId = gmenuForms[btnIndex];
	var formFinalId = formId + '_final';
	
	var jForm_el = $j('#' + formId);
	var jFormFinal_el = $j('#' + formFinalId);
	
	for(i = 0; i < jForm_el.children().length; i ++) {
		jFormFinal_el.children().get(i).checked = jForm_el.children().get(i).checked;
	}
	
	if(isChecked) 
		alert('Nadaje etykiete');
	else {
		//alert($j('#form').get());
		for(i = 0; i < jForm_el.children().length; i ++) {
			if(jFormFinal_el.children().get(i).checked) {
				//alert('Zaznaczono: ' + jFormFinal_el.children().get(i).value);
				//alert('Value: ' + jFormFinal_el.children().get(i).value + 
				//		'   Id: ' + jFormFinal_el.children().get(i).id);
				$j("#viewedLabelsMultipleId > option[value='" + jFormFinal_el.children().get(i).id + "']").attr("selected", "true");
				//alert('powinien sie zmienic');
			}
		}
	}
		
	gmenuHideFilters();
	gmenuChangeButtonState(btnIndex, false);
}

/* buduje filtry spelniajacymi pattern */
function buildFiltersDivChb(btnIndex, pattern) {
	var container_el = document.getElementById('gmenu_container');
	var shadow_el = document.getElementById('gmenu_shadow');
	var isScroll = gmenuFiltersScroll[btnIndex];
	var s = '';
	var fi = 0;	// found index
	var fs = '';	// found string
	
	/* czyscimy wszystko */
	container_el.innerHTML = '';
	
	/* wysietlamy wyszukiwarke */
	s += '<table cellspacing=0 cellpadding=0 class="filter"><tbody>';
	s += '<tr class="filter"><td class="filter_left"></td>';
	s += '<td class="filter"><input type="text" class="filter" id="gmenu_searcher" name="gmenu_search"';
	
	if(gmenuNextTab[btnIndex] != '') {
		//alert(gmenuNextTab[btnIndex]);
		s+= ' onkeydown="return keyDown(event, ' + btnIndex + ', ' + gmenuNextTab[btnIndex] + ');"';
	}
	s+= ' onkeyup="return keyUpChb(event, ' + btnIndex + ');"></td></tr>';
	
	if(isScroll) {
		s += '</tbody></table>';
		s += '<div id="gmenu_scroll" style="display: block; float: left; overflow: auto; overflow-x: hidden; height: ' + 'auto' + ';">';
		s+= '<table cellspacing=0 cellpadding=0 class="filter" id="gmenu_scroll_table"><tbody>';
	}
	
	found = -1;	
	
	for(i = 0; i < gmenuButtons[btnIndex].length; i ++) {
		var cur = $j('#' + gmenuForms[btnIndex]).children()[i];
		var isCheckedStr = cur.checked ? 'checked' : '';

		if(pattern == '') {
			s += '<tr class="filter"><td class="filter_left"></td><td id="td_filter_' + i + '" class="filter" onmouseover="replaceClass(this, \'over\')" 	onmouseout="replaceClass(this, \'filter\')" onclick="gmenuSelectFilterChb(this,' + btnIndex + ')">';
			s += '<input type="checkbox" ' + isCheckedStr + ' class="styledDynamic">';
			s += gmenuButtons[btnIndex][i];
			s += '</tr></td>';
		}
		else {
			var pat = new RegExp(pattern, 'i');
	
			if((fs=gmenuButtons[btnIndex][i].match(pat)) != null) {
				if(found == -1) {
					found = i;
				}
				var str = gmenuButtons[btnIndex][i];
				var boldPattern = '<b class="found">' + fs + '</b>';
				str = str.replace(fs, boldPattern);
				s += '<tr class="filter"><td class="filter_left"></td><td id="td_filter_' + i + '" class="filter" onmouseover="replaceClass(this, \'over\')" 	onmouseout="replaceClass(this, \'filter\')" onclick="gmenuSelectFilterChb(this,' + btnIndex + ')">';
				s += '<input type="checkbox" ' + isCheckedStr + ' class="styledDynamic">';
				s += str;
				s += '</tr></td>';
			}
		}
	}
	
	/* sprawdzamy czy mamy zaznaczony jakis dokument w #mainTable */
	var jAllChb_el = $j('#mainTable :checkbox');
	var len = jAllChb_el.length;
	var isChecked = false;
	var isCheckedStr = '<img id="gmenuBottomImg" src="" width="16" heigh="16" style="display: block; float: left; padding: 0px; margin-top: 2px !important; margin-top: 2px; margin-bottom: -3px !important; margin-bottom: 0px; margin-right: 7px !important; margin-right: 5px; margin-left: 1px;" border=0>Poka�';
	var isCheckedStr2 = '<img id="gmenuBottomImg" src="add.png" style="display: block; float: left; padding: 0px; margin-top: 2px !important; margin-top: 2px; margin-bottom: -3px !important; margin-bottom: 0px; margin-right: 7px !important; margin-right: 5px; margin-left: 1px;" border=0>';

	for(i = 0; i < len; i ++) {
		if(jAllChb_el.get(i).checked) {
			isChecked = true;
			break;
		}
	}
	if(isChecked) {		// bedziemy nadawac etykiety
		isCheckedStr = isCheckedStr2 + 'Nadaj';
	}
	
	//style="padding-left: 28px !important; padding-left: 27px;"
	s += '<tr class="filter"><td class="filter_left"></td><td class="filter" style="border-top: 1px solid #C0C0C0; color: #4560A0; font-weight: bold;"  onmouseover="replaceClass(this, \'over\')" 	onmouseout="replaceClass(this, \'filter\')" onclick="gmenuUpdateLabels(' + btnIndex + ', ' + isChecked + ')">';
	s += isCheckedStr;
	s += '</td></tr>';
	
	s += '</table>';
	
	if(isScroll)
		s += '</div>';
	
	container_el.innerHTML = s;
	if(found != -1) {
		document.getElementById('td_filter_' + found).className = 'over';
	}
	document.getElementById('gmenu_searcher').focus();
	if(ie)
		document.getElementById('gmenu_searcher').select();	// zeby wrocilo na koniec wpisywanego tekstu
	
	var search_el = document.getElementById('gmenu_searcher');
	var scroll_el;
	var scroll_tab_el;
	
	if(isScroll) {
		scroll_el = document.getElementById('gmenu_scroll');
		scroll_tab_el = document.getElementById('gmenu_scroll_table');
	}
	
	/*search_el.style.width = '0px';
	search_el.style.width = container_el.offsetWidth + 'px';*/
	
	if(isScroll) {
		scroll_tab_el.style.width = container_el.offsetWidth + 'px';
		scroll_el.style.width = container_el.offsetWidth + 'px';
	}
	
	if(isScroll) {
		if(scroll_el.offsetHeight > gmenuMaxHeight)
			scroll_el.style.height = gmenuMaxHeight + 'px';
		else
			scroll_el.style.height = 'auto';
	}
	
	CustomDynamic.init();
	
	//alert(document.getElementById(gmenuButtonsId[btnIndex]).offsetWidth);
	gmenuFixSearchWidth(btnIndex);
	search_el.style.width = container_el.offsetWidth - 33 + 'px';
	shadow_el.style.width = container_el.offsetWidth + 'px';
	shadow_el.style.filter = 'alpha(opacity=50)';
	shadow_el.style.height = container_el.offsetHeight + 'px';
	
	/* naprawiamy wyswietlanie png w ie6 */
	if(ie) {
	var tab = $j('#gmenu_container tbody tr.filter td.filter:not(:first,:last)').hover(function() {
			var backStr = $j(this).children('.checkbox').css('background-image');
			backStr = backStr.replace('.png', '_light.png');
			$j(this).children('.checkbox').css('background-image', backStr);
		}, function() {
			var backStr = $j(this).children('.checkbox').css('background-image');
			backStr = backStr.replace('_light.png', '.png');
			$j(this).children('.checkbox').css('background-image', backStr);
	});
	}
	//alert(tab);
}		

function gmenuHideFilters() {
	gmenuIsShow = false;
	var el = document.getElementById('gmenu_container');
	var shadow_el = document.getElementById('gmenu_shadow');
	el.style.visibility = 'hidden';
	shadow_el.style.visibility = 'hidden';
	
	if(gmenuCurrentButton != -1)
				gmenuChangeButtonState(gmenuCurrentButton, false);
}

function keyUp(e, btnIndex) {
	traceStart('keyUp');
	var search_el = document.getElementById('gmenu_searcher');
	var shadow_el = document.getElementById('gmenu_shadow');
	var container_el = document.getElementById('gmenu_container');
	var str = search_el.value;
	var keyNum;
		
	if(ie)
		keyNum = e.keyCode;
	else
		keyNum = e.which;
		
	trace('KN: ' + keyNum);
		
	if(keyNum != KEY_DOWN && keyNum != KEY_UP && keyNum != 13) {
		trace('Calling buildFiltersDiv');
		buildFiltersDiv(btnIndex, str);
	}
	search_el = document.getElementById('gmenu_searcher');
	search_el.value = str;
	search_el.focus();
		
	if(keyNum == 13) {	// 13 = ENTER
		
	
		
		gmenuFilterSelected = gmenuFoundList[gmenuSelectedItem];
		var domEl = document.getElementById(gmenuFilterSelected);
		
		
		$j(domEl).trigger('click');
		
		
		traceEnd();
		return;
		
	
		if(str != '' && found != -1) {		
			if(gmenuFilterSelected != -1)
				found = gmenuFilterSelected;
			var el = document.getElementById('td_filter_' + found);
			gmenuSelectFilter(el, btnIndex);
			gmenuChangeButtonState(btnIndex, false);
		} else if(gmenuFilterSelected != -1) {
			var el = document.getElementById('td_filter_' + gmenuFilterSelected);
			gmenuSelectFilter(el, btnIndex);
			gmenuChangeButtonState(btnIndex, false);
		} else {
			gmenuHideFilters();
			gmenuChangeButtonState(btnIndex, false);
		}
	} else if(keyNum == 27) {	// 27 = ESCAPE
		search_el.value = '';
		gmenuHideFilters();
		gmenuChangeButtonState(btnIndex, false);
	} else if(keyNum == KEY_DOWN) {
		traceStart('KEY_DOWN');
		
		gmenuSelectByKey = true;
		
		trace(gmenuFoundList);
		
		var jCur = $j('#' + gmenuFoundList[gmenuSelectedItem++]);
		if(gmenuSelectedItem >= gmenuFoundList.length)
			gmenuSelectedItem = gmenuFoundList.length - 1;
			
		trace('gmenuSelectedItem: ' + gmenuSelectedItem);
		var jNext = $j('#' + gmenuFoundList[gmenuSelectedItem]);
		$j('#gmenu_searcher').val(jNext.text());
		
		jCur.trigger('mouseout');
		jNext.trigger('mouseover');
		
		var selItemTop = jNext.attr('offsetTop');
		var selItemHeight = jNext.attr('offsetHeight');
		var divHeight = $j('#gmenu_scroll').height();
		
		if(selItemTop + 25 > divHeight) {
			trace('We are scrolling down ... ');
			//trace('Before: ' + $j('#gmenu_scroll').scrollTop());
			
			var newScroll = selItemHeight * (gmenuSelectedItem - 4);
			if(newScroll < 0) newScroll = 0;
			$j('#gmenu_scroll').scrollTop(newScroll);
			
			//trace('After: ' + $j('#gmenu_scroll').scrollTop());
		}
		
		traceEnd();
	} else if(keyNum == KEY_UP) {
		traceStart('KEY_UP');
		
		//trace(gmenuFoundList);
		
		gmenuSelectByKey = true;
		
		var jCur = $j('#' + gmenuFoundList[gmenuSelectedItem--]);
		if(gmenuSelectedItem < 0)
			gmenuSelectedItem = 0;
		
		var jPrev = $j('#' + gmenuFoundList[gmenuSelectedItem]);
		$j('#gmenu_searcher').val(jPrev.text());
		
		jCur.trigger('mouseout');
		jPrev.trigger('mouseover');
		
		var selItemTop = jPrev.attr('offsetTop');
		var selItemHeight = jPrev.attr('offsetHeight');
		var divHeight = $j('#gmenu_scroll').height();
		
		if(selItemTop + 25 > divHeight) {
			trace('We are scrolling down ... ');
			//trace('Before: ' + $j('#gmenu_scroll').scrollTop());
			
			var newScroll = selItemHeight * (gmenuSelectedItem - 4);
			if(newScroll < 0) newScroll = 0;
			$j('#gmenu_scroll').scrollTop(newScroll);
			
			//trace('After: ' + $j('#gmenu_scroll').scrollTop());
		}
		
		traceEnd();
	}
	
	/*search_el.style.minWidth = container_el.offsetWidth + 'px';
	search_el.style.width = '500px';
	shadow_el.style.width = container_el.offsetWidth + 'px';*/
	
	/* jesli u gory to przesuwamy */
	if(gmenuIsUp) {
		var container_el = document.getElementById('gmenu_container');
		container_el.style.top = GetElementPosition(gmenuButtonsId[btnIndex])[1] - container_el.offsetHeight + 'px';
		shadow_el.style.top = GetElementPosition(gmenuButtonsId[btnIndex])[1] - container_el.offsetHeight + 'px';
	}
	
	traceEnd();
	return false;
}

function keyUpChb(e, btnIndex) {
	var search_el = document.getElementById('gmenu_searcher');
	var shadow_el = document.getElementById('gmenu_shadow');
	var container_el = document.getElementById('gmenu_container');
	var str = search_el.value;
	var keyNum;
		
	buildFiltersDivChb(btnIndex, str);
	search_el = document.getElementById('gmenu_searcher');
	search_el.value = str;
	search_el.focus();
	
	if(ie)
		keyNum = e.keyCode;
	else
		keyNum = e.which;
		
		
	if(keyNum == 13) {	// 13 = ENTER
		if(str != '' && found != -1) {		
			if(gmenuFilterSelected != -1)
				found = gmenuFilterSelected;
			var el = document.getElementById('td_filter_' + found);
			gmenuSelectFilter(el, btnIndex);
			gmenuChangeButtonState(btnIndex, false);
		} else if(gmenuFilterSelected != -1) {
			var el = document.getElementById('td_filter_' + gmenuFilterSelected);
			gmenuSelectFilter(el, btnIndex);
			gmenuChangeButtonState(btnIndex, false);
		} else {
			gmenuHideFilters();
			gmenuChangeButtonState(btnIndex, false);
		}
	} else if(keyNum == 27) {	// 27 = ESCAPE
		search_el.value = '';
		gmenuHideFilters();
		gmenuChangeButtonState(btnIndex, false);
	}
	
	return false;
}

function keyDown(e, btnIndex, tabId) {
	var keyNum;
	traceStart('keyDown');

	if(ie)
		keyNum = e.keyCode;
	else
		keyNum = e.which;
		
	if(keyNum == 13) {	// 13 = ENTER
		if(ie)
			event.cancelBubble = true;
		else
			e.stopPropagation();
			
	
		if(tabId != '__gmenuAuto') {
			trace('calling keyup');
			keyUp(e, btnIndex);
		}
		
		traceInfo('KEY_ENTER for keyDown');
		
		if(tabId == '__gmenuAuto') {			
			trace('Parent: ' + $j('#focus_btn_' + btnIndex));
			//gmenuSwitchNextTab();
		} else {		
			document.getElementById(tabId.id).focus();
		}
		
		traceEnd();
		return false;
	} else if(keyNum == 9) { // 9 = TAB
		traceInfo('TAB');
		gmenuSelectByKey = true;
		gmenuSwitchNextTab(btnIndex);
		
		return false;
	}
	
	traceEnd();
	return true;
}

function gmenuAllowButton(btnIndex) {
	var container_el = document.getElementById('gmenu_container');
	var shadow_el = document.getElementById('gmenu_shadow');
	
	/* jesli zamykamy otwarty przycisk nim samym */
	if(gmenuCurrentButton == btnIndex && container_el.style.visibility == 'visible') {
		//alert('p');
		container_el.style.visibility = 'hidden';
		shadow_el.style.visibility = 'hidden';
		gmenuChangeButtonState(btnIndex, false);
		return false;
	} else if(gmenuCurrentButton != btnIndex) {		// jesli otwieramy nowy przycisk
		if(gmenuCurrentButton != -1)			// jesli jest jakis otwarty przycisk
			gmenuChangeButtonState(gmenuCurrentButton, false);			// zamykamy stary
		gmenuCurrentButton = btnIndex;							// otwieramy nowy
		return true;
	} 
	
	return true;
}

function gmenuShowFilters(btn, btnIndex, isUp) {
	traceStart('Function gmenuShowFilters');
	//gmenuSelectedItem = 0;
	gmenuSelectByKey = false;
	var container_el = document.getElementById('gmenu_container');
	if(container_el == null){
		$j("form:first").append("<div id='gmenu_container'></div>");
		container_el = document.getElementById('gmenu_container');
	}
	var btn_el = document.getElementById(btn.id);
	var shadow_el = document.getElementById('gmenu_shadow');
	if(shadow_el == null){
		$j("form:first").append("<div id='gmenu_shadow'></div>");
		shadow_el = document.getElementById('gmenu_shadow');
	}
	var search_el;
	var str = '';
	var str1 = '';
	gmenuIsUp = isUp;
	gmenuFilterSelected = -1;
	
	/* sprawdzamy czy mamy otwarte inne przyciski */
	/*for(i = 0; i < gmenuButtonsId.length; i ++) {
		var cur_el = document.getElementById(gmenuButtonsId[i]);
		if(cur_el.style.color != 'black' && i != btnIndex) {
			gmenuChangeButtonState(i, false);
			container_el.style.visibility = 'hidden';
			shadow_el.style.visibility = 'hidden';
			break;
		}
	}*/
	
	//window.status = 'btnIndex: ' + btnIndex + '  current: ' + gmenuCurrentButton;
	
	if(!gmenuAllowButton(btnIndex))
		return;
	
	/*if(container_el.style.visibility == 'visible') {
		container_el.style.visibility = 'hidden';
		shadow_el.style.visibility = 'hidden';
		gmenuChangeButtonState(btnIndex, false);
		return;
	} else {
		if(gmenuCurrentButton == -1)	
			gmenuCurrentButton = btnIndex;
	}*/
	
	/* wczytujemy 'wcisniete' tlo */
	gmenuChangeButtonState(btnIndex, true);
	gmenuIsShow = true;
	
	/* przesuwamy pod przycisk */
	/*container_el.style.left = btn_el.offsetLeft + 'px';
	container_el.style.top = btn_el.offsetTop + btn_el.offsetHeight + 'px';
	
	shadow_el.style.left = btn_el.offsetLeft + 5 + 'px';
	shadow_el.style.top = btn_el.offsetTop + 5 + btn_el.offsetHeight + 'px';*/
	
	if(1) {		// if ie ale tez dla firefoxa
		container_el.style.left = GetElementPosition(btn.id)[0] + 3 + 'px';
		container_el.style.top = btn_el.offsetHeight + GetElementPosition(btn.id)[1] + 'px';
		
		shadow_el.style.left = 8 + GetElementPosition(btn.id)[0] + 'px';
		shadow_el.style.top = btn_el.offsetHeight + 5 + GetElementPosition(btn.id)[1] + 'px';
	}
	container_el.style.visibility = 'visible';
	shadow_el.style.visibility = 'visible';
	
	
	/* wypelniamy filtry */
	buildFiltersDiv(btnIndex, '');
	/*shadow_el.style.width = container_el.offsetWidth + 'px';
	shadow_el.style.height = container_el.offsetHeight + 'px';*/
	
	//gmenuCurrentButton = btnIndex;
	
	var search_el = document.getElementById('gmenu_searcher');
	/*search_el.style.width = container_el.offsetWidth + 'px';
	search_el.style.minWidth = container_el.offsetWidth + 'px';*/
	
	/*shadow_el.style.width = container_el.offsetWidth + 'px';
	shadow_el.style.height = container_el.offsetHeight + 'px';*/
	
	/* jesli u gory to przesuwamy */
	if(gmenuIsUp) {
		container_el.style.top = GetElementPosition(btn.id)[1] - container_el.offsetHeight + 'px';
		shadow_el.style.top = GetElementPosition(btn.id)[1] - container_el.offsetHeight + 'px';
	}
	
	traceEnd();
	return false;
}

function gmenuShowFiltersChb(btn, btnIndex) {
	var container_el = document.getElementById('gmenu_container');
	var btn_el = document.getElementById(btn.id);
	var shadow_el = document.getElementById('gmenu_shadow');
	var search_el;
	var str = '';
	var str1 = '';
	
	
	if(!gmenuAllowButton(btnIndex))
		return;
	
	/* wczytujemy 'wcisniete' tlo */
	gmenuChangeButtonState(btnIndex, true);
	
	/* ustawiamy temp na takie warto?i jak final */
	var len = $j('#' + gmenuForms[btnIndex]).children().length;
	for(i = 0; i < len; i ++) {
		$j('#' + gmenuForms[btnIndex]).children().get(i).checked = 
			$j('#' + gmenuForms[btnIndex] + '_final').children().get(i).checked;
	}
	
	/* przesuwamy pod przycisk */	
	if(1) {		// if ie ale tez dla firefoxa
		container_el.style.left = GetElementPosition(btn.id)[0] + 3 + 'px';
		container_el.style.top = btn_el.offsetHeight + GetElementPosition(btn.id)[1] + 'px';
		
		shadow_el.style.left = 8 + GetElementPosition(btn.id)[0] + 'px';
		shadow_el.style.top = btn_el.offsetHeight + 5 + GetElementPosition(btn.id)[1] + 'px';
	}
	container_el.style.visibility = 'visible';
	shadow_el.style.visibility = 'visible';
	
	/* wypelniamy filtry */
	buildFiltersDivChb(btnIndex, '');
	gmenuIsShow = true;
	//container_el.innerHTML = 'nowe filtry';
	/*shadow_el.style.width = container_el.offsetWidth + 'px';
	shadow_el.style.height = container_el.offsetHeight + 'px';*/
	
	gmenuCurrentButton = btnIndex;
}

/* tylko dla taga extended-select */
function gmenuExtendedSelect(formId, defLabel, defPat){
	try{
		$j("#"+formId+"_div").get(0).buttonId = gmenuIdGenerator;
		gmenuImportForm(gmenuIdGenerator, formId, formId + "_div", defLabel, defPat, "__gmenuAuto");
		gmenuIdGenerator++;
	} catch (e) {
		alert(e.toString());
	}
}

function gmenuImportForm(btnIndex, formId, buttonId, defLabel, defPat, nextTabId) {
	traceStart('gmenuImportForm');
//	alert("INIT:"+btnIndex);
	var form_el = document.getElementById(formId);
	var length = form_el.length;
	if(gmenuIdGenerator <= btnIndex) gmenuIdGenerator = btnIndex + 1;
	gmenuButtons[btnIndex] = new Array();
	gmenuForms[btnIndex] = formId;
	gmenuButtonsId[btnIndex] = buttonId;
	gmenuDefaultButtons[btnIndex] = defLabel;
	gmenuDefaultPatterns[btnIndex] = defPat;
	gmenuFiltersScroll[btnIndex] = false;
	gmenuNextTab[btnIndex] = nextTabId;
	
	var img_el = document.getElementById(gmenuButtonsId[btnIndex] + '_img');
	gmenuArrowSrc = img_el.src;
	//alert(img_el.src);
	
	var btn_el = document.getElementById(gmenuButtonsId[btnIndex]);
	for(i = 0; i < length; i ++) {
		gmenuAddFilter(btnIndex, form_el.options[i].text);
		//alert(gmenuDefaultPatterns[btnIndex] + ' ' + form_el.options[i].text);
		if(form_el.options[i].selected) {
			str = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />';
			if(gmenuDefaultPatterns[btnIndex] == form_el.options[i].text)
				str += gmenuDefaultButtons[btnIndex];
			else
				str += form_el.options[i].text;	
				
			btn_el.innerHTML = '<img src="" id="' + gmenuButtonsId[btnIndex] + '_img' + '" class="arrow" />' +
			form_el.options[i].text;
			btn_el.innerHTML = str;
			
			found = i;
		}
	}
	
	if(i > gmenuMaxFilters)
		gmenuFiltersScroll[btnIndex] = true;
	
	gmenuChangeButtonState(btnIndex, false);
	
	try {
		gmenuFixButtonLength(btn_el, document.getElementById(gmenuForms[btnIndex]).options[found], btnIndex);
	} catch(e) {}
	if (nextTabId == '__gmenuAuto') {
		trace('Creating auto-focus button');
		$j(form_el).before('<button style="width:1px; height: 1px; border: none; background-color: transparent; display: block;" id="focus_btn_' + btnIndex + '" class="gmenuFocusBtn"></button>');
		$j('#focus_btn_' + btnIndex).focus(function(){
			traceStart('#focus_btn_' + btnIndex + '.focus');
			traceInfo('Called focus for: ' + this.id);
			var el = $j('#focus_btn_' + btnIndex).prev().prev().get(0);
			gmenuShowFilters(el, el.buttonId, false);
			
			traceEnd();
			return false;
		}).blur(function(){
			//$j(this).text('0');
			return false;
		});
	}
	
	traceEnd();
}

function gmenuImportFormChb(btnIndex, formId, buttonId, defLabel, defPat, nextTabId) {
	var jForm_el = $j('#' + formId);
	var jFormFinal_el = $j('#' + formId + '_final');

	gmenuButtons[btnIndex] = new Array();
	gmenuForms[btnIndex] = formId;
	gmenuButtonsId[btnIndex] = buttonId;
	gmenuDefaultButtons[btnIndex] = defLabel;
	gmenuDefaultPatterns[btnIndex] = defPat;
	gmenuFiltersScroll[btnIndex] = false;
	gmenuNextTab[btnIndex] = nextTabId;
	
	var length = jForm_el.children().length;
	
	for(i = 0; i < length; i ++) {
		// nastawiamy temporary tak jak final
		jForm_el.children().get(i).checked = jFormFinal_el.children().get(i).checked;
		gmenuAddFilter(btnIndex, jForm_el.children()[i].value);
	}
	
	for(i = 0; i < length; i ++) {
		//alert(gmenuButtons[btnIndex][i]);
	}
}

if (window.captureEvents){
	window.captureEvents(Event.CLICK);
	window.onclick=sourceCheck;
}
else
	document.onclick=sourceCheck;

function sourceCheck(e){
	var el=(typeof event!=='undefined')? event.srcElement : e.target
	//window.status = el.className + ' isShow: ' + gmenuIsShow;
	
	/* sprawdzamy czy mamy otwarty jakis przycisk */
	if(gmenuIsShow) {
		//alert('chowamy');
		if(el.className == '' || el.className == 'wrapper' || el.className == 'push')
			gmenuHideFilters();
	}
}


function gmenuDebug(btnIndex, formId, buttonId) {
	var form_el = document.getElementById(formId);
	
//	alert(form_el.length);
}
