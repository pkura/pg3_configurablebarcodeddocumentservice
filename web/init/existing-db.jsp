<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N existing-db.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.init.InitialConfigurationAction"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Wyb�r bazy danych</h3>

<edm-html:errors />

<html:form action="/init/configuration">
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>
<html:hidden property="dbBrand"/>
<html:hidden property="dbUrl" />
<html:hidden property="dbUsername" />
<html:hidden property="dbPassword" />

<p>W wybranej bazie znajduj� si� nast�puj�ce tabele:
<ul> <c:forEach items="${tableNames}" var="tableName">
    <li><c:out value="${tableName}"/></li>
    </c:forEach>
</ul>
</p>

<%--
<html:checkbox property="createTables" value="true"/> chc� utworzy� wymagane tabele od nowa
--%>

<p></p>

<html:submit property="doUseExistingDb" value="Chc� u�y� tej bazy"/>
<html:submit property="doChooseAnotherDb" value="Chc� wybra� inn� baz�"/>
<html:submit property="doUseExistingAndUpadateDb" value="Chc� u�y� tej bazy i dopisa� tabele" />

</html:form>
