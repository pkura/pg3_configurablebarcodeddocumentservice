<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N page3.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.init.InitialConfigurationAction"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Wyb�r serwera LDAP</h3>

<edm-html:errors />

<html:form action="/init/configuration">
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>
<html:hidden property="dbBrand"/>
<html:hidden property="dbUrl" />
<html:hidden property="dbUsername" />
<html:hidden property="dbPassword" />
<html:hidden property="createTables"/>

<p>Wpisz dane serwera LDAP, w kt�rym aplikacja DocuSafe b�dzie trzyma�a
dane o u�ytkownikach.</p>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>Serwer i port:</td>
    <td><html:text property="ldapServer"/> (np. <i>localhost:389</i>)</td>
</tr>
<tr>
    <td>Nazwa po��czenia:</td>
    <td><html:text property="ldapName"/></td>
</tr>
<tr>
    <td>Has�o:</td>
    <td><html:password property="ldapPassword"/></td>
</tr>
<tr>
    <td>G��wny sufiks:</td>
    <td><html:text property="ldapRoot"/> (np. <i>dc=firma,dc=pl</i>)</td>
</tr>
</table>

<html:submit property="doSetLdap" value="Zapisz"/>

</html:form>
