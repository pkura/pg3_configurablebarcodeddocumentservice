<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N existing-ldap.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.init.InitialConfigurationAction"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Wyb�r serwera LDAP</h3>

<edm-html:errors />

<html:form action="/init/configuration">
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>
<html:hidden property="dbBrand"/>
<html:hidden property="dbUrl" />
<html:hidden property="dbUsername" />
<html:hidden property="dbPassword" />
<html:hidden property="createTables"/>
<html:hidden property="ldapServer"/>
<html:hidden property="ldapName"/>
<html:hidden property="ldapPassword"/>
<html:hidden property="ldapRoot"/>

<p>Podany sufiks LDAP ju� istnieje. Czy chcesz u�y� znajduj�cych
si� pod nim danych jako bazy danych o u�ytkownikach DocuSafe?</p>

<html:submit property="doUseExistingLdap" value="Tak"/>
<html:submit property="doChooseAnotherLdap" value="Nie"/>

</html:form>
