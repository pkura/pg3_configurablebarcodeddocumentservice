<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N done.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.init.InitialConfigurationAction"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Koniec</h3>

<edm-html:errors />
<edm-html:messages />

<p>Konfiguracja zosta�a zako�czona. Od tego momentu ponowne uruchomienie
procesu konfiguracji nie b�dzie mo�liwe z wyj�tkiem sytuacji, gdy dane
konfiguracyjne b�d� niedost�pne.</p>

<html:link forward="home">Przejd� do aplikacji</html:link>
