<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N page2.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.init.InitialConfigurationAction,
                 pl.compan.docusafe.core.cfg.Configuration"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Wyb�r bazy danych</h3>

<edm-html:errors />

<html:form action="/init/configuration">
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>

<%--
<p>Wybierz rodzaj bazy danych i nazw� �r�d�a danych.</p>

<p>�r�d�o danych musi by� uprzednio skonfigurowane w serwerze aplikacyjnym
i musi by� aktualnie dost�pne.</p>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>Typ bazy danych:</td>
    <td>
        <html:select property="dbBrand">
            <html:option value="<%= InitialConfigurationAction.DB_FIREBIRD_1_0 %>">Firebird 1.0.x</html:option>
        </html:select>
    </td>
</tr>
<tr>
    <td>Nazwa �r�d�a danych:</td>
    <td><html:text property="dataSource" size="15" maxlength="100"/></td>
</tr>
</table>
--%>

<p>Wybierz rodzaj bazy danych i wpisz parametry po��czenia.</p>

<p>Baza danych musi by� aktualnie dost�pna.</p>

<table border="0" cellspacing="0" cellpadding="0">
<tr>
    <td>Typ bazy danych:</td>
    <td>
        <html:select property="dbBrand">
            <html:option value="<%= Configuration.DB_FIREBIRD_1_5 %>">Firebird 1.5.x</html:option>
<%--            <% if (!Configuration.coreOfficeAvailable()) { %>--%>
            <%--<html:option value="<%= Configuration.DB_FIREBIRD_1_0 %>">Firebird 1.0.x (tylko archiwum)</html:option>--%>
<%--            <% } %>--%>
            	<html:option value="<%= Configuration.DB_SQLSERVER2000 %>">SQLSERVER</html:option>
            	<html:option value="<%= Configuration.ORACLE_9 %>">Oracle</html:option>
        </html:select>
    </td>
</tr>
<tr>
    <td>URL JDBC:</td>
    <td><html:text property="dbUrl" size="45" maxlength="100"/></td>
</tr>
<%--
<tr>
    <td>Serwer:</td>
    <td><html:text property="dbHost" size="15" maxlength="100"/></td>
</tr>
<tr>
    <td>Plik z baz� danych</td>
    <td><html:text property="dbFile" size="15" maxlength="100"/></td>
</tr>
--%>
<tr>
    <td>Nazwa u�ytkownika:</td>
    <td><html:text property="dbUsername" size="15" maxlength="100"/></td>
</tr>
<tr>
    <td>Has�o:</td>
    <td><html:password property="dbPassword" size="15" maxlength="100"/></td>
</tr>
</table>

<html:submit property="doSetDatabase" value="Zapisz"/>

<p>Wzorcowe urle JDBC:</p>
<ul>
	<td>FIREBIRD</td>
    <li>jdbc:firebirdsql:127.0.0.1:docusafe?lc_ctype=WIN1250</li>
    <td>ORACLE</td>
    <li>jdbc:oracle:thin:@127.0.0.1:1521:XE</li>
    <td>SQLSERVER</td>
    <li>jdbc:jtds:sqlserver://127.0.0.1</li>
</ul>

</html:form>
