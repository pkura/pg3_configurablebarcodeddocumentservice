<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N existing-home.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Katalog ju� istnieje</h3>

<html:form action="/init/configuration">
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>

<b><bean:write name="configuration" property="home"/></b>

<p>Katalog, kt�rego nazw� podano istnieje i nie jest pusty. Czy chcesz
u�y� plik�w znajduj�cych si� w tym katalogu jako plik�w konfiguracyjnych
aplikacji DocuSafe?</p>

<html:submit property="doUseExistingHome" value="Tak"/>
<html:submit property="doDefault" value="Nie"/>

</html:form>
