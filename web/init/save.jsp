<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N save.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>Zapisanie konfiguracji</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/init/configuration" >
<html:hidden property="home"/>
<html:hidden property="licenseBytes"/>
<html:hidden property="userFactory"/>
<html:hidden property="dbBrand"/>
<html:hidden property="dbUrl" />
<html:hidden property="dbUsername" />
<html:hidden property="dbPassword" />
<html:hidden property="createTables"/>
<html:hidden property="ldapServer"/>
<html:hidden property="ldapName"/>
<html:hidden property="ldapPassword"/>
<html:hidden property="ldapRoot"/>
<html:hidden property="useExistingLdap"/>

<%-- Plik z licencj�: <html:file property="licenseFile"/> <br> --%>

<p>Po naci�ni�ciu przycisku "Zapisz" zostan� utworzone pliki konfiguracyjne
oraz utworzona baza danych i struktura LDAP, je�eli wybrano takie opcje.</p>

<ul>
    <li>Tworzenie plik�w konfiguracyjnych: TAK</li>
    <li>Tworzenie bazy danych:
        <logic:equal value="true" name="configuration" property="createTables" >TAK</logic:equal>
        <logic:notEqual value="true" name="configuration" property="createTables" >NIE</logic:notEqual>
    </li>
    <li>Tworzenie struktury LDAP:
        <logic:notEqual value="true" name="configuration" property="useExistingLdap" >TAK</logic:notEqual>
        <logic:equal value="true" name="configuration" property="useExistingLdap" >NIE</logic:equal>
    </li>
</ul>

<p>Rzeczowy wykaz akt</p>

<input type="radio" name="rwa" value="um" checked="true"> Urz�d Miasta <br/>
<input type="radio" name="rwa" value="sp"> Starostwo Powiatowe <br/>
<input type="radio" name="rwa" value="none"> Brak <br/>

<p></p>

<html:submit property="doSave" value="Zapisz" />

</html:form>
