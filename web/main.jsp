<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N main.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h3><ds:lang text="Witaj"/> <edm-sec:logged-user format="%n" /> <ds:lang text="wDocuSafe"/></h3>


<table width="100%">
<tr>
    <td align="right">
        <ds:lang text="OstatnieUdaneLogowanie"/>: <edm-sec:last-successful-login/> <br/>
        <ds:available test="!cas.enabled">
        <ds:lang text="OstatnieNieudaneLogowanie"/>: <edm-sec:last-unsuccessful-login/>
         </ds:available>
    </td>
</tr>
</table>
