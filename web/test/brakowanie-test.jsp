<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1"><ds:lang text="BarcodesValidation"/></h1>
<hr class="fullLine"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value='/test/brakowanie-test.action'/>" method="post"  >

	<ds:submit-event value="'Generuj wykazy'" name="'test1'" ></ds:submit-event>
	<ds:submit-event value="'Wywolaj import'" name="'wywolajImport'" ></ds:submit-event>
	
	<br/>
	<ww:label>Document id: </ww:label>
	<ww:textfield name="'docId'" size="5" cssClass="'txt'" value="''"/>
	
	<ds:submit-event value="'Slownik test'" name="'slownik'" ></ds:submit-event>
</form>