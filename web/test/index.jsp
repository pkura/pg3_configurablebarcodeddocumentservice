<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N index.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
    <title>Testy</title>
    <style type="text/css">
    body {
        font-family: Arial;
        font-size: 10pt;
    }
    a {
        color: blue;
        text-decoration: none;
    }
    a:hover {
        color: blue;
        text-decoration: underline;
    }
    </style>
</head>
<body>
</body>
<p>Lista dostępnych testów. Wyświetlenie wyników możliwe jest tylko w przeglądarce
obsługującej przetwarzanie XSL.</p>
<ul>
    <li><a href="../ServletTestRunner?suite=pl.compan.docusafe.test.TestHibernate&xsl=test/cactus-report.xsl">
        Hibernate</a></li>
    <li><a href="../ServletTestRunner?suite=pl.compan.docusafe.test.TestAuthFilter&xsl=test/cactus-report.xsl">
        AuthFilter</a></li>
</html>
