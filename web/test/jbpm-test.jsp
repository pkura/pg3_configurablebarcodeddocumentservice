<%@ taglib uri="http://struts.apache.org/tags-html"
	prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-html-el"
	prefix="html-el"%>
<%@ taglib uri="http://struts.apache.org/tags-logic"
	prefix="logic"%>
<%@ taglib uri="http://struts.apache.org/tags-bean"
	prefix="bean"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<edm-html:select-string-manager name="jspRepositoryLocalStrings" />
<edm-html:errors />
<edm-html:messages/>

<h1><ds:lang text="JBPM TEST" /></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />