<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1"><ds:lang text="BarcodesValidation"/></h1>
<hr class="fullLine"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value='/test/case-archiver-test.action'/>" method="post"  >

	<ds:submit-event value="'Generuj wykazy'" name="'test1'" ></ds:submit-event>
	
	<br /><br />
	
	Id teczki:
	<ww:textfield name="'folder_id'" size="5" cssClass="'txt'" value="''"/>
	
	<ds:submit-event value="'Generuj testowy wykaz'" name="'test2'" ></ds:submit-event>


	<br /><br />
	Id doc:
	<ww:textfield name="'doc_id'" size="5" cssClass="'txt'" value="''"/>
	<ds:submit-event value="'Pobierz proces'" name="'testJbpm'" ></ds:submit-event>
	
	<br /><br />
	<ww:if test="jBPMInfo != null">
		Proces: <ww:property value="jBPMInfo.getProcessInstanceInfo()"/>
		<br />
		
		Tasks: <br/>
		<ww:iterator value="jBPMInfo.tasks">
			&nbsp; - ActivityName: <ww:property value="activityName"/>, <br/>
			&nbsp;&nbsp; Assignee: <ww:property value="assignee"/>, <br/>
			&nbsp;&nbsp; CreateTime: <ww:property value="createTime"/>
		</ww:iterator>
		
		<br />
		Transitions: <br/>
		
		<!--  <input type="checkbox" name="deleteUsers" value="<ww:property value="name"/>"/> -->
		<ww:iterator value="jBPMInfo.transitions">
			
			&nbsp; activity: <ww:property value="activity"/><br />
			transitions: <br/>
			
			<ww:iterator value="transitions">
				&nbsp;&nbsp; - <ww:property value="name"/> <br/>
			</ww:iterator>
		</ww:iterator>
		
		<br />
		Zmienne: <br />
		<ww:iterator value="vars">
			&nbsp;&nbsp; <ww:property value="toString()"/> <br/>
		</ww:iterator>
	</ww:if>
	
</form>