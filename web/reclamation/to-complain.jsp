<%--T
	Przeróbka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N to-complain.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1>Zgłaszanie reklamacji</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
    <p>
        <ds:ww-action-errors/>
        <ds:ww-action-messages/>
    </p>

<form id="form" action="<ww:url value="'/reclamation/to-complain.action'"/>" method="post"  enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="0">
	<tr>
	    <td>
	        <ds:lang text="RodzajDokumentuDocusafe"/>:
	    </td>
	    <td>
	        <ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="key" listValue="value" value="documentKindCn" cssClass="'sel'"
	            onchange="'changeDockind();'"/>
	    </td>
	</tr>
	<tr>
		<td>
			<jsp:include page="/common/dockind-fields.jsp"/>
			<jsp:include page="/common/dockind-specific-additions.jsp"/>
		</td>
	</tr>
	<tr>
		<td>
			<input type="submit" name="doCreate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
	        	onclick="if (!validateForm()) return false;document.getElementById('doCreate').value = 'true';" />
	        <input type="submit" name="doCreate" value="<ds:lang text="ZapiszIprzejdzNaListeZadan"/>" class="btn"
	        	onclick="if (!validateForm2()) return false;document.getElementById('doCreate').value = 'true';" />
		</td>
	</tr>
</table>
<script type="text/javascript">
	
	function validateForm()
    {
        if (!validateDockind())
            return false;

        return true;
    }
    
    	function validateForm2()
    {
     	document.getElementById('goToList').value = 'true';
        if (!validateDockind())
            return false;

        return true;
    }

</script>
</form>