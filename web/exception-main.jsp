<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N exception-main.jsp N-->

<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="java.io.PrintWriter"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%
    Exception exception = (Exception) request.getAttribute("exception");
%>

<script language="JavaScript">
function toggle_div(id)
{
    var div = document.getElementById(id);
    if (div.style.display == 'none')
    {
        div.style.display = '';
    }
    else
    {
        div.style.display = 'none';
    }
}
</script>

<% if (exception == null) { %>
<h2><edm-html:message key="exception.unknown"/></h2>
<% } else { %>
<h2>Wyst�pi� b��d: <%= exception.getMessage() != null ? exception.getMessage() : exception.getClass().getName() %></h2>
<p>Skontaktuj si� z administratorem systemu.</p>
<a href="javascript:void(toggle_div('trace'));"><edm-html:message key="exception.details"/></a>
<div id="trace" style="display: none; border: 1px solid gray;">
<pre>
<%
    PrintWriter wrappedOut = new PrintWriter(out);
    exception.printStackTrace(wrappedOut);

    Throwable t = exception;
    while (t != null && t.getCause() != null)
    {
        out.print("Caused by: ");
        t = t.getCause();
        t.printStackTrace(wrappedOut);
    }
%>
</pre>
</div>
<% } %>
