function MClass(object)
{
  var Static = { };
  
  if (object["Static"]) {
    for (property in object["Static"])
	  Static[property] = object["Static"][property];

    object["static"] = null;
  }

/*
  var c = function () {
    for (property in object)
	  this[property] = object[property];

    return this.initialize.apply(this, arguments);
  };
  */
  
  var c = function () {
  	this.initialize.apply(this, arguments);
  };
  
  for (property in object)
    c.prototype[property] = object[property];

  if (Static)
    for (property in Static)
      c[property] = Static[property];

  return c;
}
