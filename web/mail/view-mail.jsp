<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N view-mail.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1>Poczekalnia maili</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/mail/view-mail.action'"/>" method="post">

<ww:select name="'emailChannelId'" list="emailChannels" listKey="key" listValue="value" cssClass="'sel'"/>
<ds:submit-event value="getText('Zmien')" name="'doSetMessages'" cssClass="'btn'" />

<table width="80%">
    <tr>
        <th></th>
        <th><h2>Od</h2></th>
        <th><h2>Tytu�</h2></th>
        <th><h2>Data wys�ania</h2></th>
        <th><h2>Za��czniki</h2></th>
        <th><h2>Rejestracja dokumentu</h2></th>
        <th></th>
    </tr>
    <ww:iterator value="messages">
        <ww:set name="sentDate" scope="page" value="sentDate"/>
        <tr height="22">
            <td><ww:checkbox  name="'deleteIds'" fieldValue="id"/></td>
            <td><a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="id"/></ww:url>"><ww:property value="from"/></a></td>
            <td><a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="id"/></ww:url>"><ww:property value="subject"/></a></td>
            <td><fmt:formatDate value="${sentDate}" type="both" pattern="dd-MM-yy HH:mm"/><hr/></td>
            
            <ww:if test="attachments != null && attachments.size() > 0">
	            <td align="center"><a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="id"/></ww:url>">
	            <img src="<ww:url value="'/img/saveBtn.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Poka� szczeg�y"/></a></td>
            </ww:if>
            <ww:else>
            	<td align="center"><a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="id"/></ww:url>">
	            <img src="<ww:url value="'/img/delete.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Poka� szczeg�y"/></a></td>
            </ww:else>
            <td align="left"> <input type="button" value="Zarejestruj w systemie" class="btn" onclick="document.location.href='<ww:url value="'/office/incoming/new.action'"><ww:param name="'doNewDocument'" value="true"/><ww:param name="'messageId'" value="id"/></ww:url>';"/></td>
            <%--  <td align="center">&nbsp;<a href="<ww:url value="'/office/incoming/new.action'"><ww:param name="'doNewDocument'" value="true"/><ww:param name="'messageId'" value="id"/></ww:url>"></td>
            <img src="<ww:url value="'/img/stampel.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" title="Zarejestruj mail w systemie"/></a></td> --%> 
        </tr>
    </ww:iterator>
    <tr>

        <td colspan="4"><ds:web-event name="'doDelete'" value="'Usu� zaznaczone'"/></td>
    </tr>
</table>

</form>