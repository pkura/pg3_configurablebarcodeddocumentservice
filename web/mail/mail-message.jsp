<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N mail-message.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<h1>Podgl�d wiadomo�ci</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>



<table>
    <tr>
        <td>Temat</td>
        <td><b><ww:property value="subject"/></b></td>
    </tr>
    <tr>
        <td>Od:</td>
        <td><b><ww:property value="from"/></b></td>
    </tr>
    <ww:if test="sender != null">
        <tr>
            <td>Nadawca:</td>
            <td><b><ww:property value="sender"/></b></td>
        </tr>
    </ww:if>
    <tr>
        <td>Data wys�ania:</td>
        <td><b><ww:property value="sentDate"/></b></td>
    </tr>
    <ww:if test="recipientsTO != null && recipientsTO.size() > 0">
        <tr>
            <td>Do:</td>
            <td><b>
                <ww:iterator value="recipientsTO">
                    <ww:property/><br/>
                </ww:iterator>
            </b></td>
        </tr>
    </ww:if>
    <ww:if test="recipientsCC != null && recipientsCC.size() > 0">
        <tr>
            <td>Kopia:</td>
            <td><b>
                <ww:iterator value="recipientsCC">
                    <ww:property/><br/>
                </ww:iterator>
            </b></td>
        </tr>
    </ww:if>
    <ww:if test="recipientsBCC != null && recipientsBCC.size() > 0">
        <tr>
            <td>Kopia ukryta:</td>
            <td><b>
                <ww:iterator value="recipientsBCC">
                    <ww:property/><br/>
                </ww:iterator>
            </b></td>
        </tr>
    </ww:if>
    <tr>
        <td valign="top">Tre��:</td>
        <td><ww:property value="content" escape="false"/></td>
    </tr>
</table>
<ww:if test="attachments != null && attachments.size() > 0">
    <table>
        <tr>
            <td valign="top">Za��czniki:</td>
            <td>
                <ww:iterator value="attachments">
                    <a href="<ww:url value="'/mail/mail-message.action'"><ww:param name="'messageId'" value="messageId"/><ww:param name="'doGetAttachment'" value="true"/><ww:param name="'attachmentNo'" value="id"/></ww:url>"><ww:property value="filename"/></a>&nbsp;<ds:format-size value="#this['filesize']"/><br/>
                </ww:iterator>

            </td>
        </tr>
    </table>
</ww:if>

<input type="button" value="Zarejestruj w systemie" class="btn" onclick="document.location.href='<ww:url value="'/office/incoming/new.action'"><ww:param name="'doNewDocument'" value="true"/><ww:param name="'messageId'" value="messageId"/></ww:url>';"/>
