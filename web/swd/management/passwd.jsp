<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Przekazywanie has�a u�ytkownika</h1>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/swd/management/passwd.action?id='+id"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'tab'"/>
	<ww:hidden name="'password'"/>



    <ww:set name="DEBUG_ON" value="false" />
    <ww:set name="ComponentNoCheckPermissions" value="true" />




	<table border=0 cellpadding=0 cellspacing=0>
        <tr>
            <td>
                <ww:set name="jspComponentAttributes" value="jspAttributes" />
                <ww:set name="jspComponentAttributesName" value="'jspAttributes'" />
                <ww:include page="/jsp/component/attributes.jsp" />
            </td>

            <td width="20"/>

            <td>
                <table border=0 cellpadding=5 cellspacing=0>

                    <tr>
                        <td/>
                        
                        <td>
                            <input type="button" value="Przejd� do profilu u�ytkownika" class="btn" onclick="document.location.href='<ww:property value="getLinkToUser()"/>';" style="width:100%">
                        </td>
                    </tr>
                    <tr>
                        <td/>
                        
                        <td>
                            <input type="button" value="Powr�t" class="btn" onclick="document.location.href='/docusafe/swd/management/passwds-list.action';" style="width:100%">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <b>Wygenerowane has�o:</b>
                        </td>

                        <td   >
                            <b><ww:property value="password"/></b>
                        </td>
                    </tr>

                    <tr>
                        <td   >
                            <input type="hidden" name="PassToSWD" id="PassToSWD">
                            <input type="submit" name="PassToSWDButton" value="PRZEKAZANIE - Zapisz w SWD" class="btn" onclick="  document.getElementById('PassToSWD').value='true';" style="width:100%">
                        </td>
                    </tr>

                    <tr>
                        <td   >
                            <input type="hidden" name="PassOnAnHour" id="PassOnAnHour">
                            <input type="submit" name="PassOnAnHourButton" value="Prze�� na za godzin�" class="btn" onclick="  document.getElementById('PassOnAnHour').value='true';" style="width:100%">
                        </td>
                    </tr>

                    <tr>
                        <td   >
                            <input type="hidden" name="PassTommorowAt9" id="PassTommorowAt9">
                            <input type="submit" name="PassTommorowAt9Button" value="Prze�� na jutro g. 9:00:" class="btn" onclick="document.getElementById('PassTommorowAt9').value='true';" style="width:100%">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <input type="hidden" name="PassOn" id="PassOn">
                            <input type="submit" name="PassOnButton" value="Prze�� na:" class="btn" onclick="document.getElementById('PassOn').value='true';" style="width:100%">
                        </td>
                        <td>

                            <ww:textfield name="'onDate'" id="onDate" cssClass="'txt'" value="getNextPassedTime()"/>

                        </td>
                    </tr>

                    <tr>
                        <td   >
                            <input type="hidden" name="CancelPassing" id="CancelPassing">
                            <input type="submit" name="CancelPassingButton" value="Anuluj przekazywanie" class="btn" onclick="  document.getElementById('CancelPassing').value='true';" style="width:100%">
                        </td>
                    </tr>
                </table>
             </td>
        </tr>
    </table>

</form>

<script type="text/javascript">

    $j(document).ready(function() {
        $j('[name=onDate]').datetimepicker({
              buttonImage     : '<ww:url value="'/calendar096/img.gif'" />'
            , showOn          : 'button'
            , buttonImageOnly : true
            , dateFormat      : 'dd-mm-yy'
            , timeFormat      : 'hh:mm'
        });
    });

</script>