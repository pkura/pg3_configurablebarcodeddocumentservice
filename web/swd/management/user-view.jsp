<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">
    <ww:property value="title"/>: <ww:property value="upUser.lastName"/> <ww:property value="upUser.firstName"/></h1>
<a href="<ww:url value="getClientLink(upUser)"/>" class="nonDroppable">
    <h4 class=" headerAdd <ds:additions test="tcLayout">highlightedColor</ds:additions>">
        Klient: <ww:property value="upUser.clientName"/>
        <ww:if test="upUser.clientName == null">BRAK PRZYPISANEGO KLIENTA</ww:if>
    </h4>
</a>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>


<ds:ww-action-errors/>
<ds:ww-action-messages/>
<link rel="stylesheet" type="text/css" href="<ww:url value="'/swd/management/css/swd.css'"/>" />

    <ds:available test="layout2">
        <div id="middleContainer" class="container"> <!-- BIG TABLE start -->
    </ds:available>


    <form name="userForm" id="userForm" action="<ww:url value="'/swd/management/user-view.action'"/>" method="post" enctype="multipart/form-data">
    <ww:hidden id="pop" name="'pop'" value="pop" />
    <ww:hidden name="'readonly'" value="readonly" />
    <ww:hidden name="'documentId'" value="documentId" />
    <ww:hidden name="'dictCn'" value="dictCn" />
    <ww:hidden id="userId" name="'userId'" value="upUser.id" />
    <ww:hidden name="'upUser.id'" value="upUser.id" />
    <ww:hidden name="'upUser.clientName'" value="upUser.clientName" />
    <ww:hidden name="'upUser.clientId'" value="upUser.clientId" />
        <div class="row">
            <ww:if test="pop">
                <div class="row col-md-offset-5 col-md-5">
                    <input type="button" class="btn" value="Cofnij" onclick="back();">
                </div>
            </ww:if>
            <!-- USER DATA TABLE -->
            <div class="row">
            <div id="userDataTable" style='min-width:358px;' class="col-md-5 <ww:property value="isVisible('USER_PROFILE',documentId, dictCn)"/>" >
                <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneOrganizacyjne"/>:</h2>
                <table id="userData">
                    <tr>
                        <td>
                            <ds:lang text="ID"/>:
                        </td>
                        <td>
                            <ww:property value="upUser.id"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Login"/>:
                        </td>
                        <td>
                            <ww:textfield id="login" name="'upUser.login'" size="40" maxlength="100" cssClass="'txt require'" value="upUser.login" readonly="isReadonly('USER_PROFILE','login',documentId, readonly,dictCn)" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Nazwisko"/>:
                        </td>
                        <td>
                            <ww:textfield id="nazwisko" name="'upUser.lastName'" size="40" maxlength="100" cssClass="'txt require'" value="upUser.lastName" readonly="isReadonly('USER_PROFILE','nazwisko',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Imie"/>:
                        </td>
                        <td>
                            <ww:textfield id="imie" name="'upUser.firstName'" size="40" maxlength="100" cssClass="'txt require'" value="upUser.firstName" readonly="isReadonly('USER_PROFILE','imie',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Stanowisko"/>:
                        </td>
                        <td>
                            <ww:textfield id="stanowisko" name="'upUser.position'" size="40" maxlength="100" cssClass="'txt'" value="upUser.position" readonly="isReadonly('USER_PROFILE','stanowisko',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Email"/>:
                        </td>
                        <td>
                            <ww:textfield id="email" name="'upUser.email'" size="40" maxlength="100" cssClass="'txt require'" value="upUser.email" readonly="isReadonly('USER_PROFILE','email',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="Telefon"/>:
                        </td>
                        <td>
                            <ww:textfield id="phone" name="'upUser.phone'" size="40" maxlength="100" cssClass="'txt require'" value="upUser.phone" readonly="isReadonly('USER_PROFILE','phone',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <ds:lang text="fax"/>:
                        </td>
                        <td>
                            <ww:textfield id="fax" name="'upUser.fax'" size="40" maxlength="100" cssClass="'txt'" value="upUser.fax" readonly="isReadonly('USER_PROFILE','fax',documentId, readonly,dictCn)"/>
                        </td>
                    </tr>

                    <ww:iterator value="roleList">
                        <ww:set name="role"/>
                        <tr>
                            <td>
                                <input type="radio" id="<ww:property value="#role.name"/>" name="generalRole"
                                    class="radio generalRole"  value="<ww:property value="#role.name"/>"
                                    <ww:property value="hasGeneralRole(#role)"/>
                                >
                                <label for="<ww:property value="#role.name"/>"><ww:property value="#role.name"/></label>
                            </td>
                        </tr>
                    </ww:iterator>

                </table>
            </div>

            <!-- ACCESS DATA TABLE -->

            <div id="accessDataTable" class="col-md-5 <ww:property value="isVisible('USER_ACCESS_DATA',documentId,dictCn)"/>">
                <div id="accessData">
                    <h2 class="row header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneDostepu"/>:</h2>
                    <div class="row">
                            <label for="active" class="col-md-5"><ds:lang text="Active"/>:</label>
                            <div id="active">
                                <ww:checkbox id="act" name="'upUser.active'" fieldValue="true" value="upUser.active" disabled="isReadonly('USER_ACCESS_DATA','act',documentId, readonly,dictCn)"/>
                            </div>
                    </div>
                    <div class="row">
                            <label for="blocked" class="col-md-5"><ds:lang text="Blocked"/>:</label>
                            <div id="blocked">
                                <ww:checkbox id="bck" name="'upUser.blocked'" fieldValue="true" value="upUser.blocked" disabled="isReadonly('USER_ACCESS_DATA','bck',documentId, readonly,dictCn)"/>
                            </div>
                    </div>
                    <div class="row">
                            <label class="col-md-5"><ds:lang text="HasloWygaslo"/>:</label>
                            <ww:checkbox id="expired"  name="'upUser.expiredPassword'" fieldValue="true"
                                    value="upUser.expiredPassword" disabled="true" />
                    </div>
                    <div class="row"></div>
                    <div class="row"></div>
                    <div class="row">
                            <label class="col-md-5"><ds:lang text="OstatnieUdaneLogowanie"/>:</label>
                            <ww:hidden name="'upUser.lastLoginTime'" value="upUser.lastLoginTime" />
                            <ww:property value="upUser.lastLoginTime"/>
                    </div>
                    <div class="row <ww:if test="upUser.isLastLoginInvalid">redHighlight</ww:if>">
                            <label class="col-md-5">
                                <ds:lang text="ostatnieNieudaneLogowanie"/>:
                            </label>
                            <ww:hidden name="'upUser.lastInvalidLoginTime'" value="upUser.lastInvalidLoginTime" />
                            <ww:property value="upUser.lastInvalidLoginTime"/>

                    </div>
                    <div class="row">
                            <label class="col-md-5"><ds:lang text="CertificateExpires"/>:</label>
                            <ww:hidden name="'upUser.maxCertExpDate'" value="upUser.maxCertExpDate" />
                            <ww:property value="upUser.maxCertExpDate"/>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <!-- SEARCH USER DOCUMENTS BUTTON -->
        <ww:if test="pop == true && hasPermission('USER_DOCUMNETS',documentId, readonly,dictCn)">
            <div class="row col-md-offset-5 col-md-5 ">
                    <input type="submit" value="<ds:lang text="Dokumenty"/>" name="doSearch" id="userDocs" >
            </div>
        </ww:if>


        <!-- ROLE -->
        <ww:if test="roleList != null && !roleList.isEmpty()">
            <h2 class="row header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Role"/>:</h2>
            <ww:iterator value="roleList">
                <ww:set name="role"/>
                <div class="row">
                    <div class="col-md-2">
                        <ww:property value="#role.name"/>:
                    </div>
                    <table class="col-md-10">
                    <tr>
                    <ww:iterator value="systemsList">
                    <ww:set name="system"/>
                        <td>
                            <input type="radio" name="upUser.role['<ww:property value="#system.regCode"/>']" value="<ww:property value="#role.name"/>"
                                class="radio role" <ww:property value="hasRoleInSystem(#system, #role)"/>
                            >

                            <label for="<ww:property value="#system.name"/>"><ww:property value="#system.regCode"/></label>
                        </td>
                    </ww:iterator>
                    </tr>
                    </table>
                </div>
            </ww:iterator>
        </ww:if>


        <!-- UPRAWNIENIA -->

        <div class="row col-md-12 <ww:property value="isVisible('USER_RIGHTS',documentId,dictCn)"/>">
        <h2 class="row header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Uprawnienia"/>:</h2>
        <input type="hidden" name="upUser.grantsCodes" value="dummy_grant"/>
            <ww:iterator value="groups">
            <ww:set name="group"/>
                    <table class="authgroup">
                    <tr><th class="row"><ww:property value="#group"/></th></tr>
                    <ww:iterator value="getListUprawnien(#group)"><ww:set name="prawo"/>
                            <tr><td>
                                <ww:if test="isReadonly('USER_RIGHTS',documentId, readonly,dictCn)">
                                    <input type="checkbox" name="upUser.grantsCodes" value="<ww:property value="#prawo"/>" <ww:property value="hasRight(#prawo)"/>
                                        disabled="true" >
                                </ww:if>
                                <ww:else>
                                    <input type="checkbox" name="upUser.grantsCodes" value="<ww:property value="#prawo"/>" <ww:property value="hasRight(#prawo)"/> >
                                </ww:else>
                                <ww:property value="#prawo"/>
                            </td></tr>
                    </ww:iterator>
                    </table>
            </ww:iterator>
        </div>

        <!-- SAVE AND SENT -->
        <ww:if test="hasPermission('USER_SAVE',documentId, readonly,dictCn)">
            <div class="row col-md-offset-5 col-md-5">
                <input type="submit" id="saveBtn" value="<ds:lang text="ZapiszIWyslij"/>" name="doSave">
            </div>
        </ww:if>


        <!-- CZYNNO�CI DO WYKONANIA -->

            <div class="row">
            <ww:if test="userId != null && hasPermission('USER_ACCESS_DATA',documentId, readonly,dictCn)">
                <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="CzynnosciDoWykonania"/>:</h2>
                <input type="checkbox" id="sentPass" name="upUser.sentPass" value="true">
                <label for="sentPass"><ds:lang text="PrzekazacHaslo"/></label>
            </ww:if>
            <ww:if test="hasPermission('USER_CERTS',documentId, readonly,dictCn)">
                <p>
                    <label><ds:lang text="ZaladujCertyfikatZPliku"/>: </label>
                    <ww:file id="'multiFiles1'" name="'multiFiles1'" size="30" cssClass="'txt multiFiles'" value="''"/>
                </p>
            </ww:if>
            </div>



        <!-- MAIL TO SENT -->
        <ww:if test="!readonly">
        <div class="row col-md-9 <ww:property value="isVisible('USER_MAIL',documentId,dictCn)"/>">
            <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="MailToSend"/>:</h2>
            <div class="col-md-3" id="mailElem">
                <div class="col-md-12" id="titleElem">
                    <ww:iterator value="mailMsg.titleFields">
                    <ww:set name="field"/>
                        <div class="row">
                            <div class="col-md-1 msgPart">
                                <ww:checkbox name="prepareName(#field)" fieldValue="#field.text" value="isMailPartSelected(#field)"/>
                            </div>
                            <label for="<ww:property value="#field.id"/>" class="col-md-11"><ww:property value="#field.name"/></label>
                        </div>
                    </ww:iterator>
                </div>
                <div class="col-md-12" id="msgElem">
                    <ww:iterator value="mailMsg.msgFields">
                    <ww:set name="field"/>
                        <div class="row">
                            <div class="col-md-1 msgPart">
                                <ww:checkbox name="prepareName(#field)" fieldValue="#field.text" value="isMailPartSelected(#field)"/>
                            </div>
                            <label for="<ww:property value="#field.id"/>" class="col-md-11">
                                <ww:property value="#field.name"/>
                            </label>
                        </div>
                    </ww:iterator>
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-2 textright"><ds:lang text="title"/>: </div>
                    <div class="col-md-10"><ww:textfield id="title" name="'mailMsg.title'" size="100" maxlength="200" cssClass="'txt'"/></div>
                </div>
                <div class="row">
                    <div class="col-md-2 textright"><ds:lang text="Wiadomosc"/>: </div>
                    <div class="col-md-10"><ww:textarea id="message" name="'mailMsg.body'" cols="100" rows="15"/></div>
                </div>
            </div>
        </div>
        </ww:if>

        <!-- CERTIFICATES -->
        <div class="row col-md-12 <ww:property value="isVisible('USER_CERTS',documentId,dictCn)"/>" id="certTable">
            <h2 class="row header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Certyfikatyuzytkownika"/>:</h2>
            <div class="row col-md-10">
                <h3 class="col-md-2 ">
                    <ds:lang text="NumerSeryjny"/>
                </h3>
                <h3 class="col-md-6 ">
                    <ds:lang text="Certyfikatyuzytkownika"/>/<ds:lang text="CertyfikatWystawcy"/>
                </h3>
                <h3 class="col-md-2 ">
                    <ds:lang text="Wazny"/> <ds:lang text="od"/> - <ds:lang text="do"/>
                </h3>
                <ww:if test="!isReadonly('USER_CERTS',documentId, readonly,dictCn)">
                <h3 class="col-md-1 ">
                    <ds:lang text="Usun"/>
                </h3>
                </ww:if>
                <h3 class="col-md-1 ">
                    <ds:lang text="Pobierz"/>
                </h3>
            </div>

            <ww:if test="upUser.certList.isEmpty()">
                <div class="row col-md-10 cert" id="noCert">
                    <h3 class="col-md-12"><ds:lang text="BrakCertyfikatow"/></br></h3>
                </div>
            </ww:if>
            <ww:else>
                <ww:iterator value="upUser.certList"><ww:set name="cert"/>
                <div class="row col-md-10 cert">
                    <div class="col-md-2">
                       <ww:property value="#cert.serialNumber"/>
                    </div>
                    <div class="col-md-6">
                       <ww:property value="#cert.subjectX500Principal"/>
                       <br />
                       <ww:property value="#cert.issuerX500Principal"/>
                       <ww:iterator value="#cert.issuerAlternativeNames"><ww:set name="altName"/>
                       <ww:property value="#altName"/>
                       </ww:iterator>
                    </div>
                    <div class="col-md-2">
                        <ww:property value="#cert.notBefore"/> - <ww:property value="#cert.notAfter"/>
                    </div>
                    <ww:if test="!isReadonly('USER_CERTS',documentId, readonly,dictCn)">
                        <div class="col-md-1">
                            <img src="<ww:url value="'/img/minus2.gif'" />" class="removeLink" onclick="delCert($j(this), <ww:property value="#cert.serialNumber"/>);"
                            title="Usun" style="cursor: pointer; position: relative; top: 3px; left: 3px;">
                        </div>
                    </ww:if>
                    <div class="col-md-1">
                        <a name="DER" value="<ww:property value="#cert.serialNumber"/>" style="padding-right: 1em;">DER</a>
                        <a name="CER" value="<ww:property value="#cert.serialNumber"/>">CER</a>
                    </div>
                </div>
                </ww:iterator>
            </ww:else>
            <input type="submit" class="hiddenForm" id="genCertFile" name="doGenCertFile" >
        </div>
    </form>

    <!-- SEARCH USER DOCS FORM -->
    <form id="searchDocs" class="hiddenForm"
    action="<ww:url value="'/repository/search-documents.action'"/>" method="post">
        <ww:hidden name="'accessedBy'" value="upUser.login"/>
        <input type="submit" id="getDocs" name="doSearch" >
    </form>


    <ds:available test="layout2">
        <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
        <div class="bigTableBottomSpacer">&nbsp;</div>
        </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
    </ds:available>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.cookie.min.js"></script>
<script type="text/javascript">
var isCreatedNewUser = <ww:property value="isCreatedNewUser()"/>;

    $j(document).ready(function () {
           $j(".radio").buttonset().css('width', '13px');
           if(!$j("#act").prop('checked')){
            changeHighlightLabel($j("#active"));
           }
           if($j("#bck").prop('checked')){
            changeHighlightLabel($j("#blocked"))
           }
           if(isCreatedNewUser){
            popupNotify();
           }
           if($j("#pop").val() && !window.opener){
            var counter = $j.cookie("counter");
            if(counter){
                counter++;
                $j.cookie("counter",counter);
            }else{
                $j.cookie("counter",1);
            }
           }

       });


    function back(){
        if(window.opener){
            window.history.go(-1);
        }else{
            var counter = $j.cookie("counter");
            $j.cookie("counter",null,{expires: -1});
            window.history.go(-counter);
        }
    }

    function popupNotify(){

            if (window.opener){
                 var newUser = new Object();


                 newUser.userId = $j("#userId").val();
                 newUser.userFirstName = $j("#imie").val();
                 newUser.userLastName = $j("#nazwisko").val();

                 //DEBUG:
                 //alert("id: "+newUser.userId + " | Fname: " + newUser.userFirstName + " |Lname: " + newUser.userLastName);


     			if (window.opener.accept)
     			{
     			     var param = window.name;
                     window.opener.accept(param, newUser);
                     window.close();
     			}
                 else
                 {
                     alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                     return;
                 }
                 return true;
            }
    }

    //Transfer submit from userDocs button to getDocs button
    $j("#userDocs").click(function(event){
        event.preventDefault();
        $j("#getDocs").click();
    });

    //Validate form on save
    $j('#saveBtn').click(function(event){
        if( $j('.require[value=]').length){
            notValidForm(event, $j('.require[value=]'));
        }
        if(!validateEmail($j("#email").val())){
            notValidForm(event, $j('#email'));
        }
        if(!validatePhone($j('#phone').val())){
            notValidForm(event, $j('#phone'));
        }
    });

    //Transfer click to doGenCertFile
    $j("#certTable a").click(function(event){
        event.preventDefault();
        var fileType= $j(this).attr("name");
        var certKey = $j(this).attr("value");

        $j("#userForm").append("<input type='hidden' id='ctf' name='certToFile' value='"+certKey+"'>");
        $j("#userForm").append("<input type='hidden' id='ft' name='fileType' value='"+fileType+"'>");

        // submit the form
        $j("#genCertFile").click();

        //clean added elements
        $j("#ctf").remove();
        $j("#ft").remove();
    });



    $j(".require").change(function(){
       if($j(this).val() != ""){
           $j(this).prop('style').removeProperty('border-color');
       }else{
            $j(this).css('border-color','#F32E1D');
       }

    });

    $j("#email").change(function(){
        if(!validateEmail($j(this).val())){
            $j(this).css('border-color','#F32E1D');
        }
    });

    $j("#phone").change(function(){
        if(!validatePhone($j(this).val())){
           $j(this).css('border-color','#F32E1D');
        }
    });

    $j("#active").click(function(){
        changeHighlightLabel($j(this));
    });

    $j("#blocked").click(function(){
        changeHighlightLabel($j(this));
    });


    $j(".msgPart").click(function(){
        fillMessage();
    });


    function fillMessage(){
        var title = "";
        var msg = "";

        $j("#attachFields").remove();
        $j("#userForm").append("<div id='attachFields'></div>");
        //get title part text and fill 'title' input box
        $j("#titleElem input[type='checkbox']:checked").each(function(){
                  var attaId = $j(this).attr("name").split("_");
                  $j("#attachFields").append("<input type='hidden' name='mailMsg.selected[" + attaId[0] + "]' value='" + attaId[1] + "'>");
                  title +=" " + $j(this).val();
                  title = $j.trim(title);
        });
        $j('#title').val(title);

        //get message part text and fill 'message' textarea
        $j("#msgElem input[type='checkbox']:checked").each(function(){
                var attaId = $j(this).attr("name").split("_");
                $j("#attachFields").append("<input type='hidden' name='mailMsg.selected[" + attaId[0] + "]' value='" + attaId[1] + "'>");
                msg += $j(this).val();
                msg = msg.replace(/<newline>/ig,"\n");
        });
        $j('#message').val(msg);
    }

    function changeHighlightLabel(field){
        $j("label[for='"+field.attr("id")+"']").toggleClass("redHighlight");
    }


    function notValidForm(event, fields){
           event.preventDefault();
           fields.css('border-color','#F32E1D');
           $j("html, body").animate({ scrollTop: 0 }, "slow");
    }

    function validatePhone(phone) {
        var filter = /^[0-9-+() ]+$/;
        if (filter.test(phone)) {
            return true;
        } else {
            return false;
        }
    }

    function validateEmail(email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      if( emailReg.test( email ) ) {
        return true;
      } else {
        return false;
      }
    }

    function delCert(domObj, certKey){
        domObj.closest('.cert').remove();
        $j("#userForm").append("<input type='hidden' name='upUser.certSerialToRem' value='"+certKey+"'>");
        if($j("#certTable").find('.cert').length == 0){
             $j("#certTable").append("<div class='row col-md-10 cert'><h3 class='col-md-12'><ds:lang text='BrakCertyfikatow'/></br></h3></div>");
        }

    }


</script>
<!--N koniec user-list.jsp N-->
