<!--N task-list.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Lista hase� do przekazania</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/swd/management/passwds-list.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedColumns);">
	<ww:hidden name="'tab'"/>



    <ww:set name="DEBUG_ON" value="false" />
    <ww:set name="ComponentNoCheckPermissions" value="true" />



	<!-- <ww:hidden name="'ascending'"/> -->
    <ds:available test="layout2">
        <div id="middleContainer"> <!-- BIG TABLE start -->
    </ds:available>

    <ww:set name="jspComponentTable" value="jspImmediatePassedPasswords" />
    <ww:set name="noCheckPermission" value="'true'" />
    <ww:set name="jspComponentTableName" value="'jspImmediatePassedPasswords'" />
    <ww:include page="/jsp/component/table.jsp" />

    <ww:set name="jspComponentTable" value="jspPasswords" />
    <ww:set name="noCheckPermission" value="'true'" />
    <ww:set name="jspComponentTableName" value="'jspPasswords'" />
    <ww:include page="/jsp/component/table.jsp" />

</form>
<!--N koniec task-list.jsp N-->
