<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Profil klienta: <ww:property value="jspOrganisationUnit.getName()"/></h1>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/swd/management/organisation-unit.action?id='+id"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'tab'"/>
	<ww:hidden name="'loaded'"/>
	<ww:hidden name="'orderBy'"/>
	<ww:hidden name="'ascending'"/>
	<ww:hidden name="'jspAttributes.jspId'"/>
	<ww:hidden name="'layout'"/>


    <ww:set name="DEBUG_ON" value="false" />


	<table border=0 cellpadding=10 cellspacing=0>
        <tr>
            <td valign="top" colspan="2" align="right">
                <input type="button" class="btn" value="Powr�t" onclick="javascript:void(window.history.go(-1));">
            </td>
        </tr>
        <tr>
            <td valign="top">
                <ww:set name="jspComponentAttributes" value="jspAttributes" />
                <ww:set name="jspComponentAttributesName" value="'jspAttributes'" />
                <ww:set name="jspPermissionManager" value="jspPermissionManager" />
                <ww:include page="/jsp/component/attributes.jsp" />
            </td>
            <td valign="top">
                <ww:if test="jspCheckboxes.jspData!=null && getJspPermissionManager()!=null && getJspPermissionManager().isComponentHidden('jspCheckboxes')==false">
                    <table>
                        <tr>
                            <ww:if test="jspCheckboxes.jspTitle != null">
                                <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="jspCheckboxes.jspTitle"/></h2>
                            </ww:if>
                        </tr>
                        <tr>
                            <ww:iterator value="jspCheckboxes.jspData">
                                <tr>
                                    <td>
                                        <ww:property value="jspTitle"/>
                                     </td>
                                     <td>

                                        <ww:if test="#DEBUG_ON==true">
                                            hidden=<ww:property value="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isComponentDisabled('jspCheckboxes')"/>
                                            disabled=<ww:property value="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isComponentHidden('jspCheckboxes')"/>
                                            todo[set hidden,disabled,readonly]
                                        </ww:if>

                                        <ww:checkbox name="'jspCheckboxes.jspSelectedIds'" fieldValue="jspId" value="jspCheckboxes.isSelected(jspId)"
                                            disabled="jspPermissionManager.isComponentDisabled('jspCheckboxes')"
                                            />
                                    </td>
                                </tr>
                            </ww:iterator>
                        </tr>
                    </table>
                 </ww:if>
             </td>
        </tr>


        <tr>
            <td valign="top" colspan="2" align="right">

                <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldHidden('BUTTON_SAVE')!=true">

                    <ds:submit-event value="'Zapisz'" name="'doSave'" cssClass="'btn'"
                        disabled="jspPermissionManager.isFieldDisabled('BUTTON_SAVE')"/>

                </ww:if>

            </td>
        </tr>

<ww:if test="!isNewOrganisationUnit()">
        <tr>
            <td valign="top" colspan="3" align="right">

                <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldHidden('BUTTON_DOCUMENTS')!=true">

                    <ds:submit-event value="'Dokumenty'" name="'doDocuments'" cssClass="'btn'"
                        disabled="jspPermissionManager.isFieldDisabled('BUTTON_DOCUMENTS')"/>

                </ww:if>

            </td>
        </tr>

    </table>

	<table border=0 cellpadding=20 cellspacing=0>

        <tr>
            <td valign="top" rowspan="3">
                <ww:set name="jspComponentAttributes" value="jspQuantity" />
                <ww:set name="jspComponentAttributesName" value="'jspQuantity'" />
                <ww:set name="jspPermissionManager" value="jspPermissionManager" />
                <ww:include page="/jsp/component/attributes.jsp" />
            </td>

            <td valign="top">
                <ww:set name="jspComponentTable" value="jspCoordinatorsTable" />
                <ww:set name="jspComponentTableName" value="'jspCoordinatorsTable'" />
                <ww:set name="jspPermissionManager" value="jspPermissionManager" />
                <ww:include page="/jsp/component/table.jsp" />
             </td>
        </tr>
        <tr>
            <td valign="top">
                <ww:set name="jspComponentTable" value="jspTechAdminsTable" />
                <ww:set name="jspComponentTableName" value="'jspTechAdminsTable'" />
                <ww:set name="jspPermissionManager" value="jspPermissionManager" />
                <ww:include page="/jsp/component/table.jsp" />
             </td>
        </tr>
        <tr>
            <td valign="top">
                <ww:set name="jspComponentTable" value="jspContactPersonsTable" />
                <ww:set name="jspComponentTableName" value="'jspContactPersonsTable'" />
                <ww:set name="jspPermissionManager" value="jspPermissionManager" />
                <ww:include page="/jsp/component/table.jsp" />
             </td>
        </tr>
</ww:if>
    </table>


    <ww:if test="!isNewOrganisationUnit()">

        <ww:set name="jspComponentTable" value="jspUsersTable" />
        <ww:set name="jspComponentTableName" value="'jspUsersTable'" />
        <ww:set name="jspPermissionManager" value="jspPermissionManager" />
        <ww:include page="/jsp/component/table.jsp" />

        <ds:submit-event value="'Eksportuj do CSV'" name="'doExportCSV'" cssClass="'btn'"/>

        <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldHidden('BUTTON_CHOOSE')!=true">

            <input type="button" 
                    value="DODAJ U�YTKOWNIKA" 
                    class="btn" 
                    onclick="document.location.href='/docusafe/swd/management/user-view.action?organisationUnit='+<ww:property value="id"/>;"
                    <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldDisabled('BUTTON_CHOOSE')==true">disabled="true"</ww:if>
                    >

        </ww:if>

    </ww:if>
</form>
<style>
    .invalid{
        border-color:#F32E1D !important;
    }
    .required{
        border-color:#F32E1D !important;
    }
</style>