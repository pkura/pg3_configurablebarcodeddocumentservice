<!--N task-list.jsp N-->

<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Zestawienia</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form" action="<ww:url value="'/swd/management/books-list.action'"/>" method="post" onsubmit="selectAllOptions(this.selectedColumns);">
	<ww:hidden name="'tab'"/>

    <ww:property value="jspReportKinds.jspTitle"/>
    <select name="jspReportKinds" class="sel">
        <ww:iterator value="jspReportKinds.jspValue.jspItems">
            <option value=<ww:property value="jspId"/> <ww:if test="isReportKindSelect(jspId)">selected="true"</ww:if> >
                <ww:property value="jspTitle"/>
            </option>
        </ww:iterator>
    </select>


    <ww:set name="jspComponentTable" value="jspReport" />
    <ww:set name="jspComponentTableName" value="'jspReport'" />
    <ww:include page="/jsp/component/table.jsp" />

    <ds:submit-event value="'Eksportuj do CSV'" name="'doExportCSV'" cssClass="'btn'"/>

</form>
<!--N koniec task-list.jsp N-->
