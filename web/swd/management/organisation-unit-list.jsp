<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Lista klient�w</h1>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="JavaScript">
	var dirty = false;
</script>

<form id="form"
    <ww:if test="layout!=nul">  action="<ww:url value="'/swd/management/organisation-unit-list.action?layout='+layout"/>"   </ww:if>
    <ww:else>                   action="<ww:url value="'/swd/management/organisation-unit-list.action'"/>"                  </ww:else>
     method="post" onsubmit="disableFormSubmits(this);">

	<ww:hidden name="'tab'"/>
	<ww:hidden name="'pop'"/>


    <ww:set name="DEBUG_ON" value="false" />


    <ww:set name="jspComponentTable" value="jspComponentTable1" />
	<ww:set name="jspComponentTableName" value="'jspComponentTable1'" />
    <ww:set name="jspPermissionManager" value="jspPermissionManager" />
	<ww:include page="/jsp/component/table.jsp"/>

	<ww:set name="jspComponentTable" value="jspComponentTable2" />
	<ww:set name="jspComponentTableName" value="'jspComponentTable2'" />
    <ww:set name="jspPermissionManager" value="jspPermissionManager" />
    <ww:include page="/jsp/component/table.jsp"/>

    <ds:submit-event value="'Eksportuj do CSV'" name="'doExportCSV'" cssClass="'btn wybierz'"/>

        <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldHidden('BUTTON_CREATE_OUNIT')!=true">

            <input type="button" value="Dodaj" class="btn" onclick="document.location.href='/docusafe/swd/management/organisation-unit.action';"
                    <ww:if test="#ComponentAttributesNoCheckPermissions!=true && jspPermissionManager!=null && jspPermissionManager.isFieldDisabled('BUTTON_CREATE_OUNIT')==true">disabled="true"</ww:if>
                    >

        </ww:if>

        <input type="button" class="btn" value="Cofnij" onclick="javascript:void(window.history.go(-1));">

</form>



<script language="JavaScript">

function choose(id){

            var strona = new Object();


            strona.ouId = id;
            var selector = "#" + id + " .NAZWA a";
            strona.ouName = $j(selector).text().trim();

            //DEBUG:
            //alert("id: "+strona.ouId + " | name: " + ouName);


			if (window.opener.accept)
			{
                window.opener.accept('SWD_ORGANISATION_UNIT', strona);
                window.close();
			}
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;

}


</script>

</script>
