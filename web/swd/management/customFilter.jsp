<%@ taglib uri="webwork" prefix="ww" %>


<div>
<ww:select id="filterSelect" name="selectedFilter.id" list="filterList" listKey="id" listValue="title" emptyOption="true" cssClass="'sel'"/>
</div>
<div>
<ww:textfield id="filterWhere" name="'selectedFilter.whereClause'" size="40" maxlength="255" cssClass="'txt'" value=""/>
</div>

<script type="text/javascript">
var map = {};
<ww:iterator value='filterList'><ww:set name='filtr'/>
    map[<ww:property value="#filtr.id"/>] = "<ww:property value="#filtr.whereClause"/>";
    map[<ww:property value="#filtr.id"/>] =  map[<ww:property value="#filtr.id"/>].replace(/&lt;/ig,"<");
    map[<ww:property value="#filtr.id"/>] =  map[<ww:property value="#filtr.id"/>].replace(/&gt;/ig,">");
</ww:iterator>


$j(document).ready(function(){
    $j('#filterSelect').change(function(){
        var filterId = $j('#filterSelect').val();
        $j('#filterWhere').val("");
        $j('#filterWhere').val(map[filterId]);
    });
});

</script>