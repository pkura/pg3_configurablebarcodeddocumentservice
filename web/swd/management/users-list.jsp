<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="userList.h1_a"/></h1>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


    <ds:available test="layout2">
        <div id="middleContainer"> <!-- BIG TABLE start -->
    </ds:available>

        <form name="formFilter" id="form" action="<ww:url value="'/swd/management/users-list.action'"/>" method="post">
            <ww:hidden id="sortField" name="'sortField'" value="sortField"/>
            <ww:hidden id="asc" name="'asc'" value="asc"/>
            <ww:hidden name="'pop'" value="pop"/>
            <ww:hidden id="typ" name="'typ'" value="typ" />
            <ww:hidden name="'readonly'" value="readonly"/>
                <ds:ww-action-errors/>
                <ds:ww-action-messages/>

                        <table id="bt" class="formTable">
                            <tr>
                                <td>
                                    <ds:lang text="IdKlienta"/>:
                                </td>
                                <td>
                                    <ww:textfield id="clientID" name="'clientID'" size="10" maxlength="10" cssClass="'txt'" value="clientID" readonly="readonly"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="IdUser"/>:
                                </td>
                                <td>
                                    <ww:textfield id="userID" name="'userID'" size="10" maxlength="10" cssClass="'txt'" value="userID"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="Login"/>:
                                </td>
                                <td>
                                    <ww:textfield id="login" name="'login'" size="35" maxlength="100" cssClass="'txt'" value="login"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="Nazwisko"/>:
                                </td>
                                <td>
                                    <ww:textfield id="lastName" name="'lastName'" size="35" maxlength="100" cssClass="'txt'" value="lastName"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="Imie"/>:
                                </td>
                                <td>
                                    <ww:textfield id="firstName" name="'firstName'" size="35" maxlength="100" cssClass="'txt'" value="firstName"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="Active"/>:
                                </td>
                                <td>
                                    <ww:select id="active" name="'active'" list="selectActive" cssClass="'sel'" value="active"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <ds:lang text="Blocked"/>:
                                </td>
                                <td>
                                    <ww:select id="blocked" name="'blocked'" list="selectBlocked" cssClass="'sel'" value="blocked"/>
                                </td>
                            </tr>

                            <!--TODO: CUSTOM FILTER -->
                            <tr>
                                <td>
                                    <ds:lang text="CustomFilter"/>:
                                </td>
                                <td>
                                <ww:include page="/swd/management/customFilter.jsp"/>
                                </td>
                            </tr>

                        </table>
                        <input type="submit" value="<ds:lang text="Znajdz"/>" id="doSearch" name="doSearch" class="btn searchBtn">
                        <input type="submit" value="<ds:lang text="PobierzDokumentCsv"/>" name="doGenerateCSV" class="btn">
        </form>
    <ds:available test="layout2">
        <!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
        <div class="bigTableBottomSpacer">&nbsp;</div>
        </div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
    </ds:available>

    <ww:if test="tableHidden == null || tableHidden == false">
        <h2 class="header <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="OdnalezieniUzytkownicy"/></h2>



        <table class="search" width="100%" cellspacing="0" id="mainTable">

            <tr class="tableheader">
                <ww:if test="pop == true">
                    <th>
                    </th>
                </ww:if>
                <ww:iterator value="tableColumns" status="status" > <%-- kolumny --%>
                    <th>
                        <nobr>
                            <ww:if test="property != 'LP'">
                                <a href="" class="nonDroppable"
                                    name="<ww:property value="property"/>"
                                    value="desc"
                                    alt="<ds:lang text="SortowanieMalejace"/>"
                                    title="<ds:lang text="SortowanieMalejace"/>">
                                        <ww:if test="sortField == property && asc == false">
                                            <img src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>"
                                        </ww:if>
                                        <ww:else>
                                            <img src="<ww:url value="'/img/strzalka-dol-lista.gif'"/>"
                                        </ww:else>
                                        width="0" height="0" border="0"/>
                                </a>
                            </ww:if>

                            <ww:property value="title"/>

                            <ww:if test="property != 'LP'">
                                <a href="" class="nonDroppable"
                                    name="<ww:property value="property"/>"
                                    value="asc"
                                    alt="<ds:lang text="SortowanieRosnace"/>"
                                    title="<ds:lang text="SortowanieRosnace"/>"
                                    >
                                        <ww:if test="sortField == property && asc == true">
                                            <img src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
                                        </ww:if>
                                        <ww:else>
                                            <img src="<ww:url value="'/img/strzalka-gora-lista.gif'"/>"
                                        </ww:else>
                                        width="0" height="0" border="0"/>
                                </a>
                            </ww:if>
                        </nobr>
                    </th>
                </ww:iterator>
            </tr>

            <ww:if test="tableData != null">
                <ww:iterator value="tableData">
                    <ww:set name="row"/>
                    <tr id="<ww:property value="#row.userID"/>"
                            <ds:available test="tasklist.highlightedRows">
                            onmouseover="$j(this).addClass('highlightedRows')"
                            onmouseout="$j(this).removeClass('highlightedRows')" </ds:available>
                    >
                    <ww:if test="pop == true">
                        <td>
                            <input type="button" value="wybierz" onclick="choose(<ww:property value="#row.userID"/>)" >
                        </td>
                    </ww:if>
                        <ww:iterator value="tableColumns">
                            <ww:set name="column"/>
                            <td>
                                <a href="<ww:url value="getTableDataLink(#row, #column.title, pop, readonly)"/>" class="nonDroppable <ww:property value="#column.property"/>" >
                                    <ww:property value="getTableData(#row, #column.title)"/>
                                </a>
                            </td>
                        </ww:iterator>
                    </tr>
                </ww:iterator>
            </ww:if>

        </table>
    </ww:if>

<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery.cookie.min.js"></script>
<script type="text/javascript">

$j(document).ready(function () {
       if($j("#pop").val() && !window.opener){
        var counter = $j.cookie("counterlist");
        if(counter){
            counter++;
            $j.cookie("counterlist",counter);
        }else{
            $j.cookie("counterlist",1);
        }
       }

});

function back(){
    if(window.opener){
        window.history.go(-1);
    }else{
        var counter = $j.cookie("counterlist");
        $j.cookie("counterlist",null,{expires: -1});
        window.history.go(-counter);
    }
}

$j("nobr a").click(function(event){
    event.preventDefault();
    var field = $j(this).attr("name");
    var asc = $j(this).attr("value");

    //FOR DEBUG
    //alert("name:"+field + " value:" + asc);

    if(asc == "asc"){
        submitForm(field,true);
    } else{
        submitForm(field,false);
    }
});


function submitForm(field,asc) {

    //FOR DEBUG
    //var a = $j('#sortField').attr("value");
    //var b = $j('#asc').attr("value");
    //alert(a + " " + b);

    $j('#sortField').attr("value",field);
    $j('#asc').attr("value",asc);

    //FOR DEBUG
    //var a = $j('#sortField').attr("value");
    //var b = $j('#asc').val();
    //alert(a + " " + b);


    // submit the form
    $j("#doSearch").click();

    //return false;

}


function choose(id){

            var strona = new Object();


            strona.userId = id;
            var selector = "#" + id + " .firstName";
            strona.userFirstName = $j(selector).text().trim();
            var selector = "#" + id + " .lastName";
            strona.userLastName = $j(selector).text().trim();

            //DEBUG:
            //alert("id: "+strona.userId + " | Fname: " + strona.userFirstName + " |Lname: " + strona.userLastName);


			if (window.opener.accept)
			{
			    var param = window.name;
			   // var isSelected = window.opener.isAlreadySelected(param, strona.userId);
                window.opener.accept(param, strona);
                window.close();
			}
            else
            {
                alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
                return;
            }
            return true;

}


</script>

<!--N koniec user-list.jsp N-->
