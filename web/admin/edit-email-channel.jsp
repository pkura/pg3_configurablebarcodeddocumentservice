<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean,
				 pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="KanalEmail"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
    <ds:xmlLink path="EmailChannels"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" class="dwr" action="<ww:url value="'/admin/edit-email-channel.action'"/>?id=<ww:property value="id"/>" method="post" enctype="multipart/form-data">
<ww:hidden name="'id'" value="id"/>
<ww:hidden name="'redirectUrl'" value="redirectUrl"/>

<ds:available test="emailChannelDwr">
    <jsp:include page="/dwrdockind/dwr-document-base.jsp"/>
</ds:available>

<table>
<tr>
	<td>
		<ds:lang text="kolejka"/>
	</td>
	<td>
		<ww:textfield name="'kolejka'" maxlength="100"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="protocol"/>
	</td>
	<td>
		<ww:select name="'protocol'" list="protocols" listKey="key" listValue="value" cssClass="'sel'" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="port"/>
	</td>
	<td>
		<ww:textfield name="'port'" maxlength="10" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="host"/>
	</td>
	<td>
		<ww:textfield name="'hostname'" maxlength="100"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="username"/>
	</td>
	<td>
		<ww:textfield name="'username'" maxlength="100"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="password"/>
	</td>
	<td>
		<ww:password show="true" name="'password'" maxlength="50" cssClass="'txt'"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="mbox"/>
	</td>
	<td>
		<ww:textfield name="'mbox'" maxlength="100"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="SSL"/>
	</td>
	<td>
		<ww:checkbox name="'SSL'" fieldValue="true" value="SSL" />
	</td>
</tr>
<%-- 
<tr>
	<td>
		<ds:lang text="enabled"/>
	</td>
	<td>
		<ww:checkbox name="'enabled'" fieldValue="true" value="enabled" />
	</td>
</tr>
 --%>
<tr>
	<td>
		<ds:lang text="Rodzaj"/>
	</td>
	<td>
		<ww:select name="'contentType'" list="contents" listKey="key" listValue="value" cssClass="'sel'"></ww:select>
		
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="Login u�ytkownika"/>
	</td>
	<td>
		<ww:textfield name="'userLogin'" maxlength="100"/>
	</td>
</tr>
</table>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<table>
<tr>
	<td>
		<ds:lang text="Warunki"/>:
	</td>
	
</tr>
<tr>
	<td colspan=4>
		<ww:if test="conditions.size > 0">
			<ww:select size="'5'" name="'conditionsPicked'" list="conditionsDescList" listKey="key" listValue="value" cssClass="'sel'"/>
		</ww:if>
	</td>	
</tr>
<tr>
	<ww:if test="conditions.size > 0">
	<td>
		<select name="conditionOperator" class="sel">
			<option value="or"><ds:lang text="lub"/></option>
			<option value="and"><ds:lang text="oraz"/></option>
		</select>
	</td>
	</ww:if>
	<td>
		<select name="conditionField" class="sel">
			<option value="from"><ds:lang text="Od"/></option>
			<option value="title"><ds:lang text="Tytul"/></option>
		</select>
	</td>
	<td>
		<ds:lang text="zawiera"/>
	</td>
	<td>
		<ww:textfield name="'conditionValue'" maxlength="50"/>
	</td>
</tr>

<!-- KONFIGURACJA SMTP -->
<tr>
	<td>
		<ds:lang text="smtpEnabled"/>
	</td>
	<td>
		<ww:checkbox name="'smtpEnabled'" fieldValue="true" value="smtpEnabled"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpHost"/>
	</td>
	<td>
		<ww:textfield name="'smtpHost'" maxlength="100" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpPort"/>
	</td>
	<td>
		<ww:textfield name="'smtpPort'" maxlength="10" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpUsername"/>
	</td>
	<td>
		<ww:textfield name="'smtpUsername'" maxlength="50"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpPassword"/>
	</td>
	<td>
		<ww:password show="true" name="'smtpPassword'" maxlength="100" cssClass="'txt'"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpTLS"/>
	</td>
	<td>
		<ww:checkbox name="'smtpTLS'" fieldValue="true" value="smtpTLS"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="smtpSSL"/>
	</td>
	<td>
		<ww:checkbox name="'smtpSSL'" fieldValue="true" value="smtpSSL"/>
	</td>
</tr>
<tr>
	<td>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="enabledEmailChannel"/>
	</td>
	<td>
		<ww:checkbox name="'enabled'" fieldValue="true" value="enabled"/>
	</td>
</tr>


<tr>
	<td colspan=4>
		<ds:submit-event value="'ZapiszKanal'" name="'doSave'" cssClass="'btn'"/>
		<ds:submit-event value="'DodajWarunek'" name="'doAddCondition'" cssClass="'btn'"/>
		<ww:if test="conditions.size > 0">
			<ds:submit-event value="'UsunZaznaczone'" name="'doRemoveConditions'" cssClass="'btn'"/>
		</ww:if>
		 <ds:available test="emailChannelDelete">
		 	<ww:if test="id != null">
		 	
		<ds:submit-event value="'Usu� kana� email'" name="'doDelateChannel'" cssClass="'btn'" confirm="Czy na pewno usuun�c kana�?"  />
			</ww:if>
		</ds:available>
        <ds:available test="emailChannelDwr">
            <ds:submit-event id="doRemoveDockind" value="'Usu� powi�zanie z dokumnentem'" name="'doRemoveDockind'" cssClass="'btn'"/>
        </ds:available>
	</td>	
</tr>

</table>

</form>

<script type="text/javascript">
    Calendar.setup({
        inputField     :    "startingDate",     // id of the input field
        ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
        button         :    "calendar_startingDate_trigger",  // trigger for the calendar (button ID)
        align          :    "Tl",           // alignment (defaults to "Bl")
        singleClick    :    true
    });


    document.getElementById("column2").remove()
</script>