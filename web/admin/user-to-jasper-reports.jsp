<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/user-to-jasper-reports.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	
	<div>
		<p>Pracownicy przypisani:</p>
		<ww:select id="usersReportList" name="'usersReportList'" list="list_user_to_jasper_report" listKey="key" listValue="value" size="10" cssClass="''" cssStyle="'min-width:1200px;'" />
		
		<ds:submit-event value="getText('Usun')" name="'doDelete'" cssClass="'btn'"/>
		<ww:if test="sorted"> 
		<ds:submit-event value="getText('Poka� wszystkich')" name="'doDefault'" cssClass="''"/>
		</ww:if>	
	</div>

	<!--   <table>
	<p>Pokazanie przypisa� wybranego u�ytkownika:</p>
	<tr>
		<td>
		<ww:select id="sortUsersList" name="'sortUsersList'" list="users" listValue="value" listKey="key" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
		<ds:submit-event value="getText('Poka� Przypisania')" name="'doSortByUser'" cssClass="''"/>
		</td>
	</tr>
	</table> -->
	
	<table>
	<p>Zdefiniowanie raport�w pracownik�w:</p>
	<tr>
		<th>Pracownik:</th>
		<th>Raport:</th> 
		<td></td>
	</tr>
	<tr>
		<td>
			<ww:select id="usersList" name="'usersList'" list="users" listValue="value" listKey="key" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
			<ww:select id="reportList" name="'reportList'" list="report" listValue="value" listKey="key" headerKey="''"  headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
			<ds:submit-event value="getText('Dodaj')" name="'doAdd'" cssClass="''"/>
		</td>
	</tr>	
	</table>
</form>