<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 02.09.08
	Poprzednia zmiana: 02.09.08
C--%>
<!--N new-label.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.settings.EmergencyAction"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:if test="id==null"><ds:lang text="DodajEtykiete"/></ww:if><ww:else><ds:lang text="EdytujEtykiete"/></ww:else></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="Labels"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
		<ds:xmlLink path="Labels"/>
	</div>
</ds:available>

<edm-html:messages/>
<edm-html:errors/>

<form id="form" action="<ww:url value="'/admin/new-label.action'"></ww:url>" method="post" >
	<ww:hidden name="'id'"/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	
	<ds:ww-action-errors/>	
	<ds:ww-action-messages/>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NazwaEtykiety"/>
			</td>
			<td>
				<ww:if test="id==null">
					<ww:textfield name="'name'" maxlength="20" cssClass="'txt'"/>
				</ww:if>
				<ww:else>
					<ww:if test="zmianaNazwy">
						<ww:textfield name="'name'" maxlength="20" cssClass="'txt'"/>
					</ww:if>
					<ww:else>	
						<ww:property value="name"/><ww:hidden name="'name'"/>
					</ww:else>
				</ww:else>
			</td>
		</tr>
		<tr>
            <td>
                <ds:lang text="NazwaKodowa"/>
            </td>
            <td>
                <ww:if test="id==null">
                    <ww:textfield name="'cn'" maxlength="20" cssClass="'txt'"/>
                </ww:if>
                <ww:else>
                    <ww:property value="cn"/><ww:hidden name="'cn'"/>
                </ww:else>
            </td>
        </tr>
		<tr>
			<td>
				<ds:lang text="OpisEtykiety"/>
			</td>
			<td>
				<ww:textarea name="'description'" cols="50" rows="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Widocznosc"/>
			</td>
			<td>
				<ww:select name="'readRights'" list="availableRights" listKey="key" listValue="value" cssClass="'sel'"></ww:select>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="PrawoNadawania"/>
			</td>
			<td>
				<ww:select name="'writeRights'" list="availableRights" listKey="key" listValue="value" cssClass="'sel'"></ww:select>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Rodzic"/>
			</td>
			<td>
				<ww:select name="'parentId'" list="availableLabels" listKey="id" listValue="name" headerKey="0" headerValue="'Root'" cssClass="'sel'"></ww:select>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="CzyUkrywaZadania"/>
			</td>
			<td>
				<ww:checkbox name="'hidesTasks'" value="hidesTasks" fieldValue="true" id="hidesTasks"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="CzasOczekiwania"/>
			</td>
			<td>
				<ww:textfield name="'inactionTime'" id="inactionTime" maxlength="3"/><ds:lang text="wDniach"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Nastepca"/>
			</td>
			<td>
				<ww:select name="'ancestorId'" list="availableLabels" listKey="id" listValue="name" headerKey="0" headerValue="'Brak'" cssClass="'sel'"></ww:select>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="UsunPoCzasieOczekiwania"/>
			</td>
			<td>
				<ww:checkbox name="'deletesAfter'" value="deletesAfter" fieldValue="true" id="deletesAfter"/>
			</td>
		</tr>
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doSave'"></ds:submit-event>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<edm-html:messages />
<edm-html:errors />

<h1><ds:lang text="DodajEtykiete"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="Labels"/>
</p>
<form id="form" action="<ww:url value="'/admin/new-label.action'"></ww:url>" method="post" >

	<ww:hidden name="'id'"/>
	<table>
		<tr>
			<td><ds:lang text="NazwaEtykiety"/></td>
			<td>
				<ww:if test="id==null">
					<ww:textfield name="'name'" maxlength="20" cssClass="'txt'"/>
				</ww:if>
				<ww:else>
					<ww:property value="name"/><ww:hidden name="'name'"/>
				</ww:else>
			</td>
		</tr>
		<tr>
			<td><ds:lang text="OpisEtykiety"/></td>
			<td><ww:textarea name="'description'" cols="50" rows="3" cssClass="'txt'"/></td>
		</tr>
		<tr>
			<td><ds:lang text="Rodzic"/></td>
			<td><ww:select name="'parentId'" list="availableLabels" listKey="id" listValue="name" headerKey="0" headerValue="'Root'"></ww:select></td>
		</tr>
		<tr>
			<td><ds:lang text="CzyUkrywaZadania"/></td>
			<td><ww:checkbox name="'hidesTasks'" value="hidesTasks" fieldValue="true" id="hidesTasks"/></td>
		</tr>
		<!-- 
		<tr>
			<td><ds:lang text="NadawanieZdejmowaniePrzezUzytwkownikow"/></td>
			<td><ww:checkbox name="'modifiable'" value="modifiable" fieldValue="true" id="modifiable"/></td>
		</tr>-->
		<tr>
			<td><ds:lang text="CzasOczekiwania"/></td>
			<td><ww:textfield name="'inactionTime'" id="inactionTime" maxlength="1"/><ds:lang text="wDniach"/></td>
		</tr>
		<tr>
			<td><ds:lang text="Nastepca"/></td>
			<td><ww:select name="'ancestorId'" list="availableLabels" listKey="id" listValue="name" headerKey="0" headerValue="'Brak'"></ww:select></td>
		</tr>
		<tr>
			<td><ds:lang text="UsunPoCzasieOczekiwania"/></td>
			<td><ww:checkbox name="'deletesAfter'" value="deletesAfter" fieldValue="true" id="deletesAfter"/></td>
		</tr>
		<tr>
			<td>
				<ds:submit-event value="getText('Zapisz')" name="'doSave'"></ds:submit-event>
			</td>
		</tr>
	</table>


</form>
--%>
<!--N koniec new-label.jsp N-->