<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ page contentType="text/html; charset=iso-8859-2"%>

<h1>
	<ds:lang text="SkrytkiEpuap" />
</h1>
<p></p>

<p>
	<a
		href='<c:out value="${pageContext.request.contextPath}"/>/admin/manage-epuap-skrytka-edit.action'><ds:lang
			text="SkrytkiEpuapNowa" />
	</a>
</p>

<ds:ww-action-errors />
<ds:ww-action-messages />

<table cellpadding="0" class="search tableMargin">
	<tr>
		<th><nobr>
				<ds:lang text="identyfikatorPodmiotu" />
			</nobr></th>
		<th><nobr>
				<ds:lang text="adresSkrytki" />
			</nobr></th>
		<th><nobr>
				<ds:lang text="nazwaSkrytki" />
			</nobr></th>
		<th><nobr>
				<ds:lang text="Aktywna" />
			</nobr></th>
		<th></th>
		<th></th>
		<th></th>
	</tr>
	<ww:iterator value="skrytki" status="status">
		<tr>
			<td><ww:property value="[0].identyfikatorPodmiotu" />
			</td>
			<td><ww:property value="[0].adresSkrytki" />
			</td>
			<td><ww:property value="[0].nazwaSkrytki" />
			</td>
			<td><ww:if test="[0].available">
						TAK
					</ww:if></td>
			<td><a
				href="<ww:url value="'/admin/manage-epuap-skrytka-edit.action'">
	               <ww:param name="'skrytka.id'" value="[0].id"/>	               
                </ww:url>"><ds:lang
						text="Modyfikuj" />
			</a></td>
			<td></td>
			<td><a
				href="<ww:url value="'/admin/manage-epuap-skrytka.action'">
	               <ww:param name="'skrytka.id'" value="[0].id"/><ww:param name="'doDelete'" value="true"/>	               
                </ww:url>"
				onclick="if(!confirm('Jeste� pewien, �e chcesz usun�� ten wpis?')) return false;"><ds:lang
						text="Usun" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/manage-epuap-skrytka.action'">
					<ww:param name="'skrytka.id'" value="[0].id"/><ww:param name="'doChangeActive'" value="true"/>
					</ww:url>"><ww:if
						test="[0].available">
						<ds:lang text="Deaktywuj" />
					</ww:if> <ww:else>
						<ds:lang text="Aktywuj" />
					</ww:else>
			</a></td>
		</tr>
	</ww:iterator>
</table>
<ds:available test="nfos.debug">
	<ww:form action="/admin/manage-epuap-skrytka.action" method="post" enctype="'multipart/form-data'">
			<ww:textfield name="'nazwaPliku'" cssClass="'txt dontFixWidth'"/>
		<ds:submit-event value="wy�lij" name="'doImport'"/>
	</ww:form>
</ds:available>

<script type="text/javascript">
	
</script>
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	<!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
