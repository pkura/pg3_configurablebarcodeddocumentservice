<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.08.08
	Poprzednia zmiana: 29.08.08
C--%>
<!--N office-ids.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Numery teczek, spraw i pism</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<style type="text/css">
	.regexp-text {
		margin-left: 5px;
	}
	.regexp {
		width: 150px;
	}
</style>

<form action="<ww:url value="'/admin/office-ids.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td class="alignTop">
				Numery teczek:
			</td>
			<td>
				<ww:textfield name="'portfolioPrefix'" size="10" maxlength="20" cssClass="'txt'" value="portfolioPrefix"/>
				<ww:textfield name="'portfolioPrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="portfolioPrefixSep"/>
				<ww:textfield name="'portfolioMiddle'" size="30" maxlength="50" cssClass="'txt'" value="portfolioMiddle"/>
				<ww:textfield name="'portfolioSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="portfolioSuffixSep"/>
				<ww:textfield name="'portfolioSuffix'" size="10" maxlength="20" cssClass="'txt'" value="portfolioSuffix"/>
				<ds:available test="numeracja.teczek.regexp">
					<span class="regexp-text">Regexp: </span><ww:textfield name="'portfolioRegexp'" size="30" maxlength="50" cssClass="'txt regexp'" value="portfolioRegexp"/>
					<ds:available test="numeracja.teczek.regexpInfo">
					<span class="text">Informacje dla uzytkownika: </span><ww:textfield name="'officeFolderRegexpInfo'" size="30" maxlength="100" cssClass="'txt'" value="officeFolderRegexpInfo"/>
					</ds:available>
				</ds:available>
			</td>
		</tr>  
		<tr>
			<td></td>
			<td>
				<span class="italic">d0 - dzia� teczki; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika</span>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				Numery podteczek:
			</td>
			<td>
				<ww:textfield name="'subportfolioPrefix'" size="10" maxlength="20" cssClass="'txt'" value="subportfolioPrefix"/>
				<ww:textfield name="'subportfolioPrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="subportfolioPrefixSep"/>
				<ww:textfield name="'subportfolioMiddle'" size="30" maxlength="50" cssClass="'txt'" value="subportfolioMiddle"/>
				<ww:textfield name="'subportfolioSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="subportfolioSuffixSep"/>
				<ww:textfield name="'subportfolioSuffix'" size="10" maxlength="20" cssClass="'txt'" value="subportfolioSuffix"/>
				<ds:available test="numeracja.teczek.regexp">
					<span class="regexp-text">Regexp: </span><ww:textfield name="'subportfolioRegexp'" size="30" maxlength="50" cssClass="'txt regexp'" value="subportfolioRegexp"/>
					<ds:available test="numeracja.teczek.regexpInfo">
					<span class="text">Informacje dla uzytkownika: </span><ww:textfield name="'officeSubfolderRegexpInfo'" size="30" maxlength="100" cssClass="'txt'" value="officeSubfolderRegexpInfo"/>
					</ds:available>
				</ds:available>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<span class="italic">d0 - dzia� podteczki; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
					lp - numer kolejny podteczki; pp - przedrostek numeru teczki;
					pm - cz�� g��wna numeru teczki; ps - przyrostek numeru teczki</span>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				Numery spraw:
			</td>
			<td>
				<ww:textfield name="'casePrefix'" size="10" maxlength="20" cssClass="'txt'" value="casePrefix"/>
				<ww:textfield name="'casePrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="casePrefixSep"/>
				<ww:textfield name="'caseMiddle'" size="30" maxlength="50" cssClass="'txt'" value="caseMiddle"/>
				<ww:textfield name="'caseSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="caseSuffixSep"/>
				<ww:textfield name="'caseSuffix'" size="10" maxlength="20" cssClass="'txt'" value="caseSuffix"/>
				<ds:available test="numeracja.teczek.regexp">
					<span class="regexp-text">Regexp: </span><ww:textfield name="'caseRegexp'" size="30" maxlength="50" cssClass="'txt regexp'" value="caseRegexp"/>
						<ds:available test="numeracja.teczek.regexpInfo">
						<span class="text">Informacje dla uzytkownika: </span><ww:textfield name="'officeCaseRegexpInfo'" size="30" maxlength="100" cssClass="'txt'" value="officeCaseRegexpInfo"/>
						</ds:available>
				</ds:available>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<span class="italic">d0 - dzia� sprawy; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
					lp - numer kolejny sprawy; pp - przedrostek numeru teczki;
					pm - cz�� g��wna numeru teczki; ps - przyrostek numeru teczki</span>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				Numery pism w sprawach:
			</td>
			<td>
				<ww:textfield name="'documentFormat'" size="50" maxlength="50" cssClass="'txt'" value="documentFormat" />
				<ds:available test="numeracja.teczek.regexp">
					<!-- <span class="regexp-text">Regexp: </span><ww:textfield name="'documentRegexp'" size="30" maxlength="50" cssClass="'txt regexp'" value="documentRegexp"/> -->
				</ds:available>
				<%--<br>(wymagane kody: d1, r; opcjonalne kody: d2, d3, d4, d5, d, p, c, y2, y4)--%>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<span class="italic">d0 - dzia� sprawy; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
					lp - numer kolejny sprawy; pp - przedrostek numeru sprawy;
					pm - cz�� g��wna numeru sprawy; ps - przyrostek numeru sprawy</span>
			</td>
		</tr>
	</table>

	<ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
</form>

<%--
<h1>Numery teczek, spraw i pism</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/office-ids.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">

<table>
	<tr>
		<td valign="top">Numery teczek:</td>
		<td>
			<ww:textfield name="'portfolioPrefix'" size="10" maxlength="20" cssClass="'txt'" value="portfolioPrefix"/>
			<ww:textfield name="'portfolioPrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="portfolioPrefixSep"/>
			<ww:textfield name="'portfolioMiddle'" size="30" maxlength="50" cssClass="'txt'" value="portfolioMiddle"/>
			<ww:textfield name="'portfolioSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="portfolioSuffixSep"/>
			<ww:textfield name="'portfolioSuffix'" size="10" maxlength="20" cssClass="'txt'" value="portfolioSuffix"/>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><i>d0 - dzia� teczki; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika</i></td>
	</tr>
	<tr>
		<td valign="top">Numery podteczek:</td>
		<td>
			<ww:textfield name="'subportfolioPrefix'" size="10" maxlength="20" cssClass="'txt'" value="subportfolioPrefix"/>
			<ww:textfield name="'subportfolioPrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="subportfolioPrefixSep"/>
			<ww:textfield name="'subportfolioMiddle'" size="30" maxlength="50" cssClass="'txt'" value="subportfolioMiddle"/>
			<ww:textfield name="'subportfolioSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="subportfolioSuffixSep"/>
			<ww:textfield name="'subportfolioSuffix'" size="10" maxlength="20" cssClass="'txt'" value="subportfolioSuffix"/>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><i>d0 - dzia� podteczki; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
				lp - numer kolejny podteczki; pp - przedrostek numeru teczki;
				pm - cz�� g��wna numeru teczki; ps - przyrostek numeru teczki</i></td>
	</tr>
	<tr>
		<td valign="top">Numery spraw:</td>
		<td>
			<ww:textfield name="'casePrefix'" size="10" maxlength="20" cssClass="'txt'" value="casePrefix"/>
			<ww:textfield name="'casePrefixSep'" size="2" maxlength="2" cssClass="'txt'" value="casePrefixSep"/>
			<ww:textfield name="'caseMiddle'" size="30" maxlength="50" cssClass="'txt'" value="caseMiddle"/>
			<ww:textfield name="'caseSuffixSep'" size="2" maxlength="2" cssClass="'txt'" value="caseSuffixSep"/>
			<ww:textfield name="'caseSuffix'" size="10" maxlength="20" cssClass="'txt'" value="caseSuffix"/>
		</td>
	</tr>
	<tr>
		<td></td>
		<td><i>d0 - dzia� sprawy; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
				lp - numer kolejny sprawy; pp - przedrostek numeru teczki;
				pm - cz�� g��wna numeru teczki; ps - przyrostek numeru teczki</i></td>
	</tr>
	<tr>
		<td valign="top">Numery pism w sprawach:</td>
		<td><ww:textfield name="'documentFormat'" size="50" maxlength="50" cssClass="'txt'" value="documentFormat" />
		<%--<br>(wymagane kody: d1, r; opcjonalne kody: d2, d3, d4, d5, d, p, c, y2, y4)
		</td>
	</tr>
	<tr>
		<td></td>
		<td><i>d0 - dzia� sprawy; d1, d2... - kody dzia��w; y2, y4 - rok; r - RWA; p - kod u�ytkownika<br/>
				lp - numer kolejny sprawy; pp - przedrostek numeru sprawy;
				pm - cz�� g��wna numeru sprawy; ps - przyrostek numeru sprawy</i></td>
	</tr>
	<tr>
		<td colspan="2">
			<ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
		</td>
	</tr>
</table>

</form>
--%>

<!--N koniec office-ids.jsp N-->