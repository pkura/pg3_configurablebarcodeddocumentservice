<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 07.08.08
	Poprzednia zmiana: 07.08.08
C--%>
<!--N download-logs.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="LogiSystemu"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>
<form id="form"
      action="<ww:url value='/admin/logs.action'/>"
      method="post" enctype="multipart/form-data" >

<table>
	<tr>
		<td style="vertical-align:top;">
			<table class="tableMargin">
				<tr>
					<td colspan="2">
						Pobierz logi
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="RowniezArchiwalne"/>:
					</td>
					<td>
						<ww:checkbox name="'archive'" id="archive" fieldValue="true"/>
					</td>
				</tr>
				<tr>
					<td>
						<ds:lang text="ZakresDat"/>:
					</td>
					<td>
						<ds:lang text="od"/>
						<ww:textfield name="'fromDate'" id="fromDate" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_fromDate_trigger"
							 style="cursor: pointer; border: 1px solid red;" title="Date selector"
							 onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<ds:lang text="do"/>
						<ww:textfield name="'toDate'" id="toDate" size="10" maxlength="10" cssClass="'txt'"/>
						<img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_toDate_trigger"
							 style="cursor: pointer; border: 1px solid red;" title="Date selector"
							 onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
					</td>
				</tr>
				<tr>
					<td colspan="2"/>
						<input type="button" class="btn" value="<ds:lang text="PobierzLogi"/>"
							onclick="document.location.href='<ww:url value="'/admin/logs.action'"/>?doDownload=true&fromDate='+document.getElementById('fromDate').value+'&toDate='+document.getElementById('toDate').value+'&archive='+document.getElementById('archive').checked;"/>
					</td>
				</tr>
			</table>
		</td>
		<td style="vertical-align:top; padding-left:20px;">
			<p>Wczytaj plik konfiguracyjny dla log4j:</p>
			<p><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></p>
			<p>
				<ds:submit-event value="'Wczytaj plik konfiguracyjny'" name="'doUpload'"/>
			</p>
			<ww:if test="properties!=null">
				<p>
					Wczytany plik:<br/>
					<ww:property value="properties" escape="false"/>
				</p>
			</ww:if>
		</td>
	</tr>
</table>
</form>
<br/>

<script type="text/javascript">
	Calendar.setup({
		inputField	 :	"fromDate",	 // id of the input field
		ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
		button		 :	"calendar_fromDate_trigger",  // trigger for the calendar (button ID)
		align		  :	"Tl",		   // alignment (defaults to "Bl")
		singleClick	:	true
	});
	Calendar.setup({
		inputField	 :	"toDate",	 // id of the input field
		ifFormat	   :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
		button		 :	"calendar_toDate_trigger",  // trigger for the calendar (button ID)
		align		  :	"Tl",		   // alignment (defaults to "Bl")
		singleClick	:	true
	});
</script>
<!--N koniec download-logs.jsp N-->