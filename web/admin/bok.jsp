<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N bok.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">BOK</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><ds:lang text="WybierzStanowiskoPrzypisaneDoBOK"/></p>

<form action="<ww:url value="'/admin/bok.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'divisionGuid'"/>
	
	<ww:if test="bokDivisionPrettyPath != null">
		<p>
			<ds:lang text="AktualnieBOKobslugiwaneJestPrzezStanowisko"/>
			<ww:property value="bokDivisionPrettyPath"/>
		</p>
	</ww:if>
	<ww:else>
		<p>
			<ds:lang text="AktualnieBOKnieJestObslugiwanePrzezZadneStanowisko"/>
		</p>
	</ww:else>
	
	<ww:property value="treeHtml" escape="false"/>
	
	<ds:submit-event value="getText('WybierzBiezaceStanowisko')" name="'doPick'" disabled="!canPick"/>
</form>

<%--
<h1>BOK</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p><ds:lang text="WybierzStanowiskoPrzypisaneDoBOK"/></p>

<form action="<ww:url value="'/admin/bok.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'divisionGuid'"/>

<ww:if test="bokDivisionPrettyPath != null">
    <p><ds:lang text="AktualnieBOKobslugiwaneJestPrzezStanowisko"/> <ww:property value="bokDivisionPrettyPath"/></p>
</ww:if>
<ww:else>
    <p><ds:lang text="AktualnieBOKnieJestObslugiwanePrzezZadneStanowisko"/></p>
</ww:else>

<table>
    <tr>
        <td>
            <ww:property value="treeHtml" escape="false"/>
        </td>
    </tr>
    <tr>
        <td>
            <ds:submit-event value="getText('WybierzBiezaceStanowisko')" name="'doPick'" disabled="!canPick"/>
        </td>
    </tr>
</table>

</form>
--%>
<!--N koniec bok.jsp N-->