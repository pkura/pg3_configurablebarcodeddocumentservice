<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 03.09.08
	Poprzednia zmiana: 03.09.08
C--%>
<!--N application-settings.jsp N-->

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="pl.compan.docusafe.util.TextUtils"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<script type="text/javascript" src="<ww:url value="'/raphael-min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/g.raphael-min.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/g.pie-min.js'" />"></script>
<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="UstawieniaAplikacji"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/application-settings.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	
	<table>
		<tr>
			<td class="alignTop">
				<ds:lang text="Wersja"/>:
			</td>
			<td>
				<%= Docusafe.getVersion() %>.<%= Docusafe.getDistNumber() %> (<%= Docusafe.getBuildId() %>)
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="WersjaPrzegladarki"/>:
			</td>
			<td>
				<%=TextUtils.parseUserAgentHeader(request.getHeader("User-Agent"))%>

				
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="RozdzielczoscEkranu"/>:
			</td>
			<td>
				<script type="text/javascript">  
					document.write(screen.width)
					document.write("/")
					document.write(screen.height)
				</script>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="DataKompilacji"/>:
			</td>
			<td>
				<%= Docusafe.getDistTime() %>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="LicencjaDla"/>:
			</td>
			<td>
				<%= Docusafe.getLicenseString() %>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="ZarejestrowanychUzytkownikow"/>:
			</td>
			<td>
				<ww:property value="userCount"/>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="InformacjeOsystemie"/>:
			</td>
			<td>
				<a href="<ww:url value="'/admin/application-settings.action?doSysinfo=true'"/>">
					<ds:lang text="pobierz"/></a>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="DefinicjaSystemu"/>:
			</td>
			<td>
				<a href="<ww:url value="'/admin/application-settings.action?doDownload=true'"/>">
					<ds:lang text="pobierz"/></a>
			</td>
		</tr>
		<tr>
			<td class="alignTop">
				<ds:lang text="AdministratorSystemu"/>:
			</td>
			<td>
				<ww:select name="'admin'" list="users" 
                                           listKey="name" 
                                           headerKey="''" 
                                           headerValue="'----'"  
                                           listValue="asFirstnameLastnameName()" 
                                           cssClass="'sel combox-chosen'" value="admin"/>
			</td>
		</tr>
		<ds:modules test="simpleoffice or office">
			
			<ds:available test="!hideAdminSettingsForAegon">
			<ds:modules test="office">
				<tr>
					<td class="alignTop">
						<ds:lang text="GrupaNatychmiastowejDekretacji"/>:
					</td>
					<td>
						<ww:select name="'instantAssignmentGroup'" list="groups"
							listKey="guid" listValue="name" cssClass="'sel'"
							value="instantAssignmentGroup" headerKey="''" headerValue="'----'"/>
					</td>
				</tr>
			</ds:modules>
			<ds:extras test="business">
			<tr>
				<td class="alignTop">
					<ds:lang text="NumeryKancelaryjneZdziennikowDodatkowych"/>:
				</td>
				<td>
					<ww:checkbox name="'officeNumbersFromAuxJournals'" fieldValue="true" value="officeNumbersFromAuxJournals"/>
				</td>
			</tr>
			</ds:extras>
			<ds:modules test="office">
				<tr>
					<td class="alignTop">
						<ds:lang text="ProcesWorkflowDlaSklonowanegoDokumentuWe"/>:
					</td>
					<td>
						<ww:checkbox name="'wfProcessForClonedInDocuments'" fieldValue="true" value="wfProcessForClonedInDocuments"/>
					</td>
				</tr>
			</ds:modules>
			</ds:available>
			
			<tr>
				<td>
					<ds:lang text="SystemDocusafeDzialaJakoInstancja"/>:
				</td>
				<td>
					<ww:select name="'docusafeInstanceType'" list="docusafeTypes" id="docusafeInstanceType"
						listKey="key" listValue="value" cssClass="'sel'"
						value="docusafeInstanceType"/>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="NazwaKlienta"/>:
				</td>
				<td>
					<ww:textfield name="'customer'" size="50" maxlength="50" cssClass="'txt'" id="customer"/>
				</td>
			</tr>
		</ds:modules>
		
		<ww:if test="emergency" >
			<tr>
				<td colspan="2">   
					<ds:lang text="SystemJestWStanieAwaryjnym"/> !!!
				</td>	
			</tr>
		</ww:if>
	</table>

<table border="0">
	<tr>
		<td><input type="button" value="<ds:lang text="StanAwaryjny"/>"
			class="btn"
			onclick="document.location.href='<ww:url value="'/admin/emergency.action'"/>';" />

		</td>
	</tr>
	<tr>
		<td><ds:event name="'doCleanTasklist'"
			value="getText('OptymalizacjaBazyDanych')" /></td>
	</tr>

	<tr>
		<td><ds:event name="'doIndex'"
			value="getText('AktualizujIndeksyTekstowe')" /></td>
	</tr>
	<tr>
		<td><ds:event name="'browserStats'" value="'Statystyki przegl�darek'" /></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" 
		   onclick="document.getElementById('doUpdate').value='true';if (!validate()) return false;" /></td>
	</tr>
</table>
<div id="chart" style="float: left"></div>
</form>

<%--
<h1><ds:lang text="UstawieniaAplikacji"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/application-settings.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doUpdate" id="doUpdate"/>

<table>
<tr>
	<td valign="top"><ds:lang text="Wersja"/>:</td>
	<td><%= Docusafe.getVersion() %>.<%= Docusafe.getDistNumber() %> (<%= Docusafe.getBuildId() %>)</td>
</tr>
<tr>
	<td valign="top"><ds:lang text="DataKompilacji"/>:</td>
	<td><%= Docusafe.getDistTime() %></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="LicencjaDla"/>:</td>
	<td><%= Docusafe.getLicenseString() %></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="ZarejestrowanychUzytkownikow"/>:</td>
	<td><ww:property value="userCount"/></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="InformacjeOsystemie"/>:</td>
	<td><a href="<ww:url value="'/admin/application-settings.action?doSysinfo=true'"/>"><ds:lang text="pobierz"/></a></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="DefinicjaSystemu"/>:</td>
	<td><a href="<ww:url value="'/admin/application-settings.action?doDownload=true'"/>"><ds:lang text="pobierz"/></a></td>
</tr>
<tr>
	<td valign="top"><ds:lang text="AdministratorSystemu"/>:</td>
	<td><ww:select name="'admin'" list="users" listKey="name" headerKey="''" headerValue="'----'"  listValue="asLastnameFirstname()" cssClass="'sel'" value="admin"/></td>
</tr>
<!-- nie usuwa� spacji dooko�a <br> -->

<ds:modules test="simpleoffice or office">
<%--
	<tr>
		<td valign="top">Numery spraw:</td>
		<td><ww:textfield name="'caseFormat'" size="50" maxlength="50" cssClass="'txt'" value="caseFormat" /><br>(wymagane kody: d1, r, c; opcjonalne kody: d0, d2, d3, d4, d5, p, y2, y4)
		</td>
	</tr>
	<tr>
		<td valign="top">Numery pism w sprawach:</td>
		<td><ww:textfield name="'documentFormat'" size="50" maxlength="50" cssClass="'txt'" value="documentFormat" /><br>(wymagane kody: d1, r; opcjonalne kody: d2, d3, d4, d5, d, p, c, y2, y4)
		</td>
	</tr>
	<tr>
		<td valign="top">Numery teczek:</td>
		<td><ww:textfield name="'portfolioFormat'" size="50" maxlength="50" cssClass="'txt'" value="portfolioFormat" /><br>(wymagane kody: d1, r; opcjonalne kody: d2, d3, d4, d5, y2, y4)
		</td>
	</tr>
-->
	<ds:modules test="office">
		<tr>
			<td valign="top"><ds:lang text="GrupaNatychmiastowejDekretacji"/>:</td>
			<td><ww:select name="'instantAssignmentGroup'" list="groups"
				listKey="guid" listValue="name" cssClass="'sel'"
				value="instantAssignmentGroup" headerKey="''" headerValue="'----'" /></td>
		</tr>
	</ds:modules>
	<tr>
		<td valign="top"><ds:lang text="NumeryKancelaryjneZdziennikowDodatkowych"/>:</td>
		<td><ww:checkbox name="'officeNumbersFromAuxJournals'" fieldValue="true" value="officeNumbersFromAuxJournals"/></td>
	</tr>
	<ds:modules test="office">
		<tr>
			<td valign="top"><ds:lang text="ProcesWorkflowDlaSklonowanegoDokumentuWe"/>:</td>
			<td><ww:checkbox name="'wfProcessForClonedInDocuments'" fieldValue="true" value="wfProcessForClonedInDocuments"/></td>
		</tr>
	</ds:modules>
	<tr>
		<td><ds:lang text="SystemDocusafeDzialaJakoInstancja"/>:</td>
		<td><ww:select name="'docusafeInstanceType'" list="docusafeTypes" id="docusafeInstanceType"
				listKey="key" listValue="value" cssClass="'sel'"
				value="docusafeInstanceType"/></td>
	</tr>
	<tr>
		<td>
			<ds:lang text="NazwaKlienta"/>:
		</td>
		<td>
			<ww:textfield name="'customer'" size="50" maxlength="50" cssClass="'txt'" id="customer"/>
		</td>
	</tr>

</ds:modules>
<ww:if test="emergency" >
	<tr>   
		<ds:lang text="SystemJestWStanieAwaryjnym"/> !!!	
	</tr>
</ww:if>
	<tr>
		<td><input type="button" value="<ds:lang text="StanAwaryjny"/>" class="btn"	
		onclick="document.location.href='<ww:url value="'/admin/emergency.action'"/>';"/> </td>
	</tr>
	<tr>
		<td><ds:event name="'doCleanTasklist'" value="getText('OptymalizacjaBazyDanych')"/></td>
	</tr>
	<tr>
		<td><ds:event name="'doCleanAttachments'" value="getText('KompresjaZalacznikow')"/></td>
	</tr>
	
<ds:modules test="office or fax">
<%--
	<tr>
		<td>Domy�lny nadawca/odbiorca:</td>
		<td><ww:checkbox name="'fillDefaultOrganization'" fieldValue="true" value="fillDefaultOrganization"/></td>
	</tr>
--%>
<%--
	<tr>
		<td>Obs�uga kod�w kreskowych:</td>
		<td><ww:checkbox name="'useBarcodes'" fieldValue="true" value="useBarcodes" /></td>
	</tr>
	<tr>
		<td>Prefiks kodu kreskowego:</td>
		<td><ww:textfield name="'barcodePrefix'" size="10" maxlength="10" cssClass="'txt'" value="barcodePrefix"/></td>
	</tr>
	<tr>
		<td>Sufiks kodu kreskowego:</td>
		<td><ww:textfield name="'barcodeSuffix'" size="10" maxlength="10" cssClass="'txt'" value="barcodeSuffix"/></td>
	</tr>
-->
</ds:modules>
<%--
<ds:modules test="office">
	<tr>
		<td>Oddzielne sekwencje numer�w spraw w podteczkach:</td>
		<td><ww:checkbox name="'subportfolioSequences'" fieldValue="true" value="subportfolioSequences"/></td>
	</tr>
</ds:modules>
-->
	<tr><td><input type="submit" name="doUpdate" value="<ds:lang text="Zapisz"/>" class="btn" onclick="document.getElementById('doUpdate').value='true';if (!validate()) return false;"/></td></tr>

</table>


<!-- 
<input type="button" class="btn" value="<ds:lang text="PobierzDefinicje"/>"
		   onclick="document.location.href='<ww:url value="'/admin/application-settings.action?doDownload=true'"/>'"/>
 -->
</form>

<script type="text/javascript">
	function validate()
	{
		var docusafeType = document.getElementById('docusafeInstanceType');
		//alert(docusafeType.value);
        if ((docusafeType != null) && (docusafeType.value != '<ww:property value="docusafeInstanceType"/>'))
		{
            var val = docusafeType.options[docusafeType.options.selectedIndex].firstChild.nodeValue;//docusafeType.options[docusafeType.options.selectedIndex].value;
			//alert(docusafeType.options[docusafeType.options.selectedIndex].firstChild.nodeValue);
			if (!confirm('Czy na pewno chcesz zmieni�, aby Docusafe dzia�a� jako instancja '+val))
				return false;
		}
		return true;
	}
</script>
--%>

<script type="text/javascript">
	function validate()
	{
		var docusafeType = $j("#docusafeInstanceType");
		if ((docusafeType != null) && (docusafeType.val() != '<ww:property value="docusafeInstanceType"/>'))
		{
			if (!confirm('<ds:lang text="CzyNaPewnoChceszZmienicAbyDocusafeDzialalJakoInstancja "/>' + $j("#docusafeInstanceType option:selected").text()))
				return false;
		}
		alert('<ds:lang text="ZapisanoZmiany"/>');
		return true;
	}

(function() {
	var browsersInfoData = '<ww:property value="browsersInfo" escape="false"/>';
	var browsers = {'ie6': [], 'ie7': [], 'ie8': [], 'firefox': [], 'chrome': [], 'other' : []};
	var percentData = null;
	var entitiesLength = 0;
	
	trace(browsers);
	if(browsersInfoData) {
		browsersInfoData = eval('(' + browsersInfoData + ')');

		entitiesCount = browsersInfoData.length;
		for(var i = 0; i < entitiesCount; i ++) {
			var item = browsersInfoData[i];
			switch(item.name) {
				case 'MSIE':
					var ver = parseInt(item.version);
					if(ver == 8)
						browsers.ie8.push(item);
					else if(ver == 7)
						browsers.ie7.push(item);
					else
						browsers.ie6.push(item);
				break;
				case 'Firefox':
					browsers.firefox.push(item);
				break;
				case 'Chrome':
					browsers.chrome.push(item);
				break;
				default:
					browsers.other.push(item);
			}
		}
		percentData = [browsers.ie6.length / entitiesCount,
		       		   browsers.ie7.length / entitiesCount,
		       		   browsers.ie8.length / entitiesCount,
		       		   browsers.firefox.length / entitiesCount,
		       		   browsers.chrome.length / entitiesCount,
		       		   browsers.other.length / entitiesCount];
		for(var i = 0; i < percentData.length; i ++)
			percentData[i] *= 100;	
		trace(percentData);	
		var raph = Raphael("chart");

		/* budujemy legend� wykresu */
		var legend = [];
		legend.push('%%.%% - Internet Explorer 6 (' + browsers.ie6.length + ')');
		legend.push('%%.%% - Internet Explorer 7 (' + browsers.ie7.length + ')');
		legend.push('%%.%% - Internet Explorer 8 (' + browsers.ie8.length + ')');
		legend.push('%%.%% - Mozilla Firefox (' + browsers.firefox.length + ')');
		legend.push('%%.%% - Google Chrome (' + browsers.chrome.length + ')');
		legend.push('%%.%% - Inne (' + browsers.other.length + ')');
		
		var pie = raph.g.piechart(120, 120, 100, percentData, {legend: legend, legendpos: "east"});

		pie.hover(function () {
            this.sector.stop();
            this.sector.scale(1.1, 1.1, this.cx, this.cy);
            if (this.label) {
                this.label[0].stop();
                this.label[0].scale(1.5);
                this.label[1].attr({"font-weight": 800});
            }
        }, function () {
            this.sector.animate({scale: [1, 1, this.cx, this.cy]}, 500, "bounce");
            if (this.label) {
                this.label[0].animate({scale: 1}, 500, "bounce");
                this.label[1].attr({"font-weight": 400});
            }
        });
	}
})();
</script>
<!--N koniec application-settings.jsp N-->