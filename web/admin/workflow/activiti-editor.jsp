<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Edytorprocesow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<iframe src="<c:out value='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}/activiti-explorer/'/>" width="1100" height="650"></iframe>