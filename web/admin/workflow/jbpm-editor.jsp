<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Edytorprocesow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<applet code="org.yaoqiang.bpmn.editor.BPMNEditorApplet" style="border: 1px solid gray;"
archive="<ww:url value="'/yaoqiang-bpmn-editor.jar'" />, <ww:url value="'/yaoqiang-docusafe-addon.jar'" />"
 codebase="."
width="800" height="600">
	<param name="java_arguments" value="-Dfile.encoding=UTF-8">
	<param name="docusafe_address" value="<c:out value='${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}'/>"/>
	<param name="process_name" value="<ww:property value="name"/>"/>
</applet>


<script type="text/javascript">

$j(document).ready(function() {
	var jqApplet = $j('applet');
	jqApplet.attr('width', $j('.mai2').innerWidth()-20).attr('height', $j('.wrapper').innerHeight()-230);
});

</script>