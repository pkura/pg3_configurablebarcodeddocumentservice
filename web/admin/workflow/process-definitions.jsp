<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N process-definitions.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
	<tr>
		<td colspan=3><br></td>
	</tr>
	<tr>
        <td><ds:lang text="Proces"/></td>
        <td><ds:lang text="NazwaProcesu"/></td>
		<td><ds:lang text="WersjaProcesu"/></td>
		<td><ds:lang text="LiczbaProcesow"/></td>
	</tr>

<ww:iterator value="oldDefinitions" status="status">
	<tr>
        <td><a href="<ww:url value='url'/>" title="<ww:property value='name'/>"><ww:property value="name"/></a></td>
        <td><a href="<ww:url value='url'/>" title="<ww:property value='nameForUser'/>"><ww:property value="nameForUser"/></a></td>
		<td><a href="<ww:url value='url'/>" title="<ww:property value='version'/>"><ww:property value="version"/></a></td>
		<td><a href="<ww:url value='url'/>" title="<ww:property value='instancesCount'/>"><ww:property value="instancesCount"/></a></td>
	</tr>
</ww:iterator>

</table>