<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N process-definition-inspect.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine"  width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>
<form action="<ww:url value="baseLink"/>" method="post">

<ww:if test="confirmDel">
	<ds:lang text="Istnieje"/> <ww:property value="processInstances.size"/> <ds:lang text="procesowUtworzonychDlaDefinicji"/> <b><ww:property value="name"/></b> <ds:lang text="wersja"/> <b><ww:property value="version"/></b>
	<br><ds:lang text="CzyNaPewnoChceszUsunacTeDefinicje"/>?<br>
	<ds:submit-event value="getText('TakUsunRazemZeWszystkimiProcesami')" name="'doDelete'" cssClass="'btn'"/>
	<ds:submit-event value="getText('NieUusuwaj')" cssClass="'btn'"/>
	

</ww:if>
<ww:else>

<table>
	<tr>
		<td><br></td>
	</tr>
	<tr>
		<td><ds:lang text="NazwaDefinicjiProcesu"/>:</td>
		<td><b><ww:property value="name"/></b></td>
	</tr>
    <tr>
		<td><ds:lang text="NazwaWyswietlanaUzytkownikowi"/>:</td>
		<td><ww:textfield name="'nameForUser'" size="50" value="nameForUser"/></td>
	</tr>
    <tr>
		<td><ds:lang text="WersjaDefinicjiProcesu"/>:</td>
		<td><b><ww:property value="version"/></b></td>
	</tr>
	<tr>
		<td><ds:submit-event name="'doUpdate'" value="getText('ZapiszZmiany')" cssClass="'btn'"/> <ds:submit-event value="getText('UsunDefinicje')" name="'doConfirm'" cssClass="'btn'"/></td>
	</tr>
	<tr>
		<td><br></td>
	</tr>
	<tr>
		<%--<td colspan=2><img src="<ww:url value="image"/>"/></td>--%>
        <td colspan="2"><img src="<ww:url value="image"/>"/></td>
    </tr>

	
<ww:if test="processInstances.size>0">
<tr><td colspan=2>
<table>
	<tr>
		<td colspan=3><b><ds:lang text="ProcesyRozpoczeteWramachDefinicji"/></b></td>
	</tr>
	<tr>
		<td><ds:lang text="ID"/></td>
		<td><ds:lang text="DataRozpoczecia"/></td>
		<td><ds:lang text="DataZakonczenia"/></td>
	</tr>
	<ww:iterator value="processInstances" status="status">
		<tr>
			<td><a href="<ww:url value='url'/>" title="<ww:property value='id'/>"><ww:property value="id"/></a></td>
			<td><a href="<ww:url value='url'/>" title="<ww:property value='start'/>"><ww:property value="start"/></a></td>
			<td><a href="<ww:url value='url'/>" title="<ww:property value='end'/>"><ww:if test="end==null"><ds:lang text="trwa"/></ww:if>
			<ww:else><ww:property value="end"/></ww:else></a></td>
		</tr>
	</ww:iterator>
</table>
</td></tr>
</ww:if>


</table>

</ww:else>
</form>