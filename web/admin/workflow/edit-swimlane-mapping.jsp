<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-swimlane-mapping.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine"  width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h2><ww:if test="creating"><ds:lang text="NoweMapowanie"/></ww:if><ww:else><ds:lang text="EdycjaMapowania"/></ww:else></h2>

<form action="<ww:url value="'/admin/workflow/edit-swimlane-mapping.action'"/>" method="post" onsubmit="return disableFormSubmits(this);">
    <ww:hidden name="'creating'" value="creating"/>
    <ww:hidden name="'id'" value="id"/>

    <table>
        <tr>
            <td colspan="2"><br/></td>
        </tr>
        <tr>
            <td><ds:lang text="NazwaDzialuWWorkflow"/></td>
            <td><ww:textfield name="'name'" value="name" id="name" readonly="!creating" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td><ds:lang text="PrzyporzadkowanyDzial"/></td>
            <td>
                <ww:select name="'divisionGuid'" list="divisions"
				    cssClass="'sel'" listKey="guid" listValue="name"
				    id="division" value="divisionGuid"/>
            </td>
        </tr>
        <tr>
            <td><ds:lang text="SposobDekretacji"/></td>
            <td>
                <ww:select name="'assignmentType'" id="assignmentType" cssClass="'sel'"
                    list="assignmentTypes" listKey="key" listValue="value"/>
            </td>                    
        </tr>
        <tr>
            <td></td>
            <td>
                <ww:if test="creating">
                   <ds:event name="'doCreate'" value="getText('Dodaj')" cssClass="'btn'"/>
                </ww:if>
                <ww:else>
                    <ds:event name="'doUpdate'" value="getText('Zapisz')" cssClass="'btn'"/>
                    <ds:event name="'doDelete'" value="getText('Usun')" cssClass="'btn'" confirm="getText('NaPewnoUsunacToMapowanie')"/>
                </ww:else>
            </td>
        </tr>
    </table>
</form>