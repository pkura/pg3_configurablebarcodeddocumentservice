<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N swimlane-mapping.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://com-pan.pl/edm/tags-fmt" prefix="edm-fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
    <tr>
        <td colspan="2"><br/></td>
    </tr>
    <tr>
        <td><ds:lang text="NazwaDzialuWWorkflow"/></td>
        <td><ds:lang text="PrzyporzadkowanyDzial"/></td>
        <td><ds:lang text="SposobDekretacji"/></td>
	</tr>

    <ww:iterator value="swimlanes" status="status">
	<tr>
        <td><a href="<ww:url value="'/admin/workflow/edit-swimlane-mapping.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="name"/></a></td>
        <td><a href="<ww:url value="'/admin/workflow/edit-swimlane-mapping.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="divisionName"/></a></td>
        <td><a href="<ww:url value="'/admin/workflow/edit-swimlane-mapping.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="assignmentType"/></a></td>
	</tr>
    </ww:iterator>
    <tr>
        <td colspan="2"><br/></td>
    </tr>
    <tr>
        <td colspan="2"><input type="button" value="<ds:lang text="DodajNoweMapowanie"/>" class="btn" onclick="gotoCreate()"/></td>
    </tr>

</table>


<script type="text/javascript">
    function gotoCreate()
    {
        document.location.href = '<ww:url value="'/admin/workflow/edit-swimlane-mapping.action'"><ww:param name="'doInitCreate'" value="true"/></ww:url>';
    }
</script>