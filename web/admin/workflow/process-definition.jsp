<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DefinicjeProcesow"/></h1>
<ds:available test="!layout2">
	<p><ds:xmlLink path="PanelWorkflow"/></p>
	</ds:available>
	
	<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PanelWorkflow"/>
	</div>
</ds:available>

<style>
.editorSpacer {
	margin-bottom: -5px;
}
</style>
	

<ww:form action="'process-definition'" namespace="'/admin/workflow'" method="'post'" enctype="'multipart/form-data'" >
	
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
	<p></p>
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<div style="padding: 20px;">
		<ww:if test="allDefinitions.size() > 0">
		<table>
			<tr>
				<th>Nazwa</th>
				<th>Wersja</th>
				<th>Liczba proces�w</th>
				<th>Liczba proces�w aktywnych</th>
				<th>Zasoby</th>
				<th>Procesy</th>
				<ds:available test="getProcessSchema">
					<th>Schematy</th>
				</ds:available>				
			</tr>
			<ww:iterator value="allDefinitions" id="procDef">
				<tr>
					<td><b><ww:property value="name"/></b>
					<ww:if test="versions.size > 0">
						<img onclick="showVersions('row_<ww:property value="name"/>')" src="<ww:url value="'/img/list-plus.gif'"/>"
						id="plus_row_<ww:property value="name"/>"/>
						<img onclick="hideVersions('row_<ww:property value="name"/>')" src="<ww:url value="'/img/list-minus.gif'"/>"
						id="minus_row_<ww:property value="name"/>" style="display : none;"/>
					</ww:if></td>
					<td><ww:property value="version"/></td>
					<td><ww:property value="processNum"/></td>
					<td><ww:property value="activeProcessNum"/></td>
					<td><ww:property value="resources"/></td>
					<td>
						<a href="<ww:url value="baseLink"><ww:param name="'doGetBpmn'" value="true"/><ww:param name="'name'" value="name"/></ww:url>">wy�wietl</a>
						<ds:available test="dockindEditor">
							<img src="<ww:url value="'/img/vline.gif'" />" class="editorSpacer"/>
							<a href="<ww:url value="'/admin/workflow/jbpm-editor.action'"><ww:param name="'name'" value="name"/></ww:url>">otw�rz w edytorze</a>
						</ds:available>
					</td>
					<ds:available test="getProcessSchema">
						<td><a href="<ww:url value="baseLink"><ww:param name="'doGetSchema'" value="true"/><ww:param name="'name'" value="name"/></ww:url>">Pobierz schemat</a></td>
					</ds:available>		
				</tr>
				<ww:iterator value="versions" id="ver">
				<tr name="row_<ww:property value="name"/>" style="display : none; ">
					<td></td>
					<td><ww:property value="version"/></td>
					<td><ww:property value="processNum"/></td>
					<td><ww:property value="activeProcessNum"/></td>
					<td><ww:property value="resources"/></td>
					<td><a href="<ww:property value="searchProcessURL"/>">wy�wietl</a></td>
				</tr>
				</ww:iterator>
			</ww:iterator>
		</table>
		</ww:if>
		<p></p>
		Za�aduj definicj� procesu:
		<table><tr>
		<td><input type="radio" name="source" value="fromFile" onchange="radioSelected()" id="fromFile">Z pliku:</td>
		<td><ww:file id="fromFileInput" name="'newProcessDef'" /></td>
		<td>
		    <ww:checkbox id="validate" name="'validate'" fieldValue="true" value="validate" cssClass="'styled-disabled dontFixWidth'"/>
		    <ds:lang text="walidacja"/>
		</td>
		</tr><tr>
		<td><input type="radio" name="source" value="predefined" onchange="radioSelected()" id="predefined">Predefiniowan�:</td>
		<td>
		<ww:select id="predefinedInput" name="'predefinedToLoad'" list="predefinedList"/>
		</td>
		</tr>

		</table>
		<ds:event name="'doLoad'" value="'Za�aduj'"/>
	
		<script type="text/javascript">
			document.getElementById('fromFileInput').disabled = true;
			document.getElementById('predefinedInput').disabled = true;
			document.getElementById('validate').disabled = true;

			function radioSelected() {
				var file = document.getElementById('fromFile');
				var predef = document.getElementById('predefined');

				if (file.checked == true) {
					document.getElementById('fromFileInput').disabled = false;
					document.getElementById('predefinedInput').disabled = true;
					document.getElementById('validate').disabled = false;
				}
				if (predef.checked == true) {
					document.getElementById('fromFileInput').disabled = true;
					document.getElementById('predefinedInput').disabled = false;
					document.getElementById('validate').disabled = true;
				}
			}

			function showVersions(rowId) {
				var rows = document.getElementsByName(rowId);
				for (var i=0; i<rows.length; i++) {
					rows[i].style.display = '';
				}
				document.getElementById('plus_'+rowId).style.display = 'none';
				document.getElementById('minus_'+rowId).style.display = '';
			}

			function hideVersions(rowId) {
				var rows = document.getElementsByName(rowId);
				for (var i=0; i<rows.length; i++) {
					rows[i].style.display = 'none';
				}
				document.getElementById('plus_'+rowId).style.display = '';
				document.getElementById('minus_'+rowId).style.display = 'none';
			}
		</script>
		
	</div>
	
		<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:form>