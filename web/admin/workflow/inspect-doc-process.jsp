<%@ page contentType="text/html; charset=iso-8859-2"%>
<%@ taglib uri="webwork" prefix="ww"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="InspekcjaZadanDlaDokumentu"/></h1>
<ds:available test="!layout2">
	<p><ds:xmlLink path="PanelWorkflow"/></p>
</ds:available>
	
<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
	<ds:xmlLink path="PanelWorkflow"/>
	</div>
</ds:available>

<ww:form action="'inspect-doc-process'" namespace="'/admin/workflow'" method="'post'" enctype="'multipart/form-data'">
	
	<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
	
	
	
	<p></p>
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
	<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<div style="padding: 20px;">
	Workflow: <ww:if test="jbpm"> jBPM</ww:if> <ww:else> Internal </ww:else>
	<table>
		<tr>
			<td>
				<ds:lang text="IdDokumentu"/>
			</td>
			<td>
				<ww:textfield name="'docId'" />
			</td>
		</tr>
			<ds:available test="inspectDocProcess.odswiezWieleDokumentow">
				<tr>
					<td><ds:lang text="IdDokumentow" /></td>
					<td><ww:textfield name="'docIdsToRefresh'" /></td>
					<td><ds:event name="'RefreshTasklistToList'"
							value="'Od�wie� list� zada� dla danych dokument�w'" />							
							<ds:infoBox
							body="'Dokumenty podawane po przecinku.'"
							header="'Informacja'" />
							</td>

				</tr>
			</ds:available>
		</table>
	<ww:submit cssClass="'btn'" value="'Poka� zadania'"/>
	
	<ww:if test="showTasks">
		<h2><ds:lang text="ZadanieWObieguRecznym"/></h2>
			<table>
				<tr>
					<td>
						<b><ds:lang text="IdZadania"/></b>
					</td>
					<td>
						<ww:property value="manual.taskId"/>
					</td>
				</tr>
				<tr>
					<td>
						<b><ds:lang text="PrzypisanyUzytkownik"/></b>
					</td>
					<td>
						<ww:property value="userFullName"/>
					</td>
				</tr>
				<tr>
					<td>
						<b><ds:lang text="PrzypisanyDzial"/></b>
					</td>
					<td>
						<ww:property value="divFullName"/>
					</td>
				</tr>
				<ww:if test="participantId != null">				
				<tr>
					<td>
						<b><ds:lang text="ParticipantId"/></b>
					</td>
					<td>
						<ww:property value="participantId"/>
					</td>
				</tr>
				</ww:if>
			</table>
			<script type="text/javascript" charset="iso-8859-2">
				function closeConfirm() {
					return confirm('Czy na pewno zako�czy� prac� z zadaniem?');
				}
				function multiCloseConfirm() {
					return confirm('Czy na pewno zako�czy� prac� z zadaniami?');
				}
			</script>
			<ds:event name="'doRefreshTasklist'" value="'Od�wie� list� zada� dla danego dokumentu'"/>
			<ds:event name="'doClose'" onclick="'if(!closeConfirm()){return false}'" value="'Zamknij awaryjnie'"/>
            <ds:event name="'doCloseActiviti'" onclick="'if(!closeConfirm()){return false}'" value="'Zamknij awaryjnie (activiti)'"/>
			<ww:if test="allowRefresh">
				<ds:event name="'doRefresh'" value="'Od�wie� proces akceptacji'"/>
			</ww:if>
		<p></p>
		<input type="hidden" name="processName" id="processName"/>
		<input type="hidden" name="processId" id="processId"/>
		<input type="hidden" name="processAction" id="processAction"/>
		<ww:iterator value="processRenderBeans">
			<ds:render template="template"/>
		</ww:iterator>
		<%--
		<ww:if test="cc.size > 0">
			<h2><ds:lang text="ZadaniaDoWiadomosci"/></h2>
			<ww:iterator value="cc" id="ccTask">
				<ww:property value="ccTask.taskId"/><br>
			</ww:iterator>
		</ww:if>
		 --%>
        <br/>
	    <h2>Zbiorowe zamykanie proces�w</h2>
	 	<table>
    		<tr>
    			<td>
    				Lista id dokument�w (rozdziela� przecinkiem):
    			</td>
    			<td>
    				<ww:textfield name="'docIds'" />
    			</td>
    		</tr>
    	</table>
		<ds:event name="'doMultiClose'" onclick="'if(!multiCloseConfirm()){return false}'" value="'Zamknij awaryjnie'"/>
	</ww:if>
	</div>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</ww:form>
<script type="text/javascript">
	function validateForm()
	{
		if(document.getElementsByName('docId')[0].value != "")
			return true;		
		else
			return false;
	}
</script>