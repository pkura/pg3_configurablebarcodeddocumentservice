<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N process-deploy.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine"  width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<form action="<ww:url value="baseLink"/>" method="post" enctype="multipart/form-data">
	<table>
			<tr>
				<td><ds:lang text="WybierzArchiwumZipZawierajaceDefinicjeProcesu"/>:</td>
			</tr>
			<tr>
                <td><ww:file name="'file'" id="file" size="50" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td><ds:lang text="NazwaTworzonejDefinicjiWyswietlanaUzytkownikowi"/></td>
            </tr>
            <tr>
                <td><ww:textfield name="'nameForUser'" id="nameForUser" size="50" cssClass="'txt'"/></td>
            </tr>
            <tr>
        		<td><ds:submit-event value="getText('Dodaj')" name="'doDeploy'" cssClass="'btn'"/></td>
    		</tr>
            
	</table>
</form>