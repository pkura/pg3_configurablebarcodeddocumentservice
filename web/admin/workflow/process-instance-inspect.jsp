<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N process-instance-inspect.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:iterator value="tabs" status="status" >
	<ww:if test="selected">
		<h1><ww:property value='title'/></h1>
	</ww:if>
</ww:iterator>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ww:iterator value="tabs" status="status" >
    <a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
    <ww:if test="!#status.last"> <img src="<ww:url value="'/img/kropka.gif'"/>" width="6" height="10"/> </ww:if>
</ww:iterator>

<form action="<ww:url value="baseLink"/>" method="post">

<ww:if test="confirmDel">
	<br><ds:lang text="CzyNaPewnoChceszUsunacProcesNumer"/> <b><ww:property value="processId"/></b> <ds:lang text="oDefinicji"/> 
	<b><ww:property value="name"/></b> <ds:lang text="wersja"/> <b><ww:property value="version"/></b>?<br>
	<ds:submit-event value="getText('TakUsun')" name="'doDelete'" cssClass="'btn'"/>
	<ds:submit-event value="getText('NieUsuwaj')" cssClass="'btn'"/>
	

</ww:if>
<ww:else>
<table>
	<tr>
		<td colspan=2><ds:lang text="RodzajProcesu"/>: <b><ww:property value="name"/></b> <ds:lang text="wersja"/> <b><ww:property value="version"/></b></td>
	</tr>
	<tr>
		<td colspan=2><br></td>
	</tr>
	<tr>
		<td colspan=2><b><ds:lang text="ZmienneProcesu"/></b></td>
	</tr>
	<tr>
		<td><ds:lang text="Nazwa"/></td>
		<td><ds:lang text="Wartosc"/></td>
	</tr>
    <ww:iterator value="variables" status="status">
        <tr>
            <td><ww:property value="key"/></td>
            <td><ww:textfield name="key" value="value" size="30" maxlength="50" cssClass="'txt'"/></td>
        </tr>
    </ww:iterator>
	<tr>
		<td><ds:submit-event value="getText('UsunProces')" name="'doConfirm'" cssClass="'btn'"/></td>
		<td><ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn'"/></td>
	</tr>



</table>


</ww:else>

</form>