<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N wf-synchronize.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h3>Synchronizacja z serwerem workflow</h3>

<edm-html:errors />
<edm-html:messages />

<html:form action="/admin/wf-synchronize">

<p>Serwer workflow: <c:out value="${wfHost}:${wfPort}/${wfServerName}"/></p>

<h4>Tworzenie mapowa� u�ytkownik�w w wybranych procesach</h4>

<table>
<c:forEach items="${processMgrs}" var="mgr">
    <tr>
        <td><input type="checkbox" name="processes" value="<c:out value="${mgr.value}"/>"/></td>
        <td>
            <c:out value="${mgr.label}"/>
        </td>
    </tr>
</c:forEach>
<tr>
    <td colspan="2"><input type="submit" name="doSynchronize" value="Utw�rz mapowania" class="btn"/></td>
</tr>
</table>

</html:form>

