<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1><ds:lang text="DrkFlagsAssociation"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/drk-flags-association.action'"/>" method="post">
<ww:hidden name="'bean'" id="bean"></ww:hidden>
<table border="0" cellspacing="5" cellpadding="0">
<tr>
	<th><ds:lang text="Flaga"/></th>
	<th><ds:lang text="OpisFlagi"/></th>
	<th><ds:lang text="Dzial"/></th>
</tr>
<ww:iterator value="bean" status="status">
<tr>
	<td><input type="text" value="<ww:property value="flaga"/>" name="flagName" class="txt" readonly="readonly"/></td>
	<td><input type="text" value="<ww:property value="opisFlagi"/>" class="txt" readonly="readonly"/></td>
	<td>
		<ww:select id="guid" name="'guid'" cssClass="'sel'" list="dzialy" listKey="guid" listValue="name" headerKey="''" headerValue="getText('select.wybierz')"/>
	</td>
</tr>
</ww:iterator>
<tr>
	<td>
		<input type="submit" name="doSaveAssociation" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" />
	</td>
	<td></td>
	<td></td>
</tr>
</table>

</form>
