<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>
<form id="form" action="<ww:url value="'/admin/all-dsservice.action'"><ww:param name="'allServices'" value="'true'"/></ww:url>" 
	method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'allServices'" value="allServices"/>
	<ww:if test="allServices">
		<ds:lang text="wybierzSerwis"/>
		<ww:select name="'serviceId'" id="servicesIDs" list="availableServices" value="serviceId" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" onchange="'reloadPage()'"/>
		<br/><br/>
	</ww:if>
	<ww:include page="/admin/dsservice-base.jsp"/>
<script type="text/javascript">
	function reloadPage()
	{
		document.forms[0].submit();
	}
</script>
</form>