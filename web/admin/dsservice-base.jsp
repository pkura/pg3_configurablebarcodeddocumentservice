<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ww:property value="serviceName"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/admin/dsservice.action'"><ww:param name="'serviceId'" value="serviceId"/></ww:url>" 
	method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'driverId'"/>
	<ww:select name="'newDriverId'" list="drivers" value="driverId" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'"/>
	<ds:submit-event value="getText('Zmien')" name="'doSetDriver'" cssClass="'btn'" disabled="!canSetDriver" />

	<ww:if test="!parameters.empty">
		<hr class="shortLine"/>

		<table class="tableMargin">
			<ww:iterator value="parameters">
				<tr>
					<td>
						<ww:property value="description"/>:
					</td>
					<td>
						<ww:if test="widget == 'select'">
							
							<ww:if test="wantsReport">
								<input type="hidden" name="doReport" id="doReport"/>
								<input type="hidden" name="reportId" id="reportId"/>

								<ww:select id="report" name="'params.'+name" cssClass="'sel'" value="value"
								list="constraints" listKey="key" listValue="value"   
								headerKey="''" headerValue="getText('select.wybierz')"/>
								<a id="repoPobierz"><b>Pobierz raport</b></a>
							</ww:if>

							<ww:else>
								<ww:select name="'params.'+name" cssClass="'sel'" value="value"
								list="constraints" listKey="key" listValue="value"   
								headerKey="''" headerValue="getText('select.wybierz')"/>
							</ww:else>

						</ww:if>
						
						<ww:elseif test="widget == 'textfield'">
							<ww:if test="wantsGuid">
								<input type="hidden" value="<ww:property value="value"/>"
									name="params.<ww:property value="name"/>"
									id="id.<ww:property value="name"/>"/>
		
								<input type="text" size="50" maxlength="100" class="txt" readonly="true"
									value="<ww:property value="prettyValue"/>"
									id="id.<ww:property value="name"/>.pretty"/>
		
								<input type="button" value="<ds:lang text="Wybierz"/>" class="btn"
									onclick="javascript:void(window.open('<ww:url value="'/office/pick-division.action'"/>?reference=id.<ww:property value="name"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"/>
							</ww:if>
							
							<ww:elseif test="wantsInDocumentDelivery">
								<ww:select name="'params.'+name" cssClass="'sel'" value="value"
									list="inDocumentDeliveries" listKey="id" listValue="name"
									headerKey="''" headerValue="getText('select.wybierz')" />  
							</ww:elseif>
							
							<ww:elseif test="wantsDate">
								<ww:textfield id="dateValue" name="'params.'+name" value="value" size="10" maxlength="10" cssClass="'txt'" disabled="disabled" />
								<img src="<ww:url value="'/calendar096/img.gif'"/>" id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
							</ww:elseif>

							<ww:else>
								<ww:textfield name="'params.'+name" value="value" size="50" maxlength="1000"
									cssClass="'txt'" disabled="disabled" />
							</ww:else>
						</ww:elseif>
						
						<ww:elseif test="widget == 'checkbox'">
							<ww:checkbox name="'params.'+name" fieldValue="true" value="value" disabled="disabled" />
						</ww:elseif>
						
						<ww:elseif test="widget == 'password'">
							<ww:password name="'params.'+name" maxlength="1000" size="50" value="value" cssClass="'txt'" show="true" />
						</ww:elseif>
						
						<ww:elseif test="date">
							<ww:textfield id="dateValue" name="'params.'+name" value="value" size="10" maxlength="10" cssClass="'txt'" disabled="disabled" />
							<img src="<ww:url value="'/calendar096/img.gif'"/>" id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;" title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
						</ww:elseif>
						
						<ww:else>
							<ww:textfield name="'params.'+name" value="value" size="50" maxlength="1000"
									cssClass="'txt'" disabled="disabled" />
						</ww:else>
					</td>
				</tr>
			</ww:iterator>
		</table>
		
		<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>
	</ww:if>
		
	<ww:if test="runtimeProperties.size > 0">
		<hr class="shortLine"/>
	
		<table class="tableMargin">
			<ww:iterator value="runtimeProperties">
				<tr>
					<td>
						<ww:property value="key"/>:
					</td>
					<td>
						<ww:property value="value"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>
	
	<ww:if test="serviceId == 'fax'">
		<p>
			<a href="<ww:url value="'/fax/view-staging-area.action'"/>"><ds:lang text="Poczekalnia"/></a>
		</p>
	</ww:if>
	
	<ww:if test="showConsole">
		<h3><ds:lang text="Konsola"/></h3>
	
		<ww:select name="''" list="messages" listKey="''" size="15" id="console" cssClass="'multi_sel dontFixWidth'" cssStyle="'width:100%;'"/>
		<br/>
		<span id="consoleStatus">&nbsp;</span>
	</ww:if>
</form>

<%--
<h1><ww:property value="serviceName"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dsservice.action'"><ww:param name="'serviceId'" value="serviceId"/></ww:url>" method="post" onsubmit="disableFormSubmits(this);">
	<%--<ww:hidden name="'serviceId'"/>
	<ww:hidden name="'driverId'"/>
	
	<table>
		<tr>
			<td colspan="2">
				<ww:select name="'newDriverId'" list="drivers" value="driverId"
					headerKey="''" headerValue="getText('select.wybierz')"
					cssClass="'sel'"/>
				<ds:submit-event value="getText('Zmien')" name="'doSetDriver'" cssClass="'btn'" disabled="!canSetDriver" />
			</td>
		</tr>
		<ww:if test="!parameters.empty">
			<tr>
				<td colspan="2"><hr/></td>
			</tr>
			<ww:iterator value="parameters">
				<tr>
					<td><ww:property value="description"/>:</td>
					<td>
						<ww:if test="widget == 'select'">
							<ww:select name="'params.'+name" cssClass="'sel'" value="value"
								list="constraints" listKey="key" listValue="value"   
								headerKey="''" headerValue="getText('select.wybierz')"/>
						</ww:if>
						<ww:if test="widget == 'textfield'">
							<ww:if test="wantsGuid">
								<input type="hidden" value="<ww:property value="value"/>"
									name="params.<ww:property value="name"/>"
									id="id.<ww:property value="name"/>"/>
		
								<input type="text" size="50" maxlength="100" class="txt" readonly="true"
									value="<ww:property value="prettyValue"/>"
									id="id.<ww:property value="name"/>.pretty"/>
		
								<input type="button" value="<ds:lang text="Wybierz"/>" class="btn" onclick="javascript:void(window.open('<ww:url value="'/office/pick-division.action'"/>?reference=id.<ww:property value="name"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no,scrollbars=yes'));"/>
							</ww:if>
							<ww:elseif test="wantsInDocumentDelivery">
								<ww:select name="'params.'+name" cssClass="'sel'" value="value"
									list="inDocumentDeliveries" listKey="id" listValue="name"
									headerKey="''" headerValue="getText('select.wybierz')" />  
							</ww:elseif>
							<ww:else>
								<ww:textfield name="'params.'+name" value="value" size="50" maxlength="100" cssClass="'txt'" disabled="disabled" />
							</ww:else>
						</ww:if>
						<ww:elseif test="widget == 'checkbox'">
							<ww:checkbox name="'params.'+name" fieldValue="true" value="value" disabled="disabled" />
						</ww:elseif>
					</td>
				</tr>
			</ww:iterator>
			<tr>
				<td></td>
				<td><ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/></td>
			</tr>
		</ww:if>
	</table>
	
	<ww:if test="runtimeProperties.size > 0">
		<hr size="1"/>
	
		<table>
			<ww:iterator value="runtimeProperties">
				<tr>
					<td><ww:property value="key"/>:</td>
					<td><ww:property value="value"/></td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>
	
	<ww:if test="serviceId == 'fax'">
		<p><a href="<ww:url value="'/fax/view-staging-area.action'"/>"><ds:lang text="Poczekalnia"/></a></p>
	</ww:if>
		
	<ww:if test="showConsole">
		<h3><ds:lang text="Konsola"/></h3>
	
		<table width="800">
			<tr>
				<td>
					<ww:select name="''" list="messages" listKey="''" size="15" id="console"
						cssClass="'multi_sel'" cssStyle="'width: 100%'"/>
				</td>
			</tr>
			<tr>
				<td><span id="consoleStatus"></span></td>
			</tr>
		</table>
	</ww:if>

</form>
--%>

<script type="text/javascript">
	$j("#repoPobierz").click(function() { getReport(); } );
	function getReport()
	{
		document.getElementById('doReport').value = true;

		for(var i=0; i < document.getElementById('report').options.length; i++)
		{
			if(document.getElementById('report').options[i].selected)
			{
				document.getElementById('reportId').value = document.getElementById('report').options[i].value;
			}
		}
		

    	document.getElementById('form').submit();
		return false;
	}

if (document.getElementById('dateValue') != null)
{
	Calendar.setup({
		inputField	 :	"dateValue",	 // id of the input field
		ifFormat	   :"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
		button		 :	"documentDateTrigger",  // trigger for the calendar (button ID)
		align		  :	"Tl",		   // alignment (defaults to "Bl")
		singleClick	:	true
	});
}
<%--
	function pickDivision(reference, guid, prettyPath)
	{
		if (reference != null && guid != null)
		{
			document.getElementById(reference).value = guid;
			document.getElementById(reference+'.pretty').value = prettyPath;
		}
	}
	
	function displayConsoleEntry()
	{
		var sel = document.getElementById('console');
		var status = document.getElementById('consoleStatus');
		if (sel.options.selectedIndex >= 0)
		{
			status.innerHTML = sel.options[sel.options.selectedIndex].text;
		}
	}

	addEventHandler(document.getElementById('console'), 'change', displayConsoleEntry);
--%>
	function displayConsoleEntry()
	{
		var console = $j("#console option:selected");
		var consoleStatus = $j("#consoleStatus");
		if ( console.val() >= 0)
		{
			consoleStatus.html(console.text());
		}
	}

	$j("#console").click(function() { displayConsoleEntry(); } );
</script>

<!--N koniec dsservice.jsp N-->