<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 07.08.08
	Poprzednia zmiana: 07.08.08
C--%>
<!--N sessions.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Sesje"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<style>

div.exp {
    height: 20px;
    overflow: hidden;
    transition: height 200ms ease;
}

div.exp:focus {
    height: auto;
}

</style>

<form action="<ww:url value="'/admin/sessions.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<p>
		<ds:lang text="LiczbaSesji"/>: <ww:property value="sessionBeans.size"/>
	</p>
	
	<ww:if test="!sessionBeans.empty">
		<table class="search tableMargin">
			<tr>
				<th>
					<ds:lang text="Uzytkownik"/>
				</th>
				<th>
					<ds:lang text="ImieInazwisko"/>
				</th>
				<th>
					<ds:lang text="Utworzona"/>
				</th>
				<th>
					<ds:lang text="OstatnioAktywna"/>
				</th>
				<th>
					IP
				</th>
				<ds:available test="menu.left.kancelaria.dwrprzyjmipismo">
				<th>
				    Pliki
				</th>
				</ds:available>
			</tr>
			<ww:iterator value="sessionBeans">
				<ww:set name="sessionCreationTime" value="sessionCreationTime" scope="page"/>
				<ww:set name="sessionLastAccessedTime" value="sessionLastAccessedTime" scope="page"/>
				<tr>
					<td>
						<ww:property value="userPrincipal.name"/>
					</td>
					<td>
						<ww:property value="fullNamePrincipal.name"/>
					</td>
					<td>
						<fmt:formatDate value="${sessionCreationTime}" type="both" pattern="dd-MM-yy HH:mm"/>
					</td>
					<td>
						<fmt:formatDate value="${sessionLastAccessedTime}" type="both" pattern="dd-MM-yy HH:mm"/>
					</td>
					<td>
						<ww:property value="clientAddr"/>
					</td>
					<ds:available test="menu.left.kancelaria.dwrprzyjmipismo">
                    <td>
                        <div class="exp" tabindex="1">
                        <table>
                            <ww:iterator value="dwrFiles" status="stat">
                                <ww:set name="fields" />

                                <tr>
                                    <td><ww:property value="#fields.key" /></td>

                                    <td>
                                        <table>
                                            <ww:iterator value="#fields.value">
                                                <ww:set name="field" />
                                                <tr><td>
                                                    <ww:property value="#field.name" />
                                                </tr></td>
                                            </ww:iterator>
                                        </table>
                                    </td>
                                </tr>
                            </ww:iterator>
                        </table>
                        </div>
                    </td>
                    </ds:available>
				</tr>
			</ww:iterator>
		</table>
	
		<br/>
		<ds:submit-event value="getText('WylogujWszystkich')" name="'doLogout'" cssClass="'btn'"/>
	</ww:if>
</form>

<%--
<h1><ds:lang text="Sesje"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/sessions.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">

<p>

<ds:lang text="LiczbaSesji"/>: <ww:property value="sessionBeans.size"/>
<br/>

<ww:if test="!sessionBeans.empty">
	<table class="search">
		<tr>
			<th><ds:lang text="Uzytkownik"/></th>
			<th><ds:lang text="ImieInazwisko"/></th>
			<th><ds:lang text="Utworzona"/></th>
			<th><ds:lang text="OstatnioAktywna"/></th>
			<th>IP</th>
		</tr>
		<ww:iterator value="sessionBeans">
			<ww:set name="sessionCreationTime" value="sessionCreationTime" scope="page"/>
			<ww:set name="sessionLastAccessedTime" value="sessionLastAccessedTime" scope="page"/>
			<tr>
				<td><ww:property value="userPrincipal.name"/></td>
				<td><ww:property value="fullNamePrincipal.name"/></td>
				<td><fmt:formatDate value="${sessionCreationTime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
				<td><fmt:formatDate value="${sessionLastAccessedTime}" type="both" pattern="dd-MM-yy HH:mm"/></td>
				<td><ww:property value="clientAddr"/></td>
			</tr>
		</ww:iterator>
	</table>

	<ds:submit-event value="getText('WylogujWszystkich')" name="'doLogout'" cssClass="'btn'"/>
</ww:if>

</form>
--%>
<%--
<p>Bie��ca sesja: <%= request.getSession() != null ? request.getSession().getId() : ""%></p>
--%>
<!--N koniec sessions.jsp N-->