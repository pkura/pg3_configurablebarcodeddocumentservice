<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N collective-assignment.jsp N-->

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="ZbiorczaDekretacja"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/collective-assignment.action'"/>" method="post" onsubmit="disableFormSubmits(this)">
<input type="hidden" name="doSave" id="doSave"/>
<input type="hidden" name="doDelete" id="doDelete"/>

<table>
    <tr>
        <td><ds:lang text="PozwolNaZbiorczaDekretacje"/>:</td>
        <td><ww:checkbox name="'enable'" fieldValue="true" value="enable"/></td>
    </tr>
    <tr>
        <td valign="top"><ds:lang text="GrupaZbiorczejDekretacji"/>:</td>
        <td><ww:select name="'collectiveAssignmentGroup'" list="groups"
            listKey="guid" listValue="name" cssClass="'sel'"
            value="collectiveAssignmentGroup" headerKey="''" headerValue="getText('select.brak')" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" onclick="document.getElementById('doSave').value='true';"/>
        </td>
    </tr>
</table>

<h4><ds:lang text="CeleDekretacjiZbiorczej"/></h4>
    <table>
    <ww:if test="collectiveAssignments.size > 0">
        <ww:iterator value="collectiveAssignments">
        <tr>
            <td><ww:checkbox name="'ids'" fieldValue="id"/></td>
            <td><ww:property value="objective"/> [<a href="<ww:url value="'/admin/edit-collective-assignment-entry.action'"><ww:param name="'id'" value="id"/></ww:url>">edycja</a>]</td>
        </tr>
        </ww:iterator>
    </ww:if>
    <ww:else>
        <tr>
            <td colspan="2"><p><i><ds:lang text="BrakZdefiniowanychCelowDekretacjiZbiorczej"/></i></p></td>
        </tr>
    </ww:else>
    <tr>
        <td colspan="2">
            <input type="button" value="<ds:lang text="NowyCelDekretacji"/>" onclick="document.location.href='<ww:url value="'/admin/edit-collective-assignment-entry.action'"/>';" class="btn"/>
            <input type="submit" name="doDelete" value="<ds:lang text="Usun"/>" class="btn" onclick="document.getElementById('doDelete').value='true';"/>
        </td>
    </tr>
    </table>
</ww:if>

</form>