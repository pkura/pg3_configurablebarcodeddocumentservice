<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 05.08.08
	Poprzednia zmiana: 05.08.08
C--%>
<!--N help-settings.jsp N-->

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="UstawieniaPomocy"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/help-settings.action'"/>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="FolderPomocyOgolnej"/>:
			</td>
			<td>
				<span class="bold"><ww:property value="generalHelpDir"/></span>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="FolderPomocyDedykowanej"/>:
			</td>
			<td>
				<span class="bold"><ww:property value="specificHelpDir"/></span>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="FolderNarzedzi"/>:
			</td>
			<td>
				<span class="bold"><ww:property value="toolsDir"/></span>
			</td>
		</tr>
	</table>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NowyPlikWfolderze"/>
				<ww:select id="folderType" name="'folderType'" list="folderTypes" cssClass="'sel'"/>
				<span class="star">*</span>
			</td>
			<td>
				<ww:file name="'file'" cssClass="'txt'" size="50" id="file"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="OpisPliku"/>:<span class="star">*</span>
			</td>
			<td>
				<ww:textfield name="'description'" value="description" cssClass="'txt'" size="50" id="description"/>
			</td>
		</tr>
	</table>
	
	<ds:event name="'doAddFile'" value="getText('DodajPlik')" onclick="'if (!validateAddForm()) return false'"/>
</form>
	
<form action="<ww:url value="'/admin/help-settings.action'"/>" method="post" id="formEdit">
	<input type="hidden" name="doDelete" id="doDelete"/>
	<ww:hidden name="'actionType'" id="actionType"/>
	<ww:hidden name="'actionFilename'" id="actionFilename"/>
	
	<ww:if test="!helpFiles.isEmpty()">
		<table class="tableMargin">
			<tr>
				<th width="20%">
					<ds:lang text="Plik"/>
				</th>
				<th width="40%">
					<ds:lang text="Opis"/>
				</th>
				<th width="20%">
					<ds:lang text="Typ"/>
				</th>
				<th width="10%"></th>
				<th width="10%"></th>
			</tr>	
			<ww:iterator value="helpFiles">
				<tr>
					<td>
						<ww:property value="filename"/>
					</td>
					<td>
						<ww:property value="description"/>
					</td>
					<td>
						<ww:property value="typeDescription"/>
					</td>
					<td>
						<a href="javascript:void(deleteFile(<ww:property value='type'/>, '<ww:property value='filename'/>'))">
							<ds:lang text="usun"/></a>
					</td>
					<td>
						<a href="javascript:void(changeDesc(<ww:property value='type'/>, '<ww:property value='filename'/>', '<ww:property value='description'/>'))">
							Zmie� opis</a>
					</td>
				</tr>	
			</ww:iterator>
		</table>
	</ww:if>
	
	<div id="divChangeDesc" style="display: none">
		<ds:lang text="Opis"/>:
		<ww:textfield name="'newDesc'" value="newDesc" cssClass="'txt'" size="50" id="newDesc" />
		<ds:event name="'doChangeDesc'" value="getText('ZmienOpis')"/>
	</div>
</form>

<%--
<h1><ds:lang text="UstawieniaPomocy"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/help-settings.action'"/>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	
	<table>
		<tr>
			<td><ds:lang text="FolderPomocyOgolnej"/>:</td>
			<td><b><ww:property value="generalHelpDir"/></b></td>
			<%--<td><ww:textfield name="'generalHelpDir'" value="generalHelpDir" cssClass="'txt'" size="50" id="generalHelpDir" /></td>-->
		</tr>
		<tr>
			<td><ds:lang text="FolderPomocyDedykowanej"/>:</td>
			<td><b><ww:property value="specificHelpDir"/></b></td>
			<%--<td><ww:textfield name="'specificHelpDir'" value="specificHelpDir" cssClass="'txt'" size="50" id="specificHelpDir" /></td>-->
		</tr>
		<tr>
			<td><ds:lang text="FolderNarzedzi"/>:</td>
			<td><b><ww:property value="toolsDir"/></b></td>
			<%--<td><ww:textfield name="'toolsDir'" value="toolsDir" cssClass="'txt'" size="50" id="toolsDir" /></td>-->
		</tr>
	</table>
	<%--<input type="submit" name="doUpdate" value="Zapisz" class="btn" onclick="document.getElementById('doUpdate').value='true';if (!validate()) return false;"/>-->
	
	<table>
		<tr>
			<td><ds:lang text="NowyPlikWfolderze"/>
				<ww:select id="folderType" name="'folderType'" list="folderTypes" cssClass="'sel'" />
				<span class="star">*</span>
			</td>
			<td><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></td>
		</tr>
		<tr>
			<td><ds:lang text="OpisPliku"/>:<span class="star">*</span></td>
			<td>
				<ww:textfield name="'description'" value="description" cssClass="'txt'" size="50" id="description" />
			</td>
		</tr>
	</table>

	<ds:event name="'doAddFile'" value="getText('DodajPlik')" onclick="'if (!validateAddForm()) return false'"/>
</form>

<form action="<ww:url value="'/admin/help-settings.action'"/>" method="post" id="formEdit">
	<input type="hidden" name="doDelete" id="doDelete"/>
	<ww:hidden name="'actionType'" id="actionType"/>
	<ww:hidden name="'actionFilename'" id="actionFilename"/>
	
	<ww:if test="!helpFiles.isEmpty()">
		<table>
			<tr>
				<th width="20%"><ds:lang text="Plik"/></th>
				<th width="40%"><ds:lang text="Opis"/></th>
				<th width="20%"><ds:lang text="Typ"/></th>
				<th width="10%"></th>
				<th width="10%"></th>
			</tr>	
			<ww:iterator value="helpFiles">
				<tr>
					<td><ww:property value="filename"/></td>
					<td><ww:property value="description"/></td>
					<td><ww:property value="typeDescription"/></td>
					<td><a href="javascript:void(deleteFile(<ww:property value='type'/>, '<ww:property value='filename'/>'))"><ds:lang text="usun"/></a></td>
					<td><a href="javascript:void(changeDesc(<ww:property value='type'/>, '<ww:property value='filename'/>', '<ww:property value='description'/>'))">Zmie� opis</a></td>
				</tr>	
			</ww:iterator>
		</table>
	</ww:if>
	
	<div id="divChangeDesc" style="display: none">
		<table>
			<tr>
				<td>
					<ds:lang text="Opis"/>:
				</td>
				<td>
					<ww:textfield name="'newDesc'" value="newDesc" cssClass="'txt'" size="50" id="newDesc" />
				</td>
				<td>
					<ds:event name="'doChangeDesc'" value="getText('ZmienOpis')"/>
				</td>
			</tr>
		</table>		
	</div>
</form>
--%>

<script type="text/javascript">
	function validateAddForm()
	{
		if (emptyField(document.getElementById('file')))
		{   
			alert('<ds:lang text="NiePodanoPliku"/>');
			return false;
		}   
		if (emptyField(document.getElementById('description')))
		{   
			alert('<ds:lang text="NiePodanoOpisu"/>');
			return false;
		}  
		return true;
	}
	
	function deleteFile(type, filename)
	{
		if (confirm('<ds:lang text="CzyNaPewnoUsunacWybranyPlik"/>?'))
		{
			//document.getElementById('doDelete').disabled = false;
			document.getElementById('doDelete').value = 'true';
			document.getElementById('actionType').value = type;
			document.getElementById('actionFilename').value = filename;
			document.getElementById('formEdit').submit();
		}
	}
	
	function changeDesc(type, filename, olddesc)
	{
		document.getElementById('actionType').value = type;
		document.getElementById('actionFilename').value = filename;
		document.getElementById('newDesc').value = olddesc;
		document.getElementById('divChangeDesc').style.display = 'block';
	}
</script>

<!--N koniec help-settings.jsp N-->