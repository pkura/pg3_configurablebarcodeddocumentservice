<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.08.08
	Poprzednia zmiana: 29.08.08
C--%>
<!--N emergency.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.settings.EmergencyAction"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="StanAwaryjny"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:messages/>
<edm-html:errors/>

<form id="form" action="<ww:url value="'/admin/emergency.action'"></ww:url>" method="post" >
	<ds:lang text="WybierzUzytkownikowKtorzyBedaMieliDostepDoSystemu"/>
	
	<table class="tableMargin">
		<tr>
			<td class="alignTop">
				<ww:select name="'availableUsers'" multiple="true" size="20" cssClass="'multi_sel combox-chosen'"
					cssStyle="'width: 300px;'" list="usersAll" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " onclick="moveOptions(this.form.availableUsers, this.form.usernames);" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "	onclick="moveOptions(this.form.usernames, this.form.availableUsers);" class="btn"/>
			</td>
			<td class="alignTop">
				<ww:select name="'usernames'" id="usernames" multiple="true"
					size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="usersSelected"/>
			</td>
		</tr>
	</table>
	
	<input type="submit" class="btn saveBtn" name="doUpdate" value="<ds:lang text="Zapisz"/>"
		onclick="selectAllOptions(document.getElementById('usernames'));"/>
		
	<ww:if test="emergency">
		<br/>
		<ds:submit-event name="'doOffEmergency'" value="getText('WylaczStanAwaryjny')"/>
	</ww:if>		
</form>

<%--
<edm-html:messages />
<edm-html:errors />

<form id="form" action="<ww:url value="'/admin/emergency.action'"></ww:url>" method="post" >

<h1><ds:lang text="StanAwaryjny"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />

<ds:lang text="WybierzUzytkownikowKtorzyBedaMieliDostepDoSystemu"/>

<table>
		<tr>
            <td valign="top">
                <ww:select name="'availableUsers'" multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                    list="usersAll" />
            </td>
            <td valign="middle">
                <input type="button" value=" &gt;&gt; "
                    onclick="moveOptions(this.form.availableUsers, this.form.usernames);"
                    class="btn"/>
                <br/>
                <input type="button" value=" &lt;&lt; "
                    onclick="moveOptions(this.form.usernames, this.form.availableUsers);"
                    class="btn"/>
                <br/>
            </td>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td rowspan="2">
                            <ww:select name="'usernames'"
                                id="usernames"
                                multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                                list="usersSelected"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
</table>
<table width="100">
    <tr>
    	<td><input type="submit" class="btn" name="doUpdate" value="<ds:lang text="Zapisz"/>"
        	onclick="selectAllOptions(document.getElementById('usernames'));"/></td>
    	<ww:if test="emergency">
    		<td><ds:submit-event name="'doOffEmergency'" value="getText('WylaczStanAwaryjny')"/></td>
    	</ww:if>    	
    </tr>
</table>        
--%>

<!--N koniec emergency.jsp N-->