<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 30.07.08
	Poprzednia zmiana: 30.07.08
C--%>
<!--N address.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneAdresowe"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>


<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/edit-floors-address.action'"/>?tabId=<ww:property value="tabId" />" enctype="multipart/form-data" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<ww:hidden name="'address.id'" />

<fieldset class="container">
	<p>
		<label for="floor.name"><ds:lang text="floorName" /></label>
		<ww:textfield id="floor.name" name="'floor.name'" maxlength="100" cssClass="'txt dontFixWidth'"/>
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="floor.description"><ds:lang text="floorDescription" /></label>
		<ww:textfield id="floor.description" name="'floor.description'" maxlength="255" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="file"><ds:lang text="floorImage" /></label>
		<ww:file name="'file'" cssClass="'txt'" size="50" id="file" />
	</p>
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Dodaj nowe pi�tro'" name="'addFloor'" cssClass="'btn'"/>
</div>
<script type="text/javascript">bindLabelDecoration();</script>
<div class="clearBoth" style="height: 30px"></div>

<ww:if test="address.id != null && address.id > 0">

<%-- PI�TRA --%>
<table class="mediumTable search bottomSpacer widePadding" cellspacing="0">
<caption><ds:lang text="PietraWLokalizacji" /></caption>
<thead>
	<tr>
		<th class="empty">&nbsp;</th>
		<th>
			<ds:lang text="floorName" />
		</th>
		<th class="empty">
			<ds:lang text="floorDescription" />
		</th>
		<th class="empty">
			<ds:lang text="floorImage" />
		</th>
	</tr>
</thead>
<tbody>
	<ww:iterator value="address.floors">
	<tr>
		<td>
			<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
		</td>
		<td>
			<ww:property value="name" />
		</td>
		<td>
			<ww:property value="description" />
		</td>
		<td>
			<a href="<ww:url value="'/admin/image-viewer.action'" />?imgId=<ww:property value="image.id" />" target="_blank">
				<ww:property value="image.name" />
			</a>
		</td>
	</tr>
	</ww:iterator>
</tbody>
</table>
	<ds:submit-event value="'Usu� zaznaczone'" name="'doDeleteFloors'" cssClass="'btn'"/>
</ww:if>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>