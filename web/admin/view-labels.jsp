<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 02.09.08
	Poprzednia zmiana: 02.09.08
C--%>
<!--N view-labels.jsp N-->

<%@ page import="pl.compan.docusafe.web.archive.settings.EmergencyAction"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Etykiety"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<p>
	<ds:xmlLink path="Labels"/>
</p>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
		<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
		<ds:xmlLink path="Labels"/>
	</div>
</ds:available>

<edm-html:messages/>
<edm-html:errors/>

<form id="form" action="<ww:url value="'/admin/view-labels.action'"></ww:url>" method="post" >
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

	<table class="tableMargin">
		<tr>
			<th></th>
			<th>
				<ds:lang text="NazwaEtykiety"/>
			</th>
			<th>
				<ds:lang text="OpisEtykiety"/>
			</th>
		</tr>
		<ww:iterator value="availableLabels">
			<tr>
				<td>
					<ww:checkbox name="'labelsIds'" fieldValue="id" id="check"/>
				</td>
				<td>
					<a href="<ww:url value="'/admin/new-label.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="name"/></a>
				</td>
				<td>
					<ww:property value="description"/>
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<ds:submit-event value="getText('Usun')" name="'doDelete'"></ds:submit-event>
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<edm-html:messages />
<edm-html:errors />

<h1><ds:lang text="Etykiety"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="Labels"/>
</p>

<form id="form" action="<ww:url value="'/admin/view-labels.action'"></ww:url>" method="post" >

<table>
	<tr>
		<td></td>
		<td><B><ds:lang text="NazwaEtykiety"/></B></td>
		<td><B><ds:lang text="OpisEtykiety"/></B></td>
	</tr>
	<ww:iterator value="availableLabels">
		<tr>
			<td><ww:checkbox name="'labelsIds'" fieldValue="id" id="check"/></td>
			<td><a href="<ww:url value="'/admin/new-label.action'"><ww:param name="'id'" value="id"/></ww:url>"><ww:property value="name"/></a></td>
			<td><ww:property value="description"/></td>
		</tr>
	
	</ww:iterator>
	<tr>
		<td><ds:submit-event value="getText('Usun')" name="'doDelete'"></ds:submit-event> </td>
		<td></td>
		<td></td>
	</tr>

</table>

</form>
--%>
<!--N view-labels.jsp N-->