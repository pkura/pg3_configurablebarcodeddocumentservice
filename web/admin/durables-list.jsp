<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="SrodkiTrwale"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- WYSZUKIWARKA -->
<form id="SearchDurablesForm" name="SearchDurablesForm" action="<ww:url value="'/admin/durables-list.action'"/>" method="post"
	enctype="application/x-www-form-urlencoded">

	<ww:hidden name="'offset'" id="pageNumber" />

<fieldset class="container">
	<p class="simpleSearch">
		<label for="durable.inventoryNumber"><ds:lang text="NumerInwentaryzacyjny"/></label>
		<ww:textfield id="durable.inventoryNumber" name="'durable.inventoryNumber'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>

	<p class="simpleSearch">
			<label for="maxResults"><ds:lang text="LiczbaRezultatowNaStronie" /></label>
			<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="durable.serialNumber"><ds:lang text="NumerSeryjny"/></label>
		<ww:textfield id="durable.serialNumber" name="'durable.serialNumber'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="durable.location"><ds:lang text="LokalizacjaSrodkaTrwalego"/></label>
		<ww:textfield id="durable.location" name="'durable.location'" maxlength="255" size="15" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Szukaj �rodk�w trwa�ych'" name="'goSearch'" cssClass="'btn btnBanner'" cssStyle="'*width: 180px;'"/>
	<a id="searchSwitcher" href="#" extendedSearch="false" >Wyszukiwanie zaawansowane</a>
	<a id="clearFields" href="#" >Wyczy��</a>
</div>

<script type="text/javascript">
bindLabelDecoration();
initExtendedSearch();
</script>

<div class="clearBoth" style="height: 30px"></div>

</form>
<!-- END / WYSZUKIWARKA -->


<!-- TABELA Z DANYMI -->
<form action="<ww:url value="'/admin/durables-list.action'"/>" method="post" enctype="application/x-www-form-urlencoded">
<table id="mainTable" class="search" cellspacing="0">
<thead>
	<tr>
		<th class="empty">
			<ds:lang text="Usun"/>
		</th>
		<th class="empty">
			<ds:lang text="NumerInwentaryzacyjny"/>
		</th>
		<th class="empty">
			<ds:lang text="NumerSeryjny"/>
		</th>
		<th class="empty">
			<ds:lang text="OpisSrodkaTrwalego"/>
		</th>
		<th class="empty">
			<ds:lang text="LokalizacjaSrodkaTrwalego"/>
		</th>
		<th>
			<ds:lang text="Edytuj"/>	
		</th>
	</tr>
</thead>
<tbody>
<ww:iterator value="durables">
<tr>
	<td>
		<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
	</td>
	<td>
		<ww:property value="inventoryNumber"/>
	</td>
	<td>
		<ww:property value="serialNumber"/>
	</td>
	<td>
		<ww:property value="info"/>
	</td>
	<td>
		<ww:property value="locations.get(location)"/>
	</td>
	<td>
		<a href="<ww:url value="'/admin/update-durable.action'"/>?durable.id=<ww:property value="id"/>">
			<ds:lang text="Edytuj"/>
		</a>
	</td>
</tr>
</ww:iterator>
<tr>
	<td>&nbsp;</td>
</tr>
<tr>
	<td>
		<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane �rodki trwa�e?'" name="'doDelete'" cssClass="'btn'"/>
	</td>
	<td>
		<input type="button" value="Dodaj nowy �rodek trwa�y" class="btn" 
			onclick="document.location.href='<ww:url value="'/admin/update-durable.action'" />'" />
	</td>	
</tr>
</tbody>
</table>
<!-- END / TABELA Z DANYMI -->

<!-- PAGER -->
<table class="table100p">
	<tr>
		<td align="center">
			<ww:set name="pager" scope="request" value="pager"/>
				<jsp:include page="/pager-links-include.jsp"/>
				<script type="text/javascript">
					$j('.pager a').click(function() {
						var page = $j(this).attr('href');
						if (page == '#')
							return false;
						$j('#pageNumber').attr('value', page);
						document.forms.SearchDurablesForm.submit();
						return false;
					});
				</script>
		</td>
	</tr>
</table>
</form>