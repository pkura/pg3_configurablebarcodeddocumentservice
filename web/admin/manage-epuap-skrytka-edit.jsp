<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>
<ww:if test="skrytka.id == null">
	<ds:lang text="SkrytkiEpuapNowa"/>
</ww:if>
<ww:else>
	<ds:lang text="SkrytkiEpuapEdycja"/>
</ww:else>
</h1>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:form action="/admin/manage-epuap-skrytka-edit" method="'post'" enctype="'multipart/form-data'">
<ww:hidden name="'skrytka.id'" value="skrytka.id"/>
<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="identyfikatorPodmiotu"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield id="skrytka.identyfikatorPodmiotu" name="'skrytka.identyfikatorPodmiotu'"
								value="skrytka.identyfikatorPodmiotu" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="adresSkrytki"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield id="skrytka.adresSkrytki" name="'skrytka.adresSkrytki'" value="skrytka.adresSkrytki"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="nazwaSkrytki"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield id="skrytka.nazwaSkrytki" name="'skrytka.nazwaSkrytki'" value="skrytka.nazwaSkrytki"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Certyfikat"/>
				<ww:if test="newSkrytka == true">
					<span class="star">*</span>
				</ww:if>:
			</td>
			<td>
				<ww:file name="'certificate'" id="certificate" cssClass="'txt dontFixWidth'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="CertyfikatHaslo"/>
					<ww:if test="newSkrytka == true">
					<span class="star">*</span>
					</ww:if>:
			</td>
			<td>
				<ww:password id="certificatePassword" name="'certificatePassword'" value="certificatePassword" cssClass="'txt dontFixWidth'"/>
			</td>
		</tr>
		<ww:hidden id="certificateHidden" name="'certificateHidden'" value="certificateHidden"></ww:hidden>
		<ww:hidden id="certificatePasswordHidden" name="'certificatePasswordHidden'" value="certificatePasswordHidden"></ww:hidden>
		<%-- <tr>
			<td>
				<ds:lang text="Aktywna"/>:
			</td>
			<td>
				<ww:checkbox id="skrytka.available" name="'skrytka.available'" fieldValue="true" value="skrytka.available" />
			</td>
		</tr> --%>
		<tr>
			<td>
				<ds:lang text="Wysy�aj powiadomienia"/>:
			</td>
			<td>
				<ww:checkbox id="skrytka.notification" name="'skrytka.notification'" fieldValue="true" value="skrytka.notification" />
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Email"/>
				<span class="star">*</span>:
			</td>
			<td>
				<ww:textfield name="'skrytka.email'" id="skrytka.email" cssClass="'txt dontFixWidth'"/>
			</td>
		</tr>
		<ds:available test="epuap.multi.param1.enable">
			<tr>
				<td>
					<%= Docusafe.getAdditionProperty("epuap.multi.param1") %>
					<span class="star">*</span>:
				</td>
				<td>
					<ds:available test="epuap.multi.param1.nfos">
						<ww:select name="'skrytka.param1'" list="nfosSystem" 
                               id="skrytka.param1"
                               listKey="id.toString()" 
                               headerKey="''" 
                               headerValue="'----'"  
                               listValue="title" 
                               cssClass="'sel'" value="skrytka.param1"/>
                   </ds:available>                  
				</td>
			</tr>
		</ds:available>
		
		<ds:available test="epuap.multi.param2.enable">
			<tr>
				<td>
					<%= Docusafe.getAdditionProperty("epuap.multi.param2") %>
					<span class="star">*</span>:
				</td>
				<td>
						<ww:textfield id="skrytka.param2" name="'skrytka.param2'" value="skrytka.param2"/>
				</td>
			</tr>
		</ds:available>
</table>

<ww:if test="newSkrytka == true">
	<ds:submit-event value="'Zapisz'" name="'doCreate'" cssClass="'btn btnBanner'"/>
</ww:if>
<ww:else>
	<ds:submit-event value="'Zapisz'" name="'doEdit'" cssClass="'btn btnBanner'"/>
</ww:else>


</ww:form>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
