<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
    <ds:xmlLink path="NewBarcodePrinter"/>
    <br/>
</ds:available>
<ds:available test="layout2">
    <h1>&nbsp;</h1>
    <div id="middleMenuContainer">
        <img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
        <ds:xmlLink path="NewBarcodePrinter"/>
    </div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/barcode-printers-list.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ds:available test="layout2">
        <div id="middleContainer"> <!-- BIG TABLE start -->
    </ds:available>
        <table class="tableMargin">
        <tr>
            <th>
                <ds:lang text="DefinedPrinters" />:
            </th>
            <td>
                <ww:select name="'selectedPrinter'" list="printers" listKey="userPrinterName" listValue="userPrinterName" headerKey="''"
                           headerValue="getText('select.wybierz')" onchange="'reloadPage()'" value="selectedPrinter" cssClass="'sel'"/>
            </td>
        </tr>
        <tr>
            <td> &nbsp </td>
        </tr>
        <ww:if test="selectedPrinter != null">
                <tr>
                    <td>
                        <ds:lang text="UserPrinterName"/>:
                        <span class="star">*</span>
                    </td>
                    <td>
                        <ww:textfield name="'userPrinterName'" size="35" maxlength="35" cssClass="'txt'" value="printer.userPrinterName"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="NazwaDrukarki"/>:
                    </td>
                    <td>
                        <ww:select name="'printerName'" id="printerName" list="availablePrinters" listKey="key"
                                   listValue="value" cssClass="'sel'" value="printer.printerName"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="DaneBarkodu"/>:
                    </td>
                    <td>
                        <ww:textfield name="'barcodeValue'" size="20" maxlength="20" cssClass="'txt'" value="printer.barcodeValue"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="UzywajDrukarkiKodowKreskowych"/>:
                    </td>
                    <td>
                        <ww:checkbox name="'usePrinter'" fieldValue="true" value="printer.usePrinter"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="JezykProgramowaniaDrukarki"/>:
                    </td>
                    <td>

                        <ww:select name="'printerLanguage'" id="printerLanguage" list="availableLanguages" listKey="key"
                                   listValue="value" cssClass="'sel'" value="printer.language"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="PierwszyWierszNaglowka"/>:
                    </td>
                    <td>
                        <ww:textfield name="'firstLine'" size="35" maxlength="35" cssClass="'txt'" value="printer.firstLine"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="DrugiWierszNaglowka"/>:
                    </td>
                    <td>
                        <ww:textfield name="'secondLine'" size="35" maxlength="35" cssClass="'txt'" value="printer.secondLine"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <ds:lang text="RodzajKodu"/>:
                    </td>
                    <td>

                        <ww:select name="'barcodeType'" id="barcodeType" list="availableBarcodes" listKey="key" headerKey="''" headerValue="getText('select.wybierz')"
                                   listValue="value" cssClass="'sel'" value="printer.barcodeType"/>
                    </td>
                </tr>
        </ww:if>

    </table>
        <ww:if test="selectedPrinter != null">
            <ds:submit-event value="getText('Test')" name="'doTest'" cssClass="'btn'"/>
            <ds:submit-event value="getText('Usun')" name="'doDelete'" cssClass="'btn'"/>
            <ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn saveBtn'"/>
        </ww:if>
    <script type="text/javascript">
        function reloadPage()
        {
            document.forms[0].submit();
        }
    </script>
</form>