<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N dispatch.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DispatchDivision"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p>
	<ds:lang text="WybierzDzialWysylek"/>
</p>

<form action="<ww:url value="'/admin/dispatch.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:hidden name="'divisionGuid'"/>
	<ww:property value="treeHtml" escape="false"/>
	
	<ds:submit-event value="getText('WybierzBiezaceStanowisko')" name="'doPick'" disabled="!canPick"/>
</form>

<ww:if test="dispatchDivisionPrettyPath != null">
	<p>
		<ds:lang text="AktualnieDzialemWysylekJest"/>
		<ww:property value="dispatchDivisionPrettyPath"/>
	</p>
</ww:if>
<ww:else>
	<p>
		<ds:lang text="AktualnieNieMaDzialuWysylek"/>
	</p>
</ww:else>
<!--N koniec dispatch.jsp N-->