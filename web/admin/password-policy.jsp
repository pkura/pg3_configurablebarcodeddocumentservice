<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N password-policy.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Hasla"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/password-policy.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="MinimalnaDlugoscHasla"/>:
			</td>
			<td>
			</td>
			<td>
			</td>
			<td>
				<ww:textfield name="'minPasswordLength'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="HasloWygasa"/>:
			</td>
			<td>
				<ww:checkbox name="'passwordExpires'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="poIluDniach"/>:
			</td>
			<td>
				<ww:textfield name="'daysToExpire'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WymaganeCyfry"/>:
			</td>
			<td>
				<ww:checkbox name="'mustHaveDigits'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="ile"/>:
			</td>
			<td>
				<ww:textfield name="'howManyDigits'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WymaganeDuzeLitery"/>:
			</td>
			<td>
				<ww:checkbox name="'mustHaveLetters'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="ile"/>:
			</td>
			<td>
				<ww:textfield name="'howManyLetters'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WymaganeMaleLitery"/>:
			</td>
			<td>
				<ww:checkbox name="'mustHaveSmallLetters'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="ile"/>:
			</td>
			<td>
				<ww:textfield name="'howManySmallLetters'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WymaganeZnakiSpecjalne"/>:
			</td>
			<td>
				<ww:checkbox name="'mustHaveSpecials'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="ile"/>:
			</td>
			<td>
				<ww:textfield name="'howManySpecials'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="BlokowanieHaselPoNieudanychProbach"/>:
			</td>
			<td>
				<ww:checkbox name="'lockingPasswords'" fieldValue="true"/>
			</td>
			<td class="alignRight">
				<ds:lang text="poIlu"/>:
			</td>
			<td>
				<ww:textfield name="'howManyIncorrectLogins'" size="2" maxlength="3" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="PoprosOZmianeHaslaPrzyPierwszymLogowaniu"/>:
			</td>
			<td>
				<ww:checkbox name="'changePasswordAtFirstLogon'" fieldValue="true"/>
			</td>		
		</tr>
		
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>
	<br/><br/>
	

</form>

<form action="<ww:url value="'/admin/password-policy.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="DataWygasniecia"/>:
			</td>
			<td colspan="3" class="alignRight">
				<ww:textfield name="'expireDate'"  size="10" maxlength="10" cssClass="'txt'" id="expireDate"/>
				<img src="<ww:url value="'/calendar096/img.gif'"/>"
					id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''"/>
			</td>
		</tr>
	</table>
	</br>
	<ds:submit-event value="getText('WyczyscHistorieHasel')" name="'doClearHistory'" cssClass="'btn'"/>
	<ds:submit-event value="getText('WygasWszystkieHasla')" name="'doSetExpireDate'" cssClass="'btn'"/>
</form>
<%--
<h1><ds:lang text="Hasla"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/admin/password-policy.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">

<table>
    <tr>
        <td><ds:lang text="MinimalnaDlugoscHasla"/>:</td>
        <td><ww:textfield name="'minPasswordLength'" size="2" maxlength="3" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td><ds:lang text="HasloWygasa"/>:</td>
        <td><ww:checkbox name="'passwordExpires'" fieldValue="true"  />
            <ds:lang text="poIluDniach"/>:
            <ww:textfield name="'daysToExpire'" size="2" maxlength="3" cssClass="'txt'" />
        </td>
    </tr>
    <tr>
        <td><ds:lang text="WymaganeCyfry"/>:</td>
        <td><ww:checkbox name="'mustHaveDigits'" fieldValue="true"/>
            <ds:lang text="ile"/>: <ww:textfield name="'howManyDigits'" size="2" maxlength="3" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="WymaganeDuzeLitery"/>:</td>
        <td><ww:checkbox name="'mustHaveLetters'" fieldValue="true"/>
            <ds:lang text="ile"/>: <ww:textfield name="'howManyLetters'" size="2" maxlength="3" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="WymaganeMaleLitery"/>:</td>
        <td><ww:checkbox name="'mustHaveSmallLetters'" fieldValue="true"/>
            <ds:lang text="ile"/>: <ww:textfield name="'howManySmallLetters'" size="2" maxlength="3" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="WymaganeZnakiSpecjalne"/>:</td>
        <td><ww:checkbox name="'mustHaveSpecials'" fieldValue="true"/>
            <ds:lang text="ile"/>: <ww:textfield name="'howManySpecials'" size="2" maxlength="3" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="BlokowanieHaselPoNieudanychProbach"/>:</td>
        <td><ww:checkbox name="'lockingPasswords'" fieldValue="true"/>
            <ds:lang text="poIlu"/>: <ww:textfield name="'howManyIncorrectLogins'" size="2" maxlength="3" cssClass="'txt'"/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>
        </td>
    </tr>
    <tr>
    <td colspan="2">
    	    <br/>
            <ds:submit-event value="getText('WyczyscHistorieHasel')" name="'doClearHistory'" cssClass="'btn'"/><br/><br/>
        </td>
    </tr>
    <tr>
    <td colspan="2">
            <ds:submit-event value="getText('WygasWszystkieHasla')" name="'doSetExpireDate'" cssClass="'btn'"/>
        </td>
    </tr>
	<tr>
    	    <td><br/><ds:lang text="DataWygasniecia"/>: </td>
    		<td><ww:textfield name="'expireDate'"  size="10" maxlength="10" cssClass="'txt'" id="expireDate"/>
        			<img src="<ww:url value="'/calendar096/img.gif'"/>"
            		id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
            		title="Date selector" onmouseover="this.style.background='red';"
            		onmouseout="this.style.background=''"/>
	    </td>
    </tr>
</table>
</form>
--%>

<script type="text/javascript">
	Calendar.setup({
	    inputField     :    "expireDate",     // id of the input field
	    ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    button         :    "documentDateTrigger",  // trigger for the calendar (button ID)
	    align          :    "Tl",           // alignment (defaults to "Bl")
	    singleClick    :    true
	});
</script>

<!--N koniec password-policy.jsp N-->