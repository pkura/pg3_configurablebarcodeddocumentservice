<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="RaportAbsencji"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>



<form action="<ww:url value="'/absences/absences-report.action'"/>" method="post">
<ww:hidden name="'maxNumber'" id="maxNumber"/>
<ww:hidden name="'fileId'" id="fileId"/>
<table>
<thead>
<tr>
	<th>
		<ds:lang text="Numerz"/>
	</th>
	<th>
		<ds:lang text="Ilosc"/>
	</th>
	<th>
		<ds:lang text="Data"/>
	</th>
	<th>
		<ds:lang text="Plik"/>
	</th>	
</tr>
</thead>
<tbody>
<ww:iterator value="zestawieniaBeans">
<tr>
	<td>
		<ww:property value="number" />
	</td>
	<td>
		<ww:property value="count" />
	</td>
	<td>
		<ww:property value="date" />
	</td>
	<td>
		<a href="<ww:url value="'/absences/absences-report.action'"/>?fileId=<ww:property value="number"/>&doFile=true"><ds:lang text="Pobierz"/></a>
	</td>	
</tr>
</ww:iterator>
<tr>
	<td colspan="2"><ds:lang text="absencjeBezZestawienia"/></td>
	<td><ww:property value="notInCount" /></td>
	<td><ds:submit-event value="'Generuj'" name="'doGenerate'" cssClass="'btn'"/></td>
</tr>
</tbody>
</table>

</form>
