<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N doctypes.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Typy dokument�w</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/doctypes.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<ww:if test="!doctypes.empty">
    <table>
    <tr>
        <th></th>
        <th>Nazwa</th>
        <th>Aktywny</th>
    </tr>
    <ww:iterator value="doctypes">
        <tr>
            <td><ww:checkbox name="'deleteIds'" fieldValue="id" value="false" disabled="inUse" /></td>
<%--            <td><input type="checkbox" name="doctypeIds" value="<ww:property value="id"/>" <ww:if test="inUse">disabled="true"</ww:if> /></td>--%>
            <td><a href="<ww:url value="'/admin/edit-doctype.action?id='+id"/>"><ww:property value="name"/></a></td>
            <td><ww:checkbox name="'enableDoctypeIds'" fieldValue="id" value="enabled" disabled="!inUse" /></td>
        </tr>
    </ww:iterator>
    <tr>
        <td></td>
        <td>
            <ds:submit-event value="'Usu�'" name="'doDelete'" cssClass="'btn'" confirm="'Na pewno usun��?'" />
            <ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'"/>
        </td>
    </tr>
    </table>
</ww:if>

<h4>Nowy typ</h4>

<table>
<tr>
    <td>Nazwa:</td>
    <td><ww:textfield name="'name'" size="30" maxlength="100" cssClass="'txt'"/></td>
    <td><ds:submit-event value="'Nowy typ'" name="'doCreate'" cssClass="'btn'"/></td>
</tr>
</table>

</form>

