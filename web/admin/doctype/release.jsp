<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N release.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<h1>Typ dokumentu</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/admin/edit-doctype.action'"/>" method="post" onsubmit="if (!validateInputs()) return false; generateInputs(); disableFormSubmits(this);">
<ww:hidden name="'id'"/>

<p>Chcesz usun�� typ dokumentu "<ww:property value="name"/>".
<ww:if test="documentCount > 0">
    Korzysta z niego <ww:property value="documentCount"/> dokument�w. Je�eli usuniesz
    typ, stracisz dodatkowe informacje zwi�zane z tym dokumentami; same dokumenty nie
    zostan� usuni�te.
</ww:if>
</p>

<ds:submit-event value="'Chc� kontynuowa� usuwanie'" name="'doRelease'" cssClass="'btn'" />
<input type="button" value="Rezygnuj�" class="btn"
    onclick="document.location.href='<ww:url value="'/admin/edit-doctype.action?id='+id"/>';"/>

</form>
 