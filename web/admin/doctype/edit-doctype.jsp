<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-doctype.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Typ dokumentu</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/admin/edit-doctype.action'"/>" method="post" enctype="multipart/form-data" onsubmit="/*if (!validateInputs()) return false;*/ disableFormSubmits(this);">
<ww:hidden name="'id'"/>

<p><ww:textfield name="'doctypeName'" size="30" maxlength="100" cssClass="'txt'" />
   <ds:submit-event value="'Zmie� nazw�'" name="'doSetName'" cssClass="'btn'" /></p>

<div id="fields">
    <ww:iterator value="fields" status="status">
        <input type="hidden" name="fieldOrder[<ww:property value="id"/>]" value="<ww:property value="#status.index"/>"/>
        <table style="border: 1px solid black; margin-top: 3px;">
        <tbody>
            <tr>
                <td colspan="2">
                    <input type="checkbox" name="deleteIds" value="<ww:property value="id"/>"/>
                </td>
            </tr>
            <tr>
                <td>Nazwa pola:</td>
                <td><input type="text" name="fieldNames[<ww:property value="id"/>]"
                    value="<ww:property value="name"/>" size="20" maxlength="50" class="txt">
                </td>
            </tr>
            <tr>
                <td>Rodzaj pola:</td>
                <td><select class="sel" name="fieldTypes[<ww:property value="id"/>]">
                    <option value="">-- wybierz --</option>
                    <option value="string" <ww:if test="type=='string'">selected="true"</ww:if> >napis</option>
                    <option value="date" <ww:if test="type=='date'">selected="true"</ww:if> >data</option>
                    <option value="bool" <ww:if test="type=='bool'">selected="true"</ww:if> >tak/nie</option>
                    <option value="enum" <ww:if test="type=='enum'">selected="true"</ww:if> >lista warto�ci</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>D�ugo��:</td>
                <td><input type="text" name="fieldLengths[<ww:property value="id"/>]"
                    value="<ww:property value="length"/>" size="3" maxlength="3" class="txt"></td>
            </tr>
            <tr>
                <td>Kod:</td>
                <td><input type="text" value="<ww:property value="cn"/>"
                    name="fieldCns[<ww:property value="id"/>]"
                    size="20" maxlength="20" class="txt"/>
                </td>
            </tr>
        </tbody>
        </table>
    </ww:iterator>

    <table style="border: 1px solid black; margin-top: 3px;">
    <tbody>
        <tr>
            <td colspan="2">
                Nowe pole
            </td>
        </tr>
        <tr>
            <td>Nazwa pola:</td>
            <td><ww:textfield name="'name'" size="20" maxlength="50" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td>Rodzaj pola:</td>
            <td><ww:select name="'type'" cssClass="'sel'"
                headerKey="''" headerValue="'-- wybierz --'"
                list="#{'string': 'napis', 'date': 'data', 'bool': 'tak/nie'}"/>
            </td>
        </tr>
        <tr>
            <td>D�ugo��:</td>
            <td><ww:textfield name="'length'" size="3" maxlength="3" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td>Kod:</td>
            <td><ww:textfield name="'cn'" size="20" maxlength="20" cssClass="'txt'"/></td>
        </tr>
        <tr>
            <td colspan="2"><ds:submit-event value="'Utw�rz'" name="'doCreate'" cssClass="'btn'"/></td>
        </tr>

    </tbody>
    </table>
</div>

<p></p>

Plik XML &nbsp&nbsp&nbsp <ww:file name="'file'" cssClass="'txt'" size="50" id="file" /><br/>

<p></p>

<input type="submit" name="doSaveXML" value="Zapisz plik XML" class="btn"/><br/>
<ds:submit-event value="'Zapisz'" name="'doUpdate'" cssClass="'btn'" />
<ds:submit-event value="'Zatwierd� list� p�l'" name="'doCommit'" cssClass="'btn'" disabled="inUse or fields.size == 0" />
<ds:submit-event value="'Anuluj zatwierdzenie'" name="'doPrepRelease'" cssClass="'btn'" />
<input type="button" value="Anuluj zmiany" onclick="document.location.href='<ww:url value="'/admin/doctypes.action'"/>';" class="btn"/>

</form>

<script>

function validateInputs()
{
    var form = document.getElementById('form');
    var fields = document.getElementById('fields');
    var cns = new Array(); // kody p�l

    var children = fields.getElementsByTagName('TABLE');
    for (var i=0; i < children.length; i++)
    {
        var table = children.item(i);
        if (table.disabled)
            continue;
        var trs = table.getElementsByTagName('TBODY').item(0).getElementsByTagName('TR');
        var fieldName = trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;
        var fieldType = trs.item(2).getElementsByTagName('TD').item(1).getElementsByTagName('SELECT').item(0).value;
        var fieldLength = trs.item(3).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;
        var fieldCn = trs.item(4).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;

        if (!isEmpty(fieldCn) && cns[fieldCn] != null)
        {
            alert('Nie mog� istnie� dwa pola o tym samym kodzie: '+fieldCn);
            return false;
        }
        else
        {
            cns[fieldCn] = true;
        }

        if (isEmpty(fieldName))
        {
            alert('Nie podano nazwy pola '+(i+1));
            return false;
        }

        if (isEmpty(fieldType))
        {
            alert('Nie wybrano typu pola '+(i+1));
            return false;
        }

        if (fieldType == 'string')
        {
            //alert('fieldName='+fieldName+', fieldLength='+fieldLength);
            if (!isEmpty(fieldName) &&
                (isEmpty(fieldLength) || parseInt(fieldLength)+'' != fieldLength))
            {
                alert('Nie podano d�ugo�ci pola '+(i+1)+' lub wpisano niepoprawn� liczb�');
                return false;
            }

            if (parseInt(fieldLength) > 100)
            {
                alert('Maksymalna d�ugo�� pola to 100 znak�w');
                return false;
            }

            if (parseInt(fieldLength) < 1)
            {
                alert('Minimalna d�ugo�� pola to 1 znak');
                return false;
            }
        }
    }

    return true;
}

<ww:if test="!fields.empty">var counter = <ww:property value="fields.size + 1"/>;</ww:if>
<ww:else>var counter = 1;</ww:else>

/**
    Tworzy elementy INPUT TYPE=HIDDEN dla ka�dego dodanego pola.
*/
function generateInputs()
{
    var form = document.getElementById('form');
    var fields = document.getElementById('fields');

    var addedFieldIds = new Array();

    var children = fields.getElementsByTagName('TABLE');

    for (var i=0; i < children.length; i++, counter++)
    {
        var table = children.item(i);
        if (table.disabled)
            continue;
        // tworzenie 'deletedField' p�niej, aby nie pokrywa�y si� z added fields
        var trs = table.getElementsByTagName('TBODY').item(0).getElementsByTagName('TR');
        var inputName = trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).name;
        var fieldName = trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;
        var fieldType = trs.item(2).getElementsByTagName('TD').item(1).getElementsByTagName('SELECT').item(0).value;
        var fieldLength = trs.item(3).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;
        var fieldCn = trs.item(4).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldNames['"+(counter+i)+"']");
        inp.setAttribute('value', fieldName);
        form.appendChild(inp);

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldTypes['"+(counter+i)+"']");
        inp.setAttribute('value', fieldType);
        form.appendChild(inp);

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldCns['"+(counter+i)+"']");
        inp.setAttribute('value', fieldCn);
        form.appendChild(inp);

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldLengths['"+(counter+i)+"']");
        inp.setAttribute('value', ''+fieldLength);
        form.appendChild(inp);

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldIds['"+(counter+i)+"']");
        inp.setAttribute('value', inputName);
        form.appendChild(inp);

        var inp = document.createElement('INPUT');
        inp.setAttribute('type', 'hidden');
        inp.setAttribute('name', "fieldOrder['"+(counter+i)+"']");
        inp.setAttribute('value', ''+i);
        form.appendChild(inp);

        addedFieldIds[inputName] = true;
    }

    // tworzenie element�w deletedFields
    for (var i=0; i < children.length; i++, counter++)
    {
        var table = children.item(i);
        if (table.disabled)
        {
            var trs = table.getElementsByTagName('TBODY').item(0).getElementsByTagName('TR');
            var inputName = trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).name;
            var fieldName = trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;
            var fieldType = trs.item(2).getElementsByTagName('TD').item(1).getElementsByTagName('SELECT').item(0).value;
            var fieldLength = trs.item(3).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value;

            if (!addedFieldIds[inputName])
            {
                var inp = document.createElement('INPUT');
                inp.setAttribute('type', 'hidden');
                inp.setAttribute('name', "deletedFieldIds['"+(counter+i)+"']");
                inp.setAttribute('value', inputName);
                form.appendChild(inp);
            }
        }
    }
}

function addField()
{
    var div = document.getElementById('fields');

    var tpl = document.getElementById('template').cloneNode(true);
    tpl.style.display = '';

    // firefox
    var trs = tpl.getElementsByTagName('TBODY').item(0).getElementsByTagName('TR');
    trs.item(1).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value = '';
    trs.item(2).getElementsByTagName('TD').item(1).getElementsByTagName('SELECT').item(0).value = '';
    trs.item(3).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value = '';
    trs.item(4).getElementsByTagName('TD').item(1).getElementsByTagName('INPUT').item(0).value = '';

    div.appendChild(tpl);
}

function removeField(tagA)
{
    var div = document.getElementById('fields');
    //var s = '';
    //for (var e in tagA) { s += ' ' + e; }; alert(e);
    var table = tagA.parentNode.parentNode.parentNode.parentNode;
    table.style.display = 'none';
    table.disabled = true;
}

function up(tagA)
{
    var table = tagA.parentNode.parentNode.parentNode.parentNode;
    if (!table.previousSibling)
        return false;
    table.swapNode(table.previousSibling);
    return true;
}

function down(tagA)
{
    var table = tagA.parentNode.parentNode.parentNode.parentNode;
    alert('table.next='+   table.nextSibling);
    if (!table.nextSibling)
        return false;
    table.nextSibling.swapNode(table);
    return true;
}

</script>
