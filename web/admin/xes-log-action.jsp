
<!--N xesLog.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://hoohoo.mine.nu/tabs" prefix="tabs" %>
<h1>XES</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />


<ds:ww-action-errors />
<ds:ww-action-messages />

<tabs:tabs var="tabs" selectedParameter="tabId" selectedDefaultId="xesloglist" selectedVar="selId" >
		<ds:available test="!layout2">
		<c:forEach items="${tabs}" var="tab" varStatus="status" >
			<c:choose>
				<c:when test="${tab.id == selId}">
					<a class="highlightedText" href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
				</c:when>
				<c:otherwise>
					<a href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
				</c:otherwise>
			</c:choose>
			<c:if test="${!status.last}">
				<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
			</c:if>
		</c:forEach>
		</ds:available>
		
		<ds:available test="layout2">
			<div id="middleMenuContainer">
				<img src="<c:out value='${pageContext.request.contextPath}'/>/img/tab2_left_corner.gif" class="tab2_img" />
				<c:forEach items="${tabs}" var="tab" varStatus="status" >
					<a href='<c:out value="${tab.link}"/>' 
					<c:choose>
						<c:when test="${tab.id == selId}">
						class="highlightedText"
						</c:when>
					</c:choose>
					><c:out value="${tab.title}"/>
					</a>
					
					<%--
					<c:choose>
						<c:when test="${tab.id == selId}">
							<a class="highlightedText" ><c:out value="${tab.title}"/></a>
						</c:when>
						<c:otherwise>
							<a href='<c:out value="${tab.link}"/>'><c:out value="${tab.title}"/></a>
						</c:otherwise>
					</c:choose>
					<c:if test="${!status.last}">
						<img src="<c:out value="${pageContext.request.contextPath}"/>/img/kropka.gif" width="6" height="10"/>
					</c:if>
					--%>
				</c:forEach>
			</div>
		</ds:available>
	<tabs:tab tabId="xesloglist" var="tab">
	   
	<form name="xesloglist"
		action="<ww:url value="'/admin/xes-log.action'"/>"
		method="post">
		<ds:available test="layout2">
				<div id="middleContainer"> <!-- BIG TABLE start -->
			</ds:available>
		<ww:hidden name="'xesLogId'" value="xesLogId" id="'xesLogId'"/>
		<ww:hidden name="'serialize'" value="serialize" id="'serialize'"/>	
			<table class="formTable" >
			
			<tr>
				<td>Wybierz rodzaj Logu:</td>
				
				<td><ww:select name="'kind'" id="kind" list="kinds" listKey="key" listValue="value" cssClass="'sel'"></ww:select></td>
			</tr>
			<tr id="tr_user">
				<td>Log u�ytkownik systemu:</td>
				<td>
				<ww:select name="'selectedUsers'" id="selectedUsers" cssClass="'sel combox-chosen'" list="users" listKey="name" 
				listValue="lastname+' '+firstname+'-'+externalName" multiple="true"
						size="4" /></td>
			</tr>
			
			<tr id="tr_createDates">
				<td>Data utworzenia od/do:</td>
				<td><ww:textfield name="'cdateFrom'" id="cdateFrom" size="10" maxlength="10" cssClass="'txt'" /> 
				<img src="<ww:url value="'/calendar096/img.gif'"/>"id="calendar_cdateFrom_trigger" style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" /> 
					<ww:textfield name="'cdateTo'" id="cdateTo" size="10" maxlength="10" cssClass="'txt'" />
					 <img src="<ww:url value="'/calendar096/img.gif'"/>" id="calendar_cdateTo_trigger" 
					 style="cursor: pointer; border: 1px solid red;"
					title="Date selector" onmouseover="this.style.background='red';"
					onmouseout="this.style.background=''" />
					<input type="submit" name="doGeneretefromToDateforUser" value="<ds:lang text="Pobierz Xes Log z zakresu"/>" class="btn" onclick="this.form.serialize.value = true"/>
					<input type="submit" name="doGeneretefromToDateforUser" value="<ds:lang text="Pobierz Xes Xml z zakresu"/>" class="btn" onclick="this.form.serialize.value = false"/>
				</td>
																										
				
				</tr>
				
			
							
			
			<tr class="formTableDisabled">
				<td colspan="2"><input type="submit" name="doSearch"class="btn" value="Szukaj" /> <input type="button"
					id="clearAllFields" name="clearAllFields" class="btn"
					value="<ds:lang text='WyczyscWszystkiePola'/>" onclick="clearFields()" /> <ww:if test="popup">
						<input type="button" class="cancel_btn" onclick="window.close();" value="Zamknij" />
					</ww:if></td>
			</tr>
		</table>
		 <table class="formTable" >
					
					<h3> Lista Wynik�w  </h3>
					<c:if test="${beans !=null && !empty beans}">
						
						<table width="100%" class="search">
							<tr>
								<th>
									Imi� i  nazwisko
								</th>
								<th>
									Data utworzenia
								</th>
								<th>
									Typ Logu
								</th>
								<th>
									Ostatnia akcja
								</th>
								<th>
									Data ostatniej akcji
								</th>
								<th>
									id dokumentu
								</th>
								<th>
									Ilo�� akcji 
								</th>
								<th>
									Pobierz Xes
								</th>
								<th>
									Pobierz xml
								</th>
								
							</tr>
							<c:forEach items="${beans}" var="bean">
								
									<tr>
										<td>
											 <b><c:out value="${bean.userFirstnameLastname}"/></b>
										</td>
										<td>
											 <b><fmt:formatDate value="${bean.creatingTime}" pattern="dd-MM-yyyy"/></b>
										</td>
										<td>
											 <b><c:out value="${bean.xesLogType}"/></b>
										</td>
										<td>
											 <b><c:out value="${bean.actionName}"/></b>
										</td>
										<td>
											 <b><fmt:formatDate value="${bean.actionTime}" pattern="dd-MM-yyyy HH:mm"/></b>
										</td>
										<td>
											<b><c:out value="${bean.documentId}"/></b>
										</td>
										<td>
											<b><c:out value="${bean.versionId}"/></b>
											
										</td>
										<td>																							
										 <input type="submit" name="doGetXesLog" value="<ds:lang text="Pobierz Xes Log"/>" class="btn" onclick="this.form.xesLogId.value = <c:out value='${bean.id}'/>; this.form.serialize.value = true"/>
										</td>
										
										<td>																							
										 <input type="submit" name="doGetXesLog" value="<ds:lang text="Pobierz Xml Log"/>" class="btn" onclick="this.form.xesLogId.value = <c:out value='${bean.id}'/>; this.form.serialize.value = false;"/>
										</td>
										
									</tr>
								
							</c:forEach>
						</table>
					</c:if>
			</table> 
		</form>
	</tabs:tab>
	
		<tabs:tab tabId="xessetings" var="tab">	
				<form name="xessetings"
			action="<ww:url value="'/admin/xes-log.action'"/>"
			method="post">
		<ww:hidden name="'xesLogId'" value="xesLogId" id="'xesLogId'"/>
		<ww:hidden name="'tabId'" value="tabId" id="'tabId'"/>	
				
				<table class="formTable" >
					
					
					<td></td>
					<h3> Ustawienia Logowania XES  </h3>
				
			<tr>
				<td>Logowanie do systemu:</td>
				<td><ww:checkbox name="'login'" value="login" fieldValue="true" id="login"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Wylogowanie z systemu:</td>
				<td><ww:checkbox name="'logout'" value="logout" fieldValue="true" id="logout"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Uworzenie dokumentu:</td>
				<td><ww:checkbox name="'doCreateDocument'" value="doCreateDocument" fieldValue="true" id="doCreateDocument"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Aktualizacja dokumentu:</td>
				<td><ww:checkbox name="'doUpdateDocument'" value="doUpdateDocument" fieldValue="true" id="doUpdateDocument"></ww:checkbox></td>
			</tr>
			<tr>
			<tr>
				<td>Zapisanie i nadanie numeru Ko:</td>
				<td><ww:checkbox name="'doSaveAndAssignOfficeNumber'" value="doSaveAndAssignOfficeNumber" fieldValue="true" id="doSaveAndAssignOfficeNumber"></ww:checkbox></td>
			</tr>
				<td>Dekretacja dokumentu:</td>
				<td><ww:checkbox name="'doAssign'" value="doAssign" fieldValue="true" id="doAssign"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Odbi�r Dekretacji dokumentu:</td>
				<td><ww:checkbox name="'doAcceptDecretation'" value="doAcceptDecretation" fieldValue="true" id="doAcceptDecretation"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Dodanie za�acznika do dokumentu:</td>
				<td><ww:checkbox name="'doAddAttachment'" value="doAddAttachment" fieldValue="true" id="doAddAttachment"></ww:checkbox></td>
			</tr>
			<tr>
				<!-- <p>Zdefiniowanie Uprawnie� dla Spraw w teczkach :</p> -->
				<td>Usuni�cie za�acznika z dokumentu:</td>
				<td><ww:checkbox name="'doDeleteAttachment'" value="doDeleteAttachment" fieldValue="true" id="doDeleteAttachment"></ww:checkbox></td>
			</tr>
			<%-- <tr>
				<td>Dodanie uwagi do dokumentu:</td>
				<td><ww:checkbox name="'doAddRemark'" value="doAddRemark" fieldValue="true" id="doAddRemark"></ww:checkbox></td>
			</tr>	
			 --%>
			
			<tr>
				<td>Utworzono sprawe i dodanie do niej dokumentu:</td>
				<td><ww:checkbox name="'doCreateCase'" value="doCreateCase" fieldValue="true" id="doCreateCase"></ww:checkbox></td>
			</tr>
			<%-- <tr>
				<td>Utworzenie teczki:</td>
				<td><ww:checkbox name="'doCreateFolder'" value="doCreateFolder" fieldValue="true" id="doCreateFolder"></ww:checkbox></td>
			</tr> --%>
			
			<tr>
				<td>Dodanie pisma do sprawy:</td>
				<td><ww:checkbox name="'doAddToCase'" value="doAddToCase" fieldValue="true" id="doAddToCase"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Dodanie pisma do nast�pnej sprawy:</td>
				<td><ww:checkbox name="'doAddToNextCase'" value="doAddToNextCase" fieldValue="true" id="doAddToNextCase"></ww:checkbox></td>
			</tr
			><tr>
				<td>Usuniecie pisma ze sprawy:</td>
				<td><ww:checkbox name="'doRemoveFromCase'" value="doRemoveFromCase" fieldValue="true" id="doRemoveFromCase"></ww:checkbox></td>
			</tr>
			<%-- <tr>
				<td>Wys�anie pisma do ePUAP:</td>
				<td><ww:checkbox name="'doWyslijDoEpuap'" value="doWyslijDoEpuap" fieldValue="true" id="doWyslijDoEpuap"></ww:checkbox></td>
			</tr> --%>
			
			
			
			<%-- <tr>
				<td>Dodanie pisma do obeserwowanych:</td>
				<td><ww:checkbox name="'doWatch'" value="doWatch" fieldValue="true" id="doWatch"></ww:checkbox></td>
			</tr> --%>
			<%-- <tr>
				<td>usuniecie pisma z obeserwowanych:</td>
				<td><ww:checkbox name="'doRemoveWatches'" value="doRemoveWatches" fieldValue="true" id="doRemoveWatches"></ww:checkbox></td>
			</tr> --%>
			
			<%-- <tr>
				<td>Przeniesienie sprawy z teczki do teczki:</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr> --%>
		<%-- 	<tr>
				<td>Powi�zanie sprawy ze spraw�:</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr> --%>
			<%-- <tr>
				<td>Zakonczenie pracy z dokumentem:</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr> --%>
			<%-- <tr>
				<td>Zamkniecie sprawy:</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr> --%>
			<%-- <tr>
				<td>Zakonczenie sprawy:</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr> --%>							
			</table>
				<input type="submit" name="doSaveSetings"class="btn" value="Zapisz ustawienia" /> 	
		</form>
			</tabs:tab>
					<ds:available test="layout2">
						<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
						<div class="bigTableBottomSpacer">&nbsp;</div>
						</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
					</ds:available>		

</tabs:tabs>
<script language="JavaScript">
	
    // funkcja wywolywana przez okienko wyboru sprawy
    function pickCase(id, caseId, mode)
    {
       if (!confirm('Na pewno powi�za� Sprawe ' +caseId+ ' do sprawy  ?'))
           return;
       
        document.getElementById('selectetIdFromSearchCases').value = id;
		document.getElementById('doAddCaseToCaseFromSearch').value = 'true';
		document.forms[0].submit();
    }
    
</script>
<script language="JavaScript">
	    function clearFields() {
	    	javascript:document.formul.reset();
    		/* document.getElementById('prettyRwaCategory').value = "";
    		document.getElementById('rwaId').value = '';
    		changeKind(); */
    		return false;
	    }
		// funkcja wywo�ywana przez okienko wyboru RWA
		/* function pickRwa(rootId, categoryId, rwaString)
		{
			document.getElementById('rwaId').value = categoryId;
			document.getElementById('prettyRwaCategory').value = rwaString;
		} */
		function setupValues(xesLogId)
		{
			document.getElementById('xesLogId').value = xesLogId;
		//	document.getElementById('precedentCaseId').innerHTML = '';
		}	    
</script>
<script type="text/javascript">
		Calendar.setup({
			inputField     :    "archDateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_archDateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "archDateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_archDateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "cdateFrom",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_cdateFrom_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		Calendar.setup({
			inputField     :    "cdateTo",     // id of the input field
			ifFormat       :    "<%=DateUtils.jsCalendarDateFormat%>",      // format of the input field
			button         :    "calendar_cdateTo_trigger",  // trigger for the calendar (button ID)
			align          :    "Tl",           // alignment (defaults to "Bl")
			singleClick    :    true
		});
		
	</script>