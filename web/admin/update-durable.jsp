<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZapiszSrodekTrwaly"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/admin/update-durable.action'"/>" enctype="application/x-www-form-urlencoded" method="post">

<ww:hidden name="'durable.id'" />

<h2>
	<input type="button" class="btn" value="<ds:lang text="WrocDoListySrodkowTrwalych" />" 
		onclick="location.href='<ww:url value="'/admin/durables-list.action'" />'"/>
</h2>

<table class="tableMargin">
<tr>
	<td>
		<ds:lang text="NumerInwentaryzacyjny"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:textfield name="'durable.inventoryNumber'" maxlength="50" size="15" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="NumerSeryjny"/>
	</td>
	<td>
		<ww:textfield name="'durable.serialNumber'" maxlength="50" size="15" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="LokalizacjaSrodkaTrwalego"/>
		<span class="star">*</span>
	</td>
	<td>
		<ww:select id="locationId" name="'durable.location'" list="locations" listKey="key" 
			listValue="value" cssClass="'sel'" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="OpisSrodkaTrwalego"/>
	</td>
	<td>
		<ww:textarea name="'durable.info'" cssClass="'txt'" cssStyle="'width: 300px; height: 80px;'" />
	</td>
</tr>
<tr>
	<td colspan="2">
		<p>
		<ds:lang text="PoleObowiazkowe"/>
		<span class="star">*</span>
		</p>
	</td>
</tr>
<tr>
	<td colspan="1">
		<ds:submit-event value="'Zapisz �rodek trwa�y'" name="'doUpdate'" cssClass="'btn'"/>
	</td>	
</tr>
</table>

</form>
