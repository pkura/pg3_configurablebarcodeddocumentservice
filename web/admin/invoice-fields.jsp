<%@ page import="pl.compan.docusafe.util.DateUtils" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Mapowanie obszar�w faktury</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<table class="mediumTable search userBilling">
	<tr>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=id&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Id <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=id&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CProject&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Projekt <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CProject&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CMpk&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Jednostka (MPK) <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CMpk&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CType&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Rodzaj kosztu <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CType&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CInventory&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Numer inwentarzowy budynk�w <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CInventory&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CFundsource&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> �r�d�o finansowania <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=CFundsource&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieMalejace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=IKind&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> Rodzaj dzia�alno�ci <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=IKind&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
		<th><a title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=startDate&ascending='+false"/>"><img
				src="<ww:url value="'/img/strzalka-dol-lista-red.gif'"/>" width="11"
				height="11" border="0" /></a> U�ytkownik <a
			title="<ds:lang text="SortowanieRosnace"/>"
			href="<ww:url value="'/admin/invoice-fields.action?sortField=startDate&ascending='+true"/>"><img
				src="<ww:url value="'/img/strzalka-gora-lista-red.gif'"/>"
				width="11" height="11" border="0" /></a></th>
	</tr>
	<ww:iterator value="invoiceFields">
		<tr>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="id" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="projectCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="mpkCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="typeCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="inventoryCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="fundsourceCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="kindCnName" />
			</a></td>
			<td><a
				href="<ww:url value="'/admin/invoice-fields.action?id='+id"/>">
					<ww:property value="userFullName" />
			</a></td>
		</tr>
	</ww:iterator>
</table>
<ww:set name="pager" scope="request" value="pager" />
		<div class="line25p">
			<jsp:include page="/pager-links-include.jsp"/>
		</div>
		
<br/><br/>
<form id="form" action="<ww:url value="'/admin/invoice-fields.action'"><ww:param name="'id'" value="id"/></ww:url>" method="post" enctype="multipart/form-data" >
	<table>
		<ww:hidden name="'id'" value="id"/>
		<tr>
			<td><font color="red">U�ytkownik: (*)</font></td>
		   	<td>
		   		<ww:select required="true" name="'dsuser'" id="user"
	            list="users" listKey="id" listValue="asLastnameFirstname()"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Projekt:</td>
		   	<td>
		   		<ww:select name="'cProject'" id="project"
	            list="getcProjects()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Jednostka (MPK):</td>
		   	<td>
		   		<ww:select name="'cMpk'" id="mpk"
	            list="getcMpks()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Rodzaj kosztu:</td>
		   	<td>
		   		<ww:select name="'cType'" id="type"
	            list="getcTypes()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Numer inwentarzowy budynk�w:</td>
		   	<td>
		   		<ww:select name="'cInventory'" id="inventory"
	            list="getcInventories()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>�r�d�o finansowania:</td>
		   	<td>
		   		<ww:select name="'cFundsource'" id="fundsource"
	            list="getcFundsources()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Rodzaj dzia�alno�ci:</td>
		   	<td>
		   		<ww:select name="'iKind'" id="kind"
	            list="getiKinds()" listKey="cn" listValue="cn+': '+title"
	            headerKey="''" headerValue="'-- wybierz --'" cssClass="'sel'"/>
	        </td>
		</tr>
		<tr>
			<td>Komentarz:</td>
		   	<td>
		   		<ww:textarea name="'remark'" cols="30" rows="8" cssClass="'txt'"/>
	        </td>
		</tr>
		<tr>
		<td colspan="2">
			<ww:if test="id == null"><ds:event name="'doAdd'" value="'Dodaj'" /></ww:if><ww:if test="id != null"><ds:event name="'doUpdate'" value="'Zapisz zmiany'" /><ds:event name="'doAdd'" value="'Zapisz jako nowy wpis'" /><ds:event name="'doDelete'" value="'Usu�'" /></ww:if>
		</td>
		</tr>
</table>
</form>