<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N barcode-printer.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<ds:available test="!layout2">
<p></p>
<ds:xmlLink path="NewBarcodePrinter"/>
<br/>
</ds:available>
<ds:available test="layout2">
	<h1>&nbsp;</h1>
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="NewBarcodePrinter"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/barcode-printer.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table class="tableMargin">
	    <tr>
	        <td>
	        	<ds:lang text="UzywajDrukarkiKodowKreskowych"/>:
	        </td>
	        <td>
	        	<ww:checkbox name="'usePrinter'" fieldValue="true"/>
	        </td>
	    </tr>
	    <tr>
	        <td>
	        	<ds:lang text="UrzadzenieSystemowe"/>:
	        </td>
	        <td>
	        	<ww:textfield name="'device'" size="20" maxlength="20" cssClass="'txt'"/>
	        </td>
	    </tr>
	    <tr>
	    	<td>
	    	</td>
	    	<td>
	    		<span class="italic"><ds:lang text="DrukarkaKodowInfo"/></span>
	    	</td>
	    </tr>
	</table>
	
	<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn saveBtn'"/>
	
	<ds:available test="layout2">
			<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
			<div class="bigTableBottomSpacer">&nbsp;</div>
			</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
		</ds:available>
</form>

<%--
<h1><ds:lang text="DrukarkaKodowKreskowych"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/barcode-printer.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

<table>
    <tr>
        <td><ds:lang text="UzywajDrukarkiKodowKreskowych"/>:</td>
        <td><ww:checkbox name="'usePrinter'" fieldValue="true"/></td>
    </tr>
    <tr>
        <td><ds:lang text="UrzadzenieSystemowe"/>:</td>
        <td><ww:textfield name="'device'" size="20" maxlength="20" cssClass="'txt'"/><br/><i>
        <ds:lang text="DrukarkaKodowInfo"/>
        </td>
    </tr>
</table>

<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>

</form>
--%>
<!--N koniec barcode-printer.jsp N-->