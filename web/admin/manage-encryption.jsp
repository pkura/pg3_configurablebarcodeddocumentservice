<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1"><ds:lang text="AutoryzacjaDostepuDoTajnych"/> <ww:property value="subtitle"/></h1>
<hr class="fullLine horizontalLine"/>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<ds:lang text="OczekujaceAutoryzacje"/>
<table>

	<tr>
		<th>
			<ds:lang text="Uzytkownik"/>
		</th>
		<th>
			<ds:lang text="KodKlucza"/>
		</th>
		<th>
			<ds:lang text="DataZadania"/>
		</th>
		<th>
		</th>
		<th>
		</th>
	</tr>
	<ww:iterator value="keys">

		<tr>
			<td>
				<ww:property value="username"/>
			</td>
			<td>
				<ww:property value="keyCode"/>
			</td>
			<td>
				<ww:property value="ctime"/>
			</td>
			<td>
				<a href="<ww:url value="'/admin/manage-encryption.action'"/>?doGrantAccess=true&keyId=<ww:property value="keyId"/>"><ds:lang text="Przydziel"/></a>
			</td>
			<td>
				<a href="<ww:url value="'/admin/manage-encryption.action'"/>?doDeleteKey=true&keyId=<ww:property value="keyId"/>"><ds:lang text="Usun"/></a>
			</td>
		</tr>
		
	</ww:iterator>
	<ww:iterator value="grantedKeys">

		<tr>
			<td>
				<ww:property value="username"/>
			</td>
			<td>
				<ww:property value="keyCode"/>
			</td>
			<td>
				<ww:property value="ctime"/>
			</td>
			<td>
				<a href="<ww:url value="'/admin/manage-encryption.action'"/>?doDeleteKey=true&keyId=<ww:property value="keyId"/>"><ds:lang text="Usun"/></a>
			</td>
		</tr>
		
	</ww:iterator>


</table>

<ww:if test="key!=null">
<h3>
	<ds:lang text="UWAGABrakKlucza"/>
</h3>
Klucz:<ww:textfield name="'key'" readonly="true" size="60"></ww:textfield>
</ww:if>