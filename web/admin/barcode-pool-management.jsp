<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 04.08.08
	Poprzednia zmiana: 04.08.08
C--%>
<!--N roles.jsp N-->

<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Pulabarkodow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<edm-html:errors />
<edm-html:messages />

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
    <tr>
        <th><ds:lang text="Wykorzystaneprefixy"/></th>
    </tr>
    <ww:iterator value="prefixes">
    	<tr>
            <td>
    		    <ww:property value="toString()"/><br>
    	    </td>
        </tr>
    </ww:iterator>
</table>

<table>
    <tr>
        <th>
            <ds:lang text="Aktualnyprefix"/>
        </th>
    </tr>
    <tr>
        <td>
            <ww:property value="prefix"/>
        </td>
    </tr>
</table>

<form method="get" action="?doAdd=doAdd">
    <table>
        <tr>
            <th>
               <ds:lang text="Nowyprefix"/>
            </th>
        </tr>
        <tr>
            <td>
               <ds:lang text="Nowyprefixwarrning"/>
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="prefixToAdd" class="txt" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="poolStart" class="txt" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" name="poolEnd" class="txt" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="<ds:lang text="Addprefix"/>" class="btn"/>
                <input type="hidden" value="doAdd" name="doAdd"/>
            </td>
        </tr>
    </table>
</form>


<%--
<h1><ds:lang text="Pulabarkodow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>
--%>


