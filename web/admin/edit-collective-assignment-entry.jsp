<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-collective-assignment-entry.jsp N-->

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="ZbiorczaDekretacja"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="entry == null">
    <p><ds:lang text="NowyTypDekretacji"/></p>
</ww:if>
<ww:else>
    <p><ds:lang text="EdycjaTypuDekretacji"/></p>
</ww:else>

<form action="<ww:url value="'/admin/edit-collective-assignment-entry.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doSave" id="doSave"/>
<ww:hidden name="'id'"/>

<table>
    <tr>
        <td><ds:lang text="NazwaDekretacjiCel"/>:</td>
        <td><ww:textfield name="'objective'" size="50" maxlength="100" value="entry != null ? entry.objective : null" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td><ds:lang text="ProcesWorkflow"/>:</td>
        <td><ww:select name="'processId'" list="processManagers" listKey="workflowName + ',' + package_id()+'::'+process_definition_id()" listValue="name()" value="entry != null ? entry.wfProcess : null" cssClass="'sel'" /></td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" name="doSave" value="<ds:lang text="Zapisz"/>" class="btn saveBtn" onclick="document.getElementById('doSave').value='true';"/>
            <input type="button" value="<ds:lang text="Anuluj"/>" onclick="document.location.href='<ww:url value="'/admin/collective-assignment.action'"/>'" class="btn cancelBtn"/>
        </td>
    </tr>
</table>
</form>
