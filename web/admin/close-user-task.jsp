<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 05.08.08
	Poprzednia zmiana: 05.08.08
C--%>
<!--N close-user-task.jsp N-->

<%@ page import="pl.compan.docusafe.web.admin.CloseUserTask"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.core.office.Person"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ZamykanieZadanUzytkownikow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:lang text="WybierzUzytkownikowKtorymChceszZamknacZadania"/>
<p></p>

<form id="form" action="<ww:url value="'/admin/close-user-task.action'"></ww:url>" method="post" >
	<table class="tableMargin">
		<tr>
			<td>
				<ww:select name="'availableUsers'" multiple="true" size="20" cssClass="'multi_sel combox-chosen'" cssStyle="'width: 300px;'" list="usersAll" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " onclick="moveOptions(this.form.availableUsers, this.form.usernames);" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " onclick="moveOptions(this.form.usernames, this.form.availableUsers);" class="btn"/>
			</td>
			<td>
				<ww:select name="'usernames'" id="usernames" multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'"/>
			</td>
		</tr>
	</table>
	
	<br/>
	<ds:lang text="ZadaniaKtoreTrafilyNaListeUzytkownikaPrzedDniem"/>:
	<ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
	<img src="<ww:url value="'/calendar096/img.gif'"/>" id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
		title="Date selector" onmouseover="this.style.background='red';" onmouseout="this.style.background=''"/>
	
	<br/>
	<input type="submit" class="btn" name="doClose" value="<ds:lang text="Rozpocznij"/>"
		onclick="selectAllOptions(document.getElementById('usernames'));"/>
	
	<h3><ds:lang text="Konsola"/></h3>
	<ww:select name="''" list="messages" listKey="''" size="15" id="console" cssClass="'multi_sel'" cssStyle="'width: 800px'"/>
	<span id="consoleStatus"></span>
</form>


<%--
<form id="form" action="<ww:url value="'/admin/close-user-task.action'"></ww:url>" method="post" >


<h1><ds:lang text="ZamykanieZadanUzytkownikow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p><p>
<ds:lang text="WybierzUzytkownikowKtorymChceszZamknacZadania"/>
<p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>
<p>

<table>
		<tr>
			<td valign="top">
				<ww:select name="'availableUsers'" multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
					list="usersAll" />
			</td>
			<td valign="middle">
				<input type="button" value=" &gt;&gt; "
					onclick="moveOptions(this.form.availableUsers, this.form.usernames);"
					class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; "
					onclick="moveOptions(this.form.usernames, this.form.availableUsers);"
					class="btn"/>
				<br/>
			</td>
			<td valign="top">
				<table width="100%">
					<tr>
						<td rowspan="2">
							<ww:select name="'usernames'"
								id="usernames"
								multiple="true" size="20" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
								/>
						</td>
					</tr>
				</table>
			</td>
		</tr>

</table>
<table>
<tr>
	<td><ds:lang text="ZadaniaKtoreTrafilyNaListeUzytkownikaPrzedDniem"/>:</td>
	<td><ww:textfield name="'documentDate'" size="10" maxlength="10" cssClass="'txt'" id="documentDate"/>
		<img src="<ww:url value="'/calendar096/img.gif'"/>"
			id="documentDateTrigger" style="cursor: pointer; border: 1px solid red;"
			title="Date selector" onmouseover="this.style.background='red';"
			onmouseout="this.style.background=''"/>
	</td>
</tr>
</table>
<table width="100">
	<tr>
		<td>
		
		<input type="submit" class="btn" name="doClose" value="<ds:lang text="Rozpocznij"/>"
			onclick="selectAllOptions(document.getElementById('usernames'));"/>
		</td>	
	</tr>
</table>

	<h3><ds:lang text="Konsola"/></h3>

	<table width="800">
		<tr>
			<td>
				<ww:select name="''" list="messages" listKey="''" size="15" id="console"
					cssClass="'multi_sel'" cssStyle="'width: 100%'"/>
			</td>
		</tr>
		<tr>
			<td><span id="consoleStatus"></span></td>
		</tr>
	</table>
--%>

<script type="text/javascript">
	Calendar.setup({
		inputField	:	"documentDate",			// id of the input field
		ifFormat	:	"%d-%m-%Y",				// format of the input field
		button		:	"documentDateTrigger",	// trigger for the calendar (button ID)
		align		:	"Bl",					// alignment (defaults to "Bl")
		singleClick	:	true
	});
</script>
<!--N koniec close-user-task.jsp N-->