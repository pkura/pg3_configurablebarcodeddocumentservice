<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N barcode-printer.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<%--  <ds:available test="!layout2">
<ds:xmlLink path="NewBarcodePrinter"/>
<br/>
</ds:available>
<ds:available test="layout2">
	<h1>&nbsp;</h1>
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="NewBarcodePrinter"/>
	</div>
</ds:available> --%>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/user-to-briefcase.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	
	
	<div>
	<!-- <p>W FUNKCJONALNOSC  TRAKCIE BUDOWY - NIE ZAIMPLEMENTOWANE</p> -->
		<p>Pracownicy przypisani:</p>
		<ww:select id="usersBriefcaseList" name="'usersBriefcaseList'" list="list_user_to_briefcase" listKey="key" listValue="value" size="10"  cssClass="''" cssStyle="'min-width:1200px;'" />
		
		<ds:submit-event value="getText('Usun')" name="'doDelete'" cssClass="'btn'"/>
		<ww:if test="sorted"> 
		<ds:submit-event value="getText('Poka� wszystkich')" name="'doDefault'" cssClass="''"/>
		</ww:if>
	
	
	</div>
	
	
	<tr>
	</tr>
	
	
	<table>
	<p>Pokazanie Przypisa� wybranego u�ytkownika</p>
	<tr>
		<td>
		<ww:select id="sortUsersList" name="'sortUsersList'" list="users" listValue="value" listKey="key" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel combox-chosen'" />
		</td>
		<td>
		<ds:submit-event value="getText('Poka� Przypisania')" name="'doSortByUser'" cssClass="''"/>
		</td>
	</tr>
	
	</table>
	
	<table>
	<p>Zdefiniowanie Uprawnie� Pracownik�w:</p>
	<tr>
		<th>Pracownik:</th>
		<th>Teczka:</th> 
		<td></td>
	</tr>
	<tr>
		<td>
			<ww:select id="usersList" name="'usersList'" list="users" listValue="value" listKey="key" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel combox-chosen'" />
		</td>
		<td>
			<ww:select id="briefcaseList" name="'briefcaseList'" list="briefcase" listValue="value" listKey="key" headerKey="''"  headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
			<ds:submit-event value="getText('Dodaj')" name="'doAdd'" cssClass="''"/>
		</td>
	</tr>	
	
		<!-- <p>Zdefiniowanie Uprawnie� dla pracownika :</p> -->
		<tr>
				<td>Mo�e podej�e�</td>
				<td><ww:checkbox name="'canView'" value="canView" fieldValue="true" id="canView"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Mo�e utworzy�</td>
				<td><ww:checkbox name="'canCreate'" value="canCreate" fieldValue="true" id="canCreate"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Mo�e modyfikowa�</td>
				<td><ww:checkbox name="'canModify'" value="canModify" fieldValue="true" id="canModify"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Mo�e wydrukowa�</td>
				<td><ww:checkbox name="'canPrint'" value="canPrint" fieldValue="true" id="canPrint"></ww:checkbox></td>
			</tr>
			
			<tr>
				<!-- <p>Zdefiniowanie Uprawnie� dla Spraw w teczkach :</p> -->
				<td>Mo�e przeglada� sprawy w teczce</td>
				<td><ww:checkbox name="'canViewCases'" value="canViewCases" fieldValue="true" id="canViewCases"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Mo�e dodawa� sprawy do teczki</td>
				<td><ww:checkbox name="'canAddCasestoBriefcase'" value="canAddCasestoBriefcase" fieldValue="true" id="canAddCasestoBriefcase"></ww:checkbox></td>
			</tr>
	</table>
	
	 
</form>