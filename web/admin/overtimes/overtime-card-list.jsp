<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KartaWykorzystaniaNadgodzinZaRok"/> <ww:property value="currentYear"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<!-- WYSZUKIWARKA -->
<form id="SearchAbsencesForm" name="SearchAbsencesForm" action="<ww:url value="baseUrl"/>" method="post"
	enctype="application/x-www-form-urlencoded">
	
	<ww:hidden name="'pageNumber'" id="pageNumber" />
	
<fieldset class="container">
	<p class="simpleSearch">
		<label for="empCard.user.lastname"><ds:lang text="NazwiskoPracownika"/></label>
		<ww:textfield id="empCard.user.lastname" name="'empCard.user.lastname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.user.firstname"><ds:lang text="ImiePracownika"/></label>
		<ww:textfield id="empCard.user.firstname" name="'empCard.user.firstname'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.KPX"><ds:lang text="NumerEwidencyjny" /></label>
		<ww:textfield id="empCard.KPX" name="'empCard.KPX'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p class="simpleSearch">
		<label for="currentYear"><ds:lang text="WybierzRok" /></label>
		<ww:select id="currentYear" name="'currentYear'" list="yearsMap" listKey="key" 
			listValue="value" cssClass="'sel dontFixWidth'" cssStyle="'width: 100px;'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="empCard.company"><ds:lang text="NazwaSpolki" /></label>
		<ww:textfield id="empCard.company" name="'empCard.company'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.department"><ds:lang text="NazwaDepartamentu"/></label>
		<ww:textfield id="empCard.department" name="'empCard.department'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p>
		<label for="empCard.position"><ds:lang text="NazwaStanowiska"/></label>
		<ww:textfield id="empCard.position" name="'empCard.position'" maxlength="50" size="15" cssClass="'txt dontFixWidth'" />
	</p>
	<p class="simpleSearch">
			<label for="maxResults"><ds:lang text="LiczbaRezultatowNaStronie" /></label>
			<ww:select name="'maxResults'" id="maxResults" list="maxResultsMap" listKey="key"
					listValue="value" cssClass="'sel dontFixWidth'" />
	</p>
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Szukaj wpis�w'" name="'goSearch'" cssClass="'btn btnBanner'"/>
	<ds:submit-event value="'Generuj raport XLS'" name="'generateReport'" cssClass="'btn btnBanner'"/>
	<a id="searchSwitcher" href="#" extendedSearch="false" >Wyszukiwanie zaawansowane</a>
	<a id="clearFields" href="#" >Wyczy��</a>
</div>

<div class="clearBoth" style="height: 30px"></div>

<ds:submit-event value="'Nalicz urlopy na bie��cy rok'" name="'chargeOvertimes'" 
                 confirm="'Zostan� naliczone nadgodziny dla os�b poni�ej. Czy naliczy�?'"
                 cssClass="'btn'" disabled="!chargeOvertimesEnabled" />
</form>
<!-- END / WYSZUKIWARKA -->

<!-- TABELA Z DANYMI -->

<form action="<ww:url value="baseUrl"/>" method="post" enctype="application/x-www-form-urlencoded">
<table class="search bottomSpacer" id="mainTable" cellspacing="0">
<thead>
<tr>
	<th class="empty">
		<ds:lang text="PodgladEdycja" />
	</th>
	<th class="empty">
		<ds:lang text="Karta" />
	</th>
	<th class="empty">
		<ds:lang text="Lp"/>
	</th>
	<th class="empty">
		<ds:lang text="NumerEwidencyjny"/>
	</th>
	<th class="empty">
		<ds:lang text="NazwiskoImiePracownika"/>
	</th>
	<th class="empty">
		<ds:lang text="LiczbaWypracowanychNadgodzin"/>
	</th>
	<th class="empty">
		<ds:lang text="LiczbaOdebranychNadgodzin"/>
	</th>
	<th class="empty">
		<ds:lang text="PozostaleNadgodziny"/>
	</th>
	
</tr>
</thead>
<tbody>
    <ww:iterator value="employees">
        <tr>
            <td>
                <a href="<ww:url value="'/absences/overtime-card.action'">
				 	<ww:param name="'empCard.id'" value="id" />
				 	<ww:param name="'currentYear'" value="currentYear" />
				 </ww:url>">
			<ds:lang text="PodgladEdycja" />
		</a>
            </td>
            <td>
                <a href="<ww:url value="'docusafe/pdf-overtimes.action'">
				 	<ww:param name="'empCardName'" value="user.name" />
				 	<ww:param name="'currentYear'" value="currentYear" />
				 	<ww:param name="'doPdf'" value="'true'" />
				 </ww:url>">
			<ds:lang text="pdf" />
		</a>
            </td>
            <td>
                <ww:property value="lp" />
		<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
            </td>
            <td>
		<ww:property value="KPX" />
            </td>
            <td>
                <ww:property value="user.lastname" /> <ww:property value="user.firstname" />
            </td>
            <td><ww:property value="empYearsSummary.allHour" /></td>
            <td><ww:property value="empYearsSummary.allHourReceive" /></td>
            <td><ww:property value="empYearsSummary.allHourLeft" /></td>
        </tr>
    </ww:iterator>
</tbody>
</table>
        <!-- PAGER -->
<table class="table100p">
    <tr>
        <td align="center">
                <ww:set name="pager" scope="request" value="pager"/>
                        <jsp:include page="/pager-links-include.jsp"/>
                        <script type="text/javascript">
                                $j('.pager a').click(function() {
                                        var page = $j(this).attr('href');
                                        if (page == '#')
                                                return false;
                                        $j('#pageNumber').attr('value', page);
                                        $j('#generateReport').attr('value', '');
                                        document.forms.SearchAbsencesForm.submit();
                                        return false;
                                });
                        </script>
        </td>
</tr>
</table>
                        
<ww:hidden name="'currentYear'" />
<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane urlopy?'" name="'doBatchDelete'" cssClass="'btn'"/>
</form>
<!-- KONIEC TABELI Z DANYMI -->


<script type="text/javascript">
	bindLabelDecoration();
	initExtendedSearch();
</script>