<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NadgodzinyPracownika"/>
	<ww:property value="currentYear"/>
	- <ww:if test="empCard.user != null">
	  	<ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />
	  </ww:if> 	
	  <ww:else>
	  	<ww:property value="empCard.externalUser" />
	  </ww:else>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="button" class="btn btnBanner" value="<ds:lang text="WrocDoKartNadgodzinPracownikow" />"
    onclick="location.href='<ww:url value="'/absences/overtime-card-list.action'">
            <ww:param name="'currentYear'" value="currentYear" />
     </ww:url>'" />

<form action="<ww:url value="'/absences/overtime-card.action'"/>" method="post">        
<ww:hidden name="'empCard.id'" value="empCard.id"/>
<ww:hidden name="'currentYear'" value="currentYear"/>

<table class="search bottomSpacer" id="mainTable" cellspacing="0">
    <thead>
        <tr>
            <th><ds:lang text="Edytuj"/></th>
            <th><ds:lang text="Lp"/></th>
            <th><ds:lang text="Data"/></th>
            <th><ds:lang text="GodzinaOd"/></th>
            <th><ds:lang text="GodzinaDo"/></th>
            <th><ds:lang text="IloscNadgodzin"/></th>
            <th><ds:lang text="Opis"/></th>
            <th><ds:lang text="Typ"/></th>
            <th><ds:lang text="DocumentId"/></th>
        </tr>
    </thead>
    <tbody>
        <ww:iterator value="overtimes">
        <tr>
            <td>
                <a href="<ww:url value="'/absences/overtime-record.action'">
                       <ww:param name="'empCard.id'" value="empCard.id"/>
                       <ww:param name="'currentYear'" value="currentYear"/>
                       <ww:param name="'overtime.id'" value="id"/>
                   </ww:url>">
                    Edytuj
                </a>
            
            </td>
            <td><ww:checkbox name="'deleteIds'" fieldValue="id" value="false" /></td>
            <td><ds:format-date value="overtimeDate" pattern="dd-MM-yyyy"/></td>
            <td><ww:property value="timeFromRead"/></td>
            <td><ww:property value="timeToRead"/></td>
            <td><ww:property value="timeCountRead"/></td>
            <td><ww:property value="description"/></td>
            <td><ww:property value="overtimeReceiveRead"/></td>
            <td><b><a href="<ww:url value="'/repository/edit-document.action'"><ww:param name="'id'" value="documentId"/></ww:url>">
                        <ww:property value="documentId" /></a></b></td>
        </tr>
        </ww:iterator>
    </tbody>
        <tr>
            <td colspan="2"><ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun�� wybrane Nadgodziny?'" name="'doBatchDelete'" cssClass="'btn cancelBtn'"/></td>
            <td>
                <input type="button" value="Dodaj nadgodziny" class="btn" 
                        onclick="document.location.href='<ww:url value="'/absences/overtime-record.action'">
                        <ww:param name="'empCard.id'" value="empCard.id" />
                        <ww:param name="'currentYear'" value="currentYear" />
                 </ww:url>'" />

            </td>
        </tr>
</table>
</form>