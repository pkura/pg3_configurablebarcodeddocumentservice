<%@page contentType="text/html" pageEncoding="ISO-8859-2"%>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="NadgodzinyPracownika"/>
	- <ww:if test="empCard.user != null">
	  	<ww:property value="empCard.user.lastname" /> <ww:property value="empCard.user.firstname" />
	  </ww:if> 	
	  <ww:else>
	  	<ww:property value="empCard.externalUser" />
	  </ww:else>
</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

    <input type="button" class="btn btnBanner" value="<ds:lang text="WrocDoKartNadgodzinPracownikow" />"
        onclick="location.href='<ww:url value="'/absences/overtime-card.action'">
            <ww:param name="'empCard.id'" value="empCard.id"/>
                <ww:param name="'currentYear'" value="currentYear" />
         </ww:url>'" />

    
    <form action="<ww:url value="'/absences/overtime-record.action'"/>" method="post">
        <ww:hidden name="'overtime.id'" value="overtime.id"/>
        <ww:hidden name="'overtime.documentId'" value="overtime.documentId"/>
        <ww:hidden name="'overtime.employeeId'" value="empCard.id"/>
        <ww:hidden name="'empCard.id'" value="empCard.id"/>
        
        <ww:hidden name="'currentYear'" value="currentYear"/>
        <table class="tableMargin">
            <tr>
                <td><ds:lang text="Data"/><span class="star">*</span></td>
                <td>
                    	<ww:textfield name="'overtimeDate'"  size="15" maxlength="10" cssClass="'txt'" id="overtimeDate"/>
			<img src="<ww:url value="'/calendar096/img.gif'"/>"
				id="overtimeDateTrigger" style="cursor: pointer; border: 1px solid red;"
				title="Date selector" onmouseover="this.style.background='red';"
				onmouseout="this.style.background=''"/>
			<script type="text/javascript">
				Calendar.setup({
	    			inputField     :    "overtimeDate",     // id of the input field
	    			ifFormat       :    "<%= DateUtils.jsCalendarDateFormat %>",      // format of the input field
	    			button         :    "overtimeDateTrigger",  // trigger for the calendar (button ID)
	    			align          :    "Tl",           // alignment (defaults to "Bl")
	    			singleClick    :    true
				});
			</script>
                </td>
            </tr>
            <tr>
                <td><ds:lang text="GodzinaOd"/><span class="star">*</span></td>
                <td><ww:textfield id="dockind_TIME_FROM" name="'overtime.timeFrom'" maxlength="5" size="15" /></td>
            </tr>
            <tr>
                <td><ds:lang text="GodzinaDo"/><span class="star">*</span></td>
                <td><ww:textfield id="dockind_TIME_TO" name="'overtime.timeTo'" maxlength="5" size="15" /></td>
            </tr>
            <tr>
                <td><ds:lang text="IloscNadgodzin"/><span class="star">*</span></td>
                <td><ww:textfield readonly="true" id="dockind_TIME_COUNT" name="'overtime.timeCount'" maxlength="5" size="15" /></td>
            </tr>
            <tr>
                <td><ds:lang text="Typ"/></td>
                <td>
                    <select name="overtime.overtimeReceive" class="sel">
                        <option value="false" <ww:if test="overtime.overtimeReceive == null"> selected="selected"</ww:if>>Zlecenie nadgodzin</option>
                        <option value="true" <ww:if test="overtime.overtimeReceive == true"> selected="selected"</ww:if>>Odbi�r nadgodzin</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><ds:lang text="Opis"/></td>
            <td><ww:textarea name="'overtime.description'" rows="5" cols="47" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td colspan="1">
                        <ds:submit-event value="'Zapisz nadgodziny'" name="'doSave'" cssClass="'btn'"/>
                </td>	
            </tr>
        </table>
    </form>
    <script type="text/javascript">
function setTimeFormat($value){
    if($value == null )
        return;
    if($value != null && $value.length > 5)
      return $value.substr(0,5).replace(/[^0-9\:]/g,'');
    else 
        return $value.substr(0,5).replace(/[^0-9\:]/g,'');
}

function setDefaultTime($object)
{
    if($object.val() == '' || $object.val() == null )
    {
        $object.val('00:00');
    }
}

    function getTimeSubtractFromString($timeFromVal, $timeToVal, validateHours)
    {
                    if(!($timeFromVal != null && $timeFromVal.length >3 && $timeToVal != null && $timeToVal.length >3)){
                            throw "BadError";
                    }else	   
                    {

                            var $timeFromArray = $timeFromVal.split(":");
                            var $timeToArray = $timeToVal.split(":");

                            if(parseInt($timeFromArray.length) < 2 || parseInt($timeToArray.length) < 2){
                                    throw "BadError";
                            }
                            if(($timeFromArray[0] == $timeToArray[0]) && ($timeFromArray[1] == $timeToArray[1]))
                                throw "SameTimeError";
                            //walidacja godzin
                            if($timeFromArray[0] > 24 || $timeFromArray[0] < 0 || $timeToArray[0] > 24 || $timeToArray[0] < 0)
                              throw "BadHour";
                            //walidacja minut
                            if($timeFromArray[1] > 59 || $timeFromArray[1] < 0 || $timeToArray[1] > 59 || $timeToArray[1] < 0)
                              throw "BadMinut";

                            if(validateHours == true)
                                if($timeFromArray[0] > $timeToArray[0] || ($timeToArray[0] == $timeFromArray[0] && $timeToArray[1] < $timeFromArray[1]))
                                    throw "BadHourTo";

                            var $hours = $timeToArray[0] - $timeFromArray[0];
                            var $minutes = $timeToArray[1] - $timeFromArray[1];

                            if($minutes < 0)
                            {
                                    $minutes = 60 - Math.abs($minutes);
                                    if($hours <= 0 && validateHours)
                                       throw "BadHourTo";
                                    $hours -= 1;
                            }

                            return (($hours < 10 && $hours >= 0? '0'+ $hours : $hours) + ':' + ($minutes < 10 && $minutes >= 0 ? '0'+ $minutes : $minutes));
                    }
    }

    function alertError(e)
    {
            switch(e)
                    { 
                      case "BadError":
                            alert('Twoja godzina jest b��dna, poprawny format to np. 08:00');
                            break;
                      case "BadHour":
                            alert('Godzina ma format w przedziale 0-23');
                            break;
                      case "BadMinut":
                            alert('Minuta ma format w przedziale 0-59');
                            break;
                      case "BadHourTo":
                            alert('Pole: \'Godzina do\' musi by� wi�ksze od pola \'Godzina od\'!');
                            break;
                      case "SameTimeError":
                            alert('Godziny nie mog� by� takie same!');
                            break;
                     default:
                             alert(e);
                    }
    }
        	
            $j(function(){
//                try
//                {
                $timeFromObject = $j('#dockind_TIME_FROM');
                $timeFromObject.val(setTimeFormat($timeFromObject.val()));
                setDefaultTime($timeFromObject);
                $timeToObject = $j('#dockind_TIME_TO');
                $timeToObject.val(setTimeFormat($timeToObject.val()));
                setDefaultTime($timeToObject);
            	$timeCountObject = $j('#dockind_TIME_COUNT');
                $timeCountObject.val(setTimeFormat($timeCountObject.val()));
                setDefaultTime($timeCountObject);
                
                $timeFromObject.blur(function(){
                    $timeFromObject.val(setTimeFormat($timeFromObject.val()));
                    try
                    {
                            $timeCountObject.val(getTimeSubtractFromString($timeFromObject.val(), $timeToObject.val(), false));
                    }catch(e){
                            alertError(e);
                            return false;
                    }
                });
                
                
                $timeToObject.blur(function(){
                    $timeToObject.val(setTimeFormat($timeToObject.val()));
                    try
                    {
                            $timeCountObject.val(getTimeSubtractFromString($timeFromObject.val(), $timeToObject.val(), true));
                    }catch(e){
                            alertError(e);
                            return false;
                    }
               });      

                
                $timeCountObject.change(function(){
                    $timeFromObject.val(setTimeFormat($timeCountObject.val()));
                });   
            });	
    </script>