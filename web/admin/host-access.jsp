<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Dost�py host�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/host-access.action'"/>" method="post" enctype="multipart/form-data">
    <ww:hidden name="'doUpdate'" id="doUpdate"/>
    <ww:hidden name="'publicKey.id'" value="publicKey.id"/>

    <table>
        <tr>
            <td>Nazwa Hosta:</td>
            <td><ww:textfield name="'publicKey.hostName'" value="publicKey.hostName" cssClass="'txt'" size="50"/></td>
        <tr>
        <tr>
            <td>Typ klucza:</td>
            <td><ww:select name="'keyType'" value="keyType" cssClass="'sel'" list="keyTypes" listKey="name()" listValue="name()"/></td>
        <tr>

        <tr>
            <td>Klucz:</td>
            <td><ww:file name="'file'" cssClass="'txt'" size="50" id="file" /></td>
        <tr>
        <tr>
            <td><ds:submit-event name="'doUpdate'" value="'Zapisz'"/><input type="button" value="Anuluj" class="btn"
                       onclick="document.location.href='<ww:url value="'/admin/host-access.action'"/>';"/><td>
        </tr>
    </table>



    <table cellspacing="0" cellpadding="3" id="mainTable">
            <tr>
                <td> Id </td>
                <td> Host </td>
                <td> Typ klucza </td>
                <td> Wgrany klucz </td>
                <td/>
                <td/>
            </tr>

            <ww:iterator value="keys" status="rowstatus">
                <tr>
                    <td><ww:property value="id"/></td>
                    <td><ww:property value="hostName"/></td>
                    <td><ww:property value="keyType"/></td>
                    <td><ww:if test="publicKey != null">TAK</ww:if></td>
                    <td>
                        <a href="<ww:url value="'/admin/host-access.action'"><ww:param name="'id'" value="id"/></ww:url>"><b><ds:lang text="Edytuj"/></b></a>
                        </td>
                     <td>
                     <a href="<ww:url value="'/admin/host-access.action'"><ww:param name="'id'" value="id"/><ww:param name="'doDelete'" value="true"/></ww:url>"><b><ds:lang text="Usun"/></b></a>
                    </td>
                </tr>
            </ww:iterator>
    </table>

</form>