<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/office-points.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	
	<div>
		<p>Zdefiniowane punkty Kancelarii:</p>
		<ww:select id="selectetDefiniedPoint" name="'selectetDefiniedPoint'" list="listOfficePoints" listKey="key" listValue="value" size="10" cssClass="''" cssStyle="'min-width:1200px;'" />
		
		<ds:submit-event value="getText('Usun')" name="'doDelete'" cssClass="'btn'"/>
			
	</div>

	<!--   <table>
	<p>Pokazanie przypisa� wybranego u�ytkownika:</p>
	<tr>
		<td>
		<ww:select id="sortUsersList" name="'sortUsersList'" list="users" listValue="value" listKey="key" headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
		<ds:submit-event value="getText('Poka� Przypisania')" name="'doSortByUser'" cssClass="''"/>
		</td>
	</tr>
	</table> -->
	
	<table>
	<p>Dodanie punktu kancelaryjnego</p>
	<tr>
		<th>Prefix</th>
		<th>Nazwa folderu na maszynie OCR:</th>
		<th>nazwa dzia�u do dekretacji:</th> 
	</tr>
	<tr>
	<td>
		<ww:select id="selectedPrefix" name="'selectedPrefix'"  list="avaialblePrefiks" listValue="value" listKey="key" headerKey="''"  headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
			<ww:textfield name="'newfolderName'" id="newfolderName" cssClass="'txt'" value="newfolderName" size="30"/>
		</td>
		<td>
			<ww:select id="selectedDivisionId" name="'selectedDivisionId'" list="divisions" listValue="value" listKey="key" headerKey="''"  headerValue="getText('select.wybierz')" cssClass="'sel'" />
		</td>
		<td>
			<ds:submit-event value="getText('Dodaj')" name="'doAdd'" cssClass="''"/>
		</td>
	</tr>	
	</table>
</form>