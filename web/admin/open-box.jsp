<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N open-box.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="Pudlo"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p> 

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<script type="text/javascript">
    function ask(id){
    	var ret = confirm("Wybranych dokument�w : "+id+" nie mo�na umiescic w danym pudle, poniewa� zostaly wczesniej przypisane do innego pudla "+
    	"czy chcesz przeniesc wszystkie dokumenty do tego pudla?" );
    	if(ret){
	        document.getElementById('doBoxPTE').value = 'true';
	        document.forms[0].submit();
    	}
    } 

</script>




<ww:if test="ask">
	<form action="<ww:url value="'/admin/open-box.action'"/>" method="post">
    	<ww:hidden name="'startId1'" id="'startId1'"/>
		<ww:hidden name="'endId1'" id="'endId1'"/>
		<ww:hidden name="'withoutId1'" id="'withoutId1'"/>
		<ww:hidden name="'boxName1'" id="'boxName1'"/>
		<ww:hidden name="'item1'" id="'item1'"/>
    	<input type="hidden" name="doBoxPTE" id="doBoxPTE"/>
    	
		<script>
			
					ask(<ww:property value="wrongId"/>);
			
		</script>
	</form>
</ww:if>



<ds:dockinds test="dc">
	<ds:available test="serialBoxing">
	   <a href="javascript:show_hide()" alt="Poka� szczeg�y" title="Poka� szczeg�y">Seryjne umieszczanie dokument�w w pudle</a>
	</ds:available>
	<form  action="<ww:url value="'/admin/open-box.action'"/>" method="post" onsubmit=" disableFormSubmits(this);">
	
	<ww:hidden name="'startId1'" id="'startId1'"/>
	<ww:hidden name="'endId1'" id="'endId1'"/>
	<ww:hidden name="'withoutId1'" id="'withoutId1'"/>
	<ww:hidden name="'boxName1'" id="'boxName1'"/>
	<ww:hidden name="'item1'" id="'item1'"/>
	<table id="hidden1" style="display:none" >
	    <tr>
	        <td>Numer pud�a:</td>
	        <td><ww:textfield name="'boxName'" id="boxName" size="20" maxlength="100" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Kategoria:</td>
	        <td><ww:select name="'item'" list="items" listKey="id" listValue="title" cssClass="'sel'" id="item" headerKey="''" headerValue="'-- wyszystkie --'"/></td>
	    </tr>
	    <tr>
	        <td>Pocz�tkowy numer sprawy:</td>
	        <td><ww:textfield name="'startId'" id="startId" size="10" maxlength="10" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Ko�cowy numer sprawy:</td>
	        <td><ww:textfield name="'endId'" id="endId" size="10" maxlength="10" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Opr�cz numeru sprawy</td>
	        <td><ww:textarea name="'withoutId'" rows="'3'" id="withoutId" cols="'12'" cssClass="'txt' "/></td>
	    </tr>	    
	    <tr>
	    	<td><ds:submit-event value="'Umie�� w pudle'" name="'doCheckBoxPTE'" cssClass="'btn'" confirm="'Czy na pewno chcesz umie�ci� wybrane sprawy w pudle?'"/></td>
	    </tr>
	</table>
	
	
	</form>
</ds:dockinds>
<hr/>




<ds:dockinds test="dc">
	<ds:available test="serialBoxing">
	   <a href="javascript:show_hideWithdraw()" alt="Poka� szczeg�y" title="Poka� szczeg�y">Seryjne wycofywanie dokument�w z pud�a</a>
	</ds:available>
	<form  action="<ww:url value="'/admin/open-box.action'"/>" method="post" onsubmit=" disableFormSubmits(this);">
	
	<ww:hidden name="'startWithdrawId1'" id="'startWithdrawId1'"/>
	<ww:hidden name="'endWithdrawId1'" id="'endWithdrawId1'"/>
	<ww:hidden name="'boxNameWithdraw1'" id="'boxNameWithdraw1'"/>
	<table id="hidden2" style="display:none" >
	    <tr>
	        <td>Numer pud�a:</td>
	        <td><ww:textfield name="'boxNameWithdraw'" id="boxNameWithdraw" size="20" maxlength="100" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Kategoria:</td>
	        <td><ww:select name="'item'" list="items" listKey="id" listValue="title" cssClass="'sel'" id="item" headerKey="''" headerValue="'-- wyszystkie --'"/></td>
	    </tr>
	    <tr>
	        <td>Pocz�tkowy numer sprawy:</td>
	        <td><ww:textfield name="'startWithdrawId'" id="startWithdrawId" size="10" maxlength="10" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	        <td>Ko�cowy numer sprawy:</td>
	        <td><ww:textfield name="'endWithdrawId'" id="endWithdrawId" size="10" maxlength="10" cssClass="'txt'"/></td>
	    </tr>
	    <tr>
	    	<td><ds:submit-event value="'Wycofaj z pud�a'" name="'doWithdraw'" cssClass="'btn'" confirm="'Czy na pewno chcesz wycofa� wybrane sprawy z pud�a?'"/></td>
	    </tr>
	</table>
	
	
	</form>
</ds:dockinds>



<ds:dockinds test="!dc">
</ds:dockinds>
<form action="<ww:url value="'/admin/open-box.action'"/>" method="post"
    onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'boxNumber'" id="boxNumber"/>
<ww:hidden name="'line'" id="line"/>
<hr/>

<ww:iterator value="boxLines">
    <table>
		 	
        <tr>
            <td><ds:lang text="Linia"/>:</td>
            <td><b><ww:property value="name"/></b></td>
        </tr>
        <tr>
            <td><ds:lang text="BiezacePudlo"/>:</td>
            <td><b><ww:property value="currentBoxNumber"/></b><ww:if test="!boxOpen && currentBoxNumber != null">(<ds:lang text="zamkniete"/>)</ww:if></td>
        </tr>
        <tr>
            <td>
                <ww:if test="currentBoxNumber != null"><ds:lang text="OtworzKolejne"/>:</ww:if>
                <ww:else><ds:lang text="OtworzNowe"/>:</ww:else>
            </td>
            <td><input type="text" id="<ww:property value="'boxNumber'+line"/>" value="<ww:property value="boxNumber"/>" class="txt" size="20" maxlength="20"/></td>
        </tr>
    </table>
    
    <ds:event value="getText('Otworz')" name="'doOpenBox'" cssClass="'btn'" disabled="!canManage" onclick="jsOnSubmit"/>
    <ds:event value="getText('Zamknij')" name="'doCloseBox'" cssClass="'btn'" disabled="!canManage" onclick="jsOnSubmit" />
    <ds:event value="getText('OtworzPonownie')" name="'doReopenBox'" cssClass="'btn'" disabled="!canManage" onclick="jsOnSubmit" />
    <br/>
    <br/>
</ww:iterator>

<hr/>

<h3><ds:lang text="ZmianaNumeruIstniejacegoPudla"/></h3>

<table>
    <tr>
        <td><ds:lang text="WybierzNumerIstniejacegoPudla"/>:</td>
        <td><ww:select name="'existingBoxId'" list="boxList"
            listKey="id" listValue="nameWithLine"  
            headerKey="''" headerValue="getText('select.wybierz')" cssClass="'sel'" />
        </td>
        <td>
            <ww:textfield name="'newBoxNumber'" cssClass="'txt'" size="30" maxlength="30" />  
        </td>
    </tr>
</table>

<ds:submit-event value="getText('ZmodyfikujNumerPudla')" name="'doRename'" disabled="!canManage" />

</form>

<script type="text/javascript">
	function show_hide(){
		document.getElementById('hidden1').style.display=
			((document.getElementById('hidden1').style.display=="none")?true:false)?"":"none";

	}
	function show_hideWithdraw(){
		document.getElementById('hidden2').style.display=
			((document.getElementById('hidden2').style.display=="none")?true:false)?"":"none";
	}
    function onSubmit(line)
    {
        var boxNumber = document.getElementById('boxNumber'+line).value;
        //document.location.href = '<ww:url value="'/admin/open-box.action'"/>?line='+line+'&boxNumber='+boxNumber;
        document.getElementById('line').value = line;
        document.getElementById('boxNumber').value = boxNumber;
    }
    

</script>