<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 30.07.08
	Poprzednia zmiana: 30.07.08
C--%>
<!--N performance-test.jsp N-->

<%@ page import="pl.compan.docusafe.core.cfg.Configuration"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="TestWydajnosci"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/performance-test.action'"/>" method="post">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="CzasTestu"/>:
			</td>
			<td>
				<ww:textfield name="'testTime'" value="testTime" cssClass="'txt'" size="5" id="testTime"/>
				<ds:lang text="JednostkaCzasuTestu"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="MinimalnyCzasOczekiwanaNaAkcje"/>:
			</td>
			<td>
				<ww:textfield name="'minWaitTime'" value="minWaitTime" cssClass="'txt'" size="5" id="minWaitTime"/>
				<ds:lang text="JednostkaCzasuTestu"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="MaksymalnyCzasOczekiwanaNaAkcje"/>:
			</td>
			<td>
				<ww:textfield name="'maxWaitTime'" value="maxWaitTime" cssClass="'txt'" size="5" id="maxWaitTime"/>
				<ds:lang text="JednostkaCzasuTestu"/>
			</td>
		</tr>
	</table>
	
	<br/>
	
	<table class="tableMargin">
		<tr>
			<th>
				<ds:lang text="DostepniUzytkownicy"/>:
			</th>
			<td></td>
			<th>
				<ds:lang text="UruchomTestDlaUzytkownikow"/>:
			</th>
		</tr>
		<tr>
			<td>
				<ww:select name="'availableUsers'" multiple="true" size="10" cssClass="'multi_sel combox-chosen'" cssStyle="'width: 300px;'" list="usersAll" />
			</td>
			<td class="alignMiddle">
				<input type="button" value=" &gt;&gt; " onclick="moveOptions(this.form.availableUsers, this.form.usernames);" class="btn"/>
				<br/>
				<input type="button" value=" &lt;&lt; " onclick="moveOptions(this.form.usernames, this.form.availableUsers);" class="btn"/>
			</td>
			<td>
				<ww:select name="'usernames'" id="usernames" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'" list="usersSelected"/>
			</td>
		</tr>
	</table>
	
	<br/>
	<ds:lang text="AkcjeTestowe"/>:
	<br/>
	<ww:checkbox name="'testActivities'" fieldValue="'tasks'"/>
	<ds:lang text="podgladListyZadan"/>
	<br/>
	<ww:checkbox name="'testActivities'" fieldValue="'summary'"/>
	<ds:lang text="podgladDokumentu"/>
	<br/>
	<ww:checkbox name="'testActivities'" fieldValue="'search'"/>
	<ds:lang text="wyszukiwanieDokumentow"/>
	<br/>
	<br/>
	<input type="submit" class="btn" name="doStartTest" value="<ds:lang text="RozpocznijTest"/>"
		onclick="selectAllOptions(document.getElementById('usernames'));"/>
</form>

<%--
<h1><ds:lang text="TestWydajnosci"/></h1>
<hr size="1" align="left" class="highlightedText" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/performance-test.action'"/>" method="post">
    <table>
        <tr>
            <td><ds:lang text="CzasTestu"/>:</td>
            <td><ww:textfield name="'testTime'" value="testTime" cssClass="'txt'" size="5" id="testTime"/><ds:lang text="JednostkaCzasuTestu"/></td>
        </tr>
        <tr>
            <td><ds:lang text="MinimalnyCzasOczekiwanaNaAkcje"/>:</td>
            <td><ww:textfield name="'minWaitTime'" value="minWaitTime" cssClass="'txt'" size="5" id="minWaitTime"/><ds:lang text="JednostkaCzasuTestu"/></td>
        </tr>
        <tr>
            <td><ds:lang text="MaksymalnyCzasOczekiwanaNaAkcje"/>:</td>
            <td><ww:textfield name="'maxWaitTime'" value="maxWaitTime" cssClass="'txt'" size="5" id="maxWaitTime"/><ds:lang text="JednostkaCzasuTestu"/></td>
        </tr>

        
        <tr>
            <th><ds:lang text="DostepniUzytkownicy"/>:</th>
            <th></th>
            <th><ds:lang text="UruchomTestDlaUzytkownikow"/>:</th>
        </tr>
        <tr>
            <td valign="top">
                <ww:select name="'availableUsers'" multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                    list="usersAll" />
            </td>
            <td valign="middle">
                <input type="button" value=" &gt;&gt; "
                    onclick="moveOptions(this.form.availableUsers, this.form.usernames);"
                    class="btn"/>
                <br/>
                <input type="button" value=" &lt;&lt; "
                    onclick="moveOptions(this.form.usernames, this.form.availableUsers);"
                    class="btn"/>
                <br/>
            </td>
            <td valign="top">
                <table width="100%">
                    <tr>
                        <td rowspan="2">
                            <ww:select name="'usernames'"
                                id="usernames"
                                multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                                list="usersSelected"/>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="2"><ds:lang text="AkcjeTestowe"/>:</td></tr>
        <tr><td colspan="2"><ww:checkbox name="'testActivities'" fieldValue="'tasks'"/><ds:lang text="podgladListyZadan"/></td></tr>
        <tr><td colspan="2"><ww:checkbox name="'testActivities'" fieldValue="'summary'"/><ds:lang text="podgladDokumentu"/></td></tr>
        <tr><td colspan="2"><ww:checkbox name="'testActivities'" fieldValue="'search'"/><ds:lang text="wyszukiwanieDokumentow"/></td></tr>
        
    </table>
    <input type="submit" class="btn" name="doStartTest" value="<ds:lang text="RozpocznijTest"/>"
        onclick="selectAllOptions(document.getElementById('usernames'));"/>
</form>
--%>
<script language="javascript">
	<%--
	var chks = document.getElementsByName('testActivities');
	for (i = 0; i < chks.length; ++i)
	{
	    chks[i].checked = true;
	}
	--%>
	$j("*[name='testActivities']").attr("checked","true");
</script>

<!--N koniec performance-test.jsp N-->