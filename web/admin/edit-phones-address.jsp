<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 30.07.08
	Poprzednia zmiana: 30.07.08
C--%>
<!--N address.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneAdresowe"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>


<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>
<p></p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/edit-phones-address.action'"/>?tabId=<ww:property value="tabId" />" enctype="application/x-www-form-urlencoded" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>


<ww:hidden name="'address.id'" />

<!-- formatka dodawania nowego telefonu -->
<fieldset class="container">
	<p>
		<label for="phone.name"><ds:lang text="phoneName" /></label>
		<ww:textfield id="phone.name" name="'phone.name'" maxlength="100" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="phone.type"><ds:lang text="phoneType" /></label>
		<ww:textfield id="phone.type" name="'phone.type'" maxlength="25" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>

<fieldset class="container">
	<p>
		<label for="phone.number"><ds:lang text="phoneNumber" /></label>
		<ww:textfield id="phone.number" name="'phone.number'" maxlength="25" cssClass="'txt dontFixWidth'" />
	</p>
</fieldset>

<div class="fieldsetSearch">
	<ds:submit-event value="'Dodaj nowy telefon'" name="'addPhone'" cssClass="'btn'"/>
</div>
<script type="text/javascript">bindLabelDecoration();</script>
<div class="clearBoth" style="height: 30px"></div>


<ww:if test="address.id != null && address.id > 0">

<%-- TELEFONY i FAXY ORGANIZACJII --%>
<table class="mediumTable search bottomSpacer widePadding" cellspacing="0">
<caption><ds:lang text="telefonyFaxy" /></caption>
<thead>
	<tr>
		<th class="empty">&nbsp;</th>
		<th class="empty">
			<ds:lang text="phoneName" />
		</th>
		<th class="empty">
			<ds:lang text="phoneType" />
		</th>
		<th class="empty">
			<ds:lang text="phoneNumber" />
		</th>
	</tr>
</thead>

<tbody>
	<ww:iterator value="address.phones">
	<tr>
		<td>
			<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
		</td>
		<td>
			<ww:property value="name" />
		</td>
		<td>
			<ww:property value="type" />
		</td>
		<td>
			<ww:property value="number" />
		</td>
	</tr>
	</ww:iterator>
</tbody>
</table>
		<ds:submit-event value="'Usu� zaznaczone'" name="'doDeletePhones'" cssClass="'btn'"/>
</ww:if>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>