<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 30.07.08
	Poprzednia zmiana: 30.07.08
C--%>
<!--N address.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneAdresowe"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>


<ds:available test="layout2">
<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ww:iterator value="tabs" status="status" >
		<a href="<ww:url value='link'/>" title="<ww:property value='title'/>" <ww:if test="selected">class="highlightedText"</ww:if>><ww:property value="name"/></a>
	</ww:iterator>
</div> 
</ds:available>

<form action="<ww:url value="'/admin/edit-address.action'"/>?tabId=<ww:property value="tabId" />" enctype="multipart/form-data" method="post">

<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:hidden name="'address.id'" />
<ww:hidden name="'address.headOffice'" />

<table class="search" id="mainTable" cellspacing="0">
<tr>
	<td>
		<ds:lang text="addressName"/>
	</td>
	<td>
		<ww:textfield name="'address.name'" maxlength="50" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="addressOrganization"/>
	</td>
	<td>
		<ww:textfield name="'address.organization'" maxlength="100"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="addressStreet"/>
	</td>
	<td>
		<ww:textfield name="'address.street'" maxlength="100" />
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="addressZipCode"/>
	</td>
	<td>
		<ww:textfield name="'address.zipCode'" maxlength="10"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="addressCity"/>
	</td>
	<td>
		<ww:textfield name="'address.city'" maxlength="50"/>
	</td>
</tr>
<tr>
	<td>
		<ds:lang text="addressRegion"/>
	</td>
	<td>
		<ww:textfield name="'address.region'" maxlength="50" />
	</td>
</tr>

<ww:if test="address.image != null">
<tr>
	<td>
		<ds:lang text="AktualnaMapkaDojazdu" />
	</td>
	<td>
		<a href="<ww:url value="'/admin/image-viewer.action'" />?imgId=<ww:property value="address.image.id" />" target="_blank">
			<ww:property value="address.image.name" />
		</a>
	</td>
</tr>
</ww:if>

<tr>
	<td>
		<ds:lang text="mapkaDojazdu" />
	</td>
	<td>
		<ww:file name="'file'" cssClass="'txt'" size="50" id="file"/>
	</td>
</tr>

<!-- ZAPISZ ZMIANY -->
<table>
<tr><td>&nbsp;</td></tr>
<tr>
	<td colspan="1">
		<ds:submit-event value="'Zapisz adres'" name="'doSave'" cssClass="'btn'"/>
	</td>	
</tr>
</table>

<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>

</form>

<%--
<h1><ds:lang text="DaneAdresowe"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<form action="<ww:url value="'/admin/address.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

    <table>
    <tr>
        <td><ds:lang text="NazwaOrganizacji"/>:</td>
        <td><ww:textfield name="'organization'" size="50" maxlength="126" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Ulica"/>:</td>
        <td><ww:textfield name="'street'" size="50" maxlength="126" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td valign="top"><ds:lang text="KodPocztowyImiasto"/>:</td>
        <td><ww:textfield name="'zip'" id="zip" size="8" maxlength="15" cssClass="'txt'"/>
            <ww:textfield name="'location'" size="38" maxlength="100" cssClass="'txt'"/>
            <br><span id="zipError"> </span>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="Wojewodztwo"/>:</td>
        <td><ww:textfield name="'region'" size="50" maxlength="126" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/></td>
    </tr>
    </table>
</form>

<script language="JavaScript">
	var szv = new Validator(document.getElementById('zip'), '##-###');
	
	szv.onOK = function() {
	    this.element.style.color = 'black';
	    document.getElementById('zipError').innerHTML = '';
	}
	
	szv.onEmpty = szv.onOK;
	
	szv.onError = function(code) {
	    this.element.style.color = 'red';
	    if (code == this.ERR_SYNTAX)
	        document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowy"/>';
	    else if (code == this.ERR_LENGTH)
	        document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakow"/>';
	}
	
	szv.canValidate = function() { return true; }
	/*
	    var country = document.getElementById('country');
	    var countryCode = country.options[country.options.selectedIndex].value;
	
	    return (countryCode == '' || countryCode == 'PL');
	*/
	szv.revalidate();
</script>
--%>
<!--N koniec address.jsp N-->
