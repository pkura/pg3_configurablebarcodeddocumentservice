<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.07.08
	Poprzednia zmiana: 29.07.08
C--%>
<!--N barcode-printer.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>

<ds:available test="!layout2">
<ds:xmlLink path="NewBarcodePrinter"/>
<br/>
</ds:available>
<ds:available test="layout2">
	<h1>&nbsp;</h1>
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'"/>" class="tab2_img" />
	<ds:xmlLink path="NewBarcodePrinter"/>
	</div>
</ds:available>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/new-barcode-printer.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ds:available test="layout2">
			<div id="middleContainer"> <!-- BIG TABLE start -->
		</ds:available>
	<table class="tableMargin">
        <tr>
            <td>
                <ds:lang text="UserPrinterName"/>:
                <span class="star">*</span>
            </td>
            <td>
                <ww:textfield name="'userPrinterName'" size="35" maxlength="35" cssClass="'txt'"/>
            </td>
        </tr>
	     <tr>
	        <td>
	        	<ds:lang text="NazwaDrukarki"/>:
	        </td>
	        <td>
	        	<ww:select name="'printerName'" id="printerName" list="availablePrinters" listKey="key"
				listValue="value" cssClass="'sel'"/>
	        </td>
	    </tr>
        <tr>
            <td>
                <ds:lang text="DaneBarkodu"/>:
            </td>
            <td>
                <ww:textfield name="'barcodeValue'" size="20" maxlength="20" cssClass="'txt'"/>
            </td>
        </tr>
	    <tr>
	        <td>
	        	<ds:lang text="UzywajDrukarkiKodowKreskowych"/>:
	        </td>
	        <td>
	        	<ww:checkbox name="'usePrinter'" fieldValue="true"/>
	        </td>
	    </tr>
	    <tr>
	        <td>
	        	<ds:lang text="JezykProgramowaniaDrukarki"/>:
	        </td>
	        <td>
	        	<ww:select name="'printerLanguage'" id="printerLanguage" list="availableLanguages" listKey="key"  
				listValue="value" cssClass="'sel'"/>
	        </td>
	    </tr>
	    
	     <tr>
	        <td>
	        	<ds:lang text="PierwszyWierszNaglowka"/>:
	        </td>
	        <td>
	        	<ww:textfield name="'firstLine'" size="35" maxlength="35" cssClass="'txt'"/>
	        </td>
	    </tr>
	    <tr>
	        <td>
	        	<ds:lang text="DrugiWierszNaglowka"/>:
	        </td>
	        <td>
	        	<ww:textfield name="'secondLine'" size="35" maxlength="35" cssClass="'txt'"/>
	        </td>
	    </tr>
	     <tr>
	        <td>
	        	<ds:lang text="RodzajKodu"/>:
	        </td>
	        <td>
	        	<ww:select name="'barcodeType'" id="barcodeType" list="availableBarcodes" listKey="key" headerKey="''" headerValue="getText('select.wybierz')"  
				listValue="value" cssClass="'sel'"/>
	        </td>
	    </tr>
	</table>
	
	<ds:submit-event value="getText('Test')" name="'doTest'" cssClass="'btn'"/>
	<%--<ds:button-redirect href="'/admin/barcode-printers-list.action'" value="'Lista drukarek'"/>--%>
	<ds:submit-event value="getText('Zapisz')" name="'doSave'" cssClass="'btn saveBtn'"/>

</form>
