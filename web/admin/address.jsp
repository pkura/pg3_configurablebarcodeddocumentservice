<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 30.07.08
	Poprzednia zmiana: 30.07.08
C--%>
<!--N address.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="DaneAdresowe"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/address.action'"/>" method="post">
<table class="search table100p bottomSpacer" id="mainTable" cellspacing="0">
<tr>
	<th class="empty">&nbsp;</th>
	<th class="empty">
		<ds:lang text="wybierzCentrale" />
	</th>
	<th class="empty">
		<ds:lang text="addressName"/>
	</th>
	<th class="empty">
		<ds:lang text="addressOrganization"/>
	</th>
	<th class="empty">
		<ds:lang text="addressStreet"/>
	</th>
	<th class="empty">
		<ds:lang text="addressZipCode"/>
	</th>
	<th class="empty">
		<ds:lang text="addressCity"/>
	</th>
	<th class="empty">
		<ds:lang text="addressRegion"/>
	</th>
</tr>
<ww:iterator value="addresses">
<tr>
	<td>
		<ww:checkbox name="'deleteIds'" fieldValue="id" value="false" />
	</td>
	<td>
		<input type="radio" name="headOfficeId" value="<ww:property value="id" />"
			<ww:if test="headOffice">
				checked="1"
			</ww:if>
		 />
	</td>
	<td>
		<a href="<ww:url value="'/admin/edit-address.action'"/>?tabId=0&address.id=<ww:property value="id"/>"><ww:property value="name"/></a>
	</td>
	<td>
		<ww:property value="organization"/>
	</td>
	<td>
		<ww:property value="street"/>
	</td>
	<td>
		<ww:property value="zipCode"/>
	</td>
	<td>
		<ww:property value="city"/>
	</td>
	<td>
		<ww:property value="region"/>
	</td>
</tr>
</ww:iterator>
</table>

<ds:submit-event value="'Usu� zaznaczone'" confirm="'Czy napewno usun��?'" name="'doDelete'" cssClass="'btn'"/>
<ds:submit-event value="'Wybierz central�'" name="'doSelectHeadOffice'" cssClass="'btn'"/>
<input type="button" value="<ds:lang text="dodajNowyAdres" />" class="btn" 
	onclick="location.href='<ww:url value="'/admin/edit-address.action'"/>'" />


<%--
<h1><ds:lang text="DaneAdresowe"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<edm-html:errors />
<edm-html:messages />

<form action="<ww:url value="'/admin/address.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

    <table>
    <tr>
        <td><ds:lang text="NazwaOrganizacji"/>:</td>
        <td><ww:textfield name="'organization'" size="50" maxlength="126" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td><ds:lang text="Ulica"/>:</td>
        <td><ww:textfield name="'street'" size="50" maxlength="126" cssClass="'txt'" /></td>
    </tr>
    <tr>
        <td valign="top"><ds:lang text="KodPocztowyImiasto"/>:</td>
        <td><ww:textfield name="'zip'" id="zip" size="8" maxlength="15" cssClass="'txt'"/>
            <ww:textfield name="'location'" size="38" maxlength="100" cssClass="'txt'"/>
            <br><span id="zipError"> </span>
        </td>
    </tr>
    <tr>
        <td><ds:lang text="Wojewodztwo"/>:</td>
        <td><ww:textfield name="'region'" size="50" maxlength="126" cssClass="'txt'"/></td>
    </tr>
    <tr>
        <td></td>
        <td><ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/></td>
    </tr>
    </table>
</form>

<script language="JavaScript">
	var szv = new Validator(document.getElementById('zip'), '##-###');
	
	szv.onOK = function() {
	    this.element.style.color = 'black';
	    document.getElementById('zipError').innerHTML = '';
	}
	
	szv.onEmpty = szv.onOK;
	
	szv.onError = function(code) {
	    this.element.style.color = 'red';
	    if (code == this.ERR_SYNTAX)
	        document.getElementById('zipError').innerHTML = '<ds:lang text="NiepoprawnyKodPocztowy"/>';
	    else if (code == this.ERR_LENGTH)
	        document.getElementById('zipError').innerHTML = '<ds:lang text="KodPocztowyMusiMiecSzescZnakow"/>';
	}
	
	szv.canValidate = function() { return true; }
	/*
	    var country = document.getElementById('country');
	    var countryCode = country.options[country.options.selectedIndex].value;
	
	    return (countryCode == '' || countryCode == 'PL');
	*/
	szv.revalidate();
</script>
--%>
<!--N koniec address.jsp N-->
