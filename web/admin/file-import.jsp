<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<!--N file-import.jsp N-->

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ImportZPliku"/></h1>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:form action="'file-import.action'" namespace="'/admin'" method="'post'" enctype="'multipart/form-data'" >
Rodzaj importu: <ds:degradable-select name="'importKindKey'" list="importKinds"/> <br/>

<ww:file name="'file'" cssClass="txt" size="50" id="file"/> <br/>

<ds:submit-event value="Importuj" name="'doImport'" cssClass="btn"/>
</ww:form>

<!--N koniec file-import.jsp N-->
