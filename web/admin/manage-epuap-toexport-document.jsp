<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="Dokumenty oczekujace na wys�anie"/></h1>
<p></p>

<ds:available test="!layout2">
<ds:xmlLink path="ExportManager"/>
</ds:available>

<ds:available test="layout2">
	<div id="middleMenuContainer">
	<img src="<ww:url value="'/img/tab2_left_corner.gif'" />" class="tab2_img" />
	<ds:xmlLink path="ExportManager"/>
	</div>
</ds:available>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>


<form action="<ww:url value="'/admin/manage-epuap-toexport-document.action'"/>" method="post">
<ds:available test="layout2">
	<div id="middleContainer"> <!-- BIG TABLE start -->
</ds:available>
<table class="mediumTable search">
	<tr>
		<th>
			ID
		</th>
		<th>
			Odbiorca
		</th>
		<th>
			Adres Skrytki
		</th>
		<th>
			Identyfikator
		</th>
		<th>
			Status
		</th>
		<th>
			Odpowiedz ePUAP
		</th>
	</tr>
	<ww:iterator value="toExportDocuments">
		<ww:set name="stat" value="status"/>
		<tr>
			<td>
				<a href="<ww:url value="'/office/outgoing/main.action'"><ww:param name="'documentId'" value="documentId"/></ww:url>" ><ww:property value="documentId"/></a>
			</td>
			<td>
				<ww:property value="person.lastname"/>&nbsp;<ww:property value="person.firstname"/>&nbsp;<ww:property value="person.organization"/>
			</td>
			<td>
				<ww:property value="person.adresSkrytkiEpuap"/>
			</td>
			<td>
				<ww:property value="person.identifikatorEpuap"/>
			</td>
			<td>
				<ww:property value="statusAsString(#stat)"/>
			</td>
			<td>
				<ww:property value="odpowiedz"/>
			</td>
		</tr>
	 </ww:iterator>
</table>
<script type="text/javascript">

</script>
<ds:available test="layout2">
	<!-- Zeby kreska na dole nie byla tuz przy przyciskach -->
	<div class="bigTableBottomSpacer">&nbsp;</div>
	</div> <!-- BIG TABLE end MUSI byc tak, bo div nie moze otaczac form -->
</ds:available>
</form>
