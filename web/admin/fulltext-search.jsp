<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 pl.compan.docusafe.util.DateUtils"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>

<jsp:include page="/office/common/inc-title.jsp"/>

<div id="messages">
	<ds:ww-action-errors/>
	<ds:ww-action-messages/>
</div>

<form method="POST">
<ds:event name="'generateAktaPubliczneIndex'" value="'Wygeneruj indeksy tabeli Akt publicznych'"/>
<br>
<ds:event name="'generateDocumentIndex'" value="'Wygeneruj indeksy pism'"/> 
</form>