<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N system-folders.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1><ds:lang text="FolderySystemowe"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/system-folders.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<ww:hidden name="'trashId'" id="trashId"/>
<ww:hidden name="'officeId'" id="officeId"/>
<ww:hidden name="'archiveId'" id="archiveId"/>

<table>
<tr>
    <td><ds:lang text="Kosz"/>:</td>
    <td><ww:textfield name="'trashPrettyPath'" id="trashPrettyPath" size="50" maxlength="200" cssClass="'txt'"/>
        <input type="button" value="<ds:lang text="Wybierz"/>" class="btn"
            onclick="javascript:void(window.open('<ww:url value="'/repository/folders-tree-popup.jsp?lparam=trash'"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no'));"/>
    </td>
</tr>
<tr>
    <td><ds:lang text="DokumentyKancelaryjne"/>:</td>
    <td><ww:textfield name="'officePrettyPath'" id="officePrettyPath" size="50" maxlength="200" cssClass="'txt'"/>
        <input type="button" value="<ds:lang text="Wybierz"/>" class="btn"
            onclick="javascript:void(window.open('<ww:url value="'/repository/folders-tree-popup.jsp?lparam=office'"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no'));"/>
    </td>
</tr>
<tr>
    <td><ds:lang text="ArchiwumBiznesowe"/>:</td>
    <td><ww:textfield name="'archivePrettyPath'" id="archivePrettyPath" size="50" maxlength="200" cssClass="'txt'"/>
        <input type="button" value="<ds:lang text="Wybierz"/>" class="btn"
            onclick="javascript:void(window.open('<ww:url value="'/repository/folders-tree-popup.jsp?lparam=archive'"/>', null, 'width=300,height=300,menubar=no,toolbar=no,status=no,location=no'));"/>
    </td>
</tr>
</table>

<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>

</form>

<script>
function pickFolder(id, prettyPath, lparam)
{
    alert('pickFolder '+id+' '+prettyPath+' '+lparam);
    if (lparam == 'trash')
    {
        document.getElementById('trashPrettyPath').value = prettyPath;
        document.getElementById('trashId').value = id;
    }
    else if (lparam == 'office')
    {
        document.getElementById('officePrettyPath').value = prettyPath;
        document.getElementById('officeId').value = id;
    }
    else if (lparam == 'archive')
    {
        document.getElementById('archivePrettyPath').value = prettyPath;
        document.getElementById('archiveId').value = id;
    }
}
</script>