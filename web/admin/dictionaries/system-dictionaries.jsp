<!--N system-dictionaries.jsp N-->
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">S�owniki systemu Docusafe</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script type="text/javascript">
	function loadPage(page, cn){
		try {
			$j("#ajaxContent").hide("slow", function(){
				$j("#ajaxContent").hide();
				$j.post(page,{}, function(data){
					$j("#ajaxContent").html(data).insertAfter("#"+cn).show("slow");
				});
			});
		} catch(ex) {
			alert(ex.toString());
		}
	}

	$j(document).ready(function(){
		$j("#dictionaries").ajaxError(function(event, request, settings){
			$j("#ajaxContent").hide("slow").html("Error requesting page " + request.toString())
				.insertAfter("#dictionaries").show("slow");
		});
	});

	function editRow(id, dictionaryCn){
		$j.post('<ww:url value="'/office/dictionaries/editable-dictionary.action'"/>',
            {
                "doPrepareEdit"  :"true",
                "id":id,
                "dictionaryCn":dictionaryCn
            },
            function(data) {
                $j("#rowId"+id).html(data);
            });
	}

	function submitEdit(id, dictionaryCn){
        var map = {};
        $j('#form_'+id+' input, #form_'+id+' select').each( function() {
            map[this.name] = $j(this).val();
        });
        map['id'] = id;
        map['doUpdate'] = 'true';
        map['dictionaryCn'] = dictionaryCn;
        $j.post('<ww:url value="'/office/dictionaries/editable-dictionary.action'"/>',
            map,
            function(data){
                $j("#ajaxContent").html(data).insertAfter("#"+cn).show("slow");
            });
	}
</script>
<div id="ajaxContent"></div>
<div id="dictionaries">
<ww:iterator value="dictionaries">
	<div id="<ww:property value="cn"/>">
		<h2><ww:property value="title"/>
		<a href="javascript:loadPage('<ww:url value="selectionURL"/>','<ww:property value="cn"/>')"><img alt="Wy�wietl" src="<ww:url value="img/dol.gif"/>"/></a>
		</h2>
	</div>
</ww:iterator>
</div>
<!--N koniec system-dictionaries.jsp N-->