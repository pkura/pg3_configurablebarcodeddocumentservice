<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N office-document-delivery.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<ww:if test="documentType == 'in'"><ww:set name="sposob" value="'dostarczenia'"/></ww:if>
<ww:else><ww:set name="sposob" value="'odbioru'"/></ww:else>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Sposoby"/> <ww:property value="#sposob"/> <ds:lang text="pisma"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doDelete" id="doDelete"/>
	
	<table class="tableMargin">
		<tr>
			<th></th>
			<th>
				<ds:lang text="Sposob"/> <ww:property value="#sposob"/> <ds:lang text="pisma"/>
			</th>
			<th></th>
		</tr>
		<ww:iterator value="deliveries">
			<tr>
				<td>
					<ww:checkbox name="'deliveryIds'" fieldValue="id" disabled="!canDelete"/>
				</td>
				<td>
					<ww:property value="name"/>
				</td>
				<td>
					[
					<a href="<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">
						<ds:lang text="edycja"/></a>
					]
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<input type="button" value="<ds:lang text="NowySposob"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery-edit.action'"/>';"/>
	<!-- <input type="submit" value="<ds:lang text="Usun"/>" class="btn" onclick="if (!confirm(getText('NaPewnoUsunac'))) return false; document.getElementById('doDelete').value='true'"/> -->
	<ds:submit-event value="'Usu�'" name="'doDelete'" cssClass="'btn'"/>
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1><ds:lang text="Sposoby"/> <ww:property value="#sposob"/> <ds:lang text="pisma"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doDelete" id="doDelete"/>

<table cellspacing="0">
<tr>
	<th></th>
	<th><ds:lang text="Sposob"/> <ww:property value="#sposob"/> <ds:lang text="pisma"/></th>
	<th></th>
</tr>
<ww:iterator value="deliveries">
	<tr>
		<td><ww:checkbox name="'deliveryIds'" fieldValue="id" disabled="!canDelete()" /></td>
		<td><ww:property value="name"/> <%--, <ww:property value="id"/>, canBeDeleted: <ww:property value="canBeDeleted()"/>, canDelete: <ww:property value="canDelete()"/> --></td>
		<td>[<a href="<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery-edit.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
	</tr>
</ww:iterator>
<tr>
	<td></td>
	<td colspan="2">
		<input type="button" value="<ds:lang text="NowySposob"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/' + documentType + '-office-document-delivery-edit.action'"/>';"/>
		<input type="submit" value="<ds:lang text="Usun"/>" class="btn"
		onclick="if (!confirm(getText('NaPewnoUsunac'))) return false; document.getElementById('doDelete').value='true'"/>
	</td>
</tr>
</table>

</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
--%>
<!--N koniec office-document-delivery.jsp N-->