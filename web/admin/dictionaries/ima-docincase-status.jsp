
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="StatusyDokumentowWSprawie"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/ima-docincase-status.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="StatusyDokumentowWSprawie"/>
			</th>
			
			<th></th>
		</tr>
		
		<ww:iterator value="kinds">
			<tr>
				<td>
					<ww:checkbox name="'kindIds'" fieldValue="id" disabled="!canDelete" />
				</td>
				<td>
					<ww:property value="title"/>
				</td>
				
				<td>
					[
					<a href="<ww:url value="'/admin/dictionaries/ima-docincase-status-edit.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>
					]
				</td>
			</tr>
		</ww:iterator>
	</table>

	<input type="button" value="<ds:lang text="NowyStatus"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/ima-docincase-status-edit.action'"/>';"/>
	<input type="submit" value="<ds:lang text="Usun"/>" class="btn"
		onclick="if (!(confirm('<ds:lang text="NaPewnoUsunac"/>'))) return false; document.getElementById('doDelete').value='true'"/>
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

