<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>S�owniki zewn�trzne</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/external-dictionaries.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">

<ww:if test="dbConnectionsList != null && tableName == null">
	<table cellspacing="0">
		<tr>
			<td>Wybierz baz� danych:</td>
			<td></td>
			<td><ww:select name="'choosenDBconnection'" id="dbConnectionSelect" list="dbConnectionsList" cssClass="'sel '" cssStyle="width: 202px;"/></td>
		</tr>
	</table>
	<ds:submit-event value="getText('Utw�rz nowy s�ownik')" name="'doCreateExternalDictionary'" cssClass="'btn searchBtn'"/>
	<ds:submit-event value="getText('Modyfikuj typ dokumentu')" name="'doModifyDockind'" cssClass="'btn searchBtn'"/>
</ww:if>				
<ww:elseif test="actionType == 'CREATE' && choosenDBconnection != null">
	<table cellspacing="0">
		<tr>
			<td>Nazwa szukanej tabeli:</td>
			<td></td>
		    <td><ww:textfield id="tableName" name="'tableName'" size="40" maxlength="100" cssStyle="width: 200px;"/></td>
		</tr>
	</table>
	<ds:submit-event value="getText('Dalej')" name="'doCreateExternalDictionary'" cssClass="'btn searchBtn'" validate="validateTableName()"/>
</ww:elseif>	
<ww:elseif test="actionType == 'CREATE' && tableName != null && dictionaryName == null">
<%-- z jakiego� powodu aby lista selectedColumnsIds zosta�a zainicjalizowana potrzeba do niej przekaza� co najmniej dwie warto�ci st�d pierwsz� inicjalizujemy na sztywno --%>
	<input type="hidden" name="selectedColumnsIds" id="selectedColumnsIds" value="0"/>
	<input type="hidden" name="tableName" id="tableName" value="<ww:property value="tableName"/>"/>
	<table cellspacing="0">
		<tr>
			<td colspan="3">Nazwa dla definiowanego s�ownika:</td>
			<td colspan="2"><ww:textfield name="'dictionaryName'" id="dictionaryName"/></td>
		</tr>
		<tr>			
		    <th colspan="1">Wybierz</th>
		    <th colspan="2">Oryginalna nazwa</th>
		    <th colspan="2">Nazwa u�ytkownika</th>
		</tr>
		<ww:iterator value="columnIdxToName" status="rowstatus">
		    <tr>
		    	<td colspan="1"><ww:checkbox name="'selectedColumnsIds'" id="selectedColumnsIds" value="false" fieldValue="key"/></td>
		        <td colspan="2"><label><ww:property value="value"/></label></td>
		        <td colspan="2"><input type="text" name="columnIdxToName[<ww:property value="key"/>]" <ww:if test="value == primaryKeyColumn || value.toLowerCase() == 'id'">readonly value="<ww:property value="primaryKeyColumn"/>"</ww:if> class="txt" style="width: 200px;"></td>
		    </tr>
		    <ww:label />
		</ww:iterator>
	</table>
	<ds:submit-event value="getText('Dalej')" name="'doCreateExternalDictionary'" cssClass="'btn searchBtn'" validate="validateExternalDictionaryContent()"/>
</ww:elseif>
<ww:elseif test="actionType == 'MODIFY' && availableExternalDictionaries != null">
	<table cellspacing="0">
	<tr>
		<td>Dost�pne s�owniki zewn�trzne:</td> 
		<td><ww:select name="'choosenExternalDictionary'" id="dictionarySelect" list="availableExternalDictionaries" cssClass="'sel '" cssStyle="width: 200px;"/></td>
	</tr>
		<td>Dost�pne typy dokument�w:</td> 
		<td><ww:select name="'choosenDockind'" id="dockindSelect" list="availableDockinds" cssClass="'sel '" cssStyle="width: 200px;"/></td>
	</tr>
	</table>
	<ds:submit-event value="getText('Potwierd�')" name="'doModifyDockind'" cssClass="'btn searchBtn'"/>
</ww:elseif>
<input type="button" value="Powr�t do listy s�ownik�w" class="btn" onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
</form>

<script type="text/javascript">
    function checkFolderSize()
    {
         /* zwiekszam wielkosc pola folder jesli konieczne */
        var element = document.getElementById('folderPath');
        if ((element.value != null) && (element.value != undefined) && (parseInt(element.value.length) > parseInt(element.getAttribute('size'))))
        {
            element.setAttribute('size', element.value.length);
        }
    }

/*     function validateSelection()
    {
		if(document.getElementById('dbConnectionSelect').value == ''){
			alert("Prosz� wybra� baz� danych");
			return false;
		}else{
			return true;
		}
    } */
    
    function validateTableName()
    {
		if(document.getElementById('tableName').value == ''){
			alert("Prosz� wprowadzi� nazw� tabeli");
			return false;
		}else{
			return true;
		}
    }
   
    function validateExternalDictionaryContent()
    {
    	
    	var checkboxArray = document.getElementsByName('selectedColumnsIds');
    	var emptyBoxes = true;
    	for(i = 0; i < checkboxArray.length; i++){
    		if(checkboxArray[i].checked == true){
    			emptyBoxes = false;
    		}
    	}
    	if(emptyBoxes){
    		alert("Prosz� wybra� co najmniej jedn� kolumn�");
			return false;
		}
    	
		if(document.getElementById('dictionaryName').value == ''){
			alert("Prosz� wprowadzi� nazw� dla s�ownika zewn�trznego");
			return false;
		}else{
			return true;
		}
    }
</script>








