<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N invoices-kind.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>�r�d�a finansowania - tematy</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/zrodla-finansowania.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doDelete" id="doDelete"/>

<table cellspacing="0">
<tr>
    <th></th>
    <th>Pozycja planu</th>
    <th>Kierownik</th>
    <th>Opiekun</th>
    <th></th>
</tr>
<ww:iterator value="topics">
    <tr>
    	<td>
    		<ww:checkbox name="'topicIds'" fieldValue="id" />
    	</td>
        <td>
        	<ww:property value="pozycja_planu"/>
        </td>
        <td>
        	<ww:property value="manager"/>
        </td>
         <td>
        	<ww:property value="supervisor"/>
        </td>
        <!-- td>[
        	<a href="<ww:url value="'/admin/dictionaries/topic-founding-source-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">edycja</a>
        	]
        </td-->
    </tr>
</ww:iterator>
<!-- tr>
    <td></td>
    <td colspan="2">
        <input type="button" value="Nowy temat" class="btn"
        onclick="document.location.href='<ww:url value="'/admin/dictionaries/topic-founding-source-edit.action'"/>';"/>
        <input type="submit" value="Usu�" class="btn"
        onclick="if (!(confirm('Na pewno usun��?'))) return false; document.getElementById('doDelete').value='true'"/>
    </td>
    <td></td>
</tr--> 
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>