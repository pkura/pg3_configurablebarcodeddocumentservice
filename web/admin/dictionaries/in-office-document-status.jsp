<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N in-office-document-status.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="StatusyPismPrzychodzacych"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/in-office-document-status.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doDelete" id="doDelete"/>
	
	<table class="tableMargin">
		<tr>
			<th></th>
			<th class="alignCenter">
				<ds:lang text="StatusPisma"/>
			</th>
			<th class="alignCenter">
				<ds:lang text="Kod"/>
			</th>
			<th></th>
		</tr>
		<ww:iterator value="statuses">
			<tr>
				<td>
					<ww:checkbox name="'statusIds'" fieldValue="id" disabled="!canDelete"/>
				</td>
				<td>
					<ww:property value="name"/>
				</td>
				<td>
					<ww:property value="cn"/>
				</td>
				<td>
					[
					<a href="<ww:url value="'/admin/dictionaries/in-office-document-status-edit.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>
					]
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<input type="button" value="<ds:lang text="NowyStatus"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/in-office-document-status-edit.action'"/>';"/>
	<input type="submit" value="<ds:lang text="Usun"/>" class="btn"
		onclick="if (!confirm('Na pewno usun��?')) return false; document.getElementById('doDelete').value='true'"/>
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1><ds:lang text="StatusyPismPrzychodzacych"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/in-office-document-status.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doDelete" id="doDelete"/>

<table cellspacing="0">
<tr>
	<th></th>
	<th><ds:lang text="StatusPisma"/></th>
	<th><ds:lang text="Kod"/></th>
	<th></th>
</tr>
<ww:iterator value="statuses">
	<tr>
		<td><ww:checkbox name="'statusIds'" fieldValue="id" disabled="!canDelete()" /></td>
		<td><ww:property value="name"/> <%--, <ww:property value="id"/>, canBeDeleted: <ww:property value="canBeDeleted()"/>, canDelete: <ww:property value="canDelete()"/> --></td>
		<td><ww:property value="cn"/></td>
		<td>[<a href="<ww:url value="'/admin/dictionaries/in-office-document-status-edit.action'"><ww:param name="'id'" value="id"/></ww:url>"><ds:lang text="edycja"/></a>]</td>
	</tr>
</ww:iterator>
<tr>
	<td></td>
	<td colspan="2">
		<input type="button" value="<ds:lang text="NowyStatus"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/in-office-document-status-edit.action'"/>';"/>
		<input type="submit" value="<ds:lang text="Usun"/>" class="btn"
		onclick="if (!confirm('Na pewno usun��?')) return false; document.getElementById('doDelete').value='true'"/>
	</td>
</tr>
</table>

</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
--%>
<!--N koniec in-office-document-status.jsp N-->