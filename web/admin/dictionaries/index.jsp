<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Slowniki"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<ds:available test="dictionares.RodzajePismWchodzacych">
<p>
	<a href="<ww:url value="'/admin/dictionaries/in-office-document-kinds.action'"/>">
		<ds:lang text="RodzajePismWchodzacych"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>
<ds:modules test="invoices">
	<p>
		<a href="<ww:url value="'/admin/dictionaries/invoices-kind.action'"/>">
			<ds:lang text="RodzajeFaktur"/>
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	</p>
</ds:modules>

<ds:available test="dictionares.StatusyPismWchodzacych">
<p>
	<a href="<ww:url value="'/admin/dictionaries/in-office-document-status.action'"/>">
		<ds:lang text="StatusyPismWchodzacych"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>
<ds:available test="dictionares.SposobyDostarczeniaPisma">
<p>
	<a href="<ww:url value="'/admin/dictionaries/in-office-document-delivery.action'"/>">
		<ds:lang text="SposobyDostarczeniaPisma"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>
<ds:available test="dictionares.SposobyOdbioruPisma">
<p>
	<a href="<ww:url value="'/admin/dictionaries/out-office-document-delivery.action'"/>">
		<ds:lang text="SposobyOdbioruPisma"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>
<ds:modules test="invoices">
	<p>
		<a href="<ww:url value="'/admin/dictionaries/invoices-cpv-codes.action'"/>">
			<ds:lang text="NumeryKodowCpv"/>
			<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	</p>
	<p>
	<a href="<ww:url value="'/admin/dictionaries/invoices-decretation.action'"/>">
		<ds:lang text="DekretacjaRachunkowa"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	</p>
</ds:modules>

<ds:available test="dictionares.Ima">
<p>
	<a href="<ww:url value="'/admin/dictionaries/ima-case-status.action'"/>">
		<ds:lang text="ImaCaseStatus"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>

<p>
	<a href="<ww:url value="'/admin/dictionaries/ima-case-type.action'"/>">
		<ds:lang text="ImaCaseType"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
<p>
	<a href="<ww:url value="'/admin/dictionaries/ima-docincase-status.action'"/>">
		<ds:lang text="StatusyDokumentowWSprawie"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
<p>
	<a href="<ww:url value="'/admin/dictionaries/ima-docincase-type.action'"/>">
		<ds:lang text="TypyDokumentowWSprawie"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:available test="ifpan.projects">
<p>
	<a href="<ww:url value="'/admin/dictionaries/cost-kind.action'"/>">
		<ds:lang text="Projekt: Rodzaje koszt�w"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:available test="dictionaries.SlownikiZewnetrzne">
<p>
	<a href="<ww:url value="'/admin/dictionaries/external-dictionaries.action'"/>">
		<ds:lang text="S�owniki zewn�trzne"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:available test="pig">
<p>
	<a href="<ww:url value="'/admin/dictionaries/topic-founding-source.action'"/>">
		<ds:lang text="�r�d�a finansowania - tematy"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:available test="dictionares.Uwagi">
<p>
	<a href="<ww:url value="'/admin/dictionaries/adnotations.action'"/>">
		<ds:lang text="Uwagi"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>

<ds:modules test="office">
	<ds:available test="dictionares.office.CeleDekretacji">
    <p>
    	<a href="<ww:url value="'/admin/dictionaries/assignment-objectives.action'"/>">
    		<ds:lang text="CeleDekretacji"/>
    		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
    </p>
    </ds:available>
</ds:modules>

<ds:available test="dockind.dictionaries.edit">
	 <p>
	 	<a href="<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"/>">
	 		<ds:lang text="SlownikiDlaDokumentowDocuSafe"/>
	 		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
    </p>
    <ds:available test="!dictionaries.system.disabled">
	<p>
		<a href="<ww:url value="'/admin/dictionaries/system-dictionaries.action'"/>">
	 		<ds:lang text="SlownikiSystemuDocusafe"/>
	 		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
	</p>
	</ds:available>
</ds:available>

<ds:extras test="!business">
    <ds:modules test="office or simpleoffice">
        <p>
        	<a href="<ww:url value="'/admin/dictionaries/case-priority.action'"/>">
        		<ds:lang text="PriorytetySpraw"/>
        		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
        </p>

        <p>
        	<a href="<ww:url value="'/admin/dictionaries/out-office-document-return-reasons.action'"/>">
        		<ds:lang text="PrzyczynyzZwrotuPisma"/>
        		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
        </p>
    </ds:modules>
</ds:extras>
<ds:available test="dictionary.import.sender.recipient">
<p>
	<a href="<ww:url value="'/admin/dictionaries/import-address-dictionary.action'"/>">
		<ds:lang text="ImportOdbiorcowNadawcow"/>
		<img src="<ww:url value="'/img/strzalka-prawo.gif'"/>" width="11" height="11" border="0"/></a>
</p>
</ds:available>
<!--N koniec index.jsp N-->