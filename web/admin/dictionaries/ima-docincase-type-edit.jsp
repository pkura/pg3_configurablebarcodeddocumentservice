<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="EdycjaTypuDokumentuWSprawie"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/ima-docincase-type-edit.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<ww:hidden name="'id'"/>

	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="Tytul"/>:
			</td>
			<td>
				<ww:textfield name="'title'" size="40" maxlength="40" cssClass="'txt'" value="status.title"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Kod"/>:
			</td>
			<td>
				<ww:textfield name="'cn'" size="20" maxlength="20" cssClass="'txt'" value="status.cn"/>
			</td>
		</tr>
	</table>
	
	<input type="submit" value="<ds:lang text="Zapisz"/>" class="btn saveBtn"
		onclick="document.getElementById('doUpdate').value='true';"/>
	<input type="button" value="<ds:lang text="Anuluj"/>" class="btn cancelBtn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/ima-docincase-type.action'"/>';"/>
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>