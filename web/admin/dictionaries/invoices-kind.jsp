<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N invoices-kind.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Rodzaje dokument�w faktury</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/invoices-kind.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doDelete" id="doDelete"/>

<table cellspacing="0">
<tr>
    <th></th>
    <th>Nazwa rodzaju</th>
    <th>Dni na za�atwienie</th>
    <th></th>
</tr>
<ww:iterator value="kinds">
    <tr>
    	<td><ww:checkbox name="'kindIds'" fieldValue="id" disabled="!canDelete" /></td>
        <td><ww:property value="name"/></td>
        <td><ww:property value="days"/></td>
        <td>[<a href="<ww:url value="'/admin/dictionaries/invoices-kind-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">edycja</a>]</td>
    </tr>
</ww:iterator>
<tr>
    <td></td>
    <td colspan="2">
        <input type="button" value="Nowy rodzaj" class="btn"
        onclick="document.location.href='<ww:url value="'/admin/dictionaries/invoices-kind-edit.action'"/>';"/>
        <input type="submit" value="Usu�" class="btn"
        onclick="if (!(confirm('Na pewno usun��?'))) return false; document.getElementById('doDelete').value='true'"/>
    </td>
    <td></td>
</tr>
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>