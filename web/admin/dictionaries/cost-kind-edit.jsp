<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N invoices-kind-edit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Edycja rodzaj�w koszt�w</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/cost-kind-edit.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doUpdate" id="doUpdate"/>
<ww:hidden name="'id'"/>

<table cellspacing="0">
<tr>
    <td>Nazwa:</td>
    <td><ww:textfield name="'title'" size="40" maxlength="40" cssClass="'txt'" value="kind.title"/></td>
</tr>
<tr>
    <td>Kategoria:</td>
    <td><ww:textfield name="'refValue'" size="40" maxlength="40" cssClass="'txt'" value="kind.refValue"/></td>
</tr>
<tr>
    <td>CN:</td>
    <td><ww:textfield name="'cn'" size="40" maxlength="40" cssClass="'txt'" value="kind.cn"/></td>
</tr>
<tr>
    <td></td>
    <td><ds:submit-event name="'doUpdate'" value="'Zapisz'"/>
        <input type="button" value="Anuluj" class="btn"
        onclick="document.location.href='<ww:url value="'/admin/dictionaries/cost-kind.action'"/>';"/>
    </td>
</tr>
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
