<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N import-address-dictionary.jsp N-->

<%@ page import="pl.compan.docusafe.util.DateUtils"%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="ImportOdbiorcowNadawcow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form id="form" action="<ww:url value="'/admin/dictionaries/import-address-dictionary.action'"/>" method="post" enctype="multipart/form-data">
	<ds:lang text="PlikZOsobami"/>:
	<ww:file name="'file'" id="file" cssClass="'txt'" size="50"/>
	
	<br/>

	<ds:submit-event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'" disabled="false"/>

	<br/>
	<ds:lang text="ImportowanyPlikMusiBycWformacieCSVPoszczegolnePolaPowinnyBycRozdzieloneSrednikami"/>
	<br/><br/>
	<b><ds:lang text="BudowaPlikuCSVImportuNadawcow"/></b>
	<br/><br/>
	<ds:lang text="WyborSlownika"/>;
	<ds:lang text="Tytul"/>;
	<ds:lang text="Imie"/>;
	<ds:lang text="Nazwisko"/>;
	<ds:lang text="FirmaUrzad"/>;
	<ds:lang text="Dzial"/>;
	<ds:lang text="AdresUlica"/>;
	<ds:lang text="Kod"/>;
	<ds:lang text="Miejscowosc"/>;
	<ds:lang text="Email"/>;
	<ds:lang text="Faks"/>;
	<ds:lang text="NIP"/>;
	<ds:lang text="PESEL"/>;
	<ds:lang text="REGON"/>;
	<ds:lang text="Grupa"/>
	<br></br>
	<ds:lang text="ignoreLineInfo"/>
</form>

<%--
<h1><ds:lang text="ImportOdbiorcowNadawcow"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />



<ds:ww-action-errors/>
<ds:ww-action-messages/>

<p></p>

<form id="form" action="<ww:url value="'/admin/dictionaries/import-address-dictionary.action'"/>" method="post" enctype="multipart/form-data">

	<table>
		<tr>
			<td><ds:lang text="PlikZOsobami"/>:</td>
			<td><ww:file name="'file'" id="file" cssClass="'txt'" size="50"/></td>
		</tr>
		<tr>
			<td></td>
			<td><ds:event name="'doLoadFile'" value="getText('Zaladuj')" cssClass="'btn'"/></td>
		</tr>
		<tr>
			<td></td>
			<td><ds:lang text="ImportowanyPlikMusiBycWformacieCSVPoszczegolnePolaPowinnyBycRozdzieloneSrednikami"/></td>
		</tr>
	</table>

</form>

</script>
--%>
<!--N koniec import-address-dictionary.jsp N-->