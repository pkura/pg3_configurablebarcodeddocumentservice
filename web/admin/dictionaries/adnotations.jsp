<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N adnotations.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="Uwagi"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/adnotations.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doDelete" id="doDelete"/>
	<input type="hidden" name="doCreate" id="doCreate"/>
	<input type="hidden" name="create" id="create"/>
	
	<ww:if test="!create">
		<table class="tableMargin">
			<tr>
				<th colspan="2">
					<ds:lang text="Uwagi"/>
				</th>
			</tr>
			
			<ww:iterator value="adnotations">
				<tr>
					<td>
						<ww:checkbox name="'priorityIds'" fieldValue="id"/>
					</td>
					<td>
						<ww:property value="name"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
		
		<input type="submit" value="<ds:lang text="NowaUwaga"/>" class="btn" onclick="changeView(true)"/>
		<ds:submit-event value="getText('doDelete')" name="'doDelete'" confirm="getText('doDelete')" />
	</ww:if>
	
	<ww:else>
		<table class="tableMargin">
			<tr>
				<th>
					<ds:lang text="NowaUwaga"/>
				</th>
			</tr>
			<tr>
				<td>
					<ww:textfield name="'newAdnotation'" size="80" maxlength="80" cssClass="'txt'" />
				</td>
			</tr>
		</table>			
		
		<input type="submit" value="<ds:lang text="Utworz"/>" class="btn"
			 onclick="document.getElementById('doCreate').value='true'"/>
		
		<br/>
		<input type="submit" value="<ds:lang text="PowrotDoListyUwag"/>" class="btn"
			onclick="changeView(false)"/>
		
	</ww:else>
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1><ds:lang text="Uwagi"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/adnotations.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
	
<input type="hidden" name="doDelete" id="doDelete"/>
<input type="hidden" name="doCreate" id="doCreate"/>
<input type="hidden" name="create" id="create"/>

<ww:if test="!create">
<table cellspacing="0" >
	<tr>
		<th><ds:lang text="Uwagi"/><br/></th>
		<th></th>
	</tr>
	
	<ww:iterator value="adnotations">
		<tr>
			<td><ww:checkbox name="'priorityIds'" fieldValue="id"/></td>
			<td><ww:property value="name"/></td>
		</tr>
	</ww:iterator>
	<tr>
		<td><br/></td><td/>
	</tr>
	
	
	<tr>
		<td>
			<ds:submit-event value="getText('doDelete')" name="'doDelete'" confirm="getText('doDelete')" />
		</td>
		<td></td>
	</tr>
	<tr>
		<td> 
			<input type="submit" value="<ds:lang text="NowaUwaga"/>" class="btn"
			onclick="changeView(true)"/>
		</td>
		<td/>
	</tr>
</table>
</ww:if>

<ww:else>
	<table cellspacing="0">
	<tr>
		<th><ds:lang text="NowaUwaga"/><br/></th>
	</tr>
	
	<tr>
		<td><ww:textfield name="'newAdnotation'" size="80" maxlength="80" cssClass="'txt'" /><br/></td>
	</tr>
	
	<tr>
		<td> 
			<input type="submit" value="<ds:lang text="PowrotDoListyUwag"/>" class="btn"
			onclick="changeView(false)"/>&nbsp&nbsp&nbsp&nbsp&nbsp
			<input type="submit" value="<ds:lang text="Utworz"/>" class="btn"
			 onclick="document.getElementById('doCreate').value='true'"/>
		</td>
	</tr>
</table>
</ww:else>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
</form>

<script type="text/javascript">
	function changeView(val)
	{
		document.getElementById('create').value=val; 
	}
</script>
--%>

<script type="text/javascript">
	function changeView(val)
	{
		$j("#create").val(val);
	}
</script>
<!--N koniec adnotations.jsp N-->