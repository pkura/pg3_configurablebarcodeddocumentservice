<%--T
	Przeróbka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N assignment-objectives.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="CeleDekretacji"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:form-event action="'/admin/dictionaries/assignment-objectives.action'" method="'post'">
	<table class="tableMargin">
		<tr>
			<th></th>
			<th>
				<ds:lang text="CelDekretacji"/>
			</th>
			<th></th>
		</tr>
		<ww:iterator value="objectives">
			<tr>
				<td>
					<ww:checkbox name="'objectiveIds'" fieldValue="id"/>
				</td>
				<td>
					<ww:property value="name"/>
				</td>
				<td>
					[
					<a href="<ww:url value="'/admin/dictionaries/assignment-objectives-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">
						edycja</a>
					]
				</td>
			</tr>
		</ww:iterator>
	</table>
	
	<ds:submit-event name="'doDelete'" value="getText('Usun')" confirm="getText('NaPewnoUsunac')"/>
	
	<br/>
	<input type="button" value="<ds:lang text="NowyRodzaj"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/assignment-objectives-edit.action'"/>';"/>
</ds:form-event>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1><ds:lang text="CeleDekretacji"/> </h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ds:form-event action="'/admin/dictionaries/assignment-objectives.action'" method="'post'">
<%--
<form action="<ww:url value="'/admin/dictionaries/assignment-objectives.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doDelete" id="doDelete"/>
-->

<table cellspacing="0">
<%--
<tr>
	<th colspan="2"></th>
	<th colspan="2" align="center">pismo przychodzące</th>
	<th colspan="2" align="center">pismo wychodzące</th>
</tr>
<tr>
	<th rowspan="2"></th>
	<th rowspan="2" valign="bottom">Cel dekretacji</th>
	<th>przyjęte w KO</th>
	<th>przyjęte w dziale</th>
	<th>w przygotowaniu</th>
	<th>czystopis</th>
</tr>
-->
<tr>
	<th></th>
	<th><ds:lang text="CelDekretacji"/></th>
	<th></th>
</tr>
<ww:iterator value="objectives">
	<tr>
		<td><ww:checkbox name="'objectiveIds'" fieldValue="id"/></td>
		<td><ww:property value="name"/></td>
		<td>[<a href="<ww:url value="'/admin/dictionaries/assignment-objectives-edit.action'"><ww:param name="'id'" value="id"/></ww:url>">edycja</a>]</td>
	</tr>
</ww:iterator>
<tr>
	<td></td>
	<td colspan="2">
		<input type="button" value="<ds:lang text="NowyRodzaj"/>" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/assignment-objectives-edit.action'"/>';"/>
		<ds:submit-event name="'doDelete'" value="getText('Usun')" confirm="getText('NaPewnoUsunac')"/>
	</td>
	<td></td>
</tr>
</table>

</ds:form-event>
<%--</form>-->

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
--%>
<!--N koniec assignment-objectives.jsp N-->