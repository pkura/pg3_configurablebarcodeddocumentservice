<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N case-priority-edit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Edycja priorytetu sprawy</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/case-priority-edit.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doUpdate" id="doUpdate"/>
<ww:hidden name="'id'"/>

<table cellspacing="0">
<tr>
    <td>Priorytet:</td>
    <td><ww:textfield name="'name'" size="40" maxlength="40" cssClass="'txt'" value="priority.name"/></td>
</tr>
<tr>
    <td></td>
    <td><input type="submit" value="Zapisz" class="btn"
        onclick="document.getElementById('doUpdate').value='true';"/>
        <input type="button" value="Anuluj" class="btn"
        onclick="document.location.href='<ww:url value="'/admin/dictionaries/case-priority.action'"/>';"/>
    </td>
</tr>
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
