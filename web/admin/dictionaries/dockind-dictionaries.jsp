<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N dockind-dictionaries.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rodzaje opis�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doDelete" id="doDelete"/>
	<input type="hidden" name="'docKind'" id="docKind" value="docKind"/>
	<ww:hidden name="'id'"/>

	<ww:select id="choosen" name="'choosen'" cssClass="'sel'" list="documentKindFields" />
	<ds:event name="'doChange'" value="getText('Wybierz')" cssClass="'btn'"/>
	<ds:event name="'doReload'" value="getText('Odswiez')" cssClass="'btn'"/>
	
	<ww:if test="choosen == null">
		<p>Wybierz rodzaj slownika</p>
	</ww:if>
	
	<ww:if test="choosen != null">
		<table class="tab tableMargin">
			<tr>
				<th></th>
				<th>
					CN opisu
				</th>
				<th>
					Rodzaj opisu
				</th>
				<th>
					Centrum
				</th>
				<th>
					Wartosc sterujaca
				</th>
				<th></th>
			</tr>
			<ww:iterator value="elements">
				<ww:if test="!available">
				<tr class="hiddenValue">
				</ww:if>
				<ww:else>
				<tr>
				</ww:else>
					<td>
						<ww:checkbox name="'descriptionIds'" fieldValue="id"/>
					</td>
					<td>
						<ww:property value="cn"/>
					</td>
					<td>
						<ww:property value="title"/>
					</td>
					<td class="alignCenter">
						<ww:property value="centrum"/>
					</td>
					<td class="alignCenter">
						<ww:property value="refValue"/>
					</td>
					<td>
						[
						<a href="<ww:url value="'/admin/dictionaries/dockind-dictionaries-edit.action'"><ww:param name="'id'" value="id"/><ww:param name="'choosen'" value="choosen"/><ww:param name="'docKind'" value="docKind"/></ww:url>">
							edycja</a>
						]
						[
						<a href="<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"><ww:param name="'id'" value="id"/><ww:param name="'toAvailable'" value="!available"/><ww:param name="'doChangeEntry'" value="true"/><ww:param name="'choosen'" value="choosen"/></ww:url>">
						<ww:if test="available">
							ukryj
						</ww:if><ww:else>
							poka�
						</ww:else></a>
						]
						[
						<a href="<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"><ww:param name="'id'" value="id"/><ww:param name="'toAvailable'" value="!available"/><ww:param name="'doDelete'" value="true"/><ww:param name="'choosen'" value="choosen"/></ww:url>">
                                                    usu�</a>
						]
					</td>
				</tr>
			</ww:iterator>
		</table>
		
		<input type="button" value="Nowy rodzaj" class="btn"
			onclick="document.location.href='<ww:url value="'/admin/dictionaries/dockind-dictionaries-edit.action'"><ww:param name="'choosen'" value="choosen"/><ww:param name="'docKind'" value="docKind"/></ww:url>';"/>

	</ww:if>
</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
<!--N koniec dockind-dictionaries.jsp N-->
