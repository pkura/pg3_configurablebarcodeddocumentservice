<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 29.08.08
	Poprzednia zmiana: 29.08.08
C--%>
<!--N in-office-document-kinds-edit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="EdycjaRodzajuPisma"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/in-office-document-kinds-edit.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<ww:hidden name="'id'"/>
	
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="Nazwa"/>:
			</td>
			<td>
				<ww:textfield name="'name'" size="40" maxlength="40" cssClass="'txt'" value="kind.name"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="LiczbaDniNaZalatwieniePisma"/>:
			</td>
			<td>
				<ww:textfield name="'days'" size="5" maxlength="5" cssClass="'txt'" value="kind.days"/>
			</td>
		</tr>
		
		<ww:if test="id != null">
			<tr>
				<td class="alignTop">
					<ds:lang text="WymaganeZalaczniki"/>:
				</td>
				<td>
					<ww:if test="kind.requiredAttachments.empty">
						<span class="italic"><ds:lang text="brak"/></span>
					</ww:if>
					
					<ww:else>
						<ww:iterator value="kind.requiredAttachments">
							<span class="continuousElement">
								<ww:checkbox name="'attachmentSids'" fieldValue="sid"/>
								<ww:property value="title"/>
							</span>
							<br/>
						</ww:iterator>
					</ww:else>
				</td>
			</tr>
			<tr>
				<td>
					<ds:lang text="NowyWymaganyZalacznik"/>:
				</td>
				<td>
					<ww:textfield name="'requiredAttachmentTitle'" size="30" maxlength="80" cssClass="'txt'"/>
					<ds:submit-event name="'doAddRequiredAttachment'" value="getText('Dodaj')"/>
				</td>
			</tr>
		</ww:if>
	</table>

	<ds:submit-event name="'doUpdate'" value="getText('Zapisz')" cssClass="'btn saveBtn'"/>
	<ds:submit-event name="'doDeleteRequiredAttachments'" value="getText('UsunZalaczniki')"/>
	<input type="button" value="<ds:lang text="Anuluj"/>" class="btn cancelBtn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/invoices-kind.action'"/>';"/>
	
</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1><ds:lang text="EdycjaRodzajuPisma"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/in-office-document-kinds-edit.action'"/>" method="post"
	onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<ww:hidden name="'id'"/>
	
	<table cellspacing="0">
		<tr>
			<td><ds:lang text="Nazwa"/>:</td>
			<td><ww:textfield name="'name'" size="40" maxlength="40" cssClass="'txt'" value="kind.name"/></td>
		</tr>
		<tr>
			<td><ds:lang text="LiczbaDniNaZalatwieniePisma"/>:</td>
			<td><ww:textfield name="'days'" size="5" maxlength="5" cssClass="'txt'" value="kind.days"/></td>
		</tr>
		<ww:if test="id != null">
			<tr>
				<td valign="top"><ds:lang text="WymaganeZalaczniki"/>:</td>
				<td>
					<ww:if test="kind.requiredAttachments.empty"><i><ds:lang text="brak"/></i></ww:if>
					<ww:else>
						<table cellspacing="0" cellpadding="0">
							<ww:iterator value="kind.requiredAttachments">
								<tr>
									<td><ww:checkbox name="'attachmentSids'" fieldValue="sid"/></td>
									<td><ww:property value="title"/></td>
								</tr>
							</ww:iterator>
						</table>
					</ww:else>
				</td>
			</tr>
			<tr>
				<td><ds:lang text="NowyWymaganyZalacznik"/>:</td>
				<td><ww:textfield name="'requiredAttachmentTitle'" size="30" maxlength="80" cssClass="'txt'"/>
					<ds:submit-event name="'doAddRequiredAttachment'" value="getText('Dodaj')"/>
				</td>
			</tr>
		</ww:if>
		<tr>
			<td></td>
			<td><ds:submit-event name="'doUpdate'" value="getText('Zapisz')"/>
				<ds:submit-event name="'doDeleteRequiredAttachments'" value="getText('UsunZalaczniki')"/>
				<input type="button" value="<ds:lang text="Anuluj"/>" class="btn"
				onclick="document.location.href='<ww:url value="'/admin/dictionaries/invoices-kind.action'"/>';"/>
			</td>
		</tr>
	</table>

</form>

<input type="button" value="<ds:lang text="PowrotDoListySlownikow"/>" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
--%>
<!--N koniec in-office-document-kinds-edit.jsp N-->