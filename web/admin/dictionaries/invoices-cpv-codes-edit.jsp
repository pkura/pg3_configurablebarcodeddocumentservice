<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N invoices-kind-edit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>

<h1>Edycja kod�w CPV</h1>
<hr size="1" align="left" class="highlightedText" <%--	color="#813 526"	--%> width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/invoices-cpv-codes-edit.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doUpdate" id="doUpdate"/>
<ww:hidden name="'id'"/>

<table cellspacing="0">
<tr>
    <td>Kod:</td>
    <td><ww:textfield name="'name'" size="40" maxlength="40" cssClass="'txt'" value="kind.name"/></td>
</tr>
<tr>
    <td>Kategoria:</td>
    <td><ww:textfield name="'category'" size="40" maxlength="40" cssClass="'txt'" value="kind.category"/></td>
</tr>
<tr>
    <td></td>
    <td><ds:submit-event name="'doUpdate'" value="'Zapisz'"/>
        <input type="button" value="Anuluj" class="btn"
        onclick="document.location.href='<ww:url value="'/admin/dictionaries/invoices-cpv-codes.action'"/>';"/>
    </td>
</tr>
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
