<%-- 
    Document   : wf-name; Slownik dla Nazw projekt�w dla dokument�w workflow
    Created on : 2011-06-16, 14:50:32
    Author     : �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
--%>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat, java.util.Date, org.apache.commons.beanutils.DynaBean, pl.compan.docusafe.util.DateUtils"%>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<p>
    <ds:ww-action-errors/>
    <ds:ww-action-messages/>
</p>

<form id="form" action="<ww:url value="requestBaseUrl"/>" method="post">
    <ww:hidden name="'id'" value="id" id="id" />
    <ww:hidden name="'param'" value="param" id="param" />
    
<ww:if test="results == null || results.empty">
    <table>
        <tbody>
            <tr>
                <td colspan="2"><ds:lang text="Nazwa projektu/produktu/ procedury/regulaminu"/>:*</td>
            </tr>
            <tr>
                <td colspan="2">
                    <ww:textfield name="'nazwa'" value="nazwa" id="nazwa" size="50" maxlength="150" onchange="'notifyChange()'" cssClass="'txt'"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><ds:lang text="Opis"/>:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <ww:textarea name="'opis'" value="opis" id="opis" onchange="'notifyChange()'" cssClass="'txt'" cols="47" rows="4"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><ds:lang text="DataRozpoczecia"/>:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <input name="dataRozpoczecia" size="10" maxlength="10" class="txt" id="dataRozpoczecia" onchange="'notifyChange()'"
                        value="<ds:format-date value="dataRozpoczecia" pattern="dd-MM-yyyy"/>"/>
             </td>
            </tr>
            <tr>
                
                <td colspan="2"><ds:lang text="Status"/>:*</td>
            </tr>
            <tr>
                <td colspan="2">
                    <ww:select name="'status'" list="statuses" onchange="'notifyChange()'"
                               id="status"
                               listKey="id" 
                               headerKey="''" 
                               headerValue="'----'"  
                               listValue="getTitle()" 
                               cssClass="'sel'" value="status"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><ds:lang text="Kierownik projektu technicznego"/>:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <ww:select name="'kierownikTechniczny'" list="users" onchange="'notifyChange()'"
                               id="kierownikTechniczny"
                               listKey="id" 
                               headerKey="''" 
                               headerValue="'----'"  
                               listValue="asFirstnameLastnameName()" 
                               cssClass="'sel'" value="kierownikTechniczny"/>
                </td>
            </tr>
            <tr>
                <td colspan="2"><ds:lang text="Kierownik projektu biznesowego"/>:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <ww:select name="'kierownikBiznesowy'" list="users" onchange="'notifyChange()'"
                               id="kierownikBiznesowy"
                               listKey="id" 
                               headerKey="''" 
                               headerValue="'----'"  
                               listValue="asFirstnameLastnameName()" 
                               cssClass="'sel'" value="kierownikBiznesowy"/>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <input type="submit" name="doSearch" value="<ds:lang text="Szukaj"/>" class="btn searchBtn"<ww:if test="!canRead">disabled="disabled"</ww:if>/>
                    <input type="submit" id="doAdd" name="doAdd" value="<ds:lang text="DodajDoSlownika"/>" onclick="if (!confirmAdd()) return false;" class="btn" <ww:if test="!canAdd || id!=null">disabled="disabled"</ww:if>/>
                    <input type="button" value="<ds:lang text="Wyczysc"/>" onclick="clearForm();" class="btn" />
                    <input type="button" value="<ds:lang text="Anuluj"/>" onclick="window.close();" class="btn cancelBtn"/>
                </td>
            </tr>
            <tr>
                <td>
                     <input type="button" id="doSubmit"  value="<ds:lang text="UmiescWformularzu"/>" onclick="if (!submitStrona()) return false; window.close();" class="btn" <ww:if test="id == null">disabled="disabled"</ww:if>/>
                     <input type="submit" name="doUpdate" value="<ds:lang text="ZapiszZmiany"/>"  class="btn" <ww:if test="id == null">disabled="disabled"</ww:if><ww:elseif test="!canModify">disabled="disabled"</ww:elseif>/>
                </td>
            </tr>
        </tfoot>
    </table>
</form>
</ww:if>
<ww:else>
    <table width="100%">
        <thead>
            <tr>
                <th><ds:lang text="Nazwa projektu/produktu/ procedury/regulaminu"/></th>
                <th><ds:lang text="DataRozpoczecia"/></th>
                <th><ds:lang text="Status"/></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <ww:iterator value="results">
                <tr>
                    <td title="<ww:property value="opis"/>"><ww:property value="nazwa"/></td>
                    <td><ds:format-date value="dataRozpoczecia" pattern="dd-MM-yyyy"/></td>
                    <td><ww:property value="statusName"/></td>
                    <td>
                        <a href="<ww:url value="requestBaseUrl">
                               <ww:param name="'id'" value="id"/>     
                               <ww:param name="'param'" value="param"/>     
                           </ww:url>"><ds:lang text="Wybierz"/></a>
                        <ww:if test="canDelete">
                            <a href="<ww:url value="requestBaseUrl">
                                   <ww:param name="'id'" value="id"/>
                                   <ww:param name="'param'" value="param"/>
                                   <ww:param name="'doDelete'" value="doDelete"/>
                               </ww:url>&doDelete=true"><ds:lang text="Usun"/></a>
                        </ww:if>
                    </td>
                </tr>
            </ww:iterator>
        </tbody>
    </table>
    <ww:set name="pager" scope="request" value="pager"/>
    <table width="70%">
        <tr><td align="center"><jsp:include page="/pager-links-include.jsp"/></td></tr>
        <tr><td><input type="button" class="btn" value="<ds:lang text="NoweWyszukiwanie"/>"
            onclick="document.location.href='<ww:url value="requestBaseUrl"><ww:param name="'param'" value="param"/></ww:url>';"/>
</td></tr>
    </table>
</ww:else>
<script type="text/javascript">
$j(function(){
    Calendar.setup({
            inputField:	"dataRozpoczecia",	 // id of the input field
            ifFormat  :	"<%= DateUtils.jsCalendarDateFormat %>",	  // format of the input field
            button    :	"dataRozpoczeciaTrigger",  // trigger for the calendar (button ID)
            align     :	"Tl",		   // alignment (defaults to "Bl")
            singleClick	: true
        });
});    
 function notifyChange()
{
    document.getElementById('doSubmit').disabled = 'disabled';
}

function submitStrona()
{
    if (!window.opener)
    {
        alert('<ds:lang text="NieZnalezionoGlownegoOkna"/>');
        return;
    }

    var strona = new Array();

    strona.id = $j('#id').val();
    strona.nazwa = $j('#nazwa').val();
    strona.opis = $j('#opis').val();
    strona.dataRozpoczecia = $j('#dataRozpoczecia').val();
    strona.status = $j('#status').val();
    strona.kierownikTechniczny = $j('#kierownikTechniczny').val();
    strona.kierownikBiznesowy = $j('#kierownikBiznesowy').val();

    if (isEmpty(strona.nazwa))
    {
        alert('<ds:lang text="NieWpisanoPodstawowychDanych"/>');
        return false;
    }

    if (window.opener.accept)
        window.opener.accept('WF_NAZWA', strona);
    else
    {
        alert('<ds:lang text="WoknieWywolujacymNieZnalezionoFunkcjiAkceptujacejWybraneDane"/>');
        return;
    }
    return true;
}

function clearForm(){
    $j('.txt').val(null);
    $j('.sel').val(null);
}
</script>