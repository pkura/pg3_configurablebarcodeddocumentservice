<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 01.09.08
	Poprzednia zmiana: 01.09.08
C--%>
<!--N dockind-dictionaries-edit.jsp N-->

<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Edycja rodzaj�w opisu</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/dockind-dictionaries-edit.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<input type="hidden" name="doUpdate" id="doUpdate"/>
	<ww:hidden name="'docKind'" value="docKind" id="docKind" />
	<ww:hidden name="'id'"/>
	<ww:hidden name="'choosen'"/>

	<table class="tableMargin">
		<tr>
			<td>
				Cn:
			</td>
			<td>
				<ww:textfield id="cn" name="'cn'" size="40" maxlength="100" cssClass="'txt'" value="cn"/>
			</td>
		</tr>
		<tr>
			<td>
				Rodzaj:
			</td>
			<td>
				<ww:textfield id="name" name="'name'" size="40" maxlength="100" cssClass="'txt'" value="name"/>
			</td>
		</tr>
		<tr>
			<td>
				Centrum:
			</td>
			<td>
				<ww:textfield id="'centrum'" name="'centrum'" size="40" maxlength="40" cssClass="'txt'" value="centrum"/>
			</td>
		</tr>
		<tr>
			<td>
				Wartosc sterujaca: 
			</td>
			<td>
				<ww:if test="refValueList.size > 0">
					<ww:select name="'refValue'" id="refValue" headerValue="refKey" headerKey="refValue" cssClass="'sel'" list="refValueList" listKey="id" listValue="title" />
				</ww:if>
				<ww:else>
					<ww:textfield name="'refValue'" size="50" maxlength="200" cssClass="'txt'" value="refValue"/>
				</ww:else>
			</td>
		</tr>
	</table>
	
	<input type="submit" value="Zapisz" class="btn"
		onclick="document.getElementById('doUpdate').value='true';"/>
	<input type="button" value="Anuluj" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"/>';"/>
</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
	onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>

<%--
<h1>Edycja rodzaj�w opisu</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dictionaries/dockind-dictionaries-edit.action'"/>" method="post"
onsubmit="disableFormSubmits(this);">
<input type="hidden" name="doUpdate" id="doUpdate"/>
<ww:hidden name="'id'"/>
<ww:hidden name="'choosen'"/>
<table cellspacing="0">
<tr>
	<td>Cn:</td>
	<td><ww:textfield name="'cn'" size="20" maxlength="20" cssClass="'txt'" value="cn"/></td>
</tr>
<tr>
	<td>Rodzaj:</td>
	<td><ww:textfield name="'name'" size="40" maxlength="40" cssClass="'txt'" value="name"/></td>
</tr>
<tr>
	<td>Centrum:</td>
	<td><ww:textfield name="'centrum'" size="40" maxlength="40" cssClass="'txt'" value="centrum"/></td>
</tr>
<tr>
	<td>Wartosc sterujaca:</td>
	<td>
	
	<ww:if test="refValueList.size > 0">
		<ww:select name="'refValue'" id="refValue" cssClass="'sel'" list="refValueList" listKey="id" listValue="title" />
	</ww:if>
	<ww:else>
		<ww:textfield name="'refValue'" size="20" maxlength="20" cssClass="'txt'" value="refValue"/>
	</ww:else>
	<!-- 
	
	 -->
	
	</td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="Zapisz" class="btn"
		onclick="document.getElementById('doUpdate').value='true';"/>
		<input type="button" value="Anuluj" class="btn"
		onclick="document.location.href='<ww:url value="'/admin/dictionaries/dockind-dictionaries.action'"/>';"/>
	</td>
</tr>
</table>

</form>

<input type="button" value="Powr�t do listy s�ownik�w" class="btn"
onclick="document.location.href='<ww:url value="'/admin/dictionaries/index.action'"/>';"/>
--%>
<!--N koniec dockind-dictionaries-edit.jsp N-->