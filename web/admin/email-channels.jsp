<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="java.text.SimpleDateFormat,
                 java.util.Date,
                 org.apache.commons.beanutils.DynaBean"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>


<h1><ds:lang text="KanalyEmail"/></h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p>
<ds:xmlLink path="EmailChannels"/>
</p>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<table>
<tr>
	<th>
		<ds:lang text="id"/>
	</th>
	<th>
		<ds:lang text="kolejka"/>
	</th>
	<th>
		<ds:lang text="protocol"/>
	</th>
	<th>
		<ds:lang text="host"/>
	</th>
	<th>
		<ds:lang text="username"/>
	</th>
	<th>
		<ds:lang text="folderName"/>
	</th>
	<th>
		<ds:lang text="port"/>
	</th>
	<th>
		<ds:lang text="mbox"/>
	</th>
	<th>
		<ds:lang text="ssl"/>
	</th>
	<th>
		<ds:lang text="wlaczony"/>
	</th>
	<th>
		<ds:lang text="contentType"/>
	</th>
	<th>
		<ds:lang text="ctime"/>
	</th>
	<th>
		<ds:lang text="userLogin"/>
	</th>
</tr>
<ww:iterator value="channels">
<tr>
	<td>
		<ww:property value="id"/>
	</td>
	<td>
		<a href="<ww:url value="'/admin/edit-email-channel.action'"/>?id=<ww:property value="id"/>&redirectUrl=<ww:property value="redirectUrl"/>"><ww:property value="kolejka"/></a>
	</td>
	
	<td>
		<ww:property value="protocol"/>
	</td>
	<td>
		<ww:property value="host"/>
	</td>
	<td>
		<ww:property value="user"/>
	</td>
	<td>
		<ww:property value="folderName"/>
	</td>
	<td>
		<ww:property value="port"/>
	</td>
	<td>
		<ww:property value="mbox"/>
	</td>
	<td>
		<ww:property value="SSL"/>
	</td>
	<td>
		<ww:property value="enabled"/>
	</td>
	<td>
		<ww:property value="contentType"/>
	</td>
	<td>
		<ww:property value="ctime"/>
	</td>
	<td>
		<ww:property value="userLogin"/>
	</td>
</tr>
</ww:iterator>
</table>