<%--T
	Przer�bka layoutu.
	Stan: TC_0_9
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 31.07.08
	Poprzednia zmiana: 31.07.08
C--%>
<!--N messenger.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Messenger</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/messenger.action'"/>" method="post">
	<ww:if test="!settingsOnView">
		<table class="tableMargin">
			<tr>
				<th>
					<ds:lang text="Lista"/> <ds:lang text="uzytkownikow"/>
				</th>
				<td>
				</td>
				<th>
					<ds:lang text="Adresaci"/> <ds:lang text="wiadomosci"/>
				</th>
			</tr>
			<tr>
				<td>
					<ww:select id="users" name="'user'" list="users" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
						listKey="name" multiple="true" size="10" listValue="asFirstnameLastnameName()" />
				</td>
				<td class="alignMiddle">
					<input type="button" class="btn" value="  >>  " onclick="addUser()"/>
					<br/>
					<input type="button" class="btn" value="  <<  " onclick="removeUser()"/>
				</td>
				<td>
					<ww:select name="'usernames'" id="usernames" list="usernames" multiple="true" size="10"
						cssClass="'multi_sel'" cssStyle="'width: 300px;'"/>
				</td>
			</tr>
		</table>
		
		<br/>
		<span class="marginLeft10">
			<ds:lang text="Tytul"/>
		</span>
		<br/>
		<ww:textfield name="'title'" id="message" size="40" maxlength="80" cssClass="'txt'"/>
		
		<br/>
		<span class="marginLeft10">
			<ds:lang text="Tresc"/>
		</span>
		<br/>
		<ww:textarea name="'message'" id="message" rows="4" cols="100" cssClass="'txt'"/>
		
		<br/>
		<input type="submit" class="btn" name="doSend" value="<ds:lang text="Wyslij"/>"
			onclick="selectAllOptions(document.getElementById('usernames'));"/>
		
		<br/>
		<br/>
		<br/>
		<a href="<ww:url value="'/admin/messenger.action'"/>?settingsOnView=true"><ds:lang text="Ustawienia"/></a>
	</ww:if>
	
</form>


<%--
<h1>Messenger</h1>
<hr size="1" align="left" class="horizontalLine" width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/messenger.action'"/>" method="post">
      
      <ww:if test="!settingsOnView">
        <table>
            <tr>
                <td>
                    <h3><ds:lang text="Lista"/><br/><ds:lang text="uzytkownikow"/></h3>
                    <ww:select id="users" name="'user'" list="users" cssClass="'multi_sel'" cssStyle="'width: 300px;'"
                               listKey="name" multiple="true" size="10"
                               listValue="asLastnameFirstname()" />
                    <br/><br/>
                </td>
                <td>
                    <input type="button" class="btn" value="  >>  " onclick="addUser()"/><br/><br/>
                    <input type="button" class="btn" value="  <<  " onclick="removeUser()"/>
                </td>
                <td>
                    <h3><ds:lang text="Adresaci"/><br/><ds:lang text="wiadomosci"/></h3>
                    <ww:select name="'usernames'" id="usernames" list="usernames"
                               multiple="true" size="10" cssClass="'multi_sel'" cssStyle="'width: 300px;'"/>
                    <br/><br/>
                </td>
            </tr>
            <tr>
                <td>&nbsp&nbsp <ds:lang text="Tytul"/></td>
            </tr>
            <tr>
                <td colspan="3"><ww:textfield name="'title'" id="message" size="40" maxlength="80" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <br/>
                <td colspan="3">&nbsp&nbsp <ds:lang text="Tresc"/></td>
            </tr>
            <tr>
                <td colspan="3"><ww:textarea name="'message'" id="message" rows="4" cols="100" cssClass="'txt'"/></td>
            </tr>
            <tr>
                <td colspan="3">
                    <input type="submit" class="btn" name="doSend" value="<ds:lang text="Wyslij"/>"
                           onclick="selectAllOptions(document.getElementById('usernames'));"/>
                    <br/><br/><br/>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <a href="<ww:url value="'/admin/messenger.action'"/>?settingsOnView=true"><ds:lang text="Ustawienia"/></a>
                </td>
            </tr>
        </table>
    </ww:if>
    <ww:else>
        <h3><ds:lang text="UstawieniaMessengera"/></h3>
        <table>
            <tr>
                <td>
                    <ww:textfield name="'port'" id="port" size="10" maxlength="10" cssClass="'txt'"/>
                </td>
                <td>
                    <ds:lang text="Port"/>
                </td>
            </tr>
            <tr>
                <br/>
                <td colspan="2">
                    <input type="submit" class="btn" name="doSave" value="<ds:lang text="Zapisz"/>"/>
                </td>
            </tr>
            <tr>
                <br/><br/><br/>
                <td colspan="2">
                    <a href="<ww:url value="'/admin/messenger.action'"/>?settingsOnView=false"><ds:lang text="WyslijWiadomosc"/></a>
                </td>
            </tr>
        </table>
    </ww:else>
</form>
--%>
<script type="text/javascript">
    function moveSelectedItems(tableIdFrom, tableIdTo)
    {
        var arr = new Array();
        var selectFrom = document.getElementById(tableIdFrom);
        var selectTo = document.getElementById(tableIdTo);
        
        for (var i=0; i < selectFrom.options.length; i++)
            if (selectFrom.options[i].selected)
            {
                selectTo.options.add(new Option(selectFrom.options[i].text, selectFrom.options[i].value));
                selectFrom.options[i] = null;
                i--;
            }
    }
    
    function addUser()
    {
        moveSelectedItems('users', 'usernames');
    }
    
    function removeUser()
    {
        moveSelectedItems('usernames', 'users');
    }
</script>

<!--N koniec messenger.jsp N-->