<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1"><ds:lang text="JackrabbitRepos"/></h1>
<hr class="fullLine horizontalLine"/>
<ds:ww-action-errors/>
<ds:ww-action-messages/>

<ww:if test="servers == null">
    <form id="modifyForm" action="<ww:url value="'/admin/jackrabbit-repos.action'"/>"
          method="post" enctype="application/x-www-form-urlencoded">
        <ww:hidden name="'id'"/>
        <fieldset class="container">
            <p>
                <label for="internalUrlPrefix">Wewnętrzny URL</label>
                <ww:textfield id="internalUrlPrefix" name="'internalUrlPrefix'"
                              cssClass="'txt dontFixWidth'"/>
            </p>
            <p>
                <label for="userUrlPrefix">URL dla użytkownika</label>
                <ww:textfield id="userUrlPrefix" name="'userUrlPrefix'"
                              cssClass="'txt dontFixWidth'"/>
            </p>
            <p>
                <label for="login">Login</label>
                <ww:textfield id="login" name="'login'"
                              cssClass="'txt dontFixWidth'"/>
            </p>
            <p>
                <label for="password">Hasło</label>
                <ww:textfield id="password" name="'password'"
                              cssClass="'txt dontFixWidth'"/>
            </p>
            <ww:if test="id != null">
                <p>
                    <label for="status">Status</label>
                    <ww:select id="status" name="'status'" list="statuses"/>
                </p>
            </ww:if>
            <p>
                <ww:if test="id == null">
                    <ds:submit-event value="'Dodaj'" name="'doAdd'" cssClass="'btn'"/>
                </ww:if><ww:else>
                    <ds:submit-event value="'Zapisz zmiany'" name="'doEdit'" cssClass="'btn'"/>
                    <ds:submit-event value="'Testuj połączenie'" name="'doTest'" cssClass="'btn'"/>
                </ww:else>
            </p>
        </fieldset>
        <script type="text/javascript">
            $j('#internalUrlPrefix').attr('placeholder', 'np. http://192.168.1.1:8081/rep01');
            $j('#userUrlPrefix').attr('placeholder', 'np. https://docusafe/rep01');
        </script>

    </form>
</ww:if><ww:else>
    <table>
        <tr>
            <th>Wewnętrzny URL</th>
            <th>URL dla użytkownika</th>
            <th>Status</th>
            <th></th>
        </tr>
        <ww:iterator value="servers">
            <tr>
                <td>
                    <ww:property value="internalUrlPrefix"/>
                </td>
                <td>
                    <ww:property value="userUrlPrefix"/>
                </td>
                <td>
                    <ww:property value="status.description"/>
                </td>
                <td>
                    <a href="<ww:url value="'/admin/jackrabbit-repos.action?id='+id"/>">Edytuj</a>
                </td>
            </tr>
        </ww:iterator>
        <tr>
            <td colspan="4">
                <a href="<ww:url value="'/admin/jackrabbit-repos.action?id=0'"/>">
                    Dodaj nowy serwer
                </a>
            </td>
        </tr>
    </table>
</ww:else>



