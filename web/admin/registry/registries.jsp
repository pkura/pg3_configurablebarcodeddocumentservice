<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 07.08.08
	Poprzednia zmiana: 07.08.08
C--%>
<!--N registries.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Rejestry</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/registries.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">
	<ww:if test="!registries.empty">
		<h4>Istniej�ce rejestry</h4>
	
		<table class="tableMargin">
			<tr>		
				<th>
					Nazwa kodowa
				</th>
				<th>
					Nazwa dla u�ytkownika(pe�na)
				</th>
				<th>
					Nazwa dla u�ytkownika(skr�cona)
				</th>
				<th>
					Tabela w bazie
				</th>
				<th>
					RWA
				</th>
			</tr>
			<ww:iterator value="registries">
				<tr>
					<td>
						<ww:property value="cn"/>
					</td>
					<td>
						<ww:property value="name"/>
					</td>
					<td>
						<ww:property value="shortName"/>
					</td>
					<td>
						<ww:property value="tablename"/>
					</td>
					<td>
						<ww:property value="requiredRwa"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>
	
	<h4>Nowy rejestr</h4>

	<table class="tableMargin">
		<tr>
			<td>
				Nazwa kodowa
			</td>
			<td>
				<ww:textfield name="'cn'" id="cn" size="30" cssClass="'txt'" maxlength="30"/>
			</td>		 
		</tr>
		<tr>
			<td>
				Plik XML
			</td>
			<td>
				<ww:file name="'file'" id="file" size="30" cssClass="'txt'"/>
			</td>
		</tr>
	</table>	

	<br/>
	<ds:event value="'Dodaj rejestr'" name="'doCreate'" cssClass="'btn'" confirm="'Na pewno doda� taki rejestr?'" onclick="'if (!validateForm()) return false'"/>
	
	<br/>
	<span class="warning">UWAGA: Dodanie rejestru o nazwie kodowej, dla kt�rej istnieje ju� inny rejestr, spowoduje nadpisanie tego istniej�cego rejestru.</span>
</form>



<%--
<h1>Rejestry</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/registries.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">

<ww:if test="!registries.empty">
	<h4>Istniej�ce rejestry</h4>

	<table>
	<tr>		
		<th>Nazwa kodowa</th>
		<th>Nazwa dla u�ytkownika(pe�na)</th>
		<th>Nazwa dla u�ytkownika(skr�cona)</th>
		<th>Tabela w bazie</th>
		<th>RWA</th>
	</tr>
	<ww:iterator value="registries">
		<tr>
			<td><ww:property value="cn"/></td>
			<td><ww:property value="name"/></td>
			<td><ww:property value="shortName"/></td>
			<td><ww:property value="tablename"/></td>
			<td><ww:property value="requiredRwa"/></td>
		</tr>
	</ww:iterator>
	</table>
</ww:if>

<h4>Nowy rejestr</h4>

<table>
	<tr>
		<td>Nazwa kodowa<!-- <span class="star">*</span>:--></td>
		<td><ww:textfield name="'cn'" id="cn" size="30" cssClass="'txt'" maxlength="30"/></td>		 
	</tr>
  <%--  <tr>
		<td>Nazwa dla u�ytkownika(pe�na)<!--<span class="star">*</span>:--></td>	
		<td><ww:textfield name="'name'" id="name" size="30" maxlength="100" cssClass="'txt'"/></td>		
	</tr>
	<tr>
		<td>Nazwa dla u�ytkownika(skr�cona)<!--<span class="star">*</span>:--></td>	
		<td><ww:textfield name="'shortName'" id="shortName" size="30" maxlength="30" cssClass="'txt'"/></td>		
	</tr>
	<tr>
		<td>Tabela w bazie<!--<span class="star">*</span>:--></td>
		<td><ww:textfield name="'tableName'" id="tableName" size="30" maxlength="100" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td>Wymagane RWA<!--<span class="star">*</span>:--></td>
		<td><ww:textfield name="'requiredRwa'" id="requiredRwa" size="30" maxlength="5" cssClass="'txt'"/></td>
	</tr> -->
	<tr>
		<td>Plik XML<!--<span class="star">*</span>:--></td>
		<td><ww:file name="'file'" id="file" size="30" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td></td>
		<td><ds:event value="'Dodaj rejestr'" name="'doCreate'" cssClass="'btn'" confirm="'Na pewno doda� taki rejestr?'" onclick="'if (!validateForm()) return false'"/></td>
	</tr>
	<tr>
		<td><span style="color:red">UWAGA:</span></td>
		<td><span style="color:red">Dodanie rejestru o nazwie kodowej, dla kt�rej istnieje ju� inny rejestr, 
		spowoduje nadpisanie tego istniej�cego rejestru.</span></td>
	</tr>
</table>

</form>
--%>
<script type="text/javascript">
	function validateForm()
	{
		var file = document.getElementById('file').value;
		var cn = document.getElementById('cn').value;
	<%--	var name = document.getElementById('name').value;
		var shortName = document.getElementById('shortName').value;	
		var tableName = document.getElementById('tableName').value;	--%>
		
		if (file == null || file.trim().length == 0) 
		{ 
			alert('Nie podano pliku XML'); 
			return false; 
		}
		if (cn == null || cn.trim().length == 0) 
		{ 
			alert('Nie podano nazwy kodowej'); 
			return false; 
		}
	<%--	if (name == null || name.trim().length == 0) 
		{ 
			alert('Nie podano pe�nej nazwy dla u�ytkownika'); 
			return false; 
		}
		if (shortName == null || shortName.trim().length == 0) 
		{ 
			alert('Nie podano skr�conej nazwy dla u�ytkownika'); 
			return false; 
		}
		if (tableName == null || tableName.trim().length == 0) 
		{ 
			alert('Nie podano tabeli w bazie'); 
			return false; 
		}	--%>
		return true;		
	}
</script>
<!--N koniec registries.jsp N-->