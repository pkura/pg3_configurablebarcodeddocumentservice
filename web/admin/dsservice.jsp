<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.util.DateUtils" %>

<form id="form" action="<ww:url value="'/admin/dsservice.action'"><ww:param name="'serviceId'" value="serviceId"/></ww:url>" 
	method="post" onsubmit="disableFormSubmits(this);">
	
	<ww:include page="/admin/dsservice-base.jsp"/>	
</form>