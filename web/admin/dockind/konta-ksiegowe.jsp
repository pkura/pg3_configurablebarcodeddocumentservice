<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N konta-ksiegowe.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Konta ksi�gowe</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/konta-ksiegowe.action'"/>" method="post">
	<ww:if test="konta != null && konta.size() > 0">
		<table class="tableMargin">
			<tr>
				<th>
					Id
				</th>		
				<th>
					Nazwa
				</th>
				<th>
					Numer
				</th>
				<ds:dockinds test="invoice_ic"><th style="width : 100px">
				    Omijanie akceptacji dyrektora finansowego
				</th></ds:dockinds>
				<th></th>
			</tr>
			<ww:iterator value="konta">
				<ww:if test="isVisibleInSystem">
					<tr>
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="id"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="name"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="account_number"/></a>
						</td>
                        <ds:dockinds test="invoice_ic"><td style="text-align:center">
                            <ww:if test="ic3098Flag">X</ww:if>
                        </td></ds:dockinds>
						<td>
							<a href="<ww:url value="deleteLink"/>">[usu�]</a>
							<a href="<ww:url value="hideLink"/>">[ukryj]</a>
						</td>
					</tr>
				</ww:if><ww:else>
					<tr class="hiddenValue">
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="id"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="name"/></a>
						</td>
						<td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action?doView=true&id='+id"/>">
							<ww:property value="account_number"/></a>
						</td>
						<ds:dockinds test="invoice_ic"><td style="text-align:center">
                            <ww:if test="ic3098Flag">X</ww:if>
                        </td></ds:dockinds>
						<td>
							<a href="<ww:url value="deleteLink"/>">[usu�]</a>
							<a href="<ww:url value="hideLink"/>">[poka�]</a>
						</td>
					</tr>
				</ww:else>
			</ww:iterator>
		</table>
	</ww:if>

	<h3>Nowe konto ksi�gowe</h3>
	<table class="tableMargin">
		<tr>
			<td>
				Opis:
			</td>
			<td>
				<ww:textfield name="'name'" value="name" id="name" size="40"/>
			</td>
		</tr>
		<tr>
			<td>
				Numer:
			</td>
			<td>
				<ww:textfield name="'number'" value="number" id="number" size="40"/>
			</td>
		</tr>
		<tr>
			<td>
				Wymaga opisu
			</td>
			<td>
				<ww:checkbox name="'needdescription'" value="needdescription" fieldValue="true" id="needdescription"></ww:checkbox>
			</td>
		</tr>
	</table>
	
	<ds:event name="'doAdd'" value="'Dodaj'" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
</form>
<script type="text/javascript">
	function validate()
	{
		var name = document.getElementById('name').value;
		var account_number = document.getElementById('number').value;
		if (account_number == null || account_number.trim().length == 0) 
		{ 
			alert('Nie podano numeru'); 
			return false; 
		}
		if (name == null || name.trim().length == 0) 
		{ 
			alert('Nie podano nazwy'); 
			return false; 
		}
		if (isNaN(parseInt(account_number))) {
			alert('Numer konta nie jest numerem');
			return false; 
		} 
		return true;
	}
</script>
<!--N koniec konta-ksiegowe.jsp N-->