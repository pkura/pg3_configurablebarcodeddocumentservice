<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-konto-ksiegowe.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Konta Ksi�gowe</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h3>Edycja konta ksi�gowego</h3>

	
	<ww:set name="accountId" value="id"/>
<form action="<ww:url value="'/admin/konta-ksiegowe.action'"/>" method="post">    
    <ww:hidden name="'id'" value="id"/>
    <table>  
        <tr>
            <td>Id</td>
            <td><ww:property value="id"/></td>
        </tr>
        <tr>
            <td>Nazwa</td>
            <td><ww:textfield id="name" name="'name'" value="name" size="40"/></td>
        </tr>
        <tr>
            <td>Numer</td>
            <td><ww:textfield id="number" name="'number'" value="number" size="40"/></td>
        </tr>
        <tr>
            <td>Wymaga opisu</td>
            <td><ww:checkbox name="'needdescription'" value="needdescription" fieldValue="true" id="needdescription"></ww:checkbox></td>
        </tr>
		<ds:dockinds test="sad">
			<tr>
				<td>Faktury zakupowe</td>
				<td><ww:checkbox name="'sadVisible'" value="sadVisible" fieldValue="true" id="sadVisible"></ww:checkbox></td>
			</tr>
			<tr>
				<td>�rodki trwa�e</td>
				<td><ww:checkbox name="'fixedAssetsVisible'" value="fixedAssetsVisible" fieldValue="true" id="fixedAssetsVisible"></ww:checkbox></td>
			</tr>
			<tr>
				<td>Nie wymaga akceptacji Dyrektora finansowego</td>
				<td><ww:checkbox name="'omitDirAcceptance'" value="omitDirAcceptance" fieldValue="true" id="omitDirAcceptance"></ww:checkbox></td>
			</tr>
			<tr>
			    <td>Komentarze:</td>
			    <td>
			        <ww:textfield id="newComment" name="'newComment'" value="" size="40"/>
			    </td><td>
			        <ds:event name="'doAddComment'" value="'Dodaj komentarz'" cssClass="'btn'"/>
			    </td>
			</tr>
			<ww:if test="!comments.empty">
			     <ww:iterator value="comments">
			       <ww:if test="visible">
					<tr><td></td>
						<td>
			            <ww:property value="name"/>
			            </td><td>
			          	<a href="<ww:url value="'/admin/konta-ksiegowe.action'">
			             		<ww:param name="'doHideComment'" value="'true'" />
			             		<ww:param name="'idComment'" value="id" />
			             		<ww:param name="'id'" value="#accountId" />
			            </ww:url>">[ukryj]</a>
			            </td></tr>
					</ww:if>
					<ww:else>
					<tr class="hiddenValue"><td></td>
						<td> 
			            <ww:property value="name"/>
			            </td><td>
							<a href="<ww:url value="'/admin/konta-ksiegowe.action'">
			             		<ww:param name="'doShowComment'" value="'true'" />
			             		<ww:param name="'idComment'" value="id" />
			             		<ww:param name="'id'" value="#accountId" />
			            	</ww:url>">[poka�]</a>

			            </td>
						</tr>
					</ww:else>
			      </ww:iterator>
			</ww:if>
			
		</ds:dockinds>
      <tr>
            <td colspan="2">
                <ds:event name="'doUpdate'" value="'Zapisz'" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
            </td>
        </tr>
    </table>

</form>   

<script type="text/javascript">
    function validate()
    {
        var name = document.getElementById('name').value;
        var account_number = document.getElementById('number').value;
        if (account_number == null || account_number.trim().length == 0) 
        { 
            alert('Nie podano numeru'); 
            return false; 
        }
        if (name == null || name.trim().length == 0) 
        { 
            alert('Nie podano nazwy'); 
            return false; 
        }
        if (isNaN(parseInt(account_number))) {
        	alert('Numer konta nie jest numerem');
            return false; 
        } 
        return true;
    }
</script>