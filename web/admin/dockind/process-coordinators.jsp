<%--T
	Przeróbka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N process-coordinators.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="KoordynatorzyProcesow"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/process-coordinators.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ds:lang text="RodzajDokumentuDocusafe"/>:
	<ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="cn" listValue="name"
		value="documentKindCn" cssClass="'sel'" onchange="'changeDockind();'"/>
		
	<br/>
	<br/>
	<table class="tableMargin">		
		<tr>
			<th>
				<ds:lang text="NazwaKanalu"/>
			</th>
			<th>
				<ds:lang text="Dzial"/>
			</th>
			<th>
				<ds:lang text="Uzytkownik"/>
			</th>
		</tr>		

		<ww:iterator value="channels">
			<input type="hidden" name="guids.<ww:property value="cn"/>" id="guids.<ww:property value="cn"/>" value="<ww:property value="guid"/>"/>
			<input type="hidden" name="user.<ww:property value="cn"/>" id="user.<ww:property value="cn"/>" value="<ww:property value="user"/>" />
			
			<tr>
				<td>
					<ww:property value="channelName"/>
				</td>
				<td id="<ww:property value="cn"/>">
					<input readonly="readonly"  type="text" name="guidName<ww:property value="cn"/>" id="guidName<ww:property value="cn"/>"
						value="<ww:property value="guidName"/>" size="30" class="txt"/>
				</td>
				<td id="u<ww:property value="cn"/>">
					<input readonly="readonly"  type="text" name="username<ww:property value="cn"/>" id="username<ww:property value="cn"/>"
						value="<ww:property value="user"/>" size="30" class="txt"/>
				</td>
				<td>
					<a name="<ww:property value="cn"/>" href="javascript:void(openAssignmentWindow('<ww:property value="guid"/>','<ww:property value="cn"/>'))">
						<ds:lang text="wybierz"/></a>
				</td>
			</tr>				
		</ww:iterator>
	</table>  

	<ds:event name="'doUpdate'" value="getText('Zapisz')" cssClass="'btn saveBtn'"/>
</form>

<%--
<h1><ds:lang text="KoordynatorzyProcesow"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/process-coordinators.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

	<table>
		<tr>
			<td>
				<ds:lang text="RodzajDokumentuDocusafe"/>:
			</td>
			<td>
				<ww:select id="documentKindCn" name="'documentKindCn'" list="documentKinds" listKey="cn" listValue="name" value="documentKindCn" cssClass="'sel'"
					onchange="'changeDockind();'"/>
			</td>
			
		</tr>
	</table>

	<table>		
		<tr>
			<th><ds:lang text="NazwaKanalu"/></th>
			<th><ds:lang text="Dzial"/></th>
			<th><ds:lang text="Uzytkownik"/></th>
		</tr>		
		<ww:iterator value="channels">
		<input type="hidden" name="guids.<ww:property value="cn"/>" id="guids.<ww:property value="cn"/>" value="<ww:property value="guid"/>"/>
		<input type="hidden" name="user.<ww:property value="cn"/>" id="user.<ww:property value="cn"/>" value="<ww:property value="user"/>" />
		
		<tr>
			<td>
				<ww:property value="channelName"/>
			</td>
			<td id="<ww:property value="cn"/>">
				<input readonly="readonly"  type="text" name="guidName<ww:property value="cn"/>" id="guidName<ww:property value="cn"/>" value="<ww:property value="guidName"/>" size="30" class="txt"/>
			</td>
			<td id="u<ww:property value="cn"/>">
				
				<input readonly="readonly"  type="text" name="username<ww:property value="cn"/>" id="username<ww:property value="cn"/>" value="<ww:property value="user"/>" size="30" class="txt"/>
			</td>
			<td>
				
				<td><a name="<ww:property value="cn"/>" href="javascript:void(openAssignmentWindow('<ww:property value="guid"/>','<ww:property value="cn"/>'))"><ds:lang text="wybierz"/></a></td>
			</td>
		</tr>				
		</ww:iterator>
		
	</table>  
	<br/>	
	<ds:event name="'doUpdate'" value="getText('Zapisz')"/>
	
</form>
--%>
<script type="text/javascript">
	/**
	 * zmienia aktualny rodzaj dokumentu - przeładowując stronę 
	 */
	function changeDockind()
	{
		document.forms[0].submit();
	}
	
	function changeField()
	{
		document.forms[0].submit();
	}

   	function openAssignmentWindow(divisionGuid,cn)
	{
		window.open('<ww:url value="'/admin/division-tree.action'"/>?divisionGuid='+divisionGuid+'&showGroups=true&cn='+cn, 'assignmentwindow', 'width=500,height=400,resizable=yes,scrollbars=yes,status=no,toolbar=no,menu=no');
	}
	
	function accept(param,cn,name,username)
		{
		var element = document.getElementById('guids.'+cn);
		element.value = param;
		var element2 = document.getElementById('guidName'+cn);
		element2.value = name;
		if(username != null)
		{
			var element3 = document.getElementById('username'+cn);
			element3.value = username;
			var element4 = document.getElementById('user.'+cn);
			element4.value = username;
		}
		else
		{
			var element3 = document.getElementById('username'+cn);
			element3.value = '';
			var element4 = document.getElementById('user.'+cn);
			element4.value = '';
		}
	}
</script>
<!--N process-coordinators.jsp N-->