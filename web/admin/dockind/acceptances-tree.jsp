<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="AcceptancesTree"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>


<form action="<ww:url value="'/absences/acceptances-tree.action'"/>" method="post">
	<ww:hidden name="'acceptanceDivisionId'" id="acceptanceDivisionId"/>
	<table>
		<tr>
		<td><ww:property value="treeHtml" escape="false"/></td>
		</tr>
		<tr>
		<td>
			<div>
			<table id="table_hide" style="display:none">
				<tr>
					<td><ds:lang text="Typ"/> : </td>
					<td><ww:select onchange="'showBox(this)'" name="'divisionType'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="divisionTypes"/></td>
				</tr>
				<tr id="tr_user" style="display:none">
					<td><ds:lang text="User"/> : </td>
					<td><ww:select name="'user'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="users" listKey="name" listValue="asLastnameFirstname()"/></td>
				</tr>
				<tr id="tr_guid" style="display:none">
					<td>Dzia� : </td>
					<td><ww:select name="'guid'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="divisions" listKey="guid" listValue="name"/></td>
				</tr>
				<tr id="tr_ck" style="display:none">
					<td>Centrum koszt�w : </td>
					<td><ww:select name="'idCK'" cssClass="'sel'" headerKey="''" headerValue="getText('select.wybierz')" list="centrumKosztow" listKey="id" listValue="name"/></td>
				</tr>
				<tr id="tr_kod" style="display:none">
					<td><ds:lang text="Kod"/> : </td>
					<td><ww:textfield name="'code'" value="code"/></td>
				</tr>
				<tr id="tr_name" style="display:none">
					<td><ds:lang text="Nazwa"/> : </td>
					<td><ww:textfield name="'name'" value="name"/></td>
				</tr>
				<tr id="tr_ogolna" style="display:none">
					<td><ds:lang text="Ogolna"/> : </td>
					<td><ww:select name="'ogolna'" cssClass="'sel'" list="#{ 1: 'TAK', 0: 'NIE' }"/></td>
				</tr>
				<tr>
					<td><ds:event name="'doAdd'" value="getText('Dodaj')"/></td>
					<td></td>
				</tr>
			</table>
			</div>
			
		</td>
		</tr>
		<tr>
		<td><ds:event name="'doPersist'" value="getText('Zapisz')"/></td>
		</tr>
	</table>
	
	
	<ww:hidden name="'toChangeId'" id="toChangeId"/>
	<ww:hidden name="'doRemove'" id="doRemove"/>	
</form>

<script type="text/javascript">
    /**
        Funkcja wywo�ywana przez odno�niki z drzewa dzia��w.
    */
    function post(guid)
    {
        document.getElementById('acceptanceDivisionId').value = guid;
        document.forms[0].submit();
    }

    function remove(id)
    {
    	document.getElementById('toChangeId').value = id;
    	document.getElementById('doRemove').value = 'true';
        document.forms[0].submit();
    }

    function add(id)
    {
    	document.getElementById('toChangeId').value = id;
    	document.getElementById('table_hide').style.display = "";
    }

    function showBox(field)
    {
        if(field.value == 'ds.user')
        {
        	document.getElementById('tr_user').style.display = "";
        	document.getElementById('tr_kod').style.display = "none";
        	document.getElementById('tr_name').style.display = "none";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "none";
        	
        }
        if(field.value == 'ac.division')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "none";
        }
        if(field.value == 'ac.acceptance')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "none";
        }
        if(field.value == 'ext.user')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "none";
        }
        if(field.value == 'ds.division')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "";
        	document.getElementById('tr_ck').style.display = "none";
        }
        if(field.value == 'ac.centrumKosztow')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "";
        }
        if(field.value == 'ac.fieldValue')
        {
        	document.getElementById('tr_user').style.display = "none";
        	document.getElementById('tr_kod').style.display = "";
        	document.getElementById('tr_name').style.display = "";
        	document.getElementById('tr_ogolna').style.display = "none";
        	document.getElementById('tr_guid').style.display = "none";
        	document.getElementById('tr_ck').style.display = "none";
        }
        
    }
    
</script>