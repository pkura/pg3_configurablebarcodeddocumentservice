<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
C--%>
<!--N acceptances.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="OsobyAkceptujace"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<script language="javascript" src="<ww:url value="'/gmenu.js'"/>" ></script>
<form action="<ww:url value="'/admin/acceptances.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="conditions != null && conditions.size() > 0">
		<table class="tableMargin formTable">		
			<tr>
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=cn'"/>">
						<ds:lang text="KodAkceptacji"/></a>
				</th>
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=fieldValue'"/>">
						<ds:lang text="WartoscSterujaca"/></a>
				</th>
				<ds:dockinds test="invoice">
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=symbol'"/>">
						<ds:lang text="MPK"/></a>
				</th>
				</ds:dockinds>
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=username'"/>">
						<ds:lang text="Uzytkownik"/></a>
				</th>
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=divisionGuid'"/>">
						<ds:lang text="Dzial"/></a>
				</th>
				<ds:dockinds test="invoice">
				<th>
					<a href="<ww:url value="'/admin/acceptances.action?by=maxAmountForAcceptance'"/>">
						<ds:lang text="MaksymalnaKwotaAkceptacji"/></a>
				</th>
				</ds:dockinds>
				<th></th>
			</tr>
			<ww:iterator value="conditions">
				<tr>
					<td>
						<ww:property value="cn"/>
					</td>
					<td class="alignCenter">
						<ww:property value="value"/>
					</td>
					<ds:dockinds test="invoice">
					<td>
						<ww:property value="mpk"/>
					</td>
					</ds:dockinds>
					<td>
						<ww:property value="user"/>
					</td>
					<td>
						<ww:property value="division"/>
					</td>
					<ds:dockinds test="invoice">
					<td>
						<ww:property value="maxAmountForAcceptance"/>
					</td>
					</ds:dockinds>
					<td>
						<a href="javascript:void(deleteAcceptance('<ww:url value="deleteLink"/>'))"><ds:lang text="usun"/></a>
					</td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>

	<h3><ds:lang text="NowyWarunekAkceptacji"/></h3>
	<table class="tableMargin formTable" >
		<tr> 
			<td>
				<ds:lang text="KodAkceptacji"/>
			</td>
			<td>
				<ww:textfield name="'cn'" value="cn" id="cn" size="40"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="WartoscSterujaca"/>
			</td>
			<td>
				<ds:extended-select id="value" name="'value'" cssClass="'sel'" list="centra" listKey="id" listValue="id+' '+name"
					headerKey="''" headerValue="getText('ogolna.select')"/>
			</td>			
		</tr>
		<tr>
			<td>
				<ds:lang text="Uzytkownik"/>
			</td>
			<td>
				<ds:extended-select id="username" name="'username'" cssClass="'sel'" list="users" listKey="name" listValue="lastname+' '+firstname"
					value="username" headerKey="''" headerValue="getText('select.brak')"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Dzial"/>
			</td>
			<td>
				<ds:extended-select name="'divisionGuid'" list="divisions" cssClass="'sel'" listKey="guid"
					listValue="name+' (' + parent.name + ')'" value="divisionGuid" headerKey="''" headerValue="getText('select.brak')"/>
			</td>
		</tr>
		<ds:dockinds test="invoice">
		<tr> 
			<td>
				<ds:lang text="MaksymalnaKwotaAkceptacji"/>
			</td>
			<td>
				<ww:textfield name="'maxAmountForAcceptance'" value="maxAmountForAcceptance" id="cn" size="40"/>
			</td>
		</tr>
		</ds:dockinds>
	</table>
	<ds:event name="'doAdd'" value="getText('Dodaj')" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
</form>

<script type="text/javascript">
	function validate()
	{
		var cn = document.getElementById('cn').value;
		var usernameField = document.getElementById('username');
		var divisionField = document.getElementById('divisionGuid');
		if (cn == null || cn.trim().length == 0) 
		{ 
			alert('<ds:lang text="NiePodanoKoduAkceptacji"/>'); 
			return false; 
		}
		if (usernameField.selectedIndex != 0 && divisionField.selectedIndex != 0)
		{
			alert('<ds:lang text="NieMoznaJednoczesnieOkreslicUzytkownikaIdzialu"/>'); 
			return false; 
		}
		
		return true;
	}

	function deleteAcceptance(link)
	{
		if (!confirm('Na pewno chcesz usun�� akceptacje?'))
			return false;
		document.location.href = link;
	}

	
	$j(document).ready(function() {
		enumTabIndex();
	});
	
</script>
<!--N koniec acceptances.jsp N-->