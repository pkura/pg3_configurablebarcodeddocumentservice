<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N division-tree.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Struktura akceptacji</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/dockind/acceptance-structure.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

    <table>
        <tr>
            <td>
                <ww:property value="treeHtml" escape="false"/>
            </td>
        </tr>
    </table>
</form>

<ww:if test="cn!=null">
	<%-- <form
		action="create-channel.action"
		method="get">
		<ww:hidden name="'cn'" value="cn"/>
		<ww:hidden name="'fieldValue'" value="fieldValue"/>
		<ds:event name="'default'" value="'Dodaj nowy kana�'" />				
	</form> --%>
</ww:if>

<ww:if test="centra!=null && centra.size >0">
	<h3>Centra kosztowe:</h3>
	<table>
		<tr>
			<th>Nazwa</th>
			<th>Symbol</th>
			<th>ID</th>
			<th></th>
		</tr>
		<ww:set name="pageCN" value="cn"/>	
		
		<ww:if test="!'centrum'.equals(editType)">
			<ww:iterator value="centra" id="centrum">
				<tr>
					<td><ww:property value="name" /></td>
					<td><ww:property value="symbol" /></td>
					<td><ww:property value="id" /></td>
					<td>
					<form
						action="acceptance-structure.action?cn=<ww:property value="#pageCN"/>&fieldValue=<ww:property value="id"/>"
						method="post">
						<ds:event name="'doConditionChange'"value="'Zmie� dzia� obs�uguj�cy'" />						
					</form>
					<form
						action="create-channel.action?cn=<ww:property value="#pageCN"/>&fieldValue=<ww:property value="id"/>"
						method="post">
						<ww:hidden name="'editing'" value="true"/>
						<ds:event name="'default'" value="'Zmie� caly kana�'" />						
					</form>
					</td>
				</tr>
			</ww:iterator>
		</ww:if>
		<ww:else>			
			<ww:iterator value="centra" id="centrum">
				<tr>
					<td><ww:property value="name" /></td>
					<td><ww:property value="symbol" /></td>
					<td><ww:property value="id" /></td>
					<td>
					<form
						action="acceptance-structure.action?cn=<ww:property value="#pageCN"/>&fieldValue=<ww:property value="id"/>"
						method="post">
						<ww:if test="id.toString().equals(fieldValue)">
							<ww:select name="'selectedDivisionGuid'" list="allDivisions" listKey="guid" listValue="name"/>
							<ds:event name="'doConditionChange'" value="'Zmie� dzia� obs�uguj�cy'" />
						</ww:if>
					</form>
					</td>
				</tr>
			</ww:iterator>
		</ww:else>
	</table>
</ww:if>

<script type="text/javascript">

</script>