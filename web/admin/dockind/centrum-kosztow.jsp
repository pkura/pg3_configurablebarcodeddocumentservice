<%--T
	Przer�bka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Strona zawiera wyb�r centrum, i jego dodawanie. Centra kosztowe s� u wielu klient�w
	dlatego nale�y uwa�a� co si� dodaje.
C--%>
<!--N centrum-kosztow.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Centra koszt�w</h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<input type="hidden" name="order" id="order"/>

<form action="<ww:url value="'/admin/centrum-kosztow.action"/>" method="post" onsubmit="disableFormSubmits(this);">
	<ww:if test="centra != null && centra.size() > 0">
		<table class="tableMargin">
			<tr>
				<th>
					<a href="<ww:url value="'/admin/centrum-kosztow.action?order=id'"/>">Id</a>
				</th>
				<th>
					<a href="<ww:url value="'/admin/centrum-kosztow.action?order=symbol'"/>">Symbol</a>
				</th>
				<th>
					<a href="<ww:url value="'/admin/centrum-kosztow.action?order=name'"/>">Nazwa</a>
				</th>
				<th colspan="3"></th>
			</tr>
			<ww:iterator value="centra">
				<tr <ww:if test="!available">class="hiddenValue"</ww:if>>
					<td>
						<a href="<ww:url value="'/admin/centrum-kosztow.action?doView=true&id='+id"/>">
							<ww:property value="id"/></a>
					</td>
					<td>
						<a href="<ww:url value="'/admin/centrum-kosztow.action?doView=true&id='+id"/>">
							<ww:property value="symbol"/></a>
					</td>
					<td>
						<a href="<ww:url value="'/admin/centrum-kosztow.action?doView=true&id='+id"/>">
							<ww:property value="name"/></a>
					</td>
					<td>						
						<a href="<ww:url value="'/admin/centrum-kosztow.action?doDelete=true&id='+id"/>">[usu�]</a>
						<a href="<ww:url value="'/admin/centrum-kosztow.action?doChangeAvailability=true&id='+id+'&available='+!available"/>"><ww:if test="available">[ukryj]</ww:if><ww:else>[poka�]</ww:else></a>
					</td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>

	<h3>Nowe centrum koszt�w</h3>
	<table class="tableMargin">
		<tr>
			<td>
				Symbol:
			</td>
			<td>
				<ww:textfield name="'symbol'" value="symbol" id="symbol" size="40"/>
			</td>
		</tr>
		<tr>
			<td>
				Nazwa:
			</td>
			<td>
				<ww:textfield name="'name'" value="name" id="name" size="40"/>
			</td>
		</tr>
	</table>
	
	<ds:event name="'doAdd'" value="'Dodaj'" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
</form>

<script type="text/javascript">
	function validate()
	{
		var symbol = document.getElementById('symbol').value;
		var name = document.getElementById('name').value;
		var usernameField = document.getElementById('username');
		var divisionField = document.getElementById('divisionGuid');
		if (symbol == null || symbol.trim().length == 0) 
		{ 
			alert('Nie podano symbolu'); 
			return false; 
		}
		if (name == null || name.trim().length == 0) 
		{ 
			alert('Nie podano nazwy'); 
			return false; 
		}
		return true;
	}
	
	function deleteCentrum(link)
	{
		if (!confirm('W przypadku, gdy istnieje dokument przypisany do tego centrum, po wykonaniu tej '
		+'operacji w dzia�aniu aplikacji moga pojawi� si� b��dy! Na pewno chcesz usun�� to centrum?'))
			return false;
		document.location.href = link;
	}
</script>
<!--N centrum-kosztow.jsp N-->
