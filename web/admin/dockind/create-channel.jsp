<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N division-tree.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<ww:if test="editing">
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Zmie� kana�</h1>
</ww:if><ww:else>
	<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>">Utw�rz kana�</h1>
</ww:else>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="create-channel.action" method="post">
	<ww:hidden name="'cn'" value="cn"/>
	<ww:hidden name="'fieldValue'" value="fieldValue"/>
	<ww:hidden name="'editing'" value="editing"/>
	
	<ww:if test="editing">
		
	</ww:if><ww:else>
		
	</ww:else>
	
	<ww:iterator value="acceptanceConditions">
	
		<ww:hidden name="'cns'" value="cn"/>
		
		<br/>
		Wybierz akceptacj� o kodzie "<ww:property value="cn" />"
		<br/>

		<ww:select name="cn" list="allDivisions" listValue="name"
			listKey="guid" headerValue="text" headerKey="divisionGuid" />
		<br />

	</ww:iterator>
	
	<ww:if test="editing">
		<ds:event name="'doEdit'"  value="'Zapisz zmiany'"/>
	</ww:if><ww:else>
		<ds:event name="'doCreate'"  value="'Dodaj kana�'"/>
	</ww:else>
</form>

