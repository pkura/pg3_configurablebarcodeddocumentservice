<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N division-tree.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/division-tree.action'"/>" method="post" onsubmit="disableFormSubmits(this);">

    <table>
        <tr>
            <td>
                <ww:property value="treeHtml" escape="false"/>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0">
        <ww:iterator value="users">
            <tr>
                <td><%--<img src="<ww:url value="'/img/folder-position.gif'"/>" width="18" height="18" class="zoomImg" class="zoomImg" border="0" alt="Dzia�"/>--%></td>
                <td><a href="javascript:void(window.opener.accept('<ww:property value="targetDivision.guid"/>','<ww:property value="cn"/>','<ww:property value="targetDivision.name"/>','<ww:property value="name"/>'));javascript:void(window.close());">
                <ww:property value="description"/>
                </a>
                </td>
            </tr>
        </ww:iterator>
    </table>
</form>

<script type="text/javascript">

</script>