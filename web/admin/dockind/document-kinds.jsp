<%--T
	Przeróbka layoutu.
	Stan: TC_0_8
	Typ: include (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 07.08.08
	Poprzednia zmiana: 07.08.08
C--%>
<!--N document-kinds.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>


<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="RodzajeDokumentowDocusafe"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/document-kinds.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">
	<ww:if test="!documentKinds.empty">
		<h4>
			<ds:lang text="IstniejaceRodzaje"/>
		</h4>
	
		<table class="tableMargin">
			<tr>		
				<th>
					<ds:lang text="NazwaKodowa"/>
				</th>
				<th>
					<ds:lang text="NazwaDlaUzytkownika"/>
				</th>
				<th>
					<ds:lang text="TabelaWbazie"/>
				</th>
				<th>
					<ds:lang text="OstatniaModyfikacja"/>
				</th>
				<th>
					<ds:lang text="Xml"/>
				</th>
				<th>
					<ds:lang text="Dostepny"/>
				</th>
				<th>
					<ds:lang text=""/>
				</th>
				<th>
					<ds:lang text="LicencjaWaznaDo"/>
				</th>
			</tr>
			<ww:iterator value="documentKinds">
				<tr>
					<td>
						<ww:property value="cn"/>
					</td>
					<td>
						<ww:property value="name"/>
					</td>
					<td>
						<ww:property value="tablename"/>
					</td>
					<td>
						<ww:property value="ForrmatedAddDate"/>
					</td>
					<td>
						<a href="<ww:url value="baseLink"><ww:param name="'doGetXml'" value="true"/><ww:param name="'cn'" value="cn"/></ww:url>"><ds:lang text="Pobierz"/></a>
					</td>
					<td>
						<a href="<ww:url value="baseLink"><ww:param name="'doEnableDisable'" value="true"/><ww:param name="'cn'" value="cn"/></ww:url>"><ww:if test="Enabled"><ds:lang text="true"/></ww:if><ww:else><ds:lang text="false"/></ww:else></a>
					</td>
					<td>
						<a href="<ww:url value="baseLink"><ww:param name="'doSaveAll'" value="true"/><ww:param name="'cn'" value="cn"/></ww:url>"><ds:lang text="ZapiszPonownie"/></a>
					</td>
					<td>
						<ds:format-date pattern="MM-yyyy" value="availableToDate"/>
					</td>
				</tr>
			</ww:iterator>
		</table>
	</ww:if>
	
	<h4><ds:lang text="NowyRodzaj"/></h4>

	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="NazwaKodowa"/>
			</td>
			<td>
				<ww:textfield name="'cn'" id="cn" size="30" cssClass="'txt'" maxlength="30"/>
			</td>		 
		</tr>
		<tr>
			<td>
				<ds:lang text="NazwaDlaUzytkownika"/>
			</td>	
			<td>
				<ww:textfield name="'name'" id="name" size="30" maxlength="100" cssClass="'txt'"/>
			</td>		
		</tr>
		<tr>
			<td>
				<ds:lang text="TabelaWbazie"/>
			</td>
			<td>
				<ww:textfield name="'tableName'" id="tableName" size="30" maxlength="200" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="PlikXML"/>
			</td>
			<td>
				<ww:file name="'file'" id="file" size="30" cssClass="'txt'"/>
			</td>
		</tr>
	</table>

	<br/>
<!-- <ds:event value="getText('DodajRodzaj')" name="'doCreate'" cssClass="'btn'" confirm="getText('NaPewnoDodacTakiRodzaj')+'?'" onclick="'if (!validateForm()) return false'"/> -->
	<ds:event value="getText('DodajRodzaj')" name="'doCreate'" cssClass="'btn'" confirm="getText('NaPewnoDodacTakiRodzaj')+'?'"/>

	<br/>
	<span class="warning"><ds:lang text="UWAGA"/>: <ds:lang text="document.kind.dodanieRodzajuOnazwie..."/>.</span>
</form>

<%--
<h1><ds:lang text="RodzajeDokumentowDocusafe"/></h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/document-kinds.action'"/>" method="post" enctype="multipart/form-data" onsubmit="disableFormSubmits(this);">

<ww:if test="!documentKinds.empty">
	<h4><ds:lang text="IstniejaceRodzaje"/></h4>

	<table>
	<tr>		
		<th><ds:lang text="NazwaKodowa"/></th>
		<th><ds:lang text="NazwaDlaUzytkownika"/></th>
		<th><ds:lang text="TabelaWbazie"/></th>
	</tr>
	<ww:iterator value="documentKinds">
		<tr>
			<td><ww:property value="cn"/></td>
			<td><ww:property value="name"/></td>
			<td><ww:property value="tablename"/></td>
		</tr>
	</ww:iterator>
	</table>
</ww:if>

<h4><ds:lang text="NowyRodzaj"/></h4>

<table>
	<tr>
		<td><ds:lang text="NazwaKodowa"/><!-- <span class="star">*</span>:--></td>
		<td><ww:textfield name="'cn'" id="cn" size="30" cssClass="'txt'" maxlength="30"/></td>		 
	</tr>
	<tr>
		<td><ds:lang text="NazwaDlaUzytkownika"/><!--<span class="star">*</span>:--></td>	
		<td><ww:textfield name="'name'" id="name" size="30" maxlength="100" cssClass="'txt'"/></td>		
	</tr>
	<tr>
		<td><ds:lang text="TabelaWbazie"/><!--<span class="star">*</span>:--></td>
		<td><ww:textfield name="'tableName'" id="tableName" size="30" maxlength="100" cssClass="'txt'"/></td>
	</tr>
	<%--<tr>
		<td>Tabela na atrybuty wielowartościowe:</td>
		<td><ww:textfield name="'multipleTableName'" id="multipleTableName" size="30" maxlength="100" cssClass="'txt'"/></td>
	</tr>-->
	<tr>
		<td><ds:lang text="PlikXML"/><!--<span class="star">*</span>:--></td>
		<td><ww:file name="'file'" id="file" size="30" cssClass="'txt'"/></td>
	</tr>
	<tr>
		<td></td>
		<td><ds:event value="getText('DodajRodzaj')" name="'doCreate'" cssClass="'btn'" confirm="getText('NaPewnoDodacTakiRodzaj')+'?'" onclick="'if (!validateForm()) return false'"/></td>
	</tr>
	<tr>
		<td><span style="color:red"><ds:lang text="UWAGA"/>:</span></td>
		<td><span style="color:red"><ds:lang text="document.kind.dodanieRodzajuOnazwie..."/>.</span></td>
	</tr>
</table>

</form>
--%>
<script type="text/javascript">
	function validateForm()
	{
		var file = document.getElementById('file').value;
		var name = document.getElementById('name').value;
		var cn = document.getElementById('cn').value;
		var tableName = document.getElementById('tableName').value;
		
		if (file == null || file.trim().length == 0) 
		{ 
			alert('<ds:lang text="NiePodanoPlikuXML"/>'); 
			return false; 
		}
		if (cn == null || cn.trim().length == 0) 
		{ 
			alert('<ds:lang text="NiePodanoNazwyKodowej"/>'); 
			return false; 
		}
		if (name == null || name.trim().length == 0) 
		{ 
			alert('<ds:lang text="NiePodanoNazwyDlaUzytkownika"/>'); 
			return false; 
		}
		if (tableName == null || tableName.trim().length == 0) 
		{ 
			alert('<ds:lang text="NiePodanoTabeliWbazie"/>'); 
			return false; 
		}
		return true;		
	}
</script>
<!--N koniec document-kinds.jsp N-->