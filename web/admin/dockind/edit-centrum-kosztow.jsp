<%--T
	Przer�bka layoutu.
	Stan: TC_0_0
	Typ: jeszcze_nie_wiem_ (layout, include, popup, jeszcze_nie_wiem)
	Ostatnia zmiana: 25.06.08
	Poprzednia zmiana: 25.06.08
C--%>
<!--N edit-centrum-kosztow.jsp N-->

<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>

<h1>Centra koszt�w</h1>
<hr size="1" align="left" class="horizontalLine"  width="77%" />
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<h3>Edycja centrum koszt�w</h3>

<form action="<ww:url value="'/admin/centrum-kosztow.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
    <ww:hidden name="'id'" value="id"/>
    <table>  
        <tr>
            <td>Id</td>
            <td><ww:property value="id"/></td>
        </tr>
        <tr>
            <td>Symbol</td>
            <td><ww:textfield id="symbol" name="'symbol'" value="symbol" size="40"/></td>
        </tr>
        <tr>
            <td>Nazwa</td>
            <td><ww:textfield id="name" name="'name'" value="name" size="40"/></td>
        </tr>     

		<ds:dockinds test="invoice_ic">
		<%-- =============== INTERCARS ============== --%>
		<tr>
            <td>Symbol Safo</td>
            <td><ww:textfield id="symbol2" name="'symbol2'" value="symbol2" size="40"/></td>
        </tr>
        <tr>
            <td>Akceptacja dla wszystkich centr�w</td>
            <td><ww:checkbox name="'intercarsPowered'" fieldValue="true" value="intercarsPowered"/></td>
        </tr>
        <tr>
           <td>Wymagana Nazwa umowy</td>
           <td><ww:checkbox name="'contractNeeded'" value="contractNeeded" fieldValue="'true'"/></td>
        </tr>
        <tr>
            <td>Automatyczny przydzia� centrum</td>
            <td>
                <input type="checkbox" name="autoAdded" value="true" <ww:if test="autoAdded">checked="true"</ww:if> onclick="showA(this)"/>
                <script type="text/javascript">
                    function showA(x){
                        $j("#autoAddedParams").toggle();
                    }
                </script>
            </td>
        </tr>
        <tbody id="autoAddedParams" <ww:if test="!autoAdded">style="display:none;"</ww:if>>
            <tr id="account-td" >
               <td>Automatycznie wybierana konto kosztowe</td>
               <td><ww:select cssClass="'sel'" id="defaultAccountId" name="'defaultAccountId'" list="accounts" listKey="id" listValue="'(' + number + ') '+ name"/></td>
            </tr>
            <tr id="location-td">
               <td>Automatycznie wybierana lokalizacja</td>
               <td><ww:select cssClass="'sel'" id="defaultLocationId" name="'defaultLocationId'" list="locations" listKey="id" listValue="cn + ' - ' + title"/></td>
            </tr>
        </tbody>
		</ds:dockinds><ds:dockinds test="invoice">
		<%-- ================== AEGON ================= --%>
		<tr>
            	<td colspan="1">
                	<ds:event name="'doUpdate'" value="'Zapisz'" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
            	</td>
				<td></td>
        </tr>
		</ds:dockinds>
    </table>

    <ds:dockinds test="invoice_ic">
	<%-- =============== INTERCARS ============== --%>
		<table>
			<tr>
				<th colspan="2"><h4>Mo�liwe tryby akceptacji:</h4></th>
				<th>Nazwa dla u�ytkownika</th>
				<th>Minimalna kwota</th>
			</tr>
        	<ww:iterator value="intercarsAcceptanceModes">
        		<tr>
        			<td><ww:property value="name"/></td>
        			<td><ww:checkbox name="'modes'" fieldValue="id" value="availableModes.contains(id)"/></td>
					<td><ww:textfield name="'acceptanceModeName'+id" value="acceptanceModeNames.get(id)" size="30"/></td>
					<td><ww:textfield name="'acceptanceModeMinimalAmount'+id" value="acceptanceModeAmounts.get(id)" size="30"/></td>
        		</tr>
        	</ww:iterator>
        
      	 	<tr>
            	<td></td>
            	<td>
                	<ds:event name="'doUpdate'" value="'Zapisz'" cssClass="'btn'" onclick="'if (!validate()) return false;'"/>
            	</td>
            	<td></td>
        	</tr> 
		</table>
    </ds:dockinds>
    
</form>   

<script type="text/javascript">
    function validate()
    {
        var symbol = document.getElementById('symbol').value;
        var name = document.getElementById('name').value;
        var usernameField = document.getElementById('username');
        var divisionField = document.getElementById('divisionGuid');
        if (symbol == null || symbol.trim().length == 0) 
        { 
            alert('Nie podano symbolu'); 
            return false; 
        }
        if (name == null || name.trim().length == 0) 
        { 
            alert('Nie podano nazwy'); 
            return false; 
        }
    /*    if (usernameField.selectedIndex != 0 && divisionField.selectedIndex != 0)
        {
            alert('Nie mo�na jednocze�nie okre�li� u�ytkownika i dzia�u'); 
            return false; 
        }  */
        return true;
    }
</script> 