<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ page contentType="text/html; charset=iso-8859-2" %>




<h1 class="header1 <ds:additions test="tcLayout">highlightedColor</ds:additions>"><ds:lang text="UstawieniaEUrzedu"/></h1>
<hr class="fullLine <ds:additions test="tcLayout">highlightedColor</ds:additions><ds:additions test="!tcLayout">horizontalLine</ds:additions>"/>
<p></p>

<ds:ww-action-errors/>
<ds:ww-action-messages/>

<form action="<ww:url value="'/admin/eurzad-settings.action'"/>" method="post" onsubmit="disableFormSubmits(this);">
	<table class="tableMargin">
		<tr>
			<td>
				<ds:lang text="AdresSerwisu"/>:
			</td>
			<td>
				<ww:textfield name="'address'" size="50" maxlength="200" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Login"/>:
			</td>
			<td>
				<ww:textfield name="'login'" size="50" maxlength="100" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Haslo"/>:
			</td>
			<td>
				<ww:textfield name="'password'" size="50" maxlength="50" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="IDUrzedu"/>:
			</td>
			<td>
				<ww:textfield name="'urzadId'" size="5" maxlength="5" cssClass="'txt'"/>
			</td>
		</tr>
		<tr>
			<td>
				<ds:lang text="Koordynator"/>:
			</td>
			<td>
				<ww:textfield id="koordynator"  name="'koordynator'" size="50" maxlength="50" cssClass="'txt'"/>
				<a name="koordynator" href="javascript:void(openAssignmentWindow('koordynator'))">
						<ds:lang text="wybierz"/></a>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<ds:submit-event value="getText('Zapisz')" name="'doUpdate'" cssClass="'btn'"/>
				<ds:submit-event value="getText('Testuj')" name="'doTest'" cssClass="'btn'"/>
			</td>
		</tr>

	</table>
</form>

<script type="text/javascript">
function openAssignmentWindow(cn)
{
	window.open('<ww:url value="'/admin/division-tree.action'"/>?divisionGuid=rootdivision&cn=koordynator', 'assignmentwindow', 'width=500,height=400,resizable=yes,scrollbars=yes,status=no,toolbar=no,menu=no');
}

function accept(param,cn,name,username)
{
	var element = document.getElementById('koordynator');
	if (username == null && name != null )
		element.value = name;
	if (username != null && name != null)
		element.value = username;
}
</script>
