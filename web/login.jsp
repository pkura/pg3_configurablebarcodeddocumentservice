<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ page import="pl.compan.docusafe.core.cfg.Configuration,
                 org.apache.commons.logging.Log,
                 org.apache.commons.logging.LogFactory,
                 javax.security.auth.login.LoginContext,
                 pl.compan.docusafe.core.users.auth.FormCallbackHandler,
                 pl.compan.docusafe.core.AccessLog,
                 pl.compan.docusafe.core.users.auth.AuthUtil,
                 javax.security.auth.login.LoginException,
                 pl.compan.docusafe.core.crypto.DockindCryptoDigester,
                 pl.compan.docusafe.core.AvailabilityManager,
                 pl.compan.docusafe.util.NetUtil"%>
<%@ page import="pl.compan.docusafe.boot.Docusafe"%>
<%@ page import="javax.security.auth.Subject"%>
<%@ page import="pl.compan.docusafe.core.users.DSUser"%>
<%@ page import="pl.compan.docusafe.util.TextUtils"%>
<%@ page import="org.apache.commons.codec.binary.Base64"%>
<%@ page import="pl.compan.docusafe.core.GlobalPreferences"%>
<%@ page import="pl.compan.docusafe.core.DSApi"%>
<%@ page import="pl.compan.docusafe.core.EdmException"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://com-pan.pl/edm/tags-html" prefix="edm-html" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-core" prefix="edm-core" %>
<%@ taglib uri="http://com-pan.pl/edm/tags-security" prefix="edm-sec" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="webwork" prefix="ww" %>
<ds:available test="layout2">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
</ds:available>

<%@ page import="java.sql.Statement"%>

<!-- Wersja: <%= Docusafe.getVersion() %>.<%= Docusafe.getDistTimestamp() %> -->

<%!
	private static final Log log = LogFactory.getLog("login");
%>
<%
	/*
		if (request.getSession(true).getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY) != null)
		{
			response.sendRedirect(request.getContextPath());
			return;
		}
	*/

	String username = request.getParameter("username");
	String password = request.getParameter("password");
	String url = request.getParameter("url");
	String password64 = request.getParameter("password64"); 
	Boolean subst = false;

	try
	{
		DSApi.openAdmin();
		request.setAttribute("customerName", GlobalPreferences.getCustomer());

		
		
		request.setAttribute("isDockindAvailable", DockindCryptoDigester.getInstance().isDockindAvailable(Docusafe.getAdditionProperty("dockind.default")));
		
		if (!GlobalPreferences.DOCUSAFE_PRODUCTION_INSTANCE.equals(GlobalPreferences.getDocusafeInstanceType()))
			request.setAttribute("docusafeType", GlobalPreferences.getDocusafeInstanceTypeName());
		DSApi.close();
	}
	catch (EdmException e)
	{
		log.error("", e);
		request.setAttribute("failed", Boolean.TRUE);
	}

	if (password64 != null && password64.length() > 0)
	{
		try
		{
			password = new String(Base64.decodeBase64(password64.getBytes()));
		}
		catch (Exception e)
		{
		}
	}

	// aby w polu formularza nie pojawi�o si� null
	if (url == null || url.startsWith("/logout.do"))
		url = "/";

	if (username != null && username.length() > 0 && password != null)
	{
		Subject subject = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

		if (subject != null)
		{
			// je�eli zalogowany jest inny u�ytkownik ni� ��dany, ponowne logowanie
			if (!AuthUtil.getUserPrincipal(subject).getName().equals(username))
			{
				session.invalidate();
				response.sendRedirect(request.getContextPath()+"/login.jsp" +
					"?username="+pl.compan.docusafe.util.HttpUtils.urlEncode(username)+
					"&password="+pl.compan.docusafe.util.HttpUtils.urlEncode(password));
				return;
			}
			// je�eli ��dany jest zalogowany ju� u�ytkownik, przekierowanie do g��wnej strony
			else
			{

				response.sendRedirect(request.getContextPath());
			}

		}

		String moduleName = application.getInitParameter("loginModuleName");

		LoginContext lc = new LoginContext(moduleName,
			new FormCallbackHandler(username, password, NetUtil.getClientAddr(request).getIp()));
		
		try
		{			
			GlobalPreferences.removeLoginFile("admin");
			GlobalPreferences.addFreeUser("admin");
			lc.login();

			// w tym punkcie wiadomo, �e nazwa u�ytkownika i has�o
			// by�y prawid�owe
			

			// dodaje warto�� parametru User-Agent uzytkownikow po zalogowaniu do bazy danych
			try
			{    				
				if(!username.equals("admin"))
				{

					String width = request.getParameter("width"); 
					String height = request.getParameter("height");
					String browserName = "";
					String userAgent = TextUtils.parseUserAgentHeader(request.getHeader("User-Agent"));
					int tempIndexOf;
					if(userAgent.indexOf("/")!=-1)
					{
						tempIndexOf = userAgent.indexOf("/");
						browserName = userAgent.substring(0,tempIndexOf);
						userAgent = userAgent.substring(tempIndexOf+1);
					}
					else
					{
							tempIndexOf = userAgent.indexOf(" ");
							browserName = userAgent.substring(0,tempIndexOf);
							userAgent = userAgent.substring(tempIndexOf+1);
					}
					DSApi.openAdmin();
					DSApi.context().begin();
					Statement userAgentSt = DSApi.context().createStatement();	
					int a = userAgentSt.executeUpdate("INSERT INTO DS_AGENT_USER_STATS VALUES('" + browserName + "','" + userAgent +"'," + width + "," +  height + ")");
					DSApi.context().closeStatement(userAgentSt);
					DSApi.context().commit();	
				}
			} 
			catch(Exception e)
			{
				log.error(e.getMessage());
			}
			finally
			{ 
				DSApi.close();
			}
			
			request.getSession().setAttribute(pl.compan.docusafe.web.filter.AuthFilter.LANGUAGE_KEY,GlobalPreferences.getUserLocal(lc.getSubject()));
			if((GlobalPreferences.EMERGENCY == true )&& (GlobalPreferences.getFreeUser(username)==null))
			{
				request.setAttribute("emergency",Boolean.TRUE);
			}
			else
			{
				DSUser.correctLogin(username);
				request.getSession(true).setAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY, lc.getSubject());
				
				String sessionTable = Docusafe.getAdditionProperty("phpbb.sessionTable");
				if(sessionTable != null)
					AuthUtil.createPhpbbSession(request, sessionTable);
				
				/*
				response.sendRedirect(request.getContextPath() +
					"/password-validity-check.action?url="+
					java.net.URLEncoder.encode(url, "iso-8859-2"));
				*/

				Subject subj = (Subject) session.getAttribute(pl.compan.docusafe.web.filter.AuthFilter.SUBJECT_KEY);

				if (url != null){
				     if ( AvailabilityManager.isAvailable("userSubstitutedPopUp") ) {
				        DSApi.open(subj);
                        if (DSApi.context().getDSUser().getIsSubstituted())
                            {
                                subst = true;
                                session.setAttribute("substitute", subst);
                            }
                        else
                            {
                                subst = false;
                                session.setAttribute("substitute", subst);
                            }

                        DSApi.close();
                    }
					response.sendRedirect(request.getContextPath()+url);
			    }
				else {
				    if (AvailabilityManager.isAvailable("userSubstitutedPopUp")) {
				        DSApi.open(subj);
                        if (DSApi.context().getDSUser().getIsSubstituted())
                            {
                                subst = true;
                                session.setAttribute("substitute", subst);
                            }
                        else
                            {
                                subst = false;
                                session.setAttribute("substitute", subst);
                            }

                        DSApi.close();
                    }
                    response.sendRedirect(request.getContextPath());
                }
				/*
				AccessLog.logSignon(AuthUtil.getUserPrincipal(lc.getSubject()).getName(), request, false);
				*/				
				return;
			}
		}
		catch (Exception e)
		{
			log.error("", e);
			if(DSUser.incorrectLogin(username))
				request.setAttribute("locked",Boolean.TRUE);
			else
				request.setAttribute("failed", Boolean.TRUE);
		}
		
	}
	else{
		if(request.getParameterMap().containsKey("username")){
			request.setAttribute("failed", Boolean.TRUE);
		}
	}
%>


<%@page import="java.sql.PreparedStatement"%>
<%@page import="pl.compan.docusafe.core.AvailabilityManager"%><html>
	<head>
		<title>
			<% if(Docusafe.getAdditionProperty("docusafe.TytulStrony") == null) {%>
				<ds:lang text="TytulStrony"/>
			<% } else { %>
				<%= Docusafe.getAdditionProperty("docusafe.TytulStrony") %>
			<% } %>
		</title>
		
		<c:if test="${docusafeType == null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		<%--	W tym IFie mozna dodac favicon dla wersji testowej systemu - podmienic tylko nazwe	--%>
		<c:if test="${docusafeType != null}">
			<link rel="shortcut icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" />
			<link rel="icon" href="<c:out value='${pageContext.request.contextPath}'/>/img/favicon.ico" type="image/vnd.microsoft.icon" /> 
		</c:if>
		
		<META HTTP-EQUIV="content-type" CONTENT="text/html; CHARSET=iso-8859-2" />
		<link rel="stylesheet" type="text/css" media="all" href="<ww:url value="'/main.css'"/>"/>

<ds:additions test="tcLayout">
		<script type="text/javascript" src="<c:out value='${pageContext.request.contextPath}'/>/jquery-1.3.2.min.js"></script>
		<script type="text/javascript">
			var $j = jQuery.noConflict();
			function wysokosc()
			{
				$j(".NEWrightContent").css("height", "auto");
				if ($j(".NEWrightContent").height() < ($j(window).height() - 161)) $j(".NEWrightContent").height(($j(window).height() - 161));
				if ($j.browser.msie) $j(".NEWrightContent").height(($j(".NEWrightContent").height() + 10));
			}

			$j(document).ready(function ()
			{
				wysokosc();
			});
		</script>
</ds:additions>

	</head>

<ds:additions test="tcLayout">
	<body onresize="wysokosc();">
		<div class="NEWmain">
			<!-- Pocz�tek nag��wka strony -->
			<div class="NEWtopBar">
				<div class="NEWlogoLeft">
					<a href="<c:out value='${pageContext.request.contextPath}'/>">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/logo_c.gif"/>
					</a>
				</div> <!-- NEWlogoLeft -->
				<div class="NEWlogoRight">
					<c:if test="${docusafeType != null}">
						<div class="NEWversion">
							Wersja
							<c:out value="${docusafeType}"/>
						</div>
					</c:if>
					<c:if test="${docusafeType == null}">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/dyskietka.gif"/>
					</c:if>
				</div> <!-- NEWlogoRight -->
			</div> <!-- NEWtopBar -->
			<!-- Koniec nag��wka strony -->
			
			<!-- Pocz�tek g�rnego menu -->
			<div class="NEWtopPanel">
			</div> <!-- NEWtopPanel -->
			<!-- Koniec g�rnego menu -->

			<!-- Pocz�tek zawarto�ci strony -->
			<div class="NEWcontent">			
				
				<!-- Pocz�tek okna z tre�ci� (right) -->
				<div class="NEWrightContent">
					<div class="NEWrightContentInner">
						<div style="display:none;" id="pageType">login</div>
						<form id="test-login" action="<%= request.getRequestURI() %>" method="post">
							<input type="hidden" name="url" value="<%= url %>">
							<div class="NEWlogin">
								<c:if test="${docusafeType != null}">
									<h3>
										<ds:lang text="Wersja"/>
										<c:out value="${docusafeType}"/>
									</h3>
								</c:if>
								<h3>
								<% if(Docusafe.getAdditionProperty("docusafe.WitamyWsystemieDocuSafe") == null) {%>
									<ds:lang text="WitamyWsystemieDocuSafe"/>
								<% } else { %>
									<%= Docusafe.getAdditionProperty("docusafe.WitamyWsystemieDocuSafe") %>
								<% } %>
									<sup>&reg;</sup>
								</h3>
								<h2>
									<c:out value="${customerName}"/>
								</h2>
								<c:if test="${!isDockindAvailable}">
									<h2 style="color:red;text-align: center;">UWAGA!!!</h2>
									<h4 style="color:red;text-align: center;">
										<ds:lang text="LicencjaNaSystemJestNieaktualna"/>
									</h4>
								</c:if>			
								<table class="login marginsLeftRightAuto">
									<tr>
										<td class="login alignRight">
											<ds:lang text="NazwaUzytkownika"/>:
										</td>
										<td>
											<input type="text" name="username" size="15" class="logintxt">
										</td>
									</tr>
									<tr>
										<td class="login alignRight">
											<ds:lang text="Haslo"/>:
										</td>
										<td>
											<input type="password" name="password" size="15" class="logintxt">
										</td>
									</tr>
								</table>
								
								<input type="submit" value="<ds:lang text="Zaloguj"/>" class="loginbtn">
								
								<c:if test="${locked}">
									<p class="bold warning">
										<ds:lang text="AccountHasBeenBlocked"/>
									</p>
								</c:if>
								<c:if test="${failed}">
									<p class="bold warning">
										<ds:lang text="PodanoBledneHasloLubNazweUzytkownika"/>
									</p>
								</c:if>
								<c:if test="${emergency}">
									<p class="bold">
										<ds:lang text="SystemJestWstanieAwaryjnymNieMozeszSieZalogowac"/>
									</p>
								</c:if>
								<p style="text-align: center;">
									<a href="<%= request.getContextPath() + "/password-recovery.jsp" %>"><ds:lang text="ZapomnialemHasla"/></a>
								</p>
							</div> <!-- NEWlogin -->	
						</form>
					</div> <!-- NEWrightContentInner -->
				</div> <!-- NEWrightContent -->
				<!-- Koniec okna z tre�ci� (right) -->

			</div> <!-- NEWcontent -->
			<!-- Koniec zawarto�ci strony -->
						
			<!-- Pocz�tek dolnego menu -->
			<div class="NEWbottomPanel">
				<div class="NEWcopyrightNotice">
				<a href='<%= Configuration.getProperty("copyright.notice.url") %>'>
					<%= Configuration.getProperty("copyright.notice") %></a>
				<a class="NEWsecondCopyrightNotice" href='<%= Configuration.getProperty("copyright.notice.url.2") %>'>
					<%= Configuration.getProperty("copyright.notice.2") %></a>
				</div> <!-- NEWcopyrightNotice -->
			</div> <!-- NEWbottomPanel -->
			<!-- Koniec dolnego menu -->
			
			<!-- Pocz�tek stopki strony -->
			<div class="NEWbottomBar">
			</div> <!-- NEWbottomBar -->
			<!-- Koniec stopki strony -->
		</div> <!-- NEWmain -->
	</body>
</ds:additions>

<ds:additions test="!tcLayout">	
	<body leftMargin=0 topMargin=0 marginwidth="0" marginheight="0" <ds:available test="uni_back"> style="background: url(<ww:url value="'/img/uni_back.gif'"/>);"</ds:available>>
		<table cellSpacing=0 cellPadding=0 width="100%" border=0  height="100%">
	
			<!-- Pocz�tek nag��wka strony -->
			<tr id="logo" height="64">
				<td width="50%">
					<a href="<c:out value='${pageContext.request.contextPath}'/>">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/logo_c.gif" border="none" />
					</a>
				</td>
				<td align="right">
					<c:if test="${docusafeType != null}">
						<font color="crimson" size="26" >
							Wersja
							<c:out value="${docusafeType}"/>
						</font>
					</c:if>
					<c:if test="${docusafeType == null}">
						<img src= "<c:out value='${pageContext.request.contextPath}'/>/img/dyskietka.gif" border="0"  height="64"/>
					</c:if>
				</td>
			</tr>
			<!-- Koniec nag��wka strony -->
			
			<!-- Pocz�tek g�rnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<td  colspan="2" bgcolor="black">
					<table id="bar" cellSpacing=0 cellPadding=0 width="640" border=0 >
						<tr>
							<td>
								&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- Koniec g�rnego menu -->
			
			<!-- Pocz�tek g��wnego okna -->
			<tr>
				<td colspan="2">
					<table border="0" width="100%" height="100%">
						<tr class="mai">
							<!-- Pocz�tek okna z tre�ci� (right) -->
							<td class="mai" height="100%">
							<div style="display:none;" id="pageType">login</div>
								<form id="test-login" action="<%= request.getRequestURI() %>" method="post" style="height: 100%">
									<input type="hidden" name="url" value="<%= url %>">
									<!-- inicjowanie wartosci z rozdzielczoscia ekranu uzytwkownika -->
									<input type="hidden" name="width" id="width">
									<input type="hidden" name="height" id="height">
									<script type="text/javascript">  
										document.getElementById('width').value=screen.width;
										document.getElementById('height').value=screen.height;
									</script>
									<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
										<tr height="25%">
											<td colspan="3" height="25%">
												&nbsp;
											</td>
										</tr>
										<tr height="50%">
											<td width="25%" height="50%">
											</td>
											<td align="center" valign="middle" height="50%">
												<div class="loginBox">
												<c:if test="${docusafeType != null}">
													<h3><ds:lang text="Wersja"/> <c:out value="${docusafeType}"/></h3>
													<p>&nbsp;</p>
												</c:if>
												<h3 style="display: inline;">
													<% if(Docusafe.getAdditionProperty("docusafe.WitamyWsystemieDocuSafe") == null) {%>
														<ds:lang text="WitamyWsystemieDocuSafe"/>
													<% } else { %>
														<%= Docusafe.getAdditionProperty("docusafe.WitamyWsystemieDocuSafe") %>
													<% } %>
													<sup>&reg;</sup></h3><br />
												<h2 style="display: inline; *display: block; *top: 20px;"><c:out value="${customerName}"/></h2>
												<table width="400" class="login topSpacer">
													<tr>
														<td colspan="2" class="login">
															<%--<b>DocuSafe <%= Configuration.getVersionString() %></b>--%>
														</td>
													</tr>
													<tr>
														<td align="right" class="login">
															<ds:lang text="NazwaUzytkownika"/>:
														</td>
														<td>
															<input type="text" name="username" size="15" class="txt username" style="width: 120px">
														</td>
													</tr>
													<tr>
														<td align="right" class="login">
															<ds:lang text="Haslo"/>:
														</td>
														<td>
															<input type="password" name="password" size="15" class="txt password" style="width: 120px">
														</td>
													</tr>
													<tr>
														<!--<td></td>-->
														<td colspan=2 align="center">
															<input type="submit" value="<ds:lang text="Zaloguj"/>" class="btn">
														</td>
													</tr>
													<c:if test="${locked}">
														<tr>
															<td colspan="2" class="login">
																<b class="warning"><ds:lang text="AccountHasBeenBlocked"/></b>
															</td>
														</tr>
													</c:if>
													<c:if test="${failed}">
														<tr>
															<td colspan="2" class="login">
																<b class="warning"><ds:lang text="PodanoBledneHasloLubNazweUzytkownika"/></b>
															</td>
														</tr>
													</c:if>
													<c:if test="${emergency}">
														<tr>
															<td colspan="2" class="login">
																<b class="warning"><ds:lang text="SystemJestWstanieAwaryjnymNieMozeszSieZalogowac"/></b>
															</td>
														</tr>
													</c:if>
													<tr>
														<td colspan="2" class="login">
															<a href="<%= request.getContextPath() + "/password-recovery.jsp" %>"><ds:lang text="ZapomnialemHasla"/></a>
														</td>
													</tr>
												</table>
												</div>
											</td>
											<td width="25%" height="50%">
											</td>
										</tr>
										<tr height="25%">
											<td colspan="3" height="25%">
												&nbsp;
											</td>
										</tr>
									</table>
								</form>
							</td>
							<!-- Koniec okna z tre�ci� (right) -->
						</tr>
					</table>
				</td>
			</tr>
			<!-- Koniec g��wnego okna -->
		
			<!-- Pocz�tek dolnego menu -->
			<tr  height="18" class="zoomImg" class="zoomImg">
				<td  colspan="2" bgcolor="black">
					<table id="bar" cellSpacing=0 cellPadding=0 width="100%" border=0 >
						<tr id="bar">
							<td align="right">
								<a href='<%= Configuration.getProperty("copyright.notice.url") %>'>
									<%= Configuration.getProperty("copyright.notice") %></a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
			<tr height="30">
				<td colspan="2" class="lbar" >
					<img src="<c:out value='${pageContext.request.contextPath}'/>/img/blank.gif" width="20" height="1"/>
					<%--	Ostatnie udane logowanie: <edm-sec:last-successful-login/>, ostatnie nieudane logowanie: <edm-sec:last-unsuccessful-login/>	--%>
				</td>
			</tr>
			<!-- Koniec dolnego menu -->
		</table>
	</body>
</ds:additions>
</html>

<!--N koniec login.jsp N-->