<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">
$j(function(){
    var $leftSelected = 'left_selected';

    $j('#left-menu-main-tasklist a').toggle();
    <ww:if test="#typeIma == 'tasklist'">    
        <ww:if test="filterName == 'Document in Case'">
            changeMenu($leftSelected);
            $j('#left-menu-tasklist-docincase a').toggleClass($leftSelected);
        </ww:if>
        <ww:if test="filterName == 'Case'">
            changeMenu($leftSelected);
            $j('#left-menu-tasklist-case a').toggleClass($leftSelected);
        </ww:if>
        <ww:if test="filterName == 'zadanie'">
            changeMenu($leftSelected);
            $j('#left-menu-tasklist-zadanie a').toggleClass($leftSelected);
        </ww:if>  
    </ww:if>   
    <ww:elseif test="#typeIma == 'dwr-document'">
        <ww:if test="documentKindCn == 'doccase'">
            changeMenu($leftSelected);
            $j('#left-menu-new-doccase a').toggleClass($leftSelected);
        </ww:if>
        <ww:if test="documentKindCn == 'docincase'">
            changeMenu($leftSelected);
            $j('#left-menu-new-docincase a').toggleClass($leftSelected);
        </ww:if>
        <ww:if test="documentKindCn == 'zadanie'">
            changeMenu($leftSelected);
            $j('#left-menu-new-zadanie a').toggleClass($leftSelected);
        </ww:if>
            
        <ww:if test="documentKindCn == 'questionnaire'">
            changeMenu($leftSelected);
            $j('#left-menu-new-questionnaire-notes a').toggleClass($leftSelected);
        </ww:if>
    </ww:elseif>
    <ww:elseif test="#typeIma == 'repository'">
        <ww:if test="documentKindCn == 'doctemplate'">
            changeMenu($leftSelected);
            $j('#left-menu-new-doctemplate a').toggleClass($leftSelected);
        </ww:if>
        <ww:if test="documentKindCn == 'mediator'">
            changeMenu($leftSelected);
            $j('#left-menu-new-mediator a').toggleClass($leftSelected);
        </ww:if>
    </ww:elseif>               
    <ww:elseif test="#typeIma == 'search-dockind'">
        <ww:if test="documentKindCn == 'doctemplate'">
            changeMenu($leftSelected);
            $j('#search-dockind-doctemplate a').toggleClass($leftSelected);
        </ww:if>
    </ww:elseif>               
});

function changeMenu($ls)
{
    $j('#ul_left_ie a').each(function(){
       if($j(this).hasClass($ls))
           $j(this).removeClass($ls);
    });
}
</script>