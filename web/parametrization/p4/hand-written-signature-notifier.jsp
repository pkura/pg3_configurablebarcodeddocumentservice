<%@ page contentType="text/html; charset=iso-8859-2" %>
<%@ taglib uri="webwork" prefix="ww" %>
<%@ taglib uri="http://com-pan.pl/docusafe/tags" prefix="ds" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div id="hand-written-notifier" title="Odbi�r przesy�ek">
	<div id="hand-written-notifier-lastname"></div>
</div>

<div id="hand-written-notifier-confirm" title="Potwierdzenie odbioru">
	<div id="hand-written-notifier-confirm-user"></div>
	<img id="hand-written-notifier-confirm-image" src="#" />
</div>

<div id="hand-written-notifier-cancel" title="Przerwanie odbioru">
	U�ytkownik <span id="hand-written-notifier-cancel-user"></span> przerwa� odbi�r.
</div>

<div id="hand-written-notifier-finish" title="Koniec odbioru">
	Zako�czono odbi�r.
</div>

<script type="text/javascript" src="<ww:url value="'/comet.js'" />"></script>
<script type="text/javascript" src="<ww:url value="'/jquery.ambiance.js'" />"></script>
<link rel="stylesheet" type="text/css" href="<ww:url value="'/jquery.ambiance.css'" />" />
<script type="text/javascript">

var login = null;

var ACTION_URL = '<ww:url value="'/office/incoming/document-reception.action'" />';
function checkSession() {
	$j.post(ACTION_URL, {checkSession: true}, function(data) {
		if(data != 'OK') {
			//window.onbeforeunload = null;
			window.onunload = null;
			window.location.href = '<ww:url value="'/'" />';	
		}
	});
}

var notifier = new CometRequest({
	url: '<ww:url value="'/comet-notifier.ajx'" />' + '?MESSAGE_KIND=SIGNATURE_REQUEST',
	autoclose: true,
	callback: function (str, params) {
		var strMsgs = str.split('\r\n'),
			message = null,	lastMessage = null, 
			i = strMsgs.length-1, strJson = null,
			notification = null;
		console.info(strMsgs);
		while(i--) {
			strJson = strMsgs[i];
			if(strJson == '') {
				continue;
			}
			try {
				message = $j.parseJSON(strJson);
			} catch(e) {
				console.error(e);
				//alert('B��d parsowanie JSON: ' + str);
			}
			
			if(lastMessage == null) {
				lastMessage = message;
			} else {
				switch(message.status) {
				case 'BEGIN':
					notification = 'U�ytkownik ' + message.firstname + ' ' + message.lastname 
						+ ' rozpocz�� odbi�r.';
				break;
				case 'CONFIRM':
					notification = 'U�ytkownik ' + message.firstname + ' ' + message.lastname
						+ ' potwierdzi� odbi�r';
				break;
				case 'CANCEL':
					notification = 'U�ytkownik ' + message.firstname + ' ' + message.lastname
						+ ' przerwa� odbi�r';
				break;
				}
				
				$j.ambiance({message: notification, type: 'success', timeout: 5, fade: true});
			}
		}

		if(lastMessage != null) {
			login = lastMessage.login;
			switch(lastMessage.status) {
			case 'BEGIN':
				$j('#hand-written-notifier-cancel').dialog('close');
				$j('#hand-written-notifier-confirm').dialog('close');
				$j('#hand-written-notifier-lastname').html(lastMessage.firstname + ' ' + lastMessage.lastname);
				$j('#hand-written-notifier').dialog('open').data('signature-request', lastMessage);
			break;
			case 'CONFIRM':
				$j('#hand-written-notifier-cancel').dialog('close');
				$j('#hand-written-notifier-confirm-user').html(lastMessage.firstname + ' ' + lastMessage.lastname);
				$j('#hand-written-notifier-confirm-image').attr('src', lastMessage.base64img);
				$j('#hand-written-notifier-confirm').dialog('open');
			break;
			case 'FINISH':
				$j('#hand-written-notifier-confirm').dialog('close');
				$j('#hand-written-notifier-cancel').dialog('close');
				$j('#hand-written-notifier-finish').dialog('open');
			break;
			case 'CANCEL':
				$j('#hand-written-notifier').dialog('close');
				$j('#hand-written-notifier-confirm').dialog('close');
				$j('#hand-written-notifier-cancel-user').html(lastMessage.firstname + ' ' + lastMessage.lastname);
				
				$j('#hand-written-notifier-cancel').dialog('open');
			break;
			}
		}
		
		if(params.state == 'complete') {
			notifier.init();
			return;
		}
	},
	error: function() {
		checkSession();
	}
});

//$j('#init').click(function() {
	notifier.init();
//});

	$j('#hand-written-notifier-finish').dialog({
		modal: true,
		autoOpen: false,
		resize: false,
		buttons: [
		{
		text: 'Powr�t do DocuSafe',
		click: function() {
			$j('#hand-written-notifier-cancel').dialog('close');
			//window.onbeforeunload = null;
			window.onunload = null;
			try {
				window.location.href = fromUrl;
			} catch(e) {}
		}
	}
		]
	});

$j('#hand-written-notifier').dialog({
	modal: true,
	autoOpen: false,
	resize: false,
	buttons: [
	{
		text: 'Anuluj',
		click: function() {
			//window.onbeforeunload = null;
			window.onunload = null;
			$j.post('<ww:url value="'/office/incoming/document-reception.action'" />',
			{
				rejectReception: true,
				login: login
			}, function(data) {
				
			});
			$j('#hand-written-notifier').dialog('close');
			try {
				window.location.href = fromUrl;
			}  catch(e) {}
		}
	},
	{
		text: 'Rozpocznij odbi�r',
		click: function() {
			//window.onbeforeunload = null;
			window.onunload = null;
			var signatureReq = $j('#hand-written-notifier').data('signature-request');
			window.location.href = '<ww:url value="'/office/incoming/document-reception.action'" />'
				+ '?login=' + signatureReq.login + '&fromUrl=' 
				+ encodeURIComponent(window.location.href);
		}
	}
	]
});

$j('#hand-written-notifier-confirm').dialog({
	modal: true,
	autoOpen: false,
	resize: false,
	width: 340,
	buttons: [
	
	{
		text: 'Odrzu�',
		click: function() {
			$j('#hand-written-notifier-confirm').dialog('close');
			//window.onbeforeunload = null;
			
			$j.post('<ww:url value="'/office/common/hand-written-signature.action'" />',
					{	
						correctReception: true,
						login: login,
						json: login
					}, function(data) {
						
					}
			);
			
			
		}
	},{
		text: 'Akceptuj',
		click: function() {
			$j('#hand-written-notifier-confirm').dialog('close');
			$j.post('<ww:url value="'/office/common/hand-written-signature.action'" />',
					{	
						acceptReception: true,
						login: login,
						json: login
					}, function(data) {
						
					}
			);
			
			
			//window.onbeforeunload = null;
			window.onunload = null;
			try {
				//window.location.href = fromUrl;
			} catch(e) {}
		}
	}
	]
});

$j('#hand-written-notifier-cancel').dialog({
	modal: true,
	autoOpen: false,
	resize: false,
	buttons: [
	{
		text: 'Powr�t do DocuSafe',
		click: function() {
			$j('#hand-written-notifier-cancel').dialog('close');
			//window.onbeforeunload = null;
			window.onunload = null;
			try {
				window.location.href = fromUrl;
			} catch(e) {}
		}
	}
	]
});

</script>

<style>

#hand-written-notifier-lastname {
	font-size: 24px;
}

#hand-written-notifier-confirm-image {
	width: 320px;
}

</style>