<?xml version="1.0" encoding="windows-1250"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--
    Tworzy plik tiles-defs.xml.

    Spos�b u�ycia: java org.apache.xalan.xslt.Process -in struts-cfg.xml -xsl struts-cfg.xsl -out tiles-defs.xml

-->

<xsl:output
    method="xml"
    indent="yes"
    doctype-public="-//Apache Software Foundation//DTD Tiles Configuration 1.1//EN"
    doctype-system="http://jakarta.apache.org/struts/dtds/tiles-config_1_1.dtd"/>

<!-- domy�lna warto�� atrybutu extends, je�eli nie podano -->
<xsl:variable name="extends">index.base</xsl:variable>

<xsl:template match="/struts">

    <xsl:comment>
        Ten plik zosta� wygenerowany automatycznie. Wszelkie zmiany nale�y
        wprowadza� w pliku struts-config-source.xml.
    </xsl:comment>

    <tiles-definitions>

        <xsl:for-each select="tiles-definitions/definition">
            <xsl:copy-of select="."/>
        </xsl:for-each>

        <xsl:apply-templates/>
    </tiles-definitions>
</xsl:template>

<!-- tiles-definitions/definition -->

<xsl:template match="action">
    <xsl:for-each select="./tiles-forward">
        <xsl:element name="definition">
            <xsl:attribute name="name"><xsl:value-of select="@path"/></xsl:attribute>
            <xsl:attribute name="extends">
                <xsl:choose>
                    <xsl:when test="@extends"><xsl:value-of select="@extends"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="$extends"/></xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:for-each select="./node()">
                <xsl:copy-of select="."/>
            </xsl:for-each>
        </xsl:element>
    </xsl:for-each>
</xsl:template>

</xsl:stylesheet>