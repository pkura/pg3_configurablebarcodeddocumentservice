/* trzeba zmienic pole field_6 w dsg_doctype_3 tak aby dopuszczalo null-e  */
/* (do tej pory bylo NOT NULL default 0)                                   */
/* zapewne trzeba troche przerobic pierwsza linijke zeby zadzialalo        */

alter table DSG_DOCTYPE_3 drop constraint DF__DSG_DOCTY__FIELD__2022C2A6;
alter table DSG_DOCTYPE_3 drop column FIELD_6;
alter table DSG_DOCTYPE_3 add FIELD_6 int;