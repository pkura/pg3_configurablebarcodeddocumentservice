CREATE TABLE [DSG_DOCTYPE_3] (
    [DOCUMENT_ID] [numeric](18, 0) NOT NULL ,
    [FIELD_0] [int] NULL ,
    [FIELD_1] [int] NULL ,
    [FIELD_2] [datetime] NULL ,
    [FIELD_3] [int] NULL ,
    [FIELD_4] [int] NULL ,
    [FIELD_5] [int] NULL
) ON [PRIMARY]
GO

CREATE TABLE [daa_agent] (
    [id] [int] IDENTITY (1, 1) NOT NULL ,
    [nazwisko] [varchar] (255) COLLATE Polish_CI_AS NOT NULL ,
    [imie] [varchar] (255) COLLATE Polish_CI_AS NOT NULL ,
    [fk_daa_agencja] [int] NULL ,
    [numer] [int] NULL ,
     PRIMARY KEY  CLUSTERED
    (
        [id]
    )  ON [PRIMARY] ,
     FOREIGN KEY
    (
        [fk_daa_agencja]
    ) REFERENCES [daa_agencja] (
        [id]
    )
) ON [PRIMARY]
GO


CREATE TABLE [daa_agencja] (
    [id] [int] IDENTITY (1, 1) NOT NULL ,
    [rodzaj] [int] NULL ,
    [nazwa] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [nip] [varchar] (20) COLLATE Polish_CI_AS NULL ,
    [ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [kod] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
    [fk_rodzaj_sieci] [int] NULL ,
     PRIMARY KEY  CLUSTERED
    (
        [id]
    )  ON [PRIMARY]
) ON [PRIMARY]
GO