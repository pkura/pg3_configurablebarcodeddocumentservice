alter table DSG_DOCTYPE_3 add FIELD_6 int NOT NULL default 0;
update DSG_DOCTYPE_3 set field_3=340 where field_3 in (350,360,370);

/* Dodanie foreign-key na dsg_doctype_3 - jesli do tej pory istnieja niespojnosci to ponizsze    */
/* zapytania moge sie nie powiesc. Mozna albo wszystkim dokumenty przypisac na field_4 i field_5 */
/* NULL-e albo na jakiegos "dobrego" agenta i/lub agencje.                                       */
/* Nastepujace zapytania pozwalaja sie dowiedziec gdzie wystepuje niespojnosc: 
    select * from dsg_doctype_3 d left join daa_agent a on d.field_4=a.id where a.imie is null and field_4 is not null;
    select * from dsg_doctype_3 d left join daa_agencja a on d.field_5=a.id where a.nazwa is null and field_5 is not null;
*/

alter table DSG_DOCTYPE_3 add FOREIGN KEY
    (
        [field_4]
    ) REFERENCES [daa_agent] (
        [id]
    );

alter table DSG_DOCTYPE_3 add FOREIGN KEY
    (
        [field_5]
    ) REFERENCES [daa_agencja] (
        [id]
    );

