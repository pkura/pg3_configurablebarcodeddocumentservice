alter table ds_datamart add event_id_tmp numeric(18,0) NOT NULL;
update ds_datamart set event_id_tmp = event_id;
alter table ds_datamart drop event_id;
alter table ds_datamart alter column event_id_tmp to event_id;
alter table ds_datamart alter column document_id type numeric(18,0);
ALTER TABLE ds_datamart ADD PRIMARY KEY (event_id);