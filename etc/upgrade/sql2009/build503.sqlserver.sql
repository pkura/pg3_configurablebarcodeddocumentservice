alter table dsw_tasklist alter column DSO_DOCUMENT_OFFICENUMBER varchar(50);
drop index dsw_jbpm_tasklist.dsw_jbpm_tasklist_4;
alter table DSW_JBPM_TASKLIST alter column OFFICE_NUMBER varchar(50);
create index dsw_jbpm_tasklist_4 on dsw_jbpm_tasklist (office_number);
