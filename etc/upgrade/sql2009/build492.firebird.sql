--@ignore-exceptions;
CREATE TABLE dso_email_channel
(
    id integer NOT NULL primary key,
    pop3_host varchar(100) NOT NULL,
    pop3_username varchar(100) NOT NULL,
    pop3_password varchar(50) NOT NULL,
    folder_name varchar(50) ,
    kolejka varchar(100) ,
    ctime timestamp NOT NULL,
    warunki varchar(1000) ,
    content_type varchar(50) NOT NULL
);
--@ignore-exceptions;
CREATE generator dso_email_channel_id;