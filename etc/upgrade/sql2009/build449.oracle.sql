create table ds_new_report
(
    id          				integer primary key,
    report_cn					varchar2(50 char) not null,
	ctime 						timestamp not null,
	start_process_time			timestamp,
	finish_process_time			timestamp,
	username 					varchar2(50 char) not null,
	status 						integer not null,
	criteria					blob,
	title						varchar2(100 char) not null,
	description					varchar2(300 char)
);

create sequence ds_new_report_id;

create index i_ds_new_report_1 on ds_new_report (status);
create index i_ds_new_report_2 on ds_new_report (username);
create index i_ds_new_report_3 on ds_new_report (report_cn);
create index i_ds_new_report_4 on ds_new_report (ctime);
create index i_ds_new_report_5 on ds_new_report (finish_process_time);