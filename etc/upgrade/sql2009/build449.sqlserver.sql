create table ds_new_report
(
    id          				integer identity(1,1) primary key,
    report_cn					varchar(50) not null,
	ctime 						datetime not null,
	start_process_time			datetime,
	finish_process_time			datetime,
	username 					varchar(50) not null,
	status 						integer not null,
	criteria					image,
	title						varchar(100) not null,
	description					varchar(300)
);

create index i_ds_new_report_1 on ds_new_report (status);
create index i_ds_new_report_2 on ds_new_report (username);
create index i_ds_new_report_3 on ds_new_report (report_cn);
create index i_ds_new_report_4 on ds_new_report (ctime);
create index i_ds_new_report_5 on ds_new_report (finish_process_time);