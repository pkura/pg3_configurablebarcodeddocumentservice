create table dso_channel_request (
    id          				numeric(19,0) identity(1,1) not null,
    content						varchar(500) collate SQL_Polish_Cp1250_CS_AS,
	incoming_date				datetime not null,
	status						smallint not null,
	request_comment				varchar(500) collate SQL_Polish_Cp1250_CS_AS,
	process_date				datetime
);