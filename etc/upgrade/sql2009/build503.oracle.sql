alter table dsw_tasklist add ko_tmp varchar(50 char);
update dsw_tasklist set ko_tmp = DSO_DOCUMENT_OFFICENUMBER;
update dsw_tasklist set DSO_DOCUMENT_OFFICENUMBER = null;
alter table dsw_tasklist modify DSO_DOCUMENT_OFFICENUMBER varchar(50 char);
update dsw_tasklist set DSO_DOCUMENT_OFFICENUMBER = ko_tmp;
alter table dsw_tasklist drop column ko_tmp;

alter table DSW_JBPM_TASKLIST add ko_tmp varchar(50 char);
update DSW_JBPM_TASKLIST set ko_tmp = OFFICE_NUMBER;
update DSW_JBPM_TASKLIST set OFFICE_NUMBER = null;
alter table DSW_JBPM_TASKLIST modify OFFICE_NUMBER varchar(50 char);
update DSW_JBPM_TASKLIST set OFFICE_NUMBER = ko_tmp;
alter table DSW_JBPM_TASKLIST drop column ko_tmp;