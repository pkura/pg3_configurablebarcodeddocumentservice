
create table ds_datamart
(
	event_id integer,
	document_id int,
	process_code varchar2(30), 
	event_code varchar2(30), 
	event_date timestamp, 
	change_field_cn varchar2(50), 
	old_value varchar2(200), 
	new_value varchar2(200), 
	username varchar2(50),
	"session" varchar2(50)
);

create sequence ds_datamart_id; 