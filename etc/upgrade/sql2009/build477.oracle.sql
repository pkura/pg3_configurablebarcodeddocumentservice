
CREATE TABLE ds_key_store
(
	id numeric(18, 0) primary key NOT NULL,
	keyCode varchar2(50 char) NOT NULL,
	username varchar2(50 char) NOT NULL,
	publicKey varchar2(2000 char) NOT NULL,
	privateKey varchar2(2000 char) NOT NULL,
	status int NOT NULL,
	encryptedKey varchar2(1000 char) NULL,
	ctime timestamp NOT NULL,
	grantedFrom varchar2(50 char) NULL
);

create sequence ds_key_store_id;