alter table dso_in_document add dailyOfficeNumber integer;
alter table  dso_out_document add dailyOfficeNumber integer;
alter table  dso_order add dailyOfficeNumber integer;
alter table  dso_journal add "day" timestamp;
alter table  dso_journal add dailySequenceId integer;
alter table  dso_journal_entry add dailySequenceId integer;


