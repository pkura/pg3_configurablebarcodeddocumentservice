create table archive_ds_document_changelog
(
 document_id integer,
 username varchar(62),
 ctime datetime,
 what varchar(100)
);

create index i_a_d_c_1 on archive_ds_document_changelog (document_id);