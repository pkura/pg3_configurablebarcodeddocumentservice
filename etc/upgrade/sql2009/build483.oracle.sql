create table archive_ds_document_changelog
(
 document_id integer,
 username varchar2(62),
 ctime date,
 what varchar2(100)
);

create index i_a_d_c_1 on archive_ds_document_changelog (document_id);