--@ignore-exceptions;
create table ds_datamart
(
	event_id integer identity (1,1),
	document_id int,
	process_code varchar(30), 
	event_code varchar(30), 
	event_date datetime, 
	change_field_cn varchar(50), 
	old_value varchar(200), 
	new_value varchar(200), 
	username varchar(50),
	session varchar(50)
);