alter table ds_box_order add orderAspect varchar(20) default 'default';
alter table ds_box_order add orderSendDate datetime;
alter table ds_box_order add orderReceiveDate datetime;
alter table ds_box_order add orderReturnDate datetime;
alter table ds_box_order add orderReturnConfirmDate datetime;