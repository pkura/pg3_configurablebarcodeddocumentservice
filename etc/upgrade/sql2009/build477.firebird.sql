
CREATE TABLE ds_key_store
(
	id numeric(18, 0) primary key NOT NULL,
	keyCode varchar(50) NOT NULL,
	username varchar(50) NOT NULL,
	publicKey varchar(2000) NOT NULL,
	privateKey varchar(2000) NOT NULL,
	status int NOT NULL,
	encryptedKey varchar(1000),
	ctime timestamp NOT NULL,
	grantedFrom varchar(50)
);
create generator ds_key_store_id;