
ALTER TABLE ds_datamart ADD object_id varchar(30);

CREATE INDEX object_id_index ON ds_datamart(object_id);
