alter table dsw_tasklist add ko_tmp numeric(18,0);
update dsw_tasklist set ko_tmp = DSO_DOCUMENT_OFFICENUMBER;
update dsw_tasklist set DSO_DOCUMENT_OFFICENUMBER = null;
alter table dsw_tasklist modify DSO_DOCUMENT_OFFICENUMBER numeric(18,0);
update dsw_tasklist set DSO_DOCUMENT_OFFICENUMBER = ko_tmp;
alter table dsw_tasklist drop column ko_tmp;

alter table DSW_JBPM_TASKLIST add ko_tmp numeric(18,0);
update DSW_JBPM_TASKLIST set ko_tmp = OFFICE_NUMBER;
update DSW_JBPM_TASKLIST set OFFICE_NUMBER = null;
alter table DSW_JBPM_TASKLIST modify OFFICE_NUMBER numeric(18,0);
update DSW_JBPM_TASKLIST set OFFICE_NUMBER = ko_tmp;
alter table DSW_JBPM_TASKLIST drop column ko_tmp;

alter table  DSW_JBPM_TASKLIST add dailyOfficeNumber integer;
alter table  DSW_TASKLIST add dailyOfficeNumber integer;