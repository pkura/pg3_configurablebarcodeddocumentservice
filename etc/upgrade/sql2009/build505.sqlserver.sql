alter table dsw_tasklist alter column DSO_DOCUMENT_OFFICENUMBER numeric(18,0);
drop index dsw_jbpm_tasklist.dsw_jbpm_tasklist_4;
alter table DSW_JBPM_TASKLIST alter column OFFICE_NUMBER numeric(18,0);
create index dsw_jbpm_tasklist_4 on dsw_jbpm_tasklist (office_number);

alter table  DSW_JBPM_TASKLIST add dailyOfficeNumber integer;
alter table  DSW_TASKLIST add dailyOfficeNumber integer;
