alter table ds_new_report add process_date timestamp;
alter table ds_new_report add remote_key varchar2(100 char);

create index i_ds_new_report_6 on ds_new_report (process_date);
create index i_ds_new_report_7 on ds_new_report (remote_key);