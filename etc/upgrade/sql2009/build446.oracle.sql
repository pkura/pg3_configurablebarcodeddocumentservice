create table dso_channel_request (
    id          				integer primary key not null,
    content						varchar2(500 char),
	incoming_date				timestamp not null,
	status						smallint not null,
	request_comment				varchar2(500 char),
	process_date				timestamp
);

create sequence dso_channel_request_id;