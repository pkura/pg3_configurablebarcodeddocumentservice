create table ic_customs_agency (
    id          				integer not null,
    title						varchar(255)
);

alter table dsg_centrum_kosztow_faktury add customsAgencyId integer;
alter table dsg_account_number add discriminator integer;
