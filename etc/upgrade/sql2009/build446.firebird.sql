create table dso_channel_request (
    id          				numeric(18,0) not null,
    content						varchar(500),
	incoming_date				date not null,
	status						smallint not null,
	request_comment				varchar(500),
	process_date				date
);

create generator dso_channel_request_id;