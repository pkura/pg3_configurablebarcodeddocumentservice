--@ignore-exceptions;
create table dsr_con_contract_history 
(
    id numeric(18,0) primary key,
    contract_id numeric(18,0) not null,
    change_date timestamp not null,
    who_change numeric(18,0) not null,
    description VARCHAR(1000)
);

--@ignore-exceptions;
CREATE TABLE DSR_CON_CONTRACT (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    HVERSION                INTEGER NOT NULL,
    SEQUENCEID              INTEGER NOT NULL,
    CREATE_YEAR             INTEGER NOT NULL,
    CONTRACTDATE            timestamp NOT NULL,
    CONTRACTKIND            VARCHAR(20) NOT NULL,
    CONTRACTNO              VARCHAR(50),
    VOUCHERNO               VARCHAR(50),
    GROSS                   NUMERIC(18,2) NOT NULL,
    DESCRIPTION             VARCHAR(160) NOT NULL,
    STARTTERM               timestamp,
    ENDTERM                 timestamp,
    VENDOR_ID               NUMERIC(18,0) NOT NULL,
    DELETED                 SMALLINT NOT NULL,
    REMARKS                 VARCHAR(512),
    contractstatus          VARCHAR(25),
    LPARAM                  VARCHAR(200),
    WPARAM                  NUMERIC(18,0),
    document_id             numeric(18,0),
    author                  VARCHAR(62)
);

--CREATE UNIQUE INDEX DSR_CON_CONTRACT_SEQ ON DSR_CON_CONTRACT(CREATE_YEAR, SEQUENCEID);

--@ignore-exceptions;
create sequence GENERATOR DSR_CON_CONTRACT_ID;

--@ignore-exceptions;
CREATE TABLE DSR_CON_CONTRACT_AUDIT (
    CONTRACT_ID          NUMERIC(18,0) NOT NULL,
    POSN                INTEGER NOT NULL,
    CTIME               TIMESTAMP NOT NULL,
    PROPERTY            VARCHAR(62),
    USERNAME            VARCHAR(62)  NOT NULL,
    DESCRIPTION         VARCHAR(200)  NOT NULL,
    LPARAM              VARCHAR(200) ,
    WPARAM              NUMERIC(18,0),
    PRIORITY            INTEGER
);

--@ignore-exceptions;
ALTER TABLE DSR_CON_CONTRACT_AUDIT ADD CONSTRAINT DSR_CON_CONTRACT_AUDIT_FK1
FOREIGN KEY (CONTRACT_ID) REFERENCES DSR_CON_CONTRACT (ID);

--@ignore-exceptions;
CREATE TABLE DSR_CON_VENDOR (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    NAME                    VARCHAR(160) NOT NULL,
    ZIP                     VARCHAR(14),
    STREET                  VARCHAR(50),
    LOCATION                VARCHAR(50),
    NIP                     VARCHAR(15),
    REGON                   VARCHAR(15),
    COUNTRY                 VARCHAR(2),
    PHONE                   VARCHAR(30),
    FAX                     VARCHAR(30),
    LPARAM                  VARCHAR(200) ,
    WPARAM                  NUMERIC(18,0)
);

--@ignore-exceptions;
create sequence  GENERATOR DSR_CON_VENDOR_ID;

--@ignore-exceptions;
ALTER TABLE DSR_CON_CONTRACT ADD CONSTRAINT DSR_CON_CONTRACT_VENDOR_FK
FOREIGN KEY (VENDOR_ID) REFERENCES DSR_CON_VENDOR (ID);


--@ignore-exceptions;
alter table dsr_con_contract add constraint id_pk primary key(id);
--@ignore-exceptions;
alter table dsr_con_contract_history add constraint dsr_con_contract_history_fk foreign key(contract_id) references dsr_con_contract(id);
commit;
create sequence dsr_con_contract_history_id;

-- okres finansowania (dla SZPZLO Warszawa-Brodno)
ALTER TABLE DSR_CON_CONTRACT ADD periodf_from date;
ALTER TABLE DSR_CON_CONTRACT ADD periodf_to date;