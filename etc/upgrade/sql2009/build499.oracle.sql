--@ignore-exceptions;
create table dsr_con_contract_history 
(
    id numeric(18,0) primary key,
    contract_id numeric(18,0) not null,
    change_date timestamp not null,
    user_id numeric(18,0) not null,
    description varchar2(1000 char)
);

--@ignore-exceptions;
CREATE TABLE DSR_CON_CONTRACT (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    HVERSION                INTEGER NOT NULL,
    SEQUENCEID              INTEGER NOT NULL,
    CREATE_YEAR             INTEGER NOT NULL,
    CONTRACTDATE            timestamp NOT NULL,
    CONTRACTKIND            VARCHAR2(20) NOT NULL,
    CONTRACTNO              VARCHAR2(50),
    VOUCHERNO               VARCHAR2(50),
    GROSS                   NUMERIC(18,2) NOT NULL,
    DESCRIPTION             VARCHAR2(160) NOT NULL,
    STARTTERM               timestamp,
    ENDTERM                 timestamp,
    VENDOR_ID               NUMERIC(18,0) NOT NULL,
    DELETED                 SMALLINT NOT NULL,
    REMARKS                 VARCHAR2(512 char),
    contractstatus          VARCHAR2(25 char),
    LPARAM                  VARCHAR2(200 char),
    WPARAM                  NUMERIC(18,0),
    document_id             numeric(18,0),
    author                  VARCHAR2(62 char)
);

--CREATE UNIQUE INDEX DSR_CON_CONTRACT_SEQ ON DSR_CON_CONTRACT(CREATE_YEAR, SEQUENCEID);

--@ignore-exceptions;
create sequence GENERATOR DSR_CON_CONTRACT_ID;

--@ignore-exceptions;
CREATE TABLE DSR_CON_CONTRACT_AUDIT (
    CONTRACT_ID          NUMERIC(18,0) NOT NULL,
    POSN                INTEGER NOT NULL,
    CTIME               TIMESTAMP NOT NULL,
    PROPERTY            VARCHAR2(62 char),
    USERNAME            VARCHAR2(62 char)  NOT NULL,
    DESCRIPTION         VARCHAR2(200 char)  NOT NULL,
    LPARAM              VARCHAR2(200 char) ,
    WPARAM              NUMERIC(18,0),
    PRIORITY            INTEGER
);

--@ignore-exceptions;
ALTER TABLE DSR_CON_CONTRACT_AUDIT ADD CONSTRAINT DSR_CON_CONTRACT_AUDIT_FK1
FOREIGN KEY (CONTRACT_ID) REFERENCES DSR_CON_CONTRACT (ID);

--@ignore-exceptions;
CREATE TABLE DSR_CON_VENDOR (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    NAME                    VARCHAR2(160 char) NOT NULL,
    ZIP                     VARCHAR2(14 char),
    STREET                  VARCHAR2(50 char),
    LOCATION                VARCHAR2(50 char),
    NIP                     VARCHAR2(15 char),
    REGON                   VARCHAR2(15 char),
    COUNTRY                 VARCHAR2(2 char),
    PHONE                   VARCHAR2(30 char),
    FAX                     VARCHAR2(30 char),
    LPARAM                  VARCHAR2(200 char) ,
    WPARAM                  NUMERIC(18,0)
);

--@ignore-exceptions;
create sequence  GENERATOR DSR_CON_VENDOR_ID;

--@ignore-exceptions;
ALTER TABLE DSR_CON_CONTRACT ADD CONSTRAINT DSR_CON_CONTRACT_VENDOR_FK
FOREIGN KEY (VENDOR_ID) REFERENCES DSR_CON_VENDOR (ID);


--@ignore-exceptions;
alter table dsr_con_contract add constraint id_pk primary key(id);
--@ignore-exceptions;
alter table dsr_con_contract_history add constraint dsr_con_contract_history_fk foreign key(contract_id) references dsr_con_contract(id);

create sequence dsr_con_contract_history_id;