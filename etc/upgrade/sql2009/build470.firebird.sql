CREATE TABLE ds_anonymous_access_ticket(
	id numeric(18, 0) PRIMARY KEY,
	document_id numeric(18, 0),
	attachment_id numeric(18, 0),
	attachment_revision_id numeric(18, 0),
	code varchar(200) NOT NULL,
	ctime timestamp NOT NULL,
	valid_till timestamp,
	access_count numeric(18, 0) NOT NULL,
	max_access numeric(18, 0) NOT NULL,
	creator varchar(50) NOT NULL,
	return_method varchar(50)
);

create generator ds_anonymous_access_ticket_id;

create index i_ds_anonymous_access_ticket_1 on ds_anonymous_access_ticket (document_id);
create index i_ds_anonymous_access_ticket_2 on ds_anonymous_access_ticket (attachment_id);
create index i_ds_anonymous_access_ticket_3 on ds_anonymous_access_ticket (attachment_revision_id);
create index i_ds_anonymous_access_ticket_4 on ds_anonymous_access_ticket (code);