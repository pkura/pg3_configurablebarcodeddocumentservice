--@ignore-exceptions;
alter table ds_datamart alter column event_id numeric(18,0);
alter table ds_datamart alter column document_id numeric(18,0);
alter table ds_datamart ADD CONSTRAINT pk_ds_datamart PRIMARY KEY(event_id)