alter table ds_box_order add orderAspect varchar2(20 char) default 'default';
alter table ds_box_order add orderSendDate date;
alter table ds_box_order add orderReceiveDate date;
alter table ds_box_order add orderReturnDate date;
alter table ds_box_order add orderReturnConfirmDate date;