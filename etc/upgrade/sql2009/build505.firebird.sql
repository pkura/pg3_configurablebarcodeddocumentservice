alter table dsw_tasklist add ko_tmp numeric(18,0);
update dsw_tasklist set ko_tmp = DSO_DOCUMENT_OFFICENUMBER;
alter table dsw_tasklist drop DSO_DOCUMENT_OFFICENUMBER;
alter table dsw_tasklist ALTER  ko_tmp to  DSO_DOCUMENT_OFFICENUMBER;

alter table DSW_JBPM_TASKLIST add ko_tmp numeric(18,0);
update DSW_JBPM_TASKLIST set ko_tmp = OFFICE_NUMBER;
--@ignore-exceptions;
drop index dsw_jbpm_tasklist_4;
alter table DSW_JBPM_TASKLIST drop OFFICE_NUMBER;
alter table DSW_JBPM_TASKLIST ALTER  ko_tmp to OFFICE_NUMBER;
create index dsw_jbpm_tasklist_4 on dsw_jbpm_tasklist (office_number);

alter table  DSW_JBPM_TASKLIST add dailyOfficeNumber integer;
alter table  DSW_TASKLIST add dailyOfficeNumber integer;