--@ignore-exceptions;
create table dsr_con_contract_history 
(
    id numeric(18,0) primary key,
    contract_id numeric(18,0) not null,
    change_date timestamp not null,
    user_id numeric(18,0) not null,
    description varchar(1000)
);

alter table dsr_con_contract_history add constraint dsr_con_contract_history_fk foreign key(contract_id) references dsr_con_contract(id);
create generator dsr_con_contract_history_id;
create index dsr_con_contract_history_index on dsr_con_contract_history(id);
