UPDATE ds_postal_code SET city=('^Z' || SUBSTR(CITY, 3)) WHERE SUBSTR(CITY, 1,  2) = '^z';
UPDATE ds_postal_code SET city=('^S' || SUBSTR(CITY, 3)) WHERE SUBSTR(CITY, 1,  2) = '^s';
UPDATE ds_postal_code SET city=('^C' || SUBSTR(CITY, 3)) WHERE SUBSTR(CITY, 1,  2) = '^c';
UPDATE ds_postal_code SET city=('^L' || SUBSTR(CITY, 3)) WHERE SUBSTR(CITY, 1,  2) = '^l';
UPDATE ds_postal_code SET city=('^X' || SUBSTR(CITY, 3)) WHERE SUBSTR(CITY, 1,  2) = '^x';