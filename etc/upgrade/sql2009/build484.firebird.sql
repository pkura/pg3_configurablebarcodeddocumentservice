CREATE GENERATOR dso_document_remarks_ID;
alter table dso_document_remarks add id numeric(18,0) not null;
update dso_document_remarks set id = gen_id(dso_document_remarks_ID,1);
ALTER TABLE dso_document_remarks ADD CONSTRAINT dso_document_remarks_ID_PK PRIMARY KEY (id);
alter table dso_document_remarks drop posn;