create table archive_ds_document_changelog
(
 document_id numeric(18, 0),
 username varchar(62),
 ctime timestamp,
 what varchar(100)
);

create index i_a_d_c_1 on archive_ds_document_changelog (document_id);