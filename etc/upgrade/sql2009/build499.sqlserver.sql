--@ignore-exceptions;
create table dsr_con_contract_history 
(
    id numeric(18,0) identity primary key,
    contract_id numeric(18,0) not null,
    change_date datetime not null,
    user_id numeric(18,0) not null,
    description varchar(1000)
);
--@ignore-exceptions;
alter table dsr_con_contract_history add constraint dsr_con_contract_history_fk foreign key(contract_id) references dsr_con_contract(id);
--@ignore-exceptions;
create index dsr_con_contract_history_index on dsr_con_contract_history(id); 