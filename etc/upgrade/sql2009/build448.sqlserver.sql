create table ic_customs_agency (
    id          				integer identity(1,1) not null,
    title						varchar(255) collate SQL_Polish_Cp1250_CS_AS
);
alter table dsg_centrum_kosztow_faktury add customsAgencyId integer;
alter table dsg_account_number add discriminator integer;