alter table ds_key_store alter column publicKey type varchar(2000) ;
alter table ds_key_store alter column privateKey type varchar(2000) ;
alter table ds_key_store add discriminator varchar(30);