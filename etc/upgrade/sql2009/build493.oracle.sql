alter table ds_key_store modify publicKey varchar2(2000 char) null;
alter table ds_key_store modify privateKey varchar2(2000 char) null;
alter table ds_key_store add discriminator varchar2(30 char) null;