alter table ds_box_order add orderAspect varchar(20) default 'default';
alter table ds_box_order add orderSendDate timestamp;
alter table ds_box_order add orderReceiveDate timestamp;
alter table ds_box_order add orderReturnDate timestamp;
alter table ds_box_order add orderReturnConfirmDate timestamp;