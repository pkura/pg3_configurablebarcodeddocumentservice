alter table ds_key_store alter column publicKey varchar(2000) null;
alter table ds_key_store alter column privateKey varchar(2000) null;
alter table ds_key_store add discriminator varchar(30) null;