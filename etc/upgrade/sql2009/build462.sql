ALTER TABLE dsg_account_number ADD visible_in_system smallint DEFAULT 1 NOT NULL;
UPDATE dsg_account_number
SET visible_in_system = 0
WHERE account_number IN ('081-38','081-47','081-52','081-56','081-57','081-58');