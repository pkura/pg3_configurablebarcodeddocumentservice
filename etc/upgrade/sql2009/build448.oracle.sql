create table ic_customs_agency (
    id          				integer primary key not null,
    title						varchar2(255 char)
);

alter table dsg_centrum_kosztow_faktury add customsAgencyId integer;
alter table dsg_account_number add discriminator integer;