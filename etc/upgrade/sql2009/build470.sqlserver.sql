CREATE TABLE ds_anonymous_access_ticket(
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[document_id] [numeric](19, 0) NULL,
	[attachment_id] [numeric](19, 0) NULL,
	[attachment_revision_id] [numeric](19, 0) NULL,
	[code] [varchar](200) NOT NULL,
	[ctime] [datetime] NOT NULL,
	[valid_till] [datetime] NULL,
	[access_count] [numeric](18, 0) NOT NULL,
	[max_access] [numeric](18, 0) NOT NULL,
	[creator] [varchar](50) NOT NULL,
	[return_method] [varchar](50) NULL
);

create index i_ds_anonymous_access_ticket_1 on ds_anonymous_access_ticket (document_id);
create index i_ds_anonymous_access_ticket_2 on ds_anonymous_access_ticket (attachment_id);
create index i_ds_anonymous_access_ticket_3 on ds_anonymous_access_ticket (attachment_revision_id);
create index i_ds_anonymous_access_ticket_4 on ds_anonymous_access_ticket (code);