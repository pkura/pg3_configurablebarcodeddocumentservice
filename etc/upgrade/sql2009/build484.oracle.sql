alter table dso_document_remarks add id numeric(18,0);
create sequence dso_document_remarks_ID;
update dso_document_remarks set id = dso_document_remarks_ID.nextval;
alter table dso_document_remarks modify id numeric(18,0) not null PRIMARY KEY ;
alter table dso_document_remarks drop column posn;