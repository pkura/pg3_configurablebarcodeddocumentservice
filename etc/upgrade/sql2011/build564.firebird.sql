
CREATE TABLE ds_electronic_signature(
    id numeric(18, 0) NOT NULL,
	fk_id numeric(18, 0) NOT NULL,
	fk_type_id smallint NOT NULL,	
	persistence_type_id smallint NOT NULL,
	date_ TIMESTAMP NOT NULL,	
	signed_file BLOB,
	signature_file BLOB,
	uri varchar(200)
 );
CREATE GENERATOR ds_electronic_signature_id;
 

