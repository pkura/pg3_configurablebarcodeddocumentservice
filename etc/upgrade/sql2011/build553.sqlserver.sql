create table ds_agent_user_stats (
    browser_name    varchar(16),
    browser_id		varchar(16),
    width           Int,
    height			Int
);