create table ds_fax_cache
(
id numeric(18,0) identity(1,1)  primary key not null,
ctime datetime,
fileName varchar(100),
documentId numeric(18,0)
);