
CREATE TABLE [ds_electronic_signature](
    id numeric(19, 0) IDENTITY(1,1),
	[fk_id] [numeric](19, 0) NOT NULL,
	[fk_type_id] [smallint] NOT NULL,
	
	[persistence_type_id] [smallint] NOT NULL,
	[date_] [datetime] NOT NULL,
	
	[signed_file] [text] NULL,
	[signature_file] [text] NULL,
	[uri] [varchar](200) NULL,
	
	PRIMARY KEY (id)
 )
 
 CREATE TABLE ds_case_status
(
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

 CREATE TABLE ds_case_type
(
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);