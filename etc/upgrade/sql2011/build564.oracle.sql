
CREATE TABLE ds_electronic_signature(
    id numeric(18, 0) NOT NULL,
	fk_id numeric(18, 0) NOT NULL,
	fk_type_id smallint NOT NULL,
	
	persistence_type_id smallint NOT NULL,
	date_ TIMESTAMP  NOT NULL,
	
	signed_file clob NULL,
	signature_file clob NULL,
	uri varchar2(200) NULL,
	
	PRIMARY KEY (id)
 );

 create sequence ds_electronic_signature_seq;

