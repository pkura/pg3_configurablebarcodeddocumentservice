CREATE TABLE ds_process_action_entry
(
    id numeric(18,0) NOT NULL,    
	username varchar(62) NOT NULL,
	date_ date NOT NULL,
	document_id numeric(18,0),
	process_id varchar(255),
	process_name varchar(255),	
	action_name  varchar(255)
 );
CREATE GENERATOR ds_process_action_entry_id;

