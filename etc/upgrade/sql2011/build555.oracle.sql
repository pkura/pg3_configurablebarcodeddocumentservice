CREATE TABLE DSO_INVOICES_CPV_CODES
(
  ID NUMBER NOT NULL 
, NAME VARCHAR2(20) 
, CATEGORY VARCHAR2(40) 
);

CREATE SEQUENCE DSO_INVOICES_CPV_CODES_ID start with 1 nocache;


CREATE TABLE DSO_INVOICES_DECRETATION
(
  ID NUMBER NOT NULL 
, NAME VARCHAR2(100) 
);

CREATE SEQUENCE DSO_INVOICES_DECRETATION_ID start with 1 nocache;