create table DSO_INVOICES_CPV_CODES 
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	NAME VARCHAR(40),
	CATEGORY VARCHAR(40)
);

create table DSO_INVOICES_DECRETATION
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	NAME VARCHAR(100),
);