alter table ds_calendar_event add 
periodic smallint not null default 0, 
periodKind numeric(2) not null default 0, 
periodLength numeric(18), 
periodEnd datetime, 
periodParentId numeric(18);