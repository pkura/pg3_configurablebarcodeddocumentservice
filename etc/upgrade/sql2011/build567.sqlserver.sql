
CREATE TABLE [ds_process_action_entry](
    id numeric(19, 0) IDENTITY(1,1),

	[username] [varchar](62) NOT NULL,
	[date_] [datetime] NOT NULL,
	
	[document_id] [numeric](19,0) NULL,
	[process_id] [varchar](255) NULL,
	[process_name] [varchar](255) NULL,
	[action_name] [varchar](255) NULL,

	PRIMARY KEY (id)
 )
