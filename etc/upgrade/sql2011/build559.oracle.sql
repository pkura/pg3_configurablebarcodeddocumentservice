create table ds_fax_cache
(
id numeric(18,0),
ctime timestamp,
fileName varchar2(100),
documentId numeric(18,0)
);

create sequence ds_fax_cache_id;