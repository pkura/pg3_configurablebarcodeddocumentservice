create table ds_fax_cache
(
id numeric(18,0),
ctime timestamp,
fileName varchar(100),
documentId numeric(18,0)
);

create generator ds_fax_cache_id;