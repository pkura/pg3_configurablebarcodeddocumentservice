
CREATE TABLE ds_process_action_entry(
    id numeric(18, 0) NOT NULL,

	username varchar(62) NOT NULL,
	date_ timestamp NOT NULL,

	document_id  numeric(19,0) NULL,
	process_id   varchar2(255) NULL,
	process_name varchar2(255) NULL,
	action_name  varchar2(255) NULL,

	PRIMARY KEY (id)
 );

 create sequence ds_process_action_entry_seq;

