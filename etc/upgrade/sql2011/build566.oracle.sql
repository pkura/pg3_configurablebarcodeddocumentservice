alter table ds_calendar_event add (
	periodic number(1) default 0 not null, 
	periodKind number(2) default 0 not null, 
	periodLength number(18), 
	periodEnd date, 
	periodParentId number(18)
);
