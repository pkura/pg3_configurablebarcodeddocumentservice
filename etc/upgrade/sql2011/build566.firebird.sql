alter table ds_calendar_event 
add	periodic smallint default 0 not null, 
add	periodKind numeric(2,0) default 0 not null , 
add	periodLength numeric(18), 
add	periodEnd date, 
add	periodParentId numeric(18);