create table dsr_project
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	projectManager VARCHAR(50),
	projectName VARCHAR(150),
	nrIFPAN VARCHAR(50),
	startDate datetime,
	finishDate datetime
);

create table dsr_project_entry
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	projectId numeric(18,0),
	dateEntry datetime,
	expenditureDate datetime,
	costName VARCHAR(50),
	costKindId numeric(18,0),
	gross float,
	net float,
	vat int,
	entryType VARCHAR(50),	
	docNr numeric(18,0),
	demandNr numeric(18,0),
	description VARCHAR(255)
);

create table dsr_project_costkind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	NAME VARCHAR(40),
	CATEGORY VARCHAR(40)
);