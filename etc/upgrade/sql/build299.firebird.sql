CREATE TABLE DSW_PROCESS_COORDINATOR (
    id			numeric(18,0) not null,
    documentKindCn      varchar(20) not null,
    name	        varchar(100),
    username            varchar(62) not null,
    type                varchar(50)
);
CREATE GENERATOR DSW_PROCESS_COORDINATOR_ID;
