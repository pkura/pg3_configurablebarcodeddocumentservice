create table dso_drk_mail_sender
(
	ID numeric(19,0) NOT NULL IDENTITY(1,1) PRIMARY KEY,
	FLAG_ID numeric(19,0),
	DOCUMENT_ID numeric(19,0),
	NR_POLISY varchar(10),
	MAILED int
);