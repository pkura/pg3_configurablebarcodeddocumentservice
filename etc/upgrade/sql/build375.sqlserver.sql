alter table dsw_process_coordinator add channelCn varchar(20);
 
alter table dsw_process_coordinator add guid varchar(100);
alter table dsw_process_coordinator drop column username;
alter table dsw_process_coordinator add username varchar(30);

alter table dsw_process_coordinator drop column name;

alter table dsw_process_coordinator drop column type;

alter table dsw_process_coordinator drop column enumfield;