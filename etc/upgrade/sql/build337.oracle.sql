create table dsg_account_number (
    account_number varchar2(50) primary key
);

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW 
(
	id 			integer primary key,
	name 			varchar2(100 char),
	symbol 			varchar2(50 char),
	username 		varchar2(62 char),
    divisionGuid 	varchar2(62 char)
);
--@ignore-exceptions;
CREATE SEQUENCE DSG_CENTRUM_KOSZTOW_ID start with 1 nocache;

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW_FAKTURY (
	id              integer NOT NULL primary key,
	centrumId       integer,
	centrumCode     varchar2(100 char),
	accountNumber   varchar2(50 char),
	amount          float,
        documentId      integer
);
--@ignore-exceptions;
CREATE SEQUENCE DSG_CENTRUM_KOSZTOW_FAKTURY_ID start with 1 nocache;