create table ic_fixed_assets_group(
	id INTEGER NOT NULL primary key,
	title VARCHAR(255) NOT NULL
);

create table ic_fixed_assets_subgroup(
	id INTEGER NOT NULL primary key,
	group_id INT NOT NULL,
	title VARCHAR(255) NOT NULL
);

alter table dsg_centrum_kosztow_faktury add subgroupId INTEGER;
alter table dsg_centrum_kosztow_faktury add storagePlace VARCHAR(300);