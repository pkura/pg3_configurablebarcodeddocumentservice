CREATE TABLE DSW_JBPM_PROCESS_DEFINITION(
    id					numeric(18,0) not null,
	definitionname	    varchar(62),
    definitionid        numeric(18,0),
	nameforuser			varchar(100),
	page				varchar(200),
    definitionversion   integer
);
CREATE GENERATOR DSW_JBPM_PROCESS_DEFINITION_ID;
