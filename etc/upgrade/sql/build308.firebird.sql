alter table dsw_tasklist add documentClerk varchar(62);
alter table dsw_tasklist add answerCounter integer;
alter table dsw_tasklist add incomingDate timestamp;

create index dso_out_document_indocument on dso_out_document (indocument);