drop table ds_box_order;

create table ds_box_order(
	id			numeric(18,0),
	remark		varchar(300),
	documentId	numeric(18,0),
	priority	int,
	status		int,
	orderDate	timestamp,
	traceNumber varchar(50),
	location	numeric(18,0)
);

--CREATE GENERATOR ds_box_order_ID;

drop table ds_box_order_history;

create table ds_box_order_history(
	id			numeric(18,0),
	orderId		numeric(18,0),
	valueName	varchar(100),
	"value"		varchar(300),
	ctime		timestamp,
	username	varchar(50)
);

--CREATE GENERATOR ds_box_order_history_ID;