create table ds_subst_hist(
    id			INT IDENTITY (1, 1) not null,
    login		varchar(62) not null,
    substitutedFrom	datetime not null,
    substitutedThrough	datetime not null,
    substLogin		varchar(62) not null
);
