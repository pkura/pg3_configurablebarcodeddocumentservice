CREATE TABLE DSW_JBPM_SWIMLANE_MAPPING(
    id					numeric(18,0) not null,
	name	            varchar(62),
    divisionGuid        varchar(62),
	lastUserNo          integer
);
CREATE GENERATOR DSW_JBPM_SWIMLANE_MAPPING_ID;
