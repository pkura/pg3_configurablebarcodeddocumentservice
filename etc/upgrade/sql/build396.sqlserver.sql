CREATE TABLE dso_label(
	id numeric(19, 0) IDENTITY(1,1) NOT NULL PRIMARY KEY,
	name varchar(30) NOT NULL,
	description varchar(200),
	parent_id numeric(19, 0),
	owner varchar(100) NOT NULL,
	inaction_time numeric(18, 0),
	hidden smallint NOT NULL,
	ancestor numeric(19, 0),
	modifiable smallint NOT NULL,
	deletes_after smallint NOT NULL
);


CREATE TABLE dso_document_to_label(
	id numeric(19, 0) IDENTITY(1,1) NOT NULL,
	document_id numeric(19, 0) NOT NULL,
	label_id numeric(19, 0) NOT NULL,
	username varchar(100) NOT NULL,
	ctime datetime NOT NULL,
	actionTime datetime 
);