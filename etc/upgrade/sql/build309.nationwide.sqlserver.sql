create table ds_compiled_attachment_entry (
  ID numeric(19, 0) IDENTITY(1,1) NOT NULL,
  documentId numeric(19, 0) NOT NULL,
  compiledDocId numeric(19,0) NOT NULL,
  pos int,
  hversion int NOT NULL
);