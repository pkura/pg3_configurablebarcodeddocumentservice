CREATE TABLE DSW_PROCESS_REMINDER(
    id					numeric(18,0) not null,
    checkTime           timestamp,
	reminderType	    integer,
    jbpmTaskid          numeric(18,0),
	lparam  			numeric(18,0),
	wparam				varchar(200)
);
CREATE GENERATOR DSW_PROCESS_REMINDER_ID;
