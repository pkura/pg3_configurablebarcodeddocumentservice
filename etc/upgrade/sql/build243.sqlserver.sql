
CREATE TABLE nw.[daa_agencja] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[rodzaj] [int] NULL ,
	[nazwa] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nip] [varchar] (20) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
)


CREATE TABLE nw.[daa_agent] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NOT NULL ,
	[fk_daa_agencja] [int] NULL ,
	[numer] [int] NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
)


