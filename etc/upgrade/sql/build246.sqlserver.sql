  CREATE TABLE DS_DOCUMENT_SIGNATURE (
	ID              NUMERIC(18,0) NOT NULL identity(1,1),
	DOCUMENT_ID     NUMERIC(18,0) NOT NULL,
	CTIME           DATETIME NOT NULL,
    	AUTHOR          VARCHAR(62) NOT NULL,
	DOCCONTENT      IMAGE,
	SIGCONTENT      IMAGE,
	DOCCONTENTREFID	NUMERIC(18,0),
	ISOFFICE        SMALLINT,
	CORRECTLYVERIFIED SMALLINT
  );