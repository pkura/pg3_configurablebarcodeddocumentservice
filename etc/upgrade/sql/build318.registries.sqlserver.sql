alter table dsr_registry add regType varchar(20);
alter table dsr_registry drop column caseRequired;

alter table dsr_registry_entry add inDocument_id numeric(18,0);

create index dsr_registry_entry_indocument on dsr_registry_entry (inDocument_id);
create index dsr_registry_entry_registry on dsr_registry_entry (registry_id);
