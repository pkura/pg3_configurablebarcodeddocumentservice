create table ds_log (
	id					integer,
	reason				varchar(200),
	username			varchar(20),
	ctime 				timestamp,
	code				integer,
	param				varchar(200),
	primary key (id)
);
CREATE GENERATOR ds_log_ID;