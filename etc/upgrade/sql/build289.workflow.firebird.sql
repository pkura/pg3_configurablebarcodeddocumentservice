CREATE TABLE DSW_JBPM_TASK_DEFINITION(
    id					numeric(18,0) not null,
	name	            varchar(62),
    jbpmProcessDefinitionName        varchar(62),
	nameforuser			varchar(100),
	page				varchar(200)
);
CREATE GENERATOR DSW_JBPM_TASK_DEFINITION_ID;
