alter table ds_location add nr varchar2(10);
alter table ds_location add region varchar2(100);
alter table ds_location add segment varchar2(20);
alter table dso_barcode_range add locationId numeric(18,0);