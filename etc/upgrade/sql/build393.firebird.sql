create table ds_user_to_archive_role (
	user_id		numeric(18,0) not null,
	role		varchar(62) not null,
	source		varchar(200)
);

alter table ds_user_to_archive_role add constraint ds_user_to_archive_role_fk1
foreign key (user_id) references ds_user (id);


alter table ds_user_to_division add source varchar(2000);
alter table dso_role_usernames add source varchar(2000);

update ds_user_to_division set source = 'own';
update dso_role_usernames set source = 'own';

create table ds_profile_to_archive_role (
	profile_id		numeric(18,0) not null,
	role		varchar(62) not null,
	source		varchar(2000)
);

alter table ds_profile_to_archive_role add constraint ds_profile_to_archive_role_fk1
foreign key (profile_id) references ds_profile (id);

