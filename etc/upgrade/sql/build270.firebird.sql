create table ds_subst_hist(
    id			integer not null,
    login		varchar(62) not null,
    substitutedFrom	timestamp not null,
    substitutedThrough	timestamp not null,
    substLogin		varchar(62) not null
);

CREATE GENERATOR DS_SUBST_HIST_ID;