ALTER TABLE dso_document_audit ADD priority integer;
ALTER TABLE dso_container_audit ADD priority integer;
--@ignore-exceptions;
ALTER TABLE dsr_con_contract_audit ADD priority integer;
--@ignore-exceptions;
ALTER TABLE dsr_inv_invoice_audit ADD priority integer;
ALTER TABLE dsr_registry_audit ADD priority integer;
