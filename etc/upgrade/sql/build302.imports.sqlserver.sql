CREATE TABLE ds_imported_file_csv(
	id numeric(19, 0) IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	imported_file_id numeric(19, 0) NOT NULL,
	type_id integer NULL,
	csv image NULL
)
