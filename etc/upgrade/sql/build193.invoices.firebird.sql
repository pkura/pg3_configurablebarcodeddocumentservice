alter table dsr_inv_invoice add create_year integer not null;
update dsr_inv_invoice set create_year = 2005;
drop index dsr_inv_invoice_seq;
create unique index dsr_inv_invoice_seq on dsr_inv_invoice(create_year, sequenceid);
