create table DS_ALD_INVOICE_INFO (
   id numeric(19,0) identity (1,1) not null,
   lineNumber int,
   status int,
   creatingUser varchar(62) NOT NULL,
   ctime datetime NOT NULL,
   bookCode varchar(5) NULL,
   nrKsiegowania varchar(50) NULL,
   supplierNumber varchar(50) NULL,
   invoiceNumber varchar(50) NULL,
   nip varchar(50) NULL,
   amount float NULL,
   initiallyPaid smallint NULL,
   errorInfo varchar(100) NULL,
   documentId numeric(19,0) NULL,
   paymentDate datetime NULL
);