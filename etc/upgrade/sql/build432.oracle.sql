drop table ds_box_order;

create table ds_box_order(
	id			numeric(18,0),
	remark		varchar(300 char),
	documentId	numeric(18,0),
	priority	int,
	status		int,
	orderDate	timestamp,
	traceNumber varchar(50 char),
	location	numeric(18,0)
);
--@ignore-exceptions;
DROP SEQUENCE ds_box_order_id;
CREATE SEQUENCE ds_box_order_id start with 1 nocache;

drop table ds_box_order_history;

create table ds_box_order_history(
	id			numeric(18,0),
	orderId		numeric(18,0),
	valueName	varchar(100),
	value		varchar(300),
	ctime		timestamp,
	username	varchar(50)
);
drop SEQUENCE ds_box_order_history_id;
CREATE SEQUENCE ds_box_order_history_id start with 1 nocache;