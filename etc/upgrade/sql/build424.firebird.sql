create table dso_barcode_pool
(
	id numeric(18,0) not null,
	prefix varchar(5) not null,
	start_barcode numeric(18,0) not null,
	end_barcode numeric(18,0) not null,
	ctime timestamp not null,
	is_active smallint not null,
	close_time timestamp,
	current_pointer numeric(18,0) not null
);

create table dso_barcode_range
(
	id numeric(18,0) not null,
	pool_id numeric(18,0),
	start_barcode numeric(18,0),
	end_barcode numeric(18,0),
	client_type smallint not null,
	client_name varchar(50) not null,
	ctime timestamp not null,
	status smallint not null,
	current_pointer numeric(18,0),
	accept_time timestamp,
	number_of_barcodes numeric(18,0) not null,
	prefix varchar(5)
);
CREATE GENERATOR DSO_BARCODE_RANGE_ID;
create index dso_barcode_range_1 on dso_barcode_range(id);
create index dso_barcode_range_2 on dso_barcode_range(status);

insert into dso_barcode_pool values (1,'8',81911700000,81921699999,'2008-12-05',1,null,81911700000);