CREATE TABLE DS_MESSAGE (
    ID                      NUMERIC(18,0) NOT NULL,
    MESSAGEID               varchar(200),
    CONTENTDATA             blob,
    SUBJECT                 varchar(200),
    SENTDATE                timestamp,
    SENDER                  varchar(200)
);
CREATE GENERATOR DS_MESSAGE_ID;