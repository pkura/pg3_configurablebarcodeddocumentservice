create table ds_time (
id numeric(18, 0) not null primary key,
starttime timestamp not null,
endtime timestamp not null,
creator_id numeric(18, 0) not null,
place varchar(64),
description varchar(256)
);

create table ds_user_to_time (
user_id numeric(18, 0) not null,
time_id numeric(18, 0) not null
);

create generator ds_time_id;

create index i_ds_user_1 on ds_user_to_time (user_id);
create index i_ds_user_2 on ds_user_to_time (time_id);
create index i_ds_time_1 on ds_time (starttime);
create index i_ds_time_2 on ds_time (endtime);
