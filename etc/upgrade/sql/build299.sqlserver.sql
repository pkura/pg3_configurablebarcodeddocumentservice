CREATE TABLE DSW_PROCESS_COORDINATOR (
    id			numeric(19,0) identity (1,1) not null,
    documentKindCn      varchar(20) not null,
    name	        varchar(100),
    username            varchar(62) not null,
    type                varchar(50)
);