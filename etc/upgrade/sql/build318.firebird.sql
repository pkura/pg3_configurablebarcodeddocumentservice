create table ds_acceptance_condition (
    id numeric(18,0) not null,
    cn varchar(20) not null,
    fieldValue varchar(100),
    username varchar(62),
    divisionGuid varchar(62),
    primary key (id)
);   

create generator ds_acceptance_condition_id;

create index ds_acceptance_condition_cn on ds_acceptance_condition (cn);

CREATE TABLE DS_DOCUMENT_ACCEPTANCE (
    ID numeric(18, 0) NOT NULL,
    document_id numeric(18, 0) NOT NULL,
    acceptanceTime timestamp not null,
    username varchar(62) not null,
    acceptanceCn varchar(20) not null,
    primary key(id)
);

create index ds_document_acceptance_document on ds_document_acceptance (document_id);

create generator DS_DOCUMENT_ACCEPTANCE_ID;