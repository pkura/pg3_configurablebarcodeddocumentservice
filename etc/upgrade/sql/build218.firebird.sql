create table ds_retained_object (
    id          numeric(18,0) not null,
    source      varchar(16) not null,
    object_type varchar(16),
    ctime       timestamp not null,
    title       VARCHAR(100) NOT NULL COLLATE PXW_PLK
);

alter table ds_retained_object add constraint ds_retained_object_pk primary key (id);
create generator ds_retained_object_id;

create table ds_retained_attachment (
    id          numeric(18,0) not null,
    object_id   numeric(18,0),
    posn        integer,
    filename    varchar(255) not null COLLATE PXW_PLK,
    cn          varchar(20),
    contentsize integer not null,
    contentdata blob,
    mime        varchar(62) not null
);

alter table ds_retained_attachment add constraint ds_retained_attachment_pk primary key (id);
create generator ds_retained_attachment_id;

create table ds_retained_object_props (
    object_id   numeric(18,0),
    property_name  varchar(255) not null COLLATE PXW_PLK,
    property_value varchar(255) not null COLLATE PXW_PLK
);

