
CREATE TABLE [dso_aa_document] (
	[ID] [numeric](19, 0) NOT NULL ,
	[fk_daa_agencja] [int] NULL ,
	[fk_daa_agent] [int] NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[ID]
	)  
)