ALTER TABLE DS_USER ADD kpx varchar2(20 char);
ALTER TABLE DS_USER ADD pesel varchar2(11 char);
ALTER TABLE DS_USER ADD validity_date timestamp;
ALTER TABLE DS_USER ADD remarks varchar(400);
ALTER TABLE DS_USER ADD location numeric(18,0);



CREATE TABLE DS_LOCATION (
	id numeric(18,0) not null,
	loc_name varchar2(100 char) not null,
	street varchar2(50 char),
	zip varchar2(10 char),
	city varchar2(50 char)
);

CREATE SEQUENCE ds_location_id start with 1 nocache;
create index ds_location_1 on ds_location (id);