CREATE TABLE DSR_CNS_DECISION_ARCHIVE (
  ID                   NUMERIC(18, 0) IDENTITY(1,1) NOT NULL,
  NAME                 VARCHAR(80),
  ADDRESS              VARCHAR(80),
  OBJECT_KIND          VARCHAR(40),
  PROPOSITION_NO       VARCHAR(80),
  PROPOSITION_IN_DATE  datetime,
  DECISION_NO          VARCHAR(80),
  DECISION_DATE        datetime,
  REFERENCE            VARCHAR(300),
  DECISION_AFTER       VARCHAR(300),
  NOTICE               VARCHAR(300)
);