CREATE TABLE DSO_INVOICES_KIND (
  ID                     INTEGER                NOT NULL PRIMARY KEY
, NAME                   VARCHAR( 40 )          NOT NULL
, DAYS                   SMALLINT
, POSN                   INTEGER
);


alter table dsr_inv_invoice add kind integer;

create generator dso_invoices_kind_id;

insert into dso_invoices_kind (id,name,days,posn) values (1,'Faktura',14,1);
insert into dso_invoices_kind (id,name,days,posn) values (2,'Rachunek',14,2);
insert into dso_invoices_kind (id,name,days,posn) values (3,'Upomnienie',14,3);
insert into dso_invoices_kind (id,name,days,posn) values (4,'Korekta',14,4);
insert into dso_invoices_kind (id,name,days,posn) values (5,'Kwit',14,5);
insert into dso_invoices_kind (id,name,days,posn) values (6,'Nota ksi�gowa',14,6);
insert into dso_invoices_kind (id,name,days,posn) values (7,'Rozliczenie',14,7);
insert into dso_invoices_kind (id,name,days,posn) values (8,'Inne',14,8);

update dsr_inv_invoice
set kind = 1
where invoicekind = 'INVOICE';

update dsr_inv_invoice
set kind = 2
where invoicekind = 'CHECK';

update dsr_inv_invoice
set kind = 3
where invoicekind = 'REMINDER';

update dsr_inv_invoice
set kind = 4
where invoicekind = 'REVISION';

update dsr_inv_invoice
set kind = 5
where invoicekind = 'SIMPLE_CHECK';

update dsr_inv_invoice
set kind = 6
where invoicekind = 'NOTE';

update dsr_inv_invoice
set kind = 7
where invoicekind = 'SQUAREUP';

update dsr_inv_invoice
set kind = 8
where invoicekind = 'OTHER';

alter table dsr_inv_invoice drop invoicekind;

set generator dso_invoices_kind_id to 10;

