
create table ds_box_order_history(
ID                              NUMERIC(18, 0) primary key Not Null,
ID_DOC                          NUMERIC(18, 0) Not Null,
TXT                             VARCHAR(100 char),
DATA_ZMIANY                     TIMESTAMP Not Null,
KTO                             VARCHAR(60 char) Not Null
);

CREATE SEQUENCE ds_box_order_history_id start with 1 nocache;