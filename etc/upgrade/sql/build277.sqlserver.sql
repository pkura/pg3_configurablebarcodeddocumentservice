CREATE TABLE DSW_TASKLIST(
        id					numeric(19,0) identity (1,1) not null,
	dsw_assignment_id			numeric(19,0),
	dsw_assignment_accepted			smallint,
	dso_document_id				numeric(18,0),
	dso_document_officenumber		int,
	dso_document_officenumberyear		int,
	dsw_activity_laststatetime		datetime,
	dsw_activity_name			varchar(200),
	dso_person_firstname			varchar(50),
	dso_person_lastname			varchar(50),
	dso_person_organization			varchar(50),
	dso_person_street			varchar(50),
	dso_person_location			varchar(50),
	dsw_process_context_param_value		varchar(512),
	dsw_activity_context_param_val		varchar(512),
	dsw_activity_activity_key		varchar(30),
	dso_document_summary			varchar(80),
	assigned_user				varchar(512),
	dso_document_referenceid		varchar(62),
	dsw_process_name			varchar(200),
	ds_document_source			varchar(80),
	dsw_process_packageid			varchar(50),
	dsw_process_processdef		varchar(50),	
	dsw_resource_res_key			varchar(62),
	dsw_process_deadlinetime		datetime,
	dsw_process_deadlineremark		varchar(200),
	dsw_process_deadlineauthor		varchar(62),
	document_type				varchar(62),
	rcpt_firstname				varchar(50),
	rcpt_lastname				varchar(50),
	rcpt_organization			varchar(50),
	rcpt_street				varchar(50),
	rcpt_location				varchar(50),
	case_id					numeric(19,0),
	case_office_id				varchar(84)	
);


create index dsw_tasklist_1 on dsw_tasklist (id);
create index dsw_tasklist_2 on dsw_tasklist (dsw_assignment_id);
create index dsw_tasklist_3 on dsw_tasklist (dso_document_id);
create index dsw_tasklist_4 on dsw_tasklist (dsw_process_context_param_value);
create index dsw_tasklist_5 on dsw_tasklist (dsw_activity_context_param_val);
create index dsw_tasklist_6 on dsw_tasklist (dsw_activity_activity_key);
create index dsw_tasklist_7 on dsw_tasklist (dsw_activity_laststatetime);
create index dsw_tasklist_8 on dsw_tasklist (assigned_user);
create index dsw_tasklist_9 on dsw_tasklist (dsw_resource_res_key);
create index dsw_tasklist_10 on dsw_tasklist (dsw_process_deadlinetime);
create index dsw_tasklist_11 on dsw_tasklist (dsw_process_deadlineremark);
create index dsw_tasklist_12 on dsw_tasklist (dsw_process_deadlineauthor);
create index dsw_tasklist_13 on dsw_tasklist (document_type);


