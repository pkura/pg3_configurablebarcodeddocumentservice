CREATE TABLE dso_adnotation (
    id numeric(18,0) NOT NULL PRIMARY KEY,
    name VARCHAR(80)
);

CREATE SEQUENCE dso_adnotation_id start with 1 nocache;
