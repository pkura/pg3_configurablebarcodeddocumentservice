alter table ds_user alter column MOBILE_PHONE_NUM type varchar(30);
alter table ds_user alter column PHONE_NUM type varchar(30);
alter table ds_user alter column EXTENSION type varchar(30);
alter table ds_user alter column COMMUNICATOR_NUM type varchar(30);