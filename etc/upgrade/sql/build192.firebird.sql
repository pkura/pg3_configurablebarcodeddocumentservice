create table ds_document_flags (
	document_id numeric(18,0) not null,
	fl1 smallint default 0 not null,
	fl2 smallint default 0 not null,
	fl3 smallint default 0 not null,
	fl4 smallint default 0 not null,
	fl5 smallint default 0 not null,
	fl6 smallint default 0 not null,
	fl7 smallint default 0 not null,
	fl8 smallint default 0 not null,
	fl9 smallint default 0 not null,
	fl10 smallint default 0 not null,
	fl1mtime timestamp,
	fl2mtime timestamp,
	fl3mtime timestamp,
	fl4mtime timestamp,
	fl5mtime timestamp,
	fl6mtime timestamp,
	fl7mtime timestamp,
	fl8mtime timestamp,
	fl9mtime timestamp,
	fl10mtime timestamp,
	fl1author varchar(62),
	fl2author varchar(62),
	fl3author varchar(62),
	fl4author varchar(62),
	fl5author varchar(62),
	fl6author varchar(62),
	fl7author varchar(62),
	fl8author varchar(62),
	fl9author varchar(62),
	fl10author varchar(62)
);

alter table ds_document_flags add constraint ds_document_flags_pk
primary key (document_id);

create index ds_document_flags_fl1 on ds_document_flags (fl1);
create index ds_document_flags_fl2 on ds_document_flags (fl2);
create index ds_document_flags_fl3 on ds_document_flags (fl3);
create index ds_document_flags_fl4 on ds_document_flags (fl4);
create index ds_document_flags_fl5 on ds_document_flags (fl5);
create index ds_document_flags_fl6 on ds_document_flags (fl5);
create index ds_document_flags_fl7 on ds_document_flags (fl5);
create index ds_document_flags_fl8 on ds_document_flags (fl5);
create index ds_document_flags_fl9 on ds_document_flags (fl5);
create index ds_document_flags_fl10 on ds_document_flags (fl5);

create index ds_document_flags_fl1mtime on ds_document_flags (fl1mtime);
create index ds_document_flags_fl2mtime on ds_document_flags (fl2mtime);
create index ds_document_flags_fl3mtime on ds_document_flags (fl3mtime);
create index ds_document_flags_fl4mtime on ds_document_flags (fl4mtime);
create index ds_document_flags_fl5mtime on ds_document_flags (fl5mtime);
create index ds_document_flags_fl6mtime on ds_document_flags (fl5mtime);
create index ds_document_flags_fl7mtime on ds_document_flags (fl5mtime);
create index ds_document_flags_fl8mtime on ds_document_flags (fl5mtime);
create index ds_document_flags_fl9mtime on ds_document_flags (fl5mtime);
create index ds_document_flags_fl10mtime on ds_document_flags (fl5mtime);

create index ds_document_flags_fl1author on ds_document_flags (fl1author);
create index ds_document_flags_fl2author on ds_document_flags (fl2author);
create index ds_document_flags_fl3author on ds_document_flags (fl3author);
create index ds_document_flags_fl4author on ds_document_flags (fl4author);
create index ds_document_flags_fl5author on ds_document_flags (fl5author);
create index ds_document_flags_fl6author on ds_document_flags (fl5author);
create index ds_document_flags_fl7author on ds_document_flags (fl5author);
create index ds_document_flags_fl85author on ds_document_flags (fl5author);
create index ds_document_flags_fl9author on ds_document_flags (fl5author);
create index ds_document_flags_fl10author on ds_document_flags (fl5author);


create table ds_document_user_flags (
	document_id numeric(18,0) not null,
    username varchar(62) not null,
	fl1 smallint default 0 not null,
	fl2 smallint default 0 not null,
	fl3 smallint default 0 not null,
	fl4 smallint default 0 not null,
	fl5 smallint default 0 not null,
	fl6 smallint default 0 not null,
	fl7 smallint default 0 not null,
	fl8 smallint default 0 not null,
	fl9 smallint default 0 not null,
	fl10 smallint default 0 not null,
	fl1mtime timestamp,
	fl2mtime timestamp,
	fl3mtime timestamp,
	fl4mtime timestamp,
	fl5mtime timestamp,
	fl6mtime timestamp,
	fl7mtime timestamp,
	fl8mtime timestamp,
	fl9mtime timestamp,
	fl10mtime timestamp,
	fl1author varchar(62),
	fl2author varchar(62),
	fl3author varchar(62),
	fl4author varchar(62),
	fl5author varchar(62),
	fl6author varchar(62),
	fl7author varchar(62),
	fl8author varchar(62),
	fl9author varchar(62),
	fl10author varchar(62)
);

alter table ds_document_user_flags add constraint ds_document_user_flags_pk
primary key (document_id, username);

create index ds_document_uflags_fl1 on ds_document_user_flags (fl1);
create index ds_document_uflags_fl2 on ds_document_user_flags (fl2);
create index ds_document_uflags_fl3 on ds_document_user_flags (fl3);
create index ds_document_uflags_fl4 on ds_document_user_flags (fl4);
create index ds_document_uflags_fl5 on ds_document_user_flags (fl5);
create index ds_document_uflags_fl6 on ds_document_user_flags (fl5);
create index ds_document_uflags_fl7 on ds_document_user_flags (fl5);
create index ds_document_uflags_fl8 on ds_document_user_flags (fl5);
create index ds_document_uflags_fl9 on ds_document_user_flags (fl5);
create index ds_document_uflags_fl10 on ds_document_user_flags (fl5);

create index ds_document_uflags_fl1mtime on ds_document_user_flags (fl1mtime);
create index ds_document_uflags_fl2mtime on ds_document_user_flags (fl2mtime);
create index ds_document_uflags_fl3mtime on ds_document_user_flags (fl3mtime);
create index ds_document_uflags_fl4mtime on ds_document_user_flags (fl4mtime);
create index ds_document_uflags_fl5mtime on ds_document_user_flags (fl5mtime);
create index ds_document_uflags_fl6mtime on ds_document_user_flags (fl5mtime);
create index ds_document_uflags_fl7mtime on ds_document_user_flags (fl5mtime);
create index ds_document_uflags_fl8mtime on ds_document_user_flags (fl5mtime);
create index ds_document_uflags_fl9mtime on ds_document_user_flags (fl5mtime);
create index ds_document_uflags_fl10mtime on ds_document_user_flags (fl5mtime);

create index ds_document_uflags_fl1author on ds_document_user_flags (fl1author);
create index ds_document_uflags_fl2author on ds_document_user_flags (fl2author);
create index ds_document_uflags_fl3author on ds_document_user_flags (fl3author);
create index ds_document_uflags_fl4author on ds_document_user_flags (fl4author);
create index ds_document_uflags_fl5author on ds_document_user_flags (fl5author);
create index ds_document_uflags_fl6author on ds_document_user_flags (fl5author);
create index ds_document_uflags_fl7author on ds_document_user_flags (fl5author);
create index ds_document_uflags_fl8author on ds_document_user_flags (fl5author);
create index ds_document_uflags_fl9author on ds_document_user_flags (fl5author);
create index ds_document_uflags_fl10author on ds_document_user_flags (fl5author);
