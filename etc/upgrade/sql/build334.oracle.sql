alter table dso_in_document modify summary varchar2(254 char);
alter table dso_out_document modify summary varchar2(254 char);
alter table dsw_tasklist modify dso_document_summary varchar2(254 char);