create table DSE_EVENT
(
 event_id numeric(19,0) identity (1,1) not null primary key,
 event_status_id integer default 0,
 submit_date datetime default getDate(),
 trigger_code varchar(100),
 handler_code varchar(100),
 due_date datetime,
 next_check_date datetime,
 params varchar(4000)
);