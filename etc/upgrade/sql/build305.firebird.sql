create table ds_box_line (
   id numeric(18,0) NOT NULL PRIMARY KEY,
   line varchar(20),
   name varchar(100)
);
create generator ds_box_line_id;

alter table ds_box add line varchar(20);