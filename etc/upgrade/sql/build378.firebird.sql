CREATE TABLE ds_box_change_history(
	id numeric(18) NOT NULL,	
	idDoc numeric(18),
	boxToName varchar(100),
	boxFromName varchar(100),
	curDate timestamp,	
	primary key(id)
	);