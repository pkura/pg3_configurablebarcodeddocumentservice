alter table dso_document_asgn_history add cdate datetime;
alter table dsw_tasklist add rcvDate datetime;

create index dso_doc_asgn_history_cdate on dso_document_asgn_history (cdate);
create index dsw_tasklist_rcvDate on dsw_tasklist (rcvDate);