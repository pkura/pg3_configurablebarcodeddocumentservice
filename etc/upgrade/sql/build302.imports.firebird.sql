CREATE TABLE ds_imported_file_csv(
	id numeric(18, 0) NOT NULL PRIMARY KEY,
	imported_file_id numeric(18, 0) NOT NULL,
	type_id integer,
	csv blob
);
CREATE GENERATOR DS_IMPORTED_FILE_CSV_ID;
