-- OLD: alter table dso_maxus_bip_state add modify_date datetime;
IF NOT EXISTS 
(
	SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
	WHERE TABLE_NAME = 'dso_maxus_bip_state' 
	AND  COLUMN_NAME = 'modify_date'
)
begin
	alter table dso_maxus_bip_state add modify_date datetime
end