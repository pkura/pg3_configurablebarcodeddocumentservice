alter table dsw_tasklist add documentCtime timestamp;
create index dsw_tasklist_docCtime on dsw_tasklist (documentCtime);

alter table dsw_tasklist add documentCdate date;
create index dsw_tasklist_docCdate on dsw_tasklist (documentCdate);