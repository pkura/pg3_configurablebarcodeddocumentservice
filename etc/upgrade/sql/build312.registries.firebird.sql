create table DSR_REGISTRY (
    id          numeric(18,0) not null,
    hversion    integer not null,
    name        varchar(100) not null,
    shortname	varchar(30) not null,
    tablename   varchar(20) not null,
    enabled     smallint not null,
    requiredrwa	varchar(5),
    content     blob,
    cn          varchar(20),
    primary key (id)
);

create generator DSR_REGISTRY_ID;

create table DSR_REGISTRY_ENTRY (
    id          numeric(18,0) not null,
    registry_id numeric(18,0) not null,
    creator		varchar(62) not null,
    ctime       timestamp not null,
    hversion    integer not null,
    sequencenumber	integer,
    regyear	integer,
    primary key (id)
);

create generator DSR_REGISTRY_ENTRY_ID;

create table DSR_REGISTRY_AUDIT (
    entry_id	numeric(18,0) NOT NULL,
    posn		int NOT NULL,
    ctime       timestamp NOT NULL,
    property    varchar(62) ,
    username    varchar(62) NOT NULL,
    description varchar(200) NOT NULL,
    lparam      varchar(200),
    wparam      numeric(18,0)
);

create table DSR_ENTRY_TO_CASE (
    entry_id numeric(18,0) not null,
    case_id numeric(18,0) not null,
    primary key (entry_id,case_id)
);

alter table ds_attachment add type integer;
alter table ds_attachment add entry_id numeric(18,0);
update ds_attachment set type=1 where type is null;
