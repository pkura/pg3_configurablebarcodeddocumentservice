create table DSR_REGISTRY (
    id          numeric(19, 0) IDENTITY (1, 1) NOT NULL,
    hversion    int not null,
    name        varchar(100) not null,
    shortname	varchar(30) not null,
    tablename   varchar(20) not null,
    enabled     smallint not null,
    requiredrwa	varchar(5),
    content     image,
    cn          varchar(20),
    primary key (id)
);

create table DSR_REGISTRY_ENTRY (
    id          numeric(19, 0) IDENTITY (1, 1) NOT NULL,
    registry_id numeric(19,0) not null,
    creator		varchar(62) not null,
    ctime       datetime not null,
    hversion    int not null,
    sequencenumber	int,
    regyear	int,
    primary key (id)
);

create table DSR_REGISTRY_AUDIT (
    entry_id    numeric(19, 0) NOT NULL,
    posn		int NOT NULL,
    ctime       datetime NOT NULL,
    property    varchar(62) ,
    username    varchar(62) NOT NULL,
    description varchar(200) NOT NULL,
    lparam      varchar(200),
    wparam      numeric(19,0)   
);

create table DSR_ENTRY_TO_CASE (
    entry_id numeric(18,0) not null,
    case_id numeric(18,0) not null,
    primary key (entry_id,case_id)
);

alter table ds_attachment add type int;
alter table ds_attachment add entry_id numeric(19,0);
update ds_attachment set type=1 where type is null;
