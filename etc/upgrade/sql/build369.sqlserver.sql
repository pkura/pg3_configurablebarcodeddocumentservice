drop table ds_box_order;
create table ds_box_order (
	ID NUMERIC(18, 0) identity(1,1) Not Null,
	ID_U VARCHAR(60) Not Null,
	ID_BOX NUMERIC(18, 0) Not Null,
	DATA_ZAM datetime Not Null,
	STATUS VARCHAR(30) Not Null,
	ST1 datetime,
	ST2 datetime,
	ST3 datetime,
	ST4 datetime,
	ST5 datetime,
	ID_PUDLA VARCHAR(60),
	DATA_ZMIANY datetime Not Null,
	KTO varchar (60)
);