CREATE TABLE dso_maxus_bip_state(
	case_id 		numeric(18) NOT NULL,
	stan 			varchar(50) NOT NULL
);
create index idx_dso_maxus_case_id on dso_maxus_bip_state(case_id);