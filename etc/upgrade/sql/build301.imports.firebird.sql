CREATE TABLE ds_imported_document_common(
	id numeric(18, 0) NOT NULL PRIMARY KEY,
	imported_file_id numeric(18, 0) NOT NULL,
	result int,
	status int,
	processedTime timestamp,
	documentId numeric(18, 0),
	numberInFile int,
	errorInfo varchar(100)
);
CREATE GENERATOR DS_IMPORTED_DOCUMENT_COMMON_ID;

CREATE TABLE ds_imported_file_blob(
	id numeric(18, 0) NOT NULL PRIMARY KEY,
	imported_file_id numeric(18, 0) NOT NULL,
	xml blob
);
CREATE GENERATOR DS_IMPORTED_FILE_BLOB_ID;
