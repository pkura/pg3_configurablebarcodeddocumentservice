CREATE TABLE DS_IMPORTED_DOCUMENT_INFO (
    id					numeric(19,0) identity (1,1) not null,
	tiffName	        varchar(62),
    documentDate        datetime,
    typDokumentuId      int,
    nrPolisy            varchar(50),
	errorInfo			varchar(100),
	boxNumber			varchar(200),
    status              int,
    result              int,
    imported_file_id    numeric(19,0),
    processedTime       datetime,
    lineNumber          int,
    documentId          numeric(19,0)
);

CREATE TABLE DS_IMPORTED_FILE_INFO (
    id                  numeric(19,0) identity (1,1) not null,
    ctime               datetime,
    endTime             datetime,
    creatingUser        varchar(62),
    importType          int,
    status              int,
    fileName            varchar(510),
    directory           varchar(510)
);
