CREATE TABLE DSW_JBPM_SWIMLANE_MAPPING(
    id					numeric(19,0) identity (1,1) not null,
	name	            varchar(62),
    divisionGuid        varchar(62),
	lastUserNo          int
);