alter table dso_document_asgn_history add cdate date;
alter table dsw_tasklist add rcvDate date;

create index dso_doc_asgn_history_cdate on dso_document_asgn_history (cdate);
create index dsw_tasklist_rcvDate on dsw_tasklist (rcvDate);