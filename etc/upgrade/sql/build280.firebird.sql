alter table DS_MESSAGE add processed smallint not null;

CREATE TABLE DS_MESSAGE_ATTACHMENT (
    ID                      NUMERIC(18,0) NOT NULL,
    MESSAGE_ID              NUMERIC(18,0) NOT NULL,
    ATTACHMENTNO            INTEGER NOT NULL,
    CONTENTSIZE             INTEGER,
    FILENAME                VARCHAR(510),
    CONTENTDATA             BLOB
);
CREATE GENERATOR DS_MESSAGE_ATTACHMENT_ID;