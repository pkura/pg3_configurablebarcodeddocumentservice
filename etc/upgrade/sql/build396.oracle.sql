CREATE TABLE dso_label(
	id numeric(18, 0) primary key NOT NULL,
	name varchar(30) NOT NULL,
	description varchar(200),
	parent_id numeric(18, 0),
	owner varchar(100) NOT NULL,
	inaction_time numeric(18, 0),
	hidden smallint NOT NULL,
	ancestor numeric(18, 0),
	modifiable smallint NOT NULL,
	deletes_after smallint NOT NULL
);


CREATE TABLE dso_document_to_label(
	id numeric(18, 0) primary key NOT NULL,
	document_id numeric(18, 0) NOT NULL,
	label_id numeric(18, 0) NOT NULL,
	username varchar(100) NOT NULL,
	ctime TIMESTAMP NOT NULL,
	actionTime TIMESTAMP
);

create sequence dso_label_id;
create sequence dso_document_to_label_id;