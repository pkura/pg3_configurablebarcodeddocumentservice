create table ds_log (
	id					integer primary key,
	reason				varchar(200 char),
	username			varchar(20 char),
	ctime 				timestamp,
	code				integer,
	param				varchar(200 char)
	
);
CREATE SEQUENCE ds_log_ID start with 1 nocache;