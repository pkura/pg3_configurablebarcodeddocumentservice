create table DSE_EVENT
(
 	event_id integer primary key NOT NULL,
 	event_status_id integer default 0,
 	submit_date timestamp NULL,
 	trigger_code varchar2(100 char) NULL,
 	handler_code varchar2(100 char) NULL,
 	due_date timestamp NULL,
 	next_check_date timestamp NULL,
 	params varchar2(4000 char) NULL
);
CREATE SEQUENCE DSE_EVENT_ID start with 1 nocache;