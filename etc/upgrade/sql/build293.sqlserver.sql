CREATE TABLE [DS_DOCUMENT_KIND] (
	[ID] [numeric](19, 0) IDENTITY (1, 1) NOT NULL ,
	[HVERSION] [int] NOT NULL ,
	[NAME] [varchar] (100) COLLATE Polish_CI_AS NOT NULL ,
	[TABLENAME] [varchar] (20) COLLATE Polish_CI_AS NOT NULL ,
	[ENABLED] [smallint] NOT NULL ,
	[CONTENT] [image] NULL ,
	[CN] [varchar] (20) COLLATE Polish_CI_AS NULL ,
	CONSTRAINT [DS_DOCUMENT_KIND_PK] PRIMARY KEY  CLUSTERED 
	(
		[ID]
	)  ON [PRIMARY] 
);

alter table ds_document add dockind_id numeric(19, 0) NULL;
