create table DS_ALD_INVOICE_INFO2 (
   id numeric(19,0) identity (1,1) not null,
   lineNumber int,
   status int,
   creatingUser varchar(62) NOT NULL,
   ctime datetime NOT NULL,
   bookCode varchar(5) NULL,
   nrKsiegowania varchar(50) NULL,
   invoiceNumber varchar(50) NULL,
   amount float NULL,
   origAmount float NULL,
   errorInfo varchar(100) NULL,
   documentId numeric(19,0) NULL
);