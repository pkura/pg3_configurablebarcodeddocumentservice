-- DROP TABLE DSR_CNS_DECISION_ARCHIVE;

CREATE TABLE DSR_CNS_DECISION_ARCHIVE (
  ID                   NUMERIC(18) NOT NULL,
  NAME                 VARCHAR(80),
  ADDRESS              VARCHAR(80),
  OBJECT_KIND          VARCHAR(40),
  PROPOSITION_NO       VARCHAR(80),
  PROPOSITION_IN_DATE  TIMESTAMP,
  DECISION_NO          VARCHAR(80),
  DECISION_DATE        TIMESTAMP,
  REFERENCE            VARCHAR(300),
  DECISION_AFTER       VARCHAR(300),
  NOTICE               VARCHAR(300),
  /* Keys */
  PRIMARY KEY (ID)
);

CREATE GENERATOR DSR_CNS_DECISION_ARCHIVE_ID;