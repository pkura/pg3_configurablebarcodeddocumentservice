alter table dsw_tasklist add divisionGuid varchar(62);
create index dsw_tasklist_divisionGuid on dsw_tasklist (divisionGuid);