CREATE TABLE [dso_email_channel](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[pop3_host] [varchar](100) NOT NULL,
	[pop3_username] [varchar](100) NOT NULL,
	[pop3_password] [varchar](50) NOT NULL,
	[folder_name] [varchar](50) NULL,
	[kolejka] [varchar](100) NULL,
	[ctime] [datetime] NOT NULL,
	[warunki] [varchar](1000) NULL,
	[content_type] [varchar](50) NOT NULL,
 	PRIMARY KEY  CLUSTERED
	(
		[ID]
	)  
);