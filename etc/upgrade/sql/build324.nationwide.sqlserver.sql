alter table DSG_DF_MULTIPLE_VALUE add field_val varchar(100);
GO
update DSG_DF_MULTIPLE_VALUE set field_val = value;
alter table DSG_DF_MULTIPLE_VALUE drop column value;

alter table DSG_DP_MULTIPLE_VALUE add field_val varchar(100);
GO
update DSG_DP_MULTIPLE_VALUE set field_val = value;
alter table DSG_DP_MULTIPLE_VALUE drop column value;
