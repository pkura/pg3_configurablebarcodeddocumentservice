CREATE TABLE ds_box_change_history(
	id numeric(18, 0) IDENTITY(1,1) NOT NULL,
	idDoc numeric(19, 0),
	boxToName varchar(100) ,
	boxFromName varchar(100),
	curDate datetime) 