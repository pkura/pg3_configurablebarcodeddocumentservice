CREATE TABLE ds_box_change_history(
	id numeric(18, 0) primary key NOT NULL,
	idDoc numeric(19, 0),
	boxToName varchar(100 char) ,
	boxFromName varchar(100 char),
	curDate timestamp) 