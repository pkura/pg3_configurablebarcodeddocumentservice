drop table ds_box_order;

create table ds_box_order(
	id			numeric(18,0) identity(1,1),
	remark		varchar(300),
	documentId	numeric(18,0),
	priority	int,
	status		int,
	orderDate	datetime,
	traceNumber varchar(50),
	location	numeric(18,0)
);

drop table ds_box_order_history;

create table ds_box_order_history(
	id			numeric(18,0) identity(1,1),
	orderId		numeric(18,0),
	valueName	varchar(100),
	value		varchar(300),
	ctime		dateTime,
	username	varchar(50)
);