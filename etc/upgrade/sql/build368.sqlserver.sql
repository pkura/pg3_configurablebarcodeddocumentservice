
create table ds_box_order_history(
ID                              NUMERIC(18, 0) identity(1,1) Not Null,
ID_DOC                          NUMERIC(18, 0) Not Null,
TXT                             VARCHAR(100),
DATA_ZMIANY                     DATETIME Not Null,
KTO                             VARCHAR(60) Not Null
);