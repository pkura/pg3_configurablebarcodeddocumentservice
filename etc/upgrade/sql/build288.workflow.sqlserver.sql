CREATE TABLE DSW_PROCESS_REMINDER(
    id					numeric(19,0) identity (1,1) not null,
    checkTime           datetime,
	reminderType	    int,
    jbpmTaskId          numeric(18,0),
	lparam  			numeric(18,0),
	wparam				varchar(200)
);

