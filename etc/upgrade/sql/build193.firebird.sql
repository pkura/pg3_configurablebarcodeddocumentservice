--@ignore-exceptions;
drop index dso_container_seq;

--@ignore-exceptions;
drop index dso_container_oid;

update dso_container set case_year = 2005 where case_year is null;
CREATE UNIQUE INDEX DSO_CONTAINER_SEQ ON DSO_CONTAINER (PARENT_ID, CASE_YEAR, SEQUENCEID);
