alter TABLE DSW_TASKLIST add dsw_process_processdef varchar(50);
update DSW_TASKLIST set dsw_process_processdef = dsw_process_processdefinitionid;
alter TABLE DSW_TASKLIST drop dsw_process_processdefinitionid;
-- dla ms alter TABLE DSW_TASKLIST drop COLUMN dsw_process_processdefinitionid;


alter TABLE DSW_TASKLIST add dsw_process_context_param_val varchar(200);
drop index dsw_tasklist_4;
--dla ms drop index dsw_tasklist_4 ON DSW_TASKLIST;
commit;
create index dsw_tasklist_4 on dsw_tasklist (dsw_process_context_param_val);
update DSW_TASKLIST set dsw_process_context_param_val = dsw_process_context_param_value;
alter TABLE DSW_TASKLIST drop dsw_process_context_param_value;
--  dla ms alter TABLE DSW_TASKLIST drop COLUMN dsw_process_context_param_value;

--DSW_JBPM_TASK_DEFINITION   tylko dla ms-------------------------------------------------------------

alter TABLE DSW_JBPM_TASK_DEFINITION add action varchar(200);
update DSW_JBPM_TASK_DEFINITION set action = page;
alter TABLE DSW_JBPM_TASK_DEFINITION drop page;
-- dla ms alter TABLE DSW_JBPM_TASK_DEFINITION drop COLUMN page;

--DSW_JBPM_PROCESS_DEFINITION   tylko dla ms----------------------------------------------------------

alter TABLE DSW_JBPM_PROCESS_DEFINITION add action varchar(200);
update DSW_JBPM_PROCESS_DEFINITION set action = page;
alter TABLE DSW_JBPM_PROCESS_DEFINITION drop page;
--dla ms alter TABLE DSW_JBPM_PROCESS_DEFINITION drop COLUMN page;