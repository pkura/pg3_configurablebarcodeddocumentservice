create table ds_retained_object (
    id          numeric(19,0) not null identity,
    source      varchar(16) not null,
    object_type varchar(16),
    ctime       datetime not null,
    title       VARCHAR(100) NOT NULL
);

alter table ds_retained_object add constraint ds_retained_object_pk primary key (id);

create table ds_retained_attachment (
    id          numeric(19,0) not null identity,
    object_id   numeric(19,0),
    posn        integer,
    filename    varchar(255) not null,
    cn          varchar(20),
    contentsize integer not null,
    contentdata image,
    mime        varchar(62) not null
);

alter table ds_retained_attachment add constraint ds_retained_attachment_pk primary key (id);
alter table ds_retained_attachment add constraint ds_retained_object_fk foreign key (object_id)
    references ds_retained_object (id); 

create table ds_retained_object_props (
    object_id   numeric(19,0),
    property_name  varchar(255) not null,
    property_value varchar(255) not null
);


