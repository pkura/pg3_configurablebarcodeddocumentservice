CREATE TABLE DS_MESSAGE (
    ID                      INT IDENTITY (1, 1) NOT NULL,
    MESSAGEID               varchar(200),
    CONTENTDATA             image,
    SUBJECT                 varchar(200),
    SENTDATE                datetime,
    SENDER                  varchar(200)
);