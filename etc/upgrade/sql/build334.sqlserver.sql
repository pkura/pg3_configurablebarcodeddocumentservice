alter table dso_in_document alter column summary varchar(254);
alter table dso_out_document alter column summary varchar(254);
alter table dsw_tasklist alter column dso_document_summary varchar(254);