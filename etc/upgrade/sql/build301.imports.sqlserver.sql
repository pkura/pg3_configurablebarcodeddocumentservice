CREATE TABLE ds_imported_document_common(
	id numeric(19, 0) IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	imported_file_id numeric(19, 0) NOT NULL,
	result int NULL,
	status int NULL,
	processedTime datetime NULL,
	documentId numeric(19, 0) NULL,
	numberInFile int NULL,
	errorInfo varchar(100) NULL
)

CREATE TABLE ds_imported_file_blob(
	id numeric(19, 0) IDENTITY(1, 1) NOT NULL PRIMARY KEY,
	imported_file_id numeric(19, 0) NOT NULL,
	xml image NULL
)
