alter table dsw_task_history_entry add externalWorkflow smallint;

create index dsw_task_history_extWorkflow on dsw_task_history_entry (externalWorkflow);
