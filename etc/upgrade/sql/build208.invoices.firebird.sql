alter table dsr_inv_invoice add vat2 integer;
update dsr_inv_invoice set vat2 = vat;
alter table dsr_inv_invoice drop vat;
alter table dsr_inv_invoice alter vat2 to vat;
