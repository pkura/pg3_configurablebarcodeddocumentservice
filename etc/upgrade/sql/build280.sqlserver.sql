alter table DS_MESSAGE add processed smallint not null default 0;

CREATE TABLE DS_MESSAGE_ATTACHMENT (
    ID                      INT IDENTITY (1, 1) NOT NULL,
    MESSAGE_ID              [numeric](19, 0) NOT NULL,
    ATTACHMENTNO            INT NOT NULL,
    CONTENTSIZE             INT,
    FILENAME                VARCHAR(510),
    CONTENTDATA             image
);