create table dsg_account_number (
    number varchar(50) not null,
    primary key(number)
);

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW (
	id int identity(1,1) NOT NULL,
	name varchar(100),
	symbol varchar(50),
	username varchar(62),
    divisionGuid varchar(62),
    primary key (id)
);

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW_FAKTURY (
	id int identity(1,1) NOT NULL,
	centrumId int,
	centrumCode varchar(100),
	accountNumber varchar(50),
	amount numeric(10, 2),
    documentId numeric(19, 0),
    primary key (id)
);
