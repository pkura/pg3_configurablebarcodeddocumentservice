drop table ds_box_order;
create table ds_box_order (
	ID NUMERIC(18, 0) Not Null,
	ID_U VARCHAR(60) Not Null,
	ID_BOX NUMERIC(18, 0) Not Null,
	DATA_ZAM TIMESTAMP Not Null,
	STATUS VARCHAR(30) Not Null,
	ST1 TIMESTAMP,
	ST2 TIMESTAMP,
	ST3 TIMESTAMP,
	ST4 TIMESTAMP,
	ST5 TIMESTAMP,
	ID_PUDLA VARCHAR(60),
	DATA_ZMIANY TIMESTAMP Not Null,
	KTO varchar(60),
	primary key (ID)
);