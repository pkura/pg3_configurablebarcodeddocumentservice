create table dsg_account_number (
    number varchar(50) not null,
    primary key(number)
);

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW 
(
	id 				integer NOT NULL,
	name 			varchar(100),
	symbol 			varchar(50),
	username 		varchar(62),
    divisionGuid 	varchar(62),
    primary key (id)
);
--@ignore-exceptions;
CREATE GENERATOR DSG_CENTRUM_KOSZTOW_ID;

--@ignore-exceptions;
CREATE TABLE DSG_CENTRUM_KOSZTOW_FAKTURY (
	id              integer NOT NULL,
	centrumId       integer,
	centrumCode     varchar(100),
	accountNumber   varchar(50),
	amount          float,
    documentId      numeric(18,0),
    primary key (id)
);
--@ignore-exceptions;
CREATE GENERATOR DSG_CENTRUM_KOSZTOW_FAKTURY_ID;