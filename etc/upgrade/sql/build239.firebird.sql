CREATE TABLE daa_agencja (
	id int NOT NULL ,
	rodzaj int ,
	nazwa varchar (255) ,
	nip varchar (20) ,
	ulica varchar (255) ,
	kod varchar (255) ,
	miejscowosc varchar (255) ,
	email varchar (255),
	faks varchar (255) ,
	telefon varchar (255) ,
	 PRIMARY KEY  (id)
);


CREATE TABLE daa_agent (
	id int not null,
	nazwisko varchar (255) ,
	imie varchar (255) ,
	fk_daa_agencja int ,
	numer int ,
	 PRIMARY KEY  (id)
);



create table dso_aa_document (
  id int not null,
  fk_daa_agencja int ,
  fk_daa_agent int ,
  data timestamp,
  primary key (id)
);


 CREATE GENERATOR daa_agent_ID;
 CREATE GENERATOR daa_agencja_ID;