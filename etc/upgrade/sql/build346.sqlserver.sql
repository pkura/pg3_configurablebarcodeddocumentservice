alter table dsw_tasklist add documentCtime datetime;
create index dsw_tasklist_docCtime on dsw_tasklist (documentCtime);

alter table dsw_tasklist add documentCdate datetime;
create index dsw_tasklist_docCdate on dsw_tasklist (documentCdate);