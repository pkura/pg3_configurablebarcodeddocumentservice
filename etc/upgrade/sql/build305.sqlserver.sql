create table ds_box_line (
   id numeric(19,0) identity (1,1) not null,
   line varchar(20),
   name varchar(100)
);

alter table ds_box add line varchar(20);