CREATE TABLE [DS_NW_DEADLINE_SNAPSHOT] (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL ,
        [RODZAJ_CN] [varchar] (100) COLLATE Polish_CI_AS NULL ,
	[STATUS_CN] [varchar] (20) COLLATE Polish_CI_AS NULL ,
	[CREATE_TIME] [numeric] (18,0) NULL ,
        [FINISH_TIME] [numeric] (18,0) NULL ,
        [NEXT_STATUS_TIME] [numeric] (18,0) NULL ,
        [STATUS_SET_TIME] [numeric] (18,0) NULL ,
);

