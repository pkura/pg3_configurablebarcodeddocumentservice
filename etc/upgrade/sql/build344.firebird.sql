CREATE TABLE DSC_CONTRACTOR_DICTIONARY 
(
	id 				integer NOT NULL,
	name 			varchar(20),
    primary key (id)
);
--@ignore-exceptions;
CREATE GENERATOR DSC_CONTRACTOR_DICTIONARY_ID;

create table DSC_Contractor (
	id					integer not null,
	contractorDictionaryId integer,
	name 				varchar(50),
	fullName			varchar(200),
	nip 				varchar(10),
	regon 				varchar(50),
	krs 				varchar(50),
	street 				varchar(50),
	code 				varchar(50),
	city 				varchar(50),
	country 			varchar(50),
	contactData			varchar(200),
	primary key (id)
);
--@ignore-exceptions;
CREATE GENERATOR DSC_Contractor_ID;

create table dsc_worker (
	id					integer not null,
	title				varchar(30),
	firstname			varchar(50),
	lastname 			varchar(50),
	street				varchar(50),
	zip					varchar(14),
	location			varchar(50),
	country				varchar(50),
	pesel 				varchar(11),
	nip					varchar(15),
	email				varchar(50),
	fax					varchar(30),
	remarks				varchar(200),
	phoneNumber			varchar(20),
	ContractorId		integer,
	primary key (id)
);
CREATE GENERATOR DSC_WORKER_ID;