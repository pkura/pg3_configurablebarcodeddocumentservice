CREATE TABLE DSR_CNS_DECISION_ARCHIVE (
  ID                   NUMERIC(18, 0) primary key NOT NULL,
  NAME                 VARCHAR(80 char),
  ADDRESS              VARCHAR(80 char),
  OBJECT_KIND          VARCHAR(40 char),
  PROPOSITION_NO       VARCHAR(80 char),
  PROPOSITION_IN_DATE  TIMESTAMP,
  DECISION_NO          VARCHAR(80 char),
  DECISION_DATE        TIMESTAMP,
  REFERENCE            VARCHAR(300 char),
  DECISION_AFTER       VARCHAR(300 char),
  NOTICE               VARCHAR(300 char)
);