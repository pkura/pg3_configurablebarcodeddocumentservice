CREATE TABLE DSW_JBPM_TASK_DEFINITION(
    id					numeric(19,0) identity (1,1) not null,
	name	            varchar(62),
    jbpmProcessDefinitionName        varchar(62),
	nameforuser			varchar(100),
	page				varchar(200),
);
