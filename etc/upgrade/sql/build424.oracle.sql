create table dso_barcode_pool
(
	id numeric(19,0) not null,
	prefix varchar2(5 char) not null,
	start_barcode numeric(19,0) not null,
	end_barcode numeric(19,0) not null,
	ctime timestamp not null,
	is_active smallint not null,
	close_time timestamp,
	current_pointer numeric(19,0) not null
);

create table dso_barcode_range
(
	id numeric(19,0) not null,
	pool_id numeric(19,0),
	start_barcode numeric(19,0),
	end_barcode numeric(19,0),
	client_type smallint not null,
	client_name varchar2(50 char) not null,
	ctime timestamp not null,
	status smallint not null,
	current_pointer numeric(19,0),
	accept_time timestamp,
	number_of_barcodes numeric(19,0) not null,
	prefix varchar2(5 char)
);
CREATE SEQUENCE dso_barcode_range_id start with 1 nocache;
create index dso_barcode_range_1 on dso_barcode_range(id);
create index dso_barcode_range_2 on dso_barcode_range(status);
--poprawka po kubie nie parsowal daty wstawilem czas systemowy
insert into dso_barcode_pool values (1,'8',81911700000,81921699999,SYSDATE ,1,null,81911700000);