create table ds_document_for_compilation (
  ID numeric(19, 0) IDENTITY(1,1) NOT NULL,
  documentId numeric(19, 0) NOT NULL,
  username varchar(62) NOT NULL,
  priority int,
  hversion int NOT NULL
);

create index ds_document_for_compil_uname on ds_document_for_compilation (username);

create index ds_compiled_attach_entry_docid on ds_compiled_attachment_entry (documentId);