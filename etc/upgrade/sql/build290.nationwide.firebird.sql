CREATE TABLE DS_IMPORTED_DOCUMENT_INFO (
    id					numeric(18,0) not null,
	tiffName	        varchar(62),
    documentDate        timestamp,
    typDokumentuId      integer,
    nrPolisy            varchar(50),
	errorInfo			varchar(100),
	boxNumber   		varchar(200),
    status              integer,
    result              integer,
    imported_file_id    numeric(18,0),
    processedTime       timestamp,
    lineNumber          integer,
    documentId          numeric(18,0)
);
CREATE GENERATOR DS_IMPORTED_DOCUMENT_INFO_ID;

CREATE TABLE DS_IMPORTED_FILE_INFO (
    id                  numeric(18,0) not null,
    ctime               timestamp,
    endTime             timestamp,
    creatingUser        varchar(62),
    status              integer,
    importType          integer,
    fileName            varchar(510),
    directory           varchar(510)
);
CREATE GENERATOR DS_IMPORTED_FILE_INFO_ID;
