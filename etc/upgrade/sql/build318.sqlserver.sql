create table ds_acceptance_condition (
    id numeric(19,0) identity(1,1) not null,
    cn varchar(20) not null,
    value varchar(100),
    username varchar(62),
    divisionGuid varchar(62),
    primary key (id)
);   

create index ds_acceptance_condition_cn on ds_acceptance_condition (cn);

CREATE TABLE [DS_DOCUMENT_ACCEPTANCE] (
    [ID] [numeric](19, 0) identity(1,1) NOT NULL,
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [acceptanceTime] [datetime] not null,
    [username] [varchar](62) not null,
    [acceptanceCn] [varchar](20) not null,
    primary key(id)
);

create index ds_document_acceptance_document on ds_document_acceptance (document_id);