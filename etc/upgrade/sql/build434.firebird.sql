create table DSE_EVENT
(
 	event_id numeric(18,0) not null primary key,
 	event_status_id integer default 0,
 	submit_date timestamp,
 	trigger_code varchar(100),
 	handler_code varchar(100),
 	due_date timestamp,
 	next_check_date timestamp,
 	params varchar(4000)
);

CREATE GENERATOR DSE_EVENT_ID;
create index DSE_EVENT_1 on DSE_EVENT (event_id);