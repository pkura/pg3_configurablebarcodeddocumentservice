create table DS_DOCUMENT_KIND (
    id          numeric(18,0) not null,
    hversion    integer not null,
    name        varchar(100) not null,
    tablename   varchar(20) not null,
    enabled     smallint not null,
    content     blob,
    cn          varchar(20),
    primary key (id)
);

alter table ds_document add dockind_id numeric(18,0);