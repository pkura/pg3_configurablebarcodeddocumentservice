alter table dso_label drop  column owner;
alter table dso_document_to_label drop column username;
alter table dso_label add type varchar(50);
alter table dso_label add read_rights varchar(100);
alter table dso_label add write_rights varchar(100);
alter table dso_label add modify_rights varchar(100);

create index dso_label_name on dso_label (name);
create index dso_label_readrights on dso_label (read_rights);
create index dso_label_writerights on dso_label (write_rights);
create index dso_label_modifyrights on dso_label (modify_rights);
create index dso_label_type on dso_label (type);
create index dso_label_beandocument on dso_document_to_label (document_id);
create index dso_label_beanlabel on dso_document_to_label (label_id);