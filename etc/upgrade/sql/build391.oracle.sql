create table ds_profile (
    id          				numeric(19,0) not null,
    name	    				varchar(62 char) not null,
    encoded_archive_roles     	varchar(80 char)
);

create sequence ds_profile_id;

alter table ds_profile add constraint ds_profile_pk primary key (id);

create table ds_profile_to_group (
	profile_id		numeric(19,0) not null,
	division_id		numeric(19,0) not null
);

alter table ds_profile_to_group add constraint ds_profile_to_group_fk1
foreign key (profile_id) references ds_profile (id);

alter table ds_profile_to_group add constraint ds_profile_to_group_fk2
foreign key (division_id) references ds_division (id);

create table ds_profile_to_role (
	profile_id		numeric(19,0) not null,
	role_id			numeric(19,0) not null
);

alter table ds_profile_to_role add constraint ds_profile_to_role_fk1
foreign key (profile_id) references ds_profile (id);

alter table ds_profile_to_role add constraint ds_profile_to_role_fk2
foreign key (role_id) references dso_role (id);

create table ds_profile_to_user (
	profile_id		numeric(19,0) not null,
	user_id			numeric(19,0) not null
);

alter table ds_profile_to_user add constraint ds_profile_to_user_fk1
foreign key (profile_id) references ds_profile (id);

alter table ds_profile_to_user add constraint ds_profile_to_user_fk2
foreign key (user_id) references ds_user (id);