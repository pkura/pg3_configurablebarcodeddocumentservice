drop table ds_box_order;
create table ds_box_order (
	ID NUMERIC(18, 0)  primary key Not Null,
	ID_U VARCHAR(60 char) Not Null,
	ID_BOX NUMERIC(18, 0) Not Null,
	DATA_ZAM TIMESTAMP Not Null,
	STATUS VARCHAR(30 char) Not Null,
	ST1 TIMESTAMP,
	ST2 TIMESTAMP,
	ST3 TIMESTAMP,
	ST4 TIMESTAMP,
	ST5 TIMESTAMP,
	ID_PUDLA VARCHAR(60 char),
	DATA_ZMIANY TIMESTAMP Not Null,
	KTO varchar (60)
);