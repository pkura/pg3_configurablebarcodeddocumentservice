CREATE TABLE ds_postal_code (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    code_from               INTEGER NOT NULL,
    code_to                 INTEGER,
    city                    VARCHAR(50) NOT NULL,
    PROVINCE			    VARCHAR(50) NOT NULL
);


CREATE GENERATOR ds_postal_code_id;
