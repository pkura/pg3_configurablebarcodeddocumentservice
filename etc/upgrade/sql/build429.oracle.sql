CREATE TABLE dso_email_channel
(
	id integer primary key NOT NULL,
	pop3_host varchar2(100 char) NOT NULL,
	pop3_username varchar2(100 char) NOT NULL,
	pop3_password varchar2(50 char) NOT NULL,
	folder_name varchar2(50 char) NULL,
	kolejka varchar2(100 char) NULL,
	ctime timestamp NOT NULL,
	warunki varchar2(1000 char) NULL,
	content_type varchar2(50 char) NOT NULL
);
CREATE SEQUENCE dso_email_channel_id start with 1 nocache;