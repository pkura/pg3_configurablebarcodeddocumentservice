create table ds_time (
id numeric(19, 0) identity(1, 1) not null primary key,
starttime datetime not null,
endtime datetime not null,
creator_id numeric(19, 0) not null,
place varchar(64),
description varchar(256)
);

create table ds_user_to_time (
user_id numeric(19, 0) not null,
time_id numeric(19, 0) not null,
);

create index i_ds_user_1 on ds_user_to_time (user_id);
create index i_ds_user_2 on ds_user_to_time (time_id);
create index i_ds_time_1 on ds_time (starttime);
create index i_ds_time_2 on ds_time (endtime);
