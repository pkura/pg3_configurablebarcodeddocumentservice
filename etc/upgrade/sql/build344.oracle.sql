CREATE TABLE DSC_CONTRACTOR_DICTIONARY 
(
	id 				integer primary key,
	name 			varchar2(20 char)
);
--@ignore-exceptions;
CREATE SEQUENCE DSC_CONTRACTOR_DICTIONARY_ID start with 1 nocache;

create table DSC_Contractor (
	id					integer not null,
	contractorDictionaryId integer,
	name 				varchar(50 char),
	fullName			varchar(200 char),
	nip 				varchar(10 char),
	regon 				varchar(50 char),
	krs 				varchar(50 char),
	street 				varchar(50 char),
	code 				varchar(50 char),
	city 				varchar(50 char),
	country 			varchar(50 char),
	contactData			varchar(200 char)
);
CREATE SEQUENCE DSC_Contractor_ID start with 1 nocache;

create table dsc_worker (
	id					integer primary key,
	title				varchar(30 char),
	firstname			varchar(50 char),
	lastname 			varchar(50 char),
	street				varchar(50 char),
	zip					varchar(14 char),
	location			varchar(50 char),
	country				varchar(50 char),
	pesel 				varchar(11 char),
	nip					varchar(15 char),
	email				varchar(50 char),
	fax					varchar(30 char),
	remarks				varchar(200 char),
	phoneNumber			varchar(20 char),
	ContractorId		integer
);
CREATE SEQUENCE DSC_WORKER_ID start with 1 nocache;