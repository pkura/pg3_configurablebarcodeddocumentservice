create table ic_fixed_assets_group(
	id INT NOT NULL primary key,
	title VARCHAR(255) NOT NULL
);

create table ic_fixed_assets_subgroup(
	id INT NOT NULL primary key,
	group_id INT NOT NULL,
	title VARCHAR(255) NOT NULL
);

alter table ic_fixed_assets_subgroup add constraint ic_fixed_assets_subgroup_group_id
	foreign key (group_id)
    references ic_fixed_assets_group (id);

alter table dsg_centrum_kosztow_faktury add subgroupId int;
alter table dsg_centrum_kosztow_faktury add storagePlace VARCHAR(300);