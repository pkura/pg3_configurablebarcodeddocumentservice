alter table dsr_con_contract add create_year integer not null;
update dsr_con_contract set create_year = 2005;
drop index DSR_CON_CONTRACT_SEQ;
create unique index DSR_CON_CONTRACT_SEQ on dsr_con_contract(create_year, sequenceid);
