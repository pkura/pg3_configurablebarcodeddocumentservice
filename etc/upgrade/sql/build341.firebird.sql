alter table dso_document_asgn_history add processType integer;

create table dsw_task_history_entry (
	id               numeric(18,0) not null,		
	documentId       numeric(18,0) not null,	
	username         varchar(62),
	divisionGuid     varchar(62),
	receiveDate      date,
	acceptDate       date,
	finishDate       date,
    receiveTime      timestamp,
	acceptTime       timestamp,
	finishTime       timestamp,
	activityKey      varchar(30),
	primary key (id)
);

create generator dsw_task_history_entry_id;

create index dsw_task_history_documentId on dsw_task_history_entry (documentId);
create index dsw_task_history_username on dsw_task_history_entry (username);
create index dsw_task_history_divisionGuid on dsw_task_history_entry (divisionGuid);
create index dsw_task_history_receiveDate on dsw_task_history_entry (receiveDate);
create index dsw_task_history_finishDate on dsw_task_history_entry (finishDate);
create index dsw_task_history_acceptDate on dsw_task_history_entry (acceptDate);
create index dsw_task_history_activityKey on dsw_task_history_entry (acceptDate);