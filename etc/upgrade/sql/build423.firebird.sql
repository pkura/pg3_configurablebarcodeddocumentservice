ALTER TABLE DS_USER ADD kpx varchar(20);
ALTER TABLE DS_USER ADD pesel varchar(11);
ALTER TABLE DS_USER ADD validity_date timestamp;
ALTER TABLE DS_USER ADD remarks varchar(400);
ALTER TABLE DS_USER ADD location numeric(18,0);

CREATE TABLE DS_LOCATION (
	id numeric(18,0) not null,
	loc_name varchar(100) not null,
	street varchar(50),
	zip varchar(10),
	city varchar(50)
);

CREATE GENERATOR DS_LOCATION_ID;
create index ds_location_1 on ds_location (id);