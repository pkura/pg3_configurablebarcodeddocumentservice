ALTER TABLE dsg_account_number add account_name VARCHAR(100);
ALTER TABLE dsg_account_number add id INTEGER;
CREATE SEQUENCE dsg_account_number_id start with 1 nocache;