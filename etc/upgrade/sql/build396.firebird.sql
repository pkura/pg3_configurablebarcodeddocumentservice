CREATE TABLE dso_label(
	id 				numeric(18) NOT NULL,
	name 			varchar(30) NOT NULL,
	description 	varchar(200),
	parent_id 		numeric(18),
	owner 			varchar(100) NOT NULL,
	inaction_time 	numeric(18),
	hidden 			smallint NOT NULL,
	ancestor 		numeric(18),
	modifiable 		smallint NOT NULL,
	deletes_after 	smallint NOT NULL,
	PRIMARY KEY (id)
);


CREATE TABLE dso_document_to_label(
	id 			numeric(18) NOT NULL,
	document_id numeric(18) NOT NULL,
	label_id 	numeric(18) NOT NULL,
	username 	varchar(100) NOT NULL,
	ctime 		TIMESTAMP NOT NULL,
	actionTime 	TIMESTAMP,
	PRIMARY KEY (id)
);

CREATE GENERATOR dso_label_id;
CREATE GENERATOR dso_label_to_document_id;