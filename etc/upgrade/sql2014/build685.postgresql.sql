CREATE TABLE DS_PUBLIC_KEY
   (ID numeric,
           DISCRIMINATOR character varying(40),
           USER_ID numeric,
           HOST_NAME character varying(500),
    PUBLIC_KEY character varying(5000)
   );



create sequence SEQ_DS_PUBLIC_KEY_ID START WITH 1 INCREMENT BY 1 CACHE 1;
