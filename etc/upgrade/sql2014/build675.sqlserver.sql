alter table DS_PACKAGE_DOCUMENTS add DZIAL integer ;
alter table DS_PACKAGE_DOCUMENTS add PACZKA_NA_DZIAL smallint;

ALTER TABLE DS_DOCUMENT ADD DEFAULT 0 FOR inpackage;

update DS_DOCUMENT set inpackage = 0 where inpackage is null;