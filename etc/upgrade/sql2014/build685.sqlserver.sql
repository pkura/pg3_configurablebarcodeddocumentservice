CREATE TABLE DS_PUBLIC_KEY
      ("ID" NUMBER,
       "DISCRIMINATOR" VARCHAR2(40 CHAR),
       "USER_ID" NUMBER,
       "HOST_NAME" VARCHAR2(500 CHAR),
       "PUBLIC_KEY" VARCHAR2(4000 CHAR)
      );
commit;