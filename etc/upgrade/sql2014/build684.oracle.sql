CREATE TABLE dso_shipping_unit_prices(
	"ID" NUMBER NOT NULL,
	"CN" VARCHAR2(50 BYTE) ,
	"TITLE" VARCHAR2(250 BYTE) ,
	"GAUGE" NUMBER ,
	"WEIGHT" NUMBER ,
	"DOCUMENTDELIVERY" NUMBER ,
	"ZPO" NUMBER(1) ,
	"PRICE" NUMBER(19,4) );

commit;

create sequence SEQ_dso_shipping_unit_priceID start with 1 nocache;
commit;

create or replace TRIGGER TRG_dso_shipping_unit_prices
before INSERT on dso_shipping_unit_prices
for each row
begin
  if :new.id is null then
  select SEQ_dso_shipping_unit_priceID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end;
--commit; 

INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('1', 'KRAJ_EKO_350_A', 'Krajowa ekonomiczna do 350g, gabaryt A', '1', '1', '1', 0, 1.7500);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('2', 'KRAJ_EKO_350_B', 'Krajowa ekonomiczna do 350g, gabaryt B', '2', '1', '1', 0, 3.7500);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('3', 'KRAJ_EKO_1000_A', 'Krajowa ekonomiczna do 1000g, gabaryt A', '1', '2', '1', 0, 3.7000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('4', 'KRAJ_EKO_1000_B', 'Krajowa ekonomiczna do 1000g, gabaryt B', '2', '2', '1', 0, 4.7500);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('5', 'KRAJ_EKO_2000_A', 'Krajowa ekonomiczna do 2000g, gabaryt A', '1', '3', '1', 0, 6.3000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('6', 'KRAJ_EKO_2000_A', 'Krajowa ekonomiczna do 2000g, gabaryt B', '2', '3', '1', 0, 7.3000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('7', 'KRAJ_EKO_POL_350_A', 'Krajowa ekonomiczna polecona do 350g, gabaryt A', '1', '1', '2', 0, 4.2000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('8', 'KRAJ_EKO_POL_350_B', 'Krajowa ekonomiczna polecona do 350g, gabaryt B', '2', '1', '2', 0, 7.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('9', 'KRAJ_EKO_POL_1000_A', 'Krajowa ekonomiczna polecona do 1000g, gabaryt A', '1', '2', '2', 0, 5.9000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('10', 'KRAJ_EKO_POL_1000_B', 'Krajowa ekonomiczna polecona do 1000g, gabaryt B', '2', '2', '2', 0, 8.3000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('11', 'KRAJ_EKO_POL_2000_A', 'Krajowa ekonomiczna polecona do 2000g, gabaryt A', '1', '3', '2', 0, 8.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('12', 'KRAJ_EKO_POL_2000_B', 'Krajowa ekonomiczna polecona do 2000g, gabaryt B', '2', '3', '2', 0, 9.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('13', 'KRAJ_EKO_POL_ZPO_350_A', 'Krajowa ekonomiczna polecona z ZPO do 350g, gabaryt A', '1', '1', '2', 1, 6.1000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('14', 'KRAJ_EKO_POL_ZPO_350_B', 'Krajowa ekonomiczna polecona z ZPO do 350g, gabaryt B', '2', '1', '2', 1, 9.4000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('15', 'KRAJ_EKO_POL_ZPO_1000_A', 'Krajowa ekonomiczna polecona z ZPO do 1000g, gabaryt A', '1', '2', '2', 1, 7.8000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('16', 'KRAJ_EKO_POL_ZPO_1000_B', 'Krajowa ekonomiczna polecona z ZPO do 1000g, gabaryt B', '2', '2', '2', 1, 10.2000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('17', 'KRAJ_EKO_POL_ZPO_2000_A', 'Krajowa ekonomiczna polecona z ZPO do 2000g, gabaryt A', '1', '3', '2', 1, 10.4000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('18', 'KRAJ_EKO_POL_ZPO_2000_B', 'Krajowa ekonomiczna polecona z ZPO do 2000g, gabaryt B', '2', '3', '2', 1, 11.4000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('19', 'KRAJ_PRIO_350_A', 'Krajowa priorytetowa do 350g, gabaryt A', '1', '1', '4', 0, 2.3500);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('20', 'KRAJ_PRIO_350_B', 'Krajowa priorytetowa do 350g, gabaryt B', '2', '1', '4', 0, 5.1000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('21', 'KRAJ_PRIO_1000_A', 'Krajowa priorytetowa do 1000g, gabaryt A', '1', '2', '4', 0, 4.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('22', 'KRAJ_PRIO_1000_B', 'Krajowa priorytetowa do 1000g, gabaryt B', '2', '2', '4', 0, 7.1000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('23', 'KRAJ_PRIO_2000_A', 'Krajowa priorytetowa do 2000g, gabaryt A', '1', '3', '4', 0, 8.8000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('24', 'KRAJ_PRIO_2000_A', 'Krajowa priorytetowa do 2000g, gabaryt B', '2', '3', '4', 0, 10.9000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('25', 'KRAJ_PRIO_POL_350_A', 'Krajowa priorytetowa polecona do 350g, gabaryt A', '1', '1', '5', 0, 5.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('26', 'KRAJ_PRIO_POL_350_B', 'Krajowa priorytetowa polecona do 350g, gabaryt B', '2', '1', '5', 0, 8.3000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('27', 'KRAJ_PRIO_POL_1000_A', 'Krajowa priorytetowa polecona do 1000g, gabaryt A', '1', '2', '5', 0, 7.2000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('28', 'KRAJ_PRIO_POL_1000_B', 'Krajowa priorytetowa polecona do 1000g, gabaryt B', '2', '2', '5', 0, 11.0000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('29', 'KRAJ_PRIO_POL_2000_A', 'Krajowa priorytetowa polecona do 2000g, gabaryt A', '1', '3', '5', 0, 11.0000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('30', 'KRAJ_PRIO_POL_2000_B', 'Krajowa priorytetowa polecona do 2000g, gabaryt B', '2', '3', '5', 0, 14.5000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('31', 'KRAJ_PRIO_POL_ZPO_350_A', 'Krajowa priorytetowa polecona z ZPO do 350g, gabaryt A', '1', '1', '5', 1, 7.4000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('32', 'KRAJ_PRIO_POL_ZPO_350_B', 'Krajowa priorytetowa polecona z ZPO do 350g, gabaryt B', '2', '1', '5', 1, 10.2000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('33', 'KRAJ_PRIO_POL_ZPO_1000_A', 'Krajowa priorytetowa polecona z ZPO do 1000g, gabaryt A', '1', '2', '5', 1, 9.1000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('34', 'KRAJ_PRIO_POL_ZPO_1000_B', 'Krajowa priorytetowa polecona z ZPO do 1000g, gabaryt B', '2', '2', '5', 1, 12.9000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('35', 'KRAJ_PRIO_POL_ZPO_2000_A', 'Krajowa priorytetowa polecona z ZPO do 2000g, gabaryt A', '1', '3', '5', 1, 12.9000);
INSERT INTO dso_shipping_unit_prices (ID, CN, TITLE, GAUGE, WEIGHT, DOCUMENTDELIVERY, ZPO, PRICE) VALUES ('36', 'KRAJ_PRIO_POL_ZPO_2000_B', 'Krajowa priorytetowa polecona z ZPO do 2000g, gabaryt B', '2', '3', '5', 1, 16.4000);

alter table dso_out_document_delivery add foreignDelivery NUMBER;
alter table dso_out_document_delivery add priorityKind NUMBER;
alter table dso_out_document add additionalZpo NUMBER;