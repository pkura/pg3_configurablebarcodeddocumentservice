alter table ds_public_key add column key_type character varying(250 ) not null;
alter table ds_public_key add column enable_uri character varying(600);
insert into ds_public_key(DISCRIMINATOR, USER_ID, HOST_NAME, PUBLIC_KEY,KEY_TYPE,enable_uri) values ('HOST',null,'*',null,'ACCESS','*');

alter table dso_email_channel add password varchar(100) not null;
alter table dso_email_channel drop column pass;

ALTER TABLE DSO_EMAIL_CHANNEL ADD DOCUMENT_KIND_ID NUMERIC(19, 0);

ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_IS_ENABLED boolean default false not null;