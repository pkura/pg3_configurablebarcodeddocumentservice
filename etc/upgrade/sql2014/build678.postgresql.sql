CREATE TABLE DS_CASE_TO_CASE 
(
  ID bigint NOT NULL 
, CASECONTAINERID bigint NOT NULL 
, CREATINGUSERID int  NOT NULL 
, CREATINGUSERNAME character varying(100) 
, CREATIONTIME TIMESTAMP NOT NULL 
, CASEOFFICEID character varying(100) 
, CASETITLE character varying(200) 
, CASEAUTHOR character varying(62) 
, LINKTOCASE character varying(200) 
, LINKEDCASEID bigint NOT NULL 
, CASEOPENDATE TIMESTAMP 
, CASEFINISHDATE TIMESTAMP 
);
commit;
create sequence  DS_CASE_TO_CASE_ID start with 1 INCREMENT BY 1 ;
commit;



CREATE OR REPLACE FUNCTION insert_to_DS_CASE_TO_CASE()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_CASE_TO_CASE_ID');--@ignore-semicolon
   Return NEW;--@ignore-semicolon
 END IF ;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_DS_CASE_TO_CASE
 BEFORE INSERT ON DS_CASE_TO_CASE
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_CASE_TO_CASE();
 