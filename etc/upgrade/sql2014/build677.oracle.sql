create table dsj_jcrpermissions
(
	source varchar2(30),
	parent varchar2(64),
	username varchar2(64),
	document_id numeric(19,0)
);
