CREATE TABLE DS_XES_LOG
   (	"ID" NUMBER(19,0) NOT NULL ENABLE, 
	"CREATINGTIME" DATE NOT NULL ENABLE, 
	"XESLOGTYPE" NUMBER NOT NULL ENABLE, 
	"USERID" NUMBER(19,0) NOT NULL ENABLE, 
	"DOCUMENTID" NUMBER(19,0), 
	"ACTIONTIME" TIMESTAMP (6) NOT NULL ENABLE, 
	"ACTIONNAME" VARCHAR2(62 BYTE) NOT NULL ENABLE, 
	"ACTIVITIID" VARCHAR2(20 BYTE), 
	"VERSIONID" NUMBER(19,0) NOT NULL ENABLE, 
	"ACTIONDATAID" NUMBER(19,0) NOT NULL ENABLE, 
	"ACTIVELOG" NUMBER, 
	"ACTIONCLASS" VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	"CASEID" NUMBER(19,0), 
	"OFFICEFOLDERID" NUMBER(19,0), 
	"ACTIONURI" VARCHAR2(200 BYTE)
   );

commit;
create sequence  SEQ_DSO_DS_XES_LOG_ID start with 1 nocache;
commit;
create  TRIGGER  TRG_SEQ_DSO_DS_XES_LOG
before insert on DS_XES_LOG
for each row
begin
  if :new.id is null then
 select SEQ_DSO_DS_XES_LOG_ID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end;

commit;

CREATE TABLE DS_XES_LOG_DATA
   (	"ID" NUMBER(19,0) NOT NULL ENABLE, 
	"CREATINGTIME" DATE NOT NULL ENABLE, 
	"XESLOGTYPE" NUMBER NOT NULL ENABLE, 
	"USERID" NUMBER(19,0) NOT NULL ENABLE, 
	"DOCUMENTID" NUMBER(19,0), 
	"ACTIONTIME" TIMESTAMP (6) NOT NULL ENABLE, 
	"ACTIONNAME" VARCHAR2(62 BYTE) NOT NULL ENABLE, 
	"ACTIVITIID" VARCHAR2(20 BYTE), 
	"VERSIONID" NUMBER(19,0) NOT NULL ENABLE, 
	"ACTIONDATAID" NUMBER(19,0) NOT NULL ENABLE, 
	"XESLOGID" NUMBER(19,0), 
	"DOCKINDID" NUMBER(19,0), 
	"XESXMLDATA" BLOB, 
	"ACTIONCLASS" VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	"CASEID" NUMBER(19,0), 
	"OFFICEFOLDERID" NUMBER(19,0), 
	"ACTIONURI" VARCHAR2(200 BYTE)
  );

commit;
create sequence  SEQ_DSO_DS_XES_LOG_DATA_ID start with 1 nocache;
commit;
create  TRIGGER  TRG_SEQ_DSO_DS_XES_LOG_DATA
before insert on DS_XES_LOG_DATA
for each row
begin
  if :new.id is null then
 select SEQ_DSO_DS_XES_LOG_DATA_ID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end;

commit;


CREATE TABLE DS_XES_LOGGED_ACTIONS
   (	"ID" NUMBER(19,0) NOT NULL ENABLE, 
	"ACTIONCLASS" VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	"LOGTHISACTION" NUMBER DEFAULT 0 NOT NULL ENABLE, 
	"ACTIONNAME" VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	"SHOWINGNAME" VARCHAR2(200 BYTE) NOT NULL ENABLE, 
	"URI" VARCHAR2(200 BYTE), 
	"METHODNAME" VARCHAR2(100 BYTE), 
	"SETINGNAME" VARCHAR2(100 BYTE), 
	 PRIMARY KEY ("ID")
   );
   
create sequence  SEQ_DS_XES_LOGGED_ACTIONS_ID start with 45 nocache;
commit;
create  TRIGGER  TRG_SEQ_DS_XES_LOGGED_ACTIONS
before insert on DS_XES_LOGGED_ACTIONS
for each row
begin
  if :new.id is null then
 select SEQ_DS_XES_LOGGED_ACTIONS_ID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end;

commit;


Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('1','pl.compan.docusafe.web.office.AcceptWfAssignmentAction','1','/docusafe/office/accept-wf-assignment.action','createXesXmlForRecivingDecretation','doAccept','Odebrano dekretacje','doAcceptDecretation');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('2','pl.compan.docusafe.web.office.in.DocumentArchiveAction','1','/docusafe/office/incoming/document-archive.action','createXesXmlForUpdateDocument','doUpdateGoToTasklist','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('3','pl.compan.docusafe.web.office.in.DocumentArchiveAction','1','/docusafe/office/incoming/document-archive.action','createXesXmlForUpdateDocument','doUpdate','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('4','pl.compan.docusafe.web.office.in.CaseAction','1','/docusafe/office/incoming/case.action','createXesXmlForRemoveFromCaseDocument','doRemoveFromCase','Usunieto dokument z sprawy','doRemoveFromCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('5','pl.compan.docusafe.web.office.in.CaseAction','0','/docusafe/office/incoming/case.action',null,'doPreCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('6','pl.compan.docusafe.web.office.EditCaseAction','0','/docusafe/office/edit-case.do',null,'AddCaseToCase','Dodanie powiazania sprawy do sprawy',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('7','pl.compan.docusafe.web.office.out.DocumentArchiveAction','1','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doUpdateGoToTasklist','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('8','pl.compan.docusafe.web.office.in.CaseAction','1','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doAddToCase','Dodanie dokumentu do sprawy','doAddToCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('9','pl.compan.docusafe.web.office.in.ManualMultiAssignmentAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('10','pl.compan.docusafe.core.users.auth.CASLogin','1','/docusafe/login.jsp','createLoginXesXmlData','login','Zalogowano do systemu','login');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('11','pl.compan.docusafe.web.common.LogoutAction','1','/docusafe/logout.do','createLogoutXesXmlData','logout','Wylogowano z systemu','logout');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('12','pl.compan.docusafe.web.IndexAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('13','pl.compan.docusafe.web.office.in.AttachmentsAction','1','/docusafe/office/incoming/attachments.action','createXesXmlForAddAttachment','doAdd','Dodanie zalacznika','doAddAttachment');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('14','pl.compan.docusafe.web.office.out.CaseAction','1','/docusafe/office/outgoing/case.action','createXesXmlForAddToCaseDocument','doAddToCase','Dodanie dokumentu do sprawy','doAddToCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('15','pl.compan.docusafe.web.office.in.CaseAction','1','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doCreate','Utworzono sprawe i dodano do niej dokument','doCreateCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('16','pl.compan.docusafe.web.office.EditCaseAction','0','/docusafe/office/edit-case.do',null,'DeleteCaseToCase','Usuniecie powiazania sprawy ze sprawa',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('17','pl.compan.docusafe.web.office.out.DocumentArchiveAction','1','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doSaveAndAssignOfficeNumber','Zapisanie i nadanie numeru Ko','doSaveAndAssignOfficeNumber');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('18','pl.compan.docusafe.web.office.in.CaseAction','1','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doAddToNextCase','Dodanie dokumentu do kolejnej sprawy','doAddToNextCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('19','pl.compan.docusafe.web.office.in.ManualMultiAssignmentAction','1','/docusafe/office/incoming/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('20','pl.compan.docusafe.web.office.int.ManualMultiAssignmentAction','1','/docusafe/office/internal/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('21','pl.compan.docusafe.web.office.out.ManualMultiAssignmentAction','1','/docusafe/office/outgoing/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('22','pl.compan.docusafe.web.office.in.DwrDocumentMainAction','1','/docusafe/office/incoming/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('23','pl.compan.docusafe.web.office.int.DwrDocumentMainAction','1','/docusafe/office/internal/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('24','pl.compan.docusafe.web.office.out.DwrDocumentMainAction','1','/docusafe/office/outgoing/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('25','pl.compan.docusafe.web.archive.settings.ApplicationSettingsAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('26','pl.compan.docusafe.web.office.out.CaseAction','0','/docusafe/office/outgoing/case.action',null,'doCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('27','pl.compan.docusafe.web.office.out.CaseAction','1','/docusafe/office/outgoing/case.action','createXesXmlForAddToCaseDocument','doAddToNextCase','Dodanie dokumentu do kolejnej sprawy','doAddToNextCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('28','pl.compan.docusafe.web.office.out.CaseAction','1','/docusafe/office/outgoing/case.action','createXesXmlForRemoveFromCaseDocument','doRemoveFromCase','Usunieto dokument z sprawy','doRemoveFromCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('29','pl.compan.docusafe.webwork.NewOfficeDocumentRedirectAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('30','pl.compan.docusafe.web.office.in.DwrDocumentMainAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('31','pl.compan.docusafe.core.xes.XesLogAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('32','pl.compan.docusafe.core.xes.XesLogAction','0','nn',null,'doGetXesLog','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('33','pl.compan.docusafe.core.xes.XesLogAction','0','nn',null,'doSaveSetings','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('34','pl.compan.docusafe.web.office.in.AttachmentsAction','1','/docusafe/office/incoming/attachments.action','createXesXmlForDeleteAttachment','doDelete','Usuniecie zalacznika','doDeleteAttachment');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('35','pl.compan.docusafe.web.office.out.CaseAction','0','/docusafe/office/outgoing/case.action',null,'doPreCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('36','pl.compan.docusafe.web.office.EditCaseAction','0','/docusafe/office/edit-case.do',null,'SetForward','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('37','pl.compan.docusafe.web.office.EditCaseAction','0','/docusafe/office/edit-case.do',null,'FillForm','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('38','pl.compan.docusafe.web.office.EditCaseAction','0','/docusafe/office/edit-case.do',null,'AddRemark','Dodanie komentarza do sprawy',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('39','pl.compan.docusafe.web.office.out.DocumentArchiveAction','1','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doUpdate','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('40','pl.compan.docusafe.web.office.out.DocumentArchiveAction','0','/docusafe/office/outgoing/document-archive.action',null,'doWyslijDoEpuap','Wyslanie pisma do ePUAP','doWyslijDoEpuap');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('41','pl.compan.docusafe.web.certificates.SignXmlAction','0','/docusafe/office/outgoing/document-archive.action',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('42','pl.compan.docusafe.web.office.out.DocumentArchiveAction','0','/docusafe/office/outgoing/document-archive.action',null,'doProcessAction','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('43','pl.compan.docusafe.web.office.tasklist.CurrentUserTaskListAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('44','pl.compan.docusafe.web.office.tasklist.UserTaskListAction','0','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);

commit;
