alter table DS_PACKAGE_DOCUMENTS add DZIAL INTEGER ;
alter table DS_PACKAGE_DOCUMENTS add PACZKA_NA_DZIAL boolean ;

ALTER TABLE DS_DOCUMENT ALTER COLUMN inpackage SET DEFAULT 0;

update DS_DOCUMENT set inpackage = 0 where inpackage is null;