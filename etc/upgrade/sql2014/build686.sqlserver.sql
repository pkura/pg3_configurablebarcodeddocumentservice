alter table ds_public_key add key_type VARCHAR(10) not null;
alter table ds_public_key add enable_uri VARCHAR(600);
insert into ds_public_key(DISCRIMINATOR, USER_ID, HOST_NAME, PUBLIC_KEY,KEY_TYPE,enable_uri) values ('HOST',null,'*',null,'ACCESS','*');