CREATE SEQUENCE dsa_mapped_field_id start WITH 1;

create table dsa_mapped_field(
    id SERIAL PRIMARY KEY NOT NULL,
    mapping_type VARCHAR(20) NOT NULL,
    dockind_cn VARCHAR(60) NOT NULL,
    dockind_field_cn VARCHAR(100) NOT NULL,
    external_field_cn VARCHAR(100) NOT NULL
 );

alter table ds_document add uuid VARCHAR(300);
