alter table ds_public_key add key_type VARCHAR2(10 CHAR) NOT NULL;
alter table ds_public_key add enable_uri VARCHAR2(600 CHAR);
insert into ds_public_key values (SEQ_DS_PUBLIC_KEY_ID.nextval,'HOST',null,'*',null,'ACCESS','*');
commit;