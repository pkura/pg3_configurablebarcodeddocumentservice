
CREATE TABLE DS_XES_LOG
   (	ID numeric(19,0) NOT NULL , 
	CREATINGTIME DATE NOT NULL , 
	XESLOGTYPE integer NOT NULL , 
	USERID numeric(19,0) NOT NULL , 
	DOCUMENTID numeric(19,0), 
	ACTIONTIME TIMESTAMP (6) NOT NULL ,
	ACTIONNAME character varying(62 ) NOT NULL , 
	ACTIVITIID character varying(20 ), 
	VERSIONID numeric(19,0) NOT NULL , 
	ACTIONDATAID numeric(19,0) NOT NULL , 
	ACTIVELOG boolean, 
	ACTIONCLASS character varying(200 ) NOT NULL , 
	CASEID numeric(19,0), 
	OFFICEFOLDERID numeric(19,0), 
	ACTIONURI character varying(200 )
   );

CREATE sequence SEQ_DS_XES_LOG_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE FUNCTION insert_to_DS_XES_LOG()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   NEW.id:=nextval('SEQ_DS_XES_LOG_ID');--@ignore-semicolon
 END IF ;--@ignore-semicolon
 Return NEW;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_SEQ_DSO_DS_XES_LOG
 BEFORE INSERT
 ON DS_XES_LOG
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_XES_LOG();

 CREATE TABLE DS_XES_LOG_DATA
   (	"ID" numeric(19,0) NOT NULL , 
	CREATINGTIME DATE NOT NULL , 
	XESLOGTYPE integer NOT NULL , 
	USERID numeric(19,0) NOT NULL , 
	DOCUMENTID numeric(19,0), 
	ACTIONTIME TIMESTAMP (6) NOT NULL , 
	ACTIONNAME character varying(62 ) NOT NULL , 
	ACTIVITIID character varying(20 ), 
	VERSIONID numeric(19,0) NOT NULL , 
	ACTIONDATAID numeric(19,0) NOT NULL , 
	XESLOGID numeric(19,0), 
	DOCKINDID numeric(19,0), 
	XESXMLDATA bytea, 
	ACTIONCLASS character varying(200) NOT NULL, 
	CASEID numeric(19,0), 
	OFFICEFOLDERID numeric(19,0), 
	ACTIONURI character varying(200 )
  );

CREATE sequence SEQ_DS_XES_LOG_DATA_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE FUNCTION insert_to_DS_XES_LOG_DATA()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('SEQ_DS_XES_LOG_DATA_ID');--@ignore-semicolon
 END IF ;--@ignore-semicolon
    Return NEW;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_SEQ_DS_XES_LOGGED_ACTIONS
 BEFORE INSERT ON DS_XES_LOG_DATA
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_XES_LOG_DATA();
 
 CREATE TABLE DS_XES_LOGGED_ACTIONS
   (	ID numeric(19,0) NOT NULL , 
	ACTIONCLASS character varying(200 ) NOT NULL ,
	LOGTHISACTION boolean DEFAULT false NOT NULL , 
	ACTIONNAME character varying(200 ) NOT NULL , 
	SHOWINGNAME character varying(200 ) NOT NULL , 
	URI character varying(200), 
	METHODNAME character varying(100 ), 
	SETINGNAME character varying(100 ), 
	 PRIMARY KEY (ID)
   );

commit;
CREATE sequence SEQ_DS_XES_LOGGED_ACTIONS_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 45
INCREMENT BY 1;

CREATE OR REPLACE FUNCTION insert_to_DS_XES_LOGGED_ACTIONS()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('SEQ_DS_XES_LOGGED_ACTIONS_ID');--@ignore-semicolon
 END IF ;--@ignore-semicolon
    Return NEW;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_SEQ_DS_XES_LOGGED_ACTIONS
 BEFORE INSERT ON DS_XES_LOGGED_ACTIONS
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_XES_LOGGED_ACTIONS();

 
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('1','pl.compan.docusafe.web.office.AcceptWfAssignmentAction','true','/docusafe/office/accept-wf-assignment.action','createXesXmlForRecivingDecretation','doAccept','Odebrano dekretacje','doAcceptDecretation');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('2','pl.compan.docusafe.web.office.in.DocumentArchiveAction','true','/docusafe/office/incoming/document-archive.action','createXesXmlForUpdateDocument','doUpdateGoToTasklist','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('3','pl.compan.docusafe.web.office.in.DocumentArchiveAction','true','/docusafe/office/incoming/document-archive.action','createXesXmlForUpdateDocument','doUpdate','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('4','pl.compan.docusafe.web.office.in.CaseAction','true','/docusafe/office/incoming/case.action','createXesXmlForRemoveFromCaseDocument','doRemoveFromCase','Usunieto dokument z sprawy','doRemoveFromCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('5','pl.compan.docusafe.web.office.in.CaseAction','false','/docusafe/office/incoming/case.action',null,'doPreCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('6','pl.compan.docusafe.web.office.EditCaseAction','false','/docusafe/office/edit-case.do',null,'AddCaseToCase','Dodanie powiazania sprawy do sprawy',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('7','pl.compan.docusafe.web.office.out.DocumentArchiveAction','true','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doUpdateGoToTasklist','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('8','pl.compan.docusafe.web.office.in.CaseAction','true','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doAddToCase','Dodanie dokumentu do sprawy','doAddToCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('9','pl.compan.docusafe.web.office.in.ManualMultiAssignmentAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('10','pl.compan.docusafe.core.users.auth.CASLogin','true','/docusafe/login.jsp','createLoginXesXmlData','login','Zalogowano do systemu','login');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('11','pl.compan.docusafe.web.common.LogoutAction','true','/docusafe/logout.do','createLogoutXesXmlData','logout','Wylogowano z systemu','logout');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('12','pl.compan.docusafe.web.IndexAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('13','pl.compan.docusafe.web.office.in.AttachmentsAction','true','/docusafe/office/incoming/attachments.action','createXesXmlForAddAttachment','doAdd','Dodanie zalacznika','doAddAttachment');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('14','pl.compan.docusafe.web.office.out.CaseAction','true','/docusafe/office/outgoing/case.action','createXesXmlForAddToCaseDocument','doAddToCase','Dodanie dokumentu do sprawy','doAddToCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('15','pl.compan.docusafe.web.office.in.CaseAction','true','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doCreate','Utworzono sprawe i dodano do niej dokument','doCreateCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('16','pl.compan.docusafe.web.office.EditCaseAction','false','/docusafe/office/edit-case.do',null,'DeleteCaseToCase','Usuniecie powiazania sprawy ze sprawa',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('17','pl.compan.docusafe.web.office.out.DocumentArchiveAction','true','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doSaveAndAssignOfficeNumber','Zapisanie i nadanie numeru Ko','doSaveAndAssignOfficeNumber');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('18','pl.compan.docusafe.web.office.in.CaseAction','true','/docusafe/office/incoming/case.action','createXesXmlForAddToCaseDocument','doAddToNextCase','Dodanie dokumentu do kolejnej sprawy','doAddToNextCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('19','pl.compan.docusafe.web.office.in.ManualMultiAssignmentAction','true','/docusafe/office/incoming/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('20','pl.compan.docusafe.web.office.int.ManualMultiAssignmentAction','true','/docusafe/office/internal/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('21','pl.compan.docusafe.web.office.out.ManualMultiAssignmentAction','true','/docusafe/office/outgoing/manual-multi-assignment.action','createXesXmlForAssignemntDocument','doAssign','Dekretacja pisma','doAssign');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('22','pl.compan.docusafe.web.office.in.DwrDocumentMainAction','true','/docusafe/office/incoming/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('23','pl.compan.docusafe.web.office.int.DwrDocumentMainAction','true','/docusafe/office/internal/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('24','pl.compan.docusafe.web.office.out.DwrDocumentMainAction','true','/docusafe/office/outgoing/dwr-document-main.action','createXesXmlForCreatingDocument','doCreate','Utworzenie pisma','doCreateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('25','pl.compan.docusafe.web.archive.settings.ApplicationSettingsAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('26','pl.compan.docusafe.web.office.out.CaseAction','false','/docusafe/office/outgoing/case.action',null,'doCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('27','pl.compan.docusafe.web.office.out.CaseAction','true','/docusafe/office/outgoing/case.action','createXesXmlForAddToCaseDocument','doAddToNextCase','Dodanie dokumentu do kolejnej sprawy','doAddToNextCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('28','pl.compan.docusafe.web.office.out.CaseAction','true','/docusafe/office/outgoing/case.action','createXesXmlForRemoveFromCaseDocument','doRemoveFromCase','Usunieto dokument z sprawy','doRemoveFromCase');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('29','pl.compan.docusafe.webwork.NewOfficeDocumentRedirectAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('30','pl.compan.docusafe.web.office.in.DwrDocumentMainAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('31','pl.compan.docusafe.core.xes.XesLogAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('32','pl.compan.docusafe.core.xes.XesLogAction','false','nn',null,'doGetXesLog','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('33','pl.compan.docusafe.core.xes.XesLogAction','false','nn',null,'doSaveSetings','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('34','pl.compan.docusafe.web.office.in.AttachmentsAction','true','/docusafe/office/incoming/attachments.action','createXesXmlForDeleteAttachment','doDelete','Usuniecie zalacznika','doDeleteAttachment');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('35','pl.compan.docusafe.web.office.out.CaseAction','false','/docusafe/office/outgoing/case.action',null,'doPreCreate','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('36','pl.compan.docusafe.web.office.EditCaseAction','false','/docusafe/office/edit-case.do',null,'SetForward','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('37','pl.compan.docusafe.web.office.EditCaseAction','false','/docusafe/office/edit-case.do',null,'FillForm','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('38','pl.compan.docusafe.web.office.EditCaseAction','false','/docusafe/office/edit-case.do',null,'AddRemark','Dodanie komentarza do sprawy',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('39','pl.compan.docusafe.web.office.out.DocumentArchiveAction','true','/docusafe/office/outgoing/document-archive.action','createXesXmlForUpdateDocument','doUpdate','Aktualizacja dokumentu','doUpdateDocument');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('40','pl.compan.docusafe.web.office.out.DocumentArchiveAction','false','/docusafe/office/outgoing/document-archive.action',null,'doWyslijDoEpuap','Wyslanie pisma do ePUAP','doWyslijDoEpuap');
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('41','pl.compan.docusafe.web.certificates.SignXmlAction','false','/docusafe/office/outgoing/document-archive.action',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('42','pl.compan.docusafe.web.office.out.DocumentArchiveAction','false','/docusafe/office/outgoing/document-archive.action',null,'doProcessAction','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('43','pl.compan.docusafe.web.office.tasklist.CurrentUserTaskListAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
Insert into DS_XES_LOGGED_ACTIONS (ID,ACTIONCLASS,LOGTHISACTION,URI,METHODNAME,ACTIONNAME,SHOWINGNAME,SETINGNAME) values ('44','pl.compan.docusafe.web.office.tasklist.UserTaskListAction','false','nn',null,'pl.compan.docusafe.webwork.event.EventActionSupport.DEFAULT_ACTION','nn',null);
