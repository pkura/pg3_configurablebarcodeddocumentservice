CREATE SEQUENCE dsa_mapped_field_id start WITH 1 nocache;

commit;

create table dsa_mapped_field(
    id NUMERIC(19,0),
    mapping_type VARCHAR2(20 CHAR) NOT NULL,
    dockind_cn VARCHAR2(60 CHAR) NOT NULL,
    dockind_field_cn VARCHAR2(100 CHAR) NOT NULL,
    external_field_cn VARCHAR2(100 CHAR) NOT NULL
 );

alter table ds_document add uuid VARCHAR2(300 char);
       commit;
alter table ds_attachment add uuid VARCHAR2(300 char);
commit;
