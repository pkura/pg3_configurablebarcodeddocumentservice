--@ignore-exceptions;
alter table DS_DOCUMENT add inPackage INTEGER NULL;
commit;
--@ignore-exceptions;
 Create or replace force view USER_TASKLIST_VIEV as 
  SELECT DISTINCT
    t.document_id as ID,
    t.assigned_resource AS USER_TASK ,
    t.document_type     AS DOC_TYPE,
    dc.name             AS DOC_KIND_NAME,
    t.document_id       AS DOC_ID,
    t.office_number     AS DOC_OFFICENUMBER,
    t.document_ctime    AS DOC_DATA,
    t.summary           AS DOC_DESCRIPTION,
    din.barcode || dout.barcode  AS DOC_BARCODE ,
    t.case_office_id    AS DOC_CASEDOCUMENTID,
    t.last_user         AS DOC_DEKRETUJACY ,
    t.document_author   AS DOC_AUTOR,
    din.sender  ||  dout.sender AS DOC_SENDER ,  
    doc.dockind_id      AS DOC_KINDID,
    doc.inPackage       as DOC_IN_PACKAGE
  FROM dsw_jbpm_tasklist t
  LEFT JOIN ds_document doc
  ON (t.document_id = doc.id)
  LEFT JOIN dso_in_document din
  ON ( doc.id = din.id)
  LEFT JOIN dso_OUT_document dout
  ON ( doc.id = dout.id)
  LEFT JOIN ds_document_kind dc
  ON (doc.dockind_id = dc.id)
  ORDER BY t.document_id desc;
  
commit;
  
  
  --@ignore-exceptions;
  CREATE TABLE DS_PACKAGE_DOCUMENTS (
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0),
	EMAILNADAWCA VARCHAR2(100 CHAR),
	EMAILODBIORCA VARCHAR2(100 CHAR),
	STATUS INTEGER,
	USER_NADAWCA INTEGER,
	USER_ODBIORCA INTEGER
	
);
commit;
 
--@ignore-exceptions;
CREATE TABLE DS_PACKAGE_DOCUMENTS_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
    
);
--@ignore-exceptions;
create sequence  DS_PACKAGE_DOCU_MULTIPLE_ID start with 1 nocache;

--@ignore-exceptions;
create or replace trigger TRGDS_PACKAGE_DOCU_MULTIPLE_ID
before insert on DS_PACKAGE_DOCUMENTS_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PACKAGE_DOCU_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;

