CREATE TABLE DS_CASE_TO_CASE 
(
  ID NUMBER (18,0) NOT NULL 
, CASECONTAINERID NUMBER(18,0) NOT NULL 
, CREATINGUSERID NUMBER  NOT NULL 
, CREATINGUSERNAME VARCHAR2(100) 
, CREATIONTIME DATE NOT NULL 
, CASEOFFICEID VARCHAR2(100) 
, CASETITLE VARCHAR2(200) 
, CASEAUTHOR VARCHAR2(62) 
, LINKTOCASE VARCHAR2(200) 
, LINKEDCASEID NUMBER (18,0) NOT NULL 
, CASEOPENDATE DATE 
, CASEFINISHDATE DATE 
);
commit;
create sequence  DS_CASE_TO_CASE_ID start with 1 nocache;
commit;

create  TRIGGER  TRG_DS_CASE_TO_CASE
before insert on DS_CASE_TO_CASE
for each row
begin
  if :new.id is null then
 select DS_CASE_TO_CASE_ID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end
commit;


