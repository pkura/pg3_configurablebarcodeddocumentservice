
alter table DS_DOCUMENT add inPackage INTEGER NULL;


 Create or replace view USER_TASKLIST_VIEV as 
  SELECT DISTINCT
    t.document_id as ID,
    t.assigned_resource AS USER_TASK ,
    t.document_type     AS DOC_TYPE,
    dc.name             AS DOC_KIND_NAME,
    t.document_id       AS DOC_ID,
    t.office_number     AS DOC_OFFICENUMBER,
    t.document_ctime    AS DOC_DATA,
    t.summary           AS DOC_DESCRIPTION,
    din.barcode || dout.barcode  AS DOC_BARCODE ,
    t.case_office_id    AS DOC_CASEDOCUMENTID,
    t.last_user         AS DOC_DEKRETUJACY ,
    t.document_author   AS DOC_AUTOR,
    din.sender::varchar || dout.sender::varchar AS DOC_SENDER ,  
    doc.dockind_id      AS DOC_KINDID,
    doc.inPackage       as DOC_IN_PACKAGE
  FROM dsw_jbpm_tasklist t
  LEFT JOIN ds_document doc
  ON (t.document_id = doc.id)
  LEFT JOIN dso_in_document din
  ON ( doc.id = din.id)
  LEFT JOIN dso_OUT_document dout
  ON ( doc.id = dout.id)
  LEFT JOIN ds_document_kind dc
  ON (doc.dockind_id = dc.id)
  ORDER BY t.document_id desc;
 
 

  CREATE TABLE DS_PACKAGE_DOCUMENTS (
	DOCUMENT_ID bigint NOT NULL,
	MIESCEUTWORZENIA VARCHAR(400),
	DODATKOWE_METADANE bigint,
	EMAILNADAWCA VARCHAR(100),
	EMAILODBIORCA VARCHAR(100),
	STATUS INTEGER,
	USER_NADAWCA INTEGER,
	USER_ODBIORCA INTEGER
);
 

CREATE TABLE DS_PACKAGE_DOCUMENTS_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
    
);
 

create sequence  DS_PACKAGE_DOCU_MULTIPLE_ID start with 1 INCREMENT BY 1;
CREATE OR REPLACE FUNCTION insert_to_DSPACKAGE_DOCU_MULTIPLE_ID()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.ID IS NULL THEN
  NEW.ID:=nextval('DS_PACKAGE_DOCU_MULTIPLE_ID');--@ignore-semicolon
 RETURN NEW;--@ignore-semicolon
  END IF;--@ignore-semicolon
 END;--@ignore-semicolon
  $BODY$
 LANGUAGE 'plpgsql' VOLATILE;


create trigger TRGDS_PACKAGE_DOCU_MULTIPLE_ID
before insert on DS_PACKAGE_DOCUMENTS_MULTIPLE
for each row 
EXECUTE PROCEDURE insert_to_DSPACKAGE_DOCU_MULTIPLE_ID();