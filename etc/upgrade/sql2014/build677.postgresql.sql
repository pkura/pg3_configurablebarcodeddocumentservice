create table dsj_jcrpermissions
(
	source varchar(30),
	parent varchar(64),
	username varchar(64),
	document_id numeric(19,0)
);
