create table dsa_mapped_field(
    id NUMERIC(19,0) IDENTITY(1,1) NOT NULL,
    mapping_type VARCHAR(20) NOT NULL,
    dockind_cn VARCHAR(60) NOT NULL,
    dockind_field_cn VARCHAR(100) NOT NULL,
    external_field_cn VARCHAR(100) NOT NULL
    CONSTRAINT [dsa_mapped_field_pk] PRIMARY KEY CLUSTERED (
    	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
 ) ON [PRIMARY]

alter table ds_document add uuid VARCHAR(300);