
alter table DS_DOCUMENT add inPackage smallint;




 Create VIEW USER_TASKLIST_VIEV as 
 SELECT DISTINCT
    t.document_id as ID,
    t.assigned_resource AS USER_TASK ,
    t.document_type     AS DOC_TYPE,
    dc.name             AS DOC_KIND_NAME,
    t.document_id       AS DOC_ID,
    t.office_number     AS DOC_OFFICENUMBER,
    t.document_ctime    AS DOC_DATA,
    t.summary           AS DOC_DESCRIPTION,
    din.barcode + dout.barcode  AS DOC_BARCODE ,
    t.case_office_id    AS DOC_CASEDOCUMENTID,
    t.last_user         AS DOC_DEKRETUJACY ,
    t.document_author   AS DOC_AUTOR,
    din.sender  +  dout.sender AS DOC_SENDER ,  
    doc.dockind_id      AS DOC_KINDID
  
  FROM dsw_jbpm_tasklist t
  LEFT JOIN ds_document doc
  ON (t.document_id = doc.id)
  LEFT JOIN dso_in_document din
  ON ( doc.id = din.id)
  LEFT JOIN dso_OUT_document dout
  ON ( doc.id = dout.id)
  LEFT JOIN ds_document_kind dc
  ON (doc.dockind_id = dc.id);
  
  
   CREATE TABLE DS_PACKAGE_DOCUMENTS (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR(400),
	DODATKOWE_METADANE numeric(18,0),
	EMAILNADAWCA VARCHAR(100 ),
	EMAILODBIORCA VARCHAR(100),
	STATUS INTEGER,
	USER_NADAWCA INTEGER,
	USER_ODBIORCA INTEGER
	
);

 

CREATE TABLE DS_PACKAGE_DOCUMENTS_MULTIPLE(
	id NUMERIC(19,0) IDENTITY(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN VARCHAR(100) ,
    FIELD_VAL VARCHAR(100) 
    
);