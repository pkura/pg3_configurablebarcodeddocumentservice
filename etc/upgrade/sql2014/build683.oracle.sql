  CREATE TABLE DSO_USER_TO_JASPER_REPORT
   ("ID" NUMBER,
   	"REPORTNAME" VARCHAR2(62 BYTE), 
	"USERID" NUMBER NOT NULL ENABLE, 
	"USERNAME" VARCHAR2(62 BYTE), 
	"USERFIRSTNAME" VARCHAR2(62 BYTE), 
	"USERLASTNAME" VARCHAR2(62 BYTE), 
	"EXTERNALNAME" VARCHAR2(62 BYTE), 
	"REPORTID" NUMBER 
   );
commit;

create sequence SEQ_DSO_USER_TO_JASPER_ID start with 1 nocache;
commit;

create TRIGGER TRG_SEQ_DSO_USER_TO_JASPER
before insert on DSO_USER_TO_JASPER_REPORT
for each row
begin
  if :new.id is null then
  select SEQ_DSO_USER_TO_JASPER_ID.nextval into :new.id from dual;--@ignore-semicolon
  end if;--@ignore-semicolon
end;
commit;




