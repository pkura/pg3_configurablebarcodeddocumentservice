CREATE TABLE dso_shipping_unit_prices(
	ID numeric(19,0) NOT NULL,
	cn character varying(50 ) ,
	title character varying(250 ) ,
	gauge numeric(18, 0) ,
	weight numeric(18, 0) ,
	documentDelivery numeric(18, 0) ,
	zpo boolean ,
	price money );


CREATE sequence SEQ_dso_shipping_unit_prices_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;

CREATE OR REPLACE FUNCTION insert_to_dso_shipping_unit_prices()
RETURNS "trigger" AS
$BODY$
BEGIN
IF NEW.id IS NULL THEN
NEW.id:=nextval('SEQ_dso_shipping_unit_prices_ID');--@ignore-semicolon
END IF ;--@ignore-semicolon
Return NEW;--@ignore-semicolon
END;--@ignore-semicolon
$BODY$

LANGUAGE 'plpgsql' VOLATILE;

CREATE TRIGGER TRG_SEQ_dso_shipping_unit_prices
BEFORE INSERT
ON dso_shipping_unit_prices
FOR EACH ROW
EXECUTE PROCEDURE insert_to_dso_shipping_unit_prices();

INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('1', 'KRAJ_EKO_350_A', 'Krajowa ekonomiczna do 350g, gabaryt A', '1', '1', '1', 'false', 1.7500);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('2', 'KRAJ_EKO_350_B', 'Krajowa ekonomiczna do 350g, gabaryt B', '2', '1', '1', 'false', 3.7500);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('3', 'KRAJ_EKO_1000_A', 'Krajowa ekonomiczna do 1000g, gabaryt A', '1', '2', '1', 'false', 3.7000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('4', 'KRAJ_EKO_1000_B', 'Krajowa ekonomiczna do 1000g, gabaryt B', '2', '2', '1', 'false', 4.7500);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('5', 'KRAJ_EKO_2000_A', 'Krajowa ekonomiczna do 2000g, gabaryt A', '1', '3', '1', 'false', 6.3000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('6', 'KRAJ_EKO_2000_A', 'Krajowa ekonomiczna do 2000g, gabaryt B', '2', '3', '1', 'false', 7.3000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('7', 'KRAJ_EKO_POL_350_A', 'Krajowa ekonomiczna polecona do 350g, gabaryt A', '1', '1', '2', 'false', 4.2000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('8', 'KRAJ_EKO_POL_350_B', 'Krajowa ekonomiczna polecona do 350g, gabaryt B', '2', '1', '2', 'false', 7.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('9', 'KRAJ_EKO_POL_1000_A', 'Krajowa ekonomiczna polecona do 1000g, gabaryt A', '1', '2', '2', 'false', 5.9000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('10', 'KRAJ_EKO_POL_1000_B', 'Krajowa ekonomiczna polecona do 1000g, gabaryt B', '2', '2', '2', 'false', 8.3000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('11', 'KRAJ_EKO_POL_2000_A', 'Krajowa ekonomiczna polecona do 2000g, gabaryt A', '1', '3', '2', 'false', 8.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('12', 'KRAJ_EKO_POL_2000_B', 'Krajowa ekonomiczna polecona do 2000g, gabaryt B', '2', '3', '2', 'false', 9.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('13', 'KRAJ_EKO_POL_ZPO_350_A', 'Krajowa ekonomiczna polecona z ZPO do 350g, gabaryt A', '1', '1', '2', 'true', 6.1000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('14', 'KRAJ_EKO_POL_ZPO_350_B', 'Krajowa ekonomiczna polecona z ZPO do 350g, gabaryt B', '2', '1', '2', 'true', 9.4000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('15', 'KRAJ_EKO_POL_ZPO_1000_A', 'Krajowa ekonomiczna polecona z ZPO do 1000g, gabaryt A', '1', '2', '2', 'true', 7.8000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('16', 'KRAJ_EKO_POL_ZPO_1000_B', 'Krajowa ekonomiczna polecona z ZPO do 1000g, gabaryt B', '2', '2', '2', 'true', 10.2000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('17', 'KRAJ_EKO_POL_ZPO_2000_A', 'Krajowa ekonomiczna polecona z ZPO do 2000g, gabaryt A', '1', '3', '2', 'true', 10.4000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('18', 'KRAJ_EKO_POL_ZPO_2000_B', 'Krajowa ekonomiczna polecona z ZPO do 2000g, gabaryt B', '2', '3', '2', 'true', 11.4000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('19', 'KRAJ_PRIO_350_A', 'Krajowa priorytetowa do 350g, gabaryt A', '1', '1', '4', 'false', 2.3500);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('20', 'KRAJ_PRIO_350_B', 'Krajowa priorytetowa do 350g, gabaryt B', '2', '1', '4', 'false', 5.1000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('21', 'KRAJ_PRIO_1000_A', 'Krajowa priorytetowa do 1000g, gabaryt A', '1', '2', '4', 'false', 4.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('22', 'KRAJ_PRIO_1000_B', 'Krajowa priorytetowa do 1000g, gabaryt B', '2', '2', '4', 'false', 7.1000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('23', 'KRAJ_PRIO_2000_A', 'Krajowa priorytetowa do 2000g, gabaryt A', '1', '3', '4', 'false', 8.8000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('24', 'KRAJ_PRIO_2000_A', 'Krajowa priorytetowa do 2000g, gabaryt B', '2', '3', '4', 'false', 10.9000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('25', 'KRAJ_PRIO_POL_350_A', 'Krajowa priorytetowa polecona do 350g, gabaryt A', '1', '1', '5', 'false', 5.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('26', 'KRAJ_PRIO_POL_350_B', 'Krajowa priorytetowa polecona do 350g, gabaryt B', '2', '1', '5', 'false', 8.3000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('27', 'KRAJ_PRIO_POL_1000_A', 'Krajowa priorytetowa polecona do 1000g, gabaryt A', '1', '2', '5', 'false', 7.2000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('28', 'KRAJ_PRIO_POL_1000_B', 'Krajowa priorytetowa polecona do 1000g, gabaryt B', '2', '2', '5', 'false', 11.0000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('29', 'KRAJ_PRIO_POL_2000_A', 'Krajowa priorytetowa polecona do 2000g, gabaryt A', '1', '3', '5', 'false', 11.0000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('30', 'KRAJ_PRIO_POL_2000_B', 'Krajowa priorytetowa polecona do 2000g, gabaryt B', '2', '3', '5', 'false', 14.5000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('31', 'KRAJ_PRIO_POL_ZPO_350_A', 'Krajowa priorytetowa polecona z ZPO do 350g, gabaryt A', '1', '1', '5', 'true', 7.4000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('32', 'KRAJ_PRIO_POL_ZPO_350_B', 'Krajowa priorytetowa polecona z ZPO do 350g, gabaryt B', '2', '1', '5', 'true', 10.2000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('33', 'KRAJ_PRIO_POL_ZPO_1000_A', 'Krajowa priorytetowa polecona z ZPO do 1000g, gabaryt A', '1', '2', '5', 'true', 9.1000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('34', 'KRAJ_PRIO_POL_ZPO_1000_B', 'Krajowa priorytetowa polecona z ZPO do 1000g, gabaryt B', '2', '2', '5', 'true', 12.9000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('35', 'KRAJ_PRIO_POL_ZPO_2000_A', 'Krajowa priorytetowa polecona z ZPO do 2000g, gabaryt A', '1', '3', '5', 'true', 12.9000);
INSERT INTO dso_shipping_unit_prices (ID, cn, title, gauge, weight, documentDelivery, zpo, price) VALUES ('36', 'KRAJ_PRIO_POL_ZPO_2000_B', 'Krajowa priorytetowa polecona z ZPO do 2000g, gabaryt B', '2', '3', '5', 'true', 16.4000);

alter table dso_out_document_delivery add column foreignDelivery boolean;
alter table dso_out_document_delivery add column priorityKind integer;
alter table dso_out_document add column additionalZpo boolean;