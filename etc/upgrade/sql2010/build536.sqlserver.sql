alter table ds_employee_card drop constraint FK_DS_EMPLOYEE_CARD_USER_ID;
alter table ds_employee_card alter column USER_ID varchar(62);

CREATE TABLE [DS_CALENDAR_FREE_DAY]
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DAY_DATE] [datetime] NOT NULL,
	[INFO] [text] NULL
);

ALTER TABLE DS_EMPLOYEE_CARD ADD DIVISION varchar(100);
ALTER TABLE DS_EMPLOYEE_CARD ADD IS_T188KP tinyint;
ALTER TABLE DS_EMPLOYEE_CARD ADD IS_TUJ tinyint;