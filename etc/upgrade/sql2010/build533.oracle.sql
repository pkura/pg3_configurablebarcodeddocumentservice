create table  ds_Rejection_Reason
(
	id numeric(18,0) primary key,
	timeId numeric(18,0),
	username varchar2(50 char),
	reason varchar2(300 char)
);