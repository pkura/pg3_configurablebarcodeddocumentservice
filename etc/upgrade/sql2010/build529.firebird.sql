CREATE TABLE ds_acceptance_division
(
	ID numeric(18, 0) NOT NULL primary key,
	GUID varchar(62),
	NAME varchar(62),
	CODE varchar(20),
	DIVISIONTYPE varchar(20),
	PARENT_ID numeric(18, 0),
	ogolna smallint
);

create generator ds_acceptance_division_id;