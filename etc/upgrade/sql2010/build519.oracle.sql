CREATE TABLE "DS_EMPLOYEE_CARD"
(
	"ID" number(18, 0) PRIMARY KEY NOT NULL,
	"KPX" varchar2(50 char) NOT NULL,
	"COMPANY" varchar2(100 char) NOT NULL,
	"EMPLOYMENT_START_DATE" timestamp NOT NULL,
	"EMPLOYMENT_END_DATE" timestamp,
	"POSITION" varchar2(255 char) NOT NULL,
	"DEPARTMENT" varchar2(100 char) NOT NULL,
	"INFO" clob,
	"USER_ID" number(19, 0)
 );
 CREATE SEQUENCE SEQ_DS_EMPLOYEE_CARD_ID;
 ALTER TABLE "DS_EMPLOYEE_CARD" ADD CONSTRAINT FK_DS_EMPLOYEE_CARD_USER_ID FOREIGN KEY ("USER_ID")
REFERENCES "DS_USER" ("ID");