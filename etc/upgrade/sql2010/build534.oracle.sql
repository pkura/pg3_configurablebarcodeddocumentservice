create table ds_epuap_export_document
(
	id numeric(18,0),
	DOCUMENTID numeric(18,0),
	odpowiedz	varchar2(200 char),
	dataNadania timestamp,
	status integer
);
create sequence ds_epuap_export_document_id;

alter table dso_person add identifikatorEpuap varchar(50 char);
alter table dso_person add adresSkrytkiEpuap varchar(50 char);
alter table dso_person add zgodaNaWysylke smallint;
alter table ds_epuap_export_document add person numeric(18,0);
alter table DS_EPUAP_EXPORT_DOCUMENT add constraint DS_EPUAP_EXPORT_DOCUMENT_CON unique (DOCUMENTID,PERSON);