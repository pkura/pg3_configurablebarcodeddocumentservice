

ALTER TABLE DS_BILING ADD bilingNumber varchar(60);

create table ds_calendar_event 
(
id numeric(18, 0) identity(1,1) not null primary key,
timeId numeric(18,0),
calendarId numeric(18,0),
confirm smallint,
allDay smallint,
starttime datetime not null,
endtime datetime not null,
place varchar(64),
description varchar(256)
);
drop index i_ds_time_1 on ds_time;
drop index i_ds_time_2 on ds_time;