
delete from DS_MESSAGE_ATTACHMENT;
delete from DS_MESSAGE;
delete from dso_email_channel;

alter table DS_MESSAGE_ATTACHMENT alter column contentType varchar(250) not null;

alter table DS_MESSAGE add email_channel_id numeric(19,0) not null;
alter table DS_MESSAGE add constraint ds_message_email_channel_fk 
	foreign key (email_channel_id) references dso_email_channel (id);
	
alter table DS_MESSAGE alter column MESSAGEID varchar(100) not null;

alter table dso_email_channel add protocol varchar(50) not null;	
alter table dso_email_channel add hostname varchar(100) not null;
alter table dso_email_channel add username varchar(50) not null;
alter table dso_email_channel add password varchar(100) not null;
alter table dso_email_channel add port integer not null default -1;
alter table dso_email_channel add mbox varchar(50) not null default 'INBOX';
alter table dso_email_channel add is_ssl smallint not null default 0;
alter table dso_email_channel add is_enabled smallint not null default 1;

alter table dso_email_channel drop column pop3_host;
alter table dso_email_channel drop column pop3_username;
alter table dso_email_channel drop column pop3_password;