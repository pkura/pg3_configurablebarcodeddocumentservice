create table ds_resources_to_time
(
	id numeric(18,0) primary key,
	time_id numeric(18,0),
	object_id numeric(18,0),
	type varchar(20),
	confirm smallint
);

create generator ds_resources_to_time_id;
create index i_ds_res_to_time_tid on ds_resources_to_time(time_id);

create table ds_any_resource
(
	id numeric(18,0) primary key,
	cn varchar(30),
	name varchar(100),
	calendarId numeric(18,0)
);

create generator ds_any_resource_id;
create index i_ds_any_resource_cn on ds_any_resource(cn);

create table ds_user_calendar
(
	resourceId numeric(18,0),
	username varchar(30),
	permissionType integer, 
	resourceType varchar(30),
	show smallint,
	PRIMARY KEY (resourceId, resourceType,username)
);

create table DS_calendar
(
	id numeric(18,0) primary key,
	name varchar(100),
	type  varchar(30),
	owner  varchar(50)
);
create generator DS_calendar_id;

ALTER TABLE ds_any_resource ADD CONSTRAINT ds_any_resource_CAL FOREIGN KEY (calendarId) REFERENCES DS_calendar (ID);