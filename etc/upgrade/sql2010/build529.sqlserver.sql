CREATE TABLE ds_acceptance_division
(
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[GUID] [varchar](62) NOT NULL,
	[NAME] [varchar](62) NOT NULL,
	[CODE] [varchar](20) NULL,
	[DIVISIONTYPE] [varchar](20) NOT NULL,
	[PARENT_ID] [numeric](19, 0) NULL,
	[ogolna] [int] NULL
);