create table ds_calendar_to_time
(
	time_id numeric(18,0),
	calendar_id numeric(18,0),
	confirm smallint,
	primary key(time_id,calendar_id)
);

create index i_ds_cal_to_time_tid on ds_calendar_to_time(time_id);
 
 
create table DS_calendar
(
	id numeric(18,0) primary key,
	name varchar2(100 char),
	type  varchar2(30 char),
	owner  varchar2(50 char)
);



create sequence DS_calendar_id;

create table  ds_any_resource
(
	id numeric(18,0) primary key,
	cn varchar2(30 char),
	name varchar2(100 char),
	calendarId numeric(18,0)
);

create sequence ds_any_resource_id;

create index i_ds_any_resource_cn on ds_any_resource(cn); 


create table ds_user_calendar
(
	calendarId numeric(18,0) ,
	username varchar2(30 char),
	permissionType integer,
	show smallint,
	PRIMARY KEY (calendarId,username)
);

ALTER TABLE ds_any_resource ADD CONSTRAINT ds_any_resource_CAL FOREIGN KEY (calendarId) REFERENCES DS_calendar (ID);