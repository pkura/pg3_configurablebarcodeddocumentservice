
ALTER TABLE DS_BILING ADD bilingNumber varchar(60);

create table ds_calendar_event 
(
id numeric(18, 0) not null primary key,
timeId numeric(18,0),
calendarId numeric(18,0),
confirm smallint,
allDay smallint,
starttime timestamp not null,
endtime timestamp not null,
place varchar(64),
description varchar(256)
);

create generator ds_calendar_event_id;