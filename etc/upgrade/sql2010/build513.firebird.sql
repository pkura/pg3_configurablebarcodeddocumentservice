
delete from DS_MESSAGE_ATTACHMENT;
delete from DS_MESSAGE;
delete from dso_email_channel;


alter table DS_MESSAGE_ATTACHMENT alter column contentType type varchar(250);


alter table DS_MESSAGE add email_channel_id integer not null;
alter table DS_MESSAGE add constraint ds_message_email_channel_fk 
	foreign key (email_channel_id) references dso_email_channel (id);


alter table dso_email_channel add protocol varchar(50) not null;	
alter table dso_email_channel add hostname varchar(100) not null;
alter table dso_email_channel add username varchar(50) not null;
alter table dso_email_channel add pass varchar(100) not null;
alter table dso_email_channel add port integer default -1 not null;
alter table dso_email_channel add mbox varchar(50) default 'INBOX'  not null ;
alter table dso_email_channel add is_ssl smallint default 0  not null;
alter table dso_email_channel add is_enabled smallint default 1 not null ;


alter table dso_email_channel drop  pop3_host;
alter table dso_email_channel drop  pop3_username;
alter table dso_email_channel drop  pop3_password;