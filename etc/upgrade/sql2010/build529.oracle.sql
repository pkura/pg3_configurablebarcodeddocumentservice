CREATE TABLE ds_acceptance_division
(
	ID numeric(18, 0) NOT NULL primary key,
	GUID varchar2(62 char),
	NAME varchar2(62 char),
	CODE varchar2(20 char),
	DIVISIONTYPE varchar2(20 char),
	PARENT_ID numeric(18, 0),
	ogolna smallint
);

create sequence ds_acceptance_division_id;