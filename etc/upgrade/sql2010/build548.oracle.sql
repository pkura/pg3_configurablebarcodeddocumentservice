alter table ds_bookmark add ascending_ SMALLINT;
alter table ds_bookmark add sort_field varchar2(64);
ALTER TABLE ds_bookmark ADD task_count INTEGER;
UPDATE ds_bookmark SET ascending_=1;
UPDATE ds_bookmark SET sort_field='receiveDate';
UPDATE ds_bookmark SET task_count=20;