

ALTER TABLE DS_BILING ADD bilingNumber varchar2(60 char);

create table ds_calendar_event 
(
id numeric(18, 0) not null primary key,
timeId numeric(18,0),
calendarId numeric(18,0),
confirm smallint,
allDay smallint,
starttime timestamp not null,
endtime timestamp not null,
place varchar2(64 char),
description varchar2(256 char)
);

create sequence ds_calendar_event_id;