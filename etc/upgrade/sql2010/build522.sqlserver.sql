CREATE TABLE [DS_ABSENCE_TYPE]
(
	[CODE] [varchar](20) NOT NULL,
	[NAME] [varchar](150) NOT NULL,
	[INFO] [text],
	[FLAG] [smallint] NOT NULL DEFAULT 0,
 CONSTRAINT [PK_DS_ABSENCE_TYPE] PRIMARY KEY ([CODE])
 );
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES ('188', 'Opieka nad dzieckiem', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('INW', 'Urlop inwalidzki', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('NN', 'Nieobecno�� nieusprawiedliwiona', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('NUN', 'Nieobecno�� usprawiedliwiona niep�atna', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('NUP', 'Nieobecno�� usprawiedliwiona p�atna', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('OG', 'Odbi�r godzin', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UB', 'Urlop bezp�atny', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UJ', 'Urlop ojcowski', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UM', 'Urlop macierzy�ski', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UMD', 'Urlop macierzy�ski dodatkowy', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UOK', 'Urlop okoliczno�ciowy', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('UW', 'Urlop wypoczynkowy', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('U�', 'Urlop na ��danie', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('WUN', 'Wymiar urlopu nale�ny', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE, NAME, INFO, FLAG)
     VALUES		('WUZ', 'Wymiar urlopu zaleg�y', NULL, 1);
 
 CREATE TABLE [DS_ABSENCE]
 (
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[YEAR] [smallint] NOT NULL,
	[START_DATE] [datetime] NOT NULL,
	[END_DATE] [datetime] NOT NULL,
	[DAYS_NUM] [smallint] NOT NULL,
	[INFO] [text],
	[CTIME] [datetime] NOT NULL,
	[EMPLOYEE_ID] [numeric](18, 0) NOT NULL,
	[USER_ID] [numeric](19, 0) NULL,
	[ABSENCE_TYPE] [varchar](20) NOT NULL,
	[MTIME] [datetime],
CONSTRAINT [PK_DS_ABSENCE_ID] PRIMARY KEY ([ID])
);

ALTER TABLE [DS_ABSENCE] ADD  CONSTRAINT [FK_DS_ABSENCE_ABSENCE_TYPE] FOREIGN KEY ([ABSENCE_TYPE])
REFERENCES [DS_ABSENCE_TYPE] ([CODE]);

ALTER TABLE [DS_ABSENCE]  ADD  CONSTRAINT [FK_DS_ABSENCE_EMPLOYEE_ID] FOREIGN KEY ([EMPLOYEE_ID])
REFERENCES [DS_EMPLOYEE_CARD] ([ID]);

ALTER TABLE [DS_ABSENCE]  ADD  CONSTRAINT [FK_DS_ABSENCE_USER_ID] FOREIGN KEY ([USER_ID])
REFERENCES [DS_USER] ([ID]);
