alter table ds_employee_card drop constraint FK_DS_EMPLOYEE_CARD_USER_ID;
--alter table ds_employee_card modify USER_ID varchar2(62);
alter table ds_employee_card add USER_ID_tmp varchar2(62 char);
update ds_employee_card set USER_ID_tmp = USER_ID;
alter table ds_employee_card drop column USER_ID;
alter table ds_employee_card rename column USER_ID_tmp  to USER_ID; 

CREATE TABLE DS_CALENDAR_FREE_DAY
(
	ID numeric(18, 0) PRIMARY KEY NOT NULL,
	DAY_DATE timestamp NOT NULL,
	INFO clob NULL
);

CREATE SEQUENCE SEQ_DS_FREE_DAY_ID;

ALTER TABLE DS_EMPLOYEE_CARD ADD DIVISION varchar2(100 char);
ALTER TABLE DS_EMPLOYEE_CARD ADD IS_T188KP smallint;
ALTER TABLE DS_EMPLOYEE_CARD ADD IS_TUJ smallint;