create table  ds_Rejection_Reason
(
	id numeric(18,0) identity(1,1)  primary key,
	timeId numeric(18,0),
	username varchar(50),
	reason varchar(300)
);