create table ds_epuap_export_document
(
	id numeric(18,0) identity(1,1),
	DOCUMENTID numeric(18,0),
	odpowiedz	varchar(200),
	dataNadania timestamp,
	status integer
);

alter table dso_person add identifikatorEpuap varchar(50);
alter table dso_person add adresSkrytkiEpuap varchar(50);
alter table dso_person add zgodaNaWysylke smallint;
alter table ds_epuap_export_document add person numeric(18,0);
alter table DS_EPUAP_EXPORT_DOCUMENT add constraint DS_EPUAP_EXPORT_DOCUMENT_CON unique (DOCUMENTID,PERSON);