
delete from DS_MESSAGE_ATTACHMENT;
delete from DS_MESSAGE;
delete from dso_email_channel;


alter table DS_MESSAGE_ATTACHMENT modify contentType varchar2(250 char) not null;


alter table DS_MESSAGE add email_channel_id numeric(19,0) not null;
alter table DS_MESSAGE add constraint ds_message_email_channel_fk 
	foreign key (email_channel_id) references dso_email_channel (id);
	
alter table DS_MESSAGE modify MESSAGEID varchar2(100 char) not null;


alter table dso_email_channel add protocol varchar2(50 char) not null;	
alter table dso_email_channel add hostname varchar2(100 char) not null;
alter table dso_email_channel add username varchar2(50 char) not null;
alter table dso_email_channel add password varchar2(100 char) not null;
alter table dso_email_channel add port integer default -1 not null;
alter table dso_email_channel add mbox varchar2(50 char) default 'INBOX' not null;
alter table dso_email_channel add is_ssl smallint default 0 not null;
alter table dso_email_channel add is_enabled smallint default 1 not null;


alter table dso_email_channel drop column pop3_host;
alter table dso_email_channel drop column pop3_username;
alter table dso_email_channel drop column pop3_password; 