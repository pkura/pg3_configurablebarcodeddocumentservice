create table ds_Rejection_Reason
(
	id numeric(18,0) primary key,
	timeId numeric(18,0),
	username varchar(50),
	reason varchar(300)
);