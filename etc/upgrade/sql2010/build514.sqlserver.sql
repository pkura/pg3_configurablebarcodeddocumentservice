CREATE TABLE DS_ADDRESS
(
	NAME varchar(50) NOT NULL,
	ORGANIZATION varchar(100) NOT NULL,
	STREET varchar(100) NOT NULL,
	ZIP_CODE varchar(10) NOT NULL,
	CITY varchar(50) NOT NULL,
	REGION varchar(50) NOT NULL,
	IMAGE_MAP image NULL,
	ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
	IMG_CONTENT_TYPE varchar(255) NULL,
	IMG_FILENAME varchar(255) NULL,
	
	CONSTRAINT PK_DS_ADDRESS_ID PRIMARY KEY (ID),
	CONSTRAINT UQ_DS_ADDRESS_ID UNIQUE (ID)
);

CREATE TABLE DS_FLOOR
(
	NAME varchar(50) NOT NULL,
	IMAGE_MAP image NULL,
	ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
	ADDRESS_ID numeric(18, 0) NOT NULL,
	DESCRIPTION varchar(255) NOT NULL,
	POSN int NULL,
	IMG_CONTENT_TYPE varchar(255) NULL,
	IMG_FILENAME varchar(255) NULL,
	
	CONSTRAINT PK_DS_FLOOR_ID PRIMARY KEY (ID),
	CONSTRAINT UQ_DS_FLOOR_ID UNIQUE (ID)
);
ALTER TABLE DS_FLOOR ADD CONSTRAINT FK_DS_FLOOR_ADDRESS_ID FOREIGN KEY (ADDRESS_ID)
	REFERENCES DS_ADDRESS (ID);
	
CREATE TABLE DS_PHONE
(
	NAME varchar(100) NOT NULL,
	TYPE varchar(25) NOT NULL,
	NUMBER varchar(25) NOT NULL,
	ADDRESS_ID numeric(18, 0) NOT NULL,
	ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
	POSN int NULL,
	
	CONSTRAINT PK_DS_PHONE_ID PRIMARY KEY (ID),
	CONSTRAINT UQ_DS_PHONE_ID UNIQUE (ID)
 );
 ALTER TABLE DS_PHONE ADD CONSTRAINT FK_DS_PHONE_ADDRESS_ID FOREIGN KEY (ADDRESS_ID)
	REFERENCES DS_ADDRESS (ID);