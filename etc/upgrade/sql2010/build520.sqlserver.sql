create table ds_calendar_to_time
(
	time_id numeric(18,0),
	calendar_id numeric(18,0),
	confirm smallint,
	primary key(time_id,calendar_id)
);

create index i_ds_cal_to_time_tid on ds_calendar_to_time(time_id);
 
create table  ds_any_resource
(
	id numeric(18,0) identity(1,1)  primary key,
	cn varchar(30),
	name varchar(100),
	calendarId numeric(18,0)
);
create index i_ds_any_resource_cn on ds_any_resource(cn); 

create table ds_user_calendar
(
	calendarId numeric(18,0) ,
	username varchar(30),
	permissionType integer,
	show smallint,
	PRIMARY KEY (calendarId,username)
);


create table DS_calendar
(
	id numeric(18,0) identity(1,1) primary key,
	name varchar(100),
	type  varchar(30),
	owner  varchar(50)
	
)
;
ALTER TABLE ds_any_resource ADD CONSTRAINT ds_any_resource_CAL FOREIGN KEY (calendarId) REFERENCES DS_calendar (ID);