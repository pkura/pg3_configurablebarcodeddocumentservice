--OLD:
--ALTER TABLE DS_ATTACHMENT ADD field_cn varchar(200 char);
--NEW:
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='DS_ATTACHMENT' and COLUMN_NAME='field_cn')
		begin
			alter table DS_ATTACHMENT alter column field_cn varchar(200 char)
		end
	ELSE
		begin
			alter table DS_ATTACHMENT add field_cn varchar(200 char)
		end
		