CREATE TABLE sso (
    idsso  INTEGER NOT NULL,
    login_ad  VARCHAR(50) NOT NULL,
guid_source VARCHAR(50),
guid_sso VARCHAR(50),
is_active CHAR(1),
login_date TIMESTAMP,
redirect_url VARCHAR(1000),
constraint PK_sso primary key (idsso)
);

CREATE GENERATOR SSO_IDSSO;
SET GENERATOR SSO_IDSSO TO 0;
