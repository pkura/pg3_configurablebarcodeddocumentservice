--OLD:
--alter table DSW_JBPM_TASKLIST add resource_count INTEGER  default 1 not null     ;
--ALTER TABLE DSW_JBPM_TASKLIST ADD markerClass varchar(30 char);
--NEW:
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='DSW_JBPM_TASKLIST' and COLUMN_NAME='resource_count')
		begin
			alter table DSW_JBPM_TASKLIST alter column resource_count INTEGER  default 1 not null
		end
	ELSE
		begin
			alter table DSW_JBPM_TASKLIST add resource_count INTEGER  default 1 not null
		end
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='DSW_JBPM_TASKLIST' and COLUMN_NAME='markerClass')
		begin
			ALTER TABLE DSW_JBPM_TASKLIST alter column markerClass varchar(30 char)
		end
	ELSE
		begin
			ALTER TABLE DSW_JBPM_TASKLIST ADD markerClass varchar(30 char)
		end
		