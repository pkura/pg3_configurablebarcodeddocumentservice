alter table dso_document_asgn_history add id BIGINT;
create sequence dso_document_asgn_history_ID;

update dso_document_asgn_history set id = nextval('dso_document_asgn_history_ID')
ALTER TABLE dso_document_asgn_history ALTER COLUMN id TYPE bigint;
ALTER TABLE dso_document_asgn_history  ALTER COLUMN id SET NOT NULL;
ALTER TABLE dso_document_asgn_history ADD PRIMARY KEY (ID);
alter table dso_document_asgn_history drop column posn;
create table dso_doc_asgn_history_targets
(
	type BIGINT,
	value character varying(62),
	aheId BIGINT,
	CONSTRAINT dso_document_asgn_history_fk2 FOREIGN KEY (aheId) REFERENCES dso_document_asgn_history  (ID)
);