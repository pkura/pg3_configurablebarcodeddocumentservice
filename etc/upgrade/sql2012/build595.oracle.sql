alter table dso_document_asgn_history add id numeric(18,0);
create sequence dso_document_asgn_history_ID;
update dso_document_asgn_history set id = dso_document_asgn_history_ID.nextval;
alter table dso_document_asgn_history modify id numeric(18,0) not null PRIMARY KEY ;
alter table dso_document_asgn_history drop column posn;
create table dso_doc_asgn_history_targets
(
	type integer,
	value VARCHAR2(62),
	aheId NUMBER(18,0),
	CONSTRAINT dso_document_asgn_history_fk2 FOREIGN KEY (aheId) REFERENCES dso_document_asgn_history  (ID)
);