alter table dso_document_asgn_history add id numeric(18,0) identity(1,1) PRIMARY KEY;
alter table dso_document_asgn_history drop column posn;
create table dso_document_asgn_history_targets
(
	type int,
	value varchar(62),
	aheId numeric(18,0) FOREIGN KEY REFERENCES dso_document_asgn_history(ID)
);