CREATE TABLE [sso](
	[idsso] [int] IDENTITY(1,1) NOT NULL,
	[login_ad] [nvarchar](50) NOT NULL,
	[guid_source] [nvarchar](50) NULL,
	[guid_sso] [nvarchar](50) NULL,
	[is_active] [bit] NULL,
	[login_date] [datetime] NULL,
	[redirect_url] [nvarchar](1000) NULL,
 CONSTRAINT [PK_sso] PRIMARY KEY CLUSTERED 
(
	[idsso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

