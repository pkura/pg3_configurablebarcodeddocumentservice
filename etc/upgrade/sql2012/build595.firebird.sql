CREATE GENERATOR dso_document_asgn_history_ID;
alter table dso_document_asgn_history add id numeric(18,0) not null;
update dso_document_asgn_history set id = gen_id(dso_document_asgn_history_ID,1);
alter table DSO_DOCUMENT_ASGN_HISTORY
add constraint DSO_DOCUMENT_ASGN_HISTORY_PK
primary key (ID);
alter table dso_document_asgn_history drop posn;

CREATE TABLE dso_doc_asgn_history_targets
(
    xTYPE integer,
    xVALUE VARCHAR(62),
    aheId NUMERIC(18)
    );
ALTER TABLE DSO_DOC_ASGN_HISTORY_TARGETS ALTER XTYPE TO "TYPE";
ALTER TABLE DSO_DOC_ASGN_HISTORY_TARGETS ALTER XVALUE TO "VALUE";
alter table DSO_DOC_ASGN_HISTORY_TARGETS
add constraint FK_DSO_DOC_ASGN_HISTORY_TARGETS
foreign key (AHEID) 
references DSO_DOCUMENT_ASGN_HISTORY (ID);