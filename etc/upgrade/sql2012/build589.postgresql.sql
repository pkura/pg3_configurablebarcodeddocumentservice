
CREATE TABLE sso (
idsso bigint NOT NULL,
login_ad character varying(50) NOT NULL,
guid_source character varying(50) ,
guid_sso character varying(50) ,
is_active boolean ,
login_date DATE ,
redirect_url character varying(1000) ,
constraint PK_sso primary key (idsso)
);

CREATE SEQUENCE SEQ_SSO_IDSSO
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1


CREATE OR REPLACE FUNCTION insert_to_SSO()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.idsso IS NULL THEN
   New.idsso:=nextval('SEQ_SSO_IDSSO');
   Return NEW;
 END IF ;
 END;
 $BODY$
 
 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_SSO_INSERT
 BEFORE INSERT ON SSO
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_SSO();
