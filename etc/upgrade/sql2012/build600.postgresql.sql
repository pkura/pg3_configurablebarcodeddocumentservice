CREATE TABLE dsg_external_dictionaries 
(
	id BIGINT,
	nazwa_polaczenia character varying(64),
	nazwa_slownika character varying(64),
	nazwa_kolumny_oryginalna character varying(64),
	nazwa_kolumny_uzytkownika character varying(64),
	typ_kolumny character varying(32),
	precyzja_kolumny character varying(16)
);

create sequence dsg_external_dictionaries_id;