CREATE TABLE dsg_external_dictionaries 
(
	id NUMERIC(18,0) IDENTITY,
	nazwa_polaczenia VARCHAR(64),
	nazwa_slownika VARCHAR(64),
	nazwa_kolumny_oryginalna VARCHAR(64),
	nazwa_kolumny_uzytkownika VARCHAR(64),
	typ_kolumny VARCHAR(32),
	precyzja_kolumny VARCHAR(16),
)