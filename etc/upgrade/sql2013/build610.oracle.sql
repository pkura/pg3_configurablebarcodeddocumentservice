alter table dsg_centrum_kosztow_faktury add orderId NUMBER(9,0) NULL;
alter table dsg_centrum_kosztow_faktury add groupId NUMBER(9,0) NULL;
alter table dsg_centrum_kosztow_faktury add productId NUMBER(9,0) NULL;



CREATE TABLE DSO_USER_TO_BRIEFCASE(
	ID int NOT NULL,
	containerId number (18, 0) NOT NULL,
	containerOfficeId varchar2(84 char) NOT NULL,
	userId number(18, 0) NOT NULL,
	userName varchar2(62 char) NULL,
	userFirstname varchar2(62 char) NULL,
	userLastname varchar2(62 char) NULL
);


alter table DSO_USER_TO_BRIEFCASE add canView smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canCreate smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canModify smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canPrint smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canViewCases smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canAddCasesToBriefcase smallint not null;

create sequence DSO_USER_TO_BRIEFCASE_ID start with 1 nocache;