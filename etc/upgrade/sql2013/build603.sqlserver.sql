CREATE TABLE ds_epuap_skrytka
(
	id NUMERIC(18,0) IDENTITY,
	adres_skrytki VARCHAR(300),
	nazwa_skrytki VARCHAR(300),
	identyfikator_podmiotu VARCHAR(300),
	available tinyint
)