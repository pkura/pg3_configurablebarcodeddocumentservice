alter table ds_electronic_signature add verifyResult CHAR(1);
alter table ds_electronic_signature add verifyDate TIMESTAMP;
alter table ds_electronic_signature add baseSignatureId NUMERIC(18,0);