create table dsg_currency(
	id integer primary key not null,
	cn varchar(50) null,
	title varchar(255) null,
	centrum int null,
	refValue varchar(20) null,
	available int null
);

insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'EUR', N'EUR - EU euro', NULL, NULL, 1, 1);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'USD', N'USD - Dolar ameryka�ski', NULL, NULL, 1, 2);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'GBP', N'GBP - Funt szterling', NULL, NULL, 1, 3);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'JPY', N'JPY - Japonia jen', NULL, NULL, 1, 4);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'CZK', N'CZK - Korona czeska', NULL, NULL, 1, 5);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'PLN', N'PLN - Z�oty polski', NULL, NULL, 1, 6);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'SKK', N'SKK - Korona s�owacka', NULL, NULL, 1, 7);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'UAH', N'UAH - Hrywna Ukraina', NULL, NULL, 1, 8);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'LTL', N'LTL - Lit Litwa', NULL, NULL, 1, 9);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'CNY', N'CNY - Juan Chiny', NULL, NULL, 1, 10);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'NOK', N'NOK - norweska korona', NULL, NULL, 1, 11);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'CHF', N'CHF - frank szwajcarski', NULL, NULL, 1, 12);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'KUNA', N'KUNA - Kuna chorwacka', NULL, NULL, 1, 13);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'BYR', N'BYR - Rubel Bia�oru�', NULL, NULL, 1, 14);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'BAM', N'BAM - Marka transferowa Bo�nia i Hercegowina', NULL, NULL, 1, 15);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'BGN', N'BGN - Lew Bu�garia', NULL, NULL, 1, 16);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'DKK', N'DKK - Korona du�ska', NULL, NULL, 1, 17);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'ISK', N'ISK - Korona islandzka', NULL, NULL, 1, 18);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'MKD', N'MKD - Denar macedo�ski', NULL, NULL, 1, 19);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'MDL', N'MDL - Lej mo�dawski', NULL, NULL, 1, 20);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'RUB', N'RUB - Rubel Rosja', NULL, NULL, 1, 21);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'RON', N'RON - Lej Rumunia', NULL, NULL, 1, 22);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'CSD', N'CSD - Dinar serbski', NULL, NULL, 1, 23);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'SEK', N'SEK - Korona szwedzka', NULL, NULL, 1, 24);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'HUF', N'HUF - Forint W�gry', NULL, NULL, 1, 25);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'LVL', N'LVL - �at �otwa', NULL, NULL, 1, 26);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'CAD', N'CAD- Dolar kanadyjski', 0, NULL, 1, 28);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'ND', N'-', NULL, NULL, 1, 29);
insert into dsg_currency (cn, title, centrum, refValue, available, id) VALUES (N'AUD', N'AUD - Dolar australijski', 0, NULL, 1, 30);