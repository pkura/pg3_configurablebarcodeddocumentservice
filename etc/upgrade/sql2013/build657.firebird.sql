ALTER TABLE dso_out_document ADD mailWeight NUMERIC(18, 0);

DROP TABLE dso_mail_weight;

CREATE GENERATOR dso_mail_weight_id;
CREATE TABLE dso_mail_weight (
  id       numeric(19),
  cn       varchar(30),
  name     varchar(30),
  weightG  int,
  weightKg int
  );


INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(gen_id(dso_mail_weight_id, 1), 'weight_50', 'Do 50g', 50),
(gen_id(dso_mail_weight_id, 1), 'weight_50-100', 'Powy�ej 50g do 100g', 100),
(gen_id(dso_mail_weight_id, 1), 'weight_100-350', 'Powy�ej 100g do 350g', 350),
(gen_id(dso_mail_weight_id, 1), 'weight_350-500', 'Powy�ej 350g do 500g', 500);

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(gen_id(dso_mail_weight_id, 1), 'weight_500-1000', 'Powy�ej 500g do 1000g', 1000, 1),
(gen_id(dso_mail_weight_id, 1), 'weight_1000-2000', 'Powy�ej 1000g do 2000g', 2000, 2);

