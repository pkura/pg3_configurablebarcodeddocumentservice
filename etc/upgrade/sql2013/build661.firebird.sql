CREATE TABLE ds_document_snapshot (
	id NUMERIC(19,0) NOT NULL,
	user_id NUMERIC(19,0) NOT NULL,
	document_id NUMERIC(19,0) NOT NULL,
	ctime TIMESTAMP,
	mtime TIMESTAMP,
	content BLOB
	primary key (id)
)