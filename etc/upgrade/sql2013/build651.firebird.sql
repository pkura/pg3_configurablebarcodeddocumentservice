DROP TABLE ds_jackrabbit_server;

CREATE TABLE ds_jcr_repository (
  id SMALLINT NOT NULL PRIMARY KEY,
  status VARCHAR(255) NOT NULL,
  internal_url VARCHAR(255) NOT NULL,
  user_url VARCHAR(255) NOT NULL,
  login VARCHAR(255) NULL,
  password VARCHAR(255) NULL,
  ignore_ssl_errors BIT NOT NULL DEFAULT(0)
)