CREATE TABLE ds_process_for_document(
  engine_id TINYINT NOT NULL,
  process_id VARCHAR(255) NOT NULL,
  document_id NUMERIC(18, 0) NOT NULL,
  definition_id VARCHAR(255) NOT NULL,

  PRIMARY KEY (process_id, engine_id)
);

CREATE INDEX idx_process_for_document_document_definition ON ds_process_for_document (document_id, definition_id);