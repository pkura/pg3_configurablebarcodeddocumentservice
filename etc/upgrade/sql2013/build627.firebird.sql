CREATE TABLE DS_USER_ADDITIONAL_DATA(	
	ID NUMERIC(18,0), 
	USER_ID NUMERIC(18,0),
	MAIL_FOOTER VARCHAR(8000)
);
alter table DS_USER_ADDITIONAL_DATA
add constraint FK_DS_USER_ADDITIONAL_DATA
foreign key (USER_ID) 
references DS_USER (ID);