alter table dso_out_document drop column if exists mailWeight;

ALTER TABLE dso_out_document ADD mailWeight NUMERIC(18,0);



drop table if exists dso_mail_weight;

CREATE TABLE dso_mail_weight (
  id       numeric(18,0) NOT NULL,
  cn       character varying(30) NOT NULL,
  name       character varying(50) NOT NULL,
  weightG  int ,
  weightKg int 
  );
 
drop SEQUENCE if exists dso_mail_weight_id;
CREATE SEQUENCE dso_mail_weight_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;
	
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_50', 'Do 50g', 50);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_50-100', 'Powy�ej 50g do 100g', 100);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_100-350', 'Powy�ej 100g do 350g', 350);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_350-500', 'Powy�ej 350g do 500g', 500);

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(nextval('dso_mail_weight_id'), 'weight_500-1000', 'Powy�ej 500g do 1000g', 1000, 1);
INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(nextval('dso_mail_weight_id'), 'weight_1000-2000', 'Powy�ej 1000g do 2000g', 2000, 2);



