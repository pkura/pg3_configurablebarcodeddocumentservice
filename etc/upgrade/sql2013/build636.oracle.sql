alter table ds_electronic_signature add verifyResult number(1);
alter table ds_electronic_signature add verifyDate date;
alter table ds_electronic_signature add baseSignatureId number(19,0);