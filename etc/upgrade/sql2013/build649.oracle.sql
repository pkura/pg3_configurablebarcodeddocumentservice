DROP VIEW PROCESS_REPORT_VIEW;

CREATE VIEW PROCESS_REPORT_VIEW (DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

--akutalne procesy
select distinct
	tVar.LONG_VALUE_									,-- 'document id',
	SUBSTR(tExe.ID_, 0, instr(tExe.ID_,'.')-1)	        ,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  -- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_ AS VARCHAR2(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

--oraz
UNION

--hist. procesy
select distinct
	CAST(tHistVar.VALUE_ AS NUMBER(19,0))				,-- 'document id',
	SUBSTR(tHisActInst.EXECUTION_, 0, instr(tHisActInst.EXECUTION_,'.')-1)	        ,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 -- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(CONCAT('%.',CAST(tHistVar.HPROCI_ AS VARCHAR2(255))), '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';