ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;

CREATE TABLE ds_jackrabbit_server (
  id SMALLINT NOT NULL PRIMARY KEY,
  status character varying(255) NOT NULL,
  internal_url character varying(255) NOT NULL,
  user_url character varying(255) NOT NULL,
  login character varying(255),
  password character varying(255),
  ignore_ssl_errors boolean DEFAULT false NOT NULL
);

CREATE SEQUENCE ds_jackrabbit_server_seq start with 1 ;