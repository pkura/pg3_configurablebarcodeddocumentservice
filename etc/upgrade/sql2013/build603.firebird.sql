CREATE GENERATOR ds_epuap_skrytka_id;
CREATE TABLE ds_epuap_skrytka
(
	id NUMERIC(18,0),
	adres_skrytki VARCHAR(300),
	nazwa_skrytki VARCHAR(300),
	identyfikator_podmiotu VARCHAR(300),
	available smallint
);