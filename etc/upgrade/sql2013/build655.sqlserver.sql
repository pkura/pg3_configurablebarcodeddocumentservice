CREATE TABLE DSG_INVOICE_P_MPK (
  id int identity(1,1) NOT NULL,
  CENTRUMID int,
  ACCOUNTNUMBER int,
  KWOTA float,
  STATUS int default 1 not null,
  TATUS_BY int default 10 not null,
  primary key (id)
);