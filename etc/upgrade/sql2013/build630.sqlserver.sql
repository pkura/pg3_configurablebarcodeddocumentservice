drop index dso_person_organization on dso_person;
alter table dso_person alter column organization varchar (249);
create index dso_person_organization on dso_person (organization);