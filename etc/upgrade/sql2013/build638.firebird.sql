alter table ds_subst_hist
add substituteCreator varchar(32);

UPDATE ds_subst_hist set substituteCreator = 'admin' where substituteCreator is null;

alter table ds_subst_hist
add status smallint;

UPDATE ds_subst_hist set status = 1;
