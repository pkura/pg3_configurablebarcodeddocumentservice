CREATE TABLE DS_INTERNAL_SIGN(
	ID bigint NOT NULL,
	USER_ID bigint ,
	CTIME timestamp ,
	SIGNATURE character varying(256) 
);

CREATE SEQUENCE DS_INTERNAL_SIGN_SEQ start with 1 ;

CREATE TABLE DS_INT_TO_DOC(
	ID bigint NOT NULL,
	SIGN_ID bigint ,
	DOCUMENT_ID bigint ,
	CTIME timestamp
);

CREATE SEQUENCE DS_INT_TO_DOC_SEQ start with 1 ;