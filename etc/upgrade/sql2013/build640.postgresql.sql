ALTER TABLE DS_DOCUMENT ADD VERSION bigint DEFAULT(1);
ALTER TABLE DS_DOCUMENT ADD VERSION_DESC character varying(60);
ALTER TABLE DS_DOCUMENT ADD CZY_CZYSTOPIS boolean;
ALTER TABLE DS_DOCUMENT ADD VERSION_GUID bigint;
ALTER TABLE DS_DOCUMENT ADD CZY_AKTUALNY boolean;
