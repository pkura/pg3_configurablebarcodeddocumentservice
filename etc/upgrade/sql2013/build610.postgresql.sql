alter table dsg_centrum_kosztow_faktury add orderId int;
alter table dsg_centrum_kosztow_faktury add groupId int ;
alter table dsg_centrum_kosztow_faktury add productId int;



CREATE TABLE DSO_USER_TO_BRIEFCASE(
	ID bigint NOT NULL,
	containerId bigint NOT NULL,
	containerOfficeId character varying(84) NOT NULL,
	userId bigint NOT NULL,
	userName character varying(62),
	userFirstname character varying(62),
	userLastname character varying(62)
);


alter table DSO_USER_TO_BRIEFCASE add canView boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canCreate boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canModify boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canPrint boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canViewCases boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canAddCasesToBriefcase boolean not null;

create sequence DSO_USER_TO_BRIEFCASE_ID start with 1 ;