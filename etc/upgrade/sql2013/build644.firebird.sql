ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;

CREATE TABLE ds_jackrabbit_server (
  id SMALLINT NOT NULL PRIMARY KEY,
  status VARCHAR(255) NOT NULL,
  internal_url VARCHAR(255) NOT NULL,
  user_url VARCHAR(255) NOT NULL,
  login VARCHAR(255) NULL,
  password VARCHAR(255) NULL,
  ignore_ssl_errors BIT NOT NULL DEFAULT(0)
)

CREATE GENERATOR ds_jackrabbit_server_seq;
SET GENERATOR ds_jackrabbit_server_seq TO 0;