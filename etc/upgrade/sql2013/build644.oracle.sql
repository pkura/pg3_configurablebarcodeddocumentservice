ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;

CREATE TABLE ds_jackrabbit_server (
  id SMALLINT NOT NULL PRIMARY KEY,
  status VARCHAR2(255) NOT NULL,
  internal_url VARCHAR2(255) NOT NULL,
  user_url VARCHAR2(255) NOT NULL,
  login VARCHAR2(255) NULL,
  password VARCHAR2(255) NULL,
  ignore_ssl_errors SMALLINT DEFAULT(0) NOT NULL
);

CREATE SEQUENCE ds_jackrabbit_server_seq start with 1 nocache;