CREATE TABLE ds_process_for_document(
  engine_id integer NOT NULL,
  process_id character varying(255) NOT NULL,
  document_id bigint NOT NULL,
  definition_id character varying(255) NOT NULL,

  PRIMARY KEY (process_id, engine_id)
);

CREATE INDEX idx_process_for_documentDocDef ON ds_process_for_document (document_id, definition_id);