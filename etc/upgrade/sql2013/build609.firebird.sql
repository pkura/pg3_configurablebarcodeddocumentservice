ALTER TABLE DS_USER ADD 
isSupervisor Smallint DEFAULT 0 NOT NULL;
UPDATE DS_USER 
SET isSupervisor = '0' 
WHERE isSupervisor IS NULL;