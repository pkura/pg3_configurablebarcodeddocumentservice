CREATE SEQUENCE ds_division_erp_id start WITH 1 nocache;

commit;

CREATE TABLE ds_division_erp(
	id NUMERIC(19,0) PRIMARY KEY,
	ds_division_idds_division numeric(19, 0) NOT NULL,
	is_cost smallint NOT NULL,
	is_department smallint NOT NULL	
);

commit;

