CREATE TABLE DS_INTERNAL_SIGN(
	ID number NOT NULL,
	USER_ID number NULL,
	CTIME timestamp NULL,
	SIGNATURE varchar(256) NULL
);

CREATE SEQUENCE DS_INTERNAL_SIGN_SEQ start with 1 nocache;

CREATE TABLE DS_INT_TO_DOC(
	ID number NOT NULL,
	SIGN_ID number NULL,
	DOCUMENT_ID number NULL,
	CTIME timestamp NULL
);

CREATE SEQUENCE DS_INT_TO_DOC_SEQ start with 1 nocache;