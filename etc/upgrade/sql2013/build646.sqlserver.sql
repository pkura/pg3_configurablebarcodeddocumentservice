--@ignore-exceptions;
CREATE VIEW PROCESS_REPORT_VIEW (ID, DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

--akutalne procesy
select distinct
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,-- 'id',
	tVar.LONG_VALUE_									,-- 'document id',
	SUBSTRING(tExe.ID_, 0, CHARINDEX('.', tExe.ID_))	,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	tUser.FIRSTNAME + ' ' + tUser.LASTNAME				 -- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like '%.' + convert(varchar,tVar.EXECUTION_)
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like tExe.id_ + '%'
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

--oraz
UNION ALL

--hist. procesy
select
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,-- 'id',
	convert(numeric,tHistVar.VALUE_)					,-- 'document id',
	SUBSTRING(tExe.ID_, 0, CHARINDEX('.', tExe.ID_))	,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	tUser.FIRSTNAME + ' ' + tUser.LASTNAME				 -- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like '%.' + convert(varchar,tHistVar.HPROCI_)
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like tExe.id_ + '%'
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';