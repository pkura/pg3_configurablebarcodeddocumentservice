CREATE TABLE ds_process_for_document(
  engine_id NUMBER(2) NOT NULL,
  process_id VARCHAR2(255) NOT NULL,
  document_id NUMBER(18, 0) NOT NULL,
  definition_id VARCHAR2(255) NOT NULL,

  PRIMARY KEY (process_id, engine_id)
);

CREATE INDEX idx_pfd_document_definition ON ds_process_for_document (document_id, definition_id);