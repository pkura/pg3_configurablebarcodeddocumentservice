ALTER TABLE dso_out_document ADD mailWeight NUMERIC(18, 0);

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'[dbo].[dso_mail_weight]')
AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [dbo].[dso_mail_weight];

CREATE TABLE [dbo].[dso_mail_weight]
( id       numeric(19, 0) IDENTITY (1,1) NOT NULL,
  cn       varchar(30)    NOT NULL,
  name     varchar(30)    NOT NULL,
  weightG  smallint       NULL,
  weightKg smallint       NULL
);

SET IDENTITY_INSERT dso_mail_weight ON;
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(1, 'weight_50', 'Do 50g', 50),
(2, 'weight_50-100', 'Powy�ej 50g do 100g', 100),
(3, 'weight_100-350', 'Powy�ej 100g do 350g', 350),
(4, 'weight_350-500', 'Powy�ej 350g do 500g', 500);

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(5, 'weight_500-1000', 'Powy�ej 500g do 1000g', 1000, 1),
(6, 'weight_1000-2000', 'Powy�ej 1000g do 2000g', 2000, 2);

SET IDENTITY_INSERT dso_mail_weight OFF;
