CREATE TABLE ds_printers
(
	id int identity(1,1),
	userPrinterName varchar(64),
	usePrinter bit,
	printerName varchar(64),
	firstLine varchar(64),
	secondLine varchar(64),
	barcodeType varchar(64),
	barcodeValue varchar(64),
	language varchar(64)
);
CREATE TABLE ds_printer_defined_fields
(
	printer_id int,
	field varchar(128)
);