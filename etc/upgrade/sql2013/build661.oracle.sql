CREATE TABLE ds_document_snapshot (
	id NUMERIC(19,0) PRIMARY KEY,
	user_id NUMERIC(19,0),
	document_id NUMERIC(19,0),
	ctime TIMESTAMP,
	mtime TIMESTAMP,
	content BLOB
)