CREATE TABLE dbo.[ds_division_erp](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[ds_division_idds_division] [numeric](19, 0) NOT NULL,
	[is_cost] [smallint] NOT NULL,
	[is_department] [smallint] NOT NULL	
 CONSTRAINT [ds_division_erp_pk] PRIMARY KEY CLUSTERED (
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]