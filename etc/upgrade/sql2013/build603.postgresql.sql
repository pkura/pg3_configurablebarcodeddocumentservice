
CREATE TABLE ds_epuap_skrytka
(
	id bigint,
	adres_skrytki character varying(300),
	nazwa_skrytki character varying(300),
	identyfikator_podmiotu character varying(300),
	available boolean default false
)
create sequence ds_epuap_skrytka_id start with 1 ;