CREATE TABLE ds_printers
(
	id int,
	userPrinterName varchar(64),
	usePrinter char(1),
	printerName varchar(64),
	firstLine varchar(64),
	secondLine varchar(64),
	barcodeType varchar(64),
	barcodeValue varchar(64),
	language varchar(64)
);

CREATE GENERATOR DS_PRINTERS_ID;
SET GENERATOR DS_PRINTERS_ID TO 1;


CREATE TABLE ds_printer_defined_fields
(
	printer_id int,
	field varchar(128)
);