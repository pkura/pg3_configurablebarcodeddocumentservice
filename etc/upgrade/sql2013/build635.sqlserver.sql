ALTER TABLE dso_out_document ADD gauge NUMERIC(18, 0);

CREATE TABLE dso_document_out_gauge (
	id NUMERIC(18, 0) IDENTITY(1, 1) NOT NULL,
	cn VARCHAR(20),
	title VARCHAR(20)
);

INSERT INTO dso_document_out_gauge (cn, title) VALUES ('A', 'A');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('B', 'B');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('C', 'C');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('D', 'D');