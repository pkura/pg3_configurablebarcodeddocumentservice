ALTER TABLE ds_document_kind ALTER COLUMN tablename TYPE character varying(30);

ALTER TABLE dso_person  ALTER COLUMN organizationdivision TYPE character varying(128);