--@ignore-exceptions;
  create table DSG_CURRENCY(
    id integer primary key not null,
    cn varchar(50) null,
    title varchar(255) null,
    centrum int null,
    refValue varchar(20) null,
    available int null
  );