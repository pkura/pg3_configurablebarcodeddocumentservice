CREATE VIEW PROCESS_REPORT_VIEW (ID, DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

--akutalne procesy
select distinct
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,-- 'id',
	tVar.LONG_VALUE_									,-- 'document id',
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  -- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_ AS character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

--oraz
UNION ALL

--hist. procesy
select
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,-- 'id',
	CAST(tHistVar.VALUE_ AS bigint)				,-- 'document id',
	
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 -- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tHistVar.HPROCI_ AS  character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';