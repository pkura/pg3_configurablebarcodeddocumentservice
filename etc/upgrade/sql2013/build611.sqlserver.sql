ALTER TABLE [dbo].[dso_rwa] ALTER COLUMN digit1 varchar(50);
ALTER TABLE [dbo].[dso_rwa] ALTER COLUMN digit2 varchar(50);
ALTER TABLE [dbo].[dso_rwa] ALTER COLUMN digit3 varchar(50);
ALTER TABLE [dbo].[dso_rwa] ALTER COLUMN digit4 varchar(50);
ALTER TABLE [dbo].[dso_rwa] ALTER COLUMN digit5 varchar(50);