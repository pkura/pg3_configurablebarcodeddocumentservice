ALTER TABLE dso_out_document ADD gauge bigint;

CREATE sequence dso_document_out_gauge_id 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
commit;
CREATE TABLE dso_document_out_gauge (
  id bigint NOT NULL,
  cn  character varying(20),
  title  character varying(20)
);
commit;


CREATE OR REPLACE FUNCTION insert_to_GAUGE()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('dso_document_out_gauge_id');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
commit;
 CREATE TRIGGER TRG_dso_document_out_gauge
 BEFORE INSERT
 ON dso_document_out_gauge
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_GAUGE();
 commit;
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('A', 'A');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('B', 'B');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('C', 'C');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('D', 'D');