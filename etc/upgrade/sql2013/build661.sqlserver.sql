CREATE TABLE dbo.ds_document_snapshot (
	id NUMERIC(19,0) IDENTITY,
	user_id NUMERIC(19,0),
	document_id NUMERIC(19,0),
	ctime DATETIME,
	mtime DATETIME,
	content IMAGE
)