if
(exists (select * from information_schema.tables where table_name= 'DS_INTERNAL_SIGNATURE'))	
begin
DROP TABLE DS_INTERNAL_SIGNATURE
end

if
(exists (select * from information_schema.tables where table_name= 'DS_INTERNALSIGN_TO_DOCUMENT'))	
begin
DROP TABLE DS_INTERNALSIGN_TO_DOCUMENT
end
	if
(not exists (select * from information_schema.tables where table_name= 'DS_INTERNAL_SIGN'))
begin
CREATE TABLE DS_INTERNAL_SIGN(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
USER_ID numeric(18, 0) NULL,
CTIME datetime NULL,
SIGNATURE varchar(256) NULL
)
end

if
(not exists(select * from information_schema.tables where table_name='DS_INT_TO_DOC'))

begin
CREATE TABLE DS_INT_TO_DOC(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
SIGN_ID numeric(18, 0) NULL,
DOCUMENT_ID numeric(18, 0) NULL,
CTIME datetime NULL
)
end

