CREATE TABLE DS_PRINTERS(
	ID int,
	USERPRINTERNAME character varying(64),
	USEPRINTER boolean,
	PRINTERNAME character varying(64),
	FIRSTLINE character varying(64),
	SECONDLINE character varying(64),
	BARCODETYPE character varying(64),
	BARCODEVALUE character varying(64),
	LANGUAGE character varying(64)
);
commit;


CREATE TABLE DS_PRINTER_DEFINED_FIELDS
(
	PRINTER_ID INT,
	FIELD character varying(128)
);
commit;


CREATE sequence DS_PRINTERS_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;
commit;



CREATE OR REPLACE FUNCTION insert_to_DS_PRINTERS()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_PRINTERS_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
commit;

 CREATE or replace TRIGGER TRG_DS_PRINTERS_INSERT
 BEFORE INSERT
 ON DS_PRINTERS
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_PRINTERS();
commit;

