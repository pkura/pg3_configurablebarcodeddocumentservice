create sequence DS_AR_UDOSTEPNIONE_ZASOBY_ID start with 1 ;

CREATE TABLE DS_AR_UDOSTEPNIONE_ZASOBY(
	ID bigint,
	USERNAME character varying(50),
	DATA_UDOSTEPNIENIA timestamp ,
	DATA_ZWROTU timestamp,
	RODZAJ character varying(20),
	ZASOB_ID bigint
)