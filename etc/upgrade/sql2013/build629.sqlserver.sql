--@ignore-exceptions;
alter table dsg_centrum_kosztow_faktury add acceptance_mode smallint;
--@ignore-exceptions;
alter table dsg_centrum_kosztow add acceptance_modes_binary int;
--@ignore-exceptions;
alter table dsg_centrum_kosztow_faktury add analytics_id int;
--@ignore-exceptions;
alter table dsg_centrum_kosztow_faktury add descriptionAmount varchar(600);