drop index dso_person_organization;
alter table dso_person alter column organization type varchar(249);
create index dso_person_organization on dso_person (organization);