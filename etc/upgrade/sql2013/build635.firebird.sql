ALTER TABLE dso_out_document ADD gauge NUMERIC(18, 0);
CREATE GENERATOR dso_document_out_gauge_id;
CREATE TABLE dso_document_out_gauge (
	id NUMERIC(18),
  	cn VARCHAR(20),
  	title VARCHAR(20)
);
INSERT INTO dso_document_out_gauge (id, cn, title) VALUES (gen_id(dso_document_out_gauge_id, 1), 'A', 'A');
INSERT INTO dso_document_out_gauge (id, cn, title) VALUES (gen_id(dso_document_out_gauge_id, 1), 'B', 'B');