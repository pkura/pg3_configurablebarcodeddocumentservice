create sequence ds_epuap_skrytka_id start with 1 nocache;
CREATE TABLE ds_epuap_skrytka
(
	id NUMERIC(18,0) ,
	adres_skrytki VARCHAR2(300 CHAR),
	nazwa_skrytki VARCHAR2(300 CHAR),
	identyfikator_podmiotu VARCHAR2(300 CHAR),
	available INTEGER default 0
)