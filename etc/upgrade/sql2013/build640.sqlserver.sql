ALTER TABLE ds_document
ADD VERSION int default(1);

ALTER TABLE ds_document
ADD VERSION_DESC VARCHAR(60);

ALTER TABLE ds_document
ADD CZY_CZYSTOPIS bit;

ALTER TABLE ds_document
ADD VERSION_GUID int;

ALTER TABLE ds_document
ADD CZY_AKTUALNY bit;

