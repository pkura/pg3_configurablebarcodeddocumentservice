alter table dsg_centrum_kosztow_faktury add orderId int null;
alter table dsg_centrum_kosztow_faktury add groupId int null;
alter table dsg_centrum_kosztow_faktury add productId int null;


CREATE TABLE DSO_USER_TO_BRIEFCASE(
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[containerId] [numeric](18, 0) NOT NULL,
	[containerOfficeId] [varchar](84) NOT NULL,
	[userId] [numeric](19, 0) NOT NULL,
	[userName] [varchar](62) NULL,
	[userFirstname] [varchar](62) NULL,
	[userLastname] [varchar](62) NULL,
)


alter table DSO_USER_TO_BRIEFCASE add canView smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canCreate smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canModify smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canPrint smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canViewCases smallint not null;
alter table DSO_USER_TO_BRIEFCASE add canAddCasesToBriefcase smallint not null;