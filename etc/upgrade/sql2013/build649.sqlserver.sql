--@ignore-exceptions;
ALTER VIEW PROCESS_REPORT_VIEW (DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

--akutalne procesy
SELECT DISTINCT
	tVar.LONG_VALUE_									,-- 'document id',
	SUBSTRING(tExe.ID_, 0, CHARINDEX('.', tExe.ID_))	,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	tUser.FIRSTNAME + ' ' + tUser.LASTNAME				 -- 'user'
FROM
	JBPM4_VARIABLE tVar
	INNER JOIN JBPM4_EXECUTION tExe						ON tExe.id_ like '%.' + CONVERT(VARCHAR,tVar.EXECUTION_)
	INNER JOIN JBPM4_HIST_ACTINST tHisActInst			ON tHisActInst.EXECUTION_ LIKE tExe.id_ + '%'
	LEFT JOIN JBPM4_HIST_TASK tHisTask					ON tHisTask.DBID_= tHisActInst.HTASK_
	LEFT JOIN JBPM4_PARTICIPATION tPart					ON tPart.TASK_ = tHisActInst.HTASK_
	LEFT JOIN DS_USER tUser								ON tPart.USERID_ = tUser.NAME OR tHisTask.ASSIGNEE_ =  tUser.NAME
	LEFT JOIN DS_DIVISION tDivision						ON tPart.GROUPID_ = tDivision.GUID
WHERE
	tVar.key_='doc-id'
	AND tHisActInst.TYPE_ = 'task'

--oraz
UNION

--hist. procesy
SELECT DISTINCT
	CONVERT(NUMERIC,tHistVar.VALUE_)					,-- 'document id',
	SUBSTRING(tHisActInst.EXECUTION_, 0, CHARINDEX('.', tHisActInst.EXECUTION_))	,-- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,-- 'status',
	tHisActInst.START_									,-- 'start',
	tHisActInst.END_									,-- 'end',
	tHisActInst.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	tUser.FIRSTNAME + ' ' + tUser.LASTNAME				 -- 'user'
FROM
	JBPM4_HIST_VAR tHistVar
	INNER JOIN JBPM4_HIST_ACTINST tHisActInst			ON tHisActInst.EXECUTION_ LIKE '%.' + CONVERT(VARCHAR,tHistVar.HPROCI_) + '%'
	LEFT JOIN JBPM4_HIST_TASK tHisTask					ON tHisTask.DBID_= tHisActInst.HTASK_
	LEFT JOIN JBPM4_PARTICIPATION tPart					ON tPart.TASK_ = tHisActInst.HTASK_
	LEFT JOIN DS_USER tUser								ON tPart.USERID_ = tUser.NAME OR tHisTask.ASSIGNEE_ =  tUser.NAME
	LEFT JOIN DS_DIVISION tDivision						ON tPart.GROUPID_ = tDivision.GUID
WHERE
	tHisActInst.TYPE_ = 'task';