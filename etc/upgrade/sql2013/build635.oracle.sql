ALTER TABLE dso_out_document ADD gauge NUMERIC(18, 0);

CREATE sequence dso_document_out_gauge_id start WITH 1 nocache;
CREATE TABLE dso_document_out_gauge (
  id NUMERIC(18, 0) NOT NULL,
  cn VARCHAR2(20 CHAR),
  title VARCHAR2(20 CHAR)
);

INSERT INTO dso_document_out_gauge (id, cn, title) VALUES (dso_document_out_gauge_id.nextVal, 'A', 'A');
INSERT INTO dso_document_out_gauge (id, cn, title) VALUES (dso_document_out_gauge_id.nextVal, 'B', 'B');