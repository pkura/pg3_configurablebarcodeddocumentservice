--@ignore-exceptions;
ALTER TABLE dso_out_document ADD mailWeight NUMERIC(18, 0);
commit;

--@ignore-exceptions;
DROP SEQUENCE dso_mail_weight_id ;
commit;
CREATE  SEQUENCE dso_mail_weight_id start WITH 1 nocache;
commit;

--@ignore-exceptions;
drop table dso_mail_weight;
commit;

CREATE TABLE dso_mail_weight (
  id       numeric(19, 0) NOT NULL,
  cn       varchar2(30 CHAR) NOT NULL,
  name     varchar2(50) NOT NULL,
  weightG  int NULL,
  weightKg int NULL
  );

commit;  

INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(dso_mail_weight_id.nextVal, 'weight_50', 'Do 50g', 50);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(dso_mail_weight_id.nextVal, 'weight_50-100', 'Powy�ej 50g do 100g', 100);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(dso_mail_weight_id.nextVal, 'weight_100-350', 'Powy�ej 100g do 350g', 350);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(dso_mail_weight_id.nextVal, 'weight_350-500', 'Powy�ej 350g do 500g', 500);

commit;

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(dso_mail_weight_id.nextVal, 'weight_500-1000', 'Powy�ej 500g do 1000g', 1000, 1);
INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(dso_mail_weight_id.nextVal, 'weight_1000-2000', 'Powy�ej 1000g do 2000g', 2000, 2);
commit;
