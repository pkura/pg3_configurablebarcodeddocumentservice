alter table DS_ATTACHMENT add fileExtension varchar(10);
alter table DS_ATTACHMENT add encrypted bit;

update DS_ATTACHMENT set encrypted = 0;