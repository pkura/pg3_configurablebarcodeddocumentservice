alter table DS_ATTACHMENT add fileExtension character varying(10);
alter table DS_ATTACHMENT add encrypted boolean;
update DS_ATTACHMENT set encrypted = false;
