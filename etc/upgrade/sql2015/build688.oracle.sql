alter table DS_ATTACHMENT add fileExtension VARCHAR2(10);
alter table DS_ATTACHMENT add encrypted smallint;

update DS_ATTACHMENT set encrypted = 0;
