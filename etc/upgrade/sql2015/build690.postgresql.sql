ALTER TABLE dso_email_channel ADD COLUMN deleted boolean;
ALTER TABLE dso_email_channel ALTER COLUMN deleted SET DEFAULT false;
UPDATE dso_email_channel SET deleted=false;
ALTER TABLE dso_email_channel ALTER COLUMN deleted SET NOT NULL;


-- CREATE OR REPLACE FUNCTION insert_to_DSO_OFFICE_POINT()
--   RETURNS trigger AS
-- $BODY$
--  BEGIN
--  IF NEW.id IS NULL THEN
--    New.id:=nextval('SEQ_DSO_OFFICE_POINT_ID');--@ignore-semicolon
--   END IF ;--@ignore-semicolon
--  Return NEW;--@ignore-semicolon
--  END;--@ignore-semicolon
--  $BODY$
--   LANGUAGE plpgsql VOLATILE
--   COST 100;


-- CREATE TRIGGER TRG_DSO_OFFICE_POINT
--  BEFORE INSERT ON DSO_OFFICE_POINT
-- FOR EACH ROW
-- EXECUTE PROCEDURE insert_to_DSO_OFFICE_POINT();


 CREATE SEQUENCE SEQ_DSO_OFFICE_POINT_ID
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
   
   CREATE TABLE DSO_OFFICE_POINT (
  id          SERIAL PRIMARY KEY NOT NULL,
  prefix    character varying(10)  NOT NULL,
  folderName   character varying(200) NOT NULL,
  divisionId numeric (18,0) NOT NULL,
  deleted boolean default false not null,
  prefixId numeric not null
);
 CREATE TABLE DSO_OFFICE_PREFIX (
  id          SERIAL PRIMARY KEY NOT NULL,
  prefixName    character varying(10)  NOT NULL,
  available boolean default true not null
);

INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K1_');

INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K2_');

INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K3_');

 INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K4_');

    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K5_');
    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K6_');
    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K7_');
    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K8_');
    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K9_');
    INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K10_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K11_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K12_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K13_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K14_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K15_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K16_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K17_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K18_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K19_');   
        INSERT INTO dso_office_prefix(
            prefixname)
    VALUES ( 'K20_');   
   