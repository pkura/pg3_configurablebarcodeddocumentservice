create table DS_PG_PUBLIC_DOCUMENT (
DOCUMENT_ID numeric(18),
MIESCEUTWORZENIA character varying(200),
AUTORDZIELA character varying(200),
SLOWAKLUCZOWE character varying(200),
DODATKOWE_METADANE character varying(200)
);

CREATE TABLE ds_pg_pub_dokument_multi
(
  id serial NOT NULL,
  document_id numeric(18,0) NOT NULL,
  field_cn character varying(100) NOT NULL,
  field_val character varying(100)
);

ALTER TABLE DS_AR_DOKUMENTY_PUBLICZNE ADD COLUMN KEYWORDS character varying(200);
ALTER TABLE DS_AR_DOKUMENTY_PUBLICZNE ADD COLUMN ATTACHMENTS_COUNT integer;
ALTER TABLE ds_ar_dokumenty_publiczne ADD COLUMN miniatura text;

ALTER TABLE ds_pg_public_document ADD COLUMN data_utworzenia_dziela date;

