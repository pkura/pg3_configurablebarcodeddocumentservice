create table DSI_IMPORT_TABLE
(
 import_id integer primary key identity,
 user_import_id varchar(50),
 document_kind varchar(100),
 import_kind integer default 0,
 submit_date datetime,
 import_status integer default 0,
 process_date datetime,
 document_id integer default -1,
 err_code varchar(200),
 file_path varchar(512),
 box_code varchar(20),
 feedback_status int default 0,
 feedback_date datetime,
 feedback_response varchar(100)
);

create table DSI_IMPORT_ATTRIBUTES  
(
  attribute_id integer primary key identity,
  import_id integer,
  attribute_cn varchar(50),
  attribute_value varchar(1000),
  set_code varchar(100),
  line_no integer
);

create table DSI_IMPORT_BLOB
(
  attachment_id integer primary key identity,
  import_id integer,
  attachment_name varchar(50) default 'skan',
  attachment image
);

 create table DSI_IMPORT_STATUS
 (
  import_status_id integer primary key,
  status_description varchar(100)
 );

insert into DSI_IMPORT_STATUS values (0,'Status początkowy');
insert into DSI_IMPORT_STATUS values (1, 'Gotowy do importu');
insert into DSI_IMPORT_STATUS values (2, 'Zaimportowany');
insert into DSI_IMPORT_STATUS values (3, 'Do skasowania');
insert into DSI_IMPORT_STATUS values (-1, 'Odrzucony');

create index dsi_import_table_1 on dsi_import_table (import_status);
create index dsi_import_attributes_1 on dsi_import_attributes (import_id);
create index dsi_import_blob_1 on dsi_import_blob (import_id);

create role im;

grant select,update,insert,delete on DSI_IMPORT_TABLE to im;
grant select,update,insert,delete on DSI_IMPORT_ATTRIBUTES  to im;
grant select,update,insert,delete on DSI_IMPORT_BLOB  to im;
grant select on DSI_IMPORT_STATUS  to im;