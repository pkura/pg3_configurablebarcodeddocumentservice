create table DSI_IMPORT_TABLE
(
 import_id numeric(18,0) primary key ,
 user_import_id varchar(50),
 document_kind varchar2(100),
 import_kind integer default 0,
 submit_date timestamp,
 import_status integer default 0,
 process_date timestamp,
 document_id integer default -1,
 err_code varchar2(200),
 file_path varchar2(512),
 box_code varchar2(20),
 feedback_status int default 0,
 feedback_date timestamp,
 feedback_response varchar2(100)
);

create sequence DSI_IMPORT_TABLE_ID;

create table DSI_IMPORT_ATTRIBUTES  
(
  attribute_id numeric(18,0) primary key ,
  import_id integer,
  attribute_cn varchar2(50),
  attribute_value varchar2(1000),
  set_code varchar2(100),
  line_no integer
);
create sequence DSI_IMPORT_ATTRIBUTES_ID;
create table DSI_IMPORT_BLOB
(
  attachment_id numeric(18,0) primary key ,
  import_id integer,
  attachment_name varchar2(50) default 'skan',
  attachment blob
);

 create table DSI_IMPORT_STATUS
 (
  import_status_id numeric(18,0) primary key,
  status_description varchar2(100)
 );

insert into DSI_IMPORT_STATUS values (0,'Status początkowy');
insert into DSI_IMPORT_STATUS values (1, 'Gotowy do importu');
insert into DSI_IMPORT_STATUS values (2, 'Zaimportowany');
insert into DSI_IMPORT_STATUS values (3, 'Do skasowania');
insert into DSI_IMPORT_STATUS values (-1, 'Odrzucony');

create index dsi_import_table_1 on dsi_import_table (import_status);
create index dsi_import_attributes_1 on dsi_import_attributes (import_id);
create index dsi_import_blob_1 on dsi_import_blob (import_id);
