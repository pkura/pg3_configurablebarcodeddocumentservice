CREATE FUNCTION ds_datamart_document_view_log(@notViewed bit, @usernames varchar(max), @from datetime, @to datetime)
RETURNS  @results TABLE 
(DOCUMENT_ID bigint NOT NULL)
AS
BEGIN
	  DECLARE @tableUsernames TABLE (username varchar(100));
	  DECLARE @DELIMITER VARCHAR(1) = ',';
      DECLARE @LENSTRING INT  
  
      WHILE LEN( @usernames ) > 0  
         BEGIN  
          
            SELECT @LENSTRING =  
               (CASE CHARINDEX( @DELIMITER, @usernames )  
                   WHEN 0 THEN LEN( @usernames )  
                   ELSE ( CHARINDEX( @DELIMITER, @usernames ) -1 ) 
                END 
               )  
                                 
            INSERT INTO @tableUsernames  
               SELECT SUBSTRING( @usernames, 1, @LENSTRING ) 
                 
            SELECT @usernames =  
               (CASE ( LEN( @usernames ) - @LENSTRING )  
                   WHEN 0 THEN ''  
                   ELSE RIGHT( @usernames, LEN( @usernames ) - @LENSTRING - 1 )  
                END 
               )  
         END 



IF (@notViewed = 1)
	BEGIN
		IF (@usernames IS NOT NULL)
			BEGIN
				insert into @results
				select id from ds_document where author not in (select * from @tableUsernames) group by id
				except
				select document_id from ds_datamart where username in (select * from @tableUsernames) and event_code='document.view' group by document_id;
			END
	END
ELSE
	BEGIN
		IF (@from is null)
			BEGIN
					IF (@to is null)
						BEGIN
							insert into @results
							select id from ds_document where author in (select * from @tableUsernames) group by id
							union
							select document_id from ds_datamart where username in (select * from @tableUsernames) and event_code='document.view' group by document_id;
						END
					ELSE
						BEGIN
							insert into @results
							select id from ds_document where author in (select * from @tableUsernames) and MTIME <= @to group by id
							union
							select document_id from ds_datamart where username in (select * from @tableUsernames) and event_code='document.view' and event_date <= @to group by document_id;
						END
			END
		ELSE
			BEGIN
					IF (@to is null)
						BEGIN
							insert into @results
							select id from ds_document where author in (select * from @tableUsernames) and MTIME >= @from group by id
							union
							select document_id from ds_datamart where username in (select * from @tableUsernames) and event_code='document.view' and event_date >= @from group by document_id;
						END
					ELSE
						BEGIN
							insert into @results
							select id from ds_document where author in (select * from @tableUsernames) and MTIME >= @from and MTIME <= @to group by id
							union
							select document_id from ds_datamart where username in (select * from @tableUsernames) and event_code='document.view' and event_date >= @from and event_date <= @to group by document_id;
						END
			END

	END

return

END
