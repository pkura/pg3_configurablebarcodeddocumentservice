ALTER VIEW [dbo].[PROCESS_REPORT_VIEW] (DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS
SELECT
	tVar.VALUE_									,-- 'document id',
	SUBSTRING(jhActi.EXECUTION_, 0, CHARINDEX('.', jhActi.EXECUTION_))	,-- 'process name',
	jhActi.ACTIVITY_NAME_  						,-- 'status',
	jhActi.START_									,-- 'start',
	jhActi.END_									,-- 'end',
	jhActi.DURATION_								,-- 'duration',
	tDivision.ID										,-- 'division id',
	tDivision.NAME										,-- 'division',
	tUser.ID                                            ,-- 'user id',
	tUser.FIRSTNAME + ' ' + tUser.LASTNAME				 -- 'user'
FROM
	JBPM4_HIST_VAR tVar WITH(NOLOCK)
	LEFT JOIN JBPM4_HIST_ACTINST jhActi WITH(NOLOCK)	ON jhActi.HPROCI_=tVar.HPROCI_ and tVar.VARNAME_='doc-id' and jhActi.TYPE_ = 'task'
	LEFT JOIN JBPM4_HIST_TASK tHisTask WITH(NOLOCK)		ON tHisTask.DBID_= jhActi.HTASK_
	LEFT JOIN JBPM4_PARTICIPATION tPart WITH(NOLOCK)	ON tPart.TASK_ = jhActi.HTASK_
	LEFT JOIN DS_USER tUser WITH(NOLOCK)				ON tPart.USERID_ = tUser.NAME OR tHisTask.ASSIGNEE_ =  tUser.NAME
	LEFT JOIN DS_DIVISION tDivision WITH(NOLOCK)		ON tPart.GROUPID_ = tDivision.GUID
GO