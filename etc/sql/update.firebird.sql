-- wersja bazowa: build 0

-- build 54: (do wstawienia po release)

create table ds_document_permission (
    document_id     NUMERIC(18,0) NOT NULL,
    SUBJECT         VARCHAR(62) NOT NULL,
    SUBJECTTYPE     VARCHAR(14) NOT NULL,
    NAME            VARCHAR(14) NOT NULL,
    NEGATIVE        SMALLINT NOT NULL
);

create index ds_document_permission_pk on ds_document_permission (document_id, subject, subjecttype, name);
alter table ds_document_permission add constraint ds_document_permission_fk foreign key (document_id) references ds_document;

create table ds_folder_permission (
    folder_id       NUMERIC(18,0) NOT NULL,
    SUBJECT         VARCHAR(62) NOT NULL,
    SUBJECTTYPE     VARCHAR(14) NOT NULL,
    NAME            VARCHAR(14) NOT NULL,
    NEGATIVE        SMALLINT NOT NULL
);

create index ds_folder_permission_pk on ds_folder_permission (folder_id, subject, subjecttype, name);
alter table ds_folder_permission add constraint ds_folder_permission_fk foreign key (folder_id) references ds_folder;

-- kopiowanie uprawnien z ds_permission
-- usuniecie ds_permission

insert into ds_document_permission (document_id, subject, subjecttype, name, negative) select objectid, subject, subjecttype, name, negative from ds_permission where objecttype='document';
insert into ds_folder_permission (folder_id, subject, subjecttype, name, negative) select objectid, subject, subjecttype, name, negative from ds_permission where objecttype='folder';

commit;

drop table ds_permission;

alter table dso_out_office_document add zpodate date;


alter table ds_attachment add BARCODE      VARCHAR(25) CHARACTER SET UNICODE_FSS;


-- build: 55

drop table dso_rwa_category;
drop table dso_rwa_root;
drop generator dso_rwa_root_id;
drop generator dso_rwa_category_id;
drop generator dso_addressee_id;
drop generator dso_simple_process_id;

drop table dso_common_office_attrs;
drop table dso_outgoing_package_attrs;
drop table DSO_INCOMING_ATTRS;
drop table dso_incoming_attrs_attach;
drop table dso_simple_process_history;
drop table dso_simple_process;
drop table dso_addressee;

alter table dso_in_office_document add referenceid varchar(60) character set unicode_fss;
alter table dso_in_office_document add ASSIGNEDDIVISION       VARCHAR(62);
alter table dso_out_office_document add ASSIGNEDDIVISION       VARCHAR(62);

update dso_in_office_document set assigneddivision='rootdivision';
update dso_out_office_document set assigneddivision='rootdivision';

alter table dso_in_office_document add master_document_id numeric(18,0);
alter table dso_out_office_document add casedocumentid varchar(84);


-- build: 56

alter table dso_in_office_document add cancelled smallint not null;
alter table dso_in_office_document add cancellationreason varchar(80) CHARACTER SET UNICODE_FSS;
alter table dso_out_office_document add cancelled smallint not null;
alter table dso_out_office_document add cancellationreason varchar(80) CHARACTER SET UNICODE_FSS;
update dso_in_office_document set cancelled=0;
update dso_out_office_document set cancelled=0;


-- build: 57

alter table dso_in_office_document add stampdate__ date;
update dso_in_office_document set stampdate__ = stampdate;
alter table dso_in_office_document drop stampdate;
alter table dso_in_office_document alter stampdate__ to stampdate;

alter table dso_in_office_document add documentdate__ date;
update dso_in_office_document set documentdate__ = documentdate;
alter table dso_in_office_document drop documentdate;
alter table dso_in_office_document alter documentdate__ to documentdate;

alter table dso_out_office_document add documentdate__ date;
update dso_out_office_document set documentdate__ = documentdate;
alter table dso_out_office_document drop documentdate;
alter table dso_out_office_document alter documentdate__ to documentdate;

alter table dso_out_office_document drop draft;
alter table dso_out_office_document add prepstate varchar(15);

alter table dso_out_office_document add cancelledbyusername varchar(62);
alter table dso_in_office_document add cancelledbyusername varchar(62);

-- build: 58

alter table dso_portfolio add constraint dso_portfolio_pk primary key (id);

create table dso_portfolio_audit (
    portfolio_id        NUMERIC(18,0) NOT NULL,
    posn                INTEGER NOT NULL,
    ctime               TIMESTAMP NOT NULL,
    property            VARCHAR(62) CHARACTER SET UNICODE_FSS,
    username            VARCHAR(62) CHARACTER SET UNICODE_FSS NOT NULL,
    description         VARCHAR(200) CHARACTER SET UNICODE_FSS NOT NULL,
    lparam              VARCHAR(200) CHARACTER SET UNICODE_FSS,
    wparam              NUMERIC(18,0)
);

alter table dso_portfolio_audit add constraint dso_portfolio_audit_fk1 foreign key (portfolio_id) references dso_portfolio (id);

-- build: 59

create table dso_collective_assignment (
    id                  integer not null,
    objective           varchar(100) CHARACTER SET UNICODE_FSS NOT NULL,
    wfprocess           varchar(50) CHARACTER SET UNICODE_FSS NOT NULL
);

alter table dso_collective_assignment add constraint dso_collective_assignment_pk primary key (id);

create generator dso_collective_assignment_id;
set generator dso_collective_assignment_id to 1;


-- build: 60

alter table dso_document_asgn_history add accepted smallint not null;
update dso_document_asgn_history set accepted = 0;

alter table dso_document_asgn_history alter targetuser to targetuser__;
alter table dso_document_asgn_history add targetuser varchar(62);
update dso_document_asgn_history set targetuser = targetuser__;
alter table dso_document_asgn_history drop targetuser__;

alter table dso_in_office_document add currentassignmentguid varchar(62);
alter table dso_in_office_document add currentassignmentusername varchar(62);
alter table dso_in_office_document add currentassignmentaccepted smallint;

alter table dso_out_office_document add currentassignmentguid varchar(62);
alter table dso_out_office_document add currentassignmentusername varchar(62);
alter table dso_out_office_document add currentassignmentaccepted smallint;

-- build: 62

update dso_role_permissions set permission_name = 'SLOWNIK_OSOB_DODAWANIE' where permission_name = 'SLOWNIK_OSOB_MODYFIKACJE';
INSERT INTO DSO_IN_OFFICE_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (12, 12, 'Faktura', 14);

alter table dso_journal alter ownerguid to og__;
alter table dso_journal add ownerguid VARCHAR(126) CHARACTER SET UNICODE_FSS;
update dso_journal set ownerguid = og__;
alter table dso_journal drop og__;

update dso_journal set ownerguid = null where ownerguid = 'rootdivision';

-- build: 63

alter table dso_journal add symbol varchar(20) character set unicode_fss;
alter table dso_in_office_document add constraint kind_fk foreign key (kind) references dso_in_office_document_kind (id);
alter table dso_in_office_document add constraint status_fk foreign key (status) references dso_in_office_document_status (id);

create generator dso_in_office_document_kind_id;
set generator dso_in_office_document_kind_id to 1;

alter table dso_in_office_document add officenumbersymbol varchar(20) character set unicode_fss;
alter table dso_out_office_document add officenumbersymbol varchar(20) character set unicode_fss;


alter table dso_in_office_document add delivery integer;
alter table dso_out_office_document add delivery integer;

create table dso_in_office_doc_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

create table dso_out_office_doc_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

alter table dso_in_office_document add constraint dso_indoc_delivery_fk foreign key (delivery) references dso_in_office_doc_delivery (id);
alter table dso_out_office_document add constraint dso_outdoc_delivery_fk foreign key (delivery) references dso_out_office_doc_delivery (id);

create generator dso_in_office_doc_delivery_id;
set generator dso_in_office_doc_delivery_id to 1;

create generator dso_out_office_doc_delivery_id;
set generator dso_out_office_doc_delivery_id to 1;

create table dso_out_doc_asgn_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL
);

create generator dso_out_doc_asgn_objective_id;
set generator dso_out_doc_asgn_objective_id to 1;


create table dso_in_doc_asgn_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL
);

create generator dso_in_doc_asgn_objective_id;
set generator dso_in_doc_asgn_objective_id to 1;


-- build: 64

drop table dso_out_doc_asgn_objective;
drop table dso_in_doc_asgn_objective;
drop generator dso_out_doc_asgn_objective_id;
drop generator dso_in_doc_asgn_objective_id;

create generator dso_assignment_objective_id;
set generator dso_assignment_objective_id to 1;

create table dso_assignment_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(80) CHARACTER SET UNICODE_FSS NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL,
    DOCUMENTTYPE            VARCHAR(10) NOT NULL
);
