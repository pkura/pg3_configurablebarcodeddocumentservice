-- procedura do czyszczenia folderow
CREATE PROCEDURE CLEAR_EMPTY_FOLDERS
AS
  DECLARE @FOLDER_ID INTEGER
  DECLARE @DOCS INTEGER
  DECLARE @FOLDERS INTEGER
  DECLARE c0 CURSOR FORWARD_ONLY 
  FOR 
    SELECT ID FROM DS_FOLDER WITH (NOLOCK) where PARENT_ID > 1 ORDER BY ID DESC
  OPEN c0
  FETCH NEXT FROM c0 INTO @FOLDER_ID

  WHILE @@FETCH_STATUS = 0
   BEGIN
    -- licze dokumenty podrzedne
    DECLARE c1 cursor FORWARD_ONLY FOR SELECT COUNT (*) FROM DS_FOLDER WITH (NOLOCK) WHERE PARENT_ID = @FOLDER_ID
    open c1
    FETCH NEXT FROM c1 INTO @FOLDERS
    close c1
    deallocate c1
    
     IF @FOLDERS = 0 
      BEGIN
       -- jesli nie ma folderow to liczymy dokumenty
		DECLARE c2 cursor FORWARD_ONLY FOR SELECT COUNT (*) FROM DS_DOCUMENT WITH (NOLOCK) WHERE FOLDER_ID = @FOLDER_ID
		open c2
        FETCH NEXT FROM c2 INTO @DOCS
        close c2
        deallocate c2
        IF @DOCS = 0 
         BEGIN
          -- kasujmey
          delete from DS_FOLDER_PERMISSION WHERE FOLDER_ID = @FOLDER_ID
          delete from DS_FOLDER WHERE ID = @FOLDER_ID
         END -- IF @DOCS
      END -- IF @FOLDERS
    FETCH NEXT FROM c0 INTO @FOLDER_ID
   END -- WHILE 
  close c0
  DEALLOCATE c0
