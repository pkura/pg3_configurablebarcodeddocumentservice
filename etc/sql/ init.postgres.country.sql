
CREATE  TABLE dsr_country(
	id numeric NOT NULL,
	cn character varying(50) NULL,
	title character varying(255) NULL,
	centrum integer NULL,
	refValue character varying(20) NULL
);



CREATE sequence dsr_country_id 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;


CREATE OR REPLACE FUNCTION insert_to_DSR_COUNRY()
 RETURNS "trigger" AS
 
$BODY$
declare id numeric(18,0);
 BEGIN
 IF NEW.id IS NULL THEN
   NEW.id:=CAST (nextval('dsr_country_id') as numeric);
   Return NEW;
 END IF ;
 END;
 $BODY$
 LANGUAGE 'plpgsql' VOLATILE;
 
drop TRIGGER IF  EXISTS TRG_dsr_country on dsr_country;
 CREATE TRIGGER TRG_dsr_country
 BEFORE INSERT
 ON dsr_country
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DSR_COUNRY();

 

		insert into dsr_country values (nextval('dsr_country_id'),'PL', 'Polska',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AF', 'Afganistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AL', 'Albania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DZ', 'Algieria',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AD', 'Andora',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AO', 'Angola',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AI', 'Anguilla',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AG', 'Antigua i Barbuda',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AN', 'Antyle Holenderskie',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SA', 'Arabia Saudyjska',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AR', 'Argentyna',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AM', 'Armenia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AW', 'Aruba',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AU', 'Australia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AT', 'Austria',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AZ', 'Azerbejd�an',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BS', 'Bahamy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BH', 'Bahrajn',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BD', 'Bangladesz',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BB', 'Barbados',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BE', 'Belgia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BZ', 'Belize',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BM', 'Bermuda',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BT', 'Bhutan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BY', 'Bia�oru�',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BO', 'Boliwia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BA', 'Bo�nia i Hercegowina',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BW', 'Botswana',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BR', 'Brazylia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BN', 'Brunei',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IO', 'Brytyjskie Terytorium Oceanu Indyjskiego',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BG', 'Bu�garia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BF', 'Burkina Faso',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BI', 'Burundi',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CL', 'Chile',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CN', 'Chiny',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'HR', 'Chorwacja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CY', 'Cypr',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TD', 'Czad',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CG', 'Czarnog�ra',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CZ', 'Czechy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DK', 'Dania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DM', 'Dominika',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DO', 'Dominikana',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DJ', 'D�ibuti',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'EC', 'Ekwador',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'EG', 'Egipt',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ER', 'Erytrea',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'EE', 'Estonia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ET', 'Etiopia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FK', 'Falklandy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FJ', 'Fid�i',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PH', 'Filipiny',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FI', 'Finlandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FR', 'Francja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GA', 'Gabon',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GM', 'Gambia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GS', 'Georgia Po�udniowa i Sandwich Po�udniowy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GH', 'Ghana',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GI', 'Gibraltar',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GR', 'Grecja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GL', 'Greenlandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GD', 'Grenada',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GE', 'Gruzja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GU', 'Guam',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GBG', 'Guernsey',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GW', 'Guinea-Bissau',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GF', 'Gujana Francuska',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GY', 'Guyana',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GP', 'Gwadelupa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GT', 'Gwatemala',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GN', 'Gwinea',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GQ', 'Gwinea R�wnikowa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'HT', 'Haiti',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ES', 'Hiszpania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NL', 'Holandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'HN', 'Honduras',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'HK', 'Hongkong',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'HU', 'Hungary',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IN', 'Indie',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ID', 'Indonezja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IR', 'Iran',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IQ', 'Irak',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IE', 'Irlandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IS', 'Islandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IL', 'Izrael',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'JM', 'Jamajka',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'JP', 'Japonia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'YE', 'Jemen',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GBJ', 'Jersey',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'JO', 'Jordania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KY', 'Kajmany',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KH', 'Kambod�a',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CM', 'Kamerun',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CA', 'Kanada',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'QA', 'Katar',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KZ', 'Kazachstan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KE', 'Kenia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KI', 'Kiribati',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CO', 'Kolumbia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KM', 'Komory',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CD', 'Kongo',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KP', 'Korea P�nocna',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KR', 'Korea Po�udniowa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CR', 'Kostaryka',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CU', 'Kuba',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KW', 'Kuwejt',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KG', 'Kirgistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LA', 'Laos',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LV', '�otwa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LB', 'Liban',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LS', 'Lesotho',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LR', 'Liberia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LY', 'Libia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LI', 'Liechtenstein',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LT', 'Litwa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LU', 'Luksemburg',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MO', 'Makau',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MK', 'Macedonia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MG', 'Madagaskar',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MW', 'Malawi',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MV', 'Malediwy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MY', 'Malezja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ML', 'Mali',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MT', 'Malta',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MP', 'Mariany P�nocne',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MQ', 'Martynika',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MR', 'Mauretania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MU', 'Mauritius',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'YT', 'Majotta',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MX', 'Meksyk',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FM', 'Mikronezja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MD', 'Mo�dawia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MC', 'Monako',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MN', 'Mongolia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MS', 'Montserrat',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MA', 'Maroko',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MZ', 'Mozambik',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MM', 'Myanmar (Birma)',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NA', 'Namibia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NR', 'Nauru',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NP', 'Nepal',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NZ', 'Nowa Zelandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DE', 'Niemcy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NE', 'Niger',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NG', 'Nigeria',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NI', 'Nikaragua',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UN', 'Niue',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NF', 'Norfolk',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NO', 'Norwegia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'NC', 'Nowa Kaledonia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'OM', 'Oman',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PK', 'Pakistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PW', 'Palau',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PA', 'Panama',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PG', 'Papua-Nowa Gwinea',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PY', 'Paragwaj',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PE', 'Peru',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PR', 'Portoryko',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PT', 'Portugalia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CF', 'Republika �rodkowoafryka�ska',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CV', 'Republika Zielonego Przyl�dka',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'RE', 'Reunion',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'RO', 'Rumunia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'RU', 'Rosja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ZA', 'RPA',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'RW', 'Rwanda',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'EH', 'Sahara Zachodnia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'KN', 'Saint Kitts i Nevis',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LC', 'Saint Lucia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PM', 'Saint-Pierre i Miquelon',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VC', 'Saint Vincent i Grenadyny',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SV', 'Salvador',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'WS', 'Samoa',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AS', 'Samoa Ameryka�skie',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SM', 'San Marino',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SN', 'Senegal',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'YU', 'Serbia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SC', 'Seszele',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SL', 'Sierra Leone',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SG', 'Singapur',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SK', 'S�owacja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SI', 'S�owenia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SO', 'Somalia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'LK', 'Sri Lanka',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VA', 'State of the Vatican City',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SD', 'Sudan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'DR', 'Surinam',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SZ', 'Szwajcaria',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SE', 'Szwecja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SH', '�wi�ta Helena',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CH', 'Switzerland',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SY', 'Syria',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TJ', 'Tad�ykistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TH', 'Tajlandia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TW', 'Tajwan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TZ', 'Tanzania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TP', 'Timor Zachodni',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TG', 'Togo',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TK', 'Tokelau',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TO', 'Tonga',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TN', 'Tunezja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TR', 'Turcja',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TM', 'Turkmenistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TC', 'Turks i Caicos',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TV', 'Tuvalu',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'TT', 'Trynidad i Tobago',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UG', 'Uganda',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UA', 'Ukraina',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UY', 'Urugwaj',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UZ', 'Uzbekistan',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'US', 'USA',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VU', 'Vanuatu',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VE', 'Venezuela',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VN', 'Wietnam',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'WF', 'Wallis i Futuna',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'UK', 'Wielka Brytania',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'IT', 'W�ochy',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CI', 'Wybrze�e Ko�ci S�oniowej',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AX', 'Wyspy Alandzkie',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'BV', 'Wyspa Bouveta',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CX', 'Wyspa Bo�ego Narodzenia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CK', 'Wyspy Cooka',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VG', 'Wyspy Dziewicze (Brytyjskie)',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'VI', 'Wyspy Dziewicze (Stan�w Zjednoczonych)',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'CC', 'Wyspy Kokosowe',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'GBM', 'Wyspa Man',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'MH', 'Wyspy Marshalla',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'FO', 'Wyspy Owcze',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'PN', 'Wyspy Pitcairn',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'SB', 'Wyspy Salomona',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ST', 'Wyspy �wi�tego Tomasza i Ksi���ca',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ZRCD', 'Zair',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ZM', 'Zambia',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'ZW', 'Zimbabwe',NULL,NULL);
		insert into dsr_country values (nextval('dsr_country_id'),'AE', 'Zjednoczone Emiraty Arabskie',NULL,NULL);
        
alter table dsr_country add available boolean default true not null;


ALTER TABLE dso_person  ALTER COLUMN country TYPE character varying(5);