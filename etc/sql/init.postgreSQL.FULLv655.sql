

---Gotowy skrypt do odpalenia aby utworzyc pe�na baz� danych do versji 655
---CREATE ROLE docusafe LOGIN ENCRYPTED PASSWORD 'md58bc2c89c07496abe357d83c0d46f0565' - docusafe
---  SUPERUSER CREATEDB CREATEROLE REPLICATION
---   VALID UNTIL 'infinity';

---CREATE DATABASE docusafe
---WITH OWNER = docusafe
---       ENCODING = 'UTF8'
---       TABLESPACE = pg_default
---       LC_COLLATE = 'Polish_Poland.1250'
---       LC_CTYPE = 'Polish_Poland.1250'
---       CONNECTION LIMIT = -1;





--- skrypt tworzacy tabele 
create table ARCHIVE_DS_DOCUMENT_CHANGELOG 
(
DOCUMENT_ID  numeric (18,0),
USERNAME  character varying(62),
CTIME  TIMESTAMP,
WHAT  character varying(100)
);

create table DAA_AGENCJA 
(
ID  INTEGER NOT NULL,
RODZAJ  INTEGER,
NAZWA  character varying(255),
NIP  character varying(20),
ULICA  character varying(255),
KOD  character varying(255),
MIEJSCOWOSC  character varying(255),
EMAIL  character varying(255),
FAKS  character varying(255),
TELEFON  character varying(255)
);

create table DAA_AGENT 
(
ID  INTEGER NOT NULL,
NAZWISKO  character varying(255),
IMIE  character varying(255),
FK_DAA_AGENCJA  INTEGER,
NUMER  INTEGER
);

create table DSC_CERTIFICATE 
(
ID  INTEGER NOT NULL,
USERNAME  character varying(62) NOT NULL,
ALIAS  character varying(10) NOT NULL,
KIND  INTEGER NOT NULL,
ENCODED  character varying(4000) NOT NULL,
CREATIONDATE  DATE,
SIGNED_ID  INTEGER
);

create table DSC_CONTRACTOR 
(
ID  INTEGER NOT NULL,
CONTRACTORDICTIONARYID  INTEGER,
NAME  character varying(50),
FULLNAME  character varying(200),
NIP  character varying(10),
REGON  character varying(50),
KRS  character varying(50),
STREET  character varying(50),
CODE  character varying(50),
CITY  character varying(50),
COUNTRY  character varying(50),
REMARKS  character varying(200),
CONTRACTORNO  character varying(50),
FAX  character varying(50),
PHONENUMBER  character varying(50),
PHONENUMBER2  character varying(50),
EMAIL  character varying(50),
WWW  character varying(50)
);

create table DSC_CONTRACTOR_DICTIONARY 
(
ID  INTEGER NOT NULL,
NAME  character varying(20)
);

create table DSC_WORKER 
(
ID  INTEGER NOT NULL,
TITLE  character varying(30),
FIRSTNAME  character varying(50),
LASTNAME  character varying(50),
STREET  character varying(50),
ZIP  character varying(14),
LOCATION  character varying(50),
COUNTRY  character varying(50),
PESEL  character varying(11),
NIP  character varying(15),
EMAIL  character varying(50),
FAX  character varying(30),
REMARKS  character varying(200),
PHONENUMBER  character varying(20),
CONTRACTORID  INTEGER
);

create table DSE_ADDRESS 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
CONTRACTOR_ID  numeric (18,0) NOT NULL,
KIND  INTEGER NOT NULL,
NAME  character varying(32),
FIRSTNAME  character varying(32),
LASTNAME  character varying(32),
PERSONALID  character varying(16),
POSTCODE  character varying(6),
CITY  character varying(32),
STREET  character varying(32),
STREETNUMBER  character varying(16),
DESCRIPTION  character varying(512)
);

create table DSE_BRANCH 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(64) NOT NULL
);

create table DSE_BRANCH_CONTRACTOR 
(
CONTRACTOR_ID  numeric (18,0) NOT NULL,
BRANCH_ID  numeric (18,0) NOT NULL
);

create table DSE_CHANGE_EVENT 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
HISTORY_ID  numeric (18,0) NOT NULL,
FIELD_NAME  character varying(16),
OLD_VALUE  character varying(512),
NEW_VALUE  character varying(512)
);

create table DSE_CLASSIFICATION 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
PARENT_ID  numeric (18,0),
SYMBOL  character varying(8) NOT NULL,
DESCRIPTION  character varying(254) NOT NULL
);

create table DSE_CLASSIFICATION_CONTRACTOR 
(
CONTRACTOR_ID  numeric (18,0) NOT NULL,
CLASSIFICATION_ID  numeric (18,0) NOT NULL,
KIND  INTEGER NOT NULL
);

create table DSE_CONTRACTOR 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(254) NOT NULL,
NIP  character varying(25) NOT NULL,
REGON  character varying(25) NOT NULL,
REG_NUMBER  character varying(25) NOT NULL,
OLD_NUMBER  character varying(25) NOT NULL,
BRANCH_ID  character varying(25) NOT NULL,
START_DATE  TIMESTAMP,
CASE_NUMBER  character varying(25) NOT NULL,
DECLARATION_DATE  TIMESTAMP,
REGISTRATION_DATE  TIMESTAMP,
CONFIRMATION_DATE  TIMESTAMP,
ACTIVITY_DESCRIPTION  character varying(254),
WHOLE_COUNTRY_ACTIVITY  BOOLEAN,
SUSPENDED  BOOLEAN,
SUSPENSION_START_DATE  TIMESTAMP,
SUSPENSION_END_DATE  TIMESTAMP,
SUSPENSION_REASON  character varying(254),
REMOVED  BOOLEAN,
REMOVE_DATE  TIMESTAMP,
REMOVE_DECISION_NUMBER  character varying(25),
REMOVE_DECISION_DATE  TIMESTAMP,
MOVED_TO_KRS  BOOLEAN,
REMOVE_REASON  character varying(254),
REGON_DECL  BOOLEAN,
REGON_DECL_ADDRESSEE_ID  numeric (18,0),
REGON_DECL_DATE  TIMESTAMP,
REGON_DECL_RESPONSE_DATE  TIMESTAMP,
NIP_DECL  BOOLEAN,
NIP_DECL_ADDRESSEE_ID  numeric (18,0),
NIP_DECL_DATE  TIMESTAMP,
NIP_DECL_RESPONSE_DATE  TIMESTAMP,
ANNOTATIONS  character varying(254),
STATUS_ID  numeric (18,0) NOT NULL
);

create table DSE_EVENT 
(
EVENT_ID  numeric (18,0) NOT NULL,
EVENT_STATUS_ID  INTEGER,
SUBMIT_DATE  TIMESTAMP,
TRIGGER_CODE  character varying(100),
HANDLER_CODE  character varying(100),
DUE_DATE  TIMESTAMP,
NEXT_CHECK_DATE  TIMESTAMP,
PARAMS  character varying(4000),
DOCUMENT_ID  numeric (18,0)
);

create table DSE_HISTORY 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
CONTRACTOR_ID  numeric (18,0) NOT NULL,
CASE_NUMBER  character varying(32) NOT NULL,
DECLARATION_DATE  TIMESTAMP NOT NULL,
REGISTRATION_DATE  TIMESTAMP NOT NULL,
DESCRIPTION  character varying(254) NOT NULL,
AUTHOR  character varying(32) NOT NULL
);

create table DSE_OFFICE 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
KIND  INTEGER NOT NULL,
NAME  character varying(128) NOT NULL,
POSTCODE  character varying(6) NOT NULL,
CITY  character varying(128) NOT NULL,
ADDRESS  character varying(128) NOT NULL,
PROVINCE_ID  numeric (18,0) NOT NULL
);

create table DSE_PROVINCE 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(64) NOT NULL
);

create table DSE_STATUS 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(64) NOT NULL
);

create table DSG_ACCOUNT_NUMBER 
(
ACCOUNT_NUMBER  character varying(50) NOT NULL,
ACCOUNT_NAME  character varying(100),
ID  INTEGER,
NEEDDESCRIPTION  INTEGER,
DISCRIMINATOR  INTEGER,
VISIBLE_IN_SYSTEM  BOOLEAN NOT NULL,
FLAG_1  BOOLEAN NOT NULL
);

create table DSG_BUDGET 
(
CENTRUM_KOSZTOW_ID  INTEGER NOT NULL,
BUDGET  double precision  NOT NULL
);

create table DSG_CENTRUM_KOSZTOW 
(
ID  INTEGER NOT NULL,
NAME  character varying(100),
SYMBOL  character varying(50),
USERNAME  character varying(62),
DIVISIONGUID  character varying(62),
NEEDDESCRIPTION  INTEGER,
DEF_SIMPLE_ACC  BOOLEAN,
IPOWERED  BOOLEAN,
DEFAULT_ACCOUNT_NUMBER_ID  INTEGER,
IS_AUTO_ADDED  BOOLEAN NOT NULL,
AVAILABLE  INTEGER,
IS_CONTRACT_NEEDED  BOOLEAN,
SYMBOL_2  character varying(50)
);

create table DSG_CENTRUM_KOSZTOW_FAKTURY 
(
ID  INTEGER NOT NULL,
CENTRUMID  INTEGER,
CENTRUMCODE  character varying(100),
ACCOUNTNUMBER  character varying(50),
AMOUNT  double precision ,
DOCUMENTID  numeric (18,0),
ACCEPTINGCENTRUMID  INTEGER,
SUBGROUPID  INTEGER,
STORAGEPLACE  character varying(300),
CUSTOMSAGENCYID  INTEGER,
LOKALIZACJA  INTEGER,
CLASS_TYPE_ID  INTEGER,
CONNECTED_TYPE_ID  INTEGER,
REMARK_ID  INTEGER,
VAT_TYPE_ID  INTEGER,
VAT_TAX_TYPE_ID  INTEGER,
amountUsed double precision
);

create table DSG_NORMAL_DOCKIND 
(
DOCUMENT_ID  numeric (18,0) NOT NULL
);

create table DSG_ORDER_OBJECT 
(
OBJECT_ID  numeric (18,0),
ORDER_ID  numeric (18,0),
OBEJCT_TYPE  character varying(300)
);

create table DSO_AA_DOCUMENT 
(
ID  INTEGER NOT NULL,
OFFICENUMBER  INTEGER,
OFFICENUMBERSYMBOL  character varying(20),
OFFICENUMBERYEAR  INTEGER,
DOCUMENTDATE  TIMESTAMP,
POSTALREGNUMBER  character varying(20),
CLERK  character varying(62),
ASSIGNEDDIVISION  character varying(62),
CREATINGUSER  character varying(62) NOT NULL,
INDOCUMENT  INTEGER,
CASEDOCUMENTID  character varying(84),
CASE_ID  INTEGER,
SENDER  INTEGER,
ZPODATE  TIMESTAMP,
BARCODE  character varying(25),
CANCELLED  BOOLEAN NOT NULL,
CANCELLATIONREASON  character varying(80),
PREPSTATE  character varying(15),
CANCELLEDBYUSERNAME  character varying(62),
CURRENTASSIGNMENTGUID  character varying(62),
CURRENTASSIGNMENTUSERNAME  character varying(62),
CURRENTASSIGNMENTACCEPTED  BOOLEAN,
DELIVERY  INTEGER,
RETURNREASON  INTEGER,
RETURNED  BOOLEAN NOT NULL,
SUMMARY  character varying(80) NOT NULL,
ARCHIVED  BOOLEAN NOT NULL,
INTERNALDOCUMENT  BOOLEAN NOT NULL,
JOURNAL_ID  INTEGER,
DIVISIONGUID  character varying(62),
STAMPFEE  INTEGER
);

create table DSO_ADNOTATION 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(80)
);

create table DSO_ASSIGNMENT_OBJECTIVE 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(80) NOT NULL,
OFFICESTATUS  character varying(20) NOT NULL,
DOCUMENTTYPE  character varying(10) NOT NULL
);

create table DSO_BARCODE_POOL 
(
ID  numeric (18,0) NOT NULL,
PREFIX  character varying(5) NOT NULL,
START_BARCODE  numeric (18,0) NOT NULL,
END_BARCODE  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
IS_ACTIVE  BOOLEAN NOT NULL,
CLOSE_TIME  TIMESTAMP,
CURRENT_POINTER  numeric (18,0) NOT NULL
);

create table DSO_BARCODE_RANGE 
(
ID  numeric (18,0) NOT NULL,
POOL_ID  numeric (18,0),
START_BARCODE  numeric (18,0),
END_BARCODE  numeric (18,0),
CLIENT_TYPE  BOOLEAN NOT NULL,
CLIENT_NAME  character varying(50) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
STATUS  BOOLEAN NOT NULL,
CURRENT_POINTER  numeric (18,0),
ACCEPT_TIME  TIMESTAMP,
NUMBER_OF_BARCODES  numeric (18,0) NOT NULL,
PREFIX  character varying(5),
MASTER_RANGE_ID  numeric (18,0),
ASSIGNED_NUMBER_BARCODES  INTEGER,
REMARK  character varying(500),
TRANSPORTSERVICENAME  character varying(300)
);

create table DSO_CASE_PRIORITY 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(80) NOT NULL
);

create table DSO_CASE_STATUS 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(80) NOT NULL
);

create table DSO_CHANNEL_REQUEST 
(
ID  numeric (18,0) NOT NULL,
CONTENT  character varying(500),
INCOMING_DATE  DATE NOT NULL,
STATUS  BOOLEAN NOT NULL,
REQUEST_COMMENT  character varying(500),
PROCESS_DATE  DATE
);

create table DSO_COLLECTIVE_ASSIGNMENT 
(
ID  INTEGER NOT NULL,
OBJECTIVE  character varying(100) NOT NULL,
WFPROCESS  character varying(50) NOT NULL
);

create table DSO_CONTAINER 
(
ID  numeric (18,0) NOT NULL,
PARENT_ID  numeric (18,0),
DISCRIMINATOR  character varying(20) NOT NULL,
OFFICEID  character varying(84) NOT NULL,
OFFICEIDPREFIX  character varying(17),
OFFICEIDMIDDLE  character varying(50) NOT NULL,
OFFICEIDSUFFIX  character varying(17),
DIVISIONGUID  character varying(128) NOT NULL,
CLERK  character varying(62) NOT NULL,
AUTHOR  character varying(62) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
SEQUENCEID  INTEGER NOT NULL,
ARCHIVED  BOOLEAN NOT NULL,
RWA_ID  INTEGER NOT NULL,
REGISTRIES  INTEGER,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
NAME  character varying(128),
BARCODE  character varying(25),
CLOSED  BOOLEAN,
CASE_YEAR  INTEGER,
OPENDATE  DATE,
FINISHDATE  DATE,
DOCSEQUENCEID  INTEGER,
TRACKINGNUMBER  character varying(20),
TRACKINGPASSWORD  character varying(20),
PRIORITY_ID  INTEGER,
STATUS_ID  INTEGER,
PRECEDENT_ID  numeric (18,0),
DAYS  SMALLINT,
DESCRIPTION  character varying(200),
ARCHIVEDDATE  TIMESTAMP,
ARCHIVESTATUS  INTEGER,
PROTOKOLDATE  TIMESTAMP,
TITLE  character varying(200)
);

create table DSO_CONTAINER_AUDIT 
(
CONTAINER_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
PROPERTY  character varying(62),
USERNAME  character varying(62) NOT NULL,
DESCRIPTION  character varying(400) NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PRIORITY  INTEGER
);

create table DSO_CONTAINER_REMARKS 
(
CONTAINER_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
CONTENT  TEXT NOT NULL,
AUTHOR  character varying(62) NOT NULL
);

create table DSO_DOCUMENT_ASGN_HISTORY 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
SOURCEUSER  character varying(62) NOT NULL,
TARGETUSER  character varying(62),
SOURCEGUID  character varying(126) NOT NULL,
TARGETGUID  character varying(126) NOT NULL,
OBJECTIVE  character varying(128) NOT NULL,
ACCEPTED  BOOLEAN NOT NULL,
FINISHED  BOOLEAN NOT NULL,
PROCESSNAME  character varying(80),
TYPE  INTEGER,
STATUS  character varying(80),
CDATE  DATE,
PROCESSTYPE  INTEGER,
SUBSTITUTEUSER  character varying(62),
SUBSTITUTEGUID  character varying(126),
id numeric (18,0)
clerk 

);

create sequence dso_document_asgn_history_ID;
update dso_document_asgn_history set id = nextval('dso_document_asgn_history_ID');

ALTER TABLE dso_document_asgn_history  ALTER COLUMN id SET NOT NULL;
ALTER TABLE dso_document_asgn_history ADD PRIMARY KEY (ID);
alter table dso_document_asgn_history drop column posn;


create table dso_doc_asgn_history_targets
(
	type numeric (18,0),
	value character varying(62),
	aheId numeric (18,0),
	CONSTRAINT dso_document_asgn_history_fk2 FOREIGN KEY (aheId) REFERENCES dso_document_asgn_history  (ID)
);

create table DSO_DOCUMENT_AUDIT 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
PROPERTY  character varying(62),
USERNAME  character varying(62) NOT NULL,
DESCRIPTION  character varying(1000) NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PRIORITY  INTEGER
);

create table DSO_DOCUMENT_REMARKS 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP,
CONTENT  character varying(4000) NOT NULL,
AUTHOR  character varying(62) NOT NULL,
ID  numeric (18,0) NOT NULL
);

create table DSO_DOCUMENT_TO_LABEL 
(
ID  numeric (18,0) NOT NULL,
DOCUMENT_ID  numeric (18,0) NOT NULL,
LABEL_ID  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
ACTIONTIME  TIMESTAMP,
HIDDING  BOOLEAN,
USERNAME  character varying(100)
);

create table DSO_EMAIL_CHANNEL 
(
ID  INTEGER NOT NULL,
FOLDER_NAME  character varying(50),
KOLEJKA  character varying(100),
CTIME  TIMESTAMP NOT NULL,
WARUNKI  character varying(1000),
CONTENT_TYPE  character varying(50) NOT NULL,
PROTOCOL  character varying(50) NOT NULL,
HOSTNAME  character varying(100) NOT NULL,
USERNAME  character varying(50) NOT NULL,
PASS  character varying(100) NOT NULL,
PORT  INTEGER NOT NULL,
MBOX  character varying(50) NOT NULL,
IS_SSL  BOOLEAN NOT NULL,
IS_ENABLED  BOOLEAN NOT NULL
);

create table DSO_FILE_BIP_STATE 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
STAN  character varying(50) NOT NULL,
MODIFY_DATE  TIMESTAMP
);

create table DSO_INVOICES_CPV_CODES 
(
ID  INTEGER NOT NULL,
NAME  character varying(40),
CATEGORY  character varying(40)
);

create table DSO_INVOICES_DECRETATION 
(
ID  INTEGER NOT NULL,
NAME  character varying(100)
);

create table DSO_INVOICES_KIND 
(
ID  INTEGER NOT NULL,
NAME  character varying(40) NOT NULL,
DAYS  SMALLINT,
POSN  INTEGER
);

create table DSO_IN_DOCUMENT 
(
ID  numeric (18,0) NOT NULL,
DOCUMENTDATE  DATE,
STAMPDATE  DATE,
INCOMINGDATE  TIMESTAMP,
POSTALREGNUMBER  character varying(25),
SENDER  numeric (18,0),
NUMATTACHMENTS  INTEGER,
PACKAGEREMARKS  character varying(240),
OTHERREMARKS  character varying(240),
TRACKINGNUMBER  character varying(20),
TRACKINGPASSWORD  character varying(20),
SUBMITTOBIP  BOOLEAN,
CASE_ID  numeric (18,0),
CASEDOCUMENTID  character varying(84),
LOCATION  character varying(60),
KIND  INTEGER,
CLERK  character varying(62),
ASSIGNEDDIVISION  character varying(62),
CREATINGUSER  character varying(62) NOT NULL,
STATUS  INTEGER,
OFFICENUMBER  INTEGER,
OFFICENUMBERYEAR  INTEGER,
BARCODE  character varying(25),
OFFICENUMBERSYMBOL  character varying(20),
REFERENCEID  character varying(60),
MASTER_DOCUMENT_ID  numeric (18,0),
CANCELLED  BOOLEAN NOT NULL,
CANCELLATIONREASON  character varying(80),
CANCELLEDBYUSERNAME  character varying(62),
CURRENTASSIGNMENTGUID  character varying(62),
CURRENTASSIGNMENTUSERNAME  character varying(62),
CURRENTASSIGNMENTACCEPTED  BOOLEAN,
DELIVERY  INTEGER,
OUTGOINGDELIVERY  INTEGER,
SUMMARY  character varying(254) NOT NULL,
ARCHIVED  BOOLEAN NOT NULL,
JOURNAL_ID  numeric (18,0),
DIVISIONGUID  character varying(62),
BOK  BOOLEAN NOT NULL,
REPLYUNNECESSARY  BOOLEAN,
ORIGINAL  BOOLEAN NOT NULL,
DAILYOFFICENUMBER  INTEGER,
recipientUser  character varying(62)
);

create table DSO_IN_DOCUMENT_DELIVERY 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(40) NOT NULL
);

create table DSO_IN_DOCUMENT_KIND 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(40) NOT NULL,
DAYS  SMALLINT,
NAME_EN  character varying(40)
);

create table DSO_IN_DOCUMENT_KIND_REQ_ATT 
(
KIND_ID  INTEGER NOT NULL,
POSN  INTEGER,
TITLE  character varying(80) NOT NULL,
SID  character varying(32)
);

create table DSO_IN_DOCUMENT_STATUS 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(40) NOT NULL,
CN  character varying(20)
);

create table DSO_JOURNAL 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
DESCRIPTION  character varying(510) NOT NULL,
OWNERGUID  character varying(126),
JOURNALTYPE  character varying(62) NOT NULL,
SEQUENCEID  INTEGER NOT NULL,
SYMBOL  character varying(20),
AUTHOR  character varying(62) NOT NULL,
CTIME  TIMESTAMP,
ARCHIVED  BOOLEAN NOT NULL,
CLOSED  BOOLEAN NOT NULL,
CYEAR  INTEGER NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
DAILYSEQUENCEID  INTEGER,
OPENDAY  TIMESTAMP
);

create table DSO_JOURNAL_ENTRY 
(
DOCUMENTID  numeric (18,0) NOT NULL,
SEQUENCEID  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
ENTRYDATE  DATE NOT NULL,
JOURNAL_ID  numeric (18,0) NOT NULL,
DAILYSEQUENCEID  INTEGER
);

create table DSO_LABEL 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(30) NOT NULL,
DESCRIPTION  character varying(200),
PARENT_ID  numeric (18,0),
INACTION_TIME  numeric (18,0),
HIDDEN  BOOLEAN NOT NULL,
ANCESTOR  numeric (18,0),
MODIFIABLE  BOOLEAN NOT NULL,
DELETES_AFTER  BOOLEAN NOT NULL,
TYPE  character varying(50),
READ_RIGHTS  character varying(100),
WRITE_RIGHTS  character varying(100),
MODIFY_RIGHTS  character varying(100)
);

create table DSO_MAXUS_BIP_STATE 
(
CASE_ID  numeric (18,0) NOT NULL,
STAN  character varying(50) NOT NULL,
MODIFY_DATE  TIMESTAMP
);

create table DSO_ORDER 
(
ID  numeric (18,0) NOT NULL,
OFFICENUMBER  INTEGER,
OFFICENUMBERSYMBOL  character varying(20),
OFFICENUMBERYEAR  INTEGER,
DOCUMENTDATE  TIMESTAMP,
CLERK  character varying(62),
ASSIGNEDDIVISION  character varying(62),
CREATINGUSER  character varying(62) NOT NULL,
CASEDOCUMENTID  character varying(84),
CASE_ID  INTEGER,
BARCODE  character varying(25),
CANCELLED  BOOLEAN NOT NULL,
CANCELLATIONREASON  character varying(80),
CANCELLEDBYUSERNAME  character varying(62),
CURRENTASSIGNMENTGUID  character varying(62),
CURRENTASSIGNMENTUSERNAME  character varying(62),
CURRENTASSIGNMENTACCEPTED  BOOLEAN,
SUMMARY  character varying(80) NOT NULL,
ARCHIVED  BOOLEAN NOT NULL,
DIVISIONGUID  character varying(62),
STATUS  character varying(30),
PRIORITY  character varying(30),
FINISHDATE  TIMESTAMP,
DOCUMENT_ID  numeric (18,0),
DETAILEDDESCRIPTION  character varying(4000),
DAILYOFFICENUMBER  INTEGER
);

create table DSO_OUT_DOCUMENT 
(
ID  numeric (18,0) NOT NULL,
OFFICENUMBER  INTEGER,
OFFICENUMBERSYMBOL  character varying(20),
OFFICENUMBERYEAR  INTEGER,
DOCUMENTDATE  DATE,
POSTALREGNUMBER  character varying(25),
CLERK  character varying(62),
ASSIGNEDDIVISION  character varying(62),
CREATINGUSER  character varying(62) NOT NULL,
INDOCUMENT  numeric (18,0),
CASEDOCUMENTID  character varying(84),
CASE_ID  numeric (18,0),
SENDER  numeric (18,0),
ZPODATE  DATE,
BARCODE  character varying(25),
CANCELLED  BOOLEAN NOT NULL,
CANCELLATIONREASON  character varying(80),
PREPSTATE  character varying(15),
CANCELLEDBYUSERNAME  character varying(62),
CURRENTASSIGNMENTGUID  character varying(62),
CURRENTASSIGNMENTUSERNAME  character varying(62),
CURRENTASSIGNMENTACCEPTED  BOOLEAN,
DELIVERY  INTEGER,
RETURNREASON  INTEGER,
RETURNED  BOOLEAN NOT NULL,
SUMMARY  character varying(254) NOT NULL,
ARCHIVED  BOOLEAN NOT NULL,
INTERNALDOCUMENT  BOOLEAN NOT NULL,
DIVISIONGUID  character varying(62),
JOURNAL_ID  numeric (18,0),
STAMPFEE  INTEGER,
REFERENCEID  character varying(60),
OFFICEDATE  TIMESTAMP,
MASTER_DOCUMENT_ID  INTEGER,
PRIORITY  boolean,
WEIGHTG  INTEGER,
WEIGHTKG  INTEGER,
DAILYOFFICENUMBER  INTEGER,
zpo boolean
);

create table DSO_OUT_DOCUMENT_DELIVERY 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(40) NOT NULL
);

create table DSO_OUT_DOCUMENT_RETREASON 
(
ID  INTEGER NOT NULL,
POSN  SMALLINT NOT NULL,
NAME  character varying(40) NOT NULL
);

create table DSO_PERSON 
(
ID  numeric (18,0) NOT NULL,
POSN  INTEGER,
DISCRIMINATOR  character varying(20) NOT NULL,
DOCUMENT_ID  numeric (18,0),
DICTIONARYTYPE  character varying(10),
DICTIONARYGUID  character varying(62),
ANONYMOUS  BOOLEAN NOT NULL,
TITLE  character varying(30),
FIRSTNAME  character varying(50),
LASTNAME  character varying(50),
ORGANIZATION  character varying(50),
ORGANIZATIONDIVISION  character varying(50),
STREET  character varying(50),
ZIP  character varying(14),
LOCATION  character varying(50),
PESEL  character varying(11),
NIP  character varying(15),
REGON  character varying(15),
COUNTRY  character varying(3),
EMAIL  character varying(50),
FAX  character varying(30),
REMARKS  character varying(200),
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PERSONGROUP  character varying(50),
IDENTIFIKATOREPUAP  character varying(50),
ADRESSKRYTKIEPUAP  character varying(50),
ZGODANAWYSYLKE  BOOLEAN,
BASEPERSONID  numeric (18,0)
);

create table DSO_ROLE 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(126) NOT NULL
);

create table DSO_ROLE_DIVISIONS 
(
ROLE_ID  numeric (18,0) NOT NULL,
DIVISION_GUID  character varying(255)
);

create table DSO_ROLE_PERMISSIONS 
(
ROLE_ID  numeric (18,0) NOT NULL,
PERMISSION_NAME  character varying(255)
);

create table DSO_ROLE_USERNAMES 
(
ROLE_ID  numeric (18,0) NOT NULL,
USERNAME  character varying(255),
SOURCE  character varying(200)
);

create table DSO_RWA 
(
ID  INTEGER NOT NULL,
DIGIT1  CHAR(1) NOT NULL,
DIGIT2  CHAR(1),
DIGIT3  CHAR(1),
DIGIT4  CHAR(1),
DIGIT5  CHAR(1),
RWALEVEL  INTEGER NOT NULL,
ACHOME  character varying(5),
ACOTHER  character varying(5),
DESCRIPTION  character varying(300) NOT NULL,
REMARKS  character varying(512),
LPARAM  character varying(200),
WPARAM  numeric (18,0),
ISACTIVE  CHAR(1)
);

create table DSO_SCHOOL 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(50) NOT NULL,
ZIP  character varying(14),
LOCATION  character varying(50),
STREET  character varying(50)
);

create table DSO_USER_TO_BRIEFCASE 
(
ID  INTEGER NOT NULL,
CONTAINERID  numeric (18,0) NOT NULL,
CONTAINEROFFICEID  character varying(84) NOT NULL,
USERID  numeric (18,0) NOT NULL,
USERNAME  character varying(62),
USERFIRSTNAME  character varying(62),
USERLASTNAME  character varying(62),
CANVIEW  BOOLEAN NOT NULL,
CANCREATE  BOOLEAN NOT NULL,
CANMODIFY  BOOLEAN NOT NULL,
CANPRINT  BOOLEAN NOT NULL,
CANVIEWCASES  BOOLEAN NOT NULL,
CANADDCASESTOBRIEFCASE  BOOLEAN NOT NULL
);

create table DSR_CNS_DECISION_ARCHIVE 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(80),
ADDRESS  character varying(80),
OBJECT_KIND  character varying(40),
PROPOSITION_NO  character varying(80),
PROPOSITION_IN_DATE  TIMESTAMP,
DECISION_NO  character varying(80),
DECISION_DATE  TIMESTAMP,
REFERENCE  character varying(300),
DECISION_AFTER  character varying(300),
NOTICE  character varying(300)
);

create table DSR_CON_CONTRACT 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
SEQUENCEID  INTEGER NOT NULL,
CREATE_YEAR  INTEGER NOT NULL,
CONTRACTDATE  DATE NOT NULL,
CONTRACTKIND  character varying(20) NOT NULL,
VOUCHERNO  character varying(50),
GROSS  numeric (18,0) NOT NULL,
DESCRIPTION  character varying(160) NOT NULL,
STARTTERM  DATE,
ENDTERM  DATE,
VENDOR_ID  numeric (18,0) NOT NULL,
DELETED  BOOLEAN NOT NULL,
REMARKS  character varying(512),
CONTRACTSTATUS  character varying(25),
LPARAM  character varying(200),
WPARAM  numeric (18,0),
DOCUMENT_ID  numeric (18,0),
AUTHOR  character varying(62),
CONTRACTNO  character varying(50),
PERIODF_TO  DATE,
PERIODF_FROM  DATE
);

create table DSR_CON_CONTRACT_AUDIT 
(
CONTRACT_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
PROPERTY  character varying(62),
USERNAME  character varying(62) NOT NULL,
DESCRIPTION  character varying(200) NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PRIORITY  INTEGER
);

create table DSR_CON_CONTRACT_HISTORY 
(
ID  numeric (18,0) NOT NULL,
CONTRACT_ID  numeric (18,0) NOT NULL,
CHANGE_DATE  TIMESTAMP NOT NULL,
USER_ID  numeric (18,0) NOT NULL,
DESCRIPTION  character varying(1000)
);

create table DSR_CON_VENDOR 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(160) NOT NULL,
ZIP  character varying(14),
STREET  character varying(50),
LOCATION  character varying(50),
NIP  character varying(15),
REGON  character varying(15),
COUNTRY  character varying(2),
PHONE  character varying(30),
FAX  character varying(30),
LPARAM  character varying(200),
WPARAM  numeric (18,0)
);

create table DSR_ENTRY_TO_CASE 
(
ENTRY_ID  numeric (18,0) NOT NULL,
CASE_ID  numeric (18,0) NOT NULL
);

create table DSR_INV_INVOICE 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
SEQUENCEID  INTEGER NOT NULL,
CREATE_YEAR  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
INVOICEDATE  DATE NOT NULL,
PAYMENTDATE  DATE NOT NULL,
SALEDATE  DATE,
TERMSOFPAYMENT  character varying(20) NOT NULL,
INVOICENO  character varying(50) NOT NULL,
GROSS  numeric (18,0) NOT NULL,
NET  numeric (18,0) NOT NULL,
DESCRIPTION  character varying(160) NOT NULL,
CONTRACTNO  character varying(50),
ACCEPTINGUSER  character varying(62),
ACCEPTDATE  DATE,
ACCEPTRETURNDATE  DATE,
BUDGETARYDESCRIPTION  character varying(50),
RELAYEDTO  character varying(80),
RB_DATE  DATE,
VENDOR_ID  numeric (18,0) NOT NULL,
DELETED  BOOLEAN NOT NULL,
REMARKS  character varying(512),
DIVISION_GUID  character varying(62),
LPARAM  character varying(200),
DOCUMENT_ID  numeric (18,0),
CONTRACTID  numeric (18,0),
WPARAM  numeric (18,0),
KIND  INTEGER,
CANCELLED  BOOLEAN,
VAT  INTEGER,
ARCHIVE  BOOLEAN,
ACCEPTINGUSERDIVISIONGUID  character varying(50),
DECRETATION  character varying(50),
CPV  character varying(30)
);

create table DSR_INV_INVOICE_AUDIT 
(
INVOICE_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
PROPERTY  character varying(62),
USERNAME  character varying(62) NOT NULL,
DESCRIPTION  character varying(200) NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PRIORITY  INTEGER
);

create table DSR_INV_VENDOR 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(160) NOT NULL,
ZIP  character varying(14),
STREET  character varying(50),
LOCATION  character varying(50),
NIP  character varying(15),
REGON  character varying(15),
COUNTRY  character varying(2),
PHONE  character varying(30),
FAX  character varying(30),
LPARAM  character varying(200),
WPARAM  numeric (18,0)
);

create table DSR_REGISTRY 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(100) NOT NULL,
SHORTNAME  character varying(30) NOT NULL,
TABLENAME  character varying(20) NOT NULL,
ENABLED  BOOLEAN NOT NULL,
REQUIREDRWA  character varying(5),
CONTENT  BYTEA,
CN  character varying(20),
REGTYPE  character varying(20)
);

create table DSR_REGISTRY_AUDIT 
(
ENTRY_ID  numeric (18,0) NOT NULL,
POSN  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
PROPERTY  character varying(62),
USERNAME  character varying(62) NOT NULL,
DESCRIPTION  character varying(1000) NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
PRIORITY  INTEGER
);

create table DSR_REGISTRY_ENTRY 
(
ID  numeric (18,0) NOT NULL,
REGISTRY_ID  numeric (18,0) NOT NULL,
CREATOR  character varying(62) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
HVERSION  INTEGER NOT NULL,
SEQUENCENUMBER  INTEGER,
REGYEAR  INTEGER,
INDOCUMENT_ID  numeric (18,0)
);

create table DSW_ACTIVITY 
(
ID  numeric (18,0) NOT NULL,
STATE  character varying(30) NOT NULL,
LASTSTATETIME  TIMESTAMP NOT NULL,
ACTIVITYDEFINITIONID  character varying(60) NOT NULL,
ACTIVITY_KEY  character varying(30) NOT NULL,
NAME  character varying(200) NOT NULL,
DESCRIPTION  character varying(200),
PROCESS_ID  numeric (18,0) NOT NULL
);

create table DSW_ACTIVITY_CONTEXT 
(
ACTIVITY_ID  numeric (18,0) NOT NULL,
PARAM_NAME  character varying(50) NOT NULL,
PARAM_VALUE  character varying(2048) NOT NULL,
DATATYPE_ID  numeric (18,0) NOT NULL
);

create table DSW_ASSIGNMENT 
(
ID  numeric (18,0) NOT NULL,
ACCEPTED  BOOLEAN NOT NULL,
ACTIVITY_ID  numeric (18,0) NOT NULL,
RESOURCE_ID  numeric (18,0) NOT NULL,
RESOURCE_COUNT  INTEGER NOT NULL
);

create table DSW_BACKUP_PARTICIPANT_MAPPING 
(
PARTICIPANT_ID  character varying(60) NOT NULL,
RESOURCE_ID  numeric (18,0) NOT NULL
);

create table DSW_BACKUP_RESOURCE 
(
ID  numeric (18,0) NOT NULL,
RES_KEY  character varying(62) NOT NULL,
RES_NAME  character varying(62),
EMAIL  character varying(62)
);

create table DSW_DATATYPE 
(
ID  numeric (18,0) NOT NULL,
TYPENAME  character varying(30) NOT NULL,
BASICTYPE  character varying(30),
LOWERINDEX  INTEGER,
UPPERINDEX  INTEGER,
MEMBERTYPE  numeric (18,0)
);

create table DSW_DATATYPE_ENUM_VALUES 
(
DATATYPE_ID  numeric (18,0) NOT NULL,
POSN  INTEGER,
ENUM_VALUE  character varying(100) NOT NULL
);

create table DSW_JBPM_TASKLIST 
(
ID  numeric (18,0) NOT NULL,
ACTIVITY_KEY  character varying(250),
ACCEPTED  boolean,
DOCUMENT_ID  numeric (18,0),
OFFICE_NUMBER_YEAR  INTEGER,
LAST_STATE_TIME  TIMESTAMP,
DESCRIPTION  character varying(200),
PERSON_FIRSTNAME  character varying(50),
PERSON_LASTNAME  character varying(50),
PERSON_ORGANIZATION  character varying(50),
PERSON_STREET  character varying(50),
PERSON_LOCATION  character varying(50),
LAST_USER  character varying(62),
OBJECTIVE  character varying(512),
PROCESS_NAME  character varying(200),
SUMMARY  character varying(254),
REFERENCE_ID  character varying(62),
DOCUMENT_SOURCE  character varying(80),
ASSIGNED_RESOURCE  character varying(62),
DEADLINE_TIME  TIMESTAMP,
DEADLINE_REMARK  character varying(200),
DEADLINE_AUTHOR  character varying(62),
DOCUMENT_TYPE  character varying(62),
RCPT_FIRSTNAME  character varying(50),
RCPT_LASTNAME  character varying(50),
RCPT_ORGANIZATION  character varying(50),
RCPT_STREET  character varying(50),
RCPT_LOCATION  character varying(50),
CASE_ID  numeric (18,0),
CASE_OFFICE_ID  character varying(84),
DOCUMENT_AUTHOR  character varying(62),
REMINDER_DATE  TIMESTAMP,
DOCUMENT_DELIVERY  character varying(40),
DOCUMENT_PREP_STATE  character varying(20),
PROCESS_STATUS  INTEGER,
DOCUMENT_CLERK  character varying(62),
ANSWER_COUNTER  INTEGER,
INCOMING_DATE  TIMESTAMP,
DOCKIND_NAME  character varying(100),
RCV_DATE  TIMESTAMP,
DIVISION_GUID  character varying(62),
DOCUMENT_CTIME  TIMESTAMP,
DOCUMENT_CDATE  TIMESTAMP,
DOCKIND_STATUS  character varying(200),
DOCKIND_NUMER_DOKUMENTU  character varying(30),
DOCKIND_KWOTA  numeric (18,0),
DOCKIND_KATEGORIA  character varying(30),
LAST_REMARK  character varying(240),
DOCKIND_BUSINESS_ATR_1  character varying(30),
DOCKIND_BUSINESS_ATR_2  character varying(30),
DOCKIND_BUSINESS_ATR_3  character varying(30),
DOCUMENT_DATE  TIMESTAMP,
BACKUP_PERSON  boolean,
DOCKIND_BUSINESS_ATR_4  character varying(30),
CASE_DEADLINE_DATE  DATE,
ISANYIMAGEINATTACHMENTS  BOOLEAN,
LABELHIDDEN  BOOLEAN,
OFFICE_NUMBER  numeric (18,0),
DAILYOFFICENUMBER  INTEGER,
DOCKIND_BUSINESS_ATR_5  character varying(30),
DOCKIND_BUSINESS_ATR_6  character varying(30),
NEW_PROCESS_STATUS  character varying(255),
ISANYATTACHMENTS  BOOLEAN,
markerClass character varying(30),
resource_count INTEGER  default 1 not null
);

create table DSW_PACKAGE 
(
ID  numeric (18,0) NOT NULL,
PACKAGEID  character varying(100) NOT NULL,
HASHID  character varying(100) NOT NULL,
XMLCONTENT  TEXT
);

create table DSW_PARTICIPANT_MAPPING 
(
PARTICIPANT_ID  character varying(60) NOT NULL,
RESOURCE_ID  numeric (18,0) NOT NULL
);

create table DSW_PROCESS 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(200) NOT NULL,
DESCRIPTION  character varying(200),
STATE  character varying(30) NOT NULL,
LASTSTATETIME  TIMESTAMP NOT NULL,
PROCESS_KEY  character varying(50) NOT NULL,
PACKAGEID  character varying(50) NOT NULL,
PACKAGE_ID  numeric (18,0) NOT NULL,
PROCESSDEFINITIONID  character varying(50) NOT NULL,
DEADLINETIME  TIMESTAMP,
DEADLINEAUTHOR  character varying(62),
DEADLINEREMARK  character varying(200),
DEADLINELASTREPORT  TIMESTAMP
);

create table DSW_PROCESS_ACTIVITIES 
(
PROCESS_ID  numeric (18,0) NOT NULL,
POSN  INTEGER,
ACTIVITY_ID  numeric (18,0) NOT NULL
);

create table DSW_PROCESS_CONTEXT 
(
PROCESS_ID  numeric (18,0) NOT NULL,
PARAM_NAME  character varying(50) NOT NULL,
PARAM_VALUE  character varying(2048) NOT NULL,
DATATYPE_ID  numeric (18,0) NOT NULL,
PARAM_VALUE_INT  INTEGER
);

create table DSW_PROCESS_COORDINATOR 
(
ID  numeric (18,0) NOT NULL,
DOCUMENTKINDCN  character varying(20) NOT NULL,
USERNAME  character varying(30),
CHANNELCN  character varying(20),
GUID  character varying(100)
);

create table DSW_RESOURCE 
(
ID  numeric (18,0) NOT NULL,
RES_KEY  character varying(62) NOT NULL,
RES_NAME  character varying(62),
EMAIL  character varying(62)
);

create table DSW_TASKLIST 
(
ID  numeric (18,0) NOT NULL,
DSW_ASSIGNMENT_ID  numeric (18,0),
DSW_ASSIGNMENT_ACCEPTED  BOOLEAN,
DSO_DOCUMENT_ID  numeric (18,0),
DSO_DOCUMENT_OFFICENUMBERYEAR  INTEGER,
DSW_ACTIVITY_LASTSTATETIME  TIMESTAMP,
DSW_ACTIVITY_NAME  character varying(200),
DSO_PERSON_FIRSTNAME  character varying(50),
DSO_PERSON_LASTNAME  character varying(50),
DSO_PERSON_ORGANIZATION  character varying(50),
DSO_PERSON_STREET  character varying(50),
DSO_PERSON_LOCATION  character varying(50),
DSW_ACTIVITY_CONTEXT_PARAM_VAL  character varying(200),
DSW_ACTIVITY_ACTIVITY_KEY  character varying(30),
DSO_DOCUMENT_SUMMARY  character varying(254),
ASSIGNED_USER  character varying(200),
DSO_DOCUMENT_REFERENCEID  character varying(62),
DSW_PROCESS_NAME  character varying(200),
DS_DOCUMENT_SOURCE  character varying(80),
DSW_PROCESS_PACKAGEID  character varying(50),
DSW_RESOURCE_RES_KEY  character varying(62),
DSW_PROCESS_DEADLINETIME  TIMESTAMP,
DSW_PROCESS_DEADLINEREMARK  character varying(200),
DSW_PROCESS_DEADLINEAUTHOR  character varying(62),
DOCUMENT_TYPE  character varying(62),
RCPT_FIRSTNAME  character varying(50),
RCPT_LASTNAME  character varying(50),
RCPT_ORGANIZATION  character varying(50),
RCPT_STREET  character varying(50),
RCPT_LOCATION  character varying(50),
CASE_ID  numeric (18,0),
CASE_OFFICE_ID  character varying(84),
EXTERNAL_WORKFLOW  BOOLEAN,
EXTERNAL_PROCESS_ID  INTEGER,
DOCUMENTAUTHOR  character varying(62),
EXTERNAL_TASK_ID  INTEGER,
REMINDERDATE  TIMESTAMP,
DOCUMENTDELIVERY  character varying(40),
DOCUMENTPREPSTATE  character varying(20),
PROCESSSTATUS  INTEGER,
DOCUMENTCLERK  character varying(62),
ANSWERCOUNTER  INTEGER,
INCOMINGDATE  TIMESTAMP,
DSW_PROCESS_PROCESSDEF  character varying(50),
DSW_PROCESS_CONTEXT_PARAM_VAL  character varying(200),
DOCKINDNAME  character varying(30),
RCVDATE  DATE,
DIVISIONGUID  character varying(62),
DOCUMENTCTIME  TIMESTAMP,
DOCUMENTCDATE  DATE,
DOCKINDSTATUS  character varying(30),
DOCKINDNUMERDOKUMENTU  character varying(30),
DOCKINDKWOTA  double precision ,
DOCKINDKATEGORIA  character varying(30),
DOCKINDBUSINESSUNIT  character varying(30),
LASTREMARK  character varying(240),
DOCKINDBUSINESSATR1  character varying(30),
DOCKINDBUSINESSATR2  character varying(30),
DOCKINDBUSINESSATR3  character varying(30),
DOCUMENTDATE  TIMESTAMP,
BACKUPPERSON  BOOLEAN,
DOCKIND_BUSINESS_ATR_4  character varying(30),
LABELHIDDEN  BOOLEAN,
CASE_DEADLINE_DATE  DATE,
ISANYIMAGEINATTACHMENTS  BOOLEAN,
MARKERCLASS  character varying(50),
DSO_DOCUMENT_OFFICENUMBER  numeric (18,0),
DAILYOFFICENUMBER  INTEGER,
RESOURCE_COUNT  INTEGER NOT NULL,
DOCKIND_BUSINESS_ATR_5  character varying(30),
DOCKIND_BUSINESS_ATR_6  character varying(30),
ISANYATTACHMENTS  BOOLEAN
);

create table DSW_TASK_HISTORY_ENTRY 
(
ID  numeric (18,0) NOT NULL,
DOCUMENTID  numeric (18,0) NOT NULL,
USERNAME  character varying(62),
DIVISIONGUID  character varying(62),
RECEIVEDATE  DATE,
ACCEPTDATE  DATE,
FINISHDATE  DATE,
RECEIVETIME  TIMESTAMP,
ACCEPTTIME  TIMESTAMP,
FINISHTIME  TIMESTAMP,
ACTIVITYKEY  character varying(30),
EXTERNALWORKFLOW  BOOLEAN
);

create table DS_ABSENCE 
(
ID  numeric (18,0) NOT NULL,
YEAR  SMALLINT NOT NULL,
START_DATE  TIMESTAMP NOT NULL,
END_DATE  TIMESTAMP NOT NULL,
DAYS_NUM  BOOLEAN NOT NULL,
INFO  TEXT,
CTIME  TIMESTAMP NOT NULL,
EMPLOYEE_ID  numeric (18,0) NOT NULL,
USER_ID  numeric (18,0),
ABSENCE_TYPE  character varying(20) NOT NULL,
MTIME  TIMESTAMP
);

create table DS_ABSENCE_TYPE 
(
CODE  character varying(20) NOT NULL,
NAME  character varying(150) NOT NULL,
INFO  TEXT,
FLAG  BOOLEAN NOT NULL
);

create table DS_ACCEPTANCE_CONDITION 
(
ID  numeric (18,0) NOT NULL,
CN  character varying(20) NOT NULL,
FIELDVALUE  character varying(100),
USERNAME  character varying(62),
DIVISIONGUID  character varying(62),
MAXIMUMAMOUNTFORACCEPTANCE  double precision 
);

create table DS_ACCEPTANCE_DIVISION 
(
ID  numeric (18,0) NOT NULL,
GUID  character varying(62),
NAME  character varying(62),
CODE  character varying(20),
DIVISIONTYPE  character varying(20),
PARENT_ID  numeric (18,0),
OGOLNA  BOOLEAN
);

create table DS_ADDRESS 
(
NAME  character varying(50) NOT NULL,
ORGANIZATION  character varying(100) NOT NULL,
STREET  character varying(100) NOT NULL,
ZIP_CODE  character varying(10) NOT NULL,
CITY  character varying(50) NOT NULL,
REGION  character varying(50) NOT NULL,
ID  numeric (18,0) NOT NULL,
IS_HEAD_OFFICE  BOOLEAN NOT NULL,
IMAGE_ID  numeric (18,0)
);

create table DS_AGENT_USER_STATS 
(
BROWSER_NAME  character varying(16),
BROWSER_ID  character varying(16),
WIDTH  INTEGER,
HEIGHT  INTEGER
);

create table DS_ANONYMOUS_ACCESS_TICKET 
(
ID  numeric (18,0) NOT NULL,
DOCUMENT_ID  numeric (18,0),
ATTACHMENT_ID  numeric (18,0),
ATTACHMENT_REVISION_ID  numeric (18,0),
CODE  character varying(200) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
VALID_TILL  TIMESTAMP,
ACCESS_COUNT  numeric (18,0) NOT NULL,
MAX_ACCESS  numeric (18,0) NOT NULL,
CREATOR  character varying(50) NOT NULL,
RETURN_METHOD  character varying(50)
);

create table DS_ANY_RESOURCE 
(
ID  numeric (18,0) NOT NULL,
CN  character varying(30),
NAME  character varying(100),
CALENDARID  numeric (18,0)
);

create table DS_ATTACHMENT 
(
ID  numeric (18,0) NOT NULL,
TITLE  character varying(254) NOT NULL,
DESCRIPTION  character varying(510) NOT NULL,
CTIME  TIMESTAMP,
AUTHOR  character varying(62) NOT NULL,
DOCUMENT_ID  numeric (18,0),
POSN  INTEGER,
DELETED  BOOLEAN NOT NULL,
BARCODE  character varying(25),
LPARAM  character varying(200),
WPARAM  numeric (18,0),
CN  character varying(20),
TYPE  INTEGER,
ENTRY_ID  numeric (18,0),
field_cn character varying(200)

);

create table DS_ATTACHMENT_REVISION 
(
ID  numeric (18,0) NOT NULL,
REVISION  INTEGER NOT NULL,
CTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
ATTACHMENT_ID  numeric (18,0),
ORIGINALFILENAME  character varying(510),
CONTENTSIZE  INTEGER,
MIME  character varying(62),
CONTENTDATA  BYTEA,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
CONTENTREFID  numeric (18,0),
EDITABLE  BOOLEAN,
ONDISC  INTEGER,
ENCRYPTED  character varying(20),
VERIFICATIONCODE  character varying(100),
INDEX_STATUS_ID  BOOLEAN NOT NULL
);

create table DS_BACKUPUSER_TO_DIVISION 
(
USER_ID  INTEGER NOT NULL,
DIVISION_ID  INTEGER NOT NULL
);

create table DS_BILING 
(
ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62),
PHONE_TYPE  character varying(50) NOT NULL,
PHONE_NUMBER_FROM  character varying(25) NOT NULL,
PHONE_NUMBER_TO  character varying(25) NOT NULL,
TIME_PERIOD  INTEGER,
DATE_TIME  TIMESTAMP NOT NULL,
NETTO  numeric (18,0),
IMPULSES  INTEGER NOT NULL,
DIRECTION  character varying(100) NOT NULL,
GPRS  INTEGER,
BILINGNUMBER  character varying(60)
);

create table DS_BOOKMARK 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(64) NOT NULL,
TITLE  character varying(256),
TYPE  character varying(32),
KIND  INTEGER,
COLUMNS  character varying(1000),
LABELS  character varying(1000),
ACTIVE  SMALLINT NOT NULL,
OWNER  character varying(32),
ACTIVEB  BOOLEAN,
ASCENDING_  BOOLEAN,
SORT_FIELD  character varying(64),
TASK_COUNT  INTEGER
);

create table DS_BOX 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(100) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
OPENTIME  TIMESTAMP,
CLOSETIME  TIMESTAMP,
ISOPEN  BOOLEAN,
LINE  character varying(20),
DESCRIPTION  character varying(300)
);

create table DS_BOX_CHANGE_HISTORY 
(
ID  numeric (18,0) NOT NULL,
IDDOC  numeric (18,0),
BOXTONAME  character varying(100),
BOXFROMNAME  character varying(100),
CURDATE  TIMESTAMP
);

create table DS_BOX_LINE 
(
ID  numeric (18,0) NOT NULL,
LINE  character varying(20),
NAME  character varying(100)
);

create table DS_BOX_ORDER 
(
ID  numeric (18,0),
REMARK  character varying(300),
DOCUMENTID  numeric (18,0),
PRIORITY  INTEGER,
STATUS  INTEGER,
ORDERDATE  TIMESTAMP,
TRACENUMBER  character varying(50),
LOCATION  numeric (18,0),
ORDERKIND  INTEGER,
USERNAME  character varying(50),
DELIVERYKIND  BOOLEAN,
ORDERASPECT  character varying(20),
ORDERSENDDATE  TIMESTAMP,
ORDERRECEIVEDATE  TIMESTAMP,
ORDERRETURNDATE  TIMESTAMP,
ORDERRETURNCONFIRMDATE  TIMESTAMP,
TRANSPORTSERVICENAME  character varying(300)
);

create table DS_BOX_ORDER_HISTORY 
(
ID  numeric (18,0),
ORDERID  numeric (18,0),
VALUENAME  character varying(100),
value  character varying(300),
CTIME  TIMESTAMP,
USERNAME  character varying(50)
);

create table DS_CALENDAR 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(100),
TYPE  character varying(30),
OWNER  character varying(50)
);

create table DS_CALENDAR_EVENT 
(
ID  numeric (18,0) NOT NULL,
TIMEID  numeric (18,0),
CALENDARID  numeric (18,0),
CONFIRM  BOOLEAN,
ALLDAY  BOOLEAN,
STARTTIME  TIMESTAMP NOT NULL,
ENDTIME  TIMESTAMP NOT NULL,
PLACE  character varying(64),
DESCRIPTION  character varying(256),
PERIODIC  BOOLEAN NOT NULL,
PERIODKIND  INTEGER NOT NULL,
PERIODLENGTH  numeric (18,0),
PERIODEND  DATE,
PERIODPARENTID  numeric (18,0)
);

create table DS_CALENDAR_FREE_DAY 
(
ID  numeric (18,0) NOT NULL,
DAY_DATE  TIMESTAMP NOT NULL,
INFO  TEXT
);

create table DS_DATAMART 
(
DOCUMENT_ID  numeric (18,0),
PROCESS_CODE  character varying(30),
EVENT_CODE  character varying(40),
EVENT_DATE  TIMESTAMP,
CHANGE_FIELD_CN  character varying(50),
OLD_VALUE  character varying(200),
NEW_VALUE  character varying(200),
USERNAME  character varying(50),
SESSION  character varying(50),
OBJECT_ID  character varying(80),
EVENT_ID  numeric (18,0) NOT NULL
);

create table DS_DIVISION 
(
ID  numeric (18,0) NOT NULL,
GUID  character varying(62) NOT NULL,
NAME  character varying(100) NOT NULL,
CODE  character varying(20),
DIVISIONTYPE  character varying(20) NOT NULL,
PARENT_ID  numeric (18,0),
HIDDEN  BOOLEAN,
DESCRIPTION  character varying(120),
EXTERNALID  character varying(30)
);

create table DS_DOCTYPE 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(100) NOT NULL,
CN  character varying(20),
TABLENAME  character varying(20) NOT NULL,
ENABLED  BOOLEAN NOT NULL,
CONTENT  BYTEA
);

create table DS_DOCUMENT 
(
ID  numeric (18,0) NOT NULL,
TITLE  character varying(254) NOT NULL,
DESCRIPTION  character varying(510) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
MTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
EXTERNALID  character varying(126),
HVERSION  INTEGER NOT NULL,
FOLDER_ID  numeric (18,0),
DOCTYPE  numeric (18,0),
OFFICE  BOOLEAN NOT NULL,
DELETED  BOOLEAN NOT NULL,
FORCEARCHIVEPERMISSIONS  BOOLEAN NOT NULL,
BOX_ID  numeric (18,0),
BOXINGDATE  TIMESTAMP,
SOURCE  character varying(80),
LPARAM  character varying(200),
WPARAM  numeric (18,0),
BLOCKED  BOOLEAN,
DOCKIND_ID  numeric (18,0),
DUPLICATEDOC  INTEGER,
LASTREMARK  character varying(240),
FINISHUSER  character varying(20),
FINISHDATE  TIMESTAMP,
REPLIED_DOCUMENT_ID  numeric (18,0),
INDEX_STATUS_ID  smallint NOT NULL
);

create table DS_DOCUMENT_ACCEPTANCE 
(
ID  numeric (18,0) NOT NULL,
DOCUMENT_ID  numeric (18,0) NOT NULL,
ACCEPTANCETIME  TIMESTAMP NOT NULL,
USERNAME  character varying(62) NOT NULL,
ACCEPTANCECN  character varying(20) NOT NULL,
OBJECTID  numeric (18,0),
AUTOMATIC  BOOLEAN
);

create table DS_DOCUMENT_CHANGELOG 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
WHAT  character varying(100) NOT NULL
);

create table DS_DOCUMENT_FLAGS 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
FL1  BOOLEAN NOT NULL,
FL2  BOOLEAN NOT NULL,
FL3  BOOLEAN NOT NULL,
FL4  BOOLEAN NOT NULL,
FL5  BOOLEAN NOT NULL,
FL6  BOOLEAN NOT NULL,
FL7  BOOLEAN NOT NULL,
FL8  BOOLEAN NOT NULL,
FL9  BOOLEAN NOT NULL,
FL10  BOOLEAN NOT NULL,
FL1MTIME  TIMESTAMP,
FL2MTIME  TIMESTAMP,
FL3MTIME  TIMESTAMP,
FL4MTIME  TIMESTAMP,
FL5MTIME  TIMESTAMP,
FL6MTIME  TIMESTAMP,
FL7MTIME  TIMESTAMP,
FL8MTIME  TIMESTAMP,
FL9MTIME  TIMESTAMP,
FL10MTIME  TIMESTAMP,
FL1AUTHOR  character varying(62),
FL2AUTHOR  character varying(62),
FL3AUTHOR  character varying(62),
FL4AUTHOR  character varying(62),
FL5AUTHOR  character varying(62),
FL6AUTHOR  character varying(62),
FL7AUTHOR  character varying(62),
FL8AUTHOR  character varying(62),
FL9AUTHOR  character varying(62),
FL10AUTHOR  character varying(62)
);

create table DS_DOCUMENT_KIND 
(
ID  numeric (18,0) NOT NULL,
HVERSION  INTEGER NOT NULL,
NAME  character varying(100) NOT NULL,
TABLENAME  character varying(20) NOT NULL,
ENABLED  BOOLEAN NOT NULL,
CONTENT  BYTEA,
CN  character varying(20),
LOGIC  character varying(120),
ADDDATE  TIMESTAMP
);

create table DS_DOCUMENT_LOCK 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
EXPIRATION  TIMESTAMP,
LOCKMODE  character varying(20) NOT NULL
);

create table DS_DOCUMENT_PERMISSION 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
SUBJECT  character varying(62) NOT NULL,
SUBJECTTYPE  character varying(14) NOT NULL,
NAME  character varying(14) NOT NULL,
NEGATIVE  BOOLEAN NOT NULL
);

create table DS_DOCUMENT_SIGNATURE 
(
ID  numeric (18,0) NOT NULL,
DOCUMENT_ID  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
DOCCONTENT  BYTEA,
SIGCONTENT  BYTEA,
CORRECTLYVERIFIED  BOOLEAN,
XMLCONTENT  BYTEA,
TYPE  INTEGER
);

create table DS_DOCUMENT_USER_FLAGS 
(
DOCUMENT_ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
FL1  BOOLEAN NOT NULL,
FL2  BOOLEAN NOT NULL,
FL3  BOOLEAN NOT NULL,
FL4  BOOLEAN NOT NULL,
FL5  BOOLEAN NOT NULL,
FL6  BOOLEAN NOT NULL,
FL7  BOOLEAN NOT NULL,
FL8  BOOLEAN NOT NULL,
FL9  BOOLEAN NOT NULL,
FL10  BOOLEAN NOT NULL,
FL1MTIME  TIMESTAMP,
FL2MTIME  TIMESTAMP,
FL3MTIME  TIMESTAMP,
FL4MTIME  TIMESTAMP,
FL5MTIME  TIMESTAMP,
FL6MTIME  TIMESTAMP,
FL7MTIME  TIMESTAMP,
FL8MTIME  TIMESTAMP,
FL9MTIME  TIMESTAMP,
FL10MTIME  TIMESTAMP,
FL1AUTHOR  character varying(62),
FL2AUTHOR  character varying(62),
FL3AUTHOR  character varying(62),
FL4AUTHOR  character varying(62),
FL5AUTHOR  character varying(62),
FL6AUTHOR  character varying(62),
FL7AUTHOR  character varying(62),
FL8AUTHOR  character varying(62),
FL9AUTHOR  character varying(62),
FL10AUTHOR  character varying(62)
);

create table DS_DOCUMENT_WATCH 
(
ID  numeric (18,0) NOT NULL,
DOCUMENT_ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
WATCH_TYPE  INTEGER NOT NULL
);

create table DS_DOCUSAFE 
(
VAR_NAME  character varying(100) NOT NULL,
VAR_VALUE  TEXT NOT NULL
);

create table DS_ELECTRONIC_SIGNATURE 
(
ID  numeric (18,0) NOT NULL,
FK_ID  numeric (18,0) NOT NULL,
FK_TYPE_ID  BOOLEAN NOT NULL,
PERSISTENCE_TYPE_ID  BOOLEAN NOT NULL,
DATE_  TIMESTAMP NOT NULL,
SIGNED_FILE  BYTEA,
SIGNATURE_FILE  BYTEA,
URI  character varying(200),
DOCUMENT_ID  numeric (18,0)
);

create table DS_EMPLOYEE_CARD 
(
ID  numeric (18,0) NOT NULL,
KPX  character varying(50) NOT NULL,
COMPANY  character varying(100) NOT NULL,
EMPLOYMENT_START_DATE  TIMESTAMP NOT NULL,
EMPLOYMENT_END_DATE  TIMESTAMP,
POSITION  character varying(255) NOT NULL,
DEPARTMENT  character varying(100) NOT NULL,
INFO  TEXT,
USER_ID  character varying(62),
DIVISION  character varying(100),
IS_T188KP  INTEGER,
IS_TUJ  INTEGER
);

create table DS_EPUAP_ASSIGNMENT_HISTORY 
(
DATA_DEKRETACJI  TIMESTAMP NOT NULL,
DEKRETUJACY  character varying(60) NOT NULL,
ZDEKTEROWANY  character varying(60) NOT NULL,
OPIS  character varying(200) NOT NULL
);

create table DS_EPUAP_EXPORT_DOCUMENT 
(
ID  numeric (18,0),
DOCUMENTID  numeric (18,0),
ODPOWIEDZ  character varying(200),
DATANADANIA  DATE,
STATUS  INTEGER,
PERSON  numeric (18,0),
RODZAJUSLUGI  INTEGER
);

create table DS_FAX_CACHE 
(
ID  numeric (18,0),
CTIME  TIMESTAMP,
FILENAME  character varying(100),
DOCUMENTID  numeric (18,0)
);

create table DS_FILTER_CONDITION 
(
ID  numeric (18,0) NOT NULL,
COLUMN_NAME  character varying(50) NOT NULL,
VALS  character varying(512) NOT NULL,
ACTIVE  SMALLINT NOT NULL,
BOOKMARK_ID  numeric (18,0),
ACTIVEF  BOOLEAN
);

create table DS_FLOOR 
(
NAME  character varying(50) NOT NULL,
ID  numeric (18,0) NOT NULL,
ADDRESS_ID  numeric (18,0) NOT NULL,
DESCRIPTION  character varying(255) NOT NULL,
POSN  INTEGER,
IMAGE_ID  numeric (18,0)
);

create table DS_FOLDER 
(
ID  numeric (18,0) NOT NULL,
TITLE  character varying(254),
PARENT_ID  numeric (18,0),
HVERSION  INTEGER,
CTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
SYSTEMNAME  character varying(32),
ATTRIBUTES  INTEGER NOT NULL,
LPARAM  character varying(200),
WPARAM  numeric (18,0),
ANCHOR  integer
);

create table DS_FOLDER_PERMISSION 
(
FOLDER_ID  numeric (18,0) NOT NULL,
SUBJECT  character varying(62) NOT NULL,
SUBJECTTYPE  character varying(14) NOT NULL,
NAME  character varying(14) NOT NULL,
NEGATIVE  BOOLEAN NOT NULL
);

create table DS_ICR_DOCUMENT_STATUS 
(
ID  INTEGER NOT NULL,
STATUS  INTEGER NOT NULL
);

create table DS_IMAGE 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(255),
DESCRIPTION  character varying(500),
IMAGE  BYTEA,
CONTENT_TYPE  character varying(255),
FILENAME  character varying(255)
);

create table DS_IMPORTED_DOCUMENT_COMMON 
(
ID  numeric (18,0) NOT NULL,
IMPORTED_FILE_ID  numeric (18,0) NOT NULL,
RESULT  INTEGER,
STATUS  INTEGER,
PROCESSEDTIME  TIMESTAMP,
DOCUMENTID  numeric (18,0),
NUMBERINFILE  INTEGER,
ERRORINFO  character varying(100)
);

create table DS_IMPORTED_FILE_BLOB 
(
ID  numeric (18,0) NOT NULL,
IMPORTED_FILE_ID  numeric (18,0) NOT NULL,
XML  BYTEA
);

create table DS_IMPORTED_FILE_CSV 
(
ID  numeric (18,0) NOT NULL,
IMPORTED_FILE_ID  numeric (18,0) NOT NULL,
TYPE_ID  INTEGER,
CSV  BYTEA
);

create table DS_KEY_STORE 
(
ID  numeric (18,0) NOT NULL,
KEYCODE  character varying(50) NOT NULL,
USERNAME  character varying(50) NOT NULL,
PUBLICKEY  character varying(2000) NOT NULL,
PRIVATEKEY  character varying(2000) NOT NULL,
STATUS  INTEGER NOT NULL,
ENCRYPTEDKEY  character varying(1000),
CTIME  TIMESTAMP NOT NULL,
GRANTEDFROM  character varying(50),
DISCRIMINATOR  character varying(30)
);

create table DS_LOCATION 
(
ID  numeric (18,0) NOT NULL,
LOC_NAME  character varying(200) NOT NULL,
STREET  character varying(50),
ZIP  character varying(10),
CITY  character varying(50),
OFFICIAL  BOOLEAN
);

create table DS_LOG 
(
ID  INTEGER NOT NULL,
REASON  character varying(200),
USERNAME  character varying(20),
CTIME  TIMESTAMP,
CODE  INTEGER,
PARAM  character varying(200),
LPARAM  INTEGER
);

create table DS_MESSAGE 
(
ID  numeric (18,0) NOT NULL,
MESSAGEID  character varying(200),
CONTENTDATA  BYTEA,
SUBJECT  character varying(200),
SENTDATE  TIMESTAMP,
SENDER  character varying(200),
PROCESSED  BOOLEAN NOT NULL,
EMAIL_CHANNEL_ID  INTEGER NOT NULL
);

create table DS_MESSAGE_ATTACHMENT 
(
ID  numeric (18,0) NOT NULL,
MESSAGE_ID  numeric (18,0) NOT NULL,
ATTACHMENTNO  INTEGER NOT NULL,
CONTENTSIZE  INTEGER,
FILENAME  character varying(510),
CONTENTDATA  BYTEA,
CONTENTTYPE  character varying(250)
);

create table DS_NEWS 
(
ID  numeric (18,0) NOT NULL,
SUBJECT  character varying(1024) NOT NULL,
DESCRIPTION  TEXT,
USER_ID  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP,
MTIME  TIMESTAMP,
NEWS_TYPE_ID  numeric (18,0) NOT NULL,
DELETED  BOOLEAN NOT NULL
);

create table DS_NEWS_TYPE 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(50) NOT NULL,
CTIME  TIMESTAMP,
DISABLED  BOOLEAN NOT NULL
);

create table DS_NEW_REPORT 
(
ID  INTEGER NOT NULL,
REPORT_CN  character varying(50) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
START_PROCESS_TIME  TIMESTAMP,
FINISH_PROCESS_TIME  TIMESTAMP,
USERNAME  character varying(50) NOT NULL,
STATUS  INTEGER NOT NULL,
CRITERIA  BYTEA,
TITLE  character varying(100) NOT NULL,
DESCRIPTION  character varying(300),
PROCESS_DATE  TIMESTAMP,
REMOTE_KEY  character varying(100),
GUID  character varying(100)
);

create table DS_NOTIFICATIONS 
(
ID  numeric (18,0) NOT NULL,
CONTENT  character varying(1024) NOT NULL,
LINK  character varying(512),
USER_ID  numeric (18,0) NOT NULL,
CTIME  TIMESTAMP,
TYPE  INTEGER
);

create table DS_PHONE 
(
NAME  character varying(100) NOT NULL,
TYPE  character varying(25) NOT NULL,
NUMBER  character varying(25) NOT NULL,
ADDRESS_ID  numeric (18,0) NOT NULL,
ID  numeric (18,0) NOT NULL,
POSN  INTEGER
);

create table DS_POSTAL_CODE 
(
ID  INTEGER NOT NULL,
CODE_FROM  INTEGER NOT NULL,
CODE_TO  INTEGER,
CITY  character varying(50) NOT NULL,
PROVINCE  character varying(50) NOT NULL
);

create table DS_PREFS_NODE 
(
ID  INTEGER NOT NULL,
PARENT_ID  INTEGER,
NAME  character varying(80) NOT NULL,
USERNAME  character varying(62)
);

create table DS_PREFS_VALUE 
(
NODE_ID  INTEGER NOT NULL,
PREF_KEY  character varying(80) NOT NULL,
PREF_VALUE  TEXT NOT NULL,
USERNAME  character varying(62)
);

create table DS_PROCESS_ACTION_ENTRY 
(
ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
DATE_  DATE NOT NULL,
DOCUMENT_ID  numeric (18,0),
PROCESS_ID  character varying(255),
PROCESS_NAME  character varying(255),
ACTION_NAME  character varying(255)
);

create table DS_PROFILE 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(62) NOT NULL,
ENCODED_ARCHIVE_ROLES  character varying(80)
);

create table DS_PROFILE_TO_ARCHIVE_ROLE 
(
PROFILE_ID  numeric (18,0) NOT NULL,
ROLE  character varying(62) NOT NULL,
SOURCE  character varying(200)
);

create table DS_PROFILE_TO_GROUP 
(
PROFILE_ID  numeric (18,0) NOT NULL,
DIVISION_ID  numeric (18,0) NOT NULL
);

create table DS_PROFILE_TO_ROLE 
(
PROFILE_ID  numeric (18,0) NOT NULL,
ROLE_ID  numeric (18,0) NOT NULL
);

create table DS_PROFILE_TO_USER 
(
PROFILE_ID  numeric (18,0) NOT NULL,
USER_ID  numeric (18,0) NOT NULL
);

create table DS_REJECTION_REASON 
(
ID  numeric (18,0) NOT NULL,
TIMEID  numeric (18,0),
USERNAME  character varying(50),
REASON  character varying(300),
periodIndex numeric (18,0)
);

create table DS_REPORT 
(
ID  numeric (18,0) NOT NULL,
REPORTID  character varying(20) NOT NULL,
VERSION  character varying(10) NOT NULL,
TITLE  character varying(254) NOT NULL,
CTIME  TIMESTAMP NOT NULL,
AUTHOR  character varying(62) NOT NULL,
STATUS  character varying(20) NOT NULL,
CONTENTDATA  BYTEA,
LPARAM  character varying(200),
WPARAM  numeric (18,0)
);

create table DS_REPORT_TEMPLATE 
(
INCOMINGDATEFROM  character varying(10),
INCOMINGDATETO  character varying(10),
LASTDAYS  INTEGER,
JOURNALID  character varying(20),
DIVISIONGUID  character varying(40),
KINDIDS  character varying(100),
AUTHOR  character varying(40),
DELIVERYID  INTEGER,
OUTGOINGDELIVERYID  INTEGER,
SENDER  character varying(40),
RECIPIENT  character varying(40),
FORCEAND  INTEGER,
WARNDAYS  INTEGER,
SHOWOVERDUES  INTEGER,
SORTFIELD  character varying(40),
REPORTTYPE  character varying(20),
REPORTNAME  character varying(20),
CREATEDATE  character varying(10)
);

create table DS_RESOURCES_TO_TIME 
(
ID  numeric (18,0) NOT NULL,
TIME_ID  numeric (18,0),
OBJECT_ID  numeric (18,0),
TYPE  character varying(20),
CONFIRM  BOOLEAN
);

create table DS_RESOURCE_STRING 
(
id  character varying(30) NOT NULL,
value  character varying(30)
);

create table DS_RETAINED_ATTACHMENT 
(
ID  numeric (18,0) NOT NULL,
OBJECT_ID  numeric (18,0),
POSN  INTEGER,
FILENAME  character varying(255) NOT NULL,
CN  character varying(20),
CONTENTSIZE  INTEGER NOT NULL,
CONTENTDATA  BYTEA,
MIME  character varying(62) NOT NULL
);

create table DS_RETAINED_OBJECT 
(
ID  numeric (18,0) NOT NULL,
SOURCE  character varying(16) NOT NULL,
OBJECT_TYPE  character varying(16),
CTIME  TIMESTAMP NOT NULL,
TITLE  character varying(100) NOT NULL
);

create table DS_RETAINED_OBJECT_PROPS 
(
OBJECT_ID  numeric (18,0),
PROPERTY_NAME  character varying(255) NOT NULL,
PROPERTY_VALUE  character varying(255) NOT NULL
);

create table DS_SUBST_HIST 
(
ID  INTEGER NOT NULL,
LOGIN  character varying(62) NOT NULL,
SUBSTITUTEDFROM  TIMESTAMP NOT NULL,
SUBSTITUTEDTHROUGH  TIMESTAMP NOT NULL,
SUBSTLOGIN  character varying(62) NOT NULL
);

create table DS_TIME 
(
ID  numeric (18,0) NOT NULL,
STARTTIME  TIMESTAMP NOT NULL,
ENDTIME  TIMESTAMP NOT NULL,
CREATOR_ID  numeric (18,0) NOT NULL,
PLACE  character varying(64),
DESCRIPTION  character varying(256),
ALLDAY  numeric (18,0)
);

create table DS_USER 
(
ID  numeric (18,0) NOT NULL,
NAME  character varying(62) NOT NULL,
FIRSTNAME  character varying(62),
LASTNAME  character varying(62),
EMAIL  character varying(62),
IDENTIFIER  character varying(20),
LOGINDISABLED  BOOLEAN NOT NULL,
USERROLES  character varying(80),
LASTSUCCESSFULLOGIN  TIMESTAMP,
LASTUNSUCCESSFULLOGIN  TIMESTAMP,
SUBSTITUTEDFROM  TIMESTAMP,
SUBSTITUTEDTHROUGH  TIMESTAMP,
DELETED  BOOLEAN NOT NULL,
CERTIFICATELOGIN  BOOLEAN NOT NULL,
LASTPASSWORDCHANGE  TIMESTAMP,
HASHEDPASSWORD  character varying(62),
SUPERVISOR_ID  numeric (18,0),
SUBSTITUTE_ID  numeric (18,0),
SYSTEMUSER  BOOLEAN NOT NULL,
INCORRECTLOGINS  INTEGER,
MOBILE_PHONE_NUM  character varying(30),
PHONE_NUM  character varying(30),
EXTENSION  character varying(30),
COMMUNICATOR_NUM  character varying(30),
CTIME  TIMESTAMP,
EXTERNALNAME  character varying(62),
KPX  character varying(20),
PESEL  character varying(11),
VALIDITY_DATE  TIMESTAMP,
REMARKS  character varying(400),
LOCATION  numeric (18,0),
ROOMNUM  character varying(30),
MUST_CHANGE_PASSWORD  BOOLEAN NOT NULL,
ACTIVE_DIRECTORY_USER   boolean default false,
DOMAIN_PREFIX  character varying(200),
IMAGE_ID  numeric (18,0),
POST_DESCRIPTION  character varying(255),
USER_LOCATION_ID  numeric (18,0),
about_me text,
sites text,
LIMITMOBILE  double precision ,
LIMITPHONE  double precision ,
AUTHLEVEL  smallint NOT NULL default 0,
AD_URL  character varying(50),
internal_number character varying(30),
issupervisor boolean not null default  false


);

create table DS_USER_CALENDAR 
(
RESOURCEID  numeric (18,0) NOT NULL,
USERNAME  character varying(30) NOT NULL,
PERMISSIONTYPE  INTEGER,
RESOURCETYPE  character varying(30) NOT NULL,
SHOW  BOOLEAN,
COLORID  INTEGER NOT NULL,
POSN  INTEGER,
DISABLE  INTEGER
);

create table DS_USER_CERT 
(
USER_ID  numeric (18,0) NOT NULL,
POSN  BOOLEAN,
CERTDATA  TEXT,
SIGNATURE  character varying(2048) NOT NULL
);

create table DS_USER_CERTIFICATE 
(
ID  numeric (18,0) NOT NULL,
USERNAME  character varying(62) NOT NULL,
ALIAS  character varying(70),
ENCODEDCSR  TEXT,
ENCODEDCRT  TEXT,
ENCODEDPKCS7  TEXT,
CTIME  TIMESTAMP NOT NULL,
SIGNINGTIME  TIMESTAMP,
EXPIRATIONTIME  TIMESTAMP,
SERIALNUMBER  INTEGER,
SUBJECTDN  character varying(200),
ENCODEDSIGNATURE  TEXT,
SIGNED  BOOLEAN NOT NULL
);

create table DS_USER_LOCATION 
(
ID  numeric (18,0) NOT NULL,
PARAM_X  INTEGER NOT NULL,
PARAM_Y  INTEGER NOT NULL,
FLOOR_ID  numeric (18,0) NOT NULL
);

create table DS_USER_PASSWD_HISTORY 
(
USERNAME  character varying(62) NOT NULL,
HASHEDPASSWORD  character varying(62) NOT NULL,
CTIME  TIMESTAMP
);

create table DS_USER_PHONE_NUMBER 
(
ID  numeric (18,0) NOT NULL,
PHONE_NUMBER  character varying(25) NOT NULL,
USERNAME  CHAR(20)
);

create table DS_USER_SUPERVISOR 
(
USER_ID  numeric (18,0) NOT NULL,
SUPERVISOR_ID  numeric (18,0) NOT NULL
);

create table DS_USER_TO_ARCHIVE_ROLE 
(
USER_ID  numeric (18,0) NOT NULL,
ROLE  character varying(62) NOT NULL,
SOURCE  character varying(200)
);

create table DS_USER_TO_DIVISION 
(
USER_ID  numeric (18,0) NOT NULL,
DIVISION_ID  numeric (18,0) NOT NULL,
SOURCE  character varying(200)
);

create table DS_USER_TO_TIME 
(
USER_ID  numeric (18,0) NOT NULL,
TIME_ID  numeric (18,0) NOT NULL
);

create table DS_WATCHLIST 
(
USERNAME  character varying(62) NOT NULL,
URN  character varying(250) NOT NULL
);

create table IC_CUSTOMS_AGENCY 
(
ID  INTEGER NOT NULL,
TITLE  character varying(255)
);

create table IC_FIXED_ASSETS_GROUP 
(
ID  INTEGER NOT NULL,
TITLE  character varying(255) NOT NULL
);

create table IC_FIXED_ASSETS_SUBGROUP 
(
ID  INTEGER NOT NULL,
GROUP_ID  INTEGER NOT NULL,
TITLE  character varying(255) NOT NULL
);

create table JBPM_ACTION 
(
ID_  numeric (18,0) NOT NULL,
CLASS  CHAR(1) NOT NULL,
NAME_  character varying(250),
ISPROPAGATIONALLOWED_  BOOLEAN,
ACTIONEXPRESSION_  character varying(250),
ISASYNC_  BOOLEAN,
REFERENCEDACTION_  numeric (18,0),
ACTIONDELEGATION_  numeric (18,0),
EVENT_  numeric (18,0),
PROCESSDEFINITION_  numeric (18,0),
EXPRESSION_  character varying(4000),
TIMERNAME_  character varying(250),
DUEDATE_  character varying(250),
REPEAT_  character varying(250),
TRANSITIONNAME_  character varying(250),
TIMERACTION_  numeric (18,0),
EVENTINDEX_  INTEGER,
EXCEPTIONHANDLER_  numeric (18,0),
EXCEPTIONHANDLERINDEX_  INTEGER
);

create table JBPM_BYTEARRAY 
(
ID_  numeric (18,0) NOT NULL,
NAME_  character varying(250),
FILEDEFINITION_  numeric (18,0)
);

create table JBPM_BYTEBLOCK 
(
PROCESSFILE_  numeric (18,0) NOT NULL,
BYTES_  BYTEA,
INDEX_  INTEGER NOT NULL
);

create table JBPM_COMMENT 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
ACTORID_  character varying(250),
TIME_  TIMESTAMP,
MESSAGE_  character varying(4000),
TOKEN_  numeric (18,0),
TASKINSTANCE_  numeric (18,0),
TOKENINDEX_  INTEGER,
TASKINSTANCEINDEX_  INTEGER
);

create table JBPM_DECISIONCONDITIONS 
(
DECISION_  numeric (18,0) NOT NULL,
TRANSITIONNAME_  character varying(250),
EXPRESSION_  character varying(250),
INDEX_  INTEGER NOT NULL
);

create table JBPM_DELEGATION 
(
ID_  numeric (18,0) NOT NULL,
CLASSNAME_  character varying(4000),
CONFIGURATION_  character varying(4000),
CONFIGTYPE_  character varying(250),
PROCESSDEFINITION_  numeric (18,0)
);

create table JBPM_EVENT 
(
ID_  numeric (18,0) NOT NULL,
EVENTTYPE_  character varying(250),
TYPE_  CHAR(1),
GRAPHELEMENT_  numeric (18,0),
PROCESSDEFINITION_  numeric (18,0),
NODE_  numeric (18,0),
TRANSITION_  numeric (18,0),
TASK_  numeric (18,0)
);

create table JBPM_EXCEPTIONHANDLER 
(
ID_  numeric (18,0) NOT NULL,
EXCEPTIONCLASSNAME_  character varying(4000),
TYPE_  CHAR(1),
GRAPHELEMENT_  numeric (18,0),
PROCESSDEFINITION_  numeric (18,0),
GRAPHELEMENTINDEX_  INTEGER,
NODE_  numeric (18,0),
TRANSITION_  numeric (18,0),
TASK_  numeric (18,0)
);

create table JBPM_ID_GROUP 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(250),
TYPE_  character varying(250),
PARENT_  numeric (18,0)
);

create table JBPM_ID_MEMBERSHIP 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(250),
ROLE_  character varying(250),
USER_  numeric (18,0),
GROUP_  numeric (18,0)
);

create table JBPM_ID_PERMISSIONS 
(
ENTITY_  numeric (18,0) NOT NULL,
CLASS_  character varying(250),
NAME_  character varying(250),
ACTION_  character varying(250)
);

create table JBPM_ID_USER 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(250),
EMAIL_  character varying(250),
PASSWORD_  character varying(250)
);

create table JBPM_JOB 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
VERSION_  INTEGER NOT NULL,
DUEDATE_  TIMESTAMP,
PROCESSINSTANCE_  numeric (18,0),
TOKEN_  numeric (18,0),
TASKINSTANCE_  numeric (18,0),
ISSUSPENDED_  BOOLEAN,
ISEXCLUSIVE_  BOOLEAN,
LOCKOWNER_  character varying(250),
LOCKTIME_  TIMESTAMP,
EXCEPTION_  character varying(4000),
RETRIES_  INTEGER,
NAME_  character varying(250),
REPEAT_  character varying(250),
TRANSITIONNAME_  character varying(250),
ACTION_  numeric (18,0),
GRAPHELEMENTTYPE_  character varying(250),
GRAPHELEMENT_  numeric (18,0),
NODE_  numeric (18,0)
);

create table JBPM_LOG 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
INDEX_  INTEGER,
DATE_  TIMESTAMP,
TOKEN_  numeric (18,0),
PARENT_  numeric (18,0),
MESSAGE_  character varying(4000),
EXCEPTION_  character varying(4000),
ACTION_  numeric (18,0),
NODE_  numeric (18,0),
ENTER_  TIMESTAMP,
LEAVE_  TIMESTAMP,
DURATION_  numeric (18,0),
NEWLONGVALUE_  numeric (18,0),
TRANSITION_  numeric (18,0),
CHILD_  numeric (18,0),
SOURCENODE_  numeric (18,0),
DESTINATIONNODE_  numeric (18,0),
VARIABLEINSTANCE_  numeric (18,0),
OLDBYTEARRAY_  numeric (18,0),
NEWBYTEARRAY_  numeric (18,0),
OLDDATEVALUE_  TIMESTAMP,
NEWDATEVALUE_  TIMESTAMP,
OLDDOUBLEVALUE_  double precision ,
NEWDOUBLEVALUE_  double precision ,
OLDLONGIDCLASS_  character varying(250),
OLDLONGIDVALUE_  numeric (18,0),
NEWLONGIDCLASS_  character varying(250),
NEWLONGIDVALUE_  numeric (18,0),
OLDSTRINGIDCLASS_  character varying(250),
OLDSTRINGIDVALUE_  character varying(250),
NEWSTRINGIDCLASS_  character varying(250),
NEWSTRINGIDVALUE_  character varying(250),
OLDLONGVALUE_  numeric (18,0),
OLDSTRINGVALUE_  character varying(4000),
NEWSTRINGVALUE_  character varying(4000),
TASKINSTANCE_  numeric (18,0),
TASKACTORID_  character varying(250),
TASKOLDACTORID_  character varying(250),
SWIMLANEINSTANCE_  numeric (18,0)
);

create table JBPM_MODULEDEFINITION 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(4000),
PROCESSDEFINITION_  numeric (18,0),
STARTTASK_  numeric (18,0)
);

create table JBPM_MODULEINSTANCE 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
VERSION_  INTEGER NOT NULL,
PROCESSINSTANCE_  numeric (18,0),
TASKMGMTDEFINITION_  numeric (18,0),
NAME_  character varying(250)
);

create table JBPM_NODE 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(250),
DESCRIPTION_  character varying(4000),
PROCESSDEFINITION_  numeric (18,0),
ISASYNC_  BOOLEAN,
ISASYNCEXCL_  BOOLEAN,
ACTION_  numeric (18,0),
SUPERSTATE_  numeric (18,0),
SUBPROCNAME_  character varying(250),
SUBPROCESSDEFINITION_  numeric (18,0),
DECISIONEXPRESSION_  character varying(250),
DECISIONDELEGATION  numeric (18,0),
SCRIPT_  numeric (18,0),
SIGNAL_  INTEGER,
CREATETASKS_  BOOLEAN,
ENDTASKS_  BOOLEAN,
NODECOLLECTIONINDEX_  INTEGER
);

create table JBPM_POOLEDACTOR 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
ACTORID_  character varying(250),
SWIMLANEINSTANCE_  numeric (18,0)
);

create table JBPM_PROCESSDEFINITION 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
NAME_  character varying(250),
DESCRIPTION_  character varying(4000),
VERSION_  INTEGER,
ISTERMINATIONIMPLICIT_  BOOLEAN,
STARTSTATE_  numeric (18,0)
);

create table JBPM_PROCESSINSTANCE 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
KEY_  character varying(250),
START_  TIMESTAMP,
END_  TIMESTAMP,
ISSUSPENDED_  BOOLEAN,
PROCESSDEFINITION_  numeric (18,0),
ROOTTOKEN_  numeric (18,0),
SUPERPROCESSTOKEN_  numeric (18,0)
);

create table JBPM_RUNTIMEACTION 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
EVENTTYPE_  character varying(250),
TYPE_  CHAR(1),
GRAPHELEMENT_  numeric (18,0),
PROCESSINSTANCE_  numeric (18,0),
ACTION_  numeric (18,0),
PROCESSINSTANCEINDEX_  INTEGER
);

create table JBPM_SWIMLANE 
(
ID_  numeric (18,0) NOT NULL,
NAME_  character varying(250),
ACTORIDEXPRESSION_  character varying(250),
POOLEDACTORSEXPRESSION_  character varying(250),
ASSIGNMENTDELEGATION_  numeric (18,0),
TASKMGMTDEFINITION_  numeric (18,0)
);

create table JBPM_SWIMLANEINSTANCE 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
NAME_  character varying(250),
ACTORID_  character varying(250),
SWIMLANE_  numeric (18,0),
TASKMGMTINSTANCE_  numeric (18,0)
);

create table JBPM_TASK 
(
ID_  numeric (18,0) NOT NULL,
NAME_  character varying(250),
DESCRIPTION_  character varying(4000),
PROCESSDEFINITION_  numeric (18,0),
ISBLOCKING_  BOOLEAN,
ISSIGNALLING_  BOOLEAN,
CONDITION_  character varying(250),
DUEDATE_  character varying(250),
PRIORITY_  INTEGER,
ACTORIDEXPRESSION_  character varying(250),
POOLEDACTORSEXPRESSION_  character varying(250),
TASKMGMTDEFINITION_  numeric (18,0),
TASKNODE_  numeric (18,0),
STARTSTATE_  numeric (18,0),
ASSIGNMENTDELEGATION_  numeric (18,0),
SWIMLANE_  numeric (18,0),
TASKCONTROLLER_  numeric (18,0)
);

create table JBPM_TASKACTORPOOL 
(
TASKINSTANCE_  numeric (18,0) NOT NULL,
POOLEDACTOR_  numeric (18,0) NOT NULL
);

create table JBPM_TASKCONTROLLER 
(
ID_  numeric (18,0) NOT NULL,
TASKCONTROLLERDELEGATION_  numeric (18,0)
);

create table JBPM_TASKINSTANCE 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
VERSION_  INTEGER NOT NULL,
NAME_  character varying(250),
DESCRIPTION_  character varying(4000),
ACTORID_  character varying(250),
CREATE_  TIMESTAMP,
START_  TIMESTAMP,
END_  TIMESTAMP,
DUEDATE_  TIMESTAMP,
PRIORITY_  INTEGER,
ISCANCELLED_  BOOLEAN,
ISSUSPENDED_  BOOLEAN,
ISOPEN_  BOOLEAN,
ISSIGNALLING_  BOOLEAN,
ISBLOCKING_  BOOLEAN,
TASK_  numeric (18,0),
TOKEN_  numeric (18,0),
PROCINST_  numeric (18,0),
SWIMLANINSTANCE_  numeric (18,0),
TASKMGMTINSTANCE_  numeric (18,0)
);

create table JBPM_TOKEN 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
NAME_  character varying(250),
START_  TIMESTAMP,
END_  TIMESTAMP,
NODEENTER_  TIMESTAMP,
NEXTLOGINDEX_  INTEGER,
ISABLETOREACTIVATEPARENT_  BOOLEAN,
ISTERMINATIONIMPLICIT_  BOOLEAN,
ISSUSPENDED_  BOOLEAN,
LOCK_  character varying(250),
NODE_  numeric (18,0),
PROCESSINSTANCE_  numeric (18,0),
PARENT_  numeric (18,0),
SUBPROCESSINSTANCE_  numeric (18,0)
);

create table JBPM_TOKENVARIABLEMAP 
(
ID_  numeric (18,0) NOT NULL,
VERSION_  INTEGER NOT NULL,
TOKEN_  numeric (18,0),
CONTEXTINSTANCE_  numeric (18,0)
);

create table JBPM_TRANSITION 
(
ID_  numeric (18,0) NOT NULL,
NAME_  character varying(250),
DESCRIPTION_  character varying(4000),
PROCESSDEFINITION_  numeric (18,0),
FROM_  numeric (18,0),
TO_  numeric (18,0),
CONDITION_  character varying(250),
FROMINDEX_  INTEGER
);

create table JBPM_VARIABLEACCESS 
(
ID_  numeric (18,0) NOT NULL,
VARIABLENAME_  character varying(250),
ACCESS_  character varying(250),
MAPPEDNAME_  character varying(250),
SCRIPT_  numeric (18,0),
PROCESSSTATE_  numeric (18,0),
TASKCONTROLLER_  numeric (18,0),
INDEX_  INTEGER
);

create table JBPM_VARIABLEINSTANCE 
(
ID_  numeric (18,0) NOT NULL,
CLASS_  CHAR(1) NOT NULL,
VERSION_  INTEGER NOT NULL,
NAME_  character varying(250),
CONVERTER_  CHAR(1),
TOKEN_  numeric (18,0),
TOKENVARIABLEMAP_  numeric (18,0),
PROCESSINSTANCE_  numeric (18,0),
BYTEARRAYVALUE_  numeric (18,0),
DATEVALUE_  TIMESTAMP,
DOUBLEVALUE_  double precision ,
LONGIDCLASS_  character varying(250),
LONGVALUE_  numeric (18,0),
STRINGIDCLASS_  character varying(250),
STRINGVALUE_  character varying(4000),
TASKINSTANCE_  numeric (18,0)
);





ALTER TABLE DS_PREFS_NODE ADD CONSTRAINT DS_PREFS_NODE_PK PRIMARY KEY (ID);
ALTER TABLE DS_DOCTYPE ADD CONSTRAINT DS_DOCTYPE_PK PRIMARY KEY (ID);
ALTER TABLE DS_BOX ADD CONSTRAINT DS_BOX_PK PRIMARY KEY (ID);
ALTER TABLE DS_ATTACHMENT ADD CONSTRAINT DS_ATTACHMENT_PKEY PRIMARY KEY (ID);
ALTER TABLE DS_ATTACHMENT_REVISION ADD CONSTRAINT DS_ATTACHMENT_REVISION_PKEY PRIMARY KEY (ID);
ALTER TABLE DS_FOLDER ADD CONSTRAINT DS_FOLDER_PKEY PRIMARY KEY (ID);
ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCUMENT_PKEY PRIMARY KEY (ID);
ALTER TABLE DS_DOCUMENT_LOCK ADD CONSTRAINT DS_DOCUMENT_LOCK_PK PRIMARY KEY (DOCUMENT_ID, USERNAME, LOCKMODE);
ALTER TABLE DS_REPORT ADD CONSTRAINT DS_REPORT_PK PRIMARY KEY (ID);
ALTER TABLE DS_USER ADD CONSTRAINT DS_USER_PK PRIMARY KEY (ID);
ALTER TABLE DS_USER_CERTIFICATE ADD CONSTRAINT DS_USER_CERTIFICATE_PK PRIMARY KEY (ID);
ALTER TABLE DS_DIVISION ADD CONSTRAINT DS_DIVISION_PK PRIMARY KEY (ID);
ALTER TABLE DSO_RWA ADD CONSTRAINT DSO_RWA_PK PRIMARY KEY (ID);
ALTER TABLE DSO_IN_DOCUMENT ADD CONSTRAINT INTEG_91 PRIMARY KEY (ID);
ALTER TABLE DSO_OUT_DOCUMENT ADD CONSTRAINT INTEG_98 PRIMARY KEY (ID);
ALTER TABLE DSO_IN_DOCUMENT_STATUS ADD CONSTRAINT INTEG_106 PRIMARY KEY (ID);
ALTER TABLE DSO_IN_DOCUMENT_KIND ADD CONSTRAINT INTEG_110 PRIMARY KEY (ID);
ALTER TABLE DSO_IN_DOCUMENT_DELIVERY ADD CONSTRAINT INTEG_116 PRIMARY KEY (ID);
ALTER TABLE DSO_OUT_DOCUMENT_DELIVERY ADD CONSTRAINT INTEG_120 PRIMARY KEY (ID);
ALTER TABLE DSO_OUT_DOCUMENT_RETREASON ADD CONSTRAINT INTEG_124 PRIMARY KEY (ID);
ALTER TABLE DSO_JOURNAL ADD CONSTRAINT DSO_JOURNAL_PKEY PRIMARY KEY (ID);
ALTER TABLE DSO_PERSON ADD CONSTRAINT DSO_PERSON_PK PRIMARY KEY (ID);
ALTER TABLE DSO_ROLE ADD CONSTRAINT INTEG_166 PRIMARY KEY (ID);
ALTER TABLE DSO_CASE_STATUS ADD CONSTRAINT DSO_CASE_STATUS_PKEY PRIMARY KEY (ID);
ALTER TABLE DSO_CASE_PRIORITY ADD CONSTRAINT DSO_CASE_PRIORITY_PK PRIMARY KEY (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PK PRIMARY KEY (ID);
ALTER TABLE DSO_COLLECTIVE_ASSIGNMENT ADD CONSTRAINT DSO_COLLECTIVE_ASSIGNMENT_PK PRIMARY KEY (ID);
ALTER TABLE DSO_ASSIGNMENT_OBJECTIVE ADD CONSTRAINT INTEG_198 PRIMARY KEY (ID);
ALTER TABLE DSW_PACKAGE ADD CONSTRAINT DSW_PACKAGE_PK PRIMARY KEY (ID);
ALTER TABLE DSW_RESOURCE ADD CONSTRAINT DSW_RESOURCE_PK PRIMARY KEY (ID);
ALTER TABLE DSW_PROCESS ADD CONSTRAINT DSW_PROCESS_PK PRIMARY KEY (ID);
ALTER TABLE DSW_ACTIVITY ADD CONSTRAINT DSW_ACTIVITY_PK PRIMARY KEY (ID);
ALTER TABLE DSW_ASSIGNMENT ADD CONSTRAINT DSW_ASSIGNMENT_PK PRIMARY KEY (ID);
ALTER TABLE DSW_DATATYPE ADD CONSTRAINT DSW_DATATYPE_PK PRIMARY KEY (ID);
ALTER TABLE DSE_CONTRACTOR ADD CONSTRAINT DSE_CONTRACTOR_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_ADDRESS ADD CONSTRAINT DSE_ADDRESS_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_HISTORY ADD CONSTRAINT DSE_HISTORY_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_CHANGE_EVENT ADD CONSTRAINT DSE_CHANGE_EVENT_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_CLASSIFICATION ADD CONSTRAINT DSE_CLASSIFICATION_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_BRANCH ADD CONSTRAINT DSE_BRANCH_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_PROVINCE ADD CONSTRAINT DSE_PROVINCE_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_OFFICE ADD CONSTRAINT DSE_OFFICE_PKEY PRIMARY KEY (ID);
ALTER TABLE DSE_STATUS ADD CONSTRAINT DSE_STATUS_PKEY PRIMARY KEY (ID);
ALTER TABLE DS_DOCUMENT_FLAGS ADD CONSTRAINT DS_DOCUMENT_FLAGS_PK PRIMARY KEY (DOCUMENT_ID);
ALTER TABLE DS_DOCUMENT_USER_FLAGS ADD CONSTRAINT DS_DOCUMENT_USER_FLAGS_PK PRIMARY KEY (DOCUMENT_ID, USERNAME);
ALTER TABLE DS_RETAINED_OBJECT ADD CONSTRAINT DS_RETAINED_OBJECT_PK PRIMARY KEY (ID);
ALTER TABLE DS_RETAINED_ATTACHMENT ADD CONSTRAINT DS_RETAINED_ATTACHMENT_PK PRIMARY KEY (ID);
ALTER TABLE DAA_AGENCJA ADD CONSTRAINT INTEG_337 PRIMARY KEY (ID);
ALTER TABLE DAA_AGENT ADD CONSTRAINT INTEG_339 PRIMARY KEY (ID);
ALTER TABLE DSO_AA_DOCUMENT ADD CONSTRAINT INTEG_349 PRIMARY KEY (ID);
ALTER TABLE DSO_ORDER ADD CONSTRAINT INTEG_358 PRIMARY KEY (ID);
ALTER TABLE DS_DOCUMENT_KIND ADD CONSTRAINT INTEG_374 PRIMARY KEY (ID);
ALTER TABLE DS_TIME ADD CONSTRAINT INTEG_376 PRIMARY KEY (ID);
ALTER TABLE DS_IMPORTED_DOCUMENT_COMMON ADD CONSTRAINT INTEG_387 PRIMARY KEY (ID);
ALTER TABLE DS_IMPORTED_FILE_BLOB ADD CONSTRAINT INTEG_390 PRIMARY KEY (ID);
ALTER TABLE DS_IMPORTED_FILE_CSV ADD CONSTRAINT INTEG_393 PRIMARY KEY (ID);
ALTER TABLE DS_BOX_LINE ADD CONSTRAINT INTEG_396 PRIMARY KEY (ID);
ALTER TABLE DSR_REGISTRY ADD CONSTRAINT INTEG_403 PRIMARY KEY (ID);
ALTER TABLE DSR_REGISTRY_ENTRY ADD CONSTRAINT INTEG_409 PRIMARY KEY (ID);
ALTER TABLE DSR_ENTRY_TO_CASE ADD CONSTRAINT INTEG_417 PRIMARY KEY (ENTRY_ID, CASE_ID);
ALTER TABLE DS_ACCEPTANCE_CONDITION ADD CONSTRAINT INTEG_421 PRIMARY KEY (ID);
ALTER TABLE DS_DOCUMENT_ACCEPTANCE ADD CONSTRAINT INTEG_427 PRIMARY KEY (ID);
ALTER TABLE DSO_ADNOTATION ADD CONSTRAINT INTEG_429 PRIMARY KEY (ID);
ALTER TABLE DSG_ACCOUNT_NUMBER ADD CONSTRAINT INTEG_440 PRIMARY KEY (ACCOUNT_NUMBER);
ALTER TABLE DSG_CENTRUM_KOSZTOW ADD CONSTRAINT INTEG_433 PRIMARY KEY (ID);
ALTER TABLE DSG_CENTRUM_KOSZTOW_FAKTURY ADD CONSTRAINT INTEG_435 PRIMARY KEY (ID);
ALTER TABLE DSW_TASK_HISTORY_ENTRY ADD CONSTRAINT INTEG_438 PRIMARY KEY (ID);
ALTER TABLE DSC_CONTRACTOR_DICTIONARY ADD CONSTRAINT INTEG_442 PRIMARY KEY (ID);
ALTER TABLE DSC_CONTRACTOR ADD CONSTRAINT INTEG_444 PRIMARY KEY (ID);
ALTER TABLE DSC_WORKER ADD CONSTRAINT INTEG_446 PRIMARY KEY (ID);
ALTER TABLE DS_LOG ADD CONSTRAINT INTEG_447 PRIMARY KEY (ID);
ALTER TABLE DSE_EVENT ADD CONSTRAINT INTEG_642 PRIMARY KEY (EVENT_ID);
ALTER TABLE IC_FIXED_ASSETS_GROUP ADD CONSTRAINT INTEG_644 PRIMARY KEY (ID);
ALTER TABLE IC_FIXED_ASSETS_SUBGROUP ADD CONSTRAINT INTEG_647 PRIMARY KEY (ID);
ALTER TABLE DS_BOX_CHANGE_HISTORY ADD CONSTRAINT INTEG_468 PRIMARY KEY (ID);
ALTER TABLE DSR_CNS_DECISION_ARCHIVE ADD CONSTRAINT INTEG_470 PRIMARY KEY (ID);
ALTER TABLE DS_PROFILE ADD CONSTRAINT DS_PROFILE_PK PRIMARY KEY (ID);
ALTER TABLE DSO_LABEL ADD CONSTRAINT INTEG_520 PRIMARY KEY (ID);
ALTER TABLE DSO_DOCUMENT_TO_LABEL ADD CONSTRAINT INTEG_526 PRIMARY KEY (ID);
ALTER TABLE DS_POSTAL_CODE ADD CONSTRAINT INTEG_528 PRIMARY KEY (ID);
ALTER TABLE JBPM_ACTION ADD CONSTRAINT INTEG_534 PRIMARY KEY (ID_);
ALTER TABLE JBPM_BYTEARRAY ADD CONSTRAINT INTEG_536 PRIMARY KEY (ID_);
ALTER TABLE JBPM_BYTEBLOCK ADD CONSTRAINT INTEG_539 PRIMARY KEY (PROCESSFILE_, INDEX_);
ALTER TABLE JBPM_COMMENT ADD CONSTRAINT INTEG_542 PRIMARY KEY (ID_);
ALTER TABLE JBPM_DECISIONCONDITIONS ADD CONSTRAINT INTEG_545 PRIMARY KEY (DECISION_, INDEX_);
ALTER TABLE JBPM_DELEGATION ADD CONSTRAINT INTEG_547 PRIMARY KEY (ID_);
ALTER TABLE JBPM_EVENT ADD CONSTRAINT INTEG_549 PRIMARY KEY (ID_);
ALTER TABLE JBPM_EXCEPTIONHANDLER ADD CONSTRAINT INTEG_551 PRIMARY KEY (ID_);
ALTER TABLE JBPM_ID_GROUP ADD CONSTRAINT INTEG_554 PRIMARY KEY (ID_);
ALTER TABLE JBPM_ID_MEMBERSHIP ADD CONSTRAINT INTEG_557 PRIMARY KEY (ID_);
ALTER TABLE JBPM_ID_USER ADD CONSTRAINT INTEG_561 PRIMARY KEY (ID_);
ALTER TABLE JBPM_JOB ADD CONSTRAINT INTEG_565 PRIMARY KEY (ID_);
ALTER TABLE JBPM_LOG ADD CONSTRAINT INTEG_568 PRIMARY KEY (ID_);
ALTER TABLE JBPM_MODULEDEFINITION ADD CONSTRAINT INTEG_571 PRIMARY KEY (ID_);
ALTER TABLE JBPM_MODULEINSTANCE ADD CONSTRAINT INTEG_575 PRIMARY KEY (ID_);
ALTER TABLE JBPM_NODE ADD CONSTRAINT INTEG_578 PRIMARY KEY (ID_);
ALTER TABLE JBPM_POOLEDACTOR ADD CONSTRAINT INTEG_581 PRIMARY KEY (ID_);
ALTER TABLE JBPM_PROCESSDEFINITION ADD CONSTRAINT INTEG_584 PRIMARY KEY (ID_);
ALTER TABLE JBPM_PROCESSINSTANCE ADD CONSTRAINT INTEG_587 PRIMARY KEY (ID_);
ALTER TABLE JBPM_RUNTIMEACTION ADD CONSTRAINT INTEG_590 PRIMARY KEY (ID_);
ALTER TABLE JBPM_SWIMLANE ADD CONSTRAINT INTEG_592 PRIMARY KEY (ID_);
ALTER TABLE JBPM_SWIMLANEINSTANCE ADD CONSTRAINT INTEG_595 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TASK ADD CONSTRAINT INTEG_597 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TASKACTORPOOL ADD CONSTRAINT INTEG_600 PRIMARY KEY (TASKINSTANCE_, POOLEDACTOR_);
ALTER TABLE JBPM_TASKCONTROLLER ADD CONSTRAINT INTEG_602 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT INTEG_606 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TOKEN ADD CONSTRAINT INTEG_609 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TOKENVARIABLEMAP ADD CONSTRAINT INTEG_612 PRIMARY KEY (ID_);
ALTER TABLE JBPM_TRANSITION ADD CONSTRAINT INTEG_614 PRIMARY KEY (ID_);
ALTER TABLE JBPM_VARIABLEACCESS ADD CONSTRAINT INTEG_616 PRIMARY KEY (ID_);
ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT INTEG_620 PRIMARY KEY (ID_);
ALTER TABLE DS_RESOURCE_STRING ADD CONSTRAINT INTEG_640 PRIMARY KEY (id);
ALTER TABLE DS_NEW_REPORT ADD CONSTRAINT INTEG_655 PRIMARY KEY (ID);
ALTER TABLE DSR_CON_CONTRACT ADD CONSTRAINT INTEG_668 PRIMARY KEY (ID);
ALTER TABLE DSR_CON_VENDOR ADD CONSTRAINT INTEG_684 PRIMARY KEY (ID);
ALTER TABLE DS_ANONYMOUS_ACCESS_TICKET ADD CONSTRAINT INTEG_687 PRIMARY KEY (ID);
ALTER TABLE DSG_BUDGET ADD CONSTRAINT INTEG_694 PRIMARY KEY (CENTRUM_KOSZTOW_ID);
ALTER TABLE DS_KEY_STORE ADD CONSTRAINT INTEG_697 PRIMARY KEY (ID);
ALTER TABLE DSO_DOCUMENT_REMARKS ADD CONSTRAINT DSO_DOCUMENT_REMARKS_ID_PK PRIMARY KEY (ID);
ALTER TABLE DSR_INV_INVOICE ADD CONSTRAINT INTEG_708 PRIMARY KEY (ID);
ALTER TABLE DSO_INVOICES_KIND ADD CONSTRAINT INTEG_724 PRIMARY KEY (ID);
ALTER TABLE DSO_EMAIL_CHANNEL ADD CONSTRAINT INTEG_727 PRIMARY KEY (ID);
ALTER TABLE DSR_INV_VENDOR ADD CONSTRAINT INTEG_739 PRIMARY KEY (ID);
ALTER TABLE DS_DATAMART ADD CONSTRAINT INTEG_742 PRIMARY KEY (EVENT_ID);
ALTER TABLE DSR_CON_CONTRACT_HISTORY ADD CONSTRAINT INTEG_744 PRIMARY KEY (ID);
ALTER TABLE DS_ADDRESS ADD CONSTRAINT INTEG_765 PRIMARY KEY (ID);
ALTER TABLE DS_FLOOR ADD CONSTRAINT INTEG_768 PRIMARY KEY (ID);
ALTER TABLE DS_PHONE ADD CONSTRAINT INTEG_831 PRIMARY KEY (ID);
ALTER TABLE DS_IMAGE ADD CONSTRAINT INTEG_779 PRIMARY KEY (ID);
ALTER TABLE DS_USER_LOCATION ADD CONSTRAINT INTEG_781 PRIMARY KEY (ID);
ALTER TABLE DS_NEWS_TYPE ADD CONSTRAINT INTEG_786 PRIMARY KEY (ID);
ALTER TABLE DS_NEWS ADD CONSTRAINT INTEG_790 PRIMARY KEY (ID);
ALTER TABLE DS_EMPLOYEE_CARD ADD CONSTRAINT INTEG_796 PRIMARY KEY (ID);
ALTER TABLE DS_RESOURCES_TO_TIME ADD CONSTRAINT INTEG_803 PRIMARY KEY (ID);
ALTER TABLE DS_ANY_RESOURCE ADD CONSTRAINT INTEG_805 PRIMARY KEY (ID);
ALTER TABLE DS_USER_CALENDAR ADD CONSTRAINT INTEG_806 PRIMARY KEY (RESOURCEID, RESOURCETYPE, USERNAME);
ALTER TABLE DS_CALENDAR ADD CONSTRAINT INTEG_808 PRIMARY KEY (ID);
ALTER TABLE DS_NOTIFICATIONS ADD CONSTRAINT INTEG_810 PRIMARY KEY (ID);
ALTER TABLE DS_ABSENCE_TYPE ADD CONSTRAINT INTEG_814 PRIMARY KEY (CODE);
ALTER TABLE DS_ABSENCE ADD CONSTRAINT INTEG_818 PRIMARY KEY (ID);
ALTER TABLE DS_BILING ADD CONSTRAINT INTEG_837 PRIMARY KEY (ID);
ALTER TABLE DS_USER_PHONE_NUMBER ADD CONSTRAINT INTEG_845 PRIMARY KEY (ID);
ALTER TABLE DS_CALENDAR_EVENT ADD CONSTRAINT INTEG_849 PRIMARY KEY (ID);
ALTER TABLE DS_ACCEPTANCE_DIVISION ADD CONSTRAINT INTEG_853 PRIMARY KEY (ID);
ALTER TABLE DS_REJECTION_REASON ADD CONSTRAINT INTEG_856 PRIMARY KEY (ID);
ALTER TABLE DS_FILTER_CONDITION ADD CONSTRAINT INTEG_858 PRIMARY KEY (ID);
ALTER TABLE DS_BOOKMARK ADD CONSTRAINT INTEG_863 PRIMARY KEY (ID);
ALTER TABLE DS_CALENDAR_FREE_DAY ADD CONSTRAINT INTEG_867 PRIMARY KEY (ID);
ALTER TABLE DSO_INVOICES_CPV_CODES ADD CONSTRAINT INTEG_341 PRIMARY KEY (ID);
ALTER TABLE DSO_INVOICES_DECRETATION ADD CONSTRAINT INTEG_418 PRIMARY KEY (ID);



CREATE TABLE ds_document_to_jbpm4(
	jbpm4_id character varying(50) NOT NULL PRIMARY KEY,
	document_id numeric (18,0) NOT NULL,
	process_name character varying(30) NOT NULL
 );

    create table JBPM4_DEPLOYMENT (
        DBID_ numeric (18,0) not null,
        NAME_ text,
        TIMESTAMP_ numeric (18,0),
        STATE_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_DEPLOYPROP (
        DBID_ numeric (18,0) not null,
        DEPLOYMENT_ numeric (18,0),
        OBJNAME_ character varying(255),
        KEY_ character varying(255),
        STRINGVAL_ character varying(255),
        LONGVAL_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_EXECUTION (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        ACTIVITYNAME_ character varying(255),
        PROCDEFID_ character varying(255),
        HASVARS_ boolean,
        NAME_ character varying(255),
        KEY_ character varying(255),
        ID_ character varying(255) unique,
        STATE_ character varying(255),
        SUSPHISTSTATE_ character varying(255),
        PRIORITY_ INTEGER,
        HISACTINST_ numeric (18,0),
        PARENT_ numeric (18,0),
        INSTANCE_ numeric (18,0),
        SUPEREXEC_ numeric (18,0),
        SUBPROCINST_ numeric (18,0),
        PARENT_IDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_ACTINST (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        HPROCI_ numeric (18,0),
        TYPE_ character varying(255),
        EXECUTION_ character varying(255),
        ACTIVITY_NAME_ character varying(255),
        START_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        TRANSITION_ character varying(255),
        NEXTIDX_ INTEGER,
        HTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_HIST_DETAIL (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        USERID_ character varying(255),
        TIME_ timestamp,
        HPROCI_ numeric (18,0),
        HPROCIIDX_ INTEGER,
        HACTI_ numeric (18,0),
        HACTIIDX_ INTEGER,
        HTASK_ numeric (18,0),
        HTASKIDX_ INTEGER,
        HVAR_ numeric (18,0),
        HVARIDX_ INTEGER,
        MESSAGE_ text,
        OLD_STR_ character varying(255),
        NEW_STR_ character varying(255),
        OLD_INT_ INTEGER,
        NEW_INT_ INTEGER,
        OLD_TIME_ timestamp,
        NEW_TIME_ timestamp,
        PARENT_ numeric (18,0),
        PARENT_IDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_PROCINST (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        PROCDEFID_ character varying(255),
        KEY_ character varying(255),
        START_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        STATE_ character varying(255),
        ENDACTIVITY_ character varying(255),
        NEXTIDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_TASK (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        EXECUTION_ character varying(255),
        OUTCOME_ character varying(255),
        ASSIGNEE_ character varying(255),
        PRIORITY_ INTEGER,
        STATE_ character varying(255),
        CREATE_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        NEXTIDX_ INTEGER,
        SUPERTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_HIST_VAR (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        PROCINSTID_ character varying(255),
        EXECUTIONID_ character varying(255),
        VARNAME_ character varying(255),
        VALUE_ character varying(255),
        HPROCI_ numeric (18,0),
        HTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_ID_GROUP (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        NAME_ character varying(255),
        TYPE_ character varying(255),
        PARENT_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_ID_MEMBERSHIP (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        USER_ numeric (18,0),
        GROUP_ numeric (18,0),
        NAME_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_ID_USER (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        PASSWORD_ character varying(255),
        GIVENNAME_ character varying(255),
        FAMILYNAME_ character varying(255),
        BUSINESSEMAIL_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_JOB (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        DUEDATE_ timestamp,
        STATE_ character varying(255),
        ISEXCLUSIVE_ INTEGER,
        LOCKOWNER_ character varying(255),
        LOCKEXPTIME_ timestamp,
        EXCEPTION_ text,
        RETRIES_ INTEGER,
        PROCESSINSTANCE_ numeric (18,0),
        EXECUTION_ numeric (18,0),
        CFG_ numeric (18,0),
        SIGNAL_ character varying(255),
        EVENT_ character varying(255),
        REPEAT_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_LOB (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        BLOB_VALUE_ numeric (18,0),
        DEPLOYMENT_ numeric (18,0),
        NAME_ text,
        primary key (DBID_)
    );

    create table JBPM4_PARTICIPATION (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        GROUPID_ character varying(255),
        USERID_ character varying(255),
        TYPE_ character varying(255),
        TASK_ numeric (18,0),
        SWIMLANE_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_PROPERTY (
        KEY_ character varying(255) not null,
        VERSION_ INTEGER not null,
        VALUE_ character varying(255),
        primary key (KEY_)
    );

    create table JBPM4_SWIMLANE (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        NAME_ character varying(255),
        ASSIGNEE_ character varying(255),
        EXECUTION_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_TASK (
        DBID_ numeric (18,0) not null,
        CLASS_ char(1 ) not null,
        DBVERSION_ INTEGER not null,
        NAME_ character varying(255),
        DESCR_ text,
        STATE_ character varying(255),
        SUSPHISTSTATE_ character varying(255),
        ASSIGNEE_ character varying(255),
        FORM_ character varying(255),
        PRIORITY_ INTEGER,
        CREATE_ timestamp,
        DUEDATE_ timestamp,
        PROGRESS_ INTEGER,
        SIGNALLING_ boolean,
        EXECUTION_ID_ character varying(255),
        ACTIVITY_NAME_ character varying(255),
        HASVARS_ boolean,
        SUPERTASK_ numeric (18,0),
        EXECUTION_ numeric (18,0),
        PROCINST_ numeric (18,0),
        SWIMLANE_ numeric (18,0),
        TASKDEFNAME_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_VARIABLE (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        KEY_ character varying(255),
        CONVERTER_ character varying(255),
        HIST_ boolean,
        EXECUTION_ numeric (18,0),
        TASK_ numeric (18,0),
        LOB_ numeric (18,0),
        DATE_VALUE_ timestamp,
        DOUBLE_VALUE_ double precision,
        CLASSNAME_ character varying(255),
        LONG_VALUE_ numeric (18,0),
        STRING_VALUE_ character varying(255),
        TEXT_VALUE_ text,
        EXESYS_ numeric (18,0),
        primary key (DBID_)
    );

    create index IDX_DEPLPROP_DEPL on JBPM4_DEPLOYPROP (DEPLOYMENT_);

    alter table JBPM4_DEPLOYPROP 
        add constraint FK_DEPLPROP_DEPL 
        foreign key (DEPLOYMENT_) 
        references JBPM4_DEPLOYMENT;

    create index IDX_EXEC_SUPEREXEC on JBPM4_EXECUTION (SUPEREXEC_);

    create index IDX_EXEC_INSTANCE on JBPM4_EXECUTION (INSTANCE_);

    create index IDX_EXEC_SUBPI on JBPM4_EXECUTION (SUBPROCINST_);

    create index IDX_EXEC_PARENT on JBPM4_EXECUTION (PARENT_);

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_PARENT 
        foreign key (PARENT_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_SUBPI 
        foreign key (SUBPROCINST_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_INSTANCE 
        foreign key (INSTANCE_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_SUPEREXEC 
        foreign key (SUPEREXEC_) 
        references JBPM4_EXECUTION;

    create index IDX_HACTI_HPROCI on JBPM4_HIST_ACTINST (HPROCI_);

    create index IDX_HTI_HTASK on JBPM4_HIST_ACTINST (HTASK_);

    alter table JBPM4_HIST_ACTINST 
        add constraint FK_HACTI_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_ACTINST 
        add constraint FK_HTI_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_HDET_HACTI on JBPM4_HIST_DETAIL (HACTI_);

    create index IDX_HDET_HPROCI on JBPM4_HIST_DETAIL (HPROCI_);

    create index IDX_HDET_HVAR on JBPM4_HIST_DETAIL (HVAR_);

    create index IDX_HDET_HTASK on JBPM4_HIST_DETAIL (HTASK_);

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HACTI 
        foreign key (HACTI_) 
        references JBPM4_HIST_ACTINST;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HVAR 
        foreign key (HVAR_) 
        references JBPM4_HIST_VAR;

    create index IDX_HSUPERT_SUB on JBPM4_HIST_TASK (SUPERTASK_);

    alter table JBPM4_HIST_TASK 
        add constraint FK_HSUPERT_SUB 
        foreign key (SUPERTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_HVAR_HPROCI on JBPM4_HIST_VAR (HPROCI_);

    create index IDX_HVAR_HTASK on JBPM4_HIST_VAR (HTASK_);

    alter table JBPM4_HIST_VAR 
        add constraint FK_HVAR_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_VAR 
        add constraint FK_HVAR_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_GROUP_PARENT on JBPM4_ID_GROUP (PARENT_);

    alter table JBPM4_ID_GROUP 
        add constraint FK_GROUP_PARENT 
        foreign key (PARENT_) 
        references JBPM4_ID_GROUP;

    create index IDX_MEM_USER on JBPM4_ID_MEMBERSHIP (USER_);

    create index IDX_MEM_GROUP on JBPM4_ID_MEMBERSHIP (GROUP_);

    alter table JBPM4_ID_MEMBERSHIP 
        add constraint FK_MEM_GROUP 
        foreign key (GROUP_) 
        references JBPM4_ID_GROUP;

    alter table JBPM4_ID_MEMBERSHIP 
        add constraint FK_MEM_USER 
        foreign key (USER_) 
        references JBPM4_ID_USER;

    create index IDX_JOBRETRIES on JBPM4_JOB (RETRIES_);

    create index IDX_JOB_CFG on JBPM4_JOB (CFG_);

    create index IDX_JOB_PRINST on JBPM4_JOB (PROCESSINSTANCE_);

    create index IDX_JOB_EXE on JBPM4_JOB (EXECUTION_);

    create index IDX_JOBLOCKEXP on JBPM4_JOB (LOCKEXPTIME_);

    create index IDX_JOBDUEDATE on JBPM4_JOB (DUEDATE_);

    alter table JBPM4_JOB 
        add constraint FK_JOB_CFG 
        foreign key (CFG_) 
        references JBPM4_LOB;

    create index IDX_LOB_DEPLOYMENT on JBPM4_LOB (DEPLOYMENT_);

    alter table JBPM4_LOB 
        add constraint FK_LOB_DEPLOYMENT 
        foreign key (DEPLOYMENT_) 
        references JBPM4_DEPLOYMENT;

    create index IDX_PART_TASK on JBPM4_PARTICIPATION (TASK_);

    alter table JBPM4_PARTICIPATION 
        add constraint FK_PART_SWIMLANE 
        foreign key (SWIMLANE_) 
        references JBPM4_SWIMLANE;

    alter table JBPM4_PARTICIPATION 
        add constraint FK_PART_TASK 
        foreign key (TASK_) 
        references JBPM4_TASK;

    create index IDX_SWIMLANE_EXEC on JBPM4_SWIMLANE (EXECUTION_);

    alter table JBPM4_SWIMLANE 
        add constraint FK_SWIMLANE_EXEC 
        foreign key (EXECUTION_) 
        references JBPM4_EXECUTION;

    create index IDX_TASK_SUPERTASK on JBPM4_TASK (SUPERTASK_);

    alter table JBPM4_TASK 
        add constraint FK_TASK_SWIML 
        foreign key (SWIMLANE_) 
        references JBPM4_SWIMLANE;

    alter table JBPM4_TASK 
        add constraint FK_TASK_SUPERTASK 
        foreign key (SUPERTASK_) 
        references JBPM4_TASK;

    create index IDX_VAR_EXESYS on JBPM4_VARIABLE (EXESYS_);

    create index IDX_VAR_TASK on JBPM4_VARIABLE (TASK_);

    create index IDX_VAR_EXECUTION on JBPM4_VARIABLE (EXECUTION_);

    create index IDX_VAR_LOB on JBPM4_VARIABLE (LOB_);

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_LOB 
        foreign key (LOB_) 
        references JBPM4_LOB;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_EXECUTION 
        foreign key (EXECUTION_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_EXESYS 
        foreign key (EXESYS_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_TASK 
        foreign key (TASK_) 
        references JBPM4_TASK;

create sequence dsg_external_dictionaries_id;

ALTER TABLE dso_in_document  ALTER COLUMN POSTALREGNUMBER TYPE character varying(25);
alter table dsg_centrum_kosztow_faktury add itemNo numeric (18,0);


CREATE TABLE dsg_external_dictionaries 
(
	id numeric (18,0),
	nazwa_polaczenia character varying(64),
	nazwa_slownika character varying(64),
	nazwa_kolumny_oryginalna character varying(64),
	nazwa_kolumny_uzytkownika character varying(64),
	typ_kolumny character varying(32),
	precyzja_kolumny character varying(16)
);
ALTER TABLE dso_in_document  ALTER COLUMN POSTALREGNUMBER TYPE character varying(25);




CREATE TABLE ds_epuap_skrytka
(
	id numeric (18,0),
	adres_skrytki character varying(300),
	nazwa_skrytki character varying(300),
	identyfikator_podmiotu character varying(300),
	available boolean default false
);
create sequence ds_epuap_skrytka_id start with 1 ;



ALTER TABLE ds_epuap_skrytka add param1 character varying(300);
ALTER TABLE ds_epuap_skrytka add param2 character varying(300);
ALTER TABLE dsg_external_dictionaries ADD nazwa_tabeli character varying(64);
alter table dsg_centrum_kosztow_faktury add additionalId integer ;
alter table dsg_centrum_kosztow_faktury add inventoryId integer ;

ALTER TABLE ds_epuap_export_document ADD epuap_skrytka int;

alter table DS_USER add isSupervisor boolean default false not null;
alter table dsg_centrum_kosztow_faktury add orderId int;
alter table dsg_centrum_kosztow_faktury add groupId int ;
alter table dsg_centrum_kosztow_faktury add productId int;



drop table DSO_USER_TO_BRIEFCASE;
CREATE TABLE DSO_USER_TO_BRIEFCASE(
	ID numeric (18,0) NOT NULL,
	containerId numeric (18,0) NOT NULL,
	containerOfficeId character varying(84) NOT NULL,
	userId numeric (18,0) NOT NULL,
	userName character varying(62),
	userFirstname character varying(62),
	userLastname character varying(62)
);


alter table DSO_USER_TO_BRIEFCASE add canView boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canCreate boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canModify boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canPrint boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canViewCases boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canAddCasesToBriefcase boolean not null;

create sequence DSO_USER_TO_BRIEFCASE_ID start with 1 ;
ALTER TABLE dso_rwa  ALTER COLUMN digit1 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit2 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit3 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit4 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit5 TYPE character varying(50);

ALTER TABLE ds_acceptance_division  ALTER COLUMN CODE TYPE character varying(30);

alter table dso_document_asgn_history add deadline character varying(10);
alter table dso_document_asgn_history add clerk character varying(62);
alter table dso_journal_entry add author character(50);
ALTER TABLE ds_epuap_skrytka add certyfikat_nazwa character varying(300);



ALTER TABLE DSW_JBPM_TASKLIST  ALTER COLUMN person_organization TYPE character varying(128);

ALTER TABLE DSO_IN_DOCUMENT ADD MESSAGE_ID numeric (18,0);

ALTER TABLE ds_document_acceptance  ALTER COLUMN acceptanceCn  set NOT NULL;
ALTER TABLE ds_document_acceptance  ALTER COLUMN acceptanceCn TYPE character varying(36);

ALTER TABLE ds_epuap_skrytka add certyfikat_haslo character varying(300);
ALTER TABLE ds_epuap_skrytka add email character varying(300);


ALTER TABLE dso_person  ALTER COLUMN organizationdivision TYPE character varying(128);

alter table ds_electronic_signature add is_timestamp_sign boolean;
update ds_electronic_signature set is_timestamp_sign = false;

ALTER TABLE DSW_TASKLIST ADD isAnyPdfInAttachments boolean;
ALTER TABLE DSW_JBPM_TASKLIST ADD isAnyPdfInAttachments boolean;



ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_IS_SSL  boolean default false not null;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_IS_TLS  boolean default false not null;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_PASSWORD character varying(100);
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_PORT int;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_USERNAME character varying(50);

ALTER TABLE DS_BOOKMARK ADD CN character varying(64);


CREATE TABLE DS_USER_ADDITIONAL_DATA(	
ID numeric (18,0), 
USER_ID numeric (18,0), 
MAIL_FOOTER text,
CONSTRAINT DS_USER_ADDITIONAL_DATA_FK FOREIGN KEY (USER_ID) REFERENCES DS_USER (ID)
);



CREATE SEQUENCE DS_USER_ADDITIONAL_DATA_ID START WITH 1 INCREMENT BY 1 ;

CREATE OR REPLACE FUNCTION insert_to_DS_USER_ADDITIONAL_DATA()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_USER_ADDITIONAL_DATA_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;


drop TRIGGER IF  EXISTS TRG_DS_USER_ADDITIONAL_DATA on DS_USER_ADDITIONAL_DATA;

 CREATE TRIGGER TRG_DS_USER_ADDITIONAL_DATA
 BEFORE INSERT ON DS_USER_ADDITIONAL_DATA
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_USER_ADDITIONAL_DATA();

COMMENT ON TABLE DS_USER_ADDITIONAL_DATA  IS 'Tabela przechowuje dodatkowe dane powi�zane z u�ytkownikiem';



alter table DSO_PERSON add externalID character varying(50);



CREATE TABLE sso (
idsso numeric  NOT NULL,
login_ad character varying(50) NOT NULL,
guid_source character varying(50) ,
guid_sso character varying(50) ,
is_active boolean ,
login_date DATE ,
redirect_url character varying(1000) ,
constraint PK_sso primary key (idsso)
);

CREATE SEQUENCE SEQ_SSO_IDSSO
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;


CREATE OR REPLACE FUNCTION insert_to_SSO()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('SEQ_SSO_IDSSO');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
 
 
 
drop TRIGGER IF  EXISTS TRG_insert_to_SSO on SSO;

 CREATE TRIGGER TRG_insert_to_SSO
 BEFORE INSERT ON SSO
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_SSO();
--629
alter table dsg_centrum_kosztow_faktury add acceptance_mode smallint;
alter table dsg_centrum_kosztow add acceptance_modes_binary int;
alter table dsg_centrum_kosztow_faktury add analytics_id int;
alter table dsg_centrum_kosztow_faktury add descriptionAmount character varying(600);


ALTER TABLE dso_person  ALTER COLUMN organization TYPE character varying(249);

ALTER TABLE DSO_PERSON  ALTER COLUMN COUNTRY TYPE character varying(3);
ALTER TABLE DSO_IN_DOCUMENT ADD  invoiceKind INTEGER;

alter table dso_label add cn character varying(30);

alter table ds_profile_to_user add USERKEY character varying(2000);
alter table ds_profile_to_user add ID numeric (18,0);

alter table ds_profile_to_user drop column ID;

--635--635

CREATE sequence dso_document_out_gauge_id 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;
CREATE TABLE dso_document_out_gauge (
  id numeric (18,0) NOT NULL,
  cn  character varying(20),
  title  character varying(20)
);
ALTER TABLE dso_out_document ADD gauge numeric (18,0);



CREATE OR REPLACE FUNCTION insert_to_GAUGE()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('dso_document_out_gauge_id');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;

drop TRIGGER IF  EXISTS TRG_dso_document_out_gauge on dso_document_out_gauge;
 CREATE TRIGGER TRG_dso_document_out_gauge
 BEFORE INSERT
 ON dso_document_out_gauge
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_GAUGE();
 
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('A', 'A');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('B', 'B');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('C', 'C');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('D', 'D');




alter table ds_electronic_signature add verifyResult boolean;
alter table ds_electronic_signature add verifyDate date ;
alter table ds_electronic_signature add baseSignatureId numeric (18,0);


--637


CREATE TABLE DS_PRINTERS(
	ID int,
	USERPRINTERNAME character varying(64),
	USEPRINTER boolean,
	PRINTERNAME character varying(64),
	FIRSTLINE character varying(64),
	SECONDLINE character varying(64),
	BARCODETYPE character varying(64),
	BARCODEVALUE character varying(64),
	LANGUAGE character varying(64)
);
commit;


CREATE TABLE DS_PRINTER_DEFINED_FIELDS
(
	PRINTER_ID INT,
	FIELD character varying(128)
);
commit;


CREATE sequence DS_PRINTERS_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;
commit;


CREATE OR REPLACE FUNCTION insert_to_DS_PRINTERS()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_PRINTERS_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
commit;

drop TRIGGER IF  EXISTS TRG_DS_PRINTERS_INSERT on DS_PRINTERS;
 CREATE  TRIGGER TRG_DS_PRINTERS_INSERT
 BEFORE INSERT
 ON DS_PRINTERS
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_PRINTERS();
commit;
 

 alter table ds_subst_hist add substituteCreator character varying(32);

UPDATE ds_subst_hist set substituteCreator = 'admin' where substituteCreator is null;

alter table ds_subst_hist
add status smallint;




UPDATE ds_subst_hist set status = 1;
---639
update ds_profile_to_user SET USERKEY='#' WHERE USERKEY IS NULL;
update ds_profile_to_user SET USERKEY='#' WHERE USERKEY='';

---640
ALTER TABLE DS_DOCUMENT ADD VERSION numeric (18,0) DEFAULT(1);
ALTER TABLE DS_DOCUMENT ADD VERSION_DESC character varying(60);
ALTER TABLE DS_DOCUMENT ADD CZY_CZYSTOPIS boolean;
ALTER TABLE DS_DOCUMENT ADD VERSION_GUID numeric (18,0);
ALTER TABLE DS_DOCUMENT ADD CZY_AKTUALNY boolean;
---641
UPDATE DS_DOCUMENT SET VERSION = 1 WHERE VERSION IS NULL;
UPDATE DS_DOCUMENT SET VERSION_GUID = ID WHERE VERSION_GUID IS NULL;
UPDATE DS_DOCUMENT SET CZY_CZYSTOPIS = false WHERE CZY_CZYSTOPIS IS NULL;
UPDATE DS_DOCUMENT SET CZY_AKTUALNY = true WHERE CZY_AKTUALNY IS NULL;
---642
alter table dso_person
add phoneNumber character varying(15);
---633
ALTER TABLE DSO_DOC_ASGN_HISTORY_TARGETS  ALTER COLUMN VALUE TYPE character varying(62);
---644

ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;

CREATE TABLE ds_jackrabbit_server (
  id SMALLINT NOT NULL PRIMARY KEY,
  status character varying(255) NOT NULL,
  internal_url character varying(255) NOT NULL,
  user_url character varying(255) NOT NULL,
  login character varying(255),
  password character varying(255),
  ignore_ssl_errors boolean DEFAULT false NOT NULL
);

CREATE SEQUENCE ds_jackrabbit_server_seq start with 1 ;


--645
ALTER TABLE DSO_PERSON  ALTER COLUMN STREET TYPE character varying(100);
---646
CREATE VIEW PROCESS_REPORT_VIEW (ID, DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

---akutalne procesy
select distinct
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,--- 'id',
	tVar.LONG_VALUE_									,--- 'document id',
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,---'status',
	tHisActInst.START_									,---'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,---'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  --- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_ AS character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

---oraz
UNION ALL

---hist. procesy
select
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,--- 'id',
	CAST(tHistVar.VALUE_ AS bigint)				,--- 'document id',
	
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 --- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tHistVar.HPROCI_ AS  character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';	



---647

CREATE TABLE DS_INTERNAL_SIGN(
	ID numeric (18,0) NOT NULL,
	USER_ID numeric (18,0) ,
	CTIME timestamp ,
	SIGNATURE character varying(256) 
);

CREATE SEQUENCE DS_INTERNAL_SIGN_SEQ start with 1 ;

CREATE TABLE DS_INT_TO_DOC(
	ID numeric (18,0) NOT NULL,
	SIGN_ID numeric (18,0) ,
	DOCUMENT_ID numeric (18,0) ,
	CTIME timestamp
);

CREATE SEQUENCE DS_INT_TO_DOC_SEQ start with 1 ;

---648
ALTER TABLE DS_DOCUMENT ADD isArchived boolean;
alter table DS_DOCUMENT add archivedDate timestamp;
---649

DROP VIEW PROCESS_REPORT_VIEW;


CREATE VIEW PROCESS_REPORT_VIEW (DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

---akutalne procesy
select distinct
	tVar.LONG_VALUE_									,--- 'document id',
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  --- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_   AS character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

---oraz
UNION

---hist. procesy
select distinct
	CAST(tHistVar.VALUE_ AS bigint)				,--- 'document id',
	SUBSTRING(tHisActInst.EXECUTION_, 0, (position('.' in tHisActInst.EXECUTION_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 --- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(CONCAT('%.',CAST(tHistVar.HPROCI_ AS character varying(255))), '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';
	
	
	

---650
alter table dso_in_document add contractId numeric (18,0);
alter table dso_out_document add countryLetter boolean;
---651
alter table ds_jackrabbit_server rename to ds_jcr_repository;
---652
alter table dso_email_channel add userLogin character varying(50);	
---653

create sequence DS_AR_UDOSTEPNIONE_ZASOBY_ID start with 1 ;

CREATE TABLE DS_AR_UDOSTEPNIONE_ZASOBY(
	ID numeric (18,0),
	USERNAME character varying(50),
	DATA_UDOSTEPNIENIA timestamp ,
	DATA_ZWROTU timestamp,
	RODZAJ character varying(20),
	ZASOB_ID numeric (18,0)
);
---654
CREATE TABLE ds_process_for_document(
  engine_id integer NOT NULL,
  process_id character varying(255) NOT NULL,
  document_id numeric (18,0) NOT NULL,
  definition_id character varying(255) NOT NULL,

  PRIMARY KEY (process_id, engine_id)
);

CREATE INDEX idx_process_for_documentDocDef ON ds_process_for_document (document_id, definition_id);

---655
alter table ds_document add accessToDocument numeric (18,0);

---- SEQUENCE
-- skrypt tworzacy sekwencje 
create sequence DS_PREFS_NODE_ID start with 1;
create sequence DS_DOCTYPE_ID start with 1;
create sequence DS_BOX_ID start with 1;
create sequence DS_ATTACHMENT_ID start with 1;
create sequence DS_ATTACHMENT_REVISION_ID start with 1;
create sequence DS_FOLDER_ID start with 1;
create sequence DS_DOCUMENT_ID start with 1;
create sequence DS_DOCUMENT_WATCH_ID start with 1;
create sequence DS_REPORT_ID start with 1;
create sequence DS_USER_ID start with 1;
create sequence DS_USER_CERTIFICATE_ID start with 1;
create sequence DS_DIVISION_ID start with 1;
create sequence DSO_RWA_ID start with 1;
create sequence DSO_IN_DOCUMENT_STATUS_ID start with 1;
create sequence DSO_IN_DOCUMENT_KIND_ID start with 1;
create sequence DSO_IN_DOCUMENT_DELIVERY_ID start with 1;
create sequence DSO_OUT_DOCUMENT_DELIVERY_ID start with 1;
create sequence DSO_OUT_DOCUMENT_RETREASON_ID start with 1;
create sequence DSO_JOURNAL_ID start with 1;
create sequence DSO_PERSON_ID start with 1;
create sequence DSO_ROLE_ID start with 1;
create sequence DSO_CASE_PRIORITY_ID start with 1;
create sequence DSO_CONTAINER_ID start with 1;
create sequence DSO_COLLECTIVE_ASSIGNMENT_ID start with 1;
create sequence DSO_ASSIGNMENT_OBJECTIVE_ID start with 1;
create sequence DSW_RESOURCE_ID start with 1;
create sequence DSW_PROCESS_ID start with 1;
create sequence DSW_ASSIGNMENT_ID start with 1;
create sequence DSW_ACTIVITY_ID start with 1;
create sequence DSW_DATATYPE_ID start with 1;
create sequence DSW_PACKAGE_ID start with 1;
create sequence DSE_CONTRACTOR_ID start with 1;
create sequence DSE_ADDRESS_ID start with 1;
create sequence DSE_HISTORY_ID start with 1;
create sequence DSE_CHANGE_EVENT_ID start with 1;
create sequence DSE_CLASSIFICATION_ID start with 1;
create sequence DSE_PROVINCE_ID start with 1;
create sequence DSE_OFFICE_ID start with 1;
create sequence DSE_STATUS_ID start with 1;
create sequence DSC_CERTIFICATE_ID start with 1;
create sequence DS_RETAINED_OBJECT_ID start with 1;
create sequence DS_RETAINED_ATTACHMENT_ID start with 1;
create sequence DAA_AGENT_ID start with 1;
create sequence DAA_AGENCJA_ID start with 1;
create sequence DSO_SCHOOL_ID start with 1;
create sequence DS_SUBST_HIST_ID start with 1;
create sequence DSW_TASKLIST_ID start with 1;
create sequence DS_MESSAGE_ID start with 1;
create sequence DS_MESSAGE_ATTACHMENT_ID start with 1;
create sequence DS_TIME_ID start with 1;
create sequence DSW_PROCESS_COORDINATOR_ID start with 1;
create sequence DS_DOCUMENT_KIND_ID start with 1;
create sequence DS_IMPORTED_DOCUMENT_COMMON_ID start with 1;
create sequence DS_IMPORTED_FILE_BLOB_ID start with 1;
create sequence DS_IMPORTED_FILE_CSV_ID start with 1;
create sequence DS_BOX_LINE_ID start with 1;
create sequence DSR_REGISTRY_ID start with 1;
create sequence DSR_REGISTRY_ENTRY_ID start with 1;
create sequence DS_ACCEPTANCE_CONDITION_ID start with 1;
create sequence DS_DOCUMENT_ACCEPTANCE_ID start with 1;
create sequence DSO_ADNOTATION_ID start with 1;
create sequence DSG_CENTRUM_KOSZTOW_ID start with 1;
create sequence DSG_CENTRUM_KOSZTOW_FAKTURY_ID start with 1;
create sequence DSW_TASK_HISTORY_ENTRY_ID start with 1;
create sequence DSC_CONTRACTOR_DICTIONARY_ID start with 1;
create sequence DSC_CONTRACTOR_ID start with 1;
create sequence DSC_WORKER_ID start with 1;
create sequence DSG_ACCOUNT_NUMBER_ID start with 1;
create sequence DS_LOG_ID start with 1;
create sequence DS_BOX_ORDER_ID start with 1;
create sequence DS_BOX_ORDER_HISTORY_ID start with 1;
create sequence DSR_CNS_DECISION_ARCHIVE_ID start with 1;
create sequence DS_PROFILE_ID start with 1;
create sequence DSO_LABEL_ID start with 1;
create sequence DSO_DOCUMENT_TO_LABEL_ID start with 1;
create sequence DS_POSTAL_CODE_ID start with 1;
create sequence HIBERNATE_SEQUENCE start with 1;
create sequence DSW_JBPM_TASKLIST_ID start with 1;
create sequence DS_LOCATION_ID start with 1;
create sequence DSO_BARCODE_RANGE_ID start with 1;
create sequence DSE_EVENT_ID start with 1;
create sequence DSO_CHANNEL_REQUEST_ID start with 1;
create sequence DS_NEW_REPORT_ID start with 1;
create sequence DS_DOCUMENT_SIGNATURE_ID start with 1;
create sequence DSR_CON_CONTRACT_ID start with 1;
create sequence DSR_CON_VENDOR_ID start with 1;
create sequence DS_ANONYMOUS_ACCESS_TICKET_ID start with 1;
create sequence DS_KEY_STORE_ID start with 1;
create sequence DS_DATAMART_ID start with 1;
create sequence DSO_DOCUMENT_REMARKS_ID start with 1;
create sequence DSO_INVOICES_KIND_ID start with 1;
create sequence DSO_EMAIL_CHANNEL_ID start with 1;
create sequence DSR_INV_INVOICE_ID start with 1;
create sequence DSR_INV_VENDOR_ID start with 1;
create sequence DSR_CON_CONTRACT_HISTORY_ID start with 1;
create sequence SEQ_DS_ADDRESS_ID start with 1;
create sequence SEQ_DS_FLOOR_ID start with 1;
create sequence SEQ_DS_PHONE_ID start with 1;
create sequence SEQ_DS_IMAGE_ID start with 1;
create sequence SEQ_DS_USER_LOCATION_ID start with 1;
create sequence SEQ_DS_NEWS_TYPE_ID start with 1;
create sequence SEQ_DS_NEWS_ID start with 1;
create sequence SEQ_DS_EMPLOYEE_CARD_ID start with 1;
create sequence DS_RESOURCES_TO_TIME_ID start with 1;
create sequence DS_ANY_RESOURCE_ID start with 1;
create sequence DS_CALENDAR_ID start with 1;
create sequence SEQ_DS_NOTIFICATIONS_ID start with 1;
create sequence SEQ_DS_ABSENCE_ID start with 1;
create sequence SEQ_DS_BILING_ID start with 1;
create sequence SEQ_DS_USER_PHONE_NUMBER_ID start with 1;
create sequence DS_CALENDAR_EVENT_ID start with 1;
create sequence DS_ACCEPTANCE_DIVISION_ID start with 1;
create sequence DS_EPUAP_EXPORT_DOCUMENT_ID start with 1;
create sequence SEQ_FILTER_CONDITION_ID start with 1;
create sequence SEQ_BOOKMARK_ID start with 1;
create sequence SEQ_DS_FREE_DAY_ID start with 1;
create sequence DSO_INVOICES_CPV_CODES_ID start with 1;
create sequence DSO_INVOICES_DECRETATION_ID start with 1;
create sequence DS_FAX_CACHE_ID start with 1;
create sequence DS_ELECTRONIC_SIGNATURE_ID start with 1;
create sequence DS_PROCESS_ACTION_ENTRY_ID start with 1;
create sequence SEQ_DSO_USER_TO_BRIEFCASE_ID start with 1;
create sequence DS_ELECTRONIC_SIGNATURE_SEQ start with 1;


---defaults 

ALTER TABLE ds_document_watch
   ALTER COLUMN watch_type SET  DEFAULT 1;


ALTER TABLE DSE_EVENT  
ALTER COLUMN EVENT_ID SET DEFAULT 1 ;

ALTER TABLE DSG_ACCOUNT_NUMBER  
ALTER COLUMN ACCOUNT_NUMBER SET DEFAULT 1 ;

ALTER TABLE DSG_CENTRUM_KOSZTOW
ALTER COLUMN IS_AUTO_ADDED SET DEFAULT false ;

ALTER TABLE DSG_CENTRUM_KOSZTOW
ALTER COLUMN IS_CONTRACT_NEEDED SET DEFAULT false ;

ALTER TABLE DSO_EMAIL_CHANNEL
ALTER COLUMN PORT SET DEFAULT -1 ;

ALTER TABLE DSO_EMAIL_CHANNEL
ALTER COLUMN MBOX SET DEFAULT 'INBOX' ;

ALTER TABLE DSO_EMAIL_CHANNEL
ALTER COLUMN IS_SSL SET DEFAULT false ;

ALTER TABLE DSO_EMAIL_CHANNEL
ALTER COLUMN IS_ENABLED SET DEFAULT true ;

ALTER TABLE DSO_IN_DOCUMENT
ALTER COLUMN ORIGINAL SET DEFAULT true ;

ALTER TABLE DSW_ASSIGNMENT
ALTER COLUMN RESOURCE_COUNT SET DEFAULT 1 ;

ALTER TABLE DSW_TASKLIST
ALTER COLUMN RESOURCE_COUNT SET DEFAULT 1 ;

ALTER TABLE DS_ABSENCE_TYPE
ALTER COLUMN FLAG SET DEFAULT false ;

ALTER TABLE DS_ADDRESS
ALTER COLUMN IS_HEAD_OFFICE SET DEFAULT false ;

ALTER TABLE DS_ATTACHMENT_REVISION
ALTER COLUMN ONDISC SET DEFAULT 0 ;

ALTER TABLE DS_ATTACHMENT_REVISION
ALTER COLUMN INDEX_STATUS_ID SET DEFAULT false ;

ALTER TABLE DS_BOOKMARK  
ALTER COLUMN ACTIVE SET DEFAULT 0 ;

ALTER TABLE DS_BOOKMARK  
ALTER COLUMN KIND SET DEFAULT 0 ;

ALTER TABLE DS_BOX_ORDER  
ALTER COLUMN ORDERASPECT SET DEFAULT 'SET DEFAULT' ;

ALTER TABLE DS_CALENDAR_EVENT  
ALTER COLUMN PERIODIC SET DEFAULT false ;

ALTER TABLE DS_CALENDAR_EVENT  
ALTER COLUMN PERIODKIND SET DEFAULT 0 ;

ALTER TABLE DS_DOCUMENT  
ALTER COLUMN INDEX_STATUS_ID SET DEFAULT 0 ;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL1 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL2 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL3 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL4 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL5 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL6 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL7 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL8 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL9 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_FLAGS  
ALTER COLUMN FL10 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL1 SET DEFAULT false;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL2 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL3 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL4 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL5 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL6 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL7 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL8 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL9 SET DEFAULT false ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS  
ALTER COLUMN FL10 SET DEFAULT false ;

ALTER TABLE DS_FILTER_CONDITION  
ALTER COLUMN ACTIVE SET DEFAULT 1 ;

ALTER TABLE DS_NEWS  
ALTER COLUMN DELETED SET DEFAULT false ;

ALTER TABLE DS_NEWS_TYPE  
ALTER COLUMN DISABLED SET DEFAULT false ;

ALTER TABLE DS_USER  
ALTER COLUMN MUST_CHANGE_PASSWORD SET DEFAULT false ;

ALTER TABLE DS_USER  
ALTER COLUMN ACTIVE_DIRECTORY_USER SET DEFAULT false ;

ALTER TABLE DS_USER  
ALTER COLUMN AUTHLEVEL SET DEFAULT 0 ;

ALTER TABLE DS_USER_CALENDAR  
ALTER COLUMN COLORID SET DEFAULT 0 ;

--- klucze obce 

-- skrypt tworzacy Klucze Obce 
ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_CRTETIMERACT_TA FOREIGN KEY (TIMERACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_ACTION_ACTNDEL FOREIGN KEY (ACTIONDELEGATION_) REFERENCES JBPM_DELEGATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_ACTION_EXPTHDL FOREIGN KEY (EXCEPTIONHANDLER_) REFERENCES JBPM_EXCEPTIONHANDLER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_ACTION_EVENT FOREIGN KEY (EVENT_) REFERENCES JBPM_EVENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_ACTION_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ACTION ADD CONSTRAINT FK_ACTION_REFACT FOREIGN KEY (REFERENCEDACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_BYTEARRAY ADD CONSTRAINT FK_BYTEARR_FILDEF FOREIGN KEY (FILEDEFINITION_) REFERENCES JBPM_MODULEDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_BYTEBLOCK ADD CONSTRAINT FK_BYTEBLOCK_FILE FOREIGN KEY (PROCESSFILE_) REFERENCES JBPM_BYTEARRAY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_COMMENT ADD CONSTRAINT FK_COMMENT_TSK FOREIGN KEY (TASKINSTANCE_) REFERENCES JBPM_TASKINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_COMMENT ADD CONSTRAINT FK_COMMENT_TOKEN FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_DELEGATION ADD CONSTRAINT FK_DELEGATION_PRCD FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_DECISIONCONDITIONS ADD CONSTRAINT FK_DECCOND_DEC FOREIGN KEY (DECISION_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_EVENT ADD CONSTRAINT FK_EVENT_TASK FOREIGN KEY (TASK_) REFERENCES JBPM_TASK ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_EVENT ADD CONSTRAINT FK_EVENT_NODE FOREIGN KEY (NODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_EVENT ADD CONSTRAINT FK_EVENT_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_EVENT ADD CONSTRAINT FK_EVENT_TRANS FOREIGN KEY (TRANSITION_) REFERENCES JBPM_TRANSITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ID_MEMBERSHIP ADD CONSTRAINT FK_ID_MEMSHIP_GRP FOREIGN KEY (GROUP_) REFERENCES JBPM_ID_GROUP ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ID_MEMBERSHIP ADD CONSTRAINT FK_ID_MEMSHIP_USR FOREIGN KEY (USER_) REFERENCES JBPM_ID_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_ID_GROUP ADD CONSTRAINT FK_ID_GRP_PARENT FOREIGN KEY (PARENT_) REFERENCES JBPM_ID_GROUP ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_JOB ADD CONSTRAINT FK_JOB_NODE FOREIGN KEY (NODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_JOB ADD CONSTRAINT FK_JOB_TOKEN FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_JOB ADD CONSTRAINT FK_JOB_TSKINST FOREIGN KEY (TASKINSTANCE_) REFERENCES JBPM_TASKINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_JOB ADD CONSTRAINT FK_JOB_ACTION FOREIGN KEY (ACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_JOB ADD CONSTRAINT FK_JOB_PRINST FOREIGN KEY (PROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_OLDBYTES FOREIGN KEY (OLDBYTEARRAY_) REFERENCES JBPM_BYTEARRAY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_NEWBYTES FOREIGN KEY (NEWBYTEARRAY_) REFERENCES JBPM_BYTEARRAY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_NODE FOREIGN KEY (NODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_TOKEN FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_PARENT FOREIGN KEY (PARENT_) REFERENCES JBPM_LOG ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_VARINST FOREIGN KEY (VARIABLEINSTANCE_) REFERENCES JBPM_VARIABLEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_ACTION FOREIGN KEY (ACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_TASKINST FOREIGN KEY (TASKINSTANCE_) REFERENCES JBPM_TASKINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_CHILDTOKEN FOREIGN KEY (CHILD_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_SWIMINST FOREIGN KEY (SWIMLANEINSTANCE_) REFERENCES JBPM_SWIMLANEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_TRANSITION FOREIGN KEY (TRANSITION_) REFERENCES JBPM_TRANSITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_SOURCENODE FOREIGN KEY (SOURCENODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_LOG ADD CONSTRAINT FK_LOG_DESTNODE FOREIGN KEY (DESTINATIONNODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_MODULEDEFINITION ADD CONSTRAINT FK_MODDEF_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_MODULEDEFINITION ADD CONSTRAINT FK_TSKDEF_START FOREIGN KEY (STARTTASK_) REFERENCES JBPM_TASK ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_MODULEINSTANCE ADD CONSTRAINT FK_MODINST_PRCINST FOREIGN KEY (PROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_MODULEINSTANCE ADD CONSTRAINT FK_TASKMGTINST_TMD FOREIGN KEY (TASKMGMTDEFINITION_) REFERENCES JBPM_MODULEDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_PROCST_SBPRCDEF FOREIGN KEY (SUBPROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_NODE_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_NODE_SUPERSTATE FOREIGN KEY (SUPERSTATE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_NODE_SCRIPT FOREIGN KEY (SCRIPT_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_NODE_ACTION FOREIGN KEY (ACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_NODE ADD CONSTRAINT FK_DECISION_DELEG FOREIGN KEY (DECISIONDELEGATION) REFERENCES JBPM_DELEGATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_PROCESSDEFINITION ADD CONSTRAINT FK_PROCDEF_STRTSTA FOREIGN KEY (STARTSTATE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_PROCESSINSTANCE ADD CONSTRAINT FK_PROCIN_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_PROCESSINSTANCE ADD CONSTRAINT FK_PROCIN_ROOTTKN FOREIGN KEY (ROOTTOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_PROCESSINSTANCE ADD CONSTRAINT FK_PROCIN_SPROCTKN FOREIGN KEY (SUPERPROCESSTOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_POOLEDACTOR ADD CONSTRAINT FK_POOLEDACTOR_SLI FOREIGN KEY (SWIMLANEINSTANCE_) REFERENCES JBPM_SWIMLANEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_RUNTIMEACTION ADD CONSTRAINT FK_RTACTN_ACTION FOREIGN KEY (ACTION_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_RUNTIMEACTION ADD CONSTRAINT FK_RTACTN_PROCINST FOREIGN KEY (PROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_SWIMLANE ADD CONSTRAINT FK_SWL_ASSDEL FOREIGN KEY (ASSIGNMENTDELEGATION_) REFERENCES JBPM_DELEGATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_SWIMLANE ADD CONSTRAINT FK_SWL_TSKMGMTDEF FOREIGN KEY (TASKMGMTDEFINITION_) REFERENCES JBPM_MODULEDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_SWIMLANEINSTANCE ADD CONSTRAINT FK_SWIMLANEINST_SL FOREIGN KEY (SWIMLANE_) REFERENCES JBPM_SWIMLANE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_SWIMLANEINSTANCE ADD CONSTRAINT FK_SWIMLANEINST_TM FOREIGN KEY (TASKMGMTINSTANCE_) REFERENCES JBPM_MODULEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_STARTST FOREIGN KEY (STARTSTATE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_SWIMLANE FOREIGN KEY (SWIMLANE_) REFERENCES JBPM_SWIMLANE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_TASKNODE FOREIGN KEY (TASKNODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_TASKMGTDEF FOREIGN KEY (TASKMGMTDEFINITION_) REFERENCES JBPM_MODULEDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TASK_ASSDEL FOREIGN KEY (ASSIGNMENTDELEGATION_) REFERENCES JBPM_DELEGATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASK ADD CONSTRAINT FK_TSK_TSKCTRL FOREIGN KEY (TASKCONTROLLER_) REFERENCES JBPM_TASKCONTROLLER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKACTORPOOL ADD CONSTRAINT FK_TSKACTPOL_PLACT FOREIGN KEY (POOLEDACTOR_) REFERENCES JBPM_POOLEDACTOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKACTORPOOL ADD CONSTRAINT FK_TASKACTPL_TSKI FOREIGN KEY (TASKINSTANCE_) REFERENCES JBPM_TASKINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKCONTROLLER ADD CONSTRAINT FK_TSKCTRL_DELEG FOREIGN KEY (TASKCONTROLLERDELEGATION_) REFERENCES JBPM_DELEGATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT FK_TSKINS_PRCINS FOREIGN KEY (PROCINST_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT FK_TASKINST_SLINST FOREIGN KEY (SWIMLANINSTANCE_) REFERENCES JBPM_SWIMLANEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT FK_TASKINST_TMINST FOREIGN KEY (TASKMGMTINSTANCE_) REFERENCES JBPM_MODULEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT FK_TASKINST_TOKEN FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TASKINSTANCE ADD CONSTRAINT FK_TASKINST_TASK FOREIGN KEY (TASK_) REFERENCES JBPM_TASK ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKEN ADD CONSTRAINT FK_TOKEN_PARENT FOREIGN KEY (PARENT_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKEN ADD CONSTRAINT FK_TOKEN_NODE FOREIGN KEY (NODE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKEN ADD CONSTRAINT FK_TOKEN_PROCINST FOREIGN KEY (PROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKEN ADD CONSTRAINT FK_TOKEN_SUBPI FOREIGN KEY (SUBPROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKENVARIABLEMAP ADD CONSTRAINT FK_TKVARMAP_TOKEN FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TOKENVARIABLEMAP ADD CONSTRAINT FK_TKVARMAP_CTXT FOREIGN KEY (CONTEXTINSTANCE_) REFERENCES JBPM_MODULEINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TRANSITION ADD CONSTRAINT FK_TRANSITION_FROM FOREIGN KEY (FROM_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TRANSITION ADD CONSTRAINT FK_TRANSITION_TO FOREIGN KEY (TO_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_TRANSITION ADD CONSTRAINT FK_TRANS_PROCDEF FOREIGN KEY (PROCESSDEFINITION_) REFERENCES JBPM_PROCESSDEFINITION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEACCESS ADD CONSTRAINT FK_VARACC_PROCST FOREIGN KEY (PROCESSSTATE_) REFERENCES JBPM_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEACCESS ADD CONSTRAINT FK_VARACC_SCRIPT FOREIGN KEY (SCRIPT_) REFERENCES JBPM_ACTION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEACCESS ADD CONSTRAINT FK_VARACC_TSKCTRL FOREIGN KEY (TASKCONTROLLER_) REFERENCES JBPM_TASKCONTROLLER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT FK_BYTEINST_ARRAY FOREIGN KEY (BYTEARRAYVALUE_) REFERENCES JBPM_BYTEARRAY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT FK_VARINST_TK FOREIGN KEY (TOKEN_) REFERENCES JBPM_TOKEN ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT FK_VARINST_TKVARMP FOREIGN KEY (TOKENVARIABLEMAP_) REFERENCES JBPM_TOKENVARIABLEMAP ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT FK_VARINST_PRCINST FOREIGN KEY (PROCESSINSTANCE_) REFERENCES JBPM_PROCESSINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE JBPM_VARIABLEINSTANCE ADD CONSTRAINT FK_VAR_TSKINST FOREIGN KEY (TASKINSTANCE_) REFERENCES JBPM_TASKINSTANCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_DATATYPE_ENUM_VALUES ADD CONSTRAINT DSW_DATATYPE_ENUM_VALUES_FK FOREIGN KEY (DATATYPE_ID) REFERENCES DSW_DATATYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PARTICIPANT_MAPPING ADD CONSTRAINT DSW_PARTICIPANT_MAPPING_RES_FK FOREIGN KEY (RESOURCE_ID) REFERENCES DSW_RESOURCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_ACTIVITY ADD CONSTRAINT DSW_ACTIVITY_FK FOREIGN KEY (PROCESS_ID) REFERENCES DSW_PROCESS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_ACTIVITY_CONTEXT ADD CONSTRAINT DSW_ACTIVITY_CONTEXT_FK FOREIGN KEY (ACTIVITY_ID) REFERENCES DSW_ACTIVITY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_ACTIVITY_CONTEXT ADD CONSTRAINT DSW_ACTIVITY_CONTEXT_DT_FK FOREIGN KEY (DATATYPE_ID) REFERENCES DSW_DATATYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCTYPE_FK FOREIGN KEY (DOCTYPE) REFERENCES DS_DOCTYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCUMENT_FK FOREIGN KEY (FOLDER_ID) REFERENCES DS_FOLDER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCUMENT_BOX FOREIGN KEY (BOX_ID) REFERENCES DS_BOX ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT_PERMISSION ADD CONSTRAINT DS_DOCUMENT_PERMISSION_FK FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE CASCADE ;

ALTER TABLE DS_DOCUMENT_CHANGELOG ADD CONSTRAINT DS_DOCUMENT_CHANGELOG_FK FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT_FLAGS ADD CONSTRAINT DS_DOCUMENT_FLAGS_DOCID FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_DOCUMENT_USER_FLAGS ADD CONSTRAINT DS_DOCUMENT_USER_FLAGS_DOCID FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_ADDRESS ADD CONSTRAINT CONTR_ADDR_FK FOREIGN KEY (CONTRACTOR_ID) REFERENCES DSE_CONTRACTOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ADDRESS ADD CONSTRAINT FK_DS_ADDRESS_IMAGE_ID FOREIGN KEY (IMAGE_ID) REFERENCES DS_IMAGE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PREFS_NODE ADD CONSTRAINT DS_PREFS_NODE_FK FOREIGN KEY (PARENT_ID) REFERENCES DS_PREFS_NODE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PREFS_VALUE ADD CONSTRAINT DS_PREFS_VALUE_FK FOREIGN KEY (NODE_ID) REFERENCES DS_PREFS_NODE ON UPDATE RESTRICT ON DELETE CASCADE ;

ALTER TABLE DS_USER ADD CONSTRAINT FK_DS_USER_IMAGE_ID FOREIGN KEY (IMAGE_ID) REFERENCES DS_IMAGE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER ADD CONSTRAINT FK_DS_USER_USER_LOCATION_ID FOREIGN KEY (USER_LOCATION_ID) REFERENCES DS_USER_LOCATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_CERT ADD CONSTRAINT DS_USER_CERT_FK FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_LOCATION ADD CONSTRAINT FK_DS_USER_LOCATION_FLOOR_ID FOREIGN KEY (FLOOR_ID) REFERENCES DS_FLOOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_TO_ARCHIVE_ROLE ADD CONSTRAINT DS_USER_TO_ARCHIVE_ROLE_FK1 FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_TO_DIVISION ADD CONSTRAINT DS_USER_TO_DIVISION_D FOREIGN KEY (DIVISION_ID) REFERENCES DS_DIVISION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_TO_DIVISION ADD CONSTRAINT DS_USER_TO_DIVISION_U FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_SUPERVISOR ADD CONSTRAINT FK_DS_USER_SUP_SUPERVISOR_ID FOREIGN KEY (SUPERVISOR_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_USER_SUPERVISOR ADD CONSTRAINT FK_DS_USER_SUP_USER_ID FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_OFFICE ADD CONSTRAINT PROV_OFF_FK FOREIGN KEY (PROVINCE_ID) REFERENCES DSE_PROVINCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_HISTORY ADD CONSTRAINT CONTR_HIST_FK FOREIGN KEY (CONTRACTOR_ID) REFERENCES DSE_CONTRACTOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_CLASSIFICATION_CONTRACTOR ADD CONSTRAINT CLASS_CONTR_FK FOREIGN KEY (CLASSIFICATION_ID) REFERENCES DSE_CLASSIFICATION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_CLASSIFICATION_CONTRACTOR ADD CONSTRAINT CONTR_CLASS_FK FOREIGN KEY (CONTRACTOR_ID) REFERENCES DSE_CONTRACTOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_FOLDER_PERMISSION ADD CONSTRAINT DS_FOLDER_PERMISSION_FK FOREIGN KEY (FOLDER_ID) REFERENCES DS_FOLDER ON UPDATE RESTRICT ON DELETE CASCADE ;

ALTER TABLE DSO_IN_DOCUMENT ADD CONSTRAINT DSO_IN_DOCUMENT_STATUS_FK FOREIGN KEY (STATUS) REFERENCES DSO_IN_DOCUMENT_STATUS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_IN_DOCUMENT ADD CONSTRAINT DSO_IN_DOCUMENT_DELIVERY_FK FOREIGN KEY (DELIVERY) REFERENCES DSO_IN_DOCUMENT_DELIVERY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_IN_DOCUMENT ADD CONSTRAINT DSO_IN_DOCUMENT_KIND_FK FOREIGN KEY (KIND) REFERENCES DSO_IN_DOCUMENT_KIND ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_IN_DOCUMENT ADD CONSTRAINT DSO_IN_DOCUMENT_OUTDELIVERY_FK FOREIGN KEY (OUTGOINGDELIVERY) REFERENCES DSO_OUT_DOCUMENT_DELIVERY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_IN_DOCUMENT_KIND_REQ_ATT ADD CONSTRAINT DSO_IN_DOC_KIND_REQ_ATT_FK FOREIGN KEY (KIND_ID) REFERENCES DSO_IN_DOCUMENT_KIND ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSR_INV_INVOICE ADD CONSTRAINT DSR_INV_INVOICE_VENDOR_FK FOREIGN KEY (VENDOR_ID) REFERENCES DSR_INV_VENDOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSR_INV_INVOICE_AUDIT ADD CONSTRAINT DSR_INV_INVOICE_AUDIT_FK1 FOREIGN KEY (INVOICE_ID) REFERENCES DSR_INV_INVOICE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_CONTRACTOR ADD CONSTRAINT STATUS_CONTR_FK FOREIGN KEY (STATUS_ID) REFERENCES DSE_STATUS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_CONTRACTOR ADD CONSTRAINT OFF_CONTR_NIP_FK FOREIGN KEY (NIP_DECL_ADDRESSEE_ID) REFERENCES DSE_OFFICE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_CONTRACTOR ADD CONSTRAINT OFF_CONTR_REG_FK FOREIGN KEY (REGON_DECL_ADDRESSEE_ID) REFERENCES DSE_OFFICE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRECEDENT FOREIGN KEY (PRECEDENT_ID) REFERENCES DSO_CONTAINER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PARENT FOREIGN KEY (PARENT_ID) REFERENCES DSO_CONTAINER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRIORITY FOREIGN KEY (PRIORITY_ID) REFERENCES DSO_CASE_PRIORITY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_RWA FOREIGN KEY (RWA_ID) REFERENCES DSO_RWA ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_STATUS FOREIGN KEY (STATUS_ID) REFERENCES DSO_CASE_STATUS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER_REMARKS ADD CONSTRAINT DSO_CONTAINER_REMARKS_FK1 FOREIGN KEY (CONTAINER_ID) REFERENCES DSO_CONTAINER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_CONTAINER_AUDIT ADD CONSTRAINT DSO_CONTAINER_AUDIT_FK1 FOREIGN KEY (CONTAINER_ID) REFERENCES DSO_CONTAINER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSR_CON_CONTRACT ADD CONSTRAINT DSR_CON_CONTRACT_VENDOR_FK FOREIGN KEY (VENDOR_ID) REFERENCES DSR_CON_VENDOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSR_CON_CONTRACT_AUDIT ADD CONSTRAINT DSR_CON_CONTRACT_AUDIT_FK1 FOREIGN KEY (CONTRACT_ID) REFERENCES DSR_CON_CONTRACT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSR_CON_CONTRACT_HISTORY ADD CONSTRAINT DSR_CON_CONTRACT_HISTORY_FK FOREIGN KEY (CONTRACT_ID) REFERENCES DSR_CON_CONTRACT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_DOCUMENT_AUDIT ADD CONSTRAINT DSO_DOCUMENT_AUDIT_FK1 FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_DOCUMENT_ASGN_HISTORY ADD CONSTRAINT DSO_DOCUMENT_ASGN_HISTORY_FK FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_DOCUMENT_REMARKS ADD CONSTRAINT DSO_DOCUMENT_REMARKS_FK FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PHONE ADD CONSTRAINT FK_DS_PHONE_ADDRESS_ID FOREIGN KEY (ADDRESS_ID) REFERENCES DS_ADDRESS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_JOURNAL_ENTRY ADD CONSTRAINT DSO_JOURNAL_ENTRY_FK FOREIGN KEY (JOURNAL_ID) REFERENCES DSO_JOURNAL ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_FLOOR ADD CONSTRAINT FK_DS_FLOOR_IMAGE_ID FOREIGN KEY (IMAGE_ID) REFERENCES DS_IMAGE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_FLOOR ADD CONSTRAINT FK_DS_FLOOR_ADDRESS_ID FOREIGN KEY (ADDRESS_ID) REFERENCES DS_ADDRESS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_ROLE ADD CONSTRAINT DS_PROFILE_TO_ROLE_FK1 FOREIGN KEY (PROFILE_ID) REFERENCES DS_PROFILE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_ROLE ADD CONSTRAINT DS_PROFILE_TO_ROLE_FK2 FOREIGN KEY (ROLE_ID) REFERENCES DSO_ROLE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_ARCHIVE_ROLE ADD CONSTRAINT DS_PROFILE_TO_ARCHIVE_ROLE_FK1 FOREIGN KEY (PROFILE_ID) REFERENCES DS_PROFILE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_USER ADD CONSTRAINT DS_PROFILE_TO_USER_FK1 FOREIGN KEY (PROFILE_ID) REFERENCES DS_PROFILE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_USER ADD CONSTRAINT DS_PROFILE_TO_USER_FK2 FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_GROUP ADD CONSTRAINT DS_PROFILE_TO_GROUP_FK1 FOREIGN KEY (PROFILE_ID) REFERENCES DS_PROFILE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_PROFILE_TO_GROUP ADD CONSTRAINT DS_PROFILE_TO_GROUP_FK2 FOREIGN KEY (DIVISION_ID) REFERENCES DS_DIVISION ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_ROLE_DIVISIONS ADD CONSTRAINT DSO_ROLE_DIVISIONS_FK FOREIGN KEY (ROLE_ID) REFERENCES DSO_ROLE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_ROLE_PERMISSIONS ADD CONSTRAINT DSO_ROLE_PERMISSIONS_FK FOREIGN KEY (ROLE_ID) REFERENCES DSO_ROLE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_ROLE_USERNAMES ADD CONSTRAINT DSO_ROLE_USERNAMES_FK FOREIGN KEY (ROLE_ID) REFERENCES DSO_ROLE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_BRANCH_CONTRACTOR ADD CONSTRAINT CONTR_BR_FK FOREIGN KEY (CONTRACTOR_ID) REFERENCES DSE_CONTRACTOR ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSE_BRANCH_CONTRACTOR ADD CONSTRAINT BR_CONTR_FK FOREIGN KEY (BRANCH_ID) REFERENCES DSE_BRANCH ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PROCESS ADD CONSTRAINT DSW_PROCESS_PACKAGE_FK FOREIGN KEY (PACKAGE_ID) REFERENCES DSW_PACKAGE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PROCESS_ACTIVITIES ADD CONSTRAINT DSW_PROCESS_ACTIVITIES_PROC_FK FOREIGN KEY (PROCESS_ID) REFERENCES DSW_PROCESS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PROCESS_ACTIVITIES ADD CONSTRAINT DSW_PROCESS_ACTIVITIES_ACT_FK FOREIGN KEY (ACTIVITY_ID) REFERENCES DSW_ACTIVITY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PROCESS_CONTEXT ADD CONSTRAINT DSW_PROCESS_CONTEXT_FK FOREIGN KEY (PROCESS_ID) REFERENCES DSW_PROCESS ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_PROCESS_CONTEXT ADD CONSTRAINT DSW_PROCESS_CONTEXT_DT_FK FOREIGN KEY (DATATYPE_ID) REFERENCES DSW_DATATYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_ASSIGNMENT ADD CONSTRAINT DSW_ASSIGNMENT_ACTIVITY_FK FOREIGN KEY (ACTIVITY_ID) REFERENCES DSW_ACTIVITY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSW_ASSIGNMENT ADD CONSTRAINT DSW_ASSIGNMENT_RESOURCE_FK FOREIGN KEY (RESOURCE_ID) REFERENCES DSW_RESOURCE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ABSENCE ADD CONSTRAINT FK_DS_ABSENCE_ABSENCE_TYPE FOREIGN KEY (ABSENCE_TYPE) REFERENCES DS_ABSENCE_TYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ABSENCE ADD CONSTRAINT FK_DS_ABSENCE_EMPLOYEE_ID FOREIGN KEY (EMPLOYEE_ID) REFERENCES DS_EMPLOYEE_CARD ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ABSENCE ADD CONSTRAINT FK_DS_ABSENCE_USER_ID FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_MESSAGE ADD CONSTRAINT DS_MESSAGE_EMAIL_CHANNEL_FK FOREIGN KEY (EMAIL_CHANNEL_ID) REFERENCES DSO_EMAIL_CHANNEL ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_RETAINED_ATTACHMENT ADD CONSTRAINT DS_RETAINED_OBJECT_FK FOREIGN KEY (OBJECT_ID) REFERENCES DS_RETAINED_OBJECT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_NOTIFICATIONS ADD CONSTRAINT FK_DS_NOTIFICATIONS_USER_ID FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ATTACHMENT ADD CONSTRAINT DS_ATTACHMENT_FK FOREIGN KEY (DOCUMENT_ID) REFERENCES DS_DOCUMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ATTACHMENT_REVISION ADD CONSTRAINT DS_ATTACHMENT_REVISION_FK FOREIGN KEY (ATTACHMENT_ID) REFERENCES DS_ATTACHMENT ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_OUT_DOCUMENT ADD CONSTRAINT DSO_OUT_DOCUMENT_DELIVERY_FK FOREIGN KEY (DELIVERY) REFERENCES DSO_OUT_DOCUMENT_DELIVERY ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DSO_OUT_DOCUMENT ADD CONSTRAINT DSO_OUT_DOCUMENT_RETREASON_FK FOREIGN KEY (RETURNREASON) REFERENCES DSO_OUT_DOCUMENT_RETREASON ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_NEWS ADD CONSTRAINT FK_DS_NEWS_USER_ID FOREIGN KEY (USER_ID) REFERENCES DS_USER ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_NEWS ADD CONSTRAINT FK_DS_NEWS_NEWS_TYPE_ID FOREIGN KEY (NEWS_TYPE_ID) REFERENCES DS_NEWS_TYPE ON UPDATE RESTRICT ON DELETE RESTRICT ;

ALTER TABLE DS_ANY_RESOURCE ADD CONSTRAINT DS_ANY_RESOURCE_CAL FOREIGN KEY (CALENDARID) REFERENCES DS_CALENDAR ON UPDATE RESTRICT ON DELETE RESTRICT ;



insert into DS_DOCUSAFE (VAR_NAME,VAR_VALUE) values ('APP_VERSION', '655');
INSERT INTO ds_user(
            id, name, firstname, lastname, email, identifier, logindisabled, 
            userroles, lastsuccessfullogin, lastunsuccessfullogin, substitutedfrom, 
            substitutedthrough, deleted, certificatelogin, lastpasswordchange, 
            hashedpassword, supervisor_id, substitute_id, systemuser, incorrectlogins, 
            mobile_phone_num, phone_num, extension, communicator_num, ctime, 
            externalname, kpx, pesel, validity_date, remarks, location, roomnum, 
            must_change_password, active_directory_user, domain_prefix, image_id, 
            post_description, user_location_id, about_me, sites, limitmobile, 
            limitphone, authlevel, ad_url, internal_number, issupervisor)
    VALUES (1,'admin','Administrator','Systemu',' ',' ',FALSE,'admin',null,null,null,null,FALSE,FALSE
   ,null,'{SHA}HfaN/uzfyGFRyA0n8L1ZH0aos2Y=',null,null,FALSE,null,   
     '  ',  '  ',  '  ',  '  ',  null,  '  ',  '  ',  '  ',  null,  '  ',
	0,  '  ',false,false  ,'  ', null,   '  ',null,null,null, 
 null,null,0,  '  ',  '  ',false
);

INSERT INTO DS_DIVISION (ID, GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, HIDDEN, DESCRIPTION, EXTERNALID) VALUES ('1', 'rootdivision', 'Dzia� g��wny', 'G', 'division', NULL, '0', NULL, NULL);
SELECT setval('public.ds_division_id', 2, true);


0;"G��wny";;0;"2005-12-13 14:23:34";"admin";"";1;"";;
1;"Dokumenty usuni�te";0;0;"2005-12-13 14:23:34";"admin";"trash";1;"";;
2;"Dokumenty kancelaryjne";0;0;"2005-12-13 14:23:34";"admin";"office";1;"";;
3;"Dokumenty przychodz�ce";0;0;"2013-11-20 15:09:58.232";"admin";"";0;"";;
4;"Dokumenty Wewn�trzne";0;0;"2013-11-21 16:38:52.441";"admin";"";0;"";;

INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (0, 'G��wny', null, 0, current_timestamp, 'admin', null,
1);


INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (1, 'Dokumenty kancelaryjne', 0, 0, current_timestamp, 'admin' ,'office',1);

--INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
--VALUES (2, 'Dokumenty usuni�te', 0, 0, current_timestamp, 'admin','trash',1);



INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (0, '*', 'ANY',
'READ', false);

INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (1, '*', 'ANY',
'READ', false);

INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (2, '*', 'ANY',
'READ', false);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED,CLOSED,CYEAR)
VALUES (0, 1, 'Dziennik g��wny','INCOMING', 1, 'admin', current_timestamp, false,false,2013);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED,CLOSED,CYEAR)
VALUES (1, 1, 'Dziennik g��wny','OUTGOING}', 1, 'admin', current_timestamp, false,false,2013);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED,CLOSED,CYEAR)
VALUES (2, 1, 'Dziennik g��wny','internal', 1, 'admin', current_timestamp, false,false,2013);



insert into dso_role (id, name) values (1, 'Referent');
insert into dso_role (id, name) values (2, 'Prezydent Miasta');
insert into dso_role (id, name) values (3, 'Zast�pca Prezydenta');
insert into dso_role (id, name) values (4, 'Sekretarz Urz�du');
insert into dso_role (id, name) values (5, 'Pracownik kancelarii');
insert into dso_role (id, name) values (6, 'Naczelnik wydzia�u');
insert into dso_role (id, name) values (7, 'Administrator merytoryczny');




insert into dso_in_document_delivery (id, posn, name) values (nextval('dso_in_document_delivery_id'), currval('dso_in_document_delivery_id'), 'Poczta');
insert into dso_in_document_delivery (id, posn, name) values (nextval('dso_in_document_delivery_id'), currval('dso_in_document_delivery_id'), 'Kurier');
insert into dso_in_document_delivery (id, posn, name) values (nextval('dso_in_document_delivery_id'), currval('dso_in_document_delivery_id'), 'Osobi�cie');

insert into dso_out_document_delivery (id, posn, name) values (nextval('DSO_OUT_DOCUMENT_DELIVERY_ID'), currval('DSO_OUT_DOCUMENT_DELIVERY_ID'), 'Poczta');
insert into dso_out_document_delivery (id, posn, name) values (nextval('DSO_OUT_DOCUMENT_DELIVERY_ID'),  currval('DSO_OUT_DOCUMENT_DELIVERY_ID'), 'Osobi�cie');


INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (1, 1, 'Interwencja', 14);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (2, 2, 'List', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (3, 3, 'Notatka', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (4, 4, 'Odwo�anie', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (5, 5, 'Opracowanie', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (6, 6, 'Projekt', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (7, 7, 'Recenzja', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (8, 8, 'Skarga', 14);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (9, 9, 'Wniosek', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (10, 10, 'Wyst�pienie', 21);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (11, 11, 'Kontrakt', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (12, 12, 'Faktura', 14);
--dokument biznesowy musi byc bo nie przyjmiemy zadnego pisma
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (13, 13, 'Dokument biznesowy', 14);


-- nie nalezy zmieniac identyfikatorow statusow
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (1, 1, 'Zarejestrowane');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (2, 2, 'W toku');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (3, 3, 'W przygotowaniu');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (4, 4, 'Zako�czone');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (5, 5, 'Zarchiwizowane');


-- nie nalezy zmieniac identyfikatorow poszczegolnych statusow
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (1, 1, 'Rozpocz�ta');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (2, 2, 'Tymczasowo zako�czona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (3, 3, 'Ostatecznie zako�czona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (4, 4, 'Wznowiona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (5, 5, 'Planowana');

insert into dso_case_priority (id, posn, name) values (1, 1, 'Zwyk�a');
insert into dso_case_priority (id, posn, name) values (2, 2, 'Wa�na');
insert into dso_case_priority (id, posn, name) values (3, 3, 'Pilna');
insert into dso_case_priority (id, posn, name) values (4, 4, 'Bardzo pilna');

ALTER TABLE DSO_EMAIL_CHANNEL ADD DOCUMENT_KIND_ID NUMBER(19,0);
COMMIT;

alter table dso_out_document drop column if exists mailWeight;

ALTER TABLE dso_out_document ADD mailWeight NUMERIC(18,0);



drop table if exists dso_mail_weight;

CREATE TABLE dso_mail_weight (
  id       numeric(18,0) NOT NULL,
  cn       character varying(30) NOT NULL,
  name       character varying(50) NOT NULL,
  weightG  int ,
  weightKg int 
  );
 
drop SEQUENCE if exists dso_mail_weight_id;
CREATE SEQUENCE dso_mail_weight_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;
	
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_50', 'Do 50g', 50);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_50-100', 'Powy�ej 50g do 100g', 100);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_100-350', 'Powy�ej 100g do 350g', 350);
INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(nextval('dso_mail_weight_id'), 'weight_350-500', 'Powy�ej 350g do 500g', 500);

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(nextval('dso_mail_weight_id'), 'weight_500-1000', 'Powy�ej 500g do 1000g', 1000, 1);
INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(nextval('dso_mail_weight_id'), 'weight_1000-2000', 'Powy�ej 1000g do 2000g', 2000, 2);

update DS_DOCUSAFE set VAR_VALUE = '657';
SELECT setval('public.ds_user_id', 2, true);
SELECT setval('public.ds_division_id', 2, true);
SELECT setval('public.ds_role_id', 8, true);
