drop table dso_out_doc_asgn_objective;
drop table dso_in_doc_asgn_objective;
drop generator dso_out_doc_asgn_objective_id;
drop generator dso_in_doc_asgn_objective_id;

create generator dso_assignment_objective_id;
set generator dso_assignment_objective_id to 1;

create table dso_assignment_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(80) CHARACTER SET UNICODE_FSS NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL,
    DOCUMENTTYPE            VARCHAR(10) NOT NULL
);

