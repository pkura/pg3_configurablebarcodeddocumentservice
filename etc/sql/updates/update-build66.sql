alter table dso_in_document add summary varchar(80) character set unicode_fss not null;
alter table dso_out_document add summary varchar(80) character set unicode_fss not null;
update dso_in_document indoc set summary = (select substring(description from 1 for 80) from ds_document where id = indoc.id);
update dso_out_document outdoc set summary = (select substring(description from 1 for 80) from ds_document where id = outdoc.id);
alter table dso_in_document add journal_id numeric(18, 0);
alter table dso_out_document add journal_id numeric(18, 0);
update dso_in_document set journal_id = (select first 1 id from dso_journal where ownerguid is null and journaltype = 'incoming');
update dso_out_document set journal_id = (select first 1 id from dso_journal where ownerguid is null and journaltype = 'outgoing');
alter table dso_in_document drop asiversion;
alter table dso_in_document drop submittoasibip;
alter table dso_in_document add submittobip smallint;
update dso_in_document set submittobip = 0;
alter table ds_attachment add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table ds_attachment add wparam       NUMERIC(18,0);
create table DSO_IN_DOCUMENT_KIND_REQ_ATT (
	kind_id		INTEGER NOT NULL,
	posn		INTEGER,
	title		VARCHAR(80) CHARACTER SET UNICODE_FSS NOT NULL,
	sid		    VARCHAR(32) NOT NULL
);
ALTER table DSO_IN_DOCUMENT_KIND_REQ_ATT ADD CONSTRAINT DSO_IN_DOCUMENT_KIND_REQ_ATT_FK FOREIGN KEY (KIND_ID) REFERENCES DSO_IN_DOCUMENT_KIND (ID);
alter table ds_document add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table ds_document add wparam       NUMERIC(18,0);
alter table ds_folder add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table ds_folder add wparam       NUMERIC(18,0);
alter table ds_attachment_revision add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table ds_attachment_revision add wparam       NUMERIC(18,0);
alter table dso_rwa add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table dso_rwa add wparam       NUMERIC(18,0);
alter table dso_journal add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table dso_journal add wparam       NUMERIC(18,0);
alter table dso_person add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table dso_person add wparam       NUMERIC(18,0);
alter table dso_case add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table dso_case add wparam       NUMERIC(18,0);
alter table dso_portfolio add lparam       VARCHAR(200) CHARACTER SET UNICODE_FSS;
alter table dso_portfolio add wparam       NUMERIC(18,0);

alter table dso_in_document alter assigneduser to clerk;
update dso_document_audit set property='clerk' where property='assignedUser';
alter table dso_out_document alter assigneduser to clerk;
alter table dso_case alter assigneduser to clerk;
alter table dso_portfolio alter assigneduser to clerk;

alter table dso_case add divisionguid varchar(62) not null;
update dso_case set divisionguid='rootdivision';

alter table dso_person add anonymous smallint not null;
update dso_person set anonymous = 0;

alter table dso_in_document drop senderanonymous;
