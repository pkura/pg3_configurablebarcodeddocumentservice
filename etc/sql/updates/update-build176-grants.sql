create table dsr_gra_request (
    id                  NUMERIC(18,0)    NOT NULL,
    hversion            integer not null,
    documentdate        date,
    sequenceid          integer not null,
    firstname           varchar(50) collate pxw_plk,
    lastname            varchar(50) collate pxw_plk,
    street              varchar(50) collate pxw_plk,
    zip                 varchar(14),
    location            varchar(50) collate pxw_plk,
    income              numeric(6,2),
    step                varchar(30),
    laststep            varchar(30),
    lastresult          varchar(30),
    OFFICEID            VARCHAR(84),
	indocumentid        NUMERIC(18,0),
	printed			    INTEGER,
    case_id             numeric(18,0) NOT NULL
);
create generator dsr_gra_request_id;

create table dsr_gra_beneficiary (
    request_id          NUMERIC(18,0) NOT NULL,
    posn                INTEGER NOT NULL,
    firstname           varchar(50) collate pxw_plk,
    lastname            varchar(50) collate pxw_plk,
    birthdate           date not null,
    fathersname         varchar(50) collate pxw_plk,
    mothersname         varchar(50) collate pxw_plk,
    schoolname          varchar(50) NOT NULL collate pxw_plk,
    schoolstreet        varchar(50) collate pxw_plk,
    schoolzip           varchar(14),
    schoollocation      varchar(50) collate pxw_plk,
    bankaccount         varchar(40),
    helprequested       varchar(30) NOT NULL,
    helpgranted         varchar(30),
    rejected            smallint,
    justification       varchar(200) collate pxw_plk,
    step                varchar(30),
    laststep            varchar(30),
    lastresult          varchar(30)
);
