alter table ds_folder_permission drop constraint ds_folder_permission_fk;
alter table ds_folder_permission add constraint ds_folder_permission_fk foreign key (folder_id) references ds_folder (id) on delete cascade;
alter table ds_document_permission drop constraint ds_document_permission_fk;
alter table ds_document_permission add constraint ds_document_permission_fk foreign key (document_id) references ds_document
on delete cascade;
