/*
   24 listopada 2005 09:58:54
   User:
   Server: LUKASZ2
   Database: nw
   Application: MS SQLEM - Data Tools
*/

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE nw.DSO_IN_DOCUMENT
	DROP CONSTRAINT dso_in_document_outdelivery_fk
GO
COMMIT
BEGIN TRANSACTION
ALTER TABLE nw.DSO_IN_DOCUMENT
	DROP CONSTRAINT dso_in_document_delivery_fk
GO
COMMIT
BEGIN TRANSACTION
ALTER TABLE nw.DSO_IN_DOCUMENT
	DROP CONSTRAINT dso_in_document_kind_fk
GO
COMMIT
BEGIN TRANSACTION
ALTER TABLE nw.DSO_IN_DOCUMENT
	DROP CONSTRAINT dso_in_document_status_fk
GO
COMMIT
BEGIN TRANSACTION
CREATE TABLE nw.Tmp_DSO_IN_DOCUMENT
	(
	ID numeric(19, 0) NOT NULL,
	DOCUMENTDATE datetime NULL,
	STAMPDATE datetime NULL,
	INCOMINGDATE datetime NULL,
	POSTALREGNUMBER varchar(20) NULL,
	SENDER numeric(19, 0) NULL,
	NUMATTACHMENTS int NULL,
	PACKAGEREMARKS varchar(240) NULL,
	OTHERREMARKS varchar(240) NULL,
	TRACKINGNUMBER varchar(20) NULL,
	trackingpassword varchar(20) NULL,
	SUBMITTOBIP smallint NULL,
	CASE_ID numeric(19, 0) NULL,
	CASEDOCUMENTID varchar(84) NULL,
	LOCATION varchar(60) NULL,
	KIND int NULL,
	clerk varchar(62) NULL,
	ASSIGNEDDIVISION varchar(62) NULL,
	CREATINGUSER varchar(62) NOT NULL,
	STATUS int NULL,
	OFFICENUMBER int NULL,
	OFFICENUMBERYEAR int NULL,
	BARCODE varchar(25) NULL,
	officenumbersymbol varchar(20) NULL,
	referenceid varchar(60) NULL,
	master_document_id numeric(19, 0) NULL,
	cancelled smallint NOT NULL,
	cancellationreason varchar(80) NULL,
	cancelledbyusername varchar(62) NULL,
	currentassignmentguid varchar(62) NULL,
	currentassignmentusername varchar(62) NULL,
	currentassignmentaccepted smallint NULL,
	delivery int NULL,
	outgoingdelivery int NULL,
	summary varchar(80) NOT NULL,
	archived smallint NOT NULL,
	journal_id numeric(19, 0) NULL,
	bok smallint NOT NULL,
	divisionguid varchar(62) NULL,
	replyunnecessary smallint NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM nw.DSO_IN_DOCUMENT)
	 EXEC('INSERT INTO nw.Tmp_DSO_IN_DOCUMENT (ID, DOCUMENTDATE, STAMPDATE, INCOMINGDATE, POSTALREGNUMBER, SENDER, NUMATTACHMENTS, PACKAGEREMARKS, OTHERREMARKS, TRACKINGNUMBER, trackingpassword, SUBMITTOBIP, CASE_ID, CASEDOCUMENTID, LOCATION, KIND, clerk, ASSIGNEDDIVISION, CREATINGUSER, STATUS, OFFICENUMBER, OFFICENUMBERYEAR, BARCODE, officenumbersymbol, referenceid, master_document_id, cancelled, cancellationreason, cancelledbyusername, currentassignmentguid, currentassignmentusername, currentassignmentaccepted, delivery, outgoingdelivery, summary, archived, journal_id, bok, divisionguid, replyunnecessary)
		SELECT ID, DOCUMENTDATE, STAMPDATE, INCOMINGDATE, POSTALREGNUMBER, SENDER, NUMATTACHMENTS, PACKAGEREMARKS, OTHERREMARKS, TRACKINGNUMBER, trackingpassword, SUBMITTOBIP, CASE_ID, CASEDOCUMENTID, LOCATION, KIND, clerk, ASSIGNEDDIVISION, CREATINGUSER, STATUS, OFFICENUMBER, OFFICENUMBERYEAR, BARCODE, officenumbersymbol, referenceid, master_document_id, cancelled, cancellationreason, cancelledbyusername, currentassignmentguid, currentassignmentusername, currentassignmentaccepted, delivery, outgoingdelivery, summary, archived, journal_id, bok, divisionguid, replyunnecessary FROM nw.DSO_IN_DOCUMENT TABLOCKX')
GO
DROP TABLE nw.DSO_IN_DOCUMENT
GO
EXECUTE sp_rename N'nw.Tmp_DSO_IN_DOCUMENT', N'DSO_IN_DOCUMENT', 'OBJECT'
GO
ALTER TABLE nw.DSO_IN_DOCUMENT ADD CONSTRAINT
	PK__DSO_IN_DOCUMENT__1DE57479 PRIMARY KEY CLUSTERED
	(
	ID
	) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX dso_in_document_sender ON nw.DSO_IN_DOCUMENT
	(
	SENDER
	) ON [PRIMARY]
GO
ALTER TABLE nw.DSO_IN_DOCUMENT WITH NOCHECK ADD CONSTRAINT
	dso_in_document_status_fk FOREIGN KEY
	(
	STATUS
	) REFERENCES nw.DSO_IN_DOCUMENT_STATUS
	(
	ID
	)
GO
ALTER TABLE nw.DSO_IN_DOCUMENT WITH NOCHECK ADD CONSTRAINT
	dso_in_document_kind_fk FOREIGN KEY
	(
	KIND
	) REFERENCES nw.DSO_IN_DOCUMENT_KIND
	(
	ID
	)
GO
ALTER TABLE nw.DSO_IN_DOCUMENT WITH NOCHECK ADD CONSTRAINT
	dso_in_document_delivery_fk FOREIGN KEY
	(
	delivery
	) REFERENCES nw.dso_in_document_delivery
	(
	ID
	)
GO
ALTER TABLE nw.DSO_IN_DOCUMENT WITH NOCHECK ADD CONSTRAINT
	dso_in_document_outdelivery_fk FOREIGN KEY
	(
	outgoingdelivery
	) REFERENCES nw.dso_out_document_delivery
	(
	ID
	)
GO
COMMIT
