alter table dso_in_document add divisionguid varchar(62);
alter table dso_out_document add divisionguid varchar(62);

update dso_in_document set divisionguid=currentassignmentguid;
update dso_out_document set divisionguid=currentassignmentguid;

update dso_in_document set divisionguid=assigneddivision where divisionguid is null;
update dso_out_document set divisionguid=assigneddivision where divisionguid is null;

update dso_in_document set divisionguid='rootdivision' where divisionguid is null;
update dso_out_document set divisionguid='rootdivision' where divisionguid is null;
