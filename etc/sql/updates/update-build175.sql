create index ds_document_deleted on ds_document (deleted);
create index ds_document_source on ds_document (source);

alter table dso_container add registries integer;
update dso_container set registries=0;
