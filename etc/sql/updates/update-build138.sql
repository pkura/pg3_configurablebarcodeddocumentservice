ALTER TABLE DSO_CONTAINER_REMARKS ADD CONTENT_TEXT BLOB SUB_TYPE 1 NOT NULL;
UPDATE DSO_CONTAINER_REMARKS SET CONTENT_TEXT = CONTENT;
ALTER TABLE DSO_CONTAINER_REMARKS DROP CONTENT;
ALTER TABLE DSO_CONTAINER_REMARKS ALTER CONTENT_TEXT TO CONTENT;
