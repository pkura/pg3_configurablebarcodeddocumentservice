update dso_role_permissions set permission_name = 'SLOWNIK_OSOB_DODAWANIE' where permission_name = 'SLOWNIK_OSOB_MODYFIKACJE';
INSERT INTO DSO_IN_OFFICE_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (12, 12, 'Faktura', 14);

alter table dso_journal alter ownerguid to og__;
alter table dso_journal add ownerguid VARCHAR(126) CHARACTER SET UNICODE_FSS;
update dso_journal set ownerguid = og__;
alter table dso_journal drop og__;

update dso_journal set ownerguid = null where ownerguid = 'rootdivision';
