CREATE TABLE DS_BOX (
    ID              numeric(18,0) NOT NULL,
    HVERSION        integer NOT NULL,
    NAME            varchar(100) NOT NULL COLLATE PXW_PLK,
    CTIME           TIMESTAMP NOT NULL,
    OPENTIME        TIMESTAMP,
    CLOSETIME       TIMESTAMP,
    ISOPEN          SMALLINT
);

ALTER TABLE DS_BOX ADD CONSTRAINT DS_BOX_PK PRIMARY KEY (ID);
create generator DS_BOX_ID;

ALTER TABLE DS_DOCUMENT ADD BOX_ID NUMERIC(18,0);
ALTER TABLE DS_DOCUMENT ADD CONSTRAINT DS_DOCUMENT_BOX FOREIGN KEY (BOX_ID)
REFERENCES DS_BOX (ID);
