alter table dso_portfolio add BARCODE      VARCHAR(25) CHARACTER SET UNICODE_FSS;

create table dso_portfolio_moved_cases (
    portfolio_id        NUMERIC(18,0) NOT NULL,
    case_id             NUMERIC(18,0) NOT NULL
);

create unique index dso_portfolio_moved_cases_u on dso_portfolio_moved_cases (portfolio_id, case_id);
alter table dso_portfolio_moved_cases add constraint dso_portfolio_moved_cases_pid foreign key (portfolio_id) references dso_portfolio;
alter table dso_portfolio_moved_cases add constraint dso_portfolio_moved_cases_cid foreign key (case_id) references dso_case;

CREATE TABLE DS_USER_SETTINGS (
    USERNAME       VARCHAR(62) NOT NULL,
    SETTING_NAME   VARCHAR(126) CHARACTER SET UNICODE_FSS NOT NULL,
    SETTING_VALUE  VARCHAR(8190) CHARACTER SET UNICODE_FSS
);

update dso_in_document set officenumberyear = 2004;
update dso_out_document set officenumberyear = 2004;

