alter table ds_document_permission drop constraint ds_document_permission_fk;
alter table ds_document_permission add constraint ds_document_permission_fk
foreign key (document_id) references ds_document on delete cascade;

alter table ds_folder_permission drop constraint ds_folder_permission_fk;
alter table ds_folder_permission add constraint ds_folder_permission_fk
foreign key (folder_id) references ds_folder on delete cascade;

alter table dso_in_document add bok smallint not null;
update dso_in_document set bok = 0;

set term !! ;

create procedure prefs
as
    declare variable root_node_id integer; -- ID ''
    declare variable node_id integer;      -- ID 'modules'
    declare variable co_node_id integer;   -- ID 'modules/coreoffice'
    declare variable open_year varchar(4);
    declare variable current_day varchar(15);
    declare variable adm varchar(62);
    declare variable iag varchar(62);
    declare variable cag varchar(62);
    declare variable eca varchar(62);
    declare variable aux varchar(62);
    declare variable clo varchar(62);
    declare variable cnt integer;
begin
    -- /
    select first 1 id from ds_prefs_node where parent_id is null into :root_node_id;
    if (root_node_id is null) then
    begin
        insert into ds_prefs_node (id, name) values (gen_id(ds_prefs_node_id, 1), '');
        select gen_id(ds_prefs_node_id, 0) from rdb$database into :root_node_id;
    end

    -- /modules
    select first 1 id from ds_prefs_node where parent_id = :root_node_id and name = 'modules' into :node_id;
    if (node_id is null) then
    begin
        insert into ds_prefs_node (id, parent_id, name) values (gen_id(ds_prefs_node_id, 1), :root_node_id, 'modules');
        select gen_id(ds_prefs_node_id, 0) from rdb$database into :node_id;
    end

    -- /modules/coreoffice
    select first 1 id from ds_prefs_node where parent_id = :node_id and name = 'coreoffice' into :co_node_id;
    if (co_node_id is null) then
    begin
        insert into ds_prefs_node (id, parent_id, name)
            values (gen_id(ds_prefs_node_id, 1), :node_id, 'coreoffice');
        select gen_id(ds_prefs_node_id, 0) from rdb$database into :co_node_id;
    end

    -- OPEN_YEAR
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='openyear' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='OPEN_YEAR' into :open_year;
        if (open_year is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'openyear', :open_year);
        end
    end

    -- CURRENT_DAY
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='currentday' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='CURRENT_DAY' into :current_day;
        if (current_day is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'currentday', :current_day);
        end
    end

    -- ADMIN_USERNAME
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='adminusername' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='ADMIN_USERNAME' into :adm;
        if (adm is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'adminusername', :adm);
        end
    end

    -- INSTANT_ASSIGNMENT_GROUP
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='instantassignmentgroup' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='INSTANT_ASSIGNMENT_GROUP' into :iag;
        if (iag is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'instantassignmentgroup', :iag);
        end
    end

    -- COLLECTIVE_ASSIGNMENT_GROUP
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='collectiveassignmentgroup' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='COLLECTIVE_ASSIGNMENT_GROUP' into :cag;
        if (cag is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'collectiveassignmentgroup', :cag);
        end
    end

    -- ENABLE_COLLECTIVE_ASSIGNMENT
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='enablecollectiveassignment' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='ENABLE_COLLECTIVE_ASSIGNMENT' into :eca;
        if (eca is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'enablecollectiveassignment', :eca);
        end
    end

    -- OFFICE_NUMBERS_FROM_AUX_JOURNALS
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='officenumbersfromauxjournals' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='OFFICE_NUMBERS_FROM_AUX_JOURNALS' into :aux;
        if (aux is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'officenumbersfromauxjournals', :aux);
        end
    end

    -- WF_PROCESS_FOR_CLONED_IN_DOCUMENTS
    select count(*) from ds_prefs_value where node_id = :co_node_id and pref_key='wfprocessforclonedindocuments' into :cnt;

    if (cnt <= 0) then
    begin
        -- co_node_id zawiera adres modules/coreoffice
        select first 1 setting_value from ds_settings where setting_name='WF_PROCESS_FOR_CLONED_IN_DOCUMENTS' into :clo;
        if (clo is not null) then
        begin
            insert into ds_prefs_value (node_id, pref_key, pref_value) values (:co_node_id, 'wfprocessforclonedindocuments', :clo);
        end
    end
end !!

set term ; !!

execute procedure prefs;
drop procedure prefs;

commit;

drop table ds_settings;
