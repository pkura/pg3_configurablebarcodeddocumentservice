alter table ds_user_certificate add expirationTime datetime;
alter table ds_user_certificate add serialNumber integer;
alter table ds_user_certificate add subjectDn varchar(200);
alter table ds_user_certificate add encodedsignature text;
