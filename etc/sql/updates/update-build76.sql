alter table ds_settings add value__ varchar(4000) character set unicode_fss;
update ds_settings set value__ = setting_value;
alter table ds_settings drop setting_value;
alter table ds_settings alter value__ to setting_value;

alter table ds_user_settings add value__ varchar(4000) character set unicode_fss;
update ds_user_settings set value__ = setting_value;
alter table ds_user_settings drop setting_value;
alter table ds_user_settings alter value__ to setting_value;

alter table dso_document_remarks add content__ varchar(4000) character set unicode_fss not null;
update dso_document_remarks set content__ = content;
alter table dso_document_remarks drop content;
alter table dso_document_remarks alter content__ to content;

alter table dso_case_remarks add content__ varchar(4000) character set unicode_fss not null;
update dso_case_remarks set content__ = content;
alter table dso_case_remarks drop content;
alter table dso_case_remarks alter content__ to content;

alter table DSO_IN_DOCUMENT_KIND_REQ_ATT drop constraint DSO_IN_DOCUMENT_KIND_REQ_ATT_FK;
ALTER table DSO_IN_DOCUMENT_KIND_REQ_ATT ADD CONSTRAINT DSO_IN_DOC_KIND_REQ_ATT_FK FOREIGN KEY (KIND_ID) REFERENCES DSO_IN_DOCUMENT_KIND (ID);
