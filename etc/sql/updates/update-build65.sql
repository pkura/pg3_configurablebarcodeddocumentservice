alter table dso_in_office_document add outgoingdelivery integer;
alter table dso_in_office_document add constraint dso_indoc_outdelivery_fk foreign key (outgoingdelivery) references dso_in_office_doc_delivery (id);

drop table ds_document_lock;

create table ds_document_lock (
    document_id     NUMERIC(18,0) NOT NULL,
    username        VARCHAR(62) NOT NULL,
    expiration      TIMESTAMP,
    lockmode        varchar(20) not null
);
alter table ds_document_lock add constraint ds_document_lock_mode_check CHECK (lockmode = 'READ' or lockmode = 'WRITE');
alter table ds_document_lock add constraint ds_document_lock_pk primary key (document_id, username, lockmode);



CREATE TABLE DSO_IN_DOCUMENT (
    ID                     NUMERIC(18,0) NOT NULL PRIMARY KEY,
    DOCUMENTDATE           DATE,
    STAMPDATE              DATE,
    INCOMINGDATE           TIMESTAMP,
    POSTALREGNUMBER        VARCHAR(20) CHARACTER SET UNICODE_FSS,
    SENDERANONYMOUS        SMALLINT,
    SENDER                 NUMERIC(18,0),
    NUMATTACHMENTS         INTEGER,
    PACKAGEREMARKS         VARCHAR(240) CHARACTER SET UNICODE_FSS,
    OTHERREMARKS           VARCHAR(240) CHARACTER SET UNICODE_FSS,
    TRACKINGNUMBER         VARCHAR(20),
    SUBMITTOASIBIP         SMALLINT,
    ASIVERSION             INTEGER,
    CASE_ID                NUMERIC(18,0),
    CASEDOCUMENTID         VARCHAR(84),
    LOCATION               VARCHAR(60) CHARACTER SET UNICODE_FSS,
    KIND                   INTEGER,
    ASSIGNEDUSER           VARCHAR(62),
    ASSIGNEDDIVISION       VARCHAR(62),
    CREATINGUSER           VARCHAR(62) NOT NULL,
    STATUS                 INTEGER,
    OFFICENUMBER           INTEGER,
    OFFICENUMBERYEAR       INTEGER,
    officenumbersymbol      varchar(20) character set unicode_fss,
    referenceid             varchar(60) character set unicode_fss,
    master_document_id      numeric(18,0),
    cancelled               smallint not null,
    cancellationreason      varchar(80) CHARACTER SET UNICODE_FSS,
    cancelledbyusername     varchar(62),
    currentassignmentguid   varchar(62),
    currentassignmentusername varchar(62),
    currentassignmentaccepted smallint,
    delivery                integer,
    outgoingdelivery        integer
);

insert into dso_in_document (
    ID,
    DOCUMENTDATE           ,
    STAMPDATE              ,
    INCOMINGDATE           ,
    POSTALREGNUMBER        ,
    SENDERANONYMOUS        ,
    SENDER                 ,
    NUMATTACHMENTS         ,
    PACKAGEREMARKS         ,
    OTHERREMARKS           ,
    TRACKINGNUMBER         ,
    SUBMITTOASIBIP         ,
    ASIVERSION             ,
    CASE_ID                ,
    CASEDOCUMENTID         ,
    LOCATION               ,
    KIND                   ,
    ASSIGNEDUSER           ,
    ASSIGNEDDIVISION       ,
    CREATINGUSER           ,
    STATUS                 ,
    OFFICENUMBER           ,
    OFFICENUMBERYEAR       ,
    officenumbersymbol     ,
    referenceid             ,
    master_document_id ,
    cancelled               ,
    cancellationreason      ,
    cancelledbyusername     ,
    currentassignmentguid   ,
    currentassignmentusername ,
    currentassignmentaccepted ,
    delivery                ,
    outgoingdelivery)
SELECT
    ID,
    DOCUMENTDATE           ,
    STAMPDATE              ,
    INCOMINGDATE           ,
    POSTALREGNUMBER        ,
    SENDERANONYMOUS        ,
    SENDER                 ,
    NUMATTACHMENTS         ,
    PACKAGEREMARKS         ,
    OTHERREMARKS           ,
    TRACKINGNUMBER         ,
    SUBMITTOASIBIP         ,
    ASIVERSION             ,
    CASE_ID                ,
    CASEDOCUMENTID         ,
    LOCATION               ,
    KIND                   ,
    ASSIGNEDUSER           ,
    ASSIGNEDDIVISION       ,
    CREATINGUSER           ,
    STATUS                 ,
    OFFICENUMBER           ,
    OFFICENUMBERYEAR       ,
    officenumbersymbol     ,
    referenceid ,
    master_document_id ,
    cancelled               ,
    cancellationreason      ,
    cancelledbyusername     ,
    currentassignmentguid   ,
    currentassignmentusername ,
    currentassignmentaccepted ,
    delivery                ,
    outgoingdelivery
FROM DSO_IN_OFFICE_DOCUMENT;

commit;

drop table dso_in_office_document;




CREATE TABLE DSO_IN_DOCUMENT_STATUS (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

insert into DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME)
select ID, POSN, NAME FROM DSO_IN_OFFICE_DOCUMENT_STATUS;

commit;

drop table DSO_IN_OFFICE_DOCUMENT_STATUS;

alter table dso_in_document add constraint dso_in_document_status_fk foreign key (status) references dso_in_document_status (id);



CREATE TABLE DSO_IN_DOCUMENT_KIND (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL,
    DAYS                    SMALLINT
);

insert into DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS)
select ID, POSN, NAME, DAYS from DSO_IN_OFFICE_DOCUMENT_KIND;

commit;

drop table DSO_IN_OFFICE_DOCUMENT_KIND;

update rdb$generators set rdb$generator_name = 'DSO_IN_DOCUMENT_KIND_ID' WHERE RDB$GENERATOR_NAME = 'DSO_IN_OFFICE_DOCUMENT_KIND_ID';

alter table dso_in_document add constraint dso_in_document_kind_fk foreign key (kind) references dso_in_document_kind (id);




create table dso_in_document_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

insert into dso_in_document_delivery (ID, POSN, NAME)
select ID, POSN, NAME from dso_in_office_doc_delivery;

commit;

drop table dso_in_office_doc_delivery;

update rdb$generators set rdb$generator_name = 'DSO_IN_DOCUMENT_DELIVERY_ID'
WHERE RDB$GENERATOR_NAME = 'DSO_IN_OFFICE_DOC_DELIVERY_ID';

alter table dso_in_document add constraint dso_in_document_delivery_fk foreign key (delivery) references dso_in_document_delivery (id);




CREATE TABLE DSO_OUT_DOCUMENT (
    ID                     NUMERIC(18,0) NOT NULL PRIMARY KEY,
    OFFICENUMBER            INTEGER,
    officenumbersymbol varchar(20) character set unicode_fss,
    OFFICENUMBERYEAR        INTEGER,
    DOCUMENTDATE            DATE,
    POSTALREGNUMBER         VARCHAR(20) CHARACTER SET UNICODE_FSS,
    ASSIGNEDUSER            VARCHAR(62),
    ASSIGNEDDIVISION       VARCHAR(62),
    CREATINGUSER            VARCHAR(62) NOT NULL,
    INDOCUMENT              NUMERIC(18,0),
    CASEDOCUMENTID          VARCHAR(84),
    CASE_ID                 NUMERIC(18,0),
    SENDER                  NUMERIC(18,0),
    ZPODATE                 DATE,
    cancelled               smallint not null,
    cancellationreason      varchar(80) CHARACTER SET UNICODE_FSS,
    prepstate               varchar(15),
    cancelledbyusername     varchar(62),
    currentassignmentguid   varchar(62),
    currentassignmentusername varchar(62),
    currentassignmentaccepted smallint,
    referenceid				varchar(60),
    delivery                integer
);


INSERT INTO DSO_OUT_DOCUMENT (
    ID                     ,
    OFFICENUMBER           ,
    officenumbersymbol ,
    OFFICENUMBERYEAR        ,
    DOCUMENTDATE            ,
    POSTALREGNUMBER         ,
    ASSIGNEDUSER            ,
    ASSIGNEDDIVISION        ,
    CREATINGUSER            ,
    INDOCUMENT              ,
    CASEDOCUMENTID          ,
    CASE_ID                 ,
    SENDER                  ,
    ZPODATE                 ,
    cancelled               ,
    cancellationreason      ,
    prepstate               ,
    cancelledbyusername     ,
    currentassignmentguid   ,
    currentassignmentusername ,
    currentassignmentaccepted ,
    delivery)
SELECT
    ID                     ,
    OFFICENUMBER           ,
    officenumbersymbol ,
    OFFICENUMBERYEAR        ,
    DOCUMENTDATE            ,
    POSTALREGNUMBER         ,
    ASSIGNEDUSER            ,
    ASSIGNEDDIVISION        ,
    CREATINGUSER            ,
    INDOCUMENT              ,
    CASEDOCUMENTID          ,
    CASE_ID                 ,
    SENDER                  ,
    ZPODATE                 ,
    cancelled               ,
    cancellationreason      ,
    prepstate               ,
    cancelledbyusername     ,
    currentassignmentguid   ,
    currentassignmentusername ,
    currentassignmentaccepted ,
    delivery
FROM DSO_OUT_OFFICE_DOCUMENT;

commit;

drop table DSO_OUT_OFFICE_DOCUMENT;


create table dso_out_document_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

INSERT INTO dso_out_document_delivery (ID, POSN, NAME)
SELECT ID, POSN, NAME FROM dso_out_office_doc_delivery;

commit;

drop table dso_out_office_doc_delivery;

alter table dso_out_document add constraint dso_out_document_delivery_fk foreign key (delivery) references dso_out_document_delivery (id);
alter table dso_in_document add constraint dso_in_document_outdelivery_fk foreign key (outgoingdelivery) references dso_out_document_delivery (id);

update rdb$generators set rdb$generator_name = 'DSO_OUT_DOCUMENT_DELIVERY_ID'
WHERE RDB$GENERATOR_NAME = 'DSO_OUT_OFFICE_DOC_DELIVERY_ID';


alter table dso_document_asgn_history add constraint dso_document_asgn_history_fk foreign key (document_id) references ds_document(id);
alter table DSO_DOCUMENT_REMARKS add constraint DSO_DOCUMENT_REMARKS_FK foreign key (document_id) references ds_document (id);

alter table dso_role_usernames drop constraint FKE4FF223352119584;
alter table dso_role_divisions drop constraint FK71E0753C52119584;
alter table dso_role_permissions drop constraint FKF3E5E6DA52119584;
alter table dso_role_usernames add constraint dso_role_usernames_fk foreign key (role_id) references dso_role (id);
alter table dso_role_divisions add constraint dso_role_divisions_fk foreign key (role_id) references dso_role (id);
alter table dso_role_permissions add constraint dso_role_permissions_fk foreign key (role_id) references dso_role (id);

alter table dsw_resource add constraint dsw_resource_pk primary key (id);
alter table dsw_process add constraint dsw_process_pk primary key (id);
alter table dsw_assignment add constraint dsw_assignment_pk primary key (id);
alter table dsw_activity add constraint dsw_activity_pk primary key (id);
alter table dsw_package add constraint dsw_package_pk primary key (id);
alter table dsw_datatype add constraint dsw_datatype_pk primary key (id);
alter table dsw_activity add constraint dsw_activity_fk foreign key (process_id) references dsw_process (id);
alter table dsw_assignment add constraint dsw_assignment_activity_fk foreign key (activity_id) references dsw_activity(id);
alter table dsw_assignment add constraint dsw_assignment_resource_fk foreign key (resource_id) references dsw_resource(id);
alter table dsw_process add constraint dsw_process_package_fk foreign key (package_id) references dsw_package(id);
alter table dsw_participant_mapping add constraint dsw_participant_mapping_res_fk foreign key (resource_id) references dsw_resource(id);
alter table dsw_process_activities add constraint dsw_process_activities_proc_fk foreign key (process_id) references dsw_process(id);
alter table dsw_process_activities add constraint dsw_process_activities_act_fk foreign key (activity_id) references dsw_activity(id);
alter table dsw_process_context add constraint dsw_process_context_fk foreign key (process_id) references dsw_process(id);
alter table dsw_process_context add constraint dsw_process_context_dt_fk foreign key (datatype_id) references dsw_datatype(id);
alter table dsw_activity_context add constraint dsw_activity_context_fk foreign key (activity_id) references dsw_activity(id);
alter table dsw_activity_context add constraint dsw_activity_context_dt_fk foreign key (datatype_id) references dsw_datatype(id);
alter table dsw_datatype_enum_values add constraint dsw_datatype_enum_values_fk foreign key (datatype_id) references dsw_datatype(id);
alter table ds_document_watch add constraint ds_document_watch_pk primary key (id);
drop index ds_document_permission_pk;
create unique index ds_document_permission_pk on ds_document_permission (document_id, subject, subjecttype, name);
drop index ds_folder_permission_pk;
create unique index ds_folder_permission_pk on ds_folder_permission (folder_id, subject, subjecttype, name);
alter table dso_person add constraint dso_person_pk primary key (id);
