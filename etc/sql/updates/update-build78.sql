alter table dso_case add     TRACKINGNUMBER         VARCHAR(20);
alter table dso_case add     TRACKINGPASSWORD         VARCHAR(20);
alter table dso_journal_entry add entrydate date not null;
update dso_journal_entry set entrydate = ctime;

alter table dso_journal add archived smallint not null;
update dso_journal set archived = 0;

alter table dso_portfolio add archived smallint not null;
update dso_portfolio set archived = 0;
 