alter table dso_in_document add tmp_replyunnecessary smallint not null;
update dso_in_document set tmp_replyunnecessary = replyunnecessary;
update dso_in_document set tmp_replyunnecessary = 0 where tmp_replyunnecessary is null; 
alter table dso_in_document drop replyunnecessary;
alter table dso_in_document alter tmp_replyunnecessary to replyunnecessary;
