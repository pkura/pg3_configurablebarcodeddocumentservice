ALTER TABLE DS_PREFS_VALUE ADD CVALUE BLOB SUB_TYPE 1 NOT NULL;
UPDATE DS_PREFS_VALUE SET CVALUE = PREF_VALUE;
ALTER TABLE DS_PREFS_VALUE DROP PREF_VALUE;
ALTER TABLE DS_PREFS_VALUE ALTER CVALUE TO PREF_VALUE;
 