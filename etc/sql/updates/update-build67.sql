create table dso_case_priority (
    id      INTEGER not null,
    posn    SMALLINT NOT NULL,
    name    VARCHAR(80) CHARACTER SET UNICODE_FSS not null
);

alter table dso_case_priority add constraint dso_case_priority_pk primary key (id);

create generator dso_case_priority_id;
set generator dso_case_priority_id to 2;

insert into dso_case_priority values (1, 1, 'Zwykly');

alter table dso_case drop priority;
alter table dso_case add priority_id integer not null;
update dso_case set priority_id = 1;
alter table dso_case add constraint dso_case_priority_fk foreign key (priority_id) references dso_case_priority (id);
