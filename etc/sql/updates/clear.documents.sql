
delete from ds_attachment_revision;
delete from ds_attachment;
delete from DSO_DOCUMENT_REMARKS;
delete from dso_document_asgn_history;
delete from dso_document_audit;
delete from ds_document_changelog;
delete from dso_in_document;
delete from dso_out_document;
delete from ds_document;
delete from DSG_AKT_NOTARIALNY;
delete from DSG_AKT_MULTIPLE_VALUE;
delete from df_inst;
DROP SEQUENCE DS_DOCUMENT_ID
CREATE SEQUENCE ds_document;