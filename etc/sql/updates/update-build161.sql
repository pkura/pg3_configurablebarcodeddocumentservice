
delete from ds_document_permission where name='read_att';
insert into ds_document_permission (document_id, subject, subjecttype, name, negative) select document_id, subject, subjecttype, 'read_att', negative from ds_document_permission where name='read';

delete from ds_document_permission where name='modify_att';
insert into ds_document_permission (document_id, subject, subjecttype, name, negative) select document_id, subject, subjecttype, 'modify_att', negative from ds_document_permission where name='modify';

update ds_attachment_revision set mime = 'image/png' where upper(originalfilename) like '%.PNG';
