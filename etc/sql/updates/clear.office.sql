delete from dsw_activity_context;
delete from dsw_process_context;
delete from dsw_assignment;
delete from dsw_process_activities;
delete from dsw_activity;
delete from dsw_process;

delete from dso_container_audit;
delete from DSO_CONTAINER_REMARKS;

alter table dso_container drop constraint DSO_CONTAINER_PARENT;
delete from dso_container;

delete from ds_attachment_revision where attachment_id in (select att.id from ds_attachment att join dso_in_document doc on att.document_id=doc.id);
delete from ds_attachment where id in (select att.id from ds_attachment att join dso_in_document doc on att.document_id=doc.id);

delete from ds_attachment_revision where attachment_id in (select att.id from ds_attachment att join dso_out_document doc on att.document_id=doc.id);
delete from ds_attachment where id in (select att.id from ds_attachment att join dso_out_document doc on att.document_id=doc.id);

delete from dso_journal_entry;
delete from DSO_DOCUMENT_REMARKS;
delete from dso_document_asgn_history;
delete from dso_document_audit;
delete from ds_document_changelog;
delete from dso_in_document;
delete from dso_out_document;
delete from dsw_task_history_entry;
delete from ds_document where office=1;
delete from dso_person; -- where discriminator = 'SENDER' or discriminator = 'RECIPIENT';

-- insert into ds_folder_permission values(0, '*', 'any', 'read', 0);

update dso_journal set sequenceid = 1;

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PARENT FOREIGN KEY (PARENT_ID) REFERENCES DSO_CONTAINER (ID);
