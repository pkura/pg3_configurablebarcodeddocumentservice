create index ds_docperm_docid on ds_document_permission (document_id);
create index ds_docperm_name on ds_document_permission(name);
create index ds_docperm_negative on ds_document_permission(negative);
create index ds_docperm_subjtype on ds_document_permission(subjecttype);
create index ds_docperm_subject on ds_document_permission(subject);

create index ds_foldperm_foldid on ds_folder_permission (folder_id);
create index ds_foldperm_name on ds_folder_permission(name);
create index ds_foldperm_negative on ds_folder_permission(negative);
create index ds_foldperm_subjtype on ds_folder_permission(subjecttype);
create index ds_foldperm_subject on ds_folder_permission(subject);
