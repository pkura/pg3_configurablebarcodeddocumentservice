drop table ds_attribute;
alter table DS_ATTRIBUTE_SET_BUNDLE drop constraint FK6ECF1C73302DCAE6;
alter table DS_ATTRIBUTE_SET_BUNDLE drop constraint FK6ECF1C73EB413C5E;
alter table ds_attribute_set drop constraint ds_attribute_set_fk;
drop table  DS_ATTRIBUTE_SET_BUNDLE;
drop table DS_EXTINF2ATTR_SET;
commit;
drop table ds_attribute_set;
alter table ds_document drop ext_inf_id;
drop table DS_DOCUMENTEXTINF;

drop generator DS_ATTRIBUTE_ID;
drop generator DS_ATTRIBUTE_SET_ID;
drop generator DS_ATTRIBUTE_SET_BUNDLE_ID;
drop generator DS_DOCUMENTEXTINF_ID;

