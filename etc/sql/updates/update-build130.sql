ALTER TABLE DSO_OUT_DOCUMENT ADD INTERNALDOCUMENT SMALLINT NOT NULL;
UPDATE DSO_OUT_DOCUMENT SET INTERNALDOCUMENT=0;

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, OWNERGUID, JOURNALTYPE, SEQUENCEID, AUTHOR,
CTIME, ARCHIVED, CLOSED, CYEAR)
VALUES (GEN_ID(DSO_JOURNAL_ID, 1), 0, 'Dziennik glowny', NULL, 'internal', 1, 'admin',
CURRENT_TIMESTAMP, 0, 0, 2005);
