create index dso_person_docid on dso_person (document_id);
create index dso_person_discriminator on dso_person (discriminator);
create index dso_person_dictionarytype on dso_person (dictionarytype);
create index dso_person_dictionaryguid on dso_person (dictionaryguid);
create index dso_person_title on dso_person (title);
create index dso_person_firstname on dso_person (firstname);
create index dso_person_lastname on dso_person (lastname);
create index dso_person_organization on dso_person (organization);
create index dso_person_zip on dso_person (zip);
create index dso_person_street on dso_person (street);
create index dso_person_location on dso_person (location);

create index dso_journal_entry_entrydate on dso_journal_entry (entrydate);
create index dso_journal_entry_journalid on dso_journal_entry (journal_id);
create index dso_journal_entry_docid on dso_journal_entry (documentid);

create index dso_in_document_sender on dso_in_document (sender);
