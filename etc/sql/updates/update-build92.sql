create table dso_out_document_retreason (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40)  NOT NULL COLLATE PXW_PLK
);

create generator dso_out_document_retreason_id;
set generator dso_out_document_retreason_id to 1;

alter table dso_out_document add returnreason integer;

alter table dso_out_document add returned smallint not null;
update dso_out_document set returned = 0;

alter table dso_out_document add constraint dso_out_document_retreason_fk foreign key (returnreason) references dso_out_document_retreason (id);

