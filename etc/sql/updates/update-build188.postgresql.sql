CREATE TABLE DS_RESOURCE (
    ID              NUMERIC(19,0) NOT NULL,
    SYSTEMNAME      VARCHAR(100),
    FILENAME        VARCHAR(100) NOT NULL,
    MIME            VARCHAR(62) NOT NULL,
    CONTENTSIZE     INT NOT NULL,
    CONTENTDATA     bytea
);

CREATE UNIQUE INDEX DS_RESOURCE_SYSNAME ON DS_RESOURCE (SYSTEMNAME);
ALTER TABLE DS_RESOURCE ADD CONSTRAINT DS_RESOURCE_PK PRIMARY KEY (ID);

create sequence  DS_RESOURCE_ID start with 1 INCREMENT BY 1;

CREATE OR REPLACE FUNCTION insert_to_DS_RESOURCE_ID()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.ID IS NULL THEN
  NEW.ID:=nextval('DS_RESOURCE_ID');--@ignore-semicolon
 RETURN NEW;--@ignore-semicolon
  END IF;--@ignore-semicolon
 END;--@ignore-semicolon
  $BODY$
 LANGUAGE 'plpgsql' VOLATILE;


create trigger TRGDS_RESOURCE_ID
before insert on DS_RESOURCE
for each row 
EXECUTE PROCEDURE insert_to_DS_RESOURCE_ID();