alter table dso_portfolio add sequenceid integer;

-- aktualizacja dso_portfolio.sequenceid, sprawy dostaja numery kolejne po podteczkach

set term !! ;

create procedure portfolio_seq
as
    declare variable pid numeric(18,0);
    declare variable spid numeric(18,0);
    declare variable cid numeric(18,0);
    declare variable i integer;
begin
    for select id from dso_portfolio where parent_id is null into :pid do
    begin
        /*
        select max(caseseqnum) from dso_case where portfolio_id = :pid into :i;
        if (i > 0) then i = i + 1;
        if (i IS NULL or i = 0) then i = 1;
        */
        i = 1;
        for select id from dso_portfolio where parent_id = :pid order by portfolioid asc into :spid do
        begin
            update dso_portfolio set sequenceid = :i where id = :spid;
            i = i + 1;
        end
        for select id from dso_case where portfolio_id = :pid order by caseseqnum into :cid do
        begin
            update dso_case set caseseqnum = :i;
            i = i + 1;
        end
    end
end !!

set term ; !!

execute procedure portfolio_seq;

drop procedure portfolio_seq;

alter table dso_case add sequenceid integer not null;
update dso_case set sequenceid=caseseqnum;
alter table dso_case drop caseseqnum;
