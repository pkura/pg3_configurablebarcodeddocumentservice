drop table dso_case_remarks;
drop table dso_case_audit;
update dso_in_document set case_id = null;
update dso_out_document set case_id = null;
alter table dso_case drop constraint dso_case_portolio_fk;
drop table dso_portfolio_moved_cases;
drop table dso_case;
drop generator dso_case_id;
drop generator dso_portfolio_id;
drop table dso_portfolio_audit;
drop table dso_portfolio;

CREATE TABLE DSO_CONTAINER (
    -- wspolne pola sprawy i teczki
    ID              NUMERIC(18,0) NOT NULL,
    PARENT_ID       NUMERIC(18,0),
    DISCRIMINATOR   VARCHAR(20) NOT NULL,
    OFFICEID        VARCHAR(84) NOT NULL COLLATE PXW_PLK,
    OFFICEIDPREFIX  VARCHAR(17) COLLATE PXW_PLK,
    OFFICEIDMIDDLE  VARCHAR(50) NOT NULL COLLATE PXW_PLK,
    OFFICEIDSUFFIX  VARCHAR(17) COLLATE PXW_PLK,
    DIVISIONGUID    VARCHAR(128) NOT NULL ,
    CLERK           VARCHAR(62) NOT NULL,
    AUTHOR          VARCHAR(62) NOT NULL,
    CTIME           TIMESTAMP NOT NULL,
    SEQUENCEID      INTEGER NOT NULL,
    ARCHIVED        SMALLINT NOT NULL,
    RWA_ID          INTEGER NOT NULL,
    LPARAM          VARCHAR(200),
    WPARAM          NUMERIC(18,0),

    -- teczka
    NAME            VARCHAR(128) COLLATE PXW_PLK,
    BARCODE         VARCHAR(25),

    -- sprawa
    CLOSED          SMALLINT,
    CASE_YEAR       INTEGER,
    OPENDATE        DATE,
    FINISHDATE      DATE,
    DOCSEQUENCEID   INTEGER,
    TRACKINGNUMBER  VARCHAR(20),
    TRACKINGPASSWORD VARCHAR(20),
    PRIORITY_ID     INTEGER,
    STATUS_ID       INTEGER,
    PRECEDENT_ID    NUMERIC(18,0)
);

ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PK PRIMARY KEY (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PARENT FOREIGN KEY (PARENT_ID)
    REFERENCES DSO_CONTAINER (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_RWA FOREIGN KEY (RWA_ID)
    REFERENCES DSO_RWA (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRIORITY FOREIGN KEY (PRIORITY_ID)
    REFERENCES DSO_CASE_PRIORITY (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_STATUS FOREIGN KEY (STATUS_ID)
    REFERENCES DSO_CASE_STATUS (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PRECEDENT FOREIGN KEY (PRECEDENT_ID)
    REFERENCES DSO_CONTAINER (ID);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_POSITIVE CHECK (SEQUENCEID > 0);
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_DISCRIMINATOR
    CHECK (DISCRIMINATOR = 'CASE' OR DISCRIMINATOR = 'FOLDER');

CREATE UNIQUE INDEX DSO_CONTAINER_SEQ ON DSO_CONTAINER (PARENT_ID, SEQUENCEID);
CREATE UNIQUE INDEX DSO_CONTAINER_OID ON DSO_CONTAINER (OFFICEID);

CREATE GENERATOR DSO_CONTAINER_ID;

create table DSO_CONTAINER_AUDIT (
    CONTAINER_ID        NUMERIC(18,0) NOT NULL,
    posn                INTEGER NOT NULL,
    ctime               TIMESTAMP NOT NULL,
    property            VARCHAR(62) ,
    username            VARCHAR(62)  NOT NULL COLLATE PXW_PLK,
    description         VARCHAR(200)  NOT NULL COLLATE PXW_PLK,
    lparam              VARCHAR(200) COLLATE PXW_PLK,
    wparam              NUMERIC(18,0)
);

alter table DSO_CONTAINER_AUDIT add constraint DSO_CONTAINER_AUDIT_FK1 foreign key (container_id) references DSO_CONTAINER (id);

CREATE TABLE DSO_CONTAINER_REMARKS (
    CONTAINER_ID NUMERIC(18,0) NOT NULL,
    POSN         INTEGER NOT NULL,
    CTIME        TIMESTAMP NOT NULL,
    CONTENT      VARCHAR(8190)  NOT NULL COLLATE PXW_PLK,
    AUTHOR       VARCHAR(62)  NOT NULL COLLATE PXW_PLK
);

ALTER TABLE DSO_CONTAINER_REMARKS ADD CONSTRAINT DSO_CONTAINER_REMARKS_FK1 FOREIGN KEY (CONTAINER_ID) REFERENCES DSO_CONTAINER (ID);

