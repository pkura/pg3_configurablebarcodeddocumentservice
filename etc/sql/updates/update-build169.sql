create table ds_document_changelog (
    document_id     numeric(18,0) NOT NULL,
    username        varchar(62) NOT NULL ,
    ctime           timestamp NOT NULL,
    what            varchar(100) NOT NULL
);
alter table ds_document_changelog add constraint ds_document_changelog_fk foreign key (document_id) references ds_document;

create index ds_document_clog_id on ds_document_changelog (document_id);
create index ds_document_clog_user on ds_document_changelog (username);
create index ds_document_clog_what on ds_document_changelog (what);
