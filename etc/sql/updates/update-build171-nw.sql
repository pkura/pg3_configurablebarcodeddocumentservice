insert into ds_document_changelog (document_id, username, ctime, what)
select document_id, username, ctime, 'ADD_ATTACHMENT'
from dso_document_audit where property='attachments' and lparam='ADD';

insert into ds_document_changelog (document_id, username, ctime, what)
select document_id, username, ctime, 'DEL_ATTACHMENT'
from dso_document_audit where property='attachments' and lparam='DELETE';

insert into ds_document_changelog (document_id, username, ctime, what)
select document_id, username, ctime, 'ADD_ATTACHMENT_REVISION'
from dso_document_audit where property='attachments' and lparam='REVISION';

insert into ds_document_changelog (document_id, username, ctime, what)
select document_id, targetuser, ctime, 'WF_ASSIGNED'
from dso_document_asgn_history where sourceguid='rootdivision' and
    targetguid='rootdivision' and finished=0;

insert into ds_document_changelog (document_id, username, ctime, what)
select id, author, ctime, 'CREATE_DOCUMENT' from ds_document;

insert into ds_document_changelog (document_id, username, ctime, what)
select id, author, ctime, 'NW_FILING' from ds_document where doctype is not null;
