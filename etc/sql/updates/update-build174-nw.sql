-- przyjęcie pisma, targetuser (WF_ASSIGNED)
insert into ds_document_changelog (document_id, username, ctime, what)
select distinct document_id, sourceuser, ctime, 'WF_ASSIGNED' from dso_document_asgn_history where sourceuser=targetuser and targetguid='rootdivision' and accepted=1 and finished=0;

-- zakończenie pracy (WF_FINISH)
insert into ds_document_changelog (document_id, username, ctime, what)
select distinct document_id, sourceuser, ctime, 'WF_FINISH' from dso_document_asgn_history where finished=1

-- dekretacja, WF_ASSIGNED
insert into ds_document_changelog (document_id, username, ctime, what)
select distinct document_id, targetuser, ctime, 'WF_ASSIGNED' from dso_document_asgn_history where finished=0 and accepted=0 and sourceuser != targetuser;

-- dekretacja, WF_ACCEPT
insert into ds_document_changelog (document_id, username, ctime, what)
select distinct document_id, targetuser, ctime, 'WF_ACCEPT' from dso_document_asgn_history where finished=0 and accepted=1 and sourceuser != targetuser;
