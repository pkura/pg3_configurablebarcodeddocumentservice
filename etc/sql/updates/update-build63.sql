alter table dso_journal add symbol varchar(20) character set unicode_fss;
alter table dso_in_office_document add constraint kind_fk foreign key (kind) references dso_in_office_document_kind (id);
alter table dso_in_office_document add constraint status_fk foreign key (status) references dso_in_office_document_status (id);

create generator dso_in_office_document_kind_id;
set generator dso_in_office_document_kind_id to 1;

alter table dso_in_office_document add officenumbersymbol varchar(20) character set unicode_fss;
alter table dso_out_office_document add officenumbersymbol varchar(20) character set unicode_fss;


alter table dso_in_office_document add delivery integer;
alter table dso_out_office_document add delivery integer;

create table dso_in_office_doc_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

create table dso_out_office_doc_delivery (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);

alter table dso_in_office_document add constraint dso_indoc_delivery_fk foreign key (delivery) references dso_in_office_doc_delivery (id);
alter table dso_out_office_document add constraint dso_outdoc_delivery_fk foreign key (delivery) references dso_out_office_doc_delivery (id);

create generator dso_in_office_doc_delivery_id;
set generator dso_in_office_doc_delivery_id to 1;

create generator dso_out_office_doc_delivery_id;
set generator dso_out_office_doc_delivery_id to 1;

create table dso_out_doc_asgn_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL
);

create generator dso_out_doc_asgn_objective_id;
set generator dso_out_doc_asgn_objective_id to 1;


create table dso_in_doc_asgn_objective (
    ID                      INTEGER NOT NULL PRIMARY KEY,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL,
    OFFICESTATUS            VARCHAR(20) NOT NULL
);

create generator dso_in_doc_asgn_objective_id;
set generator dso_in_doc_asgn_objective_id to 1;
