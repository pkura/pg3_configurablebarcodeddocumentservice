alter table dso_journal add author varchar(62) not null;
alter table dso_journal add ctime timestamp not null;

update dso_journal set ctime = current_timestamp, author = 'admin';

update dso_role_permissions set permission_name = 'SPRAWA_KOMORKA_PODGLAD' where permission_name = 'SPRAWY_KOMORKA_PODGLAD';



