alter table dso_portfolio add name__ varchar(128) character set unicode_fss not null;
update dso_portfolio set name__ = name;
alter table dso_portfolio drop name;
alter table dso_portfolio alter name__ to name;

alter table dso_case add portfolio_id__ numeric(18,0) not null;
update dso_case set portfolio_id__ = portfolio_id;
alter table dso_case drop portfolio_id;
alter table dso_case alter portfolio_id__ to portfolio_id;

alter table dso_case add constraint dso_case_portolio_fk foreign key (portfolio_id) references dso_portfolio;

select 'liczba spraw bez teczki = ' || count(*) from dso_case where portfolio_id is null;
