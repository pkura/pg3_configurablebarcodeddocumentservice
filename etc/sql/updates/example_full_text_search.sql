@vars:
Nazwa_katalogu
Nazwa_tabeli
Nazwa_indeksu - w przypadku MSSQL trzeba podać ISTNIEJĄCY indeks
Nazwa_kolumny

----Przykład jak stworzyć "Full Text Search" w bazie danych MSSQL. Uwaga max 900 znaków.
----W przypadku MSSQL można stworzyć tylko jeden index na tabeli!

--W przypadku gdy nie ma stworzonego indeksu na kolumnie
CREATE INDEX Nazwa_indeksu
ON table_name (Nazwa_kolumny)

--Tworzenie katalogu dla indeksu
CREATE FULLTEXT CATALOG Nazwa_katalogu
WITH ACCENT_SENSITIVITY = OFF

--Sprawdzenie lcid języka polskiego w bazie danych
SELECT lcid FROM sys.syslanguages where alias like 'Polish'

--Tworzenie full text index
CREATE FULLTEXT INDEX ON Nazwa_tabeli
(Nazwa_kolumny LANGUAGE 1045) --tutaj ma być lcid z zapytania powyżej. Domyślnie język polski ma numer 1045.
KEY INDEX [Nazwa_indeksu]
ON Nazwa_katalogu
WITH STOPLIST = SYSTEM


----Przykład jak stworzyć "Oracle Text" w bazie danych ORACLE
CREATE INDEX Nazwa_indeksu ON Nazwa_tabeli(Nazwa_kolumny)
       INDEXTYPE IS CTXSYS.CONTEXT;