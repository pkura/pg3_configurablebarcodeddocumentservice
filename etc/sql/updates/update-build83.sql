alter table dso_journal add closed smallint not null;
update dso_journal set closed = 0;

alter table dso_journal add cyear integer not null;
update dso_journal set cyear = 2004;
