alter table dso_case add archived smallint not null;
update dso_case set archived = 0;

alter table dso_in_document add archived smallint not null;
update dso_in_document set archived = 0;

alter table dso_out_document add archived smallint not null;
update dso_out_document set archived = 0;

alter table dso_in_document add trackingpassword varchar(20) ;

alter table dso_rwa add description__ varchar(300) not null collate pxw_plk;
update dso_rwa set description__ = description;
alter table dso_rwa drop description;
alter table dso_rwa alter description__ to description;
