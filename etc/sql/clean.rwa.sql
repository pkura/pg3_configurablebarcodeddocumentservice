update dso_in_document set case_id = null;
update dso_out_document set case_id = null;
delete from dso_container_audit;
delete from dso_container_remarks;
alter table dso_container drop constraint DSO_CONTAINER_PARENT; 
delete from dso_container;
ALTER TABLE DSO_CONTAINER ADD CONSTRAINT DSO_CONTAINER_PARENT FOREIGN KEY (PARENT_ID)
    REFERENCES DSO_CONTAINER (ID);
delete from dso_rwa;

