-- PLIK W KODOWANIU ISO-8859-2
-- W tym pliku znajduje si� pocz�tkowa zawartos�� bazy danych
-- W poleceniach SQL mo�na u�ywa� parametr�w o postaci
-- ${resource:nazwa zasobu}, gdzie "nazwa zasobu" jest identyfikatorem
-- napisu z pliku LocalStrings.properties u�ywanego przez
-- InitialConfigurationAction
-- Je�eli warto�� w nawiasach klamrowych nie zaczyna si� od "resource:",
-- jest interpretowana jako wyra�enie OGNL

UPDATE DS_USER 
SET LASTNAME='System'
WHERE ID=1;

UPDATE DS_DIVISION
SET NAME='Root division'
WHERE GUID='rootdivision';

UPDATE DS_FOLDER
SET TITLE='Main'
WHERE ID=1;

UPDATE DS_FOLDER
SET TITLE='Trash Folder'
WHERE ID=2;

UPDATE DS_FOLDER
SET TITLE='Office Documents'
WHERE ID=3;

UPDATE DSO_JOURNAL
SET DESCRIPTION='Root diary'
WHERE ID=1;

UPDATE DSO_JOURNAL
SET DESCRIPTION='Root diary'
WHERE ID=2;

UPDATE DSO_JOURNAL
SET DESCRIPTION='Root diary'
WHERE ID=3;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Interwencja'
WHERE ID=1;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='List'
WHERE ID=2;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Notatka'
WHERE ID=3;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Odwo�anie'
WHERE ID=4;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Opracowanie'
WHERE ID=5;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Projekt'
WHERE ID=6;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Recenzja'
WHERE ID=7;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Skarga'
WHERE ID=8;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Wniosek'
WHERE ID=9;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Wyst�pienie'
WHERE ID=10;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Kontrakt'
WHERE ID=11;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Faktura'
WHERE ID=12;

UPDATE DSO_IN_DOCUMENT_KIND
SET NAME='Dokument biznesowy'
WHERE ID=13;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Registered'
WHERE ID=1;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Pending'
WHERE ID=2;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Processed'
WHERE ID=3;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Finished'
WHERE ID=4;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Archived'
WHERE ID=5;

UPDATE DSO_IN_DOCUMENT_STATUS
SET NAME='Registered'
WHERE ID=6;

UPDATE DSO_CASE_STATUS
SET NAME='Incipient'
WHERE ID=1;

UPDATE DSO_CASE_STATUS
SET NAME='Temporarily finished'
WHERE ID=2;

UPDATE DSO_CASE_STATUS
SET NAME='Finally finished'
WHERE ID=3;

UPDATE DSO_CASE_STATUS
SET NAME='Restored'
WHERE ID=4;

UPDATE DSO_CASE_STATUS
SET NAME='Arranged'
WHERE ID=5;

UPDATE dso_case_priority
SET NAME='Simple'
WHERE ID=1;

UPDATE dso_case_priority
SET NAME='Important'
WHERE ID=2;

UPDATE dso_case_priority
SET NAME='Urgent'
WHERE ID=3;

UPDATE dso_case_priority
SET NAME='Very urgent'
WHERE ID=4;

--UPDATE dso_in_document_delivery
--SET NAME='Post'
--WHERE NAME='Poczta';

--UPDATE dso_in_document_delivery
--SET NAME='Courier'
--WHERE NAME='Kurier';

--UPDATE dso_in_document_delivery
--SET NAME='Personally'
--WHERE NAME='Osobi?cie';

--UPDATE dso_out_document_delivery
--SET NAME='Post'
--WHERE NAME='Poczta';

--UPDATE dso_out_document_delivery
--SET NAME='Personally'
--WHERE NAME='Osobi?cie';

UPDATE dso_role
SET NAME='Reporter'
WHERE ID=1;

UPDATE dso_role
SET NAME='Mayor'
WHERE ID=2;

UPDATE dso_role
SET NAME='Mayor assistant'
WHERE ID=3;

UPDATE dso_role
SET NAME='Secretary of town council'
WHERE ID=4;

UPDATE dso_role
SET NAME='Office employee'
WHERE ID=5;

UPDATE dso_role
SET NAME='Head of a department'
WHERE ID=6;

UPDATE dso_role
SET NAME='Essential administrator'
WHERE ID=7;
commit;