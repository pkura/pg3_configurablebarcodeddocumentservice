create table dsw_killing_log
(
 document_id integer,
 killing_date datetime,
 activity_id integer,
 process_id integer
)

select a.id, a.process_id, c1.param_value as document_id
 from dsw_activity a, dsw_activity_context c1, dsw_activity_context c2, dsw_assignment ass
where state = 'open.running' and a.id = c1.activity_id and c1.param_name = 'ds_document_id'
 and a.id = c2.activity_id and c2.param_name = 'ds_assigned_user' and len(c2.param_value) = 0
 and ass.activity_id = a.id and ass.accepted = 1
order by document_id 

create procedure killXTasks 
as
DECLARE @PROCESS_ID INTEGER
DECLARE @ACTIVITY_ID INTEGER
DECLARE @DOCUMENT_ID INTEGER

DECLARE c0 CURSOR FORWARD_ONLY
  FOR  select a.id, a.process_id, c1.param_value as document_id
 from dsw_activity a, dsw_activity_context c1, dsw_activity_context c2, dsw_assignment ass
where state = 'open.running' and a.id = c1.activity_id and c1.param_name = 'ds_document_id'
 and a.id = c2.activity_id and c2.param_name = 'ds_assigned_user' and len(c2.param_value) = 0
 and ass.activity_id = a.id and ass.accepted = 1
order by document_id 
OPEN c0
FETCH NEXT FROM c0 INTO @activity_id, @process_id, @document_id

WHILE @@FETCH_STATUS = 0
   BEGIN
    insert into dsw_killing_log values (@DOCUMENT_ID, getDate(), @ACTIVITY_ID, @PROCESS_ID)
	delete from dsw_assignment where activity_id = @ACTIVITY_ID
	update dsw_activity set state = 'closed.completed' where id = @ACTIVITY_ID
	update dsw_process set state = 'closed.completed' where id = @PROCESS_ID
    FETCH NEXT FROM c0 INTO @activity_id, @process_id, @document_id
   END

