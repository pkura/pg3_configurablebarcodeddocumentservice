-- PLIK W KODOWANIU ISO-8859-2
-- W tym pliku znajduje si� pocz�tkowa zawartos�� bazy danych
-- W poleceniach SQL mo�na u�ywa� parametr�w o postaci
-- ${resource:nazwa zasobu}, gdzie "nazwa zasobu" jest identyfikatorem
-- napisu z pliku LocalStrings.properties u�ywanego przez
-- InitialConfigurationAction
-- Je�eli warto�� w nawiasach klamrowych nie zaczyna si� od "resource:",
-- jest interpretowana jako wyra�enie OGNL

INSERT INTO DS_USER (ID, NAME, FIRSTNAME, LASTNAME, EMAIL, IDENTIFIER, LOGINDISABLED,
USERROLES, CERTIFICATELOGIN, DELETED, HASHEDPASSWORD, SYSTEMUSER) VALUES (GEN_ID(DS_USER_ID, 1), 'admin', 'Administrator',
'Systemu', NULL, NULL, 0, 'admin', 0, 0, '{SHA}0DPiKuNIrrVmD8IUCuw1hQxNqZc=', 0);

INSERT INTO DS_DIVISION (ID, GUID, NAME, DIVISIONTYPE,HIDDEN)
VALUES (GEN_ID(DS_DIVISION_ID, 1), '${@pl.compan.docusafe.core.users.DSDivision@ROOT_GUID}',
'Dzia� g��wny', '${@pl.compan.docusafe.core.users.DSDivision@TYPE_DIVISION}',0); 

INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (0, '${resource:init.rootFolderTitle}', null, 0, current_timestamp, 'admin', null,
${@pl.compan.docusafe.core.base.Folder@ATT_UNDELETABLE});

INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (1, '${resource:init.trashFolderTitle}', 0, 0, current_timestamp, 'admin',
'${@pl.compan.docusafe.core.base.Folder@SN_TRASH}',
${@pl.compan.docusafe.core.base.Folder@ATT_UNDELETABLE});

INSERT INTO DS_FOLDER (ID, TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (2, '${resource:init.officeFolderTitle}', 0, 0, current_timestamp, 'admin',
'${@pl.compan.docusafe.core.base.Folder@SN_OFFICE}',
${@pl.compan.docusafe.core.base.Folder@ATT_UNDELETABLE});

INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (0, '*', '${@pl.compan.docusafe.core.base.ObjectPermission@ANY}',
'${@pl.compan.docusafe.core.base.ObjectPermission@READ}', 0);

INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (1, '*', '${@pl.compan.docusafe.core.base.ObjectPermission@ANY}',
'${@pl.compan.docusafe.core.base.ObjectPermission@READ}', 0);

INSERT INTO DS_FOLDER_PERMISSION (FOLDER_ID, SUBJECT, SUBJECTTYPE, NAME, NEGATIVE)
VALUES (2, '*', '${@pl.compan.docusafe.core.base.ObjectPermission@ANY}',
'${@pl.compan.docusafe.core.base.ObjectPermission@READ}', 0);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, OWNERGUID, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED, CLOSED, CYEAR)
VALUES (0, 1, '${resource:init.mainIncomingJournalDescription}',
null,
'${@pl.compan.docusafe.core.office.Journal@INCOMING}', 1, 'admin', current_timestamp, 0, 0, 2005);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, OWNERGUID, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED, CLOSED, CYEAR)
VALUES (1, 1, '${resource:init.mainOutgoingJournalDescription}',
null,
'${@pl.compan.docusafe.core.office.Journal@OUTGOING}', 1, 'admin', current_timestamp, 0, 0, 2005);

INSERT INTO DSO_JOURNAL (ID, HVERSION, DESCRIPTION, OWNERGUID, JOURNALTYPE, SEQUENCEID, AUTHOR, CTIME, ARCHIVED, CLOSED, CYEAR)
VALUES (2, 1, '${resource:init.mainInternalJournalDescription}',
null,
'${@pl.compan.docusafe.core.office.Journal@INTERNAL}', 1, 'admin', current_timestamp, 0, 0, 2005);

INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (1, 1, 'Interwencja', 14);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (2, 2, 'List', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (3, 3, 'Notatka', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (4, 4, 'Odwo�anie', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (5, 5, 'Opracowanie', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (6, 6, 'Projekt', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (7, 7, 'Recenzja', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (8, 8, 'Skarga', 14);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (9, 9, 'Wniosek', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (10, 10, 'Wyst�pienie', 21);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (11, 11, 'Kontrakt', 30);
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (12, 12, 'Faktura', 14);
--dokument biznesowy musi byc bo nie przyjmiemy zadnego pisma
INSERT INTO DSO_IN_DOCUMENT_KIND (ID, POSN, NAME, DAYS) VALUES (13, 13, 'Dokument biznesowy', 14);



-- nie nalezy zmieniac identyfikatorow statusow
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (1, 1, 'Zarejestrowane');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (2, 2, 'W toku');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (3, 3, 'W przygotowaniu');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (4, 4, 'Zako�czone');
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME) VALUES (5, 5, 'Zarchiwizowane');
-- status o cn przyjety musi byc bo nie przyjmiemy zadnego pisma w wersji angielskiej zmienic tylko name 
INSERT INTO DSO_IN_DOCUMENT_STATUS (ID, POSN, NAME,CN) VALUES (6, 6, 'Przyjety','PRZYJETY');


-- nie nalezy zmieniac identyfikatorow poszczegolnych statusow
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (1, 1, 'Rozpocz�ta');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (2, 2, 'Tymczasowo zako�czona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (3, 3, 'Ostatecznie zako�czona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (4, 4, 'Wznowiona');
INSERT INTO DSO_CASE_STATUS (ID, POSN, NAME) VALUES (5, 5, 'Planowana');

insert into dso_case_priority (id, posn, name) values (1, 1, 'Zwyk�a');
insert into dso_case_priority (id, posn, name) values (2, 2, 'Wa�na');
insert into dso_case_priority (id, posn, name) values (3, 3, 'Pilna');
insert into dso_case_priority (id, posn, name) values (4, 4, 'Bardzo pilna');

insert into dso_in_document_delivery (id, posn, name) values (gen_id(dso_in_document_delivery_id, 1), gen_id(dso_in_document_delivery_id, 0), 'Poczta');
insert into dso_in_document_delivery (id, posn, name) values (gen_id(dso_in_document_delivery_id, 1), gen_id(dso_in_document_delivery_id, 0), 'Kurier');
insert into dso_in_document_delivery (id, posn, name) values (gen_id(dso_in_document_delivery_id, 1), gen_id(dso_in_document_delivery_id, 0), 'Osobi�cie');

insert into dso_out_document_delivery (id, posn, name) values (gen_id(DSO_OUT_DOCUMENT_DELIVERY_ID, 1), gen_id(DSO_OUT_DOCUMENT_DELIVERY_ID, 0), 'Poczta');
insert into dso_out_document_delivery (id, posn, name) values (gen_id(DSO_OUT_DOCUMENT_DELIVERY_ID, 1), gen_id(DSO_OUT_DOCUMENT_DELIVERY_ID, 0), 'Osobi�cie');

insert into dso_role (id, name) values (1, 'Referent');
insert into dso_role (id, name) values (2, 'Prezydent Miasta');
insert into dso_role (id, name) values (3, 'Zast�pca Prezydenta');
insert into dso_role (id, name) values (4, 'Sekretarz Urz�du');
insert into dso_role (id, name) values (5, 'Pracownik kancelarii');
insert into dso_role (id, name) values (6, 'Naczelnik wydzia�u');
insert into dso_role (id, name) values (7, 'Administrator merytoryczny');


insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_WYCH_PRZYJECIE_KO');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'TECZKA_EDYCJA_NUMERU_SPRAWY');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_KOMORKA_MODYFIKACJE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_TWORZENIE_DOWOLNY_SYMBOL');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_KO_MODYFIKACJE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_WSZYSTKIE_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_TWORZENIE_WYBOR_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_KO_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_WSZYSTKIE_MODYFIKACJE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'PISMO_KOMORKA_MODYFIKACJE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_TWORZENIE_WYBOR_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (6, 'PISMO_AKCEPTACJA_CZYSTOPISU');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_TWORZENIE_DOWOLNY_SYMBOL');
insert into dso_role_permissions (role_id, permission_name) values (1, 'SPRAWA_ZAMYKANIE');
insert into dso_role_permissions (role_id, permission_name) values (1, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (1, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (1, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (1, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SLOWNIK_OSOB_DODAWANIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'BIP_EKSPORT');
insert into dso_role_permissions (role_id, permission_name) values (7, 'ZADANIA_WSZYSTKIE_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'TECZKA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'WWF_DEKRETACJA_ZBIORCZA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'DZIENNIK_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'ZASTEPSTWA_EDYCJA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_ZAMYKANIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_ZMIANA_TERMINU');
insert into dso_role_permissions (role_id, permission_name) values (7, 'DZIENNIK_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SLOWNIK_OSOB_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'ZBIORCZA_DEKRETACJA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_WSZYSTKIE_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (7, 'RWA_ZMIANA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMA_KO_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMA_KOMORKA_NADRZEDNA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (7, 'PISMO_AKCEPTACJA_CZYSTOPISU');
insert into dso_role_permissions (role_id, permission_name) values (7, 'SPRAWA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SLOWNIK_OSOB_DODAWANIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'BIP_EKSPORT');
insert into dso_role_permissions (role_id, permission_name) values (2, 'ZADANIA_WSZYSTKIE_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (2, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'TECZKA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'WWF_DEKRETACJA_ZBIORCZA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'DZIENNIK_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'ZASTEPSTWA_EDYCJA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_ZAMYKANIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMO_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_ZMIANA_TERMINU');
insert into dso_role_permissions (role_id, permission_name) values (2, 'DZIENNIK_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SLOWNIK_OSOB_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'ZBIORCZA_DEKRETACJA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_WSZYSTKIE_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (2, 'RWA_ZMIANA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMA_KO_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMA_KOMORKA_NADRZEDNA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (2, 'PISMO_AKCEPTACJA_CZYSTOPISU');
insert into dso_role_permissions (role_id, permission_name) values (2, 'SPRAWA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (3, 'ZASTEPSTWA_EDYCJA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (3, 'BIP_EKSPORT');
insert into dso_role_permissions (role_id, permission_name) values (3, 'PISMO_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (3, 'SPRAWA_ZMIANA_TERMINU');
insert into dso_role_permissions (role_id, permission_name) values (3, 'PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE');
insert into dso_role_permissions (role_id, permission_name) values (3, 'ZBIORCZA_DEKRETACJA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (3, 'TECZKA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'WWF_DEKRETACJA_ZBIORCZA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'SPRAWA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (3, 'PISMO_AKCEPTACJA_CZYSTOPISU');
insert into dso_role_permissions (role_id, permission_name) values (3, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (3, 'SPRAWA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (4, 'ZASTEPSTWA_EDYCJA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SPRAWA_ZAMYKANIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SLOWNIK_OSOB_DODAWANIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMO_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SPRAWA_ZMIANA_TERMINU');
insert into dso_role_permissions (role_id, permission_name) values (4, 'DZIENNIK_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SLOWNIK_OSOB_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMO_AKCEPTACJA_CZYSTOPISU_COFNIECIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'TECZKA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SPRAWA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMO_AKCEPTACJA_CZYSTOPISU');
insert into dso_role_permissions (role_id, permission_name) values (4, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'DZIENNIK_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (4, 'BIP_EKSPORT');
insert into dso_role_permissions (role_id, permission_name) values (4, 'ZBIORCZA_DEKRETACJA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'RWA_ZMIANA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMA_KO_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMA_KOMORKA_NADRZEDNA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (4, 'WWF_DEKRETACJA_ZBIORCZA');
insert into dso_role_permissions (role_id, permission_name) values (4, 'SPRAWA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (5, 'PISMA_KO_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (5, 'SLOWNIK_OSOB_DODAWANIE');
insert into dso_role_permissions (role_id, permission_name) values (5, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (5, 'PISMA_KOMORKA_NADRZEDNA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (5, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (5, 'SLOWNIK_OSOB_USUWANIE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_ZAMYKANIE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (6, 'WWF_DEKRETACJA_DOWOLNA');
insert into dso_role_permissions (role_id, permission_name) values (6, 'PISMO_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (6, 'PISMA_KOMORKA_PRZYJECIE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_TWORZENIE');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_ZMIANA_TERMINU');
insert into dso_role_permissions (role_id, permission_name) values (6, 'TECZKA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_ZMIANA_REFERENTA');
insert into dso_role_permissions (role_id, permission_name) values (6, 'SPRAWA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (6, 'ZADANIE_KONCZENIE');
insert into dso_role_permissions (role_id, permission_name) values (1, 'ZADANIA_KOMORKA_PODGLAD');
insert into dso_role_permissions (role_id, permission_name) values (1, 'SPRAWA_KOMORKA_PODGLAD');
insert into ds_document_kind (ID,HVERSION,NAME,TABLENAME,ENABLED,CONTENT,CN) values (gen_id(ds_document_kind_id, 1),0,'Zwyk�y dokument','dsg_normal_dockind',1,'<?xml version="1.0" encoding="UTF-8"?><doctype><fields></fields></doctype>','normal');
