-- skrypt czysci smieci w naszym recznym workflow
-- uzywac na wylaczonym tomcacie
delete from dsw_process_context where process_id in (select id from dsw_process where state = 'closed.completed');
delete from dsw_activity_context where activity_id in (select id from dsw_activity where state = 'closed.completed');
delete from dsw_assignment where activity_id in (select id from dsw_activity where state = 'closed.completed');
delete from dsw_process_activities where activity_id in (select id from dsw_activity where state = 'closed.completed');
delete from dsw_process_activities where process_id in (select id from dsw_process where state = 'closed.completed');

delete from dsw_activity where state = 'closed.completed';
delete from dsw_process where state = 'closed.completed';