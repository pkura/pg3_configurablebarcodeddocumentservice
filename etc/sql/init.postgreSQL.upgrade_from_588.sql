
---postgres

---alter table DSW_JBPM_TASKLIST add resource_count INTEGER  default 1 not null;
---ALTER TABLE DSW_JBPM_TASKLIST ADD markerClass character varying(30);
---alter table DS_ATTACHMENT add field_cn character varying(200);
ALTER TABLE DS_DOCUMENT_KIND alter column TABLENAME  type character varying(200);



update dso_person set country = 2 where country ='PL';

CREATE TABLE sso (
idsso numeric (18,0) NOT NULL,
login_ad character varying(50) NOT NULL,
guid_source character varying(50) ,
guid_sso character varying(50) ,
is_active boolean ,
login_date DATE ,
redirect_url character varying(1000) ,
constraint PK_sso primary key (idsso)
);

CREATE SEQUENCE SEQ_SSO_IDSSO
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;

CREATE OR REPLACE FUNCTION insert_to_SSO()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.idsso IS NULL THEN
   New.idsso:=nextval('SEQ_SSO_IDSSO');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_SSO_INSERT
 BEFORE INSERT ON SSO
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_SSO();




alter table DS_ATTACHMENT add field_cn character varying(200);
alter table ds_calendar_event add periodIndex numeric (18,0);

alter table ds_rejection_reason add periodIndex numeric (18,0);

alter table dsg_centrum_kosztow_faktury add 
	amountUsed double precision;


alter table dso_in_document add recipientUser  character varying(62);

alter table dso_document_asgn_history add id numeric (18,0);
create sequence dso_document_asgn_history_ID;

update dso_document_asgn_history set id = nextval('dso_document_asgn_history_ID');
ALTER TABLE dso_document_asgn_history ALTER COLUMN id TYPE numeric (18,0);
ALTER TABLE dso_document_asgn_history  ALTER COLUMN id SET NOT NULL;
ALTER TABLE dso_document_asgn_history ADD PRIMARY KEY (ID);
alter table dso_document_asgn_history drop column posn;
create table dso_doc_asgn_history_targets
(
	type numeric (18,0),
	value character varying(62),
	aheId numeric (18,0),
	CONSTRAINT dso_document_asgn_history_fk2 FOREIGN KEY (aheId) REFERENCES dso_document_asgn_history  (ID)
);
create table dso_doc_asgn_history_targets
(
	type numeric (18,0),
	value character varying(62),
	aheId numeric (18,0),
	CONSTRAINT dso_document_asgn_history_fk2 FOREIGN KEY (aheId) REFERENCES dso_document_asgn_history  (ID)
);

alter table dso_out_document add zpo boolean;

alter table ds_user add internal_number character varying(30);

CREATE TABLE dsg_external_dictionaries 
(
	id numeric (18,0),
	nazwa_polaczenia character varying(64),
	nazwa_slownika character varying(64),
	nazwa_kolumny_oryginalna character varying(64),
	nazwa_kolumny_uzytkownika character varying(64),
	typ_kolumny character varying(32),
	precyzja_kolumny character varying(16)
);

create sequence dsg_external_dictionaries_id;

ALTER TABLE dso_in_document  ALTER COLUMN POSTALREGNUMBER TYPE character varying(25);


alter table dsg_centrum_kosztow_faktury add itemNo numeric (18,0);

CREATE TABLE ds_epuap_skrytka
(
	id numeric (18,0),
	adres_skrytki character varying(300),
	nazwa_skrytki character varying(300),
	identyfikator_podmiotu character varying(300),
	available boolean default false
);
create sequence ds_epuap_skrytka_id start with 1 ;


ALTER TABLE ds_epuap_skrytka add param1 character varying(300);
ALTER TABLE ds_epuap_skrytka add param2 character varying(300);
ALTER TABLE dsg_external_dictionaries ADD nazwa_tabeli character varying(64);
alter table dsg_centrum_kosztow_faktury add additionalId integer ;
alter table dsg_centrum_kosztow_faktury add inventoryId integer ;

ALTER TABLE ds_epuap_export_document ADD epuap_skrytka int;

alter table DS_USER add isSupervisor boolean default false not null;
alter table dsg_centrum_kosztow_faktury add orderId int;
alter table dsg_centrum_kosztow_faktury add groupId int ;
alter table dsg_centrum_kosztow_faktury add productId int;



CREATE TABLE DSO_USER_TO_BRIEFCASE(
	ID numeric (18,0) NOT NULL,
	containerId numeric (18,0) NOT NULL,
	containerOfficeId character varying(84) NOT NULL,
	userId numeric (18,0) NOT NULL,
	userName character varying(62),
	userFirstname character varying(62),
	userLastname character varying(62)
);


alter table DSO_USER_TO_BRIEFCASE add canView boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canCreate boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canModify boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canPrint boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canViewCases boolean not null;
alter table DSO_USER_TO_BRIEFCASE add canAddCasesToBriefcase boolean not null;

create sequence DSO_USER_TO_BRIEFCASE_ID start with 1 ;
ALTER TABLE dso_rwa  ALTER COLUMN digit1 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit2 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit3 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit4 TYPE character varying(50);
ALTER TABLE dso_rwa  ALTER COLUMN digit5 TYPE character varying(50);

ALTER TABLE ds_acceptance_division  ALTER COLUMN CODE TYPE character varying(30);

alter table dso_document_asgn_history add deadline character(10);
alter table dso_journal_entry add author character(50);
ALTER TABLE ds_epuap_skrytka add certyfikat_nazwa character varying(300);

ALTER TABLE DSW_JBPM_TASKLIST  ALTER COLUMN person_organization TYPE character varying(128);

ALTER TABLE DSO_IN_DOCUMENT ADD MESSAGE_ID numeric (18,0);

ALTER TABLE ds_document_acceptance  ALTER COLUMN acceptanceCn  set NOT NULL;
ALTER TABLE ds_document_acceptance  ALTER COLUMN acceptanceCn TYPE character varying(36);

ALTER TABLE ds_epuap_skrytka add certyfikat_haslo character varying(300);
ALTER TABLE ds_epuap_skrytka add email character varying(300);

ALTER TABLE dso_person  ALTER COLUMN organizationdivision TYPE character varying(128);

alter table dso_document_asgn_history add clerk character varying(62);

alter table ds_electronic_signature add is_timestamp_sign boolean;
update ds_electronic_signature set is_timestamp_sign = false;

ALTER TABLE DSW_TASKLIST ADD isAnyPdfInAttachments boolean;
ALTER TABLE DSW_JBPM_TASKLIST ADD isAnyPdfInAttachments boolean;

ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_IS_SSL  boolean default false not null;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_IS_TLS  boolean default false not null;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_PASSWORD character varying(100);
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_PORT int;
ALTER TABLE DSO_EMAIL_CHANNEL ADD SMTP_USERNAME character varying(50);

ALTER TABLE DS_BOOKMARK ADD CN character varying(64);

CREATE TABLE DS_USER_ADDITIONAL_DATA(	
ID numeric (18,0), 
USER_ID numeric (18,0), 
MAIL_FOOTER text,
CONSTRAINT DS_USER_ADDITIONAL_DATA_FK FOREIGN KEY (USER_ID) REFERENCES DS_USER (ID)
);

CREATE SEQUENCE DS_USER_ADDITIONAL_DATA_ID START WITH 1 INCREMENT BY 1 ;

CREATE OR REPLACE FUNCTION insert_to_DS_USER_ADDITIONAL_DATA()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.idsso IS NULL THEN
   New.idsso:=nextval('DS_USER_ADDITIONAL_DATA_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_DS_USER_ADDITIONAL_DATA
 BEFORE INSERT ON DS_USER_ADDITIONAL_DATA
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_USER_ADDITIONAL_DATA();

COMMENT ON TABLE DS_USER_ADDITIONAL_DATA  IS 'Tabela przechowuje dodatkowe dane powiązane z użytkownikiem';

alter table DSO_PERSON add externalID character varying(50);

---629
alter table dsg_centrum_kosztow_faktury add acceptance_mode smallint;
alter table dsg_centrum_kosztow add acceptance_modes_binary int;
alter table dsg_centrum_kosztow_faktury add analytics_id int;
alter table dsg_centrum_kosztow_faktury add descriptionAmount character varying(600);


ALTER TABLE dso_person  ALTER COLUMN organization TYPE character varying(249);

ALTER TABLE DSO_PERSON  ALTER COLUMN COUNTRY TYPE character varying(3);
ALTER TABLE DSO_IN_DOCUMENT ADD  invoiceKind INTEGER;

alter table dso_label add cn character varying(30);

alter table ds_profile_to_user add USERKEY character varying(2000);
alter table ds_profile_to_user add ID numeric (18,0);

alter table ds_profile_to_user drop column ID;

ALTER TABLE dso_out_document ADD gauge numeric (18,0);




CREATE TABLE dso_document_out_gauge (
  id numeric (18,0) NOT NULL,
  cn  character varying(20),
  title  character varying(20)
);

--635
ALTER TABLE dso_out_document ADD gauge numeric (18,0);

CREATE sequence dso_document_out_gauge_id 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1
;

CREATE TABLE dso_document_out_gauge (
  id numeric (18,0) NOT NULL,
  cn  character varying(20),
  title  character varying(20)
);


CREATE OR REPLACE FUNCTION insert_to_GAUGE()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('dso_document_out_gauge_id');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;

 CREATE TRIGGER TRG_dso_document_out_gauge
 BEFORE INSERT
 ON dso_document_out_gauge
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_GAUGE();
 
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('A', 'A');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('B', 'B');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('C', 'C');
INSERT INTO dso_document_out_gauge (cn, title) VALUES ('D', 'D');

--636
alter table ds_electronic_signature add verifyResult boolean;
alter table ds_electronic_signature add verifyDate date ;
alter table ds_electronic_signature add baseSignatureId numeric (18,0);


--637
CREATE TABLE DS_PRINTERS(
	ID int,
	USERPRINTERNAME character varying(64),
	USEPRINTER boolean,
	PRINTERNAME character varying(64),
	FIRSTLINE character varying(64),
	SECONDLINE character varying(64),
	BARCODETYPE character varying(64),
	BARCODEVALUE character varying(64),
	LANGUAGE character varying(64)
);
commit;


CREATE TABLE DS_PRINTER_DEFINED_FIELDS
(
	PRINTER_ID INT,
	FIELD character varying(128)
);
commit;


CREATE sequence DS_PRINTERS_ID 
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;
commit;


CREATE OR REPLACE FUNCTION insert_to_DS_PRINTERS()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_PRINTERS_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
commit;
drop trigger if exists TRG_DS_PRINTERS_INSERT on DS_PRINTERS;
 CREATE TRIGGER TRG_DS_PRINTERS_INSERT
 BEFORE INSERT
 ON DS_PRINTERS
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_PRINTERS();
commit;
 
---638
 alter table ds_subst_hist add substituteCreator character varying(32);

UPDATE ds_subst_hist set substituteCreator = 'admin' where substituteCreator is null;

alter table ds_subst_hist
add status smallint;

UPDATE ds_subst_hist set status = 1;
---639
update ds_profile_to_user SET USERKEY='#' WHERE USERKEY IS NULL;
update ds_profile_to_user SET USERKEY='#' WHERE USERKEY='';

---640
ALTER TABLE DS_DOCUMENT ADD VERSION numeric (18,0) DEFAULT(1);
ALTER TABLE DS_DOCUMENT ADD VERSION_DESC character varying(60);
ALTER TABLE DS_DOCUMENT ADD CZY_CZYSTOPIS boolean;
ALTER TABLE DS_DOCUMENT ADD VERSION_GUID numeric (18,0);
ALTER TABLE DS_DOCUMENT ADD CZY_AKTUALNY boolean;
---641
UPDATE DS_DOCUMENT SET VERSION = 1 WHERE VERSION IS NULL;
UPDATE DS_DOCUMENT SET VERSION_GUID = ID WHERE VERSION_GUID IS NULL;
UPDATE DS_DOCUMENT SET CZY_CZYSTOPIS = false WHERE CZY_CZYSTOPIS IS NULL;
UPDATE DS_DOCUMENT SET CZY_AKTUALNY = true WHERE CZY_AKTUALNY IS NULL;
---642
alter table dso_person
add phoneNumber character varying(15);
---633
ALTER TABLE DSO_DOC_ASGN_HISTORY_TARGETS  ALTER COLUMN VALUE TYPE character varying(62);
---644
ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;
ALTER TABLE ds_attachment_revision
ADD  jackrabbit_server_id SMALLINT NULL;

CREATE TABLE ds_jackrabbit_server (
  id SMALLINT NOT NULL PRIMARY KEY,
  status character varying(255) NOT NULL,
  internal_url character varying(255) NOT NULL,
  user_url character varying(255) NOT NULL,
  login character varying(255),
  password character varying(255),
  ignore_ssl_errors boolean DEFAULT false NOT NULL
);

CREATE SEQUENCE ds_jackrabbit_server_seq start with 1 ;

---645
ALTER TABLE DSO_PERSON  ALTER COLUMN STREET TYPE character varying(100);
---646
CREATE VIEW PROCESS_REPORT_VIEW (ID, DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

---akutalne procesy
select distinct
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,--- 'id',
	tVar.LONG_VALUE_									,--- 'document id',
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  --- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_ AS character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

---oraz
UNION ALL

---hist. procesy
select
	case
		when tHisTask.DBID_ is not null
		then tHisTask.DBID_
		else tPart.DBID_
	end                                                 ,--- 'id',
	CAST(tHistVar.VALUE_ AS bigint)				,--- 'document id',
	
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 --- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tHistVar.HPROCI_ AS  character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';	
---647

CREATE TABLE DS_INTERNAL_SIGN(
	ID numeric (18,0) NOT NULL,
	USER_ID numeric (18,0) ,
	CTIME timestamp ,
	SIGNATURE character varying(256) 
);

CREATE SEQUENCE DS_INTERNAL_SIGN_SEQ start with 1 ;

CREATE TABLE DS_INT_TO_DOC(
	ID numeric (18,0) NOT NULL,
	SIGN_ID numeric (18,0) ,
	DOCUMENT_ID numeric (18,0) ,
	CTIME timestamp
);

CREATE SEQUENCE DS_INT_TO_DOC_SEQ start with 1 ;

---648
ALTER TABLE DS_DOCUMENT ADD isArchived boolean;
alter table DS_DOCUMENT add archivedDate timestamp;
---649

DROP VIEW PROCESS_REPORT_VIEW;


CREATE VIEW PROCESS_REPORT_VIEW (DOCUMENT_ID_, PROCESS_NAME_, STATUS_CN_, START_, END_, DURATION_, DIVISION_ID_, DIVISION_, USER_ID_, USER_)
AS

---akutalne procesy
select distinct
	tVar.LONG_VALUE_									,--- 'document id',
	SUBSTRING(tExe.ID_, 0, (position('.' in tExe.ID_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))  --- 'user'
from
	JBPM4_VARIABLE tVar
	inner join JBPM4_EXECUTION tExe						on tExe.id_ like CONCAT('%.',CAST(tVar.EXECUTION_   AS character varying(255)))
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(tExe.id_, '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tVar.key_='doc-id'
	and tHisActInst.TYPE_ = 'task'

---oraz
UNION

---hist. procesy
select distinct
	CAST(tHistVar.VALUE_ AS bigint)				,--- 'document id',
	SUBSTRING(tHisActInst.EXECUTION_, 0, (position('.' in tHisActInst.EXECUTION_))-1)	        ,--- 'process name',
	tHisActInst.ACTIVITY_NAME_  						,--- 'status',
	tHisActInst.START_									,--- 'start',
	tHisActInst.END_									,--- 'end',
	tHisActInst.DURATION_								,--- 'duration',
	tDivision.ID										,--- 'division id',
	tDivision.NAME										,--- 'division',
	tUser.ID                                            ,--- 'user id',
	CONCAT(tUser.FIRSTNAME, CONCAT(' ',tUser.LASTNAME))	 --- 'user'
from
	JBPM4_HIST_VAR tHistVar
	inner join JBPM4_HIST_ACTINST tHisActInst			on tHisActInst.EXECUTION_ like CONCAT(CONCAT('%.',CAST(tHistVar.HPROCI_ AS character varying(255))), '%')
	left join JBPM4_HIST_TASK tHisTask					on tHisTask.DBID_= tHisActInst.HTASK_
	left join JBPM4_PARTICIPATION tPart					on tPart.TASK_ = tHisActInst.HTASK_
	left join DS_USER tUser								on tPart.USERID_ = tUser.NAME or tHisTask.ASSIGNEE_ =  tUser.NAME
	left join DS_DIVISION tDivision						on tPart.GROUPID_ = tDivision.GUID
where
	tHisActInst.TYPE_ = 'task';

---650
alter table dso_in_document add contractId numeric (18,0);
alter table dso_out_document add countryLetter boolean;
---651
alter table ds_jackrabbit_server rename to ds_jcr_repository;
--652
alter table dso_email_channel add userLogin character varying(50);	
---653
create sequence DS_AR_UDOSTEPNIONE_ZASOBY_ID start with 1 ;
create sequence DS_AR_UDOSTEPNIONE_ZASOBY_ID start with 1 ;

CREATE TABLE DS_AR_UDOSTEPNIONE_ZASOBY(
	ID numeric (18,0),
	USERNAME character varying(50),
	DATA_UDOSTEPNIENIA timestamp ,
	DATA_ZWROTU timestamp,
	RODZAJ character varying(20),
	ZASOB_ID numeric (18,0)
);
---654
CREATE TABLE ds_process_for_document(
  engine_id integer NOT NULL,
  process_id character varying(255) NOT NULL,
  document_id numeric (18,0) NOT NULL,
  definition_id character varying(255) NOT NULL,

  PRIMARY KEY (process_id, engine_id)
);

CREATE INDEX idx_process_for_documentDocDef ON ds_process_for_document (document_id, definition_id);

---655
alter table ds_document add accessToDocument numeric (18,0);

alter table ds_user add limitMobile double precision;
alter table ds_user add limitPhone double precision;

ALTER TABLE ds_user DROP COLUMN authlevel;
ALTER TABLE ds_user ADD COLUMN authlevel smallint;
update ds_user set authlevel = 0;

ALTER TABLE ds_user ALTER COLUMN authlevel SET DEFAULT 0;


ALTER TABLE dsw_jbpm_tasklist ALTER backup_person TYPE bool USING CASE WHEN backup_person=0 THEN FALSE ELSE TRUE END;

ALTER TABLE DS_USER drop active_directory_user;
ALTER TABLE DS_USER add active_directory_user boolean default false;
ALTER TABLE DS_USER ALTER active_directory_user TYPE bool USING CASE WHEN active_directory_user=0 THEN FALSE ELSE TRUE END;

ALTER TABLE DS_DOCUMENT drop index_status_id;
ALTER TABLE DS_DOCUMENT add index_status_id  smallint ;
update DS_DOCUMENT set index_status_id = 0; 
ALTER TABLE ds_document ALTER COLUMN index_status_id SET NOT NULL;

   
ALTER TABLE JBPM4_LOB drop BLOB_VALUE_;
ALTER TABLE JBPM4_LOB add BLOB_VALUE_  numeric (18,0) ;

ALTER TABLE DS_FOLDER drop anchor;
ALTER TABLE DS_FOLDER add anchor  integer ;
  
  
ALTER TABLE jbpm4_execution ALTER hasvars_ TYPE bool USING CASE WHEN hasvars_=0 THEN FALSE ELSE TRUE END;
  
ALTER TABLE jbpm4_task ALTER signalling_ TYPE bool USING CASE WHEN signalling_=0 THEN FALSE ELSE TRUE END;
   
ALTER TABLE jbpm4_task ALTER hasvars_ TYPE bool USING CASE WHEN hasvars_=0 THEN FALSE ELSE TRUE END;
ALTER TABLE JBPM4_VARIABLE ALTER HIST_ TYPE bool USING CASE WHEN HIST_=0 THEN FALSE ELSE TRUE END;
ALTER TABLE dsw_jbpm_tasklist ALTER accepted TYPE bool USING CASE WHEN accepted=1 THEN true ELSE false END;

ALTER TABLE DSO_OUT_DOCUMENT ALTER priority TYPE bool USING CASE WHEN priority=0 THEN FALSE ELSE TRUE END;
		
