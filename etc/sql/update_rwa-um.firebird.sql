INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '1','5','4',NULL,4,'A','Bc','Decyzje w�jta (burmistrza, prezydenta miasta)',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '1','5','5',NULL,4,'A','Bc','Realizacja uchwa� rady gminy',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '1','5','6',NULL,4,'A','Bc','Postanowienia w�jta (burmistrza, prezydenta miasta)',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '1','5','7',NULL,4,'A','Bc','Realizacja zarz�dze� w�jta (burmistrza, prezydenta miasta)',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '5','6','5',NULL,4,'B-5','Bc','Interwencje mieszka�c�w',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '0', '7','1','8',NULL,4,'BE-5','Bc','Wsp�dzielenie z gminnymi jednostkami organizacyjnymi',NULL);

UPDATE DSO_RWA SET DIGIT4='0' WHERE DIGIT1='0' AND DIGIT2='9' AND DIGIT3='0' AND DIGIT4='1' AND RWALEVEL=4;
UPDATE DSO_RWA SET DIGIT4='1' WHERE DIGIT1='0' AND DIGIT2='9' AND DIGIT3='0' AND DIGIT4='2' AND RWALEVEL=4;
UPDATE DSO_RWA SET DIGIT4='2' WHERE DIGIT1='0' AND DIGIT2='9' AND DIGIT3='0' AND DIGIT4='3' AND RWALEVEL=4;

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '1', '0','2','6',NULL,4,'B-10','Bc','Sprawy osobowe os�b dopuszczonych do informacji niejawnych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '1', '0','2','7',NULL,4,'B-50','Bc','Ewidencja os�b dopuszczonych do informacji niejawnych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '2', '3','2','8',NULL,4,'B-5','Bc','Karty drogowe',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '2', '3','2','9',NULL,4,'B-5','Bc','Tablice rejestracyjne',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '2', '3','4',NULL,NULL,3,'B-5','Bc','Ubezpieczenia samochodu',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '2','1','3',NULL,4,'B-5b','Bc','Odsetki od terminowych sp�at nale�no�ci',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','2','8',NULL,4,'B-5','Bc','Wezwania do zap�aty i wyja�nienia dotycz�ce zobowi�za� i nale�no�ci',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','2','9',NULL,4,'B-10','Bc','Zobowi�zania, umowy, por�czenia',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='3' AND DIGIT2='3' AND DIGIT3='6' AND RWALEVEL=3;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','6','0',NULL,4,'B-5','Bc','Wytyczne i wyja�nienia dla jednostek organizacyjnych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','6','1',NULL,4,'B-5','Bc','Harmonogramy wydatk�w i dochod�w',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','6','2',NULL,4,'B-5','Bc','Sprawy bankowe podleg�ych jednostek',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','6','3',NULL,4,'B-5','Bc','Badanie bilans�w',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '3', '3','6','4',NULL,4,'B-5','Bc','Inne sprawy dotycz�ce ksi�gowo�ci bud�etu gminy',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='1' AND DIGIT4='5' AND RWALEVEL=4;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','5','0',5,'B-5','Bc','Decyzje dotycz�ce zameldowa�',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','5','1',5,'B-5','Bc','Decyzje dotycz�ce wymeldowa�',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','5','2',5,'B-5','Bc','Decyzje dotycz�ce ustalania charakteru pobytu',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','5','3',5,'B-5','Bc','Decyzje dotycz�ce wpisania do rejestru wyborc�w',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','5','4',5,'B-5','Bc','Decyzje dotycz�ce skre�le� z rejestru wyborc�w',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='1' AND DIGIT4='8' AND RWALEVEL=4;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','8','0',5,'B-5','Bc','Udost�pnianie danych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','1','8','1',5,'B-5','Bc','Decyzje dotycz�ce odmowy udzielania informacji osobowych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '1','3','9',NULL,4,'B-50','Bc','Ustalanie brzmienia i pisowni imion i nazwisk',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='5' AND DIGIT2='2' AND DIGIT3='1' AND DIGIT4='0' AND RWALEVEL=4;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','0',5,'B-5','Bc','Dzia�alno�� komisji',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','1',5,'B-5','Bc','Przygotowanie poboru, listy poborowych, ksi��ki orzecze� lekarskich',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','2',5,'B-5','Bc','Post�powanie w sprawach orzecznictwa komisji lekarskich',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','3',5,'B-5','Bc','Post�powanie w sprawach orzecznictwa komisji poborowych w zakresie s�u�by zast�pczej',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','4',5,'B-5','Bc','Zawiadomienia o uj�ciu do listy poborowych i o zg�oszeniu si� do poboru',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','0','5',5,'B-5','Bc','Poszukiwania poborowych i post�powanie przymuszaj�ce do zg�oszenia si� do poboru',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='5' AND DIGIT2='2' AND DIGIT3='1' AND DIGIT4='1' AND RWALEVEL=4;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','1','0',5,'B-5','Bc','Organizacja poboru',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','1','1',5,'B-5','Bc','Przygotowanie rejestracji przedpoborowych i rejestry przedpoborowych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','1','2',5,'B-5','Bc','Zawiadomienia o uj�ciu do wykazu przedpoborowych i zg�oszeniu si� do rejestracji',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','1','1','3',5,'B-5','Bc','Poszukiwania przedpoborowych i post�powanie przymuszaj�ce do zg�oszenia si� do rejestracji przedpoborowych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','3','6',NULL,4,'B-5','Bc','Biuro rzeczy znalezionych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','4',NULL,NULL,3,NULL,NULL,'Ochrona danych osobowych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','4','0',NULL,4,'B-5','Bc','Wnioski o zg�oszeniu zbioru danych osobowych do rejestracji',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','4','1',NULL,4,'B-5','Bc','Wnioski i informacje o zbieraniu, przetwarzaniu i udost�pnianiu danych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '2','4','2',NULL,4,'B-5','Bc','Organizacja zabezpieczania danych osobowych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '4','1','6',NULL,4,'B-10','Bc','Komendant Gminny Ochrony Przeciwpo�arowej',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL, DESCRIPTION='Kl�ski �ywio�owe, katastrofy i awarie' WHERE DIGIT1='5' AND DIGIT2='4' AND DIGIT3='2' AND DIGIT4='5' AND RWALEVEL=4;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '4','2','5','0',5,'B-20','Bc','Usuwanie skutk�w kl�sk �ywio�owych, katastrof i awarii',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '4','2','5','1',5,'B-20','Bc','Przywracanie do stanu przed kl�sk� �ywio�ow�, katastrof� i awari�',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '4','2','6',NULL,4,'B-10','Bc','Ubezpieczenie cz�onk�w Ochotniczej Stra�y Po�arnej i m�odzie�owych dru�yn po�arniczych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','2','4',NULL,4,'B-50','Bc','Wydawanie i cofanie uprawnie� dla diagnost�w',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','2','5',NULL,4,'B-10','Bc','Wydawanie i cofanie uprawnie� do prowadzenia stacji diagnostycznych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','2','6',NULL,4,'B-3','Bc','Okresowe kontrole stacji diagnostycznych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','2','7',NULL,4,'B-10','Bc','Ograniczenie zakresu bada� prowadzonych przez stacje diagnostyczne',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','3','7',NULL,4,'B-5','Bc','Wydawanie uprawnie� dla egzaminator�w na prawo jazdy kat. T',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','3','8',NULL,4,'B-50','Bc','Egzaminy pa�stwowe na prawa jazdy',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '5', '5','3','9',NULL,4,'B-10','Bc','Wpisanie i skre�lenie z ewidencji instruktor�w',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '0','5','5',NULL,4,'B-5','Bc','Nadz�r nad �r�dl�dow� gospodark� ryback�',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '0','5','6',NULL,4,'B-5','Bc','Rejestracja sprz�tu p�ywaj�cego s�u��cego do po�owu ryb',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '0','5','7',NULL,4,'B-5','Bc','Wydawanie kart w�dkarskich',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '4','1','3',NULL,4,'B-5','Bc','Za�wiadczenie o wpisie z innych miast',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '4','1','4',NULL,4,'B-5','Bc','Za�wiadczenia potwierdzaj�ce dane ze zbioru ewidencji dzia�alno�ci gospodarczej',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '6', '4','3','6',NULL,4,'B-5','Bc','Ewidencja i wydawanie koncesji na �wiadczenie us�ug przewozowych',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '1','1','5',NULL,4,'B-5','Bc','Wsp�lnoty mieszkaniowe',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '1','4','3','3',5,'B-3','Bc','Dodatki mieszkaniowe',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','1','5',NULL,4,'B-5','Bc','Zatwierdzanie kryterium bilansowo�ci z��',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','1','6',NULL,4,'B-5','Bc','Zatwierdzanie projektu zagospodarowania z�o�a',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','1','6',NULL,4,'B-5','Bc','Zatwierdzanie projektu zagospodarowania z�o�a',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='7' AND DIGIT2='5' AND DIGIT3='3' AND RWALEVEL=3;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','3','0',NULL,4,'B-5','Bc','Zatwierdzanie projekt�w prac hydrogeologicznych',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','3','1',NULL,4,'B-5','Bc','Zatwierdzenie projektu zagospodarowania z�o�a',NULL);

UPDATE DSO_RWA SET ACHOME=NULL, ACOTHER=NULL WHERE DIGIT1='7' AND DIGIT2='5' AND DIGIT3='4' AND RWALEVEL=3;
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','4','0',NULL,4,'B-5','Bc','Zatwierdzanie prac projekt�w geologiczno-in�ynierskich',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '5','4','1',NULL,4,'B-5','Bc','Zatwierdzanie dokumentacji geologiczno-in�ynierskiej',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '6','3','8',NULL,4,'B-5','Bc','Opiniowanie wniosk�w o obni�enie op�aty eksploatacyjnej',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '6','6',NULL,NULL,3,NULL,NULL,'Gospodarka odpadami',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '6','6','0',NULL,4,'BE-5','Bc','Odpady niebezpieczne',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '7', '6','6','1',NULL,4,'B-5','Bc','Odpady inne ni� niebezpieczne',NULL);

INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '8', '1','7','0',NULL,4,'B-5','Bc','Zwalczanie i zapobieganie alkoholizmowi',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '8', '1','7','1',NULL,4,'B-5','Bc','Zwalczanie i zapobieganie narkomanii',NULL);
INSERT INTO DSO_RWA (ID, DIGIT1, DIGIT2, DIGIT3, DIGIT4, DIGIT5,RWALEVEL, ACHOME, ACOTHER, DESCRIPTION, REMARKS) VALUES (gen_id(dso_rwa_id, 1), '8', '1','7','2',NULL,4,'B-5','Bc','Zwalczanie i zapobieganie innym szkodliwym zjawiskom i patologiom',NULL);

UPDATE DSO_RWA SET DIGIT4='1' WHERE DIGIT1='3' AND DIGIT2='0' AND DIGIT3='4' AND DIGIT4='2' AND RWALEVEL=4;
UPDATE DSO_RWA SET DIGIT4='2' WHERE DIGIT1='3' AND DIGIT2='0' AND DIGIT3='4' AND DIGIT4='3' AND RWALEVEL=4;

UPDATE DSO_RWA SET DESCRIPTION='Wpisywanie zagranicznych akt�w s.c., odtwarzanie tre�ci akt�w s.c., statystyka' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND RWALEVEL=3;
UPDATE DSO_RWA SET ACHOME='A', DESCRIPTION='Wpisywanie tre�ci zagranicznych akt�w s.c. do polskich ksi�g s.c.' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='0' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='B-50', DESCRIPTION='Odtwarzanie tre�ci akt�w s.c. w razie zagini�cia lub zniszczenia ksi�gi przed i po wej�ciu ustawy w �ycie' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='1' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='B-50', DESCRIPTION='Odtwarzanie tre�ci akt�w s.c. sporz�dzonych za granic�' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='2' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='A', DESCRIPTION='Statystyka urodze�, ma��e�stw i zgon�w' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='3' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='B-10', DESCRIPTION='Sprawy przekazywane do rozstrzygni�cia s�dom opieku�czym' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='4' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='B-5', DESCRIPTION='Po�wiadczenia dokument�w w celu odtworzenia akt s�dowych' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='5' AND RWALEVEL=4;
UPDATE DSO_RWA SET ACHOME='B-10', DESCRIPTION='Zawiadomienia o przypiskach do akt stanu cywilnego' WHERE DIGIT1='5' AND DIGIT2='1' AND DIGIT3='5' AND DIGIT4='6' AND RWALEVEL=4;