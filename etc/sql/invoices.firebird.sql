create table dsr_inv_invoice (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    HVERSION                INTEGER NOT NULL,
    SEQUENCEID              INTEGER NOT NULL,
    CREATE_YEAR             INTEGER NOT NULL,
    CTIME                   TIMESTAMP NOT NULL,
    AUTHOR                  VARCHAR(62) NOT NULL,
    INVOICEDATE             DATE NOT NULL,
    PAYMENTDATE             DATE NOT NULL,
    SALEDATE                DATE,
    TERMSOFPAYMENT          VARCHAR(20) NOT NULL,
    INVOICENO               VARCHAR(50) NOT NULL,
    GROSS                   NUMERIC(18,2) NOT NULL,
    NET                     NUMERIC(18,2) NOT NULL,
    VAT                     INTEGER,
    DESCRIPTION             VARCHAR(160) NOT NULL COLLATE PXW_PLK,
    CONTRACTNO              VARCHAR(50) COLLATE PXW_PLK,
    ACCEPTINGUSER           VARCHAR(62) ,
    ACCEPTDATE              DATE,
    ACCEPTRETURNDATE        DATE,
    BUDGETARYDESCRIPTION    VARCHAR(50) COLLATE PXW_PLK,
    RELAYEDTO               VARCHAR(80) COLLATE PXW_PLK,
    RB_DATE                 DATE,
    VENDOR_ID               NUMERIC(18,0) NOT NULL,
    DELETED                 SMALLINT NOT NULL,
    REMARKS                 VARCHAR(512) COLLATE PXW_PLK,
    division_guid           varchar(62),
    lparam                  VARCHAR(200) COLLATE PXW_PLK,
    document_id 	   		numeric(18,0),
    contractid              numeric(18,0),
    wparam                  NUMERIC(18,0),
    kind 					integer,
    cancelled 	            smallint
);

CREATE TABLE DSO_INVOICES_KIND (
  ID                     INTEGER                NOT NULL PRIMARY KEY
, NAME                   VARCHAR( 40 )          NOT NULL
, DAYS                   SMALLINT
, POSN                   INTEGER
);


create generator dso_invoices_kind_id;

insert into dso_invoices_kind (id,name,days,posn) values (1,'Faktura',14,1);
insert into dso_invoices_kind (id,name,days,posn) values (2,'Rachunek',14,2);
insert into dso_invoices_kind (id,name,days,posn) values (3,'Upomnienie',14,3);
insert into dso_invoices_kind (id,name,days,posn) values (4,'Korekta',14,4);
insert into dso_invoices_kind (id,name,days,posn) values (5,'Kwit',14,5);
insert into dso_invoices_kind (id,name,days,posn) values (6,'Nota ksi�gowa',14,6);
insert into dso_invoices_kind (id,name,days,posn) values (7,'Rozliczenie',14,7);
insert into dso_invoices_kind (id,name,days,posn) values (8,'Inne',14,8);

update dsr_inv_invoice
set kind = 1
where invoicekind = 'INVOICE';

update dsr_inv_invoice
set kind = 2
where invoicekind = 'CHECK';

update dsr_inv_invoice
set kind = 3
where invoicekind = 'REMINDER';

update dsr_inv_invoice
set kind = 4
where invoicekind = 'REVISION';

update dsr_inv_invoice
set kind = 5
where invoicekind = 'SIMPLE_CHECK';

update dsr_inv_invoice
set kind = 6
where invoicekind = 'NOTE';

update dsr_inv_invoice
set kind = 7
where invoicekind = 'SQUAREUP';

update dsr_inv_invoice
set kind = 8
where invoicekind = 'OTHER';

set generator dso_invoices_kind_id to 10;







--create unique index dsr_inv_invoice_seq on dsr_inv_invoice(sequenceid);
create unique index dsr_inv_invoice_seq on dsr_inv_invoice(create_year, sequenceid);

CREATE GENERATOR dsr_inv_invoice_id;
SET GENERATOR dsr_inv_invoice_id TO 1;

create table dsr_inv_invoice_audit (
    invoice_id          NUMERIC(18,0) NOT NULL,
    posn                INTEGER NOT NULL,
    ctime               TIMESTAMP NOT NULL,
    property            VARCHAR(62),
    username            VARCHAR(62)  NOT NULL COLLATE PXW_PLK,
    description         VARCHAR(200)  NOT NULL COLLATE PXW_PLK,
    lparam              VARCHAR(200)  COLLATE PXW_PLK,
    wparam              NUMERIC(18,0),
    priority            INTEGER
);

alter table dsr_inv_invoice_audit add constraint dsr_inv_invoice_audit_fk1
foreign key (invoice_id) references dsr_inv_invoice (id);

create table dsr_inv_vendor (
    ID                      NUMERIC(18,0) NOT NULL PRIMARY KEY,
    NAME                    VARCHAR(160) NOT NULL COLLATE PXW_PLK,
    ZIP                     VARCHAR(14) COLLATE PXW_PLK,
    STREET                  VARCHAR(50) COLLATE PXW_PLK,
    LOCATION                VARCHAR(50) COLLATE PXW_PLK,
    NIP                     VARCHAR(15) COLLATE PXW_PLK,
    REGON                   VARCHAR(15) COLLATE PXW_PLK,
    COUNTRY                 VARCHAR(2) COLLATE PXW_PLK,
    PHONE                   VARCHAR(30) COLLATE PXW_PLK,
    FAX                     VARCHAR(30) COLLATE PXW_PLK,
    lparam                  VARCHAR(200)  COLLATE PXW_PLK,
    wparam                  NUMERIC(18,0)
);

CREATE GENERATOR dsr_inv_vendor_id;
SET GENERATOR dsr_inv_vendor_id TO 1;

alter table dsr_inv_invoice add constraint dsr_inv_invoice_vendor_fk
foreign key (vendor_id) references dsr_inv_vendor (id);
