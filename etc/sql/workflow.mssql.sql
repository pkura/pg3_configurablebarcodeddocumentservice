-- poni�sze polecenia znajduj� si� ju� w og�lnym skrypcie office.vsql
-- wi�c przy tworzeniu nowej instancji bazy nie trzeba tego wykonywa� (ale skrypt ten mo�e by�
-- przydatny jak chcemy doda� modu� Workflow do wcze�niej utworzonej bazy)

CREATE TABLE DSW_JBPM_PROCESS_DEFINITION(
    id					numeric(19,0) identity (1,1) not null,
	definitionname	    varchar(62),
    definitionid        numeric(19,0),
	nameforuser			varchar(100),
	action				varchar(200),
    definitionversion   int
);

CREATE TABLE DSW_PROCESS_REMINDER(
    id					numeric(19,0) identity (1,1) not null,
    checkTime           datetime,
	reminderType	    int,
    jbpmTaskId          numeric(19,0),
	lparam  			numeric(19,0),
	wparam				varchar(200)
);

CREATE TABLE DSW_JBPM_TASK_DEFINITION(
    id					numeric(19,0) identity (1,1) not null,
	name	            varchar(62),
    jbpmProcessDefinitionName        varchar(62),
	nameforuser			varchar(100),
	action				varchar(200),
);

CREATE TABLE DSW_JBPM_SWIMLANE_MAPPING(
    id					numeric(19,0) identity (1,1) not null,
	name	            varchar(62),
    divisionGuid        varchar(62),
	lastUserNo          int,
	assignmentType      int
);