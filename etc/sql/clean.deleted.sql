delete from ds_attachment_revision where id in (select rev.id from ds_attachment_revision rev join ds_attachment att on rev.attachment_id = att.id join ds_document doc on att.document_id = doc.id where doc.deleted = 1);

delete from ds_attachment where id in (select att.id from ds_attachment att join ds_document doc on att.document_id = doc.id where doc.deleted = 1);

delete from ds_document_lock where document_id in (select id from ds_document where deleted = 1);
delete from ds_document_watch where document_id in (select id from ds_document where deleted = 1);
delete from ds_document_permission where document_id in (select id from ds_document where deleted = 1);

delete from ds_document where deleted = 1;
