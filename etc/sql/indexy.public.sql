-- indeksy ktorych brakue w wersji dla administracji publicznej

create index i_dso_in_1 on dso_in_document (documentdate);
create index i_dso_in_2 on dso_in_document (stampdate);
create index i_dso_in_3 on dso_in_document (incomingdate);
create index i_dso_in_4 on dso_in_document (postalregnumber);
create index i_dso_in_5 on dso_in_document (officenumber);
create index i_dso_in_6 on dso_in_document (officenumberyear);
create index i_dso_in_7 on dso_in_document (case_id);
create index i_dso_in_8 on dso_in_document (casedocumentid);
create index i_dso_in_9 on dso_in_document (master_document_id);
create index i_dso_in_10 on dso_in_document (referenceid);
create index i_dso_in_11 on dso_in_document (divisionguid);
create index i_dso_in_12 on dso_in_document (journal_id);
create index i_dso_in_14 on dso_in_document (clerk);
create index i_dso_in_15 on dso_in_document (assigneddivision);
create index i_dso_in_16 on dso_in_document (creatinguser);

create index i_dso_out_1 on dso_out_document (documentdate);
create index i_dso_out_2 on dso_out_document (officenumber);
create index i_dso_out_3 on dso_out_document (officenumberyear);
create index i_dso_out_4 on dso_out_document (postalregnumber);
create index i_dso_out_5 on dso_out_document (clerk);
create index i_dso_out_6 on dso_out_document (assigneddivision);
create index i_dso_out_7 on dso_out_document (creatinguser);
create index i_dso_out_8 on dso_out_document (case_id);
create index i_dso_out_9 on dso_out_document (casedocumentid);
create index i_dso_out_10 on dso_out_document (sender);
create index i_dso_out_11 on dso_out_document (journal_id);
create index i_dso_out_12 on dso_out_document (master_document_id);
create index i_dso_out_14 on dso_out_document (referenceid);
create index i_dso_out_15 on dso_out_document (divisionguid);