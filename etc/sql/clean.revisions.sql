drop procedure clean_revisions;

/*
    Usuwa najstarsze wersje za��cznik�w pozostawiaj�c tylko
    REVS najnowszych wersji ka�dego za��cznika.
*/
set term !! ;
create procedure clean_revisions(revs integer)
as
declare variable atid integer;
declare variable maxrev integer;
begin
    for select distinct attachment_id from ds_attachment_revision where revision > :revs into :atid do
    begin
        select max(revision) from ds_attachment_revision where attachment_id = :atid into :maxrev;
        delete from ds_attachment_revision where attachment_id = :atid and revision <= (:maxrev-:revs);
    end
end !!
set term ; !!

-- execute procedure clean_revisions(1);
