create procedure killTask
@document_id integer
as
DECLARE @PROCESS_ID INTEGER
DECLARE @ACTIVITY_ID INTEGER

DECLARE c0 CURSOR FORWARD_ONLY
  FOR  select a.id, a.process_id, c1.param_value as document_id
       from dsw_activity a, dsw_activity_context c1
       where a.id = c1.activity_id and c1.param_name = 'ds_document_id' and c1.param_value = @document_id and c1.datatype_id = 2
       order by document_id 
OPEN c0
FETCH NEXT FROM c0 INTO @activity_id, @process_id, @document_id

WHILE @@FETCH_STATUS = 0
   BEGIN
	delete from dsw_assignment where activity_id = @ACTIVITY_ID
	update dsw_activity set state = 'closed.completed' where id = @ACTIVITY_ID
	update dsw_process set state = 'closed.completed' where id = @PROCESS_ID
	update dsw_process_context set param_value = 'true' where param_name = 'ds_finished' and process_id = @process_id
	update dsw_activity_context set param_value = 'true' where param_name = 'ds_finished' and activity_id = @activity_id
	delete from dsw_tasklist where dso_document_id = @document_id
    FETCH NEXT FROM c0 INTO @activity_id, @process_id, @document_id
   END
   
CLOSE c0
DEALLOCATE c0