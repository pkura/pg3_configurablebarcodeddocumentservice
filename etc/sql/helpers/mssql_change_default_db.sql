-- uruchamiamy poleceniem:
-- sqlcmd -i mssql_change_default_db.sql
USE [master]
GO
ALTER LOGIN [docusafe_ifpan] WITH DEFAULT_DATABASE=[docusafe_ifpan], DEFAULT_LANGUAGE=[polski], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
