-- Skrypt do usuwania dokumentów na twardo z tabeli ds_document.
-- Wersja komtabilna z SQL Server.
-- Prośba o aktualizacje skryptu jeśli brakuje tabel.
-- UWAGA: operacja może nie być poprawna - stosować tylko na środowisku
--        developerskim lub testowym. Na produkcji stosować na własną
--        odpowiedzialność!!

-- Tabela #delete zawiera id dokumentów do usunięcia.

-- Selectujemy wszystkie tabele dockindowe.
select tablename from ds_document_kind

-- Na podstawie listy tabel dockindowych tworzymy skrypty do usuwania z tych tabel.
-- Poniżej przykładowe linie z IFPAN.
delete from dsg_normal_dockind where document_id in (select * from #delete)
delete from dsg_ifpan_zgloszenie where document_id in (select * from #delete)
delete from dsg_ifpan_delegacja where document_id in (select * from #delete)
delete from dsg_ifpan_coinvoice where document_id in (select * from #delete)
delete from dsg_ifpan_coinvoice where document_id in (select * from #delete)
delete from dsg_ifpan_zapotrz where document_id in (select * from #delete)
delete from dsg_ifpan_contract where document_id in (select * from #delete)
delete from dsg_ifpan_bilety where document_id in (select * from #delete)
delete from dsg_ifpan_zapotrz2 where document_id in (select * from #delete)
delete from dsg_security_document where document_id in (select * from #delete)
delete from dsg_normal_dockind where document_id in (select * from #delete)
delete from dsg_normal_dockind where document_id in (select * from #delete)

-- Następnie usuwamy z tabel corowych.
delete from dso_in_document where id in (select * from #delete)
delete from dso_document_audit where document_id in (select * from #delete)
delete from ds_document_permission where document_id in (select * from #delete)
delete from dso_document_asgn_history_targets where aheId in (select id from dso_document_asgn_history where document_id in (select * from #delete))
delete from dso_document_asgn_history where document_id in (select * from #delete)
delete from dso_document_remarks where document_id in (select * from #delete)
delete from ds_document_changelog where document_id in (select * from #delete)
delete from ds_document where id in (select * from #delete)

-- Możemy jeszcze usunąć attachmenty.
delete from ds_attachment_revision where attachment_id in (select id from ds_attachment where document_id in (select * from #delete))
delete from ds_attachment where document_id in (select * from #delete)