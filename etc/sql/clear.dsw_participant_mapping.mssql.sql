--uzupelnienie brak�w w dsw_resources
insert into dsw_resource (res_key,res_name,email) select NAME,LASTNAME+' '+FIRSTNAME,EMAIL from DS_USER u where u.DELETED <> 1  and (select COUNT(1) from dsw_resource where res_key = u.NAME ) < 1  order by id;
--czyszczenie do zera
delete from dsw_participant_mapping;
-- dodanie mapowania z uzytkownikow
insert into dsw_participant_mapping (participant_id,resource_id) select u.NAME,r.id from DS_USER u, dsw_resource r where r.res_key = u.NAME and u.DELETED <> 1;
-- dodanie z dzialow
insert into dsw_participant_mapping (participant_id,resource_id) select 'ds_guid_'+d.GUID,r.id from DS_USER_TO_DIVISION ud, DS_DIVISION d, DS_USER u, dsw_resource r where ud.DIVISION_ID = d.ID and ud.USER_ID = u.ID and u.DELETED <> 1 and d.hidden <> 1 and r.res_key = u.NAME order by d.GUID;