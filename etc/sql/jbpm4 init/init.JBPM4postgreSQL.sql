CREATE TABLE ds_document_to_jbpm4(
	jbpm4_id character varying(50) NOT NULL PRIMARY KEY,
	document_id numeric (18,0) NOT NULL,
	process_name character varying(30) NOT NULL
 );

    create table JBPM4_DEPLOYMENT (
        DBID_ numeric (18,0) not null,
        NAME_ text,
        TIMESTAMP_ numeric (18,0),
        STATE_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_DEPLOYPROP (
        DBID_ numeric (18,0) not null,
        DEPLOYMENT_ numeric (18,0),
        OBJNAME_ character varying(255),
        KEY_ character varying(255),
        STRINGVAL_ character varying(255),
        LONGVAL_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_EXECUTION (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        ACTIVITYNAME_ character varying(255),
        PROCDEFID_ character varying(255),
        HASVARS_ boolean,
        NAME_ character varying(255),
        KEY_ character varying(255),
        ID_ character varying(255) unique,
        STATE_ character varying(255),
        SUSPHISTSTATE_ character varying(255),
        PRIORITY_ INTEGER,
        HISACTINST_ numeric (18,0),
        PARENT_ numeric (18,0),
        INSTANCE_ numeric (18,0),
        SUPEREXEC_ numeric (18,0),
        SUBPROCINST_ numeric (18,0),
        PARENT_IDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_ACTINST (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        HPROCI_ numeric (18,0),
        TYPE_ character varying(255),
        EXECUTION_ character varying(255),
        ACTIVITY_NAME_ character varying(255),
        START_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        TRANSITION_ character varying(255),
        NEXTIDX_ INTEGER,
        HTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_HIST_DETAIL (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        USERID_ character varying(255),
        TIME_ timestamp,
        HPROCI_ numeric (18,0),
        HPROCIIDX_ INTEGER,
        HACTI_ numeric (18,0),
        HACTIIDX_ INTEGER,
        HTASK_ numeric (18,0),
        HTASKIDX_ INTEGER,
        HVAR_ numeric (18,0),
        HVARIDX_ INTEGER,
        MESSAGE_ text,
        OLD_STR_ character varying(255),
        NEW_STR_ character varying(255),
        OLD_INT_ INTEGER,
        NEW_INT_ INTEGER,
        OLD_TIME_ timestamp,
        NEW_TIME_ timestamp,
        PARENT_ numeric (18,0),
        PARENT_IDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_PROCINST (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        PROCDEFID_ character varying(255),
        KEY_ character varying(255),
        START_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        STATE_ character varying(255),
        ENDACTIVITY_ character varying(255),
        NEXTIDX_ INTEGER,
        primary key (DBID_)
    );

    create table JBPM4_HIST_TASK (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        EXECUTION_ character varying(255),
        OUTCOME_ character varying(255),
        ASSIGNEE_ character varying(255),
        PRIORITY_ INTEGER,
        STATE_ character varying(255),
        CREATE_ timestamp,
        END_ timestamp,
        DURATION_ numeric (18,0),
        NEXTIDX_ INTEGER,
        SUPERTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_HIST_VAR (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        PROCINSTID_ character varying(255),
        EXECUTIONID_ character varying(255),
        VARNAME_ character varying(255),
        VALUE_ character varying(255),
        HPROCI_ numeric (18,0),
        HTASK_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_ID_GROUP (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        NAME_ character varying(255),
        TYPE_ character varying(255),
        PARENT_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_ID_MEMBERSHIP (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        USER_ numeric (18,0),
        GROUP_ numeric (18,0),
        NAME_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_ID_USER (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        ID_ character varying(255),
        PASSWORD_ character varying(255),
        GIVENNAME_ character varying(255),
        FAMILYNAME_ character varying(255),
        BUSINESSEMAIL_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_JOB (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        DUEDATE_ timestamp,
        STATE_ character varying(255),
        ISEXCLUSIVE_ INTEGER,
        LOCKOWNER_ character varying(255),
        LOCKEXPTIME_ timestamp,
        EXCEPTION_ text,
        RETRIES_ INTEGER,
        PROCESSINSTANCE_ numeric (18,0),
        EXECUTION_ numeric (18,0),
        CFG_ numeric (18,0),
        SIGNAL_ character varying(255),
        EVENT_ character varying(255),
        REPEAT_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_LOB (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        BLOB_VALUE_ numeric (18,0),
        DEPLOYMENT_ numeric (18,0),
        NAME_ text,
        primary key (DBID_)
    );

    create table JBPM4_PARTICIPATION (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        GROUPID_ character varying(255),
        USERID_ character varying(255),
        TYPE_ character varying(255),
        TASK_ numeric (18,0),
        SWIMLANE_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_PROPERTY (
        KEY_ character varying(255) not null,
        VERSION_ INTEGER not null,
        VALUE_ character varying(255),
        primary key (KEY_)
    );

    create table JBPM4_SWIMLANE (
        DBID_ numeric (18,0) not null,
        DBVERSION_ INTEGER not null,
        NAME_ character varying(255),
        ASSIGNEE_ character varying(255),
        EXECUTION_ numeric (18,0),
        primary key (DBID_)
    );

    create table JBPM4_TASK (
        DBID_ numeric (18,0) not null,
        CLASS_ char(1 ) not null,
        DBVERSION_ INTEGER not null,
        NAME_ character varying(255),
        DESCR_ text,
        STATE_ character varying(255),
        SUSPHISTSTATE_ character varying(255),
        ASSIGNEE_ character varying(255),
        FORM_ character varying(255),
        PRIORITY_ INTEGER,
        CREATE_ timestamp,
        DUEDATE_ timestamp,
        PROGRESS_ INTEGER,
        SIGNALLING_ boolean,
        EXECUTION_ID_ character varying(255),
        ACTIVITY_NAME_ character varying(255),
        HASVARS_ boolean,
        SUPERTASK_ numeric (18,0),
        EXECUTION_ numeric (18,0),
        PROCINST_ numeric (18,0),
        SWIMLANE_ numeric (18,0),
        TASKDEFNAME_ character varying(255),
        primary key (DBID_)
    );

    create table JBPM4_VARIABLE (
        DBID_ numeric (18,0) not null,
        CLASS_ character varying(255) not null,
        DBVERSION_ INTEGER not null,
        KEY_ character varying(255),
        CONVERTER_ character varying(255),
        HIST_ boolean,
        EXECUTION_ numeric (18,0),
        TASK_ numeric (18,0),
        LOB_ numeric (18,0),
        DATE_VALUE_ timestamp,
        DOUBLE_VALUE_ double precision,
        CLASSNAME_ character varying(255),
        LONG_VALUE_ numeric (18,0),
        STRING_VALUE_ character varying(255),
        TEXT_VALUE_ text,
        EXESYS_ numeric (18,0),
        primary key (DBID_)
    );

    create index IDX_DEPLPROP_DEPL on JBPM4_DEPLOYPROP (DEPLOYMENT_);

    alter table JBPM4_DEPLOYPROP 
        add constraint FK_DEPLPROP_DEPL 
        foreign key (DEPLOYMENT_) 
        references JBPM4_DEPLOYMENT;

    create index IDX_EXEC_SUPEREXEC on JBPM4_EXECUTION (SUPEREXEC_);

    create index IDX_EXEC_INSTANCE on JBPM4_EXECUTION (INSTANCE_);

    create index IDX_EXEC_SUBPI on JBPM4_EXECUTION (SUBPROCINST_);

    create index IDX_EXEC_PARENT on JBPM4_EXECUTION (PARENT_);

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_PARENT 
        foreign key (PARENT_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_SUBPI 
        foreign key (SUBPROCINST_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_INSTANCE 
        foreign key (INSTANCE_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_EXECUTION 
        add constraint FK_EXEC_SUPEREXEC 
        foreign key (SUPEREXEC_) 
        references JBPM4_EXECUTION;

    create index IDX_HACTI_HPROCI on JBPM4_HIST_ACTINST (HPROCI_);

    create index IDX_HTI_HTASK on JBPM4_HIST_ACTINST (HTASK_);

    alter table JBPM4_HIST_ACTINST 
        add constraint FK_HACTI_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_ACTINST 
        add constraint FK_HTI_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_HDET_HACTI on JBPM4_HIST_DETAIL (HACTI_);

    create index IDX_HDET_HPROCI on JBPM4_HIST_DETAIL (HPROCI_);

    create index IDX_HDET_HVAR on JBPM4_HIST_DETAIL (HVAR_);

    create index IDX_HDET_HTASK on JBPM4_HIST_DETAIL (HTASK_);

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HACTI 
        foreign key (HACTI_) 
        references JBPM4_HIST_ACTINST;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    alter table JBPM4_HIST_DETAIL 
        add constraint FK_HDETAIL_HVAR 
        foreign key (HVAR_) 
        references JBPM4_HIST_VAR;

    create index IDX_HSUPERT_SUB on JBPM4_HIST_TASK (SUPERTASK_);

    alter table JBPM4_HIST_TASK 
        add constraint FK_HSUPERT_SUB 
        foreign key (SUPERTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_HVAR_HPROCI on JBPM4_HIST_VAR (HPROCI_);

    create index IDX_HVAR_HTASK on JBPM4_HIST_VAR (HTASK_);

    alter table JBPM4_HIST_VAR 
        add constraint FK_HVAR_HPROCI 
        foreign key (HPROCI_) 
        references JBPM4_HIST_PROCINST;

    alter table JBPM4_HIST_VAR 
        add constraint FK_HVAR_HTASK 
        foreign key (HTASK_) 
        references JBPM4_HIST_TASK;

    create index IDX_GROUP_PARENT on JBPM4_ID_GROUP (PARENT_);

    alter table JBPM4_ID_GROUP 
        add constraint FK_GROUP_PARENT 
        foreign key (PARENT_) 
        references JBPM4_ID_GROUP;

    create index IDX_MEM_USER on JBPM4_ID_MEMBERSHIP (USER_);

    create index IDX_MEM_GROUP on JBPM4_ID_MEMBERSHIP (GROUP_);

    alter table JBPM4_ID_MEMBERSHIP 
        add constraint FK_MEM_GROUP 
        foreign key (GROUP_) 
        references JBPM4_ID_GROUP;

    alter table JBPM4_ID_MEMBERSHIP 
        add constraint FK_MEM_USER 
        foreign key (USER_) 
        references JBPM4_ID_USER;

    create index IDX_JOBRETRIES on JBPM4_JOB (RETRIES_);

    create index IDX_JOB_CFG on JBPM4_JOB (CFG_);

    create index IDX_JOB_PRINST on JBPM4_JOB (PROCESSINSTANCE_);

    create index IDX_JOB_EXE on JBPM4_JOB (EXECUTION_);

    create index IDX_JOBLOCKEXP on JBPM4_JOB (LOCKEXPTIME_);

    create index IDX_JOBDUEDATE on JBPM4_JOB (DUEDATE_);

    alter table JBPM4_JOB 
        add constraint FK_JOB_CFG 
        foreign key (CFG_) 
        references JBPM4_LOB;

    create index IDX_LOB_DEPLOYMENT on JBPM4_LOB (DEPLOYMENT_);

    alter table JBPM4_LOB 
        add constraint FK_LOB_DEPLOYMENT 
        foreign key (DEPLOYMENT_) 
        references JBPM4_DEPLOYMENT;

    create index IDX_PART_TASK on JBPM4_PARTICIPATION (TASK_);

    alter table JBPM4_PARTICIPATION 
        add constraint FK_PART_SWIMLANE 
        foreign key (SWIMLANE_) 
        references JBPM4_SWIMLANE;

    alter table JBPM4_PARTICIPATION 
        add constraint FK_PART_TASK 
        foreign key (TASK_) 
        references JBPM4_TASK;

    create index IDX_SWIMLANE_EXEC on JBPM4_SWIMLANE (EXECUTION_);

    alter table JBPM4_SWIMLANE 
        add constraint FK_SWIMLANE_EXEC 
        foreign key (EXECUTION_) 
        references JBPM4_EXECUTION;

    create index IDX_TASK_SUPERTASK on JBPM4_TASK (SUPERTASK_);

    alter table JBPM4_TASK 
        add constraint FK_TASK_SWIML 
        foreign key (SWIMLANE_) 
        references JBPM4_SWIMLANE;

    alter table JBPM4_TASK 
        add constraint FK_TASK_SUPERTASK 
        foreign key (SUPERTASK_) 
        references JBPM4_TASK;

    create index IDX_VAR_EXESYS on JBPM4_VARIABLE (EXESYS_);

    create index IDX_VAR_TASK on JBPM4_VARIABLE (TASK_);

    create index IDX_VAR_EXECUTION on JBPM4_VARIABLE (EXECUTION_);

    create index IDX_VAR_LOB on JBPM4_VARIABLE (LOB_);

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_LOB 
        foreign key (LOB_) 
        references JBPM4_LOB;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_EXECUTION 
        foreign key (EXECUTION_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_EXESYS 
        foreign key (EXESYS_) 
        references JBPM4_EXECUTION;

    alter table JBPM4_VARIABLE 
        add constraint FK_VAR_TASK 
        foreign key (TASK_) 
        references JBPM4_TASK;
