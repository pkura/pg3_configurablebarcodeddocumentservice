net stop "Tomcat7"
cd C:\Program Files (x86)\Apache Software Foundation\Tomcat 7.0\webapps
rd /s /q docusafe
cd ..
cd C:\Program Files (x86)\Apache Software Foundation\Tomcat 7.0\work
rd /s /q Catalina
cd ..
rd /s /q temp
mkdir temp
net start "Tomcat7"