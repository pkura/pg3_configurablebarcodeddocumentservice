<?xml version="1.0" encoding="windows-1250"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
    method="text"
    indent="yes"
    doctype-public="-//Apache Software Foundation//DTD Struts Configuration 1.1//EN"
    doctype-system="http://jakarta.apache.org/struts/dtds/struts-config_1_1.dtd"/>

<xsl:template match="//permission">
<xsl:value-of select="@name"/>
</xsl:template>

</xsl:stylesheet>
