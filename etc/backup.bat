@echo off

set DB=localhost:docusafe
set FIREBIRD=c:\docusafe\firebird_1_5
set OPENLDAP=c:\docusafe\openldap2.0
set BACKUPDIR=c:\docusafe\backup

for /f "tokens=1,2*" %%a in ('date/t') do set DD=%%b
for /f "tokens=1,2 delims=: usebackq" %%a in (`echo %TIME%`) do set TT=%%a%%b

echo %DD%_%TT%

set BACKUP=%BACKUPDIR%\%DD%_%TT%
mkdir %BACKUP%

echo Tworzenie kopii zapasowej bazy Firebird
"%FIREBIRD%\bin\gbak" -user sysdba -password masterkey -B %DB% %BACKUP%\docusafe.bak

if not errorlevel 0 goto ErrorFirebird

echo Tworzenie kopii zapasowej bazy OpenLDAP
"%OPENLDAP%\slapcat" -f %OPENLDAP%\slapd.conf -l %BACKUP%\docusafe.ldif

if not errorlevel 0 goto ErrorLdap

echo Utworzono kopie zapasowa w katalogu %BACKUP%
goto End


:ErrorFirebird
echo Wystapil blad podczas tworzenia kopii zapasowej bazy Firebird
goto End

:ErrorLdap
echo Wystapil blad podczas tworzenia kopii zapasowej bazy OpenLDAP
goto End

:End
