CREATE TABLE dsg_wniosek_uprawnienia (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	CTIME 	            date,
	WNIOSKODAWCA 	    bigint,
	OPIS        		varchar(3000),
	UWAGI        		varchar(1000)
);


ALTER TABLE dsg_wniosek_uprawnienia ADD PRIMARY KEY (document_id);
create index dsg_wniosek_uprawnienia_index on dsg_wniosek_uprawnienia (document_id);


CREATE TABLE dsg_wniosek_uprawnienia_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;
