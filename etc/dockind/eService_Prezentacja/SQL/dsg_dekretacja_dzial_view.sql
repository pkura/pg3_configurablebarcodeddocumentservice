create view dsg_dekretacja_dzial_view as
select ID as id,
GUID as cn,
NAME as title,
CODE as refValue,
null as centrum,
1 as available
from DS_DIVISION
where PARENT_ID=3 AND DIVISIONTYPE = 'division';