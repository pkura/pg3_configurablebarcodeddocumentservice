CREATE TABLE dsg_wniosek_urlopowy (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	WORKER_DIVISION		bigint,
	WNIOSKODAWCA 	    bigint,
	OSOBA_ZASTEPUJ 		bigint,
	URLOPRODZAJ 		bigint,
	CTIME 	            date,
	DATA_OD		        date,
	DATA_DO           	date,
	ILOSC_DNI           int,
	ADD_DOC		        numeric (18,0),
);


ALTER TABLE dsg_wniosek_urlopowy ADD PRIMARY KEY (document_id);
create index dsg_wniosek_urlopowy_index on dsg_wniosek_urlopowy (document_id);


CREATE TABLE dsg_wniosek_urlopowy_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;




CREATE VIEW DS_ABSENCE_TYPE_VIEW AS
    SELECT
        ROW_NUMBER() OVER (ORDER BY NAME) AS ID,
        CODE AS CN,
        NAME AS TITLE,
        '0' AS CENTRUM,
        NULL AS REFVALUE,
        '1' AS AVAILABLE
    FROM
        DS_ABSENCE_TYPE;