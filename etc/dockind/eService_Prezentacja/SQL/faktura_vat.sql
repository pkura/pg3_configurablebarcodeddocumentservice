CREATE TABLE dsg_faktura_vat (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	KWOTA_NETTO			numeric(18,2),
	KWOTA_BRUTTO		numeric(18,2),
	DATA_WYSTAWIENIA 	date,
	DATA_PLATNOSCI 		date,
	OBSZAR 		        int,
	OS_MERYTORYCZNA 	int,
	NA_DZIAL_BOOL		int,
	DZIAL           	int,
	UWAGI           	varchar(1000),
	SYGNATURA_UMOWY		varchar(30),
);


ALTER TABLE dsg_faktura_vat ADD PRIMARY KEY (document_id);
create index dsg_faktura_vat_index on dsg_faktura_vat (document_id);


CREATE TABLE dsg_faktura_vat_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;

