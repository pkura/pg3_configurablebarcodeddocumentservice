create view dsg_mpk_osoby as
select ID as id,
NAME as cn,
FIRSTNAME + ' ' + LASTNAME as title,
SUPERVISOR_ID as refValue,
null as centrum,
1 as available
from DS_USER
where DELETED=0;