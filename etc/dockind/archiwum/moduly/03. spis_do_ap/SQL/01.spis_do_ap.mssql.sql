/*
select * from ds_ar_spis_dok_do_ap
select * from ds_ar_spis_dok_do_ap_multi
select * from ds_ar_teczki_do_ap
select * from ds_ar_sprawy_do_ap
*/


IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_spis_dok_do_ap') 
drop table ds_ar_spis_dok_do_ap;
GO

CREATE TABLE [dbo].[ds_ar_spis_dok_do_ap](
document_id numeric(19,0),
ID integer identity(1,1),
STATUS numeric(2,0),
NUMER numeric(19,0),
KOM_ORG numeric(19,0),
MIEJSCE_AP varchar(240),
WZOR numeric(2,0),
DATA_PRZEK date,
typ int NULL,
MIESCEUTWORZENIA varchar(50) NULL,
DODATKOWE_METADANE numeric(18,0) NULL
)

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_spis_dok_do_ap_multi') 
drop table ds_ar_spis_dok_do_ap_multi;
GO

create table ds_ar_spis_dok_do_ap_multi(
DOCUMENT_ID numeric(19,0),
FIELD_CN varchar(50),
FIELD_VAL varchar(50),
ID integer identity(1,1))

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_teczki_do_ap') 
drop table ds_ar_teczki_do_ap;
GO

create table ds_ar_teczki_do_ap(
ID integer identity(1,1),
IDTECZKI numeric(19,0),
RWATYTUL varchar(300),
SYMBOL varchar(40),
DATAOD date,
DATADO date,
UWAGI varchar(240),
USUN bit
)
alter table ds_ar_teczki_do_ap add IDTECZKI2 varchar(84);

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_sprawy_do_ap') 
drop table ds_ar_sprawy_do_ap;
GO

create table ds_ar_sprawy_do_ap(
ID integer identity(1,1),
ZNAK numeric(19,0),
TYTUL varchar(400),
DATA_ROZP date,
DATA_ZAK date,
RWA_DESC varchar(400),
DOC_COUNT numeric(19,0),
UWAGI varchar(240),
USUN bit
)

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'DS_AR_TECZKI_DO_AP_VIEW') 
DROP VIEW DS_AR_TECZKI_DO_AP_VIEW;
GO
CREATE view DS_AR_TECZKI_DO_AP_VIEW  AS 
  SELECT        aii.ID AS id_Item,aii.ID as ID,con.id as FOLDERID, aii.folder_OFFICEID AS CN, aii.folder_OFFICEID AS TITLE, con.CASE_YEAR, aii.DATE_FROM AS DATAOD, aii.DATE_TO AS DATADO, 
                         rwa.description AS RWATYTUL, rwa.code AS SYMBOL,aii.category_arch as katArch,aii.FOLDER_COUNT as liczbaTomow,aii.FOLDER_LENGTH_METER as metryBiezace, NULL AS refValue, 0 AS CENTRUM, 1 AS available
--                         , CAST(NVL(con.ARCHIVESTATUS, 0) AS varchar) + CAST(kejsy.max AS varchar) AS status
FROM            DS_AR_INVENTORY_ITEMS aii LEFT OUTER JOIN
                         DSO_CONTAINER con ON aii.FOLDER_OFFICEID = con.OFFICEID LEFT OUTER JOIN
                         v_dso_rwaa rwa ON con.RWA_ID = rwa.ID LEFT OUTER JOIN
                             (SELECT        PARENT_ID, ISNULL(MAX(ARCHIVESTATUS), 0) AS max
                               FROM            DSO_CONTAINER concase
                               WHERE        (DISCRIMINATOR = 'CASE') 
                               GROUP BY PARENT_ID) kejsy ON kejsy.PARENT_ID = con.ID
WHERE        ((con.DISCRIMINATOR = 'FOLDER') and con.ARCHIVESTATUS=2 and kejsy.max=2)or (aii.folderid is null and aii.archive_status=2);
go
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'ds_ar_sprawy_do_ap_view') 
DROP VIEW ds_ar_sprawy_do_ap_view;
GO
CREATE VIEW ds_ar_sprawy_do_ap_view
AS
  SELECT        con1.ID, con1.OFFICEID AS TITLE, con1.OFFICEID AS ZNAK, con1.TITLE AS TYTUL,con1.PARENT_ID, con1.FINISHDATE AS DATA_ROZP, con1.OPENDATE AS DATA_ZAK, 
                         rwa.description AS RWA_DESC, doc.doc_count, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            DSO_CONTAINER con1 LEFT OUTER JOIN
                         v_dso_rwaa rwa ON rwa.ID = con1.RWA_ID LEFT OUTER JOIN
                             (SELECT        FOLDER_ID, COUNT(FOLDER_ID) AS doc_count
                               FROM   DS_DOCUMENT
                               GROUP BY FOLDER_ID) doc ON doc.FOLDER_ID = con1.ID
WHERE        (con1.DISCRIMINATOR = 'CASE') AND (con1.PARENT_ID IN
                             (SELECT        con.ID
                               FROM            DS_AR_INVENTORY_ITEMS aii LEFT OUTER JOIN
                                                         DSO_CONTAINER con ON aii.FOLDER_OFFICEID = con.OFFICEID
                              )) AND (con1.DISCRIMINATOR = 'CASE');










