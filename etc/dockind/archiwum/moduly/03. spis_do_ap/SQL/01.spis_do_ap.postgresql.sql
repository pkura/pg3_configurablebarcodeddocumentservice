CREATE TABLE ds_ar_spis_dok_do_ap (
  document_id        NUMERIC(19, 0),
  ID                 SERIAL PRIMARY KEY,
  STATUS             NUMERIC(2, 0),
  NUMER              NUMERIC(19, 0),
  KOM_ORG            NUMERIC(19, 0),
  MIEJSCE_AP         VARCHAR(240),
  WZOR               NUMERIC(2, 0),
  DATA_PRZEK         DATE,
  typ                INT            NULL,
  MIESCEUTWORZENIA   VARCHAR(50)    NULL,
  DODATKOWE_METADANE NUMERIC(18, 0) NULL
);

CREATE TABLE ds_ar_spis_dok_do_ap_multi (
  DOCUMENT_ID NUMERIC(19, 0),
  FIELD_CN    VARCHAR(50),
  FIELD_VAL   VARCHAR(50),
  ID          SERIAL PRIMARY KEY
);


CREATE TABLE ds_ar_teczki_do_ap (
  ID       SERIAL PRIMARY KEY,
  IDTECZKI NUMERIC(19, 0),
  RWATYTUL VARCHAR(300),
  SYMBOL   VARCHAR(40),
  DATAOD   DATE,
  DATADO   DATE,
  UWAGI    VARCHAR(240),
  USUN     BIT
);
ALTER TABLE ds_ar_teczki_do_ap ADD IDTECZKI2 VARCHAR(84);

CREATE TABLE ds_ar_sprawy_do_ap (
  ID        SERIAL PRIMARY KEY,
  ZNAK      NUMERIC(19, 0),
  TYTUL     VARCHAR(400),
  DATA_ROZP DATE,
  DATA_ZAK  DATE,
  RWA_DESC  VARCHAR(400),
  DOC_COUNT NUMERIC(19, 0),
  UWAGI     VARCHAR(240),
  USUN      BIT
);

CREATE VIEW DS_AR_TECZKI_DO_AP_VIEW AS
  SELECT
    aii.ID                  AS id_Item,
    aii.ID                  AS ID,
    con.id                  AS FOLDERID,
    aii.folder_OFFICEID     AS CN,
    aii.folder_OFFICEID     AS TITLE,
    con.CASE_YEAR,
    aii.DATE_FROM           AS DATAOD,
    aii.DATE_TO             AS DATADO,
    rwa.description         AS RWATYTUL,
    rwa.code                AS SYMBOL,
    aii.category_arch       AS katArch,
    aii.FOLDER_COUNT        AS liczbaTomow,
    aii.FOLDER_LENGTH_METER AS metryBiezace,
    NULL                    AS refValue,
    0                       AS CENTRUM,
    1                       AS available
  --                         , CAST(NVL(con.ARCHIVESTATUS, 0) AS varchar) + CAST(kejsy.max AS varchar) AS status
  FROM DS_AR_INVENTORY_ITEMS aii LEFT OUTER JOIN
    DSO_CONTAINER con ON aii.FOLDER_OFFICEID = con.OFFICEID
    LEFT OUTER JOIN
    v_dso_rwaa rwa ON con.RWA_ID = rwa.ID
    LEFT OUTER JOIN
    (SELECT
       PARENT_ID,
       coalesce(MAX(ARCHIVESTATUS), 0) AS max
     FROM DSO_CONTAINER concase
     WHERE (DISCRIMINATOR = 'CASE')
     GROUP BY PARENT_ID) kejsy ON kejsy.PARENT_ID = con.ID
  WHERE ((con.DISCRIMINATOR = 'FOLDER') AND con.ARCHIVESTATUS = 2 AND kejsy.max = 2) OR
        (aii.folderid IS NULL AND aii.archive_status = 2);


CREATE VIEW ds_ar_sprawy_do_ap_view
AS
  SELECT
    con1.ID,
    con1.OFFICEID   AS TITLE,
    con1.OFFICEID   AS ZNAK,
    con1.TITLE      AS TYTUL,
    con1.PARENT_ID,
    con1.FINISHDATE AS DATA_ROZP,
    con1.OPENDATE   AS DATA_ZAK,
    rwa.description AS RWA_DESC,
    doc.doc_count,
    NULL            AS refValue,
    0               AS CENTRUM,
    1               AS available
  FROM DSO_CONTAINER con1 LEFT OUTER JOIN
    v_dso_rwaa rwa ON rwa.ID = con1.RWA_ID
    LEFT OUTER JOIN
    (SELECT
       FOLDER_ID,
       COUNT(FOLDER_ID) AS doc_count
     FROM DS_DOCUMENT
     GROUP BY FOLDER_ID) doc ON doc.FOLDER_ID = con1.ID
  WHERE (con1.DISCRIMINATOR = 'CASE') AND (con1.PARENT_ID IN
                                           (SELECT con.ID
                                            FROM DS_AR_INVENTORY_ITEMS aii LEFT OUTER JOIN
                                              DSO_CONTAINER con ON aii.FOLDER_OFFICEID = con.OFFICEID
                                           )) AND (con1.DISCRIMINATOR = 'CASE');










