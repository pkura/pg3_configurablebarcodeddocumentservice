--Wniosek o udost�pnienie akt

CREATE TABLE DS_AR_WNIOSEK_UDOSTEP (
  DOCUMENT_ID                   NUMERIC(19, 0) NOT NULL,
  data_zlozenia_wniosku         TIMESTAMP       NULL,
  DS_USER_id                    NUMERIC(19, 0) NULL,
  DS_DIVISION_id_uzytkownika    NUMERIC(19, 0) NULL,
  DS_DIVISION_id_wlasciciel_akt NUMERIC(19, 0) NULL,
  sygnatura_archiwalna          VARCHAR(256)   NULL,
  data_dokumenty_od             TIMESTAMP       NULL,
  data_dokumenty_do             TIMESTAMP       NULL,
  tytul                         VARCHAR(255)   NULL,
  is_wypozyczenie_fizyczne      BIT            NULL,
  is_odwzorowanie_cyfrowe       BIT            NULL,
  is_udostepn_akt_na_miejscu    BIT            NULL,
  is_kserokopia                 BIT            NULL,
  is_nadanie_upr_elektr         BIT            NULL,
  inne_ch                       BIT            NULL,
  inne                          TEXT           NULL,
  uzasadnienie                  TEXT           NULL,
  DS_DIVISION_ACCEPT            NUMERIC(19, 0) NULL,
  data_odbioru_dokumentow       TIMESTAMP       NULL,
  powod_odrzucenia              TEXT           NULL,
  data_zwrotu_dokumentow        TIMESTAMP       NULL,
  is_potwierdzony_zwrot         BIT            NULL,
  STATUSDOKUMENTU               INT            NULL,
  SPOSOB_UDOSTEPNIENIA          INT            NULL,
  TERMIN_ZWROTU                 DATE           NULL,
  ODWZOROWANIE_CYFROWE          NUMERIC(18, 0) NULL,
  is_wnioskodawca_zewnetrzny    BIT            NULL,
  AKCEPT_ARCHIWISTA             INT            NULL,
  DATA_AKCEPT_ARCHIWISTY        DATE           NULL,
  STATUS_AKCEPT_ARCHIWISTY      INT            NULL,
  AKCEPT_KIEROWNIK              INT            NULL,
  DATA_AKCEPT_KIEROWNIK         DATE           NULL,
  STATUS_AKCEPT_KIEROWNIK       INT            NULL,
  AKCEPT_REKTORA                INT            NULL,
  DATA_AKCEPT_REKTORA           DATE           NULL,
  STATUS_AKCEPT_REKTORA         INT            NULL,
  typ                           INT            NULL,
  MIESCEUTWORZENIA              VARCHAR(50)    NULL,
  WNIOSK_ZEW_OS_PRAW            SMALLINT       NULL,
  DODATKOWE_METADANE            NUMERIC(18, 0) NULL
);

CREATE TABLE DS_AR_WNIOSEK_UDOSTEP_MULTI (
  DOCUMENT_id NUMERIC(19, 0) NOT NULL,
  FIELD_CN    VARCHAR(40)    NULL,
  FIELD_VAL   VARCHAR(40)    NULL,
  ID          SERIAL PRIMARY KEY         NOT NULL
);

--S�ownik zasob�w niezdigitalizowanych
CREATE TABLE DS_AR_NIEZDIGITALIZOWANE (
  ID          SERIAL PRIMARY KEY       NOT NULL,
  DESCRIPTION VARCHAR(255) NOT NULL,
  CTIME       DATE         NOT NULL,
  TITLE       VARCHAR(255) NOT NULL
);

--tabela akt publicznie dostepnych
CREATE TABLE DS_AR_DOKUMENTY_PUBLICZNE (
  DOCUMENT_ID   NUMERIC(18, 0),
  AUTHOR        VARCHAR(255),
  TITLE         VARCHAR(255),
  CTIME         DATE,
  FORMAT        VARCHAR(50),
  DOSTEP        VARCHAR(50),
  TYP_DOKUMENTU VARCHAR(50)
);

ALTER TABLE DS_AR_DOKUMENTY_PUBLICZNE ADD abstract VARCHAR(255) NULL;
ALTER TABLE DS_AR_DOKUMENTY_PUBLICZNE ADD locations VARCHAR(255) NULL;
ALTER TABLE DS_AR_DOKUMENTY_PUBLICZNE ADD organizational_unit VARCHAR(255) NULL;

--widok dla dostepnych spraw
CREATE VIEW DS_AR_SPRAWY_VIEW
AS
  SELECT
    DSO_CONTAINER.ID,
    DSO_CONTAINER.OFFICEID,
    DSO_CONTAINER.CTIME,
    DSO_CONTAINER.description,
    coalesce(dso_rwa.digit1, '')
    + coalesce(dso_rwa.digit2, '') + coalesce(dso_rwa.digit3, '') + coalesce(dso_rwa.digit4, '') +
    coalesce(dso_rwa.digit5, '') AS RWA,
    DSO_CONTAINER.TITLE
  FROM DSO_CONTAINER
    INNER JOIN
    dso_rwa ON DSO_CONTAINER.RWA_ID = dso_rwa.ID
  WHERE (DSO_CONTAINER.DISCRIMINATOR = 'CASE');

--widok dla dost�pnych teczek
CREATE VIEW DS_AR_TECZKI_VIEW
AS
  SELECT
    DSO_CONTAINER.ID,
    DSO_CONTAINER.OFFICEID,
    DSO_CONTAINER.CTIME,
    coalesce(dso_rwa.digit1, '') + coalesce(dso_rwa.digit2, '')
    + coalesce(dso_rwa.digit3, '') + coalesce(dso_rwa.digit4, '') + coalesce(dso_rwa.digit5, '') AS RWA,
    DSO_CONTAINER.NAME
  FROM DSO_CONTAINER
    INNER JOIN
    dso_rwa ON DSO_CONTAINER.RWA_ID = dso_rwa.ID
  WHERE (DSO_CONTAINER.DISCRIMINATOR = 'FOLDER');


--widok dla dost�pnych dokument�w
CREATE VIEW DS_AR_DOKUMENTY_VIEW
AS
  SELECT
    ID,
    DESCRIPTION,
    CTIME,
    TITLE
  FROM DS_DOCUMENT;