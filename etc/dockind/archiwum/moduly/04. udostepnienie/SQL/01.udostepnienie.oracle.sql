--Wniosek o udost�pnienie akt
CREATE TABLE DS_AR_WNIOSEK_UDOSTEP(
	DOCUMENT_ID number(18, 0) PRIMARY KEY,
	data_zlozenia_wniosku date ,
	DS_USER_id number(19, 0) ,
	DS_DIVISION_id_uzytkownika number(19, 0) ,
	DS_DIVISION_id_wlasciciel_akt number(19, 0) ,
	sygnatura_archiwalna VARCHAR2(256) ,
	data_dokumenty_od date ,
	data_dokumenty_do date ,
	tytul VARCHAR2(255) ,
	is_wypozyczenie_fizyczne NUMBER(3) ,
	is_odwzorowanie_cyfrowe NUMBER(3) ,
	is_udostepn_akt_na_miejscu NUMBER(3) ,
	is_kserokopia NUMBER(3) ,
	is_nadanie_upr_elektr NUMBER(3) ,
	inne_ch NUMBER(3) ,
	inne VARCHAR2(512) ,
	uzasadnienie VARCHAR2(512) ,
	DS_DIVISION_ACCEPT NUMBER(19, 0) ,
	data_odbioru_dokumentow date ,
	powod_odrzucenia VARCHAR2(512) ,
	data_zwrotu_dokumentow date ,
	is_potwierdzony_zwrot NUMBER(3) ,
	STATUSDOKUMENTU NUMBER(10) ,
	SPOSOB_UDOSTEPNIENIA NUMBER(10) ,
	TERMIN_ZWROTU date ,
	ODWZOROWANIE_CYFROWE NUMBER(18, 0) ,
	is_wnioskodawca_zewnetrzny NUMBER(3) ,
	AKCEPT_ARCHIWISTA NUMBER(10) ,
	DATA_AKCEPT_ARCHIWISTY date ,
	STATUS_AKCEPT_ARCHIWISTY NUMBER(10) ,
	AKCEPT_KIEROWNIK NUMBER(10) ,
	DATA_AKCEPT_KIEROWNIK date ,
	STATUS_AKCEPT_KIEROWNIK NUMBER(10) ,
	AKCEPT_REKTORA NUMBER(10) ,
	DATA_AKCEPT_REKTORA date ,
	STATUS_AKCEPT_REKTORA NUMBER(10)
)
alter table DS_AR_WNIOSEK_UDOSTEP add WNIOSK_ZEW_OS_PRAW smallint;


CREATE TABLE DS_AR_WNIOSEK_UDOSTEP_MULTI(
	ID NUMBER(19, 0) PRIMARY KEY,
  DOCUMENT_id NUMBER(19, 0) ,
	FIELD_CN varchar2(40) ,
	FIELD_VAL varchar2(40)
);
COMMIT;

drop sequence DS_AR_WNIOSEK_UDOSTEP_MULTI_ID;
create sequence DS_AR_WNIOSEK_UDOSTEP_MULTI_ID;

drop trigger DS_AR_WNIOSEK_UDOSTEP_MULTI_tr;
create trigger DS_AR_WNIOSEK_UDOSTEP_MULTI_tr
before insert on DS_AR_WNIOSEK_UDOSTEP_MULTI
for each row
begin
select DS_AR_WNIOSEK_UDOSTEP_MULTI_ID.nextval
into :new.ID
from dual;
end;

--S�ownik zasob�w niezdigitalizowanych
CREATE TABLE DS_AR_NIEZDIGITALIZOWANE(
	ID NUMBER(18, 0) PRIMARY KEY,
	DESCRIPTION varchar2(255) NOT NULL,
	CTIME date NOT NULL,
	TITLE varchar2(255) NOT NULL
);

drop sequence DS_AR_NIEZDIGITALIZOWANE_ID;
create sequence DS_AR_NIEZDIGITALIZOWANE_ID;


drop trigger DS_AR_NIEZDIGITALIZOWANE_ID_tr;
create trigger DS_AR_NIEZDIGITALIZOWANE_ID_tr
before insert on DS_AR_NIEZDIGITALIZOWANE
for each row
begin
select DS_AR_NIEZDIGITALIZOWANE_ID.nextval
into :new.ID
from dual;
end;



--widok dla dostepnych spraw
CREATE VIEW DS_AR_SPRAWY_VIEW
AS
SELECT        a.ID,
              a.OFFICEID,
              a.CTIME,
              a.description,
              NVL(b.digit1, '') || NVL(b.digit2, '')
                || NVL(b.digit3, '') || NVL(b.digit4, '')
                || NVL(b.digit5, '') AS RWA,
              a.TITLE
FROM DSO_CONTAINER a
INNER JOIN dso_rwa b ON a.RWA_ID = b.ID
WHERE        (a.DISCRIMINATOR = 'CASE');

--widok dla dost�pnych teczek
CREATE VIEW DS_AR_TECZKI_VIEW
AS
SELECT        a.ID,
              a.OFFICEID,
              a.CTIME,
              NVL(b.digit1, '') + NVL(b.digit2, '')
                + NVL(b.digit3, '') + NVL(b.digit4, '')
                + NVL(b.digit5, '') AS RWA,
              a.NAME
FROM DSO_CONTAINER a
INNER JOIN dso_rwa b ON a.RWA_ID = b.ID
WHERE        (a.DISCRIMINATOR = 'FOLDER')

--widok dla dost�pnych dokument�w
CREATE VIEW DS_AR_DOKUMENTY_VIEW
AS
SELECT        ID, DESCRIPTION, CTIME, TITLE
FROM            DS_DOCUMENT;


--tabela akt publicznie dostepnych
CREATE TABLE DS_AR_DOKUMENTY_PUBLICZNE(
  	DOCUMENT_ID NUMBER(18, 0),
  	AUTHOR varchar2(255) ,
  	TITLE varchar2(255) ,
  	CTIME DATE ,
  	FORMAT varchar2(50),
  	DOSTEP varchar2(50),
  	TYP_DOKUMENTU varchar2(50)
  );

alter table DS_AR_DOKUMENTY_PUBLICZNE add abstract varchar2(255) NULL;
alter table DS_AR_DOKUMENTY_PUBLICZNE add locations varchar2(255) NULL;
alter table DS_AR_DOKUMENTY_PUBLICZNE add organizational_unit varchar2(255) NULL;

