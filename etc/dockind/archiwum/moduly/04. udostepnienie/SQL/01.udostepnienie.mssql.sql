/*
select * from DS_AR_WNIOSEK_UDOSTEP
select * from DS_AR_WNIOSEK_UDOSTEP_MULTI
select * from DS_AR_NIEZDIGITALIZOWANE
select * from DS_AR_DOKUMENTY_PUBLICZNE
*/

--Wniosek o udost�pnienie akt
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_WNIOSEK_UDOSTEP') 
DROP TABLE DS_AR_WNIOSEK_UDOSTEP;
GO
CREATE TABLE [DS_AR_WNIOSEK_UDOSTEP](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[data_zlozenia_wniosku] [datetime] NULL,
	[DS_USER_id] [numeric](19, 0) NULL,
	[DS_DIVISION_id_uzytkownika] [numeric](19, 0) NULL,
	[DS_DIVISION_id_wlasciciel_akt] [numeric](19, 0) NULL,
	[sygnatura_archiwalna] [varchar](256) NULL,
	[data_dokumenty_od] [datetime] NULL,
	[data_dokumenty_do] [datetime] NULL,
	[tytul] [varchar](255) NULL,
	[is_wypozyczenie_fizyczne] [bit] NULL,
	[is_odwzorowanie_cyfrowe] [bit] NULL,
	[is_udostepn_akt_na_miejscu] [bit] NULL,
	[is_kserokopia] [bit] NULL,
	[is_nadanie_upr_elektr] [bit] NULL,
	[inne_ch] [bit] NULL,
	[inne] [text] NULL,
	[uzasadnienie] [text] NULL,
	[DS_DIVISION_ACCEPT] [numeric](19, 0) NULL,
	[data_odbioru_dokumentow] [datetime] NULL,
	[powod_odrzucenia] [text] NULL,
	[data_zwrotu_dokumentow] [datetime] NULL,
	[is_potwierdzony_zwrot] [bit] NULL,
	[STATUSDOKUMENTU] [int] NULL,
	[SPOSOB_UDOSTEPNIENIA] [int] NULL,
	[TERMIN_ZWROTU] [date] NULL,
	[ODWZOROWANIE_CYFROWE] [numeric](18, 0) NULL,
	[is_wnioskodawca_zewnetrzny] [bit] NULL,
	[AKCEPT_ARCHIWISTA] [int] NULL,
	[DATA_AKCEPT_ARCHIWISTY] [date] NULL,
	[STATUS_AKCEPT_ARCHIWISTY] [int] NULL,
	[AKCEPT_KIEROWNIK] [int] NULL,
	[DATA_AKCEPT_KIEROWNIK] [date] NULL,
	[STATUS_AKCEPT_KIEROWNIK] [int] NULL,
	[AKCEPT_REKTORA] [int] NULL,
	[DATA_AKCEPT_REKTORA] [date] NULL,
	[STATUS_AKCEPT_REKTORA] [int] NULL,
	[typ] [int] NULL,
    [MIESCEUTWORZENIA] [varchar](50) NULL,
    [WNIOSK_ZEW_OS_PRAW] [smallint] NULL,
    [DODATKOWE_METADANE] [numeric](18,0) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_WNIOSEK_UDOSTEP_MULTI') 
DROP TABLE DS_AR_WNIOSEK_UDOSTEP_MULTI;
GO

CREATE TABLE [DS_AR_WNIOSEK_UDOSTEP_MULTI](
	[DOCUMENT_id] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](40) NULL,
	[FIELD_VAL] [varchar](40) NULL,
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_NIEZDIGITALIZOWANE') 
DROP TABLE DS_AR_NIEZDIGITALIZOWANE ;
GO

--S�ownik zasob�w niezdigitalizowanych
CREATE TABLE [DS_AR_NIEZDIGITALIZOWANE](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DESCRIPTION] [varchar](255) NOT NULL,
	[CTIME] [date] NOT NULL,
	[TITLE] [varchar](255) NOT NULL,
 CONSTRAINT [PK_DS_AR_WNIOSEK_UDOSTEP_NIEZDIGITALIZOWANE] PRIMARY KEY CLUSTERED
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_DOKUMENTY_PUBLICZNE') 
DROP TABLE DS_AR_DOKUMENTY_PUBLICZNE ;
GO
--tabela akt publicznie dostepnych
CREATE TABLE DS_AR_DOKUMENTY_PUBLICZNE(
  	DOCUMENT_ID numeric(18, 0),
  	AUTHOR varchar(255) ,
  	TITLE varchar(255) ,
  	CTIME DATE ,
  	FORMAT varchar(50),
  	DOSTEP varchar(50),
  	TYP_DOKUMENTU varchar(50)
  );

alter table DS_AR_DOKUMENTY_PUBLICZNE add abstract varchar(255) NULL;
alter table DS_AR_DOKUMENTY_PUBLICZNE add locations varchar(255) NULL;
alter table DS_AR_DOKUMENTY_PUBLICZNE add organizational_unit varchar(255) NULL;

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'DS_AR_SPRAWY_VIEW') 
drop view [DS_AR_SPRAWY_VIEW];
GO
--widok dla dostepnych spraw
CREATE VIEW [DS_AR_SPRAWY_VIEW]
AS
SELECT        DSO_CONTAINER.ID, DSO_CONTAINER.OFFICEID, DSO_CONTAINER.CTIME, DSO_CONTAINER.description, ISNULL(dso_rwa.digit1, '')
                         + ISNULL(dso_rwa.digit2, '') + ISNULL(dso_rwa.digit3, '') + ISNULL(dso_rwa.digit4, '') + ISNULL(dso_rwa.digit5, '') AS RWA,
                         DSO_CONTAINER.TITLE
FROM            DSO_CONTAINER INNER JOIN
                         dso_rwa ON DSO_CONTAINER.RWA_ID = dso_rwa.ID
WHERE        (DSO_CONTAINER.DISCRIMINATOR = 'CASE')
GO

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'DS_AR_TECZKI_VIEW') 
drop view DS_AR_TECZKI_VIEW;
GO
--widok dla dost�pnych teczek
CREATE VIEW [DS_AR_TECZKI_VIEW]
AS
SELECT        DSO_CONTAINER.ID, DSO_CONTAINER.OFFICEID, DSO_CONTAINER.CTIME, ISNULL(dso_rwa.digit1, '') + ISNULL(dso_rwa.digit2, '')
                         + ISNULL(dso_rwa.digit3, '') + ISNULL(dso_rwa.digit4, '') + ISNULL(dso_rwa.digit5, '') AS RWA, DSO_CONTAINER.NAME
FROM            DSO_CONTAINER INNER JOIN
                         dso_rwa ON DSO_CONTAINER.RWA_ID = dso_rwa.ID
WHERE        (DSO_CONTAINER.DISCRIMINATOR = 'FOLDER')
GO

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'DS_AR_DOKUMENTY_VIEW') 
DROP VIEW DS_AR_DOKUMENTY_VIEW;
GO
--widok dla dost�pnych dokument�w
CREATE VIEW [DS_AR_DOKUMENTY_VIEW]
AS
SELECT        ID, DESCRIPTION, CTIME, TITLE
FROM            DS_DOCUMENT
GO