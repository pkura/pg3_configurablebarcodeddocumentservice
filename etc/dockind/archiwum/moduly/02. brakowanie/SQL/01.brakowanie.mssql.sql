/* Gdyby ktoś potrzebował sprawdzić zawartość tabel jakie są tworzone w tym skrypcie
select * from  ds_ar_brakowanie
select * from  ds_ar_brakowanie_multiple
select * from  ds_ar_brakowanie_pozycja
*/

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_brakowanie') 
DROP TABLE [ds_ar_brakowanie];
GO

CREATE TABLE [dbo].[ds_ar_brakowanie](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[rok] [int] NULL,
	[ds_division_id] [numeric](19, 0) NULL,
	[ap_numer_decyzji] [nvarchar](50) NULL,
	[ap_data_decyzji] [date] NULL,
	[status_dokumentu] [nvarchar](50) NULL,
	[document_id] [numeric](19, 0) NULL,
	[uczestnik_1] [numeric](19, 0) NULL,
	[uczestnik_2] [numeric](19, 0) NULL,
	[uczestnik_3] [numeric](19, 0) NULL,
	[przewodniczacy] [numeric](19, 0) NULL,
	[uczestnik_1_data] [datetime] NULL,
	[uczestnik_2_data] [datetime] NULL,
	[uczestnik_3_data] [datetime] NULL,
	[przewodniczacy_data] [datetime] NULL,
	[uczestnik_1_status] [numeric](19, 0) NULL,
	[uczestnik_2_status] [numeric](19, 0) NULL,
	[uczestnik_3_status] [numeric](19, 0) NULL,
	[przewodniczacy_status] [nvarchar](50) NULL,
	[uczestnik_1_uwaga] [nvarchar](50) NULL,
	[uczestnik_2_uwaga] [nvarchar](50) NULL,
	[uczestnik_3_uwaga] [nvarchar](50) NULL,
	[przewodniczacy_uwaga] [nvarchar](50) NULL,
	[typ] [int] NULL,
    [MIESCEUTWORZENIA] [varchar](50) NULL,
    [DODATKOWE_METADANE] [numeric](18,0) NULL
) ON [PRIMARY]
GO

ALTER TABLE dbo.ds_ar_brakowanie ADD SKAN_DECYZJI NUMERIC(19,0) NULL;

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_brakowanie_multiple') 
DROP TABLE [ds_ar_brakowanie_multiple];
GO

CREATE TABLE [dbo].[ds_ar_brakowanie_multiple](
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[FIELD_CN] [varchar](50) NULL,
	[FIELD_VAL] [varchar](50) NOT NULL,
	[ID] [numeric](19,0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY];
GO

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_ar_brakowanie_pozycja') 
DROP TABLE [ds_ar_brakowanie_pozycja];
GO

CREATE TABLE [dbo].[ds_ar_brakowanie_pozycja](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[numer_wykazu] [nvarchar](50) NULL,
	[symbol_wykazu] [nvarchar](50) NULL,
	[folder_officeid] [nvarchar](50) NULL,
	[tytul_wykazu] [varchar](255) NULL,
	[uwagi] [varchar](255) NULL,
	[data_od] [datetime] NULL,
	[data_do] [datetime] NULL,
	[kategoria_arch] [varchar](50) NULL,
	[liczba_tomow] [int] NULL,
	[metry_biezace] [decimal](10, 4) NULL,
	[wybrakuj] [bit] NULL,
	[usun] [bit] NULL,
	[przedluz_o_lata] [int] NULL,
	[uzasadnienie_przedluzenia] [nvarchar](1000) NULL,
	[czy_wybrakowany] [smallint] NULL,
	[czy_usuniety] [smallint] NULL
) ON [PRIMARY];
GO