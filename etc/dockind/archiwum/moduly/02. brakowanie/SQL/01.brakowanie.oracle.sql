/*
BEGIN
	EXECUTE IMMEDIATE 'DROP TABLE DS_AR_BRAKOWANIE';
	EXCEPTION
	WHEN OTHERS THEN
	IF SQLCODE <> -942 THEN
	RAISE;
END
*/

DROP TABLE DS_AR_BRAKOWANIE;
CREATE TABLE DS_AR_BRAKOWANIE (
	document_id numeric NOT NULL,
	rok int NULL,
	ds_division_id numeric(19, 0) NULL,
	ap_numer_decyzji varchar2(50) NULL,
	ap_data_decyzji date NULL,
	status_dokumentu varchar2(50) NULL,
	uczestnik_1 numeric(19, 0) NULL,
	uczestnik_2 numeric(19, 0) NULL,
	uczestnik_3 numeric(19, 0) NULL,
	przewodniczacy numeric(19, 0) NULL,
	uczestnik_1_data date NULL,
	uczestnik_2_data date NULL,
	uczestnik_3_data date NULL,
	przewodniczacy_data date NULL,
	uczestnik_1_status numeric(19, 0) NULL,
	uczestnik_2_status numeric(19, 0) NULL,
	uczestnik_3_status numeric(19, 0) NULL,
	przewodniczacy_status varchar2(50) NULL,
	uczestnik_1_uwaga varchar2(50) NULL,
	uczestnik_2_uwaga varchar2(50) NULL,
	uczestnik_3_uwaga varchar2(50) NULL,
	przewodniczacy_uwaga varchar2(50) NULL
);
alter table DS_AR_BRAKOWANIE add(SKAN_DECYZJI number(19,0));

DROP SEQUENCE DS_AR_BRAKOWANIE_ID;
CREATE SEQUENCE DS_AR_BRAKOWANIE_ID start with 1 nocache;


drop table ds_ar_brakowanie_multiple;
CREATE TABLE ds_ar_brakowanie_multiple (
	DOCUMENT_ID numeric(19, 0) NULL,
	FIELD_CN varchar2(50) NULL,
	FIELD_VAL varchar2(50) NOT NULL
);

DROP TABLE ds_ar_brakowanie_pozycja;
CREATE TABLE ds_ar_brakowanie_pozycja(
	id numeric(19, 0) NOT NULL,
	numer_wykazu varchar2(50) NULL,
	symbol_wykazu varchar2(50) NULL,
	folder_officeid varchar2(50) NULL,
	tytul_wykazu varchar2(255) NULL,
	uwagi varchar2(255) NULL,
	data_od date NULL,
	data_do date NULL,
	kategoria_arch varchar2(50) NULL,
	liczba_tomow int NULL,
	metry_biezace decimal(10, 4) NULL,
	wybrakuj int NULL,
	usun int NULL,
	przedluz_o_lata int NULL,
	uzasadnienie_przedluzenia varchar2(1000) NULL,
	czy_wybrakowany smallint NULL,
	czy_usuniety smallint NULL
);

DROP SEQUENCE ds_ar_brakowanie_pozycja_id;
CREATE SEQUENCE ds_ar_brakowanie_pozycja_id start with 1 nocache;
