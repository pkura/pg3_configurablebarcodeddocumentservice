IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'ds_ar_brakowanie_pozycja_view') 
DROP VIEW ds_ar_brakowanie_pozycja_view;
GO

CREATE VIEW ds_ar_brakowanie_pozycja_view
AS
select
spis_zodb.number_itr, aii.ID AS id_Item, con.ID, con.OFFICEID AS CN, con.OFFICEID AS TITLE, con.CASE_YEAR, aii.DATE_FROM AS DATAOD, aii.DATE_TO AS DATADO, 
                         rwa.description AS RWATYTUL, rwa.code AS SYMBOL, rwa.achome AS katArch, aii.FOLDER_COUNT AS liczbaTomow, 
                         aii.FOLDER_LENGTH_METER AS metryBiezace, NULL AS refValue, 0 AS CENTRUM, 1 AS available, CAST(ISNULL(con.ARCHIVESTATUS, 0) AS varchar) 
                         + CAST(kejsy.max AS varchar) AS status
FROM            DS_AR_INVENTORY_ITEMS AS aii LEFT OUTER JOIN
                         DSO_CONTAINER AS con ON aii.FOLDER_OFFICEID = con.OFFICEID LEFT OUTER JOIN
                         v_dso_rwaa AS rwa ON con.RWA_ID = rwa.ID LEFT OUTER JOIN
                             (SELECT        PARENT_ID, ISNULL(MAX(ARCHIVESTATUS), 0) AS max
                               FROM            DSO_CONTAINER AS concase
                               WHERE        (DISCRIMINATOR = 'case') AND (ID IN
                                                             (SELECT DISTINCT outdok.CASE_ID
                                                               FROM            DS_DOCUMENT AS dok LEFT OUTER JOIN
                                                                                         DSO_OUT_DOCUMENT AS outdok ON dok.ID = outdok.ID
                                                               WHERE        (outdok.CASE_ID IS NOT NULL))) OR
                                                         (ID IN
                                                             (SELECT DISTINCT indok.CASE_ID
                                                               FROM            DS_DOCUMENT AS dok LEFT OUTER JOIN
                                                                                         DSO_IN_DOCUMENT AS indok ON dok.ID = indok.ID
                                                               WHERE        (indok.CASE_ID IS NOT NULL)))
                               GROUP BY PARENT_ID) AS kejsy ON kejsy.PARENT_ID = con.ID
                               join DS_AR_INVENTORY_ITEMS_MULTI as spis_zodb_m on aii.ID = spis_zodb_m.FIELD_VAL
								join ds_ar_inventory_transf_and_rec as spis_zodb on spis_zodb_m.DOCUMENT_ID = spis_zodb.DOCUMENT_ID
WHERE        (con.DISCRIMINATOR = 'FOLDER' and rwa.achome != 'A' and con.ARCHIVESTATUS = 2 and dbo.czyWybrakowac(con.ARCHIVEDDATE, rwa.achome) = 1);