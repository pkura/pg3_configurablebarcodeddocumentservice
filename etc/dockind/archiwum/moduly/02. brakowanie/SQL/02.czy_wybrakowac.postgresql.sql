CREATE OR REPLACE FUNCTION czyWybrakowac(in_date TIMESTAMP, archcat VARCHAR(9))
  RETURNS INTEGER AS $$
DECLARE
  rokarchiwizacji    INT;
  lataprzechowywania INT;
BEGIN
  rokarchiwizacji = YEAR(in_date);
  IF (LOWER(archcat) = 'b')
  THEN
    lataprzechowywania = 25;
  ELSIF REPLACE(lower(archcat), 'b', '') = 'c' OR REPLACE(lower(archcat), 'b', '') = 'e'
    THEN
      lataprzechowywania = 2;
  ELSE
    lataprzechowywania = CAST(REPLACE(REPLACE(lower(archcat), 'b', ''), 'e', '') AS INT);
  END IF;

  lataprzechowywania = ABS(lataprzechowywania);

  IF (YEAR(GETDATE()) >= rokarchiwizacji + 1 + lataprzechowywania)
  THEN RETURN 1;
  ELSE RETURN 0;
  END IF;
END;
$$ LANGUAGE plpgsql


