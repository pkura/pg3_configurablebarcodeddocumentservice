﻿CREATE TABLE ds_ar_brakowanie (
  id                    SERIAL PRIMARY KEY,
  rok                   INT            NULL,
  ds_division_id        NUMERIC(19, 0) NULL,
  ap_numer_decyzji      VARCHAR(50)    NULL,
  ap_data_decyzji       DATE           NULL,
  status_dokumentu      VARCHAR(50)    NULL,
  document_id           NUMERIC(19, 0) NULL,
  uczestnik_1           NUMERIC(19, 0) NULL,
  uczestnik_2           NUMERIC(19, 0) NULL,
  uczestnik_3           NUMERIC(19, 0) NULL,
  przewodniczacy        NUMERIC(19, 0) NULL,
  uczestnik_1_data      TIMESTAMP      NULL,
  uczestnik_2_data      TIMESTAMP      NULL,
  uczestnik_3_data      TIMESTAMP      NULL,
  przewodniczacy_data   TIMESTAMP      NULL,
  uczestnik_1_status    NUMERIC(19, 0) NULL,
  uczestnik_2_status    NUMERIC(19, 0) NULL,
  uczestnik_3_status    NUMERIC(19, 0) NULL,
  przewodniczacy_status VARCHAR(50)    NULL,
  uczestnik_1_uwaga     VARCHAR(50)    NULL,
  uczestnik_2_uwaga     VARCHAR(50)    NULL,
  uczestnik_3_uwaga     VARCHAR(50)    NULL,
  przewodniczacy_uwaga  VARCHAR(50)    NULL,
  typ                   INT            NULL,
  MIESCEUTWORZENIA      VARCHAR(50)    NULL,
  DODATKOWE_METADANE    NUMERIC(18, 0) NULL
);

ALTER TABLE ds_ar_brakowanie ADD SKAN_DECYZJI NUMERIC(19, 0) NULL;

CREATE TABLE ds_ar_brakowanie_multiple (
  DOCUMENT_ID NUMERIC(19, 0) NULL,
  FIELD_CN    VARCHAR(50)    NULL,
  FIELD_VAL   VARCHAR(50)    NOT NULL,
  ID          SERIAL PRIMARY KEY
);

CREATE TABLE ds_ar_brakowanie_pozycja (
  id                        SERIAL PRIMARY KEY,
  numer_wykazu              VARCHAR(50)    NULL,
  symbol_wykazu             VARCHAR(50)    NULL,
  folder_officeid           VARCHAR(50)    NULL,
  tytul_wykazu              VARCHAR(255)   NULL,
  uwagi                     VARCHAR(255)   NULL,
  data_od                   TIMESTAMP      NULL,
  data_do                   TIMESTAMP      NULL,
  kategoria_arch            VARCHAR(50)    NULL,
  liczba_tomow              INT            NULL,
  metry_biezace             DECIMAL(10, 4) NULL,
  wybrakuj                  BIT            NULL,
  usun                      BIT            NULL,
  przedluz_o_lata           INT            NULL,
  uzasadnienie_przedluzenia VARCHAR(1000)  NULL,
  czy_wybrakowany           SMALLINT       NULL,
  czy_usuniety              SMALLINT       NULL
)