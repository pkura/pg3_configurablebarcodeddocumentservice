/****** Object:  UserDefinedFunction [dbo].[czyWybrakowac]    Script Date: 11/08/2013 10:54:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[czyWybrakowac]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION czyWybrakowac
GO

CREATE FUNCTION czyWybrakowac (@DATE datetime, @archcat varchar(9))
RETURNS varchar(9)
WITH EXECUTE AS CALLER
AS
BEGIN
     DECLARE @wybrakuj int;
     DECLARE @rokarchiwizacji int;
     Declare @lataprzechowywania int;
     
     SET @rokarchiwizacji = YEAR(@DATE);
     if LOWER(@archcat) = 'b'
		set @lataprzechowywania = 25;
     else if REPLACE(lower(@archcat),'b','') = 'c' or REPLACE(lower(@archcat),'b','') = 'e'
		set @lataprzechowywania = 2;
	else 
		set @lataprzechowywania = cast(REPLACE(REPLACE(lower(@archcat),'b',''),'e','') as int);
	
	set @lataprzechowywania = ABS(@lataprzechowywania);
	
	if(YEAR(GETDATE()) >= @rokarchiwizacji+1+@lataprzechowywania)
		set @wybrakuj = 1;
	else set @wybrakuj = 0;
    RETURN(@wybrakuj);
END;

GO


