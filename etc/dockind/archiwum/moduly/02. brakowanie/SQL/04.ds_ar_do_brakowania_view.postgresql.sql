-- Wy�wietla pozycje ze spisu zdawczo odbiorczego ktore moga zostac wybrakowane
CREATE VIEW ds_ar_do_brakowania_view
AS
  SELECT
    invt.DOCUMENT_ID,
    invt.ds_division_id,
    invi.CATEGORY_ARCH,
    invt.NUMBER_ITR,
    invi.DATE_FROM,
    invi.DATE_TO,
    invi.FOLDER_COUNT,
    invi.FOLDER_LENGTH_METER,
    invi.FOLDER_TITLE,
    invi.FOLDER_OFFICEID,
    invi.FOLDERID AS FOLDER_ID,
    teczka.archiveddate
  FROM ds_ar_inventory_transf_and_rec invt
    LEFT JOIN DS_AR_INVENTORY_ITEMS_MULTI invim ON invt.DOCUMENT_ID = invim.DOCUMENT_ID
    LEFT JOIN DS_AR_INVENTORY_ITEMS invi ON invim.FIELD_VAL = invi.ID
    LEFT JOIN DSO_CONTAINER teczka ON invi.FOLDER_OFFICEID = teczka.OFFICEID
  WHERE teczka.ARCHIVESTATUS = 2 AND invi.CATEGORY_ARCH != 'A'