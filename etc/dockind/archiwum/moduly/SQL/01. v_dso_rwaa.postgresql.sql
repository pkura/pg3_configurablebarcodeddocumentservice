CREATE VIEW v_dso_rwaa
AS
SELECT     ID, coalesce(digit1, '') + coalesce(digit2, '') + coalesce(digit3, '') + coalesce(digit4, '') + coalesce(digit5, '') AS code, achome, acother, description, remarks
FROM         dso_rwa
GO