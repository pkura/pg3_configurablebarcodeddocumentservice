IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'v_dso_rwaa') 
DROP VIEW v_dso_rwaa;
GO

CREATE VIEW v_dso_rwaa
AS
SELECT     ID, ISNULL(digit1, '') + ISNULL(digit2, '') + ISNULL(digit3, '') + ISNULL(digit4, '') + ISNULL(digit5, '') AS code, achome, acother, description, remarks
FROM         dso_rwa
GO