CREATE TABLE DS_AR_WYCOFANIE (
  DOCUMENT_ID            NUMERIC(19, 0) NOT NULL,
  data_zlozenia          TIMESTAMP      NULL,
  DS_USER_id_wnioskujacy NUMERIC(19, 0) NOT NULL,
  DS_DIVISION_id         VARCHAR(100)   NULL,
  tytul_sprawy           VARCHAR(1000)  NULL,
  sygnatura_archiwalna   VARCHAR(256)   NULL,
  data_od                TIMESTAMP      NULL,
  data_do                TIMESTAMP      NULL,
  uzasadnienie_wycofania VARCHAR(1000)  NULL,
  powod_odrzucenia       VARCHAR(1000)  NULL,
  numer_protokolu        INT            NULL,
  data_wycofania         TIMESTAMP      NULL,
  sprawa_archiwista      VARCHAR(50)    NULL,
  departamentUzytko      INT            NULL,
  STATUSDOKUMENTU        INT,
  sprawa                 VARCHAR(50),
  sprawaId               VARCHAR(50)    NULL,
  typ                    INT            NULL,
  wycofana_id            INT            NULL,
  MIESCEUTWORZENIA       VARCHAR(50)    NULL,
  DODATKOWE_METADANE     NUMERIC(18, 0) NULL
);


CREATE TABLE DS_AR_WYCOFANIE_MULTI (
  DOCUMENT_ID NUMERIC(19, 0)            NOT NULL,
  FIELD_CN    VARCHAR(20)               NOT NULL,
  FIELD_VAL   VARCHAR(100)              NULL,
  ID          SERIAL PRIMARY KEY        NOT NULL
);

--widok spraw zaarchiwizowanych
CREATE VIEW v_ds_ar_wycofanie_sprawy
AS
  SELECT
    ID,
    TITLE AS Cn,
    TITLE AS title,
    NULL  AS refValue,
    0     AS CENTRUM,
    1     AS available
  FROM DSO_CONTAINER
  WHERE ARCHIVESTATUS = 2 AND DISCRIMINATOR = 'CASE';