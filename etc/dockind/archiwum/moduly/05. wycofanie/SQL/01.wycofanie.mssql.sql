/*
select * from DS_AR_WYCOFANIE
select * from DS_AR_WYCOFANIE_MULTI
*/
--ds_ar_wycofanie
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_WYCOFANIE') 
DROP TABLE [DS_AR_WYCOFANIE];
GO

 CREATE TABLE  DS_AR_WYCOFANIE (
  DOCUMENT_ID numeric(19,0) NOT NULL,
  data_zlozenia DATETIME NULL,
  DS_USER_id_wnioskujacy numeric(19,0) NOT NULL,
  DS_DIVISION_id VARCHAR(100) NULL,
  tytul_sprawy VARCHAR(1000) NULL,
  sygnatura_archiwalna VARCHAR(256) NULL,
  data_od DATETIME NULL,
  data_do DATETIME NULL,
  uzasadnienie_wycofania VARCHAR(1000) NULL,
  powod_odrzucenia VARCHAR(1000) NULL,
  numer_protokolu int NULL,
  data_wycofania DATETIME NULL,
  sprawa_archiwista VARCHAR(50) NULL,
  departamentUzytko int NULL,
  STATUSDOKUMENTU int,
  sprawa VARCHAR(50),
  sprawaId int NULL,
  typ int NULL,
  wycofana_id int NULL,
  MIESCEUTWORZENIA varchar(50) NULL,
  DODATKOWE_METADANE numeric(18,0) NULL)
  ON [PRIMARY]
GO

--ds_ar_wycofanie_multiple
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_AR_WYCOFANIE_MULTI') 
DROP TABLE [DS_AR_WYCOFANIE_MULTI];
GO

 CREATE TABLE DS_AR_WYCOFANIE_MULTI(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(20) NOT NULL,
	FIELD_VAL varchar(100) NULL,
	ID numeric(19, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
GO

--widok spraw zaarchiwizowanych
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'v_ds_ar_wycofanie_sprawy') 
DROP VIEW [v_ds_ar_wycofanie_sprawy];
GO

CREATE VIEW [v_ds_ar_wycofanie_sprawy]
AS
SELECT        ID, TITLE as Cn, TITLE as title,NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            DSO_CONTAINER
where ARCHIVESTATUS=2 and DISCRIMINATOR='CASE'

GO