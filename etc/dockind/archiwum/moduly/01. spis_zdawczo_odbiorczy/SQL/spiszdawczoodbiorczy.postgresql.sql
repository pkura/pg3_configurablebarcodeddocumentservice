create TABLE ds_ar_inventory_transf_and_rec(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	date_inventory_transfer date NULL,
	number_itr int NULL,
	is_category_a int NULL,
	ds_archive_team_id numeric(19, 0) NULL,
	ds_division_id numeric(19, 0) NULL,
	created_by numeric(19, 0) NULL,
	date_designation_inventory date NULL,
	date_proposed_transfer date NULL,
	date_check date NULL,
	date_transfer_physically date NULL,
	manager_guidelines varchar(255) NULL,
	STATUSDOKUMENTU int NULL,
	rejection_reason varchar(255) NULL,
	TYP int NULL,
    MIESCEUTWORZENIA varchar(50) NULL,
    DODATKOWE_METADANE numeric(18,0) NULL
);

alter table ds_ar_inventory_transf_and_rec add ARCHIVE_PLACE varchar(5) NULL;
alter table ds_ar_inventory_transf_and_rec add rejection_reason_arch varchar(255) NULL;
alter table ds_ar_inventory_transf_and_rec add rejection_reason_arch_last varchar(255) NULL;
CREATE TABLE DS_AR_INVENTORY_ITEMS(
	ID SERIAL PRIMARY KEY NOT NULL,
	FOLDER_ID int NULL,
	FOLDER_OFFICEID varchar(255) NULL,
	FOLDER_TITLE varchar(255) NULL,
	COMMENTS varchar(255) NULL,
	DATE_FROM date NULL,
	DATE_TO date NULL,
	CATEGORY_ARCH varchar(50) NULL,
	FOLDER_COUNT int NULL,
	FOLDER_LENGTH_METER decimal(10, 4) NULL,
	ARCH_PLACE varchar(255) NULL,
	DATE_SHRED date NULL,
	ROOM varchar(50) NULL,
	RACK varchar(50) NULL,
	SHELF varchar(50) NULL,
	WAREHOUSE varchar(50) NULL,
	STREET varchar(50) NULL,
	BUILDING varchar(50) NULL
);


 --pole uzupelniane tylko dal pozycji wygenerowanych automatycznie,
 -- pozycje reczne nie sa w sytemie wiec maja to pole puste
alter table ds_ar_inventory_items drop column FOLDER_ID;
alter table ds_ar_inventory_items add FOLDERID numeric(38,0);
 --pole uzupelniane dla pozycji recznych, dla elektronicznych jest w dso_container
alter table DS_AR_INVENTORY_ITEMS add archive_date date;
--archive_status
alter table DS_AR_INVENTORY_ITEMS add archive_status int NULL;

CREATE TABLE DS_AR_INVENTORY_ITEMS_MULTI(
	ID SERIAL PRIMARY KEY NOT NULL,
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(20) NOT NULL,
	FIELD_VAL varchar(100) NULL
);

CREATE TABLE ds_archives_team(
	id SERIAL PRIMARY KEY ,
	cn varchar(50) NOT NULL,
	title varchar(50) NOT NULL,
	centrum int NULL,
	refValue varchar(50) NULL,
	available int NULL
);