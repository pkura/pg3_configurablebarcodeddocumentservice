CREATE OR REPLACE TABLE ds_ar_inventory_transf_and_rec(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	date_inventory_transfer date NULL,
	number_itr int NULL,
	is_category_a int NULL,
	ds_archive_team_id numeric(19, 0) NULL,
	ds_division_id numeric(19, 0) NULL,
	created_by numeric(19, 0) NULL,
	date_designation_inventory date NULL,
	date_proposed_transfer date NULL,
	date_check date NULL,
	date_transfer_physically date NULL,
	manager_guidelines nchar(255) NULL,
	STATUSDOKUMENTU int NULL,
	rejection_reason nchar(255) NULL
);

 alter table ds_ar_inventory_transf_and_rec add ARCHIVE_PLACE varchar2(5) NULL
 alter table ds_ar_inventory_transf_and_rec add rejection_reason_arch varchar2(255) NULL
 alter table ds_ar_inventory_transf_and_rec add rejection_reason_arch_last varchar2(255) NULL

CREATE OR REPLACE TABLE DS_AR_INVENTORY_ITEMS(
	ID numeric(19, 0) NOT NULL,
	FOLDER_ID int NULL,
	FOLDER_OFFICEID varchar2(255) NULL,
	FOLDER_TITLE varchar2(255) NULL,
	COMMENTS varchar2(255) NULL,
	DATE_FROM date NULL,
	DATE_TO date NULL,
	CATEGORY_ARCH varchar2(50) NULL,
	FOLDER_COUNT int NULL,
	FOLDER_LENGTH_METER decimal(10, 4) NULL,
	ARCH_PLACE varchar2(255) NULL,
	DATE_SHRED date NULL,
	ROOM varchar2(50) NULL,
	RACK varchar2(50) NULL,
	SHELF varchar2(50) NULL,
	WAREHOUSE varchar2(50) NULL,
	STREET varchar2(50) NULL,
	BUILDING varchar2(50) NULL
);

drop sequence DS_AR_INVENTORY_ITEMS_ID;
create sequence DS_AR_INVENTORY_ITEMS_ID;

create trigger DS_AR_INV_ITEMS_trigger
before insert on DS_AR_INVENTORY_ITEMS
for each row
begin
select DS_AR_INVENTORY_ITEMS_ID.nextval
into :new.ID
from dual;
end;




CREATE OR REPLACE TABLE DS_AR_INVENTORY_ITEMS_MULTI(
	ID numeric(19, 0) NOT NULL,
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar2(20) NOT NULL,
	FIELD_VAL varchar2(100) NULL
);

drop sequence DS_AR_INV_ITEMS_MULTI_ID;
create sequence DS_AR_INV_ITEMS_MULTI_ID;

drop trigger DS_AR_INV_ITEMS_mult_trigger;
create trigger DS_AR_INV_ITEMS_mult_trigger
before insert on DS_AR_INVENTORY_ITEMS_MULTI
for each row
begin
select DS_AR_INV_ITEMS_MULTI_ID.nextval
into :new.ID
from dual;
end;

 --pole uzupelniane tylko dal pozycji wygenerowanych automatycznie,
 -- pozycje reczne nie sa w sytemie wiec maja to pole puste
alter table ds_ar_inventory_items drop column FOLDER_ID;
alter table ds_ar_inventory_items add FOLDERID number(38,0);
 --pole uzupelniane dla pozycji recznych, dla elektronicznych jest w dso_container
alter table DS_AR_INVENTORY_ITEMS add archive_date date;
--archive_status
alter table DS_AR_INVENTORY_ITEMS add archive_status int NULL;