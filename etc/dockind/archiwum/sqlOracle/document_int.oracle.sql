CREATE TABLE DS_UTP_DOC_INT(
	DOCUMENT_ID numeric(19, 0) PRIMARY KEY NOT NULL,
	rodzaj_int number NULL,
	send_kind number NULL,
	signature varchar(150) NULL,
	BARCODE varchar(50) NULL
) 

alter table DS_UTP_DOC_INT add typ varchar(250) null;
alter table DS_UTP_DOC_INT add format varchar(250) null;
alter table DS_UTP_DOC_INT add dostep number null;
alter table DS_UTP_DOC_INT add abstract varchar2(255) NULL;
alter table DS_UTP_DOC_INT add locations varchar2(255) NULL;
alter table DS_UTP_DOC_INT add organizational_unit varchar2(255) NULL;