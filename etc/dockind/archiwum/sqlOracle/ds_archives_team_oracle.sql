CREATE TABLE ds_archives_team(
	id NUMBER(19, 0) PRIMARY KEY NOT NULL,
	cn varchar2(50) NOT NULL,
	title varchar2(50) NOT NULL,
	centrum int NULL,
	refValue varchar2(50) NULL,
	available int NULL
);
COMMIT;

CREATE SEQUENCE ds_archives_team_id START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;