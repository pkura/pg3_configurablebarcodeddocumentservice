create table DS_ACCESS_TO_DOCUMENT (
    ID                      INTEGER NOT NULL PRIMARY KEY ,
    POSN                    SMALLINT NOT NULL,
    NAME                    VARCHAR(40) NOT NULL
);



create SEQUENCE DS_ACCESS_TO_DOCUMENT_ID;

create trigger trg_access_id
      before insert on DS_ACCESS_TO_DOCUMENT
      for each row
   begin
    select DS_ACCESS_TO_DOCUMENT_ID.nextval
        into :new.ID
        from dual;
    end;

insert into DS_ACCESS_TO_DOCUMENT (POSN, NAME) values  ( '1', 'Publiczny � dost�pny w ca�o�ci');
insert into DS_ACCESS_TO_DOCUMENT (POSN, NAME) values ( '2', 'Niepubliczny');