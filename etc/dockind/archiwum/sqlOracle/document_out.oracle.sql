CREATE TABLE DS_UTP_DOC_OUT(
	DOCUMENT_ID numeric(19, 0) PRIMARY KEY NOT NULL,
	rodzaj_out number NULL,
	send_kind number NULL,
	signature varchar(150) NULL,
	BARCODE varchar(50) NULL,
	zwrotka smallint NULL,
	datazwrotki date NULL,
	potwierdzenie_odbioru smallint NULL,
	GABARYT numeric(18, 0) NULL,
	sposob_obslugi numeric(18, 0) NULL
)

alter table DS_UTP_DOC_OUT add data_nadania date null;
alter table DS_UTP_DOC_OUT add dostep number null;
alter table DS_UTP_DOC_OUT add liczbaZalacznikow number null;
alter table DS_UTP_DOC_OUT add typ varchar(250) null;
alter table DS_UTP_DOC_OUT add format varchar(250) null;
alter table DS_UTP_DOC_OUT add abstract varchar2(255) NULL;
alter table DS_UTP_DOC_OUT add locations varchar2(255) NULL;
alter table DS_UTP_DOC_OUT add organizational_unit varchar2(255) NULL;





