CREATE OR REPLACE FORCE VIEW "V_DSO_RWAA" ("ID", "CODE", "ACHOME", "ACOTHER", "DESCRIPTION", "REMARKS") AS 
SELECT ID, CONCAT(CONCAT(CONCAT(CONCAT(NVL(digit1, ''), NVL(digit2, '')) , NVL(digit3, '')) , NVL(digit4, '')) , NVL(digit5, '')) AS code, achome, acother, description, remarks
FROM dso_rwa;
