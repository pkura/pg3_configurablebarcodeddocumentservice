CREATE TABLE DS_AR_WYCOFANIE (
  DOCUMENT_ID numeric(19,0) PRIMARY KEY NOT NULL,
  data_zlozenia timestamp NULL,
  DS_USER_id_wnioskujacy numeric(19,0) NOT NULL,
  DS_DIVISION_id VARCHAR2(50 CHAR) NULL,
  tytul_sprawy VARCHAR2(1000 CHAR) NULL,
  sygnatura_archiwalna VARCHAR2(256 CHAR) NULL,
  data_od timestamp NULL,
  data_do timestamp NULL,
  uzasadnienie_wycofania VARCHAR2(1000 CHAR) NULL,
  powod_odrzucenia VARCHAR2(1000 CHAR) NULL,
  numer_protokolu number NULL,
  data_wycofania timestamp NULL,
  sprawa_archiwista VARCHAR2(50 CHAR) NULL,
  departamentUzytko number NULL,
  STATUSDOKUMENTU number NULL,
  sprawaId number NULL,
  sprawa VARCHAR2(50 char) NULL);
  COMMIT;
  
 create TABLE DS_AR_WYCOFANIE_MULTI(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar2(20 char) NOT NULL,
	FIELD_VAL varchar2(100 char) NULL,
	ID numeric(19, 0) PRIMARY KEY NOT NULL
); 
COMMIT;
create SEQUENCE DS_AR_WYCO_MULTI_ID ;
COMMIT;
create trigger trg_wycofanie_multi_id
      before insert on DS_AR_WYCOFANIE_MULTI
      for each row
   begin
    select DS_AR_WYCO_MULTI_ID.nextval
        into :new.ID
        from dual;
    end;

CREATE VIEW v_ds_ar_wycofanie_sprawy
AS
SELECT        ID, TITLE as Cn, TITLE as title,NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            DSO_CONTAINER
where ARCHIVESTATUS=2 and DISCRIMINATOR='CASE'
COMMIT;