CREATE TABLE DS_TYPE(
	id number PRIMARY KEY NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum number NULL,
	refValue varchar(20) NULL,
	available number default 1 
)

create SEQUENCE DS_TYPE_ID;

create trigger trg_type_id
      before insert on DS_TYPE
      for each row
   begin
    select DS_TYPE_ID.nextval
        into :new.ID
        from dual;
    end;
 
insert into DS_TYPE (cn, title) values  ( '1', 'Nieuporz�dkowany zbi�r danych');
insert into DS_TYPE (cn, title) values ( '2', 'Uporz�dkowany zbi�r danych');
insert into DS_TYPE (cn, title) values ( '3', 'Obraz ruchomy');
insert into DS_TYPE (cn, title) values ( '4', 'Obiekt fizyczny');
insert into DS_TYPE (cn, title) values ( '5', 'Oprogramowanie');
insert into DS_TYPE (cn, title) values ( '6', 'D�wi�k');
insert into DS_TYPE (cn, title) values ( '7', 'Obraz nieruchomy');
insert into DS_TYPE (cn, title) values ( '8', 'Tekst');