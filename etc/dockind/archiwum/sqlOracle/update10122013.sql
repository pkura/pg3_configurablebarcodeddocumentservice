 	alter table DS_AR_WNIOSEK_UDOSTEP add WNIOSK_ZEW_OS_PRAW smallint
 	
--- z sqlManagera jesli nei pujdzie poprzez upgrade
DECLARE
  t_count INTEGER;
  v_sql VARCHAR2(1000) := '
          create table DSG_CURRENCY(
            id integer primary key not null,
            cn varchar(50) null,
            title varchar(255) null,
            centrum int null,
            refValue varchar(20) null,
            available int null
          )
  ';
BEGIN
  SELECT COUNT(*)
    INTO t_count
    FROM user_tables
   WHERE table_name = 'DSG_CURRENCY2';

  IF t_count = 0 THEN
    EXECUTE IMMEDIATE v_sql;
  END IF;
END;

UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');
UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');
UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');
UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');
UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');
UPDATE DSO_RWA SET DESCRIPTION = REPLACE(DESCRIPTION, '�', '�');

-- dodane pole building do ds_ar_inventory_items
alter TABLE DS_AR_INVENTORY_ITEMS add BUILDING varchar2(50) NULL;
-- wczesniej nie wrzucony na svn skrypt, ale mozliwe ze kolumna juz jest w bazie
alter table DS_AR_INVENTORY_ITEMS add archive_status int NULL;

--brakowanie
alter table DS_AR_BRAKOWANIE add(SKAN_DECYZJI number(19,0));
CREATE OR REPLACE VIEW "DS_AR_DO_BRAKOWANIA_VIEW" as 
select 
	invt.DOCUMENT_ID, invt.ds_division_id,
	invi.CATEGORY_ARCH, invt.NUMBER_ITR,
	invi.DATE_FROM, invi.DATE_TO,
	invi.FOLDER_COUNT, invi.FOLDER_LENGTH_METER,
	invi.FOLDER_TITLE, invi.FOLDER_OFFICEID,
	invi.FOLDERID as FOLDER_ID, teczka.archiveddate
from ds_ar_inventory_transf_and_rec invt
left join DS_AR_INVENTORY_ITEMS_MULTI invim on invt.DOCUMENT_ID = invim.DOCUMENT_ID
left join DS_AR_INVENTORY_ITEMS invi on invim.FIELD_VAL = invi.ID
left join DSO_CONTAINER teczka on invi.FOLDER_OFFICEID = teczka.OFFICEID
where teczka.ARCHIVESTATUS = 2 and  invi.CATEGORY_ARCH != 'A';