CREATE FULLTEXT CATALOG dsCatalog AS DEFAULT;

ALTER TABLE dsg_bfl_zarzadzenie ADD text_data clob;
alter table dsg_bfl_zarzadzenie alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_zarzadzenie ON dsg_bfl_zarzadzenie(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_zarzadzenie(text_data Language 1045)KEY INDEX IDX_id_zarzadzenie WITH STOPLIST = SYSTEM;

ALTER TABLE dsg_bfl_uchwala ADD text_data TEXT;
alter table dsg_bfl_uchwala alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_uchwala ON dsg_bfl_uchwala(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_uchwala(text_data Language 1045)KEY INDEX IDX_id_uchwala WITH STOPLIST = SYSTEM;

ALTER TABLE dsg_bfl_komunikat ADD text_data TEXT;
alter table dsg_bfl_komunikat alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_komunikat ON dsg_bfl_komunikat(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_komunikat(text_data Language 1045)KEY INDEX IDX_id_komunikat WITH STOPLIST = SYSTEM;


ALTER TABLE dsg_bfl_legislacyjny ADD text_data TEXT;
alter table dsg_bfl_legislacyjny alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_leg ON dsg_bfl_legislacyjny(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_legislacyjny(text_data Language 1045)KEY INDEX IDX_id_leg WITH STOPLIST = SYSTEM;

delete from dsg_bfl_protokol where document_id is null;
ALTER TABLE dsg_bfl_protokol ADD text_data TEXT;
alter table dsg_bfl_protokol alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_protokol ON dsg_bfl_protokol(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_protokol(text_data Language 1045)KEY INDEX IDX_id_protokol WITH STOPLIST = SYSTEM;


ALTER TABLE dsg_bfl_wal_zgrom ADD text_data TEXT;
alter table dsg_bfl_wal_zgrom alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_wal_zgr ON dsg_bfl_wal_zgrom(document_id);
CREATE FULLTEXT INDEX ON dsg_bfl_wal_zgrom(text_data Language 1045)KEY INDEX IDX_id_wal_zgr WITH STOPLIST = SYSTEM;


ALTER TABLE DSG_BFL_WN_ZARZ ADD text_data TEXT;
alter table DSG_BFL_WN_ZARZ alter column document_id numeric(18,0) not null;
CREATE UNIQUE INDEX IDX_id_wnz ON DSG_BFL_WN_ZARZ(document_id);
CREATE FULLTEXT INDEX ON DSG_BFL_WN_ZARZ(text_data Language 1045)KEY INDEX IDX_id_wnz WITH STOPLIST = SYSTEM;

alter table DSG_BFL_uchwala add dsg_bfl_zarzadzenie numeric(18,0);
alter table DSG_BFL_uchwala add opis varchar(300);
alter table DSG_BFL_zarzadzenie add opis varchar(300);
update DSG_BFL_UCHWALA set DSG_BFL_UCHWALA.dsg_bfl_zarzadzenie = (select MAX(z.document_id) from dsg_bfl_zarzadzenie z where z.DSG_BFL_UCHWALA= DSG_BFL_UCHWALA.document_id  );
