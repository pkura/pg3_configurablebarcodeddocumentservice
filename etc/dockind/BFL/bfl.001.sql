create table dsg_bfl_zarzadzenie
(
	document_id numeric(18,0),
	status integer,
	NUMER varchar(50),
	DNIA datetime,
	OBOWIAZUJE_DO datetime,
	DSG_BFL_UCHWALA numeric(18,0)
);

create table DSG_BFL_UCHWALA
(
	document_id numeric(18,0),
	numer varchar(50 ),
	dnia datetime,
	protokol_nr numeric(18,0)
);



create table dsg_bfl_legislacyjny
(
	document_id numeric(18,0),
	typ	integer,
	status		integer,
	opis	varchar(600 ),
	DSG_BFL_UCHWALA numeric(18,0)
	
);

create table dsg_bfl_protokol
(
	document_id numeric(18,0),
	spolka	integer,
	NUMER varchar(50),
	z_dnia datetime
);

create table dsg_bfl_wal_zgrom
(
	document_id numeric(18,0),
	spolka	integer,
	z_dnia datetime
);

create table DSG_BFL_WN_ZARZ
(
	document_id numeric(18,0),
	spolka	integer,
	NUMER varchar(50),
	z_dnia datetime
);

create table dsg_bfl_komunikat
(
	document_id numeric(18,0),
	status		integer,
	opis	varchar(600 ),
	Z_DNIA datetime,
	nr_komunik varchar(50 ),
	dsg_bfl_zarzadzenie numeric(18,0),
	DSG_BFL_UCHWALA numeric(18,0)
);

create table dsg_bfl_multiple_value
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	DOCUMENT_ID numeric(18,0) NOT NULL,
	FIELD_CN varchar(150),
	FIELD_VAL varchar (50)
);

ALTER TABLE dsg_bfl_legislacyjny ADD dsg_bfl_zarzadzenie numeric(18,0);

