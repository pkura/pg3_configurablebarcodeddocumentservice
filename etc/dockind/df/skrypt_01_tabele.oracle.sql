CREATE TABLE DSG_FINANSOWY(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	KLASA varchar(50)  NOT NULL,
	TYP varchar(50)  NOT NULL,
	DATA timestamp NULL,
	INVOICE_NUMBER varchar(50) null,
	STATUS integer NULL,
	OKRESMIESIAC integer NULL,
	OKRESROK integer NULL,
	NIP varchar(15)  NULL,
	numer_agenta varchar(15)  NULL,
	INST integer NULL,
	NUMER_RACHUNKU integer NULL
);


CREATE TABLE df_inst (
	id integer PRIMARY KEY NOT NULL ,
	imie varchar (255)  NULL ,
	nazwisko varchar (255)  NULL ,
	name varchar (255)  NULL ,
	oldname varchar (255)  NULL ,
	nip varchar (30)  NULL ,
	regon varchar (30)  NULL ,
	ulica varchar (255)  NULL ,
	kod varchar (10)  NULL ,
	miejscowosc varchar (255)  NULL ,
	email varchar (255)  NULL ,
	faks varchar (255)  NULL ,
	telefon varchar (255)  NULL ,
	kraj varchar (10)  NULL 
);

CREATE SEQUENCE df_inst_ID start with 1 nocache;

CREATE TABLE DSG_DF_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE INDEX idx_dsg_df_multiple_value ON DSG_DF_MULTIPLE_VALUE (DOCUMENT_ID,FIELD_CN);