CREATE TABLE [DSG_FINANSOWY](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[KLASA] [varchar](50) COLLATE Polish_CI_AS NOT NULL,
	[TYP] [varchar](50) COLLATE Polish_CI_AS NOT NULL,
	[DATA] [datetime] NULL,
	[INVOICE_NUMBER] [varchar](50) null,
	[STATUS] [int] NULL,
	[OKRESMIESIAC] [int] NULL,
	[OKRESROK] [int] NULL,
	[NIP] [varchar](15) COLLATE Polish_CI_AS NULL,
	[numer_agenta] [varchar](15) COLLATE Polish_CI_AS NULL,
	[INST] [int] NULL,
	[NUMER_RACHUNKU] [int] NULL
);


CREATE TABLE [df_inst] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[name] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[oldname] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nip] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[regon] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

CREATE TABLE [DSG_DF_MULTIPLE_VALUE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);

CREATE INDEX idx_dsg_df_multiple_value ON DSG_DF_MULTIPLE_VALUE (DOCUMENT_ID,FIELD_CN);