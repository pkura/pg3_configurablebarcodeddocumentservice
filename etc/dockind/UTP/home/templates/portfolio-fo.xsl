<?xml version="1.0" encoding="iso-8859-2" ?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:output omit-xml-declaration="yes" />

<xsl:template match="/">	
    <fo:root >
        <fo:layout-master-set>
            <fo:simple-page-master  master-name="simple"
                                    page-height="29.7cm"
                                    page-width="21cm"
                                    margin-top="1cm"
                                    margin-bottom="1cm"
                                    margin-left="1cm"
                                    margin-right="1cm">
                <fo:region-body margin-top="1cm" margin-bottom="0cm"/>
                <fo:region-before extent="1cm"/>
            </fo:simple-page-master>

        </fo:layout-master-set>


        <fo:page-sequence master-reference="simple">


            <fo:flow flow-name="xsl-region-body">

                <fo:table table-layout="fixed" border="1pt solid black">

                    <fo:table-column column-width="1cm"/>
                    <fo:table-column column-width="3cm"/>
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="2cm"/>
                    <fo:table-column column-width="10.5cm" />

                    <fo:table-body>
						<fo:table-row>
							<fo:table-cell border-style="solid" ><fo:block font-family="Arial" font-size="10pt" text-align="center"><xsl:value-of select="/teczka/rok" /></fo:block></fo:table-cell>
							<fo:table-cell border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center"><xsl:value-of select="/teczka/referent" /></fo:block></fo:table-cell>
							<fo:table-cell border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center"><xsl:value-of select="/teczka/symbol" /></fo:block></fo:table-cell>
							<fo:table-cell border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center"><xsl:value-of select="/teczka/rwa" /></fo:block></fo:table-cell>
							<fo:table-cell border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center"><xsl:value-of select="/teczka/tytul" /></fo:block></fo:table-cell>
						</fo:table-row>
                    </fo:table-body>

                </fo:table>

                <fo:block></fo:block>


                <fo:table table-layout="fixed" border="1pt solid black" >
                    <fo:table-column column-width="1cm" />
                    <fo:table-column column-width="4.5cm" />
                    <fo:table-column column-width="3cm" />
                    <fo:table-column column-width="2cm" />
                    <fo:table-column column-width="2.5cm" />
                    <fo:table-column column-width="2.5cm" />
                    <fo:table-column column-width="3cm" />
                    <fo:table-body>
                        <fo:table-row>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt" text-align="center">Lp.</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt" text-align="center">SPRAWA<fo:block></fo:block>(kr�tka tre��)</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt" text-align="center">OD KOGO<fo:block></fo:block>WPYNʣA</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt" text-align="center">DATA</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt" text-align="center">UWAGI</fo:block></fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center">znak pisma</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center">z dnia</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center">wszcz�cie sprawy</fo:block></fo:table-cell>
                            <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" text-align="center">ostateczne za�atwienie</fo:block></fo:table-cell>
                        </fo:table-row>

                        <xsl:apply-templates select="//sprawa" />

                    </fo:table-body>
                </fo:table>
            </fo:flow>
        </fo:page-sequence>
    </fo:root>
</xsl:template>

<xsl:template match="sprawa">
	<xsl:variable name="od_kogo" select="od_kogo"/>
    <fo:table-row>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="lp" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="znak_sprawy" /></fo:block></fo:table-cell>
        <xsl:choose>
        	<xsl:when test="$od_kogo='1'"><fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="nadawca_przychodzace" /></fo:block></fo:table-cell></xsl:when>
        	<xsl:when test="$od_kogo='0'"><fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt">sprawa w�asna</fo:block></fo:table-cell></xsl:when>
        	<xsl:otherwise><fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="1" number-columns-spanned="2" ><fo:block font-family="Arial" font-size="10pt"></fo:block></fo:table-cell></xsl:otherwise>
        </xsl:choose>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" number-columns-spanned="1" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="data_wszczecia_sprawy" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" number-columns-spanned="1" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="data_zakonczenia_sprawy" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid" number-rows-spanned="2" ><fo:block font-family="Arial" font-size="10pt"><xsl:value-of select="przeniesienia_sprawy_do_innych_teczek" /></fo:block></fo:table-cell>
    </fo:table-row>
    <fo:table-row>
        <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="znak_pisma_przychodzacego" /></fo:block></fo:table-cell>
        <fo:table-cell padding="2pt" border-style="solid"><fo:block font-family="Arial" font-size="10pt" ><xsl:value-of select="z_dnia_przychodzacego" /></fo:block></fo:table-cell>
    </fo:table-row>

</xsl:template>

</xsl:stylesheet>
