--sposob dostarczenia dla dokumentow przychodzacych
delete FROM dso_in_document_delivery 
	insert into dso_in_document_delivery values ('1','Zwyk�y');
	insert into dso_in_document_delivery values ('2','Rejestrowany');
	insert into dso_in_document_delivery values ('3','Rejestrowany+ZPO');
	insert into dso_in_document_delivery values ('4','Kurier');
	insert into dso_in_document_delivery values ('5','Fax');
	insert into dso_in_document_delivery values ('6','E-mail');
	insert into dso_in_document_delivery values ('7','esp-ePUAP');
	insert into dso_in_document_delivery values ('8', 'Osobi�cie');
	insert into dso_in_document_delivery values ('9', 'No�nik optyczny/pen-drive');

--sposob dostarczenia dla dokumentow wychodzacych	
 delete FROM dso_out_document_delivery 
	insert into dso_out_document_delivery values ('1','Zwyk�y');
	insert into dso_out_document_delivery values ('2','Rejestrowany');
	insert into dso_out_document_delivery values ('3','Rejestrowany + ZPO');
	insert into dso_out_document_delivery values ('4','Ekspresowy (kurier)');
	insert into dso_out_document_delivery values ('5','E-mail');
	insert into dso_out_document_delivery values ('6','Fax');
	insert into dso_out_document_delivery values ('7','Osobi�cie');
	insert into dso_out_document_delivery values ('8','ePUAP');	

	drop table DS_OUT_DELIVERY_GABARYT;
	
	-- rodzaje Gabaryt�w 
	CREATE TABLE [dbo].[DS_OUT_DELIVERY_GABARYT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1 
	
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_GABARYT (cn, title) values  ( '1', 'KRAJ - Gabaryt A');
insert into DS_OUT_DELIVERY_GABARYT (cn, title) values ( '2', 'KRAJ - Gabaryt B');
insert into DS_OUT_DELIVERY_GABARYT (cn, title) values ( '3', '�WIAT - Gabaryt A');
insert into DS_OUT_DELIVERY_GABARYT (cn, title) values ( '4', '�WIAT - Gabaryt B');
insert into DS_OUT_DELIVERY_GABARYT (cn, title) values ( '5', '�WIAT - Gabaryt C');
insert into DS_OUT_DELIVERY_GABARYT (cn, title) values ( '6', '�WIAT - Gabaryt D');

drop table DS_OUT_DELIVERY_SERVICE

	--sposoby obs�ugi
	CREATE TABLE [dbo].[DS_OUT_DELIVERY_SERVICE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1 
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('1', 'Ostro�nie szk�o');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('2', 'Chroni� przed upadkiem');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('3', 'g�ra/d�');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('4', 'Chroni� przed wilgoci�');

--typ
CREATE TABLE [dbo].[DS_UTP_TYPE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1 
	
) ON [PRIMARY]

insert into DS_UTP_TYPE (cn, title) values  ( '1', 'Nieuporz�dkowany zbi�r danych');
insert into DS_UTP_TYPE (cn, title) values ( '2', 'Uporz�dkowany zbi�r danych');
insert into DS_UTP_TYPE (cn, title) values ( '3', 'Obraz ruchomy');
insert into DS_UTP_TYPE (cn, title) values ( '4', 'Obiekt fizyczny');
insert into DS_UTP_TYPE (cn, title) values ( '5', 'Oprogramowanie');
insert into DS_UTP_TYPE (cn, title) values ( '6', 'D�wi�k');
insert into DS_UTP_TYPE (cn, title) values ( '7', 'Obraz nieruchomy');
insert into DS_UTP_TYPE (cn, title) values ( '8', 'Tekst');
