--trigger do wiazania dokumentow przychodzacych
Create TRIGGER [t_multiLink] 
    ON [ds_utp_dokuments_multi]
    for INSERT
AS
BEGIN
	
	DECLARE @id as Int
	DECLARE @CN as varchar(100)
	DECLARE @VAL as Int
	SET @id = (SELECT DOCUMENT_ID FROM inserted)
	SET @CN =(SELECT FIELD_CN from inserted)
	SET @val = (SELECT FIELD_VAL FROM inserted)
	if EXISTS((SELECT * from ds_utp_dokuments_multi where field_val=@val and field_cn=@CN and document_id=@id) )
	begin 
		if(@CN='LINKS')
		begin
			INSERT INTO ds_utp_dokuments_multi (DOCUMENT_ID,FIELD_CN,FIELD_VAL) values (@val,@CN,@id) 
		end
	end
END
