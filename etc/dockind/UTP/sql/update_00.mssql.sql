--tabela dla zapisu filtru prostego
 
CREATE TABLE [dbo].[utp_search_filter](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[document_id] [int] NULL,
	[title] [varchar](50) NULL,
	[description] [varchar](50) NULL,
	[attribute] [varchar](50) NULL,
	[author] [varchar](50) NULL,
	[attachmentBarcode] [varchar](50) NULL,
	[startCtime] [date] NULL,
	[endCtime] [date] NULL,
	[startMtime] [date] NULL,
	[endMtime] [date] NULL,
	[lastRemark] [varchar](50) NULL,
	[accessedBy] [varchar](50) NULL,
	[filterName] [varchar](50) NULL
) ON [PRIMARY]

GO
alter table utp_search_filter add documentKindIds varchar(50);
--zwiekszenie pola cel dekretacji
alter table dso_document_asgn_history alter column objective VARCHAR(512)
--zwiekszenie pola numer r
alter table dbo.DSO_IN_DOCUMENT ALTER COLUMN POSTALREGNUMBER varchar(60) null;
alter table dbo.DSO_OUT_DOCUMENT ALTER COLUMN POSTALREGNUMBER varchar(60) null;