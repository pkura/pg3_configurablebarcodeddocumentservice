CREATE TABLE [dbo].[ds_utp_dokument_in_dictionary_lista](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](255) NULL,
 CONSTRAINT [PK_ds_utp_dokument_in_dictionary_lista] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)
) ON [PRIMARY]

GO
