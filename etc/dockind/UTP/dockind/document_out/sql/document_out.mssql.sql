-- Tabela dockindu
CREATE TABLE [DS_UTP_DOC_OUT](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_out] [int] NULL,
	[send_kind] [int] NULL,
	[signature] [varchar](150) NULL,
	[BARCODE] [varchar](50) NULL,
	[zwrotka] [bit] NULL,
	[datazwrotki] [date] NULL,
	[potwierdzenie_odbioru] [bit] NULL,
	[GABARYT] [numeric](18, 0) NULL,
	[sposob_obslugi] [numeric](18, 0) NULL
) ON [PRIMARY]

alter table DS_UTP_DOC_OUT add data_nadania date null;
alter table DS_UTP_DOC_OUT add dostep int null;
alter table DS_UTP_DOC_OUT add liczbaZalacznikow int null;
alter table DS_UTP_DOC_OUT add typ varchar(250) null;
alter table DS_UTP_DOC_OUT add format varchar(250) null;
ALTER TABLE DS_UTP_DOC_OUT ADD typ_slownika int NULL;
ALTER TABLE DS_UTP_DOC_OUT ADD student varchar(250) NULL;
ALTER TABLE DS_UTP_DOC_OUT ADD pracownik varchar(250) NULL;
ALTER TABLE DS_UTP_DOC_OUT ADD kontrahent varchar(250) NULL;
ALTER TABLE DS_UTP_DOC_OUT ADD kandydat varchar(250) NULL;
alter table DS_UTP_DOC_OUT add NIE_PELNE_CYFROWE smallint;
alter table DS_UTP_DOC_OUT add WERYFIKACJA_WG_JRWA smallint;
alter table DS_UTP_DOC_OUT add numerKolejny int null;
ALTER TABLE DS_UTP_DOC_OUT ADD EPUAP [numeric](18, 0) NULL;
