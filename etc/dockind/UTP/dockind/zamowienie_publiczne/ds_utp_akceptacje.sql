USE [utp]
GO

/****** Object:  Table [dbo].[DS_UTP_AKCEPTACJE]    Script Date: 2014-03-13 14:47:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DS_UTP_AKCEPTACJE](
	[ID] [numeric](18, 0)identity(1,1) NOT NULL,
	[OSOBA] [numeric](18, 0) NULL,
	[DATA] [date] NULL,
	[STATUS] [numeric](18, 0) NULL,
	[UWAGI] [varchar](250) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

