create view [dsg_invoice_general_budzet_with_ref] AS
  SELECT        *
FROM            (SELECT        CONVERT(numeric(19, 0), bd_budzet_id) AS id, id AS cn, (budzet_nazwa + ': ' + dok_kom_nazwa) AS title, ROW_NUMBER() OVER (PARTITION BY 
                                                    bd_budzet_id
                          ORDER BY id) AS centrum, convert(numeric(19,0),budzet_id) AS refValue, available
FROM            dsg_budzety
WHERE        bd_rodzaj = 'BK') a
WHERE        a.centrum = 1;


  create view [dsg_plany_zakupow_view] as 
SELECT        erpId AS id, idm AS cn, nazwa AS title, budzet_id AS refvalue, NULL AS centrum, available
FROM            dbo.dsg_plany_zakupow
WHERE        (czyAgregat = 0)





 	create table dsg_budget_to_division (
	id numeric(19,0) identity(1,1),
	[budzet_id] [numeric](10, 0) NOT NULL,
	[budzet_idm] [varchar](20) NOT NULL,
	[wersja] [int] NOT NULL,
	[nazwa] [varchar](128) NULL,
	[data_obow_od] [datetime] NULL,
	[data_obow_do] [datetime] NULL,
	[czy_archiwalny] [numeric](1, 0) NOT NULL,
	komorka_idn varchar(50) not null,
	komorka_id [numeric](19, 0) NOT NULL,
	division_id [numeric](19, 0) NOT NULL,
	division_guid varchar(50) NOT NULL
	);

insert into dsg_budget_to_division
select 
b.budzet_id as [budzet_id], b.idm as [budzet_idm], b.[wersja], b.[nazwa], null,null,0,div.CODE as komorka_idn, cast(cast(d.erp_id as float) as bigint) as komorka_id, d.id as division_id, div.guid as division_guid
from dsg_budzetyKO_komorek b
join ds_division_erp d on b.erpId=d.erp_id 
join DS_DIVISION div on div.ID=d.ds_division_id 




--create view dsg_budzetyKO_do_komorek as(
--select btd.budzet_id as id, btd.budzet_idm as cn,Concat(div.NAME,' : ',b.nazwa)  as title,b.budzet_id as refValue, null as centrum, 1 as available
--from dsg_budget_to_division btd
--left join dsg_budzetyKO_komorek b on b.id=btd.budzet_id
--left join DS_DIVISION div on div.ID=btd.division_id
--) 

Create view [dbo].[DS_UTP_ZAMPUB_KIERJED_VIEW]
AS
select users.id,users.name as cn,users.firstname +' '+ users.lastname as title,null as refvalue,null as centrum,1 as available from DS_USER users
left join DS_USER_TO_DIVISION utd on utd.USER_ID=users.id
left join DS_DIVISION div on div.ID=utd.DIVISION_ID
where div.GUID='D479D58F72A7A1DE1451FC0BC6E0314EFAB' and div.DIVISIONTYPE='group'




