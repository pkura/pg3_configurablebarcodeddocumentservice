CREATE VIEW [typy_rezerwacji]
AS
SELECT        erpId AS id, idm AS cn, nazwa AS title, czy_obsluga_pzp AS refValue, czy_aktywny AS available, NULL AS centrum, czy_obsluga_pzp
FROM            dbo.dsg_typy_wnioskow_o_rez
GO