USE [utp]
GO

/****** Object:  View [dbo].[DS_UTP_ZAMPUB_KIERJED_VIEW]    Script Date: 2014-04-02 08:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[DS_UTP_ZAMPUB_KIERJED_VIEW]
AS
select users.id,users.name as cn,users.firstname +' '+ users.lastname as title,null as refvalue,null as centrum,1 as available from DS_USER users
left join DS_USER_TO_DIVISION utd on utd.USER_ID=users.id
left join DS_DIVISION div on div.ID=utd.DIVISION_ID
where div.GUID='AC0F949B001931DB1452123A556CDF5A943' and div.DIVISIONTYPE='group'


GO

