USE [utp]
GO

/****** Object:  View [dbo].[dsg_plany_zakupow_view]    Script Date: 2014-04-03 09:07:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[dsg_plany_zakupow_view]
AS
SELECT     pz.erpId AS id, pz.idm AS cn, pz.nazwa AS title, bKOk.erpId AS refvalue, NULL AS centrum, pz.available
FROM         dbo.dsg_plany_zakupow AS pz LEFT OUTER JOIN
                      dbo.dsg_budzetyKO_komorek AS bKOk ON bKOk.budzet_id = pz.budzet_id
GO

