USE [utp]
GO

/****** Object:  Table [dbo].[DS_UTP_WYL_USTAWOWE]    Script Date: 03/21/2014 16:20:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DS_UTP_WYL_USTAWOWE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](50) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

ALTER TABLE [DS_UTP_WYL_USTAWOWE] ALTER COLUMN [title] [varchar](1024) NULL

INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust1','art.. 4 ust.1 lit. a) Szczeg�lna procedura organizacji mi�dzynarodowej odmienna od okre�lonej w ustawie PZP',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust2','art.. 4 ust.2 Zam�wienia dla Narodowego Banku Polskiego',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3a','art.. 4 ust. 3 lit. a) Us�ugi arbitra�owe lub pojednawcze',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3b','art.. 4 ust. 3 lit. b) Us�ugi Narodowego Banku Polskiego',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3e','art.. 4 ust. 3 lit. e) Us�ugi w zakresie bada� naukowych i prac rozwojowych oraz �wiadczenie us�ug badawczych, kt�re nie s� w ca�o�ci op�acane przez zamawiaj�cego lub kt�rych rezultaty nie stanowi� wy��cznie jego w�asno�ci',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3g','art.. 4 ust. 3 lit. g) Nabycie, przygotowanie, produkcja lub koprodukcja materia��w programowych przeznazonych do emicji w radiu, telewizji lub internecie',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3h','art.. 4 ust. 3 lit. h) Zakup czasu antenowego',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3i','art.. 4 ust. 3 lit. i) Nabycie w�asno�ci nieruchomo�ci oraz innych praw do nieruchomo�ci w szczeg�lno�ci dzier�awy i najmu',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust3l','art.. 4 ust. 3 lit. l) Us�ugi BGK w zakresie bankowej obs�ugi jednostek',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust4','art.. 4 ust. 4 Umowy z zakresu prawa pracy',0,NULL,1)
INSERT INTO [dbo].[DS_UTP_WYL_USTAWOWE]([cn],[title],[centrum],[refValue],[available]) VALUES('art4ust10','art. 4 ust. 10 Zam�wienia, kt�rych g��wnym celem jest pozwolenie zamawiaj�cym na oddanie do dyspozycji publicznej sieci telekomunikacyjnej lub eksploatacja publicznej sieci telekomunikacyjnej lub �wiadczenie publicznie dost�pnych us�ug. Uwagi dodatkowe: W przypadku kiedy pole inne wy��czenia ustawowe jest wype�nione a warto�� wniosku nie przekracza 1000 Euro to pole formularza s� takie same jak w notatce. W przypadku kiedy wniosek przekracza warto�� 1000 Euro, pola wyst�puj�ce na formularzu s� takie same jak dla Zaproszenia. W przypadku odrzucenia zawsze nale�y poda� pow�d odrzucenia telekomunikacyjnych za pomoc� publicznej sieci telekomunikacyjnej. Nabywanie dostaw, us�ug lub rob�t budowlanych od centralnego zamawiaj�cego',0,NULL,1)
GO


GO

SET ANSI_PADDING OFF
GO

