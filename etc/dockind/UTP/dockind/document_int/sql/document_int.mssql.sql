-- Tabela dockindu
CREATE TABLE [DS_UTP_DOC_INT](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_int] [int] NULL,
	[send_kind] [int] NULL,
	[signature] [varchar](150) NULL,
	[BARCODE] [varchar](50) NULL
) ON [PRIMARY]

alter table DS_UTP_DOC_INT add typ varchar(250) null;
alter table DS_UTP_DOC_INT add format varchar(250) null;
alter table DS_UTP_DOC_INT add dostep int null;
alter table DS_UTP_DOC_INT add NIE_PELNE_CYFROWE smallint;
alter table DS_UTP_DOC_INT add WERYFIKACJA_WG_JRWA smallint;
alter table DS_UTP_DOC_INT add numerKolejny int null;