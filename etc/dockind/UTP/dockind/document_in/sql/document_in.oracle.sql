CREATE TABLE DS_UTP_DOC_IN(
	DOCUMENT_ID numeric(19, 0) Primary key NOT NULL,
	rodzaj_in int NULL,
	send_kind int NULL,
	signature varchar(150) NULL,
	storage_place varchar(250) null,
	BARCODE varchar(50) NULL,
	dokument_bez_daty smallint NULL,
	data date NULL,
	opis_zalacznikow varchar(250) NULL,
	oryginal_kopia smallint NULL,
	student varchar(250) NULL,
	pracownik varchar(250) NULL,
	kontrahent varchar(250) NULL,
	kandydat varchar(250) NULL,
	typ varchar(250) NULL,
	country varchar(250) NULL,
	format varchar(250) NULL,
	korespondencja_prywatna smallint NULL,
	data_nadania date NULL,
	typ_slownika int NULL
) 

alter table DS_UTP_DOC_IN add pozostali varchar(250) null;
alter table DS_UTP_DOC_IN add liczbaZalacznikow int null;
alter table DS_UTP_DOC_IN add osobaFizyczna smallint null;
alter table DS_UTP_DOC_IN add anonim smallint null;
alter table DS_UTP_DOC_IN add numerKolejny int null;
alter table DS_UTP_DOC_IN add NIE_PELNE_CYFROWE smallint;
alter table DS_UTP_DOC_IN add WERYFIKACJA_WG_JRWA smallint;
alter table DS_UTP_DOC_IN add znak_przesylki varchar(250) null;

