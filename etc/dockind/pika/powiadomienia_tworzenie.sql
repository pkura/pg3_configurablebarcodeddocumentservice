/**
 * Tworzy powiadomienia z istniejących dokumentów
 */
DECLARE @documentId numeric(19,0);
DECLARE @i int;
DECLARE @dateTime DATETIME;
DECLARE db_cursor CURSOR FOR SELECT top(2) id FROM ds_document order by ID desc;

OPEN db_cursor 
SET @i = 1;
SET @dateTime =  SYSDATETIME();
fetch next from db_cursor into @documentId;

while @@FETCH_STATUS = 0
BEGIN
	SET @i = @i +1;
	
	if(@i % 50 = 0)
	BEGIN
		SET @dateTime = DATEADD(mi,5, @dateTime);
		
	END
	INSERT INTO [pika].[dbo].[DSE_EVENT]
     VALUES
           (0,SYSDATETIME(),'immediateTrigger','pikaMailHandler',null,@dateTime,CONVERT(varchar(20), @documentId) + '|1|'wstawic adres email'|true',null);
	INSERT INTO [pika].[dbo].[DSE_EVENT]
     VALUES
           (0,SYSDATETIME(),'immediateTrigger','pikaMailHandler',null,@dateTime,CONVERT(varchar(20), @documentId) + '|1|'wstawic adres email'|false',null);
	fetch next from db_cursor into @documentId;
END

close db_cursor
DEALLOCATE db_cursor