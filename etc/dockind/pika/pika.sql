insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_DELETE','Dokumenty zwyk造 - usuwanie','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_ATT_MODIFY','Dokumenty zwyk造 zalacznik - modyfikacja','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ','Dokumenty zwyk造- odczyt','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_MODIFY','Dokumenty zwyk造 - modyfikacja','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_ATT_READ','Dokumenty zwyk造 zalacznik - odczyt','brak','group',         1,1);

CREATE TABLE DSG_PIKA
(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP int NULL,
	DATA datetime null,
	KOD_PIKA varchar(40) null,
	KOD_INNE varchar(1000) null,
	ODBIORCA int,
	NADAWCA int,
	ADRESAT int,
	SEGMENT_KLIENTA int,
	TYP_PRZESYLKI int,
	MIEJSCE_DOSTAW_2 int,
	TYP_NADAWCY int,
	RODZAJ int,
	KATEGORIA int,
	POD_KATEGORIA int
)


CREATE TABLE [dbo].[DSG_PIKA_NADAWCA](
	[ID] [int] IDENTITY(1,1) NOT NULL primary key,
	[KLIENT] [varchar](100) NULL,
	[NIP] [varchar](30) NOT NULL,
	[FIRMA] [varchar](200) NULL,
	[POCZTA] [varchar](20) NULL,
	[MIEJSCOWOSC] [varchar](50) NULL,
	[ULICA] [varchar](50) NULL,
	[NRDOMU] [varchar](20) NULL,
	[NRLOKALU] [varchar](20) NULL,
	[DORADCA] [varchar](100) NULL,
	[DORADCAEMAIL] [varchar](100) NULL,
	[DORADCAADRES] [varchar](100) NULL,
	[WYDZIAL] [varchar](100) NULL,
	[WSPARCIE] [varchar](100) NULL,
	[WSPARCIEMAIL] [varchar](100) NULL,
	[WSPARCIEADRES] [varchar](100) NULL,
	[KIEROWNIK] [varchar](100) NULL,
	[SEGMENT] [varchar](100) NULL
) ON [PRIMARY]


CREATE TABLE DSG_PIKA_ADRESAT
(
	ID	 		[int] IDENTITY(1,1) NOT NULL primary key,
	IMIE 		varchar(30) NULL,
	NAZWISKO    varchar(100),
	ULICA VARCHAR(50),
	KOD_POCZTOWY VARCHAR(20),
	POCZTA VARCHAR(50),
	MIEJSCOWOSC VARCHAR(50),
	EMAIL VARCHAR(50),
	PIKAODBIO int
);

CREATE TABLE DSG_PIKA_KATEGORIE(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);


INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (1,'umowa',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (2,'aneks',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (3,'porozumienie',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (4,'wypowiedzenie',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (5,'reklamacja',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (6,'skarga',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (7,'nota',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(8,'oferty',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(9,'wnioski oferty masowej',0,0,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(10,'faktury i noty',0,1,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(11,'umowy',0,1,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(12,'dokumenty s康owe',0,1,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(13,'pisma korporacyjne',0,1,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES(14,'pracownicze',0,1,1);

CREATE TABLE DSG_PIKA_POD_KATEGORIE(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

INSERT INTO DSG_PIKA_POD_KATEGORIE
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'skarga na pracownika',0,6,1),
           (2,'skarga na firm�',0,6,1);

/*        
CREATE TABLE DSG_PIKA_SEGMENT_KLIENTA(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

INSERT INTO DSG_PIKA_SEGMENT_KLIENTA
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'KS',0,null,1),
           (2,'KB',0,null,1),
           (3,'SME',0,null,1),
           (4,'Po鈔ednia',0,null,1),
           (5,'BIP',0,null,1),
           (6,'Klienci RM EOiS',0,null,1);

*/
           
CREATE TABLE DSG_PIKA_MIEJSCE_DOSTAWY(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'EOB - kurier PIKA',0,0,1),
           (2,'EOB - przesy趾a',0,0,1),
           (3,'PIKA',0,0,1),
           (4,'EOB',0,1,1),
           (5,'PIKA',0,1,1),
           (6,'EOB',0,2,1),
           (7,'EOP',0,2,1),
           (8,'EOiS',0,2,1),
           (9,'TPA',0,2,1);
           
           
--- UPDATE 001

ALTER TABLE DSG_PIKA add KOD_PAKIETU varchar(100);
ALTER TABLE DSG_PIKA add KOD_PUDLA varchar(100);

insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('KOLEJKA_SKANOW','Kolejka skanowania','KS','group',1,0);

-- UPDATE 002
CREATE TABLE DSG_PIKA_ODBIORCY(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);
           
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (1,'EOiS',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (2,'EOP',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (3,'ENSA',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (4,'EAG',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (5,'EIN',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (6,'Innowacje',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES (7,'Eslovakia',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES(8,'ECUW',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES(9,'inne',0,null,1);
INSERT INTO DSG_PIKA_ODBIORCY (cn, title,centrum,refValue,available) VALUES(10,'EOB',0,null,1);
update DSG_PIKA set ODBIORCA = ODBIORCA+1

--UPDATE 004
CREATE TABLE DSG_PIKA_UZEW
(
	ID	 		[int] IDENTITY(1,1) NOT NULL primary key,
	IMIE 		varchar(50) NULL,
	NAZWISKO varchar(100),
	EMAIL varchar(255)
);

INSERT INTO DSG_PIKA_UZEW values ('LUKASZ','WOZNIAK','lukasz.wozniak@docusafe.pl');

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'V_PIKA_UZEW')
   DROP VIEW V_PIKA_UZEW
   
CREATE VIEW V_PIKA_UZEW as
SELECT ID as ID,ID as cn,
	   NAZWISKO + ' ' + IMIE + '('+EMAIL+')' as title, 
	   0 as centrum, null as refValue, 1 as available 
FROM DSG_PIKA_UZEW;

ALTER TABLE DSG_PIKA add UZEW int null;

-- UPDATE 005

insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('ZARZADZ_BEZ_OTWIERANIA','Zarz康zanie dokumentami Bez otwierania','ZBO','group',1,0);


-- UPDATE 006
alter table dsg_pika add kod_miasto int;
CREATE TABLE DSG_PIKA_MIASTO
(
	ID	 		[int] IDENTITY(1,1) NOT NULL primary key,
	KODPOCZTOWY varchar(30),
	MIASTO varchar(100),
);

-- UPDATE 007
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES('EOBTPA','EOB TPA',0,0,1);
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES('EOBTPA','EOB TPA',0,1,1);
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES('EOBTPA','EOB TPA',0,2,1);

INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (15,'umowy o 安iadczenie Us逝g Dystrybucji',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (16,'aneksy umowy o 鈍iadczenie us逝g dystrybucji',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (17,'informacje o zako鎍zeniu 鈍iadczenia us逝g dystrybucji dla Klienta TPA',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (18,'korespondencja ze znakiem EOBR-WT-2012',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (19,'weryfikacja zg這szenia zmiany sprzedawcy',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (20,'weryfikacja wypowiedzenia ( w procesie zmiany sprzedawcy)',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (21,'korespondencja dot. likwidacji obiektu',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (22,'o鈍iadczenia o wstrzymaniu energii elektrycznej',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (23,'informacje o odst徙ienia wstrzymania energii elektrycznej',0,3,1);
INSERT INTO DSG_PIKA_KATEGORIE (cn, title,centrum,refValue,available) VALUES (24,'wnioski o rozwi您anie umowy',0,3,1);

INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES (1,'EOB - kurier PIKA',0,3,1);
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES (2,'EOB - przesy趾a',0,3,1);
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES (3,'PIKA',0,3,1);
           
INSERT INTO DSG_PIKA_MIEJSCE_DOSTAWY VALUES('EOBTPA','EOB TPA',0,3,1);

-- UPDATE 008
CREATE TABLE DSG_PIKA_ODPOWIEDZIALNA
(
	ID	 		[int] IDENTITY(1,1) NOT NULL primary key,
	MIEJSCOWOSC VARCHAR(50),
	NAZWA		varchar(100) null,
	IMIE 		varchar(30) NULL,
	NAZWISKO    varchar(100),
	EMAIL VARCHAR(50)
);

INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null, 'obs逝ga klient闚 biznesowych KB/KS','Aneta','Krajdocha','aneta.krajdocha@energa.pl');  
INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null, 'SME/SOHO - po鈔ednia','Romam','Kucharski','roman.kucharski@energa.pl'); 
INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null, 'departament wsparcia sprzeda篡','Jerzy','Pilip','jerzy.pilip@energa.pl'); 
INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null, 'pozosta貫 kierowane na dyrektor闚','Jacek','Taraszkiewicz','jacek.taraszkiewicz@energa.pl'); 
INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null, 'na pracownik闚 DOZU','Wojciech','Wa郾iewski','wojciech.wasniewski@energa.pl');

INSERT INTO [DSG_PIKA_ODPOWIEDZIALNA] VALUES (null,'Rzecznik Praw Klienta','Piotr','Skurzy雟ki','piotr.skurzynski@energa.pl');

alter table DSG_PIKA add ODPOWIEDZIALNY int;

-- update 009
alter table dsg_pika add do_poprawy int;
alter table DSG_PIKA_NADAWCA add KIEROWNIK_EMAIL varchar(100);

-- update 010
alter table dsg_pika_adresat add NAZWA varchar(200);
CREATE TABLE DSG_PIKA_ODBIORCY_F(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

SET IDENTITY_INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ON
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(5, 0)), N'1', N'Energa Obs逝ga i Sprzeda� Sp馧ka z o.o. ', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(5, 0)), N'2', N'Energa-Operator S.A.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(5, 0)), N'3', N'Energa S.A.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(5, 0)), N'4', N'Energa Agregator Sp. z o.o.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(5, 0)), N'5', N'Energa Informatyka i Technologie Sp. z o.o.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(5, 0)), N'6', N'Energa Innowacje Sp. z o.o.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(5, 0)), N'7', N'ENERGA Slovakia, s.r.o. , Jozef Semanko', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(5, 0)), N'8', N'ENERGA Centrum Us逝g Wsp鏊nych Sp. z o. o.', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(5, 0)), N'9', N'inne', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(5, 0)), N'10', N'Energa Obr鏒 S.A', 0, NULL, 1)
INSERT [dbo].[DSG_PIKA_ODBIORCY_F] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(5, 0)), N'11', N'Energa Obr鏒 SA -TPA', 0, NULL, 1)
SET IDENTITY_INSERT [dbo].[DSG_PIKA_ODBIORCY_F] OFF

update DSG_PIKA_ADRESAT set NAZWA = (SELECT title from DSG_PIKA_ODBIORCY_F where ID = DSG_PIKA_ADRESAT.PIKAODBIO) 

--UPDATE 011
create view dsg_pika_v_miasto as
select id, id 'cn',kodpocztowy + ' ' + miasto as 'title', 0 as centrum, null as refValue, 1 as 'available' from dsg_pika_miasto

--update 012
INSERT INTO DSG_PIKA_ADRESAT VALUES (null,null,'Towarowa 38','80-218','Gda雟k','Gda雟k',null,1,null);
INSERT INTO DSG_PIKA_ADRESAT VALUES (null,null,'Zamkowa 8','62-800','Kalisz','Kalisz',null,1,null);
INSERT INTO DSG_PIKA_ADRESAT VALUES (null,null,'Czerwona Droga 1','87-100','Toru�','Toru�',null,1,null);
INSERT INTO DSG_PIKA_ADRESAT VALUES (null,null,'Dwrocowa 3','10-413','Olsztyn','Olsztyn',null,1,null);
           
alter table dsg_pika_miasto add adresat_id int;

update dsg_pika_nadawca set kierownik_email = 'Anna.Nowicka@energa.pl' where kierownik = 'Anna Nowicka'
update dsg_pika_nadawca set kierownik_email = 'Remigiusz.Markizanski@energa.pl' where kierownik = 'Remigiusz Markiza雟ki'
update dsg_pika_nadawca set kierownik_email = 'Marek.Ciesilski@energa.pl' where kierownik = 'Marek Ciesilski'
update dsg_pika_nadawca set kierownik_email = 'Dariusz.Mikulak@energa.pl' where kierownik = 'Dariusz Mikulak'
update dsg_pika_nadawca set kierownik_email = 'Marcin.Dawidowski@energa.pl' where kierownik = 'Marcin Dawidowski'
update dsg_pika_nadawca set kierownik_email = 'Dorota.Korytkowska@energa.pl' where kierownik = 'Dorota Korytkowska'
update dsg_pika_nadawca set kierownik_email = 'Tomasz.Barejka@energa.pl' where kierownik = 'Tomasz Barejka'
update dsg_pika_nadawca set kierownik_email = 'Artur.Dembny@energa.pl' where kierownik = 'Artur Dembny'
update dsg_pika_nadawca set kierownik_email = 'Marcin.Dobosz@energa.pl' where kierownik = 'Marcin Dobosz'
update dsg_pika_nadawca set kierownik_email = 'Grzegorz.Konpa@energa.pl' where kierownik = 'Grzegorz Ko雗a'
update dsg_pika_nadawca set kierownik_email = 'Filip.Czerwinski@energa.pl' where kierownik = 'Filip Czerwi雟ki'

--update 013
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('KANCELARIA_EOB','KANCELARIA EOB','KEOB','division',7,0);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('KANCELARIA_PIKA','KANCELARIA PIKA','KEOB','division',7,0);

--update 014
alter table DSG_PIKA add STATUS int;

-- update 015
alter table dsg_pika_nadawca alter column ulica varchar(100);










