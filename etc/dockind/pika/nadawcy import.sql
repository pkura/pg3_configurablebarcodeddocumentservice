CREATE TABLE [PIKA_NADAWCA_1301](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[KLIENT] [varchar](100) NULL,
	[NIP] [varchar](30) NOT NULL,
	[FIRMA] [varchar](200) NULL,
	[POCZTA] [varchar](20) NULL,
	[MIEJSCOWOSC] [varchar](50) NULL,
	[ULICA] [varchar](50) NULL,
	[NRDOMU] [varchar](20) NULL,
	[NRLOKALU] [varchar](20) NULL,
	[DORADCA] [varchar](100) NULL,
	[DORADCAEMAIL] [varchar](100) NULL,
	[DORADCAADRES] [varchar](100) NULL,
	[WYDZIAL] [varchar](100) NULL,
	[WSPARCIE] [varchar](100) NULL,
	[WSPARCIEMAIL] [varchar](100) NULL,
	[WSPARCIEADRES] [varchar](100) NULL,
	[KIEROWNIK] [varchar](100) NULL,
[KIEROWNIK_EMAIL] [varchar](100) NULL,
	[SEGMENT] [varchar](100) NULL,
	
 CONSTRAINT [pkNadawcaid] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('kwachowski ','Wachowski','Krzysztof','krzysztof.wachowski@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mmehring','Mehring','Mateusz','mateusz.mehring@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('ewerbel','Werbel','Ewa','ewa.werbel@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('apiatkowska','Pi�tkowska','Aleksandra','aleksandra.piatkowska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('kmalczuk','Ma�czuk','Karolina','karolina.malczuk@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('abrydzinska','Brydzi�ska','Aleksandra','bz.pracownik@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('jtomaszewska','Tomaszewska','Joanna','bzpracownik.apt@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('astrzepek','Strz�pek','Agata','bzpracownik.apt@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('agliniecka','Gliniecka','Agata','bzpracownik.apt@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('jkubiak','Kubiak','Joanna','bzpracownik.apt@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('muszczynska','Uszczy�ska','Mariola','mariola.uszczynska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mmelonek','Melonek','Magdalena','magdalena.melonek@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mszeflinski','Szefli�ski','Marcin','marcin.szelfinski@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('ptaflinska','Tafli�ska','Paulina','paulina.taflinska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('rlangowski','��angowski','Rafa��','rafal.langowski@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('rsoszka','Soszka','Robert','robert.soszka@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('astolarz','Stolarz','Adam','adam.stolarz@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mpopek','Popek','Marta','marta.popek@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('gpastor','Pastor','Gabriela','gabriela.pastor@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('imiotke','Miotke','Iwona','iwona.miotke@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mochecka','Och�cka','Magdalena','magdalena.ochecka@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('pbartnik','Bartnik','Piotr','piotr.bartnik@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('it1','it1','','',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('it2','it2','','',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('apilska','Pilska-B�aszczyk','Agnieszka','agnieszka.pilska-blaszczyk@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('ikaminska','Kami�ska','Iwona','iwona.kaminska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('ikadziela','K�dziela','Iwona','iwona.kadziela@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('jszygulska','Szygulska','Joanna','joanna.szygulska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('mgilka','GI��KA','MA��GORZATA','malgorzata.gilka@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('rwesolek','WESO��EK','ROBERT','robert.wesolek@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('echrobocinska','CHROBOCI�SKA','EMILIA','emilia.chrobocinska@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('ikaminska','KAMI�SKA - MATL�GA','MAGDALENA','magdalena.kaminska-matlega@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('wpodgorny','PODG�RNY','WALDEMAR','waldemar.podgorny@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('aklimczuk','KLIMCZUK','ALINA','alina.klimczuk@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('bwozniak','WO�NIAK','BEATA','beata.wozniak@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('pszczesniak','SZCZE��NIAK','PRZEMYS��AW','przemyslaw.szczesniak@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('jzawodna','ZAWODNA','JOANNA','joanna.zawodna@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('dmiller','MILLER','DOROTA','dorota.miller@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('diluczonek','ILIUCZONEK','DOROTA','dorota.iliuczonek@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('dmiller','MILLER','DOROTA','dorota.miller@energa.pl',0,0,0,0);
insert into ds_user(NAME,LASTNAME,FIRSTNAME,EMAIL,LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER)values('diluczonek','ILIUCZONEK','DOROTA','dorota.iliuczonek@energa.pl',0,0,0,0);

--haslo qwe123
update ds_user set hashedpassword = '{SHA}xTJVMXuxFwfQ9hRpazzm8iHQ4vI=' where ID > 1

--- kursor dodanie roli pracownik kancelarii
DECLARE @name VARCHAR(50);

DECLARE db_cursor CURSOR FOR  
SELECT name FROM DS_USER WHERE ID > 2;

OPEN db_cursor

fetch next from db_cursor into @name

while @@FETCH_STATUS = 0
BEGIN
	insert into dso_role_usernames values(5, @name, 'own');
	fetch next from db_cursor into @name
END

close db_cursor
DEALLOCATE db_cursor


-- kursor dodanie uprawnie� dokument zwykly tworzenie i poboczne uprawnienia
DECLARE @name int;

DECLARE db_cursor CURSOR FOR  
SELECT id FROM DS_USER WHERE ID > 2;

OPEN db_cursor

fetch next from db_cursor into @name

while @@FETCH_STATUS = 0
BEGIN
	insert into ds_user_to_division values(@name, 2, 'own');
	insert into ds_user_to_division values(@name, 3, 'own');
	insert into ds_user_to_division values(@name, 4, 'own');
	insert into ds_user_to_division values(@name, 5, 'own');
	insert into ds_user_to_division values(@name, 6, 'own');
	fetch next from db_cursor into @name
END

close db_cursor
DEALLOCATE db_cursor
