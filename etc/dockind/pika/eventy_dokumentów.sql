DECLARE @documentId Numeric(19,0);
declare db_cursor CURSOR FOR select document_id from ds_Attachment where id in (select attachment_id from ds_attachment_revision where contentsize = 0) and deleted = 0;

OPEN db_cursor

fetch next from db_cursor into @documentId;

while @@FETCH_STATUS = 0
BEGIN
	update dse_event set event_status_id = 0 where params like cast(@documentId as varchar(200)) + '%' and params not like cast(@documentId as varchar(200)) + '|4%' ;
	fetch next from db_cursor into @documentId;
END
close db_cursor
DEALLOCATE db_cursor