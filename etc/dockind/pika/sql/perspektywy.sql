--mapuje dokumenty 
create view v_pika_emaile as
select p.document_id, 
case(doradcaemail)
when '-' then NULL
WHEN 'EOBeKancelaria@energa.pl' THEN null
else doradcaemail
END 'doradca',
n.wsparciemail, 
a.email 'adresatemail' 
from dsg_pika p
join dsg_pika_nadawca n ON n.id = p.nadawca
join dsg_pika_adresat a ON p.adresat = a.id
where p.document_id > 43 and doradcaemail is not null and wsparciemail is not null and a.email is not null
--order by document_id


--odwzorowuje document_id i doradce jako parametry w tabeli dse_Event
create view v_pika_doradca_mail_param as
(select pe.document_id,cast(pe.document_id as VARCHAR(20)) + '|1|'+pe.doradca as params
from v_pika_emaile pe
where cast(pe.document_id as VARCHAR(20)) + '|1|'+pe.doradca is not null)

-- odwzorowuje document_id i wsparcie jako parametry w tabeli dse_Event
create view [dbo].[v_pika_wsparcie_mail_param] as
(select pe.document_id,pe.wsparciemail, cast(pe.document_id as VARCHAR(20)) + '|5|'+pe.wsparciemail as params
from v_pika_emaile pe
where cast(pe.document_id as VARCHAR(20)) + '|4|'+pe.wsparciemail is not null)

--
create view [dbo].[v_pika_adresat_mail_param] as
(select pe.document_id,pe.adresatemail, cast(pe.document_id as VARCHAR(20)) + '|6|'+pe.adresatemail as params
from v_pika_emaile pe
where cast(pe.document_id as VARCHAR(20)) + '|6|'+pe.adresatemail is not null)

--
select DoradcaMail.document_id from dse_event e
join ( select dp.document_id, dp.params from v_pika_doradca_mail_param dp ) as DoradcaMail ON e.params like DoradcaMail.params +'%'


----------------zalaczniki razem z nuemrami ko. Wyciaganie po wielkosci powyzej 10 mb

--select j.sequence_id,ar.author,ar.ctime,ar.contentsize, ar.orginalfilename
select * 
from ds_attachment_revision ar with (nolock) 
left join ds_Attachment a with (nolock) ON a.id = ar.attachment_id
left join dso_journal_entry j with (nolock) ON j.documentid = a.document_id
where ar.contentsize > 10000000
order by ar.id, ar.revision
