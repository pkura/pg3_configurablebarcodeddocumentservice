CREATE TABLE [dbo].[dsg_ukw_delegacja](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[DATA_WYPELNIENIA] [date] NULL,
	[WORKER] [numeric](18,0) NULL,
	[WORKER_DIVISION] numeric(18,0) NULL,
	[START_DATE] [date] NULL,
	[FINISH_DATE] [date] NULL,
	[TRIP_PURPOSE] [varchar](251) NULL,
	[JEDNOSTKA_DELEGACJI] [varchar](150) NULL,
	[MIEJSCOWOSC_WYJAZDU] [varchar](150) NULL,
	[DODATKOWE_INFO] [varchar](350) NULL,
	[SRODEK_LOKOMOCJI] [varchar](100) NULL,
	[RYCZALT_COST] numeric(18,0) null,
	[SUMA_KOSZTOW_PODROZY] [numeric](18, 2) NULL,
	[DIETY] [numeric](18, 0) NULL,
	[NOCLEGI] numeric(18,0) null,
	[NOCLEGI_RYCZALT] [numeric](18, 2) NULL,
	[INNE_WYDATKI] [numeric](18, 0) NULL,
	[SUMA] numeric(18,0) null
) ON [PRIMARY]

CREATE TABLE dsg_ukw_delegacja_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);