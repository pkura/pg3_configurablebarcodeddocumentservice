CREATE TABLE DSG_KOSZTY_PODROZY
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	FROM_PLACE varchar(50),
	FROM_DATE datetime,
	TO_PLACE varchar (50),
	TO_DATE datetime,
	TRANSPORT varchar(100),
	OPIS varchar (249),
	KOSZT numeric (18,2)
);
