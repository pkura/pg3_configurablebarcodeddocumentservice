CREATE TABLE [dbo].[dsg_ukw_faktura_zakupu](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[contractor] [numeric](18,0) NULL,
	[kontrahent_nr_konta] numeric(18,0) NULL,
	[rodzaj_faktury] numeric(18,0) null,
	[nr_faktury] [varchar](30) NULL,
	[data_wystawienia] [date] NULL,
	[termin_platnosci] [date] NULL,
	[data_wplywu_faktury] [date] NULL,
	[kwota_brutto] [numeric](18, 2) NULL,
	[sposob] [numeric](18, 0) NULL,
	[opis] [varchar](4096) NULL,
	[addinfo] [varchar](4096) NULL,
) ON [PRIMARY]

CREATE TABLE dsg_ukw_faktura_zakupu_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);