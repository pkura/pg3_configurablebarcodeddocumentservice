CREATE TABLE [dbo].[dsg_ukw_ogolne](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[DATA_WYPELNIENIA] [date] NULL,
	[WORKER] [numeric](18,0) NULL,
	[TRESC] [varchar](4096) NULL,
) ON [PRIMARY]

CREATE TABLE dsg_ukw_delegacja_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);