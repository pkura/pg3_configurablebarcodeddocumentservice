CREATE TABLE [DSG_ALD_DOCKIND] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
    [status] [int] NULL ,
    [kategoria] [int] NULL,
    [rodzaj_dokumentu] [int] NULL ,
    [nr_kontraktu] [varchar] (50) NULL,
    [nr_rejestracyjny] [varchar] (50) NULL,
    [nr_vin] [varchar] (50) NULL,
    [kwota_brutto] [float] NULL,
    [nr_ksiegowania] [numeric] (19,0) NULL,
    [nr_faktury] [varchar] (50) NULL,
    [nip_dostawcy] [varchar] (50) NULL,
    [numer_dostawcy] [varchar] (50) NULL,
    [book_code] [varchar] (50) NULL,
    [data] [datetime] NULL ,
    [data_platnosci] [datetime] NULL ,
    [zaplacony] [smallint] NULL,
    [nr_autoryzacji] [varchar] (50) NULL,
    [nazwa_klienta] [varchar] (50) NULL,
    [nazwa_dostawcy] [varchar] (50) NULL
);

insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_ACCOUNTING', 'Ksi�gowo��','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_SERWIS', 'Obs�uga dokument�w serwisowych' ,'group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_HANDLOWY', 'Dzia� handlowy','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_IT', 'Dzia� IT','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_MARKETING', 'Dzia� marketingu','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_UBEZPIECZENIA', 'Ubezpieczenia','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_ZARZAD', 'Zarz�d','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_BACKOFFICE', 'Dzia� Backoffice','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ALD_CREDIT', 'Kredyt i finanse','group',1,0);