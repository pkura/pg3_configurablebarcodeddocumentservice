CREATE TABLE [dbo].[dsg_imgw_wniosek_rekrutacyjny](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[opis_przyczyny] [varchar](256) NULL,
	[stanowisko] [varchar](256) NULL,
	[wymagania] [varchar](4000) NULL,
	[os_wnioskujaca] NUMERIC (18,0) NULL,
	[author_division] NUMERIC (18,0) NULL,

    PRIMARY KEY CLUSTERED ([document_id] ASC)
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

--alter table dsg_imgw_wniosek_rekrutacyjny add [author_division] NUMERIC (18,0) NULL;