CREATE TABLE [dbo].[dsg_zrodlo_finansowania](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_zrodlo_finansowania ADD PRIMARY KEY (ID);
create index dsg_zrodlo_finansowania_index on dsg_zrodlo_finansowania (ID);

INSERT [dbo].[dsg_zrodlo_finansowania] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'KO', N'Koszty og�lne', NULL, NULL, 1);
INSERT [dbo].[dsg_zrodlo_finansowania] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'KZ', N'Koszty zak�adowe', NULL, NULL, 1);
INSERT [dbo].[dsg_zrodlo_finansowania] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'INW', N'Inwestycyjne', NULL, NULL, 1);