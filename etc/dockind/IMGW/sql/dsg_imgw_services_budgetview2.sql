create table dsg_imgw_services_budgetview2 (
id bigint identity(1,1) not null,

bdBudzetId numeric(18,2),
bdBudzetIdm varchar(50),
bdBudzetRodzajId numeric(18,2),
bdBudzetRodzajZadaniaId numeric(18,2),
bdBudzetRodzajZrodlaId numeric(18,2),
bdGrupaRodzajId numeric(18,2),
bdRodzaj varchar(100),
bdStrBudzetIdn varchar(50),
bdStrBudzetIds varchar(50),
bdSzablonPozId numeric(18,2),
bdZadanieId numeric(18,2),
bdZadanieIdn varchar(50),
budWOkrNazwa varchar(50),
budzetId numeric(18,2),
budzetIdm varchar(50),
budzetIds varchar(50),
budzetNazwa varchar(200),
czyAktywna numeric(18,2),
dokBdStrBudzetIdn varchar(50),
dokKomNazwa varchar(200),
dokPozLimit numeric(18,2),
dokPozNazwa varchar(200),
dokPozNrpoz numeric(18,0),
dokPozPIlosc numeric(18,2),
dokPozPKosz numeric(18,2),
dokPozRIlosc numeric(18,2),
dokPozRKoszt numeric(18,2),
dokPozRezIlosc numeric(18,2),
dokPozRezKoszt numeric(18,2),
okrrozlId numeric(18,2),
pozycjaPodrzednaId numeric(18,2),
strBudNazwa varchar(200),
wspolczynnikPozycji numeric(18,0),
wytworId numeric(18,2),
wytworIdm varchar(50),
zadanieNazwa varchar(200),
zadaniePIlosc numeric(18,2),
zadaniePKoszt numeric(18,2),
zadanieRIlosc numeric(18,2),
zadanieRKoszt numeric(18,2),
zadanieRezIlosc numeric(18,2),
zadanieRezKoszt numeric(18,2),
zrodloId numeric(18,2),
zrodloIdn varchar(50),
zrodloNazwa varchar(200),
zrodloPKoszt numeric(18,2),
zrodloRKoszt numeric(18,2),
zrodloRezKoszt numeric(18,2),
available bit default(1) not null
);

CREATE UNIQUE INDEX dsg_imgw_services_budgetview2_id
    ON dsg_imgw_services_budgetview2(id);


ALTER VIEW dsg_imgw_budget AS
SELECT * FROM
(
SELECT bdBudzetId as id,
1 AS available,
bdBudzetIdm AS cn,
bdNazwa AS title,
id AS refValue,
ROW_NUMBER() OVER (PARTITION BY bdBudzetId ORDER BY bdBudzetId) AS centrum
FROM dsg_imgw_services_budgetview2
)a
WHERE a.centrum = 1;


ALTER VIEW dsg_imgw_budget_position AS
SELECT bdBudzetRodzajId AS id,
1 AS available,
bdBudzetRodzajId AS cn,
dokPozNazwa AS title,
bdBudzetId AS refValue,
null AS centrum
FROM dsg_imgw_services_budgetview2;


ALTER VIEW dsg_imgw_sources AS
SELECT zrodloId AS id,
1 AS available,
zrodloIdn AS cn,
zrodloNazwa AS title,
bdBudzetRodzajId AS refValue,
null AS centrum
FROM dsg_imgw_services_budgetview2;