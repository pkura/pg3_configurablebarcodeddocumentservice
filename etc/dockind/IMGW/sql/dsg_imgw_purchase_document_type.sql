CREATE TABLE dsg_imgw_purchase_document_type (
	id numeric(18, 0) identity(1,1) NOT NULL,
	idm varchar(50),
	name varchar(150),
	currency_symbol varchar(10),
	document_type_ids varchar(50),
	czy_aktywny numeric(18, 2),
	czywal numeric(18, 2),
	available bit,
	erpId numeric(18, 2)
)


CREATE VIEW dsg_imgw_view_purchase_document_type AS
	SELECT
		id,
        idm AS cn,
        name AS title,
        convert(bit, czywal) AS centrum,
        CAST(erpId as int) AS refValue,
        available
	FROM dsg_imgw_purchase_document_type
	
	

update dsg_imgw_purchase_document_type set czy_aktywny = 1.0
update dsg_imgw_purchase_document_type set czywal = 0.0
update dsg_imgw_purchase_document_type set available = 1

