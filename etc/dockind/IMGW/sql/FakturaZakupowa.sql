USE [imgw]
GO

/****** Object:  Table [dbo].[dsg_imgw_faktura_zakupowa]    Script Date: 2013-08-19 16:07:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dsg_imgw_faktura_zakupowa](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[rodzaj_przedmiotu] [int] NULL,
	[data_wystawienia] [datetime] NULL,
	[termin_platnosci] [datetime] NULL,
	[nr_faktury] [varchar](30) NULL,
	[kwota_netto] [numeric](19, 2) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[kwota_vat] [numeric](19, 2) NULL,
	[opis] [numeric] (18,0) NULL,
	[podstawa_zakupu] [int] NULL,
	[nr_w_st] [varchar](30) NULL,
	[zrodlo_finansowania] [int] NULL,
	[tryb_zakupu] [int] NULL,
	[termin_platnosci_umowa] [datetime] NULL,
	[wykonano_w_terminie] [tinyint] NULL,
	[zgodnie_z_umowa] [tinyint] NULL,
	[opis_do_projektu] [varchar](4096) NULL,
	[osoba_merytoryczna] [numeric](18, 0) NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



ALTER TABLE dsg_imgw_faktura_zakupowa ADD PROTOKOL_ODBIORU numeric (18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD [numer_wniosku] [varchar](50) NULL;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD [numer_sprawy] [varchar](50) NULL;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD [postepowanie_nr] [varchar](512) null;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD [tryb_zamowienia] int;


CREATE TABLE dsg_imgw_faktura_zakupowa_multiple_value (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;


ALTER TABLE dsg_imgw_faktura_zakupowa ADD WNIOSEK_ZAKUPOWY numeric(18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD WNIOSEK_ZAKUPOWY_BOOL smallint;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD OPLATA_ADMINISTRACYJNA smallint;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD UMOWA smallint;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD NUMER_UMOWY varchar(50);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD OPIS_OPLATY varchar(512);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD OS_REALIZUJACA numeric(18,0);

SELECT CAST(opis AS numeric(18,0)) FROM dsg_imgw_faktura_zakupowa;
UPDATE dsg_imgw_faktura_zakupowa set opis = null;
ALTER TABLE dsg_imgw_faktura_zakupowa ALTER COLUMN opis numeric(18,0) NULL;

ALTER TABLE dsg_imgw_faktura_zakupowa ADD kontrahent_nr_konta_id numeric(18,0);

ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP numeric(18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_NUMER_UMOWY varchar(100);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_KWOTA_OGOLEM numeric(18,2);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_KWOTA_INWEST numeric(18,2);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_KWOTA_NIEINWEST numeric(18,2);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_DATA_UMOWY date;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_OPIS_ZWIAZKU varchar(500);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_KATEGORIA varchar(100);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_KWOTA_KWALIF numeric(18,2);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_NR_PROTOKOL varchar(100);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD STAMP_DATA_PROTOKOL date;

ALTER TABLE dsg_imgw_faktura_zakupowa ADD BRAK_KONTRAHENTA smallint;

ALTER TABLE dsg_imgw_faktura_zakupowa ADD WALUTA numeric(18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD OS_KSIEGOWOSC numeric(18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD IMPORTED_DOCUMENT numeric(18,0);

ALTER TABLE dsg_imgw_faktura_zakupowa ADD TYP_FAKTURY numeric(18,0);
ALTER TABLE dsg_imgw_faktura_zakupowa ADD ZWROT_KOSZTOW smallint;
ALTER TABLE dsg_imgw_faktura_zakupowa ADD BEZ_PROTOKOLU smallint;
