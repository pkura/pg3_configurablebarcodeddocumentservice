create table dsg_imgw_services_budget (
    id bigint identity(1,1) not null,
    bdBudzetId numeric(18,0) null,
    bdBudzetIdm varchar(50) null,
    bdNazwa varchar(128) null,
    bdPozBudzetId numeric(18,0) null,
    bdPozDoWykKoszt numeric(18,2) null,
    bdRodzaj varchar(128) null,
    bdUklad bigint null,
    czyArchiwalny bigint null,
    komBudId numeric(18,0) null,
    komBudIdn varchar(50) null,
    komBudNazwa varchar(128) null,
    okrBudId numeric(18,0) null,
    okrBudIdm varchar(50) null,
    okrBudNazwa varchar(128) null,
    okrBudRokObr numeric(18,0) null,
    okrBudTypId numeric(18,0) null,
    pBdPozBudzetNazwa varchar(128) null,
    pBdPozBudzetNrpoz bigint null,
    pBdPozPKoszt numeric(18,2) null,
    pBdPozRKoszt numeric(18,2) null,
    pBdPozRezKoszt numeric(18,2) null )


CREATE VIEW dsg_imgw_budget AS
SELECT * FROM
(
SELECT bdBudzetId as id,
1 AS available,
bdBudzetIdm AS cn,
bdNazwa AS title,
id AS refValue,
ROW_NUMBER() OVER (PARTITION BY bdBudzetId ORDER BY bdBudzetId) AS centrum
FROM dsg_imgw_services_budget
)a
WHERE a.centrum = 1


CREATE VIEW dsg_imgw_budget_position AS
SELECT bdPozBudzetId AS id,
1 AS available,
bdPozBudzetId AS cn,
pBdPozBudzetNazwa AS title,
bdBudzetId AS refValue,
null AS centrum
FROM dsg_imgw_services_budget;