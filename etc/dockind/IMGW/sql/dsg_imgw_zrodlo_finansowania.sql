create table dsg_imgw_zrodlo_finansowania (
id BIGINT IDENTITY(1,1) NOT NULL,

ACTIVITY_KIND INT NULL,
COST_KIND INT NULL,
BUDGET INT NULL,
POSITION INT NULL,
SOURCE INT NULL,
AMOUNT FLOAT NULL,

)

ALTER TABLE dsg_imgw_zrodlo_finansowania ADD CONTRACT INT NULL;
ALTER TABLE dsg_imgw_zrodlo_finansowania ADD PRODUCT INT NULL;
ALTER TABLE dsg_imgw_zrodlo_finansowania ADD STAWKA_VAT INT NULL;
ALTER TABLE dsg_imgw_zrodlo_finansowania ADD KWOTA_VAT FLOAT NULL;
ALTER TABLE dsg_imgw_zrodlo_finansowania ADD KIEROWNIK INT NULL;
ALTER TABLE dsg_imgw_zrodlo_finansowania ADD ROZL_VAT INT NULL;

alter table dsg_imgw_zrodlo_finansowania add  TERMIN_POZYSKANIA date;
alter table dsg_imgw_zrodlo_finansowania add  WARTOSC_NETTO numeric(18,2);
alter table dsg_imgw_zrodlo_finansowania add  ILOSC numeric(18,0);
alter table dsg_imgw_zrodlo_finansowania add  NAZWA_PRZEDMIOTU varchar(100);


alter table dsg_imgw_zrodlo_finansowania add  DOSTAWA varchar(100);
alter table dsg_imgw_zrodlo_finansowania add  CENA_JEDN_NETTO numeric(18,2);
