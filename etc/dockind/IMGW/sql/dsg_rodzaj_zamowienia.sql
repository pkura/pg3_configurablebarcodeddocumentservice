CREATE TABLE [dbo].[dsg_rodzaj_zamowienia](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_rodzaj_zamowienia ADD PRIMARY KEY (ID);
create index dsg_rodzaj_zamowienia_index on dsg_rodzaj_zamowienia (ID);

INSERT [dbo].[dsg_rodzaj_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'USLUGA', N'Us�uga', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'DOSTAWA', N'Dostawa', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ROBOTA_BUD', N'Robota budowlana', NULL, NULL, 1);