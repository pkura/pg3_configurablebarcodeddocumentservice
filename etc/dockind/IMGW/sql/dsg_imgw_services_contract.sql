create table dsg_imgw_services_contract (
id bigint identity(1,1) not null,
erpId numeric(18,2) null,
identyfikatorNum numeric(18,2) null,
idm varchar(128) null,
nazwa varchar(128) null,
pDataKo date null,
pDataPo date null,
typKontraktId numeric(18,2) null,
typKontraktIdn varchar(128) null,
kierownikId numeric(18,0) null,
opiekunId numeric(18,0) null,
dodatkowaAkceptacja smallint null );

CREATE CLUSTERED INDEX [c-wytworId] ON [dbo].[dsg_imgw_services_contract]
(
	[erpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


CREATE VIEW dsg_imgw_view_contract AS
SELECT id AS id,
1 AS available,
idm AS cn,
nazwa AS title,
kierownikId AS refValue,
erpId AS centrum
FROM dsg_imgw_services_contract;

ALTER TABLE dsg_imgw_services_contract ADD bpRodzajWersjiId numeric(18,2);