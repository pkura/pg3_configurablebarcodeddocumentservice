CREATE TABLE [dbo].[dsg_imgw_wniosek_zakupowy](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[rodzaj_zamowienia] [numeric](18, 0) NULL,
	[rodzaj_przedmiotu] [numeric](18, 0) NULL,
	[przedmiot_zamowienia] [numeric](18, 0) NULL,
	[wartosc_netto] [numeric](18, 2) NULL,
	[zal_protokol_badania] [numeric](18, 0) NULL,
	[uzasadnienie_zakupu] [varchar](350) NULL,
	[realizator] [numeric](18, 0) NULL,
	[zal_oferty_1] [numeric](18, 0) NULL,
	[zal_oferty_2] [numeric](18, 0) NULL,
	[zal_druk_zamowienia] [numeric](18, 0) NULL,
	[inicjator] [numeric](18, 0) NULL,
	[symbol_komorki] [varchar](50) NULL,
	[numer_wniosku] [varchar](50) NULL,
	[kody_cpv] [varchar](50) NULL,
	[osoba_odpowiedzialna] [numeric](18,0),
	[project_accept] [bit],
	[more_att] [bit]
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
	
ALTER TABLE [dbo].[dsg_imgw_wniosek_zakupowy]
ADD OS_REALIZUJACA int

CREATE TABLE [dbo].[dsg_imgw_wniosek_zakupowy_przedmiot](
	[id] [numeric](19, 0) identity(1,1) NOT NULL,
	[nazwa] [varchar](100) NULL,
	[cena_jedn_netto] [numeric](18, 2) NULL,
	[ilosc] [numeric](18, 0) NULL,
	[wartosc_netto] [numeric](18, 2) NULL,
	[zrodlo_finansowania] [numeric](18, 0) NULL,
	[dostawa] [varchar](100) NULL
)