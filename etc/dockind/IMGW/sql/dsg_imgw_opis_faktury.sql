CREATE TABLE [dbo].[dsg_imgw_opis_faktury](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](100) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_imgw_opis_faktury ADD PRIMARY KEY (ID);
create index dsg_imgw_opis_faktury_index on dsg_imgw_opis_faktury (ID);

INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(Sie� stacji hydrologicznych)', N'Prace na Sieci stacji hydrologicznych i meteorologicznych - ', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(POLRAD/PERUN)', N'Prace na sieci POLRAD i/lub PERUN w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(System Telemetrii)', N'Prace na rzecz Systemu Telemtrii w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(stacje aerologiczne)', N'Prace na sieci stacji aerologicznych w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(biura prognoz hydrologicznych)', N'Prace na rzecz biur prognoz hydrologicznych w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(biura prognoz meteorologicznych)', N'Prace na rzecz biur prognoz meteorologicznych w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(system teleinformatyczny)', N'Prace na rzecz systemu teleinformatycznego w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(system gromadzenia danych)', N'Prace na rzecz systemu gromadzenia danych w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(system przesy�ania i dystrybucji danych)', N'Prace na rzecz systemu przesy�ania i dystrybucji danych w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(rozliczenie miesi�cznej dzia�alno�ci)', N'Sprawy zwi�zane z rozliczeniem miesi�cznej dzia�alno�ci
(DSPO,BPH,BPM,RSHM,SHM,SH,WOM,LSM,SK,inne) ', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(wody graniczne)', N'Wsp�praca na wodach granicznych:
- wsp�lne pomiary hydrometryczne w:
- uzgodnienia przep�yw�w
- spotkanie na temat:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Tel. stacjonarne (PSHM)', N'Op�ata za telefony stacjonarne wykorzystywane dla potrzeb PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Tel. stacjonarne (PSHM-rycza�t)', N'Op�ata za telefony stacjonarne wykorzystywane dla ��czno�ci z obserwatorami rycza�towymi PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Tel. kom�rkowe (PSHM)', N'Op�ata za telefony kom�rkowe wykorzystywane dla potrzeb PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Tel. kom�rkowe (PSHM-rycza�t)', N'Op�ata za telefony kom�rkowe wykorzystywane dla ��czno�ci z obserwatorami rycza�towymi PSHM w::', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Karty telefoniczne (PSHM)', N'Karty telefoniczne do telefon�w kom�rkowych wykorzystywanych dla potrzeb PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Karty telefoniczne (PSHM-rycza�t)', N'Karty telefoniczne do telefon�w kom�rkowych wykorzystywanych dla ��czno�ci z obserwatorami rycza�towymi PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Op�aty telefoniczne (Szczecinek)', N'Op�ata telefoniczna za transmisj� danych drog� modemow� ze stacji automatycznej w Szczecinku', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Op�aty pocztowe', N'Op�aty pocztowe za przesy�anie materia��w PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'POLPAK', N'Dzier�awa ��czy - szybka sie� transmisji danych POLPAK-T dla potrzeb PSHM', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VSAT', N'Us�uga satelitarnej transmisji danych na potrzeby Sieci stacji hydrologicznych i meteorologicznych PSHM (z funkcj� telemetryczn�)', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PTK Centerltel,RS TV SA,TP EMITEL', N'Wynajem infrastruktury i op�aty pod instalacje urz�dze� nadawczo odbiorczych pracuj�cych dla PSHM', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'DATATRANS', N'��cza satelitarne dla potrzeb PSHM', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'TP SA (ISDN)', N'��czno�� ISDN na potrzeby Sieci stacji hydrologicznych i meteorologicznych PSHM (z funkcj� telemetryczn�)', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'TP SA (infolinia)', N'Infolinia do zg�aszania awarii na potrzeby Sieci stacji hydrologicznych i meteorologicznych PSHM (z funkcj� telemetryczn�)', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'TP SA (INMARSAT-C)', N'Us�uga satelitarnej transmisji danych w sieci INMARSAT-C na potrzeby Sieci stacji hydrologicznych i meteorologicznych PSHM (z funkcj� telemetryczn�)', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'NETSYS', N'Serwis i utrzymanie systemu telekomunikacyjnego pracuj�cego w GTS WMO', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'EQUANT', N'Dzier�awa ��czy mi�dzynarodowych dla przesy�ania danych w GTS WMO', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'CROWLEY,NASK,TASK', N'Dost�p do sieci Internet dla PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'GTS PARTNERS', N'Op�ata za skrzynki poczty elektronicznej dla stacji synoptycznych PSHM, nieposiadaj�cych dost�pu do sieci WAN', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'energia elektryczna', N'Zu�ycie energii elektrycznej na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'energia cieplna', N'Zu�ycie energii cieplnej na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'gaz', N'Zu�ycie gazu na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'opa� (olej opa�owy, w�giel, koks, inne)', N'Zu�ycie opa�u na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'zu�ycie wody, woda', N'Zu�ycie wody na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'wyw�z nieczysto�ci, �cieki', N'Wyw�z nieczysto�ci z obiektu PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'wyw�z �mieci, �mieci', N'Wyw�z �mieci z obiektu PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ubezpieczenie', N'Ubezpieczenie obiekt�w (sprz�tu) PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'dzier�awa', N'Op�ata za dzier�aw� (obiektu, terenu, lokalu) na potrzeby PSHM w:', NULL, NULL, 1);


INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(dla cz�onk�w zarz�du)', N'Kontrola pracy:
- Pa�stwowej S�u�by Hydrologiczno-Meteorologicznej
- Hydrologiczno-Meteorologicznej S�u�by Pomiarowo-Obserwacyjnej
- S�u�by Prognoz Meteorologicznych
- S�u�by Prognoz Hydrologicznych
- Systemu przesy�ania i dystrybucji danych i produkt�w', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Delegacja(opis z zaproszenia)', N'(opis z zaproszenia na szkolenie, zebranie seminarium, konferencje, inne spotkanie-jednolity dla wszystkich zaproszonych)', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Op�aty telefoniczne', N'- Op�ata telefoniczna za transmisj� danych PSHM drog� modemow� z:
- Refundacja abonamentu telefonicznego z tytu�u u�ytkowania telefonu prywatnego na potrzeby sygnalizacji PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Op�aty internetowe', N'Refundacja koszt�w dost�pu do sieci Internet wykorzystywanego na potrzeby PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Zwi�zane ze �rodkami tranportu', N'Do samochodu, przyczepy, �odzi, pontonu PSHM w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Przesy�ki kurierskie, konduktorskie, itp.', N'Op�ata za przes�anie materia��w PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Materia�y biurowe', N'Materia�y biurowe na potrzeby PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Materia�y eksploatacyjne, budowlane i inne', N'Materia�y na rzecz PSHM wykorzystano w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Op�ata za GPS', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Polisa ubezpieczeniowa', N'Polisa na wyjazd zwi�zany z PSHM', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Us�ugi', N'Us�ugi na rzecz PSHM wykorzystano w:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Oprogramowanie', N'Oprogramowanie dla PSHM (opisa� jakie):', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Posi�ki regeneracyjne', N'Posi�ki regeneracyjne dla pracownik�w PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Odzie� ochronna i robocza', N'Odzie� ochronna (robocza) dla pracownik�w PSHM:', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_opis_faktury] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Odpady', N'Utylizacja odpad�w wynikaj�cych z dzia�alno�ci PSHM w:', NULL, NULL, 1);
