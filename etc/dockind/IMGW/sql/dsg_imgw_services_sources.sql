create table dsg_imgw_services_sources (
    id bigint identity(1,1) not null,
    bdBudzetPozId numeric(18,0) null,
    rodzaj bigint null,
    zrodloId numeric(18,0) null,
    zrodloIdn varchar(50) null,
    zrodloNazwa varchar(250) null,
    zrodloPKoszt numeric(18,2) null,
    zrodloRKoszt numeric(18,2) null,
    zrodloRezKoszt numeric(18,2) null )


CREATE VIEW dsg_imgw_sources AS
SELECT id AS id,
1 AS available,
zrodloId AS cn,
zrodloNazwa AS title,
bdBudzetPozId AS refValue,
zrodloId AS centrum
FROM dsg_imgw_services_sources;