CREATE TABLE [dbo].[dsg_imgw_rodzaj_przedmiotu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_imgw_rodzaj_przedmiotu ADD PRIMARY KEY (ID);
create index dsg_imgw_rodzaj_przedmiotu_index on dsg_imgw_rodzaj_przedmiotu (ID);

INSERT [dbo].[dsg_imgw_rodzaj_przedmiotu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'SRODKI_SPOZYWCZE', N'�rodki spo�ywcze', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_rodzaj_przedmiotu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'KOMPUTEROWE', N'Komputerowe', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_rodzaj_przedmiotu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'BHP', N'BHP', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_rodzaj_przedmiotu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'SZKOLENIOWE', N'Szkoleniowe', NULL, NULL, 1);
INSERT [dbo].[dsg_imgw_rodzaj_przedmiotu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'INNE', N'Inne', NULL, NULL, 1);