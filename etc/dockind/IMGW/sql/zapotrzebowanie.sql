CREATE TABLE dsg_imgw_zapotrzebowanie_przedmiot (
	id numeric(18, 0) identity(1,1) NOT NULL,
	nazwa_przedmiotu varchar(100),
	ilosc numeric(18, 0),
	wartosc_netto numeric(18, 2),
	centrumid numeric(18, 0),
	termin_pozyskania date
);

CREATE TABLE dsg_imgw_zapotrzebowanie (
	document_id numeric(18, 0) NOT NULL,
	status numeric(18, 0),
	wartosc_netto numeric(18, 2),
	uzasadnienie_zakupu varchar(4096),
	inicjator numeric(18, 0),
	symbol_komorki varchar(50),
	numer_zapotrzebowania varchar(50),
	opinia varchar(500),
	decyzja varchar(350)
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE dsg_imgw_zapotrzebowanie_multiple (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL 
);

ALTER TABLE dsg_zrodlo_finansowania ADD supervisors varchar(50);