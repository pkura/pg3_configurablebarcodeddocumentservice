CREATE TABLE dsg_imgw_wniosek_zampub (
	DOCUMENT_ID 		    bigint NOT NULL,
	STATUS 				    bigint,
	RODZAJ_ZAMOWIENIA	    int,
	PRZEDMIOT_ZAMOWIENIA    varchar(300),
	ZRODLO_FINANSOWANIA 	int,
	POZYCJA_PLANU   		varchar(400),
	TERMIN_POZYSKANIA    	date,
	SYMBOL_KOMORKI      	varchar(10),
	NR_WNIOSKU      		varchar(44),
	TRYB_ZAMOWIENIA     	int,
	PROJECT_MANAGER_ACC     int,
	OPIS_PRZEDMIOTU         numeric (18,0),
	DOK_SZACOWANIA          numeric (18,0),
	SZACUNKOWA_WARTOSC      int,
	STATUS_DH               int,
	OS_REALIZUJACA          int
);


ALTER TABLE dsg_imgw_wniosek_zampub ADD PRIMARY KEY (document_id);
create index dsg_imgw_wniosek_zampub_index on dsg_imgw_wniosek_zampub (document_id);


CREATE TABLE dsg_imgw_wniosek_zampub_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;
