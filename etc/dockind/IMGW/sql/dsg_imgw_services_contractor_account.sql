create table dsg_imgw_services_contractor_account (
id bigint identity(1,1) not null,
czyAktywne numeric(18,2) null,
erpId numeric(18,2) null,
identyfikatorNum numeric(18,2) null,
idm varchar(128) null,
kolejnoscUzycia int null,
numerKonta varchar(128) null,
symbolwaluty varchar(128) null,
available int null);

CREATE CLUSTERED INDEX [c-wytworId] ON [dbo].[dsg_imgw_services_contractor_account]
(
	[erpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view dsg_imgw_view_contractor_account as
select
acc.id as id,
acc.erpId as cn,
acc.idm+' '+acc.numerKonta as title,
acc.kolejnoscUzycia as centrum,
acc.available as available,
per.id as refValue
from
dsg_imgw_contractor per
join dsg_imgw_services_contractor_account acc on per.erpId=acc.identyfikatorNum;



--select * into dsg_imgw_contractor_account from dsg_ul_view_contractor_account;
--
--CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_imgw_contractor_account
--(
--	[cn] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
--GO


