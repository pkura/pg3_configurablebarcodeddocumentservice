create table dsg_imgw_services_cost_invoice_product (
    id bigint identity(1,1) not null,
    available int null,
    jmIdn varchar(50) null,
    klasyWytwIdn varchar(50) null,
    nazwa varchar(200) null,
    rodzTowaru numeric(18,0) null,
    vatStawIds varchar(50) null,
    wytworId numeric(18,0) null,
    wytworIdm varchar(50) null )


CREATE VIEW dsg_imgw_produkt AS
SELECT id AS id,
available AS available,
wytworIdm AS cn,
wytworIdm + ' ' + nazwa AS title,
null AS refValue,
wytworId AS centrum
FROM dsg_imgw_services_cost_invoice_product;