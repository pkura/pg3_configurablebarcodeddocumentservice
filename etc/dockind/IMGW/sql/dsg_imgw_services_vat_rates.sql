create table dsg_imgw_services_vat_rates (
id bigint identity(1,1) not null,
czyAktywna numeric(18,2) null,
czyZwolniona numeric(18,2) null,
dataObowDo date null,
erpId numeric(18,2) null,
idm varchar(128) null,
nazwa varchar(128) null,
niePodlega numeric(18,2) null,
procent numeric(18,2) null,
available int null);

CREATE CLUSTERED INDEX [c-wytworId] ON [dbo].[dsg_imgw_services_vat_rates]
(
	[erpId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view dsg_imgw_vat_rates as
select
id as id,
niePodlega as cn,
idm+' '+nazwa as title,
erpId as centrum,
available as available,
czyZwolniona as refValue
from dsg_imgw_services_vat_rates;