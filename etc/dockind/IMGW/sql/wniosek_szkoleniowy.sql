CREATE TABLE [dbo].[dsg_imgw_wniosek_szkoleniowy](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] numeric(18,0) NULL,
	[nazwa_szkolenia] [varchar](350) NULL,
	[wnioskujacy] numeric(18,0) NULL,
	[symbol_komorki] [varchar](50) NULL,
	[termin_szkolenia_od] [datetime] NULL,
	[termin_szkolenia_do] [datetime] NULL,
	[nazwa_organizacji_prowadzacej] [varchar](350) NULL,
	[laczny_koszt] integer NULL,
	[procent_finansowania] numeric(18,2) NULL,
	[zrodlo_finansowania] numeric(18,0) NULL,
	[text10] [varchar](1024) NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE dsg_imgw_wniosek_szkoleniowy_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;
