CREATE TABLE [dbo].[dsg_tryb_zamowienia](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_tryb_zamowienia ADD PRIMARY KEY (ID);
create index dsg_tryb_zamowienia_index on dsg_tryb_zamowienia (ID);

INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PN', N'Przetarg nieograniczony', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PO', N'Przetarg ograniczony', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZOC', N'Zapytanie o cen�', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'WR', N'Wolna r�ka', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'DK', N'Dialog konkurencyjny', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'NBO', N'Negocjacje bez og�oszenia', NULL, NULL, 1);
INSERT [dbo].[dsg_tryb_zamowienia] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'NZO', N'Negocjacje z og�oszeniem', NULL, NULL, 1);