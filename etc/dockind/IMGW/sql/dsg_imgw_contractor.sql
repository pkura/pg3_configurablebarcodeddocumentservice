create table dsg_imgw_contractor (
id int identity(1,1),
available bit default(1) not null,
erpId numeric(18,2),
idm varchar(50),
identyfikatorNum numeric(18,2),
klasdostId numeric(18,2),
klasdostIdn varchar(50),
nazwa varchar(300),
nip varchar(50),
miasto varchar(50),
kod_pocztowy varchar(50),
ulica varchar(50),
nr_domu varchar(50),
nr_mieszkania varchar(50)
)
CREATE UNIQUE INDEX dsg_imgw_contractor_id
    ON dsg_imgw_contractor(id);


alter table dsg_imgw_faktura_zakupowa add contractor int;