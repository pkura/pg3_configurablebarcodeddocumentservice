alter table dsg_prawny add przedmiot_uchwaly varchar(50);

alter table dsg_prawny add przedmiot_umowy int;
alter table dsg_prawny add nieaktywny smallint default 0;


-- slownik dla przedmiotu umowy
CREATE TABLE [dsg_przedmiot_umowy](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL
) ON [PRIMARY];
alter table DSG_PRAWNY add constraint dsg_prawny_przedmiot_umowy_id_fk foreign key (przedmiot_umowy) references dsg_przedmiot_umowy ON DELETE SET NULL;

-- pocz�tkowe dane 
INSERT INTO [dsg_przedmiot_umowy]
           ([cn] ,[title] ,[centrum] ,[refValue] ,[available])
     VALUES
           (100,'Telekomunikacyjne', null, null, 1),
           (110,'Najmu', null, null, 1),
           (120,'Inwestycyjne', null, null, 1),
           (130,'Marketingowe', null, null, 1),
           (135,'Sprzeda�owe', null, null, 1),
           (140,'IT', null, null, 1),
           (150,'Administracyjne', null, null, 1),
           (160,'HR', null, null, 1),
           (170,'Reasekuracyjne', null, null, 1);


-- Tabela do modulu zarzadzanie umow�
CREATE TABLE [dso_contract_managment](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[document_id] [numeric](19, 0) NOT NULL,
	[user_id] [numeric](19, 0) NOT NULL,
	[contract_date] [datetime] NULL,
	[return_date] [datetime] NULL,
	[note] [varchar](4000) NULL
) ON [PRIMARY];

-- tworzenie grup dla dezi do generowania raport�w
INSERT INTO [DS_DIVISION]
           ([GUID]
           ,[NAME]
           ,[CODE]
           ,[DIVISIONTYPE]
           ,[PARENT_ID]
           ,[hidden]
           ,[description])
     VALUES
           ('DEZI_DIVISION', 'Raport obs�ugi um�w', NULL, 'group', 1, 0, NULL),
           ('AUTORIZED_DEZI_DIVISION', 'Raport CRU', NULL, 'group', 1, 0, NULL);

