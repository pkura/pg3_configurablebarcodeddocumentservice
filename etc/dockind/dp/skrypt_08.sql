alter table dsg_prawny add orginal_attachment smallint default 0;
alter table dsg_prawny add bissness_owner [numeric](19, 0) NULL;
alter table dsg_prawny add deadline datetime null;

UPDATE [DS_DIVISION]
   SET [DIVISIONTYPE] = 'group'
 WHERE [GUID] IN ('DEZI_DIVISION', 'AUTORIZED_DEZI_DIVISION')
