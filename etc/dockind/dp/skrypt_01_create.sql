CREATE TABLE [DSG_PRAWNY]
(
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL primary key,
	[KLASA] [varchar](50) COLLATE Polish_CI_AS NOT NULL,
	[TYP] [varchar](50) COLLATE Polish_CI_AS NOT NULL,
	[STRONA] [int] NULL,
	[DATA_UMOWY] [datetime] NULL,
	[DATA_ANEKSU] [datetime] NULL,
	[DATA] [datetime] NULL,
	[DATA_POCZATKOWA] [datetime] NULL,
	[DATA_KONCOWA] [datetime] NULL,
	[SYGNATURA] [varchar](50) COLLATE Polish_CI_AS NULL,
	[STATUS] [int] NULL,
	[RODZAJ_SPRAWY] [varchar](80) COLLATE Polish_CI_AS NULL,
	[INST] [int] NULL,
	[IMIE] [varchar](50) COLLATE Polish_CI_AS NULL,
	[NAZWISKO] [nvarchar](50) COLLATE Polish_CI_AS NULL,
	[PRODUKT] [int] NULL,
	[MIEJSCOWOSC] [int] NULL,
	[NUMTRANS1] [varchar](50) COLLATE Polish_CI_AS NULL,
	[NUMTRANS2] [varchar](50) COLLATE Polish_CI_AS NULL,
	[NUMTRANS3] [varchar](50) COLLATE Polish_CI_AS NULL,
	[NUMTRANS4] [varchar](50) COLLATE Polish_CI_AS NULL,
	[NUMTRANS5] [varchar](50) COLLATE Polish_CI_AS NULL,
	PRODUKT_INFO varchar(4096) NULL
);

CREATE TABLE [dp_inst] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[name] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[oldname] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

CREATE TABLE [dp_strona] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[sygnatura] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

CREATE TABLE [DSG_DP_MULTIPLE_VALUE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);

CREATE INDEX idx_dsg_dp_multiple_value ON DSG_DP_MULTIPLE_VALUE (DOCUMENT_ID,FIELD_CN);