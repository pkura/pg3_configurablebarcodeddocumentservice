CREATE TABLE [DSG_UMOWA_PN](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	data_wplywu datetime NULL,
	kontrahent int NULL,
	data_od datetime NULL,
	data_do datetime NULL,	
	wartosc varchar(100) NULL,
	dzial int NULL,
	kary_info varchar(200) NULL
);