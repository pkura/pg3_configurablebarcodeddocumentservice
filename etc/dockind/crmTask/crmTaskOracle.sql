create table DSC_TASK(
    document_id         integer not null,
    kategoria		integer,
    rodzaj              integer,
    contractor_number   integer,
    worker_number       integer,    
    opis                varchar2(600 char),
    ctime               timestamp
);

CREATE SEQUENCE DSC_TASK_ID start with 1 nocache;