create table DSC_BUSINESS_TASK(
    document_id         integer not null,
    kategoria           integer,
    contractor_number   integer,
    worker_number       integer,
    wynik_kontaktu      integer,
    opis                varchar2(600 char),
    forma_leasingu		integer,
    cena 				float,
    przedmiot_leasingu	integer,
	region				integer,
    rok_produkcji 		varchar2(4 char),
    okres_leasingu		integer,
    WPLATA_WSTEPNA		float,
    ubezpieczenie_kom	number(1),
    ubezpieczenie_das	number(1),
    ubezpieczenie_gap	number(1),
    data_kolejnego_kontaktu 	timestamp,
    status_oferty		integer,
	action_count		smallint  
);
CREATE SEQUENCE DSC_BUSINESS_TASK_ID start with 1 nocache;