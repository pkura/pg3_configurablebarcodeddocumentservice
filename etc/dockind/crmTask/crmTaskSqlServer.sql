create table DSC_TASK(
    document_id numeric(19, 0) NOT NULL,
    kategoria		integer,
    rodzaj              integer,
    contractor_number   integer,
    worker_number       integer,    
    opis                varchar(600),
    ctime               datetime
);