create table DSC_APPLICATION(
    document_id numeric(19, 0) NOT NULL ,
    rodzaj              integer,
    contractor_number   integer,
    worker_number       integer,
    archiwizacja        smallint,
    opis                varchar(600 ),
    ctime               datetime
);
