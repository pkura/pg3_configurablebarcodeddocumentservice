create table DSC_APPLICATION(
    document_id                  integer not null,
    rodzaj              integer,
    contractor_number   integer,
    worker_number       integer,
    archiwizacja        smallint,
    opis                varchar2(600 char),
    ctime               timestamp,
    data				timestamp
);
CREATE SEQUENCE DSC_APPLICATION_ID start with 1 nocache;