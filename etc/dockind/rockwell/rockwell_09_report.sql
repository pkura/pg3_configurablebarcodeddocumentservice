create view ap_report as 
select 
 ra.document_id as document_id,
 doc.ctime as document_date,
 typ.type_code as document_type,
 cat.category_name as category_name,
 ra.invoice_number as invoice_number,
 ven.name as vendor_name,
 right('000000000'+rtrim(ltrim(str(ven.vendor_id))),10) as vendor_id,
 bu.business_unit_name as business_unit,
 ra.invoice_receive_date as invoice_receive_date,
 ra.invoice_date as invoice_date,
 ra.gross_amount as gross_amount,
 ra.net_amount as net_amount,
 ra.vat_amount as vat_amount,
 cur.currency_code as currency,
 ra.document_status as status,
 ra.po_number as po_number,
 ra.barcode as bar_code,
 ra.registr_no as registr_no,
 ra.submitted_by as created_by,
 ra.voucher_number as voucher_number,
 ra.eva_number as eva_number,
 ra.receive_date as eva_receive_date,
 ra.eva_finish_date as eva_finish_date,
 doc.finishuser as process_finish_user,
 doc.finishdate as process_finish_date
 from 
 dsg_rockwell ra join ds_document doc on ra.document_id = doc.id
 join dsg_rockwell_category cat on ra.category_id = cat.category_id
 join dsg_rockwell_business_unit bu on bu.business_unit_id = ra.business_unit_id
 join dsg_rockwell_type typ on typ.type_id = ra.type_id 
 left join dsg_rockwell_currency cur on cur.currency_id = ra.currency_id
 left join dsg_rockwell_vendor_dictionary ven on ra.docusafe_vendor_id = ven.id;


GO