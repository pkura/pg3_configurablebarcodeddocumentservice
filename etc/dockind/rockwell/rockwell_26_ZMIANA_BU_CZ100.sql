update DS_DIVISION set NAME = REPLACE(NAME,'CZ100','4009') where NAME like '%CZ100%'
update DS_DIVISION set code = REPLACE(code,'CZ100','4009') where code like '%CZ100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4009' where business_unit_name = 'CZ100';

update DS_DIVISION set NAME = REPLACE(NAME,'DE100','4014') where NAME like '%DE100%'
update DS_DIVISION set code = REPLACE(code,'DE100','4014') where code like '%DE100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4014' where business_unit_name = 'DE100';

update DS_DIVISION set NAME = REPLACE(NAME,'DE400','4017') where NAME like '%DE400%'
update DS_DIVISION set code = REPLACE(code,'DE400','4017') where code like '%DE400%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4017' where business_unit_name = 'DE400';

update DS_DIVISION set NAME = REPLACE(NAME,'SK100','4024') where NAME like '%SK100%'
update DS_DIVISION set code = REPLACE(code,'SK100','4024') where code like '%SK100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4024' where business_unit_name = 'SK100';

update DS_DIVISION set NAME = REPLACE(NAME,'CH100','4028') where NAME like '%CH100%'
update DS_DIVISION set code = REPLACE(code,'CH100','4028') where code like '%CH100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4028' where business_unit_name = 'CH100';

update DS_DIVISION set NAME = REPLACE(NAME,'GB100','4030') where NAME like '%GB100%'
update DS_DIVISION set code = REPLACE(code,'GB100','4030') where code like '%GB100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4030' where business_unit_name = 'GB100';

insert into DSG_ROCKWELL_CATEGORY values (64,'FINAL_REMINDER');
insert into DSG_ROCKWELL_TYPE values (34,'RO','Final Reminder');