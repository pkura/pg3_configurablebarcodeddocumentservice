create index ra_duplicate_1 on ds_document (duplicatedoc);

ALTER VIEW AP_REPORT AS 
SELECT     ra.document_id, doc.CTIME AS document_date, typ.type_code AS document_type, cat.category_name, ra.invoice_number, ven.name AS vendor_name, 
                      ven.vendor_id, bu.business_unit_name AS business_unit, ra.invoice_receive_date, ra.invoice_date, ra.gross_amount, ra.net_amount, ra.vat_amount, 
                      cur.currency_code AS currency, ra.document_status AS status, ra.barcode AS bar_code, ra.registr_no, ra.submitted_by AS created_by, 
                      ra.voucher_number, ra.eva_number, ra.receive_date AS eva_receive_date, ra.eva_finish_date, doc.finishUser AS process_finish_user, 
                      doc.finishDate AS process_finish_date, doc.DUPLICATEDOC AS duplicated_document
FROM         dbo.DSG_ROCKWELL AS ra INNER JOIN
                      dbo.DS_DOCUMENT AS doc ON ra.document_id = doc.ID INNER JOIN
                      dbo.DSG_ROCKWELL_CATEGORY AS cat ON ra.category_id = cat.category_id INNER JOIN
                      dbo.DSG_ROCKWELL_BUSINESS_UNIT AS bu ON bu.business_unit_id = ra.business_unit_id INNER JOIN
                      dbo.DSG_ROCKWELL_TYPE AS typ ON typ.type_id = ra.type_id LEFT OUTER JOIN
                      dbo.DSG_ROCKWELL_CURRENCY AS cur ON cur.currency_id = ra.currency_id LEFT OUTER JOIN
                      dbo.DSG_ROCKWELL_VENDOR_DICTIONARY AS ven ON ra.docusafe_vendor_id = ven.id