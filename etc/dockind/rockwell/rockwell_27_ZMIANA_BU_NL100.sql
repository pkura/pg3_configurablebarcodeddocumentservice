update DS_DIVISION set NAME = REPLACE(NAME,'NL100','4000') where NAME like '%NL100%'
update DS_DIVISION set code = REPLACE(code,'NL100','4000') where code like '%NL100%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4000' where business_unit_name = 'NL100';

update DS_DIVISION set NAME = REPLACE(NAME,'S30MCC','4022') where NAME like '%S30MCC%'
update DS_DIVISION set code = REPLACE(code,'S30MCC','4022') where code like '%S30MCC%'
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4022' where business_unit_name = 'S30MCC';