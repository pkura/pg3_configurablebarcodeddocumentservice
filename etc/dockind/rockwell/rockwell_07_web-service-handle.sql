--alter table dsg_rockwell add web_service_response int default -1;
--alter table dsg_rockwell add web_service_response_date datetime;

alter table dsg_rockwell add eva_url_response varchar(1000);
alter table dsg_rockwell add eva_url_response_date datetime;

create table dsg_rockwell_dict_web_service_response
(
 web_service_response int primary key,
 description varchar(100)
);

GO

insert into dsg_rockwell_dict_web_service_response values (-2, 'Odpowiedz bledna');
insert into dsg_rockwell_dict_web_service_response values (-1, 'Brak odpowiedzi');
insert into dsg_rockwell_dict_web_service_response values (0, 'OK');
insert into dsg_rockwell_dict_web_service_response values (1, 'B��d autoryzacji');
insert into dsg_rockwell_dict_web_service_response values (2, 'Dokument nieznany');
insert into dsg_rockwell_dict_web_service_response values (3, 'Zmiana statusu dokumentu nie jest mo�liwa');
insert into dsg_rockwell_dict_web_service_response values (4, 'Procedura nie zosta�a wykonana');