CREATE TABLE DSG_ROCKWELL 
(
    document_id 		numeric(19, 0) NOT NULL primary key,
    type_id 			integer,    
    category_id			integer,
    business_unit_id		integer,
    barcode				varchar (40),
    docusafe_vendor_id			integer,
    invoice_receive_date	datetime,
    invoice_date   		datetime,
    invoice_number		varchar(30),
    currency_id			integer,
    gross_amount		money,
    net_amount			money,
    vat_amount			money,
    po_number			varchar(30),
    voucher_number		varchar(30),
    GRNI_number			varchar(30),
    payslip_1			varchar(30),
    payslip_2			varchar(30),
    registr_no			varchar(30),
    document_status		integer,
    eva_status			integer default 0,
    eva_number			varchar(30),
    submitted_by		varchar(50),
    submit_date			datetime null,
    receive_date		datetime null,
    eva_finish_date		datetime null,
	url					varchar(50)
);


-- slownik vendorow

create table DSG_ROCKWELL_VENDOR_DICTIONARY
(
 id integer primary key identity,
 vendor_id integer,
 set_id varchar(10),
 short_name varchar(100),
 name varchar(100),
 location varchar(20),
 bank_account_number varchar(50),
 bank_id varchar(50),
 swift_id varchar(50),
 iban varchar(50),
 address varchar(50),
 city varchar(20)
);
-- Tabele słownikowe

create table DSG_ROCKWELL_TYPE
(
  type_id integer primary key,
  type_code varchar(2),
  type_name varchar(100)
);

create table DSG_ROCKWELL_CATEGORY
(
 category_id integer primary key,
 category_name varchar(100) not null
);


create table DSG_ROCKWELL_BUSINESS_UNIT
(
 business_unit_id integer primary key,
 business_unit_name varchar(100) not null
);

create table DSG_ROCKWELL_CURRENCY
(  
 currency_id integer primary key,
 currency_code varchar(10),
 currency_name varchar(100)
);

create index i_dsg_rockwell_1 on dsg_rockwell (type_id);
create index i_dsg_rockwell_2 on dsg_rockwell (category_id);
create index i_dsg_rockwell_3 on dsg_rockwell (docusafe_vendor_id);