alter table DSG_ROCKWELL add DUE_DATE datetime;

alter table EVA_VENDOR_LOAD_TABLE add payment_terms integer default 7;
alter table DSG_ROCKWELL_VENDOR_DICTIONARY add payment_terms integer default 7;

GO

update EVA_VENDOR_LOAD_TABLE set payment_terms = 7;
update DSG_ROCKWELL_VENDOR_DICTIONARY set payment_terms = 7;

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO
ALTER Procedure [dbo].[EVA_UPDATE_VENDOR_LIST]
@eva_number varchar(30)
as
 DECLARE @vendor_id varchar(50)
 DECLARE @set_id varchar(10)
 DECLARE @short_name varchar(100)
 DECLARE @name varchar(100)
 DECLARE @location varchar(20)
 DECLARE @bank_account_number varchar(50)
 DECLARE @bank_id varchar(50)
 DECLARE @swift_id varchar(50)
 DECLARE @iban varchar(50)
 DECLARE @address varchar(50)
 DECLARE @city varchar(20)
 DECLARE @PAYMENT_TERMS INTEGER

 DECLARE @DS_ID integer
 
 DECLARE @records_count INTEGER
 DECLARE @records_inserted INTEGER
 DECLARE @records_updated INTEGER

 DECLARE c0 CURSOR FORWARD_ONLY
  FOR 
   SELECT COUNT (*) AS ILE FROM EVA_VENDOR_LOAD_TABLE
 OPEN c0
 FETCH NEXT FROM c0 INTO @records_count
 CLOSE c0
 DEALLOCATE c0

 SELECT @records_inserted = 0
 SELECT @records_updated = 0
 
 INSERT INTO eva_vendor_load_log (event_date, event_code,records_count) values (getDate(), 'START', @records_count)
 DECLARE c1 CURSOR FORWARD_ONLY 
  FOR 
    SELECT VENDOR_ID, SET_ID, SHORT_NAME, NAME, LOCATION, BANK_ACCOUNT_NUMBER, BANK_ID, SWIFT_ID, IBAN, ADDRESS, CITY, PAYMENT_TERMS FROM EVA_VENDOR_LOAD_TABLE
  OPEN c1
  FETCH NEXT FROM c1 INTO @VENDOR_ID, @SET_ID, @SHORT_NAME, @NAME, @LOCATION, @BANK_ACCOUNT_NUMBER, @BANK_ID, @SWIFT_ID, @IBAN, @ADDRESS, @CITY, @PAYMENT_TERMS

  WHILE @@FETCH_STATUS = 0
   BEGIN
    
    DECLARE c2 CURSOR FORWARD_ONLY 
     FOR
      SELECT ID FROM DSG_ROCKWELL_VENDOR_DICTIONARY where VENDOR_ID = @VENDOR_ID and SET_ID = @SET_ID
     OPEN c2
     FETCH NEXT FROM c2 INTO @DS_ID
     IF @@FETCH_STATUS = 0
      BEGIN -- jest
       SELECT @records_updated = @records_updated + 1
       UPDATE DSG_ROCKWELL_VENDOR_DICTIONARY SET
         VENDOR_ID = @VENDOR_ID, SET_ID = @SET_ID, SHORT_NAME = @SHORT_NAME, NAME = @NAME, 
	     LOCATION = @LOCATION, BANK_ACCOUNT_NUMBER = @BANK_ACCOUNT_NUMBER, BANK_ID = @BANK_ID, 
	     SWIFT_ID = @SWIFT_ID, IBAN = @IBAN, ADDRESS = @ADDRESS, CITY = @CITY, PAYMENT_TERMS = @PAYMENT_TERMS
        WHERE ID = @DS_ID
      END
     ELSE
      BEGIN
       SELECT @records_inserted = @records_inserted + 1 
       INSERT INTO DSG_ROCKWELL_VENDOR_DICTIONARY 
          (VENDOR_ID, SET_ID, SHORT_NAME, NAME, LOCATION, BANK_ACCOUNT_NUMBER, BANK_ID, SWIFT_ID, IBAN, ADDRESS, CITY, PAYMENT_TERMS) 
	 VALUES
          (@VENDOR_ID, @SET_ID, @SHORT_NAME, @NAME, @LOCATION, @BANK_ACCOUNT_NUMBER, @BANK_ID, @SWIFT_ID, @IBAN, @ADDRESS, @CITY, @PAYMENT_TERMS)
      END
      DELETE FROM EVA_VENDOR_LOAD_TABLE WHERE VENDOR_ID = @VENDOR_ID and SET_ID = @SET_ID
      CLOSE c2
      DEALLOCATE c2
    FETCH NEXT FROM c1 INTO @VENDOR_ID, @SET_ID, @SHORT_NAME, @NAME, @LOCATION, @BANK_ACCOUNT_NUMBER, @BANK_ID, @SWIFT_ID, @IBAN, @ADDRESS, @CITY, @PAYMENT_TERMS
   END
   CLOSE c1
   DEALLOCATE c1
   
   DECLARE c00 CURSOR FORWARD_ONLY
    FOR 
     SELECT COUNT (*) AS ILE FROM EVA_VENDOR_LOAD_TABLE
    OPEN c00
   FETCH NEXT FROM c00 INTO @records_count
   CLOSE c00
   DEALLOCATE c00
 
   INSERT INTO eva_vendor_load_log (event_date, event_code,records_count,records_inserted,records_updated) values (getDate(), 'STOP', @records_count,@records_inserted,@records_updated)



-- zamienic recznie

SELECT     ra.document_id, doc.CTIME AS document_date, typ.type_code AS document_type, ra.invoice_number, ven.vendor_id, 
                      bu.business_unit_name AS business_unit, ra.invoice_receive_date, ra.invoice_date, ra.gross_amount, ra.net_amount, ra.vat_amount, 
                      cur.currency_code AS currency, ra.url, ra.barcode AS bar_code, ra.registr_no, ra.submitted_by AS created_by, 
                      ra.invoice_date + ra.due_date AS due_date
FROM         dbo.DSG_ROCKWELL AS ra INNER JOIN
                      dbo.DSG_ROCKWELL_CURRENCY AS cur ON ra.currency_id = cur.currency_id INNER JOIN
                      dbo.DSG_ROCKWELL_BUSINESS_UNIT AS bu ON ra.business_unit_id = bu.business_unit_id INNER JOIN
                      dbo.DSG_ROCKWELL_TYPE AS typ ON ra.type_id = typ.type_id INNER JOIN
                      dbo.DSG_ROCKWELL_VENDOR_DICTIONARY AS ven ON ra.docusafe_vendor_id = ven.id INNER JOIN
                      dbo.DS_DOCUMENT AS doc ON ra.document_id = doc.ID
WHERE     (ra.eva_status = 1)
