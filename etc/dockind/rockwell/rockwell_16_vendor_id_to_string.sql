CREATE TABLE DSG_ROCKWELL_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

create index dsg_ra_multi_1 on dsg_rockwell_multiple_value (document_id);
create index dsg_ra_multi_2 on dsg_rockwell_multiple_value (field_cn);
create index dsg_ra_multi_3 on dsg_rockwell_multiple_value (field_val);

insert into DSG_ROCKWELL_MULTIPLE_VALUE select document_id, 'PO_NUMBER', po_number from dsg_rockwell where po_number is not null;
insert into DSG_ROCKWELL_MULTIPLE_VALUE select document_id, 'GRNI_NUMBER', GRNI_number from dsg_rockwell where GRNI_number is not null;
