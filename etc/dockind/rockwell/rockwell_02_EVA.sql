create view eva_interface as 
select 
 ra.document_id as document_id, 
 doc.ctime as document_date,
 typ.type_code as document_type,
 ra.invoice_number as invoice_number,
 right('000000000'+rtrim(ltrim(str(ven.vendor_id))),10) as vendor_id,
 bu.business_unit_name as business_unit,
 ra.invoice_receive_date as invoice_receive_date,
 ra.invoice_date as invoice_date,
 ra.gross_amount as gross_amount,
 ra.net_amount as net_amount,
 ra.vat_amount as vat_amount,
 cur.currency_code as currency,
 ra.url as url,
 ra.barcode as bar_code,
 ra.registr_no as registr_no,
 ra.submitted_by as created_by 
 from dsg_rockwell ra, ds_document doc, dsg_rockwell_type typ, dsg_rockwell_business_unit bu, 
      dsg_rockwell_currency cur, dsg_rockwell_vendor_dictionary ven
   where cur.currency_id = ra.currency_id and bu.business_unit_id = ra.business_unit_id and typ.type_id = ra.type_id 
     and ra.docusafe_vendor_id = ven.id and doc.id = ra.document_id and ra.eva_status = 1;

GO

 CREATE Procedure eva_confirm_import
@document_id integer,
@eva_number varchar(30)
as
update dsg_rockwell set eva_status = 2, receive_date = getDate(), eva_number = @eva_number where document_id = @document_id

GO

 CREATE Procedure eva_confirm_process 
@document_id integer,
@voucher_number  varchar(30)
as
update dsg_rockwell set eva_status = 3, eva_finish_date = getDate(), voucher_number = @voucher_number where document_id = @document_id

GO

create view eva_control_view as 
select 
 ra.document_id as document_id, 
 doc.ctime as document_date,
 typ.type_code as document_type,
 ra.invoice_number as invoice_number,
 right('000000000'+rtrim(ltrim(ven.vendor_id)),10) as vendor_id,
 bu.business_unit_name as business_unit,
 ra.invoice_receive_date as invoice_receive_date,
 ra.invoice_date as invoice_date,
 ra.gross_amount as gross_amount,
 ra.net_amount as net_amount,
 ra.vat_amount as vat_amount,
 cur.currency_code as currency,
 ra.url as url,
 ra.barcode as bar_code,
 ra.registr_no as registr_no,
 ra.submitted_by as created_by,
 ra.voucher_number as voucher_number,
 ra.eva_status as eva_status,
 ra.eva_number as eva_number,
 ra.submitted_by as submitted_by,
 ra.submit_date as submit_date,
 ra.receive_date as receive_date,
 ra.eva_finish_date as eva_finish_date
 from dsg_rockwell ra, ds_document doc, dsg_rockwell_type typ, dsg_rockwell_business_unit bu, 
      dsg_rockwell_currency cur, dsg_rockwell_vendor_dictionary ven
   where cur.currency_id = ra.currency_id and bu.business_unit_id = ra.business_unit_id and typ.type_id = ra.type_id 
     and ra.docusafe_vendor_id = ven.id and doc.id = ra.document_id and ra.eva_status <> 0;

GO

create table EVA_VENDOR_LOAD_TABLE
(
 vendor_id integer,
 set_id varchar(10),
 short_name varchar(100),
 name varchar(100),
 location varchar(20),
 bank_account_number varchar(50),
 bank_id varchar(50),
 swift_id varchar(50),
 iban varchar(50),
 address varchar(50),
 city varchar(20)
);

GO

CREATE Procedure EVA_UPDATE_VENDOR_LIST

as

GO

create role eva_user;

grant select,insert,update,delete on eva_vendor_load_table to eva_user;
grant select on eva_interface to eva_user;
grant execute on eva_confirm_import to eva_user;
grant execute on eva_confirm_process  to eva_user;
grant select on eva_control_view to eva_user;
grant execute on EVA_UPDATE_VENDOR_LIST  to eva_user;