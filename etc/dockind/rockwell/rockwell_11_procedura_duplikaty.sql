create view duplicate_checklist
 as select doc.id as id, doc.dockind_id as dockind_id, doc.duplicatedoc as duplicatedoc,
  ra.invoice_number as invoice_number, ra.gross_amount as gross_amount, ra.invoice_date as invoice_date,
  vendors.vendor_id as vendor_id, vendors.set_id as set_id
 from ds_document doc, dsg_rockwell ra, dsg_rockwell_vendor_dictionary vendors where ra.document_id = doc.id and ra.type_id = 10
  and vendors.id = ra.docusafe_vendor_id

GO 

CREATE Procedure FIND_DUPLICS
as
 DECLARE @doc_id integer
 DECLARE @VENDOR_ID INTEGER
 DECLARE @SET_ID VARCHAR(10)
 DECLARE @INVOICE_NUMBER VARCHAR(30)
 DECLARE @INVOICE_DATE DATETIME
 DECLARE @GROSS_AMOUNT MONEY

 DECLARE @ORIGINAL_ID INTEGER


 DECLARE c1 CURSOR FORWARD_ONLY 
  FOR 
    SELECT ID, VENDOR_ID, SET_ID, INVOICE_NUMBER, INVOICE_DATE, GROSS_AMOUNT  FROM DUPLICATE_CHECKLIST where DOCKIND_ID = 3 AND DUPLICATEDOC IS NULL ORDER BY ID
  OPEN c1
  FETCH NEXT FROM c1 INTO @DOC_ID, @VENDOR_ID, @SET_ID, @INVOICE_NUMBER, @INVOICE_DATE, @GROSS_AMOUNT

  WHILE @@FETCH_STATUS = 0
   BEGIN
     DECLARE c2 CURSOR FORWARD_ONLY 
     FOR
      SELECT ID FROM DUPLICATE_CHECKLIST where ID < @DOC_ID AND DOCKIND_ID = 3 AND DUPLICATEDOC IS NULL 
        AND VENDOR_ID = @VENDOR_ID AND SET_ID = @SET_ID 
	AND INVOICE_NUMBER = @INVOICE_NUMBER AND INVOICE_DATE = @INVOICE_DATE AND GROSS_AMOUNT = @GROSS_AMOUNT
       ORDER BY ID
      OPEN c2

      
       FETCH NEXT FROM c2 INTO @ORIGINAL_ID
       WHILE @@FETCH_STATUS = 0
        BEGIN
	 update ds_document set duplicatedoc = @ORIGINAL_ID where id = @doc_id
	 FETCH NEXT FROM c2 INTO @ORIGINAL_ID
	END
      CLOSE c2
      DEALLOCATE c2

    FETCH NEXT FROM c1 INTO @DOC_ID, @VENDOR_ID, @SET_ID, @INVOICE_NUMBER, @INVOICE_DATE, @GROSS_AMOUNT
   END
   CLOSE c1
   DEALLOCATE c1
GO

select doc.id as id, doc.duplicatedoc as original_id,
  ra.invoice_number as invoice_number, ra.gross_amount as gross_amount, ra.invoice_date as invoice_date,
  vendors.vendor_id as vendor_id, vendors.set_id as set_id, bu.business_unit_name as bu
 from ds_document doc, dsg_rockwell ra, dsg_rockwell_vendor_dictionary vendors, dsg_rockwell_business_unit bu
where ra.document_id = doc.id and ra.type_id = 10 and bu.business_unit_id = ra.business_unit_id
  and vendors.id = ra.docusafe_vendor_id and doc.duplicatedoc > 0 order by bu