update DS_DIVISION set NAME = REPLACE(NAME,'DK100','4011') where NAME like '%DK100%'
update DS_DIVISION set NAME = REPLACE(NAME,'SE100','4027') where NAME like '%SE100%'
update DS_DIVISION set code = REPLACE(code,'DK100','4011') where code like '%DK100%'
update DS_DIVISION set code = REPLACE(code,'SE100','4027') where code like '%SE100%'

update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4011' where business_unit_name = 'DK100';
update DSG_ROCKWELL_BUSINESS_UNIT set business_unit_name = '4027' where business_unit_name = 'SE100';

