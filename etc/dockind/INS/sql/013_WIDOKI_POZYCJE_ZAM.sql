CREATE TABLE [dbo].[dsg_ins_costinvoice_zamowienia_pozycje_rozliczenie](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[ZAMDOST_ID] [numeric](18, 0) NULL,
	[POZYCJA] [numeric](18, 0) NULL,
	[NETTO] [numeric](18, 2) NULL,
	[STAWKA] [int] NULL,
	[VAT] [numeric](18, 2) NULL,
	[BRUTTO] [numeric](18, 2) NULL,
	[MPK] [numeric](18, 0) NULL,
	[KONTRAKT] [numeric](18, 0) NULL,
	[BUDZET] [numeric](18, 0) NULL,
	[FAZA] [numeric](18, 0) NULL,
	[ZASOB] [numeric](18, 0) NULL,
	[ZLECENIA] [numeric](18, 0) NULL,
	[PRZEZNACZENIE] [numeric](18, 0) NULL,
	[KOMORKAID] [numeric](18, 0) NULL,
	[BUDZETID] [numeric](18, 0) NULL,
	[POZYCJAID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_dsg_ins_costinvoice_zamowienia_pozycje_rozliczenie] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_dsg_ins_costinvoice_zamowienia_pozycje] ON [dbo].[dsg_ins_costinvoice_zamowienia_pozycje]
(
	[DOCUMENT_ID] ASC,
	[ZAMDOST_ID] ASC,
	[POZ_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO

CREATE TABLE [dbo].[dsg_ins_costinvoice_zamowienia_pozycje](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[ZAMDOST_ID] [numeric](18, 0) NULL,
	[POZ_ID] [numeric](18, 0) NULL,
	[KONTO] [numeric](18, 0) NULL,
	[PRODUKT] [varchar](100) NULL,
	[NAZWA] [varchar](255) NULL,
	[ILOSC_ZAMOWIONA] [int] NULL,
	[ILOSC_ZREALIZOWANA] [int] NULL,
	[WARTOSC] [numeric](18, 2) NULL,
	[STAWKA_VAT] [int] NULL,
	[WARTOSC_VAT] [numeric](18, 2) NULL,
	[WARTOSC_ZREALIZOWANA] [numeric](18, 2) NULL,
	[MPK_ID] [numeric](18, 0) NULL,
	[PROJEKT_ID] [numeric](18, 0) NULL,
	[BUDZET_PROJEKTU_ID] [numeric](18, 0) NULL,
	[ETAP_ID] [numeric](18, 0) NULL,
	[ZASOB_ID] [numeric](18, 0) NULL,
	[ZLECENIA_ID] [numeric](18, 0) NULL,
	[PRZEZNACZENIE_ID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_dsg_ins_costinvoice_zamowienia_pozycje] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--nazwa zamowienia
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdost_view]
AS
SELECT        z.zamdost_id AS ID, z.zamdost_id AS cn, z.dok_idm AS title, p.ID AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdost AS z LEFT OUTER JOIN
                         dbo.DSO_PERSON AS p ON p.externalID = z.dostawca_id
WHERE        (dok_idm IS NOT NULL)
GO
                      
--ilosc zamowiona    
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_ilosc_zam]
AS
SELECT        p.zamdospoz_id AS ID, p.zamdospoz_id AS cn, p.ilpotw AS title, z.zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdost AS z LEFT OUTER JOIN
                         TEST_AW.dbo.zamdostpoz AS p ON p.zamdost_id = z.zamdost_id
WHERE        (p.ilpotw IS NOT NULL)
GO

--ilosc zrealizowana
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_ilosc_zreal]
AS
SELECT        p.zamdospoz_id AS ID, p.zamdospoz_id AS cn, p.ilzreal AS title, z.zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdost AS z LEFT OUTER JOIN
                         TEST_AW.dbo.zamdostpoz AS p ON p.zamdost_id = z.zamdost_id
WHERE        (p.ilzreal IS NOT NULL)
GO
                         
--wartosc
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_wartosc]
AS
SELECT        zamdospoz_id AS ID, zamdospoz_id AS cn, cena AS title, zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM           TEST_AW.dbo.zamdostpoz 
GO

--stawka vat
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_stawka_vat]
AS
SELECT        v.vatstaw_id AS ID, v.vatstaw_ids AS CN, v.nazwa AS TITLE, CAST(d.vatstaw_id AS INT) AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS d LEFT OUTER JOIN
                         TEST_AW.dbo.vatstaw AS v ON v.vatstaw_id = d.vatstaw_id
WHERE        (v.nazwa IS NOT NULL)
GO
                         
--wartosc vat zrealizowanych
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_wartosc_vat]
AS
SELECT        zamdospoz_id AS ID, zamdospoz_id AS cn, kwotvat AS title, zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM         	TEST_AW.dbo.zamdostpoz 
GO

--projekt
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_projekt]
AS
SELECT        z.zamdospoz_id AS ID, z.zamdospoz_id AS cn, b.nazwa AS title, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS z LEFT OUTER JOIN
                         TEST_AW.dbo.bp_kontrakt AS b ON b.kontrakt_id = z.kontrakt_id
WHERE        (b.nazwa IS NOT NULL)
GO
                         
--budzet projektu
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_budzet_projektu]
AS
SELECT        z.zamdospoz_id AS ID, z.zamdospoz_id AS cn, b.nazwa AS title, b.kontrakt_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS z LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_projektu AS b ON b.projekt_id = z.projekt_id
WHERE        (b.nazwa IS NOT NULL)
GO
--etap                         
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_etap]
AS
SELECT        z.zamdospoz_id AS ID, z.zamdospoz_id AS cn, b.nazwa AS title, b.projekt_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS z LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_etap_projektu AS b ON b.etap_id = z.etap_id
WHERE        (b.nazwa IS NOT NULL)
GO

--zasob
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_zasob]
AS
SELECT        z.zamdospoz_id AS ID, z.zamdospoz_id AS cn, b.nazwa AS title, b.etap_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS z LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_etap_projektu_zasob AS b ON b.zasob_id = z.zasob_id
WHERE        (b.nazwa IS NOT NULL)
GO

--kwota netto
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_kwota]
AS
SELECT        zamdospoz_id AS ID, zamdospoz_id AS cn, kwotnett AS title, zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz
GO

--kwota vat
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_kwota_vat]
AS
SELECT        zamdospoz_id AS ID, zamdospoz_id AS cn, kwotvat AS title, zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz
GO

--kwota brutto
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_kwota_brutto]
AS
SELECT        zamdospoz_id AS ID, zamdospoz_id AS cn, kwotnett + kwotvat AS title, zamdost_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz
GO

--magazyn      
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_magazyn]
AS
SELECT        m.magazyn_id AS ID, m.magazyn_idn AS CN, m.nazwa AS TITLE, CAST(m.magazyn_id AS INT) AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.zamdostpoz AS d LEFT OUTER JOIN
                         TEST_AW.dbo.magazyn AS m ON m.magazyn_id = d.magazyn_id
WHERE        (m.magazyn_idn IS NOT NULL)
GO

--pozycje zamowienia
CREATE VIEW [dbo].[simple_erp_per_docusafe_zamdostpoz_view]
AS
SELECT     z.zamdospoz_id AS ID, z.zamdost_id AS ZAMOWIENIE_ID, dost.dok_idm AS NAZWA, z.ilpotw AS ILOSC_ZAMOWIONA, z.cenapotw AS WARTOSC, 
                      v.vatstaw_id AS STAWKA_VAT, z.kwotvat AS WARTOSC_VAT, z.ilzreal AS ILOSC_ZREALIZOWANA, RTRIM(w.wytwor_idm) + ' : ' + w.nazwa AS PRODUKT, 
                      z.kontrakt_id AS PROJEKT_ID, bp.projekt_id AS BUDZET_PROJEKTU_ID, e.etap_id, zas.zasob_id, zlec.zlecprod_id AS ZLECENIA_ID, NULL AS PRZEZNACZENIE_ID, 
                      kom.komorka_id AS MPK_ID
FROM            TEST_AW.dbo.zamdostpoz AS z LEFT OUTER JOIN
                         TEST_AW.dbo.zamdost AS dost ON z.zamdost_id = dost.zamdost_id LEFT OUTER JOIN
                         TEST_AW.dbo.vatstaw AS v ON v.vatstaw_id = z.vatstaw_id LEFT OUTER JOIN
                         TEST_AW.dbo.wytwor AS w ON w.wytwor_id = z.wytwor_id LEFT OUTER JOIN
                         TEST_AW.dbo.bp_kontrakt AS k ON k.kontrakt_id = z.kontrakt_id LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_projektu AS bp ON bp.projekt_id = z.projekt_id LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_etap_projektu AS e ON e.etap_id = z.etap_id LEFT OUTER JOIN
                         TEST_AW.dbo.bp_budzet_etap_projektu_zasob AS zas ON zas.zasob_id = z.zasob_id LEFT OUTER JOIN
                         TEST_AW.dbo.zlecprod AS zlec ON zlec.zlecprod_id = z.zlecprod_id LEFT OUTER JOIN
                         TEST_AW.dbo.komorka AS kom ON kom.komorka_id = z.komorka_id
GO                         
--grupy dysponent�w musz� si� nazywa� 'Dysponenci'
Create view dbo.COSTINVOICE_DYSPONENCI 
AS
select users.id,users.name as cn,users.firstname +' '+ users.lastname as title,null as refvalue,null as centrum,1 as available from DS_USER users
left join DS_USER_TO_DIVISION utd on utd.USER_ID=users.id
left join DS_DIVISION div on div.ID=utd.DIVISION_ID
where div.NAME='Dysponenci' and div.DIVISIONTYPE='group'
Go