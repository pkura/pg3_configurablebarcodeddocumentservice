UPDATE [dbo].[DSO_IN_DOCUMENT] SET delivery=NULL, outgoingdelivery=NULL

DELETE FROM [dbo].[dso_out_document_delivery]

INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (1, N'Poste restante')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (2, N'Osobi�cie')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (3, N'Krajowa ekonomiczna')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (4, N'Krajowa priorytetowa')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (5, N'Zagraniczna ekonomiczna')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (6, N'Zagraniczna priorytetowa')
INSERT [dbo].[dso_out_document_delivery] ([POSN], [NAME]) VALUES (7, N'Za pobraniem')

DELETE FROM [dbo].[dso_in_document_delivery]

INSERT [dbo].[dso_in_document_delivery] ([POSN], [NAME]) VALUES (1, N'Zwyk�a')
INSERT [dbo].[dso_in_document_delivery] ([POSN], [NAME]) VALUES (2, N'Polecona')
INSERT [dbo].[dso_in_document_delivery] ([POSN], [NAME]) VALUES (3, N'Inna')