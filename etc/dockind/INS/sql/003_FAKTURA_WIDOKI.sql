--typy dokumentów zakupu
create view simple_erp_per_docusafe_typdokzak_wal_view
as
select
	typdokzak_id  as id,
	typdok_idn as cn,
	nazwa as title,
	'0' AS CENTRUM,
	null AS refValue,
	'1' as available
from INS_TEST_DSF.dbo.typ_dokumentu_zakupu
where czy_aktywny = 1
and czywal = 1;

-- dokumenty pln
create view simple_erp_per_docusafe_typdokzak_pln_view
as
select
	typdokzak_id  as id,
	typdok_idn as cn,
	nazwa as title,
	'0' AS CENTRUM,
	null AS refValue,
	'1' as available
from INS_TEST_DSF.dbo.typ_dokumentu_zakupu
where czy_aktywny = 1
and czywal = 0;

-- typ platnosci
create view simple_erp_per_docusafe_warpla_view
as
	select DISTINCT warplat_id as id,
	warplat_idn   as cn,
	warplat_idn as title,
	'0' AS CENTRUM,
	null AS refValue,
	'1' as available
	from  INS_TEST_DSF.dbo.warplat
	WHERE warplat_id in (1, 13, 15, 9, 16);

-- columna DSG_STAWKI_VAT_FAKTURY.pozycja
create view simple_erp_per_docusafe_wytwor_view
as
select
	wytwor_id as id,
	wytwor_idm as cn,
	RTRIM(wytwor_idm) + ' : ' + nazwa  as title,
	'0' AS CENTRUM,
	null AS refValue,
	'1' as available
from INS_TEST_DSF.dbo.wytwor
where wytwor_idm like '4%';

-- columna DSG_STAWKI_VAT_FAKTURY.stawka
create view simple_erp_per_docusafe_vatstaw_view
as
select
	vatstaw_id as id,
 	procent as cn,
	vatstaw_ids as title,
	procent as refValue,
	'0' AS CENTRUM,
	'1' as available
	from INS_TEST_DSF.dbo.vatstaw
where data_obow_do is null or convert ( date , data_obow_do ) >= CONVERT ( date , getdate () );

-- columna WALUTA
create view simple_erp_per_docusafe_waluta_view
as
select
	waluta_id as id,
 	symbolwaluty as cn,
	symbolwaluty as title,
	null as refValue,
	'0' AS CENTRUM,
	'1' as available
from INS_TEST_DSF.dbo.waluta;

alter table DSG_STAWKI_VAT_FAKTURY add komorka numeric (18,0);
-- columna dsg_stawki_vat.komorka
create view simple_erp_per_docusafe_komorka_view
as
select
	komorka_id as id,
 	komorka_idk as cn,
	nazwa as title,
	null as refValue,
	'0' AS CENTRUM,
	'1' as available
from INS_TEST_DSF.dbo.komorka;

alter table DSG_STAWKI_VAT_FAKTURY add zlecenie numeric (18,0);
-- columna dsg_stawki_vat.zlecenie
create view simple_erp_per_docusafe_zlecprod_view
as
select
	zlecprod_id as id,
 	zlecprod_idm as cn,
	nazwa as title,
	null as refValue,
	'0' AS CENTRUM,
	'1' as available
from INS_TEST_DSF.dbo.zlecprod;

-- kontrhenci
--select dostawca_id , dostawca_idk , dostawca_idk , kontrahent.nazwa  , kodpoczt , miasto , ulica , nrdomu, nrmieszk ,  kraj_idn , nip
--from dostawca join kontrahent on dostawca.kontrahent_id = kontrahent.kontrahent_id
--join kraj on kontrahent.kraj_rejpod_id = kraj.kraj_id

CREATE VIEW [dbo].[simple_erp_per_docusafe_kontrahenci_view]
AS
SELECT
    'PERSON' AS DISCRIMINATOR,
    'ERP' AS lparam,
    INS_TEST_DSF.dbo.dostawca.dostawca_id AS externalID,
    INS_TEST_DSF.dbo.kontrahent.nazwa AS organization,
    INS_TEST_DSF.dbo.kontrahent.kodpoczt AS zip,
    INS_TEST_DSF.dbo.kontrahent.miasto AS location,
    CASE
        WHEN nrdomu IS NOT NULL AND nrmieszk IS NOT NULL
            THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu) + '/' + RTRIM(nrmieszk)
        WHEN nrdomu IS NOT NULL AND nrmieszk IS NULL
            THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu)
        WHEN nrdomu IS NULL AND nrmieszk IS NULL
            THEN ulica
    END AS street,
    RTRIM(INS_TEST_DSF.dbo.kraj.kraj_idn) AS country,
    CASE
        WHEN nip IS NOT NULL
            THEN REPLACE(nip, '-', '')
        ELSE nip
    END AS nip
FROM INS_TEST_DSF.dbo.dostawca
INNER JOIN INS_TEST_DSF.dbo.kontrahent ON INS_TEST_DSF.dbo.dostawca.kontrahent_id = INS_TEST_DSF.dbo.kontrahent.kontrahent_id
INNER JOIN INS_TEST_DSF.dbo.kraj ON INS_TEST_DSF.dbo.kontrahent.kraj_rejpod_id = INS_TEST_DSF.dbo.kraj.kraj_id

-- warunki platnosci
--select warplat_id , warplat_idn!!!   from warplat where warplat_id in (19 , 22 , 24  )
-- order by 2

-- wskaznik
-- select wskaznik_id ,  rok , wartosc from sys_vat_wskaznik