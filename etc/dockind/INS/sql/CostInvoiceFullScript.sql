--CostInvoice
CREATE TABLE [dbo].[dsg_ins_costinvoice](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wystawienia] [date] NULL,
	[termin_platnosci] [date] NULL,
	[netto] [numeric](18, 2) NULL,
	[vat] [numeric](18, 2) NULL,
	[brutto] [numeric](18, 2) NULL,
	[KW_POZ_NETTO] [numeric](18, 2) NULL,
	[KW_POZ_VAT] [numeric](18, 2) NULL,
	[KW_POZ_BRUTTO] [numeric](18, 2) NULL,
	[sposob] [numeric](18, 0) NULL,
	[dzialalnosc] [numeric](18, 0) NULL,
	[opis] [varchar](4096) NULL,
	[addinfo] [varchar](4096) NULL,
	[dzial] [varchar](50) NULL,
	[data_wplywu] [date] NULL,
	[waluta] [numeric](18, 0) NULL,
	[kwotaWwalucie] [numeric](18, 2) NULL,
	[kurs] [numeric](18, 4) NULL,
	[forma_rozliczenia] [varchar](4096) NULL,
	[platnosc_szczegolna] [bit] NULL,
	[umowa_nie_w_systemie] [bit] NULL,
	[nr_umowy_przetargowej] [varchar](150) NULL,
	[nr_wniosku] [varchar](50) NULL,
	[typ_faktury_wal] [numeric](18, 0) NULL,
	[typ_faktury_pln] [numeric](18, 0) NULL,
	[contract] [numeric](18, 0) NULL,
	[nr_umowy_rk] [varchar](50) NULL,
	[MIEJSCE_UZYTKOWANIA] [varchar](250) NULL,
	[srodek_trwaly] [numeric](18, 0) NULL,
	[decyzja_dzp] [numeric](18, 0) NULL,
	[termin_platnosci_z_umowy] [date] NULL,
	[msg_library] [bit] NULL,
	[nr_biblioteka] [varchar](50) NULL,
	[korekta] [bit] NULL,
	[korekta_faktura] [numeric](18, 0) NULL,
	[dodatkowy_dzial] [numeric](18, 0) NULL,
	[sekcja_dekretacji] [numeric](18, 0) NULL,
	[wybrany_dzial] [numeric](18, 0) NULL,
	[faktura_skan] [numeric](18, 0) NULL,
	[ostatnia_faktura_do_umowy] [numeric](18, 0) NULL,
	[bd_rezerwacja_id] [int] NULL,
	[STANOWISKO_DOSTAW] [numeric](18, 0) NULL,
	[BUDZETOWANIE_PROJEKTU] [bit] NULL,
	[PRACOWNIK_DELEGACJA] [numeric](18, 0) NULL,
	[ZLECENIA] [numeric](18, 0) NULL,
	[AKCEPTANT_MERYT] [numeric](18, 0) NULL,
	[DATA_AKCEPTACJI_MERYT] [date] NULL,
	[STATUS_AKCEPTACJI_MERYT] [numeric](18, 0) NULL,
	[AKCEPTANT_FORM_RACH] [numeric](18, 0) NULL,
	[DATA_AKCEPTACJI_FORM_RACH] [date] NULL,
	[STATUS_AKCEPTACJI_FORM_RACH] [numeric](18, 0) NULL,
	[AKCEPTANT_FORM_RACH2] [numeric](18, 0) NULL,
	[DATA_AKCEPTACJI_FORM_RACH2] [date] NULL,
	[STATUS_AKCEPTACJI_FORM_RACH2] [numeric](18, 0) NULL,
	[AKCEPTANT_ZAPLATY] [numeric](18, 0) NULL,
	[DATA_AKCEPTACJI_ZAPLATY] [date] NULL,
	[STATUS_AKCEPTACJI_ZAPLATY] [numeric](18, 0) NULL,
	[AKCEPTANT_ZAPLATY2] [numeric](18, 0) NULL,
	[DATA_AKCEPTACJI_ZAPLATY2] [date] NULL,
	[STATUS_AKCEPTACJI_ZAPLATY2] [numeric](18, 0) NULL
) ON [PRIMARY]


--TABELA MULTI
CREATE TABLE [dbo].[dsg_ins_costinvoice_multiple_value](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

--POZYCJE FAKTURY
CREATE TABLE [dbo].[DSG_STAWKI_VAT_FAKTURY](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[POZYCJA] [numeric](18, 0) NULL,
	[NETTO] [numeric](18, 2) NULL,
	[STAWKA] [numeric](18, 0) NULL,
	[KWOTA_VAT] [numeric](18, 2) NULL,
	[BRUTTO] [numeric](18, 2) NULL,
	[POZYCJAID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DSG_STAWKI_VAT_FAKTURY] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

--POZYCJE BUDZETOWE
CREATE TABLE [dbo].[DSG_COSTINVOICE_MPK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
    	[POZYCJA] [numeric](18, 0) NULL,
    	[NETTO] [numeric](18, 2) NULL,
    	[STAWKA] [numeric](18, 0) NULL,
    	[VAT] [numeric](18, 2) NULL,
    	[BRUTTO] [numeric](18, 2) NULL,
    	[MPK] [numeric](18, 0) NULL,
    	[KONTRAKT] [numeric](18, 0) NULL,
    	[BUDZET] [numeric](18, 0) NULL,
    	[FAZA] [numeric](18, 0) NULL,
    	[ZASOB] [numeric](18, 0) NULL,
    	[PRZEZNACZENIE] [numeric](18, 0) NULL,
    	[ZLECENIA] [numeric](18, 0) NULL,
    	[POZID] [numeric](18, 0) NULL,
    	[KOMORKAID] [numeric](18, 0) NULL,
    	[BUDZETID] [numeric](18, 0) NULL,
    	[POZYCJAID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_DSG_COSTINVOICE_MPK] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



--------------------------------------WIDOKI----------------------------------------------------



--Faktury wyeksportowane do ERP / do korekt
CREATE VIEW [dbo].[v_invoces_exported_to_erp]
AS
SELECT        DOCUMENT_ID, nr_faktury, data_wystawienia, termin_platnosci, brutto, data_wplywu, nr_wniosku
FROM            dbo.dsg_ins_costinvoice
WHERE        (nr_wniosku IS NOT NULL)

--WIDOK TYP�W DOKUMENT�W ZAKUPU WALUTOWYCH
CREATE VIEW [dbo].[simple_erp_per_docusafe_typdokzak_wal_view]
AS
SELECT        typdokzak_id AS id, typdok_idn AS cn, nazwa AS title, '0' AS CENTRUM, NULL AS refValue, '1' AS available
FROM            TEST_AW.dbo.typ_dokumentu_zakupu
WHERE        (czy_aktywny = 1) AND (czywal = 1)

--WIDOK TYP�W DOKUMENT�W ZAKUPU PLN
CREATE VIEW [dbo].[simple_erp_per_docusafe_typdokzak_pln_view]
AS
SELECT        typdokzak_id AS id, typdok_idn AS cn, nazwa AS title, '0' AS CENTRUM, NULL AS refValue, '1' AS available
FROM            TEST_AW.dbo.typ_dokumentu_zakupu
WHERE        (czy_aktywny = 1) AND (czywal = 0)

--WIDOK PRODUKT�W DO FAKTURY
CREATE VIEW [dbo].[simple_erp_per_docusafe_wytwor_view]
AS
SELECT        wytwor_id AS id, wytwor_idm AS cn, RTRIM(wytwor_idm) + ' : ' + nazwa AS title, '0' AS CENTRUM, NULL AS refValue, '1' AS available
FROM            TEST_AW.dbo.wytwor
WHERE        (wytwor_idm LIKE '4%')

--WIDOK WARUNK�W P�ATNO�CI
select DISTINCT a.warplat_id AS id, warplat_idn AS cn, warplat_idn AS title, '0' AS CENTRUM, NULL AS refValue, '1' AS available
,liczdni as liczdni
from TEST_AW.dbo.warplat a join TEST_AW..warplatpoz b on a.warplat_id = b.warplat_id and zakupsprzedaz = 0

--WIDOK STANOWISK DOSTAWY
CREATE VIEW [dbo].[simple_erp_per_docusafe_stdost_view]
AS
SELECT        stdost_id AS id, stdost_idn AS cn, RTRIM(stdost_ids) + ' : ' + nazwa AS title, '0' AS CENTRUM, NULL AS refValue, czy_aktywna AS available
FROM            TEST_AW.dbo.stdost

--WIDOK PRZEZNACZE� ZAKUPU
CREATE VIEW [dbo].[simple_erp_per_docusafe_przeznzak_view]
AS
SELECT        rodzaj AS id, rodz_idn AS cn, nazwa AS title, 0 AS CENTRUM, NULL AS refValue, 1 AS available
FROM            TEST_AW.dbo.vprzeznaczenie_zakupu

--WIDOK KONTRAHENT�W/WYSTAWC�W FAKTUR
CREATE VIEW [dbo].[simple_erp_per_docusafe_kontrahenci_view]
AS
SELECT        'PERSON' AS DISCRIMINATOR, 'ERP' AS lparam, TEST_AW.dbo.dostawca.dostawca_id AS externalID, TEST_AW.dbo.kontrahent.nazwa AS organization, 
                         TEST_AW.dbo.kontrahent.kodpoczt AS zip, TEST_AW.dbo.kontrahent.miasto AS location, CASE WHEN nrdomu IS NOT NULL AND nrmieszk IS NOT NULL 
                         THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu) + '/' + RTRIM(nrmieszk) WHEN nrdomu IS NOT NULL AND nrmieszk IS NULL THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu) 
                         WHEN nrdomu IS NULL AND nrmieszk IS NULL THEN ulica END AS street, RTRIM(TEST_AW.dbo.kraj.kraj_idn) AS country, CASE WHEN nip IS NOT NULL 
                         THEN REPLACE(nip, '-', '') ELSE nip END AS nip
FROM            TEST_AW.dbo.dostawca INNER JOIN
                         TEST_AW.dbo.kontrahent ON TEST_AW.dbo.dostawca.kontrahent_id = TEST_AW.dbo.kontrahent.kontrahent_id INNER JOIN
                         TEST_AW.dbo.kraj ON TEST_AW.dbo.kontrahent.kraj_rejpod_id = TEST_AW.dbo.kraj.kraj_id


--WIDOK KOM�REK BUD�ETOWYCH
CREATE VIEW [dbo].[simple_erp_per_docusafe_bd_str_budzet_view]
AS
SELECT DISTINCT 
                         kom_bud_id AS ID, REPLACE(RTRIM(kom_bud_idn), ' ', '-') + CAST(kom_bud_id AS varchar) AS cn, kom_bud_nazwa AS TITLE, NULL AS refValue, 0 AS CENTRUM, 
                         1 AS available
FROM            TEST_AW.dbo.v_bd_budzetowanie

--WIDOK BUD�ET�W
CREATE VIEW [dbo].[simple_erp_per_docusafe_bd_budzety_view]
AS
SELECT DISTINCT bd_budzet_id AS ID, bd_budzet_id AS cn, bd_budzet_idm AS title, kom_bud_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.v_bd_budzetowanie
WHERE        (rodzaj_id = 1)

--WIDOK POZYCJI BUD�ET�W
CREATE VIEW [dbo].[simple_erp_per_docusafe_bd_pozycje_view]
AS
SELECT DISTINCT bd_poz_budzet_id AS ID, bd_poz_budzet_id AS cn, bd_poz_budzet_nazwa AS title, bd_budzet_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.v_bd_budzetowanie
WHERE        (bd_poz_budzet_id IS NOT NULL)

--WIDOK POZYCJI BUD�ETOWYCH
CREATE VIEW [dbo].[pozycje_budzetowe_doc_view]
AS
SELECT        mpk.id, CAST(mpk.id AS varchar(50)) AS cn, CAST(mpk.id AS varchar(255)) AS title, 0 AS centrum, CAST(mul.DOCUMENT_ID AS varchar(20)) AS refValue, 
                         1 AS available
FROM            dbo.dsg_ins_costinvoice_multiple_value AS mul INNER JOIN
                         dbo.DSG_COSTINVOICE_MPK AS mpk ON mul.FIELD_CN = 'MPK' AND mpk.id = mul.FIELD_VAL




--WIDOK OSOB DO AKCEPTACJI MERYTORYCZNEJ
CREATE VIEW [dbo].[AKC_MERYT_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO

--WIDOK OSOB DO AKCEPTACJI FORMALNO-RACHUNKOWEJ
CREATE VIEW [dbo].[AKC_FORMRACH_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO

--WIDOK OS�B DO AKCEPTACJI FORMALNO-RACHUNKOWEJ II
CREATE VIEW [dbo].[AKC_FORMRACH2_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER

--WIDOK OS�B DO AKCEPTACJI ZAP�ATY WST�PNEJ
CREATE VIEW [dbo].[AKC_ZAPLWSTEPNA_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO

--WIDOK OS�B DO AKCEPTACJI ZAP�ATY OSTATECZNEJ
CREATE VIEW [dbo].[AKC_ZAPLFINAL_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO
