CREATE VIEW [dbo].[v_erp_users_to_divs]
AS
SELECT        uzytk_idn, imie, nazwisko, ID_KomPod
FROM            INS_TEST_DSF.dbo.uv_gsd_docusafe_strorg
WHERE        (uzytk_idn IS NOT NULL) AND (imie IS NOT NULL) AND (nazwisko IS NOT NULL)

CREATE VIEW [dbo].[v_erp_org_structure]
AS
SELECT        ID_KomPod, IDK_KomPod, Nazwa_KomPoda, ID_KomNad, IDK_KomNad, Nazwa_KomNad
FROM            INS_TEST_DSF.dbo.uv_gsd_docusafe_strorg
