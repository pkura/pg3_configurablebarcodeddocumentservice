CREATE VIEW [dbo].[AKC_MERYT_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO



CREATE VIEW [dbo].[AKC_FORMRACH_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO


CREATE VIEW [dbo].[AKC_FORMRACH2_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER


CREATE VIEW [dbo].[AKC_ZAPLWSTEPNA_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO


CREATE VIEW [dbo].[AKC_ZAPLFINAL_DICT]
AS
SELECT        ID, ID AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            dbo.DS_USER
GO