--WniosekZapDost
CREATE TABLE [dbo].[INS_WniosekZapDoDost](
	[document_id] [numeric](19, 0) NULL,
	[odzial] [numeric](19, 0) NULL,
	[TYP] [numeric](19, 0) NULL,
	[komorka] [numeric](19, 0) NULL,
	[status] [numeric](2, 0) NULL,
	[PRACOWNIK] [numeric](19, 0) NULL,
	[nrTel] [varchar](15) NULL,
	[dataDokumentu] [date] NULL,
	[DATAZAPOTRZEBOWANIA] [date] NULL,
	[WALUTA] [numeric](19, 0) NULL,
	[KURS] [numeric](18, 4) NULL,
	[suma] [numeric](19, 4) NULL,
	[NrERP] [varchar](512) NULL,
	[zglaszajacy] [numeric](19, 0) NULL,
	[akceptujacy] [numeric](19, 0) NULL,
	[zatwierdzajacy] [numeric](19, 0) NULL,
	[przyjmujacy] [numeric](19, 0) NULL,
	[realizujacy] [numeric](19, 0) NULL,
	[dataZgloszenia] [date] NULL,
	[dataAkceptacji] [date] NULL,
	[dataZatwierdzenia] [date] NULL,
	[dataPrzyjeciaERP] [date] NULL,
	[dataRealizacji] [date] NULL,
	[ERPID] [numeric](18, 0) NULL,
	[NETTO] [numeric](18, 4) NULL,
	[VAT] [numeric](18, 4) NULL,
	[BRUTTO] [numeric](18, 4) NULL,
	[ZAMOWIONYTOWAR] [numeric](18, 0) NULL,
	[KOSZTY_WYDZIAL] [int] NULL
) ON [PRIMARY]

--TABELA MULTI
CREATE TABLE [dbo].[INS_WniosZapDost_Dict](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[FIELD_CN] [varchar](50) NULL,
	[FIELD_VAL] [varchar](50) NULL
) ON [PRIMARY]

--TABELA POZYCJI WNIOSKU
CREATE TABLE [dbo].[INS_WnioZapDoDost_Pozycje](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LP] [varchar](20) NULL,
	[POZYCJA_BUDZETU] [numeric](19, 0) NULL,
	[PRODUKT] [numeric](19, 0) NULL,
	[OPIS] [varchar](240) NULL,
	[ILOSC] [numeric](19, 3) NULL,
	[JM] [numeric](19, 0) NULL,
	[CENA] [numeric](19, 2) NULL,
	[CENA_BAZ] [numeric](19, 2) NULL,
	[NETTO] [numeric](19, 2) NULL,
	[NETTO_BAZ] [numeric](19, 2) NULL,
	[VAT] [numeric](19, 0) NULL,
	[DATAOCZEKIWANA] [date] NULL,
	[IND] [numeric](18, 0) NULL
) ON [PRIMARY]


--TABELA POZYCJI BUD�ETOWYCH
CREATE TABLE [dbo].[INS_BUDZETPOZ](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ZLECENIE] [varchar](19) NULL,
	[KONTRAKT] [varchar](19) NULL,
	[ZASOB] [varchar](19) NULL,
	[BUDZET] [varchar](19) NULL,
	[FAZA] [varchar](19) NULL,
	[ZLECENIA] [varchar](19) NULL
) ON [PRIMARY]

--TABELA WIDOCZNA DLA DYREKTORA
CREATE TABLE [dbo].[INS_WnioZapDoDost_ZAMOWIONY_TOWAR](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[PRODUKT] [numeric](19, 0) NULL,
	[ILOSC] [numeric](19, 3) NULL,
	[BRUTTO] [numeric](19, 2) NULL,
	[DOTYCZY] [varchar](1000) NULL,
	[IND] [numeric](19, 0) NULL,
	[UWAGI] [varchar](1000) NULL,
 CONSTRAINT [PK_INS_WnioZapDoDost_ZAMOWIONY_TOWAR] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]





------------------------------------------------------WIDOKI-----------------------------------
--REALIZACJA POZYCJI
CREATE VIEW [dbo].[zapdostpoz_v]
AS
SELECT        a.zapdostpoz_id AS ID, a.ilosc, a.ilzreal, a.ildost, a.ildost_rozlicz AS ildostrozlicz, a.zapdost_id, RTRIM(b.wytwor_idm) + ': ' + b.nazwa AS PRODUKT, 
                         CAST(a.uwagi AS varchar(255)) AS uwagi
FROM            TEST_AW.dbo.zapdostpoz AS a LEFT OUTER JOIN
                         TEST_AW.dbo.wytwor AS b ON a.wytwor_id = b.wytwor_id

--WIDOK INDEKSOW ZAWEZAJACYCH PRODUKTY
CREATE VIEW [dbo].[simple_erp_per_docusafe_indexy_zapdost_view]
AS
SELECT DISTINCT w.klaswytw_id AS ID, w.klaswytw_id AS CN, kw.nazwa AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' AS available
FROM            TEST_AW.dbo.wytwor AS w LEFT OUTER JOIN
                         TEST_AW.dbo.klaswytw AS kw ON w.klaswytw_id = kw.klaswytw_id
WHERE        (TEST_AW.dbo.wytwor.klaswytw_id <> 5)


--WIDOK PRODUKT�W DO WNIOSKU ZAPOTRZEBOWANIA
CREATE VIEW [dbo].[simple_erp_per_docusafe_wytwory_zapdost_view]
AS
SELECT        TEST_AW.dbo.wytwor.wytwor_id AS id, TEST_AW.dbo.wytwor.wytwor_idm AS cn, RTRIM(TEST_AW.dbo.wytwor.wytwor_idm) 
                         + ' : ' + TEST_AW.dbo.wytwor.nazwa AS title, '0' AS CENTRUM, TEST_AW.dbo.wytwor.klaswytw_id AS refValue, '1' AS available, vatview.id AS vat_id,
                         TEST_AW.dbo.wytwor.jmbazowa
FROM            TEST_AW.dbo.wytwor LEFT OUTER JOIN
                         dbo.simple_erp_per_docusafe_vatstaw_view AS vatview ON vatview.id = TEST_AW.dbo.wytwor.vatstaw_id
WHERE        (TEST_AW.dbo.wytwor.klaswytw_id <> 5)

--WIDOK TYP�W ZAPOTRZEBOWANIA DOSTAWY
CREATE VIEW [dbo].[simple_erp_per_docusafe_typ_zapotrz_dost]
AS
SELECT        typ_zapdost_id AS ID, typdok_idn AS CN, nazwa AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available, waluta_id, czywal
FROM            TEST_AW.dbo.typ_zapotrzebowania_dostawy
WHERE        (odmiana = 0) AND (rodzaj = 0)

--WIDOK ODDZIA��W
CREATE VIEW [dbo].[simple_erp_per_docusafe_oddzialy_zapdost]
AS
SELECT        komorka_id AS ID, komorka_ids AS CN, komorka_idn AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.komorka
WHERE        (oddzial_id = komorka_id)

--WIDOK KOM�REK KOSZTOWYCH
CREATE VIEW [dbo].[simple_erp_per_docusafe_komorki_zapdost]
AS
SELECT        komorka_id AS ID, komorka_ids AS CN, nazwa AS TITLE, oddzial_id AS refValue, 0 AS CENTRUM, czy_aktywna AS available
FROM            TEST_AW.dbo.komorka
WHERE        (czy_kosztowa = 1)

--WIDOK JEDNOSTEK MIAR
CREATE VIEW [dbo].[simple_erp_per_docusafe_jm_view]
AS
SELECT        jm_id AS ID, jm_idn AS CN, nazwa AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available, precyzjajm
FROM            TEST_AW.dbo.jm

--WIDOK NIEZMAPOWANYCH POZYCJI WNIOSKU ZAPOTRZEBOWANIA
CREATE VIEW [dbo].[INS_NIEZMAPOWANE_POZYCJE]
AS
SELECT        document_id
FROM            dbo.INS_WniosekZapDoDost AS a
WHERE        (status = 4) AND (document_id NOT IN
                             (SELECT        DOCUMENT_ID
                               FROM            dbo.INS_WniosZapDost_Dict AS b
                               WHERE        (FIELD_CN = 'REALIZACJAPOZYCJI'))) AND (document_id IN
                             (SELECT        DOCUMENT_ID
                               FROM            dbo.INS_WniosZapDost_Dict AS b
                               WHERE        (FIELD_CN = 'ZAMOWIENIAPOZ')))

--WIDOK BUDZETOWYCH POZYCJI WNIOSKU ZAPOTRZEBOWANIA
CREATE VIEW [dbo].[INS_BUDZETPOZ_view]
AS
SELECT        poz.ID, CAST(poz.ID AS varchar(50)) AS cn, CAST(poz.ID AS varchar(255)) AS title, 0 AS centrum, CAST(dic.DOCUMENT_ID AS varchar(20)) AS refValue, 
                         1 AS available
FROM            dbo.INS_WniosZapDost_Dict AS dic INNER JOIN
                         dbo.INS_BUDZETPOZ AS poz ON dic.FIELD_CN = 'BUDZET_POZ' AND poz.ID = dic.FIELD_VAL
GO

