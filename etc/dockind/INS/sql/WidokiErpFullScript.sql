--WIDOKI ERP'owe



--WIDOK UZYTKOWNIKOW PRZYPISANYCH DO STRUKTURY ORG
CREATE VIEW [dbo].[v_erp_users_to_divs]
AS
SELECT        uzytk_idn, imie, nazwisko, ID_KomPod
FROM            TEST_AW.dbo.uv_gsd_docusafe_strorg
WHERE        (uzytk_idn IS NOT NULL) AND (imie IS NOT NULL) AND (nazwisko IS NOT NULL)

--WIDOK STRUKTURY ORGANIZACYJNEJ
CREATE VIEW [dbo].[v_erp_org_structure]
AS
SELECT        ID_KomPod, IDK_KomPod, Nazwa_KomPoda, ID_KomNad, IDK_KomNad, Nazwa_KomNad, czy_aktywna
FROM            TEST_AW.dbo.uv_gsd_docusafe_strorg

--WIDOK WALUT
CREATE VIEW [dbo].[simple_erp_per_docusafe_waluta_view]
AS
SELECT        waluta_id AS id, symbolwaluty AS cn, symbolwaluty AS title, NULL AS refValue, '0' AS CENTRUM, '1' AS available
FROM            TEST_AW.dbo.waluta

--WIDOK STAWEK VAT
CREATE VIEW [dbo].[simple_erp_per_docusafe_vatstaw_view]
AS
SELECT        vatstaw_id AS id, procent AS cn, vatstaw_ids AS title, procent AS refValue, '0' AS CENTRUM, '1' AS available
FROM            TEST_AW.dbo.vatstaw
WHERE        (data_obow_do IS NULL) OR
                         (CONVERT(date, data_obow_do) >= CONVERT(date, GETDATE()))

--WIDOK PROJEKT�W
CREATE VIEW [dbo].[simple_erp_per_docusafe_projekty]
AS
SELECT        kontrakt_id AS id, kontrakt_id AS cn, nazwa AS title, NULL AS refValue, 0 AS CENTRUM, '1' AS available
FROM            TEST_AW.dbo.bp_kontrakt
WHERE        (status = 0) AND (stan = 1)

--WIDOK BUD�ET�W PROJEKT�W
CREATE VIEW [dbo].[simple_erp_per_docusafe_budzety_projektow]
AS
SELECT        projekt_id AS id, projekt_id AS cn, nazwa AS title, kontrakt_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.bp_budzet_projektu
GO

--WIDOK ETAP�W PROJEKT�W
CREATE VIEW [dbo].[simple_erp_per_docusafe_etapy_projektow]
AS
SELECT        etap_id AS id, etap_id AS cn, nazwa AS title, projekt_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.bp_budzet_etap_projektu


--WIDOK ZASOB�W PROJEKTOWYCH
CREATE VIEW [dbo].[simple_erp_per_docusafe_zasoby_projektow]
AS
SELECT        zasob_id AS id, zasob_id AS cn, nazwa AS title, etap_id AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.bp_budzet_etap_projektu_zasob



--WIDOK ZLECE� PRODUKCYJNYCH
CREATE VIEW [dbo].[simple_erp_per_docusafe_zlecprod_view]
AS
SELECT        zlecprod_id AS id, RTRIM(zlecprod_idm) AS cn, nazwa AS title, NULL AS refValue, '0' AS CENTRUM, '1' AS available
FROM            TEST_AW.dbo.zlecprod

--WIDOK PRACOWNIK�W ERP
CREATE VIEW [dbo].[simple_erp_per_docusafe_pracownicy]
AS
SELECT        pracownik_id AS ID, RTRIM(pracownik_idn) AS CN, RTRIM(nazwisko) + ' ' + RTRIM(imie) AS TITLE, NULL AS refValue, 0 AS CENTRUM, 
                         czy_aktywny AS available
FROM            TEST_AW.dbo.kp_pracownik

--WIDOK PRACOWNIK�W ERP filtrowane po komorkach
CREATE VIEW simple_erp_per_docusafe_pracownicy_filtered
AS
SELECT        p.pracownik_id AS ID,
RTRIM(p.pracownik_idn) AS CN,
RTRIM(p.nazwisko) + ' ' + RTRIM(p.imie) AS TITLE,
k.komorka_id AS refValue, 0 AS CENTRUM,
p.czy_aktywny AS available
FROM            TEST_AW.dbo.kp_pracownik as p
INNER JOIN TEST_AW.dbo.komorka as k ON p.komorka_org=k.komorka_idn



