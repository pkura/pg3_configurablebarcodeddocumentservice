

create table INS_WniosekZapDoDost(
document_id numeric(19,0),
odzial numeric(19,0),
TYP numeric(19,0),
komorka numeric(19,0),
status numeric(2,0),
PRACOWNIK numeric(19,0),
nrTel varchar(15),
dataDokumentu date,
DATAZAPOTRZEBOWANIA date,
WALUTA numeric(19,0),
KURS numeric(18,4),
suma numeric(19,4),
NrERP varchar(512),
zglaszajacy numeric(19,0),
akceptujacy numeric(19,0),
zatwierdzajacy numeric(19,0),
przyjmujacy numeric(19,0),
realizujacy numeric(19,0),
dataZgloszenia date,
dataAkceptacji date,
dataZatwierdzenia date,
dataPrzyjeciaERP date,
dataRealizacji date,
ERPID numeric(18,0) )

create table INS_WnioZapDoDost_Pozycje(
ID integer identity(1,1),
LP varchar(20),
POZYCJA_BUDZETU numeric(19,0),
IND varchar(40),
PRODUKT numeric(19,0),
OPIS varchar(240),
ILOSC numeric(19,3),
JM numeric(19,0),
CENA numeric(19,2),
CENA_BAZ numeric(19,2),
NETTO numeric(19,2),
NETTO_BAZ numeric(19,2),
VAT numeric(19,0),
DATAOCZEKIWANA date
)


create table INS_WniosZapDost_Dict(
DOCUMENT_ID numeric(19,0),
FIELD_CN varchar(50),
FIELD_VAL varchar(50),
ID integer identity(1,1))

create table INS_BUDZETPOZ(
ID integer identity(1,1),
KONTRAKT varchar(19),
BUDZET varchar(19),
FAZA varchar(19),
ZASOB varchar(19),
ZLECENIA varchar(19)
)

CREATE VIEW [dbo].[INS_BUDZETPOZ_view]
AS
SELECT        poz.ID, CAST(poz.ID AS varchar(50)) AS cn, CAST(poz.ID AS varchar(255)) AS title, 0 AS centrum, CAST(dic.DOCUMENT_ID AS varchar(20)) AS refValue, 
                         1 AS available
FROM            dbo.INS_WniosZapDost_Dict AS dic INNER JOIN
                         dbo.INS_BUDZETPOZ AS poz ON dic.FIELD_CN = 'BUDZET_POZ' AND poz.ID = dic.FIELD_VAL
                         
                         
create VIEW [dbo].[simple_erp_per_docusafe_indexy_zapdost_view]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT CAST(SUBSTRING(TEST_AW.dbo.wytwor.wytwor_idm, 0, PATINDEX('%-%', TEST_AW.dbo.wytwor.wytwor_idm)) AS int) AS ID, 
                         SUBSTRING(TEST_AW.dbo.wytwor.wytwor_idm, 0, PATINDEX('%-%', TEST_AW.dbo.wytwor.wytwor_idm)) AS CN, SUBSTRING(TEST_AW.dbo.wytwor.wytwor_idm, 0, 
                         PATINDEX('%-%', TEST_AW.dbo.wytwor.wytwor_idm)) AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' AS available
FROM            TEST_AW.dbo.wytwor LEFT OUTER JOIN
                         dbo.simple_erp_per_docusafe_vatstaw_view AS vatview ON vatview.id = TEST_AW.dbo.wytwor.vatstaw_id
WHERE        (TEST_AW.dbo.wytwor.wytwor_idm LIKE '[^a-z4]%') 


CREATE  VIEW [dbo].[simple_erp_per_docusafe_wytwory_zapdost_view]
AS
SELECT        TEST_AW.dbo.wytwor.wytwor_id AS ID, TEST_AW.dbo.wytwor.wytwor_idm AS CN, RTRIM(TEST_AW.dbo.wytwor.wytwor_idm) 
                         + ' : ' + TEST_AW.dbo.wytwor.nazwa AS TITLE, '0' AS CENTRUM, CAST(SUBSTRING(TEST_AW.dbo.wytwor.wytwor_idm, 0, PATINDEX('%-%', 
                         TEST_AW.dbo.wytwor.wytwor_idm)) AS int) AS refValue, '1' AS available, vatview.id AS vat_id
FROM            TEST_AW.dbo.wytwor LEFT OUTER JOIN
                         dbo.simple_erp_per_docusafe_vatstaw_view AS vatview ON vatview.id = TEST_AW.dbo.wytwor.vatstaw_id
WHERE        (TEST_AW.dbo.wytwor.wytwor_idm LIKE '[^a-z4]%') 



CREATE VIEW [dbo].[simple_erp_per_docusafe_typ_zapotrz_dost]
 AS
SELECT        typ_zapdost_id AS ID, typdok_idn AS CN, nazwa AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available, waluta_id, czywal
FROM            TEST_AW.dbo.typ_zapotrzebowania_dostawy
WHERE        (odmiana = 0) AND (rodzaj = 0)

create view [dbo].[simple_erp_per_docusafe_jm_view] 
AS
SELECT        jm_id AS ID, jm_idn AS CN, nazwa AS TITLE, precyzjajm, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.jm




CREATE VIEW [dbo].[simple_erp_per_docusafe_zlecenia__view]
 AS
SELECT        zlecprod_id AS ID, zlecprod_idm AS CN, nazwa AS TITLE, NULL AS refValue, 0 AS CENTRUM, 1 AS available
FROM            TEST_AW.dbo.pp_zlecprod


CREATE VIEW [dbo].[zapdostpoz_v]
AS
SELECT        a.zapdostpoz_id AS ID, a.ilosc, a.ilzreal, a.ildost, a.ildost_rozlicz AS ildostrozlicz, a.zapdost_id, RTRIM(b.wytwor_idm) + ': ' + b.nazwa AS PRODUKT,
                         CAST(a.uwagi AS varchar(255)) AS uwagi
FROM            TEST_AW.dbo.zapdostpoz AS a LEFT OUTER JOIN
                         TEST_AW.dbo.wytwor AS b ON a.wytwor_id = b.wytwor_id