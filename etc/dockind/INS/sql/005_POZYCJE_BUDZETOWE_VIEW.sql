create view pozycje_budzetowe_doc_view as
SELECT        mpk.ID,
CAST(mpk.ID AS varchar(50)) AS cn,
CAST(mpk.ID AS varchar(255)) AS title,
0 AS centrum,
CAST(mul.DOCUMENT_ID AS varchar(20)) AS refValue,
1 AS available
FROM dbo.dsg_ins_costinvoice_multiple_value AS mul
INNER JOIN dbo.DSG_STAWKI_VAT_FAKTURY AS mpk
ON mul.FIELD_CN = 'STAWKI_VAT' AND mpk.ID = mul.FIELD_VAL