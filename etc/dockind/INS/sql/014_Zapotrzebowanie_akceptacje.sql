CREATE TABLE [dbo].[INS_zapotrz_akceptacje](
	[typZapotrzebowania] [varchar](50) NULL,
	[akceptujacy] [varchar](50) NULL,
	[rodzaj] [varchar](50) NULL
) ON [PRIMARY]

GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NIB', N'abajson', N'AM')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIS', N'mtomkowicz', N'AM')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NO', N'wwawer', N'AM')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIB', N'dmuszynski', N'AM')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NIB', N'DN', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIS', N'NT', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NO', N'NT', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIB', N'NB', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'ZDP', N'NT', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'ZDT', N'NT', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'LM', N'NB', N'AD')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NIB', N'FH', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIS', N'BIS', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'NO', N'FH', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'BIB', N'FH', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'ZDP', N'FH', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'ZDT', N'FH', N'R')
GO
INSERT [dbo].[INS_zapotrz_akceptacje] ([typZapotrzebowania], [akceptujacy], [rodzaj]) VALUES (N'LM', N'FH', N'R')
GO
