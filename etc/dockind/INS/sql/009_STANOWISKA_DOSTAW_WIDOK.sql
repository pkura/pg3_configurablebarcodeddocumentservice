CREATE VIEW [dbo].[simple_erp_per_docusafe_stdost_view]
AS
SELECT        stdost_id AS id, stdost_idn AS cn, RTRIM(stdost_ids) + ' : ' + nazwa AS title, '0' AS CENTRUM, NULL AS refValue, czy_aktywna AS available
FROM            INS_TEST_DSF.dbo.stdost