CREATE TABLE dsg_ins_contract
	(
	ID numeric(18, 0) NOT NULL IDENTITY (1, 1),
	contract_no varchar(50) NULL,
	tender_signature varchar(50) NULL,
	)

CREATE TABLE dsg_ins_contract_item
	(
	ID numeric(18, 0) NOT NULL IDENTITY (1, 1),
	name varchar(50) NULL,
	descr varchar(MAX) NULL,
	unit varchar(50) NULL,
	quantity numeric(18, 0) NULL,
	netAmount numeric(18, 2) NULL,
	vatAmount numeric(18, 2) NULL
)