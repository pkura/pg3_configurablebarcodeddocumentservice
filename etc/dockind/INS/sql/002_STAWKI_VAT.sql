CREATE TABLE DSG_STAWKI_VAT_FAKTURY
(
	ID numeric(18,0) identity (1,1) not null,
	pozycja numeric (18,0),
	blokada numeric(18,0),
	pozycja numeric(18,0),
	netto numeric(18,2),
	stawka numeric(18,0),
	kwota_vat numeric (18,2),
	dzialalnosc numeric (18,0),
	obciazenie numeric (18,2),
	brutto numeric (18,2)
);

  CREATE TABLE simple_erp_per_docusafe_bd_pozycje_vat_view(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	);