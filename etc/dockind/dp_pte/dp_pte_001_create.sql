CREATE TABLE [DSG_PRAWNY_PTE]
(
	[DOCUMENT_ID] numeric(18, 0) NOT NULL primary key,
	[KLASA] int NOT NULL,
	[TYP] int NOT NULL,
	[PODTYP1] int NULL,
	[DATA170] datetime NULL,
	[DATA180] datetime NULL,
	[DATA190] datetime NULL,
	[DATA200] datetime NULL,
	[OKRES_TYP] smallint NULL,
	[OKRES_OD] datetime NULL,
	[OKRES_DO] datetime NULL,
	[OKRES_KONTROLI_OD] datetime NULL,
	[OKRES_KONTROLI_DO] datetime NULL,
	[SYGNATURA] varchar(50) COLLATE Polish_CI_AS NULL,
	[SYGNATURA_WYROKU] varchar(50) COLLATE Polish_CI_AS NULL,
	[SYGNATURA_KM] varchar(50) COLLATE Polish_CI_AS NULL,
	[INSTYTUCJA30] int NULL,
	[INSTYTUCJA40] int NULL,
	[INSTYTUCJA50] int NULL,
	[INSTYTUCJA60] int NULL,
	[PRZEDMIOT] varchar(50) COLLATE Polish_CI_AS NULL,
	[PRZEDMIOT_UCHWALY] varchar(50) COLLATE Polish_CI_AS NULL,
	[ZMIANY_PTE] varchar(50) COLLATE Polish_CI_AS NULL,
	[ZMIANY_OFE] varchar(50) COLLATE Polish_CI_AS NULL,
	[REPERTORIUM] varchar(50) COLLATE Polish_CI_AS NULL,
	[DLUZNIK] varchar(50) COLLATE Polish_CI_AS NULL,
	[IMIE] varchar(50) COLLATE Polish_CI_AS NULL,
	[NAZWISKO] varchar(50) COLLATE Polish_CI_AS NULL,
	[NUMER] varchar(50) COLLATE Polish_CI_AS NULL
);


CREATE TABLE [dp_inst] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[name] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[oldname] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

CREATE TABLE [DSG_DP_PTE_MULTIPLE_VALUE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);

insert into ds_box_line (line,name) values ('dp_pte','Dokumenty prawne');