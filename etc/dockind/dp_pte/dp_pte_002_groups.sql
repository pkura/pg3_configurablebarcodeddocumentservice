insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_READ','Dokumenty prawne - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_MODIFY','Dokumenty prawne - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_READ','Dokumenty prawne - Umowy - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_MODIFY','Dokumenty prawne - Umowy - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_ZARZADU_READ','Dokumenty prawne - Protoko造 Zarz康u - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_ZARZADU_MODIFY','Dokumenty prawne - Protoko造 Zarz康u - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_WALNEGO_ZGROMADZENIA_READ','Dokumenty prawne - Protoko造 Walnego Zgromadzenia - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_WALNEGO_ZGROMADZENIA_MODIFY','Dokumenty prawne - Protoko造 Walnego Zgromadzenia - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_RADY_NADZORCZEJ_READ','Dokumenty prawne - Protoko造 Rady Nadzorczej - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PROTOKOLY_RADY_NADZORCZEJ_MODIFY','Dokumenty prawne - Protoko造 Rady Nadzorczej - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KORESPONDENCJA_KNF_READ','Dokumenty prawne - Korespondencja KNF - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KORESPONDENCJA_KNF_MODIFY','Dokumenty prawne - Korespondencja KNF - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KONTROLA_KNF_READ','Dokumenty prawne - Kontrola KNF - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KONTROLA_KNF_MODIFY','Dokumenty prawne - Kontrola KNF - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KONTROLA_INNE_READ','Dokumenty prawne - Kontrola - inne - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_KONTROLA_INNE_MODIFY','Dokumenty prawne - Kontrola - inne - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_SPRAWY_EGZEKUCYJNE_READ','Dokumenty prawne - Sprawy egzekucyjne - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_SPRAWY_EGZEKUCYJNE_MODIFY','Dokumenty prawne - Sprawy egzekucyjne - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_DOKUMENTY_REJESTROWE_READ','Dokumenty prawne - Dokumenty rejestrowe - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_DOKUMENTY_REJESTROWE_MODIFY','Dokumenty prawne - Dokumenty rejestrowe - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PELNOMOCNICTWA_READ','Dokumenty prawne - Pe軟omocnictwa - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_PELNOMOCNICTWA_MODIFY','Dokumenty prawne - Pe軟omocnictwa - zapis',null,'group',1,0);
