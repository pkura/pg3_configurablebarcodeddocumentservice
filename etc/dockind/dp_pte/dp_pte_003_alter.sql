alter table DSG_PRAWNY_PTE add PODTYP2 int null;
go
update DSG_PRAWNY_PTE set PODTYP2 = PODTYP1, PODTYP1 = NULL where typ = 410 or typ = 420;
update DSG_PRAWNY_PTE set PODTYP2 = PODTYP2-40 where typ = 410 or typ = 420 and podtyp1 > 40;
