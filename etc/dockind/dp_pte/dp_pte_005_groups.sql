﻿insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_DECYZJE_KNF_READ','Dokumenty prawne - Decyzje KNF - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_DECYZJE_KNF_MODIFY','Dokumenty prawne - Decyzje KNF - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_AKWIZYCYJNE_READ','Dokumenty prawne - Umowy Akwizycyjne - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_AKWIZYCYJNE_MODIFY','Dokumenty prawne - Umowy Akwizycyjne - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_INWESTYCYJNE_READ','Dokumenty prawne - Umowy Inwestycyjne - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_INWESTYCYJNE_MODIFY','Dokumenty prawne - Umowy Inwestycyjne - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_OFE_READ','Dokumenty prawne - Umowy OFE - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_OFE_MODIFY','Dokumenty prawne - Umowy OFE - zapis',null,'group',1,0);

insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_PTE_READ','Dokumenty prawne - Umowy PTE - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('DP_PTE_UMOWY_PTE_MODIFY','Dokumenty prawne - Umowy PTE - zapis',null,'group',1,0);