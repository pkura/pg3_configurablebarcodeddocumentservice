

CREATE TRIGGER INSERT_NEXT_NUMER 
   ON  DSG_PRACOWNICZY
   FOR INSERT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	-- SET NOCOUNT ON;

    -- Insert statements for trigger here

	declare @nextnumer numeric(18,0);
	select @nextnumer=(max(numer)+1) from DSG_PRACOWNICZY where pracownik in (select pracownik from inserted);
	IF @nextnumer is NULL begin
		select @nextnumer = 1;
	END
	update DSG_PRACOWNICZY set numer = @nextnumer where document_id in (select document_id from inserted);
END
GO
