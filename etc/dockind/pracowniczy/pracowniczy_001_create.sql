CREATE TABLE DSG_PRACOWNICZY (
	DOCUMENT_ID NUMERIC(19,0) PRIMARY KEY FOREIGN KEY REFERENCES DS_DOCUMENT(ID),
	KLASA SMALLINT NOT NULL,
	TYP SMALLINT NOT NULL,
	[DATA] datetime NULL,
	OPIS varchar(100) COLLATE Polish_CI_AS NULL,
	NUMER NUMERIC(19,0)
);

CREATE TABLE DSG_WORKER_DICTIONARY
(
	id			int identity(1,1) not null,
	nazwisko 	varchar(50) not null,
	imie		varchar(50) not null,
	pracuje		char(1) not null,
	idPracownika varchar(6) not null
);

alter table nw.DSG_PRACOWNICZY add pracownik numeric(18,0);
alter table nw.DSG_PRACOWNICZY add ZWOLNIONY char(1);

insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('PRACOWNICZA_GRUPA', 'Dokument Pracowniczy','group',?id roota?,0);
