create table dsg_PLEXI_REKLAMACJA
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	nr_reklamacji	varchar(50),
	kod_produktu	integer,
	nazwa_produktu integer,
	nr_reklamacji_na_towarze varchar(50),
	informacja_o_transporcie varchar(50),
	data_zgloszenia datetime,
	termin_realizacji datetime,
	osoba_zglaszajaca varchar(50),
	osoba_odp integer
);