create table dsg_PLEXI_ATEST
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	nazwa_atestu	varchar(50),
	dzial	integer,
	obiekt integer,
	osoba_odp integer,
	data_ostatniej_kontroli datetime,
	data_waznosci datetime,
	zlecenia integer
);

CREATE TABLE [DSG_PLEX_MULTIPLE_VALUE] 
(
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);