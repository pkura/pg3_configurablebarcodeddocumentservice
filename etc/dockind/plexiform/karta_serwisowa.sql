create table dsg_PLEXI_KARTA
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	nazwa_karty	varchar(50),
	dzial	integer,
	maszyna integer,
	osoba_odp	integer,
	data_ostatniej_kontroli	datetime,
	data_kolejnej_kontroli	datetime	
);