create table dsg_PLEXI_PROCEDURA
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	nazwa_procedury	varchar(50),
	data_wprowadzenia	datetime,
	data_obowiazywania	datetime,
	osoba_odp	integer,
	dzial	integer,
	osoba_zatwierdzajaca	integer
);