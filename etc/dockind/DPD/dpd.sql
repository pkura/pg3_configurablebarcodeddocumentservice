﻿create table dsg_DPD
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	kod_wydruku	varchar(50),
	delivery integer,
	name varchar(50),
	data datetime
);

alter table dsg_DPD add id_pracownika integer;