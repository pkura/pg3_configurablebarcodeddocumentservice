CREATE TABLE dsg_eco_faktura_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE dsg_eco_faktura_faktura (
  	DOCUMENT_ID numeric(18, 0) NOT NULL,
	sposob_dostarczenia numeric(18, 0),
	invoice_number varchar(100),
	invoice_date date,
	payment_date date,
	waluta numeric(18, 0),
	umowa numeric(18, 0),
	status numeric(18, 0),
	akceptacje numeric(18, 0),
	acceptance numeric(18, 0)
);

CREATE TABLE dsg_eco_akcpetacje (
	ID numeric(18,0) identity(1,1) NOT NULL,
	nazwa varchar(100),
	acc_date date,
	nazwisko varchar(100)
);

CREATE VIEW umowa_view
AS 
select d.CTIME, p.NIP, p.ORGANIZATION, u.* from ds_document d join dsg_ifpan_contract u on d.id=u.DOCUMENT_ID left join dso_person p on u.contractor = p.id;