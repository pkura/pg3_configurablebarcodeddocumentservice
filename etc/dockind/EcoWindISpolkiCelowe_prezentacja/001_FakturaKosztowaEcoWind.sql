alter table dsg_eco_faktura_faktura add gross_amount numeric(18,2);
alter table dsg_eco_faktura_faktura add centra_kosztow numeric(18,0); 
alter table dsg_eco_faktura_multiple_value alter column FIELD_CN varchar(120);

alter table DSG_CENTRUM_KOSZTOW_FAKTURY add a_acc_date date;
alter table DSG_CENTRUM_KOSZTOW_FAKTURY add akcept varchar(70);

