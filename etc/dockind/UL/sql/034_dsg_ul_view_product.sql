ALTER view [dbo].[dsg_ul_view_product] as
select id as id,
available as available,
produktIdn as cn,
produktNazwa as title,
null as refValue,
wytworId as centrum
from dsg_ul_services_product
where 
produktGrupa='4XX'
union
select id as id,
0 as available,
produktIdn as cn,
produktNazwa as title,
null as refValue,
wytworId as centrum
from dsg_ul_services_product
where 
produktGrupa='z_ZAKUPY'
union
SELECT - 1 AS id, 0 AS available, 'ND' AS cn,  'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum;
GO

