alter view dsg_ul_dziekan as
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
w.id as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
join DS_USER_TO_DIVISION ud on u.id=ud.USER_ID
join DS_DIVISION d on ud.DIVISION_ID=d.ID
join wydzialy_view w on SUBSTRING(d.CODE,1,2)=SUBSTRING(w.title,1,2)
where a.cn='dean'