ALTER view [dbo].[dsg_ul_view_project_osoba_upowazniona] as
WITH user_base
AS ( 
select u.id as uid,u.name as uname,u.DELETED as udeleted,u.LASTNAME+' '+u.FIRSTNAME as utitle, d.id,d.PARENT_ID,d.code,d.guid,d.NAME,d.HIDDEN from
ds_user u
join DS_USER_TO_DIVISION ud on u.ID=ud.USER_ID
join DS_DIVISION d on ud.DIVISION_ID=d.ID
UNION ALL
SELECT RCTE.uid,RCTE.uname,RCTE.udeleted,RCTE.utitle, H2.Id, H2.PARENT_ID,H2.code, H2.guid, h2.name,H2.HIDDEN
FROM ds_division H2
INNER JOIN user_base RCTE ON H2.ID = RCTE.PARENT_ID
where H2.PARENT_ID is not NULL
)

select
u.uid as id,
u.uname as cn,
u.utitle as title,
(1-u.udeleted) as available,
org.id as refValue,
null as centrum
from user_base u
join dsg_ul_mpk_mapping map on u.CODE=map.ds_div_cn
join dsg_ul_organizational_unit org on map.mpk_cn=org.cn
union
select 
u.id as id, 
u.name as cn,
u.LASTNAME+' '+u.FIRSTNAME as title, 
0 as refValue,
(1-u.DELETED) as available,
null as centrum
from ds_user u
GO


