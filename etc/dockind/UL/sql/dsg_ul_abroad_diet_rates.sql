CREATE TABLE [dbo].[dsg_ul_abroad_diet_rates](
	[country] [varchar](50) NULL,
	[currency_cn] [varchar](3) NULL,
	[country_cn] [varchar](5) NULL,
	[diet_rate] [numeric](19, 2) NULL,
	[acc_limit] [numeric](19, 2) NULL
) ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [countryCodeIndex] ON [dbo].[dsg_ul_abroad_diet_rates]
(
	[country_cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'palestyńska Władza Narodowa', N'EUR', NULL, CAST(50.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Stany Zjednoczone Ameryki (USA) - Nowy Jork', N'USD', NULL, CAST(59.00 AS Numeric(19, 2)), CAST(350.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Stany Zjednoczone Ameryki (USA) - Waszyngton', N'USD', NULL, CAST(59.00 AS Numeric(19, 2)), CAST(300.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Andora', N'EUR', N'AD', CAST(50.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Zjednoczone Emiraty Arabskie', N'EUR', N'AE', CAST(39.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Afganistan', N'EUR', N'AF', CAST(47.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Albania', N'EUR', N'AL', CAST(41.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Armenia', N'EUR', N'AM', CAST(42.00 AS Numeric(19, 2)), CAST(145.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Angola', N'USD', N'AO', CAST(61.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Argentyna', N'USD', N'AR', CAST(50.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Austria', N'EUR', N'AT', CAST(52.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Australia', N'AUD', N'AU', CAST(88.00 AS Numeric(19, 2)), CAST(250.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Azerbejdżan', N'EUR', N'AZ', CAST(43.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Bośnia i Hercegowina', N'EUR', N'BA', CAST(41.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Bangladesz', N'USD', N'BD', CAST(50.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Belgia', N'EUR', N'BE', CAST(48.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Bułgaria', N'EUR', N'BG', CAST(40.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Brazylia', N'EUR', N'BR', CAST(43.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Białoruś', N'EUR', N'BY', CAST(42.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kanada', N'CAD', N'CA', CAST(71.00 AS Numeric(19, 2)), CAST(190.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kongo', N'USD', N'CD', CAST(66.00 AS Numeric(19, 2)), CAST(220.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Czarnogóra', N'EUR', N'CG', CAST(40.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Wybrzeże Kości Słoniowej', N'EUR', N'CI', CAST(33.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Chile', N'USD', N'CL', CAST(60.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Chiny', N'EUR', N'CN', CAST(55.00 AS Numeric(19, 2)), CAST(170.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kolumbia', N'USD', N'CO', CAST(49.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kostaryka', N'USD', N'CR', CAST(50.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kuba', N'EUR', N'CU', CAST(42.00 AS Numeric(19, 2)), CAST(110.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Cypr', N'EUR', N'CY', CAST(43.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Czechy', N'EUR', N'CZ', CAST(41.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Niemcy', N'EUR', N'DE', CAST(49.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Dania', N'DKK', N'DK', CAST(406.00 AS Numeric(19, 2)), CAST(1300.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Algieria', N'EUR', N'DZ', CAST(50.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Ekwador', N'USD', N'EC', CAST(44.00 AS Numeric(19, 2)), CAST(110.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Estonia', N'EUR', N'EE', CAST(41.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Egipt', N'USD', N'EG', CAST(55.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Hiszpania', N'EUR', N'ES', CAST(50.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Etiopia', N'USD', N'ET', CAST(55.00 AS Numeric(19, 2)), CAST(300.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Finlandia', N'EUR', N'FI', CAST(48.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Francja', N'EUR', N'FR', CAST(50.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Gruzja', N'EUR', N'GE', CAST(43.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Gibraltar', N'GBP', N'GI', CAST(35.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Grecja', N'EUR', N'GR', CAST(48.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Hongkong', N'USD', N'HK', CAST(55.00 AS Numeric(19, 2)), CAST(250.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Chorwacja', N'EUR', N'HR', CAST(42.00 AS Numeric(19, 2)), CAST(125.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Węgry', N'EUR', N'HU', CAST(44.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Indonezja', N'EUR', N'ID', CAST(41.00 AS Numeric(19, 2)), CAST(110.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Irlandia', N'EUR', N'IE', CAST(52.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Izrael', N'EUR', N'IL', CAST(50.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Indie', N'EUR', N'IN', CAST(38.00 AS Numeric(19, 2)), CAST(190.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Irak', N'USD', N'IQ', CAST(60.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Iran', N'EUR', N'IR', CAST(41.00 AS Numeric(19, 2)), CAST(95.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Islandia', N'EUR', N'IS', CAST(56.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Włochy', N'EUR', N'IT', CAST(48.00 AS Numeric(19, 2)), CAST(174.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Jordania', N'EUR', N'JO', CAST(40.00 AS Numeric(19, 2)), CAST(95.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Japonia', N'JPY', N'JP', CAST(7532.00 AS Numeric(19, 2)), CAST(22000.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kenia', N'EUR', N'KE', CAST(41.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kirgistan', N'USD', N'KG', CAST(41.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kambodża', N'USD', N'KH', CAST(45.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Korea Północna', N'EUR', N'KP', CAST(48.00 AS Numeric(19, 2)), CAST(170.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Korea Południowa', N'EUR', N'KR', CAST(46.00 AS Numeric(19, 2)), CAST(170.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kuwejt', N'EUR', N'KW', CAST(39.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Kazachstan', N'EUR', N'KZ', CAST(41.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Laos', N'USD', N'LA', CAST(54.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Liban', N'USD', N'LB', CAST(57.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Liechtenstein', N'CHF', N'LI', CAST(88.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Litwa', N'EUR', N'LT', CAST(39.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Luksemburg', N'EUR', N'LU', CAST(48.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Łotwa', N'EUR', N'LV', CAST(57.00 AS Numeric(19, 2)), CAST(132.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Libia', N'EUR', N'LY', CAST(52.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Maroko', N'EUR', N'MA', CAST(41.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Monako', N'EUR', N'MC', CAST(50.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Mołdawia', N'EUR', N'MD', CAST(41.00 AS Numeric(19, 2)), CAST(85.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Macedonia', N'EUR', N'MK', CAST(39.00 AS Numeric(19, 2)), CAST(125.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Mongolia', N'EUR', N'MN', CAST(45.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Malta', N'EUR', N'MT', CAST(43.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Meksyk', N'USD', N'MX', CAST(53.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Malezja', N'EUR', N'MY', CAST(41.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Nigeria', N'EUR', N'NG', CAST(46.00 AS Numeric(19, 2)), CAST(240.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Holandia', N'EUR', N'NL', CAST(50.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Norwegia', N'NOK', N'NO', CAST(451.00 AS Numeric(19, 2)), CAST(1500.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Nowa Zelandia', N'USD', N'NZ', CAST(58.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Oman', N'EUR', N'OM', CAST(40.00 AS Numeric(19, 2)), CAST(240.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'OTHER', N'EUR', N'OTHER', CAST(41.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Panama', N'USD', N'PA', CAST(52.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Peru', N'USD', N'PE', CAST(50.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Pakistan', N'EUR', N'PK', CAST(38.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Portugalia', N'EUR', N'PT', CAST(49.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Katar', N'EUR', N'QA', CAST(41.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Rumunia', N'EUR', N'RO', CAST(38.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Rosja', N'EUR', N'RU', CAST(48.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Arabia Saudyjska', N'EUR', N'SA', CAST(45.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Szwecja', N'SEK', N'SE', CAST(459.00 AS Numeric(19, 2)), CAST(1800.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Singapur', N'USD', N'SG', CAST(56.00 AS Numeric(19, 2)), CAST(230.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Słowenia', N'EUR', N'SI', CAST(41.00 AS Numeric(19, 2)), CAST(130.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Słowacja', N'EUR', N'SK', CAST(43.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'San Marino', N'EUR', N'SM', CAST(48.00 AS Numeric(19, 2)), CAST(174.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Senegal', N'EUR', N'SN', CAST(44.00 AS Numeric(19, 2)), CAST(120.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Syria', N'USD', N'SY', CAST(50.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Szwajcaria', N'CHF', N'SZ', CAST(88.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Tajlandia', N'USD', N'TH', CAST(42.00 AS Numeric(19, 2)), CAST(110.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Tadżykistan', N'EUR', N'TJ', CAST(41.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Turkmenistan', N'USD', N'TM', CAST(47.00 AS Numeric(19, 2)), CAST(90.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Tunezja', N'EUR', N'TN', CAST(37.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Turcja', N'USD', N'TR', CAST(53.00 AS Numeric(19, 2)), CAST(173.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Tajwan', N'EUR', N'TW', CAST(40.00 AS Numeric(19, 2)), CAST(142.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Tanzania', N'USD', N'TZ', CAST(53.00 AS Numeric(19, 2)), CAST(150.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Ukraina', N'EUR', N'UA', CAST(41.00 AS Numeric(19, 2)), CAST(180.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Wielka Brytania', N'GBP', N'UK', CAST(35.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Stany Zjednoczone', N'USD', N'US', CAST(59.00 AS Numeric(19, 2)), CAST(200.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Urugwaj', N'USD', N'UY', CAST(50.00 AS Numeric(19, 2)), CAST(80.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Uzbekistan', N'EUR', N'UZ', CAST(41.00 AS Numeric(19, 2)), CAST(140.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Wenezuela', N'USD', N'VE', CAST(60.00 AS Numeric(19, 2)), CAST(220.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Wietnam', N'USD', N'VN', CAST(53.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Jemen', N'USD', N'YE', CAST(48.00 AS Numeric(19, 2)), CAST(160.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Serbia', N'EUR', N'YU', CAST(40.00 AS Numeric(19, 2)), CAST(100.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'RPA', N'USD', N'ZA', CAST(52.00 AS Numeric(19, 2)), CAST(275.00 AS Numeric(19, 2)))
GO
INSERT [dbo].[dsg_ul_abroad_diet_rates] ([country], [currency_cn], [country_cn], [diet_rate], [acc_limit]) VALUES (N'Zimbabwe', N'EUR', N'ZW', CAST(39.00 AS Numeric(19, 2)), CAST(90.00 AS Numeric(19, 2)))
GO
