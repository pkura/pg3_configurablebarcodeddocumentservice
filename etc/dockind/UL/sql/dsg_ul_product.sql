create table dsg_ul_services_product (
id bigint identity(1,1) not null,
available int null,
produktGrupa varchar(50) null,
produktIdn varchar(50) null,
produktNazwa varchar(150) null,
wytworId bigint null);

CREATE UNIQUE CLUSTERED INDEX [c-wytworId] ON [dbo].[dsg_ul_services_product] 
(
	[wytworId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view [dbo].[dsg_ul_view_product] as
select id as id,
available as available,
produktIdn as cn,
produktNazwa as title,
null as refValue,
produktGrupa as centrum
from dsg_ul_services_product
where 
(produktGrupa='410' or produktGrupa='411' or produktGrupa='420')
and SUBSTRING(produktIdn,1,3)=produktGrupa
and SUBSTRING(produktIdn,12,2)='xx'
and SUBSTRING(produktIdn,10,1)<>'x'
union
SELECT - 1 AS id, 1 AS available, 'ND' AS cn,  'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum;

select * into dsg_ul_product from dsg_ul_view_product;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_product
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
