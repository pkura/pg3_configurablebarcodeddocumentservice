create table dsg_ul_rachunki
(
	id numeric (18,0) identity(1,1) not null,
	nr_rachunku numeric (18,0),
	kwota numeric (18,2),
	data date
);