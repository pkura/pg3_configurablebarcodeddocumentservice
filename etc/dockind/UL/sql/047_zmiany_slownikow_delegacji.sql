delete from [dsg_ul_trip_transports];
SET IDENTITY_INSERT [dbo].[dsg_ul_trip_transports] ON
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(18, 0)), N'PKP', N'Poci�g', 0, N'0', 1)
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(18, 0)), N'AIR', N'Samolot', 0, N'0', 1)
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(18, 0)), N'CAR', N'Auto', 0, N'0', 1)
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(18, 0)), N'BUS', N'Autokar', 0, N'0', 1)
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(18, 0)), N'CAR100', N'Samoch�d prywatny (100%)', 0, N'0', 1)
INSERT [dbo].[dsg_ul_trip_transports] ([ID], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(18, 0)), N'BK', N'Brak koszt�w przejazdu', 0, N'0', 1)
SET IDENTITY_INSERT [dbo].[dsg_ul_trip_transports] OFF

update dso_delegate_cost set refValue=6 where cn='RYCZALT_DOJAZDY';
 
CREATE TABLE [dbo].[dsg_ul_delegation_diet_dic](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[bool1] bit null,
	[bool2] bit null,
	[bool3] bit null,
	[date] datetime NULL
) ON [PRIMARY]
GO