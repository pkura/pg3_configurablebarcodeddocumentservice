alter view [dbo].[dsg_ul_structure_budget_position]
with schemabinding
as
select 
bs.bd_budzet_rodzaj_id as id,
bs.dok_poz_nrpoz as cn,
bs.dok_poz_nazwa 
+ ' (kwota: ' + CAST((CAST(ISNULL(bs.dok_poz_p_kosz,0) as numeric(17,2))-CAST(ISNULL(bs.dok_poz_rez_koszt,0) as numeric(17,2))) as varchar(50)) + ')' as title,
MIN(CAST(bs.available as INT)) * MIN(CAST(bs.czy_aktywna as INT)) as available,
null as centrum,
bs.bd_budzet_id as refValue
from dbo.dsg_ul_services_structure_budgets bs
group by bs.bd_budzet_id,bs.bd_budzet_rodzaj_id,dok_poz_nrpoz,dok_poz_nazwa,bs.budzet_nazwa,bs.dok_poz_p_kosz,bs.dok_poz_rez_koszt
GO