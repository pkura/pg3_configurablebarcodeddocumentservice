CREATE TABLE [dbo].[dsg_ul_services_structure_budgets](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[available] [bit] NOT NULL,
	[bd_budzet_id] [bigint] NULL,
	[bd_budzet_idm] [varchar](100) NULL,
	[bd_budzet_rodzaj_id] [bigint] NULL,
	[bd_budzet_rodzaj_zrodla_id] [bigint] NULL,
	[bd_grupa_rodzaj_id] [bigint] NULL,
	[bd_rodzaj] [varchar](100) NULL,
	[bd_str_budzet_idn] [varchar](100) NULL,
	[bd_str_budzet_ids] [varchar](100) NULL,
	[bd_szablon_poz_id] [bigint] NULL,
	[bud_w_okr_nazwa] [varchar](100) NULL,
	[budzet_id] [bigint] NULL,
	[budzet_idm] [varchar](100) NULL,
	[budzet_ids] [varchar](100) NULL,
	[budzet_nazwa] [varchar](100) NULL,
	[czy_aktywna] [int] NULL,
	[dok_bd_str_budzet_idn] [varchar](100) NULL,
	[dok_kom_nazwa] [varchar](100) NULL,
	[dok_poz_limit] [numeric](15, 4) NULL,
	[dok_poz_nazwa] [varchar](100) NULL,
	[dok_poz_nrpoz] [int] NULL,
	[dok_poz_p_ilosc] [numeric](15, 4) NULL,
	[dok_poz_p_kosz] [numeric](15, 4) NULL,
	[dok_poz_r_ilosc] [numeric](15, 4) NULL,
	[dok_poz_r_koszt] [numeric](15, 4) NULL,
	[dok_poz_rez_ilosc] [numeric](15, 4) NULL,
	[dok_poz_rez_koszt] [numeric](15, 4) NULL,
	[okrrozl_id] [bigint] NULL,
	[pozycja_podrzedna_id] [bigint] NULL,
	[str_bud_nazwa] [varchar](100) NULL,
	[wspolczynnik_pozycji] [int] NULL,
	[wytwor_id] [bigint] NULL,
	[zrodlo_id] [bigint] NULL,
	[zrodlo_idn] [varchar](100) NULL,
	[zrodlo_nazwa] [varchar](100) NULL,
	[zrodlo_p_koszt] [numeric](15, 4) NULL,
	[zrodlo_r_koszt] [numeric](15, 4) NULL,
	[zrodlo_rez_koszt] [numeric](15, 4) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[dsg_ul_services_structure_budgets] ADD  DEFAULT ((1)) FOR [available]
GO

CREATE UNIQUE INDEX dsg_ul_services_structure_budgets_ID
ON dsg_ul_services_structure_budgets (id);
CREATE NONCLUSTERED INDEX dsg_ul_services_structure_budgets_MIXED
ON dsg_ul_services_structure_budgets (bd_budzet_id,bd_budzet_rodzaj_id,dok_bd_str_budzet_idn);