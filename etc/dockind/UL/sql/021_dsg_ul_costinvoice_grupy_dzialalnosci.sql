EXEC sp_rename 'dsg_ul_costinvoice_grupy_dzialalnosci', 'dsg_ul_costinvoice_grupy_dzialalnosci_base';
GO

create view dsg_ul_costinvoice_grupy_dzialalnosci as
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1621 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1620 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1622 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1623 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1624 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1625 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1626 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1627 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')
union
select
id as id, 
cn as cn,
title as title,
centrum as centrum,
available as available,
1628 as refValue
from dsg_ul_costinvoice_grupy_dzialalnosci_base
where cn not in ('080-X','081-X')