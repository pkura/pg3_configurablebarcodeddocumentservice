alter view [dbo].[dsg_ul_order_dostawa_osoby] as
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
null as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
where a.cn='tender_auth_it'
union
select u.ID as id,
u.name as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
null as refValue,
0 as available
from ds_user u