drop table dsg_ul_organizational_unit;
exec sp_rename 'dsg_ul_view_organizational_unit', 'dsg_ul_organizational_unit';

drop table dsg_ul_project_osoba_upowazniona;
exec sp_rename 'dsg_ul_view_project_osoba_upowazniona', 'dsg_ul_project_osoba_upowazniona';

drop table dsg_ul_projects;
exec sp_rename 'dsg_ul_view_projects', 'dsg_ul_projects';

drop table dsg_ul_project_budgets;
exec sp_rename 'dsg_ul_view_project_budgets', 'dsg_ul_project_budgets';

drop table dsg_ul_project_budget_positions;
exec sp_rename 'dsg_ul_view_project_budget_positions', 'dsg_ul_project_budget_positions';

drop table dsg_ul_project_budget_position_resources;
exec sp_rename 'dsg_ul_view_project_budget_position_resources', 'dsg_ul_project_budget_position_resources';

drop table dsg_ul_projects_fund_sources;
exec sp_rename 'dsg_ul_view_projects_fund_sources', 'dsg_ul_projects_fund_sources';

drop table dsg_ul_product;
exec sp_rename 'dsg_ul_view_product', 'dsg_ul_product';

drop table dsg_ul_production_orders;
exec sp_rename 'dsg_ul_view_production_orders', 'dsg_ul_production_orders';

drop view dsg_ul_view_fpmsid_852;
drop view dsg_ul_view_fpmsid_852_1_20;
drop view dsg_ul_view_fpmsid_grupa_kosztu_00;
drop view dsg_ul_view_fpmsid_grupa_kosztu_ds;
drop view dsg_ul_view_fpmsid_grupa_kosztu_stolowki;
drop view dsg_ul_view_fpmsid_typ_powierz_stolowki;
drop view dsg_ul_view_fpmsid_typ_powierzchni;
drop view dsg_ul_view_fpmsid_typ_powierzchni_ds852;
drop view dsg_ul_view_koszty_rodzajowe_zespol_8;
drop view dsg_ul_view_mt08;
drop view dsg_ul_view_numer_inwentarzowy;
drop view dsg_ul_view_samochody;
drop view dsg_ul_view_zfss_wydatki;
drop view pracownicy_to_wydzialy_view;
drop view dsg_ul_project_osoba_upowazniona_mod;

drop table dsg_ul_services_fpmsid_852;
drop table dsg_ul_services_fpmsid_852_1_20;
drop table dsg_ul_services_fpmsid_grupa_kosztu_00;
drop table dsg_ul_services_fpmsid_grupa_kosztu_ds;
drop table dsg_ul_services_fpmsid_grupa_kosztu_stolowki;
drop table dsg_ul_services_fpmsid_typ_powierz_stolowki;
drop table dsg_ul_services_fpmsid_typ_powierzchni;
drop table dsg_ul_services_fpmsid_typ_powierzchni_ds852;
drop table dsg_ul_services_koszty_rodzajowe_zespol_8;
drop table dsg_ul_services_numer_inwentarzowy;
drop table dsg_ul_services_mt08;
drop table dsg_ul_services_zfss_wydatki;
drop table dsg_ul_numer_inwentarzowy;
drop table dsg_ul_samochody;

drop table dsg_ul_fpmsid_852;
drop table dsg_ul_fpmsid_852_1_20;
drop table dsg_ul_fpmsid_grupa_kosztu_00;
drop table dsg_ul_fpmsid_grupa_kosztu_ds;
drop table dsg_ul_fpmsid_grupa_kosztu_stolowki;
drop table dsg_ul_fpmsid_typ_powierz_stolowki;
drop table dsg_ul_fpmsid_typ_powierzchni;
drop table dsg_ul_fpmsid_typ_powierzchni_ds852;
drop table dsg_ul_koszty_rodzajowe_zespol_8;
drop table dsg_ul_mt08;
drop table dsg_ul_zfss_wydatki;

