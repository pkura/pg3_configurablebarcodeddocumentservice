create table dsg_ul_invoice_fields (
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
)
insert into dsg_ul_invoice_fields (refValue,available,centrum,cn,title) values (null,1,null,'budzet_dydaktyka','Dydaktyka');
insert into dsg_ul_invoice_fields (refValue,available,centrum,cn,title) values (null,1,null,'budzet_naukowe','Naukowo-badawcze');
insert into dsg_ul_invoice_fields (refValue,available,centrum,cn,title) values (null,1,null,'budzet_ue','Międzynardowo (unijne)');
insert into dsg_ul_invoice_fields (refValue,available,centrum,cn,title) values (null,1,null,'budzet_konferencja','Konferencja');