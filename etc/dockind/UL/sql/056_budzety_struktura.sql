insert into dsd_dictionary values(0, 'C_STRUCTURE_BUDGET', 'dsg_ul_structure_budget', 2, 'C_STRUCTURE_BUDGET', (select top 1 id from dsd_dictionary where cn='C_MPK'), 0, 'ul_costinvoice');
insert into dsd_dictionary values(0, 'C_STRUCTURE_BUDGET_POSITION', 'dsg_ul_structure_budget_position', 2, 'C_STRUCTURE_BUDGET_POSITION', (select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET'), 0, 'ul_costinvoice');

insert into dsd_dictionary_map
select map.control_value,(select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET'),0,1,'ul_costinvoice'
from (
select distinct control_value from dsd_dictionary_map where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_MPK')
EXCEPT
select distinct control_value from dsd_dictionary_map
where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_PROJECT')
) map

insert into dsd_dictionary_map
select map.control_value,(select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET_POSITION'),0,1,'ul_costinvoice'
from (
select distinct control_value from dsd_dictionary_map where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_MPK')
EXCEPT
select distinct control_value from dsd_dictionary_map
where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_PROJECT')
) map

alter table dsg_ul_budget_position
add c_structure_budget int;
alter table dsg_ul_budget_position
add c_structure_budget_position int;