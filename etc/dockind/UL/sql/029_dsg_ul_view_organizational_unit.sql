ALTER view [dbo].[dsg_ul_view_organizational_unit] as
select 
mpk.id as id,
mpk.cn as cn,
mpk.title as title,
mpk.centrum as centrum, 
d.id as refValue,
mpk.available as available
from dsg_ul_services_organizational_unit mpk
join dsg_ul_mpk_mapping m on m.mpk_cn=mpk.cn
join ds_division d on LEFT(d.CODE,2)=LEFT(m.ds_div_cn,2)
where LEFT(d.CODE,2) in ('01','02','04','05','06','07','08','11','13','14','15','16')
UNION
select 
mpk.id as id,
mpk.cn as cn,
mpk.title as title,
mpk.centrum as centrum, 
d.id as refValue,
mpk.available as available
from dsg_ul_services_organizational_unit mpk
join ds_division d on 1=1
where LEFT(d.CODE,2) not in ('01','02','04','05','06','07','08','11','13','14','15','16')
--01	Wydzia� Filologiczny
--02	Wydzia� Filozoficzno-Historyczny
--04	Wydzia� Biologii i Ochrony �rodowiska
--05	Wydzia� Prawa i Administracji
--06	Wydzia� Ekonomiczno-Socjologiczny
--07	Wydzia� Nauk o Wychowaniu
--08	Wydzia� Zarz�dzania
--11	Wydzia� Matematyki i Informatyki
--13	Wydzia� Studi�w Mi�dzynarodowych i Politologicznych
--14	Wydzia� Nauk Geograficznych
--15	Wydzia� Fizyki i Informatyki Stosowanej
--16	Wydzia� Chemii