create view dsg_ul_dziekan as
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
null as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
where a.cn='dean'