ALTER view [dbo].[wydzialy_view] WITH SCHEMABINDING as
select 
ID as id,
GUID as cn,
CODE+': '+ SUBSTRING(NAME,0,125) +
CASE 
	WHEN LEN(NAME)>125 THEN '...'
	ELSE ''
END
 as title,
parent_id as centrum,
null as refValue,
(1-HIDDEN) as available
from dbo.DS_DIVISION
where DIVISIONTYPE='division' and PARENT_ID is not null
GO