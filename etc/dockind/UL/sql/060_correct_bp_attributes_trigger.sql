CREATE TRIGGER CORRECT_BP_ATTRIBUTES ON dsg_ul_budget_position
AFTER UPDATE as 
--Rozwi�zanie problemu ukrywania p�l w zale�no�ci od wybranych kont ksi�gowych. Pole, kt�re jest ukrywane nie jest zapisywane, a je�li mia�o ju� jako� warto�� w bazie, to nie jest nullowane.
BEGIN
	update dsg_ul_budget_position set c_project=null,c_budget=null,c_position=null,c_resource=null
	from dsg_ul_budget_position bp with(nolock)
	join inserted i on bp.id=i.id
	where i.i_kind is not null and i.i_kind in (select id from dsg_ul_costinvoice_rodzaj_dzialalnosci r with(nolock)
	where cn is not null and cn in (
	--NIEPROJEKTOWE
	select distinct control_value from dsd_dictionary_map with(nolock)
	except
	select distinct control_value from dsd_dictionary_map with(nolock) where dictionary_id=(select top 1 id from dsd_dictionary with(nolock) where code='C_PROJECT')
	))
	update dsg_ul_budget_position set c_user=null
	from dsg_ul_budget_position bp with(nolock)
	join inserted i on bp.id=i.id
	where i.i_kind is not null and  i.i_kind in (select id from dsg_ul_costinvoice_rodzaj_dzialalnosci r with(nolock)
	where cn is not null and cn in (
	--PROJEKTOWE
	select distinct control_value from dsd_dictionary_map where dictionary_id=(select top 1 id from dsd_dictionary where code='C_PROJECT')
	))
END