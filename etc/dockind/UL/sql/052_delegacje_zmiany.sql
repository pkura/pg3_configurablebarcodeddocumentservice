alter table dsg_ul_delegation
add refundacja_koszty_czy bit;
alter table dsg_ul_delegation
add refundacja_koszty_opis varchar(500);
alter table dsg_ul_delegation
add dieta_wyzywienie int;
alter table dsg_ul_delegation
add srodek_lokomocji_samochod bit;
alter table dsg_ul_delegation
add umowa_podroz_samochodem numeric(19,0);
alter table dsg_ul_delegation
add ewidencja_przebiegu numeric(19,0);
alter table dsg_ul_delegation
add typ_dokumentu int;
alter table dsg_ul_delegation
add nr_tel varchar(50);
alter table dsg_ul_delegation
add nr_dowodu varchar(50);
alter table dsg_ul_delegation
add email varchar(150);
alter table dsg_ul_delegation
add kod_pracownika varchar(150);
alter table dsg_ul_delegation
add adres_zamieszkania varchar(500);


--ZAGRANICZNA
alter table [dsg_ul_delegation_abroad_dic]
add [int1] [int] NULL;
alter table [dsg_ul_delegation_abroad_dic]
add [int2] [int] NULL;
alter table [dsg_ul_delegation_abroad_dic]
add [int3] [int] NULL;

alter table dsg_ul_delegation_abroad
add refundacja_koszty_czy bit;
alter table dsg_ul_delegation_abroad
add zakup_biletu_czy bit;
alter table dsg_ul_delegation_abroad
add rachunki_za_nocleg_czy bit;
alter table dsg_ul_delegation_abroad
add rachunki_za_nocleg numeric(19,0);
alter table dsg_ul_delegation_abroad
add refundacja_koszty_opis varchar(500);
alter table dsg_ul_delegation_abroad
add umowa_zal4 numeric(19,0);
alter table dsg_ul_delegation_abroad
add zakup_biletu numeric(19,0);
alter table dsg_ul_delegation_abroad
add umowa_podroz_samochodem numeric(19,0);
alter table dsg_ul_delegation_abroad
add ewidencja_przebiegu numeric(19,0);
alter table dsg_ul_delegation_abroad
add typ_dokumentu int;
alter table dsg_ul_delegation_abroad
add nr_tel varchar(50);
alter table dsg_ul_delegation_abroad
add nr_dowodu varchar(50);
alter table dsg_ul_delegation_abroad
add email varchar(150);
alter table dsg_ul_delegation_abroad
add kod_pracownika varchar(150);
alter table dsg_ul_delegation_abroad
add adres_zamieszkania varchar(500);
alter table dsg_ul_delegation_abroad_dic
add	add_rate numeric(19, 2) NULL;