ALTER view [dbo].[dsg_ul_view_projects_fund_sources] as
select 
f.erp_id as id,
r.zasobId as refValue, f.title as title, f.cn as cn, f.centrum as centrum, 
f.available *
CASE
	WHEN RIGHT(cn,3) <> '.00' THEN 1
	ELSE 0
END as available
from dsg_ul_services_projects_fund_sources f
join dsg_ul_services_resources_fund_sources rf on f.erp_id=rf.zrodloId
join dsg_ul_services_project_budget_position_resources r on rf.zasobId=r.zasobId
union
select 
erp_id as id, 
0 as refValue,
title as title,
cn as cn,
 null as centrum,
available *
CASE
	WHEN RIGHT(cn,3) <> '.00' THEN 1
	ELSE 0
END as available
from dsg_ul_services_projects_fund_sources