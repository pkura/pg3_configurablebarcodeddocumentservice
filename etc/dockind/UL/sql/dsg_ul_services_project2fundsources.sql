create table dsg_ul_services_project2fundsources (
kontrakt_id bigint,
zrodlo_id bigint
);
CREATE CLUSTERED INDEX [ClusteredIndex-20131029-154215] ON [dbo].[dsg_ul_services_project2fundsources]
(
	[zrodlo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

