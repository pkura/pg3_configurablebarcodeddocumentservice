ALTER view [dbo].[dsg_ul_document_types] as
select t.id as id,
t.available*ISNULL(czy_aktywny,0) as available,
CASE
	when DATALENGTH(LTRIM(RTRIM(t.typdok_idn)))<5 THEN LEFT(LTRIM(RTRIM(t.typdok_idn))+'     ',5)
	ELSE LTRIM(RTRIM(t.typdok_idn))
END 
as cn,
t.nazwa as title,
(
select top 1 rep_atrybut_id from dsd_simple_repository_attribute where objectid=t.typdokzak_id and context_id=6 and rep_klasa_id=187 and available=1
)
 as refValue,
t.typdokzak_id as centrum
from dsg_ul_services_document_types t
GO