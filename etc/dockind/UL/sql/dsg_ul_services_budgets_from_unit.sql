create table dsg_ul_services_budgets_from_units (
id numeric(19,0) identity(1,1),
available bit,
bd_str_budzet_id  numeric(19,0),
bd_str_budzet_idn varchar(256),
budzet_id  numeric(19,0),
budzet_idm varchar(256),
czy_agregat  numeric(19,0),
id_ numeric(19,0),
idm varchar(256),
nazwa varchar(256)
);
CREATE INDEX dsg_ul_services_budgets_from_units_id ON dsg_ul_services_budgets_from_units(id_);

create view dsg_ul_budgets_from_units
with schemabinding as
select
b.id_ as id,
ISNULL(b.idm,'') as cn,
ISNULL(b.nazwa,'') as title,
o.id as refValue,
b.available*(1-b.czy_agregat) as available,
null as centrum
from dbo.dsg_ul_services_budgets_from_units b
join dbo.dsg_ul_organizational_unit o on b.bd_str_budzet_idn=o.cn

