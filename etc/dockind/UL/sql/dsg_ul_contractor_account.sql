create table dsg_ul_services_contractor_account (
id bigint identity(1,1) not null,
available numeric(1,0) null,
bankId bigint null,
bankIdn varchar(128) null,
bankNazwa varchar(128) null,
czyIban int null,
dostawcaId bigint null,
dostawcaIdn varchar(128) null,
kolejnoscUzycia int null,
kontoIdm varchar(128) null,
kontobankoweId bigint null,
nazwa varchar(128) null,
numerKonta varchar(128) null,
symbolwaluty varchar(128) null,
typkursId bigint null,
typkursIdn varchar(128) null,
walutaId bigint null,
zrodkurswalId bigint null,
zrodkurswalIdn varchar(128) null,
zrodkurswalNazwa varchar(128) null);

CREATE CLUSTERED INDEX [c-wytworId] ON [dbo].[dsg_ul_services_contractor_account] 
(
	[kontobankoweId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view dsg_ul_view_contractor_account as
select 
acc.id as id, 
acc.kontoIdm as cn,
acc.bankNazwa+' '+acc.numerKonta as title,
acc.kontobankoweId as centrum,
acc.available as available,
per.id as refValue
from 
dso_person per
join dsg_ul_services_contractor_account acc on per.wparam=acc.dostawcaId;

select * into dsg_ul_contractor_account from dsg_ul_view_contractor_account;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_contractor_account
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO



