create view [dbo].[dsg_ul_delegation_exchange_rate] as
select MIN(id) as id, waluta_nom_symbolwaluty as cn, min(waluta_nom_nazwa) as title, null as centrum, null as refValue, 1 as available  from dsg_ul_services_exchange_rate group by waluta_nom_symbolwaluty
union
select 0 as id, 'PLN' as cn, 'polski z�oty' as title, null as centrum, null as refValue, 1 as available
union
--dop�ki nie pojawi� si� w erpie
select -1 as id, 'AUD' as cn, 'dolar australijski (brak tabel kurs�w)' as title, null as centrum, null as refValue, 1 as available
union 
select -2 as id, 'CAD' as cn, 'dolar kanadyjski (brak tabel kurs�w)' as title, null as centrum, null as refValue, 1 as available
union 
select -3 as id, 'JPY' as cn, 'jen japo�ski (brak tabel kurs�w)' as title, null as centrum, null as refValue, 1 as available
union 
select -4 as id, 'NOK' as cn, 'korona norweska (brak tabel kurs�w)' as title, null as centrum, null as refValue, 1 as available
union 
select -5 as id, 'SEK' as cn, 'korona szwedzka (brak tabel kurs�w)' as title, null as centrum, null as refValue, 1 as available