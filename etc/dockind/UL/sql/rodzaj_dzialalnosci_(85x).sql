insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'851-2','Zak�adowy Fundusz �wiadcze� Socjalnych ');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'852-1-20','Fundusz Pomocy Materialnej dla Student�w - z dotacji - wykorzystanie');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'852-3-20','Koszty DS-�w nierozliczone na powierzchni� (k.roliczeniowe');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'852-3-21','Koszty DS-�w rozliczone na powierzchni� ( k. brane do wyniku');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'852-3-60','Koszty sto��wek studenckich');

insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'851-X','Zak�adowy Fundusz �wiadcze� Socjalnych ');
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'852-X','Fundusz Pomocy Materialnej dla Student�w i Doktorant�w');


update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_grupy_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=cn+': '+title;
update dsg_ul_costinvoice_grupy_dzialalnosci set title=cn+': '+title;

update dsg_ul_costinvoice_rodzaj_dzialalnosci set refValue=g.id
from dsg_ul_costinvoice_grupy_dzialalnosci g
where substring(dsg_ul_costinvoice_rodzaj_dzialalnosci.cn,1,4)=substring(g.cn,1,4) and g.cn like '85%';

insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('851-X','851-2',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('852-X','852-1-20',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('852-X','852-3-20',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('852-X','852-3-21',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('852-X','852-3-60',1,0,0,0,0,0,1);