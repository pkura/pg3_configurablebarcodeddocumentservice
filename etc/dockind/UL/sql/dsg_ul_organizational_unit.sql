ALTER view [dbo].[dsg_ul_organizational_unit] 
WITH SCHEMABINDING
as
select 
id as id, 
cn as cn, 
title as title, 
centrum as centrum, 
null as refValue, 
CASE
	WHEN cn in ('1000000000','3200000000','3310000000','4300000000','4800000000','2100000000','2110000000','2120000000','2130000000','2140000000','2150000000','2160000000','2170000000','2180000000','2190000000','2200000000','2210000000','2220000000') THEN 0
	ELSE available
END
as available
from dbo.dsg_ul_services_organizational_unit