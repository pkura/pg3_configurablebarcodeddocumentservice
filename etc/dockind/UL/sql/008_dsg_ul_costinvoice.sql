alter table dsg_ul_costinvoice
add sposob_zwrotu numeric(18,0);

alter table dsg_ul_costinvoice
drop column pracownik;
alter table dsg_ul_costinvoice
drop column pracownik_jednostka;
alter table dsg_ul_costinvoice
drop column grupy_dzialalnosci;
alter table dsg_ul_costinvoice
drop column rodzaj_dzialalnosci;