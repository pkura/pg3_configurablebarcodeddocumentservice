alter view dsg_ul_projects as
SELECT     proj.cn, proj.erp_id AS id, proj.cn + ': ' + proj.title AS title, unit.id AS refValue, proj.centrum, proj.available
FROM         dbo.dsg_ul_services_projects AS proj INNER JOIN
                      dbo.dsg_ul_organizational_unit_extended AS unit ON ISNUMERIC(SUBSTRING(unit.cn, 4, 1)) <> 1 AND SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2)
WHERE     (SUBSTRING(unit.cn, 1, 1) = '2')