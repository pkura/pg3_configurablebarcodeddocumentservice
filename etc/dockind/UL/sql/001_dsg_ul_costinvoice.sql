alter table dsg_ul_costinvoice
add grupy_dzialalnosci numeric(18,0) NULL;
alter table dsg_ul_costinvoice
add rodzaj_dzialalnosci numeric(18,0) NULL;

alter table dsg_ul_costinvoice
add upowazniony_projekt numeric(18,0) NULL;

alter table dsg_ul_costinvoice
add is_slownik_komorek int NULL;
alter table dsg_ul_costinvoice
add is_numer_projektu int NULL;
alter table dsg_ul_costinvoice
add is_numer_zlecenia int NULL;
alter table dsg_ul_costinvoice
add is_numer_inwentarzowy int NULL;
alter table dsg_ul_costinvoice
add is_oznaczenie_grupy_pomieszczen int NULL;
alter table dsg_ul_costinvoice
add is_rodzaj_kosztu int NULL;
alter table dsg_ul_costinvoice
add is_zrodlo_finansowania int NULL;
