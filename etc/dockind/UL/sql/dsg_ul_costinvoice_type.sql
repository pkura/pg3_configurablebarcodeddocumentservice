create table dsg_ul_costinvoice_type (
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
)

insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZDP','zaliczki z dzia�alno�ci dydaktycznej faktura pln -zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZBP','zaliczki z dzia�alno�ci badawczej faktura w pln -zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZPP','zaliczki z pozosta�ej dzia�alno�ci faktura w pln -zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZDW','zaliczki z dzia�alno�ci dydaktycznej faktura w walucie obcej-zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZBW','zaliczki z dzia�alno�ci badawczej faktura w walucie obcej -zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZPW','zaliczki z pozosta�ej dzia�alno�ci faktura w walucie obcej-zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'FZP','dostawcy krajowi-  zakup towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'WNT','dostawcy zagraniczni -wewn�trzwsp�lnotowe nabycie towar�w')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'IU','2dostawcy zagraniczni - import us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'PNZ','dostawcy zagraniczni -pozosta�e nabycia towar�w i us�ug')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'FIP','zakupy inwestycyjne  -faktura pln ')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'FIW','zakupy inwestycyjne  faktury w walucie obcej')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'F�S','zakupy towar�w i us�ug  dotycz�cych ')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZPP','pracownik -zaliczki na zakup towar�w i us�ug faktura w pln')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZPW','pracownik-zaliczki na zakup towar�w i us�ug faktura w walucie obcej')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZFP','zaliczki na zakup towar�w i us�ug z dzia�alno�ci funduszy strukturalnych faktura PLN')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'ZFW','zaliczki na zakup towar�w i us�ug z dzia�alno�ci funduszy strukturalnych faktura w walucie obcej')
insert into dsg_ul_costinvoice_type (refValue,centrum,available,cn,title) values (null,null,1,'FPP','pracownik  zakup towar�w i us�ug faktura w pln')