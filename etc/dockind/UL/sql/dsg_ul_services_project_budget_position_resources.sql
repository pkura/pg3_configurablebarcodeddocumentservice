drop table dsg_ul_services_project_budget_position_resources;

create table dsg_ul_services_project_budget_position_resources (
id bigint identity(1,1) not null,
available int not null,
etapId bigint null,
jmId bigint null,
kontraktId bigint null,
nazwa varchar(128) null,
nrpoz int null,
pCena numeric(18,4) null,
pIlosc numeric(18,3) null,
pWartosc numeric(18,4) null,
projektId bigint null,
rIlosc numeric(18,3) null,
rWartosc numeric(18,4) null,
rodzajZasobu int null,
typZasobuId bigint null,
wytworId bigint null,
zasobId bigint null);