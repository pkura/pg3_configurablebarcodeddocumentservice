ALTER view [dbo].[dsg_ul_projects_fund_sources] as
select 
f.erp_id as id,
pf.kontrakt_id as refValue, f.title as title, f.cn as cn, f.centrum as centrum, 
f.available *
CASE
--ZGODNIE Z USTALENIAMI ORAZ SYROP #81732
    WHEN LEFT(LTRIM(f.cn),1) = 'S' THEN 0
	WHEN LEFT(LTRIM(f.cn),1) = '9' THEN 0
	WHEN f.cn = '1.01.01.000.99' THEN 0
	WHEN f.cn = '1.90.00.000.00' THEN 0
    ELSE 1
END as available
from dsg_ul_services_projects_fund_sources f
join dsg_ul_services_project2fundsources pf on f.erp_id=pf.zrodlo_id
--join dsg_ul_services_projects p on pf.kontrakt_id=p.erp_id
union
select 
erp_id as id, 
0 as refValue,
title as title,
cn as cn,
 null as centrum,
available *
CASE
--ZGODNIE Z USTALENIAMI ORAZ SYROP #81732
    WHEN LEFT(LTRIM(f.cn),1) = 'S' THEN 0
	WHEN LEFT(LTRIM(f.cn),1) = '9' THEN 0
	WHEN f.cn = '1.01.01.000.99' THEN 0
	WHEN f.cn = '1.90.00.000.00' THEN 0
    ELSE 1
END as available
from dsg_ul_services_projects_fund_sources f;
GO