ALTER view [dbo].[dsg_ul_view_contractor_account] as
select 
acc.id as id, 
acc.id as cn,
acc.bankNazwa+' '+acc.numerKonta as title,
acc.kontobankoweId as centrum,
acc.available as available,
per.id as refValue
from 
dsg_ul_contractor per
join dsg_ul_services_contractor_account acc on per.dostawca_id=acc.dostawcaId;

GO

alter table dsg_ul_costinvoice add contractor bigint;

update dsg_ul_costinvoice set contractor=(select top 1 con.id from dsg_ul_costinvoice c
left join dso_in_document i on c.document_id=i.id
left join dso_person p on i.sender=p.id
left join dsg_ul_contractor con on con.dostawca_id=p.wparam
where dsg_ul_costinvoice.document_id=c.document_id)

update dso_in_document set sender=null where id in (select document_id from  dsg_ul_costinvoice)

delete from dso_person where id in (select i.sender from dsg_ul_costinvoice c
left join dso_in_document i on c.document_id=i.id)


select c.document_id,i.sender,p.*,p.wparam, con.nazwa from dsg_ul_costinvoice c
left join dso_in_document i on c.document_id=i.id
left join dso_person p on i.sender=p.id
left join dsg_ul_contractor con on con.dostawca_id=p.wparam
where con.dostawca_id is null


