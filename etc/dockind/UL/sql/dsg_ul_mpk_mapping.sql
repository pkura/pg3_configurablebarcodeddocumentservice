create table dsg_ul_mpk_mapping (
	id bigint IDENTITY(1,1) NOT NULL,
	ds_div_cn varchar(15) NULL,
	ds_div_name varchar(60) null,
	mpk_cn varchar(15) NULL
);
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0100000000','Wydzia� Filologiczny','2130000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0200000000','Wydzia� Filozoficzno-Historyczny','2140000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0400000000','Wydzia� Biologii i Ochrony �rodowiska','2100000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0500000000','Wydzia� Prawa i Administracji','2190000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0600000000','Wydzia� Ekonomiczno-Socjologiczny','2120000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0700000000','Wydzia� Nauk o Wychowaniu','2180000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('0800000000','Wydzia� Zarz�dzania','2210000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1100000000','Wydzia� Matematyki i Informatyki','2160000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1200000000','Filia U� w Tomaszowie Mazowieckim','2220000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1300000000','Wydzia� Studi�w Mi�dzynarodowych i Politologicznych','2200000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1400000000','Wydzia� Nauk Geograficznych','2170000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1500000000','Wydzia� Fizyki i Informatyki Stosowanej','2150000000');
insert into dsg_ul_mpk_mapping (ds_div_cn,ds_div_name,mpk_cn) values ('1600000000','Wydzia� Chemii','2110000000');