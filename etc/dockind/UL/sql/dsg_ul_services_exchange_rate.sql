create table dsg_ul_services_exchange_rate (
id bigint identity(1,1) not null,
bank_id numeric(18,0) null,
czymod int null,
dataczasoglosz datetime null,
datdo datetime null,
datod datetime null,
kurs numeric(18,8) null,
nominal int null,
tabkurs_id numeric(18,0) null,
tabkurs_idn varchar(50) null,
tolerancja numeric(18,2) null,
typkurs_id numeric(18,0) null,
typkurs__idn varchar(50) null,
waluta_baz_nazwa varchar(50) null,
waluta_baz_nazwgrosz varchar(50) null,
waluta_baz_precyzja int null,
waluta_baz_precyzja_ceny int null,
waluta_baz_symbolwaluty varchar(50) null,
waluta_baz_waluta_id numeric(18,0) null,
waluta_nom_id numeric(18,0) null,
waluta_nom_nazwa varchar(50) null,
waluta_nom_nazwgrosz varchar(50) null,
waluta_nom_precyzja int null,
waluta_nom_precyzja_ceny int null,
waluta_nom_symbolwaluty varchar(50) null,
zrodkurswal_id numeric(18,0) null,
zrodkurswal_idn varchar(50) null);

insert into dsg_ul_services_exchange_rate values ('0','1','2012-12-12',null,'2012-12-12','3.14590000','1','1','241/A/NBP/2012','99.99','1','�redni','Polski z�oty','grosze','2','2','PLN','1','3','dolar ameryka�ski',null,'2','2','USD','1','NBP');
insert into dsg_ul_services_exchange_rate values ('0','1','2012-12-12',null,'2012-12-12','4.09370000','1','1','241/A/NBP/2012','99.99','1','�redni','Polski z�oty','grosze','2','2','PLN','1','9','euro','eurocent','2','2','EUR','1','NBP');
insert into dsg_ul_services_exchange_rate values ('0','1','2012-12-12',null,'2012-12-12','3.37750000','1','1','241/A/NBP/2012','99.99','1','�redni','Polski z�oty','grosze','2','2','PLN','1','11','frank szwajcarski',null,'2','2','CHF','1','NBP');
insert into dsg_ul_services_exchange_rate values ('0','1','2012-12-12',null,'2012-12-12','5.07650000','1','1','241/A/NBP/2012','99.99','1','�redni','Polski z�oty','grosze','2','2','PLN','1','12','funt szterling',null,'2','2','GBP','1','NBP');
