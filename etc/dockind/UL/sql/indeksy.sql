select * into dsg_ul_projects from dsg_ul_view_projects;
select * into dsg_ul_project_budgets from dsg_ul_view_project_budgets;
select * into dsg_ul_organizational_unit from dsg_ul_view_organizational_unit;
select * into dsg_ul_project_osoba_upowazniona from dsg_ul_view_project_osoba_upowazniona;
select * into dsg_ul_project_budget_positions from dsg_ul_view_project_budget_positions;
select * into dsg_ul_project_budget_position_resources from dsg_ul_view_project_budget_position_resources;
select * into dsg_ul_projects_fund_sources from dsg_ul_view_projects_fund_sources;


CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_projects
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_project_budgets
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_project_osoba_upowazniona
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_project_budget_positions
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_project_budget_position_resources
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_organizational_unit
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_projects_fund_sources
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO