create table dsg_ul_services_production_orders (
id bigint identity(1,1) not null,
available numeric(1,0) null,
nazwa varchar(128) null,
zlec_status int null,
zlecprodId bigint null,
zlecprodIdm varchar(128) null
)

CREATE UNIQUE CLUSTERED INDEX [zlecprodId] ON [dbo].[dsg_ul_services_production_orders] 
(
	[zlecprodId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view [dbo].[dsg_ul_view_production_orders] as
select id as id,
available as available,
zlecprodIdm as cn,
nazwa as title,
null as refValue,
zlecprodId as centrum
from dsg_ul_services_production_orders

select * into dsg_ul_production_orders from dsg_ul_view_production_orders;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_production_orders
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
