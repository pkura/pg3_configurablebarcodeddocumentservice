ALTER view [dbo].[pracownicy_to_wydzialy_view] as
select	CAST(CAST(d.ID as varchar(18))+SUBSTRING(CAST(100000+u.id as varchar(6)),2,5) as numeric(18,0)) as id,
		u.NAME as cn,
		u.FIRSTNAME+' '+u.LASTNAME as title,
		d.ID as refValue,
		1 as available,
		null as centrum
from ds_user u
JOIN  DS_USER_TO_DIVISION ud ON ud.USER_ID=u.id
JOIN DS_DIVISION d on ud.DIVISION_ID=d.ID
--JOIN DS_DIVISION d2 on SUBSTRING(d.CODE,1,2)+'00000000'=d2.CODE
where d.DIVISIONTYPE='division'