drop table dsg_ul_services_projects_fund_sources
drop table dsg_ul_services_projects
drop table dsg_ul_services_project_budgets
drop table dsg_ul_services_project_budget_positions
drop table dsg_ul_services_project_budget_position_resources
drop table dsg_ul_services_vat_rates

create table dsg_ul_services_projects_fund_sources ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_projects ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_project_budgets ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_project_budget_positions ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_project_budget_position_resources ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_vat_rates ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 
create table dsg_ul_services_organizational_unit ( 	[cn] [varchar](50) NULL, 	[title] [varchar](255) NULL, 	[centrum] [int] NULL, 	[refValue] [varchar](20) NULL, 	[available] [bit] NULL, 	[erp_id] [numeric](18,0) NULL, 	[id] [bigint] IDENTITY(1,1) NOT NULL, ) 

create view dsg_ul_organizational_unit as
select id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_organizational_unit

create view dsg_ul_vat_rates as
select id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_vat_rates

create view dsg_ul_projects_fund_sources as
select erp_id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_projects_fund_sources

create view dsg_ul_projects as
select erp_id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_projects

create view dsg_ul_project_budgets as
select erp_id as id, cn as cn, title as title, centrum as centrum, refValue as refValue, available as available
from dsg_ul_services_project_budgets

create view dsg_ul_project_budget_positions as
select erp_id as id, cn as cn, title as title, centrum as centrum, refValue as refValue, available as available
from dsg_ul_services_project_budget_positions

create view dsg_ul_project_budget_position_resources as
select erp_id as id, cn as cn, title as title, centrum as centrum, refValue as refValue, available as available
from dsg_ul_services_project_budget_position_resources