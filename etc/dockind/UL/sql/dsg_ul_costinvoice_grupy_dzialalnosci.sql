create table dsg_ul_costinvoice_grupy_dzialalnosci (
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
)

insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'50X','Koszty bezpo�rednie procesu kszta�cenia wg projekt�w')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'51X','Koszty bezpo�rednie procesu badawczego wg projekt�w')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'52X','Koszty po�rednie procesu infrastruktury wspieraj�cej proces kszta�cenia i bada� naukowych')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'53X','Koszty bezpo�rednie procesu infrastruktury technicznej')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'54X','Koszty bezpo�rednie procesu logistyki')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'55X','Koszty procesu zarz�dzania Uczelni�')
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'56X','Koszty procesu marketingu i sprzeda�y')