alter view dsg_ul_project_cost_kind_extended as
select 
CAST(CAST(base.ID as varchar(18))+SUBSTRING(CAST(1000000+rodzaj.id as varchar(7)),2,6) as numeric(18,0)) as id,
base.available,base.centrum,base.cn,base.title,rodzaj.id as refValue from 
dsg_ul_costinvoice_rodzaj_dzialalnosci rodzaj
join dsg_ul_costinvoice_dzialnosci_mapping_table map on rodzaj.cn=map.rodzaj_dzial_idn
full outer join dsg_ul_project_cost_kind base on 1=1
where map.is_rodzaj_kosztu=1
union
select id as id, 1 as available, null as centrum, 'nd' as cn, 'nie dotyczy' as title, id as refValue
from dsg_ul_costinvoice_rodzaj_dzialalnosci