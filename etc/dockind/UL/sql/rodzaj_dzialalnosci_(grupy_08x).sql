insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-0','�rodki trwa�e w budowie (bez aparatury badawczej) - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-1','�rodki trwa�e w budowie-aparatura badawcza - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-2','WNiP w budowie (bez aparatury badawczej) - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-3','WNiP w budowie -aparatura badawcza - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-4','Nak�ady inwestycyjne na du�� infrastruktur� badawcz�  - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-9','Pierwsze wyposa�enie - poza projektami');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-0','�rodki trwa�e w budowie (bez aparatury badawczej) - w ramach projekt�w');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-1','�rodki trwa�e w budowie-aparatura badawcza - w ramach projekt�w');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-2','WNiP w budowie (bez aparatury badawczej) - w ramach projekt�w');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-3','WNiP w budowie -aparatura badawcza - w ramach projekt�w');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-4','Nak�ady inwestycyjne na du�� infrastruktur� badawcz� - w ramach projekt�w');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-9','Pierwsze wyposa�enie - w ramach projekt�w');

insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'080-X','Poza projektami');
insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'081-X','W ramach projekt�w');

update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_grupy_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=cn+': '+title;
update dsg_ul_costinvoice_grupy_dzialalnosci set title=cn+': '+title;

update dsg_ul_costinvoice_rodzaj_dzialalnosci set refValue=g.id
from dsg_ul_costinvoice_grupy_dzialalnosci g
where substring(dsg_ul_costinvoice_rodzaj_dzialalnosci.cn,1,4)=substring(g.cn,1,4);

insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-0',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-1',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-2',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-3',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-4',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('080-X','080-9',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-0',1,1,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-1',1,1,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-2',1,1,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-3',1,1,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-4',1,1,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('081-X','081-9',1,1,0,0,0,0,1);