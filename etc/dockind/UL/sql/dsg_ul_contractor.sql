create table dsg_ul_contractor (
id bigint identity(1,1),
available bit default(1) not null,
dostawca_id bigint,
dostawca_idn varchar(50),
identyfikator_num bigint,
nazwa varchar(300),
nip varchar(50),
miasto varchar(50),
kod_pocztowy varchar(50),
ulica varchar(150),
nr_domu varchar(50),
nr_mieszkania varchar(50)
)
ALTER TABLE dsg_ul_contractor ADD CONSTRAINT PK_dsg_ul_contractor PRIMARY KEY(id);

