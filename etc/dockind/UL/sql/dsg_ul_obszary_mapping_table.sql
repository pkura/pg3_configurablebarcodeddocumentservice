create table dsg_ul_obszary_mapping_table (
id bigint identity(1,1) not null,
c_mpk varchar(50) null,
i_kind varchar(50) null,
c_project varchar(50) null,
c_inventory varchar(50) null,
c_fundsource varchar(50) null,
c_type varchar(50) null,
dsuser int not null,
create_time datetime null,
create_user int null,
modify_time datetime null,
modify_user int null,
remark varchar(max) null
);