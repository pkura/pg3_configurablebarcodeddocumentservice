CREATE TABLE [dbo].[dsg_ul_trip_costs](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[FROM_PLACE] [varchar](50) NULL,
	[FROM_DATE] [datetime] NULL,
	[TO_PLACE] [varchar](50) NULL,
	[TO_DATE] [datetime] NULL,
	[TRANSPORT] [numeric](18, 0) NULL,
	[OPIS] [varchar](249) NULL,
	[KOSZT] [numeric](18, 2) NULL,
	stawka [numeric](19, 4) NULL,
	[ILOSC_KM] [numeric](18, 0) NULL,
) ON [PRIMARY]

GO

delete from dsg_ul_delegation_multiple_value where FIELD_CN='KOSZTY_PODROZY';
drop table [dsg_ul_trip_costs];
