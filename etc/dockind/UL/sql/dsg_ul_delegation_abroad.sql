CREATE TABLE [dbo].[dsg_ul_delegation_abroad](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[nr_wyjazdu] [varchar](150) NULL,
	[wnioskodawca] [int] NULL,
	[worker_division] [int] NULL,
	[dziekan] [int] NULL,
	[delegowany_wybor] [int] NULL,
	[delegowany] [varchar](300) NULL,
	[delegowany_inny] [int] NULL,
	[data_wypelnienia] [datetime] NULL,
	[czy_skierowanie] [tinyint] NULL,
	[cel] [varchar](500) NULL,
	[add_info] [varchar](500) NULL,
	[transport] [int] NULL,
	[transport_desc] [varchar](500) NULL,
	[wyjazd_info] [varchar](500) NULL,
	[czy_urlop] [tinyint] NULL,
	[urlop] [int] NULL,
	[urlop_od] [datetime] NULL,
	[urlop_do] [datetime] NULL,
	[wyjazd_date] [datetime] NULL,
	[przyjazd_date] [datetime] NULL,
	[kalkulacja_kwota] [numeric](19, 2) NULL,
	[zaliczka] [tinyint] NULL,
	[forma_zaliczki] [int] NULL,
	[przelew_na] [varchar](40) NULL,
	[suma_pozycji] [numeric](19, 2) NULL,
	[czas_podr_wyjazd_miasto] [varchar](100) NULL,
	[czas_podr_wyjazd_data] [datetime] NULL,
	[czas_podr_wyjazd_data_gr] [datetime] NULL,
	[czas_podr_powrot_miasto] [varchar](100) NULL,
	[czas_podr_powrot_data] [datetime] NULL,
	[czas_podr_powrot_data_gr] [datetime] NULL,
	[czas_podr_kraj] [varchar](100) NULL,
	[czas_podr_zagr] [varchar](100) NULL,
	[dieta_krajowa_suma] [float] NULL,
	[dieta_krajowa_money] [numeric](19, 2) NULL,
	[dieta_zagraniczna_suma] [float] NULL,
	[dieta_zagraniczna_rozliczenie] [float] NULL,
	[rozliczenie_suma_pln] [numeric](19, 2) NULL,
	[zaplata_czy_nadplata] [int] NULL,
	refundacja_koszty_opis varchar(500),
	refundacja_koszty_czy bit,	
	zakup_biletu_czy bit,
	zakup_biletu numeric(19,0),
	umowa_zal4 numeric(19,0),
	umowa_podroz_samochodem numeric(19,0),
	ewidencja_przebiegu numeric(19,0),
	typ_dokumentu int,
	nr_tel varchar(50),
	nr_dowodu varchar(50),
	email varchar(150),
	kod_pracownika varchar(150),
	adres_zamieszkania varchar(500),
	rachunki_za_nocleg_czy bit,
	rachunki_za_nocleg numeric(19,0),
	zaliczka_walutowa bit,
	zaliczka_walutowa_bank tinyint,
	zaliczka_walutowa_waluta int,
	zaliczka_walutowa_kwota numeric(19,2),
	zaliczka_kwota numeric(19,2),
	rozliczenie_vat tinyint,
	zaliczka_nr_konta numeric(19,0),
	suma_pozycji_roz numeric(17,2),
	rozliczenie_typ tinyint,
	suma_rozliczenia_pln numeric(17,2)
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[dsg_ul_delegation_multiple_value](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[dsg_ul_delegation_diet_dic](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[bool1] [bit] NULL,
	[bool2] [bit] NULL,
	[bool3] [bit] NULL,
	[date] [datetime] NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[dsg_ul_delegation_abroad_country](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[country] [numeric](19, 0) NULL,
	[city] [varchar](50) NULL,
	[organization] [varchar](50) NULL,
	[destination] [numeric](19, 0) NULL
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[dsg_ul_delegation_abroad_dic](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[enum] [numeric](19, 0) NULL,
	[value] [numeric](19, 2) NULL,
	[rate] [numeric](19, 2) NULL,
	[add_rate] [numeric](19, 2) NULL,
	[description] [varchar](500) NULL,
	[money_value] [numeric](18, 2) NULL,
	[money_value_pln] [numeric](18, 2) NULL,
	[currency] [int] NULL,
	[currency_rate] [numeric](18, 4) NULL,
	[currency_rate_date] [datetime] NULL,
	[bool1] [bit] NULL,
	[bool2] [bit] NULL,
	[bool3] [bit] NULL,
	[bool4] [bit] NULL,
	[country] [int] NULL,
	[add_money_value] [numeric](19, 2) NULL,
	[add_money_value_pln] [numeric](19, 2) NULL,
	[add_enum] [numeric](19, 0) NULL,
	[int1] [int] NULL,
	[int2] [int] NULL,
	[int3] [int] NULL,
	[int4] [int] NULL,
	[int5] [int] NULL,
) ON [PRIMARY]

GO

alter table dsg_ul_delegation_abroad
add zaliczka_nr_konta numeric(19,0);

alter table dsg_ul_delegation_abroad
add suma_pozycji_roz numeric(17,2);

alter table dsg_ul_delegation_abroad
add rozliczenie_typ tinyint;

alter table dsg_ul_delegation_abroad
add wyjazd_anulowany bit;

alter table dsg_ul_delegation_abroad
add suma_kosztow_pln numeric(17,2);

CREATE TABLE [dbo].[dsg_ul_delegation_abroad_account_number](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[nr_konta] VARCHAR(100) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[dsg_ul_delegation_abroad_account_dictionary](
	[id] int IDENTITY(1,1) NOT NULL,
	[cn] varchar(50) NOT NULL,
	[title] varchar(250) NULL,
	[refValue] varchar(50) NULL,
	[centrum] int NULL,
	[available] bit
) ON [PRIMARY]

GO

INSERT INTO [dsg_ul_delegation_abroad_account_dictionary] VALUES ('1','95 1240 3028 1978 0010 4461 1227',null,null,1);
INSERT INTO [dsg_ul_delegation_abroad_account_dictionary] VALUES ('1','86 1240 3028 1111 0000 2822 2488',null,null,1);
