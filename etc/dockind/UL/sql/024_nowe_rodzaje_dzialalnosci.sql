update dsg_ul_costinvoice_rodzaj_dzialalnosci set available=0 where cn='503';

insert dsg_ul_costinvoice_rodzaj_dzialalnosci (cn,title,refValue,available,centrum)
values ('506','Koszty kszta�cenia wed�ug projekt�w - konferencje',
(select top 1 id from dsg_ul_costinvoice_grupy_dzialalnosci where cn='50X'),
1,null);

INSERT INTO [dsg_ul_costinvoice_dzialnosci_mapping_table]
           ([grupa_dzial_idn]
           ,[rodzaj_dzial_idn]
           ,[is_slownik_komorek]
           ,[is_numer_projektu]
           ,[is_numer_zlecenia]
           ,[is_numer_inwentarzowy]
           ,[is_oznaczenie_grupy_pomieszczen]
           ,[is_rodzaj_kosztu]
           ,[is_zrodlo_finansowania]
           ,[is_fpmsid_852]
           ,[is_zfss_wydatki]
           ,[is_grupy_mt08]
           ,[is_koszty_rodzajowe_zespol_8]
           ,[is_fpmsid_typ_powierzchni_ds852]
           ,[is_fpmsid_typ_powierzchni]
           ,[is_fpmsid_typ_powierz_stolowki]
           ,[is_fpmsid_grupa_kosztu_stolowki]
           ,[is_fpmsid_grupa_kosztu_ds]
           ,[is_fpmsid_grupa_kosztu_00]
           ,[is_fpmsid_852_1_20]
           ,[is_samochody])
 select [grupa_dzial_idn]
,'506'
,[is_slownik_komorek]
,[is_numer_projektu]
,[is_numer_zlecenia]
,[is_numer_inwentarzowy]
,[is_oznaczenie_grupy_pomieszczen]
,[is_rodzaj_kosztu]
,[is_zrodlo_finansowania]
,[is_fpmsid_852]
,[is_zfss_wydatki]
,[is_grupy_mt08]
,[is_koszty_rodzajowe_zespol_8]
,[is_fpmsid_typ_powierzchni_ds852]
,[is_fpmsid_typ_powierzchni]
,[is_fpmsid_typ_powierz_stolowki]
,[is_fpmsid_grupa_kosztu_stolowki]
,[is_fpmsid_grupa_kosztu_ds]
,[is_fpmsid_grupa_kosztu_00]
,[is_fpmsid_852_1_20]
,[is_samochody]
from dsg_ul_costinvoice_dzialnosci_mapping_table
where rodzaj_dzial_idn = '501';