create table dsg_ul_mpk_to_user 
(
mpk_code_expr varchar(10) not null,
username_external varchar(50) not null
)

insert into dsg_ul_mpk_to_user values 
('1*', 'lidia.kolbus@adm.uni.lodz.pl'), 
('1*', 'ewa.bernas@adm.uni.lodz.pl'), 
('1*', 'teresa.kochanska@adm.uni.lodz.pl'), 
('1*', 'aleksandra.dyszerowicz@adm.uni.lodz.pl'), 
('1*', 'malgorzata.wroblewska@adm.uni.lodz.pl'), 
('1*', 'tadeusz.kurek@adm.uni.lodz.pl'), 
('1*', 'pawel.mokrzycki@ci.uni.lodz.pl');