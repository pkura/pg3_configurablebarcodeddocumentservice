SET IDENTITY_INSERT dsg_ul_budget_position ON;

INSERT INTO dsg_ul_budget_position(id,i_no,i_group,i_kind,i_type,i_amount,i_field,c_mpk,c_user,c_project,c_budget,c_resource,c_position,c_fund_source,c_kind,c_type,c_mt08,c_inventory,c_order,c_group,c_car,s_zfss_wydatki,s_fpmsid_852_20,s_fpmsid_852,s_fpmsid_typ_powierzchni,s_fpmsid_typ_powierzchni_ds852,s_fpmsid_typ_powierz_stolowki,s_koszty_rodzajowe_zespol_8,s_fpmsid_grupa_kosztu_00,s_fpmsid_grupa_kosztu_stolowki,s_fpmsid_grupa_kosztu_ds)
SELECT
id as id,
acceptingcentrumid as c_position,
itemno as i_no,
remark_id as c_kind,
lokalizacja as i_group,
analytics_id as i_kind,
vat_tax_type_id as i_type,
subgroupid as c_mpk,
acceptance_mode as c_user,
accountnumber as c_project,
centrumid as c_budget,
vat_type_id as c_resource,
customsagencyid as c_fund_source,
class_type_id as c_type,
additionalid as c_mt08,
inventoryid as c_inventory,
amount as i_amount,
connected_type_id as i_field,

null as c_order,
null as c_group,
null as c_car,
null as s_zfss_wydatki,
null as s_fpmsid_852_20,
null as s_fpmsid_852,
null as s_fpmsid_typ_powierzchni,
null as s_fpmsid_typ_powierzchni_ds852,
null as s_fpmsid_typ_powierz_stolowki,
null as s_koszty_rodzajowe_zespol_8,
null as s_fpmsid_grupa_kosztu_00,
null as s_fpmsid_grupa_kosztu_stolowki,
null as s_fpmsid_grupa_kosztu_ds

from dsg_centrum_kosztow_faktury

SET IDENTITY_INSERT dsg_ul_budget_position OFF;

update dsg_ul_costinvoice_multiple_value set FIELD_CN='BP' where FIELD_CN='MPK'