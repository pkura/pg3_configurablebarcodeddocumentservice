create view [dbo].[dsg_ul_view_koszty_rodzajowe_zespol] as
select id as id,
available as available,
produktIdn as cn,
produktNazwa as title,
null as refValue,
wytworId as centrum
from dsg_ul_services_product
where 
produktGrupa='z_ZAKUP_FUNDUSZ';

INSERT INTO [dsd_dictionary] ([code] ,[tablename] ,[dict_source] ,[cn] ,[ref_dict] ,[is_control_dict]) VALUES 
('S_KOSZTY_RODZAJOWE_ZESPOL_8','dsg_ul_view_koszty_rodzajowe_zespol',2, 'S_KOSZTY_RODZAJOWE_ZESPOL_8', null, 0);

INSERT INTO [dsd_dictionary_map] ([control_value] ,[dictionary_id] ,[show_all_referenced] ,[must_show])
select map.rodzaj_dzial_idn, (select top 1 id from dsd_dictionary where cn='S_KOSZTY_RODZAJOWE_ZESPOL_8'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_koszty_rodzajowe_zespol_8=1;