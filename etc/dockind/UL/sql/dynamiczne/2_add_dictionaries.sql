INSERT INTO [dsd_dictionary] ([code] ,[tablename], [dockind_cn] ,[dict_source] ,[cn] ,[ref_dict] ,[is_control_dict]) VALUES 
('C_MPK','dsg_ul_organizational_unit', 'ul_costinvoice', 2, 'C_MPK', null, 0),
('C_USER','dsg_ul_project_osoba_upowazniona', 'ul_costinvoice',1, 'C_USER', null, 0),
('C_PROJECT','dsg_ul_projects', 'ul_costinvoice', 2, 'C_PROJECT', null, 0),
('C_BUDGET','dsg_ul_project_budgets', 'ul_costinvoice',2, 'C_BUDGET', null, 0),
('C_POSITION','dsg_ul_project_budget_positions', 'ul_costinvoice',2, 'C_POSITION', null, 0),
('C_RESOURCE','dsg_ul_project_budget_position_resources', 'ul_costinvoice',2, 'C_RESOURCE', null, 0),
('C_FUND_SOURCE','dsg_ul_projects_fund_sources', 'ul_costinvoice',2, 'C_FUND_SOURCE', null, 0),
('C_TYPE','dsg_ul_product', 'ul_costinvoice',2, 'C_TYPE', null, 0),
('C_ORDER','dsg_ul_production_orders', 'ul_costinvoice',2, 'C_ORDER', null, 0)
;

update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_MPK') where cn='C_USER';
update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_MPK') where cn='C_PROJECT';
update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_PROJECT') where cn='C_BUDGET';
update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_BUDGET') where cn='C_POSITION';
update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_POSITION') where cn='C_RESOURCE';
update dsd_dictionary set ref_dict=(select top 1 id from dsd_dictionary where cn='C_RESOURCE') where cn='C_FUND_SOURCE';


INSERT INTO [dsd_dictionary_map] ([control_value] ,[dockind_cn] ,[dictionary_id] ,[show_all_referenced] ,[must_show])
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_MPK'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_slownik_komorek=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_PROJECT'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_projektu=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_BUDGET'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_projektu=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_POSITION'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_projektu=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_RESOURCE'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_projektu=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_TYPE'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_rodzaj_kosztu=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_ORDER'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_zlecenia=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_FUND_SOURCE'),
CASE
	WHEN map.is_numer_projektu=1 THEN 0
	ELSE 1
END
, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_zrodlo_finansowania=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn='C_USER'), 0, 1
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_projektu=0 and map.is_slownik_komorek=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_MT08'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_grupy_mt08=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_SAMOCHODY'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_samochody=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_FPMSID_GRUPA_KOSZTU_DS'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_fpmsid_grupa_kosztu_ds=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_FPMSID_PODMIOT_852'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_fpmsid_852=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_NUMER_INWENTARZOWY'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_numer_inwentarzowy=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_ZFSS_WYDATKI'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_zfss_wydatki=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_FPMSID_TYP_POWIERZCHNI_00'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_fpmsid_typ_powierzchni=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_FPMSID_TYP_POWIERZ_STOLOWKI_852'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_fpmsid_typ_powierz_stolowki=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_KOSZTY_RODZAJ_FUNDUSZ'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_koszty_rodzajowe_zespol_8=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_FPMSID_GRUPA_KOSZTU_STOLOWKI'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_fpmsid_grupa_kosztu_stolowki=1
union
select map.rodzaj_dzial_idn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_KATEGORIE_POWIERZCHNI'), 0, 0
from dsg_ul_costinvoice_dzialnosci_mapping_table map
where map.is_oznaczenie_grupy_pomieszczen=1

INSERT INTO [dsd_dictionary_map] ([control_value] ,[dockind_cn] ,[dictionary_id] ,[show_all_referenced] ,[must_show])
select r.cn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like '%_851ZESP_4'), 0, 0
from dsg_ul_costinvoice_rodzaj_dzialalnosci r
where r.cn like '851%'


select * from dsd_dictionary
select * from dsd_dictionary_map
select * from dsg_ul_costinvoice_dzialnosci_mapping_table


select * from dsg_ul_costinvoice_rodzaj_dzialalnosci



update dsg_ul_costinvoice_rodzaj_dzialalnosci set centrum=k.centrum from dsd_repo187_koszty k where k.cn=dsg_ul_costinvoice_rodzaj_dzialalnosci.cn
update dsg_ul_costinvoice_rodzaj_dzialalnosci set centrum=p.wytworId from dsg_ul_services_product p where p.produktIdn=dsg_ul_costinvoice_rodzaj_dzialalnosci.cn


select * from dsd_dictionary_map map
join dsd_dictionary d on map.dictionary_id=d.id
where map.control_value='533'