ALTER view [dbo].[dsg_ul_document_types] as
select t.id as id,
t.available as available,
t.typdok_ids as cn,
t.nazwa as title,
(
select top 1 rep_atrybut_id from dsd_simple_repository_attribute where objectid=t.typdokzak_id and context_id=6 and rep_klasa_id=187 and available=1
)
 as refValue,
t.typdokzak_id as centrum
from dsg_ul_services_document_types t