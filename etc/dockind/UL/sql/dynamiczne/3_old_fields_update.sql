update dsg_ul_costinvoice set typ_faktury=t.id 
from dsg_ul_document_types t 
left outer join dsg_ul_costinvoice_type told on t.cn=told.cn
where dsg_ul_costinvoice.typ_faktury = told.id

--AKTUALIZACJA POZYCJI

--dsg_ul_mt08	c_mt08	mt08			x	dsd_repo173_mt08

select * from dsg_ul_mt08
select * from dsd_repo173_mt08
select * from dsg_ul_budget_position where c_mt08 is not null
update dsg_ul_budget_position set 
dsd_repo173_mt08 = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo173_mt08 r on a.rep_atrybut_id=r.refValue
join dsg_ul_mt08 d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.c_mt08=d.id;



select * from dsd_repo80_numer_inwentarzowy
select * from dsg_ul_numer_inwentarzowy
select * from dsg_ul_budget_position where c_inventory is not null

update dsg_ul_budget_position set 
dsd_repo80_numer_inwentarzowy = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo80_numer_inwentarzowy r on a.rep_atrybut_id=r.refValue
join dsg_ul_numer_inwentarzowy d on r.cn=d.cn
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.c_inventory=d.id;

select * from dsd_repo110_samochody
select * from dsg_ul_samochody
select * from dsg_ul_budget_position where c_car is not null

update dsg_ul_budget_position set 
dsd_repo110_samochody = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo110_samochody r on a.rep_atrybut_id=r.refValue
join dsg_ul_samochody d on r.cn=d.cn
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.c_car=d.id;


select * from dsd_repo89_zfss_wydatki
select * from dsg_ul_zfss_wydatki
select * from dsg_ul_budget_position where s_zfss_wydatki is not null


update dsg_ul_budget_position set 
dsd_repo89_zfss_wydatki = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo89_zfss_wydatki r on a.rep_atrybut_id=r.refValue
join dsg_ul_zfss_wydatki d on r.cn=d.cn
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_zfss_wydatki=d.id;



select * from dsd_repo119_fpmsid_podmiot_852
select * from dsg_ul_fpmsid_852
select * from dsg_ul_budget_position where s_fpmsid_852 is not null


update dsg_ul_budget_position set 
dsd_repo119_fpmsid_podmiot_852 = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo119_fpmsid_podmiot_852 r on a.rep_atrybut_id=r.refValue
join dsg_ul_fpmsid_852 d on r.cn=d.cn
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_fpmsid_852=d.id;


select * from dsd_repo127_fpmsid_typ_powierzchni_00
select * from dsg_ul_fpmsid_typ_powierzchni
select * from dsg_ul_budget_position where s_fpmsid_typ_powierzchni is not null

update dsg_ul_budget_position set 
dsd_repo127_fpmsid_typ_powierzchni_00 = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo127_fpmsid_typ_powierzchni_00 r on a.rep_atrybut_id=r.refValue
join dsg_ul_fpmsid_typ_powierzchni d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_fpmsid_typ_powierzchni=d.id;



select * from dsd_repo125_fpmsid_typ_powierz_stolowki_852
select * from dsg_ul_fpmsid_typ_powierz_stolowki
select * from dsg_ul_budget_position where s_fpmsid_typ_powierz_stolowki is not null

update dsg_ul_budget_position set 
dsd_repo125_fpmsid_typ_powierz_stolowki_852 = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo125_fpmsid_typ_powierz_stolowki_852 r on a.rep_atrybut_id=r.refValue
join dsg_ul_fpmsid_typ_powierz_stolowki d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_fpmsid_typ_powierz_stolowki=d.id;

select * from dsd_repo190_koszty_rodzaj_fundusz
select * from dsg_ul_koszty_rodzajowe_zespol_8
select * from dsg_ul_budget_position where s_koszty_rodzajowe_zespol_8 is not null

update dsg_ul_budget_position set 
dsd_repo190_koszty_rodzaj_fundusz = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo190_koszty_rodzaj_fundusz r on a.rep_atrybut_id=r.refValue
join dsg_ul_koszty_rodzajowe_zespol_8 d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_koszty_rodzajowe_zespol_8=d.id;


select * from dsd_repo126_fpmsid_grupa_kosztu_stolowki
select * from dsg_ul_fpmsid_grupa_kosztu_stolowki
select * from dsg_ul_budget_position where s_fpmsid_grupa_kosztu_stolowki is not null

update dsg_ul_budget_position set 
dsd_repo126_fpmsid_grupa_kosztu_stolowki = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo126_fpmsid_grupa_kosztu_stolowki r on a.rep_atrybut_id=r.refValue
join dsg_ul_fpmsid_grupa_kosztu_stolowki d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_fpmsid_grupa_kosztu_stolowki=d.id;


select * from dsd_repo123_fpmsid_grupa_kosztu_ds
select * from dsg_ul_fpmsid_grupa_kosztu_ds
select * from dsg_ul_budget_position where s_fpmsid_grupa_kosztu_ds is not null and s_fpmsid_grupa_kosztu_ds <> -1

update dsg_ul_budget_position set 
dsd_repo123_fpmsid_grupa_kosztu_ds = r.id
from dsg_ul_costinvoice_multiple_value mu
join dsg_ul_costinvoice c on mu.DOCUMENT_ID=c.document_id
join dsg_ul_document_types t on c.typ_faktury=t.id
join dsd_simple_repository_attribute a on t.centrum=a.objectid
join dsd_repo123_fpmsid_grupa_kosztu_ds r on a.rep_atrybut_id=r.refValue
join dsg_ul_fpmsid_grupa_kosztu_ds d on r.cn=RIGHT('0'+d.cn,2)
where mu.FIELD_VAL=dsg_ul_budget_position.id and mu.FIELD_CN='BP' and dsg_ul_budget_position.s_fpmsid_grupa_kosztu_ds=d.id;