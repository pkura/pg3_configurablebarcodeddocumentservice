CREATE TABLE [dsg_stawki_vat_faktury](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[pozycjaid] [numeric](18, 0) NULL,
	[blokada] [numeric](18, 0) NULL,
	[pozycja] [numeric](18, 0) NULL,
	[netto] [numeric](18, 2) NULL,
	[stawka] [numeric](18, 0) NULL,
	[kwota_vat] [numeric](18, 2) NULL,
	[dzialalnosc] [numeric](18, 0) NULL,
	[obciazenie] [numeric](18, 2) NULL
) ON [PRIMARY]