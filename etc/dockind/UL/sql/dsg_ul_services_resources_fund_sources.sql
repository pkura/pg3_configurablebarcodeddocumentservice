create table dsg_ul_services_resources_fund_sources (
id bigint identity(0,1) not null,
bpZasobZrodlaId numeric(18,0) null,
identyfikatorNum numeric(18,0) null,
pKoszt numeric(18,2) null,
rKoszt numeric(18,2) null,
wspProc numeric(18,2) null,
zasobId numeric(18,0) null,
zrodloId numeric(18,0) null,
zrodloIdn varchar(50) null,
zrodloNazwa varchar(128) null);