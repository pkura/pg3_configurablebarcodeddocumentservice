begin transaction;
DECLARE @inserted_ids TABLE ([id] INT, [name] varchar(50));
insert into ds_user (NAME,FIRSTNAME,LASTNAME,LOGINDISABLED, DELETED, CERTIFICATELOGIN, HASHEDPASSWORD, SYSTEMUSER) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids select REPLACE(NAME,'sylwia_kobza','dorota_cieslak') as NAME, REPLACE(FIRSTNAME,'Sylwia','Dorota') as FIRSTNAME, 'Cie�lak' as LASTNAME, LOGINDISABLED, DELETED, CERTIFICATELOGIN, HASHEDPASSWORD, SYSTEMUSER from ds_user where LASTNAME ='Kobza';
select * from @inserted_ids;
rollback;

insert into DS_USER_TO_DIVISION (source,DIVISION_ID,USER_ID) 
select 'Administrator' as source, ud.division_id, u2.id as user_id from DS_USER_TO_DIVISION ud
join ds_user u ON ud.USER_ID=u.ID
join ds_user u2 ON REPLACE(REPLACE(u2.FIRSTNAME,'Sylwia',''),'Dorota','Sylwia')=u.FIRSTNAME
where u.LASTNAME='Kobza'