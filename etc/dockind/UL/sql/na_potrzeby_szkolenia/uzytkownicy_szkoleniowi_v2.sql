begin transaction; 

begin try 
--KANCELARIA
DECLARE @inserted_div_ids TABLE ([id] INT);
insert into DS_DIVISION (GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN) OUTPUT INSERTED.[id] INTO @inserted_div_ids values ('SZKOLENIE','SZKOLENIE',null,'division',1,0);


DECLARE @inserted_ids TABLE ([id] INT, [name] varchar(50));
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_kancelaria',	'KANCELARIA - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_kancelaria',	'KANCELARIA - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_kancelaria',	'KANCELARIA - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_kancelaria',	'KANCELARIA - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_kancelaria',	'KANCELARIA - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_kancelaria',	'KANCELARIA - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_kancelaria',	'KANCELARIA - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_kancelaria',	'KANCELARIA - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_kancelaria',	'KANCELARIA - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_kancelaria',	'KANCELARIA - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_kancelaria',	'KANCELARIA - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_kancelaria',	'KANCELARIA - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;


delete from @inserted_ids;
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_upowazniona',	'UPOWAZNIONA - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_upowazniona',	'UPOWAZNIONA - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_upowazniona',	'UPOWAZNIONA - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_upowazniona',	'UPOWAZNIONA - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_upowazniona',	'UPOWAZNIONA - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_upowazniona',	'UPOWAZNIONA - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_upowazniona',	'UPOWAZNIONA - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_upowazniona',	'UPOWAZNIONA - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_upowazniona',	'UPOWAZNIONA - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_upowazniona',	'UPOWAZNIONA - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_upowazniona',	'UPOWAZNIONA - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_upowazniona',	'UPOWAZNIONA - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;



delete from @inserted_ids;
--WYDZIAL SZKOLENIOWY
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_upkw',	'UPKW - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_upkw',	'UPKW - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_upkw',	'UPKW - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_upkw',	'UPKW - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_upkw',	'UPKW - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_upkw',	'UPKW - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_upkw',	'UPKW - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_upkw',	'UPKW - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_upkw',	'UPKW - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_upkw',	'UPKW - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_upkw',	'UPKW - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_upkw',	'UPKW - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--SEKRETARIAT
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_sekretariat',	'SEKRETARIAT - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_sekretariat',	'SEKRETARIAT - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_sekretariat',	'SEKRETARIAT - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_sekretariat',	'SEKRETARIAT - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_sekretariat',	'SEKRETARIAT - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_sekretariat',	'SEKRETARIAT - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_sekretariat',	'SEKRETARIAT - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_sekretariat',	'SEKRETARIAT - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_sekretariat',	'SEKRETARIAT - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_sekretariat',	'SEKRETARIAT - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_sekretariat',	'SEKRETARIAT - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_sekretariat',	'SEKRETARIAT - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--OBSZAR DYDAKTYKA
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_dydaktyka',	'DYDAKTYKA - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_dydaktyka',	'DYDAKTYKA - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_dydaktyka',	'DYDAKTYKA - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_dydaktyka',	'DYDAKTYKA - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_dydaktyka',	'DYDAKTYKA - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_dydaktyka',	'DYDAKTYKA - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_dydaktyka',	'DYDAKTYKA - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_dydaktyka',	'DYDAKTYKA - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_dydaktyka',	'DYDAKTYKA - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_dydaktyka',	'DYDAKTYKA - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_dydaktyka',	'DYDAKTYKA - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_dydaktyka',	'DYDAKTYKA - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--OBSZAR NAUKOWE
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_naukowe',	'NAUKOWE - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_naukowe',	'NAUKOWE - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_naukowe',	'NAUKOWE - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_naukowe',	'NAUKOWE - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_naukowe',	'NAUKOWE - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_naukowe',	'NAUKOWE - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_naukowe',	'NAUKOWE - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_naukowe',	'NAUKOWE - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_naukowe',	'NAUKOWE - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_naukowe',	'NAUKOWE - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_naukowe',	'NAUKOWE - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_naukowe',	'NAUKOWE - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--OBSZAR UE
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_ue',	'UE - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_ue',	'UE - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_ue',	'UE - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_ue',	'UE - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_ue',	'UE - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_ue',	'UE - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_ue',	'UE - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_ue',	'UE - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_ue',	'UE - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_ue',	'UE - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_ue',	'UE - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_ue',	'UE - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--KWESTOR
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_kwestor',	'KWESTOR - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_kwestor',	'KWESTOR - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_kwestor',	'KWESTOR - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_kwestor',	'KWESTOR - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_kwestor',	'KWESTOR - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_kwestor',	'KWESTOR - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_kwestor',	'KWESTOR - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_kwestor',	'KWESTOR - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_kwestor',	'KWESTOR - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_kwestor',	'KWESTOR - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_kwestor',	'KWESTOR - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_kwestor',	'KWESTOR - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--KANCLERZ
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_kanclerz',	'KANCLERZ - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_kanclerz',	'KANCLERZ - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_kanclerz',	'KANCLERZ - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_kanclerz',	'KANCLERZ - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_kanclerz',	'KANCLERZ - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_kanclerz',	'KANCLERZ - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_kanclerz',	'KANCLERZ - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_kanclerz',	'KANCLERZ - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_kanclerz',	'KANCLERZ - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_kanclerz',	'KANCLERZ - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_kanclerz',	'KANCLERZ - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_kanclerz',	'KANCLERZ - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;

delete from @inserted_ids;
--PROJEKT
insert into ds_user (LOGINDISABLED,DELETED,CERTIFICATELOGIN,SYSTEMUSER,HASHEDPASSWORD,NAME,FIRSTNAME,LASTNAME) OUTPUT INSERTED.[id], INSERTED.[NAME] INTO @inserted_ids values 
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_bryl_projekt',	'PROJEKT - Ewa',	'Bryl'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','karolina_walecka_projekt',	'PROJEKT - Karolina',	'Walecka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','wioletta_kosatka_projekt',	'PROJEKT - Wioletta',	'Kosatka'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','joanna_gryzewska_projekt',	'PROJEKT - Joanna',	'Gryzewska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_pisula_projekt',	'PROJEKT - Marta',	'Pisula'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','marta_janikowska_projekt',	'PROJEKT - Marta',	'Janikowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','katarzyna_zajaczkowska_projekt',	'PROJEKT - Katarzyna',	'Zajaczkowska'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','kamila_nieckarz_projekt',	'PROJEKT - Kamila',	'Nieckarz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','sylwia_kobza_projekt',	'PROJEKT - Sylwia',	'Kobza'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','ewa_lisiewicz_projekt',	'PROJEKT - Ewa',	'Lisiewicz'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','alicja_kurczewska-lefik_projekt',	'PROJEKT - Alicja',	'Kurczewska-Lefik'),
(0,0,0,0,'{SHA}NDLT4zESwLe4p1mkltDR3jUgot4=','radomir_dziubich_projekt',	'PROJEKT - Radomir',	'Dziubich');
insert into DS_USER_TO_DIVISION (DIVISION_ID,USER_ID,source) select (select top 1 id from @inserted_div_ids) as DIVISION_ID, id as USER_ID, 'Administrator' as source from @inserted_ids;
insert into DSO_role_usernames (role_id,username,source) select 5 as role_id, name as username, 'Administrator' as source from @inserted_ids;


commit;
select 'COMMMIT';
end try
begin catch
rollback;
select 'ROLLBACK';
end catch