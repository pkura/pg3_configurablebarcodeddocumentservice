drop table dsr_currency_to_country;
create table dsr_currency_to_country
(country varchar(4) not null,
currency varchar(3) not null);

insert into dsr_currency_to_country
select cn, 'USD' from dsr_country;

insert into dsr_currency_to_country values
('AF','AFN'),('AL','ALL'),('DZ','DZD'),('AS','USD'),('AD','EUR'),('AO','AOA'),('AI','XCD'),('AG','XCD'),('AR','ARS'),('AM','AMD'),('AW','AWG'),('AU','AUD'),('AT','EUR'),('AZ','AZN'),('BS','BSD'),('BH','BHD'),('BD','BDT'),('BB','BBD'),('BE','EUR'),('BZ','BZD'),('BJ','XOF'),('BM','BMD'),('BT','BTN'),('BO','BOB'),('BW','BWP'),('BV','NOK'),('BR','BRL'),('IO','GBP'),('VG','USD'),('BN','BND'),('BG','BGN'),('BF','XOF'),('BI','BIF'),('KH','KHR'),('CM','XAF'),('CA','CAD'),('CV','CVE'),('KY','KYD'),('CF','XAF'),('TD','XAF'),('CL','CLP'),('CN','CNY'),('CX','AUD'),('CC','AUD'),('CO','COP'),('KM','KMF'),('CG','XAF'),('CK','NZD'),('CR','CRC'),('HR','HRK'),('CU','CUC'),('CY','EUR'),('CZ','CZK'),('DK','DKK'),('DJ','DJF'),('DM','XCD'),('DO','DOP'),('TP','IDR'),('EC','USD'),('EG','EGP'),('SV','USD'),('ER','ERN'),('EE','EUR'),('ET','ETB'),('EU','EUR'),('FK','FKP'),('FO','DKK'),('FJ','FJD'),('FI','EUR'),('FR','EUR'),('FR','EUR'),('GF','EUR'),('PF','XPF'),('TF','EUR'),('GA','XAF'),('GM','GMD'),('GE','GEL'),('DE','EUR'),('GH','GHS'),('GI','GIP'),('UK','GBP'),('GR','EUR'),('GL','DKK'),('GD','XCD'),('GP','EUR'),('GU','USD'),('GT','GTQ'),('GN','GNF'),('GW','XAF'),('GY','GYD'),('HT','HTG'),('HM','AUD'),('HN','HNL'),('HK','HKD'),('HU','HUF'),('IS','ISK'),('IN','INR'),('ID','IDR'),('IR','IRR'),('IQ','IQD'),('IE','EUR'),('IL','ILS'),('IT','EUR'),('CI','XOF'),('JM','JMD'),('JP','JPY'),('JO','JOD'),('KZ','KZT'),('KE','KES'),('KI','AUD'),('KW','KWD'),('KG','KGS'),('LA','LAK'),('LV','LVL'),('LB','LBP'),('LS','LSL'),('LR','LRD'),('LY','LYD'),('LI','CHF'),('LT','LTL'),('LU','EUR'),('MO','MOP'),('MK','MKD'),('MG','MGA'),('MW','MWK'),('MY','MYR'),('MV','MVR'),('ML','XOF'),('MT','EUR'),('MQ','EUR'),('MR','MRO'),('MU','MUR'),('YT','EUR'),('MX','MXN'),('FM','USD'),('MD','MDL'),('MC','EUR'),('MN','MNT'),('MS','XCD'),('MA','MAD'),('MZ','MZN'),('MM','MMK'),('NA','NAD'),('NR','AUD'),('NP','NPR'),('AN','ANG'),('NL','EUR'),('NC','XPF'),('NZ','NZD'),('NI','NIO'),('NE','XOF'),('NG','NGN'),('NU','NZD'),('NF','AUD'),('KP','KPW'),('NO','NOK'),('OM','OMR'),('PK','PKR'),('PW','USD'),('PA','PAB'),('PG','PGK'),('PY','PYG'),('PE','PEN'),('PH','PHP'),('PN','NZD'),('PL','PLN'),('PT','EUR'),('PR','USD'),('QA','QAR'),('RE','EUR'),('RO','RON'),('RU','RUB'),('RW','RWF'),('LC','XCD'),('PM','EUR'),('SM','EUR'),('SA','SAR'),('SN','XOF'),('SC','SCR'),('SL','SLL'),('SG','SGD'),('SK','EUR'),('SI','EUR'),('SB','SBD'),('SO','SOS'),('ZA','ZAR'),('KR','KRW'),('RU','RUB'),('ES','EUR'),('LK','LKR'),('SH','SHP'),('ST','STD'),('KN','XCD'),('VC','XCD'),('SD','SDG'),('SR','SRD'),('SJ','NOK'),('SZ','SZL'),('SE','SEK'),('CH','CHF'),('SY','SYP'),('TW','TWD'),('TZ','TZS'),('TH','THB'),('TG','XOF'),('TK','NZD'),('TO','TOP'),('TT','TTD'),('TN','TND'),('TR','TRY'),('TM','TMT'),('TC','USD'),('TV','AUD'),('UG','UGX'),('UA','UAH'),('AE','AED'),('US','USD'),('UY','UYU'),('UM','USD'),('VI','USD'),('UZ','UZS'),('VU','VUV'),('VA','EUR'),('VE','VEF'),('VN','VND'),('WF','XPF'),('EH','ESP'),('EH','MAD'),('EH','MRO'),('WS','WST'),('YE','YER'),('YU','YUN'),('ZR','CDF'),('ZM','ZMK'),('ZW','USD');

select * from dsr_currency_to_country;

create view dsg_ul_currency_mapped as
select 
r.id as id,
r.cn as cn,
r.title as title,
r.centrum as centrum,
r.available as available,
c.id as refValue
from dsg_ul_exchange_rate r
left outer join dsr_currency_to_country map on r.cn=map.currency
join dsr_country c on map.country=c.cn;