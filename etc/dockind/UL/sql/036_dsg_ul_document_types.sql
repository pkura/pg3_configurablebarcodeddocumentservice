alter view [dbo].[dsg_ul_document_types] as
select t.id as id,
t.available as available,
t.typdok_ids as cn,
t.nazwa as title,
a.rep_atrybut_id as refValue,
t.typdokzak_id as centrum
from dsg_ul_services_document_types t
left outer join dsd_simple_repository_attribute a on t.typdokzak_id=a.objectid and a.rep_klasa_id=187