alter table dsg_ul_costinvoice_dzialnosci_mapping_table
add is_koszty_rodzajowe_zespol_8 int not null default 0;

CREATE VIEW dsg_ul_view_koszty_rodzajowe_zespol_8
AS
SELECT 
CAST(idn as varchar(18)) AS cn, 
id AS id,
nazwa AS title, 
null AS refValue, 
null AS centrum, 
available AS available
FROM dsg_ul_services_koszty_rodzajowe_zespol_8
UNION
SELECT 'ND' AS cn, - 1 AS id, 'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum, 1 AS available

select * into dsg_ul_koszty_rodzajowe_zespol_8 from dsg_ul_view_koszty_rodzajowe_zespol_8;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_koszty_rodzajowe_zespol_8
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
update dsg_ul_costinvoice_dzialnosci_mapping_table set is_koszty_rodzajowe_zespol_8=1 where rodzaj_dzial_idn  in ('852-3-20','852-3-21','852-3-60');