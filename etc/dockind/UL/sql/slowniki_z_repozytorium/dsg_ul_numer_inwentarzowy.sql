CREATE VIEW dsg_ul_view_numer_inwentarzowy
AS
SELECT 
CAST(idn as varchar(18)) AS cn, 
id AS id,
nazwa AS title, 
null AS refValue, 
null AS centrum, 
available AS available
FROM dsg_ul_services_numer_inwentarzowy
UNION
SELECT 'ND' AS cn, - 1 AS id, 'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum, 1 AS available

select * into dsg_ul_numer_inwentarzowy from dsg_ul_view_numer_inwentarzowy;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_numer_inwentarzowy
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
