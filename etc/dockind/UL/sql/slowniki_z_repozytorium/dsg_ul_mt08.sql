alter table dsg_ul_costinvoice_dzialnosci_mapping_table
add is_grupy_mt08 int null;

CREATE VIEW dsg_ul_view_mt08
AS
SELECT 
CAST(idn as varchar(18)) AS cn, 
id AS id,
nazwa AS title, 
null AS refValue, 
null AS centrum, 
available AS available
FROM dsg_ul_services_mt08
UNION
SELECT 'ND' AS cn, - 1 AS id, 'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum, 1 AS available

select * into dsg_ul_mt08 from dsg_ul_view_mt08;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_mt08
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

update dsg_ul_costinvoice_dzialnosci_mapping_table set is_grupy_mt08=0
update dsg_ul_costinvoice_dzialnosci_mapping_table set is_grupy_mt08=1 where grupa_dzial_idn like '08%'