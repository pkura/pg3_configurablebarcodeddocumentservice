create view dsg_ul_kwestor_upowazniony as
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
null as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
where a.cn='kwestor_upowazniony'


create table dsg_ul_costinvoice_kwestor_upowazniony (
id int identity(1,1) not null,
UPOWAZNIONY int null);