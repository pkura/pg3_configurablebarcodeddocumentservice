ALTER view [dbo].[dsg_ul_dziekan] as
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
w.id as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
join DS_USER_TO_DIVISION ud on u.id=ud.USER_ID
join DS_DIVISION d on ud.DIVISION_ID=d.ID
join wydzialy_view w on SUBSTRING(d.CODE,1,2)=SUBSTRING(w.title,1,2)
where a.cn='dean' and SUBSTRING(w.title,1,2)<>'35'
--DLA BIBLIOTEK WYDZIALOWYCH
--Zgodnie z ustaleniami przesyłam powiązania kodów wydziału z kodami bibliotek wydziałowych. 1i2 cyfra kodu wydziału odpowiada 5i6 cyfrze kodu bibliotek wydziałowych tj.:
union
select u.ID as id,
a.username as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
w.id as refValue,
(1-u.deleted) as available
 from ds_acceptance_condition a
join ds_user u on a.username=u.NAME
join DS_USER_TO_DIVISION ud on u.id=ud.USER_ID
join DS_DIVISION d on ud.DIVISION_ID=d.ID
join wydzialy_view w on SUBSTRING(d.CODE,1,2)=SUBSTRING(w.title,5,2)
where a.cn='dean' and SUBSTRING(w.title,1,2)='35'
union
select ID as id,
name as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
null as centrum,
null as refValue,
0 as available
 from ds_user u