create table dsg_ul_services_document_types (
id int identity(1,1) not null,
available numeric(1,0) not null,
czy_aktywny int null,
czywal int null,
nazwa varchar(128) null,
typdok_idn varchar(128) null,
typdok_ids varchar(128) null,
typdokzak_id bigint null
);

CREATE CLUSTERED INDEX [c-typdokzak_id] ON [dbo].[dsg_ul_services_document_types] 
(
	[typdokzak_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

create view [dbo].[dsg_ul_document_types] as
select id as id,
available as available,
typdok_ids as cn,
nazwa as title,
null as refValue,
typdokzak_id as centrum
from dsg_ul_services_document_types