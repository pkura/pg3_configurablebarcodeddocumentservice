alter table dsg_ul_costinvoice
add typy_dzialalnosci numeric(18,0) null;
alter table dsg_ul_costinvoice
add pracownik numeric(18,0) null;
alter table dsg_ul_costinvoice
add pracownik_jednostka numeric(18,0) null;
alter table dsg_ul_costinvoice
add pracownik_nr_konta varchar(100) null;
alter table dsg_ul_costinvoice
add kwota_pozostala_mpk numeric(18,2) null;
alter table dsg_ul_costinvoice
add wydzial_dodatkowy numeric(18,0) null;
alter table dsg_ul_costinvoice
add tryb_zamowienia numeric(18,0) null;

alter table dsg_ul_costinvoice
add wydatek_strukturalny numeric(18,0) null;
alter table dsg_ul_costinvoice
add wydatek_kwota numeric(18,2) null;
alter table dsg_ul_costinvoice
add wydatek_kod varchar(100) null;
alter table dsg_ul_costinvoice
add WYDATEK_OBSZAR varchar(100) null;