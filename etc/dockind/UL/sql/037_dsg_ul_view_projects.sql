ALTER VIEW [dbo].[dsg_ul_view_projects]
AS
SELECT     proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, proj.available AS available
FROM         dsg_ul_services_projects proj JOIN
                      dsg_ul_services_organizational_unit unit ON ( SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2))
--WHERE     SUBSTRING(unit.cn, 1, 1) = '2'
/* pierwsze cyfra: 2 - oznacza wydzia�y*/ UNION
SELECT     'ND' AS cn, - 1 AS id, 'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum, 1 AS available