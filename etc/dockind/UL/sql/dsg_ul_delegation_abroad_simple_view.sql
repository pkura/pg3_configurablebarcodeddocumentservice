CREATE view [dbo].[dsg_ul_delegation_abroad_simple_view] 
with SCHEMABINDING
as
select doc.id as document_id, doc.id, a.nr_wyjazdu from dbo.ds_document doc
join dbo.dsg_ul_delegation_abroad a on doc.ID=a.DOCUMENT_ID
where doc.deleted<>1;
CREATE UNIQUE CLUSTERED INDEX dsg_ul_delegation_abroad_simple_view_INDEX 
    ON [dbo].[dsg_ul_delegation_abroad_simple_view] (document_id);
GO