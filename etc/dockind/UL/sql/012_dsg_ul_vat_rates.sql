ALTER view dsg_ul_vat_rates as
select id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_vat_rates 
EXCEPT
select top 1 id as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_vat_rates where cn = '0'
UNION
select top 1 0 as id, cn as cn, title as title, centrum as centrum, null as refValue, available as available
from dsg_ul_services_vat_rates where cn = '0'
GO