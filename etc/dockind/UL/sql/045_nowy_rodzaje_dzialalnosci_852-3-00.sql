insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) 
values ((select id from dsg_ul_costinvoice_grupy_dzialalnosci_base where cn='852-X'),1,1256,'852-3-00','Remont DS z dotacji');

INSERT INTO [dsd_dictionary_map] ([control_value] ,[dockind_cn] ,[dictionary_id] ,[show_all_referenced] ,[must_show])
select cn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like 'DSD_REPO119_FPMSID_PODMIOT_852'), 0, 0
from dsg_ul_costinvoice_rodzaj_dzialalnosci
where cn='852-3-00'
union
select cn, 'ul_costinvoice', (select top 1 id from dsd_dictionary where cn like 'DSD_REPO190_KOSZTY_RODZAJ_FUNDUSZ'), 0, 0
from dsg_ul_costinvoice_rodzaj_dzialalnosci
where cn='852-3-00'