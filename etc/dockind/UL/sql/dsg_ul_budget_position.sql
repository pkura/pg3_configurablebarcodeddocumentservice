create table dsg_ul_budget_position (
id BIGINT IDENTITY(1,1) NOT NULL,

--item
i_no INT NULL,
i_group INT NULL,
i_kind INT NULL,
i_type INT NULL,
i_amount FLOAT NULL,
i_field INT NULL,

--cost
c_mpk INT NULL,
c_user INT NULL,
c_project INT NULL,
c_budget INT NULL,
c_resource INT NULL,
c_resource_raw varchar(256) NULL,
c_position INT NULL,
c_fund_source INT NULL,
c_kind INT NULL,
c_type INT NULL,
c_mt08 INT NULL,
c_inventory INT NULL,
c_order INT NULL,
c_group INT NULL,
c_car INT NULL,


--social
s_zfss_wydatki INT NULL,
s_fpmsid_852_20 INT NULL,
s_fpmsid_852 INT NULL,
s_fpmsid_typ_powierzchni INT NULL,
s_fpmsid_typ_powierzchni_ds852 INT NULL,
s_fpmsid_typ_powierz_stolowki INT NULL,
s_koszty_rodzajowe_zespol_8 INT NULL,
s_fpmsid_grupa_kosztu_00 INT NULL,
s_fpmsid_grupa_kosztu_stolowki INT NULL,
s_fpmsid_grupa_kosztu_ds INT NULL,
s_kind INT NULL
 CONSTRAINT [PK_dsg_ul_invoice_position] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

alter table dsg_ul_budget_position add c_structure_budget_position_raw varchar(256);
alter table dsg_ul_budget_position add b_reservation_code varchar(128);
alter table dsg_ul_budget_position add b_reservation_position_id numeric(19,0);