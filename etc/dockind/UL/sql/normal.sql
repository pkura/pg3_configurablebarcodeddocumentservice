CREATE TABLE [dsg_rodzaj_ul_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	
);

CREATE TABLE [dsg_rodzaj_ul_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

CREATE TABLE [dsg_rodzaj_ul_INT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);


alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add signature varchar(150);
alter table DSG_NORMAL_DOCKIND add NADAWCA numeric(18,0);
alter table DSG_NORMAL_DOCKIND add PAPIER bit default 0;
alter table DSG_NORMAL_DOCKIND add STATUS numeric(18,0);
alter table DSG_NORMAL_DOCKIND add typ tinyint;
alter table DSG_NORMAL_DOCKIND add osoba_prowadzaca int;
alter table DSG_NORMAL_DOCKIND add tytul_prowadzenia varchar(150);
alter table DSG_NORMAL_DOCKIND add dysponent varchar(150);
alter table DSG_NORMAL_DOCKIND add przedmiot varchar(150);
alter table dsg_normal_dockind add original bit;


insert into dsg_rodzaj_ul_in(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_ul_in(cn,title,available)values('ZAMOWIENIA','Zamówienia',1);
insert into dsg_rodzaj_ul_in(cn,title,available)values('ODPOWIEDZ','Odpowiedzi na zapytania ofertowe',1);
insert into dsg_rodzaj_ul_in(cn,title,available)values('Z_JEDNOSTEK','Pisma z jednostek nadrzędnych',1);
insert into dsg_rodzaj_ul_in(cn,title,available)values('WNIOSEK_STUDENTA','Wniosek studenta',1);
insert into dsg_rodzaj_ul_in(cn,title,available)values('INNE','Pozostałe pisma',1);

insert into dsg_rodzaj_ul_out(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_ul_out(cn,title,available)values('DO_JEDNOSTEK','Pisma do jednostek nadrzędnych',1);
insert into dsg_rodzaj_ul_out(cn,title,available)values('DO_STUDENTOW','Pisma do studentów',1);
insert into dsg_rodzaj_ul_out(cn,title,available)values('INNE','Pozostała  korespondencja ',1);

insert into dsg_rodzaj_ul_INT(cn,title,available)values('NOTATKA','Notatka służbowa',1);
insert into dsg_rodzaj_ul_INT(cn,title,available)values('PODANIA','Podania',1);
