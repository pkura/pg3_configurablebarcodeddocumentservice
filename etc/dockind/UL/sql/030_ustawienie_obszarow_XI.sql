--zmiana zgodnie z mailem "Fwd: zmiana osoby upowaznionej -CSK Rogowska" z 13.03.2013

update dsg_ul_obszary_mapping_table set dsuser=(select top 1 id from ds_user where name='alinarolczak') where c_mpk=601;
insert into dsg_ul_obszary_mapping_table
select c_mpk as c_mpk, i_kind as i_kind, c_project as c_project, (select top 1 id from ds_user where name='agnieszkanaroznik') as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark, null as c_type, null as c_inventory 
from dsg_ul_obszary_mapping_table m
where m.c_mpk=601;