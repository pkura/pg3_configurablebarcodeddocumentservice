ALTER view [dbo].[dsg_ul_view_project_osoba_upowazniona] as
select
u.id as id,
u.NAME as cn,
u.FIRSTNAME+' '+u.LASTNAME as title,
org.id as refValue,
(1-u.DELETED) as available,
null as centrum
from ds_user u
join DS_USER_TO_DIVISION u2d on u.ID=u2d.USER_ID
join DS_DIVISION div on u2d.DIVISION_ID=div.ID
join dsg_ul_mpk_mapping map on div.CODE=map.ds_div_cn
join dsg_ul_organizational_unit org on map.mpk_cn=org.cn
union
select 
u.id as id, 
u.name as cn,
u.FIRSTNAME+' '+u.LASTNAME as title, 
0 as refValue,
(1-u.DELETED) as available,
null as centrum
from ds_user u