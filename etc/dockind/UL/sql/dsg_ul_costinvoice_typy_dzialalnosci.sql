create table dsg_ul_costinvoice_typy_dzialalnosci (
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
)
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x1','Rozw�j specjalno�ci (Art.18 ust.1 p.1 pp.a ustawy o finansowaniu nauki)')
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x2','Zatrudnienie (Art.18 ust.1 p.1 pp.c ustawy o finansowaniu nauki)')
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x3','Infrastruktura badawcza (Art.18 ust.1 p.1 pp.b ustawy o finansowaniu nauki)')
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x4','Aparatura naukowo-badawcza (Art.18 ust.1 p.1 pp.d ustawy o finansowaniu nauki)')
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x5','Wsp�praca (Art.18 ust.1 p.1 pp.e ustawy o finansowaniu nauki)')
insert into dsg_ul_costinvoice_typy_dzialalnosci (refValue,available,centrum,cn,title) values (2,1,null,'51x6','Upowszechnianie nauki (Art.18 ust.1 p.1 pp.f ustawy o finansowaniu nauki)')