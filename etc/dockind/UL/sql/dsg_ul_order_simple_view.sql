alter view dsg_ul_order_simple_view as
	SELECT * 
	 FROM (
	 select o.document_id as id, mpk.cn as mpk_cn,
	 ROW_NUMBER() OVER (PARTITION BY o.document_id ORDER BY o.document_id) as mpk_lp,o.*
			from dsg_ul_order o
			join dsg_ul_order_multiple_value mu on o.DOCUMENT_ID=mu.DOCUMENT_ID and mu.FIELD_CN='BP'
			join dsg_ul_budget_position bp on mu.FIELD_VAL=bp.id
			join dsg_ul_organizational_unit mpk on bp.c_mpk=mpk.id
			where o.erp_document_idm is not null
			) d where d.mpk_lp = 1

