insert into dsg_ul_costinvoice_grupy_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-X','Rozliczenie zakupu');


insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-01','Rozliczenie zakupu energia elektryczna');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-02','Rozliczenie zakupu energia cieplna');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-03','Rozliczenie zakupu woda  i �cieki');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-04','Rozliczenie zakupu gaz');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-05','Rozliczenie zakupu us�ugi porz�dkowe');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-06','Rozliczenie zakupu wyw�z nieczysto�ci');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-07','Rozliczenie zakupu us�ugi telekomunikacyjne');
insert into dsg_ul_costinvoice_rodzaj_dzialalnosci (refValue,available,centrum,cn,title) values (null,1,null,'301-3-09','Rozliczenie zakupu pozosta�e zakupy');

update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_grupy_dzialalnosci set title=REPLACE(title,cn+': ','')
update dsg_ul_costinvoice_rodzaj_dzialalnosci set title=cn+': '+title;
update dsg_ul_costinvoice_grupy_dzialalnosci set title=cn+': '+title;

update dsg_ul_costinvoice_rodzaj_dzialalnosci set refValue=g.id
from dsg_ul_costinvoice_grupy_dzialalnosci g
where substring(dsg_ul_costinvoice_rodzaj_dzialalnosci.cn,1,4)=substring(g.cn,1,4) and g.cn='301-X';

insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-01',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-02',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-03',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-04',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-05',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-06',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-07',1,0,0,0,0,0,1);
insert into dsg_ul_costinvoice_dzialnosci_mapping_table (grupa_dzial_idn,rodzaj_dzial_idn,is_slownik_komorek,is_numer_projektu,is_numer_zlecenia,is_numer_inwentarzowy,is_oznaczenie_grupy_pomieszczen,is_rodzaj_kosztu,is_zrodlo_finansowania) values ('301-X','301-3-09',1,0,0,0,0,0,1);