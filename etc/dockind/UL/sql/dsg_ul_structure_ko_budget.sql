drop view [dsg_ul_structure_budget];
create view [dbo].[dsg_ul_structure_ko_budget] 
with schemabinding
as
select 
bs.bd_budzet_id as id,
bs.bd_budzet_idm as cn,
bs.budzet_nazwa as title,
MAX(CAST(bs.available as INT)) * MAX(CAST(bs.czy_aktywna as INT)) as available,
bs.budzet_id as centrum,
mpk.id as refValue
from dbo.dsg_ul_services_structure_budgets bs
join dbo.dsg_ul_organizational_unit mpk on bs.dok_bd_str_budzet_idn=mpk.cn
group by bs.bd_budzet_id,bs.bd_budzet_idm,bs.budzet_nazwa,mpk.id,bs.budzet_id
GO

--create view [dbo].[dsg_ul_structure_ko_budget] 
--with schemabinding
--as
--select 
--bs.bd_budzet_id as id,
--bs.bd_budzet_idm as cn,
--bs.budzet_idm+' - '+bs.budzet_nazwa as title,
--MAX(CAST(bs.available as INT)) * MAX(CAST(bs.czy_aktywna as INT)) as available,
--bs.budzet_id as centrum,
--mpk.id as refValue
--from dbo.dsg_ul_services_structure_budgets bs
--join dbo.dsg_ul_organizational_unit mpk on bs.dok_bd_str_budzet_idn=mpk.cn
--group by bs.bd_budzet_id,bs.bd_budzet_idm,bs.budzet_nazwa,mpk.id,bs.budzet_id,bs.budzet_idm
--GO

select top 10000 * from dsg_ul_services_structure_budgets where bd_budzet_id=141 and bd_budzet_rodzaj_id=91735