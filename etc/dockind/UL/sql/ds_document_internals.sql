create view ds_document_internals as
select doc.id as document_id, doc.*,doc_out.OFFICENUMBER from ds_document doc
join dso_out_document doc_out on doc.ID=doc_out.ID
where doc.dockind_id=(select id from DS_DOCUMENT_KIND where cn='normal_int')