insert into dsd_dictionary (deleted,code,tablename,dockind_cn,dict_source,cn,ref_dict,is_control_dict) 
values(0, 'C_STRUCTURE_BUDGET_KO', 'dsg_ul_budgets_from_units', 'ul_costinvoice', 2, 'C_STRUCTURE_BUDGET_KO', (select top 1 id from dsd_dictionary where cn='C_MPK'), 0);
insert into dsd_dictionary (deleted,code,tablename,dockind_cn,dict_source,cn,ref_dict,is_control_dict) 
values(0, 'C_STRUCTURE_BUDGET_POSITION_RAW', null, 'ul_costinvoice', 4, 'C_STRUCTURE_BUDGET_POSITION_RAW', (select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET_KO'), 0);

select * from dsd_dictionary_map
insert into dsd_dictionary_map (control_value,dictionary_id,show_all_referenced,must_show,dockind_cn)
select map.control_value,(select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET_KO'),0,1,'ul_costinvoice'
from (
select distinct control_value from dsd_dictionary_map where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_MPK')
EXCEPT
select distinct control_value from dsd_dictionary_map
where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_PROJECT')
) map

insert into dsd_dictionary_map (control_value,dictionary_id,show_all_referenced,must_show,dockind_cn)
select map.control_value,(select top 1 id from dsd_dictionary where cn='C_STRUCTURE_BUDGET_POSITION_RAW'),0,1,'ul_costinvoice'
from (
select distinct control_value from dsd_dictionary_map where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_MPK')
EXCEPT
select distinct control_value from dsd_dictionary_map
where dictionary_id = (select top 1 id from dsd_dictionary where cn='C_PROJECT')
) map

alter table dsg_ul_budget_position
add c_structure_budget int;
alter table dsg_ul_budget_position
add c_structure_budget_position int;


--1
update dsd_dictionary set tablename='dsg_ul_budgets_from_units' where cn ='C_STRUCTURE_BUDGET_KO';

--2
update dsd_dictionary set tablename=null,code='C_STRUCTURE_BUDGET_POSITION_RAW',cn='C_STRUCTURE_BUDGET_POSITION_RAW',dict_source=4 
where cn ='C_STRUCTURE_BUDGET_POSITION';

--3
update dsd_dictionary set tablename=null,code='C_RESOURCE_RAW',cn='C_RESOURCE_RAW',dict_source=4 
where cn ='C_RESOURCE';

sp_rename 'dsg_ul_budget_position.c_structure_budget', 'c_structure_budget_ko'
UPDATE dsd_dictionary set code='C_STRUCTURE_BUDGET_KO', tablename='dsg_ul_structure_budget_ko', cn='C_STRUCTURE_BUDGET_KO' where cn='C_STRUCTURE_BUDGET';