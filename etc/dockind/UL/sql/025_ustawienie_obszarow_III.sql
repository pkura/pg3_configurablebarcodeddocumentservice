declare @d_code varchar(64);
declare @u_name varchar(64);

set @d_code='0400000000';
set @u_name='ewagaszewska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');

 
set @d_code='0800000000';
set @u_name='ewagaszewska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='0200000000';
set @u_name='ewagaszewska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='0700000000';
set @u_name='sylwialawska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='1500000000';
set @u_name='sylwialawska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='1600000000';
set @u_name='sylwialawska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');

 
set @d_code='0600000000';
set @u_name='urszulawaclawiak';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='0100000000';
set @u_name='urszulawaclawiak';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='1200000000';
set @u_name='urszulawaclawiak';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='1400000000';
set @u_name='irenakarpinska';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');


set @d_code='0500000000';
set @u_name='agnieszkakasperek';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');

 
set @d_code='1100000000';
set @u_name='agnieszkakasperek';
WITH div_rek(id,parent_id,code,name) AS ( 
select id,parent_id,code,name from ds_division where code=@d_code UNION ALL select d.id,d.parent_id,d.code,d.name from ds_division d join div_rek r on r.id=d.PARENT_ID )  
insert into dsg_ul_obszary_mapping_table select o.id as c_mpk,r.id as i_kind, null as c_project, 
(select top 1 id from ds_user where name=@u_name) as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark
 from div_rek d join dsg_ul_mpk_mapping m on d.code=m.ds_div_cn join dsg_ul_organizational_unit o on m.mpk_cn=o.cn join dsg_ul_costinvoice_rodzaj_dzialalnosci r on 1=1 where r.cn in ('509','552','506','541','561','080-1','080-2','080-3','080-4','080-5','080-6','080-7','080-8','080-9','543');





 insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-01') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Sylwia �awska';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-02') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Sylwia �awska';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-03') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Sylwia �awska';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-04') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Sylwia �awska';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-05') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Agnieszka Kasperek';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-06') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Agnieszka Kasperek';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-07') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Agnieszka Kasperek';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-08') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Irena Karpi�ska';
insert into dsg_ul_obszary_mapping_table select null as c_mpk, (select top 1 id from dsg_ul_costinvoice_rodzaj_dzialalnosci where cn='301-3-09') as i_kind, null as c_project, id as dsuser, getdate() as create_time, 1 as create_user,  getdate() as modify_time, 1 as modify_user, 'z systemu' as remark from ds_user where firstname+' '+lastname='Agnieszka Kasperek';