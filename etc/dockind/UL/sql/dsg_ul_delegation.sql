CREATE TABLE [dbo].[dsg_ul_delegation](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[data_wypelnienia] [datetime] NULL,
	[CEL_WYJAZDU] [varchar](300) NULL,
	[TERMIN_WYJAZDU] [datetime] NULL,
	[TERMIN_POWROTU] [datetime] NULL,
	[JEDNOSTKA_DELEGACJI] [varchar](150) NULL,
	[MIEJSCOWOSC_WYJAZDU] [varchar](150) NULL,
	[DODATKOWE_INFO] [varchar](350) NULL,
	[SRODEK_LOKOMOCJI] [varchar](100) NULL,
	[PRZEWIDYWANY_KOSZT] [numeric](19, 2) NULL,
	[ZALICZKA] [tinyint] NULL,
	[forma_zaliczki] [int] NULL,
	[przelew_na] [varchar](40) NULL,
	[DOJAZDY_RYCZALT_ILOSC] [numeric](19, 2) NULL,
	[RYCZALT_COST] [numeric](19, 2) NULL,
	[SUMA_KOSZTOW_PODROZY] [numeric](19, 2) NULL,
	[DIETY_ILOSC] [numeric](19, 2) NULL,
	[DIETY] [numeric](19, 2) NULL,
	[NOCLEGI] [numeric](19, 2) NULL,
	[NOCLEGI_RYCZALT_ILOSC] [numeric](19, 2) NULL,
	[NOCLEGI_RYCZALT] [numeric](19, 2) NULL,
	[INNE_WYDATKI] [numeric](19, 2) NULL,
	[SUMA] [numeric](19, 2) NULL,
	[POBRANA_ZALICZKA] [numeric](19, 2) NULL,
	[DO_WYPLATY] [numeric](19, 2) NULL,
	[DO_ZWROTU] [numeric](19, 2) NULL,
	[FORMA_ZWROTU] [int] NULL,
	[ROZLICZENIE_PRZELEW_NA] [varchar](40) NULL,
	[SLOWNIK_KOSZTOW_DELEGACJI] [int] NULL,
	[suma_pozycji] [numeric](18, 2) NULL,
	[wnioskodawca] [int] NULL,
	[worker_division] [int] NULL,
	[potwierdzenie_pobytu] [numeric](18, 0) NULL,
	[nr_wyjazdu] [varchar](64) NULL,
	[delegowany_wybor] [int] NULL,
	[delegowany] [varchar](300) NULL,
	[delegowany_inny] [int] NULL,
	[dieta_krajowa_ilosc] [numeric](19, 2) NULL,
	[dieta_krajowa_money] [numeric](19, 2) NULL,
	dojazdy_udokumentowane numeric(19,2),
	refundacja_koszty_opis varchar(500),
	refundacja_koszty_czy bit,
	dieta_wyzywienie int,
	srodek_lokomocji_samochod bit,
	umowa_podroz_samochodem numeric(19,0),
	ewidencja_przebiegu numeric(19,0),
	typ_dokumentu int,
	nr_tel varchar(50),
	nr_dowodu varchar(50),
	email varchar(150),
	kod_pracownika varchar(150),
	adres_zamieszkania varchar(500),
	zaliczka_kwota numeric(19,2),
	rozliczenie_vat tinyint
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE TABLE [dbo].[dsg_ul_delegation_diet_dic](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	sniadanie int,
	obiad int,
	kolacja int,
) ON [PRIMARY]

GO