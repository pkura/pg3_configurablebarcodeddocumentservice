CREATE TABLE [dbo].[dsg_ul_projects_mpk_mapping](
	[project_cn] [varchar](25) NOT NULL,
	[mpk_cn] [varchar](25) NOT NULL
) ON [PRIMARY]

INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1110000000200000', N'1206010000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I0810000000100000', N'1206010000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121000000063ED00', N'1201700000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000453KN00', N'1201311000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P111000000021ED00', N'1201711000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12121320Z999PLPP', N'4350000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D00000000000000X', N'1207000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P091000000067ED00', N'1201711000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1214200000495000', N'4201000012')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B0621000000022000', N'2120000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1214200000496000', N'4201000012')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1012100000427000', N'2200000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1320000000085000', N'2142222000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100993RCEUNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1310000000500000', N'1100000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100994RMOBNDEP', N'1100000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100997RSMSNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000001WK00', N'1205010500')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121000000064ED00', N'1201700000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B0911000000494000', N'2130000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100994RMOBNDEP', N'1100000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000001ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000058ED00', N'1201311000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121000000039ED00', N'1201311000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121210000039ED00', N'1000000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B0721000000023000', N'2120000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'M122180000004DD00', N'2212500000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000002ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000003ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000004ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121000000022ED00', N'1200000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P112000000008ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131420000001PN00', N'4201000012')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1311100000017.03', N'2102121000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100996ROOMNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100996ROOMNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992RSMONDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992RSMINDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1210000000400000', N'1206011000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992RSTINDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992RSTONDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000005ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P112000000007ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121000000023ED00', N'1201311000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D000000000000000X', N'1207000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B0522100000065000', N'2200000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P092130000049ED00', N'1201000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000042ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000044ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000045ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000046ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000047ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P102000000048ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P132000000051ED00', N'1200000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P132000000053ED00', N'1200000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P132000000054ED00', N'1200000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1021300000083000', N'2142340000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P132000000052ED00', N'1200000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1120000000071000', N'1201311000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131120000490KN00', N'2112400000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D1222300Z999PLPP0', N'2220000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100997ROOMNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100997ROOMNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100997RSMPNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100997RSMPNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100997RSMSNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100997RSTANDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D12100997RSTTNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100997RSTTNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100997RSTANDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1210000000500000', N'1206011000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1310000000600000', N'2102332000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P092000000075ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131420000418KN00', N'4201000012')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131450000498KD00', N'4500000000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000066ED00', N'1205010500')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131420000002PN00', N'4201000012')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000001WZ00', N'2220000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131190000011WZ00', N'2180000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P121130000028ED00', N'2122110000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131160000489KE00', N'2210000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P112000000006ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131000000076ED00', N'1201711000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P132000000077ED00', N'1201312000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1311900000185.01', N'2220000010')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'B1211300000196.01', N'2172120000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'P131110000485KD00', N'2122021000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100993RCEUNDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1110000000300000', N'1206010000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'I1311000000700000', N'1206011000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992ROMINDEP', N'1201310000')
GO
INSERT [dbo].[dsg_ul_projects_mpk_mapping] ([project_cn], [mpk_cn]) VALUES (N'D13100992ROMONDEP', N'1201310000')
GO

ALTER VIEW [dbo].[dsg_ul_view_projects]
AS
SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, proj.available AS available
FROM dsg_ul_services_projects proj JOIN
dsg_ul_services_organizational_unit unit ON ( SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2))
union
SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, mpk.id AS refValue, proj.centrum AS centrum, proj.available AS available
FROM dsg_ul_services_projects proj 
JOIN dsg_ul_projects_mpk_mapping map ON proj.cn=map.project_cn
JOIN dsg_ul_services_organizational_unit mpk on map.mpk_cn=mpk.cn