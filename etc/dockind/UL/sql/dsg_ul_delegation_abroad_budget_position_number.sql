CREATE view dbo.dsg_ul_delegation_abroad_budget_position_number
WITH SCHEMABINDING
as
select bp.id, CAST(bp.id as varchar(50)) as cn, ISNULL(CAST(bp.i_no as varchar(255)),'...') as title, null as centrum, CAST(mul.DOCUMENT_ID as varchar(20)) as refValue, 1 as available
from dbo.dsg_ul_delegation_abroad_multiple_value mul
JOIN dbo.dsg_ul_budget_position bp ON mul.FIELD_CN='BP_ROZ' and bp.id=mul.FIELD_VAL
GO