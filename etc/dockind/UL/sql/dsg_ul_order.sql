create table dsg_ul_order (
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[dokumenty] [numeric](18, 0) NULL,
	[nr_sprawy] [varchar](20) NULL,
	[kwota_brutto] [numeric](18, 2) NULL,
	[kwota_euro] [numeric](18, 2) NULL,
	[kwota_pozostala_mpk] [numeric](18, 2) NULL,
	[kwota_kurs] [numeric](18, 3) NULL,
	[realizacja] [date] NULL,
	[nazwa_zamowienia] [varchar](251) NULL,
	[opis] [varchar](4096) NULL,
	[szczegolowy_opis] [varchar](max) NULL,
	[uzasadnienie] [varchar](max) NULL,
	[kwota_waluta_data] [date] NULL,
	[typ_zamowienia] int NULL,
	[rodzaj_zamowienia] int NULL,
	[tryb_zamowienia] int NULL,
	[szczegoly_zamowienia] int NULL,
	[zarzadzenie_przetarg_attach] [numeric](18, 0) NULL,
	[mpk] int NULL,
	[doc_type] int NULL
);

CREATE TABLE dsg_ul_order_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

alter table dsg_ul_order
add attachment numeric(19,0) null;
alter table dsg_ul_order
add kwota_netto numeric(18,2) null;
alter table dsg_ul_order
add rodzaj_zamowienia_osoba int null;

ALTER TABLE dsg_ul_order ADD erp_status int;
ALTER TABLE dsg_ul_order ADD erp_document_idm varchar(50);