create view [dbo].[dsg_ul_view_samochody] as
select id as id,
available as available,
produktIdn as cn,
produktNazwa as title,
null as refValue,
null as centrum
from dsg_ul_services_product
where 
produktGrupa='SAMOCHODY'
union
SELECT - 1 AS id, 1 AS available, 'ND' AS cn,  'nie dotyczy' AS title, NULL AS refValue, NULL AS centrum;

select * into dsg_ul_samochody from dsg_ul_view_samochody;

CREATE CLUSTERED INDEX [ClusteredIndex-20130115-221219] ON dsg_ul_samochody
(
	[cn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

alter table dsg_ul_costinvoice_dzialnosci_mapping_table
add is_samochody int not null default 0;

update dsg_ul_costinvoice_dzialnosci_mapping_table set is_samochody=1 where rodzaj_dzial_idn='533'