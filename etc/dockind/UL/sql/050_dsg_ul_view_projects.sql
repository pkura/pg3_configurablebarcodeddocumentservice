ALTER VIEW [dbo].[dsg_ul_view_projects] AS
SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, proj.available AS available
FROM dsg_ul_services_projects proj 
JOIN dsg_ul_services_organizational_unit unit on proj.refValue=unit.erp_id
where proj.refValue is not null and proj.refValue <> 0
UNION
SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, proj.available AS available
FROM dsg_ul_services_projects proj 
JOIN dsg_ul_services_organizational_unit unit ON ( SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2))
------------------------------------------------------
--naprawi� b��d dla wpis�w niedost�pnych, avaialble=0
------------------------------------------------------
--where proj.refValue is null OR proj.refValue = 0
--UNION
--SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, 0 AS available
--FROM dsg_ul_services_projects proj 
--JOIN dsg_ul_services_organizational_unit unit ON ( SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2))
--where proj.refValue is not null and proj.refValue <> 0

--Stary widok
--CREATE VIEW [dbo].[dsg_ul_view_projects]
--AS
--SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, unit.id AS refValue, proj.centrum AS centrum, proj.available AS available
--FROM dsg_ul_services_projects proj JOIN
--dsg_ul_services_organizational_unit unit ON ( SUBSTRING(unit.cn, 2, 2) = SUBSTRING(proj.cn, 5, 2))
--union
--SELECT proj.cn AS cn, proj.erp_id AS id, proj.title AS title, mpk.id AS refValue, proj.centrum AS centrum, proj.available AS available
--FROM dsg_ul_services_projects proj 
--JOIN dsg_ul_projects_mpk_mapping map ON proj.cn=map.project_cn
--JOIN dsg_ul_services_organizational_unit mpk on map.mpk_cn=mpk.cn