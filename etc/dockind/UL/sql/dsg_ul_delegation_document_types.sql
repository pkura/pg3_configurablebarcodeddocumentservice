ALTER view [dbo].[dsg_ul_delegation_document_types] 
WITH SCHEMABINDING
as
select
id as id,
LTRIM(RTRIM(typdok_idn)) as cn,
typdokzak_id as centrum,
(
select top 1 rep_atrybut_id from dbo.dsd_simple_repository_attribute where objectid=typdokzak_id and context_id=6 and rep_klasa_id=187 and available=1
)
 as refValue,
nazwa as title,
available as available
from dbo.dsg_ul_services_document_types where typdok_idn in ('DPP','DPZP','DOP')
GO