CREATE TABLE [DSG_COMPLAINT](
	[document_id] [numeric](19, 0) NOT NULL,
	[kind] [int] NOT NULL,
	[sender] [varchar](50) NULL,
	[number] [varchar](50) NULL,
	[receive_date] [datetime] NULL,
	[document_date] [datetime] NULL,
	[deliverer] [varchar](50) NULL,
	[shipment_number] [varchar](50) NULL,
	[delivery_place] [varchar](100) NULL,
	[receive_kind] [int] NOT NULL,
	[description] [text] NULL,
	[realization_date] [datetime] NULL,
	[dsuser_responsible] [numeric](18, 0) NULL,
	[status] [int] NULL,
	[realization_type] [int] NULL,
	[cause_incomplete] [bit] NULL,
	[cause_broken] [bit] NULL,
	[cause_bad_quality] [bit] NULL,
	[cause_rude_service] [bit] NULL,
	[days_left_text] [varchar](50) NULL,
 CONSTRAINT [PK_DSG_COMPLAINT] PRIMARY KEY CLUSTERED
(
	[document_id] ASC
)
)
