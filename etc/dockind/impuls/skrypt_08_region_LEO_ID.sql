alter table dsc_region add LEO_ID numeric(18,0); 
update dsc_region set LEO_ID = 178 where cn = 'DOL';
update dsc_region set LEO_ID = 84	where cn = 'KUPOM';
update dsc_region set LEO_ID = 176	where cn = 'LAB';
update dsc_region set LEO_ID = 179	where cn = 'LOD';
update dsc_region set LEO_ID = 180	where cn = 'MAL';
update dsc_region set LEO_ID = 101	where cn = 'PODK';
update dsc_region set LEO_ID = 177	where cn = 'PODL';
update dsc_region set LEO_ID = 173	where cn = 'POM';
update dsc_region set LEO_ID = 174	where cn = 'SLA';
update dsc_region set LEO_ID = 169	where cn = 'SWI';
update dsc_region set LEO_ID = 172	where cn = 'WIE';
update dsc_region set LEO_ID = 82	where cn = 'ZPOM';

alter table 
