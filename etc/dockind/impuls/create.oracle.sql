CREATE TABLE DSG_BACKOFFICE (
    DOCUMENT_ID 		numeric(18, 0) NOT NULL ,
    kategoria 			integer,
    nip_dostawcy 		varchar(50  char),
    data_faktury 		timestamp,
    data_platnosci  	timestamp,
    numer_faktury 		varchar(50  char),
    kwota_faktury 		float,
    opis_faktury        varchar (100  char) ,
    typ_dokumentu       varchar (100  char) ,
    data_zaplacenia_faktury timestamp ,
    nr_ksiegowania      numeric(18,0) ,
    zaplacono 			smallint,
    nazwa_dostawcy 		varchar(50  char),
    centrum_kosztow 	integer,
    akceptacja_finalna 	smallint,
    status 				integer
);

CREATE TABLE DSG_LEASING (
    DOCUMENT_ID			integer NOT NULL ,
    klient	 			integer,
    numer_umowy			integer,
    numer_wniosku		integer,
    rodzaj_dokumentu 	integer,
    RODZAJ_RAPORTU		integer,
    RODZAJ_DOKUMENTU_FINANSOWEGO integer,
    RODZAJ_DEKLARACJI   integer,
	RODZAJ_dw		    integer,
	RODZAJ_dl		    integer,
	RODZAJ_zpl		    integer,
	RODZAJ_dz		    integer,
	RODZAJ_dru		    integer,
	RODZAJ_filpol	    integer,
    typ_dokumentu 		integer,
    data_dokumentu		timestamp,
    status_dokumentu	integer,
    numer_dokumentu		varchar(30  char),
    numer_fabryczny		varchar(30  char),
    kwota				float,
	action_count		smallint,
    opis				varchar(200  char)    
);

CREATE TABLE DSG_LEASING_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20  char) NOT NULL,
    FIELD_VAL varchar(100  char) NULL
);


insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL, 'IMPULS_PRAC_KANCELARII', 'Pracownik kancelarii','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL, 'IMPULS_PRAC_KSIEGOWOSCI', 'Pracownik księgowości' ,'group',1,0);

CREATE TABLE DL_CONTRACT_DICTIONARY(
	id					integer not null,
    numerUmowy			varchar(50 char),
    idKlienta			integer,
    opisUmowy			varchar(200 char),
    status				varchar(20 char),
	wartoscUmowy		float,
	dataUmowy			timestamp,
	dataKoncaUmowy		timestamp
);
CREATE SEQUENCE DL_CONTRACT_DICTIONARY_ID start with 1 nocache;


CREATE TABLE DL_APPLICATION_DICTIONARY(
	id					integer not null,
    numerWniosku		varchar(20 char),
    idKlienta			integer,
    opis				varchar(200 char),
	wartosc				float,
	kwota				float,
	liczbaRat 			integer,
	wykup				float,
    rodzaj				varchar(20 char)	
);
CREATE SEQUENCE DL_APPLICATION_DICTIONARY_ID start with 1 nocache;