create or replace
FUNCTION IF_CAN_MOVE (nr_wniosku IN VARCHAR2,nr_umowy IN VARCHAR2,nr_przejscia integer) RETURN integer IS 
cursor warunki is( select typ_dokumentu,minimalny_status from dsg_warunki_przejscia);
liczba integer;
BEGIN
  FOR warunek in warunki
    LOOP
    EXIT WHEN warunki%NOTFOUND;
    select count(1) into liczba from dsg_leasing doc, dsg_leasing_multiple_value multi, dl_application_dictionary app  where doc.DOCUMENT_ID = multi.document_id
        AND multi.FIELD_CN = 'NUMER_WNIOSKU' and app.id = multi.field_val and app.numer_wniosku = nr_wniosku and doc.status_dokumentu >= warunek.minimalny_status and doc.typ_dokumentu = warunek.typ_dokumentu;

    if liczba < 1 THEN
        select count(1) into liczba from dsg_leasing doc, dsg_leasing_multiple_value multi, dl_contract_dictionary con  where doc.DOCUMENT_ID = multi.document_id
        AND multi.FIELD_CN = 'NUMER_UMOWY' and con.id = multi.field_val and con.numerumowy = nr_umowy and doc.status_dokumentu >= warunek.minimalny_status and doc.typ_dokumentu = warunek.typ_dokumentu;
    END IF;  
    if liczba < 1 THEN
        return 0;
      END IF;
    END LOOP;
    return 1;
EXCEPTION   
  WHEN OTHERS THEN
  DBMS_OUTPUT.PUT_LINE(SQLERRM);
  return 0;
END IF_CAN_MOVE;


create or replace
FUNCTION GET_PLACE (nr_wniosku IN VARCHAR2,nr_umowy IN VARCHAR2) RETURN VARCHAR2 IS
place number;
  BEGIN
        select doc.workflow_place into place from dsg_dlbinder doc, dl_application_dictionary app where app.id = doc.numer_wniosku and app.numerwniosku = nr_wniosku;
      IF place = 9 THEN
        RETURN 'DS';
      ELSIF  PLACE = 19 THEN
        RETURN 'DR';
      ELSIF  PLACE = 25 THEN
        RETURN 'Du';
      ELSIF  PLACE = 29 THEN
        RETURN 'DO';
      ELSIF  PLACE = 39 THEN
        RETURN 'DK';
      END IF;
        RETURN 'BRAK';
        
         RETURN 'BRAK';
    EXCEPTION
          WHEN NO_DATA_FOUND THEN
            select doc.workflow_place into place from dsg_dlbinder doc, dl_contract_dictionary con where con.id = doc.numer_umowy and con.numerumowy = nr_umowy;
            IF place = 9 THEN
              RETURN 'DS';
            ELSIF  PLACE = 19 THEN
              RETURN 'DR';
            ELSIF  PLACE = 25 THEN
              RETURN 'Du';
            ELSIF  PLACE = 29 THEN
              RETURN 'DO';
            ELSIF  PLACE = 39 THEN
              RETURN 'DK';
            END IF;
              RETURN 'BRAK';
END GET_PLACE;