
create table DSC_machine (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);

CREATE SEQUENCE   DSC_machine_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table ds_contractor_to_machine (
	contractor_id		numeric(19,0) not null,
	machine_id		numeric(19,0) not null
);

alter table dsc_contact add RODZAJNASTEPNEGOKONTAKTU integer;
alter table ds_contractor_to_machine add constraint ds_contractor_to_machine_fk1
foreign key (contractor_id) references DSC_Contractor (id);


insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Maszyna/Urz�dzenie','MU');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Sprz�t medyczny','SM');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'W�zek widlowy','WW');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Meble','ME');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Rega�y magazynowe','RM');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Komputery','KOM');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Jacht','JA');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Maszyna Poligraficzna','MP');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Sprz�t rolniczy - ci�gniki','SRC');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Kontenery','KON');
insert into DSC_machine(id,name,cn)values(DSC_machine_id.nextval,'Sprz�t biurowy','SB');


insert into ds_division (id,guid,name,code,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'DL_BINDER_DU','Teczka Ubezpieczenia',null,'group',172,0);
