insert into ds_division (id,guid,name,code,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'DL_DEL_ATTA','Usuwanie załączników',null,'group',0,0);
insert into ds_division (id,guid,name,code,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'DL_DEL_DOC','Usuwanie dokumentów',null,'group',0,0);

create table dl_import_attachment
(
	document_id numeric(18,0),
	attachment_name varchar2(600 char)
);

alter table dsg_leasing add rodzaj_kontrahenta integer;