alter table dsg_dlbinder add workflow_place NUMBER(18,0) DEFAULT 9;
update dsg_dlbinder set  workflow_place = 9;
alter table dl_import_attachment add attachment_id NUMBER(18,0);
alter table dl_import_attachment add import_id NUMBER(18,0);
alter table dl_import_attachment add username varchar2(50 char);
create sequence dl_import_attachment_id;
alter table dsc_role add leoId integer;