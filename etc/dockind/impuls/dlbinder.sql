create table dsg_dlbinder
(
	document_id numeric(18,0) not null,
	numer_umowy varchar(50 char),
	numer_wniosku varchar(50 char),
	klient numeric(18,0),
	dostawca numeric(18,0),
	status integer
);
CREATE SEQUENCE dsg_dlbinder_id start with 1 nocache;

create index i_dsg_dlbinder1 on dsg_dlbinder (numer_umowy);
create index i_dsg_dlbinder2 on dsg_dlbinder (klient);
create index i_dsg_dlbinder3 on dsg_dlbinder (dostawca);
create index i_dsg_dlbinder4 on dsg_dlbinder (status);