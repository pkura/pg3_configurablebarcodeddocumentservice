CREATE TABLE dsg_rodzaj
(
	id integer,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
create sequence dsg_rodzaj_id;
CREATE TABLE dsg_typ
(
	id integer,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
create sequence dsg_typ_id;

CREATE TABLE dsg_warunki_przejscia
(
	numer_przejscia integer,
	typ_dokumentu integer,
	minimalny_status
);
