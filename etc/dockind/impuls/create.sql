CREATE TABLE DSG_BACKOFFICE (
    DOCUMENT_ID 		numeric(18, 0) NOT NULL ,
    kategoria 			int,
    nip_dostawcy 		varchar(50),
    data_faktury 		datetime,
    data_platnosci  	datetime,
    numer_faktury 		varchar(50),
    kwota_faktury 		float,
    opis_faktury        varchar (100) ,
    typ_dokumentu       varchar (100) ,
    data_zaplacenia_faktury datetime ,
    nr_ksiegowania      numeric(18,0) ,
    zaplacono 			smallint,
    nazwa_dostawcy 		varchar(50),
    centrum_kosztow 	int,
    akceptacja_finalna 	smallint,
    status 				int
);

CREATE TABLE DSG_LEASING (
    DOCUMENT_ID			int NOT NULL ,
    klient	 			int,
    numer_umowy			int,
    numer_wniosku		int,
    rodzaj_dokumentu 	int,
    RODZAJ_RAPORTU		int,
    RODZAJ_DOKUMENTU_FINANSOWEGO int,
    RODZAJ_DEKLARACJI   int,
	RODZAJ_dw		    int,
	RODZAJ_dl		    int,
	RODZAJ_zpl		    int,
	RODZAJ_dz		    int,
	RODZAJ_dru		    int,
	RODZAJ_filpol	    int,
    typ_dokumentu 		int,
    data_dokumentu		datetime,
    status_dokumentu	int,
    numer_dokumentu		varchar(30),
    numer_fabryczny		varchar(30),
    kwota				float,
	action_count		smallint,
    opis				varchar(200)    
);

CREATE TABLE DSG_LEASING_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);


insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('IMPULS_PRAC_KANCELARII', 'Pracownik kancelarii','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('IMPULS_PRAC_KSIEGOWOSCI', 'Pracownik księgowości' ,'group',1,0);

CREATE TABLE DL_CONTRACT_DICTIONARY(
	id					int not null,
    numerUmowy			varchar(10),
    idKlienta			int,
    opisUmowy			varchar(200),
    status				varchar(20),
	wartoscUmowy		float,
	dataUmowy			datetime,
	dataKoncaUmowy		datetime
);



CREATE TABLE DL_APPLICATION_DICTIONARY(
	id					int not null,
    numerWniosku		varchar(20),
    idKlienta			int,
    opis				varchar(200),
	wartosc				float,
	kwota				float,
	liczbaRat 			int,
	wykup				float,
    rodzaj				varchar(20)	
);
