insert into ds_division (id,guid, name, code, divisiontype, parent_id, hidden) values (ds_division_id.nextval,'UMW_EDIT','Edycaj wszystkich p�l',null,'group',1,0);
alter table DSG_AKT_NOTARIALNY add  text_data clob;
CREATE INDEX AKT_TEXT_DATA_IDX ON dsg_akt_notarialny
  (
    text_data
  )
  INDEXTYPE IS "CTXSYS"."CONTEXT" ;
  
BEGIN
-- Job defined entirely by the CREATE JOB procedure.
DBMS_SCHEDULER.create_job (
job_name => 'sync_index',
job_type => 'PLSQL_BLOCK',
job_action => 'begin ctx_ddl.sync_index(''AKT_TEXT_DATA_IDX''); end;',
start_date => SYSTIMESTAMP,
repeat_interval => 'freq=minutely',
end_date => NULL,
enabled => TRUE,
comments => 'updating text index');
END;
