CREATE TABLE [DSG_AKT_NOTARIALNY] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
    [numer_kancelaryjny] varchar(20),
    [data_wplywu] datetime,
    [repertorium_aktu] varchar(75),
    [pozycja] varchar(20),
    [data_zawarcia] datetime,
    [dzielnica] int,
	[nazwa_kancelarii_slownik] int
);

CREATE TABLE [DSG_AKT_NOTARIALNY_MULTIPLE_VALUE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);

CREATE TABLE [df_inst] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[name] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[oldname] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nip] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[regon] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

alter table DSG_AKT_NOTARIALNY add barcode varchar(20);