CREATE TABLE DSG_AKT_NOTARIALNY (
    DOCUMENT_ID numeric(19, 0) NOT NULL ,
    numer_kancelaryjny varchar2(20 char),
    data_wplywu timestamp,
    repertorium_aktu varchar2(50 char),
    pozycja varchar2(20 char),
    data_zawarcia timestamp,
    dzielnica int,
	nazwa_kancelarii_slownik int
);

CREATE TABLE DSG_AKT_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar2(20 char) NOT NULL,
    FIELD_VAL varchar2(100 char) NULL
);


CREATE TABLE df_inst (
	id numeric (18,0) PRIMARY KEY  NOT NULL ,
	imie varchar2 (255 char) ,
	nazwisko varchar2 (255 char) ,
	name varchar2 (255 char) ,
	oldname varchar2 (255 char) ,
	nip varchar2 (30 char) ,
	regon varchar2 (30 char) ,
	ulica varchar2 (255 char) ,
	kod varchar2 (10 char) ,
	miejscowosc varchar2 (255 char) ,
	email varchar2 (255 char) ,
	faks varchar2 (255 char) ,
	telefon varchar2 (255 char) ,
	kraj varchar2 (10 char) 
);

alter table DSG_AKT_NOTARIALNY add barcode varchar(20);