

CREATE TABLE dsg_bos_multiple (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);


CREATE TABLE dsg_bos_coinvoice
(
    DOCUMENT_ID numeric(18, 0) NOT NULL ,
    status numeric(18, 0),
    invoice_kind numeric(18, 0),
    dostawca numeric(18, 0),
    invoice_date datetime, 
    invoice_number varchar(30),
    payment_date datetime,
    net_amount numeric(18,2),  
    vat numeric(18,2),
    gross_amount numeric(18,2),  
    centra_kosztow numeric(18, 0)
);

CREATE TABLE dsg_founding_source
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18,0),
	refValue varchar(20),
	available numeric(18,0) default 1 not null
);

CREATE TABLE dsg_cost_category
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18,0),
	refValue varchar(20),
	available numeric(18,0) default 1 not null
);

CREATE TABLE dsg_acc_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18,0),
	refValue varchar(20),
	available numeric(18,0) default 1 not null
);

INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN1','Jednostka1' ,0 ,NULL ,1 );
INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN2','Jednostka2' ,0 ,NULL ,1 );
     INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN2','Jednostka2' ,0 ,NULL ,1 );
     INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN2','Jednostka2' ,0 ,NULL ,1 );
     INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN2','Jednostka2' ,0 ,NULL ,1 );
     INSERT INTO [dsg_founding_source] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('JEDN2','Jednostka2' ,0 ,NULL ,1 );

INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('USLUGI_TRANSPORTOWE','US�UGI TRANSPORTOWE' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('USLUGI_SZKOLENIOWE','US�UGI SZKOLENIOWE' ,0 ,NULL ,1 );
INSERT INTO [dbo].[dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('PRACOWNICY','PRACOWNICY' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('OBSLUGA_FILII','PRACOWNICY, OBS�UGA FILII' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('ZUZYCIA','ZU�YCIA' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('KOSZTY_POL�CZE�_TEL','KOSZTY PO��CZE� TELEFONICZNYCH' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('KOSZTY_ROZNE','KOSZTY RӯNE' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('USLUGI_WYSYLKI_POCZTOWE','US�UGI WYSY�KI - POCZTOWE' ,0 ,NULL ,1 );
INSERT INTO [dsg_cost_category] ([CN]  ,[TITLE]  ,[centrum]   ,[refValue] ,[available])
     VALUES ('USLUGI_KURIERSKIE','US�UGI KURIERSKIE' ,0 ,NULL ,1 );



INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZTY_USLUG_TRANS' , 'KOSZTY US�UG TRANSPORTOWYCH',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZTY_USLUG_SZKO' , 'KOSZTY US�UG SZKOLENOWYCH',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('POSREDNICTWO_SPRZE' , 'PO�REDNICTWO SPRZEDA�Y',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('POSREDNICTWO_SPR_STALA' , 'PO�REDNICTWO SPRZEDA�Y CZʦ� STA�A',NULL ,null  ,1);    
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('POSREDNI_PRZED_ZMIENNA' , 'PO�REDNICTWO SPRZEDA�Y CZʦ� ZMIENNA',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_PAL_SAMOCH' , 'ZU�YCIE PALIWA DO SAMOCHOD�W',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZMATNAPOTRZWL' , 'ZU�YCIE MATERIA��W NA POTRZEBY W�ASNE',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_CZES_SAMOCH' , 'ZU�YCIE CZʦCI SAMOCHODOWYCH',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_ELEKTR' , 'ZU�YCIE ENERGII ELEKTRYCZNEJ',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_CIEPLNEJ' , 'ZU�YCIE ENERGII CIEPLNEJ',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_GAZ' , 'ZU�YCIE ENERGII GAZOWEJ',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('WYWOZODP' , 'WYW�Z ODPAD�W',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('ZUZ_WODY_SCIEK' , 'ZU�YCIE WODY I WYW�Z �CIEK�W',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('OGRZ_EKO' , 'OGRZEWANIE EKO GROSZEK ITP.',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZT_STACJ' , 'KOSZTY TELEFON�W STACJONARNYCH',NULL ,null  ,1);    
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZT_MOBIL' , 'KOSZTY TELEFON�W MOBILNYCH',NULL ,null  ,1);   
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('WYPOSAZ' , 'WYPOSA�ENIE (NOWE URZ�DZENIA, MEBLE)',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('USLUG_INFORMAT' , 'US�UGI INFORMATYCZNE, OP�ATY ZA DOST�P DO PROGRAM�W',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSYT_REKLA' , 'KOSZTY REKLAMY',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSYT_MARK' , 'KOSZTY MARKETINGU',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('OPLAT_ZA_EMISJ' , 'OP�ATY ZA EMISJE OG�OSZE� RӯNYCH',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('OGLOSZ_NAPORT_MOT' , 'OG�OSZENIA NA PORTALACH MOTORYZACYJNYCH',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('OGLOSZ_INNE' , 'OG�OSZENIA INNE (NP. OG�OSZENIA O PRAC�)',NULL ,null  ,1);
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('USLUG_SAMOCH' , 'US�UGI SAMOCHODOWE',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZT_WYSYLK' , 'KOSZTY WYSY�KI REFAKTUROWALNE NA KLIENT�W',NULL ,null  ,1);     
INSERT INTO [dsg_acc_kind] ([CN]     ,[TITLE]  ,[centrum],[refValue]   ,[available])
     VALUES ('KOSZT_WYS_NIER' , 'KOSZTY WYSY�KI NIEREFAKTUROWALNE NA KLIENT�W',NULL ,null  ,1);