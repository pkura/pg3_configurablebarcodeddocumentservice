CREATE TABLE dsg_faktura_p(
	document_id numeric(19, 0) NOT NULL,
	kwota float NULL
);

drop table dsg_account_number;

create table dsg_account_number (
  id int identity(1,1) NOT NULL,
  [account_number] [varchar](50) NOT NULL,
  [name] [varchar](100) NULL,
  [needdescription] [int] NULL,
  [discriminator] [int] NULL,
  [visible_in_system] [smallint] NOT NULL,
  [flag_1] [bit] NOT NULL,
  [account_name] [varchar](100) NULL,
  primary key (id)
);

--tak aby cc i an byly enumem
alter table DSG_CENTRUM_KOSZTOW add CN varchar(50);
alter table DSG_CENTRUM_KOSZTOW add TITLE varchar(255);
alter table DSG_CENTRUM_KOSZTOW add refValue varchar(20);
alter table DSG_CENTRUM_KOSZTOW add centrum int;
alter table dsg_account_number add CN varchar(50);
alter table dsg_account_number add TITLE varchar(255);
alter table dsg_account_number add refValue varchar(20);
alter table dsg_account_number add available int default 1 not null;
alter table dsg_account_number add centrum int;

update an set an.title = an1.account_number, an.cn = an1.account_name  from dsg_account_number an join dsg_account_number an1 on an1.id = an.id;
update ck set ck.cn = ck1.symbol, ck.title = ck1.name from DSG_CENTRUM_KOSZTOW ck join DSG_CENTRUM_KOSZTOW ck1 on ck1.id = ck.id

create trigger ck_insert
on DSG_CENTRUM_KOSZTOW
after insert
as
begin
  update ck set ck.cn = ck1.symbol, ck.title = ck1.symbol from DSG_CENTRUM_KOSZTOW ck join DSG_CENTRUM_KOSZTOW ck1 on ck1.id = ck.id;
end

create trigger an_insert
on dsg_account_number
after insert
as
begin
  update an set an.title = an1.account_number, an.cn = an1.account_name  from dsg_account_number an join dsg_account_number an1 on an1.id = an.id;
end

CREATE TABLE dsg_invoice_p_multipe (
  ID numeric(18,0) identity(1,1) NOT NULL,
  DOCUMENT_ID numeric(18, 0) NOT NULL,
  FIELD_CN varchar(100) NOT NULL,
  FIELD_VAL varchar(100) NULL
);

CREATE TABLE DSG_INVOICE_P_MPK (
  id int identity(1,1) NOT NULL,
  CENTRUMID int,
  ACCOUNTNUMBER int,
  KWOTA float,
  primary key (id)
);

alter table DSG_INVOICE_P_MPK add STATUS int default 1 null;
alter table dsg_faktura_p add STATUS int default 1 null;
alter table DSG_INVOICE_P_MPK add STATUS_BY int default 10 null;

alter table DSG_INVOICE_P_MPK add SZEF_DZIALU smallint;
alter table DSG_INVOICE_P_MPK add SZEF_PIONU smallint;
alter table DSG_INVOICE_P_MPK add DYREKTOR_FINANSOWY smallint;


--Poni�ej niezb�dne do prezentacji!

CREATE TABLE [dsg_rozdzielacz](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int NOT NULL DEFAULT 1
);

insert into [dsg_rozdzielacz] (cn, title) values ('WA', 'WA - korekta warto�ci')
insert into [dsg_rozdzielacz] (cn, title) values ('IL', 'IL - korekta ilo�ci')
insert into [dsg_rozdzielacz] (cn, title) values ('RE', 'RE - korekta  reklamacyjna')
insert into [dsg_rozdzielacz] (cn, title) values ('RA', 'RA - rabat posprzeda�ny')
insert into [dsg_rozdzielacz] (cn, title) values ('AK', 'AK - akcje specjalne')
insert into [dsg_rozdzielacz] (cn, title) values ('WC', 'WC - wyr�wnanie cen na p�kach')
insert into [dsg_rozdzielacz] (cn, title) values ('SK', 'SK - skonto')
insert into [dsg_rozdzielacz] (cn, title) values ('IN', 'IN - inne')
insert into [dsg_rozdzielacz] (cn, title) values ('KIK', 'KIK - korekta ilo�ci KO�O')
insert into [dsg_rozdzielacz] (cn, title) values ('KWK', 'KWK - korekta warto�ci KO�O')
insert into [dsg_rozdzielacz] (cn, title) values ('RPK', 'RPK - Rabat posprzeda�ny KO�O')
insert into [dsg_rozdzielacz] (cn, title) values ('SKO', 'SKO - Skonto KO�O')
insert into [dsg_rozdzielacz] (cn, title) values ('RM', 'RM - Rabat marketingowy')
insert into [dsg_rozdzielacz] (cn, title) values ('RRU', 'RRU - rabat reklamacyjny wg umowy')
insert into [dsg_rozdzielacz] (cn, title) values ('KM', 'KM - korekty motocykle')

CREATE TABLE [dsg_payment_method](
	[id]        [int] IDENTITY(1,1) NOT NULL,
	[cn]        [varchar](50) NULL,
	[title]     [varchar](255) NULL,
	[centrum]   [int] NULL,
	[refValue]  [varchar](20) NULL,
	[available] [bit] NOT NULL DEFAULT 1,

    CONSTRAINT [PK_payment_method] PRIMARY KEY ([id] ASC)
);

insert into dsg_payment_method (cn, title) values ('01','Got�wka');
insert into dsg_payment_method (cn, title) values ('02','Odroczona');
insert into dsg_payment_method (cn, title) values ('03','Karta/czek');
insert into dsg_payment_method (cn, title) values ('04','Kompensata');
insert into dsg_payment_method (cn, title) values ('05','Za pobraniem');
insert into dsg_payment_method (cn, title) values ('06','Przedp�ata');
insert into dsg_payment_method (cn, title) values ('07','Bezp�atne');

alter table dsg_faktura_p add rozdzielacz int null;
alter table dsg_faktura_p add payment_method_id integer null;
alter table dsg_faktura_p add rodzaj_faktury integer;
alter table dsg_faktura_p add faktura_specjalna integer;
alter table dsg_faktura_p add pochodzenie smallint null;
alter table dsg_faktura_p add bledne_dane bit null;
alter table dsg_faktura_p add korekty bit;
alter table dsg_faktura_p add nr_faktury [varchar](30);
alter table dsg_faktura_p add advance_invoice_number varchar(255) null;
alter table dsg_faktura_p add contractor numeric(18,0);
alter table dsg_faktura_p add numer_rachunku_bankowego varchar(32) null;
alter table dsg_faktura_p add data_wystawienia [datetime] null;
alter table dsg_faktura_p add data_platnosci [datetime] null;
alter table dsg_faktura_p add data_zaksieg [datetime] null;
alter table dsg_faktura_p add kwota_netto [float] null;
alter table dsg_faktura_p add vat [float] null;
alter table dsg_faktura_p add waluta integer;
alter table dsg_faktura_p add akceptacja_stala bit default 0;

