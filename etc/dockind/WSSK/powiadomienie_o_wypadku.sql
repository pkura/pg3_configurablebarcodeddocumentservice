CREATE TABLE [WSSK_POWIADOM_WYPADEK](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[DESCRIPTION]		[varchar](250) NULL,
	[STATUSDOKUMENTU]	[varchar](20)  NULL,
	[DSUSER1]			[numeric](18,0)NULL,
	[DSUSER]			[numeric](18,0)NULL
);