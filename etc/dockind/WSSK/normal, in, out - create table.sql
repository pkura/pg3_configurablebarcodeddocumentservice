CREATE TABLE [dsg_rodzaj_wssk_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	available integer	
);

CREATE TABLE [dsg_rodzaj_wssk_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	available integer
);

CREATE TABLE [dbo].[DSG_NORMAL_DOCKIND](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_in] [int] NULL,
	[rodzaj_out] [int] NULL,
	[doc_number] [varchar](150) NULL,
	[storage_place] [varchar](250) NULL
) ON [PRIMARY];