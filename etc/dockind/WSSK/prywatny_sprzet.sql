CREATE TABLE [dbo].[WSSK_PRYWATNY_SPRZET](
	[DOCUMENT_ID]		[numeric](19, 0) NOT NULL,	
	[FIRSTNAME]			[varchar](20) NULL,
	[LASTNAME]			[varchar](20) NULL,
	[DIVISION]			[varchar](50) NULL,
	[WORKPLACE]			[varchar](20) NULL,
	[PHONE1]			[varchar](20) NULL,
	[PHONE2]			[varchar](20) NULL,
	[REASON]			[varchar](250) NULL,
	[DEVICEKIND]		[varchar](20) NULL,
	[DEVICETYPE]		[varchar](20) NULL,
	[OPERATINGSYSTEM]	[varchar](20) NULL,
	[ANTYVIRUS]			[varchar](20) NULL,
	[STAT]				[tinyint] NULL,
	[ADNOTATION]		[varchar](250) NULL,
	[STATUSDOKUMENTU]	[varchar](20) NULL,
) ON [PRIMARY]

GO

