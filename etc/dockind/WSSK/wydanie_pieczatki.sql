
CREATE TABLE [WSSK_WYD_PIECZATKI](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[TRESC]				[varchar](200) NOT NULL,
	[ILOSC]				[integer]      NOT NULL,
	[IMIE]				[varchar](35)  NULL,
	[NAZWISKO]			[varchar](35)  NULL,
	[DATAODB]			[date]         NULL,
	[TYPWNIOSKU]		[varchar](3)   NOT NULL,
	[DSUSER]			[numeric](18,0)NULL,
	
	[STATUSDOKUMENTU]	[varchar](20)  NULL,
	[STATUSPROCESU]		[varchar](20)  NULL,
	[MARKERWZNOWIENIA]	[INT] NULL
);

GO

Insert into DS_DIVISION values ('0A69720F013390AE1369BCA3DC84079587A', 'Dzia� Organizacyjno-Prawny', 'DzOP', 'division', 1, NULL, 0, NULL);

GO

Insert into DS_DIVISION values ('0A69820F013390AE1369BCA3DC84079587A', 'Dzia� Zaopatrzenia i Zam�wie� Publicznych', 'DzZZP', 'division', 1, NULL, 0, NULL);

GO