--faktura z opisem
CREATE TABLE [dbo].[WSSK_FAKTURA2](
	[DOCUMENT_ID]	[numeric](18, 0) NULL,
	[nr_faktury]	[varchar](30) NULL,
	
	[data_wystawienia]	[date] NULL,
	[data_wplywu]		[date] NULL,
	[termin_platnosci]	[date] NULL,
	
	[netto]				[numeric](18, 2) NULL,
	[vat]				[numeric](18, 2) NULL,
	[brutto]			[numeric](18, 2) NULL,
	[kwota_pozostala]	[numeric](18, 2) NULL,
	
	[sposob]			[numeric](18, 0) NULL,
	[opis]				[varchar](4096) NULL,
	[addinfo]			[varchar](4096) NULL,
	[naruszenie]		[tinyint] NULL,
	[dnipoterminie]		[numeric](18, 0) NULL,
	
	[faktura_skan]		[numeric](18, 0) NULL,
	[statusdokumentu]	[numeric](18, 0) NULL,
) ON [PRIMARY]
GO

--tabela multi dla faktury
CREATE TABLE WSSK_FAKTURA_MULTI (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

--tabela MPK dla faktury
CREATE TABLE [dbo].[WSSK_MPK_FAKTURY](
	ID						numeric(18,0) identity(1,1) NOT NULL,
	
	[centrumId]				[int] NULL,
	[accountNumber]			[varchar](50) NULL,
	[acceptingCentrumId]	[int] NULL,
	
	[amount] 				[float] NULL,
	
	[akceptant]				[numeric](18,0) NULL,
	[dataAkceptacji]		[date] NULL,
	[statusAkceptacji]		[int] NULL,
	
	[documentId]			[numeric](19, 0) NULL,
) ON [PRIMARY]
GO

