CREATE TABLE WSSK_ZWOLNIONY_PRACOWNIK(
	[DOCUMENT_ID] numeric (19,0) NOT NULL,
	[FIRSTNAME] [varchar](20) NULL,
	[LASTNAME] [varchar](20) NULL,
	[POSITION] [varchar](50) NULL,
	[TITLE] [varchar](20) NULL,
	[WORKPLACE] [varchar](20) NULL,
	
	[STATUSDOKUMENTU] [varchar] (20) null,
);

Insert into DS_DIVISION values ('0A64420F013390AE1369BCA3DC84079587A', 'Zainteresowani_Zwolnienie', 'zz', 'division', 1, NULL, 0, NULL);
GO