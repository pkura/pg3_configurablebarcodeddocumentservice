Stan tego opisu:
NIEPE�NY
pisane przed powstaniem us�ugi.


## Sens procesu:
kierownik mo�e chcie� zg�osi� zapotrzebowanie wewn�trzne (na materia�y, przedmioty itp.).
Zapotrzebowanie to zostanie przes�ane do dzia�u zam�wie� ale nie przez DocuSafe lecz przez ERP (automatycznie us�ug�).
W ERP pracownik dzia�u zam�wie� mo�e zmodyfikowa� zam�wienie (ilo�ciowo i jako�ciowo) i skierowac do realizacji
- w odpowiedzi aktualizowany jest dokument w EOD (kolumna ZREALIZOWANE).


## Powi�zania i zale�no�ci:
#zale�no�� od struktury (guid):
* Kierownicy
#us�uga wysy�aj�ca zapotrzebowanie do ERP 


## Inicjacja:
Tylko kierownik


## Mechanika procesu:
Inicjator na formatce ma pola s�u��ce do wyboru asortymentu z listy (na podstawie zaci�ganych z zewn�trz s�ownik�w).
Pozycje s�ownika maj� status AKTYWNY/SKASOWANY. Wyb�r pozycji jest z widoku pokazuj�cego tylko aktywne,
ale wy�wietlanie jest z ca�ej tabeli, �eby nie psu�y si� stare dokumenty.  
Nast�pnie lista ta trafia (przez us�ug�!) do dzia�u zam�wie� publicznych.
Na koniec ERP zwraca do DocuSafe, co zosta�o zam�wione. W DS jest por�wnanie:
Je�li pozycja istnieje w DS, to ma update kolumny ZREALIZOWANE;
je�li pozycja nie istnieje w DS. to robiony jest INSERT do tabeli dockindu z warto�ciami Artyku�=nazwa, ZAM�WIONE=0, ZREALIZOWANE=x
