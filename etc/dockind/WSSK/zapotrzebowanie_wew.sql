-- @author Rafal Odoj
-- @date 2012-08-07

CREATE TABLE WSSK_ZAPOTRZEBOWANIEWEW(
	[DOCUMENT_ID] 		[numeric](19, 0) NOT NULL,
	[ZAPOTRZEBOWANIE_DICT]		[int] NULL,  -- niepotrzebne pole (puste ale konieczne)
	[STATUSDOKUMENTU]			[varchar](20)  NULL,
);
GO

-- slownik wielowartosciowy, wielokrotny, jego pozycje sa wybierane ze slownikow zewnetrznych (asortymentu i jednostek miar)
CREATE TABLE WSSK_ZAPOTRZEBOWANIE_LISTA(
	[ID]	 		[int] IDENTITY(1,1) NOT NULL,
	[UNIT_FK] 		[numeric](19, 0) NOT NULL,
	[ASORT_FK]	 	[numeric](19, 0) NOT NULL,
	[AMOUNT] 		[numeric](9,  3) NULL,
	[ACCEPTEDAMOUNT][numeric](9,  3) NULL,
);
GO

-- wspolna tabela asocjacyjna (multiple value)
CREATE TABLE WSSK_ZAPOTRZEBOWANIE_MULTI(
	ID				int IDENTITY(1,1) NOT NULL,
    DOCUMENT_ID		numeric(18,0) NOT NULL,
    FIELD_CN		varchar(40) NOT NULL,
    FIELD_VAL		varchar(100) NULL
);
GO

-- slownik jednostek miar (zewnetrzny)
CREATE TABLE [WSSK_EXTERN_DICT_UNIT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) NULL
) ON [PRIMARY]
GO

INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('szt','sztuka',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('kg', 'kg',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('l',  'l',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('g',  'g',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('ml', 'ml',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('op', 'opakowanie',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_UNIT (cn,title,centrum,available,language) VALUES ('zg', 'zgrzewka',0,0,'pl');
GO

-- slownik asortymentu (zewnetrzny)
CREATE TABLE [WSSK_EXTERN_DICT_ASORT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) NULL
) ON [PRIMARY]
GO

INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('mop',				'mop',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('druciak',			'druciak',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('szczotka',		'szczotka',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('proszek_szor',	'proszek do szorowania',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('cypisek',			'Cypisek',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('ixi',				'IXI',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('radion',			'RADION',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('mydlo4you',		'myd�o 4YOU',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('oranzada',		'oran�ada',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('wegiel',			'w�giel',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('nakretkiM4',		'nakr�tki M4',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('plyn_Ludwik',		'p�yn do mycia naczy� Ludwik',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('plyn_Pur',		'p�yn do mycia naczy� Pur',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('myjka_Efekt',		'myjka Efekt!',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('woda_miner_5l',	'woda mineralna 5l',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('spinacze',		'spinacze biurowe',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('papier_a4',		'papier A4 POL-BIEL (ryza 500 k.)',0,0,'pl');
INSERT INTO WSSK_EXTERN_DICT_ASORT (cn,title,centrum,available,language) VALUES ('smar_grafit',		'smar grafitowy',0,0,'pl');
GO

Insert into DS_DIVISION values ('0A69920F013390AE1369BCA3DC84079587A', 'Kierownicy', NULL, 'group', 1, NULL, 0, NULL);
GO