CREATE TABLE WSSK_UDOSTEP_DOK_ZEWN (
DOCUMENT_ID			numeric (19,0),

FIRSTNAME			VARCHAR (50) NULL,
LASTNAME			VARCHAR (50) NULL,
PESEL				VARCHAR (11) NULL,
DATEFROM			date         NULL,
DATETO				date         NULL,

id 					int IDENTITY(1,1) NOT NULL, --wymagane przez dokument odpowiedzi
TITLE				varchar (150) NULL, --wymagane przez dokument odpowiedzi

STATUSDOKUMENTU		varchar(25),
);
