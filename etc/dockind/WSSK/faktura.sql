CREATE TABLE [dbo].[WSSK_FAKTURA](
	[DOCUMENT_ID]	numeric (19,0) NOT NULL,
	
	[sprzedawca]	[varchar](100) NULL,
	[nr_faktury]	[varchar](50) NULL,
	[kod_pocztowy]	[varchar](6)  NULL,
	[nip]			[varchar](13) NULL,
	[data_wystawienia]	[date]    NULL,
	[miejscowosc]	[varchar](50) null,
	[ulica]			[varchar](50) null,
	[nr_konta]		[varchar](50) null,
	[netto]			[numeric](18,2) null,
	[brutto]		[numeric](18,2) null,
	
	[GUID_JEDNOSTKA_DO_PRZEKAZANIA]		[varchar](50) NULL,
);
