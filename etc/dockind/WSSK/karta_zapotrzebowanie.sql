CREATE TABLE [dbo].[WSSK_KARTA_ZAPOTRZEBOWANIE](
	[DOCUMENT_ID]	numeric (19,0) NOT NULL,

	[FIRSTNAME]		[varchar](100) NULL,
	[LASTNAME]		[varchar](100) NULL,
	[POSITION]		[varchar](50) NULL,
	[ORGCELL]		[varchar](100) NULL,
	[PESEL]			[varchar](11) NULL,
	[MAIL]			[varchar](50) NULL,
	[PHONE]			[varchar](30) null,
	[READER_NEEDED]	[tinyint] null,

	[STATUSDOKUMENTU] [varchar] (20) null,
);
