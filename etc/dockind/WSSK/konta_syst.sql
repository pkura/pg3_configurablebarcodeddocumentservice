CREATE TABLE [dbo].[WSSK_KONTA_SYST](
	[DOCUMENT_ID] numeric (19,0) NOT NULL,
	[FIRSTNAME] [varchar](20) NULL,
	[LASTNAME] [varchar](20) NULL,
	[POSITION] [varchar](50) NULL,
	[TITLE] [varchar](20) NULL,
	[WORKPLACE] [varchar](20) NULL,
	[PESEL] [varchar](11) NULL,
	[NR1] [varchar](20) NULL,
	[NR2] [varchar](20) NULL,
	
	[MAIL] [tinyint] NULL,
	[SIMP] [tinyint] NULL,
	[IMPAX] [tinyint] NULL,
	[PORTAL] [tinyint] NULL,
	[ALTERIS] [tinyint] NULL,
	[INTERCLINIC] [tinyint] NULL,
	[APTEKA] [tinyint] NULL,
	[SZOI] [tinyint] NULL,
	[MZPROCEDURY] [tinyint] NULL,
	
	[PHONE1] [varchar] (20) null,
	[INTER] [tinyint] NULL,
	[LIMIT1] [varchar] (20) null,
	[LIMITVALUE] [NUMERIC] (19,0) null,
	[PHONE2] [varchar] (20) null,
	[INTER2] [tinyint] NULL,
	[LIMIT2] [varchar] (20) null,
	[DESCRIPTION] [varchar] (600) null,
	[ADNOTATION] [varchar] (600) null,
	[LOGIN] [varchar] (20) null,
	[TEL1] [varchar] (20) null,
	[TEL2] [varchar] (20) null,
	[GM] [tinyint] NULL,
	

	STATUSDOKUMENTU		varchar(20) NULL,
	MARKERWZNOWIENIA	INT NULL
);
