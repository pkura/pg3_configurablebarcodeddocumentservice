CREATE TABLE WSSK_UDOSTEP_DOK_ODP (
DOCUMENT_ID		numeric (19,0) NOT NULL,
[rodzaj_in]		[int] NULL,
[rodzaj_out]	[int] NULL,
[doc_number]	[varchar](150) NULL,
[storage_place]	[varchar](250) NULL,

odpowiedz_na	nvarchar (50) NULL, --document_id wniosku, na ktory odpowiadamy

odpowiedz		numeric (19,0) NULL, --tresc odpowiedzi

STATUSDOKUMENTU	varchar(25) NULL,
);
