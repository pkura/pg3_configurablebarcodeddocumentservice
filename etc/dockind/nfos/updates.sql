  
	--**************************************************
	--
	--		__NIE__  MODYFIKOWAC   W   NOTEPAD++
	--
	--   tu jest kodowanie ISO-8859-2 a nie cp1250 !
	--
	--**************************************************
  
  --poprawka, bo wczesniej � si� nie wy�wietlalo
  update [nfos].[dbo].[dso_in_document_delivery]
  set NAME='Osobi�cie' WHERE ID=3;
  update [nfos].[dbo].[dso_out_document_delivery]
  set NAME='Osobi�cie' WHERE ID=2;
  
  --typ dostawy ePUAP
  insert into [nfos].[dbo].[dso_in_document_delivery] 
  (POSN, NAME) VALUES (4, 'ePUAP');
  insert into [nfos].[dbo].[dso_out_document_delivery] 
  (POSN, NAME) VALUES (3, 'ePUAP');
	
  --admin musi miec koniecznie dzial, bo to jest wymagane do przyjecia pisma z ePUAP
  
  
  --modyfikacja DSO_Person - poszerzenie pol
  alter table dso_person alter column organization varchar(120) null;
  alter table dso_person alter column organizationdivision varchar(120) null;
  
  --001
alter table NFOS_DOC_IN add EPUAP_ID NUMERIC(19,0);
alter table NFOS_DOC_OUT add EPUAP_ID NUMERIC(19,0);
  
--zmiana timestamp na date
drop TABLE [dbo].[ds_epuap_export_document];

CREATE TABLE [dbo].[ds_epuap_export_document](
	[id]		[numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENTID]	[numeric](18, 0) NULL,
	[odpowiedz]		[varchar](200) NULL,
	[dataNadania]	[date] NOT NULL,
	[status]		[int] NULL,
	[person]		[numeric](18, 0) NULL,
	[rodzajUslugi]	[int] NULL,
	[epuap_skrytka] [numeric](19, 0) NULL,
 CONSTRAINT [DS_EPUAP_EXPORT_DOCUMENT_CON] UNIQUE NONCLUSTERED 
(
	[DOCUMENTID] ASC,
	[person] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY];

--002 - dodanie kolumny adres�w wsdl
ALTER TABLE NFOS_DICT_SYSTEM add ADRESWSDL VARCHAR(255);

--003 - perspektywa do RWA
alter table dso_rwa add isActive tinyint;

create view v_dso_rwa as
select id, ISNULL(digit1,'') + ISNULL(digit2,'')+ ISNULL(digit3,'')+ ISNULL(digit4,'')+ ISNULL(digit5,'') 'code',
achome,acother, description, remarks
from dso_rwa
where isActive is null;

--[2013.03.26] Tabela dla document_blank.xml
CREATE TABLE [NFOS_BLANK_OUT]( [DOCUMENT_ID] NUMERIC(19,0) NULL, ANULOWANY tinyint);

alter table nfos_doc_in add DOCCZYUPO integer;
alter table nfos_doc_out add DOCCZYUPO integer;

-- 004 - NFOS_LOTUS_EMAIL
CREATE TABLE NFOS_LOTUS_EMAIL (
	[id] [int] IDENTITY(1,1) NOT NULL,
	[import_id] numeric(19,0) NULL,
	[status] [int] not null,
	[error_code] [varchar](300) ,
	[create_date] datetime not null DEFAULT getdate(),
	[parse_date] datetime,
	[subject] [varchar](500),
	[content] [varchar](1000)
);
--stare
CREATE TABLE NFOS_DOC_EMAIL
(
	[DOCUMENT_ID]		[numeric](18, 0) NULL,
	[statusdokumentu]	[numeric](18, 0) NULL,
	[subject] [varchar](500),
	[EMAILCONTENT] [varchar](1000)
);
--koniec_stare

ALTER TABLE [dbo].[DSI_IMPORT_TABLE] ADD  DEFAULT ((0)) FOR [import_kind]
GO
ALTER TABLE [dbo].[DSI_IMPORT_TABLE] ADD  DEFAULT ((0)) FOR [import_status]
GO
ALTER TABLE [dbo].[DSI_IMPORT_TABLE] ADD  DEFAULT ((-1)) FOR [document_id]
GO
ALTER TABLE [dbo].[DSI_IMPORT_TABLE] ADD  DEFAULT ((0)) FOR [feedback_status]
GO
ALTER TABLE [dbo].[DSI_IMPORT_TABLE] ADD  CONSTRAINT [DF_DSI_IMPORT_TABLE_feedback_response]  DEFAULT ('DISABLED') FOR [feedback_response]
GO

--[2013.04.11] -- zmienione na varchar
ALTER TABLE NFOS_DOC_IN ADD IDOC_NUMER numeric (18,0)
ALTER TABLE NFOS_DOC_OUT ADD IDOC_NUMER numeric (18,0)

--[2013.04.12]
alter table NFOS_DICT_SYSTEM alter column refValue [varchar](200) NULL

ALTER VIEW [dbo].[v_bledne_ds_epuap_export_document]
AS
SELECT     id, DOCUMENTID, odpowiedz, dataNadania, status, person, rodzajUslugi, epuap_skrytka
FROM         dbo.ds_epuap_export_document
WHERE     (status < 9)

--[2013.04.30]-stare
CREATE TABLE NFOS_LOTUS_ATTACHMENT (
	id [int] IDENTITY(1,1) NOT NULL,
	lotus_email_id numeric(19,0) NOT NULL,
	create_date datetime not null DEFAULT getdate(),
	mime varchar(40) NOT NULL,
	content_file image not null	
	
);

--[2013.05.06]
CREATE TABLE NFOS_DOC_EMAIL_RECIPIENTS_MULTI(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY

--[2013.05.07]
ALTER TABLE NFOS_DOC_IN ALTER COLUMN IDOC_NUMER VARCHAR(50) NOT NULL

--[2013.05.09]
CREATE TABLE [dbo].[NFOS_DOC_EMAIL_LISTA](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FIRSTNAME] [varchar](50) NULL,
	[LASTNAME] [varchar](50) NULL,
	[ORGANIZATION] [varchar](50) NULL,
	[LOCATION] [varchar](50) NULL,
	[ORGANIZATIONDIVISION] [varchar](50) NULL,
	[STREET] [varchar](50) NULL,
	[ZIP] [varchar](50) NULL,
	[EMAIL] [varchar](50) NULL,
	[FAX] [varchar](50) NULL,
	[NIP] [varchar](50) NULL,
	[PESEL] [varchar](50) NULL,
	[REGON] [varchar](50) NULL,
	[TITLE] [varchar](50) NULL
) ON [PRIMARY]

--[2013.05.15]
CREATE TABLE [dbo].[NFOS_LOTUS_ATTACHMENT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[lotus_email_id] [numeric](19, 0) NOT NULL,
	[create_date] [datetime] NOT NULL, -- Default getdate(),
	[mime] [varchar](40) NOT NULL,
	[file_name] [varchar](50) NOT NULL,
	--[content_file] [text] NOT NULL,
	[content_file] image NOT NULL
 CONSTRAINT [PK_NFOS_LOTUS_ATTACHMENT] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[NFOS_LOTUS_ATTACHMENT]  WITH CHECK ADD  CONSTRAINT [FK_NFOS_LOTUS_ATTACHMENT_NFOS_LOTUS_EMAIL] FOREIGN KEY([lotus_email_id])
REFERENCES [dbo].[NFOS_LOTUS_EMAIL] ([id])
GO

ALTER TABLE [dbo].[NFOS_LOTUS_ATTACHMENT] CHECK CONSTRAINT [FK_NFOS_LOTUS_ATTACHMENT_NFOS_LOTUS_EMAIL]
GO

ALTER TABLE [dbo].[NFOS_LOTUS_ATTACHMENT] ADD  CONSTRAINT [DF__NFOS_LOTU__creat__02B25B50]  DEFAULT (getdate()) FOR [create_date]
GO
--------
CREATE TABLE [dbo].[NFOS_LOTUS_EMAIL](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[import_id] [numeric](19, 0) NULL,
	[status] [int] NOT NULL,
	[error_code] [varchar](300) NULL,
	[create_date] [datetime] NOT NULL,
	[parse_date] [datetime] NULL,
	[subject] [varchar](500) NULL,
	[content] [varchar](1000) NULL,
	[id_rwa] [numeric](18, 0) NULL,
	[sender] [varchar](50) NULL,
	[recipient] [varchar](50) NULL,
 CONSTRAINT [PK_NFOS_LOTUS_EMAIL] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[NFOS_LOTUS_EMAIL] ADD  CONSTRAINT [DF__NFOS_LOTU__creat__43C1049E]  DEFAULT (getdate()) FOR [create_date]
GO

--[2013-05-21] baza: dev i test identyczne.

-- zagubiony update 353
ALTER TABLE dsg_account_number add account_name VARCHAR(100) NULL;
--2013.06.25
alter table NFOS_DOC_OUT add TAGS varchar(500) NULL
alter table NFOS_DOC_OUT add LINK varchar(500) NULL
alter table NFOS_DOC_IN add TAGS varchar(500) NULL
alter table NFOS_DOC_IN add LINK varchar(500) NULL
alter table NFSO_DOC_EMAIL add IDOC_NUMER varchar(50) NULL
alter table NFOS_DOC_EMAIL add IDOC_DOCUMENTDATA datetime NULL
alter table ds_epuap_skrytka add [notification] tinyint null
update ds_epuap_skrytka set notification = 1

--2013.07.17
ALTER TABLE NFOS_EXPORT ADD error_message VARCHAR(250) NULL
ALTER TABLE DSI_IMPORT_ATTRIBUTES ALTER COLUMN attribute_value TEXT NULL
--2013.08.28
ALTER TABLE DSO_PERSON ALTER COLUMN FIRSTNAME varchar(128)
ALTER TABLE DSO_PERSON ALTER COLUMN LASTNAME varchar(128)