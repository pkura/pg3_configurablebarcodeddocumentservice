USE [nfos]
GO
/****** Object:  StoredProcedure [dbo].[dodajIDocNumerWychodzacy]    Script Date: 04/11/2013 14:14:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[dodajIDocNumerWychodzacy]
	@docId numeric (18, 0),
	@iDocNumer numeric (18, 0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @isIDOCNUM numeric (18, 0);
	

    -- Insert statements for procedure here
    BEGIN TRY
		BEGIN TRANSACTION
		IF EXISTS (SELECT * FROM NFOS_DOC_OUT WHERE DOCUMENT_ID = @docId and IDOC_NUMER is NULL)
			BEGIN
			UPDATE NFOS_DOC_OUT SET IDOC_NUMER = @iDocNumer WHERE DOCUMENT_ID = @docId;
			PRINT 'OK';
			END
		ELSE 
		PRINT 'Błąd';
		
		COMMIT
	END TRY
	BEGIN CATCH
		-- Determine if an error occurred.
			IF @@TRANCOUNT > 0
			ROLLBACK
		-- Return the error information.
		DECLARE @ErrorMessage nvarchar(4000),  @ErrorSeverity int;
		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY();
		RAISERROR(@ErrorMessage, @ErrorSeverity, 1);
	END CATCH;
    
    
    -- ALTER FROM NFOS_DOC_OUT WHERE DOCUMENT_ID = @docId;
    
    
END
