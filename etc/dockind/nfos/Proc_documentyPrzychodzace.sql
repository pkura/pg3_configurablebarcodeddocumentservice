/**Poniżej lista danych pobieranych przez iDoc z zewnętrznego systemu dla pism nadesłanych elektronicznie:
Tytuł/Temat/Czego dotyczy pismo
Nazwa nadawcy
Adres e-mail nadawcy
Data wpływu
Data pisma/wysłania (opcjonalne)
Znak pisma (opcjonalne)
Unikalny numer identyfikacyjny (używany przy zwrotnej wysyłce numeru kancelaryjnego do zewnętrznego systemu)
*/
ALTER PROCEDURE [documentyPrzychodzace]
AS
BEGIN
	Select doc.id as documentId,doc.TITLE as tytul,person.Title as NadawcaNazwa, person.FIRSTNAME as nadawcaImie, person.LASTNAME as nadawcaEmail,
	doc.CTIME as dataWplywu, null as dataPisma, null as znakPisma
	from DS_DOCUMENT doc
	left join DSO_IN_DOCUMENT did ON doc.ID = did.ID
	right join NFOS_DOC_IN  ndi ON doc.ID = ndi.DOCUMENT_ID
	right join DSO_PERSON person ON person.DOCUMENT_ID = doc.ID
	where ndi.DOCCZYUPO = 151
END
