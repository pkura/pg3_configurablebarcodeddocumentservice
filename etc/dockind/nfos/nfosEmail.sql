--dokument email
CREATE TABLE [dbo].[NFOS_DOC_EMAIL](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[statusdokumentu] [numeric](18, 0) NULL,
	[subject] [varchar](500) NULL,
	[EMAILCONTENT] [varchar](1000) NULL,
	[KATEGORIAARCHIWALNA] [numeric](18, 0) NULL,
	[ODBIORCY_DICT] [int] NULL,
	[IDOC_NUMER] [varchar](50) NULL,
	[IDOC_DOCUMENTDATA] [datetime] NULL,
	[archive_date] [datetime] NULL,
	[destroy_date] [datetime] NULL
) ON [PRIMARY]

GO

