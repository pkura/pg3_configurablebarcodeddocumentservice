--dokument wychodzacy
CREATE TABLE [dbo].[NFOS_DOC_OUT](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[statusdokumentu] [numeric](18, 0) NULL,
	[KATEGORIAARCHIWALNA] [numeric](18, 0) NULL,
	[SYSTEM] [numeric](18, 0) NULL,
	[DZIAL] [numeric](18, 0) NULL,
	[EPUAP_ID] [numeric](18, 0) NULL,
	[POWODZENIEWYSLANIA] [tinyint] NULL,
	[DATAWYSLANIA] [date] NULL,
	[WYMAGANEUPO] [tinyint] NULL,
	[DATAODBIORU] [date] NULL,
	[UPOLINKS] [numeric](18, 0) NULL,
	[DOCCZYUPO] [int] NULL,
	[IDOC_NUMER] [varchar](50) NULL,
	[IDOC_DOCUMENTDATA] [datetime] NULL,
	[TAGS] [varchar](1000) NULL,
	[LINK] [varchar](1000) NULL,
	[archive_date] [datetime] NULL,
	[destroy_date] [datetime] NULL
) ON [PRIMARY]

GO

--tabela multi dla dokumentu
CREATE TABLE [dbo].NFOS_DOC_OUT_MULTI (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

-- slownik systemow + ogolny
CREATE TABLE [dbo].NFOS_DICT_SYSTEM (
	[id]	[numeric](18,0) IDENTITY(1,1) NOT NULL,
	[cn]		[varchar](50) NULL,
	[title]		[varchar](255) NULL,
	[centrum]	[int] NULL,
	[refValue]	[varchar](20) NULL,
	[available]	[int] NULL,
	[language]	[varchar](256) NULL
	[ADRESWSDL]	[varchar](255) NULL;
);

---- slownik kategorii archiwalnych
--CREATE TABLE [dbo].NFOS_DICT_KATEGORIA_ARCHIWALNA (
--	[id] [int] IDENTITY(1,1) NOT NULL,
--	[cn] [varchar](50) NULL,
--	[title] [varchar](255) NULL,
--	[centrum] [int] NULL,
--	[refValue] [varchar](20) NULL,
--	[available] [int] NULL,
--	[language] [varchar](256) NULL
--);


CREATE TABLE [dbo].NFOS_DICT_KATEGORIA_ARCHIWALNA (
	[id]		[int] IDENTITY(1,1) NOT NULL,
	[nazwa]		[varchar](100) NULL,
	[lata]		[int] NULL
);

--uzupelnienie tabeli system�w dziedzinowych
INSERT [dbo].[NFOS_DICT_SYSTEM] ([cn], [title], [centrum], [refValue], [available], [language]) VALUES ('ogolny', 'og�lny', 0, NULL, 1, 'pl');
INSERT [dbo].[NFOS_DICT_SYSTEM] ([cn], [title], [centrum], [refValue], [available], [language]) VALUES ('kolektory', 'Kolektory', 0, NULL, 1, 'pl');
INSERT [dbo].[NFOS_DICT_SYSTEM] ([cn], [title], [centrum], [refValue], [available], [language]) VALUES ('eoplaty', 'eOplaty.geolog', 0, NULL, 1, 'pl');
INSERT [dbo].[NFOS_DICT_SYSTEM] ([cn], [title], [centrum], [refValue], [available], [language]) VALUES ('genWnioskowOPlatnosc', 'Generator wniosk�w o p�atno��', 0, NULL, 1, 'pl');

--uzupelnienie tabeli kategorii archiwalnych
INSERT [dbo].[NFOS_DICT_KATEGORIA_ARCHIWALNA] ([nazwa], [lata]) VALUES ('a5', 5);
INSERT [dbo].[NFOS_DICT_KATEGORIA_ARCHIWALNA] ([nazwa], [lata]) VALUES ('b5', 5);
INSERT [dbo].[NFOS_DICT_KATEGORIA_ARCHIWALNA] ([nazwa], [lata]) VALUES ('b25', 25);
INSERT [dbo].[NFOS_DICT_KATEGORIA_ARCHIWALNA] ([nazwa], [lata]) VALUES ('a50', 50);
INSERT [dbo].[NFOS_DICT_KATEGORIA_ARCHIWALNA] ([nazwa], [lata]) VALUES ('a100', 100);

-- tabela do wysylania dokument�w do system�w dziedzinowych
CREATE TABLE [dbo].NFOS_EXPORT (
	[id] [int] IDENTITY(1,1) NOT NULL,
	[document_id] numeric(19,0) NOT NULL,
	[status] [int] not null,
	[create_date] datetime not null,
	[send_date] datetime
);

