--dokument przychodzacy
CREATE TABLE [dbo].[NFOS_DOC_IN](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[statusdokumentu] [numeric](18, 0) NULL,
	[DOCCZYUPO] [numeric](18, 0) NULL,
	[KATEGORIAARCHIWALNA] [int] NULL,
	[SYSTEM] [int] NULL,
	[DZIAL] [numeric](18, 0) NULL,
	[EPUAP_ID] [numeric](18, 0) NULL,
	[UPOLINKS] [numeric](18, 0) NULL,
	[IDOC_NUMER] [varchar](50) NULL,
	[IDOC_DOCUMENTDATA] [datetime] NULL,
	[archive_date] [datetime] NULL,
	[destroy_date] [datetime] NULL,
	[tags] [varchar](1000) NULL,
	[LINK] [varchar](1000) NULL
) ON [PRIMARY]
GO

--tabela multi dla dokumentu
CREATE TABLE [dbo].NFOS_DOC_IN_MULTI (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

-- slowniki i ich zawartosc jest w pliku z sql dla dokumentu wychodzacego