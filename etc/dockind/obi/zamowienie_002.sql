CREATE TABLE [dbo].[DSG_CENTRUM_KOSZTOW_FAKTURY](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[centrumId] [int] NULL,
	[centrumCode] [varchar](100) NULL,
	[accountNumber] [varchar](50) NULL,
	[amount] [float] NULL,
	[documentId] [numeric](19, 0) NULL,
	[descriptionAmount] [varchar](600) NULL,
	[acceptance_mode] [smallint] NULL,
	[acceptingCentrumId] [int] NULL,
	[subgroupId] [int] NULL,
	[storagePlace] [varchar](300) NULL,
	[customsAgencyId] [int] NULL
)

CREATE TABLE [dbo].[DSG_CENTRUM_KOSZTOW](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[symbol] [varchar](50) NULL,
	[username] [varchar](62) NULL,
	[divisionGuid] [varchar](62) NULL,
	[needdescription] [int] NULL,
	[def_simple_acc] [smallint] NULL,
	[acceptance_modes_binary] [int] NULL,
	[iPowered] [smallint] NULL
)

CREATE TABLE [df_inst] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[imie] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nazwisko] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[name] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[oldname] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[nip] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[regon] [varchar] (30) COLLATE Polish_CI_AS NULL ,
	[ulica] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kod] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	[miejscowosc] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[email] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[faks] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[telefon] [varchar] (255) COLLATE Polish_CI_AS NULL ,
	[kraj] [varchar] (10) COLLATE Polish_CI_AS NULL ,
	 PRIMARY KEY  CLUSTERED
	(
		[id]
	)
);

create table dsg_item_order
(
	id 	numeric(18,0) identity(1,1),
	volume numeric(16,2),
	amount numeric(16,2),
	contractor numeric(18,0),
	product numeric(18,0),
	productGroup numeric(18,0),
	description varchar(600)
);

create table dsg_product
(
	id 	numeric(18,0) identity(1,1),
	name varchar(100),
	price numeric(16,2),
	cn varchar(50),
	contractorId 	numeric(18,0),
	productGroup numeric(18,0)
);

create table dsg_product_group
(
	id 	numeric(18,0) identity(1,1),
	name varchar(100),
	cn varchar(50)
);

create table dsg_kurs
(
	id 	numeric(18,0) identity(1,1),
	id_waluty varchar(100),
	kurs varchar(50)
);

insert into dsg_kurs(id_waluty,kurs)values(10,0.2);
insert into dsg_kurs(id_waluty,kurs)values(20,2.2);
insert into dsg_kurs(id_waluty,kurs)values(30,1.67);
insert into dsg_kurs(id_waluty,kurs)values(40,8.12);
insert into dsg_kurs(id_waluty,kurs)values(50,3.2);
insert into dsg_kurs(id_waluty,kurs)values(60,0.24);
insert into dsg_kurs(id_waluty,kurs)values(70,0.45);
insert into dsg_kurs(id_waluty,kurs)values(80,0.02);
insert into dsg_kurs(id_waluty,kurs)values(90,6.02);

create table dsg_umowa
(
	id 	numeric(18,0) identity(1,1),
	kwota numeric(16,2),
	termin datetime,
	kontrahent numeric(18,0),
	zalacznik numeric(18,0)
);

create table dsg_centrum_to_rachunek
(
	id_centrum numeric(18,0),
	numer_rach varchar(50),
);

create table dsg_dic_to_max
(
	dic_id numeric(18,0),
	kwota numeric(16,2)
);

INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (1, 'ZAMOWIENIE_WYSYLKA', 'Wysy�ka Zam�wienia', 'group',0);