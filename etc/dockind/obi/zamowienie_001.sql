CREATE TABLE [DSG_SANTANDER](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[data_wystawienia] [datetime] NULL,
	[data_dostawy] [datetime] NULL,
	[dostawca] [numeric](19, 0) NULL,
	[kwota] [float] NULL,
	[opis_zamowienia] [varchar](800) NULL,
	[nr_zamowienia] [numeric](19, 0) NULL,
	[centrum_kosztowe] [int]) NULL,
	akceptacja1 varchar(300),
	akceptacja2 varchar(300),
	akceptacja3 varchar(300),
	MPK numeric(18,0),
	rodzaj_zamowienia integer,
	zaliczka numeric(18,0),
	kwota_pln numeric(16,2),
	kurs numeric(16,2),
	waluta integer,
	bank varchar(50),
	kwota_zal double,
	nr_rachunku varchar(30),
	umowa numeric(18,0),
	O_UMOWA smallint
);

CREATE TABLE [DSG_SANTANDER_MULTIPLE_VALUE](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](20) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);

CREATE TABLE [dsg_zaliczka](
	[ID] [numeric](19, 0) identity(1,1) NOT NULL,
	[amount] [numeric](16, 2) NULL,
	numerRachunku varchar(50),
	bank varchar(100)
);