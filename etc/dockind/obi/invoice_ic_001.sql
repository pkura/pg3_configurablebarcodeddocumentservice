CREATE TABLE [DSG_INVOICE_IC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[data_wystawienia] [datetime] NULL,
	[data_platnosci] [datetime] NULL,
	[data_zaplaty] [datetime] NULL,
	[data_zaksieg] [datetime] NULL,
	[dostawca] [numeric](19, 0) NULL,
	[kwota_brutto] [float] NULL,
	[opis_towaru] [varchar](200) NULL,
	[nr_zestawienia] [numeric](19, 0) NULL,
	[nr_faktury] [varchar](30) NULL,
	[zaplacono] [smallint] NULL,
	[akceptacja_finalna] [smallint] NULL,
	[numer_rachunku_bankowego] [varchar](32) NULL,
	[kategoria] [int] NULL DEFAULT ((10)),
	[rozliczono] [smallint] NULL,
	[akceptacja_merytoryczna] [smallint] NULL,
	[akceptacja_ksiegowa] [smallint] NULL,
	[kwota_netto] [float] NULL,
	[vat] [float] NULL,
	[lokalizacja] [int] NULL,
	[kod_ksiegowania] [int] NULL,
	[numer_ksiegowania] [float] NULL,
	[stanowisko] [int] NULL,
	[struktura] [int] NULL,
	[data_ksiegowania] [datetime] NULL,
);

CREATE TABLE [DSG_INVOICE_IC_MULTIPLE_VALUE](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](20) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);