CREATE TABLE [dsg_enum1](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
);
insert into dsg_enum1 (cn,title)values('401-0110','Paliwo do sam.s�u�bowych zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('401-0120','Paliwo do w�k�w wid�owych');
insert into dsg_enum1 (cn,title)values('401-0130','Paliwo inne');
insert into dsg_enum1 (cn,title)values('401-0200','Artyku�y biurowe');
insert into dsg_enum1 (cn,title)values('401-0210','Materia�y konserwacyjne');
insert into dsg_enum1 (cn,title)values('401-0310','Ksi��ki,czasopisma,druki-penumerata');
insert into dsg_enum1 (cn,title)values('401-0320','Ksi��ki,czasopisma,druki-inne');
insert into dsg_enum1 (cn,title)values('401-0410','Mat.eksploatacyjne do sam.s�u�bowych-w�asne zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('401-0420','Mat.eksploatacyjne do sam.s�u�bowych-obce zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('401-0430','Mat.eksploatacyjne pozosta�e');
insert into dsg_enum1 (cn,title)values('401-0510','Materia�y informacyjne-katalogi');
insert into dsg_enum1 (cn,title)values('401-0520','Materia�y informacyjne-pozosta�e');
insert into dsg_enum1 (cn,title)values('401-0530','Materia�y do test�w');
insert into dsg_enum1 (cn,title)values('401-0611','Mat.eksploatacyjne do pozosta�ych �r.trwa�ych');
insert into dsg_enum1 (cn,title)values('401-0700','Pozosta�e materia�y');
insert into dsg_enum1 (cn,title)values('401-0810','�rodki czysto�ci');
insert into dsg_enum1 (cn,title)values('401-0820','Opakowania');
insert into dsg_enum1 (cn,title)values('402-0100','Zu�ycie energii - energia elektryczna');
insert into dsg_enum1 (cn,title)values('402-0200','Zu�ycie energii - gaz');
insert into dsg_enum1 (cn,title)values('402-0300','Zu�ycie energii - inna');
insert into dsg_enum1 (cn,title)values('403-0111','Transport dot.sprzeda�y - samochody krajowe');
insert into dsg_enum1 (cn,title)values('403-0112','Transport dot.sprzeda�y - samochody zagraniczne');
insert into dsg_enum1 (cn,title)values('403-0113','Transport dot.sprzeda�y - przesy�ki kurierskie');
insert into dsg_enum1 (cn,title)values('403-0114','Transport dot.sprzedazy - kolejowy');
insert into dsg_enum1 (cn,title)values('403-0115','Transport dot.sprzedazy - inny');
insert into dsg_enum1 (cn,title)values('403-0116','Transport - zatowarowanie filii');
insert into dsg_enum1 (cn,title)values('403-0120','Transport - odbi�r dostaw');
insert into dsg_enum1 (cn,title)values('403-0121','Transport os�b -wynajem �rodk�w transportu');
insert into dsg_enum1 (cn,title)values('403-0122','Transport os�b - taxi');
insert into dsg_enum1 (cn,title)values('403-0123','Transport os�b -inny');
insert into dsg_enum1 (cn,title)values('403-0124','Myjnie samochodowe zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('403-0200','Us�ugi remontowe');
insert into dsg_enum1 (cn,title)values('403-0300','Us�ugi komunalne');
insert into dsg_enum1 (cn,title)values('403-0310','Sprz�tanie i porz�dkowanie obiektu');
insert into dsg_enum1 (cn,title)values('403-0320','Utrzymanie obiektu - inne');
insert into dsg_enum1 (cn,title)values('403-0400','Og�oszenia - niedotyczace reklamy i o prac�');
insert into dsg_enum1 (cn,title)values('403-0521','Reklama-og�oszenia w �rodkach masowego przekazu');
insert into dsg_enum1 (cn,title)values('403-0530','Reklama - koszt targ�w i imprez');
insert into dsg_enum1 (cn,title)values('403-0541','Reklama - us�ugi pozosta�e');
insert into dsg_enum1 (cn,title)values('403-0600','Us�ugi pocztowe');
insert into dsg_enum1 (cn,title)values('403-0710','Przegl�dy samochod�w s�u�bowych zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('403-0720','Naprawy dodatkowe samochod�w s�u�bowych zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('403-0730','Naprawy sam.s�u�b.nieuznane przez ubezpieczycieli zawsze podajemy nr rejestracyjny samochodu');
insert into dsg_enum1 (cn,title)values('403-0810','Przegl�dy pozosta�ych �rodk�w trwa�ych');
insert into dsg_enum1 (cn,title)values('403-0820','Naprawy dodatkowe pozosta�ych �rodk�w trwa�ych');
insert into dsg_enum1 (cn,title)values('403-0900','Us�ugi informatyczne');
insert into dsg_enum1 (cn,title)values('403-0910','Us�ugi telekomunikacyjne - telefonia stacjonarna');
insert into dsg_enum1 (cn,title)values('403-0920','Us�ugi telekomunikacyjne - telefonia kom�rkowa');
insert into dsg_enum1 (cn,title)values('403-0930','Us�ugi telekomunikacyjne - ��czno�� teleinformatyczna');
insert into dsg_enum1 (cn,title)values('403-0940','Us�ugi telekomunikacyjne -pozosta�e');
insert into dsg_enum1 (cn,title)values('403-0950','Koszt ujednolicenia operatora');
insert into dsg_enum1 (cn,title)values('403-0960','Us�ugi szkoleniowe');
insert into dsg_enum1 (cn,title)values('403-1000','Monitoring i ochrona');
insert into dsg_enum1 (cn,title)values('403-1110','Us�ugi prawne - konsultingowe');
insert into dsg_enum1 (cn,title)values('403-1120','Us�ugi prawne - obs�uga inkasa i windykacji');
insert into dsg_enum1 (cn,title)values('403-1130','Us�ugi prawne - inne');
insert into dsg_enum1 (cn,title)values('403-1140','Us�ugi doradcze');
insert into dsg_enum1 (cn,title)values('403-1200','Us�uga z tytu�u reklamacji');
insert into dsg_enum1 (cn,title)values('403-1310','Us�ugi leasingowe - samochody s�u�bowe');
insert into dsg_enum1 (cn,title)values('403-1320','Us�ugi leasingowe i dzier�awa - inne �rodki trwa�e');
insert into dsg_enum1 (cn,title)values('403-1321','Us�ugi leasingowe pozosta�e');
insert into dsg_enum1 (cn,title)values('403-1400','Pozosta�e us�ugi');
insert into dsg_enum1 (cn,title)values('403-1401','ATR - sk�adniki,koszty,analizy');
insert into dsg_enum1 (cn,title)values('403-1402','SDCM - sk�adniki,koszty,analizy');
insert into dsg_enum1 (cn,title)values('403-1403','GIPA - sk�adniki,koszty,analizy');
insert into dsg_enum1 (cn,title)values('403-1404','Audyt sprawozda� finansowych');
insert into dsg_enum1 (cn,title)values('403-1405','Prowizje od sprzedawanych cz�ci');
insert into dsg_enum1 (cn,title)values('403-1406','T�umaczenia');
insert into dsg_enum1 (cn,title)values('403-1407','Us�uga przechowywania opon');
insert into dsg_enum1 (cn,title)values('403-1500','Obs�uga wynagrodze�');
insert into dsg_enum1 (cn,title)values('403-1600','Us�uga dystrybucyjna');
insert into dsg_enum1 (cn,title)values('403-1610','Us�uga dystrybucyjna nie wynikaj�ca z mar�y');
insert into dsg_enum1 (cn,title)values('403-1620','Us�uga dystrybucyjna - pozosta�a');
insert into dsg_enum1 (cn,title)values('403-1630','Us�uga logistyczna');
insert into dsg_enum1 (cn,title)values('403-1640','Us�uga dystrybucyjna - obs�uga floty');
insert into dsg_enum1 (cn,title)values('403-1650','Us�uga dystrybucyjna - cz�ci regenerowane');
insert into dsg_enum1 (cn,title)values('403-1660','Us�uga dystrybucyjna - cz�ci reklamacyjne');
insert into dsg_enum1 (cn,title)values('403-1670','Us�uga dystrybucyjna - marketingowa');
insert into dsg_enum1 (cn,title)values('403-1690','Us�ugi prowadzenia magazynu');
insert into dsg_enum1 (cn,title)values('403-1700','Koszty obs�ugi importu');
insert into dsg_enum1 (cn,title)values('403-1800','Koszty bankowe');
insert into dsg_enum1 (cn,title)values('403-1850','Us�ugi dotycz�ce analiz');
insert into dsg_enum1 (cn,title)values('403-1860','Badania homologacyjne');
