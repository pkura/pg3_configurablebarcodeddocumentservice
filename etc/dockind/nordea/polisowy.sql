﻿create table dsg_POLISOWY
(
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	barcode	varchar(20),
	data_przyjecia	datetime,
	status integer,
	numer_polisy varchar(20),
	klient integer,
	klasa integer,
	typ integer,
	deadline datetime
);