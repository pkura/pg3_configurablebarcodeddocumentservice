DROP TABLE dsg_yetico_plan_prewencyjny;

CREATE TABLE dsg_yetico_plan_prewencyjny(
	document_id numeric(18,0),
	status numeric(18,0),
	rok numeric(18,0),
	lista_prewencyjna_dict numeric(18,0),
);

CREATE TABLE dsg_yetico_plan_prewencyjny_multiple_value(
	DOCUMENT_ID numeric(18,0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100)
);


ALTER TABLE dsg_yetico_plan_prewencyjny ADD zaklad numeric(18, 0);