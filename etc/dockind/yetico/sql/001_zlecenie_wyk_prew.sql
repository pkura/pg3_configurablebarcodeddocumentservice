CREATE TABLE dsg_yetico_zlecenie_wyk_prew (
	document_id numeric(18,0),
	status numeric(18, 0),
	prewencja numeric(18, 0)
);

CREATE TABLE dsg_yetico_zlecenie_wyk_prew_multiple_value (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL 
);