CREATE TABLE dsg_yetico_wyrob(
	id numeric (18,0) IDENTITY(1,1) NOT NULL,
	nazwa varchar (249),
	wymiar varchar (150),
	rodzaj numeric (18,0),
	typ numeric (18,0),
	opis varchar(4096), 
	nr_faktury varchar (150),
	ilosc_rek_wyrobu numeric (18,0),
	ilosc_wyrobu_na_fakturze numeric (18,0),
	data_godzina_produkcji datetime,
	data_zaladunku date,
	gniazdo varchar (249),
	zmiana varchar (249),
	data_dostawy date,
	miejsce_dostawy varchar (249)
)
ALTER TABLE dsg_yetico_wyrob ADD PRIMARY KEY (id);
create index dsg_yetico_wyrob_index on dsg_yetico_wyrob (id);


alter table dsg_yetico_wyrob add BASE smallint null;

 ALTER TABLE dsg_yetico_wyrob ALTER COLUMN ilosc_rek_wyrobu numeric(18,4);
 ALTER TABLE dsg_yetico_wyrob ALTER COLUMN ilosc_wyrobu_na_fakturze numeric(18,4);

   alter table dsg_yetico_wyrob
   add erp_id numeric (18,0) null
   add klient_id varcahr (30)null