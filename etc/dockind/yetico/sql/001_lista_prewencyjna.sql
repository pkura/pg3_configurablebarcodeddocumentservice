DROP TABLE dsg_yetico_lista_prewencyjna;

CREATE TABLE dsg_yetico_lista_prewencyjna(
	document_id numeric(18,0),
	status numeric(18,0),
	nr_listy varchar(100),
	opis varchar(500),
	prewencja numeric(18,0),
	plan_prewencyjny_dict numeric(18,0)
);

CREATE TABLE dsg_yetico_lista_prewencyjna_multiple_value(
	DOCUMENT_ID numeric(18,0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100)
);

ALTER TABLE dsg_yetico_lista_prewencyjna ADD zaklad numeric(18, 0);