drop table dsg_yetico_prewencja_item;

CREATE TABLE [dbo].[dsg_yetico_prewencja_item](
	[id] numeric(18, 0) IDENTITY(1,1) NOT NULL,
	[nr_listy] varchar(100) NULL,
	[akcja_prewencyjna] numeric(18,0),
	[opis] varchar(500),
	[planowana_data_realizacji] datetime NULL,
	[planowana_data_zakonczenia] datetime NULL,
	[data_realizacji] datetime NULL,
	[czas_realizacji] numeric(18,0) NULL,
	[status] numeric(18, 0) NULL,
	[imie_nazwisko] numeric(18,0)
);


CREATE TABLE [dbo].[dsg_yetico_multiple_value_prewencja_item] (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

DROP TABLE [dbo].[dsg_yetico_prewencja]

CREATE TABLE [dbo].[dsg_yetico_prewencja]
(
	[document_id] [numeric](18, 0) NULL,
	[maszyna_podzespol_czesc] varchar (50),
	[typ] [numeric](18, 0) NULL,
	[czestotliwosc] [numeric] (18,0) NULL,
	[data_poczatkowa] datetime NULL,
	[planowany_czas_realizacji] numeric (18,0),
	[liczba_pracownikow] numeric (18,0),
	[zawod] numeric (18,0),
	[postoj] bit,
	[opis_czynnosci] varchar (500),
	[termin_realizacji] varchar (50),
	[status] numeric (18,0),
	prewencja numeric(18,0)
);

alter table dsg_yetico_prewencja drop column maszyna_podzespol_czesc
alter table dsg_yetico_prewencja add maszyna_podzespol_czesc numeric(18,0)