-- rodzaje niezgodnosci
--<enum-item id="10" cn="ILOSCIOWE" title="Ilo�ciowe"/>
--<enum-item id="15" cn="JAKOSCIOWE" title="Jako�ciowe"/>
--<enum-item id="20" cn="HANDLOWE" title="Handlowe"/>
--<enum-item id="25" cn="LOGISTYCZNE" title="Logistyczne"/>
--<enum-item id="30" cn="MAGAZYNOWE" title="Magazynowe"/>
create table dsg_yetico_rodzaje_niezgodnosci
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL,
	
);
SET IDENTITY_INSERT dsg_yetico_rodzaje_niezgodnosci on;
insert into dsg_yetico_rodzaje_niezgodnosci (id, title, refValue, cn, centrum, available) values (10, 'ILOSCIOWE', null,  'ILOSCIOWE', null,1);
insert into dsg_yetico_rodzaje_niezgodnosci (id, title, refValue, cn, centrum, available) values (15, 'JAKOSCIOWE', null, 'JAKOSCIOWE',null,1);
insert into dsg_yetico_rodzaje_niezgodnosci (id, title, refValue, cn, centrum, available) values (20, 'HANDLOWE', null, 'HANDLOWE',null,1);
insert into dsg_yetico_rodzaje_niezgodnosci (id, title, refValue, cn, centrum, available) values (25, 'LOGISTYCZNE', null, 'LOGISTYCZNE',null,1);
insert into dsg_yetico_rodzaje_niezgodnosci (id, title, refValue, cn, centrum, available) values (30, 'MAGAZYNOWE', null, 'MAGAZYNOWE',null,1);
SET IDENTITY_INSERT dsg_yetico_rodzaje_niezgodnosci off;
-- typy niezgodnosci zalezne od rodzaju
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIECZYTELNE WA�KI', 15, '1',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK WA�K�W', 15, '2',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEDOPAROWANY\ KRUCHY WYR�B', 15, '3',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZEPAROWANY\ SKURCZONY WYR�B', 15, '4',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRUDNY WYR�B', 30, '5',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONY WYR�B PODCZAS TRANSPORTU', 25, '6',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONY WYR�B', 30, '7',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SPIEKI W WYROBIE', 15, '8',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEWIDOCZNA DATA PRODUKCJI', 15, '9',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK DATY PRODUKCJI', 15, '10',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEWIDOCZNY NUMER\ NAZWA OPERATORA', 15, '11',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK NUMERU\ NAZWY OPERATORA', 15, '12',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEWIDOCZNA NAZWA ZAK�ADU PRODUKCYJNEGO', 15, '13',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK NAZWY ZAK�ADU PRODUKCYJNEGO', 15, '14',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('KRZYWY BALOT/BALOTY', 30, '15',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEPOSKLEJANA FOLIA', 15, '16',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('POROZRYWANA FOLIA', 15, '17',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PACZKI W BALOCIE NIEPOUK�ADANE WG. WZORU FOLII', 15, '18',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OGONY ZE STRECZU', 15, '19',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('DATA PRODUKCJI POWY�EJ MIESI�CA', 15, '20',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('KRZYWO U�O�ONE P�YWY W PACZCE', 15, '21',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('Z�ӣK�Y WYR�B', 15, '22',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEPOPRAWNIE OPISANY FORMULARZ REKLAMACJI', 25, '23',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('CECHY GEOMETRYCZNE POZA TOLERANCJ�', 15, '24',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('D�UGO�� POZA TOLERANCJ�', 15, '25',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SZEROKO�� POZA TOLERANCJ�', 15, '26',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('GRUBO�� POZA TOLERANCJ�', 15, '27',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PROSTOK�TNO�� POZA TOLERANCJ�', 15, '28',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('P�ASKO�� POZA TOLERANCJ�', 15, '29',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MASA\ G�STO�� WYROBU POZA TOLERANCJ�', 15, '30',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEZABEZPIECZONE OKTABINY Z SUROWCEM', 15, '31',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEZABEZPIECZONE KLATKI Z BUTLAMI Z GAZEM', 15, '32',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('DOBRY WYR�B PRZEZNACZONY DO ZMIELENIA', 15, '33',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYR�B STARSZY NI� 1 MIESI�C', 30, '34',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ZA MA�A ILO�� OTWOR�W WENTYLACYJNYCH W STRECZU', 15, '35',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SZTYWNO�� DYNAMICZNA POZA TOLERANCJ�', 15, '36',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYR�B BRUDNY', 15, '37',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZEWR�CONE PALETY', 30, '38',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEDOSYPANY WYR�B', 15, '39',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('CIA�A OBCE W WYROBIE', 15, '40',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SUROWIEC INNEGO KOLORU W WYROBIE', 15, '41',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MOKRY WYR�B', 15, '42',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('STOPIONY WYR�B', 15, '43',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEW�A�CIWE OZNAKOWANIE WYROBU', 15, '44',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAKI W DOSTAWIE', 10, '45',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NADSTAN W DOSTAWIE', 10, '46',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYTOPIENIA W WYROBIE', 15, '47',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYPUK�O�CI W WYROBIE', 15, '48',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZE�WITY MI�DZY GRANULKAMI', 15, '49',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK NӯEK W DOSTAWIE', 30, '50',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEW�A�CIWA ILO�� PACZEK W DOSTAWIE', 10, '51',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYR�B SKURCZY� SI� PODCZAS OBR�BKI', 15, '52',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SPӬNIONA DOSTAWA', 25, '53',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('B��DNY ADRES DOSTAWY', 25, '54',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('B��DNIE WYPISANE ZLECENIE PRODUKCYJNE', 20, '55',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('B��DNIE WYPISNY DOKUMENT WZ', 20, '56',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('B��DNIE WYPISANA FAKTURA', 20, '57',null,1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('B��DNIE ODCZYTANE ZAM�WIENIE ', 20, '58',null,1);
-------------------------------------------------------------------------

delete from dsg_yetico_typ_niezgodnosci where title = 'NIECZYTELNE WA�KI';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEDOPAROWANY\ KRUCHY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'PRZEPAROWANY\ SKURCZONY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'BRUDNY WYR�B';

delete from dsg_yetico_typ_niezgodnosci where title = 'USZKODZONY WYR�B PODCZAS TRANSPORTU';
delete from dsg_yetico_typ_niezgodnosci where title = 'USZKODZONY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'SPIEKI W WYROBIE';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEWIDOCZNA DATA PRODUKCJI';

delete from dsg_yetico_typ_niezgodnosci where title = 'NIEWIDOCZNY NUMER\ NAZWA OPERATORA';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEWIDOCZNA NAZWA ZAK�ADU PRODUKCYJNEGO';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEPOSKLEJANA FOLIA';
delete from dsg_yetico_typ_niezgodnosci where title = 'POROZRYWANA FOLIA';

delete from dsg_yetico_typ_niezgodnosci where title = 'PACZKI W BALOCIE NIEPOUK�ADANE WG. WZORU FOLII';
delete from dsg_yetico_typ_niezgodnosci where title = 'OGONY ZE STRECZU';
delete from dsg_yetico_typ_niezgodnosci where title = 'DATA PRODUKCJI POWY�EJ MIESI�CA';
delete from dsg_yetico_typ_niezgodnosci where title = 'Z�ӣK�Y WYR�B';

delete from dsg_yetico_typ_niezgodnosci where title = 'CECHY GEOMETRYCZNE POZA TOLERANCJ�';
delete from dsg_yetico_typ_niezgodnosci where title = 'MASA\ G�STO�� WYROBU POZA TOLERANCJ�';
delete from dsg_yetico_typ_niezgodnosci where title = 'DOBRY WYR�B PRZEZNACZONY DO ZMIELENIA';
delete from dsg_yetico_typ_niezgodnosci where title = 'WYR�B STARSZY NI� 1 MIESI�C';

delete from dsg_yetico_typ_niezgodnosci where title = 'WYR�B BRUDNY';
delete from dsg_yetico_typ_niezgodnosci where title = 'PRZEWR�CONE PALETY';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEDOSYPANY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'CIA�A OBCE W WYROBIE';

delete from dsg_yetico_typ_niezgodnosci where title = 'SUROWIEC INNEGO KOLORU W WYROBIE';
delete from dsg_yetico_typ_niezgodnosci where title = 'MOKRY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'STOPIONY WYR�B';
delete from dsg_yetico_typ_niezgodnosci where title = 'NIEW�A�CIWE OZNAKOWANIE WYROBU';

delete from dsg_yetico_typ_niezgodnosci where title = 'WYTOPIENIA W WYROBIE';
delete from dsg_yetico_typ_niezgodnosci where title = 'WYPUK�O�CI W WYROBIE';
delete from dsg_yetico_typ_niezgodnosci where title = 'PRZE�WITY MI�DZY GRANULKAMI';
delete from dsg_yetico_typ_niezgodnosci where title = 'WYR�B SKURCZY� SI� PODCZAS OBR�BKI';

insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('G�STO�� NASYPOWA SPIENIANEGO SUROWCA POZA TOLERANCJ�', 15, '100', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MASA\ G�STO�� BLOK�W POZA TOLERANCJ�', 15, '101', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MASA\ G�STO�� WYROBU GOTOWEGO POZA TOLERANCJ�', 15, '102', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SUROWIEC SEZONOWANY POZA TOLERANCJ�', 15, '103', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BLOKI SEZONOWANE POZA TOLERANCJ�', 15, '104', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WTR�CENIA W WYROBIE GOTOWYM', 15, '105', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WTR�CENIA W BLOKACH', 15, '100', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK AKTUALNYCH INFORMACJI NA TEMAT MAS\ G�STO�CI\ CZAS�W SEZONOWANIA SUROWCA I WYROB�W GOTOWYCH', 15, '106', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEZGODNA ETYKIETA WYROBU GOTOWEGO', 15, '107', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SPIEKI W WYROBIE GOTOWYM', 15, '108', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SPIEKI W BLOKACH', 15, '109', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('SPIEKI PODCZAS SPIENIANIA', 15, '110', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEDOPAROWANY\ KRUCHY WYR�B GOTOWY', 15, '111', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEDOPAROWANY\ KRUCHY BLOK', 15, '112', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZEPAROWANY\ SKURCZONY WYR�B GOTOWY', 15, '113', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZEPAROWANY\ SKURCZONY BLOK', 15, '114', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ILO�� DODAWANEGO KRUSZYWA POZA TOLERANCJ�', 15, '115', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ZASYP NIEW�A�CIWEGO KRUSZYWA DO BLOKU', 15, '116', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ZASYP NIEW�A�CIWEGO SILOSA Z KRUSZYWEM', 15, '117', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEW�A�CIWE KRUSZYWO W WYROBIE GOTOWYM - "SER"', 15, '118', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK ETYKIET ODK�ADANYCH PRZEZ SPIENIAJ�CYCH DO CEL�W REKLAMACJI SUROWCA', 15, '119', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PY� W BLOKACH', 15, '120', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('FREZ P�YT POZA TOLERANCJ�', 15, '121', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PACZKI W BALOCIE NIEU�O�ONE WG INSTRUKCJI', 15, '122', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('POMIESZANE SUROWCE W SILOSIE', 15, '123', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OZNAKOWANIE WYROBU GOTOWEGO NIEPOPRAWNE', 15, '124', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OZNAKOWANIE BLOKU NIEPOPRAWNE', 15, '125', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OZNAKOWANIE WYROBU GOTOWEGO NIECZYTELNE', 15, '126', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OZNAKOWANIE BLOKU NIECZYTELNE', 15, '127', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OZNAKOWANIE OKTABIN ETYKIET� Z DAT� WA�NO�CI NIEW�A�CIWE', 15, '128', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRAK ETYKIETY Z DAT� WA�NO�CI NA OKTABINIE', 15, '129', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('CI�CIE NIEODPOWIEDNIEGO WYROBU', 15, '130', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NADLEWY W WYROBIE GOTOWYM', 15, '131', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEDOSYPANY WYR�B GOTOWY', 15, '132', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('�CISKANIE (CS(10)) POZA TOLERANCJ�', 15, '133', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ZGINANIE (BS) POZA TOLERANCJ�', 15, '134', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ROZRYWANIE (TR) POZA TOLERANCJ�', 15, '135', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('LAMBDA (?) POZA TOLERANCJ�', 15, '136', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PALNO�� POZA TOLERANCJ�', 15, '137', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZE�WITY MI�DZY GRANULKAMI W WYROBIE GOTOWYM', 15, '138', null, 1);

insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONA OKTABINA Z SUROWCEM', 30, '200', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONY BLOK', 30, '201', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONY WYR�B GOTOWY', 30, '202', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('FOLIA USZKODZONA W WYROBIE GOTOWYM', 30, '203', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('FOLIA NIEPOSKLEJANA W WYROBIE GOTOWYM', 30, '204', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('FOLIA ZA LU�NO ZA�O�ONA NA PACZKI', 30, '205', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('FOLIA STRETCH ZA LU�NO ZA�O�ONA NA BALOTY', 30, '206', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WODA Z DACHU KAPIE NA SUROWIEC', 30, '207', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('ZAOKR�GLONE ROGI WYROBU GOTOWEGO\ BALOTU', 30, '208', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MIELENIE WYROBU GOTOWEGO BEZ WAD', 30, '209', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('PRZEZNACZENIE DO MIELENIA WYROBU GOTOWEGO BEZ WAD', 30, '210', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MOKRE BLOKI', 30, '211', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('MOKRY WYR�B GOTOWY', 30, '212', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('OGONY ZE STRETCHU W BALOCIE', 30, '213', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('NIEPORZ�DEK NA MAGAZYNIE', 30, '214', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('BRUDNY WYR�B GOTOWY', 30, '215', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('Z�ӣK�Y WYR�B GOTOWY', 30, '216', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('WYR�B GOTOWY STARSZY NI� 1 MIESI�C', 30, '217', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('STOPIONY WYR�B GOTOWY', 30, '218', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('STOPIONY BLOK', 30, '219', null, 1);
insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('STOPIONA FOLIA W WYROBIE GOTOWYM', 30, '220', null, 1);

insert into dsg_yetico_typ_niezgodnosci (title, refValue, cn, centrum, available) values ('USZKODZONY WYR�B GOTOWY PODCZAS TRANSPORTU', 25, '300', null, 1);