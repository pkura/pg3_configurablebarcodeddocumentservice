CREATE TABLE [dbo].[dsg_yetico_plan_prewencji](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[rok] [numeric](18, 0) NULL,
	[data_utworzenia] [date] NULL,
	[czas_postoju] [float] NULL,
	[planowana_data_realizacji] [date] NULL,
	[termin_realizacji] [numeric] (18,0) NULL)


CREATE TABLE [dbo].[dsg_yetico_lista_prewencji](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[nr_listy] [varchar](50) NULL,
	[opis] [varchar](4096) NULL,
	[planowana_data_zakonczenia] [date] NULL,
	[data_realizacji] [date] NULL,
	[czas_realizacji] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[imie_nazwisko] [numeric] (18,0) NULL
);


CREATE TABLE [dbo].[dsg_yetico_naprawa](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[nr_zlecenia_erp] [varchar](50) NULL,
	[czas_postoju] [numeric] (18, 0) NULL,
	[status_naprawy] [numeric](18, 0) NULL,
	[opis] [varchar](4096) NULL,
	[czas_realizacji] [numeric](18, 0) NULL,
	[status_prewencji] [numeric](18, 0) NULL,
	[imie_nazwisko] [numeric] (18,0) NULL,
	[data_realizacji] [date] NULL,
);

CREATE TABLE [dbo].[dsg_yetico_prewencja]
(
	[document_id] [numeric](18, 0) NULL,
	[maszyna_podzespol_czesc] varchar (50),
	[typ] [numeric](18, 0) NULL,
	[czestotliwosc] [numeric] (18,0) NULL,
	[data_poczatkowa] date NULL,
	[planowany_czas_realizacji] numeric (18,0),
	[liczba_pracownikow] numeric (18,0),
	[zawod] numeric (18,0),
	[postoj] bit,
	[opis_czynnosci] varchar (50),
	[termin_realizacji] varchar (50),
);

alter table [dbo].[dsg_yetico_prewencja] add [status] numeric (18,0)