ALTER TABLE dsg_yetico_prewencja_item ADD id_mirror numeric(18, 0)

CREATE TRIGGER yetico_prewencja_item_AfterInsert
	ON dsg_yetico_prewencja_item
	AFTER INSERT
AS
	UPDATE dsg_yetico_prewencja_item
	SET mirror_id = @@IDENTITY WHERE dsg_yetico_prewencja_item.id = @@IDENTITY;
GO