USE [yetico]
GO

CREATE TABLE [dbo].[dsg_yetico_stawki_reklamacji](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


alter table dsg_yetico_rek_zew add STAWKI_REKLAMACJI numeric (18,0);


set IDENTITY_INSERT  [dsg_yetico_stawki_reklamacji] ON;
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1010, 'WYTWORZENIE_GAL', 'WYTWORZENIE_GAL', '14.31',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1015, 'WYTWORZENIE_GW', 'WYTWORZENIE_GW', '19.11',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1020, 'WYTWORZENIE_OL', 'WYTWORZENIE_OL', '13.23',1);

insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1510, 'FOLIA_GAL', 'FOLIA_GAL', '1.69',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1515, 'FOLIA_GW', 'FOLIA_GW', '1.69',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (1520, 'FOLIA_OL', 'FOLIA_OL', '1.69',1);

insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2010, 'TRANSPORT_PL_GAL', 'TRANSPORT_PL_GAL', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2015, 'TRANSPORT_PL_GW', 'TRANSPORT_PL_GW', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2020, 'TRANSPORT_PL_OL', 'TRANSPORT_PL_OL', '1',1);

insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2510, 'TRANSPORT_DE_GAL', 'TRANSPORT_DE_GAL', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2515, 'TRANSPORT_DE_GW', 'TRANSPORT_DE_GW', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (2520, 'TRANSPORT_DE_OL', 'TRANSPORT_DE_OL', '1',1);

insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3010, 'RECYKLING_GAL', 'RECYKLING_GAL', '2.4',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3015, 'RECYKLING_GW', 'RECYKLING_GW', '2.4',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3020, 'RECYKLING_OL', 'RECYKLING_OL', '2.4',1);

insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3510, 'INNE_GAL', 'INNE_GAL', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3515, 'INNE_GW', 'INNE_GW', '1',1);
insert into [dsg_yetico_stawki_reklamacji] (ID, CN, TITLE, refValue, available)
values (3520, 'INNE_OL', 'INNE_OL', '1',1);


set IDENTITY_INSERT  [dsg_yetico_stawki_reklamacji] OFF;