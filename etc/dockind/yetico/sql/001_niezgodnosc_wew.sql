CREATE TABLE dsg_yetico_wymagana_masa_gestosc_wyrobu(
	id numeric(18, 0) IDENTITY(1,1) NOT NULL PRIMARY KEY,
	wymagana_masa numeric(18, 2),
	masa_zmierzona numeric(18, 2),
	odchylka_masy numeric(18, 2),
	wymagana_gestosc numeric(18, 2),
	gestosc_zmierzona numeric(18, 2),
	odchylka_gestosci numeric(18, 2),
	opis varchar(4096)
);