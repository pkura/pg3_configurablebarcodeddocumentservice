insert into DS_DIVISION values ('REJESTR_REKLAMACJI','Rejestr reklamacji',null,'group',1,null,0,null);




USE yetico;
GO
-- Declare the variables to store the values returned by FETCH.
DECLARE @userID numeric(19,0);
DECLARE @divisionID numeric (19,0);

DECLARE kursor CURSOR FOR
select USER_ID from DS_USER_TO_DIVISION 
where DIVISION_ID in (
select ID from DS_DIVISION where name in ('Dzia� kontroli wewn�trznej','Kierownik laboratorium','Laborantka')
union
select ID from DS_DIVISION where PARENT_ID in(select ID from DS_DIVISION 
where name in ('Dzia� kontroli wewn�trznej','Kierownik laboratorium','Laborantka'))
)

OPEN kursor;

SELECT @divisionID = ID from DS_DIVISION where GUID = 'REJESTR_REKLAMACJI'

-- Perform the first fetch and store the values in variables.
-- Note: The variables are in the same order as the columns
-- in the SELECT statement. 

FETCH NEXT FROM kursor
INTO @userID;

-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
WHILE @@FETCH_STATUS = 0
BEGIN

	INSERT INTO DS_USER_TO_DIVISION (USER_ID,DIVISION_ID,source) values (@userID, @divisionID, 'own')

   -- This is executed as long as the previous fetch succeeds.
   FETCH NEXT FROM kursor
   INTO @userID;
END

CLOSE kursor;
DEALLOCATE kursor;
GO



INSERT INTO DS_USER_TO_DIVISION (USER_ID,DIVISION_ID,source) values ((select USER_ID from DS_USER_TO_DIVISION where DIVISION_ID = (
select ID from DS_DIVISION where name like '%technolog%' 
)), (select ID from DS_DIVISION where GUID = 'REJESTR_REKLAMACJI'), 'own')