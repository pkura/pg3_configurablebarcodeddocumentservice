DROP TABLE dsg_yetico_naprawa;

CREATE TABLE dsg_yetico_naprawa(
	[document_id] numeric(18, 0) NOT NULL,
	[nr_zlecenia_erp] varchar(100) NULL,
	[lider_zespolu] numeric(18,0) NULL,
	[czas_postoju] numeric(18,0) NULL,
	[status_naprawy] numeric(18,0) NULL,
	[opis] varchar(500) NULL,
	[czas_realizacji] numeric(18,0) NULL,
	[status_prewencji] numeric(18,0) NULL,
	[imie_nazwisko] numeric(18,0) NULL,
	[data_realizacji] date NULL,
	[status] numeric(18,0) NULL,
	[maszyna_podzespol_czesc] numeric(18,0)
);

ALTER TABLE dsg_yetico_naprawa ADD zaklad numeric(18, 0);