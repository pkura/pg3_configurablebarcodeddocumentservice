
-- 1010 WYTWORZENIE_GAL
UPDATE [dbo].[dsg_yetico_stawki_reklamacji]
   SET refValue = '18.13'
 WHERE id=1010

--1015	WYTWORZENIE_GW
 UPDATE [dbo].[dsg_yetico_stawki_reklamacji]
   SET refValue = '19.04'
 WHERE id=1015

 --1020	WYTWORZENIE_OL
 UPDATE [dbo].[dsg_yetico_stawki_reklamacji]
   SET refValue = '12.88'
 WHERE id=1020

--2010	TRANSPORT_PL_GAL
--2015	TRANSPORT_PL_GW
--2020	TRANSPORT_PL_OL
 UPDATE [dbo].[dsg_yetico_stawki_reklamacji]
   SET refValue = '9.40'
 WHERE id in (2010,2015,2020)

--2510	TRANSPORT_DE_GAL
--2515	TRANSPORT_DE_GW
--2520	TRANSPORT_DE_OL
 UPDATE [dbo].[dsg_yetico_stawki_reklamacji]
   SET refValue = '24.04'
 WHERE id in (2510,2515,2520)