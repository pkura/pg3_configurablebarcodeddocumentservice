CREATE TABLE [dbo].[dsg_yetico_prewencja_item](
	[id] numeric(18, 0) IDENTITY(1,1) NOT NULL,
	[nr_listy] varchar(100) NULL,
	[akcja_prewencyjna] numeric(18,0),
	[opis] varchar(1000),
	[planowana_data_realizacji] date NULL,
	[data_realizacji] date NULL,
	[czas_realizacji] numeric(18,0) NULL,
	[status] numeric(18, 0) NULL,
	[imie_nazwisko] varchar(100)
);

alter table dsg_yetico_lista_prewencyjna add prewencja numeric(18,0)