ALTER TABLE dsg_yetico_wyrob ADD nr_zlecenia varchar(150)

ALTER TABLE dsg_yetico_wyrob ADD jm numeric(18)

CREATE TABLE dsg_yetico_jm(
	ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int NOT NULL,	
);

insert into dsg_yetico_jm values ('KG', 'Kg.', null, null, 1);
insert into dsg_yetico_jm values ('SZT', 'Szt.', null, null, 1);

----------------------------------------------------
insert into dsg_yetico_jm values ('M3', 'm3', null, null, 1);
insert into dsg_yetico_jm values ('PACZKI', 'Paczki', null, null, 1);