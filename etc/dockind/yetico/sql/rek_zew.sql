alter table dsg_yetico_rek_zew add akceptacja_szefa_dzialu numeric(18,0);
alter table DS_ATTACHMENT add field_cn varchar(200);


alter table dsg_yetico_rek_zew add PRODUKCJA_GNIAZDO varchar(249);
alter table dsg_yetico_rek_zew add PRODUKCJA_ZMIANA varchar(249);

CREATE TABLE [dbo].[dsg_yetico_rek_zew]
(
	[document_id] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[nr_reklamacji] [varchar](50) NULL,
	[data_zgloszenia] [date] NULL,
	[dot_zakladu] [numeric](18, 0) NULL,
	[klient] [numeric](18, 0) NULL,
	[osoba_odpowiedzialna] [varchar](150) NULL,
	[wyrob] [numeric](18, 0) NULL,
	[osoba_poinformowana] [varchar](150) NULL,
	[straty_klienta] [varchar](4096) NULL,
	[nr_faktury] [varchar](150) NULL,
	[data_produkcji] [date] NULL,
	[data_dostawy] [date] NULL,
	[miejsce_dostawy] [varchar](249) NULL,
	[ilosc_rek_wyrobu] [numeric](18, 2) NULL,
	[ilosc_rek_wyrobu_na_fk] [numeric](18, 2) NULL,
	[prop_klienta] [varchar](4096) NULL,
	[inf_dod] [varchar](4096) NULL,
	[laczny_koszt] [numeric](18, 2) NULL
);

CREATE TABLE [dbo].[dsg_yetico_rek_zew_multiple_value](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;
alter table dsg_yetico_rek_zew
add [data_zakonczenia] [int] NULL

create table dsg_yetico_klient
(
	id numeric (18,0) IDENTITY(1,1) NOT NULL,
	nazwa varchar (249),
	miejscowosc varchar (249),
	kraj numeric (18,0),
	mail varchar (150),
	erp_id numeric (18,0)
);
ALTER TABLE dsg_yetico_klient ADD PRIMARY KEY (id);
create index dsg_yetico_klient_index on dsg_yetico_klient (id);
create index dsg_yetico_klient_nazwa on dsg_yetico_klient (nazwa);
create index dsg_yetico_klient_miejscowosc on dsg_yetico_klient (miejscowosc);
create index dsg_yetico_klient_kraj on dsg_yetico_klient (kraj);
create index dsg_yetico_klient_mail on dsg_yetico_klient (mail);

delete from yetico.dbo.dsg_yetico_klient;
insert into yetico.dbo.dsg_yetico_klient (nazwa, miejscowosc, kraj, mail, erp_id)
	select 
		RTRIM (k.nazwa),  
		RTRIM (k.miasto), 
		(select id from yetico.dbo.dsr_country where cn collate Polish_CI_AS = kraj.kraj_idn),
		RTRIM (k.email),
		d.dostawca_id
	from simple.dbo.kontrahent k join simple.dbo.dostawca d on k.kontrahent_id = d.kontrahent_id
	join simple.dbo.kraj on k.kraj_rejpod_id = kraj.kraj_id;

create table dsg_yetico_wyrob
(
	id numeric (18,0) IDENTITY(1,1) NOT NULL,
	nazwa varchar (249),
	typ varchar (150),
	wymiary varchar (50),
	nr_faktury varchar (150),
);
ALTER TABLE dsg_yetico_wyrob ADD PRIMARY KEY (id);
create index dsg_yetico_wyrob_index on dsg_yetico_wyrob (id);

create table dsg_yetico_niezgodnosc
(
	id numeric (18,0) IDENTITY(1,1) NOT NULL,
	rodzaj numeric (18,0),
	typ numeric (18,0),
	opis varchar (4096)
);
ALTER TABLE dsg_yetico_niezgodnosc ADD PRIMARY KEY (id);
create index dsg_yetico_niezgodnosc_index on dsg_yetico_niezgodnosc (id);

create table dsg_yetico_rodzaje_niezgodnosci
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL,
	
);
ALTER TABLE dsg_yetico_rodzaje_niezgodnosci ADD PRIMARY KEY (id);
create index dsg_yetico_rodzaje_niezgodnosci_index on dsg_yetico_rodzaje_niezgodnosci (id);

create table dsg_yetico_typ_niezgodnosci
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL,
);
ALTER TABLE dsg_yetico_typ_niezgodnosci ADD PRIMARY KEY (id);
create index dsg_yetico_typ_niezgodnosci_index on dsg_yetico_typ_niezgodnosci (id);

create table dsg_yetico_zadanie_status
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL,
);
ALTER TABLE dsg_yetico_zadanie_status ADD PRIMARY KEY (id);
create index dsg_yetico_zadanie_status_index on dsg_yetico_zadanie_status (id);
SET IDENTITY_INSERT dsg_yetico_zadanie_status on;
insert into dsg_yetico_zadanie_status (id, cn, title, available) values (10, 'OCZ_NA_PRZEKAZANIE', 'Ocz. na przekazanie', 1);
insert into dsg_yetico_zadanie_status (id, cn, title, available) values (15, 'OCZ_NA_REALIZACJE', 'Ocz. na realizację', 1);
insert into dsg_yetico_zadanie_status (id, cn, title, available) values (20, 'ZREALIZOWANE', 'Zrealizowane', 1);
SET IDENTITY_INSERT dsg_yetico_zadanie_status off;

create table dsg_yetico_zadanie
(
	document_id numeric (18,0) not null,
	status numeric (18,0),
	typ numeric (18,0),
	pracownik_odpowiedzialny numeric (18,0),
	pracownik_zlecajacy numeric (18,0),
	decyzja numeric (18,0),
	opis varchar (2048),
	opis_realizacji  varchar (2048),
	od date,
	do date
);
ALTER TABLE dsg_yetico_zadanie ADD PRIMARY KEY (document_id);
create index dsg_yetico_zadanie_index on dsg_yetico_zadanie (document_id);
alter table dsg_yetico_zadanie add reklamacja numeric (18,0)

CREATE TABLE [dbo].[dsg_yetico_koszty_reklamacji]
(
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[zaklad] [numeric](18, 0) NULL,
	[typ_kosztu] [numeric](18, 0) NULL,
	[opis] [varchar](2048) NULL,
	[koszt_jednostkowy] [numeric](18, 2) NULL,
	[ilosc] [numeric](18, 2) NULL,
	[koszt_calkowity] [numeric](18, 2) NULL
);
	
ALTER TABLE dsg_yetico_koszty_reklamacji ADD PRIMARY KEY (id);
create index dsg_yetico_koszty_reklamacji_index on dsg_yetico_koszty_reklamacji (id);