alter table dsg_yetico_niezgodnosc_wew add typ numeric(18,0);
alter table dsg_yetico_niezgodnosc_wew add AKCEPTACJA_SZEFA_DZIALU numeric(18,0);

CREATE TABLE [dbo].[dsg_yetico_niezgodnosc_wew]
(
	[document_id] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[status_niezgodnosci] [numeric](18, 0) NULL,
	[nr_reklamacji] [varchar](50) NULL,
	[rodzaj] numeric (18,0) NULL,
	autor numeric (18,0),
	data_wykrycia date,
	dot_zakladu numeric (18,0),
	miejsce_wykrycia varchar (1024),
	osoba_poinformowana varchar (249),
	nr_zdjecia varchar (150),
	informacje varchar (4096),
	uwagi_tekst varchar (4096),
	uwagi_link varchar (4096),
	wymagana_masa_wyrobu numeric (18,2),
	wymagana_gestosc_wyrobu numeric (18,2),
	masa_zmierzona numeric (18,2),
	gestosc_zmierzona numeric (18,2),
	odchylka_masy numeric (18,2),
	odchylka_gestosci numeric (18,2),
	wnioski_analizy varchar (4096),
	przyczyna_niezgodnosci varchar (4096),
	laczny_koszt numeric (18,2)
);

create table dsg_yetico_dzialania_korygujace
(
	id numeric (18,0) IDENTITY(1,1) NOT NULL,
	opis varchar (2048),
	osoba_odpowiedzialna numeric (18,0),
	od date,
	do date,
	status numeric (18,0),
);
