
CREATE TABLE dsg_liberty_order
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	nr_zamowienia varchar (50),
	opis varchar (2048),
	ilosc numeric(18,0),
	centrumid numeric(18,0),
	storageplace varchar (50),
	amount numeric (18,2),
	accountnumber varchar (50),
	kwota numeric(18,2),
	uzasadnienie varchar (2048),
	data_realizacji date
);

CREATE TABLE dsg_liberty_order_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);



CREATE INDEX liberty_order_index ON dsg_liberty_order (DOCUMENT_ID);

create  view dsg_liberty_filie_view as
select d.ID as ID, d.guid as CN, d.NAME as TITLE, '0' AS CENTRUM, null as refValue, '1' as available 
from DS_DIVISION as d
where CODE like 'F%-%';

create  view dsg_liberty_budzet_view as
select d.ID as ID, d.CODE as CN, d.NAME as TITLE, '0' AS CENTRUM, null as refValue, '1' as available 
from DS_DIVISION as d
where CODE like 'F%-%';