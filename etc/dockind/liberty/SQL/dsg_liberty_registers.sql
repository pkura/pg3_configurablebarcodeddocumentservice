CREATE TABLE dsg_liberty_registers(
	ID numeric(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available integer default 1
);

create sequence dsg_liberty_registers_id;

-- faktury towarowe
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVZM', 'I Rej. Tow. Handlowe Marato�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVZL', 'II Rej. Tow. Handlowe �opusza�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVZZ', 'VI Rej. Tow. Handlowe Pozna� Autostacja');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FKZ', 'VIII Rej. Tow. Handlowe - korekty');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'WNT', 'Rejestr naliczony WNT');
-- faktury sprzedazy
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FKD', 'VI Rej. Sprzeda�y korekt - dystrybucja');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSD', 'VI Rej. Sprzeda�y - dystrybucja');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSO', '0 Rej. Sprzeda�y - centrala');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSKO', '0 Rej. Sprzeda�y korekt - centrala');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FM', 'Rejestr mar�a');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FS1', 'I Rej. Sprzeda� Marato�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSK1', 'I Rej. Korekty Marato�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZ1', 'I Rej. Zaliczki Marato�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZK1', 'I Rej. Zaliczki Korekty Marato�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FS2', 'I Rej. Sprzeda� �opusza�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSK2', 'I Rej. Korekty �opusza�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZ2', 'I Rej. Zaliczki �opusza�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZK2', 'I Rej. Zaliczki Korekty �opusza�ska');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FS4', 'I Rej. Sprzeda� Pozna�');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FSK4', 'I Rej. Korekty Pozna�');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZ4', 'I Rej. Zaliczki Pozna�');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FZK4', 'I Rej. Zaliczki Korekty Pozna�');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'WDT', 'rejestr nale�ny WDT');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'WDTK', 'rejestr korekt nale�ny WDT');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'WNT', 'Rejestr nale�ny WNT');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'DEX', 'Eksport');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval, 'DEXK', 'Eksport - korekta');
-- faktury kosztowe                                       
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVG', 'Koszty got�wka');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FKG', 'Korekty Koszty got�wka');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK1', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK2', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK3', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK4', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK5', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK6', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK7', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK8', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVK9', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVKP', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVKL', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FVKG', 'Rejestr zakupu');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FKK', 'Rejestr zakupu kor.');
-- Srodki trwale                                         
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'FST', 'Rejestr zakupu �rodk�w trwa�ych');
insert into dsg_liberty_registers (id,cn, title) values (dsg_liberty_registers_id.nextval,'KST', 'Rejestr korekt zakupu �rodk�w trwa�ych');
