create table dsr_project
(
	ID numeric(18,0) NOT NULL,
	projectManager VARCHAR(50 char),
	projectName VARCHAR(150 char),
	nrIFPAN VARCHAR(50 char),
	startDate timestamp,
	finishDate timestamp
);

create sequence dsr_project_id;

create table dsr_project_entry
(
	ID numeric(18,0)  NOT NULL,
	projectId numeric(18,0),
	dateEntry timestamp,
	expenditureDate timestamp,
	costName VARCHAR(50),
	costKindId numeric(18,0),
	gross numeric(18,2),
	net numeric(18,2),
	vat int,
	entryType VARCHAR(50),	
	docNr numeric(18,0),
	demandNr numeric(18,0),
	description VARCHAR(255 char)
);
create sequence dsr_project_entry_id;

create table dsr_project_costkind
(
	ID numeric(18,0)NOT NULL,
	NAME VARCHAR(40 char),
	CATEGORY VARCHAR(40 char)
);

create sequence dsr_project_costkind_id;

insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'PROJECT_READ', 'Projekty - odczyt','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'PROJECT_MODIFY', 'Projekty - modyfikacja','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.nextval,'KANCELARIA', 'Kancelaria','group',1,0);

alter table dsr_project add projectType varchar(50);
alter table dsr_project_entry add settlementKind varchar(50);
alter table dsr_project_entry add currency varchar(5);
alter table dsr_project_entry add rate numeric(16,2);
alter table dsr_project add contractNumber varchar(100) ;
alter table dsr_project add remarks varchar(600) ;
alter table dsr_project add fullGross varchar(50) ;
alter table dsr_project add defaultCurrency varchar(10) ;
alter table dsr_project add zalacznik numeric(18,0);
alter table dsr_project_entry add accountingCostKind integer;
alter table dsr_project add percentValue float;
alter table dsr_project add mtime timestamp;
alter table dsr_project_entry add mtime timestamp;
ALTER TABLE dsr_project_entry ADD caseNumber numeric(18,0);
ALTER TABLE dsr_project_entry ADD entryNumber varchar(20);
alter table dsr_project_entry modify costkindId integer;
alter table dsr_project add  clearance char(1);
alter table dsr_project add  finalReport char(1);
alter table dsr_project add  applicationNumber varchar(50);
alter table dsr_project add  financingInstitutionKind integer;
alter table dsr_project_entry modify demandNr varchar(20);
alter table dsr_project_entry add costDate timestamp;
alter table dsr_project_entry modify gross numeric (18,2);
alter table dsr_project_entry modify net numeric (18,2);
alter table dsr_project_entry modify vat numeric (18,2);
alter table dsr_project_entry modify docNr varchar(50);
alter table dsr_project_entry add costID numeric(18,0);