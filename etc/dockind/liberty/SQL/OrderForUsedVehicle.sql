CREATE TABLE dsg_lib_orderfusedvehicle
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	centrumid numeric(18,0),
	storageplace varchar (50),
	amount numeric (18,2),
	accountnumber varchar (50),
	kwota numeric(18,2),
	marka varchar(249),
	rok_produkcji varchar (4),
	kontrahent numeric (18,0)
);

CREATE INDEX liberty_orderforusedvehicle_index ON dsg_lib_orderfusedvehicle (DOCUMENT_ID);

CREATE TABLE dsg_lib_orusedveh_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);



