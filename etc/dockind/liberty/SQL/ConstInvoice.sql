CREATE TABLE dsg_liberty_constinvoice
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	filia numeric (18,0),
	nr_faktury varchar (50),
	contractor numeric(18,0),
	numer_konta varchar (50),
	data_wplywu date,
	data_platnosci date,
	data_ksiegowania date,
	data_wystawienia date,
	netto numeric (18,2),
	brutto numeric (18,2),
	vat numeric (18,2),
	waluta numeric (18,0),
	umowa numeric (18,0),
	zamowienie numeric (18,0),
	rejestr numeric (18,0),
	mpk numeric (18,0)
);

CREATE INDEX liberty_constinvoice_index ON dsg_liberty_constinvoice (DOCUMENT_ID);

CREATE TABLE dsg_lib_coinv_multi_value (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
