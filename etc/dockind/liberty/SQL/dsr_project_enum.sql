create or replace view dsg_liberty_budget_view as
select 
	entry.ID as ID,
	division.guid as CN, 
	project.projectName || '-' || c.TITLE || '-' || kind.TITLE as title,
	null as refValue,
	entry.projectId as centrum,
	1 as available
from 
	dsr_project_entry entry join dsg_ifpan_acc_kind kind on entry.accountingCostKind = kind.ID
	join dsr_project project on entry.projectId = project.ID
	join dsg_ifpan_cost_category c on entry.costKindId = c.ID
	join DS_DIVISION division on division.NAME = project.projectName
where
	to_char(entry.dateEntry, 'Month')=to_char(sysdate, 'Month')

UNION
select 
	entry.ID as ID,
	project.projectName as CN, 
	project.projectName || '-' || c.TITLE || '-' || kind.TITLE as title,
	null as refValue,
	entry.projectId as centrum,
	1 as available
from 
	dsr_project_entry entry join dsg_ifpan_acc_kind kind on entry.accountingCostKind = kind.ID
	join dsr_project project on entry.projectId = project.ID
	join dsg_ifpan_cost_category c on entry.costKindId = c.ID
WHERE
  project.projectname LIKE 'MAR' and
	to_char(entry.dateEntry, 'Month')=to_char(sysdate, 'Month')

union
	select 
	entry.ID as ID,
	project.projectName as CN, 
	project.projectName || '-' || c.TITLE || '-' || kind.TITLE as title,
	null as refValue,
	entry.projectId as centrum,
	1 as available
from 
	dsr_project_entry entry join dsg_ifpan_acc_kind kind on entry.accountingCostKind = kind.ID
	join dsr_project project on entry.projectId = project.ID
	join dsg_ifpan_cost_category c on entry.costKindId = c.ID
WHERE
  project.projectname LIKE 'MLO' and
	to_char(entry.dateEntry, 'Month')=to_char(sysdate, 'Month');



-- budzety dystrybucji
	create or replace view dsg_liberty_dys_budget_view as
select 
	entry.ID as ID,
	division.guid as CN, 
	project.projectName || '-' || c.TITLE || '-' || kind.TITLE as title,
	null as refValue,
	entry.projectId as centrum,
	1 as available
from 
	dsr_project_entry entry join dsg_ifpan_acc_kind kind on entry.accountingCostKind = kind.ID
	join dsr_project project on entry.projectId = project.ID
	join dsg_ifpan_cost_category c on entry.costKindId = c.ID
	join DS_DIVISION division on division.NAME = project.projectName
where
	to_char(entry.dateEntry, 'Month')=to_char(sysdate, 'Month')
and
	division.CODE like 'FD-%';
	
-- budzety moto
	create or replace view dbo.dsg_liberty_moto_budget_view as
select 
	entry.ID as ID,
	division.guid as CN, 
	project.projectName || '-' || c.TITLE + '-' || kind.TITLE as title,
	null as refValue,
	entry.projectId as centrum,
	1 as available
from 
	dsr_project_entry entry join dsg_ifpan_acc_kind kind on entry.accountingCostKind = kind.ID
	join dsr_project project on entry.projectId = project.ID
	join dsg_ifpan_cost_category c on entry.costKindId = c.ID
	join DS_DIVISION division on division.NAME = project.projectName
where
	to_char(entry.dateEntry, 'Month')=to_char(sysdate, 'Month')
and
	division.CODE like 'FM-%';