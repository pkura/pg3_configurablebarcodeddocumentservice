CREATE TABLE dsg_liberty_accounts(
	ID numeric(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available integer default 1
);

create sequence dsg_liberty_accounts_id;