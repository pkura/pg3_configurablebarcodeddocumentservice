drop table dsg_lib_comm_mul_va;
CREATE TABLE dsg_lib_comm_mul_va (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_coinv_multi_value;
CREATE TABLE dsg_lib_coinv_multi_value (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_costinv_multi_value;
CREATE TABLE dsg_lib_costinv_multi_value (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_cinvfixed_multi;
CREATE TABLE dsg_lib_cinvfixed_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_costinvsales_multi;
CREATE TABLE dsg_lib_costinvsales_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_costinvtrans_multi;
CREATE TABLE dsg_lib_costinvtrans_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_liberty_order_multi;
CREATE TABLE dsg_liberty_order_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_orderforsales_multi;
CREATE TABLE dsg_lib_orderforsales_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_orusedveh_multi;
CREATE TABLE dsg_lib_orusedveh_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop table dsg_lib_salesinvoice_multi;
CREATE TABLE dsg_lib_salesinvoice_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

alter table DSG_CENTRUM_KOSZTOW_FAKTURY add analytics_id int;


