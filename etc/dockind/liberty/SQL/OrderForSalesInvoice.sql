
CREATE TABLE dsg_liberty_orderforsales
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	filia numeric (18,0),
	tresc varchar (2048),
	kontrahent numeric (18,0),
	adres varchar (2048),
	data_wystawienia date,
	termin_platnosci date,
	data_sprzedazy date,
	data_realizacji date
);

CREATE INDEX liberty_orderforsales_index ON dsg_liberty_orderforsales (DOCUMENT_ID);

CREATE TABLE dsg_lib_orderforsales_multi (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
