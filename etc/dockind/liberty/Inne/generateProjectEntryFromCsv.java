import java.io.*;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;


public class Main 
{
	public static void main(String[] args) 
	{
		try
		{
			String projextNamt = "ZGI";
			File existingDirFile = new File(
					projextNamt +".csv"); // assign a file
			FileReader fr = new FileReader(existingDirFile);
			BufferedReader br = new BufferedReader(fr); // make a Reader
			String s;
			StringBuffer strBuf = null;
			String kategoriaKosztów = null;
			String ksiegowaKategoriaKosztów = null;
			String selectProjectId = "(SELECT DISTINCT ID FROM dsr_project WHERE projectName = '"+ projextNamt +"'),";
			String selectkategoriaKosztow = null;
			String selectKsiegowaKategoriaKosztow = null;
			Calendar calendar = Calendar.getInstance();
			
			File textFile = new File(projextNamt + ".sql");
		      OutputStreamWriter writer = new OutputStreamWriter(
		      new FileOutputStream(textFile));
			
			while( (s = br.readLine()) != null)
			{
				
				int countValue = 0;
				String[] tokens = s.split(";");
				kategoriaKosztów = null;
				selectKsiegowaKategoriaKosztow = null;
				for(int i=0; i <tokens.length; ++i)
				{
					String temp = tokens[i];
					if(i==0 && !temp.equals(""))
					{
						kategoriaKosztów = temp.trim();
						selectkategoriaKosztow = "(SELECT DISTINCT ID FROM dsg_ifpan_cost_category WHERE TITLE = '" + kategoriaKosztów + "'),";
						break;
					}
					else if (i==1 && !temp.equals(""))
					{
						ksiegowaKategoriaKosztów = temp.trim();
						selectKsiegowaKategoriaKosztow = "(SELECT DISTINCT ID FROM dsg_ifpan_acc_kind WHERE TITLE = '" + ksiegowaKategoriaKosztów +"'),";
					}
					else if(!temp.equals(""))
					{
						++countValue;
						StringBuffer sb = new StringBuffer("INSERT INTO dsr_project_entry  (projectId ,costKindId,accountingCostKind,gross,dateEntry)   VALUES       (");  
						sb.append(selectProjectId);
						sb.append(selectkategoriaKosztow);
						sb.append(selectKsiegowaKategoriaKosztow);
						temp = temp.trim().replace("zł", "").replace(",", "").replace(" ","");
						sb.append(new BigDecimal(temp).divide(new BigDecimal(100.00), 2) + ",");   // kwota
						
						// ostatni dzien miesiaca
						calendar.set(2012, countValue-1, 1); // Months are 0 to 11
						sb.append("'" + countValue +"-"+calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)+"-2012'");
						sb.append(");");
						writer.write(sb + "\n");

						// tworzymy konkretne zapytanie
					}
					writer.flush();
				}
				if(countValue != 12 && selectKsiegowaKategoriaKosztow != null)
				{
					System.out.println("brakuje wpisow: " + kategoriaKosztów + " " +ksiegowaKategoriaKosztów  );
				}
			}
			br.close();
	    }
 		catch (Exception e)
 		{			    	
	      System.err.println("Error: " + e +  e.getMessage());
	    }
	}
}