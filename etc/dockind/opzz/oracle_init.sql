CREATE TABLE DS_OPZZ_CASES(
	DOCUMENT_ID numeric(19, 0) NOT NULL PRIMARY KEY,
	TYP int NOT NULL,
	STATUS int NOT NULL,
	TYTUL varchar2(1000) NOT NULL,
	OPIS varchar2(2000) NULL,
	TERMIN_WYKONANIA timestamp NULL,
	CHARAKTERYSTYKA varchar2(500) NULL,
	TERMIN timestamp NULL,
	ORGAN varchar2(1000) NULL,
	MIEJSCE varchar2(1000) NULL,
	NAZWA varchar2(1000) NULL,
	ADRES varchar2(1000) NULL,
	ORGANIZACJA varchar2(1000) NULL,
	WYKRESLENIE smallint null,
	OPIS_UPRAWNIEN varchar2(1000) NULL,
	PRZYZNANIE smallint null,
	EVENT_ID varchar2(1000) null,
    CZAS_TRWANIA varchar2(10),
    DATA_KONCOWA timestamp,
    EVENT_ID numeric(19,0)
);

CREATE TABLE DS_OPZZ_CASES_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL
);

CREATE SEQUENCE DS_OPZZ_CASES_MULTIPLE_ID;

CREATE TABLE DS_OPZZ_CASES_TYP(
	id numeric(5,0) NOT NULL primary key,
	cn varchar2(50) NULL,
	title varchar2(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

CREATE TABLE DS_OPZZ_CASES_STATUS(
	id numeric(5,0) NOT NULL primary key,
	cn varchar2(50) NULL,
	title varchar2(255) NULL,
	centrum int NULL,
	refValue varchar2(20) NULL,
	available int NULL
);

CREATE SEQUENCE DS_OPZZ_CASES_STATUS_ID;

CREATE SEQUENCE DS_OPZZ_CASES_TYP_ID;
----
CREATE TABLE DS_OPZZ_DOC(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	OPZZ_CASE_ID numeric(19, 0) NULL,
	TYTUL varchar2(1000) NOT NULL,
	OPIS varchar2(2000) NULL,
	OPINIA smallint null,
	ID numeric(19,0) NULL
);

CREATE TABLE DS_OPZZ_DOC_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar2(50) NOT NULL,
	FIELD_VAL varchar2(50) NOT NULL
);

CREATE SEQUENCE DS_OPZZ_DOC_MULTIPLE_ID start with 1 nocache;

INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (1,1,'Problem',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (2,2,'Posiedzenie OPZZ', 0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (3,3,'Nabycie członkostwa w OPZZ',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (4,4,'Zawieszenie lub wykrelenie członka',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (5,5,'Obsługa przyznawania odznak honorowych',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (6,6,'Obsługa wyborów członków struktur OPZZ',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (7,7,'Obsługa zobowiązań majątkowych',0,null,1);
INSERT INTO DS_OPZZ_CASES_TYP (id,cn, title ,centrum ,refValue,available) VALUES
           (8,8,'Opiniowanie aktów prawnych i dokumentów konsultacji unijnych',0,null,1);
           
           
           ----------
INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (1,1,'Zgłoszony',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (2,2,'Rozwiązywany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (3,3,'rozwiązany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (4,4,'nie rozwiązany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (5,5,'zaplanowane',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (6,6,'odbyte',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (7,7,'odwołane',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (8,8,'złożony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (9,9,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (10,10,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (11,11,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (12,12,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (13,13,'zgłoszony',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (14,14,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (15,15,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (16,16,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (17,17,'złożony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1);    
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (18,18,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (19,19,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (20,20,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (21,21,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (22,22,'zaplanowane',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (23,23,'odbyte',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (24,24,'odwołane',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (25,25,'złożony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (26,26,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (27,27,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (28,28,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (29,29,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (30,30,'w przygotowaniu',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (31,31,'opiniowanie',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (32,32,'konsultacje',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1);
     INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn, title ,centrum ,refValue ,available) VALUES
     (33,33,'zamknięty',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1);
     
     
CREATE VIEW V_DS_OPZZ_ASSIGNED
AS
SELECT ow.ID, ow.NAME AS CN, concat(ow.LASTNAME,concat( ' ',ow.FIRSTNAME)) AS TITLE, 0 AS CENTRUM, NULL AS REFVALUE, 0 AS AVAILABLE
FROM DS_USER ow 
	left join DS_USER_TO_DIVISION ud ON ow.id = ud.USER_ID
	left join DS_DIVISION div ON ud.DIVISION_ID = div.id
	WHERE div.GUID = 'Zwiazkowcy'
	
	
CREATE VIEW V_DS_OPZZ_OWNER
AS
SELECT ow.ID, ow.NAME AS CN, concat(ow.LASTNAME,concat( ' ',ow.FIRSTNAME)) AS TITLE, 0 AS CENTRUM, NULL AS REFVALUE, 0 AS AVAILABLE
FROM DS_USER ow 
	left join DS_USER_TO_DIVISION ud ON ow.id = ud.USER_ID
	left join DS_DIVISION div ON ud.DIVISION_ID = div.id
	WHERE div.GUID = 'Zwiazkowcy'
	
	
CREATE VIEW V_DS_OPZZ_DOC_CASES
AS
SELECT od.DOCUMENT_ID as ID, od.OPZZ_CASE_ID, od.TYTUL, oc.TYP, oc.STATUS, oc.TYTUL as TYTUL_SPRAWY
  FROM DS_OPZZ_DOC od
  left join DS_OPZZ_CASES oc ON od.OPZZ_CASE_ID = oc.DOCUMENT_ID 
  

CREATE TABLE DS_OPZZ_DOC_OPINION(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP smallint not null,
	LINK varchar2(2000) null,
	OPIS varchar2(2000) NULL,
	TERMIN timestamp NULL,
	atr1 smallint
);

CREATE TABLE DS_OPZZ_DOC_OP_MULT(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar2(50) NOT NULL,
	FIELD_VAL varchar2(50) NOT NULL
);

CREATE SEQUENCE DS_OPZZ_DOC_OP_MULT_ID start with 1 nocache;

CREATE TABLE DS_OPZZ_QUEST(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	OPZZ_CASE_ID numeric(19,0) NOT NULL,
	TYTUL varchar2(2000) NULL,
	OPIS varchar2(2000) NULL,
	ODPOWIEDZIALNY numeric(19,0) NOT NULL,
	TERMIN timestamp NULL
);

CREATE TABLE DS_OPZZ_QUEST_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar2(50) NOT NULL,
	FIELD_VAL varchar2(50) NOT NULL
)

CREATE SEQUENCE DS_OPZZ_QUEST_MULTIPLE_ID start;

CREATE VIEW V_DS_OPZZ_PROBLEM
AS
select cas.DOCUMENT_ID as id, cas.TYTUL as TYTUL, cas.STATUS  from DS_OPZZ_CASES cas
	where cas.TYP = 1
	
CREATE TABLE DS_OPZZ_ORG_USER(
	id numeric(5,0) NOT NULL PRIMARY KEY,
	username varchar2(200) not null,
	organization_key numeric (19,0) not null,
	organization_name varchar2(500) not null,
	hidden smallint
);

CREATE SEQUENCE DS_OPZZ_ORG_USER_ID start with 1 nocache;

CREATE VIEW V_DS_OPZZ_DELEGATE
AS
SELECT org.id as id, ur.FIRSTNAME, ur.LASTNAME, org.username, org.organization_name, org.hidden
  FROM DS_OPZZ_ORG_USER org
  LEFT JOIN DS_USER ur ON ur.name = org.username;  
  
CREATE VIEW V_DS_OPZZ_DOC
as
select DOCUMENT_ID as id, ds.* from DS_OPZZ_DOC ds where OPZZ_CASE_ID is NULL;

CREATE VIEW V_DS_OPZZ_QUEST
as
select * from DS_OPZZ_QUEST where OPZZ_CASE_ID is NULL;

ALTER TABLE DS_OPZZ_CASES modify TYTUL VARCHAR(1000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify OPIS VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify CHARAKTERYSTYKA VARCHAR2(1000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify ORGAN VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify OPIS_UPRAWNIEN VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify MIEJSCE VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify NAZWA VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify ADRES VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_CASES modify ORGANIZACJA VARCHAR2(2000 CHAR);

ALTER TABLE DS_OPZZ_DOC modify TYTUL VARCHAR2(1000 CHAR);
ALTER TABLE DS_OPZZ_DOC modify OPIS VARCHAR2(2000 CHAR);

ALTER TABLE DS_OPZZ_DOC_OPINION modify OPIS VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_DOC_OPINION modify LINK VARCHAR2(2000 CHAR);

ALTER TABLE DS_OPZZ_ORGANIZATION_USER modify organization_name VARCHAR2(2000 CHAR);

ALTER TABLE DS_OPZZ_QUEST modify OPIS VARCHAR2(2000 CHAR);
ALTER TABLE DS_OPZZ_QUEST modify TYTUL VARCHAR2(2000 CHAR);
