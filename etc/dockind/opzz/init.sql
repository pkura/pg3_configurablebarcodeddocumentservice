                      
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_OPZZ_CASES') 
DROP TABLE DS_OPZZ_CASES;

GO

CREATE TABLE DS_OPZZ_CASES(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP int NOT NULL,
	STATUS int NOT NULL,
	TYTUL varchar(100) NOT NULL,
	OPIS varchar(100) NULL,
	TERMIN_WYKONANIA datetime NULL,
	CHARAKTERYSTYKA varchar(500) NULL,
	TERMIN datetime NULL,
	ORGAN varchar(500) NULL,
	MIEJSCE varchar(500) NULL,
	NAZWA varchar(500) NULL,
	ADRES varchar(500) NULL,
	ORGANIZACJA varchar(500) NULL,
	WYKRESLENIE smallint null,
	OPIS_UPRAWNIEN varchar(800) NULL,
	PRZYZNANIE smallint null,
) ON PRIMARY

GO

CREATE TABLE DS_OPZZ_CASES_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY

GO

CREATE TABLE DS_OPZZ_CASES_STATUS(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
) ON PRIMARY;

CREATE TABLE DS_OPZZ_CASES_TYP(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
) ON PRIMARY;

truncate table ds_opzz_cases_typ
SET IDENTITY_INSERT DS_OPZZ_CASES_TYP ON

INSERT INTO DS_OPZZ_CASES_TYP
           (id,cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,1,'Problem',0,null,1),
           (2,2,'Posiedzenie OPZZ', 0,null,1),
           (3,3,'Nabycie cz�onkostwa w OPZZ',0,null,1),
           (4,4,'Zawieszenie lub wykre�lenie cz�onka',0,null,1),
           (5,5,'Obs�uga przyznawania odznak honorowych',0,null,1),
           (6,6,'Obs�uga wybor�w cz�onk�w struktur OPZZ',0,null,1),
           (7,7,'Obs�uga zobowi�za� maj�tkowych',0,null,1),
           (8,8,'Opiniowanie akt�w prawnych i dokument�w konsultacji unijnych',0,null,1);

set identity_insert ds_opzz_cases_typ off

truncate table ds_opzz_cases_Status
SET IDENTITY_INSERT DS_OPZZ_CASES_STATUS ON

INSERT INTO DS_OPZZ_CASES_STATUS
           (id,cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
     (1,1,'Zg�oszony',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1),
     (2,2,'Rozwi�zywany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1),
     (3,3,'rozwi�zany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1),
     (4,4,'nie rozwi�zany',0,(select id from DS_OPZZ_CASES_TYP where cn = 1),1),
     (5,5,'zaplanowane',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1),
     (6,6,'odbyte',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1),
     (7,7,'odwo�ane',0,(select id from DS_OPZZ_CASES_TYP where cn = 2),1),
     (8,8,'z�o�ony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1),     
     (9,9,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1),     
     (10,10,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1),     
     (11,11,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1),     
     (12,12,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 3),1),
     (13,13,'zg�oszony',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1),
     (14,14,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1),
     (15,15,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1),
     (16,16,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 4),1),
     (17,17,'z�o�ony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1),     
     (18,18,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1),     
     (19,19,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1),     
     (20,20,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1),     
     (21,21,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 5),1),
     (22,22,'zaplanowane',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1),
     (23,23,'odbyte',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1),
     (24,24,'odwo�ane',0,(select id from DS_OPZZ_CASES_TYP where cn = 6),1),
     (25,25,'z�o�ony wniosek',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1),     
     (26,26,' weryfikacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1),     
     (27,27,'akceptacja',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1),     
     (28,28,'zaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1),     
     (29,29,'niezaakceptowany',0,(select id from DS_OPZZ_CASES_TYP where cn = 7),1),
     (30,30,'w przygotowaniu',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1),
     (31,31,'opiniowanie',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1),
     (32,32,'konsultacje',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1),
     (33,33,'zamkni�ty',0,(select id from DS_OPZZ_CASES_TYP where cn = 8),1);
     
SET IDENTITY_INSERT DS_OPZZ_CASES_STATUS OFF

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_OPZZ_DOC') 
DROP TABLE DS_OPZZ_DOC;

GO

CREATE TABLE DS_OPZZ_DOC(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	OPZZ_CASE_ID numeric(19, 0)NULL,
	TYTUL varchar(100) NOT NULL,
	OPIS varchar(100) NULL,
	ID numeric(19,0) NULL
) ON PRIMARY

GO

CREATE TABLE DS_OPZZ_DOC_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY

go

-- dla przydzielonych i wlascicieli
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_ASSIGNED') 
DROP VIEW V_DS_OPZZ_ASSIGNED;

GO 

CREATE VIEW V_DS_OPZZ_ASSIGNED
AS
SELECT     ow.ID, ow.NAME AS CN, ow.LASTNAME + ' ' + ow.FIRSTNAME AS TITLE, 0 AS CENTRUM, NULL AS REFVALUE, 0 AS AVAILABLE
FROM DS_USER ow 
	left join DS_USER_TO_DIVISION ud ON ow.id = ud.USER_ID
	left join DS_DIVISION div ON ud.DIVISION_ID = div.id
	WHERE div.GUID = 'Zwiazkowcy'

GO

-- dla przydzielonych i wlascicieli
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_OWNER') 
DROP VIEW V_DS_OPZZ_OWNER;

GO 

CREATE VIEW V_DS_OPZZ_OWNER
AS
SELECT     ow.ID, ow.NAME AS CN, ow.LASTNAME + ' ' + ow.FIRSTNAME AS TITLE, 0 AS CENTRUM, NULL AS REFVALUE, 0 AS AVAILABLE
FROM         dbo.DS_USER ow 
	left join DS_USER_TO_DIVISION ud ON ow.id = ud.USER_ID
	left join DS_DIVISION div ON ud.DIVISION_ID = div.id
	WHERE div.GUID = 'Zwiazkowcy'

GO

-- widok dokument�w i powiazanych z nimi sprawami
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_DOC_CASES') 
DROP VIEW V_DS_OPZZ_DOC_CASES;

GO 
 
CREATE VIEW V_DS_OPZZ_DOC_CASES
AS
SELECT od.DOCUMENT_ID as ID, od.OPZZ_CASE_ID, od.TYTUL, oc.TYP, oc.STATUS, oc.TYTUL as TYTUL_SPRAWY
  FROM DS_OPZZ_DOC od
  left join DS_OPZZ_CASES oc ON od.OPZZ_CASE_ID = oc.DOCUMENT_ID 
  
GO

CREATE TABLE DS_OPZZ_DOC_OPINION(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP smallint not null,
	LINK varchar(300) null,
	OPIS varchar(500) NULL,
	TERMIN datetime NULL,
	atr1 smallint
) ON PRIMARY

GO

CREATE TABLE DS_OPZZ_DOC_OPINION_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY

go


IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_OPZZ_QUEST') 
DROP TABLE DS_OPZZ_QUEST;

GO
CREATE TABLE DS_OPZZ_QUEST(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	OPZZ_CASE_ID numeric(19,0) NOT NULL,
	TYTUL varchar(500) NULL,
	OPIS varchar(500) NULL,
	ODPOWIEDZIALNY numeric(19,0) NOT NULL,
	TERMIN datetime NULL
) ON PRIMARY

GO

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'DS_OPZZ_QUEST_MULTIPLE') 
DROP TABLE DS_OPZZ_QUEST_MULTIPLE;

GO

CREATE TABLE DS_OPZZ_QUEST_MULTIPLE(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY

go