CREATE TABLE DS_OPZZ_CASES(
	DOCUMENT_ID numeric(18, 0) NOT NULL primary key,
	TYP int NOT NULL,
	STATUS int NOT NULL,
	TYTUL varchar(100) NOT NULL,
	OPIS varchar(100) ,
	TERMIN_WYKONANIA timestamp,
	CHARAKTERYSTYKA varchar(500),
	TERMIN timestamp,
	ORGAN varchar(500),
	MIEJSCE varchar(500),
	NAZWA varchar(500),
	ADRES varchar(500),
	ORGANIZACJA varchar(500),
	WYKRESLENIE smallint,
	OPIS_UPRAWNIEN varchar(800),
	PRZYZNANIE smallint
);

CREATE TABLE DS_OPZZ_CASES_MULTIPLE(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) NOT NULL primary key
)

CREATE TABLE DS_OPZZ_CASES_STATUS(
	id numeric(5,0) NOT NULL primary key,
	cn varchar(50) ,
	title varchar(255),
	centrum int,
	refValue varchar(20),
	available int
)

CREATE TABLE DS_OPZZ_CASES_TYP(
	id numeric(5,0) NOT NULL primary key,
	cn varchar(50),
	title varchar(255) ,
	centrum int,
	refValue varchar(20),
	available int
)

CREATE TABLE DS_OPZZ_DOC(
	DOCUMENT_ID numeric(18, 0) NOT NULL priamry key,
	OPZZ_CASE_ID numeric(18, 0) NOT NULL,
	TYTUL varchar(100) NOT NULL,
	OPIS varchar(100)
)

CREATE TABLE DS_OPZZ_DOC_MULTIPLE(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0)NOT NULL primary key
)

CREATE TABLE DS_OPZZ_DOC_MULTIPLE(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0)NOT NULL primary key
)