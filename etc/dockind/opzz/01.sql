ALTER TABLE DS_OPZZ_CASES add EVENT_ID varchar (200) null;
ALTER TABLE DS_OPZZ_CASES add CZAS_TRWANIA varchar(10);
ALTER TABLE DS_OPZZ_CASES add DATA_KONCOWA datetime;
ALTER TABLE DS_OPZZ_CASES add EVENT_ID numeric(19,0);

-- widok dokument�w i powiazanych z nimi sprawami
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_PROBLEM') 
DROP VIEW V_DS_OPZZ_PROBLEM;

GO 

CREATE VIEW V_DS_OPZZ_PROBLEM
AS
select cas.DOCUMENT_ID as id, cas.TYTUL as TYTUL, cas.STATUS  from DS_OPZZ_CASES cas
	where cas.TYP = 1
GO


--tabela z organizacjami użytkowników
CREATE TABLE DS_OPZZ_ORG_USER(
	id numeric(5,0) IDENTITY(1,1) NOT NULL,
	username varchar(200) not null,
	organization_key numeric (19,0) not null,
	organization_name varchar (500) not null,
	hidden smallint null
) ON PRIMARY

GO

-- slownik delegat�w
IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_DELEGATE') 
DROP VIEW V_DS_OPZZ_DELEGATE;

GO 

CREATE VIEW V_DS_OPZZ_DELEGATE
AS
SELECT org.id as id, ur.FIRSTNAME, ur.LASTNAME, org.username, org.organization_name, org.hidden
  FROM DS_OPZZ_ORGANIZATION_USER org
  LEFT JOIN DS_USER ur ON ur.name = org.username;  
go

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_DOC') 
DROP VIEW V_DS_OPZZ_DOC;

go

CREATE VIEW V_DS_OPZZ_DOC
as
select DOCUMENT_ID as id, * from DS_OPZZ_DOC where OPZZ_CASE_ID is NULL

alter table DS_OPZZ_DOC add OPINIA smallint null;

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE  TABLE_NAME = 'V_DS_OPZZ_QUEST') 
DROP VIEW V_DS_OPZZ_QUEST;

go

CREATE VIEW V_DS_OPZZ_QUEST
as
select * from DS_OPZZ_QUEST where OPZZ_CASE_ID is NULL