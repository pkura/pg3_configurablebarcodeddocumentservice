CREATE TABLE [DSG_EUROPOL_INV](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	rodzaj int NULL,
	dzial int NULL,
	nr_faktury varchar(20) NULL,
	typ int NULL,	
	dostawca int NULL,
	numer_rachunku_bankowego varchar(50) NULL,
	data_wystawienia datetime NULL,
	data_platnosci datetime NULL,
	data_zaksieg datetime NULL,
	kwota_netto float NULL,
	kwota_brutto float NULL,
	vat float NULL,
	waluta int NULL,
	nr_umowy varchar(20) NULL
);

CREATE TABLE [DF_DICINVOICE](
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	numerKontrahenta varchar(255) NOT NULL,
	numerKontaBankowego varchar(255) NULL,
	imie varchar(255) NULL,
	nazwisko varchar(255) NULL,
	name varchar(255) NULL,
	oldname varchar(255) NULL,
	nip varchar(30) NULL,
	regon varchar(30) NULL,
	ulica varchar(255) NULL,
	kod varchar(10) NULL,
	miejscowosc varchar(255) NULL,
	email varchar(255) NULL,
	faks varchar(255) NULL,
	telefon varchar(255) NULL,
	kraj varchar(10) NULL
);

alter table DSG_NORMAL_DOCKIND add integer rodzaj;