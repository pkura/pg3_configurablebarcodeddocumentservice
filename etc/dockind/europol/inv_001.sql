CREATE TABLE [dsg_waluta](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
);

insert into dsg_waluta (cn,title)values('PLN','PLN - Z�oty polski');
insert into dsg_waluta (cn,title)values('EUR','EUR - EU euro');
insert into dsg_waluta (cn,title)values('CZK','CZK - Korona czeska');
alter table dsg_waluta add available integer
update dsg_waluta set available = 1;