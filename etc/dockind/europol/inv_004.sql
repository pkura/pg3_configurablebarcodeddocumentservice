update DSO_IN_DOCUMENT set original = 1;
alter table dso_person alter column title varchar(255);
alter table dso_person alter column organization varchar(255);
alter table dso_person alter column street varchar(255);
alter table dso_person alter column location varchar(255);
alter table dso_person alter column country varchar(10);
alter table dso_person alter column firstname varchar(255);
alter table dso_person alter column lastname varchar(255);
alter table dso_person alter column title varchar(255);

insert into DSO_PERSON(DISCRIMINATOR,DICTIONARYTYPE,DICTIONARYGUID,ANONYMOUS,TITLE,FIRSTNAME,LASTNAME,ORGANIZATION,ORGANIZATIONDIVISION,STREET,ZIP,LOCATION,NIP,REGON,COUNTRY) 
select 'PERSON','sender','rootdivision',0,numerKontrahenta,imie,nazwisko,name,'',ulica,kod,miejscowosc,nip,regon,kraj from DF_DICINVOICE;

update DSO_PERSON set COUNTRY = 'PL' where COUNTry is null or COUNTRY = '';
alter table DSG_EUROPOL_INV add kwota numeric(16,2);


insert into ds_box_line (line,name) values ('line_2','2');
insert into ds_box_line (line,name) values ('line_4','4');
insert into ds_box_line (line,name) values ('line_5','5');
insert into ds_box_line (line,name) values ('line_7','7');