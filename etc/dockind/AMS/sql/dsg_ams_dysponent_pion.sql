--create
alter
view [dbo].[dsg_ams_dysponent_pion]
--NIE ZMIENIAC refValue, ATRYBUT DO WYCIAGANIA KIEROWNIKOW PIONU
with schemabinding
as
select distinct
u.id as id, 
u.name as cn,
u.lastname + ' ' + u.firstname + 
CASE 
	WHEN d2.code like '0835%' THEN ' (Pion Dziekana Wydzia�u In�ynieryjno-Ekonomicznego Transportu)'
	WHEN d2.code like '073%' THEN ' (Pion Dziekana Wydzia�u Mechanicznego)'
	WHEN d2.code like '0625%' THEN ' (Pion Dziekana Wydzia�u Nawigacyjnego)'
	WHEN d2.code like '052%' THEN ' (Pion Kanclerza)'
	WHEN d2.code like '0415%' THEN ' (Pion Prorektora ds. Morskich)'
	WHEN d2.code like '031%' THEN ' (Pion Prorektora ds. Nauczania)'
	WHEN d2.code like '0205%' THEN ' (Pion Prorektora ds. Nauki)'
	WHEN d2.code like '01%' THEN ' (Pion Rektora)'
	WHEN d2.code like 'test%' THEN ' (TEST)'
	ELSE ''
END
as title,
CASE 
	WHEN d2.code like '0835%' THEN '0835%'
	WHEN d2.code like '073%' THEN '073%'
	WHEN d2.code like '0625%' THEN '0625%'
	WHEN d2.code like '052%' THEN '052%'
	WHEN d2.code like '0415%' THEN '0415%'
	WHEN d2.code like '031%' THEN '031%'
	WHEN d2.code like '0205%' THEN '0205%'
	WHEN d2.code like '01%' THEN '01%'
	WHEN d2.code like 'test%' THEN 'test%'
	ELSE ''
END
 as refValue,
d2.code as centrum,
1-u.DELETED as available
from dbo.ds_division d2
join dbo.ds_user_to_division ud on d2.id=ud.division_id
join dbo.ds_user u on ud.user_id=u.id
join dbo.ds_profile_to_user p2u on p2u.user_id=u.id and p2u.userkey like d2.guid
join dbo.ds_profile p on p.id=p2u.profile_id
where p.name='Dysponent'
union all
select
u.id as id, 
u.name as cn,
u.name as title,
null as refValue,
null as centrum,
0 as available
from dbo.ds_user u
where id not in (select distinct u.id
from dbo.ds_division d2
join dbo.ds_user_to_division ud on d2.id=ud.division_id
join dbo.ds_user u on ud.user_id=u.id
join dbo.ds_profile_to_user p2u on p2u.user_id=u.id and p2u.userkey like d2.guid
join dbo.ds_profile p on p.id=p2u.profile_id
where p.name='Dysponent')