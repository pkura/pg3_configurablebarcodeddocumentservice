CREATE TABLE dsg_ams_production_order (
	id numeric(18, 0) identity(1,1) NOT NULL,
	id_from_webservice numeric(18, 2),
	idm varchar(50),
	nazwa varchar(150)
)

create view dsg_ams_view_production_order as
	select
		id,
		idm as cn,
		nazwa as title,
		NULL as centrum,
		NULL as refValue,
		1 as available
	from dsg_ams_production_order;