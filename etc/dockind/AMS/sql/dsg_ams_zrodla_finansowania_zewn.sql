CREATE TABLE [dbo].[dsg_ams_zrodla_finansowania_zewn](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
    CONSTRAINT [PK__dsg_ams_zrodla_finansowania_zewn]
    PRIMARY KEY CLUSTERED ([id] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

GO
SET IDENTITY_INSERT [dbo].[dsg_ams_zrodla_finansowania_zewn] ON
    INSERT [dbo].[dsg_ams_zrodla_finansowania_zewn] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'ZZF', N'Zewn�trzne �r�d�a finansowania', NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[dsg_ams_zrodla_finansowania_zewn] OFF




--dla testow
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF1', N'�r�d�o finansowania zewn 1', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF2', N'�r�d�o finansowania zewn 2', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF3', N'�r�d�o finansowania zewn 3', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF4', N'�r�d�o finansowania zewn 4', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF5', N'�r�d�o finansowania zewn 5', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF6', N'�r�d�o finansowania zewn 6', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania_zewn ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF7', N'�r�d�o finansowania zewn 7', NULL, NULL, 1)