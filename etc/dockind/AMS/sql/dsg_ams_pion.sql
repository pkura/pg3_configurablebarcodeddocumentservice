create view dsg_ams_pion as
select d.id, max(u.name) as cn, d.name as title,  null as refValue, null as centrum, 1-d.hidden as available 
from ds_division d 
join ds_user_to_division ud on d.id=ud.DIVISION_ID
join ds_user u on ud.USER_ID=u.id
where d.name like '%pion%' and u.DELETED=0
group by d.id,d.guid,d.name,d.hidde