CREATE TABLE dsg_ams_rachunki (
	id numeric(18, 0) identity(1,1) NOT NULL,
	nr_rachunku varchar(50),
	kwota numeric(18, 2)
);