create view dsd_ams_view_asset_responsible_person as 
select 
id,cn,title, 
ISNULL((select top 1 name from ds_user where LTRIM(RTRIM(title))=LTRIM(RTRIM(firstname)) + ' ' + LTRIM(RTRIM(lastname)) and deleted=0),'admin') as refvalue,
centrum,
ISNULL((select top 1 1-deleted name from ds_user where LTRIM(RTRIM(title))=LTRIM(RTRIM(firstname)) + ' ' + LTRIM(RTRIM(lastname)) and deleted=0), 0) as available
from dsd_ams_asset_responsible_person;