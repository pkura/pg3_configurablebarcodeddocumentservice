CREATE TABLE dsg_ams_likwidacja_wnip (
	document_id numeric(18,0) NOT NULL,
	status numeric(18, 0),
	uwagi varchar(1024),
	data_obowiazywania date,
	oddzial numeric(18, 0),
    PRIMARY KEY CLUSTERED ( document_id ASC )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE dsg_ams_likwidacja_wnip_multiple_value (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100)
);
CREATE INDEX dsg_ams_likwidacja_wnip_multiple_valueDI on dsg_ams_likwidacja_wnip_multiple_value (DOCUMENT_ID);
alter table dsg_ams_likwidacja_wnip
add identyfikator_likwidacji int;