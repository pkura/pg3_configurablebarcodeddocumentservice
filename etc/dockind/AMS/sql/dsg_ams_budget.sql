CREATE TABLE dsg_ams_budget (
	id numeric(18, 0) identity(1,1) NOT NULL,
	bd_budzet_id numeric(18, 2),
	bd_budzet_idm varchar(50),
	bd_budzet_rodzaj_id numeric(18, 2),
	bd_budzet_rodzaj_zadania_id numeric(18, 2),
	bd_budzet_rodzaj_zrodla_id numeric(18, 2),
	bd_grupa_rodzaj_id numeric(18, 2),
	bd_rodzaj varchar(50),
	bd_str_budzet_idn varchar(50),
	bd_str_budzet_ids varchar(50),
	bd_szablon_poz_id numeric(18, 2),
	bd_zadanie_id numeric(18, 2),
	bd_zadanie_idn varchar(50),
	bud_w_okr_nazwa varchar(100),
	budzet_id numeric(18, 2),
	budzet_idm varchar(50),
	budzet_ids varchar(50),
	budzet_nazwa varchar(100),
	czy_aktywna numeric(18, 2),
	dok_bd_str_budzet_idn varchar(50),
	dok_kom_nazwa varchar(100),
	dok_poz_limit numeric(18, 2),
	dok_poz_nazwa varchar(100),
	dok_poz_nrpoz numeric(18, 0),
	dok_poz_p_koszt numeric(18, 2),
	dok_poz_r_koszt numeric(18, 2),
	dok_poz_rez_koszt numeric(18, 2),
	okrrozl_id numeric(18, 2),
	pozycja_podrzedna_id numeric(18, 2),
	str_bud_nazwa varchar(100),
	wspolczynnik_pozycji numeric(18, 0),
	wytwor_id numeric(18, 2),
	zadanie_nazwa varchar(100),
	zadanie_p_koszt numeric(18, 2),
	zadanie_r_koszt numeric(18, 2),
	zadanie_rez_koszt numeric(18, 2),
	zrodlo_id numeric(18, 2),
	zrodlo_idn varchar(50),
	zrodlo_nazwa varchar(100),
	zrodlo_p_koszt numeric(18, 2),
	zrodlo_r_koszt numeric(18, 2),
	zrodlo_rez_koszt numeric(18, 2),
	available bit
)

	CREATE VIEW dsg_ams_view_budget_okres AS
	SELECT * FROM (
			SELECT 
				convert(numeric(18, 0), budzet_id) AS id,
				bd_budzet_idm AS cn,
				bud_w_okr_nazwa AS title,
				ROW_NUMBER() OVER (PARTITION BY bud_w_okr_nazwa ORDER BY bud_w_okr_nazwa) AS centrum,
				NULL AS refValue,
				available
			FROM dsg_ams_budget
	)a WHERE a.centrum = 1;
	
	
	CREATE VIEW dsg_ams_view_budget AS
	SELECT * FROM (
			SELECT 
				convert(numeric(18, 0), bd_budzet_id) AS id,
				bd_budzet_idm AS cn,
				budzet_nazwa AS title,
				ROW_NUMBER() OVER (PARTITION BY bd_budzet_id ORDER BY bd_budzet_id) AS centrum,
				NULL AS refValue,
				available
			FROM dsg_ams_budget
	)a WHERE a.centrum = 1;
	
	
	CREATE VIEW dsg_ams_view_budget_position AS
	SELECT
		id,
		convert(varchar(50), id) AS cn,
		dok_poz_nazwa AS title,
		NULL AS centrum,
		convert(varchar(20), convert(numeric(18, 0), bd_budzet_id)) AS refValue,
		available
	FROM dsg_ams_budget;
	
	
CREATE VIEW dsg_ams_view_budget_cell AS
	SELECT * FROM (
		SELECT
			id,
			dok_bd_str_budzet_idn AS cn,
			dok_kom_nazwa AS title,
			ROW_NUMBER() OVER (PARTITION BY dok_bd_str_budzet_idn ORDER BY dok_bd_str_budzet_idn) AS centrum,
			convert(numeric(18, 0),bd_budzet_id) AS refValue,
			available
		FROM dsg_ams_budget
	)a WHERE a.centrum = 1;
	
	
drop table dsg_ams_budget;

	CREATE TABLE dsg_ams_budget (
	id numeric(18, 0) identity(1,1) NOT NULL,
	bd_budzet_id numeric(18, 2),
	bd_budzet_idm varchar(50),
	bd_budzet_rodzaj_id numeric(18, 2),
	bd_budzet_rodzaj_zadania_id numeric(18, 2),
	bd_budzet_rodzaj_zrodla_id numeric(18, 2),
	bd_grupa_rodzaj_id numeric(18, 2),
	bd_rodzaj varchar(50),
	bd_str_budzet_idn varchar(50),
	bd_str_budzet_ids varchar(50),
	bd_szablon_poz_id numeric(18, 2),
	bd_zadanie_id numeric(18, 2),
	bd_zadanie_idn varchar(50),
	bud_w_okr_nazwa varchar(100),
	budzet_id numeric(18, 2),
	budzet_idm varchar(50),
	budzet_ids varchar(50),
	budzet_nazwa varchar(100),
	czy_aktywna numeric(18, 2),
	dok_bd_str_budzet_idn varchar(50),
	dok_kom_nazwa varchar(100),
	dok_poz_limit numeric(18, 2),
	dok_poz_nazwa varchar(100),
	dok_poz_nrpoz numeric(18, 0),
	dok_poz_p_ilosc numeric(18, 2),
	dok_poz_p_kosz numeric(18, 2),
	dok_poz_r_ilosc numeric(18, 2),
	dok_poz_r_koszt numeric(18, 2),
	dok_poz_rez_ilosc numeric(18, 2),
	dok_poz_rez_koszt numeric(18, 2),
	okrrozl_id numeric(18, 2),
	pozycja_podrzedna_id numeric(18, 2),
	str_bud_nazwa varchar(100),
	wspolczynnik_pozycji numeric(18, 0),
	wytwor_id numeric(18, 2),
	zadanie_nazwa varchar(100),
	zadanie_p_ilosc numeric(18, 2),
	zadanie_p_koszt numeric(18, 2),
	zadanie_r_ilosc numeric(18, 2),
	zadanie_r_koszt numeric(18, 2),
	zadanie_rez_ilosc numeric(18, 2),
	zadanie_rez_koszt numeric(18, 2),
	zrodlo_id numeric(18, 2),
	zrodlo_idn varchar(50),
	zrodlo_nazwa varchar(100),
	zrodlo_p_koszt numeric(18, 2),
	zrodlo_r_koszt numeric(18, 2),
	zrodlo_rez_koszt numeric(18, 2),
	available bit
)

DROP VIEW dsg_ams_view_budget;

	CREATE VIEW dsg_ams_view_budget AS
	SELECT * FROM (
			SELECT 
				convert(numeric(18, 0), bd_budzet_id) AS id,
				bd_budzet_idm AS cn,
				budzet_nazwa AS title,
				ROW_NUMBER() OVER (PARTITION BY bd_budzet_id ORDER BY bd_budzet_id) AS centrum,
				CASE
					WHEN bd_rodzaj = 'BK'
						THEN 5
					WHEN bd_rodzaj = 'PZB'
						THEN 10
				END AS refValue,
				available
			FROM dsg_ams_budget
	)a WHERE a.centrum = 1;
	
ALTER TABLE dsg_ams_budget ADD wytwor_idm varchar(50);