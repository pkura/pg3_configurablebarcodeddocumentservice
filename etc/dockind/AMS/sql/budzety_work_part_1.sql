SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_ams_services_purchase_plan](
	[id] [numeric](10, 0) NOT NULL,
	[idm] [varchar](20) NOT NULL,
	[budzet_id] [numeric](10, 0) NULL,
	[budzet_idm] [varchar](20) NULL,
	[nazwa] [varchar](128) NULL,
	[bd_str_budzet_id] [numeric](10, 0) NULL,
	[bd_str_budzet_idn] [varchar](15) NULL,
	[czy_agregat] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_ams_services_unit_budget_ko](
	[id] [numeric](10, 0) NOT NULL,
	[idm] [varchar](20) NOT NULL,
	[budzet_id] [numeric](10, 0) NULL,
	[budzet_idm] [varchar](20) NULL,
	[nazwa] [varchar](128) NULL,
	[bd_str_budzet_id] [numeric](10, 0) NULL,
	[bd_str_budzet_idn] [varchar](15) NULL,
	[czy_agregat] [int] NOT NULL
) ON [PRIMARY]

GO

CREATE INDEX dsg_ams_services_unit_budget_ko_I1 on dsg_ams_services_unit_budget_ko(bd_str_budzet_idn);
CREATE INDEX dsg_ams_services_purchase_plan_I1 on dsg_ams_services_purchase_plan(bd_str_budzet_idn);

SET ANSI_PADDING OFF
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(302 AS Numeric(10, 0)), N'PZB/2014/0001', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SUMA AKADEMIA MORSKA W SZCZECINIE', CAST(1 AS Numeric(10, 0)), N'AMS', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(303 AS Numeric(10, 0)), N'PZB/2014/0002', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION REKTORA', CAST(2 AS Numeric(10, 0)), N'R SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(304 AS Numeric(10, 0)), N'PZB/2014/0003', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RZECZNIK PRASOWY', CAST(4 AS Numeric(10, 0)), N'RPR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(305 AS Numeric(10, 0)), N'PZB/2014/0004', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� ORGANIZACYJNO-PRAWNY', CAST(5 AS Numeric(10, 0)), N'ROP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(306 AS Numeric(10, 0)), N'PZB/2014/0005', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� PROMOCJI', CAST(6 AS Numeric(10, 0)), N'RP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(307 AS Numeric(10, 0)), N'PZB/2014/0006', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� KADR', CAST(7 AS Numeric(10, 0)), N'RK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(308 AS Numeric(10, 0)), N'PZB/2014/0007', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SAMODZIELNE STANOWISKO DS. AUDYTU WEWN�T', CAST(8 AS Numeric(10, 0)), N'RA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(309 AS Numeric(10, 0)), N'PZB/2014/0008', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SUMA PION PROREKTORA DS. NAUKI', CAST(12 AS Numeric(10, 0)), N'RB', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(310 AS Numeric(10, 0)), N'PZB/2014/0009', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� NAUKI', CAST(13 AS Numeric(10, 0)), N'BB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(311 AS Numeric(10, 0)), N'PZB/2014/0010', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'UCZELNIANE CENTRUM INFORMATYCZNE', CAST(14 AS Numeric(10, 0)), N'BI', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(312 AS Numeric(10, 0)), N'PZB/2014/0011', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� ZAM�WIE� PUBLICZNYCH', CAST(15 AS Numeric(10, 0)), N'BZB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(313 AS Numeric(10, 0)), N'PZB/2014/0012', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA APARATURY', CAST(16 AS Numeric(10, 0)), N'BA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(314 AS Numeric(10, 0)), N'PZB/2014/0013', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� WYDAWNICTW', CAST(17 AS Numeric(10, 0)), N'BW', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(315 AS Numeric(10, 0)), N'PZB/2014/0014', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM TRANSFERU TECHNOLOGII MORSKICH', CAST(18 AS Numeric(10, 0)), N'BC', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(316 AS Numeric(10, 0)), N'PZB/2014/0015', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WYDAWNICTWO NAUKOWE', CAST(19 AS Numeric(10, 0)), N'WN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(317 AS Numeric(10, 0)), N'PZB/2014/0016', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BIBLIOTEKA G��WNA', CAST(20 AS Numeric(10, 0)), N'BG SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(318 AS Numeric(10, 0)), N'PZB/2014/0017', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA INFORMACJI NAUKOWEJ', CAST(21 AS Numeric(10, 0)), N'SIG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(319 AS Numeric(10, 0)), N'PZB/2014/0018', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA GROMADZENIA I OPRACOWANIA ZBIOR�W', CAST(22 AS Numeric(10, 0)), N'SGiOZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(320 AS Numeric(10, 0)), N'PZB/2014/0019', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA UDOST�PNIANIA ZBIOR�W', CAST(23 AS Numeric(10, 0)), N'SUZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(321 AS Numeric(10, 0)), N'PZB/2014/0020', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA CZYTEL�', CAST(24 AS Numeric(10, 0)), N'SC', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(322 AS Numeric(10, 0)), N'PZB/2014/0021', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ARCHIWUM ZAK�ADOWE', CAST(25 AS Numeric(10, 0)), N'AZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(323 AS Numeric(10, 0)), N'PZB/2014/0022', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'FUNDUSZ WSPӣPRACY Z ZAGRANIC�', CAST(26 AS Numeric(10, 0)), N'FWZZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(324 AS Numeric(10, 0)), N'PZB/2014/0023', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION PROREKTORA DS. NAUCZANIA', CAST(27 AS Numeric(10, 0)), N'RN SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(325 AS Numeric(10, 0)), N'PZB/2014/0024', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� NAUCZANIA I CERTYFIKACJI', CAST(28 AS Numeric(10, 0)), N'NN SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(326 AS Numeric(10, 0)), N'PZB/2014/0025', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BIURO PERSONALIZACJI DANYCH STUDENTA', CAST(29 AS Numeric(10, 0)), N'NB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(327 AS Numeric(10, 0)), N'PZB/2014/0026', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZARZ�DZANIE JAKO�CI� ISO', CAST(30 AS Numeric(10, 0)), N'ZJISO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(328 AS Numeric(10, 0)), N'PZB/2014/0027', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'AUDITY', CAST(31 AS Numeric(10, 0)), N'AUD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(329 AS Numeric(10, 0)), N'PZB/2014/0028', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� DS.. OBCOKRAJOWC�W I WYMIANY MI�DZ', CAST(32 AS Numeric(10, 0)), N'NM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(330 AS Numeric(10, 0)), N'PZB/2014/0029', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM KSZTA�CENIA USTAWICZNEGO', CAST(33 AS Numeric(10, 0)), N'CKU', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(332 AS Numeric(10, 0)), N'PZB/2014/0031', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDIUM WYCHOWANIA FIZYCZNEGO I SPORTU', CAST(35 AS Numeric(10, 0)), N'SWFiS SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(333 AS Numeric(10, 0)), N'PZB/2014/0032', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RATOWNICTWO', CAST(36 AS Numeric(10, 0)), N'RAT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(334 AS Numeric(10, 0)), N'PZB/2014/0033', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA PI�KI KOSZYKOWEJ', CAST(37 AS Numeric(10, 0)), N'SPK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(335 AS Numeric(10, 0)), N'PZB/2014/0034', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA P�KI SIATKOWEJ', CAST(38 AS Numeric(10, 0)), N'SPS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(336 AS Numeric(10, 0)), N'PZB/2014/0035', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA PI�KU NO�NEJ', CAST(39 AS Numeric(10, 0)), N'SPN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(337 AS Numeric(10, 0)), N'PZB/2014/0036', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA WIO�LARSKA', CAST(40 AS Numeric(10, 0)), N'SW', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(338 AS Numeric(10, 0)), N'PZB/2014/0037', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA P�YWACKA', CAST(41 AS Numeric(10, 0)), N'SP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(339 AS Numeric(10, 0)), N'PZB/2014/0038', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA KULTURYSTYCZNA', CAST(42 AS Numeric(10, 0)), N'SKULT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(340 AS Numeric(10, 0)), N'PZB/2014/0039', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA KARATE', CAST(43 AS Numeric(10, 0)), N'SK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(341 AS Numeric(10, 0)), N'PZB/2014/0040', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SPORTY MASOWE', CAST(44 AS Numeric(10, 0)), N'SM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(342 AS Numeric(10, 0)), N'PZB/2014/0041', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA �EGLARSKA', CAST(45 AS Numeric(10, 0)), N'S�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(343 AS Numeric(10, 0)), N'PZB/2014/0042', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA NARCIARSKA', CAST(46 AS Numeric(10, 0)), N'SN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(344 AS Numeric(10, 0)), N'PZB/2014/0043', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA SZALUPINGU', CAST(47 AS Numeric(10, 0)), N'SSZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(345 AS Numeric(10, 0)), N'PZB/2014/0044', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'UPOMINKI DLA NAJLEPSZYCH SPORTOWC�W', CAST(48 AS Numeric(10, 0)), N'UDNS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(346 AS Numeric(10, 0)), N'PZB/2014/0045', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INNE SEKCJE', CAST(49 AS Numeric(10, 0)), N'IS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(347 AS Numeric(10, 0)), N'PZB/2014/0046', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'FUNDUSZ SOCJALNO-KULTURALNY STUDENT�W', CAST(50 AS Numeric(10, 0)), N'FSKS SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(348 AS Numeric(10, 0)), N'PZB/2014/0047', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� SPRAW M�ODZIE�Y', CAST(51 AS Numeric(10, 0)), N'DSM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(349 AS Numeric(10, 0)), N'PZB/2014/0048', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CH�R AKADEMII MORSKIEJ', CAST(52 AS Numeric(10, 0)), N'CAM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(350 AS Numeric(10, 0)), N'PZB/2014/0049', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KLUB POD MASZTAMI', CAST(53 AS Numeric(10, 0)), N'KPM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(351 AS Numeric(10, 0)), N'PZB/2014/0050', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDENCKIE KO�A NAUKOWE', CAST(54 AS Numeric(10, 0)), N'SKN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(352 AS Numeric(10, 0)), N'PZB/2014/0051', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION PROREKTORA DS. MORSKICH', CAST(55 AS Numeric(10, 0)), N'RM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(353 AS Numeric(10, 0)), N'PZB/2014/0052', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� SPRAW MORSKICH I PRAKTYK', CAST(56 AS Numeric(10, 0)), N'MM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(354 AS Numeric(10, 0)), N'PZB/2014/0053', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKCJA EKSPLOATACJI STATKU', CAST(57 AS Numeric(10, 0)), N'MS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(355 AS Numeric(10, 0)), N'PZB/2014/0054', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BIURO KARIER', CAST(58 AS Numeric(10, 0)), N'MK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(356 AS Numeric(10, 0)), N'PZB/2014/0055', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STATEK �NAWIGATOR XXI�', CAST(59 AS Numeric(10, 0)), N'NAWIGATOR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(357 AS Numeric(10, 0)), N'PZB/2014/0056', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM �EGLUGI �R�DL�DOWEJ', CAST(60 AS Numeric(10, 0)), N'C��', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(358 AS Numeric(10, 0)), N'PZB/2014/0057', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDIUM DOSKONALENIA KADR OFICERSKICH', CAST(61 AS Numeric(10, 0)), N'SDKO SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(359 AS Numeric(10, 0)), N'PZB/2014/0058', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KURSY DP', CAST(62 AS Numeric(10, 0)), N'KDP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(360 AS Numeric(10, 0)), N'PZB/2014/0059', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'O�RODEK SZKOLENIOWY RATOWNICTWA MORSKIEG', CAST(63 AS Numeric(10, 0)), N'OSRM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(361 AS Numeric(10, 0)), N'PZB/2014/0060', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KOSZTY KURS�W - O% NARZUTU', CAST(64 AS Numeric(10, 0)), N'KKN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(362 AS Numeric(10, 0)), N'PZB/2014/0061', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MORSKI O�RODEK SZKOLENIOWY', CAST(65 AS Numeric(10, 0)), N'MOS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(363 AS Numeric(10, 0)), N'PZB/2014/0062', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MORSKI O�RODEK SZKOLENIOWY W KO�OBRZEGU', CAST(66 AS Numeric(10, 0)), N'MOSK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(364 AS Numeric(10, 0)), N'PZB/2014/0063', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'O�RODEK SZKOLENIOWY RYBO��WSTWA BA�TYCKI', CAST(67 AS Numeric(10, 0)), N'OSRB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(365 AS Numeric(10, 0)), N'PZB/2014/0064', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MARITIME ENGLISH CENTER', CAST(68 AS Numeric(10, 0)), N'MEC', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(366 AS Numeric(10, 0)), N'PZB/2014/0065', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION KANCLERZA', CAST(69 AS Numeric(10, 0)), N'A SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(367 AS Numeric(10, 0)), N'PZB/2014/0066', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SEKRETARIAT KANCLERZA', CAST(70 AS Numeric(10, 0)), N'AS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(368 AS Numeric(10, 0)), N'PZB/2014/0067', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAST�PCA KANCLERZA DS. ROZWOJU', CAST(71 AS Numeric(10, 0)), N'A-2', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(369 AS Numeric(10, 0)), N'PZB/2014/0068', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� INWESTYCJI I ROZWOJU', CAST(72 AS Numeric(10, 0)), N'AR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(370 AS Numeric(10, 0)), N'PZB/2014/0069', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� TECHNICZNY', CAST(73 AS Numeric(10, 0)), N'AT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(371 AS Numeric(10, 0)), N'PZB/2014/0070', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'GRUPA KONSERWACYJNA', CAST(74 AS Numeric(10, 0)), N'GK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(372 AS Numeric(10, 0)), N'PZB/2014/0071', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY BUDYNK�W I BUDOWLI', CAST(75 AS Numeric(10, 0)), N'RBiB SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(373 AS Numeric(10, 0)), N'PZB/2014/0072', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-NABRZE�E JANA Z KOLNA', CAST(76 AS Numeric(10, 0)), N'RNJZK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(374 AS Numeric(10, 0)), N'PZB/2014/0073', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-BUDYNEK OSRM', CAST(77 AS Numeric(10, 0)), N'RBOSRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(375 AS Numeric(10, 0)), N'PZB/2014/0074', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-NABRZE�E OSRM', CAST(78 AS Numeric(10, 0)), N'RNOSRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(376 AS Numeric(10, 0)), N'PZB/2014/0075', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-MOS', CAST(79 AS Numeric(10, 0)), N'RMOS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(377 AS Numeric(10, 0)), N'PZB/2014/0076', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-MOSK', CAST(80 AS Numeric(10, 0)), N'RMOSK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(378 AS Numeric(10, 0)), N'PZB/2014/0077', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-OSRB', CAST(81 AS Numeric(10, 0)), N'ROSRB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(379 AS Numeric(10, 0)), N'PZB/2014/0078', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-WA�Y CHROBREGO 1', CAST(82 AS Numeric(10, 0)), N'RWCh1', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(380 AS Numeric(10, 0)), N'PZB/2014/0079', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-WA�Y CHROBREGO 2', CAST(83 AS Numeric(10, 0)), N'RWCH2', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(381 AS Numeric(10, 0)), N'PZB/2014/0080', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-WA�Y CHROBREGO-SALA GIMNASTYCZNA', CAST(84 AS Numeric(10, 0)), N'RWCHSG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(382 AS Numeric(10, 0)), N'PZB/2014/0081', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-PODG�RNA', CAST(85 AS Numeric(10, 0)), N'RPOD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(383 AS Numeric(10, 0)), N'PZB/2014/0082', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-�O�NIERSKA', CAST(86 AS Numeric(10, 0)), N'R�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(384 AS Numeric(10, 0)), N'PZB/2014/0083', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-WILLOWA', CAST(87 AS Numeric(10, 0)), N'RW', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(385 AS Numeric(10, 0)), N'PZB/2014/0084', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-KORAB-POMIESZCZENIA ADMINISTRACY', CAST(88 AS Numeric(10, 0)), N'RKPA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(386 AS Numeric(10, 0)), N'PZB/2014/0085', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-PASAT-POMIESZCZENIA ADMINISTRACY', CAST(89 AS Numeric(10, 0)), N'RPPA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(387 AS Numeric(10, 0)), N'PZB/2014/0086', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-POBO�NEGO', CAST(90 AS Numeric(10, 0)), N'RPOB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(388 AS Numeric(10, 0)), N'PZB/2014/0087', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-P�YWALNIA', CAST(91 AS Numeric(10, 0)), N'RP�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(389 AS Numeric(10, 0)), N'PZB/2014/0088', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY STO��WKA UL.SZCZERBCOWA-POMIESZC', CAST(92 AS Numeric(10, 0)), N'RSSZPA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(390 AS Numeric(10, 0)), N'PZB/2014/0089', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-SALE DYDAKTYCZNE', CAST(93 AS Numeric(10, 0)), N'RSD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(391 AS Numeric(10, 0)), N'PZB/2014/0090', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REMONTY-DPT �WINOUJ�CIE', CAST(94 AS Numeric(10, 0)), N'RDPT�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(392 AS Numeric(10, 0)), N'PZB/2014/0091', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� FINANSOWY', CAST(95 AS Numeric(10, 0)), N'FF', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(393 AS Numeric(10, 0)), N'PZB/2014/0092', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� KSI�GOWO�CI', CAST(96 AS Numeric(10, 0)), N'FK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(394 AS Numeric(10, 0)), N'PZB/2014/0093', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� OBS�UGI KSI�GOWEJ PROJEKT�W MI�DZY', CAST(97 AS Numeric(10, 0)), N'FZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(395 AS Numeric(10, 0)), N'PZB/2014/0094', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� P�AC', CAST(98 AS Numeric(10, 0)), N'FP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(396 AS Numeric(10, 0)), N'PZB/2014/0095', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KWESTURA', CAST(99 AS Numeric(10, 0)), N'AF SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(397 AS Numeric(10, 0)), N'PZB/2014/0096', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SAMODZIELNE STANOWISKO DS. OCHRONY PRZEC', CAST(100 AS Numeric(10, 0)), N'AOP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(398 AS Numeric(10, 0)), N'PZB/2014/0097', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� ADMINISTRACYJNO-GOSPODARCZY', CAST(101 AS Numeric(10, 0)), N'AG Suma', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(399 AS Numeric(10, 0)), N'PZB/2014/0098', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OBIEKT DYDAKTYCZNO-ADMINISTRACYJNY UL. W', CAST(102 AS Numeric(10, 0)), N'ODAWCH', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(400 AS Numeric(10, 0)), N'PZB/2014/0099', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OBIEKT DYDAKTYCZNY UL. PODG�RNA', CAST(103 AS Numeric(10, 0)), N'ODPO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(401 AS Numeric(10, 0)), N'PZB/2014/0100', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OBIEKT DYDAKTYCZNY UL. �O�NIERSKA', CAST(104 AS Numeric(10, 0)), N'OD�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(402 AS Numeric(10, 0)), N'PZB/2014/0101', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OBIEKT DYDAKTYCZNY UL. WILLOWA', CAST(105 AS Numeric(10, 0)), N'ODW', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(403 AS Numeric(10, 0)), N'PZB/2014/0102', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OSIEDLE AKADEMICKIE', CAST(106 AS Numeric(10, 0)), N'ADS SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(404 AS Numeric(10, 0)), N'PZB/2014/0103', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDENCKI DOM MARYNARZA KORAB-POMIESZCZE', CAST(107 AS Numeric(10, 0)), N'ADS-1PA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(405 AS Numeric(10, 0)), N'PZB/2014/0104', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDENCKI DOM MARYNARZA PASAT-POMIESZCZE', CAST(108 AS Numeric(10, 0)), N'ADS-2PA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(406 AS Numeric(10, 0)), N'PZB/2014/0105', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OBIEKT DYDAKTYCZNY UL. POBO�NEGO', CAST(109 AS Numeric(10, 0)), N'ODP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(407 AS Numeric(10, 0)), N'PZB/2014/0106', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'P�YWALNIA', CAST(110 AS Numeric(10, 0)), N'ADS-P', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(408 AS Numeric(10, 0)), N'PZB/2014/0107', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STO��WKA UL.  SZCZERBCOWA-POMIESZCZENIA', CAST(111 AS Numeric(10, 0)), N'SSZPA', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(409 AS Numeric(10, 0)), N'PZB/2014/0108', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SALE DYDAKTYCZNE W STO��WCE', CAST(112 AS Numeric(10, 0)), N'SDWS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(410 AS Numeric(10, 0)), N'PZB/2014/0109', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DOM PRACY TW�RCZEJ W �WINOUJ�CIU', CAST(113 AS Numeric(10, 0)), N'AW', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(411 AS Numeric(10, 0)), N'PZB/2014/0110', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SALE DYDAKTYCZNE WSP�LNE-AUDIOWIZUALNA,I', CAST(114 AS Numeric(10, 0)), N'SDW�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(412 AS Numeric(10, 0)), N'PZB/2014/0111', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U NAWIGACYJNEGO', CAST(115 AS Numeric(10, 0)), N'DN', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(413 AS Numeric(10, 0)), N'PZB/2014/0112', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KOSZTY KSZTA�CENIA', CAST(116 AS Numeric(10, 0)), N'DNKK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(414 AS Numeric(10, 0)), N'PZB/2014/0113', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W MORSKIE', CAST(117 AS Numeric(10, 0)), N'DNPSM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(415 AS Numeric(10, 0)), N'PZB/2014/0114', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W L�DOWE', CAST(118 AS Numeric(10, 0)), N'DNPSL', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(416 AS Numeric(10, 0)), N'PZB/2014/0115', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W NAWIGATOR XXI', CAST(119 AS Numeric(10, 0)), N'DNPSN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(417 AS Numeric(10, 0)), N'PZB/2014/0116', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'UMUNDUROWANIE STUDENT�W', CAST(120 AS Numeric(10, 0)), N'DNUS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(418 AS Numeric(10, 0)), N'PZB/2014/0117', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REKRUTACJA', CAST(121 AS Numeric(10, 0)), N'DNR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(419 AS Numeric(10, 0)), N'PZB/2014/0118', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIEKANAT WYDZIA�U NAWIGACYJNEGO', CAST(122 AS Numeric(10, 0)), N'DND', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(420 AS Numeric(10, 0)), N'PZB/2014/0119', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT NAWIGACJI MORSKIEJ', CAST(123 AS Numeric(10, 0)), N'INM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(421 AS Numeric(10, 0)), N'PZB/2014/0120', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD NAWIGACJI MORSKIEJ', CAST(124 AS Numeric(10, 0)), N'ZNM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(422 AS Numeric(10, 0)), N'PZB/2014/0121', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD BUDOWY I STATECZNO�CI STATKU', CAST(125 AS Numeric(10, 0)), N'ZBISS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(423 AS Numeric(10, 0)), N'PZB/2014/0122', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD METEOROLOGII I OCEANOGRAFII', CAST(126 AS Numeric(10, 0)), N'ZMIO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(424 AS Numeric(10, 0)), N'PZB/2014/0123', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD RATOWNICTWA I OCHRONY �EGLUGI', CAST(127 AS Numeric(10, 0)), N'ZRIO�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(425 AS Numeric(10, 0)), N'PZB/2014/0124', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM TECHNOLOGII PRZEWOZ�W LNG', CAST(128 AS Numeric(10, 0)), N'CTP LNG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(426 AS Numeric(10, 0)), N'PZB/2014/0125', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM NAUKOWO-BADAWCZE ANALIZY RYZYKA', CAST(129 AS Numeric(10, 0)), N'CNBARES', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(427 AS Numeric(10, 0)), N'PZB/2014/0126', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT IN�YNIERII RUCHU MORSKIEGO', CAST(130 AS Numeric(10, 0)), N'IIRM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(428 AS Numeric(10, 0)), N'PZB/2014/0127', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD MANEWROWANIA I PILOTA�U MORSKIEGO', CAST(131 AS Numeric(10, 0)), N'ZMIPM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(429 AS Numeric(10, 0)), N'PZB/2014/0128', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD BEZPIECZE�STWA NAWIGACYJNEGO', CAST(132 AS Numeric(10, 0)), N'ZBN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(430 AS Numeric(10, 0)), N'PZB/2014/0129', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD URZ�DZE� NAWIGACYJNYCH', CAST(133 AS Numeric(10, 0)), N'ZUN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(431 AS Numeric(10, 0)), N'PZB/2014/0130', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD RYBO��WSTWA MORSKIEGO', CAST(134 AS Numeric(10, 0)), N'ZRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(432 AS Numeric(10, 0)), N'PZB/2014/0131', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT TECHNOLOGII MORSKICH', CAST(135 AS Numeric(10, 0)), N'ITM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(433 AS Numeric(10, 0)), N'PZB/2014/0132', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD INFORMATYCZNYCH TECHNOLOGII MORSK', CAST(136 AS Numeric(10, 0)), N'ZITM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(434 AS Numeric(10, 0)), N'PZB/2014/0133', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD KOMUNIKACYJNYCH TECHNOLOGII MORSK', CAST(137 AS Numeric(10, 0)), N'ZKTM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(435 AS Numeric(10, 0)), N'PZB/2014/0134', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD MATEMATYKI', CAST(138 AS Numeric(10, 0)), N'ZM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(436 AS Numeric(10, 0)), N'PZB/2014/0135', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT GEOINFORMATYKI', CAST(139 AS Numeric(10, 0)), N'IG SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(437 AS Numeric(10, 0)), N'PZB/2014/0136', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD GEODEZJI I HYDROGRAFI', CAST(140 AS Numeric(10, 0)), N'ZGIH', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(438 AS Numeric(10, 0)), N'PZB/2014/0137', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD KARTOGRAFII I GEOINFORMATYKI', CAST(141 AS Numeric(10, 0)), N'ZKIG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(439 AS Numeric(10, 0)), N'PZB/2014/0138', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'CENTRUM IN�YNIERII RUCHU MORSKIEGO', CAST(142 AS Numeric(10, 0)), N'CIRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(440 AS Numeric(10, 0)), N'PZB/2014/0139', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U MECHANICZNEGO', CAST(143 AS Numeric(10, 0)), N'DM', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(441 AS Numeric(10, 0)), N'PZB/2014/0140', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KSZTA�CENIE KADRY', CAST(144 AS Numeric(10, 0)), N'DMKK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(442 AS Numeric(10, 0)), N'PZB/2014/0141', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W MORSKIE', CAST(145 AS Numeric(10, 0)), N'DMPSM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(443 AS Numeric(10, 0)), N'PZB/2014/0142', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W L�DOWE', CAST(146 AS Numeric(10, 0)), N'DMPSL', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(444 AS Numeric(10, 0)), N'PZB/2014/0143', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W NAWIGATOR XXI', CAST(147 AS Numeric(10, 0)), N'DMPSN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(445 AS Numeric(10, 0)), N'PZB/2014/0144', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'UMUNDUROWANIE STUDENT�W', CAST(148 AS Numeric(10, 0)), N'DMUS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(446 AS Numeric(10, 0)), N'PZB/2014/0145', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REKRUTACJA', CAST(149 AS Numeric(10, 0)), N'DMR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(447 AS Numeric(10, 0)), N'PZB/2014/0146', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIEKANAT WYDZIA�U MECHANICZNEGO', CAST(150 AS Numeric(10, 0)), N'DMD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(448 AS Numeric(10, 0)), N'PZB/2014/0147', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT PODSTAWOWYCH NAUK TECHNICZNYCH', CAST(151 AS Numeric(10, 0)), N'IPNT SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(449 AS Numeric(10, 0)), N'PZB/2014/0148', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD MECHANIKI TECHNICZNEJ', CAST(152 AS Numeric(10, 0)), N'ZMT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(450 AS Numeric(10, 0)), N'PZB/2014/0149', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD IN�YNIERII MATERIA��W OKR�TOWYCH', CAST(153 AS Numeric(10, 0)), N'ZIMO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(451 AS Numeric(10, 0)), N'PZB/2014/0150', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD PODSTAW BUDOWY I EKSPLOATACJI MAS', CAST(154 AS Numeric(10, 0)), N'ZPBIEM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(452 AS Numeric(10, 0)), N'PZB/2014/0151', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT EKSPLOATACJI SI�OWNI OKR�TOWYCH', CAST(155 AS Numeric(10, 0)), N'IESO SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(453 AS Numeric(10, 0)), N'PZB/2014/0152', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD MASZYN I URZ�DZE� OKR�TOWYCH', CAST(156 AS Numeric(10, 0)), N'ZMIUO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(454 AS Numeric(10, 0)), N'PZB/2014/0153', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD SI�OWNI OKR�TOWYCH', CAST(157 AS Numeric(10, 0)), N'ZSO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(455 AS Numeric(10, 0)), N'PZB/2014/0154', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'LABORATORIUM SI�OWNI OKR�TOWYCH', CAST(158 AS Numeric(10, 0)), N'LSO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(456 AS Numeric(10, 0)), N'PZB/2014/0155', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WYDZIA�OWE LABORATORIUM BADAWCZE', CAST(159 AS Numeric(10, 0)), N'WLB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(457 AS Numeric(10, 0)), N'PZB/2014/0156', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT ELEKTROTECHNIKI I AUTOMATYKI OK', CAST(160 AS Numeric(10, 0)), N'IEIAO SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(458 AS Numeric(10, 0)), N'PZB/2014/0157', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD ELEKTROTECHNIKI I ELEKTRONIKI OKR', CAST(161 AS Numeric(10, 0)), N'ZEIEO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(459 AS Numeric(10, 0)), N'PZB/2014/0158', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD AUTOMATYKI I ROBOTYKI', CAST(162 AS Numeric(10, 0)), N'ZAIR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(460 AS Numeric(10, 0)), N'PZB/2014/0159', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KATEDRA DIAGNOSTYKI I REMONT�W MASZYN', CAST(163 AS Numeric(10, 0)), N'KDIRS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(461 AS Numeric(10, 0)), N'PZB/2014/0160', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KATEDRA FIZYKI I CHEMII', CAST(164 AS Numeric(10, 0)), N'KFIC', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(462 AS Numeric(10, 0)), N'PZB/2014/0161', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U IN�YNIERYJNO-EKON', CAST(165 AS Numeric(10, 0)), N'DT', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(463 AS Numeric(10, 0)), N'PZB/2014/0162', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KOSZTY KSZTA�CENIA', CAST(166 AS Numeric(10, 0)), N'DTKK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(464 AS Numeric(10, 0)), N'PZB/2014/0163', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W MORSKIE', CAST(167 AS Numeric(10, 0)), N'DTPSM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(465 AS Numeric(10, 0)), N'PZB/2014/0164', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W L�DOWE', CAST(168 AS Numeric(10, 0)), N'DTPSL', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(466 AS Numeric(10, 0)), N'PZB/2014/0165', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRAKTYKI STUDENT�W NAWIGATOR XXI', CAST(169 AS Numeric(10, 0)), N'DTPSN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(467 AS Numeric(10, 0)), N'PZB/2014/0166', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'UMUNDUROWANIE STUDENT�W', CAST(170 AS Numeric(10, 0)), N'DTUS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(468 AS Numeric(10, 0)), N'PZB/2014/0167', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'REKRUTACJA', CAST(171 AS Numeric(10, 0)), N'DTR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(469 AS Numeric(10, 0)), N'PZB/2014/0168', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIEKANAT WYDZIA�U IN�YNIERYJNO-EKONOMIC', CAST(172 AS Numeric(10, 0)), N'DTD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(470 AS Numeric(10, 0)), N'PZB/2014/0169', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRACOWNIA ANALIZ I PROGNOZ EKONOMICZNYCH', CAST(173 AS Numeric(10, 0)), N'PAIPESGM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(471 AS Numeric(10, 0)), N'PZB/2014/0170', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT IN�YNIERII TRANSPORTU', CAST(174 AS Numeric(10, 0)), N'IIT SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(472 AS Numeric(10, 0)), N'PZB/2014/0171', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PRACOWNIA ZASTOSOWA� TELEINFORMATYKI W T', CAST(175 AS Numeric(10, 0)), N'PZTWT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(473 AS Numeric(10, 0)), N'PZB/2014/0172', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD TECHNIKI TRANSPORTU', CAST(176 AS Numeric(10, 0)), N'ZTT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(474 AS Numeric(10, 0)), N'PZB/2014/0173', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD TECHNOLOGII TRANSPORTU ZINTEGROWA', CAST(177 AS Numeric(10, 0)), N'ZTTZIO�', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(475 AS Numeric(10, 0)), N'PZB/2014/0174', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD TOWAROZNAWSTWA I ZARZ�DZANIA JAKO', CAST(178 AS Numeric(10, 0)), N'ZTIZJ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(476 AS Numeric(10, 0)), N'PZB/2014/0175', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD METOD KOMPUTEROWYCH', CAST(179 AS Numeric(10, 0)), N'ZMK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(477 AS Numeric(10, 0)), N'PZB/2014/0176', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INSTYTUT ZARZ�DZANIA TRANSPORTEM', CAST(180 AS Numeric(10, 0)), N'IZT SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(478 AS Numeric(10, 0)), N'PZB/2014/0177', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD NAUK EKONOMICZNYCH I SPO�ECZNYCH', CAST(181 AS Numeric(10, 0)), N'ZNEIS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(479 AS Numeric(10, 0)), N'PZB/2014/0178', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD ORGANIZACJI I ZARZ�DZANIA', CAST(182 AS Numeric(10, 0)), N'ZOIZ', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(480 AS Numeric(10, 0)), N'PZB/2014/0179', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD LOGISTYKI I SYSTEM�W TRANSPORTOWY', CAST(183 AS Numeric(10, 0)), N'ZLIST', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(481 AS Numeric(10, 0)), N'PZB/2014/0180', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD GOSPODARKI MORSKIEJ I POLITYKI TR', CAST(184 AS Numeric(10, 0)), N'ZGMIPT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(482 AS Numeric(10, 0)), N'PZB/2014/0181', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD METOD ILO�CIOWYCH I PROGNOZOWANIA', CAST(185 AS Numeric(10, 0)), N'ZMIIP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(483 AS Numeric(10, 0)), N'PZB/2014/0182', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD FINANS�W I MARKETINGU', CAST(186 AS Numeric(10, 0)), N'ZFIM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(484 AS Numeric(10, 0)), N'PZB/2014/0183', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�AD IN�YNIERII PRODUKCJI', CAST(187 AS Numeric(10, 0)), N'ZIP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(485 AS Numeric(10, 0)), N'PZB/2014/0184', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KONFERENCJE', CAST(188 AS Numeric(10, 0)), N'KONF SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(486 AS Numeric(10, 0)), N'PZB/2014/0185', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KONFERENCJA INLAND SHIPPING', CAST(189 AS Numeric(10, 0)), N'INLAND SHIPPING', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(487 AS Numeric(10, 0)), N'PZB/2014/0186', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KONFERENCJA EXPLO-SHIP', CAST(190 AS Numeric(10, 0)), N'EXPLO-SHIP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(488 AS Numeric(10, 0)), N'PZB/2014/0187', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KONFERENCJA TECHNOLOGIE EDUKACYJNE', CAST(191 AS Numeric(10, 0)), N'TECH. INFOR.', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(489 AS Numeric(10, 0)), N'PZB/2014/0188', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZAK�ADOWY FUNDUSZ �WIADCZE� SOCJALNYCH', CAST(192 AS Numeric(10, 0)), N'ZF�S SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(490 AS Numeric(10, 0)), N'PZB/2014/0189', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ODPIS NA ZAK�ADOWY FUNDUSZ �WIADCZE� SOC', CAST(193 AS Numeric(10, 0)), N'ODPIS ZF�S', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(491 AS Numeric(10, 0)), N'PZB/2014/0190', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PO�YCZKI MIESZKANIOWE', CAST(194 AS Numeric(10, 0)), N'PM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(492 AS Numeric(10, 0)), N'PZB/2014/0191', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POMOC RZECZOWO-FINANSOWA', CAST(195 AS Numeric(10, 0)), N'PRF', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(493 AS Numeric(10, 0)), N'PZB/2014/0192', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WYPOCZYNEK PRACOWNICZY', CAST(196 AS Numeric(10, 0)), N'WP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(494 AS Numeric(10, 0)), N'PZB/2014/0193', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WYPOCZYNEK DZIECI', CAST(197 AS Numeric(10, 0)), N'WD', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(495 AS Numeric(10, 0)), N'PZB/2014/0194', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA�ALNO�� KULTURALNA', CAST(198 AS Numeric(10, 0)), N'DK', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(496 AS Numeric(10, 0)), N'PZB/2014/0195', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KOSZTY ZWI�ZANE Z POGRZEBAMI', CAST(199 AS Numeric(10, 0)), N'KZP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(497 AS Numeric(10, 0)), N'PZB/2014/0196', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BIURO REKTORA', CAST(211 AS Numeric(10, 0)), N'RR', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(498 AS Numeric(10, 0)), N'PZB/2014/0197', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SAMODZIELNE STANOWISKO DS. INWE. I KONTR', CAST(212 AS Numeric(10, 0)), N'RI', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(499 AS Numeric(10, 0)), N'PZB/2014/0198', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SAMODZIELNE STANOWISKO DS.BHP', CAST(213 AS Numeric(10, 0)), N'RBHP', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(500 AS Numeric(10, 0)), N'PZB/2014/0199', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SAMODZIELNE STANOWISKO DS. OBRONY', CAST(214 AS Numeric(10, 0)), N'RO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(501 AS Numeric(10, 0)), N'PZB/2014/0200', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PROJEKTY', CAST(215 AS Numeric(10, 0)), N'PROJEKTY', 1)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(502 AS Numeric(10, 0)), N'PZB/2014/0201', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1733/IIRM/11/WLA', CAST(216 AS Numeric(10, 0)), N'G001', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(503 AS Numeric(10, 0)), N'PZB/2014/0202', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1736/IPNT/11/WLA', CAST(217 AS Numeric(10, 0)), N'G002', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(504 AS Numeric(10, 0)), N'PZB/2014/0203', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1744/IESO/11/WLA', CAST(218 AS Numeric(10, 0)), N'G003', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(505 AS Numeric(10, 0)), N'PZB/2014/0204', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1752/IESO/11/SON', CAST(219 AS Numeric(10, 0)), N'G004', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(506 AS Numeric(10, 0)), N'PZB/2014/0205', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1777/IZT/13/OPU', CAST(220 AS Numeric(10, 0)), N'G005', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(507 AS Numeric(10, 0)), N'PZB/2014/0206', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1779/IZT/13/OPU', CAST(221 AS Numeric(10, 0)), N'G006', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(508 AS Numeric(10, 0)), N'PZB/2014/0207', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1778/IIT/13/OPU', CAST(222 AS Numeric(10, 0)), N'G007', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(509 AS Numeric(10, 0)), N'PZB/2014/0208', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1775/IPNT/12/PBS', CAST(223 AS Numeric(10, 0)), N'G008', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(510 AS Numeric(10, 0)), N'PZB/2014/0209', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/IEIAO/11', CAST(224 AS Numeric(10, 0)), N'S001', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(511 AS Numeric(10, 0)), N'PZB/2014/0210', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'2/S/IEIAO/11', CAST(225 AS Numeric(10, 0)), N'S002', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(512 AS Numeric(10, 0)), N'PZB/2014/0211', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/KF/11', CAST(226 AS Numeric(10, 0)), N'S003', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(513 AS Numeric(10, 0)), N'PZB/2014/0212', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'3/S/IEIAO/11', CAST(227 AS Numeric(10, 0)), N'S004', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(514 AS Numeric(10, 0)), N'PZB/2014/0213', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/IESO/09', CAST(228 AS Numeric(10, 0)), N'S005', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(515 AS Numeric(10, 0)), N'PZB/2014/0214', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'2/S/IESO/09', CAST(229 AS Numeric(10, 0)), N'S006', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(516 AS Numeric(10, 0)), N'PZB/2014/0215', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'3/S/IESO/09', CAST(230 AS Numeric(10, 0)), N'S007', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(517 AS Numeric(10, 0)), N'PZB/2014/0216', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'4/S/IESO/09', CAST(231 AS Numeric(10, 0)), N'S008', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(518 AS Numeric(10, 0)), N'PZB/2014/0217', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'5/S/IESO/09', CAST(232 AS Numeric(10, 0)), N'S009', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(519 AS Numeric(10, 0)), N'PZB/2014/0218', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'6/S/KDIRM/09', CAST(233 AS Numeric(10, 0)), N'S010', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(520 AS Numeric(10, 0)), N'PZB/2014/0219', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'7/S/KDIRM/09', CAST(234 AS Numeric(10, 0)), N'S011', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(521 AS Numeric(10, 0)), N'PZB/2014/0220', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'8/S/IPNT/09', CAST(235 AS Numeric(10, 0)), N'S012', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(522 AS Numeric(10, 0)), N'PZB/2014/0221', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'9/S/IMFICH/09', CAST(236 AS Numeric(10, 0)), N'S013', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(523 AS Numeric(10, 0)), N'PZB/2014/0222', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'10/S/IESO/12', CAST(237 AS Numeric(10, 0)), N'S014', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(524 AS Numeric(10, 0)), N'PZB/2014/0223', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'11/S/KDIRM/12', CAST(238 AS Numeric(10, 0)), N'S015', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(525 AS Numeric(10, 0)), N'PZB/2014/0224', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/WM/98/13', CAST(239 AS Numeric(10, 0)), N'S016', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(526 AS Numeric(10, 0)), N'PZB/2014/0225', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/WM/83/13', CAST(240 AS Numeric(10, 0)), N'S017', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(527 AS Numeric(10, 0)), N'PZB/2014/0226', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/CIRM/13', CAST(241 AS Numeric(10, 0)), N'S018', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(528 AS Numeric(10, 0)), N'PZB/2014/0227', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/IIRM/13', CAST(242 AS Numeric(10, 0)), N'S019', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(529 AS Numeric(10, 0)), N'PZB/2014/0228', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/INM/13', CAST(243 AS Numeric(10, 0)), N'S020', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(530 AS Numeric(10, 0)), N'PZB/2014/0229', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'2/S/INM/13', CAST(244 AS Numeric(10, 0)), N'S021', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(531 AS Numeric(10, 0)), N'PZB/2014/0230', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'3/S/INM/13', CAST(245 AS Numeric(10, 0)), N'S022', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(532 AS Numeric(10, 0)), N'PZB/2014/0231', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/KG/13', CAST(246 AS Numeric(10, 0)), N'S023', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(533 AS Numeric(10, 0)), N'PZB/2014/0232', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/ITM/13', CAST(247 AS Numeric(10, 0)), N'S024', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(534 AS Numeric(10, 0)), N'PZB/2014/0233', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'2/S/ITM/13', CAST(248 AS Numeric(10, 0)), N'S025', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(535 AS Numeric(10, 0)), N'PZB/2014/0234', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/WN/98/13', CAST(249 AS Numeric(10, 0)), N'S026', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(536 AS Numeric(10, 0)), N'PZB/2014/0235', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/S/WN/83/13', CAST(250 AS Numeric(10, 0)), N'S027', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(537 AS Numeric(10, 0)), N'PZB/2014/0236', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'6/DC/MN/IZT/2012', CAST(251 AS Numeric(10, 0)), N'S028', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(538 AS Numeric(10, 0)), N'PZB/2014/0237', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'7/DC/MN/IZT/2012', CAST(252 AS Numeric(10, 0)), N'S029', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(539 AS Numeric(10, 0)), N'PZB/2014/0238', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'8/DC/MN/IIT/2012', CAST(253 AS Numeric(10, 0)), N'S030', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(540 AS Numeric(10, 0)), N'PZB/2014/0239', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'12/DC/MN/KG/2012', CAST(254 AS Numeric(10, 0)), N'S031', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(541 AS Numeric(10, 0)), N'PZB/2014/0240', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'13/DC/MN/ITM/2012', CAST(255 AS Numeric(10, 0)), N'S032', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(542 AS Numeric(10, 0)), N'PZB/2014/0241', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'14/DC/MN/ITM/2012', CAST(256 AS Numeric(10, 0)), N'S033', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(543 AS Numeric(10, 0)), N'PZB/2014/0242', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'15/DC/MN/INM/2012', CAST(257 AS Numeric(10, 0)), N'S034', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(544 AS Numeric(10, 0)), N'PZB/2014/0243', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'16/DC/MN/INM/2012', CAST(258 AS Numeric(10, 0)), N'S035', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(545 AS Numeric(10, 0)), N'PZB/2014/0244', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'17/DC/MN/IIRM/2012', CAST(259 AS Numeric(10, 0)), N'S036', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(546 AS Numeric(10, 0)), N'PZB/2014/0245', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'18/DC/MN/IIRM/2012', CAST(260 AS Numeric(10, 0)), N'S037', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(547 AS Numeric(10, 0)), N'PZB/2014/0246', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'19/DC/MN/IIRM/2012', CAST(261 AS Numeric(10, 0)), N'S038', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(548 AS Numeric(10, 0)), N'PZB/2014/0247', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'20/DC/MN/IIRM/2012', CAST(262 AS Numeric(10, 0)), N'S039', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(549 AS Numeric(10, 0)), N'PZB/2014/0248', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'21/DC/MN/CIRM/2013', CAST(263 AS Numeric(10, 0)), N'S040', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(550 AS Numeric(10, 0)), N'PZB/2014/0249', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'22/DC/MN/KG/2013', CAST(264 AS Numeric(10, 0)), N'S041', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(551 AS Numeric(10, 0)), N'PZB/2014/0250', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'23/DC/MN/ITM/2013', CAST(265 AS Numeric(10, 0)), N'S042', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(552 AS Numeric(10, 0)), N'PZB/2014/0251', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'24/DC/MN/INM/2013', CAST(266 AS Numeric(10, 0)), N'S043', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(553 AS Numeric(10, 0)), N'PZB/2014/0252', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'25/DC/MN/INM/2013', CAST(267 AS Numeric(10, 0)), N'S044', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(554 AS Numeric(10, 0)), N'PZB/2014/0253', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'26/DC/MN/CIRM/2013', CAST(268 AS Numeric(10, 0)), N'S045', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(555 AS Numeric(10, 0)), N'PZB/2014/0254', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'27/DC/MN/INM/2013', CAST(269 AS Numeric(10, 0)), N'S046', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(556 AS Numeric(10, 0)), N'PZB/2014/0255', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'28/DC/MN/INM/2013', CAST(270 AS Numeric(10, 0)), N'S047', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(557 AS Numeric(10, 0)), N'PZB/2014/0256', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'29/DC/MN/IIRM/2013', CAST(271 AS Numeric(10, 0)), N'S048', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(558 AS Numeric(10, 0)), N'PZB/2014/0257', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'30/DC/MN/INM/2013', CAST(272 AS Numeric(10, 0)), N'S049', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(559 AS Numeric(10, 0)), N'PZB/2014/0258', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'31/DC/MN/IIRM/2013', CAST(273 AS Numeric(10, 0)), N'S050', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(560 AS Numeric(10, 0)), N'PZB/2014/0259', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'32/DC/MN/ITM/2013', CAST(274 AS Numeric(10, 0)), N'S051', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(561 AS Numeric(10, 0)), N'PZB/2014/0260', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'33/DC/MN/IIRM/2013', CAST(275 AS Numeric(10, 0)), N'S052', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(562 AS Numeric(10, 0)), N'PZB/2014/0261', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'34/DC/MN/KG/2013', CAST(276 AS Numeric(10, 0)), N'S053', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(563 AS Numeric(10, 0)), N'PZB/2014/0262', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/SPUB/KG/12', CAST(277 AS Numeric(10, 0)), N'S054', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(564 AS Numeric(10, 0)), N'PZB/2014/0263', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'1/SPUB/KG/13', CAST(278 AS Numeric(10, 0)), N'S055', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(565 AS Numeric(10, 0)), N'PZB/2014/0264', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.06.02.00-32-003/13', CAST(279 AS Numeric(10, 0)), N'M022', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(566 AS Numeric(10, 0)), N'PZB/2014/0265', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.04.04.00-00-093/12', CAST(280 AS Numeric(10, 0)), N'M021', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(567 AS Numeric(10, 0)), N'PZB/2014/0266', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DS./1359/6/WZ4/�K/2013', CAST(281 AS Numeric(10, 0)), N'M020', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(568 AS Numeric(10, 0)), N'PZB/2014/0267', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'2013-1-PL1-ERA02-39250', CAST(282 AS Numeric(10, 0)), N'M019', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(569 AS Numeric(10, 0)), N'PZB/2014/0268', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POL-NOR/197155/15/2013', CAST(283 AS Numeric(10, 0)), N'M018', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(570 AS Numeric(10, 0)), N'PZB/2014/0269', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'00021-61720-OR1600010/12/13', CAST(284 AS Numeric(10, 0)), N'M017', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(571 AS Numeric(10, 0)), N'PZB/2014/0270', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'00020-61720-OR1600009/12/13', CAST(285 AS Numeric(10, 0)), N'M016', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(572 AS Numeric(10, 0)), N'PZB/2014/0271', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.04.01.01-00-023/12', CAST(286 AS Numeric(10, 0)), N'M015', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(573 AS Numeric(10, 0)), N'PZB/2014/0272', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OR16-61704-OR1600008/12', CAST(287 AS Numeric(10, 0)), N'M014', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(574 AS Numeric(10, 0)), N'PZB/2014/0273', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WND-RPZP.01.02.02-32-003/11', CAST(288 AS Numeric(10, 0)), N'M013', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(575 AS Numeric(10, 0)), N'PZB/2014/0274', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WND-POKL.08.01.01-32-014/11', CAST(289 AS Numeric(10, 0)), N'M012', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(576 AS Numeric(10, 0)), N'PZB/2014/0275', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'00007-61720-OR1600007/12', CAST(290 AS Numeric(10, 0)), N'M011', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(577 AS Numeric(10, 0)), N'PZB/2014/0276', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL-04.01.02-00-110/12', CAST(291 AS Numeric(10, 0)), N'M010', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(578 AS Numeric(10, 0)), N'PZB/2014/0277', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL-04.01.02-00-111/12', CAST(292 AS Numeric(10, 0)), N'M009', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(579 AS Numeric(10, 0)), N'PZB/2014/0278', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WND-RPZP.01.02.02-32-001/11', CAST(293 AS Numeric(10, 0)), N'M008', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(580 AS Numeric(10, 0)), N'PZB/2014/0279', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'WTPB.01.01.00-072-019/11', CAST(294 AS Numeric(10, 0)), N'M007', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(581 AS Numeric(10, 0)), N'PZB/2014/0280', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IEE/10/154/SI2.589407', CAST(295 AS Numeric(10, 0)), N'M006', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(582 AS Numeric(10, 0)), N'PZB/2014/0281', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'00004-61720-OR1600006/10/11', CAST(296 AS Numeric(10, 0)), N'M005', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(583 AS Numeric(10, 0)), N'PZB/2014/0282', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.04.01.02-00-040/10', CAST(297 AS Numeric(10, 0)), N'M004', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(584 AS Numeric(10, 0)), N'PZB/2014/0283', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.04.01.01-00-052/09', CAST(298 AS Numeric(10, 0)), N'M003', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(585 AS Numeric(10, 0)), N'PZB/2014/0284', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'POKL.04.01.01-00-227/09', CAST(299 AS Numeric(10, 0)), N'M002', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(586 AS Numeric(10, 0)), N'PZB/2014/0285', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'N� FU06-CT-2004-00081', CAST(300 AS Numeric(10, 0)), N'M001', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(587 AS Numeric(10, 0)), N'PZB/2014/0286', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/01', CAST(301 AS Numeric(10, 0)), N'B001', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(588 AS Numeric(10, 0)), N'PZB/2014/0287', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/02', CAST(302 AS Numeric(10, 0)), N'B002', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(589 AS Numeric(10, 0)), N'PZB/2014/0288', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/03', CAST(303 AS Numeric(10, 0)), N'B003', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(590 AS Numeric(10, 0)), N'PZB/2014/0289', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/04', CAST(304 AS Numeric(10, 0)), N'B004', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(591 AS Numeric(10, 0)), N'PZB/2014/0290', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/05', CAST(305 AS Numeric(10, 0)), N'B005', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(592 AS Numeric(10, 0)), N'PZB/2014/0291', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/06', CAST(306 AS Numeric(10, 0)), N'B006', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(593 AS Numeric(10, 0)), N'PZB/2014/0292', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/07', CAST(307 AS Numeric(10, 0)), N'B007', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(594 AS Numeric(10, 0)), N'PZB/2014/0293', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/08', CAST(308 AS Numeric(10, 0)), N'B008', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(595 AS Numeric(10, 0)), N'PZB/2014/0294', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BUD/2013/09', CAST(309 AS Numeric(10, 0)), N'B009', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(596 AS Numeric(10, 0)), N'PZB/2014/0030', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDIUM NAUKI J�ZYK�W OBCYCH', CAST(34 AS Numeric(10, 0)), N'SNJO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(598 AS Numeric(10, 0)), N'PZB/2014/0295', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DZIA� ADMINISTRACYJNO GOSPODARCZY', CAST(310 AS Numeric(10, 0)), N'AG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(599 AS Numeric(10, 0)), N'PZB/2014/0296', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ADS - PZB/2014/0296', CAST(311 AS Numeric(10, 0)), N'ADS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(600 AS Numeric(10, 0)), N'PZB/2014/0297', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'BG - PZB/2014/0297', CAST(312 AS Numeric(10, 0)), N'BG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(601 AS Numeric(10, 0)), N'PZB/2014/0298', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'NN - PZB/2014/0298', CAST(314 AS Numeric(10, 0)), N'NN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(602 AS Numeric(10, 0)), N'PZB/2014/0299', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MM - PZB/2014/0299', CAST(315 AS Numeric(10, 0)), N'MM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(603 AS Numeric(10, 0)), N'PZB/2014/0300', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ITM - PZB/2014/0300', CAST(316 AS Numeric(10, 0)), N'ITM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(604 AS Numeric(10, 0)), N'PZB/2014/0301', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IIT - PZB/2014/0301', CAST(317 AS Numeric(10, 0)), N'IIT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(605 AS Numeric(10, 0)), N'PZB/2014/0302', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'R - PZB/2014/0302', CAST(318 AS Numeric(10, 0)), N'R', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(606 AS Numeric(10, 0)), N'PZB/2014/0303', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SDKO - PZB/2014/0303', CAST(319 AS Numeric(10, 0)), N'SDKO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(607 AS Numeric(10, 0)), N'PZB/2014/0304', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'INM - PZB/2014/0304', CAST(320 AS Numeric(10, 0)), N'INM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(608 AS Numeric(10, 0)), N'PZB/2014/0305', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'AF - PZB/2014/0305', CAST(321 AS Numeric(10, 0)), N'AF', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(609 AS Numeric(10, 0)), N'PZB/2014/0306', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IG - PZB/2014/0306', CAST(322 AS Numeric(10, 0)), N'IG', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(610 AS Numeric(10, 0)), N'PZB/2014/0307', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'OSRM - PZB/2014/0307', CAST(323 AS Numeric(10, 0)), N'OSRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(611 AS Numeric(10, 0)), N'PZB/2014/0308', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IESO - PZB/2014/0308', CAST(324 AS Numeric(10, 0)), N'IESO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(612 AS Numeric(10, 0)), N'PZB/2014/0309', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IIRM - PZB/2014/0309', CAST(325 AS Numeric(10, 0)), N'IIRM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(613 AS Numeric(10, 0)), N'PZB/2014/0310', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IZT - PZB/2014/0310', CAST(326 AS Numeric(10, 0)), N'IZT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(614 AS Numeric(10, 0)), N'PZB/2014/0311', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RM - PZB/2014/0311', CAST(327 AS Numeric(10, 0)), N'RM', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(615 AS Numeric(10, 0)), N'PZB/2014/0312', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RN - PZB/2014/0312', CAST(328 AS Numeric(10, 0)), N'RN', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(616 AS Numeric(10, 0)), N'PZB/2014/0313', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'ZF�S - PZB/2014/0313', CAST(329 AS Numeric(10, 0)), N'ZF�S', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(617 AS Numeric(10, 0)), N'PZB/2014/0314', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SWFIS - PZB/2014/0314', CAST(330 AS Numeric(10, 0)), N'SWFIS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(618 AS Numeric(10, 0)), N'PZB/2014/0315', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RBiB - PZB/2014/0315', CAST(331 AS Numeric(10, 0)), N'RBiB', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(619 AS Numeric(10, 0)), N'PZB/2014/0316', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'KONF - PZB/2014/0316', CAST(332 AS Numeric(10, 0)), N'KONF', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(620 AS Numeric(10, 0)), N'PZB/2014/0317', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IPNT - PZB/2014/0317', CAST(333 AS Numeric(10, 0)), N'IPNT', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(621 AS Numeric(10, 0)), N'PZB/2014/0318', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'IEIAO - PZB/2014/0318', CAST(334 AS Numeric(10, 0)), N'IEIAO', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(622 AS Numeric(10, 0)), N'PZB/2014/0319', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'FSKS - PZB/2014/0319', CAST(335 AS Numeric(10, 0)), N'FSKS', 0)
GO
INSERT [dbo].[dsg_ams_services_purchase_plan] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(623 AS Numeric(10, 0)), N'PZB/2014/0320', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'AZS - PZB/2014/0320', CAST(336 AS Numeric(10, 0)), N'AZS', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(38 AS Numeric(10, 0)), N'BK/2014/0001', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SUMA  AKADEMIA MORSKA W SZCZECINIE', CAST(1 AS Numeric(10, 0)), N'AMS', 1)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(42 AS Numeric(10, 0)), N'BK/2014/0005', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SUMA  PION PROREKTORA DS. MORSKICH', CAST(55 AS Numeric(10, 0)), N'RM SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(50 AS Numeric(10, 0)), N'BK/2014/0013', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'SUMA  PION KANCLERZA', CAST(69 AS Numeric(10, 0)), N'A SUMA', 1)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(69 AS Numeric(10, 0)), N'BK/2014/0028', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'G008 - BK/2014/0028', CAST(223 AS Numeric(10, 0)), N'G008', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(62 AS Numeric(10, 0)), N'BK/2014/0021', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Bud�et projektu Martech', CAST(294 AS Numeric(10, 0)), N'M007', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(68 AS Numeric(10, 0)), N'BK/2014/0027', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'G007 - BK/2014/0027', CAST(222 AS Numeric(10, 0)), N'G007', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(39 AS Numeric(10, 0)), N'BK/2014/0002', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION REKTORA', CAST(2 AS Numeric(10, 0)), N'R SUMA', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(48 AS Numeric(10, 0)), N'BK/2014/0011', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'O�RODEK SZKOLENIOWY RYBO��WSTWA BA�TYCKI', CAST(67 AS Numeric(10, 0)), N'OSRB', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(49 AS Numeric(10, 0)), N'BK/2014/0012', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MARITIME ENGLISH CENTER', CAST(68 AS Numeric(10, 0)), N'MEC', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(64 AS Numeric(10, 0)), N'BK/2014/0023', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Bud�et projektu GRASS', CAST(283 AS Numeric(10, 0)), N'M018', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(44 AS Numeric(10, 0)), N'BK/2014/0007', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STUDIUM DOSKONALENIA KADR OFICERSKICH', CAST(61 AS Numeric(10, 0)), N'SDKO SUMA', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(46 AS Numeric(10, 0)), N'BK/2014/0009', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MORSKI O�RODEK SZKOLENIOWY', CAST(65 AS Numeric(10, 0)), N'MOS', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(73 AS Numeric(10, 0)), N'BK/2014/0032', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'M002 - BK/2014/0032', CAST(299 AS Numeric(10, 0)), N'M002', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(53 AS Numeric(10, 0)), N'BK/2014/0016', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U NAWIGACYJNEGO', CAST(115 AS Numeric(10, 0)), N'DN', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(55 AS Numeric(10, 0)), N'BK/2014/0018', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U IN�YNIERYJNO-EKON', CAST(165 AS Numeric(10, 0)), N'DT', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(54 AS Numeric(10, 0)), N'BK/2014/0017', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION DZIEKANA WYDZIA�U MECHANICZNEGO', CAST(143 AS Numeric(10, 0)), N'DM', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(52 AS Numeric(10, 0)), N'BK/2014/0015', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'DOM PRACY TW�RCZEJ W �WINOUJ�CIU', CAST(113 AS Numeric(10, 0)), N'AW', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(43 AS Numeric(10, 0)), N'BK/2014/0006', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'STATEK �NAWIGATOR XXI�', CAST(59 AS Numeric(10, 0)), N'NAWIGATOR', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(65 AS Numeric(10, 0)), N'BK/2014/0024', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'G004 - BK/2014/0024', CAST(219 AS Numeric(10, 0)), N'G004', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(40 AS Numeric(10, 0)), N'BK/2014/0003', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION PROREKTORA DS. NAUKI', CAST(12 AS Numeric(10, 0)), N'RB', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(57 AS Numeric(10, 0)), N'BK/2014/0020', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Bud�et projektu Ekspertyzy', CAST(285 AS Numeric(10, 0)), N'M016', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(72 AS Numeric(10, 0)), N'BK/2014/0031', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'M003 - CA�A NAPRZ�D BK/2014/0031', CAST(298 AS Numeric(10, 0)), N'M003', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(56 AS Numeric(10, 0)), N'BK/2014/0019', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Bud�et projektu Dorsze', CAST(290 AS Numeric(10, 0)), N'M011', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(45 AS Numeric(10, 0)), N'BK/2014/0008', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'O�RODEK SZKOLENIOWY RATOWNICTWA MORSKIEG', CAST(63 AS Numeric(10, 0)), N'OSRM SUMA', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(41 AS Numeric(10, 0)), N'BK/2014/0004', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'PION PROREKTORA DS. NAUCZANIA', CAST(27 AS Numeric(10, 0)), N'RN SUMA', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(70 AS Numeric(10, 0)), N'BK/2014/0029', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'M017 - Audyty energ BK/2014/0029', CAST(284 AS Numeric(10, 0)), N'M017', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(63 AS Numeric(10, 0)), N'BK/2014/0022', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Bud�et projektu Kolumbowie', CAST(279 AS Numeric(10, 0)), N'M022', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(75 AS Numeric(10, 0)), N'BK/2014/0034', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'Pion Kanclerza', CAST(337 AS Numeric(10, 0)), N'A', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(67 AS Numeric(10, 0)), N'BK/2014/0026', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'G006 - BK/2014/0026', CAST(221 AS Numeric(10, 0)), N'G006', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(66 AS Numeric(10, 0)), N'BK/2014/0025', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'G005 - BK/2014/0025', CAST(220 AS Numeric(10, 0)), N'G005', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(47 AS Numeric(10, 0)), N'BK/2014/0010', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'MORSKI O�RODEK SZKOLENIOWY W KO�OBRZEGU', CAST(66 AS Numeric(10, 0)), N'MOSK', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(74 AS Numeric(10, 0)), N'BK/2014/0033', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'RM - BK/2014/0033', CAST(327 AS Numeric(10, 0)), N'RM', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(71 AS Numeric(10, 0)), N'BK/2014/0030', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'M010 - Informatyka BK/2014/0030', CAST(291 AS Numeric(10, 0)), N'M010', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(76 AS Numeric(10, 0)), N'BK/2014/0035', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'bud�et eksplatacji statku', CAST(57 AS Numeric(10, 0)), N'MS', 0)
GO
INSERT [dbo].[dsg_ams_services_unit_budget_ko] ([id], [idm], [budzet_id], [budzet_idm], [nazwa], [bd_str_budzet_id], [bd_str_budzet_idn], [czy_agregat]) VALUES (CAST(51 AS Numeric(10, 0)), N'BK/2014/0014', CAST(1 AS Numeric(10, 0)), N'B/2014/0001', N'P�YWALNIA', CAST(110 AS Numeric(10, 0)), N'ADS-P', 0)
GO
