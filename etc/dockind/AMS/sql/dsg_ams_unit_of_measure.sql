CREATE TABLE [dbo].[dsg_ams_unit_of_measure](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [varchar](50) NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]
CREATE UNIQUE INDEX [dsg_ams_unit_of_measure_id] ON [dsg_ams_unit_of_measure](id);

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_ams_unit_of_measure] ON 

GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'm', N'metr', N'3', NULL, 1, 1)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'm3', N'metr sze�cienny', N'1', NULL, 1, 2)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'l', N'litr', N'0', NULL, 1, 3)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'szt', N'sztuka', N'0', NULL, 1, 4)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'par', N'para', N'0', NULL, 1, 5)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'kg', N'kilogram', N'3', NULL, 1, 6)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'kpl', N'komplet', N'0', NULL, 1, 7)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'km', N'kilometr', N'3', NULL, 1, 8)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'm2', N'metr kwadratowy', N'1', NULL, 1, 9)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'kWh', N'kilowatogodzina', N'0', NULL, 1, 10)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N't', N'tona', N'3', NULL, 1, 11)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'us�', N'us�uga', N'0', NULL, 1, 12)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'op', N'opakowanie', N'0', NULL, 1, 13)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'ryz', N'ryza', N'0', NULL, 1, 14)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'godz', N'godzina', N'0', NULL, 1, 15)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'egz', N'egzemplarz', N'0', NULL, 1, 16)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'%', N'procent', N'0', NULL, 1, 17)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'doba', N'doba', N'0', NULL, 1, 18)
GO
INSERT [dbo].[dsg_ams_unit_of_measure] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'minuta', N'minuta', N'3', NULL, 1, 19)
GO
SET IDENTITY_INSERT [dbo].[dsg_ams_unit_of_measure] OFF
GO
