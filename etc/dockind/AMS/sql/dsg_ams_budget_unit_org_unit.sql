drop table dsg_ams_budget_unit_org_unit;

CREATE TABLE dsg_ams_budget_unit_org_unit (
    ID BIGINT IDENTITY(1,1) NOT NULL,
    BD_STR_BUDZET_ID REAL NULL,
    BD_STR_BUDZET_IDN VARCHAR(50) NULL,
    BD_STR_BUDZET_NAD_ID REAL NULL,
    BD_STR_BUDZET_NAD_IDN VARCHAR(50) NULL,
    BUDZET_ID REAL NULL,
    CZY_WIODACA REAL NULL,
    KOMORKA_ID REAL NULL,
    KOMORKA_IDN VARCHAR(50) NULL,
    NAZWA_KOMORKI_BUDZETOWEJ VARCHAR(100) NULL,
    NAZWA_KOMORKI_ORGANIZACYJNEJ VARCHAR(100) NULL,
    available bit default(1) not null
)
ALTER TABLE dsg_ams_budget_unit_org_unit
ADD CONSTRAINT PK_dsg_ams_budget_unit_org_unit PRIMARY KEY(ID);

CREATE INDEX IDX_dsg_ams_budget_unit_org_unit ON dsg_ams_budget_unit_org_unit(BD_STR_BUDZET_ID,BUDZET_ID,KOMORKA_ID);

CREATE VIEW dsg_ams_budget_unit_org_unit_view AS
	SELECT
		ID AS id,
		IDM AS cn,
		NAZWA AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM dsg_ams_budget_unit_org_unit


sp_rename 'dsg_ams_budget_unit_org_unit.bd_str_budzet_nas_id','bd_str_budzet_nad_id'