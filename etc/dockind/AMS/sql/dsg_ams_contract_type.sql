CREATE TABLE dsg_ams_contract_type (
	id numeric(18, 0) identity(1,1) NOT NULL,
	idm varchar(50),
	name varchar(100)
)