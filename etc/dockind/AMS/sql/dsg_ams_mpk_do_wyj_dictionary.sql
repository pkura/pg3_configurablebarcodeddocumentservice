CREATE TABLE dsg_ams_mpk_do_wyj_dictionary (
	id numeric(18, 0) identity(1,1) NOT NULL,
	rodzaj numeric(18, 0),
	zlecenie numeric(18, 0),
	okres_budzetowy numeric(18, 0),
	komorka_budzetowa numeric(18, 0),
	pozycja_planu_zakupow numeric(18, 0),
	kontrakt numeric(18, 0),
	budzet_kontraktu numeric(18, 0),
	pozycja_w_budzecie numeric(18, 0),
	zadanie_finansowe numeric(18, 0),
	zrodlo_finansowania numeric(18, 0),
	netto numeric(18, 2),
	stawka numeric(18, 0),
	kwota_vat numeric(18, 2),
	brutto numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE dsg_ams_mpk_do_wyj_dictionary add rodzaj_budzetu numeric(18, 0)
ALTER TABLE dsg_ams_mpk_do_wyj_dictionary add identyfikator_planu_budzetu numeric(18, 0)
ALTER TABLE dsg_ams_mpk_do_wyj_dictionary add kwota_budzetu numeric(18, 2)


DROP TABLE dsg_ams_mpk_do_wyj_dictionary;

CREATE TABLE dsg_ams_mpk_do_wyj_dictionary (
	id numeric(18, 0) identity(1,1) NOT NULL,
	okres_budzetowy_do_wyj numeric(18, 0),
	komorka_budzetowa_do_wyj numeric(18, 0),
	rodzaj_budzetu_do_wyj numeric(18, 0),
	identyfikator_planu_do_wyj numeric(18, 0),
	pozycja_planu_zakupow_do_wyj numeric(18, 0),
	zadanie_finansowe_do_wyj numeric(18, 0),
	kwota_budzetu_do_wyj numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]