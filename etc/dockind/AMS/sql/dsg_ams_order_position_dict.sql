CREATE TABLE dsg_ams_order_position_dict (
	id numeric(18, 0) identity(1,1) NOT NULL,
	nr_poz int,
	wytwor varchar(100),
	ilosc numeric(18, 2),
	cena numeric(18, 2),
	cena_brutto numeric(18, 2),
	koszt numeric(18, 2),
	kwota_netto numeric(18, 2),
	kwota_vat numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE dsg_ams_order_position_dict ADD wytwor_nazwa varchar(150);