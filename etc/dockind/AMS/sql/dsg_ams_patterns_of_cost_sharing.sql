CREATE TABLE dsg_ams_patterns_of_cost_sharing (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 2),
	idm varchar(100),
	nazwa varchar(100)
)

CREATE VIEW dsg_ams_view_patterns_of_cost_sharing AS
	SELECT
		id,
		idm AS cn,
		nazwa AS title,
		NULL AS centrum,
		NULL AS refValue,
		1 AS available
	FROM dsg_ams_patterns_of_cost_sharing;