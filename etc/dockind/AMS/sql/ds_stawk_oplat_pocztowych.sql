-- waga
-- zpo 				0 - bez ZPO, 1 - za ZPO
-- poziomGabarytu   1 - A, 2 - B
-- poziomListu      4 - zwyk�y krajowy ekonomiczny, 5 - zwyk�y krajowy priorytet, 6 - polecony krajowy ekonomiczny, 7 - polecony krajowy priorytet
-- poziomWagi       2 - do 350g 3 - do 1000g  4 - do 2000g 1 - do 50g
-- kwota
CREATE TABLE DS_STAWKI_OPLAT_POCZTOWYCH
(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
waga varchar(20) not null,
zpo smallint not null,
poziomGabarytu smallint not null,
poziomListu smallint not null,
poziomWagi smallint not null,
kwota numeric(6, 2) not null
);

-- PRZESY�KI ZWYK�E KRAJOWE -- EKONOMICZNE -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 1, 4, 1, 1.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 1, 4, 2, 3.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 1, 4, 3, 6.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 1, 4, 4, 1.60);

-- PRZESY�KI ZWYK�E KRAJOWE -- EKONOMICZNE -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 2, 4, 1, 3.75);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 2, 4, 2, 4.75);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 2, 4, 3, 7.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 2, 4, 4, 3.75);


-- PRZESY�KI ZWYK�E KRAJOWE -- PRIORYTETOWE -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 1, 5, 1, 2.35);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 1, 5, 2, 2.35);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 1, 5, 3, 4.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 1, 5, 4, 8.80);


-- PRZESY�KI ZWYK�E KRAJOWE -- PRIORYTETOWE -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 2, 5, 1, 5.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 2, 5, 2, 5.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 2, 5, 3, 7.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 2, 5, 4, 10.90);


-- PRZESY�KI POLECONE KRAJOWE -- EKONOMICZNE -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 1, 6, 1, 3.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 1, 6, 2, 3.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 1, 6, 3, 5.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 1, 6, 4, 8.50);


-- PRZESY�KI POLECONE KRAJOWE -- EKONOMICZNE -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 2, 6, 1, 5.95);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 2, 6, 2, 5.95);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 2, 6, 3, 6.95);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 2, 6, 4, 9.50);


-- PRZESY�KI POLECONE KRAJOWE -- PRIORYTETOWE -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 1, 7, 1, 4.55);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 1, 7, 2, 4.55);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 1, 7, 3, 6.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 1, 7, 4, 11.00);


-- PRZESY�KI POLECONE KRAJOWE -- PRIORYTETOWE -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 2, 7, 1, 7.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     0, 2, 7, 2, 7.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    0, 2, 7, 3, 9.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    0, 2, 7, 4, 13.10);


-- PRZESY�KI POLECONE KRAJOWE -- EKONOMICZNE -- ZPO -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 1, 6, 1, 5.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     1, 1, 6, 2, 5.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    1, 1, 6, 3, 7.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    1, 1, 6, 4, 10.40);


-- PRZESY�KI POLECONE KRAJOWE -- EKONOMICZNE -- ZPO -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 2, 6, 1, 7.85);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     1, 2, 6, 2, 7.85);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    1, 2, 6, 3, 8.85);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    1, 2, 6, 4, 11.40);


-- PRZESY�KI POLECONE KRAJOWE -- PRIORYTETOWE -- ZPO -- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 1, 7, 1, 6.45);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     1, 1, 7, 2, 6.45);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    1, 1, 7, 3, 8.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    1, 1, 7, 4, 12.90);


-- PRZESY�KI POLECONE KRAJOWE -- PRIORYTETOWE -- ZPO -- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 2, 7, 1, 9.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',     1, 2, 7, 2, 9.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',    1, 2, 7, 3, 11.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',    1, 2, 7, 4, 15.00);
