--DROP TABLE IF EXISTS DSG_AMS_ZRODLA_FINANSOWANIA
--CREATE TABLE [DSG_AMS_ZRODLA_FINANSOWANIA](
--	[id] [bigint] IDENTITY(1,1) NOT NULL,
--
--	[cn] [varchar](50) NULL,
--	[title] [varchar](255) NULL,
--	[centrum] [int] NULL,
--	[refValue] [varchar](20) NULL,
--	[available] [bit] NULL
--);
--ALTER TABLE DSG_AMS_ZRODLA_FINANSOWANIA ADD PRIMARY KEY (ID);
--CREATE INDEX DSG_AMS_ZRODLA_FINANSOWANIA_INDEX ON DSG_AMS_ZRODLA_FINANSOWANIA (ID);
--

CREATE TABLE [DSG_AMS_PROJ_MNAR_ZR_FINANS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_PROJ_MNAR_ZR_FINANS ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_PROJ_MNAR_ZR_FINANS_INDEX ON DSG_AMS_PROJ_MNAR_ZR_FINANS (ID);
--INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('ZRF',N'ZRODLO 1 dla projektu mi�dzynarodowego',1,'NM');
--DELETE FROM [DSG_AMS_PROJ_MNAR_ZR_FINANS] WHERE CN = 'ZRF' AND TITLE = N'ZRODLO 1 dla projektu mi�dzynarodowego';
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('AMCS',N'AKCJE Marie Curie Sk�odowskiej',1,'NM');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('DAAD',N'DAAD',1,'NM');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('ERASMUS',N'ERASMUS',1,'NM');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('FNfs',N'Fundusze Norweskie - fundusz stypendialny',1,'NM');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('FN',N'Fundusze Norweskie (inne ni� fundusz stypendialny)',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('LdV',N'Leonardo da Vinci',1,'NM');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POIiS',N'Program Operacyjny Infrastruktura i �rodowisko',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POIG',N'Program Operacyjny Innowacyjna Gospodarka',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POIR',N'Program Operacyjny Inteligentny Rozw�j',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POKL',N'Program Operacyjny Kapita� Ludzki',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POPC',N'Program Operacyjny Polska Cyfrowa',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('POWER',N'Program Operacyjny Wiedza, Edukacja, Rozw�j ',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('PR',N'Program Ramowy',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('SPPW',N'Szwajcarsko-Polski Program Wsp�racy',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('NZnsz',N'INNE �R�D�O (nie zwi�zane z wyjazdami na sta�e zagraniczne)',1,'BC');
INSERT INTO [DSG_AMS_PROJ_MNAR_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('NZsz',N'INNE �R�D�O (zwi�zane z wyjazdami na sta�e zagraniczne)',1,'NM');

CREATE TABLE [DSG_AMS_PROJ_KRAJ_ZR_FINANS](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_PROJ_KRAJ_ZR_FINANS ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_PROJ_KRAJ_ZR_FINANS_INDEX ON DSG_AMS_PROJ_KRAJ_ZR_FINANS (ID);
INSERT INTO DSG_AMS_PROJ_KRAJ_ZR_FINANS ([cn],[title],[available]) VALUES ('ZRF',N'Narodowe Centrum Nauki',1);
INSERT INTO DSG_AMS_PROJ_KRAJ_ZR_FINANS ([cn],[title],[available]) VALUES ('ZRF',N'Narodowe Centrum Badan i Rozwoju,',1);
INSERT INTO DSG_AMS_PROJ_KRAJ_ZR_FINANS ([cn],[title],[available]) VALUES ('ZRF',N'Fundusz Nauki Polskiej',1);
INSERT INTO DSG_AMS_PROJ_KRAJ_ZR_FINANS ([cn],[title],[available]) VALUES ('ZRF',N'Ministerstwo Nauki i Szkolnictwa Wy�szego',1);
--
--CREATE TABLE [DSG_AMS_PR_ZLEC_ZR_FINANS](
--	[id] [bigint] IDENTITY(1,1) NOT NULL,
--
--	[cn] [varchar](50) NULL,
--	[title] [varchar](255) NULL,
--	[centrum] [int] NULL,
--	[refValue] [varchar](20) NULL,
--	[available] [bit] NULL
--);
--ALTER TABLE DSG_AMS_PR_ZLEC_ZR_FINANS ADD PRIMARY KEY (ID);
--CREATE INDEX DSG_AMS_PR_ZLEC_ZR_FINANS_INDEX ON DSG_AMS_PR_ZLEC_ZR_FINANS (ID);
----INSERT INTO [DSG_AMS_PR_ZLEC_ZR_FINANS] ([cn],[title],[available],[refValue]) VALUES ('ZRF',N'ZRODLO 1 dla prac zleconych',1,'NM');

CREATE TABLE dsg_ams_komorka_nadzorujaca_projekty (
	DIVISION_ID numeric(19,0) NOT NULL,
	FOREIGN KEY (DIVISION_ID) REFERENCES DS_DIVISION(ID)
)

CREATE VIEW dsg_ams_komorka_nadzorujaca_projekty_division_view AS
	SELECT
		d.ID AS ID,
		d.GUID AS cn,
		d.NAME AS title,
		null AS refValue,
		null as centrum,
		case d.HIDDEN when 0 then 1 else 0 end as available
	FROM dsg_ams_komorka_nadzorujaca_projekty k
	INNER JOIN DS_DIVISION d on k.DIVISION_ID = d.ID

--TODO DODAC ODPOWIEDNIE DZIALY
--INSERT INTO dsg_ams_komorka_nadzorujaca_projekty VALUES (3);
--INSERT INTO dsg_ams_komorka_nadzorujaca_projekty VALUES (4);

--select * from DS_DIVISION
--select * from dsg_ams_komorka_nadzorujaca_projekty_division_view


CREATE TABLE [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_EFEKTY_PROJEKTU_RODZAJ ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_EFEKTY_PROJEKTU_RODZAJ_INDEX ON DSG_AMS_EFEKTY_PROJEKTU_RODZAJ (ID);

CREATE TABLE [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE_INDEX ON DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE (ID);



CREATE TABLE [DSG_AMS_RODZAJ_PROJ_MNAR](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_RODZAJ_PROJ_MNAR ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_RODZAJ_PROJ_MNAR_INDEX ON DSG_AMS_RODZAJ_PROJ_MNAR (ID);

CREATE TABLE [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN](
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN_INDEX ON DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN (ID);

CREATE TABLE [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] (
	[id] [bigint] IDENTITY(1,1) NOT NULL,

	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
ALTER TABLE DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY ADD PRIMARY KEY (ID);
CREATE INDEX DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY_INDEX ON DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY (ID);




INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR1',N'Rozw�j dyscypliny naukowej',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR2',N'Publikacje',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR3',N'Prototyp',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR4',N'Oprogramowanie',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR5',N'�rodek trwa�y',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_RODZAJ] ([cn],[title],[available]) VALUES ('EPR6',N'Inne',1);

INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE] ([cn],[title],[available]) VALUES ('EPP1',N'Potrzeby w�asne uczelni',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE] ([cn],[title],[available]) VALUES ('EPP2',N'Na sprzeda�',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE] ([cn],[title],[available]) VALUES ('EPP3',N'Wdro�enie',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE] ([cn],[title],[available]) VALUES ('EPP4',N'Przekazanie',1);
INSERT INTO [DSG_AMS_EFEKTY_PROJEKTU_PRZEZNACZENIE] ([cn],[title],[available]) VALUES ('EPP5',N'Inne',1);

INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ1',N'�?r�d�a finansowania zewn. 1',1);
INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ2',N'�?r�d�a finansowania zewn. 2',1);
INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ3',N'�?r�d�a finansowania zewn. 3',1);
INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ4',N'�?r�d�a finansowania zewn. 4',1);
INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ5',N'�?r�d�a finansowania zewn. 5',1);
INSERT INTO [DSG_AMS_ZRODLA_FINANSOWANIA_ZEWN] ([cn],[title],[available]) VALUES ('ZFZ6',N'�?r�d�a finansowania zewn. 6',1);

INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP1','Badania naukowe',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP2','Prace rozwojowe',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP3','Ekspertyza',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP4','Analiza',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP5','Raport',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP6','Studium wykonalno�ci',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP7','Opracowanie techniczne',1);
INSERT INTO [DSG_AMS_PRACE_ZLECONE_CHARAKTER_PRACY] ([cn],[title],[available]) VALUES ('PZCP8','Inne prace badawcze',1);

--INSERT INTO [DSG_AMS_RODZAJ_PROJ_MNAR] ([cn],[title],[available]) VALUES ('NAUKOWY',N'Naukowy',1);
--UPDATE [DSG_AMS_RODZAJ_PROJ_MNAR] SET [AVAILABLE]=0 WHERE [CN]='NAUKOWY'
INSERT INTO [DSG_AMS_RODZAJ_PROJ_MNAR] ([cn],[title],[available]) VALUES ('NAUKBADAW',N'Naukowo-badawczy',1);
INSERT INTO [DSG_AMS_RODZAJ_PROJ_MNAR] ([cn],[title],[available]) VALUES ('EDYKACYJNY',N'Edukacyjny',1);
INSERT INTO [DSG_AMS_RODZAJ_PROJ_MNAR] ([cn],[title],[available]) VALUES ('INWESTYCYJNY',N'Inwestycyjny',1);
