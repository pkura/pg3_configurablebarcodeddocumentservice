CREATE TABLE DSG_AMS_ASSET_LIQUIDATION (
    ID BIGINT IDENTITY(1,1) NOT NULL,

    ERP_ID NUMERIC(18,0) NULL,
    IDM VARCHAR(50) NULL,
    CZY_SPRZEDAZ REAL NULL,
    LIKWIDACJA_NAZWA VARCHAR(100),
	available bit default(1) not null
)

CREATE VIEW dsg_ams_asset_liquidation_view AS
	SELECT
		ID AS id,
		IDM AS cn,
		LIKWIDACJA_NAZWA AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM DSG_AMS_ASSET_LIQUIDATION