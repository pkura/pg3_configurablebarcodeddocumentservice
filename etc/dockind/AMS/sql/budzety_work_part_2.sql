CREATE TABLE [dbo].[dsg_ams_services_unit_budget](
	[id] [numeric](10, 0) IDENTITY(1,1) NOT NULL,
	[idm] [varchar](20) NOT NULL,
	[wersja] [int] NOT NULL,
	[nazwa] [varchar](128) NULL,
	[data_obow_od] [datetime] NULL,
	[data_obow_do] [datetime] NULL,
	[czy_archiwalny] [numeric](1, 0) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_ams_services_unit_budget] ON 

GO
INSERT [dbo].[dsg_ams_services_unit_budget] ([id], [idm], [wersja], [nazwa], [data_obow_od], [data_obow_do], [czy_archiwalny]) VALUES (CAST(1 AS Numeric(10, 0)), N'B/2014/0001', 1, N'Bud�et Uczelni 2014', CAST(0x0000A2A600000000 AS DateTime), CAST(0x0000A41200000000 AS DateTime), CAST(0 AS Numeric(1, 0)))
GO
SET IDENTITY_INSERT [dbo].[dsg_ams_services_unit_budget] OFF
GO


--CREATE TABLE dsg_ams_unit_budget_to_budget(
--	[id] numeric(19,0) identity(1,1),
--	[budzet_id] [numeric](10, 0) NOT NULL,
--	[bd_str_budzet_id] [numeric](10, 0) NOT NULL,
--	[bd_str_budzet_idn] [varchar](15) NOT NULL,
--	[nazwa_komorki_budzetowej] [varchar](40) NULL,
--	[czy_wiodaca] [numeric](1, 0) NOT NULL,
--	[komorka_id] [numeric](10, 0) NOT NULL,
--	[komorka_idn] [char](15) NOT NULL,
--	[nazwa_komorki_organizacyjnej] [varchar](60) NOT NULL
--) ON [PRIMARY]


--insert into dsg_ams_unit_budget_to_budget
--select budzet_id,bd_str_budzet_id,bd_str_budzet_idn,nazwa_komorki_budzetowej,czy_wiodaca,komorka_id,komorka_idn,nazwa_komorki_organizacyjnej from dsg_ams_services_kb2ko

---------
---------

create table dsg_ams_budget_to_division (
	id numeric(19,0) identity(1,1),
	[budzet_id] [numeric](10, 0) NOT NULL,
	[budzet_idm] [varchar](20) NOT NULL,
	[wersja] [int] NOT NULL,
	[nazwa] [varchar](128) NULL,
	[data_obow_od] [datetime] NULL,
	[data_obow_do] [datetime] NULL,
	[czy_archiwalny] [numeric](1, 0) NOT NULL,
	komorka_idn varchar(50) not null,
	komorka_id [numeric](19, 0) NOT NULL,
	division_id [numeric](19, 0) NOT NULL,
	division_guid varchar(50) NOT NULL
	);

insert into dsg_ams_budget_to_division
select 
b.id as [budzet_id], b.idm as [budzet_idm], b.[wersja], b.[nazwa], b.[data_obow_od], b.[data_obow_do], b.[czy_archiwalny], d.CODE as komorka_idn, cast(cast(d.EXTERNALID as float) as bigint) as komorka_id, d.id as division_id, d.guid as division_guid
from dsg_ams_services_unit_budget b
join DS_DIVISION d on externalid is not null


create view dsg_ams_view_budget_phase as (
select
id as id,
budzet_idm as cn,
nazwa as title,
1 as available,
komorka_id as centrum,
division_id as refValue
from dsg_ams_budget_to_division
)


CREATE TABLE [dbo].[dsg_ams_kb2ko](
	id numeric(19,0) identity(1,1),
	[budzet_id] [numeric](10, 0) NOT NULL,
	[bd_str_budzet_id] [numeric](10, 0) NOT NULL,
	[bd_str_budzet_idn] [varchar](15) NOT NULL,
	[nazwa_komorki_budzetowej] [varchar](40) NULL,
	[czy_wiodaca] [numeric](1, 0) NOT NULL,
	[komorka_id] [numeric](10, 0) NOT NULL,
	[komorka_idn] [char](15) NOT NULL,
	[nazwa_komorki_organizacyjnej] [varchar](60) NOT NULL
) ON [PRIMARY]


insert into  [dsg_ams_kb2ko] select
[budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]
from dsg_ams_services_kb2ko

create view dsg_ams_view_budget_unit as (
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
1 as available,
bd_str_budzet_id as centrum,
b.id as refValue
from dsg_ams_budget_to_division b
join [dsg_ams_kb2ko] ko on b.budzet_id=ko.budzet_id and b.komorka_id=ko.komorka_id
)

alter view dsg_ams_view_budget_unit_ko as (
select
budget_ko.id as id,
budget_ko.idm as cn,
budget_ko.nazwa as title,
budget_ko.budzet_id as centrum,
ko.id as refValue,
1-czy_agregat as available
from [dsg_ams_services_unit_budget_ko] budget_ko
join dsg_ams_kb2ko ko on budget_ko.budzet_id=ko.budzet_id and budget_ko.bd_str_budzet_id=ko.bd_str_budzet_id
)



select * from ds_division d
join  dsg_ams_view_budget b on d.id=b.refValue
join dsg_ams_view_budget_unit u on b.id=u.refValue
join dsg_ams_view_budget_unit_ko ko on u.id=ko.refValue


alter view dsg_ams_view_purchase_plan as (
select
p.id as id,
p.idm as cn,
p.nazwa as title,
p.budzet_id as centrum,
ko.id as refValue,
1-czy_agregat as available
from [dsg_ams_services_purchase_plan] p
join dsg_ams_kb2ko ko on p.budzet_id=ko.budzet_id and p.bd_str_budzet_id=ko.bd_str_budzet_id
)

select * from ds_division d
join  dsg_ams_view_budget b on d.id=b.refValue
join dsg_ams_view_budget_unit u on b.id=u.refValue
join dsg_ams_view_purchase_plan p on u.id=p.refValue
order by p.cn

