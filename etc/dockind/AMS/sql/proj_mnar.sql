CREATE TABLE DSG_AMS_PROJ_MNAR (
	[DOCUMENT_ID] [numeric] (18,0) NOT NULL,
	[STATUS] [numeric](18, 0) NULL,
	[TYP_KARTY] [numeric](1, 0) NULL,

	--[TYP_PROJEKTU] [numeric](18, 0) NULL,
	--[NAZWA_PROJEKTU] [varchar](255) NULL,
    [TYTUL_PROJEKTU] [varchar](255) NULL,
	[RODZAJ_PROJEKTU] [numeric](18, 0) NULL,
    [KIEROWNIK_PROJEKTU] [numeric](18, 0) NULL,
    [JEDN_REALIZUJACA] [numeric](18, 0) NULL,
    [KIEROWNIK_PIONU] [numeric](18, 0) NULL,
    [OPIEKUN_PROJEKTU] [numeric] (18,0) NULL,
    [KOMORKA_ORG_AMS_NADZOR] [numeric](18, 0) NULL,
    [JEDN_FINANSUJACA] [varchar](255) NULL,

    [DATA_ROZPOCZECIA] [date] NULL,
    [DATA_ZAKONCZENIA] [date] NULL,

    [ZRODLO_FINANSOWANIA] [numeric](18, 0) NULL,
    [ZRODLO_FINANSOWANIA_ZEWNETRZNEGO] [numeric](18, 0) NULL,
    [FINANSOWANIE_ZEWNETRZNE_PROC_WP] [numeric](18, 4) NULL,

    [KONKURS] [varchar](255) NULL,
    [WALUTA] [numeric](18, 0) NULL,
    [KOSZTY_BEZPOSREDNIE] [numeric](18, 2) NULL,
    [KOSZTY_BEZPOSREDNIE_PROC] [numeric](18, 4) NULL,
    [KOSZTY_POSREDNIE] [numeric](18, 2) NULL,
    [KOSZTY_POSREDNIE_PROC_KB] [numeric](18, 4) NULL,
    [LACZNA_WARTOSC_PROJEKTU] [numeric](18, 2) NULL,
    [WKLAD_WLASNY_AM_PROC_WP] [numeric](18, 4) NULL,
    [WKLAD_WLASNY_WARTOSC]  [numeric](18, 2) NULL,

    [KWALIFIKOWANY_VAT] [numeric](18, 0) NULL,
    [PREFINANSOWANIE] [numeric](18, 0) NULL,
    [PREFINANSOWANIE_WARTOSC] [numeric](18, 2) NULL,

    [DYSPONENCI_SUMA_WKLADU_WLASNEGO]  [numeric](18, 2) NULL,
    [DYSPONENCI_SUMA_PREFINANSOWANIA]  [numeric](18, 2) NULL,

    [CEL_PROJEKTU] [varchar](500) NULL,
    [SPOSOB_REALIZACJI_PROJEKTU] [numeric](18, 0) NULL,
    [ROLA_AM_W_PARTNERSTWIE_TYP] [numeric](18, 0) NULL,
    [PARTNERZY] [varchar](255) NULL,
    [POTRZEBY_TECH_LOK] [varchar](255) NULL,
    [ZALACZNIK_EXCEL] [numeric](18, 0) NULL,
    [ZALACZNIK_RZETELNOSC_KALK] [numeric](18, 0) NULL,
    [OSWIAD_O_RZETELNOSCI] [SMALLINT] NULL,


    [OPINIA_CTTM] [varchar](500) NULL,
    [OPINIA_NM] [varchar](500) NULL,
    [OPINIA_JEDN_REALIZUJACEJ] [varchar](500) NULL,
    [OPINIA_KIER_PIONU] [varchar](500) NULL,
    [OPINIA_KWESTORA] [varchar](500) NULL,
    [OPINIA_KANCLERZA] [varchar](500) NULL,

    [OSOBA_ZATWIERDZAJACA] [numeric](18, 0) NULL,

    [OPINIA_JM_REKTORA] [varchar](500) NULL,
    [OPINIA_PROREKTORA_DS_NAUKI] [varchar](500) NULL,
    [OPINIA_PROREKTORA_DS_NAUCZANIA] [varchar](500) NULL,

    [DATA_ZLOZENIA_KARTY_OBIEGOWEJ] [date] NULL,


    PRIMARY KEY CLUSTERED ( [DOCUMENT_ID] ASC )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [DSG_AMS_PROJ_MNAR_MULTIPLE_VALUE] (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);


--alter table [DSG_AMS_PROJ_MNAR] add [OSWIAD_O_RZETELNOSCI] [SMALLINT] NULL;
--alter table [DSG_AMS_PROJ_MNAR] add [WALUTA] [numeric](18, 0) NULL;
--2014.03.25
--alter table [DSG_AMS_PROJ_MNAR] drop column [TYP_PROJEKTU];
--alter table [DSG_AMS_PROJ_MNAR] add [PARTNERZY] [varchar](255) NULL;
--alter table [DSG_AMS_PROJ_MNAR] add [KOSZTY_POSREDNIE] [numeric](18, 2) NULL;
--2014.04.07
--alter table [DSG_AMS_PROJ_MNAR] drop column [NAZWA_PROJEKTU];
--alter table [DSG_AMS_PROJ_MNAR] add [TYP_KARTY] [numeric](1, 0) NULL;
--2014.04.08
--alter table [DSG_AMS_PROJ_MNAR] drop column [ZRODLO_FINANSOWANIA];
--alter table DSG_AMS_PROJ_MNAR alter column [FINANSOWANIE_ZEWNETRZNE_PROC_WP] [numeric](18, 4) NULL;
--alter table DSG_AMS_PROJ_MNAR alter column [KOSZTY_BEZPOSREDNIE_PROC] [numeric](18, 4) NULL;
--alter table DSG_AMS_PROJ_MNAR alter column [KOSZTY_POSREDNIE_PROC_KB] [numeric](18, 4) NULL;
--alter table DSG_AMS_PROJ_MNAR alter column [WKLAD_WLASNY_AM_PROC_WP] [numeric](18, 4) NULL;
--2014.04.22
--alter table DSG_AMS_PROJ_MNAR add [OPINIA_KANCLERZA] [varchar](500) NULL;
