CREATE TABLE dsg_ams_contract (
	id numeric(18, 0) identity(1,1) NOT NULL,
	identyfikator_num numeric(18, 2),
	idm varchar(50),
	nazwa varchar(150),
	p_datako date,
	p_datapo date,
	typ_kontrakt_id numeric(18, 2),
	typ_kontrakt_idn varchar(50)
)

create view dsg_ams_view_contract as
	select
		id,
		idm as cn,
		nazwa as title,
		NULL as centrum,
		NULL as refValue,
		1 as available
	from dsg_ams_contract
	
	

ALTER TABLE dsg_ams_contract ADD erpId numeric(18, 2)
UPDATE dsg_ams_contract set erpId = 0

drop view dsg_ams_view_contract

create view dsg_ams_view_contract as
	select
		id,
		idm as cn,
		nazwa as title,
		erpId as centrum,
		NULL as refValue,
		1 as available
	from dsg_ams_contract

ALTER view [dbo].[dsg_ams_view_contract] as
	select
		id,
		LTRIM(RTRIM(idm)) as cn,
		LTRIM(RTRIM(idm)) + ' ' + LTRIM(RTRIM(nazwa)) AS title,
		NULL as centrum,
		NULL as refValue,
		1 as available
	from dsg_ams_contract
GO

