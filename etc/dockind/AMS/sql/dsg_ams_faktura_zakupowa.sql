
CREATE TABLE dsg_ams_faktura_zakupowa (
	[document_id] numeric (18,0) IDENTITY(1,1) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[typ_faktury] [numeric](18, 0) NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wplywu_faktury] [date] NULL,
	[data_wystawienia] [date] NULL,
	[termin_platnosci] [date] NULL,
	[waluta] [numeric](18, 0) NULL,
	[kwota_netto] [numeric](18, 2) NULL,
	[kwota_brutto] [numeric](18, 2) NULL,
	[kwota_vat] [numeric](18, 2) NULL,
	[sposob] [numeric](18, 0) NULL,
	[opis] [varchar](4096) NULL,
	[OSOBA_MERYTORYCZNA] [numeric](18, 0) NULL,
	[NR_UMOWY] [varchar](30) NULL,
	[F_PROFORMA] [numeric](18, 0) NULL,
	[OKRES_FAKTURY] [varchar](30) NULL,
	[ZARZAD] [numeric](18, 0) NULL
);


ALTER TABLE dsg_ams_faktura_zakupowa ADD PRIMARY KEY (document_id);
create index dsg_ams_faktura_zakupowa_index on dsg_ams_faktura_zakupowa (document_id);


alter table dsg_ams_faktura_zakupowa add MPK_CORRECTION smallint;
alter table dsg_ams_faktura_zakupowa add HIDE_MPK smallint;

alter table dsg_ams_faktura_zakupowa add czy_zaliczka smallint;
alter table dsg_ams_faktura_zakupowa add zaliczka numeric (18,2);

alter table dsg_ams_faktura_zakupowa add czy_kara smallint;
alter table dsg_ams_faktura_zakupowa add czy_potracona smallint;

alter table dsg_ams_faktura_zakupowa add kwota_kary numeric(18,2);
alter table dsg_ams_faktura_zakupowa add opis_kary varchar(500);

alter table dsg_ams_faktura_zakupowa add rodzaj_faktury numeric(18,0);

alter table dsg_ams_faktura_zakupowa add sposob_zaliczki numeric(18,0);

alter table dsg_ams_faktura_zakupowa add nr_zamowienia varchar(30);

ALTER TABLE dsg_ams_faktura_zakupowa add MAGAZYN numeric(18, 0);

ALTER TABLE dsg_ams_faktura_zakupowa ADD CZY_KWESTURA BIT;

ALTER TABLE dsg_ams_faktura_zakupowa ADD ZAMOWIENIE numeric(18, 0);
ALTER TABLE dsg_ams_faktura_zakupowa ADD ZALICZKA_DICT numeric(18, 0);
ALTER TABLE dsg_ams_faktura_zakupowa ADD CZY_RATY BIT;
ALTER TABLE dsg_ams_faktura_zakupowa ADD OPIS_RATY varchar(4096);
ALTER TABLE dsg_ams_faktura_zakupowa ADD KWOTA_UZNANIA numeric(18, 2);

ALTER TABLE dsg_ams_faktura_zakupowa ADD HIDE_MPK_DO_WYJ BIT;
ALTER TABLE dsg_ams_faktura_zakupowa ADD SHOW_STAWKI_VAT BIT;
ALTER TABLE dsg_ams_faktura_zakupowa ADD SHOW_RACHUNKI BIT;

ALTER TABLE dsg_ams_faktura_zakupowa ADD BRAK_WNIOSKU BIT;
ALTER TABLE dsg_ams_faktura_zakupowa ADD OPIS_BRAK_WNIOSKU VARCHAR(4096);
ALTER TABLE dsg_ams_faktura_zakupowa ADD MPK_DO_WYJ_SUM_AMOUNT numeric(18, 2);
ALTER TABLE dsg_ams_faktura_zakupowa ADD MPK_SUM_AMOUNT numeric(18, 2);

ALTER TABLE dsg_ams_faktura_zakupowa ADD SUMA_RAT numeric(18, 2);
ALTER TABLE dsg_ams_faktura_zakupowa DROP COLUMN CZY_KWESTURA;
ALTER TABLE dsg_ams_faktura_zakupowa DROP COLUMN SHOW_STAWKI_VAT;
ALTER TABLE dsg_ams_faktura_zakupowa ADD VAT_FILE numeric(18, 0);

alter table dsg_ams_faktura_zakupowa add supplier numeric(18, 0)
alter table dsg_ams_faktura_zakupowa add dostawca_nr_konta_id numeric(18, 0)
alter table dsg_ams_faktura_zakupowa add nr_konta_nowy varchar(50)

ALTER TABLE dsg_ams_faktura_zakupowa ADD dysponent numeric(18, 0)
ALTER TABLE dsg_ams_faktura_zakupowa ADD barcode numeric(18, 0)
ALTER TABLE dsg_ams_faktura_zakupowa ADD kurs numeric(18, 2);
ALTER TABLE dsg_ams_faktura_zakupowa ADD show_kurs bit;

ALTER TABLE dsg_ams_faktura_zakupowa ADD dekrety_sum_amount numeric(18, 2);

ALTER TABLE dsg_ams_faktura_zakupowa ADD NR_ZAMOWIENIA_FROM_ERP numeric(18, 0);
ALTER TABLE dsg_ams_faktura_zakupowa ADD POZYCJE_ZAMOWIENIA numeric(18, 0);

ALTER TABLE dsg_ams_faktura_zakupowa ADD dekrety_kwota_do_rozpisania numeric(18, 2);

ALTER TABLE dsg_ams_faktura_zakupowa ADD vat_ratio numeric(18, 0);
ALTER TABLE dsg_ams_faktura_zakupowa DROP COLUMN dekrety_kwota_do_rozpisania;
ALTER TABLE dsg_ams_faktura_zakupowa ADD mpk_kwota_do_rozpisania numeric(18, 2);

ALTER TABLE dsg_ams_faktura_zakupowa ADD vat_do_odliczenia_sum numeric(18, 2);
ALTER TABLE dsg_ams_faktura_zakupowa ADD vat_koszty_sum numeric(18, 2);
ALTER TABLE dsg_ams_faktura_zakupowa ADD data_wykonania_uslugi date;
ALTER TABLE dsg_ams_faktura_zakupowa ADD show_add_fields bit;

ALTER TABLE dsg_ams_faktura_zakupowa ALTER COLUMN kurs numeric(18, 4)

ALTER TABLE dsg_ams_faktura_zakupowa ADD czy_miedzynarodowy BIT;