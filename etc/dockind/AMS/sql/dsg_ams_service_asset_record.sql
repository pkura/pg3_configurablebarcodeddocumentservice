CREATE TABLE dsg_ams_asset_rec_sys_op_type (
    ID BIGINT IDENTITY(1,1) NOT NULL,

    ERP_ID REAL NULL,
    TYP_OPE_ID REAL NULL,
    TYP_OPE_IDN varchar(50) NULL,
    TYP_SYSEWI_ID REAL NULL,
    TYP_SYSEWI_IDN varchar(50) NULL,
    available bit default(1) not null
)

CREATE VIEW dsg_ams_asset_rec_sys_op_type_view AS
	SELECT
		ID AS id,
		TYP_OPE_IDN AS cn,
		TYP_SYSEWI_IDN AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM dsg_ams_asset_rec_sys_op_type