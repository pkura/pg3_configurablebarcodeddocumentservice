CREATE TABLE dsg_ams_contract_bogdet (
	id numeric(18, 0) identity(1,1) NOT NULL,
	id_from_webservice numeric(18, 2),
	idm varchar(50),
	kontrakt_id numeric(18, 2),
	nazwa varchar(150),
	p_datako date,
	p_datapo date,
	stan numeric(18, 0),
	status numeric(18, 0)
)

create view dsg_ams_view_contract_bogdet as
	select
		id,
		idm as cn,
		nazwa as title,
		NULL as centrum,
		NULL as refValue,
		1 as available
	from dsg_ams_contract_bogdet