ALTER view [dbo].[dsg_ams_view_budget_position] as
	SELECT distinct 
		convert(numeric(18, 0), bd_budzet_rodzaj_id) AS id,
		convert(varchar(50), bd_budzet_rodzaj_id) AS cn,
		LTRIM(RTRIM(wytwor_idm)) + ' ' + dok_poz_nazwa + ' (' + CASE WHEN ISNULL(dok_poz_p_kosz,0) - ISNULL(dok_poz_rez_koszt,0) - ISNULL(dok_poz_r_koszt,0)<=0 THEN 'brak wolnych �rodk�w' ELSE 'pozosta�y limit (kwota): ' + CONVERT(varchar(50),ISNULL(dok_poz_p_kosz,0) - ISNULL(dok_poz_rez_koszt,0) - ISNULL(dok_poz_r_koszt,0)) END + ')'
		AS title,
		NULL AS centrum,
		convert(varchar(20), convert(numeric(18, 0), bd_budzet_id)) AS refValue,
		1 AS available
	FROM dsg_ams_budget_copy  where bd_rodzaj = 'BK' or bd_rodzaj = 'PZB';

GO