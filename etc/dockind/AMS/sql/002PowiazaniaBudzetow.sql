ALTER VIEW dsg_ams_view_budget AS
	SELECT * FROM (
			SELECT 
				convert(numeric(18, 0), bd_budzet_id) AS id,
				bd_budzet_idm AS cn,
				budzet_nazwa AS title,
				ROW_NUMBER() OVER (PARTITION BY bd_budzet_id ORDER BY bd_budzet_id) AS centrum,
				CASE
					WHEN bd_rodzaj = 'BK'
						--THEN 5
						THEN convert(numeric(18, 0), bd_budzet_id)+50000000
					WHEN bd_rodzaj = 'PZB'
						--THEN 10
						THEN convert(numeric(18, 0), '10000000'+bd_budzet_id)
					WHEN bd_rodzaj != 'BK' AND bd_rodzaj != 'PZB'
						THEN convert(numeric(18, 0),bd_budzet_id)
				END AS refValue,
				available
			FROM dsg_ams_budget where bd_rodzaj = 'BK' or bd_rodzaj = 'PZB'
	)a WHERE a.centrum = 1;

ALTER VIEW dsg_ams_view_budget_cell AS
	SELECT * FROM (
		SELECT
			convert(numeric(18, 0),bd_budzet_id) AS id,
			dok_bd_str_budzet_idn AS cn,
			dok_kom_nazwa AS title,
			ROW_NUMBER() OVER (PARTITION BY dok_bd_str_budzet_idn ORDER BY dok_bd_str_budzet_idn) AS centrum,
			convert(numeric(18, 0),bd_budzet_id) AS refValue,
			available
		FROM dsg_ams_budget where bd_rodzaj = 'BK' or bd_rodzaj = 'PZB'
	)a WHERE a.centrum = 1;

 create table DSG_AMS_B_KIND 
  (
	id integer,
	name  varchar(10),
	cn varchar (10)
  );
  insert into DSG_AMS_B_KIND (id,name,cn) values (10000000, 'PZB', 'Plan zakup�w'); 
  insert into DSG_AMS_B_KIND (id,name,cn) values (50000000, 'BK', 'Bud�et koszt�w'); 

  
 ALTER VIEW dsg_ams_view_budget_kind AS
  select 
  convert(numeric(18, 0), b.refValue) + kind.id as ID,
  kind.cn as CN,
  name as title,
  b.centrum as centrum,
  b.id as refValue,
  b.available as available
  from dsg_ams_view_budget_cell b, DSG_AMS_B_KIND kind ;