CREATE TABLE dsg_ams_product (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 2),
	idm varchar(50),
	jm_idn varchar(50),
	klaswytw_idn varchar(50),
	nazwa varchar(150),
	rodztowaru numeric(18, 0),
	vatstaw_ids varchar(50)
)

ALTER VIEW [dbo].[dsg_ams_view_product] 
with schemabinding
AS
	SELECT 
	 id,
	 LTRIM(RTRIM(idm)) AS cn,
	 LTRIM(RTRIM(idm)) + ' ' + LTRIM(RTRIM(nazwa)) AS title,
	 NULL AS centrum,
	 LTRIM(RTRIM(jm_idn)) AS refValue,
	 1 AS available
	FROM dbo.dsg_ams_product;
