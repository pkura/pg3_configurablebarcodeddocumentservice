CREATE TABLE dsg_ams_purchase_document_type (
	id numeric(18, 0) identity(1,1) NOT NULL,
	idm varchar(50),
	name varchar(150),
	currency_symbol varchar(10),
	document_type_ids varchar(50)
)


CREATE VIEW dsg_ams_view_purchase_document_type AS
	SELECT
		id,
		idm AS cn,
		name AS title,
		NULL AS centrum,
		NULL AS refValue,
		1 AS available
	FROM dsg_ams_purchase_document_type
	
	
alter table dsg_ams_purchase_document_type add czy_aktywny numeric(18, 2);
alter table dsg_ams_purchase_document_type add czywal numeric(18, 2);
alter table dsg_ams_purchase_document_type add available bit;

update dsg_ams_purchase_document_type set czy_aktywny = 1.0
update dsg_ams_purchase_document_type set czywal = 0.0
update dsg_ams_purchase_document_type set available = 1

drop view dsg_ams_view_purchase_document_type;

ALTER TABLE dsg_ams_purchase_document_type ADD erpId numeric(18, 2);

ALTER VIEW [dbo].[dsg_ams_view_purchase_document_type] AS 
	SELECT
		id,
		idm AS cn,
		name AS title,
		convert(bit, czywal) AS centrum,
		CAST(erpId as int) AS refValue,
		available
	FROM dsg_ams_purchase_document_type
GO