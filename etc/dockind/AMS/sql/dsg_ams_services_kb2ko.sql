SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_ams_services_kb2ko](
	[budzet_id] [numeric](10, 0) NOT NULL,
	[bd_str_budzet_id] [numeric](10, 0) NOT NULL,
	[bd_str_budzet_idn] [varchar](15) NOT NULL,
	[nazwa_komorki_budzetowej] [varchar](40) NULL,
	[czy_wiodaca] [numeric](1, 0) NOT NULL,
	[komorka_id] [numeric](10, 0) NOT NULL,
	[komorka_idn] [char](15) NOT NULL,
	[nazwa_komorki_organizacyjnej] [varchar](60) NOT NULL
) ON [PRIMARY]
CREATE INDEX dsg_ams_services_kb2ko_I1 on dsg_ams_services_kb2ko(bd_str_budzet_idn,komorka_idn);

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(4 AS Numeric(10, 0)), N'RPR', N'Rzecznik Prasowy', CAST(1 AS Numeric(1, 0)), CAST(5 AS Numeric(10, 0)), N'01002000       ', N'Rzecznik Prasowy')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(5 AS Numeric(10, 0)), N'ROP', N'Dział Organizacyjno-Prawny', CAST(1 AS Numeric(1, 0)), CAST(6 AS Numeric(10, 0)), N'01003000       ', N'Dział Organizacyjno-Prawny ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(6 AS Numeric(10, 0)), N'RP', N'Dział Promocji', CAST(1 AS Numeric(1, 0)), CAST(7 AS Numeric(10, 0)), N'01004000       ', N'Dział Promocji ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(7 AS Numeric(10, 0)), N'RK', N'Dział Kadr', CAST(1 AS Numeric(1, 0)), CAST(8 AS Numeric(10, 0)), N'01005000       ', N'Dział Kadr ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(8 AS Numeric(10, 0)), N'RA', N'Samodzielne Stanowisko ds. Audytu Wewnęt', CAST(1 AS Numeric(1, 0)), CAST(9 AS Numeric(10, 0)), N'01006000       ', N'Samodzielne Stanowisko ds. Audytu Wewnętrznego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(12 AS Numeric(10, 0)), N'RB', N'Pion Prorektora ds. Nauki', CAST(1 AS Numeric(1, 0)), CAST(13 AS Numeric(10, 0)), N'02050000       ', N'Pion Prorektora ds. Nauki ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(13 AS Numeric(10, 0)), N'BB', N'Dział Nauki', CAST(1 AS Numeric(1, 0)), CAST(14 AS Numeric(10, 0)), N'02051000       ', N'Dział Nauki ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(14 AS Numeric(10, 0)), N'BI', N'Uczelniane Centrum Informatyczne', CAST(1 AS Numeric(1, 0)), CAST(15 AS Numeric(10, 0)), N'02052000       ', N'Uczelniane Centrum Informatyczne ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(15 AS Numeric(10, 0)), N'BZB', N'Dział Zamówień Publicznych', CAST(1 AS Numeric(1, 0)), CAST(16 AS Numeric(10, 0)), N'02053000       ', N'Dział Zamówień Publicznych ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(16 AS Numeric(10, 0)), N'BA', N'Sekcja Aparatury', CAST(1 AS Numeric(1, 0)), CAST(17 AS Numeric(10, 0)), N'02054000       ', N'Sekcja Aparatury ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(17 AS Numeric(10, 0)), N'BW', N'Dział Wydawnictw', CAST(1 AS Numeric(1, 0)), CAST(18 AS Numeric(10, 0)), N'02055000       ', N'Dział Wydawnictw ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(18 AS Numeric(10, 0)), N'BC', N'Centrum Transferu Technologii Morskich', CAST(1 AS Numeric(1, 0)), CAST(19 AS Numeric(10, 0)), N'02056000       ', N'Centrum Transferu Technologii Morskich ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(19 AS Numeric(10, 0)), N'WN', N'Wydawnictwo Naukowe', CAST(1 AS Numeric(1, 0)), CAST(20 AS Numeric(10, 0)), N'02057000       ', N'Wydawnictwo Naukowe ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(21 AS Numeric(10, 0)), N'SIG', N'Sekcja Informacji Naukowej', CAST(1 AS Numeric(1, 0)), CAST(22 AS Numeric(10, 0)), N'02058011       ', N'Sekcja Informacji Naukowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(22 AS Numeric(10, 0)), N'SGiOZ', N'Sekcja Gromadzenia i Opracowania Zbiorów', CAST(1 AS Numeric(1, 0)), CAST(23 AS Numeric(10, 0)), N'02058012       ', N'Sekcja Gromadzenia i Opracowania Zbiorów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(23 AS Numeric(10, 0)), N'SUZ', N'Sekcja Udostępniania Zbiorów', CAST(1 AS Numeric(1, 0)), CAST(24 AS Numeric(10, 0)), N'02058013       ', N'Sekcja Udostępniania Zbiorów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(24 AS Numeric(10, 0)), N'SC', N'Sekcja Czytelń', CAST(1 AS Numeric(1, 0)), CAST(25 AS Numeric(10, 0)), N'02058014       ', N'Sekcja Czytelń')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(25 AS Numeric(10, 0)), N'AZ', N'Archiwum Zakładowe', CAST(1 AS Numeric(1, 0)), CAST(26 AS Numeric(10, 0)), N'02058015       ', N'Archiwum Zakładowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(26 AS Numeric(10, 0)), N'FWZZ', N'Fundusz Współpracy z Zagranicą', CAST(1 AS Numeric(1, 0)), CAST(27 AS Numeric(10, 0)), N'02059000       ', N'Fundusz Współpracy z Zagranicą')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(29 AS Numeric(10, 0)), N'NB', N'Biuro Personalizacji Danych Studenta', CAST(1 AS Numeric(1, 0)), CAST(30 AS Numeric(10, 0)), N'03101051       ', N'Biuro Personalizacji Danych Studenta ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(30 AS Numeric(10, 0)), N'ZJISO', N'Zarządzanie jakością ISO', CAST(1 AS Numeric(1, 0)), CAST(31 AS Numeric(10, 0)), N'03101052       ', N'Zarządzanie jakością ISO')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(31 AS Numeric(10, 0)), N'AUD', N'Audity', CAST(1 AS Numeric(1, 0)), CAST(32 AS Numeric(10, 0)), N'03101053       ', N'Audity')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(32 AS Numeric(10, 0)), N'NM', N'Dział ds.. Obcokrajowców i Wymiany Międz', CAST(1 AS Numeric(1, 0)), CAST(33 AS Numeric(10, 0)), N'03102000       ', N'Dział ds.. Obcokrajowców i Wymiany Międzynarodowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(33 AS Numeric(10, 0)), N'CKU', N'Centrum Kształcenia Ustawicznego', CAST(1 AS Numeric(1, 0)), CAST(34 AS Numeric(10, 0)), N'03103000       ', N'Centrum Kształcenia Ustawicznego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(34 AS Numeric(10, 0)), N'SNJO', N'Studium Nauki Języków Obcych', CAST(1 AS Numeric(1, 0)), CAST(35 AS Numeric(10, 0)), N'03104000       ', N'Studium Nauki Języków Obcych ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(36 AS Numeric(10, 0)), N'RAT', N'Ratownictwo', CAST(1 AS Numeric(1, 0)), CAST(37 AS Numeric(10, 0)), N'03105061       ', N'Ratownictwo')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(37 AS Numeric(10, 0)), N'SPK', N'Sekcja Piłki Koszykowej', CAST(1 AS Numeric(1, 0)), CAST(38 AS Numeric(10, 0)), N'03105062       ', N'Sekcja Piłki Koszykowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(38 AS Numeric(10, 0)), N'SPS', N'Sekcja Płki Siatkowej', CAST(1 AS Numeric(1, 0)), CAST(39 AS Numeric(10, 0)), N'03105063       ', N'Sekcja Płki Siatkowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(39 AS Numeric(10, 0)), N'SPN', N'Sekcja Piłku nożnej', CAST(1 AS Numeric(1, 0)), CAST(40 AS Numeric(10, 0)), N'03105064       ', N'Sekcja Piłku nożnej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(40 AS Numeric(10, 0)), N'SW', N'Sekcja Wioślarska', CAST(1 AS Numeric(1, 0)), CAST(41 AS Numeric(10, 0)), N'03105065       ', N'Sekcja Wioślarska')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(41 AS Numeric(10, 0)), N'SP', N'Sekcja Pływacka', CAST(1 AS Numeric(1, 0)), CAST(42 AS Numeric(10, 0)), N'03105066       ', N'Sekcja Pływacka')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(42 AS Numeric(10, 0)), N'SKULT', N'Sekcja Kulturystyczna', CAST(1 AS Numeric(1, 0)), CAST(43 AS Numeric(10, 0)), N'03105067       ', N'Sekcja Kulturystyczna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(43 AS Numeric(10, 0)), N'SK', N'Sekcja Karate', CAST(1 AS Numeric(1, 0)), CAST(44 AS Numeric(10, 0)), N'03105068       ', N'Sekcja Karate')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(44 AS Numeric(10, 0)), N'SM', N'Sporty Masowe', CAST(1 AS Numeric(1, 0)), CAST(45 AS Numeric(10, 0)), N'03105069       ', N'Sporty Masowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(45 AS Numeric(10, 0)), N'SŻ', N'Sekcja Żeglarska', CAST(1 AS Numeric(1, 0)), CAST(46 AS Numeric(10, 0)), N'03105070       ', N'Sekcja Żeglarska')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(46 AS Numeric(10, 0)), N'SN', N'Sekcja Narciarska', CAST(1 AS Numeric(1, 0)), CAST(47 AS Numeric(10, 0)), N'03105071       ', N'Sekcja Narciarska')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(47 AS Numeric(10, 0)), N'SSZ', N'Sekcja Szalupingu', CAST(1 AS Numeric(1, 0)), CAST(48 AS Numeric(10, 0)), N'03105072       ', N'Sekcja Szalupingu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(48 AS Numeric(10, 0)), N'UDNS', N'Upominki dla najlepszych sportowców', CAST(1 AS Numeric(1, 0)), CAST(49 AS Numeric(10, 0)), N'03105073       ', N'Upominki dla najlepszych sportowców')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(49 AS Numeric(10, 0)), N'IS', N'Inne sekcje', CAST(1 AS Numeric(1, 0)), CAST(50 AS Numeric(10, 0)), N'03105074       ', N'Inne sekcje')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(51 AS Numeric(10, 0)), N'DSM', N'Dział spraw młodzieży', CAST(1 AS Numeric(1, 0)), CAST(52 AS Numeric(10, 0)), N'03106081       ', N'Dział spraw młodzieży')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(52 AS Numeric(10, 0)), N'CAM', N'Chór Akademii Morskiej', CAST(1 AS Numeric(1, 0)), CAST(53 AS Numeric(10, 0)), N'03106082       ', N'Chór Akademii Morskiej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(53 AS Numeric(10, 0)), N'KPM', N'Klub pod masztami', CAST(1 AS Numeric(1, 0)), CAST(54 AS Numeric(10, 0)), N'03106083       ', N'Klub pod masztami')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(54 AS Numeric(10, 0)), N'SKN', N'Studenckie koła naukowe', CAST(1 AS Numeric(1, 0)), CAST(55 AS Numeric(10, 0)), N'03106084       ', N'Studenckie koła naukowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(57 AS Numeric(10, 0)), N'MS', N'Sekcja Eksploatacji Statku', CAST(1 AS Numeric(1, 0)), CAST(58 AS Numeric(10, 0)), N'04151101       ', N'Sekcja Eksploatacji Statku ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(58 AS Numeric(10, 0)), N'MK', N'Biuro Karier', CAST(1 AS Numeric(1, 0)), CAST(59 AS Numeric(10, 0)), N'04151102       ', N'Biuro Karier ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(59 AS Numeric(10, 0)), N'NAWIGATOR', N'Statek „Nawigator XXI”', CAST(1 AS Numeric(1, 0)), CAST(60 AS Numeric(10, 0)), N'04152000       ', N'Statek „Nawigator XXI”')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(60 AS Numeric(10, 0)), N'CŻŚ', N'Centrum Żeglugi Śródlądowej', CAST(1 AS Numeric(1, 0)), CAST(61 AS Numeric(10, 0)), N'04153000       ', N'Centrum Żeglugi Śródlądowej ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(62 AS Numeric(10, 0)), N'KDP', N'Kursy DP', CAST(1 AS Numeric(1, 0)), CAST(63 AS Numeric(10, 0)), N'04154105       ', N'Kursy DP')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(64 AS Numeric(10, 0)), N'KKN', N'Koszty kursów - O% narzutu', CAST(1 AS Numeric(1, 0)), CAST(65 AS Numeric(10, 0)), N'04155110       ', N'Koszty kursów - O% narzutu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(65 AS Numeric(10, 0)), N'MOS', N'Morski Ośrodek Szkoleniowy', CAST(1 AS Numeric(1, 0)), CAST(66 AS Numeric(10, 0)), N'04156000       ', N'Morski Ośrodek Szkoleniowy')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(66 AS Numeric(10, 0)), N'MOSK', N'Morski Ośrodek Szkoleniowy w Kołobrzegu', CAST(1 AS Numeric(1, 0)), CAST(67 AS Numeric(10, 0)), N'04157000       ', N'Morski Ośrodek Szkoleniowy w Kołobrzegu ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(67 AS Numeric(10, 0)), N'OSRB', N'Ośrodek Szkoleniowy Rybołówstwa Bałtycki', CAST(1 AS Numeric(1, 0)), CAST(68 AS Numeric(10, 0)), N'04158000       ', N'Ośrodek Szkoleniowy Rybołówstwa Bałtyckiego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(68 AS Numeric(10, 0)), N'MEC', N'Maritime English Center', CAST(1 AS Numeric(1, 0)), CAST(69 AS Numeric(10, 0)), N'04159000       ', N'Maritime English Center ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(70 AS Numeric(10, 0)), N'AS', N'Sekretariat Kanclerza', CAST(1 AS Numeric(1, 0)), CAST(71 AS Numeric(10, 0)), N'05201000       ', N'Sekretariat Kanclerza ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(71 AS Numeric(10, 0)), N'A-2', N'Zastępca Kanclerza ds. Rozwoju', CAST(1 AS Numeric(1, 0)), CAST(72 AS Numeric(10, 0)), N'05202000       ', N'Zastępca Kanclerza ds. Rozwoju ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(72 AS Numeric(10, 0)), N'AR', N'Dział Inwestycji i Rozwoju', CAST(1 AS Numeric(1, 0)), CAST(73 AS Numeric(10, 0)), N'05202151       ', N'Dział Inwestycji i Rozwoju ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(73 AS Numeric(10, 0)), N'AT', N'Dział Techniczny', CAST(1 AS Numeric(1, 0)), CAST(74 AS Numeric(10, 0)), N'05202152       ', N'Dział Techniczny')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(74 AS Numeric(10, 0)), N'GK', N'Grupa konserwacyjna', CAST(1 AS Numeric(1, 0)), CAST(75 AS Numeric(10, 0)), N'05202153       ', N'Grupa konserwacyjna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(76 AS Numeric(10, 0)), N'RNJZK', N'Remonty-nabrzeże Jana z Kolna', CAST(1 AS Numeric(1, 0)), CAST(77 AS Numeric(10, 0)), N'05203161       ', N'Remonty-nabrzeże Jana z Kolna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(77 AS Numeric(10, 0)), N'RBOSRM', N'Remonty-budynek OSRM', CAST(1 AS Numeric(1, 0)), CAST(78 AS Numeric(10, 0)), N'05203162       ', N'Remonty-budynek OSRM')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(78 AS Numeric(10, 0)), N'RNOSRM', N'Remonty-nabrzeże OSRM', CAST(1 AS Numeric(1, 0)), CAST(79 AS Numeric(10, 0)), N'05203163       ', N'Remonty-nabrzeże OSRM')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(79 AS Numeric(10, 0)), N'RMOS', N'Remonty-MOS', CAST(1 AS Numeric(1, 0)), CAST(80 AS Numeric(10, 0)), N'05203164       ', N'Remonty-MOS')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(80 AS Numeric(10, 0)), N'RMOSK', N'Remonty-MOSK', CAST(1 AS Numeric(1, 0)), CAST(81 AS Numeric(10, 0)), N'05203165       ', N'Remonty-MOSK')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(81 AS Numeric(10, 0)), N'ROSRB', N'Remonty-OSRB', CAST(1 AS Numeric(1, 0)), CAST(82 AS Numeric(10, 0)), N'05203166       ', N'Remonty-OSRB')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(82 AS Numeric(10, 0)), N'RWCh1', N'Remonty-Wały Chrobrego 1', CAST(1 AS Numeric(1, 0)), CAST(83 AS Numeric(10, 0)), N'05203167       ', N'Remonty-Wały Chrobrego 1')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(83 AS Numeric(10, 0)), N'RWCH2', N'Remonty-Wały Chrobrego 2', CAST(1 AS Numeric(1, 0)), CAST(84 AS Numeric(10, 0)), N'05203168       ', N'Remonty-Wały Chrobrego 2')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(84 AS Numeric(10, 0)), N'RWCHSG', N'Remonty-Wały Chrobrego-sala gimnastyczna', CAST(1 AS Numeric(1, 0)), CAST(85 AS Numeric(10, 0)), N'05203169       ', N'Remonty-Wały Chrobrego-sala gimnastyczna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(85 AS Numeric(10, 0)), N'RPOD', N'Remonty-Podgórna', CAST(1 AS Numeric(1, 0)), CAST(86 AS Numeric(10, 0)), N'05203170       ', N'Remonty-Podgórna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(86 AS Numeric(10, 0)), N'RŻ', N'Remonty-Żołnierska', CAST(1 AS Numeric(1, 0)), CAST(87 AS Numeric(10, 0)), N'05203171       ', N'Remonty-Żołnierska')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(87 AS Numeric(10, 0)), N'RW', N'Remonty-Willowa', CAST(1 AS Numeric(1, 0)), CAST(88 AS Numeric(10, 0)), N'05203172       ', N'Remonty-Willowa')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(88 AS Numeric(10, 0)), N'RKPA', N'Remonty-Korab-pomieszczenia administracy', CAST(1 AS Numeric(1, 0)), CAST(89 AS Numeric(10, 0)), N'05203173       ', N'Remonty-Korab-pomieszczenia administracyjne')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(89 AS Numeric(10, 0)), N'RPPA', N'Remonty-Pasat-pomieszczenia administracy', CAST(1 AS Numeric(1, 0)), CAST(90 AS Numeric(10, 0)), N'05203174       ', N'Remonty-Pasat-pomieszczenia administracyjne ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(90 AS Numeric(10, 0)), N'RPOB', N'Remonty-Pobożnego', CAST(1 AS Numeric(1, 0)), CAST(91 AS Numeric(10, 0)), N'05203175       ', N'Remonty-Pobożnego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(91 AS Numeric(10, 0)), N'RPŁ', N'Remonty-Pływalnia', CAST(1 AS Numeric(1, 0)), CAST(92 AS Numeric(10, 0)), N'05203176       ', N'Remonty-Pływalnia')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(92 AS Numeric(10, 0)), N'RSSZPA', N'Remonty stołówka ul.Szczerbcowa-pomieszc', CAST(1 AS Numeric(1, 0)), CAST(93 AS Numeric(10, 0)), N'05203177       ', N'Remonty stołówka ul.Szczerbcowa-pomieszczenia administracyjn')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(93 AS Numeric(10, 0)), N'RSD', N'Remonty-sale dydaktyczne', CAST(1 AS Numeric(1, 0)), CAST(94 AS Numeric(10, 0)), N'05203178       ', N'Remonty-sale dydaktyczne')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(94 AS Numeric(10, 0)), N'RDPTŚ', N'Remonty-DPT Świnoujście', CAST(1 AS Numeric(1, 0)), CAST(95 AS Numeric(10, 0)), N'05203179       ', N'Remonty-DPT Świnoujście')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(95 AS Numeric(10, 0)), N'FF', N'Dział Finansowy', CAST(1 AS Numeric(1, 0)), CAST(98 AS Numeric(10, 0)), N'05204201       ', N'Dział Finansowy ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(96 AS Numeric(10, 0)), N'FK', N'Dział Księgowości', CAST(1 AS Numeric(1, 0)), CAST(99 AS Numeric(10, 0)), N'05204202       ', N'Dział Księgowości ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(97 AS Numeric(10, 0)), N'FZ', N'Dział Obsługi Księgowej Projektów Między', CAST(1 AS Numeric(1, 0)), CAST(100 AS Numeric(10, 0)), N'05204203       ', N'Dział Obsługi Księgowej Projektów Międzynarodowych ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(98 AS Numeric(10, 0)), N'FP', N'Dział Płac', CAST(1 AS Numeric(1, 0)), CAST(101 AS Numeric(10, 0)), N'05204204       ', N'Dział Płac ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(100 AS Numeric(10, 0)), N'AOP', N'Samodzielne Stanowisko ds. Ochrony Przec', CAST(1 AS Numeric(1, 0)), CAST(105 AS Numeric(10, 0)), N'05205000       ', N'Samodzielne Stanowisko ds. Ochrony Przeciwpożarowej ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(102 AS Numeric(10, 0)), N'ODAWCH', N'Obiekt Dydaktyczno-Administracyjny ul. W', CAST(1 AS Numeric(1, 0)), CAST(107 AS Numeric(10, 0)), N'05206211       ', N'Obiekt Dydaktyczno-Administracyjny ul. Wały Chrobrego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(103 AS Numeric(10, 0)), N'ODPO', N'Obiekt Dydaktyczny ul. Podgórna', CAST(1 AS Numeric(1, 0)), CAST(108 AS Numeric(10, 0)), N'05206212       ', N'Obiekt Dydaktyczny ul. Podgórna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(104 AS Numeric(10, 0)), N'ODŻ', N'Obiekt Dydaktyczny ul. Żołnierska', CAST(1 AS Numeric(1, 0)), CAST(109 AS Numeric(10, 0)), N'05206213       ', N'Obiekt Dydaktyczny ul. Żołnierska')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(105 AS Numeric(10, 0)), N'ODW', N'Obiekt Dydaktyczny ul. Willowa', CAST(1 AS Numeric(1, 0)), CAST(110 AS Numeric(10, 0)), N'05206214       ', N'Obiekt Dydaktyczny ul. Willowa')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(107 AS Numeric(10, 0)), N'ADS-1PA', N'Studencki Dom Marynarza KORAB-pomieszcze', CAST(1 AS Numeric(1, 0)), CAST(112 AS Numeric(10, 0)), N'05207221       ', N'Studencki Dom Marynarza KORAB')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(108 AS Numeric(10, 0)), N'ADS-2PA', N'Studencki Dom Marynarza PASAT-pomieszcze', CAST(1 AS Numeric(1, 0)), CAST(113 AS Numeric(10, 0)), N'05207222       ', N'Studencki Dom Marynarza PASAT')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(109 AS Numeric(10, 0)), N'ODP', N'Obiekt Dydaktyczny ul. Pobożnego', CAST(1 AS Numeric(1, 0)), CAST(114 AS Numeric(10, 0)), N'05207223       ', N'Obiekt Dydaktyczny ul. Pobożnego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(110 AS Numeric(10, 0)), N'ADS-P', N'Pływalnia', CAST(1 AS Numeric(1, 0)), CAST(115 AS Numeric(10, 0)), N'05207224       ', N'Pływalnia ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(111 AS Numeric(10, 0)), N'SSZPA', N'Stołówka ul.  Szczerbcowa-pomieszczenia ', CAST(1 AS Numeric(1, 0)), CAST(116 AS Numeric(10, 0)), N'05207225       ', N'Pływalnia-komercja')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(112 AS Numeric(10, 0)), N'SDWS', N'Sale dydaktyczne w stołówce', CAST(1 AS Numeric(1, 0)), CAST(117 AS Numeric(10, 0)), N'05207226       ', N'Stołówka ul.  Szczerbcowa')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(113 AS Numeric(10, 0)), N'AW', N'Dom Pracy Twórczej w Świnoujściu', CAST(1 AS Numeric(1, 0)), CAST(118 AS Numeric(10, 0)), N'05208000       ', N'Dom Pracy Twórczej w Świnoujściu ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(114 AS Numeric(10, 0)), N'SDWŁ', N'Sale dydaktyczne wspólne-audiowizualna,i', CAST(1 AS Numeric(1, 0)), CAST(119 AS Numeric(10, 0)), N'05209000       ', N'Sale dydaktyczne wspólne-audiowizualna,im.Łaskiego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(115 AS Numeric(10, 0)), N'DN', N'Pion Dziekana Wydziału Nawigacyjnego', CAST(1 AS Numeric(1, 0)), CAST(120 AS Numeric(10, 0)), N'06250000       ', N'Pion Dziekana Wydziału Nawigacyjnego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(116 AS Numeric(10, 0)), N'DNKK', N'Koszty kształcenia', CAST(1 AS Numeric(1, 0)), CAST(121 AS Numeric(10, 0)), N'06250001       ', N'Koszty kształcenia')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(117 AS Numeric(10, 0)), N'DNPSM', N'Praktyki studentów morskie', CAST(1 AS Numeric(1, 0)), CAST(122 AS Numeric(10, 0)), N'06250002       ', N'Praktyki studentów morskie')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(118 AS Numeric(10, 0)), N'DNPSL', N'Praktyki studentów lądowe', CAST(1 AS Numeric(1, 0)), CAST(123 AS Numeric(10, 0)), N'06250003       ', N'Praktyki studentów lądowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(119 AS Numeric(10, 0)), N'DNPSN', N'Praktyki studentów Nawigator XXI', CAST(1 AS Numeric(1, 0)), CAST(124 AS Numeric(10, 0)), N'06250004       ', N'Praktyki studentów Nawigator XXI')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(120 AS Numeric(10, 0)), N'DNUS', N'Umundurowanie studentów', CAST(1 AS Numeric(1, 0)), CAST(125 AS Numeric(10, 0)), N'06250005       ', N'Umundurowanie studentów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(121 AS Numeric(10, 0)), N'DNR', N'Rekrutacja', CAST(1 AS Numeric(1, 0)), CAST(126 AS Numeric(10, 0)), N'06250006       ', N'')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(122 AS Numeric(10, 0)), N'DND', N'Dziekanat Wydziału Nawigacyjnego', CAST(1 AS Numeric(1, 0)), CAST(127 AS Numeric(10, 0)), N'06251000       ', N'Dziekanat Wydziału Nawigacyjnego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(124 AS Numeric(10, 0)), N'ZNM', N'Zakład Nawigacji Morskiej', CAST(1 AS Numeric(1, 0)), CAST(129 AS Numeric(10, 0)), N'06252241       ', N'Zakład Nawigacji Morskiej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(125 AS Numeric(10, 0)), N'ZBISS', N'Zakład Budowy i Stateczności Statku', CAST(1 AS Numeric(1, 0)), CAST(130 AS Numeric(10, 0)), N'06252242       ', N'Zakład Budowy i Stateczności Statku')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(126 AS Numeric(10, 0)), N'ZMIO', N'Zakład Meteorologii i Oceanografii', CAST(1 AS Numeric(1, 0)), CAST(131 AS Numeric(10, 0)), N'06252243       ', N'Zakład Meteorologii i Oceanografii')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(127 AS Numeric(10, 0)), N'ZRIOŻ', N'Zakład Ratownictwa i Ochrony Żeglugi', CAST(1 AS Numeric(1, 0)), CAST(132 AS Numeric(10, 0)), N'06252244       ', N'Zakład Ratownictwa i Ochrony Żeglugi')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(128 AS Numeric(10, 0)), N'CTP LNG', N'Centrum Technologii Przewozów LNG', CAST(1 AS Numeric(1, 0)), CAST(133 AS Numeric(10, 0)), N'06252245       ', N'Centrum Technologii Przewozów LNG')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(129 AS Numeric(10, 0)), N'CNBARES', N'Centrum Naukowo-Badawcze Analizy Ryzyka ', CAST(1 AS Numeric(1, 0)), CAST(134 AS Numeric(10, 0)), N'06252246       ', N'Centrum Naukowo-Badawcze Analizy Ryzyka Eksploatacji Statków')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(131 AS Numeric(10, 0)), N'ZMIPM', N'Zakład Manewrowania i Pilotażu Morskiego', CAST(1 AS Numeric(1, 0)), CAST(136 AS Numeric(10, 0)), N'06253251       ', N'Zakład Manewrowania i Pilotażu Morskiego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(132 AS Numeric(10, 0)), N'ZBN', N'Zakład Bezpieczeństwa Nawigacyjnego', CAST(1 AS Numeric(1, 0)), CAST(137 AS Numeric(10, 0)), N'06253252       ', N'Zakład Bezpieczeństwa Nawigacyjnego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(133 AS Numeric(10, 0)), N'ZUN', N'Zakład Urządzeń Nawigacyjnych', CAST(1 AS Numeric(1, 0)), CAST(138 AS Numeric(10, 0)), N'06253253       ', N'Zakład Urządzeń Nawigacyjnych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(134 AS Numeric(10, 0)), N'ZRM', N'Zakład Rybołówstwa Morskiego', CAST(1 AS Numeric(1, 0)), CAST(139 AS Numeric(10, 0)), N'06253254       ', N'Zakład Rybołówstwa Morskiego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(136 AS Numeric(10, 0)), N'ZITM', N'Zakład Informatycznych Technologii Morsk', CAST(1 AS Numeric(1, 0)), CAST(141 AS Numeric(10, 0)), N'06254261       ', N'Zakład Informatycznych Technologii Morskich')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(137 AS Numeric(10, 0)), N'ZKTM', N'Zakład Komunikacyjnych Technologii Morsk', CAST(1 AS Numeric(1, 0)), CAST(142 AS Numeric(10, 0)), N'06254262       ', N'Zakład Komunikacyjnych Technologii Morskich')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(138 AS Numeric(10, 0)), N'ZM', N'Zakład Matematyki', CAST(1 AS Numeric(1, 0)), CAST(143 AS Numeric(10, 0)), N'06254263       ', N'Zakład Matematyki')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(140 AS Numeric(10, 0)), N'ZGIH', N'Zakład Geodezji i Hydrografi', CAST(1 AS Numeric(1, 0)), CAST(145 AS Numeric(10, 0)), N'06255271       ', N'Zakład Geodezji i Hydrografi')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(141 AS Numeric(10, 0)), N'ZKIG', N'Zakład Kartografii i Geoinformatyki', CAST(1 AS Numeric(1, 0)), CAST(146 AS Numeric(10, 0)), N'06255272       ', N'Zakład Kartografii i Geoinformatyki')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(142 AS Numeric(10, 0)), N'CIRM', N'Centrum Inżynierii Ruchu Morskiego', CAST(1 AS Numeric(1, 0)), CAST(147 AS Numeric(10, 0)), N'06256000       ', N'Centrum Inżynierii Ruchu Morskiego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(143 AS Numeric(10, 0)), N'DM', N'Pion Dziekana Wydziału Mechanicznego', CAST(1 AS Numeric(1, 0)), CAST(148 AS Numeric(10, 0)), N'07300000       ', N'Pion Dziekana Wydziału Mechanicznego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(144 AS Numeric(10, 0)), N'DMKK', N'Kształcenie kadry', CAST(1 AS Numeric(1, 0)), CAST(149 AS Numeric(10, 0)), N'07300001       ', N'Kształcenie kadry')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(145 AS Numeric(10, 0)), N'DMPSM', N'Praktyki studentów morskie', CAST(1 AS Numeric(1, 0)), CAST(150 AS Numeric(10, 0)), N'07300002       ', N'Praktyki studentów morskie')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(146 AS Numeric(10, 0)), N'DMPSL', N'Praktyki studentów lądowe', CAST(1 AS Numeric(1, 0)), CAST(151 AS Numeric(10, 0)), N'07300003       ', N'Praktyki studentów lądowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(147 AS Numeric(10, 0)), N'DMPSN', N'Praktyki studentów Nawigator XXI', CAST(1 AS Numeric(1, 0)), CAST(152 AS Numeric(10, 0)), N'07300004       ', N'Praktyki studentów Nawigator XXI')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(148 AS Numeric(10, 0)), N'DMUS', N'Umundurowanie studentów', CAST(1 AS Numeric(1, 0)), CAST(153 AS Numeric(10, 0)), N'07300005       ', N'Umundurowanie studentów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(149 AS Numeric(10, 0)), N'DMR', N'Rekrutacja', CAST(1 AS Numeric(1, 0)), CAST(154 AS Numeric(10, 0)), N'07300006       ', N'Rekrutacja')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(150 AS Numeric(10, 0)), N'DMD', N'Dziekanat Wydziału Mechanicznego', CAST(1 AS Numeric(1, 0)), CAST(155 AS Numeric(10, 0)), N'07301000       ', N'Dziekanat Wydziału Mechanicznego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(152 AS Numeric(10, 0)), N'ZMT', N'Zakład Mechaniki Technicznej', CAST(1 AS Numeric(1, 0)), CAST(157 AS Numeric(10, 0)), N'07302311       ', N'Zakład Mechaniki Technicznej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(153 AS Numeric(10, 0)), N'ZIMO', N'Zakład Inżynierii Materiałów Okrętowych', CAST(1 AS Numeric(1, 0)), CAST(158 AS Numeric(10, 0)), N'07302312       ', N'Zakład Inżynierii Materiałów Okrętowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(154 AS Numeric(10, 0)), N'ZPBIEM', N'Zakład Podstaw Budowy i Eksploatacji Mas', CAST(1 AS Numeric(1, 0)), CAST(159 AS Numeric(10, 0)), N'07302313       ', N'Zakład Podstaw Budowy i Eksploatacji Maszyn')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(156 AS Numeric(10, 0)), N'ZMIUO', N'Zakład Maszyn i Urządzeń Okrętowych', CAST(1 AS Numeric(1, 0)), CAST(161 AS Numeric(10, 0)), N'07303321       ', N'Zakład Maszyn i Urządzeń Okrętowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(157 AS Numeric(10, 0)), N'ZSO', N'Zakład Siłowni Okrętowych', CAST(1 AS Numeric(1, 0)), CAST(162 AS Numeric(10, 0)), N'07303322       ', N'Zakład Siłowni Okrętowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(158 AS Numeric(10, 0)), N'LSO', N'Laboratorium Siłowni Okrętowych', CAST(1 AS Numeric(1, 0)), CAST(163 AS Numeric(10, 0)), N'07303323       ', N'Laboratorium Siłowni Okrętowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(159 AS Numeric(10, 0)), N'WLB', N'Wydziałowe Laboratorium Badawcze', CAST(1 AS Numeric(1, 0)), CAST(164 AS Numeric(10, 0)), N'07303324       ', N'Wydziałowe Laboratorium Badawcze')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(161 AS Numeric(10, 0)), N'ZEIEO', N'Zakład Elektrotechniki i Elektroniki Okr', CAST(1 AS Numeric(1, 0)), CAST(166 AS Numeric(10, 0)), N'07304331       ', N'Zakład Elektrotechniki i Elektroniki Okrętowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(162 AS Numeric(10, 0)), N'ZAIR', N'Zakład Automatyki i Robotyki', CAST(1 AS Numeric(1, 0)), CAST(167 AS Numeric(10, 0)), N'07304332       ', N'Zakład Automatyki i Robotyki')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(163 AS Numeric(10, 0)), N'KDIRS', N'Katedra Diagnostyki i Remontów Maszyn', CAST(1 AS Numeric(1, 0)), CAST(168 AS Numeric(10, 0)), N'07305000       ', N'Katedra Diagnostyki i Remontów Maszyn')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(164 AS Numeric(10, 0)), N'KFIC', N'Katedra Fizyki i Chemii', CAST(1 AS Numeric(1, 0)), CAST(169 AS Numeric(10, 0)), N'07306000       ', N'Katedra Fizyki i Chemii')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(165 AS Numeric(10, 0)), N'DT', N'Pion Dziekana Wydziału Inżynieryjno-Ekon', CAST(1 AS Numeric(1, 0)), CAST(170 AS Numeric(10, 0)), N'08350000       ', N'Pion Dziekana Wydziału Inżynieryjno-Ekonomicznego Transportu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(166 AS Numeric(10, 0)), N'DTKK', N'Koszty kształcenia', CAST(1 AS Numeric(1, 0)), CAST(171 AS Numeric(10, 0)), N'08350001       ', N'Koszty kształcenia')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(167 AS Numeric(10, 0)), N'DTPSM', N'Praktyki studentów morskie', CAST(1 AS Numeric(1, 0)), CAST(172 AS Numeric(10, 0)), N'08350002       ', N'Praktyki studentów morskie')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(168 AS Numeric(10, 0)), N'DTPSL', N'Praktyki studentów lądowe', CAST(1 AS Numeric(1, 0)), CAST(173 AS Numeric(10, 0)), N'08350003       ', N'Praktyki studentów lądowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(169 AS Numeric(10, 0)), N'DTPSN', N'Praktyki studentów Nawigator XXI', CAST(1 AS Numeric(1, 0)), CAST(174 AS Numeric(10, 0)), N'08350004       ', N'Praktyki studentów Nawigator XXI')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(170 AS Numeric(10, 0)), N'DTUS', N'Umundurowanie studentów', CAST(1 AS Numeric(1, 0)), CAST(175 AS Numeric(10, 0)), N'08350005       ', N'Umundurowanie studentów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(171 AS Numeric(10, 0)), N'DTR', N'Rekrutacja', CAST(1 AS Numeric(1, 0)), CAST(176 AS Numeric(10, 0)), N'08350006       ', N'Rekrutacja')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(172 AS Numeric(10, 0)), N'DTD', N'Dziekanat Wydziału Inżynieryjno-Ekonomic', CAST(1 AS Numeric(1, 0)), CAST(177 AS Numeric(10, 0)), N'08351000       ', N'Dziekanat Wydziału Inżynieryjno-Ekonomicznego Transportu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(173 AS Numeric(10, 0)), N'PAIPESGM', N'Pracownia Analiz i Prognoz Ekonomicznych', CAST(1 AS Numeric(1, 0)), CAST(178 AS Numeric(10, 0)), N'08352000       ', N'Pracownia Analiz i Prognoz Ekonomicznych Sektora Gospodarki ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(175 AS Numeric(10, 0)), N'PZTWT', N'Pracownia Zastosowań Teleinformatyki w T', CAST(1 AS Numeric(1, 0)), CAST(180 AS Numeric(10, 0)), N'08353361       ', N'Pracownia Zastosowań Teleinformatyki w Transporcie')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(176 AS Numeric(10, 0)), N'ZTT', N'Zakład Techniki Transportu', CAST(1 AS Numeric(1, 0)), CAST(181 AS Numeric(10, 0)), N'08353362       ', N'Zakład Techniki Transportu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(177 AS Numeric(10, 0)), N'ZTTZIOŚ', N'Zakład Technologii Transportu Zintegrowa', CAST(1 AS Numeric(1, 0)), CAST(182 AS Numeric(10, 0)), N'08353363       ', N'Zakład Technologii Transportu Zintegrowanego i Ochrony Środo')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(178 AS Numeric(10, 0)), N'ZTIZJ', N'Zakład Towaroznawstwa i Zarządzania Jako', CAST(1 AS Numeric(1, 0)), CAST(183 AS Numeric(10, 0)), N'08353364       ', N'Zakład Towaroznawstwa i Zarządzania Jakością')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(179 AS Numeric(10, 0)), N'ZMK', N'Zakład Metod Komputerowych', CAST(1 AS Numeric(1, 0)), CAST(184 AS Numeric(10, 0)), N'08353365       ', N'Zakład Metod Komputerowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(180 AS Numeric(10, 0)), N'IZT SUMA', N'Instytut Zarządzania Transportem', CAST(1 AS Numeric(1, 0)), CAST(185 AS Numeric(10, 0)), N'08354000       ', N'Instytut Zarządzania Transportem')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(181 AS Numeric(10, 0)), N'ZNEIS', N'Zakład Nauk Ekonomicznych i Społecznych', CAST(1 AS Numeric(1, 0)), CAST(186 AS Numeric(10, 0)), N'08354371       ', N'Zakład Nauk Ekonomicznych i Społecznych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(182 AS Numeric(10, 0)), N'ZOIZ', N'Zakład Organizacji i Zarządzania', CAST(1 AS Numeric(1, 0)), CAST(187 AS Numeric(10, 0)), N'08354372       ', N'Zakład Organizacji i Zarządzania')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(183 AS Numeric(10, 0)), N'ZLIST', N'Zakład Logistyki i Systemów Transportowy', CAST(1 AS Numeric(1, 0)), CAST(188 AS Numeric(10, 0)), N'08354373       ', N'Zakład Logistyki i Systemów Transportowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(184 AS Numeric(10, 0)), N'ZGMIPT', N'Zakład Gospodarki Morskiej i Polityki Tr', CAST(1 AS Numeric(1, 0)), CAST(189 AS Numeric(10, 0)), N'08354374       ', N'Zakład Gospodarki Morskiej i Polityki Transportowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(185 AS Numeric(10, 0)), N'ZMIIP', N'Zakład Metod Ilościowych i Prognozowania', CAST(1 AS Numeric(1, 0)), CAST(190 AS Numeric(10, 0)), N'08354375       ', N'Zakład Metod Ilościowych i Prognozowania')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(186 AS Numeric(10, 0)), N'ZFIM', N'Zakład Finansów i Marketingu', CAST(1 AS Numeric(1, 0)), CAST(191 AS Numeric(10, 0)), N'08354376       ', N'Zakład Finansów i Marketingu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(187 AS Numeric(10, 0)), N'ZIP', N'Zakład Inżynierii Produkcji', CAST(1 AS Numeric(1, 0)), CAST(192 AS Numeric(10, 0)), N'08354377       ', N'Zakład Inżynierii Produkcji')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(189 AS Numeric(10, 0)), N'INLAND SHIPPING', N'Konferencja Inland Shipping', CAST(1 AS Numeric(1, 0)), CAST(194 AS Numeric(10, 0)), N'09400401       ', N'Konferencja Inland Shipping')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(190 AS Numeric(10, 0)), N'EXPLO-SHIP', N'Konferencja Explo-Ship', CAST(1 AS Numeric(1, 0)), CAST(195 AS Numeric(10, 0)), N'09400402       ', N'Konferencja Explo-Ship')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(191 AS Numeric(10, 0)), N'TECH. INFOR.', N'Konferencja Technologie Edukacyjne', CAST(1 AS Numeric(1, 0)), CAST(196 AS Numeric(10, 0)), N'09400403       ', N'Konferencja Technologie Edukacyjne')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(193 AS Numeric(10, 0)), N'ODPIS ZFŚS', N'Odpis na Zakładowy Fundusz Świadczeń Soc', CAST(1 AS Numeric(1, 0)), CAST(198 AS Numeric(10, 0)), N'85001000       ', N'Odpis na Zakładowy Fundusz Świadczeń Socjalnych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(194 AS Numeric(10, 0)), N'PM', N'Pożyczki mieszkaniowe', CAST(1 AS Numeric(1, 0)), CAST(199 AS Numeric(10, 0)), N'85002000       ', N'Pożyczki mieszkaniowe')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(195 AS Numeric(10, 0)), N'PRF', N'Pomoc rzeczowo-finansowa', CAST(1 AS Numeric(1, 0)), CAST(200 AS Numeric(10, 0)), N'85003000       ', N'Pomoc rzeczowo-finansowa')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(196 AS Numeric(10, 0)), N'WP', N'Wypoczynek pracowniczy', CAST(1 AS Numeric(1, 0)), CAST(201 AS Numeric(10, 0)), N'85004000       ', N'Wypoczynek pracowniczy')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(197 AS Numeric(10, 0)), N'WD', N'Wypoczynek dzieci', CAST(1 AS Numeric(1, 0)), CAST(202 AS Numeric(10, 0)), N'85005000       ', N'Wypoczynek dzieci')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(198 AS Numeric(10, 0)), N'DK', N'Działalność kulturalna', CAST(1 AS Numeric(1, 0)), CAST(203 AS Numeric(10, 0)), N'85006000       ', N'Działalność kulturalna')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(199 AS Numeric(10, 0)), N'KZP', N'Koszty związane z pogrzebami', CAST(1 AS Numeric(1, 0)), CAST(204 AS Numeric(10, 0)), N'85007000       ', N'Koszty związane z pogrzebami')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(8 AS Numeric(10, 0)), N'RA', N'Samodzielne Stanowisko ds. Audytu Wewnęt', CAST(0 AS Numeric(1, 0)), CAST(4 AS Numeric(10, 0)), N'01001000       ', N'Biuro Rektora ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(211 AS Numeric(10, 0)), N'RR', N'Biuro Rektora', CAST(1 AS Numeric(1, 0)), CAST(4 AS Numeric(10, 0)), N'01001000       ', N'Biuro Rektora ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(212 AS Numeric(10, 0)), N'RI', N'Samodzielne stanowisko ds. inwe. i kontr', CAST(1 AS Numeric(1, 0)), CAST(10 AS Numeric(10, 0)), N'01007000       ', N'Samodzielne Stanowisko ds. Inwentaryzacji i Kontroli ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(213 AS Numeric(10, 0)), N'RBHP', N'Samodzielne stanowisko ds.BHP', CAST(1 AS Numeric(1, 0)), CAST(11 AS Numeric(10, 0)), N'01008000       ', N'Samodzielne Stanowisko ds. BHP ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(214 AS Numeric(10, 0)), N'RO', N'Samodzielne stanowisko ds. Obrony', CAST(1 AS Numeric(1, 0)), CAST(12 AS Numeric(10, 0)), N'01009000       ', N'Samodzielne Stanowisko ds. Obronnych ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(310 AS Numeric(10, 0)), N'AG', N'Dział Administracyjno-Gospodarczy', CAST(1 AS Numeric(1, 0)), CAST(106 AS Numeric(10, 0)), N'05206000       ', N'Dział Administracyjno-Gospodarczy ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(311 AS Numeric(10, 0)), N'ADS', N'Akademicki Dom Studencki Korab/Pasat', CAST(1 AS Numeric(1, 0)), CAST(111 AS Numeric(10, 0)), N'05207000       ', N'Osiedle Akademickie ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(312 AS Numeric(10, 0)), N'BG', N'Biblioteka', CAST(1 AS Numeric(1, 0)), CAST(21 AS Numeric(10, 0)), N'02058000       ', N'Biblioteka Główna ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(314 AS Numeric(10, 0)), N'NN', N'Dział nauczania i certyfikacji', CAST(1 AS Numeric(1, 0)), CAST(29 AS Numeric(10, 0)), N'03101000       ', N'Dział Nauczania i Certyfikacji ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(315 AS Numeric(10, 0)), N'MM', N'Dział Spraw Morskich i Praktyk', CAST(1 AS Numeric(1, 0)), CAST(57 AS Numeric(10, 0)), N'04151000       ', N'Dział Spraw Morskich i Praktyk ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(316 AS Numeric(10, 0)), N'ITM', N'Instytut Technologii Morskich', CAST(1 AS Numeric(1, 0)), CAST(140 AS Numeric(10, 0)), N'06254000       ', N'Instytut Technologii Morskich')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(317 AS Numeric(10, 0)), N'IIT', N'Instytut Inżynierii Transportu', CAST(1 AS Numeric(1, 0)), CAST(179 AS Numeric(10, 0)), N'08353000       ', N'Instytut Inżynierii Transportu')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(318 AS Numeric(10, 0)), N'R', N'Pion Rektora', CAST(1 AS Numeric(1, 0)), CAST(2 AS Numeric(10, 0)), N'01000000       ', N'Pion Rektora ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(319 AS Numeric(10, 0)), N'SDKO', N'Studium Doskonalenia Kadr Oficerskich', CAST(1 AS Numeric(1, 0)), CAST(62 AS Numeric(10, 0)), N'04154000       ', N'Studium Doskonalenia Kadr Oficerskich ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(320 AS Numeric(10, 0)), N'INM', N'Instytut Nawigacji Morskiej', CAST(1 AS Numeric(1, 0)), CAST(128 AS Numeric(10, 0)), N'06252000       ', N'Instytut Nawigacji Morskiej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(321 AS Numeric(10, 0)), N'AF', N'Kwestura', CAST(0 AS Numeric(1, 0)), CAST(104 AS Numeric(10, 0)), N'05204000       ', N'Kwestura')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(322 AS Numeric(10, 0)), N'IG', N'Instytut Geoinformatyki', CAST(1 AS Numeric(1, 0)), CAST(144 AS Numeric(10, 0)), N'06255000       ', N'Instytut Geoinformatyki')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(323 AS Numeric(10, 0)), N'OSRM', N'OSRM', CAST(1 AS Numeric(1, 0)), CAST(64 AS Numeric(10, 0)), N'04155000       ', N'Ośrodek Szkoleniowy Ratownictwa Morskiego ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(324 AS Numeric(10, 0)), N'IESO', N'Instytut Eksploatacji Siłowni Okrętowych', CAST(1 AS Numeric(1, 0)), CAST(160 AS Numeric(10, 0)), N'07303000       ', N'Instytut Eksploatacji Siłowni Okrętowych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(325 AS Numeric(10, 0)), N'IIRM', N'Instytut Inżynierii Ruchu Morskiego', CAST(1 AS Numeric(1, 0)), CAST(135 AS Numeric(10, 0)), N'06253000       ', N'Instytut Inżynierii Ruchu Morskiego')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(326 AS Numeric(10, 0)), N'IZT', N'Instytut Zarządzania Transportem', CAST(1 AS Numeric(1, 0)), CAST(185 AS Numeric(10, 0)), N'08354000       ', N'Instytut Zarządzania Transportem')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(327 AS Numeric(10, 0)), N'RM', N'Pion Prorektora ds Morskich', CAST(1 AS Numeric(1, 0)), CAST(56 AS Numeric(10, 0)), N'04150000       ', N'Pion Prorektora ds. Morskich ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(328 AS Numeric(10, 0)), N'RN', N'Pion Prorektora ds. Nauczania', CAST(1 AS Numeric(1, 0)), CAST(28 AS Numeric(10, 0)), N'03100000       ', N'Pion Prorektora ds. Nauczania ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(329 AS Numeric(10, 0)), N'ZFŚS', N'Zakładowy Fundusz Świadczeń Socjalnych', CAST(1 AS Numeric(1, 0)), CAST(197 AS Numeric(10, 0)), N'85000000       ', N'Zakładowy Fundusz Świadczeń Socjalnych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(330 AS Numeric(10, 0)), N'SWFIS', N'Studium Wychowania Fizycznego i Sportu', CAST(1 AS Numeric(1, 0)), CAST(36 AS Numeric(10, 0)), N'03105000       ', N'Studium Wychowania Fizycznego i Sportu ')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(331 AS Numeric(10, 0)), N'RBiB', N'Remonty budynków i budowli', CAST(1 AS Numeric(1, 0)), CAST(76 AS Numeric(10, 0)), N'05203000       ', N'Remonty budynków i budowli')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(332 AS Numeric(10, 0)), N'KONF', N'Konferencje', CAST(1 AS Numeric(1, 0)), CAST(193 AS Numeric(10, 0)), N'09400000       ', N'Konferencje')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(333 AS Numeric(10, 0)), N'IPNT', N'Instytut Podstawowych Nauk Technicznych', CAST(1 AS Numeric(1, 0)), CAST(156 AS Numeric(10, 0)), N'07302000       ', N'Instytut Podstawowych Nauk Technicznych')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(334 AS Numeric(10, 0)), N'IEIAO', N'Instytut Elektrotechniki i Automatyki Ok', CAST(1 AS Numeric(1, 0)), CAST(165 AS Numeric(10, 0)), N'07304000       ', N'Instytut Elektrotechniki i Automatyki Okrętowej')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(335 AS Numeric(10, 0)), N'FSKS', N'Fundusz socjalno-kulturalny studentów', CAST(1 AS Numeric(1, 0)), CAST(51 AS Numeric(10, 0)), N'03106000       ', N'Fundusz socjalno-kulturalny studentów')
GO
INSERT [dbo].[dsg_ams_services_kb2ko] ([budzet_id], [bd_str_budzet_id], [bd_str_budzet_idn], [nazwa_komorki_budzetowej], [czy_wiodaca], [komorka_id], [komorka_idn], [nazwa_komorki_organizacyjnej]) VALUES (CAST(1 AS Numeric(10, 0)), CAST(337 AS Numeric(10, 0)), N'A', N'Pion Kanclerza', CAST(1 AS Numeric(1, 0)), CAST(70 AS Numeric(10, 0)), N'05200000       ', N'Pion Kanclerza ')
GO
