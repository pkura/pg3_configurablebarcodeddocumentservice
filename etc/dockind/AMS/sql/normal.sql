CREATE TABLE [dsg_rodzaj_ams_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	
);

CREATE TABLE [dsg_rodzaj_ams_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

CREATE TABLE [dsg_rodzaj_ams_inT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);


alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add signature varchar(150);
alter table DSG_NORMAL_DOCKIND add NADAWCA numeric(18,0);
alter table DSG_NORMAL_DOCKIND add PAPIER bit default 0;
alter table DSG_NORMAL_DOCKIND add STATUS numeric(18,0);
alter table DSG_NORMAL_DOCKIND add zpo bit;
alter table DSG_NORMAL_DOCKIND add data_zpo date;

alter table dsg_normal_dockind add czy_mail smallint;
alter table dsg_normal_dockind add SUBJECT_EMAIL char(750);
alter table dsg_normal_dockind add RESPONSE_CONTENT char(2000);
alter table dsg_normal_dockind add RECIVER numeric(18,0);

DELETE FROM dsg_rodzaj_ams_in;
DELETE FROM dsg_rodzaj_ams_inT
DELETE FROM dsg_rodzaj_ams_out;

insert into dsg_rodzaj_ams_inT(cn,title,available)values('RSM','Wniosek o uj�cie w ewidencji ksi�gowej RSM (o ujawnienie RSM)',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('WNIP_C','Wniosek o uj�cie ewidencji ksi�gowej WNiP (o ujawnienie WNiP)',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('WNip_D','Wniosek o zdj�cie ewidencji ksi�gowej WNiP (likwidacja WNiP)',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('RSM_L','Wniosek o likwidacj� RSM',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('RSM_Z','Potrzeba zdj�cia z ewidencji ksi�gowej RSM',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('ST','Wniosek o przekwalifikowanie �T na wyposa�enie',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('ZAMOWIENIE','Wniosek o udzielenie zam�wienia',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('ZAKUP','Obs�uga faktur zakupu',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('BUDZET','Wniosek o zmian� bud�etu',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PLAN','Wniosek o zmian� planu zakup�w',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('DYSPOZYCJA_F','Wniosek dyspozycji fakturowania',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PRZENIESIENIE_A','Przeniesienie aparatury naukowej z ewidencji pozabilansowej do bilansowej.',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('DZIERZAWA','Dzier�awa �rodka trwa�ego od innego podmiotu',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROTOTYP','Wytworzenie prototypu, jako wynik dzia�alno�ci naukowej, itp.',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('SRODEK_T','Nieodp�atne otrzymanie �rodka trwa�ego',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PLAN_C','Plan zakup�w',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PLAN_B','Plan bud�et�w',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('ZLECONE','Wniosek o prace zlecone',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_K','Wniosek o utworzenie projektu: projekt krajowy',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_M','Wniosek o utworzenie projektu : projekt mi�dzynarodowy',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_I','Wniosek o utworzenie projektu: projekt inwestycyjny',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_B','Wniosek o utworzenie projektu : projekt budowlany',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_R','Wniosek o utworzenie projektu : projekt remontowy',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_K','Wniosek o utworzenie projektu : projekt komercyjny',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_C','Wniosek o utworzenie projektu : projekt celowy (dotacje)',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('PROJEKT_O','Wniosek o utworzenie projektu : projekt organizacyjny',1);
insert into dsg_rodzaj_ams_inT(cn,title,available)values('POZOSTALE','Pozosta�e',1);


insert into dsg_rodzaj_ams_out(cn,title,available)values('MINISTERSTWO','Pisma do ministerstw',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('PANSTWOWE','Pisma do instytucji pa�stwowych',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('SZKOLY','Pisma do szk�',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('PUBLICZNE','Zam�wienia publiczne',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('ZAGRANICZNE','Pisma do instytucji zagranicznych',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('OSRODKI','Pisma do o�rodk�w AM',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('FAKTURY','Faktury',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('RACHUNKI','Rachunki',1);
insert into dsg_rodzaj_ams_out(cn,title,available)values('POZOSTALE','Pozosta�e',1);

insert into dsg_rodzaj_ams_in(cn,title,available)values('MINISTERSTWO','Pisma z ministerstw',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('PANSTWOWE','Pisma z instytucji pa�stwowych',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('SZKOLY','Pisma ze szk�',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('ZAGRANICZNE','Pisma od istytucji zagranicznych',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('OSRODKI','Pisma z o�rodk�w AM',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('FAKTURY','Faktury',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('RACHUNKI','Rachunki',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('STUDENT','Dot. student�w',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('PROJEKT','Projekty UE',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('PISMA','Pisma imienne nieotwierane',1);
insert into dsg_rodzaj_ams_in(cn,title,available)values('POZOSTALE','Pozosta�e',1);


CREATE TABLE dsg_ams_normal_dockind_multiple_value (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;
