CREATE TABLE [dbo].[dsg_ams_zamowienie](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[rodzaj_zamowienia] [int] NULL,
	[nazwa_zamowienia] [varchar](251) NULL,
	[nr_sprawy] [varchar](20) NULL,
	[realizacja] [datetime] NULL,
	[szczegolowy_opis] [varchar](8000) NULL,
	[uzasadnienie] [varchar](8000) NULL,
	[opinia_uci] [varchar](8000) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[tryb_zamowienia] [int] NULL,
	[szczegoly_zamowienia] [int] NULL,
	[supplier] [numeric](18, 0) NULL,
	[dostawca_nr_konta_id] [numeric](18, 0) NULL,
	--[dysponent] [int] NULL,
	--[pion] [int] NULL,

PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE dsg_ams_zamowienie_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);

ALTER TABLE [dsg_ams_zamowienie] ADD wnioskodawca numeric(19,0);
ALTER TABLE [dsg_ams_zamowienie] ADD wnioskodawca_jednostka numeric(19,0);
ALTER TABLE [dsg_ams_zamowienie] ADD czy_uci bit;
--ALTER TABLE [dsg_ams_zamowienie] ADD okres_budzetowy int;




--update [dsg_ams_zamowienie] set rodzaj_zamowienia=1 where rodzaj_zamowienia >= 2700;

ALTER TABLE [dsg_ams_zamowienie] ADD erp_status int;
ALTER TABLE [dsg_ams_zamowienie] ADD erp_document_idm varchar(50);

ALTER TABLE [dsg_ams_zamowienie] ADD typ_rezerwacji int;

ALTER TABLE [dsg_ams_zamowienie] ADD czy_opinia_kwestury int;
ALTER TABLE [dsg_ams_zamowienie] ADD czy_weryfikacja_zadania int;
ALTER TABLE [dsg_ams_zamowienie] ADD uwagi_dodatkowe varchar(500);
ALTER TABLE [dsg_ams_zamowienie] ADD opinia_kwestury varchar(500);
ALTER TABLE [dsg_ams_zamowienie] ADD opinia_zadania_finansowego varchar(500);

ALTER TABLE dsg_ams_zamowienie ADD opiekun_projektu numeric(18, 0);

ALTER TABLE [dsg_ams_zamowienie] ADD czy_edycja_dzp int;
ALTER TABLE [dsg_ams_zamowienie] ADD uzasadnienie_edycji_dzp varchar(500);
alter table dsg_ams_zamowienie
add typ_rezerwacji_final int;
ALTER TABLE [dsg_ams_zamowienie] ADD jednostka_mpk int;


alter table dsg_ams_zamowienie add kod_cpv varchar(30);
alter table dsg_ams_zamowienie add numer_zamowienia varchar(128);

ALTER TABLE [dsg_ams_zamowienie] ADD komorka_budzetowa int;
ALTER TABLE [dsg_ams_zamowienie] ADD okres_budzetowy int;

alter table dsg_ams_zamowienie add wydruk_helper varchar(1);


ALTER TABLE dsg_ams_zamowienie ADD czy_zamowienie_sukcesywnie int;
ALTER TABLE dsg_ams_zamowienie ADD czy_zamowienie_bez_ustawy int;

ALTER TABLE dsg_ams_zamowienie ADD wymagany_typ_dokumentu_wniosek int;
ALTER TABLE dsg_ams_zamowienie ADD wymagany_typ_dokumentu_umowa int;
ALTER TABLE dsg_ams_zamowienie ADD wymagany_typ_dokumentu_zamowienie int;

ALTER TABLE dsg_ams_zamowienie DROP COLUMN czy_zamowienie_bez_ustawy;

ALTER TABLE dsg_ams_zamowienie ADD suma_netto numeric(18, 2);
ALTER TABLE dsg_ams_zamowienie ADD czy_suma_netto_manualna int;

ALTER TABLE dsg_ams_zamowienie ADD jednostka_realizujaca int;
ALTER TABLE dsg_ams_zamowienie ADD jednostka_realizujaca_auto int;
