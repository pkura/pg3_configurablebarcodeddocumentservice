drop table dsg_ams_unit_budget_ko;

CREATE TABLE dsg_ams_unit_budget_ko (
    ID BIGINT IDENTITY(1,1) NOT NULL,
    ERP_ID bigint NULL,
    IDM VARCHAR(50) NULL,
    NAZWA VARCHAR(100) NULL,
    BD_STR_BUDZET_ID REAL NULL,
    BD_STR_BUDZET_IDN VARCHAR(50) NULL,
    BUDZET_ID REAL NULL,
    BUDZET_IDM VARCHAR(50) NULL,
    CZY_AGREGAT integer NULL,
    available bit default(1) not null
)
ALTER TABLE dsg_ams_unit_budget_ko
ADD CONSTRAINT PK_dsg_ams_unit_budget_ko PRIMARY KEY(ID);

CREATE INDEX IDX_dsg_ams_unit_budget_ko ON dsg_ams_unit_budget_ko(ERP_ID);

CREATE VIEW dsg_ams_unit_budget_ko_view AS
	SELECT
		ID AS id,
		IDM AS cn,
		NAZWA AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM dsg_ams_unit_budget_ko
	
