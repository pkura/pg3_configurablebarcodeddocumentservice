CREATE TABLE [dbo].[dsg_ams_zrodla_finansowania](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
    CONSTRAINT [PK__dsg_ams_zrodla_finansowania]
    PRIMARY KEY CLUSTERED ([id] ASC)
    WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF

GO
SET IDENTITY_INSERT [dbo].[dsg_ams_zrodla_finansowania] ON
    INSERT [dbo].[dsg_ams_zrodla_finansowania] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'ZF', N'�r�d�a finansowania', NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[dsg_ams_zrodla_finansowania] OFF




--dla testow
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF1', N'�r�d�a finansowania 1', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF2', N'�r�d�a finansowania 2', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF3', N'�r�d�a finansowania 3', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF4', N'�r�d�a finansowania 4', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF5', N'�r�d�a finansowania 5', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF6', N'�r�d�a finansowania 6', NULL, NULL, 1)
INSERT INTO dsg_ams_zrodla_finansowania ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZF7', N'�r�d�a finansowania 7', NULL, NULL, 1)