CREATE TABLE dsg_ams_payment_terms (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 2),
	idm varchar(100),
	przeznaczenie varchar(100)
)

CREATE VIEW dsg_ams_view_payment_terms AS
	SELECT
		id,
		id AS cn,
		idm AS title,
		NULL AS centrum,
		CASE
			WHEN idm LIKE 'Przelew%'
				THEN 1
			WHEN idm LIKE 'Got�wka'
				THEN 0
		END AS refValue,
		1 AS available
	FROM dsg_ams_payment_terms WHERE przeznaczenie LIKE 'Zakup';