create table dsg_ams_zamowienie_dysponent_dic (
id numeric(19,0) identity(1,1),
dysponent int null,
procent numeric(10,2) null);
create unique index dsg_ams_zamowienie_dysponent_dic_id on dsg_ams_zamowienie_dysponent_dic(id);