CREATE TABLE DSG_AMS_KARTA_OBIEGOWA (
	[DOCUMENT_ID] [numeric] (18,0) NOT NULL,
	[STATUS] [numeric](18, 0) NULL,

	[TYP_PROJEKTU] [numeric](18, 0) NULL,
	[TEMAT_PRACY] [varchar](255) NULL,
	[JEDN_FINANSUJACA] [varchar](255) NULL,
    [DYSPONENT_SRODKOW] [numeric](18, 0) NULL,
    [DATA_ROZPOCZECIA] [date] NULL,
    [DATA_ZAKONCZENIA] [date] NULL,
    [KOMORKA_ORG_AMS_NADZOR] [numeric](18, 0) NULL,
    [ZRODLO_FINANSOWANIA] [numeric](18, 0) NULL,
    [KIEROWNIK_PROJEKTU] [numeric](18, 0) NULL,
    [KIEROWNIK_PROJEKTU_AUTHOR] [numeric](1, 0) NULL,
    [KIEROWNIK_PIONU] [numeric](18, 0) NULL,
    [JEDN_REALIZUJACA] [numeric](18, 0) NULL,

	[DYREKTOR_INSTYTUTU] [numeric](18, 0) NULL,



    [DATA_ZLOZENIA_KARTY_OBIEGOWEJ] [date] NULL,
    [OPIEKUN_PROJEKTU] [numeric] (18,0) NULL,
    [KONKURS] [varchar](255) NULL,
    [ZRODLO_FINANSOWANIA_ZEWNETRZNEGO] [numeric](18, 0) NULL,
    [KOSZTY_BEZPOSREDNIE] [numeric](18, 2) NULL,
    [KOSZTY_BEZPOSREDNIE_PROC] [numeric](18, 2) NULL,
    [KOSZTY_POSREDNIE_PROC_KB] [numeric](18, 0) NULL,
    [LACZNA_WARTOSC_PROJEKTU] [numeric](18, 2) NULL,
    [FINANSOWANIE_ZEWNETRZNE_PROC_WP] [numeric](18, 0) NULL,
    [WKLAD_WLASNY_AM_PROC_WP] [numeric](18, 0) NULL,
    [WKLAD_WLASNY_WARTOSC]  [numeric](18, 2) NULL,
    [KWALIFIKOWANY_VAT] [numeric](18, 0) NULL,
    [PREFINANSOWANIE] [numeric](18, 0) NULL,
    [PREFINANSOWANIE_WARTOSC] [numeric](18, 2) NULL,
    [ZADANIE_FINANSOWE] [varchar](255) NULL,
    [CEL_PROJEKTU] [varchar](500) NULL,
    [SPOSOB_REALIZACJI_PROJEKTU] [numeric](18, 0) NULL,
    [ROLA_AM_W_PARTNERSTWIE_TYP] [numeric](18, 0) NULL,
    [ROLA_AM_W_PARTNERSTWIE_OPIS] [varchar](255) NULL,
    [POTRZEBY_TECH_LOK] [varchar](255) NULL,
    [ZALACZNIK_EXCEL] [numeric](18, 0) NULL,
    [ZALACZNIK_RZETELNOSC_KALK] [numeric](18, 0) NULL,
    [OPIEKUN] [numeric] (18,0) NULL,


    [OPINIA_DYREKTORA_CTTM] [varchar](500) NULL,
    [OPINIA_NM] [varchar](500) NULL,
    [OPINIA_JEDN_REALIZUJACEJ] [varchar](500) NULL,
    [OPINIA_BB] [varchar](500) NULL,
    [OPINIA_KIER_PIONU] [varchar](500) NULL,
    [OPINIA_KIER_PIONU_2] [varchar](500) NULL,
    [OPINIA_PRAC_BZ] [varchar](500) NULL,
    [OPINIA_KWESTORA] [varchar](500) NULL,
    [OPINIA_PRAC_BC] [varchar](500) NULL,
    [OPINIA_KANCLERZA] [varchar](500) NULL,



    [ZAMAWIAJACY] [varchar](255) NULL,
    [DATA_ZAMOWIENIA] [date] NULL,
    [NUMER_ZAMOWIENIA] [varchar](100) NULL,
    [CHARAKTER_PRACY] [numeric](18, 0) NULL,
    --[PRACOWNICY_WYKONYWUJACY] [numeric](18, 0) NULL,

    [KONSORCJUM] [numeric](18, 0) NULL,

    PRIMARY KEY CLUSTERED ( [DOCUMENT_ID] ASC )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE [DSG_AMS_KARTA_OBIEGOWA_MULTIPLE_VALUE] (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);


--26.03.2014
--sp_rename 'DSG_AMS_KARTA_OBIEGOWA.NAZWA_PROJEKTU', 'TEMAT_PRACY', 'COLUMN'
--ALTER TABLE [DSG_AMS_KARTA_OBIEGOWA] DROP COLUMN [TYTUL_PROJEKTU]
--ALTER TABLE [DSG_AMS_KARTA_OBIEGOWA] ADD [KIEROWNIK_PROJEKTU_AUTHOR] [numeric](1, 0) NULL;
--07.04.2014
--ALTER TABLE [DSG_AMS_KARTA_OBIEGOWA] ADD [DYREKTOR_INSTYTUTU] [numeric](18, 0) NULL;
--ALTER TABLE [DSG_AMS_KARTA_OBIEGOWA] ADD [OSWIAD_O_RZETELNOSCI] [SMALLINT] NULL;