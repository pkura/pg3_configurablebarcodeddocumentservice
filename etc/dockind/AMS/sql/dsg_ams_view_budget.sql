	ALTER VIEW [dbo].[dsg_ams_view_budget] as
	SELECT * 
	 FROM (
			SELECT 
				convert(numeric(18, 0), bd_budzet_id) AS id,
				bd_budzet_idm AS cn,
				budzet_nazwa AS title,
				ROW_NUMBER() OVER (PARTITION BY bd_budzet_id ORDER BY bd_budzet_id) AS centrum,
				BINARY_CHECKSUM(dok_bd_str_budzet_idn,bd_rodzaj) AS refValue,
				1 AS available
			FROM dsg_ams_budget_copy where (bd_rodzaj = 'BK' or bd_rodzaj = 'PZB') and LTRIM(budzet_nazwa) not like 'SUMA%'
	)a WHERE a.centrum = 1;
GO