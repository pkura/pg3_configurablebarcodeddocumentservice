CREATE TABLE dsg_ams_asset_operation_type (
    ID BIGINT IDENTITY(1,1) NOT NULL,

    ERP_ID NUMERIC(18,0) NULL,
    IDM VARCHAR(50) NULL,
    NAZWA VARCHAR(100) NULL,
    AKTYWNY REAL NULL,
    ILOSC_DO_POZYCJI REAL NULL,
    WALUTOWY REAL NULL,
    WIELOPOZ REAL NULL,
    DOKUMENT NUMERIC (18,0) NULL,
    RODZAJ_OPE VARCHAR(50) NULL,
    RODZAJ_OPEPOZ VARCHAR(50) NULL,
    WALUTA_ID REAL NULL,
    WPLYW_OPERACJI NUMERIC(18,0) NULL,
    available bit default(1) not null
)

CREATE VIEW dsg_ams_asset_operation_type_view AS
	SELECT
		ID AS id,
		IDM AS cn,
		NAZWA AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM dsg_ams_asset_operation_type
	

CREATE TABLE dsg_ams_asset_operation_type_temp(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[refValue] varchar(20) NULL,
	[centrum] int NULL,
	[available] bit default 1 not null
);

CREATE VIEW [dbo].[dsg_ams_asset_operation_type_view] AS
	SELECT
		ID AS id,
		IDM AS cn,
		NAZWA AS title,
		null AS refValue,
		ERP_ID as centrum,
		available as available
	FROM dsg_ams_asset_operation_type where idm like 'LTT' or idm like 'LCT';
GO

add(SZMATY).
			add(WYSYPISKO).
			add(ZLOM).
			add(UTYLIZACJA).


insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Sprzeda�','SPRZEDAZ/DAROWIZNA', 1, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Zu�ycie','FIZYCZNA_LIKWIDACJA', 2, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Kradzie�','FIZYCZNA_LIKWIDACJA', 3, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Niedob�r','FIZYCZNA_LIKWIDACJA', 4, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Demonta�','FIZYCZNA_LIKWIDACJA', 5, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('ZLOM', 'Z�omowanie','FIZYCZNA_LIKWIDACJA', 6, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Przekwalifikowa','SPRZEDAZ/DAROWIZNA', 7, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER ', 'Przeniesienie ','SPRZEDAZ/DAROWIZNA', 8, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('OTHER', 'Przekazanie','SPRZEDAZ/DAROWIZNA', 9, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('WYSYPISKO', 'Przekazanie na wysypisko �mieci','FIZYCZNA_LIKWIDACJA', 10, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('SZMATY', 'Przekazanie na szmaty','FIZYCZNA_LIKWIDACJA', 11, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('ZLOM', 'Przekazanie na z�om','FIZYCZNA_LIKWIDACJA', 12, 1);
insert into dsg_ams_asset_operation_type_temp (cn,title,refValue,centrum,available) values ('UTYLIZACJA', 'Utylizacja materia��w specjalnych','FIZYCZNA_LIKWIDACJA', 13, 1);

