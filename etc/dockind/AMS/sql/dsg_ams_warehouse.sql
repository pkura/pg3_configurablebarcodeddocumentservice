CREATE TABLE dsg_ams_warehouse (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(250),
	title varchar(250),
	centrum int,
	refValue varchar(20),
	available bit,
	type tinyint, 
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


drop table dsg_ams_warehouse;


CREATE TABLE dsg_ams_warehouse (
	id numeric(18, 0) identity(1,1) NOT NULL,
	czy_aktywna numeric(18, 2),
	czy_bilansowy numeric(18, 2),
	id_from_webservice numeric(18, 2),
	identyfikator_num numeric(18, 2),
	idm varchar(50),
	magazyn_ids varchar(50),
	nazwa varchar(150),
	pracownik_id numeric(18, 2),
	available bit
)

create view dsg_ams_view_warehouse as
	select
		id,
		idm as cn,
		nazwa as title,
		NULL as centrum,
		NULL as refValue,
		available
	from dsg_ams_warehouse;