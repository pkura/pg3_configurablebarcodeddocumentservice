CREATE TABLE dsg_ams_wniosek_zmiana_budzetu (
	document_id numeric(18,0) NOT NULL,
	status numeric(18, 0),
	budzet numeric(18, 0),
	dysponent_zrodlowy numeric(18, 0),
	budzet_final numeric(18, 0),
	dysponent_docelowy numeric(18, 0),
	opis varchar(512),
	uzasadnienie_zmiany varchar(512),
	czy_opinie bit,
	jednostka_opinie numeric(18, 0),
	opinie varchar(512),
	czy_dodatkowe_opinie bit,
	jednostka_dodatkowe_opinie numeric(18, 0),
	dodatkowe_opinie varchar(512),
	czy_dodatkowe_opinie_second_trustee bit,
	jednostka_dodatkowe_opinie_second_trustee numeric(18, 0),
	dodatkowe_opinie_second_trustee varchar(512)
    PRIMARY KEY CLUSTERED ( document_id ASC )
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
) ON [PRIMARY];

CREATE TABLE dsg_ams_wniosek_zmiana_budzetu_multiple_value (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100)
);

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD opis_opinii varchar(512);

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_okres_budzetowy numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_komorka_budzetowa numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_budzet numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_pozycja_budzetu_raw varchar(256);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_zadanie_finansowe_budzetu_raw varchar(256);

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_final_okres_budzetowy numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_final_komorka_budzetowa numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_final_budzet numeric(18, 0);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_final_pozycja_budzetu_raw varchar(256);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD budget_final_zadanie_finansowe_budzetu_raw varchar(256);

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD is_two_trustee BIT;

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD opinia_dysponent_zr varchar(512);
ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD opinia_dysponent_doc varchar(512);

ALTER TABLE dsg_ams_wniosek_zmiana_budzetu ADD czy_rektor BIT;