	alter view dsg_ams_view_budget_cell as
	SELECT * FROM (
		SELECT
			BINARY_CHECKSUM(b.dok_bd_str_budzet_idn,b.bd_rodzaj) as ID,--convert(numeric(18, 0),bd_budzet_id) AS id,
			b.dok_bd_str_budzet_idn AS cn,
			b.dok_kom_nazwa AS title,
			ROW_NUMBER() OVER (PARTITION BY b.dok_bd_str_budzet_idn,b.bd_rodzaj ORDER BY b.dok_bd_str_budzet_idn) AS centrum,
			--kind.id AS refValue,
			CASE 
				WHEN b.bd_rodzaj = 'BK' THEN 1
				WHEN b.bd_rodzaj = 'PZB' THEN 2
				ELSE 0
			END as refValue,
			1 AS available
		FROM dsg_ams_budget_copy b
		--join DSG_AMS_B_KIND kind on b.bd_rodzaj=kind.cn
		where b.bd_rodzaj = 'BK' or b.bd_rodzaj = 'PZB'
	)a WHERE a.centrum = 1;
GO
