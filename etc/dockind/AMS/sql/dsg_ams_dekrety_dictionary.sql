CREATE TABLE dsg_ams_dekrety_dictionary (
	id numeric(18, 0) identity(1,1) NOT NULL,
	zlecenie numeric(18, 0),
	mpk numeric(18, 0),
	projekt numeric(18, 0),
	zrodlo_finansowania numeric(18, 0),
	produkt numeric(18, 0),
	sprzedaz_opodatkowana numeric(18, 0),
	netto numeric(18, 2),
	stawka numeric(18, 0),
	kwota_vat numeric(18, 2),
	brutto numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE dsg_ams_dekrety_dictionary ADD numer_pozycji numeric(18, 0);

ALTER TABLE dsg_ams_dekrety_dictionary ADD podzial_kosztow numeric(18, 0);


ALTER TABLE dsg_ams_dekrety_dictionary ADD kwota_vat_do_odliczenia numeric(18, 2);
ALTER TABLE dsg_ams_dekrety_dictionary ADD kwota_vat_koszty numeric(18, 2);

ALTER TABLE dsg_ams_dekrety_dictionary ADD ilosc numeric(12,3);
ALTER TABLE dsg_ams_dekrety_dictionary ADD jm varchar(50);
ALTER TABLE dsg_ams_dekrety_dictionary ADD opis varchar(50);
ALTER TABLE dsg_ams_dekrety_dictionary ADD uwagi varchar(4096);

ALTER TABLE dsg_ams_dekrety_dictionary ADD magazynowany bit
ALTER TABLE dsg_ams_dekrety_dictionary ADD srodek_trwaly bit