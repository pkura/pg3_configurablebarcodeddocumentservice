CREATE TABLE dsg_ams_orders_ERP (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(250),
	title varchar(250),
	centrum int,
	refValue varchar(20),
	available bit, 
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]