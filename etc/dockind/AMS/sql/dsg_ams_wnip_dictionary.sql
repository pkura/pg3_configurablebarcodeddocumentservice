CREATE TABLE dsg_ams_wnip_dictionary (
	id numeric(18, 0) identity(1,1) NOT NULL,
	number numeric(18, 0)

PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
