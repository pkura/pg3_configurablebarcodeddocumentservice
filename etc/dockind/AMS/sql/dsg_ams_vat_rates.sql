CREATE TABLE dsg_ams_vat_rates (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(250),
	title varchar(250),
	centrum int,
	refValue varchar(20),
	available bit, 
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO dsg_ams_vat_rates VALUES('RATE_1_23', 'Podstawowa (23%)', null, '0.23', 1);
INSERT INTO dsg_ams_vat_rates VALUES('RATE_2_22', 'Podstawowa (22%)', null, '0.22', 1);
INSERT INTO dsg_ams_vat_rates VALUES('RATE_3_8', 'Podatkowa (8%)', null, '0.08', 1);
INSERT INTO dsg_ams_vat_rates VALUES('RATE_4_7', 'Podatkowa (7%)', null, '0.07', 1);
INSERT INTO dsg_ams_vat_rates VALUES('RATE_5_5', 'Podatkowa (5%)', null, '0.05', 1);
INSERT INTO dsg_ams_vat_rates VALUES('RATE_6_3', 'Podatkowa (3%)', null, '0.03', 1);