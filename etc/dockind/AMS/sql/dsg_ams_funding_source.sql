CREATE TABLE dsg_ams_funding_source (
	id numeric(18, 0) identity(1,1) NOT NULL,
	numeric_id numeric(18, 2),
	idm varchar(50),
	source_name varchar(150)
)

create view dsg_ams_view_funding_source as
	select
		id,
		idm as cn,
		source_name as title,
		NULL as centrum,
		NULL as refValue,
		1 available
	from dsg_ams_funding_source;
	
ALTERT TABLE dsg_ams_funding_source ADD erpId numeric(18, 2);
UPDATE dsg_ams_funding_source SET erpId = 0;