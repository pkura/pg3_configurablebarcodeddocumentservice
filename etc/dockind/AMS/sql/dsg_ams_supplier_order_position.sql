create table dsg_ams_supplier_order_position
(
id numeric(18,0) identity,
supplier_order_id numeric(18,0),
supplier_order_idm varchar(100),
wytwor_idm varchar (100),
ilosc float,
nrpoz integer,
cena float,
cenabrutto float,
koszt float,
kwotnett float,
kwotvat float
);

alter table  dsg_ams_supplier_order_position add zamdospoz_id numeric(18,0);


ALTER TABLE dsg_ams_supplier_order_position ADD budzet_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD budzet_idm varchar(100);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_budzet_ko_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_budzet_ko_idm varchar(100);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_rodzaj_ko_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_plan_zam_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_plan_zam_idm varchar(100);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_rodzaj_plan_zam_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_rezerwacja_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_rezerwacja_idm varchar(100);
ALTER TABLE dsg_ams_supplier_order_position ADD bd_rezerwacja_poz_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD kontrakt_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD projekt_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD etap_id numeric(18, 0);
ALTER TABLE dsg_ams_supplier_order_position ADD zasob_id numeric(18, 0);