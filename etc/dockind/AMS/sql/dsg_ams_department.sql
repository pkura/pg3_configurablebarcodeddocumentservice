CREATE TABLE dsg_ams_department (
	id numeric(18, 0) identity(1,1) NOT NULL,
	czy_aktywna numeric(18, 2),
	erpId numeric(18, 2),
	identyfikator_num numeric(18, 2),
	idm varchar(50),
	nazwa varchar(100),
	nazwa_rozw varchar(100),
	available BIT
)

CREATE VIEW dsg_ams_view_department AS
	SELECT 
		id,
		idm AS cn,
		nazwa AS title,
		erpId AS centrum,
		null AS refValue,
		available
	FROM dsg_ams_department WHERE idm NOT LIKE '_BRAK'