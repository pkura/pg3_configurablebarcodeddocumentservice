CREATE TABLE dsg_ams_stawki_vat_faktury (
	id numeric(18, 0) identity(1,1) NOT NULL,
	netto numeric(18, 2),
	stawka numeric(18, 0),
	kwota_vat numeric(18, 2),
	brutto numeric(18, 2),
	dzialalnosc numeric(18, 0),
	obciazenie_ratio numeric(18, 2),
	obciazenie numeric(18, 2)
);