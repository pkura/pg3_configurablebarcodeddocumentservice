CREATE TABLE dsg_ams_supplier_order_header (
	id numeric(18, 0) identity(1,1) NOT NULL,
	dat_dok date,
	dat_odb date,
	dostawca_id numeric(18, 2),
	dostawca_idn varchar(100),
	id_from_webservice numeric(18, 2),
	idm varchar(50),
	opis_wewn varchar(150),
	uwagi varchar(150),
	wartdok numeric(18, 2)
)

CREATE VIEW dsg_ams_view_supplier_order_header AS
	SELECT
		id,
		idm as cn,
		idm as title,
		NULL as centrum,
		dostawca_id as refValue,
		1 as available
	FROM dsg_ams_supplier_order_header;