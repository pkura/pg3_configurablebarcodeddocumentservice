CREATE TABLE dsg_ams_mpk_dictionary (
	id numeric(18, 0) identity(1,1) NOT NULL,
	rodzaj numeric(18, 0),
	zlecenie numeric(18, 0),
	okres_budzetowy numeric(18, 0),
	komorka_budzetowa numeric(18, 0),
	pozycja_planu_zakupow numeric(18, 0),
	kontrakt numeric(18, 0),
	budzet_kontraktu numeric(18, 0),
	pozycja_w_budzecie numeric(18, 0),
	zadanie_finansowe numeric(18, 0),
	zrodlo_finansowania numeric(18, 0),
	netto numeric(18, 2),
	stawka numeric(18, 0),
	kwota_vat numeric(18, 2),
	brutto numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE dsg_ams_mpk_dictionary add rodzaj_budzetu numeric(18, 0)
ALTER TABLE dsg_ams_mpk_dictionary add identyfikator_planu_budzetu numeric(18, 0)
ALTER TABLE dsg_ams_mpk_dictionary add kwota_budzetu numeric(18, 2)

ALTER TABLE dsg_ams_mpk_dictionary add jednostka_wnioskujaca numeric(18, 0)

ALTER TABLE dsg_ams_mpk_dictionary ADD pozycjaid numeric(18, 0)

alter table dsg_ams_mpk_dictionary add projekt numeric(18, 0);

ALTER TABLE dsg_ams_mpk_dictionary ADD magazynowany bit;
ALTER TABLE dsg_ams_mpk_dictionary ADD srodek_trwaly bit;

ALTER TABLE dsg_ams_mpk_dictionary DROP COLUMN magazynowany
ALTER TABLE dsg_ams_mpk_dictionary DROP COLUMN srodek_trwaly

ALTER TABLE dsg_ams_mpk_dictionary ADD jednostka_miary numeric(18, 0);
ALTER TABLE dsg_ams_mpk_dictionary ADD ilosc_sztuk numeric(18, 0);

ALTER TABLE dsg_ams_mpk_dictionary ADD procent numeric(17, 2);

ALTER TABLE dsg_ams_mpk_dictionary ADD produkt int;
ALTER TABLE dsg_ams_mpk_dictionary ADD ilosc numeric(12,3);
ALTER TABLE dsg_ams_mpk_dictionary ADD jm varchar(50);
ALTER TABLE dsg_ams_mpk_dictionary ADD opis varchar(50);
ALTER TABLE dsg_ams_mpk_dictionary ADD uwagi varchar(4096);
ALTER TABLE dsg_ams_mpk_dictionary ALTER COLUMN opis varchar(400);
ALTER TABLE dsg_ams_mpk_dictionary add budzet int;
ALTER TABLE dsg_ams_mpk_dictionary add pozycja_budzetu_raw varchar(256);
ALTER TABLE dsg_ams_mpk_dictionary add pozycja_planu_zakupow_raw varchar(256);

ALTER TABLE dsg_ams_mpk_dictionary add zadanie_finansowe_planu_raw varchar(256);
ALTER TABLE dsg_ams_mpk_dictionary add zadanie_finansowe_budzetu_raw varchar(256);

ALTER TABLE dsg_ams_mpk_dictionary add komorka_budzetowa_bk int;
ALTER TABLE dsg_ams_mpk_dictionary add okres_budzetowy_bk int;

ALTER TABLE dsg_ams_mpk_dictionary add zrodlo_budzetu_raw varchar(256);