CREATE TABLE dsg_ams_mpk_do_wyj (
	id numeric(18, 0) identity(1,1) NOT NULL,
	zrodlo_finansowania numeric(18, 0),
	rodzaj_kosztu numeric(18, 0),
	centrumid numeric(18, 0),
	amount numeric(18, 2)
);