CREATE TABLE dsg_ams_reservation_position_dict (
	id numeric(18, 0) identity(1,1) NOT NULL,
	rezerwacja_idm varchar(100),
	nr_poz_rez int,
	wytwor_rez varchar(100),
	wytwor_nazwa_rez varchar(150),
	ilosc_rez numeric(18, 2),
	cena_rez numeric(18, 2),
	cena_brutto_rez numeric(18, 2),
	koszt_rez numeric(18, 2),
	kwota_netto_rez numeric(18, 2),
	kwota_vat_rez numeric(18, 2)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]