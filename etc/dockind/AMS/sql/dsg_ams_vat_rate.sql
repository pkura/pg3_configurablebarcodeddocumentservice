CREATE TABLE dsg_ams_vat_rate (
	id numeric(18, 0) identity(1,1) NOT NULL,
	czy_aktywna numeric(18, 2),
	czy_zwolniona numeric(18, 2),
	data_obow_do date,
	id_from_webservice numeric(18, 2),
	idm varchar(50),
	nazwa varchar(150),
	nie_podlega numeric(18, 2),
	procent numeric(18, 2),
	available bit,
)

CREATE VIEW dsg_ams_view_vat_rate AS
	SELECT
		id,
		LTRIM(RTRIM(idm)) AS cn,
		nazwa AS title,
		NULL AS centrum,
		NULL AS refValue,
		available,
		procent
	FROM dsg_ams_vat_rate;