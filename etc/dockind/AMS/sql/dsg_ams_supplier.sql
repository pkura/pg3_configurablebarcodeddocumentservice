CREATE TABLE dsg_ams_supplier (
	id numeric(18, 0) identity(1,1) NOT NULL,
	available bit,
	erpId numeric(18, 2),
	idm varchar(50),
	identyfikatorNum numeric(18, 2),
	klasdostId numeric(18, 2),
	klasdostIdn varchar(50),
	nazwa varchar(300),
	nip varchar(50),
	miasto varchar(50),
	kod_pocztowy varchar(50),
	ulica varchar(50),
	nr_domu varchar(50),
	nr_mieszkania varchar(50)
)