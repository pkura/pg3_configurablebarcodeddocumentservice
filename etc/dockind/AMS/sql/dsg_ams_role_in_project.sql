CREATE TABLE dsg_ams_role_in_project (
	id numeric(18, 0) identity(1,1) NOT NULL,
	bp_rola_id numeric(18, 2),
	bp_rola_idn varchar(50),
	bp_rola_kontrakt_id numeric(18, 2),
	czy_blokada_prac numeric(18, 2),
	czy_blokada_roli numeric(18, 2),
	kontrakt_id numeric(18, 2),
	kontrakt_idm varchar(50),
	pracownik_id numeric(18, 2),
	pracownik_idn varchar(50),
	pracownik_nrewid numeric(18, 0)
)

alter table dsg_ams_role_in_project add osobaGuid varchar(150);