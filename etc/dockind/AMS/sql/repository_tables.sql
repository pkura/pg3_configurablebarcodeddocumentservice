create table dsd_dictionary (
	id int identity(1,1) not null,
	deleted numeric(1,0) default 0,
	code varchar(64),
	tablename varchar(64),
	dockind_cn varchar(50) not null,
	dict_source int,
	cn varchar(64),
	ref_dict int,
	is_control_dict numeric(1,0) default 0,
	PRIMARY KEY (id),
	CONSTRAINT REFERENCE_DICT FOREIGN KEY (ref_dict) REFERENCES dsd_dictionary (id)
);
CREATE UNIQUE INDEX dsd_dictionary_id
ON dsd_dictionary (id);

create table dsd_simple_repository_attribute (
	id int identity(1,1) not null,
	available numeric(1,0) not null default(1),
	used numeric(1,0) not null default(1),
	table_name varchar(128) null,
	context_id int null,
	objectid int null,
	czy_aktywny int null,
	czy_blokada int null,
	nazwa varchar(128) null,
	obiekt_systemu_id bigint null,
	rep_atrybut_id bigint null,
	rep_atrybut_ido varchar(128) null,
	rep_disp_atr_id bigint null,
	rep_klasa_id bigint null,
	rep_typ_id bigint null,
	rep_typ_prosty_ido varchar(128) null,
	rep_wartosc_id bigint null,
	rodzaj int null,
	rodzaj_typu int null,
	rodzaj_wartosci int null,
	typ_dlugosc int null,
	typ_format varchar(128) null,
	typ_wartosci int null,
	wartosc_date datetime null,
	wartosc_decimal numeric(18,6) null,
	wartosc_instancja_id bigint null,
	wartosc_long int null,
	wartosc_string varchar(128) null
);
CREATE UNIQUE INDEX dsd_simple_repository_attribute_id
ON dsd_simple_repository_attribute (id);