CREATE TABLE dsg_ams_financial_document (
	id numeric(18, 0) identity(1,1) NOT NULL,
	data_platnosci date,
	datdok date,
	datoper date,
	erpId numeric(18, 2),
	idm varchar(50),
	imie varchar(50),
	nazwisko varchar(50),
	pesel varchar(50),
	pracownik_nrewid numeric(18, 0),
	kurs numeric(18, 2),
	kwota numeric(18, 2),
	kwotawalbaz numeric(18, 2),
	status numeric(18, 0)
)

CREATE VIEW dsg_ams_view_financial_document AS
	SELECT
		id,
		idm AS cn,
		idm AS title,
		NULL AS centrum,
		NULL AS refValue,
		1 AS available
	FROM dsg_ams_financial_document
	
	
DROP VIEW dsg_ams_view_financial_document

 CREATE VIEW dsg_ams_view_financial_document AS
	SELECT
		id,
		idm AS cn,
		idm AS title,
		NULL AS centrum,
		pracownik_nrewid AS refValue,
		1 AS available
	FROM dsg_ams_financial_document