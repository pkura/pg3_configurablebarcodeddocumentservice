CREATE TABLE dsg_ams_raty (
	id numeric(18, 0) identity(1,1) NOT NULL,
	data date,
	kwota numeric(18, 2)
);