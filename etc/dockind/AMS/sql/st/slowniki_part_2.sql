SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_cost_center](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_cost_center] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_settlement_sub_period](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_settlement_sub_period] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dsd_ams_asset_cost_center] ON 

GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052', N'Uczelniane Centrum Informatyczne 
 - Warlikowski Ł', 260, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-056', N'.', 264, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-157', N'Morski Ośrodek Szkoleniowy w Kołobrzegu -serwerown', 265, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-158', N'Ośrodek Szkoleniowy Rybołówstwa Bałtyckiego-serwer', 266, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-207', N'Uczelniane Centrum Informatyczne-Domy studenckie', 267, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-211', N'Uczelniane Centrum Informatyczne-administracja', 268, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-213', N'Uczelniane Centrum Informatyczne-Obiekt Dydaktyczn', 270, NULL, 1, 7)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-221', N'Uczelniane Centrum Informatyczne-SDM Korab', 271, NULL, 1, 8)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-222', N'Uczelniane Centrum Informatyczne-SDM Pasat', 272, NULL, 1, 9)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-223', N'Uczelniane Centrum Informatyczne-Obiekt Dydaktyczn', 273, NULL, 1, 10)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'055', N'Dział Wydawnictw', 276, NULL, 1, 11)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'058', N'Biblioteka Główna 
Osoba Odp.Edelman Elżbieta', 277, NULL, 1, 12)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'104', N'Studium Nauki Języków Obcych', 278, NULL, 1, 13)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'104-211', N'Sala Senatu - Aparatura Nagłaśniająca', 279, NULL, 1, 14)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'105', N'Studium Wychowania Fizycznego i Sportu', 280, NULL, 1, 15)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'105-001', N'Studium Wychowania Fizycznego i Sportu-sprzęt do n', 281, NULL, 1, 16)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'105-211', N'MAGAZYN SPRZĘTU SPORTOWEGO', 282, NULL, 1, 17)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'152', N'Statek „Nawigator XXI”', 283, NULL, 1, 18)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'152-151', N'Sekcja Eksploatacji Statku -Nawigator XXI', 284, NULL, 1, 19)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'154', N'Studium Doskonalenia Kadr Oficerskich', 285, NULL, 1, 20)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'154-227', N'.', 286, NULL, 1, 21)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'155', N'Ośrodek Szkoleniowy Ratownictwa Morskiego', 287, NULL, 1, 22)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'156', N'Morski Ośrodek Szkoleniowy w Śwonoujściu', 288, NULL, 1, 23)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'157', N'Morski Ośrodek Szkoleniowy w Kołobrzegu', 289, NULL, 1, 24)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'158', N'Ośrodek Szkoleniowy Rybołówstwa Bałtyckiego w Koło', 290, NULL, 1, 25)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'159', N'Maritime English Center', 291, NULL, 1, 26)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'202-152', N'Dział Techniczny', 292, NULL, 1, 27)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-211', N'Obiekt Dydaktyczno-Administracyjny ul. Wały Chrobr', 293, NULL, 1, 28)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-211-13', N'Magazyn Główny-wyposażenie', 294, NULL, 1, 29)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-211-14', N'Klub "POD MASZTAMI"', 295, NULL, 1, 30)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-212', N'Obiekt Dydaktyczny ul. Podgórna', 299, NULL, 1, 31)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-213', N'Obiekt Dydaktyczny ul. Żołnierska', 300, NULL, 1, 32)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-215', N'Obiekt Dydaktyczny ul. Żołnierska-mieszkania', 301, NULL, 1, 33)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-000', N'Osiedle Akademickie-Domy Studenckie -Narzędzia', 302, NULL, 1, 34)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-221', N'Studencki Dom Marynarza KORAB', 303, NULL, 1, 35)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-221-106', N'Studencki Dom Marynarza KORAB-Samorząd Studencki', 304, NULL, 1, 36)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-222', N'Studencki Dom Marynarza PASAT
Osoba Odp.Zubel Joan', 305, NULL, 1, 37)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-223', N'Obiekt Dydaktyczny ul. Pobożnego', 306, NULL, 1, 38)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-224', N'Pływalnia', 307, NULL, 1, 39)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-226', N'Stołówka ul.  Szczerbcowa', 308, NULL, 1, 40)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-227', N'Sale dydaktyczne w stołówce', 309, NULL, 1, 41)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-230', N'Studencki Dom Marynarza PASAT-pomieszczenia admini', 310, NULL, 1, 42)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'208', N'Dom Pracy Twórczej w Świnoujściu', 311, NULL, 1, 43)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'251', N'Dziekanat Wydziału Nawigacyjnego', 312, NULL, 1, 44)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252', N'Instytut Nawigacji Morskiej', 313, NULL, 1, 45)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-002', N'Instytut Nawigacji Morskiej-Sekretariat', 314, NULL, 1, 46)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-030', N'Instytut Nawigacji Morskiej-Pracownia cybernetyki ', 315, NULL, 1, 47)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-031', N'Instytut Nawigacji Morskiej-Pracownia Meteorologii', 316, NULL, 1, 48)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-032', N'Instytut Nawigacji Morskiej-Planetarium i pracowni', 317, NULL, 1, 49)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-033', N'Instytut Nawigacji Morskiej-Pracownia Podstaw Nawi', 318, NULL, 1, 50)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-241', N'Zakład Nawigacji Morskiej', 319, NULL, 1, 51)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-242', N'Zakład Budowy i Stateczności Statku', 320, NULL, 1, 52)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-243', N'Zakład Meteorologii i Oceanografii', 321, NULL, 1, 53)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-244', N'Zakład Ratownictwa i Ochrony Żeglugi ul.Ludowa', 322, NULL, 1, 54)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'252-245', N'Centrum Technologii Przewozów LNG', 323, NULL, 1, 55)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253', N'Instytut Inżynierii Ruchu Morskiego', 324, NULL, 1, 56)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-002', N'Instytut Inżynierii Ruchu Morskiego-Sekretariat', 325, NULL, 1, 57)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-034', N'Instytut Inżynierii Ruchu Morskiego-Pracownia Symu', 326, NULL, 1, 58)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-035', N'Instytut Inżynierii Ruchu Morskiego-Pomoce Naukowe', 327, NULL, 1, 59)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-036', N'Instytut Inżynierii Ruchu Morskiego-PracowniaTechn', 328, NULL, 1, 60)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-037', N'Instytut Inżynierii Ruchu Morskiego-Pracownia Żyro', 329, NULL, 1, 61)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-251', N'Zakład Manewrowania i Pilotażu Morskiego', 330, NULL, 1, 62)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-252', N'Zakład Bezpieczeństwa Nawigacyjnego', 331, NULL, 1, 63)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-253', N'Zakład Urządzeń Nawigacyjnych', 332, NULL, 1, 64)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'253-254', N'Zakład Rybołówstwa Morskiego', 333, NULL, 1, 65)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'254', N'Instytut Technologii Morskich
Osoba Odp.Uriasz Jan', 334, NULL, 1, 66)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'254-002', N'Instytut Technologii Morskich - Sekretariat', 335, NULL, 1, 67)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'254-261', N'Zakład Informatycznych Technologii Morskich', 336, NULL, 1, 68)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'254-262', N'Zakład Komunikacyjnych Technologii Morskich', 337, NULL, 1, 69)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'254-263', N'Zakład Matematyki', 338, NULL, 1, 70)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'255', N'Instytut Geoinformatyki', 339, NULL, 1, 71)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'256', N'Centrum Inżynierii Ruchu Morskiego', 340, NULL, 1, 72)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'301', N'Dziekanat Wydziału Mechanicznego', 341, NULL, 1, 73)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'302', N'Instytut Podstawowych Nauk Technicznych
Osoba Odp.', 342, NULL, 1, 74)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'302-022', N'Instytut Podstawowych Nauk Technicznych-laboratori', 343, NULL, 1, 75)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'302-023', N'Instytut Podstawowych Nauk Technicznych-warsztaty ', 344, NULL, 1, 76)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'302-024', N'Instytut Podstawowych Nauk Technicznych- Narzędzio', 345, NULL, 1, 77)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'302-311', N'Zakład Mechaniki Technicznej', 346, NULL, 1, 78)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303', N'Instytut Eksploatacji Siłowni Okrętowych', 347, NULL, 1, 79)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-002', N'Instytut Eksploatacji Siłowni Okrętowych-Sekretari', 348, NULL, 1, 80)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-025', N'Instytut Eksploatacji Siłowni Okrętowych -Laborato', 349, NULL, 1, 81)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-026', N'Instytut Eksploatacji Siłowni Okrętowych -Laborato', 350, NULL, 1, 82)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-027', N'Instytut Eksploatacji Siłowni Okrętowych-Pracownia', 351, NULL, 1, 83)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-028', N'Instytut Eksploatacji Siłowni Okrętowych- Zespół B', 352, NULL, 1, 84)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-029', N'Instytut Eksploatacji Siłowni Okrętowych-Laborator', 353, NULL, 1, 85)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-321', N'Zakład Maszyn i Urządzeń Okrętowych', 354, NULL, 1, 86)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-322', N'Zakład Siłowni Okrętowych', 355, NULL, 1, 87)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-323', N'Laboratorium Siłowni Okrętowych', 356, NULL, 1, 88)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'304', N'Instytut Elektrotechniki i Automatyki Okrętowej', 357, NULL, 1, 89)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'304-002', N'Instytut Elektrotechniki i Automatyki Okrętowej-Se', 358, NULL, 1, 90)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'304-331', N'Zakład Elektrotechniki i Elektroniki Okrętowej', 359, NULL, 1, 91)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'304-331-11', N'Zakład Elektrotechniki i Elektroniki Okrętowej-Wyp', 360, NULL, 1, 92)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'304-332', N'Zakład Automatyki i Robotyki', 361, NULL, 1, 93)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'305', N'Katedra Diagnostyki i Remontów Maszyn', 362, NULL, 1, 94)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'306', N'Katedra Fizyki i Chemii', 363, NULL, 1, 95)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'351', N'Dziekanat Wydziału Inżynieryjno-Ekonomicznego.Tran', 364, NULL, 1, 96)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'352', N'Pracownia Analiz i Prognoz Ekonomicznych Sektora G', 365, NULL, 1, 97)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'353', N'Instytut Inżynierii Transportu', 366, NULL, 1, 98)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'353-002', N'Instytut Inżynierii Transportu-Sekretariat', 367, NULL, 1, 99)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'353-362', N'Zakład Techniki Transportu', 368, NULL, 1, 100)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'353-363', N'Zakład Technologii Transportu Zintegrowanego i Och', 369, NULL, 1, 101)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'353-364', N'Zakład Towaroznawstwa i Zarządzania Jakością', 370, NULL, 1, 102)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354', N'Instytut Zarządzania Transportem', 371, NULL, 1, 103)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-002', N'Instytut Zarządzania Transportem-Sekretariat', 372, NULL, 1, 104)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-021', N'Zakład Logistyki i Systemów Transportowych-studenc', 373, NULL, 1, 105)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-371', N'Zakład Nauk Ekonomicznych i Społecznych', 374, NULL, 1, 106)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-372', N'Zakład Organizacji i Zarządzania', 375, NULL, 1, 107)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-373', N'Zakład Logistyki i Systemów Transportowych', 376, NULL, 1, 108)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'354-375', N'Zakład Metod Ilościowych i Prognozowania', 378, NULL, 1, 109)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'901-354', N'Projekt PO KL-Europejski Logistyk-EFS', 379, NULL, 1, 110)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'902-301', N'Projekt SPO RYBY Opracowanie wytycznych do moderni', 380, NULL, 1, 111)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'903-301', N'"Projekt POKL Kierunki Zamawiane-Mechatronika II""', 381, NULL, 1, 112)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'904-301', N'"Projekt POKL Kierunki Zamawiane-Mechatronika I"""', 382, NULL, 1, 113)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'905-301', N'Projekt POKL Rozwój i promocja kierunków techniczn', 383, NULL, 1, 114)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'906-303', N'"Projekt PO RYBY ""Zmniejszenie zużycia paliwa"""', 384, NULL, 1, 115)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'907-303', N'"Projekt SPO RYBY ""Zastosowanie paliw pochodzenia', 385, NULL, 1, 116)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'908-303', N'"Projekt BSR ""Innoship"""', 386, NULL, 1, 117)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'909-303', N'"Projekt ZPORR ""Doposażenie i modernizacja symula', 387, NULL, 1, 118)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'909-322', N'Instytut Eksploatacji Siłowni Okrętowych -Laborato', 388, NULL, 1, 119)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'910-211', N'"Projekt RPO ""Zielona Energetyka-Rozwój Bazy B+R ', 389, NULL, 1, 120)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'910-304', N'"Projekt RPO ""Zielona Energetyka-Rozwój Bazy B+R ', 390, NULL, 1, 121)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'911-252', N'"Projekt POKL ""Cała Naprzód"""', 391, NULL, 1, 122)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'912-252', N'"Projekt""Budowa Centrum Naukowo-Badawczego Analiz', 392, NULL, 1, 123)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'913-253', N'"Projekt ""Transfer kompetencji-morskich,technol.i', 393, NULL, 1, 124)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'914-253', N'"Projekt PO RYBY ""Badania eksperymentalne dorsza"', 394, NULL, 1, 125)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'915-211', N'"Projekt RPO ""Budowa Centrum Symulacyjnego Termin', 395, NULL, 1, 126)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'915-253', N'"Projekt RPO ""Budowa Centrum Symulacyjnego Termin', 396, NULL, 1, 127)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'916-253', N'"Projekt POIG ""Budowa Systemu Pilotowo-Dokującego', 397, NULL, 1, 128)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'917-211', N'"Projekt POIG ""Centrum Technologii Nawigacyjnej G', 398, NULL, 1, 129)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'917-256', N'"Projekt POIG ""Centrum Technologii Nawigacyjnej G', 399, NULL, 1, 130)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'918-211', N'"Projekt ZPORR ""Budowa Centrum Inżynierii Ruchu M', 400, NULL, 1, 131)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'918-256', N'"Projekt ZPORR ""Budowa Centrum Inżynierii Ruchu M', 401, NULL, 1, 132)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'919-158', N'"Projekt RPO ""Uruchomienie Ośrodka Szkoleniowego ', 402, NULL, 1, 133)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'920-157', N'"Projekt POKL ""Ekoturystyka Żeglarska"""', 403, NULL, 1, 134)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'921-051', N'InMor', 404, NULL, 1, 135)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'922-058', N'"Projekt POIG ""Świat Morskich Publikacji:"""', 405, NULL, 1, 136)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'923-058', N'"Projekt ZPORR ""Czytelnia multimedialna"""', 406, NULL, 1, 137)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'924-052', N'"Projekt ""Morskie Opowieści"""', 407, NULL, 1, 138)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'925-200', N'"Projekt ""Nauka-Nowoczesna Administracja Uczelni ', 408, NULL, 1, 139)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'926-353', N'"Projekt ""OVERSIZE BALTIC"""', 409, NULL, 1, 140)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'927-302', N'"Projekt POKL ""Autoryzowane Szkolenia AUTOCAD II"', 410, NULL, 1, 141)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'928-254', N'"Projekt POKL ""Kierunki Zamawiane - Informatyka""', 411, NULL, 1, 142)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'929-104', N'Z angielskim w świat biznesu"', 412, NULL, 1, 143)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'930-159', N'Maritime English Center-grant IMTT', 413, NULL, 1, 144)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'931-303', N'Projekt "Przeprowadzenie ekspertyz', 414, NULL, 1, 145)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'932-303', N'Projekt "Przeprowadzenie audytów', 415, NULL, 1, 146)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'933-151', N'Projekt "Opracowanie programów szkoleniowych', 416, NULL, 1, 147)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'934-302', N'Projekt "AUTOCAD"', 417, NULL, 1, 148)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206', N'AG-przeznaczone do sprzedaży , dzierżawione', 418, NULL, 1, 149)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'206-214', N'Obiekt Dydaktyczny Willowa', 419, NULL, 1, 150)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'207-222-106', N'Studencki Dom Marynarza PASAT-Samorząd Studencki', 420, NULL, 1, 151)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'058-101', N'Biblioteka Główna-Studenci', 421, NULL, 1, 152)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'052-227', N'Sale dydaktyczne w stołówce-kompleks dydaktyczno-k', 422, NULL, 1, 153)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'101', N'Dział Nauczania', 423, NULL, 1, 154)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'303-020', N'Instytut Eksploatacji Siłowni Okrętowych-Laborator', 424, NULL, 1, 155)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'935-303', N'Projekt POKL Marine Power', 425, NULL, 1, 156)
GO
INSERT [dsd_ams_asset_cost_center] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'935-354', N'Projekt GRASS-Norweski Mechanizm Finansowy', 426, NULL, 1, 157)
GO
SET IDENTITY_INSERT [dsd_ams_asset_cost_center] OFF
GO
SET IDENTITY_INSERT [dsd_ams_asset_settlement_sub_period] ON 

GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2013 01/31/2013', 1, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2013 02/28/2013', 2, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2013 03/31/2013', 3, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2013 04/30/2013', 4, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2013 05/31/2013', 5, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2013 06/30/2013', 6, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2013 07/31/2013', 7, NULL, 1, 7)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2013 08/31/2013', 8, NULL, 1, 8)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2013 09/30/2013', 9, NULL, 1, 9)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2013 10/31/2013', 10, NULL, 1, 10)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2013 11/30/2013', 11, NULL, 1, 11)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2013 12/31/2013', 12, NULL, 1, 12)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2014 01/31/2014', 13, NULL, 1, 13)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2014 02/28/2014', 14, NULL, 1, 14)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2014 03/31/2014', 15, NULL, 1, 15)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2014 04/30/2014', 16, NULL, 1, 16)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2014 05/31/2014', 17, NULL, 1, 17)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2014 06/30/2014', 18, NULL, 1, 18)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2014 07/31/2014', 19, NULL, 1, 19)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2014 08/31/2014', 20, NULL, 1, 20)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2014 09/30/2014', 21, NULL, 1, 21)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2014 10/31/2014', 22, NULL, 1, 22)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2014 11/30/2014', 23, NULL, 1, 23)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2014 12/31/2014', 24, NULL, 1, 24)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2015 01/31/2015', 25, NULL, 1, 25)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2015 02/28/2015', 26, NULL, 1, 26)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2015 03/31/2015', 27, NULL, 1, 27)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2015 04/30/2015', 28, NULL, 1, 28)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2015 05/31/2015', 29, NULL, 1, 29)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2015 06/30/2015', 30, NULL, 1, 30)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2015 07/31/2015', 31, NULL, 1, 31)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2015 08/31/2015', 32, NULL, 1, 32)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2015 09/30/2015', 33, NULL, 1, 33)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2015 10/31/2015', 34, NULL, 1, 34)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2015 11/30/2015', 35, NULL, 1, 35)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2015 12/31/2015', 36, NULL, 1, 36)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2016 01/31/2016', 37, NULL, 1, 37)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2016 02/29/2016', 38, NULL, 1, 38)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2016 03/31/2016', 39, NULL, 1, 39)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2016 04/30/2016', 40, NULL, 1, 40)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2016 05/31/2016', 41, NULL, 1, 41)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2016 06/30/2016', 42, NULL, 1, 42)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2016 07/31/2016', 43, NULL, 1, 43)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2016 08/31/2016', 44, NULL, 1, 44)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2016 09/30/2016', 45, NULL, 1, 45)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2016 10/31/2016', 46, NULL, 1, 46)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2016 11/30/2016', 47, NULL, 1, 47)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2016 12/31/2016', 48, NULL, 1, 48)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2017 01/31/2017', 49, NULL, 1, 49)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2017 02/28/2017', 50, NULL, 1, 50)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2017 03/31/2017', 51, NULL, 1, 51)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2017 04/30/2017', 52, NULL, 1, 52)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2017 05/31/2017', 53, NULL, 1, 53)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2017 06/30/2017', 54, NULL, 1, 54)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2017 07/31/2017', 55, NULL, 1, 55)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2017 08/31/2017', 56, NULL, 1, 56)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2017 09/30/2017', 57, NULL, 1, 57)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2017 10/31/2017', 58, NULL, 1, 58)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2017 11/30/2017', 59, NULL, 1, 59)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2017 12/31/2017', 60, NULL, 1, 60)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2018 01/31/2018', 61, NULL, 1, 61)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2018 02/28/2018', 62, NULL, 1, 62)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2018 03/31/2018', 63, NULL, 1, 63)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2018 04/30/2018', 64, NULL, 1, 64)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2018 05/31/2018', 65, NULL, 1, 65)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2018 06/30/2018', 66, NULL, 1, 66)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2018 07/31/2018', 67, NULL, 1, 67)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2018 08/31/2018', 68, NULL, 1, 68)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2018 09/30/2018', 69, NULL, 1, 69)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2018 10/31/2018', 70, NULL, 1, 70)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2018 11/30/2018', 71, NULL, 1, 71)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2018 12/31/2018', 72, NULL, 1, 72)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2019 01/31/2019', 73, NULL, 1, 73)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2019 02/28/2019', 74, NULL, 1, 74)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2019 03/31/2019', 75, NULL, 1, 75)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2019 04/30/2019', 76, NULL, 1, 76)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2019 05/31/2019', 77, NULL, 1, 77)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2019 06/30/2019', 78, NULL, 1, 78)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2019 07/31/2019', 79, NULL, 1, 79)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2019 08/31/2019', 80, NULL, 1, 80)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2019 09/30/2019', 81, NULL, 1, 81)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2019 10/31/2019', 82, NULL, 1, 82)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2019 11/30/2019', 83, NULL, 1, 83)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2019 12/31/2019', 84, NULL, 1, 84)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2020 01/31/2020', 85, NULL, 1, 85)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2020 02/29/2020', 86, NULL, 1, 86)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2020 03/31/2020', 87, NULL, 1, 87)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2020 04/30/2020', 88, NULL, 1, 88)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2020 05/31/2020', 89, NULL, 1, 89)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2020 06/30/2020', 90, NULL, 1, 90)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2020 07/31/2020', 91, NULL, 1, 91)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2020 08/31/2020', 92, NULL, 1, 92)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2020 09/30/2020', 93, NULL, 1, 93)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2020 10/31/2020', 94, NULL, 1, 94)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2020 11/30/2020', 95, NULL, 1, 95)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2020 12/31/2020', 96, NULL, 1, 96)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2021 01/31/2021', 97, NULL, 1, 97)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2021 02/28/2021', 98, NULL, 1, 98)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2021 03/31/2021', 99, NULL, 1, 99)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2021 04/30/2021', 100, NULL, 1, 100)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2021 05/31/2021', 101, NULL, 1, 101)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2021 06/30/2021', 102, NULL, 1, 102)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2021 07/31/2021', 103, NULL, 1, 103)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2021 08/31/2021', 104, NULL, 1, 104)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2021 09/30/2021', 105, NULL, 1, 105)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2021 10/31/2021', 106, NULL, 1, 106)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2021 11/30/2021', 107, NULL, 1, 107)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2021 12/31/2021', 108, NULL, 1, 108)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2022 01/31/2022', 109, NULL, 1, 109)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2022 02/28/2022', 110, NULL, 1, 110)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2022 03/31/2022', 111, NULL, 1, 111)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2022 04/30/2022', 112, NULL, 1, 112)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2022 05/31/2022', 113, NULL, 1, 113)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2022 06/30/2022', 114, NULL, 1, 114)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2022 07/31/2022', 115, NULL, 1, 115)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2022 08/31/2022', 116, NULL, 1, 116)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2022 09/30/2022', 117, NULL, 1, 117)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2022 10/31/2022', 118, NULL, 1, 118)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2022 11/30/2022', 119, NULL, 1, 119)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2022 12/31/2022', 120, NULL, 1, 120)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'01/01/2023 01/31/2023', 121, NULL, 1, 121)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2', N'02/01/2023 02/28/2023', 122, NULL, 1, 122)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3', N'03/01/2023 03/31/2023', 123, NULL, 1, 123)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'4', N'04/01/2023 04/30/2023', 124, NULL, 1, 124)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'5', N'05/01/2023 05/31/2023', 125, NULL, 1, 125)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'6', N'06/01/2023 06/30/2023', 126, NULL, 1, 126)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'7', N'07/01/2023 07/31/2023', 127, NULL, 1, 127)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'8', N'08/01/2023 08/31/2023', 128, NULL, 1, 128)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'9', N'09/01/2023 09/30/2023', 129, NULL, 1, 129)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'10', N'10/01/2023 10/31/2023', 130, NULL, 1, 130)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'11', N'11/01/2023 11/30/2023', 131, NULL, 1, 131)
GO
INSERT [dsd_ams_asset_settlement_sub_period] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'12', N'12/01/2023 12/31/2023', 132, NULL, 1, 132)
GO
SET IDENTITY_INSERT [dsd_ams_asset_settlement_sub_period] OFF
GO
