SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_classification](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_classification] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_obtainment_form](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_obtainment_form] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_ownership_form](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_ownership_form] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_purpose](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_purpose] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dsd_ams_asset_responsible_person](
	[cn] [varchar](25) NULL,
	[title] [varchar](50) NULL,
	[centrum] [float] NULL,
	[refValue] [int] NULL,
	[available] [int] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_dsd_ams_asset_responsible_person] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dsd_ams_asset_classification] ON 

GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'010', N'Grunty orne', 4, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'011', N'Sady', 5, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'012', N'Łąki trwałe', 6, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'013', N'Pastwiska trwałe', 7, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'020', N'Lasy', 9, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'021', N'Grunty zadrzewione i zakrzewione', 10, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'030', N'Tereny mieszkaniowe', 12, NULL, 1, 7)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'031', N'Tereny przemysłowe', 13, NULL, 1, 8)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'032', N'Tereny zabudowane inne', 14, NULL, 1, 9)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'033', N'Zurbanizowane tereny nie zabudowane', 15, NULL, 1, 10)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'034', N'Tereny rekreacyjno-wypoczynkowe', 16, NULL, 1, 11)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'035', N'Użytki kopalne', 17, NULL, 1, 12)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'036', N'Tereny komunikacyjne', 18, NULL, 1, 13)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'040', N'Użytki ekologiczne', 20, NULL, 1, 14)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'050', N'Tereny różne', 22, NULL, 1, 15)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'060', N'Nieużytki', 24, NULL, 1, 16)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'070', N'Wody', 26, NULL, 1, 17)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'101', N'Budynki przemysłowe', 29, NULL, 1, 18)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'102', N'Budynki transportu i łączności', 30, NULL, 1, 19)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'103', N'Budynki handlowo-usługowe', 31, NULL, 1, 20)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'104', N'Zbiorniki, silosy i budynki magazynowe', 32, NULL, 1, 21)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'105', N'Budynki biurowe', 33, NULL, 1, 22)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'106', N'Budynki szpitali i zakładów opieki medycznej', 34, NULL, 1, 23)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'107', N'Budynki oświaty, nauki i kultury oraz budynki spor', 35, NULL, 1, 24)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'108', N'Budynki produkcyjne, usługowe i gospodarcze dla ro', 36, NULL, 1, 25)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'109', N'Inne budynki niemieszkalne', 37, NULL, 1, 26)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'110', N'Budynki mieszkalne', 39, NULL, 1, 27)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'121', N'Lokale niemieszkalne', 41, NULL, 1, 28)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'122', N'Lokale mieszkalne', 42, NULL, 1, 29)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'200', N'Budowle dla górnictwa i kopalnictwa', 45, NULL, 1, 30)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'201', N'Elektrownie', 46, NULL, 1, 31)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'202', N'Zakłady chemiczne', 47, NULL, 1, 32)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'203', N'Zakłady przemysłu ciężkiego, gdzie indziej nie skl', 48, NULL, 1, 33)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'210', N'Rurociągi i linie telekomunikacyjne oraz linie ele', 50, NULL, 1, 34)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'211', N'Rurociągi sieci rozdzielczej oraz linie kablowe ro', 51, NULL, 1, 35)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'220', N'Autostrady, drogi ekspresowe, ulice i drogi pozost', 53, NULL, 1, 36)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'221', N'Drogi kolejowe', 54, NULL, 1, 37)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'222', N'Drogi lotniskowe', 55, NULL, 1, 38)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'223', N'Mosty, wiadukty, estakady, tunele i przejścia nadz', 56, NULL, 1, 39)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'224', N'Budowle wodne, z wyjątkiem melioracji', 57, NULL, 1, 40)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'225', N'Melioracje podstawowe', 58, NULL, 1, 41)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'226', N'Melioracje szczegółowe', 59, NULL, 1, 42)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'290', N'Obiekty sportowe i rekreacyjne', 61, NULL, 1, 43)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'291', N'Obiekty inżynierii lądowej i wodnej pozostałe, gdz', 62, NULL, 1, 44)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'310', N'Kotły grzejne wodne', 65, NULL, 1, 45)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'311', N'Kotły grzejne parowe', 66, NULL, 1, 46)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'312', N'Kotły dautermowe', 67, NULL, 1, 47)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'313', N'Kotły parowe', 68, NULL, 1, 48)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'314', N'Inne kotły grzejne i parowe', 69, NULL, 1, 49)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'320', N'Turbiny parowe', 71, NULL, 1, 50)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'321', N'Turbiny wodne', 72, NULL, 1, 51)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'322', N'Maszyny parowe tłokowe', 73, NULL, 1, 52)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'323', N'Silniki spalinowe na paliwo lekkie', 74, NULL, 1, 53)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'324', N'Silniki spalinowe na paliwo ciężkie', 75, NULL, 1, 54)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'325', N'Silniki spalinowe na paliwo gazowe', 76, NULL, 1, 55)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'326', N'Silniki powietrzne', 77, NULL, 1, 56)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'327', N'Koła wodne', 78, NULL, 1, 57)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'328', N'Silniki napędzane powietrzem sprężonym', 79, NULL, 1, 58)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'330', N'Silniki prądu stałego', 81, NULL, 1, 59)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'331', N'Prądnice prądu stałego', 82, NULL, 1, 60)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'332', N'Silniki asynchroniczne 50 okr./sek klatkowe', 83, NULL, 1, 61)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'333', N'Silniki asynchroniczne 50 okr./sek pierścieniowe', 84, NULL, 1, 62)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'334', N'Silniki synchroniczne 50 okr./sek', 85, NULL, 1, 63)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'335', N'Prądnice synchroniczne 50 okr./sek', 86, NULL, 1, 64)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'336', N'Silniki prądu zmiennego na inne okresy', 87, NULL, 1, 65)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'337', N'Silniki komutatorowe prądu zmiennego', 88, NULL, 1, 66)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'338', N'Kompensatory wirujące', 89, NULL, 1, 67)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'339', N'Układy napędowe elektryczne skojarzone oraz maszyn', 90, NULL, 1, 68)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'340', N'Turbozespoły parowe', 92, NULL, 1, 69)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'341', N'Turbozespoły wodne', 93, NULL, 1, 70)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'342', N'Zespoły elektroenergetyczne na silniki parowe tłok', 94, NULL, 1, 71)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'343', N'Zespoły elektroenergetyczne z silnikami spalinowym', 95, NULL, 1, 72)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'344', N'Zespoły elektroenergetyczne z silnikami spalinowym', 96, NULL, 1, 73)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'345', N'Zespoły elektroenergetyczne z silnikami spalinowym', 97, NULL, 1, 74)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'346', N'Zespoły wiatrowo-elektryczne', 98, NULL, 1, 75)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'347', N'Przetwornice prądu', 99, NULL, 1, 76)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'348', N'Inne maszyny, zespoły i turbozespoły oraz agregaty', 100, NULL, 1, 77)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'349', N'Reaktory jądrowe', 101, NULL, 1, 78)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'410', N'Tokarki', 104, NULL, 1, 79)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'411', N'Wiertarki, wytaczarki i wiertarko-frezarki', 105, NULL, 1, 80)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'412', N'Frezarki', 106, NULL, 1, 81)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'413', N'Przecinarki i nakiełczarki', 107, NULL, 1, 82)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'414', N'Strugarki, dłutownice, przeciągarki i przepycharki', 108, NULL, 1, 83)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'415', N'Szlifierki', 109, NULL, 1, 84)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'416', N'Obrabiarki do uzwojeń i uzębień', 110, NULL, 1, 85)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'417', N'Obrabiarki kombinowane, jednostki obróbcze, obrabi', 111, NULL, 1, 86)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'420', N'Młoty i kuźniarki', 113, NULL, 1, 87)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'421', N'Prasy do metali i tworzyw sztucznych', 114, NULL, 1, 88)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'422', N'Maszyny do gięcia i prostowania', 115, NULL, 1, 89)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'423', N'Nitownice', 116, NULL, 1, 90)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'424', N'Nożyce do metali i tworzyw sztucznych', 117, NULL, 1, 91)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'425', N'Ciągarki', 118, NULL, 1, 92)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'426', N'Maszyny do skręcania, tkania i wyrobu sprężyn i li', 119, NULL, 1, 93)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'429', N'Inne maszyny do obróbki plastycznej metali i tworz', 120, NULL, 1, 94)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'430', N'Maszyny, urządzenia i aparaty do rozdrabniania, ro', 122, NULL, 1, 95)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'431', N'Maszyny, urządzenia i aparaty filtracyjne', 123, NULL, 1, 96)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'432', N'Maszyny, urządzenia i aparaty do pasteryzacji', 124, NULL, 1, 97)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'433', N'Maszyny, urządzenia i aparaty do dozowania i napeł', 125, NULL, 1, 98)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'434', N'Maszyny, urządzenia i aparaty do zamykania, korkow', 126, NULL, 1, 99)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'435', N'Maszyny, urządzenia i aparaty do pakowania w karto', 127, NULL, 1, 100)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'436', N'Maszyny, urządzenia i aparaty do mycia i czyszczen', 128, NULL, 1, 101)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'439', N'Inne maszyny, urządzenia i aparaty w przemysłach r', 129, NULL, 1, 102)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'440', N'Pompy nurnikowe i tłokowe', 131, NULL, 1, 103)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'441', N'Pompy wirowe', 132, NULL, 1, 104)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'442', N'Pompy rotacyjne', 133, NULL, 1, 105)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'443', N'Pompy zębate', 134, NULL, 1, 106)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'444', N'Sprężarki', 135, NULL, 1, 107)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'445', N'Dmuchawy i ssawy', 136, NULL, 1, 108)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'446', N'Wentylatory', 137, NULL, 1, 109)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'449', N'Inne maszyny i urządzenia do przetłaczania i spręż', 138, NULL, 1, 110)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'450', N'Piece do przerobu surowców', 140, NULL, 1, 111)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'451', N'Piece do przetwarzania paliw', 141, NULL, 1, 112)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'452', N'Piece grzewcze', 142, NULL, 1, 113)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'453', N'Piece topielne', 143, NULL, 1, 114)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'454', N'Piece do wypalania', 144, NULL, 1, 115)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'455', N'Konwertory i mieszalniki stalownicze', 145, NULL, 1, 116)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'456', N'Piece indukcyjne wysokiej częstotliwości stosowane', 146, NULL, 1, 117)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'460', N'Wymienniki przeponowe dwupłaszczowe', 148, NULL, 1, 118)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'461', N'Wymienniki przeponowe płaszczowo-rurkowe', 149, NULL, 1, 119)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'462', N'Wymienniki przeponowe komorowo-grzejnikowe', 150, NULL, 1, 120)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'463', N'Wymienniki przeponowe płaszczowo-wężownicowe', 151, NULL, 1, 121)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'464', N'Wymienniki przeponowe dwupłaszczowo-wężownicowe', 152, NULL, 1, 122)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'465', N'Wymienniki przeponowe rurowe', 153, NULL, 1, 123)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'466', N'Wymienniki przeponowe kanałowe', 154, NULL, 1, 124)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'469', N'Inne aparaty do wymiany ciepła', 155, NULL, 1, 125)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'470', N'Mieszalniki cieczy bezciśnieniowe z urządzeniami m', 157, NULL, 1, 126)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'471', N'Mieszalniki cieczy na ciśnienie do 16 atm. z mecha', 158, NULL, 1, 127)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'472', N'Mieszalniki cieczy na ciśnienie powyżej 16 atm. z ', 159, NULL, 1, 128)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'473', N'Skrubery', 160, NULL, 1, 129)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'474', N'Kolumny', 161, NULL, 1, 130)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'475', N'Aparaty bębnowe', 162, NULL, 1, 131)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'476', N'Aparaty walcowe skrobakowe', 163, NULL, 1, 132)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'477', N'Suszarki komorowe', 164, NULL, 1, 133)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'479', N'Inne maszyny i aparaty do operacji i procesów mate', 165, NULL, 1, 134)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'480', N'Elektrolizery', 167, NULL, 1, 135)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'481', N'Aparaty i urządzenia do powierzchniowej obróbki me', 168, NULL, 1, 136)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'482', N'Aparaty i urządzenia do powierzchniowej obróbki me', 169, NULL, 1, 137)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'483', N'Urządzenia i aparaty do elektroerozyjnej obróbki m', 170, NULL, 1, 138)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'484', N'Maszyny i aparaty do spawania, zgrzewania, natrysk', 171, NULL, 1, 139)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'485', N'Urządzenia i aparaty chłodnicze nieprzenośne', 172, NULL, 1, 140)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'486', N'Urządzenia i aparaty chłodnicze przenośne', 173, NULL, 1, 141)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'490', N'Maszyny i urządzenia do przygotowania maszynowych ', 175, NULL, 1, 142)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'491', N'Zespoły komputerowe', 176, NULL, 1, 143)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'492', N'Samodzielne urządzenia do automatycznej regulacji ', 177, NULL, 1, 144)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'493', N'Roboty', 178, NULL, 1, 145)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'500', N'Maszyny, urządzenia i aparaty przemysłu azotowego ', 181, NULL, 1, 146)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'501', N'Maszyny, urządzenia i aparaty przemysłu farmaceuty', 182, NULL, 1, 147)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'502', N'Maszyny, urządzenia i aparaty przemysłu włókien sz', 183, NULL, 1, 148)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'503', N'Maszyny, urządzenia i aparaty przemysłu tworzyw sz', 184, NULL, 1, 149)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'504', N'Maszyny, urządzenia i aparaty przemysłu gumowego', 185, NULL, 1, 150)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'505', N'Maszyny, urządzenia i aparaty przemysłu nieorganic', 186, NULL, 1, 151)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'506', N'Maszyny, urządzenia i aparaty przemysłu gazów tech', 187, NULL, 1, 152)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'507', N'Maszyny, urządzenia i aparaty przemysłu rafineryjn', 188, NULL, 1, 153)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'510', N'Maszyny i urządzenia wiertnicze', 190, NULL, 1, 154)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'511', N'Maszyny górnicze', 191, NULL, 1, 155)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'512', N'Maszyny i urządzenia do eksploatacji otworów wiert', 192, NULL, 1, 156)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'513', N'Maszyny i urządzenia do przeróbki mechanicznej rud', 193, NULL, 1, 157)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'514', N'Maszyny i urządzenia hutnicze', 194, NULL, 1, 158)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'515', N'Maszyny i urządzenia koksownicze i gazownicze', 195, NULL, 1, 159)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'516', N'Maszyny i urządzenia odlewnicze', 196, NULL, 1, 160)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'517', N'Maszyny i urządzenia torfiarskie', 197, NULL, 1, 161)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'518', N'Aparaty i urządzenia do pomiarów i zabiegów geofiz', 198, NULL, 1, 162)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'520', N'Maszyny i urządzenia przemysłu kamieniarskiego', 200, NULL, 1, 163)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'521', N'Maszyny i urządzenia przemysłu mineralnego', 201, NULL, 1, 164)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'522', N'Maszyny i urządzenia przemysłu wapienniczego i gip', 202, NULL, 1, 165)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'523', N'Maszyny i urządzenia przemysłu cementowego', 203, NULL, 1, 166)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'524', N'Maszyny, urządzenia i aparaty przemysłu izolacyjne', 204, NULL, 1, 167)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'525', N'Maszyny i urządzenia do produkcji ceramiki budowla', 205, NULL, 1, 168)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'526', N'Maszyny przemysłu ceramiki szlachetnej i techniczn', 206, NULL, 1, 169)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'527', N'Maszyny i urządzenia do produkcji szkła i wyrobów ', 207, NULL, 1, 170)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'528', N'Maszyny i urządzenia przemysłu materiałów ściernyc', 208, NULL, 1, 171)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'529', N'Maszyny, urządzenia i agregaty do produkcji innych', 209, NULL, 1, 172)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'531', N'Maszyny specjalne do produkcji wyrobów drobnej gal', 211, NULL, 1, 173)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'532', N'Maszyny do produkcji wyrobów metalowych', 212, NULL, 1, 174)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'533', N'Maszyny i urządzenia do produkcji kabli i przedmio', 213, NULL, 1, 175)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'534', N'Maszyny i urządzenia do produkcji akumulatorów i b', 214, NULL, 1, 176)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'535', N'Maszyny i urządzenia do produkcji żarówek i lamp e', 215, NULL, 1, 177)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'536', N'Maszyny i aparaty do produkcji taśmy filmowej i ma', 216, NULL, 1, 178)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'537', N'Maszyny i aparaty do produkcji filmów i kopii film', 217, NULL, 1, 179)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'538', N'Maszyny do nawijania cewek oraz izolacji silników,', 218, NULL, 1, 180)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'540', N'Maszyny, urządzenia i aparaty do ścinki drzew oraz', 220, NULL, 1, 181)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'541', N'Maszyny, urządzenia i aparaty do wyrobu oklein, pł', 221, NULL, 1, 182)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'542', N'Maszyny, urządzenia i aparaty do wyrobu płyt pilśn', 222, NULL, 1, 183)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'543', N'Maszyny, urządzenia i aparaty do wyrobu zapałek', 223, NULL, 1, 184)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'544', N'Maszyny, urządzenia i aparaty do wyrobu beczek, sk', 224, NULL, 1, 185)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'545', N'Maszyny, urządzenia i aparaty do wyrobu celulozy', 225, NULL, 1, 186)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'546', N'Maszyny i urządzenia do wyrobu papieru i tektury', 226, NULL, 1, 187)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'547', N'Maszyny, urządzenia i aparaty do wyrobów papiernic', 227, NULL, 1, 188)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'548', N'Maszyny, urządzenia i aparaty poligraficzne', 228, NULL, 1, 189)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'549', N'Maszyny i urządzenia do produkcji wyrobów z drewna', 229, NULL, 1, 190)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'550', N'Maszyny do wstępnej obróbki włókien łykowych', 231, NULL, 1, 191)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'551', N'Maszyny przędzalnicze', 232, NULL, 1, 192)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'552', N'Maszyny do produkcji nici, sznurków i lin', 233, NULL, 1, 193)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'553', N'Maszyny tkackie', 234, NULL, 1, 194)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'554', N'Maszyny dziewiarskie i pończosznicze', 235, NULL, 1, 195)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'555', N'Maszyny wykańczalnicze i farbiarskie', 236, NULL, 1, 196)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'556', N'Maszyny do produkcji filcu i artykułów z filcu', 237, NULL, 1, 197)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'557', N'Maszyny szwalnicze, odzieżowe i do wyrobu guzików', 238, NULL, 1, 198)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'558', N'Maszyny do produkcji skóry i wyrobów ze skóry', 239, NULL, 1, 199)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'559', N'Inne maszyny i urządzenia przemysłu włók.,odzież. ', 240, NULL, 1, 200)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'560', N'Maszyny, urządzenia i aparaty przemysłu cukrownicz', 242, NULL, 1, 201)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'561', N'Maszyny, urządzenia i aparaty przemysłu piwowarsko', 243, NULL, 1, 202)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'562', N'Maszyny, urządzenia i aparaty do produkcji drożdży', 244, NULL, 1, 203)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'563', N'Maszyny, urządzenia i aparaty do produkcji wyrobów', 245, NULL, 1, 204)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'564', N'Maszyny, urządzenia i aparaty do produkcji octu', 246, NULL, 1, 205)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'565', N'Maszyny, urządzenia i aparaty przemysłu ziemniacza', 247, NULL, 1, 206)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'566', N'Maszyny, urządzenia i aparaty przemysłu owocowo-wa', 248, NULL, 1, 207)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'567', N'Maszyny, urządzenia i aparaty przemysłu młynarskie', 249, NULL, 1, 208)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'568', N'Maszyny, urządzenia i aparaty przemysłu piekarnicz', 250, NULL, 1, 209)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'570', N'Maszyny, urządzenia i aparaty przemysłu cukiernicz', 252, NULL, 1, 210)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'571', N'Maszyny, urządzenia i aparaty przemysłu koncentrat', 253, NULL, 1, 211)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'572', N'Maszyny, urządzenia i aparaty przemysłu tytonioweg', 254, NULL, 1, 212)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'573', N'Maszyny, urządzenia i aparaty przemysłu mięsnego', 255, NULL, 1, 213)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'574', N'Maszyny, urządzenia i aparaty przemysłu rybnego', 256, NULL, 1, 214)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'575', N'Maszyny, urządzenia i aparaty przemysłu tłuszczowe', 257, NULL, 1, 215)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'576', N'Maszyny, urządzenia i aparaty przemysłu mleczarski', 258, NULL, 1, 216)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'577', N'Maszyny, urządzenia i aparaty przemysłu jajczarsko', 259, NULL, 1, 217)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'578', N'Maszyny, urządzenia i aparaty przemysłu gastronomi', 260, NULL, 1, 218)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'579', N'Inne maszyny, urządzenia i aparaty przemysłów spoż', 261, NULL, 1, 219)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'580', N'Maszyny do robót ziemnych i fundamentowych', 263, NULL, 1, 220)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'581', N'Maszyny do robót budowlanych', 264, NULL, 1, 221)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'582', N'Maszyny do robót drogowych', 265, NULL, 1, 222)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'583', N'Koparki i zwałowarki używane w kopalnictwie odkryw', 266, NULL, 1, 223)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'584', N'Maszyny do robót torowych kolejowych', 267, NULL, 1, 224)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'590', N'Maszyny i narzędzia uprawowe', 269, NULL, 1, 225)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'591', N'Maszyny i urządzenia do siewu, sadzenia, nawożenia', 270, NULL, 1, 226)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'592', N'Maszyny i narzędzia pielęgnacyjne', 271, NULL, 1, 227)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'593', N'Maszyny i urządzenia do ochrony roślin', 272, NULL, 1, 228)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'594', N'Maszyny i urządzenia do zbioru ziemiopłodów', 273, NULL, 1, 229)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'595', N'Maszyny i urządzenia omłotowe, suszarnie i urządze', 274, NULL, 1, 230)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'596', N'Maszyny i urządzenia do czyszczenia, sortowania i ', 275, NULL, 1, 231)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'597', N'Maszyny i urządzenia do przetwórstwa paszowego', 276, NULL, 1, 232)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'598', N'Maszyny oraz urządzenia do chowu i hodowli zwierzą', 277, NULL, 1, 233)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'599', N'Inne maszyny i urządzenia różne', 278, NULL, 1, 234)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'600', N'Zbiorniki naziemne ceglane', 281, NULL, 1, 235)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'601', N'Zbiorniki naziemne betonowe', 282, NULL, 1, 236)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'602', N'Zbiorniki naziemne drewniane', 283, NULL, 1, 237)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'603', N'Zbiorniki naziemne z tworzyw naturalnych i sztuczn', 284, NULL, 1, 238)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'604', N'Zbiorniki naziemne stalowe', 285, NULL, 1, 239)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'605', N'Zbiorniki naziemne z metali nieżelaznych', 286, NULL, 1, 240)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'610', N'Urządzenia rozdzielcze prądu zmiennego', 288, NULL, 1, 241)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'611', N'Urządzenia nastawcze prądu zmiennego i stałego', 289, NULL, 1, 242)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'612', N'Aparatura prądu zmiennego', 290, NULL, 1, 243)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'613', N'Stacje transformatorowe stałe i przewoźne', 291, NULL, 1, 244)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'614', N'Urządzenia i aparatura rozdzielcza prądu stałego', 292, NULL, 1, 245)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'615', N'Aparatura prądu stałego', 293, NULL, 1, 246)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'620', N'Urządzenia i aparatura radionadawcza', 295, NULL, 1, 247)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'621', N'Urządzenia i aparatura radioodbiorcza', 296, NULL, 1, 248)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'622', N'Urządzenia elektroakustyczne i elektrowizyjne', 297, NULL, 1, 249)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'623', N'Urządzenia teletransmisji przewodowej', 298, NULL, 1, 250)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'624', N'Urządzenia alarmowe i sygnalizacyjne', 299, NULL, 1, 251)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'626', N'Urządzenia telefoniczne', 300, NULL, 1, 252)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'627', N'Urządzenia telegraficzne', 301, NULL, 1, 253)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'629', N'Inne urządzenia tele- i radiotechniczne', 302, NULL, 1, 254)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'630', N'Transformatory', 304, NULL, 1, 255)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'631', N'Zespoły prostownikowe', 305, NULL, 1, 256)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'632', N'Kondensatory statyczne', 306, NULL, 1, 257)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'633', N'Baterie akumulatorów elektrycznych stacjonarnych', 307, NULL, 1, 258)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'634', N'Baterie akumulatorów elektrycznych zasadowych', 308, NULL, 1, 259)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'640', N'Dźwigi osobowe i towarowe', 310, NULL, 1, 260)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'641', N'Dźwigniki, wciągarki i wciągniki przejezdne oraz n', 311, NULL, 1, 261)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'642', N'Żurawie (dźwigi przeładunkowe)', 312, NULL, 1, 262)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'643', N'Przenośniki (transportery) ogólnego zastosowania', 313, NULL, 1, 263)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'644', N'Przenośniki (transportery) kopalniane', 314, NULL, 1, 264)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'645', N'Przenośniki (transportery) powietrzne i wodne', 315, NULL, 1, 265)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'646', N'Suwnice i wsadzarki', 316, NULL, 1, 266)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'647', N'Przesuwnice, obrotnice i wywrotnice', 317, NULL, 1, 267)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'648', N'Tory, kolejki linowe i dźwignice linowe', 318, NULL, 1, 268)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'650', N'Przekładnie', 320, NULL, 1, 269)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'652', N'Urządzenia wentylacyjne', 321, NULL, 1, 270)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'653', N'Urządzenia klimatyzacyjne', 322, NULL, 1, 271)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'654', N'Urządzenia do oczyszczania wody', 323, NULL, 1, 272)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'655', N'Urządzenia do oczyszczania gazów', 324, NULL, 1, 273)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'656', N'Urządzenia do odpopielania i odżużlania', 325, NULL, 1, 274)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'657', N'Akumulatory hydrauliczne', 326, NULL, 1, 275)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'658', N'Urządzenia do oczyszczania ścieków', 327, NULL, 1, 276)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'659', N'Pozostałe urządzenia przemysłowe nie wymienione', 328, NULL, 1, 277)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'660', N'Wagi pojazdowe, wagonowe i inne wbudowane', 330, NULL, 1, 278)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'661', N'Urządzenia pralni i farbiarni', 331, NULL, 1, 279)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'662', N'Urządzenia i aparaty projekcyjne', 332, NULL, 1, 280)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'663', N'Urządzenia przeciwpożarowe', 333, NULL, 1, 281)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'664', N'Urządzenia i aparatura do przeprowadzania badań te', 334, NULL, 1, 282)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'668', N'Urządzenia techniczne teatralne', 335, NULL, 1, 283)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'669', N'Pozostałe urządzenia nieprzemysłowe', 336, NULL, 1, 284)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'681', N'Kontenery', 338, NULL, 1, 285)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'682', N'Nadwozia wymienne', 339, NULL, 1, 286)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'700', N'Kolejowy tabor szynowy naziemny', 342, NULL, 1, 287)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'710', N'Kolejowy tabor szynowy podziemny', 344, NULL, 1, 288)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'720', N'Tramwajowy tabor szynowy', 346, NULL, 1, 289)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'730', N'Pozostały tabor szynowy naziemny', 348, NULL, 1, 290)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'740', N'Motocykle, przyczepy i wózki motocyklowe', 350, NULL, 1, 291)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'741', N'Samochody osobowe', 351, NULL, 1, 292)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'742', N'Samochody ciężarowe', 352, NULL, 1, 293)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'743', N'Samochody specjalne', 353, NULL, 1, 294)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'744', N'Autobusy i autokary', 354, NULL, 1, 295)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'745', N'Trolejbusy i samochody o napędzie elektrycznym', 355, NULL, 1, 296)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'746', N'Ciągniki', 356, NULL, 1, 297)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'747', N'Naczepy', 357, NULL, 1, 298)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'748', N'Przyczepy', 358, NULL, 1, 299)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'750', N'Tabor konny', 360, NULL, 1, 300)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'760', N'Wózki jezdniowe z pomostem stałym o napędzie elekt', 362, NULL, 1, 301)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'761', N'Wózki jezdniowe podnośnikowe z nisko podnoszoną pl', 363, NULL, 1, 302)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'762', N'Wózki jezdniowe podnośnikowe, mechaniczne, wysokie', 364, NULL, 1, 303)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'763', N'Wózki jezdniowe podnośnikowe, mechaniczne, wysokie', 365, NULL, 1, 304)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'764', N'Wózki jezdniowe transportowe ze stałą platformą i ', 366, NULL, 1, 305)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'768', N'Inne wózki jezdniowe', 367, NULL, 1, 306)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'770', N'Tabor transportu morskiego', 369, NULL, 1, 307)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'771', N'Tabor rybołówstwa morskiego', 370, NULL, 1, 308)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'772', N'Tabor techniczny i ratowniczy morski', 371, NULL, 1, 309)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'773', N'Tabor transportu śródlądowego', 372, NULL, 1, 310)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'774', N'Tabor techniczny i ratowniczy śródlądowy', 373, NULL, 1, 311)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'778', N'Inny tabor pływający', 374, NULL, 1, 312)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'780', N'Samoloty', 376, NULL, 1, 313)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'781', N'Śmigłowce', 377, NULL, 1, 314)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'782', N'Szybowce', 378, NULL, 1, 315)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'783', N'Balony', 379, NULL, 1, 316)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'788', N'Inne środki transportu lotniczego', 380, NULL, 1, 317)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'790', N'Środki transportu pozostałe', 382, NULL, 1, 318)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'800', N'Narzędzia, przyrządy, sprawdziany itp.', 385, NULL, 1, 319)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'801', N'Wyposażenie, aparaty i sprzęt laboratoryjny', 386, NULL, 1, 320)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'802', N'Wyposażenie, aparaty i sprzęt medyczny', 387, NULL, 1, 321)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'803', N'Wyposażenie techniczne dla prac biurowych', 388, NULL, 1, 322)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'804', N'Wyposażenie i sprzęt cyrkowy', 389, NULL, 1, 323)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'805', N'Wyposażenie i sprzęt kin, teatrów, placówek kultur', 390, NULL, 1, 324)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'806', N'Kioski, budki, baraki, domki campingowe itp.', 391, NULL, 1, 325)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'808', N'Narzędzia, przyrządy ruchomości i wyposażenie pozo', 392, NULL, 1, 326)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'900', N'Inwentarz żywy', 395, NULL, 1, 327)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'900', N'OPROGRAMOWANIE', 421, NULL, 1, 328)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'900', N'OPROGRAMOWANIE WNiP', 422, NULL, 1, 329)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'400', N'LICENCJE OSOBNO EWIDECJONOWANE', 423, NULL, 1, 330)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'100', N'PRACE ROZWOJOWE ZAKOŃCZONE', 424, NULL, 1, 331)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'001', N'Budynki', 426, NULL, 1, 332)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'002', N'Pozostałe', 427, NULL, 1, 333)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1', N'', 458, NULL, 1, 334)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'901', N'PRACE ROZWOJOWE ZAKOŃCZONE', 462, NULL, 1, 335)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'904', N'LICENCJE OSOBNO EWIDENCJONOWANE', 463, NULL, 1, 336)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'909', N'BAZY DOSTĘPU', 526, NULL, 1, 337)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'902', N'ROZRUCH TECHNOLOGICZNY', 527, NULL, 1, 338)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'', N'', 554, NULL, 1, 339)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'', N'', 566, NULL, 1, 340)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000001', N'', 567, NULL, 1, 341)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000059', N'Szcze.OD Wały 01-Ptr.-01-Pok.000059 (po.gosp.)', 568, NULL, 1, 342)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000060', N'Szcze.OD Wały 01-Ptr.-01-Pok.000060 (po.gosp.)', 569, NULL, 1, 343)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000061', N'Szcze.OD Wały 01-Ptr.-01-Pok.000061 (pom.tech)', 570, NULL, 1, 344)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000062', N'Szcze.OD Wały 01-Ptr.-01-Pok.000062 (po.gosp.)', 571, NULL, 1, 345)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000063', N'Szcze.OD Wały 01-Ptr.-01-Pok.000063 (po.gosp.)', 572, NULL, 1, 346)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000064', N'Szcze.OD Wały 01-Ptr.-01-Pok.000064 (pom.tech)', 573, NULL, 1, 347)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000065', N'Szcze.OD Wały 01-Ptr.-01-Pok.000065 (pom.tech)', 574, NULL, 1, 348)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000066', N'Szcze.OD Wały 01-Ptr.-01-Pok.000066 (po.gosp.)', 575, NULL, 1, 349)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000067', N'Szcze.OD Wały 01-Ptr.-01-Pok.000067 (po.gosp.)', 576, NULL, 1, 350)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000068', N'Szcze.OD Wały 01-Ptr.-01-Pok.000068 (szatnia)', 577, NULL, 1, 351)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000069', N'Szcze.OD Wały 01-Ptr.-01-Pok.000069 (po.gosp.)', 578, NULL, 1, 352)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000070', N'Szcze.OD Wały 01-Ptr.-01-Pok.000070 (lab.)', 579, NULL, 1, 353)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000071', N'Szcze.OD Wały 01-Ptr.-01-Pok.000071 (lab.)', 580, NULL, 1, 354)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000072', N'Szcze.OD Wały 01-Ptr.-01-Pok.000072 (pom.tech)', 581, NULL, 1, 355)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000068', N'Szcze.OD Wały 01-Ptr.-01-Pok.000068 (lab.)', 582, NULL, 1, 356)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000069', N'Szcze.OD Wały 01-Ptr.-01-Pok.000069 (lab.)', 583, NULL, 1, 357)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000070', N'Szcze.OD Wały 01-Ptr.-01-Pok.000070 (biuro)', 584, NULL, 1, 358)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000071', N'Szcze.OD Wały 01-Ptr.-01-Pok.000071 (pom.soc.)', 585, NULL, 1, 359)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000072', N'Szcze.OD Wały 01-Ptr.-01-Pok.000072 (toaleta)', 586, NULL, 1, 360)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000073', N'Szcze.OD Wały 01-Ptr.-01-Pok.000073 (lab.)', 587, NULL, 1, 361)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000074', N'Szcze.OD Wały 01-Ptr.-01-Pok.000074 (biuro)', 588, NULL, 1, 362)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000075', N'Szcze.OD Wały 01-Ptr.-01-Pok.000075 (sal.wykł)', 589, NULL, 1, 363)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000076', N'Szcze.OD Wały 01-Ptr.-01-Pok.000076 (warszt)', 590, NULL, 1, 364)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000077', N'Szcze.OD Wały 01-Ptr.-01-Pok.000077 (lab.)', 591, NULL, 1, 365)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000078', N'Szcze.OD Wały 01-Ptr.-01-Pok.000078 (lab.)', 592, NULL, 1, 366)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000079', N'Szcze.OD Wały 01-Ptr.-01-Pok.000079 (lab.)', 593, NULL, 1, 367)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000080', N'Szcze.OD Wały 01-Ptr.-01-Pok.000080 (lab.)', 594, NULL, 1, 368)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000081', N'Szcze.OD Wały 01-Ptr.-01-Pok.000081 (po.gosp.)', 595, NULL, 1, 369)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000082', N'Szcze.OD Wały 01-Ptr.-01-Pok.000082 (korytarz)', 596, NULL, 1, 370)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000083', N'Szcze.OD Wały 01-Ptr.-01-Pok.000083 (kuch.)', 597, NULL, 1, 371)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000084', N'Szcze.OD Wały 01-Ptr.-01-Pok.000084 (kuch.)', 598, NULL, 1, 372)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000085', N'Szcze.OD Wały 01-Ptr.-01-Pok.000085 (kuch.)', 599, NULL, 1, 373)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000086', N'Szcze.OD Wały 01-Ptr.-01-Pok.000086 (szatnia)', 600, NULL, 1, 374)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000087', N'Szcze.OD Wały 01-Ptr.-01-Pok.000087 (stołów.)', 601, NULL, 1, 375)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000088', N'Szcze.OD Wały 01-Ptr.-01-Pok.000088 (stołów.)', 602, NULL, 1, 376)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000089', N'Szcze.OD Wały 01-Ptr.-01-Pok.000089 (korytarz)', 603, NULL, 1, 377)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000090', N'Szcze.OD Wały 01-Ptr.-01-Pok.000090 (stołów.)', 604, NULL, 1, 378)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.-01.000091', N'Szcze.OD Wały 01-Ptr.-01-Pok.000091 (stołów.)', 605, NULL, 1, 379)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000001', N'Szcze.OD Wały 01-Ptr.000-Pok.000001 (serwer)', 606, NULL, 1, 380)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000002', N'Szcze.OD Wały 01-Ptr.000-Pok.000002 (biuro)', 607, NULL, 1, 381)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000003', N'Szcze.OD Wały 01-Ptr.000-Pok.000003 (biuro)', 608, NULL, 1, 382)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000004', N'Szcze.OD Wały 01-Ptr.000-Pok.000004 (biuro)', 609, NULL, 1, 383)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000005', N'Szcze.OD Wały 01-Ptr.000-Pok.000005 (biuro)', 610, NULL, 1, 384)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000006', N'Szcze.OD Wały 01-Ptr.000-Pok.000006 (biuro)', 611, NULL, 1, 385)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000007', N'Szcze.OD Wały 01-Ptr.000-Pok.000007 (po.gosp.)', 612, NULL, 1, 386)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000054', N'Szcze.OD Wały 01-Ptr.000-Pok.000054 (biuro)', 613, NULL, 1, 387)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000055', N'Szcze.OD Wały 01-Ptr.000-Pok.000055 (sal.wykł)', 614, NULL, 1, 388)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000056', N'Szcze.OD Wały 01-Ptr.000-Pok.000056 (biuro)', 615, NULL, 1, 389)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000057', N'Szcze.OD Wały 01-Ptr.000-Pok.000057 (biuro)', 616, NULL, 1, 390)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00057A', N'Szcze.OD Wały 01-Ptr.000-Pok.00057A (biuro)', 617, NULL, 1, 391)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000058', N'Szcze.OD Wały 01-Ptr.000-Pok.000058 (sal.repr)', 618, NULL, 1, 392)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00058A', N'Szcze.OD Wały 01-Ptr.000-Pok.00058A (toaleta)', 619, NULL, 1, 393)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000059', N'Szcze.OD Wały 01-Ptr.000-Pok.000059 (biuro)', 620, NULL, 1, 394)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000060', N'Szcze.OD Wały 01-Ptr.000-Pok.000060 (biuro)', 621, NULL, 1, 395)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00060A', N'Szcze.OD Wały 01-Ptr.000-Pok.00060A (biuro)', 622, NULL, 1, 396)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000061', N'Szcze.OD Wały 01-Ptr.000-Pok.000061 (sal.wykł)', 623, NULL, 1, 397)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00061A', N'Szcze.OD Wały 01-Ptr.000-Pok.00061A (biuro)', 624, NULL, 1, 398)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000062', N'Szcze.OD Wały 01-Ptr.000-Pok.000062 (biuro)', 625, NULL, 1, 399)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000063', N'Szcze.OD Wały 01-Ptr.000-Pok.000063 (biuro)', 626, NULL, 1, 400)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000064', N'Szcze.OD Wały 01-Ptr.000-Pok.000064 (biuro)', 627, NULL, 1, 401)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000065', N'Szcze.OD Wały 01-Ptr.000-Pok.000065 (biuro)', 628, NULL, 1, 402)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000066', N'Szcze.OD Wały 01-Ptr.000-Pok.000066 (biuro)', 629, NULL, 1, 403)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000067', N'Szcze.OD Wały 01-Ptr.000-Pok.000067 (biuro)', 630, NULL, 1, 404)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000068', N'Szcze.OD Wały 01-Ptr.000-Pok.000068 (biuro)', 631, NULL, 1, 405)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000069', N'Szcze.OD Wały 01-Ptr.000-Pok.000069 (biuro)', 632, NULL, 1, 406)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00069A', N'Szcze.OD Wały 01-Ptr.000-Pok.00069A (serwer)', 633, NULL, 1, 407)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000070', N'Szcze.OD Wały 01-Ptr.000-Pok.000070 (biuro)', 634, NULL, 1, 408)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000071', N'Szcze.OD Wały 01-Ptr.000-Pok.000071 (biuro)', 635, NULL, 1, 409)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00071A', N'Szcze.OD Wały 01-Ptr.000-Pok.00071A (biuro)', 636, NULL, 1, 410)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000072', N'Szcze.OD Wały 01-Ptr.000-Pok.000072 (biuro)', 637, NULL, 1, 411)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00073B', N'Szcze.OD Wały 01-Ptr.000-Pok.00073B (biuro)', 638, NULL, 1, 412)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000073', N'Szcze.OD Wały 01-Ptr.000-Pok.000073 (biuro)', 639, NULL, 1, 413)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00073A', N'Szcze.OD Wały 01-Ptr.000-Pok.00073A (biuro)', 640, NULL, 1, 414)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.00074A', N'Szcze.OD Wały 01-Ptr.000-Pok.00074A (biuro)', 641, NULL, 1, 415)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000074', N'Szcze.OD Wały 01-Ptr.000-Pok.000074 (biuro)', 642, NULL, 1, 416)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000075', N'Szcze.OD Wały 01-Ptr.000-Pok.000075 (biuro)', 643, NULL, 1, 417)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000076', N'Szcze.OD Wały 01-Ptr.000-Pok.000076 (biuro)', 644, NULL, 1, 418)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000077', N'Szcze.OD Wały 01-Ptr.000-Pok.000077 (biuro)', 645, NULL, 1, 419)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000078', N'Szcze.OD Wały 01-Ptr.000-Pok.000078 (biuro)', 646, NULL, 1, 420)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.000.000079', N'Szcze.OD Wały 01-Ptr.000-Pok.000079 (biuro)', 647, NULL, 1, 421)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000101', N'Szcze.OD Wały 01-Ptr.001-Pok.000101 (biuro)', 648, NULL, 1, 422)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000102', N'Szcze.OD Wały 01-Ptr.001-Pok.000102 (biuro)', 649, NULL, 1, 423)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000103', N'Szcze.OD Wały 01-Ptr.001-Pok.000103 (biuro)', 650, NULL, 1, 424)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000104', N'Szcze.OD Wały 01-Ptr.001-Pok.000104 (biuro)', 651, NULL, 1, 425)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000159', N'Szcze.OD Wały 01-Ptr.001-Pok.000159 (biuro)', 652, NULL, 1, 426)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.00159A', N'Szcze.OD Wały 01-Ptr.001-Pok.00159A (biuro)', 653, NULL, 1, 427)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000160', N'Szcze.OD Wały 01-Ptr.001-Pok.000160 (lab.)', 654, NULL, 1, 428)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000161', N'Szcze.OD Wały 01-Ptr.001-Pok.000161 (lab.)', 655, NULL, 1, 429)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000162', N'Szcze.OD Wały 01-Ptr.001-Pok.000162 (biuro)', 656, NULL, 1, 430)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000163', N'Szcze.OD Wały 01-Ptr.001-Pok.000163 (biuro)', 657, NULL, 1, 431)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000164', N'Szcze.OD Wały 01-Ptr.001-Pok.000164 (biuro)', 658, NULL, 1, 432)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000165', N'Szcze.OD Wały 01-Ptr.001-Pok.000165 (biuro)', 659, NULL, 1, 433)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000166', N'Szcze.OD Wały 01-Ptr.001-Pok.000166 (biuro)', 660, NULL, 1, 434)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000167', N'Szcze.OD Wały 01-Ptr.001-Pok.000167 (biuro)', 661, NULL, 1, 435)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000168', N'Szcze.OD Wały 01-Ptr.001-Pok.000168 (biuro)', 662, NULL, 1, 436)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.00168A', N'Szcze.OD Wały 01-Ptr.001-Pok.00168A (biuro)', 663, NULL, 1, 437)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000169', N'Szcze.OD Wały 01-Ptr.001-Pok.000169 (sal.wykł)', 664, NULL, 1, 438)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.00169A', N'Szcze.OD Wały 01-Ptr.001-Pok.00169A (biuro)', 665, NULL, 1, 439)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000170', N'Szcze.OD Wały 01-Ptr.001-Pok.000170 (lab.)', 666, NULL, 1, 440)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000171', N'Szcze.OD Wały 01-Ptr.001-Pok.000171 (lab.)', 667, NULL, 1, 441)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000172', N'Szcze.OD Wały 01-Ptr.001-Pok.000172 (sal.wykł)', 668, NULL, 1, 442)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000174', N'Szcze.OD Wały 01-Ptr.001-Pok.000174 (biuro)', 669, NULL, 1, 443)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000175', N'Szcze.OD Wały 01-Ptr.001-Pok.000175 (biuro)', 670, NULL, 1, 444)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000176', N'Szcze.OD Wały 01-Ptr.001-Pok.000176 (biuro)', 671, NULL, 1, 445)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000177', N'Szcze.OD Wały 01-Ptr.001-Pok.000177 (lab.)', 672, NULL, 1, 446)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000178', N'Szcze.OD Wały 01-Ptr.001-Pok.000178 (lab.)', 673, NULL, 1, 447)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000179', N'Szcze.OD Wały 01-Ptr.001-Pok.000179 (lab.)', 674, NULL, 1, 448)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.001.000180', N'Szcze.OD Wały 01-Ptr.001-Pok.000180 (sal.wykł)', 675, NULL, 1, 449)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000201', N'Szcze.OD Wały 01-Ptr.002-Pok.000201 (biuro)', 676, NULL, 1, 450)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000202', N'Szcze.OD Wały 01-Ptr.002-Pok.000202 (biuro)', 677, NULL, 1, 451)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000203', N'Szcze.OD Wały 01-Ptr.002-Pok.000203 (biuro)', 678, NULL, 1, 452)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000204', N'Szcze.OD Wały 01-Ptr.002-Pok.000204 (biuro)', 679, NULL, 1, 453)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000246', N'Szcze.OD Wały 01-Ptr.002-Pok.000246 (sal.wykł)', 680, NULL, 1, 454)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000247', N'Szcze.OD Wały 01-Ptr.002-Pok.000247 (sal.wykł)', 681, NULL, 1, 455)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000248', N'Szcze.OD Wały 01-Ptr.002-Pok.000248 (biuro)', 682, NULL, 1, 456)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000249', N'Szcze.OD Wały 01-Ptr.002-Pok.000249 (biuro)', 683, NULL, 1, 457)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000250', N'Szcze.OD Wały 01-Ptr.002-Pok.000250 (biuro)', 684, NULL, 1, 458)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000251', N'Szcze.OD Wały 01-Ptr.002-Pok.000251 (biuro)', 685, NULL, 1, 459)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000252', N'Szcze.OD Wały 01-Ptr.002-Pok.000252 (biuro)', 686, NULL, 1, 460)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000253', N'Szcze.OD Wały 01-Ptr.002-Pok.000253 (biuro)', 687, NULL, 1, 461)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000254', N'Szcze.OD Wały 01-Ptr.002-Pok.000254 (biuro)', 688, NULL, 1, 462)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000255', N'Szcze.OD Wały 01-Ptr.002-Pok.000255 (biuro)', 689, NULL, 1, 463)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000256', N'Szcze.OD Wały 01-Ptr.002-Pok.000256 (po.gosp.)', 690, NULL, 1, 464)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000257', N'Szcze.OD Wały 01-Ptr.002-Pok.000257 (serwer)', 691, NULL, 1, 465)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000258', N'Szcze.OD Wały 01-Ptr.002-Pok.000258 (biuro)', 692, NULL, 1, 466)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000259', N'Szcze.OD Wały 01-Ptr.002-Pok.000259 (biuro)', 693, NULL, 1, 467)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000260', N'Szcze.OD Wały 01-Ptr.002-Pok.000260 (biuro)', 694, NULL, 1, 468)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000261', N'Szcze.OD Wały 01-Ptr.002-Pok.000261 (biuro)', 695, NULL, 1, 469)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000262', N'Szcze.OD Wały 01-Ptr.002-Pok.000262 (biuro)', 696, NULL, 1, 470)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000263', N'Szcze.OD Wały 01-Ptr.002-Pok.000263 (biuro)', 697, NULL, 1, 471)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000264', N'Szcze.OD Wały 01-Ptr.002-Pok.000264 (biuro)', 698, NULL, 1, 472)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000265', N'Szcze.OD Wały 01-Ptr.002-Pok.000265 (sal.wykł)', 699, NULL, 1, 473)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000267', N'Szcze.OD Wały 01-Ptr.002-Pok.000267 (sal.wykł)', 700, NULL, 1, 474)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000271', N'Szcze.OD Wały 01-Ptr.002-Pok.000271 (lab.)', 701, NULL, 1, 475)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000272', N'Szcze.OD Wały 01-Ptr.002-Pok.000272 (lab.)', 702, NULL, 1, 476)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000273', N'Szcze.OD Wały 01-Ptr.002-Pok.000273 (lab.)', 703, NULL, 1, 477)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000274', N'Szcze.OD Wały 01-Ptr.002-Pok.000274 (lab.)', 704, NULL, 1, 478)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000275', N'Szcze.OD Wały 01-Ptr.002-Pok.000275 (lab.)', 705, NULL, 1, 479)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000276', N'Szcze.OD Wały 01-Ptr.002-Pok.000276 (biuro)', 706, NULL, 1, 480)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000277', N'Szcze.OD Wały 01-Ptr.002-Pok.000277 (biuro)', 707, NULL, 1, 481)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000278', N'Szcze.OD Wały 01-Ptr.002-Pok.000278 (biuro)', 708, NULL, 1, 482)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000279', N'Szcze.OD Wały 01-Ptr.002-Pok.000279 (biuro)', 709, NULL, 1, 483)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000280', N'Szcze.OD Wały 01-Ptr.002-Pok.000280 (sal.repr)', 710, NULL, 1, 484)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000281', N'Szcze.OD Wały 01-Ptr.002-Pok.000281 (lab.)', 711, NULL, 1, 485)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.002.000282', N'Szcze.OD Wały 01-Ptr.002-Pok.000282 (biuro)', 712, NULL, 1, 486)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.003.00348A', N'Szcze.OD Wały 01-Ptr.003-Pok.00348A (sal.wykł)', 713, NULL, 1, 487)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.003.000349', N'Szcze.OD Wały 01-Ptr.003-Pok.000349 (sal.wykł)', 714, NULL, 1, 488)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.003.000350', N'Szcze.OD Wały 01-Ptr.003-Pok.000350 (biuro)', 715, NULL, 1, 489)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.01.003.00350A', N'Szcze.OD Wały 01-Ptr.003-Pok.00350A (toaleta)', 716, NULL, 1, 490)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000001', N'Szcze.OD Wały 02-Ptr.-01-Pok.000001 (warszt)', 717, NULL, 1, 491)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000002', N'Szcze.OD Wały 02-Ptr.-01-Pok.000002 (warszt)', 718, NULL, 1, 492)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000003', N'Szcze.OD Wały 02-Ptr.-01-Pok.000003 (lab.)', 719, NULL, 1, 493)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000004', N'Szcze.OD Wały 02-Ptr.-01-Pok.000004 (biuro)', 720, NULL, 1, 494)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000005', N'Szcze.OD Wały 02-Ptr.-01-Pok.000005 (biuro)', 721, NULL, 1, 495)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000006', N'Szcze.OD Wały 02-Ptr.-01-Pok.000006 (biuro)', 722, NULL, 1, 496)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000007', N'Szcze.OD Wały 02-Ptr.-01-Pok.000007 (lab.)', 723, NULL, 1, 497)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000008', N'Szcze.OD Wały 02-Ptr.-01-Pok.000008 (pom.tech)', 724, NULL, 1, 498)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000009', N'Szcze.OD Wały 02-Ptr.-01-Pok.000009 (po.gosp.)', 725, NULL, 1, 499)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000010', N'Szcze.OD Wały 02-Ptr.-01-Pok.000010 (po.gosp.)', 726, NULL, 1, 500)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000011', N'Szcze.OD Wały 02-Ptr.-01-Pok.000011 (po.gosp.)', 727, NULL, 1, 501)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000012', N'Szcze.OD Wały 02-Ptr.-01-Pok.000012 (warszt)', 728, NULL, 1, 502)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000013', N'Szcze.OD Wały 02-Ptr.-01-Pok.000013 (po.gosp.)', 729, NULL, 1, 503)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000014', N'Szcze.OD Wały 02-Ptr.-01-Pok.000014 (warszt)', 730, NULL, 1, 504)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000015', N'Szcze.OD Wały 02-Ptr.-01-Pok.000015 (pom.soc.)', 731, NULL, 1, 505)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000016', N'Szcze.OD Wały 02-Ptr.-01-Pok.000016 (mag.)', 732, NULL, 1, 506)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000017', N'Szcze.OD Wały 02-Ptr.-01-Pok.000017 (mag.)', 733, NULL, 1, 507)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000018', N'Szcze.OD Wały 02-Ptr.-01-Pok.000018 (mag.)', 734, NULL, 1, 508)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00018A', N'Szcze.OD Wały 02-Ptr.-01-Pok.00018A (mag.)', 735, NULL, 1, 509)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00018B', N'Szcze.OD Wały 02-Ptr.-01-Pok.00018B (mag.)', 736, NULL, 1, 510)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00018C', N'Szcze.OD Wały 02-Ptr.-01-Pok.00018C (mag.)', 737, NULL, 1, 511)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00018D', N'Szcze.OD Wały 02-Ptr.-01-Pok.00018D (mag.)', 738, NULL, 1, 512)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000019', N'Szcze.OD Wały 02-Ptr.-01-Pok.000019 (po.gosp.)', 739, NULL, 1, 513)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000020', N'Szcze.OD Wały 02-Ptr.-01-Pok.000020 (po.gosp.)', 740, NULL, 1, 514)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000021', N'Szcze.OD Wały 02-Ptr.-01-Pok.000021 (po.gosp.)', 741, NULL, 1, 515)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000022', N'Szcze.OD Wały 02-Ptr.-01-Pok.000022 (po.gosp.)', 742, NULL, 1, 516)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000023', N'Szcze.OD Wały 02-Ptr.-01-Pok.000023 (po.gosp.)', 743, NULL, 1, 517)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000024', N'Szcze.OD Wały 02-Ptr.-01-Pok.000024 (po.gosp.)', 744, NULL, 1, 518)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000025', N'Szcze.OD Wały 02-Ptr.-01-Pok.000025 (toaleta)', 745, NULL, 1, 519)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000026', N'Szcze.OD Wały 02-Ptr.-01-Pok.000026 (po.gosp.)', 746, NULL, 1, 520)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000027', N'Szcze.OD Wały 02-Ptr.-01-Pok.000027 (szatnia)', 747, NULL, 1, 521)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000028', N'Szcze.OD Wały 02-Ptr.-01-Pok.000028 (biuro)', 748, NULL, 1, 522)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000029', N'Szcze.OD Wały 02-Ptr.-01-Pok.000029 (po.gosp.)', 749, NULL, 1, 523)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000030', N'Szcze.OD Wały 02-Ptr.-01-Pok.000030 (warszt)', 750, NULL, 1, 524)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000031', N'Szcze.OD Wały 02-Ptr.-01-Pok.000031 (warszt)', 751, NULL, 1, 525)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000032', N'Szcze.OD Wały 02-Ptr.-01-Pok.000032 (warszt)', 752, NULL, 1, 526)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000033', N'Szcze.OD Wały 02-Ptr.-01-Pok.000033 (warszt)', 753, NULL, 1, 527)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000034', N'Szcze.OD Wały 02-Ptr.-01-Pok.000034 (biuro)', 754, NULL, 1, 528)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000035', N'Szcze.OD Wały 02-Ptr.-01-Pok.000035 (biuro)', 755, NULL, 1, 529)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00035A', N'Szcze.OD Wały 02-Ptr.-01-Pok.00035A (portie.)', 756, NULL, 1, 530)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000036', N'Szcze.OD Wały 02-Ptr.-01-Pok.000036 (lab.)', 757, NULL, 1, 531)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00036A', N'Szcze.OD Wały 02-Ptr.-01-Pok.00036A (biuro)', 758, NULL, 1, 532)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000037', N'Szcze.OD Wały 02-Ptr.-01-Pok.000037 (toaleta)', 759, NULL, 1, 533)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000038', N'Szcze.OD Wały 02-Ptr.-01-Pok.000038 (szatnia)', 760, NULL, 1, 534)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000039', N'Szcze.OD Wały 02-Ptr.-01-Pok.000039 (po.gosp.)', 761, NULL, 1, 535)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000042', N'Szcze.OD Wały 02-Ptr.-01-Pok.000042 (lab.)', 762, NULL, 1, 536)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.00042A', N'Szcze.OD Wały 02-Ptr.-01-Pok.00042A (lab.)', 763, NULL, 1, 537)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000043', N'Szcze.OD Wały 02-Ptr.-01-Pok.000043 (lab.)', 764, NULL, 1, 538)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000044', N'Szcze.OD Wały 02-Ptr.-01-Pok.000044 (lab.)', 765, NULL, 1, 539)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000048', N'Szcze.OD Wały 02-Ptr.-01-Pok.000048 (lab.)', 766, NULL, 1, 540)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000049', N'Szcze.OD Wały 02-Ptr.-01-Pok.000049 (po.gosp.)', 767, NULL, 1, 541)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000050', N'Szcze.OD Wały 02-Ptr.-01-Pok.000050 (biuro)', 768, NULL, 1, 542)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000051', N'Szcze.OD Wały 02-Ptr.-01-Pok.000051 (lab.)', 769, NULL, 1, 543)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000052', N'Szcze.OD Wały 02-Ptr.-01-Pok.000052 (po.gosp.)', 770, NULL, 1, 544)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000053', N'Szcze.OD Wały 02-Ptr.-01-Pok.000053 (lab.)', 771, NULL, 1, 545)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000054', N'Szcze.OD Wały 02-Ptr.-01-Pok.000054 (lab.)', 772, NULL, 1, 546)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000055', N'Szcze.OD Wały 02-Ptr.-01-Pok.000055 (po.gosp.)', 773, NULL, 1, 547)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000056', N'Szcze.OD Wały 02-Ptr.-01-Pok.000056 (po.gosp.)', 774, NULL, 1, 548)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000057', N'Szcze.OD Wały 02-Ptr.-01-Pok.000057 (po.gosp.)', 775, NULL, 1, 549)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.-01.000058', N'Szcze.OD Wały 02-Ptr.-01-Pok.000058 (po.gosp.)', 776, NULL, 1, 550)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000008', N'Szcze.OD Wały 02-Ptr.000-Pok.000008 (biuro)', 777, NULL, 1, 551)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000009', N'Szcze.OD Wały 02-Ptr.000-Pok.000009 (biuro)', 778, NULL, 1, 552)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000010', N'Szcze.OD Wały 02-Ptr.000-Pok.000010 (biuro)', 779, NULL, 1, 553)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000011', N'Szcze.OD Wały 02-Ptr.000-Pok.000011 (biuro)', 780, NULL, 1, 554)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000012', N'Szcze.OD Wały 02-Ptr.000-Pok.000012 (sal.wykł)', 781, NULL, 1, 555)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000013', N'Szcze.OD Wały 02-Ptr.000-Pok.000013 (sal.wykł)', 782, NULL, 1, 556)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000014', N'Szcze.OD Wały 02-Ptr.000-Pok.000014 (sal.wykł)', 783, NULL, 1, 557)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000015', N'Szcze.OD Wały 02-Ptr.000-Pok.000015 (portie.)', 784, NULL, 1, 558)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000016', N'Szcze.OD Wały 02-Ptr.000-Pok.000016 (serwer)', 785, NULL, 1, 559)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000017', N'Szcze.OD Wały 02-Ptr.000-Pok.000017 (biuro)', 786, NULL, 1, 560)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000018', N'Szcze.OD Wały 02-Ptr.000-Pok.000018 (biuro)', 787, NULL, 1, 561)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000019', N'Szcze.OD Wały 02-Ptr.000-Pok.000019 (sal.wykł)', 788, NULL, 1, 562)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000021', N'Szcze.OD Wały 02-Ptr.000-Pok.000021 (sal.wykł)', 789, NULL, 1, 563)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000022', N'Szcze.OD Wały 02-Ptr.000-Pok.000022 (sal.wykł)', 790, NULL, 1, 564)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000023', N'Szcze.OD Wały 02-Ptr.000-Pok.000023 (po.gosp.)', 791, NULL, 1, 565)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000024', N'Szcze.OD Wały 02-Ptr.000-Pok.000024 (biuro)', 792, NULL, 1, 566)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000025', N'Szcze.OD Wały 02-Ptr.000-Pok.000025 (biuro)', 793, NULL, 1, 567)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000027', N'Szcze.OD Wały 02-Ptr.000-Pok.000027 (po.gosp.)', 794, NULL, 1, 568)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000028', N'Szcze.OD Wały 02-Ptr.000-Pok.000028 (sal.wykł)', 795, NULL, 1, 569)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000029', N'Szcze.OD Wały 02-Ptr.000-Pok.000029 (toaleta)', 796, NULL, 1, 570)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000030', N'Szcze.OD Wały 02-Ptr.000-Pok.000030 (sal.wykł)', 797, NULL, 1, 571)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000031', N'Szcze.OD Wały 02-Ptr.000-Pok.000031 (lab.)', 798, NULL, 1, 572)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000032', N'Szcze.OD Wały 02-Ptr.000-Pok.000032 (lab.)', 799, NULL, 1, 573)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000033', N'Szcze.OD Wały 02-Ptr.000-Pok.000033 (sal.wykł)', 800, NULL, 1, 574)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000034', N'Szcze.OD Wały 02-Ptr.000-Pok.000034 (biuro)', 801, NULL, 1, 575)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000035', N'Szcze.OD Wały 02-Ptr.000-Pok.000035 (biuro)', 802, NULL, 1, 576)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000036', N'Szcze.OD Wały 02-Ptr.000-Pok.000036 (lab.)', 803, NULL, 1, 577)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.00036A', N'Szcze.OD Wały 02-Ptr.000-Pok.00036A (biuro)', 804, NULL, 1, 578)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000037', N'Szcze.OD Wały 02-Ptr.000-Pok.000037 (lab.)', 805, NULL, 1, 579)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000038', N'Szcze.OD Wały 02-Ptr.000-Pok.000038 (biuro)', 806, NULL, 1, 580)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000039', N'Szcze.OD Wały 02-Ptr.000-Pok.000039 (biuro)', 807, NULL, 1, 581)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.00040A', N'Szcze.OD Wały 02-Ptr.000-Pok.00040A (serwer)', 808, NULL, 1, 582)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000040', N'Szcze.OD Wały 02-Ptr.000-Pok.000040 (lab.)', 809, NULL, 1, 583)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.00042A', N'Szcze.OD Wały 02-Ptr.000-Pok.00042A (lab.)', 810, NULL, 1, 584)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.00042B', N'Szcze.OD Wały 02-Ptr.000-Pok.00042B (lab.)', 811, NULL, 1, 585)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000046', N'Szcze.OD Wały 02-Ptr.000-Pok.000046 (lab.)', 812, NULL, 1, 586)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000047', N'Szcze.OD Wały 02-Ptr.000-Pok.000047 (lab.)', 813, NULL, 1, 587)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000048', N'Szcze.OD Wały 02-Ptr.000-Pok.000048 (biuro)', 814, NULL, 1, 588)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000049', N'Szcze.OD Wały 02-Ptr.000-Pok.000049 (biuro)', 815, NULL, 1, 589)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000050', N'Szcze.OD Wały 02-Ptr.000-Pok.000050 (biuro)', 816, NULL, 1, 590)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.00050A', N'Szcze.OD Wały 02-Ptr.000-Pok.00050A (toaleta)', 817, NULL, 1, 591)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000051', N'Szcze.OD Wały 02-Ptr.000-Pok.000051 (lab.)', 818, NULL, 1, 592)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.000.000052', N'Szcze.OD Wały 02-Ptr.000-Pok.000052 (po.gosp.)', 819, NULL, 1, 593)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000105', N'Szcze.OD Wały 02-Ptr.001-Pok.000105 (biuro)', 820, NULL, 1, 594)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000106', N'Szcze.OD Wały 02-Ptr.001-Pok.000106 (biuro)', 821, NULL, 1, 595)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000107', N'Szcze.OD Wały 02-Ptr.001-Pok.000107 (biuro)', 822, NULL, 1, 596)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000108', N'Szcze.OD Wały 02-Ptr.001-Pok.000108 (biuro)', 823, NULL, 1, 597)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000109', N'Szcze.OD Wały 02-Ptr.001-Pok.000109 (biuro)', 824, NULL, 1, 598)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000110', N'Szcze.OD Wały 02-Ptr.001-Pok.000110 (lab.)', 825, NULL, 1, 599)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000111', N'Szcze.OD Wały 02-Ptr.001-Pok.000111 (lab.)', 826, NULL, 1, 600)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000112', N'Szcze.OD Wały 02-Ptr.001-Pok.000112 (sal.wykł)', 827, NULL, 1, 601)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000113', N'Szcze.OD Wały 02-Ptr.001-Pok.000113 (lab.)', 828, NULL, 1, 602)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000114', N'Szcze.OD Wały 02-Ptr.001-Pok.000114 (lab.)', 829, NULL, 1, 603)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000115', N'Szcze.OD Wały 02-Ptr.001-Pok.000115 (lab.)', 830, NULL, 1, 604)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000116', N'Szcze.OD Wały 02-Ptr.001-Pok.000116 (sal.wykł)', 831, NULL, 1, 605)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000117', N'Szcze.OD Wały 02-Ptr.001-Pok.000117 (sal.wykł)', 832, NULL, 1, 606)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000118', N'Szcze.OD Wały 02-Ptr.001-Pok.000118 (sal.wykł)', 833, NULL, 1, 607)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000119', N'Szcze.OD Wały 02-Ptr.001-Pok.000119 (sal.wykł)', 834, NULL, 1, 608)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000120', N'Szcze.OD Wały 02-Ptr.001-Pok.000120 (sal.wykł)', 835, NULL, 1, 609)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000121', N'Szcze.OD Wały 02-Ptr.001-Pok.000121 (sal.wykł)', 836, NULL, 1, 610)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000122', N'Szcze.OD Wały 02-Ptr.001-Pok.000122 (pom.soc.)', 837, NULL, 1, 611)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000123', N'Szcze.OD Wały 02-Ptr.001-Pok.000123 (biuro)', 838, NULL, 1, 612)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.00123A', N'Szcze.OD Wały 02-Ptr.001-Pok.00123A (toaleta)', 839, NULL, 1, 613)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000124', N'Szcze.OD Wały 02-Ptr.001-Pok.000124 (biuro)', 840, NULL, 1, 614)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000127', N'Szcze.OD Wały 02-Ptr.001-Pok.000127 (biuro)', 841, NULL, 1, 615)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000128', N'Szcze.OD Wały 02-Ptr.001-Pok.000128 (biuro)', 842, NULL, 1, 616)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000129', N'Szcze.OD Wały 02-Ptr.001-Pok.000129 (biuro)', 843, NULL, 1, 617)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000130', N'Szcze.OD Wały 02-Ptr.001-Pok.000130 (biuro)', 844, NULL, 1, 618)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000131', N'Szcze.OD Wały 02-Ptr.001-Pok.000131 (sal.wykł)', 845, NULL, 1, 619)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.00131A', N'Szcze.OD Wały 02-Ptr.001-Pok.00131A (biuro)', 846, NULL, 1, 620)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000133', N'Szcze.OD Wały 02-Ptr.001-Pok.000133 (biuro)', 847, NULL, 1, 621)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000134', N'Szcze.OD Wały 02-Ptr.001-Pok.000134 (lab.)', 848, NULL, 1, 622)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.00134A', N'Szcze.OD Wały 02-Ptr.001-Pok.00134A (biuro)', 849, NULL, 1, 623)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000135', N'Szcze.OD Wały 02-Ptr.001-Pok.000135 (biuro)', 850, NULL, 1, 624)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000136', N'Szcze.OD Wały 02-Ptr.001-Pok.000136 (biuro)', 851, NULL, 1, 625)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000137', N'Szcze.OD Wały 02-Ptr.001-Pok.000137 (biuro)', 852, NULL, 1, 626)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000138', N'Szcze.OD Wały 02-Ptr.001-Pok.000138 (biuro)', 853, NULL, 1, 627)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000139', N'Szcze.OD Wały 02-Ptr.001-Pok.000139 (biuro)', 854, NULL, 1, 628)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000140', N'Szcze.OD Wały 02-Ptr.001-Pok.000140 (biuro)', 855, NULL, 1, 629)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000141', N'Szcze.OD Wały 02-Ptr.001-Pok.000141 (biuro)', 856, NULL, 1, 630)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000142', N'Szcze.OD Wały 02-Ptr.001-Pok.000142 (biuro)', 857, NULL, 1, 631)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000143', N'Szcze.OD Wały 02-Ptr.001-Pok.000143 (biuro)', 858, NULL, 1, 632)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000144', N'Szcze.OD Wały 02-Ptr.001-Pok.000144 (biuro)', 859, NULL, 1, 633)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000145', N'Szcze.OD Wały 02-Ptr.001-Pok.000145 (biuro)', 860, NULL, 1, 634)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000146', N'Szcze.OD Wały 02-Ptr.001-Pok.000146 (biuro)', 861, NULL, 1, 635)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000147', N'Szcze.OD Wały 02-Ptr.001-Pok.000147 (biuro)', 862, NULL, 1, 636)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000148', N'Szcze.OD Wały 02-Ptr.001-Pok.000148 (biuro)', 863, NULL, 1, 637)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000149', N'Szcze.OD Wały 02-Ptr.001-Pok.000149 (biuro)', 864, NULL, 1, 638)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000150', N'Szcze.OD Wały 02-Ptr.001-Pok.000150 (biuro)', 865, NULL, 1, 639)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000151', N'Szcze.OD Wały 02-Ptr.001-Pok.000151 (biuro)', 866, NULL, 1, 640)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000152', N'Szcze.OD Wały 02-Ptr.001-Pok.000152 (lab.)', 867, NULL, 1, 641)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000153', N'Szcze.OD Wały 02-Ptr.001-Pok.000153 (biuro)', 868, NULL, 1, 642)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000154', N'Szcze.OD Wały 02-Ptr.001-Pok.000154 (biuro)', 869, NULL, 1, 643)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000155', N'Szcze.OD Wały 02-Ptr.001-Pok.000155 (biuro)', 870, NULL, 1, 644)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000156', N'Szcze.OD Wały 02-Ptr.001-Pok.000156 (biuro)', 871, NULL, 1, 645)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000157', N'Szcze.OD Wały 02-Ptr.001-Pok.000157 (biuro)', 872, NULL, 1, 646)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.001.000158', N'Szcze.OD Wały 02-Ptr.001-Pok.000158 (biuro)', 873, NULL, 1, 647)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000205', N'Szcze.OD Wały 02-Ptr.002-Pok.000205 (biuro)', 874, NULL, 1, 648)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000206', N'Szcze.OD Wały 02-Ptr.002-Pok.000206 (biuro)', 875, NULL, 1, 649)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000208', N'Szcze.OD Wały 02-Ptr.002-Pok.000208 (lab.)', 876, NULL, 1, 650)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000209', N'Szcze.OD Wały 02-Ptr.002-Pok.000209 (lab.)', 877, NULL, 1, 651)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000210', N'Szcze.OD Wały 02-Ptr.002-Pok.000210 (sal.wykł)', 878, NULL, 1, 652)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000211', N'Szcze.OD Wały 02-Ptr.002-Pok.000211 (sal.wykł)', 879, NULL, 1, 653)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000212', N'Szcze.OD Wały 02-Ptr.002-Pok.000212 (lab.)', 880, NULL, 1, 654)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000213', N'Szcze.OD Wały 02-Ptr.002-Pok.000213 (lab.)', 881, NULL, 1, 655)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000214', N'Szcze.OD Wały 02-Ptr.002-Pok.000214 (lab.)', 882, NULL, 1, 656)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214A', N'Szcze.OD Wały 02-Ptr.002-Pok.00214A (lab.)', 883, NULL, 1, 657)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214B', N'Szcze.OD Wały 02-Ptr.002-Pok.00214B (serwer)', 884, NULL, 1, 658)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214C', N'Szcze.OD Wały 02-Ptr.002-Pok.00214C (biuro)', 885, NULL, 1, 659)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214D', N'Szcze.OD Wały 02-Ptr.002-Pok.00214D (biuro)', 886, NULL, 1, 660)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214E', N'Szcze.OD Wały 02-Ptr.002-Pok.00214E (po.gosp.)', 887, NULL, 1, 661)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00214F', N'Szcze.OD Wały 02-Ptr.002-Pok.00214F (toaleta)', 888, NULL, 1, 662)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000215', N'Szcze.OD Wały 02-Ptr.002-Pok.000215 (serwer)', 889, NULL, 1, 663)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.00215A', N'Szcze.OD Wały 02-Ptr.002-Pok.00215A (biuro)', 890, NULL, 1, 664)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000216', N'Szcze.OD Wały 02-Ptr.002-Pok.000216 (lab.)', 891, NULL, 1, 665)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000217', N'Szcze.OD Wały 02-Ptr.002-Pok.000217 (sal.wykł)', 892, NULL, 1, 666)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000218', N'Szcze.OD Wały 02-Ptr.002-Pok.000218 (lab.)', 893, NULL, 1, 667)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000219', N'Szcze.OD Wały 02-Ptr.002-Pok.000219 (biuro)', 894, NULL, 1, 668)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000220', N'Szcze.OD Wały 02-Ptr.002-Pok.000220 (sal.wykł)', 895, NULL, 1, 669)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000221', N'Szcze.OD Wały 02-Ptr.002-Pok.000221 (sal.wykł)', 896, NULL, 1, 670)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000222', N'Szcze.OD Wały 02-Ptr.002-Pok.000222 (biuro)', 897, NULL, 1, 671)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000223', N'Szcze.OD Wały 02-Ptr.002-Pok.000223 (biuro)', 898, NULL, 1, 672)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000225', N'Szcze.OD Wały 02-Ptr.002-Pok.000225 (serwer)', 899, NULL, 1, 673)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000226', N'Szcze.OD Wały 02-Ptr.002-Pok.000226 (lab.)', 900, NULL, 1, 674)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000227', N'Szcze.OD Wały 02-Ptr.002-Pok.000227 (biuro)', 901, NULL, 1, 675)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000228', N'Szcze.OD Wały 02-Ptr.002-Pok.000228 (biuro)', 902, NULL, 1, 676)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000229', N'Szcze.OD Wały 02-Ptr.002-Pok.000229 (biuro)', 903, NULL, 1, 677)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000232', N'Szcze.OD Wały 02-Ptr.002-Pok.000232 (biuro)', 904, NULL, 1, 678)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000233', N'Szcze.OD Wały 02-Ptr.002-Pok.000233 (biuro)', 905, NULL, 1, 679)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000234', N'Szcze.OD Wały 02-Ptr.002-Pok.000234 (biuro)', 906, NULL, 1, 680)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000235', N'Szcze.OD Wały 02-Ptr.002-Pok.000235 (biuro)', 907, NULL, 1, 681)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000236', N'Szcze.OD Wały 02-Ptr.002-Pok.000236 (biuro)', 908, NULL, 1, 682)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000237', N'Szcze.OD Wały 02-Ptr.002-Pok.000237 (biuro)', 909, NULL, 1, 683)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000238', N'Szcze.OD Wały 02-Ptr.002-Pok.000238 (biuro)', 910, NULL, 1, 684)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000239', N'Szcze.OD Wały 02-Ptr.002-Pok.000239 (biuro)', 911, NULL, 1, 685)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000240', N'Szcze.OD Wały 02-Ptr.002-Pok.000240 (biuro)', 912, NULL, 1, 686)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.002.000244', N'Szcze.OD Wały 02-Ptr.002-Pok.000244 (biuro)', 913, NULL, 1, 687)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000301', N'Szcze.OD Wały 02-Ptr.003-Pok.000301 (lab.)', 914, NULL, 1, 688)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000302', N'Szcze.OD Wały 02-Ptr.003-Pok.000302 (biuro)', 915, NULL, 1, 689)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000303', N'Szcze.OD Wały 02-Ptr.003-Pok.000303 (biuro)', 916, NULL, 1, 690)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000304', N'Szcze.OD Wały 02-Ptr.003-Pok.000304 (biuro)', 917, NULL, 1, 691)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000306', N'Szcze.OD Wały 02-Ptr.003-Pok.000306 (lab.)', 918, NULL, 1, 692)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000307', N'Szcze.OD Wały 02-Ptr.003-Pok.000307 (lab.)', 919, NULL, 1, 693)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000308', N'Szcze.OD Wały 02-Ptr.003-Pok.000308 (lab.)', 920, NULL, 1, 694)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000309', N'Szcze.OD Wały 02-Ptr.003-Pok.000309 (lab.)', 921, NULL, 1, 695)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000310', N'Szcze.OD Wały 02-Ptr.003-Pok.000310 (pom.tech)', 922, NULL, 1, 696)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000311', N'Szcze.OD Wały 02-Ptr.003-Pok.000311 (lab.)', 923, NULL, 1, 697)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000312', N'Szcze.OD Wały 02-Ptr.003-Pok.000312 (lab.)', 924, NULL, 1, 698)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000313', N'Szcze.OD Wały 02-Ptr.003-Pok.000313 (lab.)', 925, NULL, 1, 699)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000315', N'Szcze.OD Wały 02-Ptr.003-Pok.000315 (biuro)', 926, NULL, 1, 700)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000316', N'Szcze.OD Wały 02-Ptr.003-Pok.000316 (biuro)', 927, NULL, 1, 701)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000317', N'Szcze.OD Wały 02-Ptr.003-Pok.000317 (lab.)', 928, NULL, 1, 702)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000318', N'Szcze.OD Wały 02-Ptr.003-Pok.000318 (lab.)', 929, NULL, 1, 703)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000319', N'Szcze.OD Wały 02-Ptr.003-Pok.000319 (lab.)', 930, NULL, 1, 704)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000320', N'Szcze.OD Wały 02-Ptr.003-Pok.000320 (lab.)', 931, NULL, 1, 705)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000321', N'Szcze.OD Wały 02-Ptr.003-Pok.000321 (lab.)', 932, NULL, 1, 706)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000322', N'Szcze.OD Wały 02-Ptr.003-Pok.000322 (biuro)', 933, NULL, 1, 707)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000323', N'Szcze.OD Wały 02-Ptr.003-Pok.000323 (lab.)', 934, NULL, 1, 708)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000324', N'Szcze.OD Wały 02-Ptr.003-Pok.000324 (lab.)', 935, NULL, 1, 709)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000325', N'Szcze.OD Wały 02-Ptr.003-Pok.000325 (biuro)', 936, NULL, 1, 710)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000326', N'Szcze.OD Wały 02-Ptr.003-Pok.000326 (lab.)', 937, NULL, 1, 711)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000327', N'Szcze.OD Wały 02-Ptr.003-Pok.000327 (lab.)', 938, NULL, 1, 712)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000328', N'Szcze.OD Wały 02-Ptr.003-Pok.000328 (biuro)', 939, NULL, 1, 713)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000329', N'Szcze.OD Wały 02-Ptr.003-Pok.000329 (biuro)', 940, NULL, 1, 714)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000330', N'Szcze.OD Wały 02-Ptr.003-Pok.000330 (lab.)', 941, NULL, 1, 715)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000331', N'Szcze.OD Wały 02-Ptr.003-Pok.000331 (lab.)', 942, NULL, 1, 716)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000334', N'Szcze.OD Wały 02-Ptr.003-Pok.000334 (biuro)', 943, NULL, 1, 717)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000337', N'Szcze.OD Wały 02-Ptr.003-Pok.000337 (lab.)', 944, NULL, 1, 718)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000338', N'Szcze.OD Wały 02-Ptr.003-Pok.000338 (biuro)', 945, NULL, 1, 719)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000339', N'Szcze.OD Wały 02-Ptr.003-Pok.000339 (lab.)', 946, NULL, 1, 720)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000340', N'Szcze.OD Wały 02-Ptr.003-Pok.000340 (biuro)', 947, NULL, 1, 721)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000341', N'Szcze.OD Wały 02-Ptr.003-Pok.000341 (biuro)', 948, NULL, 1, 722)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000342', N'Szcze.OD Wały 02-Ptr.003-Pok.000342 (biuro)', 949, NULL, 1, 723)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.003.000343', N'Szcze.OD Wały 02-Ptr.003-Pok.000343 (biuro)', 950, NULL, 1, 724)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000401', N'Szcze.OD Wały 02-Ptr.004-Pok.000401 (biuro)', 951, NULL, 1, 725)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000402', N'Szcze.OD Wały 02-Ptr.004-Pok.000402 (lab.)', 952, NULL, 1, 726)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000403', N'Szcze.OD Wały 02-Ptr.004-Pok.000403 (serwer)', 953, NULL, 1, 727)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000404', N'Szcze.OD Wały 02-Ptr.004-Pok.000404 (lab.)', 954, NULL, 1, 728)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000405', N'Szcze.OD Wały 02-Ptr.004-Pok.000405 (lab.)', 955, NULL, 1, 729)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000406', N'Szcze.OD Wały 02-Ptr.004-Pok.000406 (sal.wykł)', 956, NULL, 1, 730)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000407', N'Szcze.OD Wały 02-Ptr.004-Pok.000407 (sal.wykł)', 957, NULL, 1, 731)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000408', N'Szcze.OD Wały 02-Ptr.004-Pok.000408 (lab.)', 958, NULL, 1, 732)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000409', N'Szcze.OD Wały 02-Ptr.004-Pok.000409 (biuro)', 959, NULL, 1, 733)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000410', N'Szcze.OD Wały 02-Ptr.004-Pok.000410 (toaleta)', 960, NULL, 1, 734)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000411', N'Szcze.OD Wały 02-Ptr.004-Pok.000411 (lab.)', 961, NULL, 1, 735)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000412', N'Szcze.OD Wały 02-Ptr.004-Pok.000412 (lab.)', 962, NULL, 1, 736)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000413', N'Szcze.OD Wały 02-Ptr.004-Pok.000413 (lab.)', 963, NULL, 1, 737)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000414', N'Szcze.OD Wały 02-Ptr.004-Pok.000414 (lab.)', 964, NULL, 1, 738)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000415', N'Szcze.OD Wały 02-Ptr.004-Pok.000415 (lab.)', 965, NULL, 1, 739)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000416', N'Szcze.OD Wały 02-Ptr.004-Pok.000416 (lab.)', 966, NULL, 1, 740)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.02.004.000417', N'Szcze.OD Wały 02-Ptr.004-Pok.000417 (sal.wykł)', 967, NULL, 1, 741)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000001', N'Szcze.OD Wały 03-Ptr.-01-Pok.000001 (lab.)', 968, NULL, 1, 742)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000002', N'Szcze.OD Wały 03-Ptr.-01-Pok.000002 (lab.)', 969, NULL, 1, 743)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000003', N'Szcze.OD Wały 03-Ptr.-01-Pok.000003 (sal.wykł)', 970, NULL, 1, 744)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000004', N'Szcze.OD Wały 03-Ptr.-01-Pok.000004 (sal.wykł)', 971, NULL, 1, 745)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000005', N'Szcze.OD Wały 03-Ptr.-01-Pok.000005 (warszt)', 972, NULL, 1, 746)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000006', N'Szcze.OD Wały 03-Ptr.-01-Pok.000006 (biuro)', 973, NULL, 1, 747)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000007', N'Szcze.OD Wały 03-Ptr.-01-Pok.000007 (pom.soc.)', 974, NULL, 1, 748)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000008', N'Szcze.OD Wały 03-Ptr.-01-Pok.000008 (biuro)', 975, NULL, 1, 749)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000009', N'Szcze.OD Wały 03-Ptr.-01-Pok.000009 (lab.)', 976, NULL, 1, 750)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000010', N'Szcze.OD Wały 03-Ptr.-01-Pok.000010 (toaleta)', 977, NULL, 1, 751)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000011', N'Szcze.OD Wały 03-Ptr.-01-Pok.000011 (toaleta)', 978, NULL, 1, 752)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000012', N'Szcze.OD Wały 03-Ptr.-01-Pok.000012 (po.gosp.)', 979, NULL, 1, 753)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000013', N'Szcze.OD Wały 03-Ptr.-01-Pok.000013 (łazie.)', 980, NULL, 1, 754)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000014', N'Szcze.OD Wały 03-Ptr.-01-Pok.000014 (szatnia)', 981, NULL, 1, 755)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000015', N'Szcze.OD Wały 03-Ptr.-01-Pok.000015 (szatnia)', 982, NULL, 1, 756)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000016', N'Szcze.OD Wały 03-Ptr.-01-Pok.000016 (szatnia)', 983, NULL, 1, 757)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000017', N'Szcze.OD Wały 03-Ptr.-01-Pok.000017 (łazie.)', 984, NULL, 1, 758)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.-01.000018', N'Szcze.OD Wały 03-Ptr.-01-Pok.000018 (po.gosp.)', 985, NULL, 1, 759)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.000.000019', N'Szcze.OD Wały 03-Ptr.000-Pok.000019 (szatnia)', 986, NULL, 1, 760)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.000.000020', N'Szcze.OD Wały 03-Ptr.000-Pok.000020 (sal.gim.)', 987, NULL, 1, 761)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.03.001.000021', N'Szcze.OD Wały 03-Ptr.001-Pok.000021 (biuro)', 988, NULL, 1, 762)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000001', N'Szcze.SDM Korab-Ptr.-01-Pok.000001 (po.gosp.)', 989, NULL, 1, 763)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000002', N'Szcze.SDM Korab-Ptr.-01-Pok.000002 (mag.)', 990, NULL, 1, 764)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000003', N'Szcze.SDM Korab-Ptr.-01-Pok.000003 (warszt)', 991, NULL, 1, 765)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000005', N'Szcze.SDM Korab-Ptr.-01-Pok.000005 (warszt)', 992, NULL, 1, 766)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000006', N'Szcze.SDM Korab-Ptr.-01-Pok.000006 (mag.)', 993, NULL, 1, 767)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000007', N'Szcze.SDM Korab-Ptr.-01-Pok.000007 (mag.)', 994, NULL, 1, 768)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000008', N'Szcze.SDM Korab-Ptr.-01-Pok.000008 (pom.tech)', 995, NULL, 1, 769)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00009A', N'Szcze.SDM Korab-Ptr.-01-Pok.00009A (po.gosp.)', 996, NULL, 1, 770)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000011', N'Szcze.SDM Korab-Ptr.-01-Pok.000011 (po.gosp.)', 997, NULL, 1, 771)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000012', N'Szcze.SDM Korab-Ptr.-01-Pok.000012 (pom.soc.)', 998, NULL, 1, 772)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000013', N'Szcze.SDM Korab-Ptr.-01-Pok.000013 (pom.soc.)', 999, NULL, 1, 773)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000014', N'Szcze.SDM Korab-Ptr.-01-Pok.000014 (pom.tech)', 1000, NULL, 1, 774)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00014A', N'Szcze.SDM Korab-Ptr.-01-Pok.00014A (warszt)', 1001, NULL, 1, 775)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00014B', N'Szcze.SDM Korab-Ptr.-01-Pok.00014B (po.gosp.)', 1002, NULL, 1, 776)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00014C', N'Szcze.SDM Korab-Ptr.-01-Pok.00014C (po.gosp.)', 1003, NULL, 1, 777)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00014D', N'Szcze.SDM Korab-Ptr.-01-Pok.00014D (po.gosp.)', 1004, NULL, 1, 778)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000015', N'Szcze.SDM Korab-Ptr.-01-Pok.000015 (po.gosp.)', 1005, NULL, 1, 779)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000016', N'Szcze.SDM Korab-Ptr.-01-Pok.000016 (pom.tech)', 1006, NULL, 1, 780)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000019', N'Szcze.SDM Korab-Ptr.-01-Pok.000019 (pom.soc.)', 1007, NULL, 1, 781)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000020', N'Szcze.SDM Korab-Ptr.-01-Pok.000020 (mag.)', 1008, NULL, 1, 782)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.000021', N'Szcze.SDM Korab-Ptr.-01-Pok.000021 (po.gosp.)', 1009, NULL, 1, 783)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.-01.00021A', N'Szcze.SDM Korab-Ptr.-01-Pok.00021A (toaleta)', 1010, NULL, 1, 784)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.000.00008B', N'Szcze.SDM Korab-Ptr.000-Pok.00008B (biuro)', 1011, NULL, 1, 785)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.000.000008', N'Szcze.SDM Korab-Ptr.000-Pok.000008 (portie.)', 1012, NULL, 1, 786)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.000.00008A', N'Szcze.SDM Korab-Ptr.000-Pok.00008A (biuro)', 1013, NULL, 1, 787)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.000101', N'Szcze.SDM Korab-Ptr.001-Pok.000101 (biuro)', 1014, NULL, 1, 788)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00102A', N'Szcze.SDM Korab-Ptr.001-Pok.00102A (pokój)', 1015, NULL, 1, 789)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00102B', N'Szcze.SDM Korab-Ptr.001-Pok.00102B (pokój)', 1016, NULL, 1, 790)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00103A', N'Szcze.SDM Korab-Ptr.001-Pok.00103A (pokój)', 1017, NULL, 1, 791)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00103B', N'Szcze.SDM Korab-Ptr.001-Pok.00103B (pokój)', 1018, NULL, 1, 792)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00104A', N'Szcze.SDM Korab-Ptr.001-Pok.00104A (pokój)', 1019, NULL, 1, 793)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00104B', N'Szcze.SDM Korab-Ptr.001-Pok.00104B (pokój)', 1020, NULL, 1, 794)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00105A', N'Szcze.SDM Korab-Ptr.001-Pok.00105A (biuro)', 1021, NULL, 1, 795)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00105B', N'Szcze.SDM Korab-Ptr.001-Pok.00105B (biuro)', 1022, NULL, 1, 796)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00106A', N'Szcze.SDM Korab-Ptr.001-Pok.00106A (biuro)', 1023, NULL, 1, 797)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00106B', N'Szcze.SDM Korab-Ptr.001-Pok.00106B (biuro)', 1024, NULL, 1, 798)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00107A', N'Szcze.SDM Korab-Ptr.001-Pok.00107A (pokój)', 1025, NULL, 1, 799)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00107B', N'Szcze.SDM Korab-Ptr.001-Pok.00107B (pokój)', 1026, NULL, 1, 800)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00108A', N'Szcze.SDM Korab-Ptr.001-Pok.00108A (pokój)', 1027, NULL, 1, 801)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00108B', N'Szcze.SDM Korab-Ptr.001-Pok.00108B (pokój)', 1028, NULL, 1, 802)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00109A', N'Szcze.SDM Korab-Ptr.001-Pok.00109A (pokój)', 1029, NULL, 1, 803)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00109B', N'Szcze.SDM Korab-Ptr.001-Pok.00109B (pokój)', 1030, NULL, 1, 804)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00110A', N'Szcze.SDM Korab-Ptr.001-Pok.00110A (pokój)', 1031, NULL, 1, 805)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00110B', N'Szcze.SDM Korab-Ptr.001-Pok.00110B (pokój)', 1032, NULL, 1, 806)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00111A', N'Szcze.SDM Korab-Ptr.001-Pok.00111A (pokój)', 1033, NULL, 1, 807)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00111B', N'Szcze.SDM Korab-Ptr.001-Pok.00111B (pokój)', 1034, NULL, 1, 808)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00112A', N'Szcze.SDM Korab-Ptr.001-Pok.00112A (pokój)', 1035, NULL, 1, 809)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.00112B', N'Szcze.SDM Korab-Ptr.001-Pok.00112B (pokój)', 1036, NULL, 1, 810)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.000113', N'Szcze.SDM Korab-Ptr.001-Pok.000113 (zsyp)', 1037, NULL, 1, 811)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.000114', N'Szcze.SDM Korab-Ptr.001-Pok.000114 (kuch.)', 1038, NULL, 1, 812)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.000115', N'Szcze.SDM Korab-Ptr.001-Pok.000115 (arch)', 1039, NULL, 1, 813)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.001.000116', N'Szcze.SDM Korab-Ptr.001-Pok.000116 (mag.)', 1040, NULL, 1, 814)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.000201', N'Szcze.SDM Korab-Ptr.002-Pok.000201 (mag.)', 1041, NULL, 1, 815)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00202A', N'Szcze.SDM Korab-Ptr.002-Pok.00202A (pokój)', 1042, NULL, 1, 816)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00202B', N'Szcze.SDM Korab-Ptr.002-Pok.00202B (pokój)', 1043, NULL, 1, 817)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00203A', N'Szcze.SDM Korab-Ptr.002-Pok.00203A (pokój)', 1044, NULL, 1, 818)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00203B', N'Szcze.SDM Korab-Ptr.002-Pok.00203B (pokój)', 1045, NULL, 1, 819)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00204A', N'Szcze.SDM Korab-Ptr.002-Pok.00204A (pokój)', 1046, NULL, 1, 820)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00204B', N'Szcze.SDM Korab-Ptr.002-Pok.00204B (pokój)', 1047, NULL, 1, 821)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00205A', N'Szcze.SDM Korab-Ptr.002-Pok.00205A (pokój)', 1048, NULL, 1, 822)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00205B', N'Szcze.SDM Korab-Ptr.002-Pok.00205B (pokój)', 1049, NULL, 1, 823)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00206A', N'Szcze.SDM Korab-Ptr.002-Pok.00206A (pokój)', 1050, NULL, 1, 824)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00206B', N'Szcze.SDM Korab-Ptr.002-Pok.00206B (pokój)', 1051, NULL, 1, 825)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00207A', N'Szcze.SDM Korab-Ptr.002-Pok.00207A (pokój)', 1052, NULL, 1, 826)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00207B', N'Szcze.SDM Korab-Ptr.002-Pok.00207B (pokój)', 1053, NULL, 1, 827)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00208A', N'Szcze.SDM Korab-Ptr.002-Pok.00208A (pokój)', 1054, NULL, 1, 828)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00208B', N'Szcze.SDM Korab-Ptr.002-Pok.00208B (pokój)', 1055, NULL, 1, 829)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00209A', N'Szcze.SDM Korab-Ptr.002-Pok.00209A (pokój)', 1056, NULL, 1, 830)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00209B', N'Szcze.SDM Korab-Ptr.002-Pok.00209B (pokój)', 1057, NULL, 1, 831)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00210A', N'Szcze.SDM Korab-Ptr.002-Pok.00210A (pokój)', 1058, NULL, 1, 832)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00210B', N'Szcze.SDM Korab-Ptr.002-Pok.00210B (pokój)', 1059, NULL, 1, 833)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00211A', N'Szcze.SDM Korab-Ptr.002-Pok.00211A (pokój)', 1060, NULL, 1, 834)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00211B', N'Szcze.SDM Korab-Ptr.002-Pok.00211B (pokój)', 1061, NULL, 1, 835)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00212A', N'Szcze.SDM Korab-Ptr.002-Pok.00212A (pokój)', 1062, NULL, 1, 836)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.00212B', N'Szcze.SDM Korab-Ptr.002-Pok.00212B (pokój)', 1063, NULL, 1, 837)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.000213', N'Szcze.SDM Korab-Ptr.002-Pok.000213 (zsyp)', 1064, NULL, 1, 838)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.000214', N'Szcze.SDM Korab-Ptr.002-Pok.000214 (kuch.)', 1065, NULL, 1, 839)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.000215', N'Szcze.SDM Korab-Ptr.002-Pok.000215 (serwer)', 1066, NULL, 1, 840)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.002.000216', N'Szcze.SDM Korab-Ptr.002-Pok.000216 (po.gosp.)', 1067, NULL, 1, 841)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.000301', N'Szcze.SDM Korab-Ptr.003-Pok.000301 (sklep)', 1068, NULL, 1, 842)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00302A', N'Szcze.SDM Korab-Ptr.003-Pok.00302A (pokój)', 1069, NULL, 1, 843)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00302B', N'Szcze.SDM Korab-Ptr.003-Pok.00302B (pokój)', 1070, NULL, 1, 844)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00303A', N'Szcze.SDM Korab-Ptr.003-Pok.00303A (pokój)', 1071, NULL, 1, 845)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00303B', N'Szcze.SDM Korab-Ptr.003-Pok.00303B (pokój)', 1072, NULL, 1, 846)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00304A', N'Szcze.SDM Korab-Ptr.003-Pok.00304A (pokój)', 1073, NULL, 1, 847)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00304B', N'Szcze.SDM Korab-Ptr.003-Pok.00304B (pokój)', 1074, NULL, 1, 848)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00305A', N'Szcze.SDM Korab-Ptr.003-Pok.00305A (pokój)', 1075, NULL, 1, 849)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00305B', N'Szcze.SDM Korab-Ptr.003-Pok.00305B (pokój)', 1076, NULL, 1, 850)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00306A', N'Szcze.SDM Korab-Ptr.003-Pok.00306A (pokój)', 1077, NULL, 1, 851)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00306B', N'Szcze.SDM Korab-Ptr.003-Pok.00306B (pokój)', 1078, NULL, 1, 852)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00307A', N'Szcze.SDM Korab-Ptr.003-Pok.00307A (pokój)', 1079, NULL, 1, 853)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00307B', N'Szcze.SDM Korab-Ptr.003-Pok.00307B (pokój)', 1080, NULL, 1, 854)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00308A', N'Szcze.SDM Korab-Ptr.003-Pok.00308A (pokój)', 1081, NULL, 1, 855)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00308B', N'Szcze.SDM Korab-Ptr.003-Pok.00308B (pokój)', 1082, NULL, 1, 856)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00309A', N'Szcze.SDM Korab-Ptr.003-Pok.00309A (pokój)', 1083, NULL, 1, 857)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00309B', N'Szcze.SDM Korab-Ptr.003-Pok.00309B (pokój)', 1084, NULL, 1, 858)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00310A', N'Szcze.SDM Korab-Ptr.003-Pok.00310A (pokój)', 1085, NULL, 1, 859)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00310B', N'Szcze.SDM Korab-Ptr.003-Pok.00310B (pokój)', 1086, NULL, 1, 860)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00311A', N'Szcze.SDM Korab-Ptr.003-Pok.00311A (pokój)', 1087, NULL, 1, 861)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00311B', N'Szcze.SDM Korab-Ptr.003-Pok.00311B (pokój)', 1088, NULL, 1, 862)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00312A', N'Szcze.SDM Korab-Ptr.003-Pok.00312A (pokój)', 1089, NULL, 1, 863)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.00312B', N'Szcze.SDM Korab-Ptr.003-Pok.00312B (pokój)', 1090, NULL, 1, 864)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.000313', N'Szcze.SDM Korab-Ptr.003-Pok.000313 (zsyp)', 1091, NULL, 1, 865)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.000314', N'Szcze.SDM Korab-Ptr.003-Pok.000314 (kuch.)', 1092, NULL, 1, 866)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.000315', N'Szcze.SDM Korab-Ptr.003-Pok.000315 (po.gosp.)', 1093, NULL, 1, 867)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.003.000316', N'Szcze.SDM Korab-Ptr.003-Pok.000316 (po.gosp.)', 1094, NULL, 1, 868)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.000401', N'Szcze.SDM Korab-Ptr.004-Pok.000401 (sal.stud)', 1095, NULL, 1, 869)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00402A', N'Szcze.SDM Korab-Ptr.004-Pok.00402A (pokój)', 1096, NULL, 1, 870)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00402B', N'Szcze.SDM Korab-Ptr.004-Pok.00402B (pokój)', 1097, NULL, 1, 871)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00403A', N'Szcze.SDM Korab-Ptr.004-Pok.00403A (pokój)', 1098, NULL, 1, 872)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00403B', N'Szcze.SDM Korab-Ptr.004-Pok.00403B (pokój)', 1099, NULL, 1, 873)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00404A', N'Szcze.SDM Korab-Ptr.004-Pok.00404A (pokój)', 1100, NULL, 1, 874)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00404B', N'Szcze.SDM Korab-Ptr.004-Pok.00404B (pokój)', 1101, NULL, 1, 875)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00405A', N'Szcze.SDM Korab-Ptr.004-Pok.00405A (pokój)', 1102, NULL, 1, 876)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00405B', N'Szcze.SDM Korab-Ptr.004-Pok.00405B (pokój)', 1103, NULL, 1, 877)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00406A', N'Szcze.SDM Korab-Ptr.004-Pok.00406A (pokój)', 1104, NULL, 1, 878)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00406B', N'Szcze.SDM Korab-Ptr.004-Pok.00406B (pokój)', 1105, NULL, 1, 879)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00407A', N'Szcze.SDM Korab-Ptr.004-Pok.00407A (pokój)', 1106, NULL, 1, 880)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00407B', N'Szcze.SDM Korab-Ptr.004-Pok.00407B (pokój)', 1107, NULL, 1, 881)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00408A', N'Szcze.SDM Korab-Ptr.004-Pok.00408A (pokój)', 1108, NULL, 1, 882)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00408B', N'Szcze.SDM Korab-Ptr.004-Pok.00408B (pokój)', 1109, NULL, 1, 883)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00409A', N'Szcze.SDM Korab-Ptr.004-Pok.00409A (pokój)', 1110, NULL, 1, 884)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00409B', N'Szcze.SDM Korab-Ptr.004-Pok.00409B (pokój)', 1111, NULL, 1, 885)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00410A', N'Szcze.SDM Korab-Ptr.004-Pok.00410A (pokój)', 1112, NULL, 1, 886)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00410B', N'Szcze.SDM Korab-Ptr.004-Pok.00410B (pokój)', 1113, NULL, 1, 887)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00411A', N'Szcze.SDM Korab-Ptr.004-Pok.00411A (pokój)', 1114, NULL, 1, 888)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00411B', N'Szcze.SDM Korab-Ptr.004-Pok.00411B (pokój)', 1115, NULL, 1, 889)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00412A', N'Szcze.SDM Korab-Ptr.004-Pok.00412A (pokój)', 1116, NULL, 1, 890)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.00412B', N'Szcze.SDM Korab-Ptr.004-Pok.00412B (pokój)', 1117, NULL, 1, 891)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.000413', N'Szcze.SDM Korab-Ptr.004-Pok.000413 (zsyp)', 1118, NULL, 1, 892)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.000414', N'Szcze.SDM Korab-Ptr.004-Pok.000414 (kuch.)', 1119, NULL, 1, 893)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.000415', N'Szcze.SDM Korab-Ptr.004-Pok.000415 (po.gosp.)', 1120, NULL, 1, 894)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.004.000416', N'Szcze.SDM Korab-Ptr.004-Pok.000416 (po.gosp.)', 1121, NULL, 1, 895)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.000501', N'Szcze.SDM Korab-Ptr.005-Pok.000501 (pokój)', 1122, NULL, 1, 896)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00502A', N'Szcze.SDM Korab-Ptr.005-Pok.00502A (pokój)', 1123, NULL, 1, 897)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00502B', N'Szcze.SDM Korab-Ptr.005-Pok.00502B (pokój)', 1124, NULL, 1, 898)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00503A', N'Szcze.SDM Korab-Ptr.005-Pok.00503A (pokój)', 1125, NULL, 1, 899)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00503B', N'Szcze.SDM Korab-Ptr.005-Pok.00503B (pokój)', 1126, NULL, 1, 900)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00504A', N'Szcze.SDM Korab-Ptr.005-Pok.00504A (pokój)', 1127, NULL, 1, 901)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00504B', N'Szcze.SDM Korab-Ptr.005-Pok.00504B (pokój)', 1128, NULL, 1, 902)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00505A', N'Szcze.SDM Korab-Ptr.005-Pok.00505A (pokój)', 1129, NULL, 1, 903)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00505B', N'Szcze.SDM Korab-Ptr.005-Pok.00505B (pokój)', 1130, NULL, 1, 904)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00506A', N'Szcze.SDM Korab-Ptr.005-Pok.00506A (pokój)', 1131, NULL, 1, 905)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00506B', N'Szcze.SDM Korab-Ptr.005-Pok.00506B (pokój)', 1132, NULL, 1, 906)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00507A', N'Szcze.SDM Korab-Ptr.005-Pok.00507A (pokój)', 1133, NULL, 1, 907)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00507B', N'Szcze.SDM Korab-Ptr.005-Pok.00507B (pokój)', 1134, NULL, 1, 908)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00508A', N'Szcze.SDM Korab-Ptr.005-Pok.00508A (pokój)', 1135, NULL, 1, 909)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00508B', N'Szcze.SDM Korab-Ptr.005-Pok.00508B (pokój)', 1136, NULL, 1, 910)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00509A', N'Szcze.SDM Korab-Ptr.005-Pok.00509A (pokój)', 1137, NULL, 1, 911)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00509B', N'Szcze.SDM Korab-Ptr.005-Pok.00509B (pokój)', 1138, NULL, 1, 912)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00510A', N'Szcze.SDM Korab-Ptr.005-Pok.00510A (pokój)', 1139, NULL, 1, 913)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00510B', N'Szcze.SDM Korab-Ptr.005-Pok.00510B (pokój)', 1140, NULL, 1, 914)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00511A', N'Szcze.SDM Korab-Ptr.005-Pok.00511A (pokój)', 1141, NULL, 1, 915)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00511B', N'Szcze.SDM Korab-Ptr.005-Pok.00511B (pokój)', 1142, NULL, 1, 916)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00512A', N'Szcze.SDM Korab-Ptr.005-Pok.00512A (pokój)', 1143, NULL, 1, 917)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.00512B', N'Szcze.SDM Korab-Ptr.005-Pok.00512B (pokój)', 1144, NULL, 1, 918)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.000513', N'Szcze.SDM Korab-Ptr.005-Pok.000513 (zsyp)', 1145, NULL, 1, 919)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.000514', N'Szcze.SDM Korab-Ptr.005-Pok.000514 (kuch.)', 1146, NULL, 1, 920)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.000515', N'Szcze.SDM Korab-Ptr.005-Pok.000515 (po.gosp.)', 1147, NULL, 1, 921)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.005.000516', N'Szcze.SDM Korab-Ptr.005-Pok.000516 (po.gosp.)', 1148, NULL, 1, 922)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.000601', N'Szcze.SDM Korab-Ptr.006-Pok.000601 (pokój)', 1149, NULL, 1, 923)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00602A', N'Szcze.SDM Korab-Ptr.006-Pok.00602A (pokój)', 1150, NULL, 1, 924)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00602B', N'Szcze.SDM Korab-Ptr.006-Pok.00602B (pokój)', 1151, NULL, 1, 925)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00603A', N'Szcze.SDM Korab-Ptr.006-Pok.00603A (pokój)', 1152, NULL, 1, 926)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00603B', N'Szcze.SDM Korab-Ptr.006-Pok.00603B (pokój)', 1153, NULL, 1, 927)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00604A', N'Szcze.SDM Korab-Ptr.006-Pok.00604A (pokój)', 1154, NULL, 1, 928)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00604B', N'Szcze.SDM Korab-Ptr.006-Pok.00604B (pokój)', 1155, NULL, 1, 929)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00605A', N'Szcze.SDM Korab-Ptr.006-Pok.00605A (pokój)', 1156, NULL, 1, 930)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00605B', N'Szcze.SDM Korab-Ptr.006-Pok.00605B (pokój)', 1157, NULL, 1, 931)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00606A', N'Szcze.SDM Korab-Ptr.006-Pok.00606A (pokój)', 1158, NULL, 1, 932)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00606B', N'Szcze.SDM Korab-Ptr.006-Pok.00606B (pokój)', 1159, NULL, 1, 933)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00607A', N'Szcze.SDM Korab-Ptr.006-Pok.00607A (pokój)', 1160, NULL, 1, 934)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00607B', N'Szcze.SDM Korab-Ptr.006-Pok.00607B (pokój)', 1161, NULL, 1, 935)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00608A', N'Szcze.SDM Korab-Ptr.006-Pok.00608A (pokój)', 1162, NULL, 1, 936)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00608B', N'Szcze.SDM Korab-Ptr.006-Pok.00608B (pokój)', 1163, NULL, 1, 937)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00609A', N'Szcze.SDM Korab-Ptr.006-Pok.00609A (pokój)', 1164, NULL, 1, 938)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00609B', N'Szcze.SDM Korab-Ptr.006-Pok.00609B (pokój)', 1165, NULL, 1, 939)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00610A', N'Szcze.SDM Korab-Ptr.006-Pok.00610A (pokój)', 1166, NULL, 1, 940)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00610B', N'Szcze.SDM Korab-Ptr.006-Pok.00610B (pokój)', 1167, NULL, 1, 941)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00611A', N'Szcze.SDM Korab-Ptr.006-Pok.00611A (pokój)', 1168, NULL, 1, 942)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00611B', N'Szcze.SDM Korab-Ptr.006-Pok.00611B (pokój)', 1169, NULL, 1, 943)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00612A', N'Szcze.SDM Korab-Ptr.006-Pok.00612A (pokój)', 1170, NULL, 1, 944)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.00612B', N'Szcze.SDM Korab-Ptr.006-Pok.00612B (pokój)', 1171, NULL, 1, 945)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.000613', N'Szcze.SDM Korab-Ptr.006-Pok.000613 (zsyp)', 1172, NULL, 1, 946)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.000614', N'Szcze.SDM Korab-Ptr.006-Pok.000614 (kuch.)', 1173, NULL, 1, 947)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.000615', N'Szcze.SDM Korab-Ptr.006-Pok.000615 (po.gosp.)', 1174, NULL, 1, 948)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.006.000616', N'Szcze.SDM Korab-Ptr.006-Pok.000616 (po.gosp.)', 1175, NULL, 1, 949)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.000701', N'Szcze.SDM Korab-Ptr.007-Pok.000701 (po.gosp.)', 1176, NULL, 1, 950)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00702A', N'Szcze.SDM Korab-Ptr.007-Pok.00702A (pokój)', 1177, NULL, 1, 951)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00702B', N'Szcze.SDM Korab-Ptr.007-Pok.00702B (pokój)', 1178, NULL, 1, 952)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00703A', N'Szcze.SDM Korab-Ptr.007-Pok.00703A (pokój)', 1179, NULL, 1, 953)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00703B', N'Szcze.SDM Korab-Ptr.007-Pok.00703B (pokój)', 1180, NULL, 1, 954)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00704A', N'Szcze.SDM Korab-Ptr.007-Pok.00704A (pokój)', 1181, NULL, 1, 955)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00704B', N'Szcze.SDM Korab-Ptr.007-Pok.00704B (pokój)', 1182, NULL, 1, 956)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00705A', N'Szcze.SDM Korab-Ptr.007-Pok.00705A (pokój)', 1183, NULL, 1, 957)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00705B', N'Szcze.SDM Korab-Ptr.007-Pok.00705B (pokój)', 1184, NULL, 1, 958)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00706A', N'Szcze.SDM Korab-Ptr.007-Pok.00706A (pokój)', 1185, NULL, 1, 959)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00706B', N'Szcze.SDM Korab-Ptr.007-Pok.00706B (pokój)', 1186, NULL, 1, 960)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00707A', N'Szcze.SDM Korab-Ptr.007-Pok.00707A (pokój)', 1187, NULL, 1, 961)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00707B', N'Szcze.SDM Korab-Ptr.007-Pok.00707B (pokój)', 1188, NULL, 1, 962)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00708A', N'Szcze.SDM Korab-Ptr.007-Pok.00708A (pokój)', 1189, NULL, 1, 963)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00708B', N'Szcze.SDM Korab-Ptr.007-Pok.00708B (pokój)', 1190, NULL, 1, 964)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00709A', N'Szcze.SDM Korab-Ptr.007-Pok.00709A (pokój)', 1191, NULL, 1, 965)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00709B', N'Szcze.SDM Korab-Ptr.007-Pok.00709B (pokój)', 1192, NULL, 1, 966)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00710A', N'Szcze.SDM Korab-Ptr.007-Pok.00710A (pokój)', 1193, NULL, 1, 967)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00710B', N'Szcze.SDM Korab-Ptr.007-Pok.00710B (pokój)', 1194, NULL, 1, 968)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00711A', N'Szcze.SDM Korab-Ptr.007-Pok.00711A (pokój)', 1195, NULL, 1, 969)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00711B', N'Szcze.SDM Korab-Ptr.007-Pok.00711B (pokój)', 1196, NULL, 1, 970)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00712A', N'Szcze.SDM Korab-Ptr.007-Pok.00712A (pokój)', 1197, NULL, 1, 971)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.00712B', N'Szcze.SDM Korab-Ptr.007-Pok.00712B (pokój)', 1198, NULL, 1, 972)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.000713', N'Szcze.SDM Korab-Ptr.007-Pok.000713 (zsyp)', 1199, NULL, 1, 973)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.000714', N'Szcze.SDM Korab-Ptr.007-Pok.000714 (kuch.)', 1200, NULL, 1, 974)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.000715', N'Szcze.SDM Korab-Ptr.007-Pok.000715 (po.gosp.)', 1201, NULL, 1, 975)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.007.000716', N'Szcze.SDM Korab-Ptr.007-Pok.000716 (po.gosp.)', 1202, NULL, 1, 976)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.000801', N'Szcze.SDM Korab-Ptr.008-Pok.000801 (sal.stud)', 1203, NULL, 1, 977)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00802A', N'Szcze.SDM Korab-Ptr.008-Pok.00802A (pokój)', 1204, NULL, 1, 978)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00802B', N'Szcze.SDM Korab-Ptr.008-Pok.00802B (pokój)', 1205, NULL, 1, 979)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00803A', N'Szcze.SDM Korab-Ptr.008-Pok.00803A (pokój)', 1206, NULL, 1, 980)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00803B', N'Szcze.SDM Korab-Ptr.008-Pok.00803B (pokój)', 1207, NULL, 1, 981)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00804A', N'Szcze.SDM Korab-Ptr.008-Pok.00804A (pokój)', 1208, NULL, 1, 982)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00804B', N'Szcze.SDM Korab-Ptr.008-Pok.00804B (pokój)', 1209, NULL, 1, 983)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00805A', N'Szcze.SDM Korab-Ptr.008-Pok.00805A (pokój)', 1210, NULL, 1, 984)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00805B', N'Szcze.SDM Korab-Ptr.008-Pok.00805B (pokój)', 1211, NULL, 1, 985)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00806A', N'Szcze.SDM Korab-Ptr.008-Pok.00806A (pokój)', 1212, NULL, 1, 986)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00806B', N'Szcze.SDM Korab-Ptr.008-Pok.00806B (pokój)', 1213, NULL, 1, 987)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00807A', N'Szcze.SDM Korab-Ptr.008-Pok.00807A (pokój)', 1214, NULL, 1, 988)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00807B', N'Szcze.SDM Korab-Ptr.008-Pok.00807B (pokój)', 1215, NULL, 1, 989)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00808A', N'Szcze.SDM Korab-Ptr.008-Pok.00808A (pokój)', 1216, NULL, 1, 990)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00808B', N'Szcze.SDM Korab-Ptr.008-Pok.00808B (pokój)', 1217, NULL, 1, 991)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00809A', N'Szcze.SDM Korab-Ptr.008-Pok.00809A (pokój)', 1218, NULL, 1, 992)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00809B', N'Szcze.SDM Korab-Ptr.008-Pok.00809B (pokój)', 1219, NULL, 1, 993)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00810A', N'Szcze.SDM Korab-Ptr.008-Pok.00810A (pokój)', 1220, NULL, 1, 994)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00810B', N'Szcze.SDM Korab-Ptr.008-Pok.00810B (pokój)', 1221, NULL, 1, 995)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00811A', N'Szcze.SDM Korab-Ptr.008-Pok.00811A (pokój)', 1222, NULL, 1, 996)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00811B', N'Szcze.SDM Korab-Ptr.008-Pok.00811B (pokój)', 1223, NULL, 1, 997)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00812A', N'Szcze.SDM Korab-Ptr.008-Pok.00812A (pokój)', 1224, NULL, 1, 998)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.00812B', N'Szcze.SDM Korab-Ptr.008-Pok.00812B (pokój)', 1225, NULL, 1, 999)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.000813', N'Szcze.SDM Korab-Ptr.008-Pok.000813 (zsyp)', 1226, NULL, 1, 1000)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.000814', N'Szcze.SDM Korab-Ptr.008-Pok.000814 (kuch.)', 1227, NULL, 1, 1001)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.000815', N'Szcze.SDM Korab-Ptr.008-Pok.000815 (po.gosp.)', 1228, NULL, 1, 1002)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.008.000816', N'Szcze.SDM Korab-Ptr.008-Pok.000816 (po.gosp.)', 1229, NULL, 1, 1003)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.000901', N'Szcze.SDM Korab-Ptr.009-Pok.000901 (sal.stud)', 1230, NULL, 1, 1004)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.90002A', N'Szcze.SDM Korab-Ptr.009-Pok.90002A (pokój)', 1231, NULL, 1, 1005)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00902B', N'Szcze.SDM Korab-Ptr.009-Pok.00902B (pokój)', 1232, NULL, 1, 1006)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00903A', N'Szcze.SDM Korab-Ptr.009-Pok.00903A (pokój)', 1233, NULL, 1, 1007)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00903B', N'Szcze.SDM Korab-Ptr.009-Pok.00903B (pokój)', 1234, NULL, 1, 1008)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00904A', N'Szcze.SDM Korab-Ptr.009-Pok.00904A (pokój)', 1235, NULL, 1, 1009)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00904B', N'Szcze.SDM Korab-Ptr.009-Pok.00904B (pokój)', 1236, NULL, 1, 1010)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00905A', N'Szcze.SDM Korab-Ptr.009-Pok.00905A (pokój)', 1237, NULL, 1, 1011)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00905B', N'Szcze.SDM Korab-Ptr.009-Pok.00905B (pokój)', 1238, NULL, 1, 1012)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00906A', N'Szcze.SDM Korab-Ptr.009-Pok.00906A (pokój)', 1239, NULL, 1, 1013)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00906B', N'Szcze.SDM Korab-Ptr.009-Pok.00906B (pokój)', 1240, NULL, 1, 1014)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00907A', N'Szcze.SDM Korab-Ptr.009-Pok.00907A (pokój)', 1241, NULL, 1, 1015)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00907B', N'Szcze.SDM Korab-Ptr.009-Pok.00907B (pokój)', 1242, NULL, 1, 1016)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00908A', N'Szcze.SDM Korab-Ptr.009-Pok.00908A (pokój)', 1243, NULL, 1, 1017)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00908B', N'Szcze.SDM Korab-Ptr.009-Pok.00908B (pokój)', 1244, NULL, 1, 1018)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00909A', N'Szcze.SDM Korab-Ptr.009-Pok.00909A (pokój)', 1245, NULL, 1, 1019)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00909B', N'Szcze.SDM Korab-Ptr.009-Pok.00909B (pokój)', 1246, NULL, 1, 1020)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00910A', N'Szcze.SDM Korab-Ptr.009-Pok.00910A (pokój)', 1247, NULL, 1, 1021)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00910B', N'Szcze.SDM Korab-Ptr.009-Pok.00910B (pokój)', 1248, NULL, 1, 1022)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00911A', N'Szcze.SDM Korab-Ptr.009-Pok.00911A (pokój)', 1249, NULL, 1, 1023)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00911B', N'Szcze.SDM Korab-Ptr.009-Pok.00911B (pokój)', 1250, NULL, 1, 1024)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00912A', N'Szcze.SDM Korab-Ptr.009-Pok.00912A (pokój)', 1251, NULL, 1, 1025)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.00912B', N'Szcze.SDM Korab-Ptr.009-Pok.00912B (pokój)', 1252, NULL, 1, 1026)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.000913', N'Szcze.SDM Korab-Ptr.009-Pok.000913 (zsyp)', 1253, NULL, 1, 1027)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.000914', N'Szcze.SDM Korab-Ptr.009-Pok.000914 (kuch.)', 1254, NULL, 1, 1028)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.000915', N'Szcze.SDM Korab-Ptr.009-Pok.000915 (po.gosp.)', 1255, NULL, 1, 1029)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.009.000916', N'Szcze.SDM Korab-Ptr.009-Pok.000916 (po.gosp.)', 1256, NULL, 1, 1030)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.001001', N'Szcze.SDM Korab-Ptr.010-Pok.001001 (po.gosp.)', 1257, NULL, 1, 1031)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01002A', N'Szcze.SDM Korab-Ptr.010-Pok.01002A (pokój)', 1258, NULL, 1, 1032)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01002B', N'Szcze.SDM Korab-Ptr.010-Pok.01002B (pokój)', 1259, NULL, 1, 1033)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01003A', N'Szcze.SDM Korab-Ptr.010-Pok.01003A (pokój)', 1260, NULL, 1, 1034)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01003B', N'Szcze.SDM Korab-Ptr.010-Pok.01003B (pokój)', 1261, NULL, 1, 1035)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01004A', N'Szcze.SDM Korab-Ptr.010-Pok.01004A (pokój)', 1262, NULL, 1, 1036)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01004B', N'Szcze.SDM Korab-Ptr.010-Pok.01004B (pokój)', 1263, NULL, 1, 1037)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01005A', N'Szcze.SDM Korab-Ptr.010-Pok.01005A (pokój)', 1264, NULL, 1, 1038)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01005B', N'Szcze.SDM Korab-Ptr.010-Pok.01005B (pokój)', 1265, NULL, 1, 1039)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01006A', N'Szcze.SDM Korab-Ptr.010-Pok.01006A (pokój)', 1266, NULL, 1, 1040)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01006B', N'Szcze.SDM Korab-Ptr.010-Pok.01006B (pokój)', 1267, NULL, 1, 1041)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01007A', N'Szcze.SDM Korab-Ptr.010-Pok.01007A (pokój)', 1268, NULL, 1, 1042)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01007B', N'Szcze.SDM Korab-Ptr.010-Pok.01007B (pokój)', 1269, NULL, 1, 1043)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01008A', N'Szcze.SDM Korab-Ptr.010-Pok.01008A (pokój)', 1270, NULL, 1, 1044)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01008B', N'Szcze.SDM Korab-Ptr.010-Pok.01008B (pokój)', 1271, NULL, 1, 1045)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01009A', N'Szcze.SDM Korab-Ptr.010-Pok.01009A (pokój)', 1272, NULL, 1, 1046)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01009B', N'Szcze.SDM Korab-Ptr.010-Pok.01009B (pokój)', 1273, NULL, 1, 1047)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01010A', N'Szcze.SDM Korab-Ptr.010-Pok.01010A (pokój)', 1274, NULL, 1, 1048)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01010B', N'Szcze.SDM Korab-Ptr.010-Pok.01010B (pokój)', 1275, NULL, 1, 1049)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01011A', N'Szcze.SDM Korab-Ptr.010-Pok.01011A (pokój)', 1276, NULL, 1, 1050)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01011B', N'Szcze.SDM Korab-Ptr.010-Pok.01011B (pokój)', 1277, NULL, 1, 1051)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01012A', N'Szcze.SDM Korab-Ptr.010-Pok.01012A (pokój)', 1278, NULL, 1, 1052)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.01012B', N'Szcze.SDM Korab-Ptr.010-Pok.01012B (pokój)', 1279, NULL, 1, 1053)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.001013', N'Szcze.SDM Korab-Ptr.010-Pok.001013 (zsyp)', 1280, NULL, 1, 1054)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.001014', N'Szcze.SDM Korab-Ptr.010-Pok.001014 (kuch.)', 1281, NULL, 1, 1055)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.001015', N'Szcze.SDM Korab-Ptr.010-Pok.001015 (po.gosp.)', 1282, NULL, 1, 1056)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.010.001016', N'Szcze.SDM Korab-Ptr.010-Pok.001016 (po.gosp.)', 1283, NULL, 1, 1057)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.011.001017', N'Szcze.SDM Korab-Ptr.011-Pok.001017 (po.gosp.)', 1284, NULL, 1, 1058)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.011.01018A', N'Szcze.SDM Korab-Ptr.011-Pok.01018A (pom.tech)', 1285, NULL, 1, 1059)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.04.011.001018', N'Szcze.SDM Korab-Ptr.011-Pok.001018 (pom.tech)', 1286, NULL, 1, 1060)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000001', N'Szcze.SDM Pasat-Ptr.-01-Pok.000001 (warszt)', 1287, NULL, 1, 1061)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000002', N'Szcze.SDM Pasat-Ptr.-01-Pok.000002 (mag.)', 1288, NULL, 1, 1062)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000003', N'Szcze.SDM Pasat-Ptr.-01-Pok.000003 (mag.)', 1289, NULL, 1, 1063)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000004', N'Szcze.SDM Pasat-Ptr.-01-Pok.000004 (mag.)', 1290, NULL, 1, 1064)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000005', N'Szcze.SDM Pasat-Ptr.-01-Pok.000005 (mag.)', 1291, NULL, 1, 1065)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000006', N'Szcze.SDM Pasat-Ptr.-01-Pok.000006 (mag.)', 1292, NULL, 1, 1066)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000007', N'Szcze.SDM Pasat-Ptr.-01-Pok.000007 (po.gosp.)', 1293, NULL, 1, 1067)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000008', N'Szcze.SDM Pasat-Ptr.-01-Pok.000008 (po.gosp.)', 1294, NULL, 1, 1068)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000009', N'Szcze.SDM Pasat-Ptr.-01-Pok.000009 (mag.)', 1295, NULL, 1, 1069)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000010', N'Szcze.SDM Pasat-Ptr.-01-Pok.000010 (po.gosp.)', 1296, NULL, 1, 1070)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000011', N'Szcze.SDM Pasat-Ptr.-01-Pok.000011 (po.gosp.)', 1297, NULL, 1, 1071)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000012', N'Szcze.SDM Pasat-Ptr.-01-Pok.000012 (toaleta)', 1298, NULL, 1, 1072)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000013', N'Szcze.SDM Pasat-Ptr.-01-Pok.000013 (pom.tech)', 1299, NULL, 1, 1073)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000014', N'Szcze.SDM Pasat-Ptr.-01-Pok.000014 (warszt)', 1300, NULL, 1, 1074)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000015', N'Szcze.SDM Pasat-Ptr.-01-Pok.000015 (po.gosp.)', 1301, NULL, 1, 1075)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000016', N'Szcze.SDM Pasat-Ptr.-01-Pok.000016 (pom.tech)', 1302, NULL, 1, 1076)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.-01.000017', N'Szcze.SDM Pasat-Ptr.-01-Pok.000017 (mag.)', 1303, NULL, 1, 1077)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000001', N'Szcze.SDM Pasat-Ptr.000-Pok.000001 (gab.lek.)', 1304, NULL, 1, 1078)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00001A', N'Szcze.SDM Pasat-Ptr.000-Pok.00001A (gab.lek.)', 1305, NULL, 1, 1079)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000002', N'Szcze.SDM Pasat-Ptr.000-Pok.000002 (gab.lek.)', 1306, NULL, 1, 1080)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00002A', N'Szcze.SDM Pasat-Ptr.000-Pok.00002A (gab.lek.)', 1307, NULL, 1, 1081)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00002B', N'Szcze.SDM Pasat-Ptr.000-Pok.00002B (pom.soc.)', 1308, NULL, 1, 1082)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000003', N'Szcze.SDM Pasat-Ptr.000-Pok.000003 (korytarz)', 1309, NULL, 1, 1083)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00003A', N'Szcze.SDM Pasat-Ptr.000-Pok.00003A (gab.lek.)', 1310, NULL, 1, 1084)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00003B', N'Szcze.SDM Pasat-Ptr.000-Pok.00003B (gab.lek.)', 1311, NULL, 1, 1085)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000004', N'Szcze.SDM Pasat-Ptr.000-Pok.000004 (korytarz)', 1312, NULL, 1, 1086)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00004A', N'Szcze.SDM Pasat-Ptr.000-Pok.00004A (pom.soc.)', 1313, NULL, 1, 1087)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00004B', N'Szcze.SDM Pasat-Ptr.000-Pok.00004B (szatnia)', 1314, NULL, 1, 1088)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000005', N'Szcze.SDM Pasat-Ptr.000-Pok.000005 (korytarz)', 1315, NULL, 1, 1089)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00005A', N'Szcze.SDM Pasat-Ptr.000-Pok.00005A (pom.soc.)', 1316, NULL, 1, 1090)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00005B', N'Szcze.SDM Pasat-Ptr.000-Pok.00005B (szatnia)', 1317, NULL, 1, 1091)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000007', N'Szcze.SDM Pasat-Ptr.000-Pok.000007 (korytarz)', 1318, NULL, 1, 1092)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000008', N'Szcze.SDM Pasat-Ptr.000-Pok.000008 (po.gosp.)', 1319, NULL, 1, 1093)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000009', N'Szcze.SDM Pasat-Ptr.000-Pok.000009 (mag.)', 1320, NULL, 1, 1094)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000011', N'Szcze.SDM Pasat-Ptr.000-Pok.000011 (arch)', 1321, NULL, 1, 1095)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00012A', N'Szcze.SDM Pasat-Ptr.000-Pok.00012A (biuro)', 1322, NULL, 1, 1096)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00012B', N'Szcze.SDM Pasat-Ptr.000-Pok.00012B (biuro)', 1323, NULL, 1, 1097)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000013', N'Szcze.SDM Pasat-Ptr.000-Pok.000013 (portie.)', 1324, NULL, 1, 1098)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.000014', N'Szcze.SDM Pasat-Ptr.000-Pok.000014 (korytarz)', 1325, NULL, 1, 1099)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00014A', N'Szcze.SDM Pasat-Ptr.000-Pok.00014A (biuro)', 1326, NULL, 1, 1100)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00014B', N'Szcze.SDM Pasat-Ptr.000-Pok.00014B (biuro)', 1327, NULL, 1, 1101)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.000.00014C', N'Szcze.SDM Pasat-Ptr.000-Pok.00014C (biuro)', 1328, NULL, 1, 1102)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000100', N'Szcze.SDM Pasat-Ptr.001-Pok.000100 (po.gosp.)', 1329, NULL, 1, 1103)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000101', N'Szcze.SDM Pasat-Ptr.001-Pok.000101 (korytarz)', 1330, NULL, 1, 1104)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00101A', N'Szcze.SDM Pasat-Ptr.001-Pok.00101A (biuro)', 1331, NULL, 1, 1105)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00101B', N'Szcze.SDM Pasat-Ptr.001-Pok.00101B (biuro)', 1332, NULL, 1, 1106)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000102', N'Szcze.SDM Pasat-Ptr.001-Pok.000102 (korytarz)', 1333, NULL, 1, 1107)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00102A', N'Szcze.SDM Pasat-Ptr.001-Pok.00102A (pokój)', 1334, NULL, 1, 1108)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00102B', N'Szcze.SDM Pasat-Ptr.001-Pok.00102B (pokój)', 1335, NULL, 1, 1109)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000103', N'Szcze.SDM Pasat-Ptr.001-Pok.000103 (korytarz)', 1336, NULL, 1, 1110)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00103A', N'Szcze.SDM Pasat-Ptr.001-Pok.00103A (pokój)', 1337, NULL, 1, 1111)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00103B', N'Szcze.SDM Pasat-Ptr.001-Pok.00103B (pokój)', 1338, NULL, 1, 1112)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000104', N'Szcze.SDM Pasat-Ptr.001-Pok.000104 (korytarz)', 1339, NULL, 1, 1113)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00104A', N'Szcze.SDM Pasat-Ptr.001-Pok.00104A (pokój)', 1340, NULL, 1, 1114)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00104B', N'Szcze.SDM Pasat-Ptr.001-Pok.00104B (pokój)', 1341, NULL, 1, 1115)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000105', N'Szcze.SDM Pasat-Ptr.001-Pok.000105 (korytarz)', 1342, NULL, 1, 1116)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00105A', N'Szcze.SDM Pasat-Ptr.001-Pok.00105A (pokój)', 1343, NULL, 1, 1117)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00105B', N'Szcze.SDM Pasat-Ptr.001-Pok.00105B (pokój)', 1344, NULL, 1, 1118)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000106', N'Szcze.SDM Pasat-Ptr.001-Pok.000106 (korytarz)', 1345, NULL, 1, 1119)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00106A', N'Szcze.SDM Pasat-Ptr.001-Pok.00106A (pokój)', 1346, NULL, 1, 1120)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00106B', N'Szcze.SDM Pasat-Ptr.001-Pok.00106B (pokój)', 1347, NULL, 1, 1121)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000107', N'Szcze.SDM Pasat-Ptr.001-Pok.000107 (korytarz)', 1348, NULL, 1, 1122)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00107A', N'Szcze.SDM Pasat-Ptr.001-Pok.00107A (pokój)', 1349, NULL, 1, 1123)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00107B', N'Szcze.SDM Pasat-Ptr.001-Pok.00107B (pokój)', 1350, NULL, 1, 1124)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000108', N'Szcze.SDM Pasat-Ptr.001-Pok.000108 (korytarz)', 1351, NULL, 1, 1125)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00108A', N'Szcze.SDM Pasat-Ptr.001-Pok.00108A (pokój)', 1352, NULL, 1, 1126)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00108B', N'Szcze.SDM Pasat-Ptr.001-Pok.00108B (pokój)', 1353, NULL, 1, 1127)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000109', N'Szcze.SDM Pasat-Ptr.001-Pok.000109 (korytarz)', 1354, NULL, 1, 1128)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00109A', N'Szcze.SDM Pasat-Ptr.001-Pok.00109A (pokój)', 1355, NULL, 1, 1129)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00109B', N'Szcze.SDM Pasat-Ptr.001-Pok.00109B (pokój)', 1356, NULL, 1, 1130)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000110', N'Szcze.SDM Pasat-Ptr.001-Pok.000110 (korytarz)', 1357, NULL, 1, 1131)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00110A', N'Szcze.SDM Pasat-Ptr.001-Pok.00110A (pokój)', 1358, NULL, 1, 1132)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.00110B', N'Szcze.SDM Pasat-Ptr.001-Pok.00110B (pokój)', 1359, NULL, 1, 1133)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000111', N'Szcze.SDM Pasat-Ptr.001-Pok.000111 (po.gosp.)', 1360, NULL, 1, 1134)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000112', N'Szcze.SDM Pasat-Ptr.001-Pok.000112 (po.gosp.)', 1361, NULL, 1, 1135)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000113', N'Szcze.SDM Pasat-Ptr.001-Pok.000113 (po.gosp.)', 1362, NULL, 1, 1136)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000114', N'Szcze.SDM Pasat-Ptr.001-Pok.000114 (kuch.)', 1363, NULL, 1, 1137)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.001.000116', N'Szcze.SDM Pasat-Ptr.001-Pok.000116 (po.gosp.)', 1364, NULL, 1, 1138)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000200', N'Szcze.SDM Pasat-Ptr.002-Pok.000200 (biuro)', 1365, NULL, 1, 1139)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00200A', N'Szcze.SDM Pasat-Ptr.002-Pok.00200A (biuro)', 1366, NULL, 1, 1140)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00200B', N'Szcze.SDM Pasat-Ptr.002-Pok.00200B (biuro)', 1367, NULL, 1, 1141)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000201', N'Szcze.SDM Pasat-Ptr.002-Pok.000201 (korytarz)', 1368, NULL, 1, 1142)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00201A', N'Szcze.SDM Pasat-Ptr.002-Pok.00201A (pokój)', 1369, NULL, 1, 1143)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00201B', N'Szcze.SDM Pasat-Ptr.002-Pok.00201B (pokój)', 1370, NULL, 1, 1144)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000202', N'Szcze.SDM Pasat-Ptr.002-Pok.000202 (korytarz)', 1371, NULL, 1, 1145)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00202A', N'Szcze.SDM Pasat-Ptr.002-Pok.00202A (pokój)', 1372, NULL, 1, 1146)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00202B', N'Szcze.SDM Pasat-Ptr.002-Pok.00202B (pokój)', 1373, NULL, 1, 1147)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000203', N'Szcze.SDM Pasat-Ptr.002-Pok.000203 (korytarz)', 1374, NULL, 1, 1148)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00203A', N'Szcze.SDM Pasat-Ptr.002-Pok.00203A (pokój)', 1375, NULL, 1, 1149)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00203B', N'Szcze.SDM Pasat-Ptr.002-Pok.00203B (pokój)', 1376, NULL, 1, 1150)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000204', N'Szcze.SDM Pasat-Ptr.002-Pok.000204 (korytarz)', 1377, NULL, 1, 1151)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00204A', N'Szcze.SDM Pasat-Ptr.002-Pok.00204A (pokój)', 1378, NULL, 1, 1152)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00204B', N'Szcze.SDM Pasat-Ptr.002-Pok.00204B (pokój)', 1379, NULL, 1, 1153)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000205', N'Szcze.SDM Pasat-Ptr.002-Pok.000205 (korytarz)', 1380, NULL, 1, 1154)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00205A', N'Szcze.SDM Pasat-Ptr.002-Pok.00205A (pokój)', 1381, NULL, 1, 1155)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00205B', N'Szcze.SDM Pasat-Ptr.002-Pok.00205B (pokój)', 1382, NULL, 1, 1156)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000206', N'Szcze.SDM Pasat-Ptr.002-Pok.000206 (korytarz)', 1383, NULL, 1, 1157)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00206A', N'Szcze.SDM Pasat-Ptr.002-Pok.00206A (pokój)', 1384, NULL, 1, 1158)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00206B', N'Szcze.SDM Pasat-Ptr.002-Pok.00206B (pokój)', 1385, NULL, 1, 1159)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000207', N'Szcze.SDM Pasat-Ptr.002-Pok.000207 (korytarz)', 1386, NULL, 1, 1160)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00207A', N'Szcze.SDM Pasat-Ptr.002-Pok.00207A (pokój)', 1387, NULL, 1, 1161)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00207B', N'Szcze.SDM Pasat-Ptr.002-Pok.00207B (pokój)', 1388, NULL, 1, 1162)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000208', N'Szcze.SDM Pasat-Ptr.002-Pok.000208 (korytarz)', 1389, NULL, 1, 1163)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00208A', N'Szcze.SDM Pasat-Ptr.002-Pok.00208A (pokój)', 1390, NULL, 1, 1164)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00208B', N'Szcze.SDM Pasat-Ptr.002-Pok.00208B (pokój)', 1391, NULL, 1, 1165)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000209', N'Szcze.SDM Pasat-Ptr.002-Pok.000209 (korytarz)', 1392, NULL, 1, 1166)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00209A', N'Szcze.SDM Pasat-Ptr.002-Pok.00209A (pokój)', 1393, NULL, 1, 1167)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00209B', N'Szcze.SDM Pasat-Ptr.002-Pok.00209B (pokój)', 1394, NULL, 1, 1168)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000210', N'Szcze.SDM Pasat-Ptr.002-Pok.000210 (korytarz)', 1395, NULL, 1, 1169)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00210A', N'Szcze.SDM Pasat-Ptr.002-Pok.00210A (pokój)', 1396, NULL, 1, 1170)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00210B', N'Szcze.SDM Pasat-Ptr.002-Pok.00210B (pokój)', 1397, NULL, 1, 1171)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000211', N'Szcze.SDM Pasat-Ptr.002-Pok.000211 (korytarz)', 1398, NULL, 1, 1172)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00211A', N'Szcze.SDM Pasat-Ptr.002-Pok.00211A (pokój)', 1399, NULL, 1, 1173)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.00211B', N'Szcze.SDM Pasat-Ptr.002-Pok.00211B (pokój)', 1400, NULL, 1, 1174)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000212', N'Szcze.SDM Pasat-Ptr.002-Pok.000212 (po.gosp.)', 1401, NULL, 1, 1175)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000214', N'Szcze.SDM Pasat-Ptr.002-Pok.000214 (kuch.)', 1402, NULL, 1, 1176)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.002.000216', N'Szcze.SDM Pasat-Ptr.002-Pok.000216 (po.gosp.)', 1403, NULL, 1, 1177)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.000300', N'Szcze.SDM Pasat-Ptr.003-Pok.000300 (sklep)', 1404, NULL, 1, 1178)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00301A', N'Szcze.SDM Pasat-Ptr.003-Pok.00301A (pokój)', 1405, NULL, 1, 1179)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00301B', N'Szcze.SDM Pasat-Ptr.003-Pok.00301B (pokój)', 1406, NULL, 1, 1180)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00302A', N'Szcze.SDM Pasat-Ptr.003-Pok.00302A (pokój)', 1407, NULL, 1, 1181)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00302B', N'Szcze.SDM Pasat-Ptr.003-Pok.00302B (pokój)', 1408, NULL, 1, 1182)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00303A', N'Szcze.SDM Pasat-Ptr.003-Pok.00303A (pokój)', 1409, NULL, 1, 1183)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00303B', N'Szcze.SDM Pasat-Ptr.003-Pok.00303B (pokój)', 1410, NULL, 1, 1184)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00304A', N'Szcze.SDM Pasat-Ptr.003-Pok.00304A (pokój)', 1411, NULL, 1, 1185)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00304B', N'Szcze.SDM Pasat-Ptr.003-Pok.00304B (pokój)', 1412, NULL, 1, 1186)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00305A', N'Szcze.SDM Pasat-Ptr.003-Pok.00305A (pokój)', 1413, NULL, 1, 1187)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00305B', N'Szcze.SDM Pasat-Ptr.003-Pok.00305B (pokój)', 1414, NULL, 1, 1188)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00306A', N'Szcze.SDM Pasat-Ptr.003-Pok.00306A (pokój)', 1415, NULL, 1, 1189)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00306B', N'Szcze.SDM Pasat-Ptr.003-Pok.00306B (pokój)', 1416, NULL, 1, 1190)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00307A', N'Szcze.SDM Pasat-Ptr.003-Pok.00307A (pokój)', 1417, NULL, 1, 1191)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00307B', N'Szcze.SDM Pasat-Ptr.003-Pok.00307B (pokój)', 1418, NULL, 1, 1192)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00308A', N'Szcze.SDM Pasat-Ptr.003-Pok.00308A (pokój)', 1419, NULL, 1, 1193)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00308B', N'Szcze.SDM Pasat-Ptr.003-Pok.00308B (pokój)', 1420, NULL, 1, 1194)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00309A', N'Szcze.SDM Pasat-Ptr.003-Pok.00309A (pokój)', 1421, NULL, 1, 1195)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00309B', N'Szcze.SDM Pasat-Ptr.003-Pok.00309B (pokój)', 1422, NULL, 1, 1196)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00310A', N'Szcze.SDM Pasat-Ptr.003-Pok.00310A (pokój)', 1423, NULL, 1, 1197)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00310B', N'Szcze.SDM Pasat-Ptr.003-Pok.00310B (pokój)', 1424, NULL, 1, 1198)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00311A', N'Szcze.SDM Pasat-Ptr.003-Pok.00311A (pokój)', 1425, NULL, 1, 1199)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.00311B', N'Szcze.SDM Pasat-Ptr.003-Pok.00311B (pokój)', 1426, NULL, 1, 1200)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.000312', N'Szcze.SDM Pasat-Ptr.003-Pok.000312 (po.gosp.)', 1427, NULL, 1, 1201)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.000313', N'Szcze.SDM Pasat-Ptr.003-Pok.000313 (po.gosp.)', 1428, NULL, 1, 1202)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.000314', N'Szcze.SDM Pasat-Ptr.003-Pok.000314 (kuch.)', 1429, NULL, 1, 1203)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.003.000316', N'Szcze.SDM Pasat-Ptr.003-Pok.000316 (po.gosp.)', 1430, NULL, 1, 1204)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.000400', N'Szcze.SDM Pasat-Ptr.004-Pok.000400 (sal.stud)', 1431, NULL, 1, 1205)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00401A', N'Szcze.SDM Pasat-Ptr.004-Pok.00401A (pokój)', 1432, NULL, 1, 1206)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00401B', N'Szcze.SDM Pasat-Ptr.004-Pok.00401B (pokój)', 1433, NULL, 1, 1207)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00402A', N'Szcze.SDM Pasat-Ptr.004-Pok.00402A (pokój)', 1434, NULL, 1, 1208)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00402B', N'Szcze.SDM Pasat-Ptr.004-Pok.00402B (pokój)', 1435, NULL, 1, 1209)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00403A', N'Szcze.SDM Pasat-Ptr.004-Pok.00403A (pokój)', 1436, NULL, 1, 1210)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00403B', N'Szcze.SDM Pasat-Ptr.004-Pok.00403B (pokój)', 1437, NULL, 1, 1211)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00404A', N'Szcze.SDM Pasat-Ptr.004-Pok.00404A (pokój)', 1438, NULL, 1, 1212)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00404B', N'Szcze.SDM Pasat-Ptr.004-Pok.00404B (pokój)', 1439, NULL, 1, 1213)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00405A', N'Szcze.SDM Pasat-Ptr.004-Pok.00405A (pokój)', 1440, NULL, 1, 1214)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00405B', N'Szcze.SDM Pasat-Ptr.004-Pok.00405B (pokój)', 1441, NULL, 1, 1215)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00406A', N'Szcze.SDM Pasat-Ptr.004-Pok.00406A (pokój)', 1442, NULL, 1, 1216)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00406B', N'Szcze.SDM Pasat-Ptr.004-Pok.00406B (pokój)', 1443, NULL, 1, 1217)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00407A', N'Szcze.SDM Pasat-Ptr.004-Pok.00407A (pokój)', 1444, NULL, 1, 1218)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00407B', N'Szcze.SDM Pasat-Ptr.004-Pok.00407B (pokój)', 1445, NULL, 1, 1219)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00408A', N'Szcze.SDM Pasat-Ptr.004-Pok.00408A (pokój)', 1446, NULL, 1, 1220)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00408B', N'Szcze.SDM Pasat-Ptr.004-Pok.00408B (pokój)', 1447, NULL, 1, 1221)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00409A', N'Szcze.SDM Pasat-Ptr.004-Pok.00409A (pokój)', 1448, NULL, 1, 1222)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00409B', N'Szcze.SDM Pasat-Ptr.004-Pok.00409B (pokój)', 1449, NULL, 1, 1223)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00410A', N'Szcze.SDM Pasat-Ptr.004-Pok.00410A (pokój)', 1450, NULL, 1, 1224)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00410B', N'Szcze.SDM Pasat-Ptr.004-Pok.00410B (pokój)', 1451, NULL, 1, 1225)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00411A', N'Szcze.SDM Pasat-Ptr.004-Pok.00411A (pokój)', 1452, NULL, 1, 1226)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.00411B', N'Szcze.SDM Pasat-Ptr.004-Pok.00411B (pokój)', 1453, NULL, 1, 1227)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.000412', N'Szcze.SDM Pasat-Ptr.004-Pok.000412 (po.gosp.)', 1454, NULL, 1, 1228)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.000413', N'Szcze.SDM Pasat-Ptr.004-Pok.000413 (po.gosp.)', 1455, NULL, 1, 1229)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.000414', N'Szcze.SDM Pasat-Ptr.004-Pok.000414 (kuch.)', 1456, NULL, 1, 1230)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.004.000416', N'Szcze.SDM Pasat-Ptr.004-Pok.000416 (po.gosp.)', 1457, NULL, 1, 1231)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.000500', N'Szcze.SDM Pasat-Ptr.005-Pok.000500 (po.gosp.)', 1458, NULL, 1, 1232)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00501A', N'Szcze.SDM Pasat-Ptr.005-Pok.00501A (pokój)', 1459, NULL, 1, 1233)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00501B', N'Szcze.SDM Pasat-Ptr.005-Pok.00501B (pokój)', 1460, NULL, 1, 1234)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00502A', N'Szcze.SDM Pasat-Ptr.005-Pok.00502A (pokój)', 1461, NULL, 1, 1235)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00502B', N'Szcze.SDM Pasat-Ptr.005-Pok.00502B (pokój)', 1462, NULL, 1, 1236)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00503A', N'Szcze.SDM Pasat-Ptr.005-Pok.00503A (pokój)', 1463, NULL, 1, 1237)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00503B', N'Szcze.SDM Pasat-Ptr.005-Pok.00503B (pokój)', 1464, NULL, 1, 1238)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00504A', N'Szcze.SDM Pasat-Ptr.005-Pok.00504A (pokój)', 1465, NULL, 1, 1239)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00504B', N'Szcze.SDM Pasat-Ptr.005-Pok.00504B (pokój)', 1466, NULL, 1, 1240)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00505A', N'Szcze.SDM Pasat-Ptr.005-Pok.00505A (pokój)', 1467, NULL, 1, 1241)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00505B', N'Szcze.SDM Pasat-Ptr.005-Pok.00505B (pokój)', 1468, NULL, 1, 1242)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00506A', N'Szcze.SDM Pasat-Ptr.005-Pok.00506A (pokój)', 1469, NULL, 1, 1243)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00506B', N'Szcze.SDM Pasat-Ptr.005-Pok.00506B (pokój)', 1470, NULL, 1, 1244)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00507A', N'Szcze.SDM Pasat-Ptr.005-Pok.00507A (pokój)', 1471, NULL, 1, 1245)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00507B', N'Szcze.SDM Pasat-Ptr.005-Pok.00507B (pokój)', 1472, NULL, 1, 1246)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00508A', N'Szcze.SDM Pasat-Ptr.005-Pok.00508A (pokój)', 1473, NULL, 1, 1247)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00508B', N'Szcze.SDM Pasat-Ptr.005-Pok.00508B (pokój)', 1474, NULL, 1, 1248)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00509A', N'Szcze.SDM Pasat-Ptr.005-Pok.00509A (pokój)', 1475, NULL, 1, 1249)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00509B', N'Szcze.SDM Pasat-Ptr.005-Pok.00509B (pokój)', 1476, NULL, 1, 1250)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00510A', N'Szcze.SDM Pasat-Ptr.005-Pok.00510A (pokój)', 1477, NULL, 1, 1251)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00510B', N'Szcze.SDM Pasat-Ptr.005-Pok.00510B (pokój)', 1478, NULL, 1, 1252)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00511A', N'Szcze.SDM Pasat-Ptr.005-Pok.00511A (pokój)', 1479, NULL, 1, 1253)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.00511B', N'Szcze.SDM Pasat-Ptr.005-Pok.00511B (pokój)', 1480, NULL, 1, 1254)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.000512', N'Szcze.SDM Pasat-Ptr.005-Pok.000512 (po.gosp.)', 1481, NULL, 1, 1255)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.000513', N'Szcze.SDM Pasat-Ptr.005-Pok.000513 (po.gosp.)', 1482, NULL, 1, 1256)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.000514', N'Szcze.SDM Pasat-Ptr.005-Pok.000514 (kuch.)', 1483, NULL, 1, 1257)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.005.000516', N'Szcze.SDM Pasat-Ptr.005-Pok.000516 (po.gosp.)', 1484, NULL, 1, 1258)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.000600', N'Szcze.SDM Pasat-Ptr.006-Pok.000600 (po.gosp.)', 1485, NULL, 1, 1259)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00601A', N'Szcze.SDM Pasat-Ptr.006-Pok.00601A (pokój)', 1486, NULL, 1, 1260)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00601B', N'Szcze.SDM Pasat-Ptr.006-Pok.00601B (pokój)', 1487, NULL, 1, 1261)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00602A', N'Szcze.SDM Pasat-Ptr.006-Pok.00602A (pokój)', 1488, NULL, 1, 1262)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00602B', N'Szcze.SDM Pasat-Ptr.006-Pok.00602B (pokój)', 1489, NULL, 1, 1263)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00603A', N'Szcze.SDM Pasat-Ptr.006-Pok.00603A (pokój)', 1490, NULL, 1, 1264)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00603B', N'Szcze.SDM Pasat-Ptr.006-Pok.00603B (pokój)', 1491, NULL, 1, 1265)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00604A', N'Szcze.SDM Pasat-Ptr.006-Pok.00604A (pokój)', 1492, NULL, 1, 1266)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00604B', N'Szcze.SDM Pasat-Ptr.006-Pok.00604B (pokój)', 1493, NULL, 1, 1267)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00605A', N'Szcze.SDM Pasat-Ptr.006-Pok.00605A (pokój)', 1494, NULL, 1, 1268)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00605B', N'Szcze.SDM Pasat-Ptr.006-Pok.00605B (pokój)', 1495, NULL, 1, 1269)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00606A', N'Szcze.SDM Pasat-Ptr.006-Pok.00606A (pokój)', 1496, NULL, 1, 1270)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00606B', N'Szcze.SDM Pasat-Ptr.006-Pok.00606B (pokój)', 1497, NULL, 1, 1271)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00607A', N'Szcze.SDM Pasat-Ptr.006-Pok.00607A (pokój)', 1498, NULL, 1, 1272)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00607B', N'Szcze.SDM Pasat-Ptr.006-Pok.00607B (pokój)', 1499, NULL, 1, 1273)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00608A', N'Szcze.SDM Pasat-Ptr.006-Pok.00608A (pokój)', 1500, NULL, 1, 1274)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00608B', N'Szcze.SDM Pasat-Ptr.006-Pok.00608B (pokój)', 1501, NULL, 1, 1275)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00609A', N'Szcze.SDM Pasat-Ptr.006-Pok.00609A (pokój)', 1502, NULL, 1, 1276)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00609B', N'Szcze.SDM Pasat-Ptr.006-Pok.00609B (pokój)', 1503, NULL, 1, 1277)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00610A', N'Szcze.SDM Pasat-Ptr.006-Pok.00610A (pokój)', 1504, NULL, 1, 1278)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00610B', N'Szcze.SDM Pasat-Ptr.006-Pok.00610B (pokój)', 1505, NULL, 1, 1279)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00611A', N'Szcze.SDM Pasat-Ptr.006-Pok.00611A (pokój)', 1506, NULL, 1, 1280)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.00611B', N'Szcze.SDM Pasat-Ptr.006-Pok.00611B (pokój)', 1507, NULL, 1, 1281)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.000612', N'Szcze.SDM Pasat-Ptr.006-Pok.000612 (po.gosp.)', 1508, NULL, 1, 1282)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.000613', N'Szcze.SDM Pasat-Ptr.006-Pok.000613 (po.gosp.)', 1509, NULL, 1, 1283)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.000614', N'Szcze.SDM Pasat-Ptr.006-Pok.000614 (kuch.)', 1510, NULL, 1, 1284)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.006.000616', N'Szcze.SDM Pasat-Ptr.006-Pok.000616 (po.gosp.)', 1511, NULL, 1, 1285)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.000700', N'Szcze.SDM Pasat-Ptr.007-Pok.000700 (po.gosp.)', 1512, NULL, 1, 1286)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00701A', N'Szcze.SDM Pasat-Ptr.007-Pok.00701A (pokój)', 1513, NULL, 1, 1287)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00701B', N'Szcze.SDM Pasat-Ptr.007-Pok.00701B (pokój)', 1514, NULL, 1, 1288)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00702A', N'Szcze.SDM Pasat-Ptr.007-Pok.00702A (pokój)', 1515, NULL, 1, 1289)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00702B', N'Szcze.SDM Pasat-Ptr.007-Pok.00702B (pokój)', 1516, NULL, 1, 1290)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00703A', N'Szcze.SDM Pasat-Ptr.007-Pok.00703A (pokój)', 1517, NULL, 1, 1291)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00703B', N'Szcze.SDM Pasat-Ptr.007-Pok.00703B (pokój)', 1518, NULL, 1, 1292)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00704A', N'Szcze.SDM Pasat-Ptr.007-Pok.00704A (pokój)', 1519, NULL, 1, 1293)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00704B', N'Szcze.SDM Pasat-Ptr.007-Pok.00704B (pokój)', 1520, NULL, 1, 1294)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00705A', N'Szcze.SDM Pasat-Ptr.007-Pok.00705A (pokój)', 1521, NULL, 1, 1295)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00505B', N'Szcze.SDM Pasat-Ptr.007-Pok.00505B (pokój)', 1522, NULL, 1, 1296)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00706A', N'Szcze.SDM Pasat-Ptr.007-Pok.00706A (pokój)', 1523, NULL, 1, 1297)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00706B', N'Szcze.SDM Pasat-Ptr.007-Pok.00706B (pokój)', 1524, NULL, 1, 1298)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00707A', N'Szcze.SDM Pasat-Ptr.007-Pok.00707A (pokój)', 1525, NULL, 1, 1299)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00707B', N'Szcze.SDM Pasat-Ptr.007-Pok.00707B (pokój)', 1526, NULL, 1, 1300)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00708A', N'Szcze.SDM Pasat-Ptr.007-Pok.00708A (pokój)', 1527, NULL, 1, 1301)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00708B', N'Szcze.SDM Pasat-Ptr.007-Pok.00708B (pokój)', 1528, NULL, 1, 1302)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00709A', N'Szcze.SDM Pasat-Ptr.007-Pok.00709A (pokój)', 1529, NULL, 1, 1303)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00709B', N'Szcze.SDM Pasat-Ptr.007-Pok.00709B (pokój)', 1530, NULL, 1, 1304)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00710A', N'Szcze.SDM Pasat-Ptr.007-Pok.00710A (pokój)', 1531, NULL, 1, 1305)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00710B', N'Szcze.SDM Pasat-Ptr.007-Pok.00710B (pokój)', 1532, NULL, 1, 1306)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00711A', N'Szcze.SDM Pasat-Ptr.007-Pok.00711A (pokój)', 1533, NULL, 1, 1307)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.00711B', N'Szcze.SDM Pasat-Ptr.007-Pok.00711B (pokój)', 1534, NULL, 1, 1308)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.000712', N'Szcze.SDM Pasat-Ptr.007-Pok.000712 (po.gosp.)', 1535, NULL, 1, 1309)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.000713', N'Szcze.SDM Pasat-Ptr.007-Pok.000713 (po.gosp.)', 1536, NULL, 1, 1310)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.000714', N'Szcze.SDM Pasat-Ptr.007-Pok.000714 (kuch.)', 1537, NULL, 1, 1311)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.007.000716', N'Szcze.SDM Pasat-Ptr.007-Pok.000716 (po.gosp.)', 1538, NULL, 1, 1312)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.000800', N'Szcze.SDM Pasat-Ptr.008-Pok.000800 (biuro)', 1539, NULL, 1, 1313)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00801A', N'Szcze.SDM Pasat-Ptr.008-Pok.00801A (pokój)', 1540, NULL, 1, 1314)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00801B', N'Szcze.SDM Pasat-Ptr.008-Pok.00801B (pokój)', 1541, NULL, 1, 1315)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00802A', N'Szcze.SDM Pasat-Ptr.008-Pok.00802A (pokój)', 1542, NULL, 1, 1316)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00802B', N'Szcze.SDM Pasat-Ptr.008-Pok.00802B (pokój)', 1543, NULL, 1, 1317)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00803A', N'Szcze.SDM Pasat-Ptr.008-Pok.00803A (pokój)', 1544, NULL, 1, 1318)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00803B', N'Szcze.SDM Pasat-Ptr.008-Pok.00803B (pokój)', 1545, NULL, 1, 1319)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00804A', N'Szcze.SDM Pasat-Ptr.008-Pok.00804A (pokój)', 1546, NULL, 1, 1320)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00804B', N'Szcze.SDM Pasat-Ptr.008-Pok.00804B (pokój)', 1547, NULL, 1, 1321)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00805A', N'Szcze.SDM Pasat-Ptr.008-Pok.00805A (pokój)', 1548, NULL, 1, 1322)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00805B', N'Szcze.SDM Pasat-Ptr.008-Pok.00805B (pokój)', 1549, NULL, 1, 1323)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00806A', N'Szcze.SDM Pasat-Ptr.008-Pok.00806A (pokój)', 1550, NULL, 1, 1324)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00806B', N'Szcze.SDM Pasat-Ptr.008-Pok.00806B (pokój)', 1551, NULL, 1, 1325)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00807A', N'Szcze.SDM Pasat-Ptr.008-Pok.00807A (pokój)', 1552, NULL, 1, 1326)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00807B', N'Szcze.SDM Pasat-Ptr.008-Pok.00807B (pokój)', 1553, NULL, 1, 1327)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00808A', N'Szcze.SDM Pasat-Ptr.008-Pok.00808A (pokój)', 1554, NULL, 1, 1328)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00808B', N'Szcze.SDM Pasat-Ptr.008-Pok.00808B (pokój)', 1555, NULL, 1, 1329)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00809A', N'Szcze.SDM Pasat-Ptr.008-Pok.00809A (pokój)', 1556, NULL, 1, 1330)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00809B', N'Szcze.SDM Pasat-Ptr.008-Pok.00809B (pokój)', 1557, NULL, 1, 1331)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00810A', N'Szcze.SDM Pasat-Ptr.008-Pok.00810A (pokój)', 1558, NULL, 1, 1332)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00810B', N'Szcze.SDM Pasat-Ptr.008-Pok.00810B (pokój)', 1559, NULL, 1, 1333)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00811A', N'Szcze.SDM Pasat-Ptr.008-Pok.00811A (pokój)', 1560, NULL, 1, 1334)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.00811B', N'Szcze.SDM Pasat-Ptr.008-Pok.00811B (pokój)', 1561, NULL, 1, 1335)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.000812', N'Szcze.SDM Pasat-Ptr.008-Pok.000812 (po.gosp.)', 1562, NULL, 1, 1336)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.000813', N'Szcze.SDM Pasat-Ptr.008-Pok.000813 (po.gosp.)', 1563, NULL, 1, 1337)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.000814', N'Szcze.SDM Pasat-Ptr.008-Pok.000814 (kuch.)', 1564, NULL, 1, 1338)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.008.000816', N'Szcze.SDM Pasat-Ptr.008-Pok.000816 (po.gosp.)', 1565, NULL, 1, 1339)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.000900', N'Szcze.SDM Pasat-Ptr.009-Pok.000900 (sal.stud)', 1566, NULL, 1, 1340)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00901A', N'Szcze.SDM Pasat-Ptr.009-Pok.00901A (pokój)', 1567, NULL, 1, 1341)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00901B', N'Szcze.SDM Pasat-Ptr.009-Pok.00901B (pokój)', 1568, NULL, 1, 1342)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00902A', N'Szcze.SDM Pasat-Ptr.009-Pok.00902A (pokój)', 1569, NULL, 1, 1343)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00902B', N'Szcze.SDM Pasat-Ptr.009-Pok.00902B (pokój)', 1570, NULL, 1, 1344)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00903A', N'Szcze.SDM Pasat-Ptr.009-Pok.00903A (pokój)', 1571, NULL, 1, 1345)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00903B', N'Szcze.SDM Pasat-Ptr.009-Pok.00903B (pokój)', 1572, NULL, 1, 1346)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00904A', N'Szcze.SDM Pasat-Ptr.009-Pok.00904A (pokój)', 1573, NULL, 1, 1347)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00904B', N'Szcze.SDM Pasat-Ptr.009-Pok.00904B (pokój)', 1574, NULL, 1, 1348)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00905A', N'Szcze.SDM Pasat-Ptr.009-Pok.00905A (pokój)', 1575, NULL, 1, 1349)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00905B', N'Szcze.SDM Pasat-Ptr.009-Pok.00905B (pokój)', 1576, NULL, 1, 1350)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00906A', N'Szcze.SDM Pasat-Ptr.009-Pok.00906A (pokój)', 1577, NULL, 1, 1351)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00906B', N'Szcze.SDM Pasat-Ptr.009-Pok.00906B (pokój)', 1578, NULL, 1, 1352)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00907A', N'Szcze.SDM Pasat-Ptr.009-Pok.00907A (pokój)', 1579, NULL, 1, 1353)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00907B', N'Szcze.SDM Pasat-Ptr.009-Pok.00907B (pokój)', 1580, NULL, 1, 1354)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00908A', N'Szcze.SDM Pasat-Ptr.009-Pok.00908A (pokój)', 1581, NULL, 1, 1355)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00908B', N'Szcze.SDM Pasat-Ptr.009-Pok.00908B (pokój)', 1582, NULL, 1, 1356)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00909A', N'Szcze.SDM Pasat-Ptr.009-Pok.00909A (pokój)', 1583, NULL, 1, 1357)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00909B', N'Szcze.SDM Pasat-Ptr.009-Pok.00909B (pokój)', 1584, NULL, 1, 1358)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00910A', N'Szcze.SDM Pasat-Ptr.009-Pok.00910A (pokój)', 1585, NULL, 1, 1359)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00910B', N'Szcze.SDM Pasat-Ptr.009-Pok.00910B (pokój)', 1586, NULL, 1, 1360)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00911A', N'Szcze.SDM Pasat-Ptr.009-Pok.00911A (pokój)', 1587, NULL, 1, 1361)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.00911B', N'Szcze.SDM Pasat-Ptr.009-Pok.00911B (pokój)', 1588, NULL, 1, 1362)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.000912', N'Szcze.SDM Pasat-Ptr.009-Pok.000912 (po.gosp.)', 1589, NULL, 1, 1363)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.000913', N'Szcze.SDM Pasat-Ptr.009-Pok.000913 (po.gosp.)', 1590, NULL, 1, 1364)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.000914', N'Szcze.SDM Pasat-Ptr.009-Pok.000914 (kuch.)', 1591, NULL, 1, 1365)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.009.000916', N'Szcze.SDM Pasat-Ptr.009-Pok.000916 (po.gosp.)', 1592, NULL, 1, 1366)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001000', N'Szcze.SDM Pasat-Ptr.010-Pok.001000 (sal.stud)', 1593, NULL, 1, 1367)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01001A', N'Szcze.SDM Pasat-Ptr.010-Pok.01001A (pokój)', 1594, NULL, 1, 1368)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01001B', N'Szcze.SDM Pasat-Ptr.010-Pok.01001B (pokój)', 1595, NULL, 1, 1369)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01002A', N'Szcze.SDM Pasat-Ptr.010-Pok.01002A (pokój)', 1596, NULL, 1, 1370)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01002B', N'Szcze.SDM Pasat-Ptr.010-Pok.01002B (pokój)', 1597, NULL, 1, 1371)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01003A', N'Szcze.SDM Pasat-Ptr.010-Pok.01003A (pokój)', 1598, NULL, 1, 1372)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01003B', N'Szcze.SDM Pasat-Ptr.010-Pok.01003B (pokój)', 1599, NULL, 1, 1373)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01004A', N'Szcze.SDM Pasat-Ptr.010-Pok.01004A (pokój)', 1600, NULL, 1, 1374)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01004B', N'Szcze.SDM Pasat-Ptr.010-Pok.01004B (pokój)', 1601, NULL, 1, 1375)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01005A', N'Szcze.SDM Pasat-Ptr.010-Pok.01005A (pokój)', 1602, NULL, 1, 1376)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01005B', N'Szcze.SDM Pasat-Ptr.010-Pok.01005B (pokój)', 1603, NULL, 1, 1377)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01006A', N'Szcze.SDM Pasat-Ptr.010-Pok.01006A (pokój)', 1604, NULL, 1, 1378)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01006B', N'Szcze.SDM Pasat-Ptr.010-Pok.01006B (pokój)', 1605, NULL, 1, 1379)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01007A', N'Szcze.SDM Pasat-Ptr.010-Pok.01007A (pokój)', 1606, NULL, 1, 1380)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01007B', N'Szcze.SDM Pasat-Ptr.010-Pok.01007B (pokój)', 1607, NULL, 1, 1381)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01008A', N'Szcze.SDM Pasat-Ptr.010-Pok.01008A (pokój)', 1608, NULL, 1, 1382)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01008B', N'Szcze.SDM Pasat-Ptr.010-Pok.01008B (pokój)', 1609, NULL, 1, 1383)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01009A', N'Szcze.SDM Pasat-Ptr.010-Pok.01009A (pokój)', 1610, NULL, 1, 1384)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01009B', N'Szcze.SDM Pasat-Ptr.010-Pok.01009B (pokój)', 1611, NULL, 1, 1385)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01010A', N'Szcze.SDM Pasat-Ptr.010-Pok.01010A (pokój)', 1612, NULL, 1, 1386)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01010B', N'Szcze.SDM Pasat-Ptr.010-Pok.01010B (pokój)', 1613, NULL, 1, 1387)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01011A', N'Szcze.SDM Pasat-Ptr.010-Pok.01011A (pokój)', 1614, NULL, 1, 1388)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.01011B', N'Szcze.SDM Pasat-Ptr.010-Pok.01011B (pokój)', 1615, NULL, 1, 1389)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001012', N'Szcze.SDM Pasat-Ptr.010-Pok.001012 (serwer)', 1616, NULL, 1, 1390)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001013', N'Szcze.SDM Pasat-Ptr.010-Pok.001013 (po.gosp.)', 1617, NULL, 1, 1391)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001014', N'Szcze.SDM Pasat-Ptr.010-Pok.001014 (kuch.)', 1618, NULL, 1, 1392)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001015', N'Szcze.SDM Pasat-Ptr.010-Pok.001015 (zsyp)', 1619, NULL, 1, 1393)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.010.001016', N'Szcze.SDM Pasat-Ptr.010-Pok.001016 (po.gosp.)', 1620, NULL, 1, 1394)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.011.001017', N'Szcze.SDM Pasat-Ptr.011-Pok.001017 (po.gosp.)', 1621, NULL, 1, 1395)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.011.001018', N'Szcze.SDM Pasat-Ptr.011-Pok.001018 (pom.tech)', 1622, NULL, 1, 1396)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.05.011.01018A', N'Szcze.SDM Pasat-Ptr.011-Pok.01018A (pom.tech)', 1623, NULL, 1, 1397)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000008', N'Szcze.OD Poboż.-Ptr.-01-Pok.000008 (sal.dyd.)', 1624, NULL, 1, 1398)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000004', N'Szcze.OD Poboż.-Ptr.-01-Pok.000004 (lab.)', 1625, NULL, 1, 1399)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000006', N'Szcze.OD Poboż.-Ptr.-01-Pok.000006 (lab.)', 1626, NULL, 1, 1400)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000007', N'Szcze.OD Poboż.-Ptr.-01-Pok.000007 (lab.)', 1627, NULL, 1, 1401)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000008', N'Szcze.OD Poboż.-Ptr.-01-Pok.000008 (pom.tech)', 1628, NULL, 1, 1402)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.00008A', N'Szcze.OD Poboż.-Ptr.-01-Pok.00008A (pom.tech)', 1629, NULL, 1, 1403)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000010', N'Szcze.OD Poboż.-Ptr.-01-Pok.000010 (pom.tech)', 1630, NULL, 1, 1404)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000013', N'Szcze.OD Poboż.-Ptr.-01-Pok.000013 (po.gosp.)', 1631, NULL, 1, 1405)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000015', N'Szcze.OD Poboż.-Ptr.-01-Pok.000015 (lab.)', 1632, NULL, 1, 1406)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000016', N'Szcze.OD Poboż.-Ptr.-01-Pok.000016 (mag.)', 1633, NULL, 1, 1407)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000017', N'Szcze.OD Poboż.-Ptr.-01-Pok.000017 (lab.)', 1634, NULL, 1, 1408)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.00018A', N'Szcze.OD Poboż.-Ptr.-01-Pok.00018A (po.gosp.)', 1635, NULL, 1, 1409)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000019', N'Szcze.OD Poboż.-Ptr.-01-Pok.000019 (pom.tech)', 1636, NULL, 1, 1410)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000020', N'Szcze.OD Poboż.-Ptr.-01-Pok.000020 (po.gosp.)', 1637, NULL, 1, 1411)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000021', N'Szcze.OD Poboż.-Ptr.-01-Pok.000021 (szatnia)', 1638, NULL, 1, 1412)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000022', N'Szcze.OD Poboż.-Ptr.-01-Pok.000022 (szatnia)', 1639, NULL, 1, 1413)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000023', N'Szcze.OD Poboż.-Ptr.-01-Pok.000023 (po.gosp.)', 1640, NULL, 1, 1414)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000024', N'Szcze.OD Poboż.-Ptr.-01-Pok.000024 (po.gosp.)', 1641, NULL, 1, 1415)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000025', N'Szcze.OD Poboż.-Ptr.-01-Pok.000025 (po.gosp.)', 1642, NULL, 1, 1416)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000026', N'Szcze.OD Poboż.-Ptr.-01-Pok.000026 (po.gosp.)', 1643, NULL, 1, 1417)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.-01.000027', N'Szcze.OD Poboż.-Ptr.-01-Pok.000027 (toaleta)', 1644, NULL, 1, 1418)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000001', N'Szcze.OD Poboż.-Ptr.000-Pok.000001 (portie.)', 1645, NULL, 1, 1419)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000002', N'Szcze.OD Poboż.-Ptr.000-Pok.000002 (szatnia)', 1646, NULL, 1, 1420)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000003', N'Szcze.OD Poboż.-Ptr.000-Pok.000003 (sklep)', 1647, NULL, 1, 1421)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000008', N'Szcze.OD Poboż.-Ptr.000-Pok.000008 (sal.wykł)', 1648, NULL, 1, 1422)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000009', N'Szcze.OD Poboż.-Ptr.000-Pok.000009 (biuro)', 1649, NULL, 1, 1423)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000010', N'Szcze.OD Poboż.-Ptr.000-Pok.000010 (biuro)', 1650, NULL, 1, 1424)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000011', N'Szcze.OD Poboż.-Ptr.000-Pok.000011 (biuro)', 1651, NULL, 1, 1425)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000012', N'Szcze.OD Poboż.-Ptr.000-Pok.000012 (biuro)', 1652, NULL, 1, 1426)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000014', N'Szcze.OD Poboż.-Ptr.000-Pok.000014 (sal.wykł)', 1653, NULL, 1, 1427)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000015', N'Szcze.OD Poboż.-Ptr.000-Pok.000015 (pom.soc.)', 1654, NULL, 1, 1428)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.00015B', N'Szcze.OD Poboż.-Ptr.000-Pok.00015B (biuro)', 1655, NULL, 1, 1429)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.00015C', N'Szcze.OD Poboż.-Ptr.000-Pok.00015C (biuro)', 1656, NULL, 1, 1430)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.00015A', N'Szcze.OD Poboż.-Ptr.000-Pok.00015A (biuro)', 1657, NULL, 1, 1431)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000016', N'Szcze.OD Poboż.-Ptr.000-Pok.000016 (biuro)', 1658, NULL, 1, 1432)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000017', N'Szcze.OD Poboż.-Ptr.000-Pok.000017 (biuro)', 1659, NULL, 1, 1433)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000018', N'Szcze.OD Poboż.-Ptr.000-Pok.000018 (biuro)', 1660, NULL, 1, 1434)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000019', N'Szcze.OD Poboż.-Ptr.000-Pok.000019 (biuro)', 1661, NULL, 1, 1435)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.00019A', N'Szcze.OD Poboż.-Ptr.000-Pok.00019A (pom.soc.)', 1662, NULL, 1, 1436)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000020', N'Szcze.OD Poboż.-Ptr.000-Pok.000020 (biuro)', 1663, NULL, 1, 1437)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000021', N'Szcze.OD Poboż.-Ptr.000-Pok.000021 (biuro)', 1664, NULL, 1, 1438)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000022', N'Szcze.OD Poboż.-Ptr.000-Pok.000022 (biuro)', 1665, NULL, 1, 1439)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000023', N'Szcze.OD Poboż.-Ptr.000-Pok.000023 (biuro)', 1666, NULL, 1, 1440)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000024', N'Szcze.OD Poboż.-Ptr.000-Pok.000024 (toaleta)', 1667, NULL, 1, 1441)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000025', N'Szcze.OD Poboż.-Ptr.000-Pok.000025 (toaleta)', 1668, NULL, 1, 1442)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000026', N'Szcze.OD Poboż.-Ptr.000-Pok.000026 (biuro)', 1669, NULL, 1, 1443)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000027', N'Szcze.OD Poboż.-Ptr.000-Pok.000027 (po.gosp.)', 1670, NULL, 1, 1444)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000028', N'Szcze.OD Poboż.-Ptr.000-Pok.000028 (toaleta)', 1671, NULL, 1, 1445)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.000.000029', N'Szcze.OD Poboż.-Ptr.000-Pok.000029 (toaleta)', 1672, NULL, 1, 1446)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000103', N'Szcze.OD Poboż.-Ptr.001-Pok.000103 (sal.komp)', 1673, NULL, 1, 1447)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000104', N'Szcze.OD Poboż.-Ptr.001-Pok.000104 (sal.komp)', 1674, NULL, 1, 1448)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000105', N'Szcze.OD Poboż.-Ptr.001-Pok.000105 (sal.wykł)', 1675, NULL, 1, 1449)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000106', N'Szcze.OD Poboż.-Ptr.001-Pok.000106 (sal.wykł)', 1676, NULL, 1, 1450)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000107', N'Szcze.OD Poboż.-Ptr.001-Pok.000107 (sal.wykł)', 1677, NULL, 1, 1451)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000108', N'Szcze.OD Poboż.-Ptr.001-Pok.000108 (biuro)', 1678, NULL, 1, 1452)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000109', N'Szcze.OD Poboż.-Ptr.001-Pok.000109 (biuro)', 1679, NULL, 1, 1453)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000110', N'Szcze.OD Poboż.-Ptr.001-Pok.000110 (biuro)', 1680, NULL, 1, 1454)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.00111A', N'Szcze.OD Poboż.-Ptr.001-Pok.00111A (biuro)', 1681, NULL, 1, 1455)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000111', N'Szcze.OD Poboż.-Ptr.001-Pok.000111 (biuro)', 1682, NULL, 1, 1456)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000112', N'Szcze.OD Poboż.-Ptr.001-Pok.000112 (biuro)', 1683, NULL, 1, 1457)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000113', N'Szcze.OD Poboż.-Ptr.001-Pok.000113 (biuro)', 1684, NULL, 1, 1458)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000114', N'Szcze.OD Poboż.-Ptr.001-Pok.000114 (biuro)', 1685, NULL, 1, 1459)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.00114A', N'Szcze.OD Poboż.-Ptr.001-Pok.00114A (po.gosp.)', 1686, NULL, 1, 1460)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000115', N'Szcze.OD Poboż.-Ptr.001-Pok.000115 (sal.wykł)', 1687, NULL, 1, 1461)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000116', N'Szcze.OD Poboż.-Ptr.001-Pok.000116 (toaleta)', 1688, NULL, 1, 1462)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000117', N'Szcze.OD Poboż.-Ptr.001-Pok.000117 (toaleta)', 1689, NULL, 1, 1463)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000118', N'Szcze.OD Poboż.-Ptr.001-Pok.000118 (toaleta)', 1690, NULL, 1, 1464)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000119', N'Szcze.OD Poboż.-Ptr.001-Pok.000119 (serwer)', 1691, NULL, 1, 1465)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000120', N'Szcze.OD Poboż.-Ptr.001-Pok.000120 (toaleta)', 1692, NULL, 1, 1466)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.001.000121', N'Szcze.OD Poboż.-Ptr.001-Pok.000121 (toaleta)', 1693, NULL, 1, 1467)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000203', N'Szcze.OD Poboż.-Ptr.002-Pok.000203 (sal.wykł)', 1694, NULL, 1, 1468)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000204', N'Szcze.OD Poboż.-Ptr.002-Pok.000204 (lab.)', 1695, NULL, 1, 1469)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.00205A', N'Szcze.OD Poboż.-Ptr.002-Pok.00205A (biuro)', 1696, NULL, 1, 1470)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000205', N'Szcze.OD Poboż.-Ptr.002-Pok.000205 (lab.)', 1697, NULL, 1, 1471)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.00206A', N'Szcze.OD Poboż.-Ptr.002-Pok.00206A (biuro)', 1698, NULL, 1, 1472)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.00206B', N'Szcze.OD Poboż.-Ptr.002-Pok.00206B (biuro)', 1699, NULL, 1, 1473)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000207', N'Szcze.OD Poboż.-Ptr.002-Pok.000207 (biuro)', 1700, NULL, 1, 1474)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000208', N'Szcze.OD Poboż.-Ptr.002-Pok.000208 (biuro)', 1701, NULL, 1, 1475)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000209', N'Szcze.OD Poboż.-Ptr.002-Pok.000209 (biuro)', 1702, NULL, 1, 1476)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000210', N'Szcze.OD Poboż.-Ptr.002-Pok.000210 (biuro)', 1703, NULL, 1, 1477)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000211', N'Szcze.OD Poboż.-Ptr.002-Pok.000211 (po.gosp.)', 1704, NULL, 1, 1478)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000213', N'Szcze.OD Poboż.-Ptr.002-Pok.000213 (sal.dyd.)', 1705, NULL, 1, 1479)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000215', N'Szcze.OD Poboż.-Ptr.002-Pok.000215 (biuro)', 1706, NULL, 1, 1480)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000216', N'Szcze.OD Poboż.-Ptr.002-Pok.000216 (biuro)', 1707, NULL, 1, 1481)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000218', N'Szcze.OD Poboż.-Ptr.002-Pok.000218 (biuro)', 1708, NULL, 1, 1482)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000219', N'Szcze.OD Poboż.-Ptr.002-Pok.000219 (sal.dyd.)', 1709, NULL, 1, 1483)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000221', N'Szcze.OD Poboż.-Ptr.002-Pok.000221 (biuro)', 1710, NULL, 1, 1484)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000222', N'Szcze.OD Poboż.-Ptr.002-Pok.000222 (biuro)', 1711, NULL, 1, 1485)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000223', N'Szcze.OD Poboż.-Ptr.002-Pok.000223 (biuro)', 1712, NULL, 1, 1486)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000224', N'Szcze.OD Poboż.-Ptr.002-Pok.000224 (biuro)', 1713, NULL, 1, 1487)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000225', N'Szcze.OD Poboż.-Ptr.002-Pok.000225 (lab.)', 1714, NULL, 1, 1488)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000227', N'Szcze.OD Poboż.-Ptr.002-Pok.000227 (lab.)', 1715, NULL, 1, 1489)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.00227A', N'Szcze.OD Poboż.-Ptr.002-Pok.00227A (lab.)', 1716, NULL, 1, 1490)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000231', N'Szcze.OD Poboż.-Ptr.002-Pok.000231 (po.gosp.)', 1717, NULL, 1, 1491)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000232', N'Szcze.OD Poboż.-Ptr.002-Pok.000232 (pom.soc.)', 1718, NULL, 1, 1492)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000233', N'Szcze.OD Poboż.-Ptr.002-Pok.000233 (toaleta)', 1719, NULL, 1, 1493)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000236', N'Szcze.OD Poboż.-Ptr.002-Pok.000236 (toaleta)', 1720, NULL, 1, 1494)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000237', N'Szcze.OD Poboż.-Ptr.002-Pok.000237 (toaleta)', 1721, NULL, 1, 1495)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000239', N'Szcze.OD Poboż.-Ptr.002-Pok.000239 (toaleta)', 1722, NULL, 1, 1496)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000303', N'Szcze.OD Poboż.-Ptr.002-Pok.000303 (sal.wykł)', 1723, NULL, 1, 1497)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000304', N'Szcze.OD Poboż.-Ptr.002-Pok.000304 (sal.dyd.)', 1724, NULL, 1, 1498)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.002.000305', N'Szcze.OD Poboż.-Ptr.002-Pok.000305 (sal.dyd.)', 1725, NULL, 1, 1499)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000306', N'Szcze.OD Poboż.-Ptr.003-Pok.000306 (sal.wykł)', 1726, NULL, 1, 1500)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000307', N'Szcze.OD Poboż.-Ptr.003-Pok.000307 (sal.dyd.)', 1727, NULL, 1, 1501)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000308', N'Szcze.OD Poboż.-Ptr.003-Pok.000308 (sal.dyd.)', 1728, NULL, 1, 1502)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000309', N'Szcze.OD Poboż.-Ptr.003-Pok.000309 (sal.dyd.)', 1729, NULL, 1, 1503)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000310', N'Szcze.OD Poboż.-Ptr.003-Pok.000310 (lab.)', 1730, NULL, 1, 1504)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000312', N'Szcze.OD Poboż.-Ptr.003-Pok.000312 (biuro)', 1731, NULL, 1, 1505)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000313', N'Szcze.OD Poboż.-Ptr.003-Pok.000313 (biuro)', 1732, NULL, 1, 1506)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000314', N'Szcze.OD Poboż.-Ptr.003-Pok.000314 (biuro)', 1733, NULL, 1, 1507)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000315', N'Szcze.OD Poboż.-Ptr.003-Pok.000315 (biuro)', 1734, NULL, 1, 1508)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000316', N'Szcze.OD Poboż.-Ptr.003-Pok.000316 (biuro)', 1735, NULL, 1, 1509)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000317', N'Szcze.OD Poboż.-Ptr.003-Pok.000317 (biuro)', 1736, NULL, 1, 1510)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000318', N'Szcze.OD Poboż.-Ptr.003-Pok.000318 (biuro)', 1737, NULL, 1, 1511)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000319', N'Szcze.OD Poboż.-Ptr.003-Pok.000319 (biuro)', 1738, NULL, 1, 1512)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000321', N'Szcze.OD Poboż.-Ptr.003-Pok.000321 (biuro)', 1739, NULL, 1, 1513)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000322', N'Szcze.OD Poboż.-Ptr.003-Pok.000322 (biuro)', 1740, NULL, 1, 1514)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000323', N'Szcze.OD Poboż.-Ptr.003-Pok.000323 (biuro)', 1741, NULL, 1, 1515)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000324', N'Szcze.OD Poboż.-Ptr.003-Pok.000324 (biuro)', 1742, NULL, 1, 1516)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000327', N'Szcze.OD Poboż.-Ptr.003-Pok.000327 (toaleta)', 1743, NULL, 1, 1517)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000329', N'Szcze.OD Poboż.-Ptr.003-Pok.000329 (toaleta)', 1744, NULL, 1, 1518)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000331', N'Szcze.OD Poboż.-Ptr.003-Pok.000331 (toaleta)', 1745, NULL, 1, 1519)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000333', N'Szcze.OD Poboż.-Ptr.003-Pok.000333 (toaleta)', 1746, NULL, 1, 1520)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000336', N'Szcze.OD Poboż.-Ptr.003-Pok.000336 (po.gosp.)', 1747, NULL, 1, 1521)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000338', N'Szcze.OD Poboż.-Ptr.003-Pok.000338 (pom.soc.)', 1748, NULL, 1, 1522)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000403', N'Szcze.OD Poboż.-Ptr.003-Pok.000403 (sal.wykł)', 1749, NULL, 1, 1523)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000404', N'Szcze.OD Poboż.-Ptr.003-Pok.000404 (sal.dyd.)', 1750, NULL, 1, 1524)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000405', N'Szcze.OD Poboż.-Ptr.003-Pok.000405 (sal.dyd.)', 1751, NULL, 1, 1525)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000406', N'Szcze.OD Poboż.-Ptr.003-Pok.000406 (sal.dyd.)', 1752, NULL, 1, 1526)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000407', N'Szcze.OD Poboż.-Ptr.003-Pok.000407 (lab.)', 1753, NULL, 1, 1527)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.003.000409', N'Szcze.OD Poboż.-Ptr.003-Pok.000409 (lab.)', 1754, NULL, 1, 1528)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000410', N'Szcze.OD Poboż.-Ptr.004-Pok.000410 (lab.)', 1755, NULL, 1, 1529)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000412', N'Szcze.OD Poboż.-Ptr.004-Pok.000412 (biuro)', 1756, NULL, 1, 1530)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000413', N'Szcze.OD Poboż.-Ptr.004-Pok.000413 (biuro)', 1757, NULL, 1, 1531)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000414', N'Szcze.OD Poboż.-Ptr.004-Pok.000414 (biuro)', 1758, NULL, 1, 1532)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000415', N'Szcze.OD Poboż.-Ptr.004-Pok.000415 (biuro)', 1759, NULL, 1, 1533)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000416', N'Szcze.OD Poboż.-Ptr.004-Pok.000416 (biuro)', 1760, NULL, 1, 1534)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000417', N'Szcze.OD Poboż.-Ptr.004-Pok.000417 (biuro)', 1761, NULL, 1, 1535)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000418', N'Szcze.OD Poboż.-Ptr.004-Pok.000418 (biuro)', 1762, NULL, 1, 1536)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000419', N'Szcze.OD Poboż.-Ptr.004-Pok.000419 (biuro)', 1763, NULL, 1, 1537)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000421', N'Szcze.OD Poboż.-Ptr.004-Pok.000421 (biuro)', 1764, NULL, 1, 1538)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000422', N'Szcze.OD Poboż.-Ptr.004-Pok.000422 (biuro)', 1765, NULL, 1, 1539)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000423', N'Szcze.OD Poboż.-Ptr.004-Pok.000423 (biuro)', 1766, NULL, 1, 1540)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000424', N'Szcze.OD Poboż.-Ptr.004-Pok.000424 (biuro)', 1767, NULL, 1, 1541)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000427', N'Szcze.OD Poboż.-Ptr.004-Pok.000427 (toaleta)', 1768, NULL, 1, 1542)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000429', N'Szcze.OD Poboż.-Ptr.004-Pok.000429 (toaleta)', 1769, NULL, 1, 1543)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000431', N'Szcze.OD Poboż.-Ptr.004-Pok.000431 (toaleta)', 1770, NULL, 1, 1544)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000434', N'Szcze.OD Poboż.-Ptr.004-Pok.000434 (toaleta)', 1771, NULL, 1, 1545)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000437', N'Szcze.OD Poboż.-Ptr.004-Pok.000437 (po.gosp.)', 1772, NULL, 1, 1546)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.004.000438', N'Szcze.OD Poboż.-Ptr.004-Pok.000438 (po.gosp.)', 1773, NULL, 1, 1547)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000501', N'Szcze.OD Poboż.-Ptr.005-Pok.000501 (biuro)', 1774, NULL, 1, 1548)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000502', N'Szcze.OD Poboż.-Ptr.005-Pok.000502 (biuro)', 1775, NULL, 1, 1549)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000503', N'Szcze.OD Poboż.-Ptr.005-Pok.000503 (biuro)', 1776, NULL, 1, 1550)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000504', N'Szcze.OD Poboż.-Ptr.005-Pok.000504 (biuro)', 1777, NULL, 1, 1551)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000505', N'Szcze.OD Poboż.-Ptr.005-Pok.000505 (biuro)', 1778, NULL, 1, 1552)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000506', N'Szcze.OD Poboż.-Ptr.005-Pok.000506 (biuro)', 1779, NULL, 1, 1553)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000507', N'Szcze.OD Poboż.-Ptr.005-Pok.000507 (po.gosp.)', 1780, NULL, 1, 1554)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000508', N'Szcze.OD Poboż.-Ptr.005-Pok.000508 (serwer)', 1781, NULL, 1, 1555)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000509', N'Szcze.OD Poboż.-Ptr.005-Pok.000509 (biuro)', 1782, NULL, 1, 1556)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000510', N'Szcze.OD Poboż.-Ptr.005-Pok.000510 (biuro)', 1783, NULL, 1, 1557)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000511', N'Szcze.OD Poboż.-Ptr.005-Pok.000511 (biuro)', 1784, NULL, 1, 1558)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000512', N'Szcze.OD Poboż.-Ptr.005-Pok.000512 (biuro)', 1785, NULL, 1, 1559)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.00512A', N'Szcze.OD Poboż.-Ptr.005-Pok.00512A (pom.soc.)', 1786, NULL, 1, 1560)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000513', N'Szcze.OD Poboż.-Ptr.005-Pok.000513 (biuro)', 1787, NULL, 1, 1561)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.00513B', N'Szcze.OD Poboż.-Ptr.005-Pok.00513B (biuro)', 1788, NULL, 1, 1562)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000514', N'Szcze.OD Poboż.-Ptr.005-Pok.000514 (toaleta)', 1789, NULL, 1, 1563)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000515', N'Szcze.OD Poboż.-Ptr.005-Pok.000515 (toaleta)', 1790, NULL, 1, 1564)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000516', N'Szcze.OD Poboż.-Ptr.005-Pok.000516 (pom.soc.)', 1791, NULL, 1, 1565)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000517', N'Szcze.OD Poboż.-Ptr.005-Pok.000517 (po.gosp.)', 1792, NULL, 1, 1566)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000518', N'Szcze.OD Poboż.-Ptr.005-Pok.000518 (toaleta)', 1793, NULL, 1, 1567)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.005.000519', N'Szcze.OD Poboż.-Ptr.005-Pok.000519 (toaleta)', 1794, NULL, 1, 1568)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00601A', N'Szcze.OD Poboż.-Ptr.006-Pok.00601A (po.gosp.)', 1795, NULL, 1, 1569)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00601B', N'Szcze.OD Poboż.-Ptr.006-Pok.00601B (po.gosp.)', 1796, NULL, 1, 1570)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.000602', N'Szcze.OD Poboż.-Ptr.006-Pok.000602 (arch)', 1797, NULL, 1, 1571)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00602A', N'Szcze.OD Poboż.-Ptr.006-Pok.00602A (arch)', 1798, NULL, 1, 1572)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.000603', N'Szcze.OD Poboż.-Ptr.006-Pok.000603 (pom.soc.)', 1799, NULL, 1, 1573)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.000604', N'Szcze.OD Poboż.-Ptr.006-Pok.000604 (toaleta)', 1800, NULL, 1, 1574)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00605A', N'Szcze.OD Poboż.-Ptr.006-Pok.00605A (po.gosp.)', 1801, NULL, 1, 1575)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00605B', N'Szcze.OD Poboż.-Ptr.006-Pok.00605B (po.gosp.)', 1802, NULL, 1, 1576)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.000606', N'Szcze.OD Poboż.-Ptr.006-Pok.000606 (pom.tech)', 1803, NULL, 1, 1577)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00607A', N'Szcze.OD Poboż.-Ptr.006-Pok.00607A (biuro)', 1804, NULL, 1, 1578)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.06.006.00607B', N'Szcze.OD Poboż.-Ptr.006-Pok.00607B (pom.tech)', 1805, NULL, 1, 1579)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000001', N'Szcze.Pływalnia-Ptr.-01-Pok.000001 (warszt)', 1806, NULL, 1, 1580)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000002', N'Szcze.Pływalnia-Ptr.-01-Pok.000002 (pom.tech)', 1807, NULL, 1, 1581)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000003', N'Szcze.Pływalnia-Ptr.-01-Pok.000003 (pom.tech)', 1808, NULL, 1, 1582)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000004', N'Szcze.Pływalnia-Ptr.-01-Pok.000004 (pom.tech)', 1809, NULL, 1, 1583)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000005', N'Szcze.Pływalnia-Ptr.-01-Pok.000005 (korytarz)', 1810, NULL, 1, 1584)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000006', N'Szcze.Pływalnia-Ptr.-01-Pok.000006 (korytarz)', 1811, NULL, 1, 1585)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000007', N'Szcze.Pływalnia-Ptr.-01-Pok.000007 (po.gosp.)', 1812, NULL, 1, 1586)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000008', N'Szcze.Pływalnia-Ptr.-01-Pok.000008 (po.gosp.)', 1813, NULL, 1, 1587)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000009', N'Szcze.Pływalnia-Ptr.-01-Pok.000009 (po.gosp.)', 1814, NULL, 1, 1588)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000010', N'Szcze.Pływalnia-Ptr.-01-Pok.000010 (po.gosp.)', 1815, NULL, 1, 1589)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.-01.000011', N'Szcze.Pływalnia-Ptr.-01-Pok.000011 (pom.tech)', 1816, NULL, 1, 1590)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000001', N'Szcze.Pływalnia-Ptr.000-Pok.000001 (korytarz)', 1817, NULL, 1, 1591)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001A', N'Szcze.Pływalnia-Ptr.000-Pok.00001A (przebier)', 1818, NULL, 1, 1592)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001B', N'Szcze.Pływalnia-Ptr.000-Pok.00001B (korytarz)', 1819, NULL, 1, 1593)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001C', N'Szcze.Pływalnia-Ptr.000-Pok.00001C (toaleta)', 1820, NULL, 1, 1594)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001D', N'Szcze.Pływalnia-Ptr.000-Pok.00001D (przebier)', 1821, NULL, 1, 1595)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001E', N'Szcze.Pływalnia-Ptr.000-Pok.00001E (łazie.)', 1822, NULL, 1, 1596)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001F', N'Szcze.Pływalnia-Ptr.000-Pok.00001F (przebier)', 1823, NULL, 1, 1597)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001G', N'Szcze.Pływalnia-Ptr.000-Pok.00001G (korytarz)', 1824, NULL, 1, 1598)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001H', N'Szcze.Pływalnia-Ptr.000-Pok.00001H (toaleta)', 1825, NULL, 1, 1599)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001I', N'Szcze.Pływalnia-Ptr.000-Pok.00001I (przebier)', 1826, NULL, 1, 1600)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001J', N'Szcze.Pływalnia-Ptr.000-Pok.00001J (łazie.)', 1827, NULL, 1, 1601)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00001K', N'Szcze.Pływalnia-Ptr.000-Pok.00001K (brodzik)', 1828, NULL, 1, 1602)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000002', N'Szcze.Pływalnia-Ptr.000-Pok.000002 (toaleta)', 1829, NULL, 1, 1603)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002A', N'Szcze.Pływalnia-Ptr.000-Pok.00002A (basen)', 1830, NULL, 1, 1604)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002B', N'Szcze.Pływalnia-Ptr.000-Pok.00002B (pom.soc.)', 1831, NULL, 1, 1605)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002C', N'Szcze.Pływalnia-Ptr.000-Pok.00002C (gab.lek.)', 1832, NULL, 1, 1606)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002D', N'Szcze.Pływalnia-Ptr.000-Pok.00002D (pom.soc.)', 1833, NULL, 1, 1607)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002E', N'Szcze.Pływalnia-Ptr.000-Pok.00002E (pom.soc.)', 1834, NULL, 1, 1608)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002F', N'Szcze.Pływalnia-Ptr.000-Pok.00002F (toaleta)', 1835, NULL, 1, 1609)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002G', N'Szcze.Pływalnia-Ptr.000-Pok.00002G (toaleta)', 1836, NULL, 1, 1610)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002H', N'Szcze.Pływalnia-Ptr.000-Pok.00002H (pom.tech)', 1837, NULL, 1, 1611)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002I', N'Szcze.Pływalnia-Ptr.000-Pok.00002I (pom.tech)', 1838, NULL, 1, 1612)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002J', N'Szcze.Pływalnia-Ptr.000-Pok.00002J (pom.tech)', 1839, NULL, 1, 1613)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002K', N'Szcze.Pływalnia-Ptr.000-Pok.00002K (pom.tech)', 1840, NULL, 1, 1614)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00002L', N'Szcze.Pływalnia-Ptr.000-Pok.00002L (po.gosp.)', 1841, NULL, 1, 1615)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000003', N'Szcze.Pływalnia-Ptr.000-Pok.000003 (biuro)', 1842, NULL, 1, 1616)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000004', N'Szcze.Pływalnia-Ptr.000-Pok.000004 (biuro)', 1843, NULL, 1, 1617)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000005', N'Szcze.Pływalnia-Ptr.000-Pok.000005 (biuro)', 1844, NULL, 1, 1618)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000006', N'Szcze.Pływalnia-Ptr.000-Pok.000006 (szatnia)', 1845, NULL, 1, 1619)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000007', N'Szcze.Pływalnia-Ptr.000-Pok.000007 (sklep)', 1846, NULL, 1, 1620)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.000014', N'Szcze.Pływalnia-Ptr.000-Pok.000014 (korytarz)', 1847, NULL, 1, 1621)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00014A', N'Szcze.Pływalnia-Ptr.000-Pok.00014A (toaleta)', 1848, NULL, 1, 1622)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00014B', N'Szcze.Pływalnia-Ptr.000-Pok.00014B (toaleta)', 1849, NULL, 1, 1623)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00014C', N'Szcze.Pływalnia-Ptr.000-Pok.00014C (toaleta)', 1850, NULL, 1, 1624)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.000.00014D', N'Szcze.Pływalnia-Ptr.000-Pok.00014D (korytarz)', 1851, NULL, 1, 1625)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000008', N'Szcze.Pływalnia-Ptr.001-Pok.000008 (sal.gim.)', 1852, NULL, 1, 1626)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00008A', N'Szcze.Pływalnia-Ptr.001-Pok.00008A (szatnia)', 1853, NULL, 1, 1627)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00008B', N'Szcze.Pływalnia-Ptr.001-Pok.00008B (szatnia)', 1854, NULL, 1, 1628)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000015', N'Szcze.Pływalnia-Ptr.001-Pok.000015 (korytarz)', 1855, NULL, 1, 1629)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000009', N'Szcze.Pływalnia-Ptr.001-Pok.000009 (sauna)', 1856, NULL, 1, 1630)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00009A', N'Szcze.Pływalnia-Ptr.001-Pok.00009A (sauna)', 1857, NULL, 1, 1631)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00009B', N'Szcze.Pływalnia-Ptr.001-Pok.00009B (łazie.)', 1858, NULL, 1, 1632)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00009C', N'Szcze.Pływalnia-Ptr.001-Pok.00009C (szatnia)', 1859, NULL, 1, 1633)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000010', N'Szcze.Pływalnia-Ptr.001-Pok.000010 (biuro)', 1860, NULL, 1, 1634)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000016', N'Szcze.Pływalnia-Ptr.001-Pok.000016 (korytarz)', 1861, NULL, 1, 1635)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00016A', N'Szcze.Pływalnia-Ptr.001-Pok.00016A (toaleta)', 1862, NULL, 1, 1636)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00016B', N'Szcze.Pływalnia-Ptr.001-Pok.00016B (toaleta)', 1863, NULL, 1, 1637)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000011', N'Szcze.Pływalnia-Ptr.001-Pok.000011 (biuro)', 1864, NULL, 1, 1638)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.00011A', N'Szcze.Pływalnia-Ptr.001-Pok.00011A (biuro)', 1865, NULL, 1, 1639)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000012', N'Szcze.Pływalnia-Ptr.001-Pok.000012 (pom.soc.)', 1866, NULL, 1, 1640)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.07.001.000013', N'Szcze.Pływalnia-Ptr.001-Pok.000013 (widow)', 1867, NULL, 1, 1641)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000001', N'Szcze.OD Szcze.-Ptr.000-Pok.000001 (szatnia)', 1868, NULL, 1, 1642)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00001A', N'Szcze.OD Szcze.-Ptr.000-Pok.00001A (portie.)', 1869, NULL, 1, 1643)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000002', N'Szcze.OD Szcze.-Ptr.000-Pok.000002 (toaleta)', 1870, NULL, 1, 1644)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000003', N'Szcze.OD Szcze.-Ptr.000-Pok.000003 (toaleta)', 1871, NULL, 1, 1645)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000008', N'Szcze.OD Szcze.-Ptr.000-Pok.000008 (biuro)', 1872, NULL, 1, 1646)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000009', N'Szcze.OD Szcze.-Ptr.000-Pok.000009 (biuro)', 1873, NULL, 1, 1647)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000010', N'Szcze.OD Szcze.-Ptr.000-Pok.000010 (biuro)', 1874, NULL, 1, 1648)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000011', N'Szcze.OD Szcze.-Ptr.000-Pok.000011 (korytarz)', 1875, NULL, 1, 1649)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000012', N'Szcze.OD Szcze.-Ptr.000-Pok.000012 (korytarz)', 1876, NULL, 1, 1650)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00012A', N'Szcze.OD Szcze.-Ptr.000-Pok.00012A (sal.komp)', 1877, NULL, 1, 1651)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00012B', N'Szcze.OD Szcze.-Ptr.000-Pok.00012B (sal.komp)', 1878, NULL, 1, 1652)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00012C', N'Szcze.OD Szcze.-Ptr.000-Pok.00012C (biuro)', 1879, NULL, 1, 1653)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00012D', N'Szcze.OD Szcze.-Ptr.000-Pok.00012D (toaleta)', 1880, NULL, 1, 1654)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000013', N'Szcze.OD Szcze.-Ptr.000-Pok.000013 (łazie.)', 1881, NULL, 1, 1655)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000014', N'Szcze.OD Szcze.-Ptr.000-Pok.000014 (szatnia)', 1882, NULL, 1, 1656)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000015', N'Szcze.OD Szcze.-Ptr.000-Pok.000015 (toaleta)', 1883, NULL, 1, 1657)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000016', N'Szcze.OD Szcze.-Ptr.000-Pok.000016 (po.gosp.)', 1884, NULL, 1, 1658)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000017', N'Szcze.OD Szcze.-Ptr.000-Pok.000017 (po.gosp.)', 1885, NULL, 1, 1659)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000018', N'Szcze.OD Szcze.-Ptr.000-Pok.000018 (szatnia)', 1886, NULL, 1, 1660)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000019', N'Szcze.OD Szcze.-Ptr.000-Pok.000019 (toaleta)', 1887, NULL, 1, 1661)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000020', N'Szcze.OD Szcze.-Ptr.000-Pok.000020 (po.gosp.)', 1888, NULL, 1, 1662)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000021', N'Szcze.OD Szcze.-Ptr.000-Pok.000021 (po.gosp.)', 1889, NULL, 1, 1663)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000022', N'Szcze.OD Szcze.-Ptr.000-Pok.000022 (po.gosp.)', 1890, NULL, 1, 1664)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000023', N'Szcze.OD Szcze.-Ptr.000-Pok.000023 (toaleta)', 1891, NULL, 1, 1665)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000024', N'Szcze.OD Szcze.-Ptr.000-Pok.000024 (toaleta)', 1892, NULL, 1, 1666)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000025', N'Szcze.OD Szcze.-Ptr.000-Pok.000025 (pom.tech)', 1893, NULL, 1, 1667)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000026', N'Szcze.OD Szcze.-Ptr.000-Pok.000026 (po.gosp.)', 1894, NULL, 1, 1668)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000027', N'Szcze.OD Szcze.-Ptr.000-Pok.000027 (po.gosp.)', 1895, NULL, 1, 1669)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000028', N'Szcze.OD Szcze.-Ptr.000-Pok.000028 (po.gosp.)', 1896, NULL, 1, 1670)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000029', N'Szcze.OD Szcze.-Ptr.000-Pok.000029 (toaleta)', 1897, NULL, 1, 1671)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000030', N'Szcze.OD Szcze.-Ptr.000-Pok.000030 (toaleta)', 1898, NULL, 1, 1672)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000031', N'Szcze.OD Szcze.-Ptr.000-Pok.000031 (korytarz)', 1899, NULL, 1, 1673)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.00031A', N'Szcze.OD Szcze.-Ptr.000-Pok.00031A (biuro)', 1900, NULL, 1, 1674)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000032', N'Szcze.OD Szcze.-Ptr.000-Pok.000032 (pom.tech)', 1901, NULL, 1, 1675)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000033', N'Szcze.OD Szcze.-Ptr.000-Pok.000033 (po.gosp.)', 1902, NULL, 1, 1676)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000040', N'Szcze.OD Szcze.-Ptr.000-Pok.000040 (garaż)', 1903, NULL, 1, 1677)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.000.000041', N'Szcze.OD Szcze.-Ptr.000-Pok.000041 (po.gosp.)', 1904, NULL, 1, 1678)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000004', N'Szcze.OD Szcze.-Ptr.001-Pok.000004 (sal.wykł)', 1905, NULL, 1, 1679)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.00004A', N'Szcze.OD Szcze.-Ptr.001-Pok.00004A (sal.repr)', 1906, NULL, 1, 1680)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000005', N'Szcze.OD Szcze.-Ptr.001-Pok.000005 (sal.wykł)', 1907, NULL, 1, 1681)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000006', N'Szcze.OD Szcze.-Ptr.001-Pok.000006 (sal.wykł)', 1908, NULL, 1, 1682)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000007', N'Szcze.OD Szcze.-Ptr.001-Pok.000007 (sal.wykł)', 1909, NULL, 1, 1683)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000034', N'Szcze.OD Szcze.-Ptr.001-Pok.000034 (stołów.)', 1910, NULL, 1, 1684)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000035', N'Szcze.OD Szcze.-Ptr.001-Pok.000035 (kuch.)', 1911, NULL, 1, 1685)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000036', N'Szcze.OD Szcze.-Ptr.001-Pok.000036 (kuch.)', 1912, NULL, 1, 1686)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000037', N'Szcze.OD Szcze.-Ptr.001-Pok.000037 (toaleta)', 1913, NULL, 1, 1687)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000038', N'Szcze.OD Szcze.-Ptr.001-Pok.000038 (sal.wykł)', 1914, NULL, 1, 1688)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.00038A', N'Szcze.OD Szcze.-Ptr.001-Pok.00038A (po.gosp.)', 1915, NULL, 1, 1689)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.000039', N'Szcze.OD Szcze.-Ptr.001-Pok.000039 (pom.soc.)', 1916, NULL, 1, 1690)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.08.001.00039A', N'Szcze.OD Szcze.-Ptr.001-Pok.00039A (po.gosp.)', 1917, NULL, 1, 1691)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.0000K1', N'Szcze.OD Podg.-Ptr.-01-Pok.0000K1 (korytarz)', 1918, NULL, 1, 1692)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000001', N'Szcze.OD Podg.-Ptr.-01-Pok.000001 (po.gosp.)', 1919, NULL, 1, 1693)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000002', N'Szcze.OD Podg.-Ptr.-01-Pok.000002 (toaleta)', 1920, NULL, 1, 1694)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000003', N'Szcze.OD Podg.-Ptr.-01-Pok.000003 (pom.tech)', 1921, NULL, 1, 1695)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.0000K2', N'Szcze.OD Podg.-Ptr.-01-Pok.0000K2 (korytarz)', 1922, NULL, 1, 1696)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000004', N'Szcze.OD Podg.-Ptr.-01-Pok.000004 (warszt)', 1923, NULL, 1, 1697)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000005', N'Szcze.OD Podg.-Ptr.-01-Pok.000005 (pom.tech)', 1924, NULL, 1, 1698)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000006', N'Szcze.OD Podg.-Ptr.-01-Pok.000006 (po.gosp.)', 1925, NULL, 1, 1699)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000007', N'Szcze.OD Podg.-Ptr.-01-Pok.000007 (po.gosp.)', 1926, NULL, 1, 1700)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.00000P', N'Szcze.OD Podg.-Ptr.-01-Pok.00000P (przebier)', 1927, NULL, 1, 1701)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000008', N'Szcze.OD Podg.-Ptr.-01-Pok.000008 (lab.)', 1928, NULL, 1, 1702)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.00008A', N'Szcze.OD Podg.-Ptr.-01-Pok.00008A (lab.)', 1929, NULL, 1, 1703)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000009', N'Szcze.OD Podg.-Ptr.-01-Pok.000009 (pom.tech)', 1930, NULL, 1, 1704)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000010', N'Szcze.OD Podg.-Ptr.-01-Pok.000010 (lab.)', 1931, NULL, 1, 1705)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.00010A', N'Szcze.OD Podg.-Ptr.-01-Pok.00010A (po.gosp.)', 1932, NULL, 1, 1706)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000011', N'Szcze.OD Podg.-Ptr.-01-Pok.000011 (lab.)', 1933, NULL, 1, 1707)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000012', N'Szcze.OD Podg.-Ptr.-01-Pok.000012 (lab.)', 1934, NULL, 1, 1708)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.00012A', N'Szcze.OD Podg.-Ptr.-01-Pok.00012A (pom.tech)', 1935, NULL, 1, 1709)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.000013', N'Szcze.OD Podg.-Ptr.-01-Pok.000013 (lab.)', 1936, NULL, 1, 1710)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.-01.00013A', N'Szcze.OD Podg.-Ptr.-01-Pok.00013A (pom.tech)', 1937, NULL, 1, 1711)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.0000K3', N'Szcze.OD Podg.-Ptr.000-Pok.0000K3 (korytarz)', 1938, NULL, 1, 1712)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.0000KS', N'Szcze.OD Podg.-Ptr.000-Pok.0000KS (korytarz)', 1939, NULL, 1, 1713)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000002', N'Szcze.OD Podg.-Ptr.000-Pok.000002 (portie.)', 1940, NULL, 1, 1714)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000003', N'Szcze.OD Podg.-Ptr.000-Pok.000003 (biuro)', 1941, NULL, 1, 1715)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000004', N'Szcze.OD Podg.-Ptr.000-Pok.000004 (lab.)', 1942, NULL, 1, 1716)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000005', N'Szcze.OD Podg.-Ptr.000-Pok.000005 (lab.)', 1943, NULL, 1, 1717)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00005A', N'Szcze.OD Podg.-Ptr.000-Pok.00005A (lab.)', 1944, NULL, 1, 1718)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000006', N'Szcze.OD Podg.-Ptr.000-Pok.000006 (lab.)', 1945, NULL, 1, 1719)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00006A', N'Szcze.OD Podg.-Ptr.000-Pok.00006A (po.gosp.)', 1946, NULL, 1, 1720)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00006B', N'Szcze.OD Podg.-Ptr.000-Pok.00006B (po.gosp.)', 1947, NULL, 1, 1721)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00006C', N'Szcze.OD Podg.-Ptr.000-Pok.00006C (po.gosp.)', 1948, NULL, 1, 1722)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000007', N'Szcze.OD Podg.-Ptr.000-Pok.000007 (po.gosp.)', 1949, NULL, 1, 1723)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000008', N'Szcze.OD Podg.-Ptr.000-Pok.000008 (szatnia)', 1950, NULL, 1, 1724)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.000009', N'Szcze.OD Podg.-Ptr.000-Pok.000009 (lab.)', 1951, NULL, 1, 1725)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00009A', N'Szcze.OD Podg.-Ptr.000-Pok.00009A (po.gosp.)', 1952, NULL, 1, 1726)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.000.00009B', N'Szcze.OD Podg.-Ptr.000-Pok.00009B (po.gosp.)', 1953, NULL, 1, 1727)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.0000K4', N'Szcze.OD Podg.-Ptr.001-Pok.0000K4 (korytarz)', 1954, NULL, 1, 1728)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.0000KS', N'Szcze.OD Podg.-Ptr.001-Pok.0000KS (korytarz)', 1955, NULL, 1, 1729)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000101', N'Szcze.OD Podg.-Ptr.001-Pok.000101 (sal.wykł)', 1956, NULL, 1, 1730)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.00101A', N'Szcze.OD Podg.-Ptr.001-Pok.00101A (biuro)', 1957, NULL, 1, 1731)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.00101B', N'Szcze.OD Podg.-Ptr.001-Pok.00101B (sal.komp)', 1958, NULL, 1, 1732)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000102', N'Szcze.OD Podg.-Ptr.001-Pok.000102 (biuro)', 1959, NULL, 1, 1733)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000103', N'Szcze.OD Podg.-Ptr.001-Pok.000103 (biuro)', 1960, NULL, 1, 1734)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000104', N'Szcze.OD Podg.-Ptr.001-Pok.000104 (biuro)', 1961, NULL, 1, 1735)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000105', N'Szcze.OD Podg.-Ptr.001-Pok.000105 (biuro)', 1962, NULL, 1, 1736)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000106', N'Szcze.OD Podg.-Ptr.001-Pok.000106 (biuro)', 1963, NULL, 1, 1737)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000107', N'Szcze.OD Podg.-Ptr.001-Pok.000107 (biuro)', 1964, NULL, 1, 1738)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000108', N'Szcze.OD Podg.-Ptr.001-Pok.000108 (biuro)', 1965, NULL, 1, 1739)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000109', N'Szcze.OD Podg.-Ptr.001-Pok.000109 (biuro)', 1966, NULL, 1, 1740)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.00109B', N'Szcze.OD Podg.-Ptr.001-Pok.00109B (pom.soc.)', 1967, NULL, 1, 1741)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.00109A', N'Szcze.OD Podg.-Ptr.001-Pok.00109A (sal.konf)', 1968, NULL, 1, 1742)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000110', N'Szcze.OD Podg.-Ptr.001-Pok.000110 (biuro)', 1969, NULL, 1, 1743)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000111', N'Szcze.OD Podg.-Ptr.001-Pok.000111 (biuro)', 1970, NULL, 1, 1744)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000112', N'Szcze.OD Podg.-Ptr.001-Pok.000112 (sal.konf)', 1971, NULL, 1, 1745)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.000113', N'Szcze.OD Podg.-Ptr.001-Pok.000113 (sal.wykł)', 1972, NULL, 1, 1746)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.001.00113A', N'Szcze.OD Podg.-Ptr.001-Pok.00113A (biuro)', 1973, NULL, 1, 1747)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.0000K5', N'Szcze.OD Podg.-Ptr.002-Pok.0000K5 (korytarz)', 1974, NULL, 1, 1748)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.0000KS', N'Szcze.OD Podg.-Ptr.002-Pok.0000KS (korytarz)', 1975, NULL, 1, 1749)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000201', N'Szcze.OD Podg.-Ptr.002-Pok.000201 (sal.komp)', 1976, NULL, 1, 1750)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000202', N'Szcze.OD Podg.-Ptr.002-Pok.000202 (biuro)', 1977, NULL, 1, 1751)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000203', N'Szcze.OD Podg.-Ptr.002-Pok.000203 (sal.wykł)', 1978, NULL, 1, 1752)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000204', N'Szcze.OD Podg.-Ptr.002-Pok.000204 (lab.)', 1979, NULL, 1, 1753)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000205', N'Szcze.OD Podg.-Ptr.002-Pok.000205 (lab.)', 1980, NULL, 1, 1754)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000206', N'Szcze.OD Podg.-Ptr.002-Pok.000206 (lab.)', 1981, NULL, 1, 1755)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000207', N'Szcze.OD Podg.-Ptr.002-Pok.000207 (lab.)', 1982, NULL, 1, 1756)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000208', N'Szcze.OD Podg.-Ptr.002-Pok.000208 (lab.)', 1983, NULL, 1, 1757)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000209', N'Szcze.OD Podg.-Ptr.002-Pok.000209 (biuro)', 1984, NULL, 1, 1758)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000210', N'Szcze.OD Podg.-Ptr.002-Pok.000210 (biuro)', 1985, NULL, 1, 1759)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000211', N'Szcze.OD Podg.-Ptr.002-Pok.000211 (biuro)', 1986, NULL, 1, 1760)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000212', N'Szcze.OD Podg.-Ptr.002-Pok.000212 (biuro)', 1987, NULL, 1, 1761)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000213', N'Szcze.OD Podg.-Ptr.002-Pok.000213 (po.gosp.)', 1988, NULL, 1, 1762)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000214', N'Szcze.OD Podg.-Ptr.002-Pok.000214 (biuro)', 1989, NULL, 1, 1763)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000215', N'Szcze.OD Podg.-Ptr.002-Pok.000215 (lab.)', 1990, NULL, 1, 1764)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000216', N'Szcze.OD Podg.-Ptr.002-Pok.000216 (lab.)', 1991, NULL, 1, 1765)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000217', N'Szcze.OD Podg.-Ptr.002-Pok.000217 (lab.)', 1992, NULL, 1, 1766)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.002.000218', N'Szcze.OD Podg.-Ptr.002-Pok.000218 (biuro)', 1993, NULL, 1, 1767)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.0000K6', N'Szcze.OD Podg.-Ptr.003-Pok.0000K6 (korytarz)', 1994, NULL, 1, 1768)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.0000KS', N'Szcze.OD Podg.-Ptr.003-Pok.0000KS (korytarz)', 1995, NULL, 1, 1769)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000301', N'Szcze.OD Podg.-Ptr.003-Pok.000301 (pom.soc.)', 1996, NULL, 1, 1770)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000303', N'Szcze.OD Podg.-Ptr.003-Pok.000303 (biuro)', 1997, NULL, 1, 1771)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000304', N'Szcze.OD Podg.-Ptr.003-Pok.000304 (biuro)', 1998, NULL, 1, 1772)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000305', N'Szcze.OD Podg.-Ptr.003-Pok.000305 (biuro)', 1999, NULL, 1, 1773)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000306', N'Szcze.OD Podg.-Ptr.003-Pok.000306 (biuro)', 2000, NULL, 1, 1774)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000307', N'Szcze.OD Podg.-Ptr.003-Pok.000307 (lab.)', 2001, NULL, 1, 1775)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000308', N'Szcze.OD Podg.-Ptr.003-Pok.000308 (biuro)', 2002, NULL, 1, 1776)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000309', N'Szcze.OD Podg.-Ptr.003-Pok.000309 (biuro)', 2003, NULL, 1, 1777)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000310', N'Szcze.OD Podg.-Ptr.003-Pok.000310 (biuro)', 2004, NULL, 1, 1778)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000311', N'Szcze.OD Podg.-Ptr.003-Pok.000311 (biuro)', 2005, NULL, 1, 1779)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000312', N'Szcze.OD Podg.-Ptr.003-Pok.000312 (po.gosp.)', 2006, NULL, 1, 1780)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000314', N'Szcze.OD Podg.-Ptr.003-Pok.000314 (serwer)', 2007, NULL, 1, 1781)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000315', N'Szcze.OD Podg.-Ptr.003-Pok.000315 (biuro)', 2008, NULL, 1, 1782)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000316', N'Szcze.OD Podg.-Ptr.003-Pok.000316 (lab.)', 2009, NULL, 1, 1783)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000317', N'Szcze.OD Podg.-Ptr.003-Pok.000317 (lab.)', 2010, NULL, 1, 1784)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000318', N'Szcze.OD Podg.-Ptr.003-Pok.000318 (lab.)', 2011, NULL, 1, 1785)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000320', N'Szcze.OD Podg.-Ptr.003-Pok.000320 (sal.wykł)', 2012, NULL, 1, 1786)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000321', N'Szcze.OD Podg.-Ptr.003-Pok.000321 (sal.wykł)', 2013, NULL, 1, 1787)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000322', N'Szcze.OD Podg.-Ptr.003-Pok.000322 (sal.wykł)', 2014, NULL, 1, 1788)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.003.000323', N'Szcze.OD Podg.-Ptr.003-Pok.000323 (sal.wykł)', 2015, NULL, 1, 1789)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.004.000401', N'Szcze.OD Podg.-Ptr.004-Pok.000401 (po.gosp.)', 2016, NULL, 1, 1790)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.004.0000KS', N'Szcze.OD Podg.-Ptr.004-Pok.0000KS (korytarz)', 2017, NULL, 1, 1791)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.004.000402', N'Szcze.OD Podg.-Ptr.004-Pok.000402 (sal.wykł)', 2018, NULL, 1, 1792)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.09.004.000403', N'Szcze.OD Podg.-Ptr.004-Pok.000403 (strych)', 2019, NULL, 1, 1793)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000002', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000002 (pom.tech)', 2020, NULL, 1, 1794)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000003', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000003 (mag.)', 2021, NULL, 1, 1795)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000005', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000005 (pom.soc.)', 2022, NULL, 1, 1796)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000008', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000008 (po.gosp.)', 2023, NULL, 1, 1797)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000009', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000009 (pom.tech)', 2024, NULL, 1, 1798)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000017', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000017 (szatnia)', 2025, NULL, 1, 1799)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000022', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000022 (mag.)', 2026, NULL, 1, 1800)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.-01.000023', N'Szcze.OD Żołnie.-Ptr.-01-Pok.000023 (mag.)', 2027, NULL, 1, 1801)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000004', N'Szcze.OD Żołnie.-Ptr.000-Pok.000004 (portie.)', 2028, NULL, 1, 1802)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000005', N'Szcze.OD Żołnie.-Ptr.000-Pok.000005 (lab.)', 2029, NULL, 1, 1803)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000006', N'Szcze.OD Żołnie.-Ptr.000-Pok.000006 (po.gosp.)', 2030, NULL, 1, 1804)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000009', N'Szcze.OD Żołnie.-Ptr.000-Pok.000009 (toaleta)', 2031, NULL, 1, 1805)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000012', N'Szcze.OD Żołnie.-Ptr.000-Pok.000012 (biuro)', 2032, NULL, 1, 1806)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000013', N'Szcze.OD Żołnie.-Ptr.000-Pok.000013 (biuro)', 2033, NULL, 1, 1807)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000015', N'Szcze.OD Żołnie.-Ptr.000-Pok.000015 (serwer)', 2034, NULL, 1, 1808)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000016', N'Szcze.OD Żołnie.-Ptr.000-Pok.000016 (biuro)', 2035, NULL, 1, 1809)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000017', N'Szcze.OD Żołnie.-Ptr.000-Pok.000017 (sal.wykł)', 2036, NULL, 1, 1810)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000018', N'Szcze.OD Żołnie.-Ptr.000-Pok.000018 (sal.wykł)', 2037, NULL, 1, 1811)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000019', N'Szcze.OD Żołnie.-Ptr.000-Pok.000019 (biuro)', 2038, NULL, 1, 1812)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000021', N'Szcze.OD Żołnie.-Ptr.000-Pok.000021 (lab.)', 2039, NULL, 1, 1813)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000024', N'Szcze.OD Żołnie.-Ptr.000-Pok.000024 (sal.wykł)', 2040, NULL, 1, 1814)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000001', N'Szcze.OD Żołnie.-Ptr.000-Pok.000001 (miesz.)', 2041, NULL, 1, 1815)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.000.000002', N'Szcze.OD Żołnie.-Ptr.000-Pok.000002 (miesz.)', 2042, NULL, 1, 1816)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000103', N'Szcze.OD Żołnie.-Ptr.001-Pok.000103 (biuro)', 2043, NULL, 1, 1817)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000104', N'Szcze.OD Żołnie.-Ptr.001-Pok.000104 (biuro)', 2044, NULL, 1, 1818)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000105', N'Szcze.OD Żołnie.-Ptr.001-Pok.000105 (bufet)', 2045, NULL, 1, 1819)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000108', N'Szcze.OD Żołnie.-Ptr.001-Pok.000108 (toaleta)', 2046, NULL, 1, 1820)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000109', N'Szcze.OD Żołnie.-Ptr.001-Pok.000109 (po.gosp.)', 2047, NULL, 1, 1821)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000110', N'Szcze.OD Żołnie.-Ptr.001-Pok.000110 (toaleta)', 2048, NULL, 1, 1822)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000119', N'Szcze.OD Żołnie.-Ptr.001-Pok.000119 (lab.)', 2049, NULL, 1, 1823)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000120', N'Szcze.OD Żołnie.-Ptr.001-Pok.000120 (biuro)', 2050, NULL, 1, 1824)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000121', N'Szcze.OD Żołnie.-Ptr.001-Pok.000121 (biuro)', 2051, NULL, 1, 1825)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000122', N'Szcze.OD Żołnie.-Ptr.001-Pok.000122 (biuro)', 2052, NULL, 1, 1826)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000123', N'Szcze.OD Żołnie.-Ptr.001-Pok.000123 (biuro)', 2053, NULL, 1, 1827)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000124', N'Szcze.OD Żołnie.-Ptr.001-Pok.000124 (sal.wykł)', 2054, NULL, 1, 1828)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000125', N'Szcze.OD Żołnie.-Ptr.001-Pok.000125 (sal.wykł)', 2055, NULL, 1, 1829)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000003', N'Szcze.OD Żołnie.-Ptr.001-Pok.000003 (miesz.)', 2056, NULL, 1, 1830)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.001.000004', N'Szcze.OD Żołnie.-Ptr.001-Pok.000004 (miesz.)', 2057, NULL, 1, 1831)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000202', N'Szcze.OD Żołnie.-Ptr.002-Pok.000202 (toaleta)', 2058, NULL, 1, 1832)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000203', N'Szcze.OD Żołnie.-Ptr.002-Pok.000203 (toaleta)', 2059, NULL, 1, 1833)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000204', N'Szcze.OD Żołnie.-Ptr.002-Pok.000204 (pom.soc.)', 2060, NULL, 1, 1834)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000205', N'Szcze.OD Żołnie.-Ptr.002-Pok.000205 (biuro)', 2061, NULL, 1, 1835)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000207', N'Szcze.OD Żołnie.-Ptr.002-Pok.000207 (biuro)', 2062, NULL, 1, 1836)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000208', N'Szcze.OD Żołnie.-Ptr.002-Pok.000208 (biuro)', 2063, NULL, 1, 1837)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000005', N'Szcze.OD Żołnie.-Ptr.002-Pok.000005 (miesz.)', 2064, NULL, 1, 1838)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000006', N'Szcze.OD Żołnie.-Ptr.002-Pok.000006 (miesz.)', 2065, NULL, 1, 1839)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000007', N'Szcze.OD Żołnie.-Ptr.002-Pok.000007 (miesz.)', 2066, NULL, 1, 1840)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000008', N'Szcze.OD Żołnie.-Ptr.002-Pok.000008 (miesz.)', 2067, NULL, 1, 1841)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000009', N'Szcze.OD Żołnie.-Ptr.002-Pok.000009 (miesz.)', 2068, NULL, 1, 1842)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000010', N'Szcze.OD Żołnie.-Ptr.002-Pok.000010 (miesz.)', 2069, NULL, 1, 1843)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000011', N'Szcze.OD Żołnie.-Ptr.002-Pok.000011 (miesz.)', 2070, NULL, 1, 1844)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000213', N'Szcze.OD Żołnie.-Ptr.002-Pok.000213 (toaleta)', 2071, NULL, 1, 1845)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'1.10.002.000216', N'Szcze.OD Żołnie.-Ptr.002-Pok.000216 (pom.tech)', 2072, NULL, 1, 1846)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000002', N'Świn.DPT 1-Ptr.-01-Pok.000002 (po.gosp.)', 2073, NULL, 1, 1847)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000003', N'Świn.DPT 1-Ptr.-01-Pok.000003 (po.gosp.)', 2074, NULL, 1, 1848)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000004', N'Świn.DPT 1-Ptr.-01-Pok.000004 (po.gosp.)', 2075, NULL, 1, 1849)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000005', N'Świn.DPT 1-Ptr.-01-Pok.000005 (sal.dyd.)', 2076, NULL, 1, 1850)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000006', N'Świn.DPT 1-Ptr.-01-Pok.000006 (sal.dyd.)', 2077, NULL, 1, 1851)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000008', N'Świn.DPT 1-Ptr.-01-Pok.000008 (po.gosp.)', 2078, NULL, 1, 1852)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000009', N'Świn.DPT 1-Ptr.-01-Pok.000009 (toaleta)', 2079, NULL, 1, 1853)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000010', N'Świn.DPT 1-Ptr.-01-Pok.000010 (pom.soc.)', 2080, NULL, 1, 1854)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000011', N'Świn.DPT 1-Ptr.-01-Pok.000011 (po.gosp.)', 2081, NULL, 1, 1855)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000012', N'Świn.DPT 1-Ptr.-01-Pok.000012 (po.gosp.)', 2082, NULL, 1, 1856)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000013', N'Świn.DPT 1-Ptr.-01-Pok.000013 (po.gosp.)', 2083, NULL, 1, 1857)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000014', N'Świn.DPT 1-Ptr.-01-Pok.000014 (po.gosp.)', 2084, NULL, 1, 1858)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000023', N'Świn.DPT 1-Ptr.-01-Pok.000023 (po.gosp.)', 2085, NULL, 1, 1859)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000015', N'Świn.DPT 1-Ptr.-01-Pok.000015 (biuro)', 2086, NULL, 1, 1860)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000016', N'Świn.DPT 1-Ptr.-01-Pok.000016 (toaleta)', 2087, NULL, 1, 1861)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000017', N'Świn.DPT 1-Ptr.-01-Pok.000017 (sal.dyd.)', 2088, NULL, 1, 1862)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.-01.000018', N'Świn.DPT 1-Ptr.-01-Pok.000018 (pom.tech)', 2089, NULL, 1, 1863)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000008', N'Świn.DPT 1-Ptr.000-Pok.000008 (portie.)', 2090, NULL, 1, 1864)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000009', N'Świn.DPT 1-Ptr.000-Pok.000009 (szatnia)', 2091, NULL, 1, 1865)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000010', N'Świn.DPT 1-Ptr.000-Pok.000010 (sal.dyd.)', 2092, NULL, 1, 1866)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000011', N'Świn.DPT 1-Ptr.000-Pok.000011 (sal.dyd.)', 2093, NULL, 1, 1867)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000012', N'Świn.DPT 1-Ptr.000-Pok.000012 (sal.konf)', 2094, NULL, 1, 1868)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000015', N'Świn.DPT 1-Ptr.000-Pok.000015 (bufet)', 2095, NULL, 1, 1869)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000018', N'Świn.DPT 1-Ptr.000-Pok.000018 (sauna)', 2096, NULL, 1, 1870)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000019', N'Świn.DPT 1-Ptr.000-Pok.000019 (toaleta)', 2097, NULL, 1, 1871)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.000.000020', N'Świn.DPT 1-Ptr.000-Pok.000020 (toaleta)', 2098, NULL, 1, 1872)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000100', N'Świn.DPT 1-Ptr.001-Pok.000100 (pokój)', 2099, NULL, 1, 1873)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000101', N'Świn.DPT 1-Ptr.001-Pok.000101 (pokój)', 2100, NULL, 1, 1874)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000102', N'Świn.DPT 1-Ptr.001-Pok.000102 (pokój)', 2101, NULL, 1, 1875)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000103', N'Świn.DPT 1-Ptr.001-Pok.000103 (pokój)', 2102, NULL, 1, 1876)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000104', N'Świn.DPT 1-Ptr.001-Pok.000104 (pokój)', 2103, NULL, 1, 1877)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000105', N'Świn.DPT 1-Ptr.001-Pok.000105 (pokój)', 2104, NULL, 1, 1878)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000106', N'Świn.DPT 1-Ptr.001-Pok.000106 (pokój)', 2105, NULL, 1, 1879)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000107', N'Świn.DPT 1-Ptr.001-Pok.000107 (pokój)', 2106, NULL, 1, 1880)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000108', N'Świn.DPT 1-Ptr.001-Pok.000108 (pokój)', 2107, NULL, 1, 1881)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000109', N'Świn.DPT 1-Ptr.001-Pok.000109 (pokój)', 2108, NULL, 1, 1882)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000110', N'Świn.DPT 1-Ptr.001-Pok.000110 (pokój)', 2109, NULL, 1, 1883)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000111', N'Świn.DPT 1-Ptr.001-Pok.000111 (pokój)', 2110, NULL, 1, 1884)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000112', N'Świn.DPT 1-Ptr.001-Pok.000112 (po.gosp.)', 2111, NULL, 1, 1885)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000113', N'Świn.DPT 1-Ptr.001-Pok.000113 (pokój)', 2112, NULL, 1, 1886)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.001.000114', N'Świn.DPT 1-Ptr.001-Pok.000114 (pokój)', 2113, NULL, 1, 1887)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.002.000200', N'Świn.DPT 1-Ptr.002-Pok.000200 (lab.)', 2114, NULL, 1, 1888)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.01.002.000201', N'Świn.DPT 1-Ptr.002-Pok.000201 (po.gosp.)', 2115, NULL, 1, 1889)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.-01.000001', N'Świn.DPT 2-Ptr.-01-Pok.000001 (bufet)', 2116, NULL, 1, 1890)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.-01.000002', N'Świn.DPT 2-Ptr.-01-Pok.000002 (lab.)', 2117, NULL, 1, 1891)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000020', N'Świn.DPT 2-Ptr.000-Pok.000020 (pokój)', 2118, NULL, 1, 1892)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000021', N'Świn.DPT 2-Ptr.000-Pok.000021 (pokój)', 2119, NULL, 1, 1893)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000022', N'Świn.DPT 2-Ptr.000-Pok.000022 (pokój)', 2120, NULL, 1, 1894)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000023', N'Świn.DPT 2-Ptr.000-Pok.000023 (pokój)', 2121, NULL, 1, 1895)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000024', N'Świn.DPT 2-Ptr.000-Pok.000024 (pokój)', 2122, NULL, 1, 1896)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000025', N'Świn.DPT 2-Ptr.000-Pok.000025 (pokój)', 2123, NULL, 1, 1897)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000026', N'Świn.DPT 2-Ptr.000-Pok.000026 (pokój)', 2124, NULL, 1, 1898)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000027', N'Świn.DPT 2-Ptr.000-Pok.000027 (pokój)', 2125, NULL, 1, 1899)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000028', N'Świn.DPT 2-Ptr.000-Pok.000028 (po.gosp.)', 2126, NULL, 1, 1900)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000029', N'Świn.DPT 2-Ptr.000-Pok.000029 (pokój)', 2127, NULL, 1, 1901)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000030', N'Świn.DPT 2-Ptr.000-Pok.000030 (pokój)', 2128, NULL, 1, 1902)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000031', N'Świn.DPT 2-Ptr.000-Pok.000031 (pokój)', 2129, NULL, 1, 1903)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000032', N'Świn.DPT 2-Ptr.000-Pok.000032 (pokój)', 2130, NULL, 1, 1904)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000033', N'Świn.DPT 2-Ptr.000-Pok.000033 (pokój)', 2131, NULL, 1, 1905)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000034', N'Świn.DPT 2-Ptr.000-Pok.000034 (pokój)', 2132, NULL, 1, 1906)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000035', N'Świn.DPT 2-Ptr.000-Pok.000035 (biuro)', 2133, NULL, 1, 1907)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000036', N'Świn.DPT 2-Ptr.000-Pok.000036 (toaleta)', 2134, NULL, 1, 1908)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.000.000037', N'Świn.DPT 2-Ptr.000-Pok.000037 (toaleta)', 2135, NULL, 1, 1909)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000120', N'Świn.DPT 2-Ptr.001-Pok.000120 (pokój)', 2136, NULL, 1, 1910)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000121', N'Świn.DPT 2-Ptr.001-Pok.000121 (pokój)', 2137, NULL, 1, 1911)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000122', N'Świn.DPT 2-Ptr.001-Pok.000122 (pokój)', 2138, NULL, 1, 1912)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000123', N'Świn.DPT 2-Ptr.001-Pok.000123 (pokój)', 2139, NULL, 1, 1913)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000124', N'Świn.DPT 2-Ptr.001-Pok.000124 (pokój)', 2140, NULL, 1, 1914)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000125', N'Świn.DPT 2-Ptr.001-Pok.000125 (pokój)', 2141, NULL, 1, 1915)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000126', N'Świn.DPT 2-Ptr.001-Pok.000126 (pokój)', 2142, NULL, 1, 1916)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000127', N'Świn.DPT 2-Ptr.001-Pok.000127 (pokój)', 2143, NULL, 1, 1917)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000128', N'Świn.DPT 2-Ptr.001-Pok.000128 (po.gosp.)', 2144, NULL, 1, 1918)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000129', N'Świn.DPT 2-Ptr.001-Pok.000129 (pokój)', 2145, NULL, 1, 1919)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000130', N'Świn.DPT 2-Ptr.001-Pok.000130 (pokój)', 2146, NULL, 1, 1920)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000131', N'Świn.DPT 2-Ptr.001-Pok.000131 (pokój)', 2147, NULL, 1, 1921)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000132', N'Świn.DPT 2-Ptr.001-Pok.000132 (pokój)', 2148, NULL, 1, 1922)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000133', N'Świn.DPT 2-Ptr.001-Pok.000133 (pokój)', 2149, NULL, 1, 1923)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000134', N'Świn.DPT 2-Ptr.001-Pok.000134 (pokój)', 2150, NULL, 1, 1924)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.001.000135', N'Świn.DPT 2-Ptr.001-Pok.000135 (pokój)', 2151, NULL, 1, 1925)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000220', N'Świn.DPT 2-Ptr.002-Pok.000220 (pokój)', 2152, NULL, 1, 1926)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000221', N'Świn.DPT 2-Ptr.002-Pok.000221 (pokój)', 2153, NULL, 1, 1927)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000222', N'Świn.DPT 2-Ptr.002-Pok.000222 (pokój)', 2154, NULL, 1, 1928)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000223', N'Świn.DPT 2-Ptr.002-Pok.000223 (po.gosp.)', 2155, NULL, 1, 1929)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000224', N'Świn.DPT 2-Ptr.002-Pok.000224 (pokój)', 2156, NULL, 1, 1930)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000225', N'Świn.DPT 2-Ptr.002-Pok.000225 (pokój)', 2157, NULL, 1, 1931)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.02.002.000226', N'Świn.DPT 2-Ptr.002-Pok.000226 (pokój)', 2158, NULL, 1, 1932)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.03.000.000001', N'Świn.DPT 3-Ptr.000-Pok.000001 (po.gosp.)', 2159, NULL, 1, 1933)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.03.000.000002', N'Świn.DPT 3-Ptr.000-Pok.000002 (po.gosp.)', 2160, NULL, 1, 1934)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.03.000.000003', N'Świn.DPT 3-Ptr.000-Pok.000003 (po.gosp.)', 2161, NULL, 1, 1935)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.03.000.000004', N'Świn.DPT 3-Ptr.000-Pok.000004 (po.gosp.)', 2162, NULL, 1, 1936)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'2.03.000.000005', N'Świn.DPT 3-Ptr.000-Pok.000005 (toaleta)', 2163, NULL, 1, 1937)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000007', N'Kołob.OD Kołob.-Ptr.000-Pok.000007 (sal.wykł)', 2164, NULL, 1, 1938)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000008', N'Kołob.OD Kołob.-Ptr.000-Pok.000008 (sal.wykł)', 2165, NULL, 1, 1939)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000010', N'Kołob.OD Kołob.-Ptr.000-Pok.000010 (po.gosp.)', 2166, NULL, 1, 1940)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000012', N'Kołob.OD Kołob.-Ptr.000-Pok.000012 (pom.tech)', 2167, NULL, 1, 1941)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000013', N'Kołob.OD Kołob.-Ptr.000-Pok.000013 (serwer)', 2168, NULL, 1, 1942)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000014', N'Kołob.OD Kołob.-Ptr.000-Pok.000014 (toaleta)', 2169, NULL, 1, 1943)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000016', N'Kołob.OD Kołob.-Ptr.000-Pok.000016 (toaleta)', 2170, NULL, 1, 1944)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000015', N'Kołob.OD Kołob.-Ptr.000-Pok.000015 (toaleta)', 2171, NULL, 1, 1945)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000006', N'Kołob.OD Kołob.-Ptr.000-Pok.000006 (pom.tech)', 2172, NULL, 1, 1946)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000003', N'Kołob.OD Kołob.-Ptr.000-Pok.000003 (biuro)', 2173, NULL, 1, 1947)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000004', N'Kołob.OD Kołob.-Ptr.000-Pok.000004 (biuro)', 2174, NULL, 1, 1948)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.000.000005', N'Kołob.OD Kołob.-Ptr.000-Pok.000005 (arch)', 2175, NULL, 1, 1949)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000105', N'Kołob.OD Kołob.-Ptr.001-Pok.000105 (lab.)', 2176, NULL, 1, 1950)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000106', N'Kołob.OD Kołob.-Ptr.001-Pok.000106 (lab.)', 2177, NULL, 1, 1951)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000108', N'Kołob.OD Kołob.-Ptr.001-Pok.000108 (lab.)', 2178, NULL, 1, 1952)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000104', N'Kołob.OD Kołob.-Ptr.001-Pok.000104 (lab.)', 2179, NULL, 1, 1953)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000103', N'Kołob.OD Kołob.-Ptr.001-Pok.000103 (lab.)', 2180, NULL, 1, 1954)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000109', N'Kołob.OD Kołob.-Ptr.001-Pok.000109 (lab.)', 2181, NULL, 1, 1955)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000111', N'Kołob.OD Kołob.-Ptr.001-Pok.000111 (toaleta)', 2182, NULL, 1, 1956)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.001.000110', N'Kołob.OD Kołob.-Ptr.001-Pok.000110 (toaleta)', 2183, NULL, 1, 1957)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000206', N'Kołob.OD Kołob.-Ptr.002-Pok.000206 (korytarz)', 2184, NULL, 1, 1958)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000207', N'Kołob.OD Kołob.-Ptr.002-Pok.000207 (pokój)', 2185, NULL, 1, 1959)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000208', N'Kołob.OD Kołob.-Ptr.002-Pok.000208 (pokój)', 2186, NULL, 1, 1960)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000209', N'Kołob.OD Kołob.-Ptr.002-Pok.000209 (pokój)', 2187, NULL, 1, 1961)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000210', N'Kołob.OD Kołob.-Ptr.002-Pok.000210 (pokój)', 2188, NULL, 1, 1962)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000205', N'Kołob.OD Kołob.-Ptr.002-Pok.000205 (pom.soc.)', 2189, NULL, 1, 1963)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000203', N'Kołob.OD Kołob.-Ptr.002-Pok.000203 (sal.komp)', 2190, NULL, 1, 1964)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.01.002.000202', N'Kołob.OD Kołob.-Ptr.002-Pok.000202 (lab.)', 2191, NULL, 1, 1965)
GO
INSERT [dsd_ams_asset_classification] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'3.02.000.000001', N'Kołob.Siecia. Kołob.-Ptr.000-Pok.000001 (lab.)', 2192, NULL, 1, 1966)
GO
SET IDENTITY_INSERT [dsd_ams_asset_classification] OFF
GO
SET IDENTITY_INSERT [dsd_ams_asset_obtainment_form] ON 

GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-T', N'Środki trwałe umarzane w czasie', 1, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-W', N'Wyposażenie', 7, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-WNiP', N'WNiP umarzane w czasie', 9, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-OPR', N'Oprogramowanie umarzane 100%', 10, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-MNiSW', N'Aparatura badawcza (pozabilanowo)', 12, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-OBT', N'Środki trwałe dzierżawione', 13, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'B', N'Budynki', 14, NULL, 1, 7)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'P', N'Pokoje', 15, NULL, 1, 8)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-K', N'Składowa', 20, NULL, 1, 9)
GO
INSERT [dsd_ams_asset_obtainment_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM-SPRZ', N'Środki trwałe przeznaczone do sprzedaży', 21, NULL, 1, 10)
GO
SET IDENTITY_INSERT [dsd_ams_asset_obtainment_form] OFF
GO
SET IDENTITY_INSERT [dsd_ams_asset_ownership_form] ON 

GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'011', N'Środki trwałe + wyposażenie', 3, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'012', N'Projekty Unijne Środki trwałe + wyposażenie', 9, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'020', N'WNiP', 10, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'021', N'Oprogramowanie niskiej wartości', 11, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'022', N'Projekty Unijne WNiP', 12, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'023', N'Projekty Unijne Oprogramowanie niskiej wartości', 13, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_ownership_form] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'999', N'Inne', 14, NULL, 1, 7)
GO
SET IDENTITY_INSERT [dsd_ams_asset_ownership_form] OFF
GO
SET IDENTITY_INSERT [dsd_ams_asset_purpose] ON 

GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Dydaktyka', N'dydaktyka', 1, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Administracja', N'administracja', 2, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FPM', N'Fundusz pomocy materialnej', 3, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Projekty', N'Projekty  Unijne', 4, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Prace', N'Prace badawcze', 5, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Prace badawcze', N'Prace badawcze', 6, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_purpose] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'Inne', N'Inne', 7, NULL, 1, 7)
GO
SET IDENTITY_INSERT [dsd_ams_asset_purpose] OFF
GO
SET IDENTITY_INSERT [dsd_ams_asset_responsible_person] ON 

GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Nowak', 2, NULL, 1, 1)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Beata', 5, NULL, 1, 2)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Adamkiewicz', 6, NULL, 1, 3)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Artyszuk', 7, NULL, 1, 4)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Banaszak', 8, NULL, 1, 5)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Behrendt', 9, NULL, 1, 6)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Bejger', 10, NULL, 1, 7)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Białas-Motyl', 11, NULL, 1, 8)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Bienkiewicz', 12, NULL, 1, 9)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Bogdzia', 13, NULL, 1, 10)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Borkowski', 14, NULL, 1, 11)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Borkowski', 15, NULL, 1, 12)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Brzózka', 16, NULL, 1, 13)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chmiel', 17, NULL, 1, 14)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chorzelewski', 18, NULL, 1, 15)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chrzanowski', 19, NULL, 1, 16)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chuta', 21, NULL, 1, 17)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chwesiuk', 22, NULL, 1, 18)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Czarnecki', 23, NULL, 1, 19)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Czarnecki', 24, NULL, 1, 20)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Druchniak', 25, NULL, 1, 21)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Drzewieniecka', 26, NULL, 1, 22)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Dzikowski', 27, NULL, 1, 23)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Edelman', 28, NULL, 1, 24)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Ehrentraut', 29, NULL, 1, 25)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Frydrych', 30, NULL, 1, 26)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Gabriel', 31, NULL, 1, 27)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Galor', 33, NULL, 1, 28)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Gawdzińska', 34, NULL, 1, 29)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Górtowska', 35, NULL, 1, 30)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Górzeński', 36, NULL, 1, 31)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Grabian', 37, NULL, 1, 32)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Grządziel', 38, NULL, 1, 33)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Grzybowska', 39, NULL, 1, 34)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Gucma', 40, NULL, 1, 35)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Guziewicz-Fajdek', 41, NULL, 1, 36)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Hajder', 42, NULL, 1, 37)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Iwan', 43, NULL, 1, 38)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Jackowski', 44, NULL, 1, 39)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Jankowska', 45, NULL, 1, 40)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Jankowski', 46, NULL, 1, 41)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Jóźwiak', 47, NULL, 1, 42)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kasyk', 48, NULL, 1, 43)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Klewski', 49, NULL, 1, 44)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Klyus', 50, NULL, 1, 45)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kowalski', 51, NULL, 1, 46)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kozak', 52, NULL, 1, 47)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Krasowska', 53, NULL, 1, 48)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Krysiak', 54, NULL, 1, 49)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Lachowicz-Zielińska', 55, NULL, 1, 50)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Lewandowski', 56, NULL, 1, 51)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Lipecki', 57, NULL, 1, 52)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Lisaj', 58, NULL, 1, 53)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Łopyta', 59, NULL, 1, 54)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Maksymiec', 60, NULL, 1, 55)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Marzec', 61, NULL, 1, 56)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zbigniew', 62, NULL, 1, 57)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Medyna', 63, NULL, 1, 58)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Monieta', 64, NULL, 1, 59)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Montwiłł', 65, NULL, 1, 60)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Nicewicz', 66, NULL, 1, 61)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Nienartowicz', 67, NULL, 1, 62)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Nowak', 68, NULL, 1, 63)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Nowakowski', 69, NULL, 1, 64)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Pawlak', 70, NULL, 1, 65)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Piasecka', 71, NULL, 1, 66)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Pietrzykowski', 72, NULL, 1, 67)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Pleskacz', 73, NULL, 1, 68)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Plucińska', 74, NULL, 1, 69)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Podlecka', 75, NULL, 1, 70)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kubiak', 76, NULL, 1, 71)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Puszcz', 77, NULL, 1, 72)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Rutkowski', 78, NULL, 1, 73)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Rybak', 79, NULL, 1, 74)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Smulska', 80, NULL, 1, 75)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Stefaniak', 81, NULL, 1, 76)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Stefanowski', 82, NULL, 1, 77)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Stojek', 83, NULL, 1, 78)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Szozda', 84, NULL, 1, 79)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Szubtarska-Nyczka', 85, NULL, 1, 80)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Szymczak', 86, NULL, 1, 81)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Ślączka', 87, NULL, 1, 82)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Świtalska', 88, NULL, 1, 83)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Tatko', 89, NULL, 1, 84)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Tomaszkiewicz', 90, NULL, 1, 85)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Treichel', 91, NULL, 1, 86)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Tuleja', 92, NULL, 1, 87)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Uriasz', 93, NULL, 1, 88)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Warlikowski', 94, NULL, 1, 89)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Waszczuk', 95, NULL, 1, 90)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Winnicki', 96, NULL, 1, 91)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Woś', 97, NULL, 1, 92)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Wysocka', 98, NULL, 1, 93)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zalewski', 99, NULL, 1, 94)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zdziarska', 100, NULL, 1, 95)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zieliński', 101, NULL, 1, 96)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zieziula', 102, NULL, 1, 97)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Zubel', 103, NULL, 1, 98)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Łowicka', 104, NULL, 1, 99)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chrzanowski', 105, NULL, 1, 100)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kosińska', 106, NULL, 1, 101)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Matuszak', 107, NULL, 1, 102)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Bober', 108, NULL, 1, 103)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Cepowski', 109, NULL, 1, 104)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Chomski', 110, NULL, 1, 105)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Kozłowski', 111, NULL, 1, 106)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Wiśniewski', 112, NULL, 1, 107)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Bierdek', 113, NULL, 1, 108)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Szczepanek', 114, NULL, 1, 109)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Wiese', 115, NULL, 1, 110)
GO
INSERT [dsd_ams_asset_responsible_person] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'0.0', N'Christowa', 116, NULL, 1, 111)
GO
SET IDENTITY_INSERT [dsd_ams_asset_responsible_person] OFF
GO
