alter VIEW [dbo].[dsg_ams_asset_rec_sys_op_type_view_LTT] AS
	SELECT
		TYP_SYSEWI_ID AS id,
		TYP_SYSEWI_IDN AS cn,
		TYP_SYSEWI_IDN AS title,
		null AS refValue,
		TYP_SYSEWI_ID as centrum,
		available as available
	FROM dsg_ams_asset_rec_sys_op_type where typ_ope_idn='LTT'

alter VIEW [dbo].[dsg_ams_asset_rec_sys_op_type_view_PSW] AS
	SELECT
		TYP_SYSEWI_ID AS id,
		TYP_SYSEWI_IDN AS cn,
		TYP_SYSEWI_IDN AS title,
		null AS refValue,
		TYP_SYSEWI_ID as centrum,
		available as available
	FROM dsg_ams_asset_rec_sys_op_type where typ_ope_idn='PSW'

