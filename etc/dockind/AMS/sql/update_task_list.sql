DECLARE @documentId numeric(19,0)
DECLARE ids cursor for (select document_id from DSG_ams_FAKT_ZAKUP)
	open ids
		while @@FETCH_STATUS = 0
			begin
			FETCH NEXT FROM ids INTO @documentId

				update DSW_JBPM_TASKLIST 
				set dockind_business_atr_3 = (select nr_faktury from DSG_ams_FAKT_ZAKUP where document_id = @documentId) where DSW_JBPM_TASKLIST.document_id = @documentId;

			end
	CLOSE ids
DEALLOCATE ids
GO