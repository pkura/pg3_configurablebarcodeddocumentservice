create view dsg_ams_view_budget_phase_no_ref as (
select
budzet_id as id,
budzet_idm as cn,
nazwa as title,
1 as available,
null as centrum,
null as refValue
from dsg_ams_budget_to_division
group by budzet_id,budzet_idm,nazwa
)

create view dsg_ams_view_budget_unit_no_ref as
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
1 as available,
bd_str_budzet_id as centrum,
b.budzet_id as refValue
from
(select budzet_id from dsg_ams_budget_to_division group by budzet_id) b
join [dsg_ams_kb2ko] ko on b.budzet_id=ko.budzet_id