ALTER view [dbo].[dsg_ams_view_budget_unit_ko] as (
select
budget_ko.erp_id as id,
budget_ko.idm as cn,
budget_ko.nazwa as title,
budget_ko.budzet_id as centrum,
ko.id as refValue,
budget_ko.available*(1-czy_agregat) as available
from dsg_ams_unit_budget_ko budget_ko
join dsg_ams_budget_unit_org_unit ko on budget_ko.budzet_id=ko.budzet_id and budget_ko.bd_str_budzet_id=ko.bd_str_budzet_id
union all
select
budget_ko.erp_id as id,
budget_ko.idm as cn,
budget_ko.nazwa as title,
budget_ko.budzet_id as centrum,
ko2.id as refValue,
budget_ko.available*(1-czy_agregat) as available
from dsg_ams_unit_budget_ko budget_ko
join dsg_ams_budget_unit_org_unit ko on budget_ko.budzet_id=ko.budzet_id and budget_ko.bd_str_budzet_id=ko.bd_str_budzet_id and ko.bd_str_budzet_nad_idn='PROJEKTY'
join dsg_ams_budget_unit_org_unit ko2 on ko.budzet_id=ko2.budzet_id and ko.bd_str_budzet_nad_id=ko2.bd_str_budzet_id
)