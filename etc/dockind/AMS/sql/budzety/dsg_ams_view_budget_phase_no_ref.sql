ALTER view [dbo].[dsg_ams_view_budget_phase_no_ref] 
with schemabinding
as
select
erp_id as id,
idm as cn,
nazwa as title,
available as available,
null as centrum,
null as refValue
from dbo.dsg_ams_unit_budget