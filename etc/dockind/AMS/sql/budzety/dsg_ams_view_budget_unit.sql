ALTER view [dbo].[dsg_ams_view_budget_unit] as
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
ko.available as available,
bd_str_budzet_id as centrum,
b.id as refValue
from dsg_ams_view_budget_phase b
join dsg_ams_budget_unit_org_unit ko on RIGHT(b.id,3)=ko.budzet_id and b.centrum=ko.komorka_id
where ko.bd_str_budzet_idn not like '% SUMA'
union all 
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
ko.available as available,
bd_str_budzet_id as centrum,
b.id as refValue
from dsg_ams_view_budget_phase b
join dsg_ams_budget_unit_org_unit ko on RIGHT(b.id,3)=ko.budzet_id and ko.bd_str_budzet_idn ='PROJEKTY'