ALTER view [dbo].[dsg_ams_view_budget_phase] 
with schemabinding
as
select
CAST(CONCAT(d.id,RIGHT(CAST(1000+b.erp_Id as varchar(4)), 3)) as int) as id,
b.idm as cn,
nazwa as title,
b.available*(1-d.hidden) as available,
FLOOR(d.externalid) as centrum,
d.id as refValue
from dbo.dsg_ams_unit_budget b
left outer join dbo.ds_division d on d.externalid is not null