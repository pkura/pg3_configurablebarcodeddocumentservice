ALTER view [dbo].[dsg_ams_view_budget_unit_no_ref] as
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
ko.available as available,
bd_str_budzet_id as centrum,
b.erp_id as refValue
from dsg_ams_unit_budget b
join dsg_ams_budget_unit_org_unit ko on b.erp_id=ko.budzet_id
where ko.bd_str_budzet_idn not like '% SUMA'
union all 
select
ko.id as id,
ko.bd_str_budzet_idn as cn,
ko.nazwa_komorki_budzetowej as title,
ko.available as available,
bd_str_budzet_id as centrum,
b.erp_id as refValue
from dsg_ams_unit_budget b
join dsg_ams_budget_unit_org_unit ko on b.erp_id=ko.budzet_id and ko.bd_str_budzet_idn ='PROJEKTY'