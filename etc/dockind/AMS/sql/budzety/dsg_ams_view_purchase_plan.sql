ALTER view [dbo].[dsg_ams_view_purchase_plan] as (
select
p.erp_id as id,
p.idm as cn,
p.nazwa as title,
p.budzet_id as centrum,
ko.id as refValue,
p.available*(1-czy_agregat) as available
from dsg_ams_purchase_plan p
join dsg_ams_budget_unit_org_unit ko on p.budzet_id=ko.budzet_id and p.bd_str_budzet_id=ko.bd_str_budzet_id
union all
select
p.erp_id as id,
p.idm as cn,
p.nazwa as title,
p.budzet_id as centrum,
ko2.id as refValue,
p.available*(1-czy_agregat) as available
from dsg_ams_purchase_plan p
join dsg_ams_budget_unit_org_unit ko on p.budzet_id=ko.budzet_id and p.bd_str_budzet_id=ko.bd_str_budzet_id and ko.bd_str_budzet_nad_idn='PROJEKTY'
join dsg_ams_budget_unit_org_unit ko2 on ko.budzet_id=ko2.budzet_id and ko.bd_str_budzet_nad_id=ko2.bd_str_budzet_id
)