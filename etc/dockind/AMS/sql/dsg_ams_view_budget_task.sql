	ALTER VIEW [dbo].[dsg_ams_view_budget_task] AS
	SELECT
		id,
		convert(varchar(50), id) AS cn,
		CASE
			WHEN zadanie_nazwa IS NULL
				THEN 'Nie dotyczy'
			WHEN zadanie_nazwa IS NOT NULL
				THEN RTRIM(LTRIM(bd_zadanie_idn)) + ' ' + RTRIM(LTRIM(zadanie_nazwa)) +  ' (pozosta�y limit (kwota): ' + CASE WHEN ISNULL(zadanie_p_koszt,0) - ISNULL(zadanie_rez_koszt,0) - ISNULL(zadanie_r_koszt,0) <= 0 THEN 'brak �rodk�w' ELSE CONVERT(varchar(50),ISNULL(zadanie_p_koszt,0) - ISNULL(zadanie_rez_koszt,0) - ISNULL(zadanie_r_koszt,0)) END + ')'
		END 
		AS title,
		NULL AS centrum,
		convert(numeric(18, 0), bd_budzet_rodzaj_id) AS refValue,
		1 as available
	FROM dsg_ams_budget_copy

GO

