CREATE TABLE dsg_ams_supplier_bank_account (
	id numeric(18, 0) identity(1,1) NOT NULL,
	id_from_webservice numeric(18, 2),
	czy_aktywne numeric(18, 2),
	identyfikator_num numeric(18, 2),
	idm varchar(50),
	kolejnosc_uzycia numeric(18, 0),
	numer_konta varchar(50),
	symbol_waluty varchar(10),
	available bit
)

CREATE VIEW dsg_ams_view_supplier_bank_account AS
	SELECT
		acc.id AS id,
		acc.id_from_webservice AS cn,
		acc.numer_konta AS title,
		acc.id_from_webservice AS centrum,
		acc.available AS available,
		sup.id AS refValue
	FROM dsg_ams_supplier sup
	JOIN dsg_ams_supplier_bank_account acc ON sup.erpId = acc.identyfikator_num;