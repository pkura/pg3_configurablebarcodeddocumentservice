 CREATE TABLE dsg_ams_employee (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 2),
	identyfikator_num numeric(18, 2),
	idm numeric(18, 0),
	imie varchar(50),
	nazwisko varchar(50),
	komorka_org varchar(50),
	nip varchar(50),
	pesel varchar(50)
)


 CREATE VIEW dsg_ams_view_employee AS
	SELECT 
		idm AS id,
		idm AS cn,
		REPLACE(nazwisko, ' ', '') + ' ' + REPLACE(imie, ' ', '') AS title,
		NULL AS centrum,
		NULL AS refValue,
		1 AS available
	FROM dsg_ams_employee