drop table dsg_ams_unit_budget;

CREATE TABLE dsg_ams_unit_budget (
    ID BIGINT IDENTITY(1,1) NOT NULL,
    ERP_ID bigint NULL,
    IDM VARCHAR(50) NULL,
    NAZWA VARCHAR(100) NULL,
    CZY_ARCHIWALNY REAL NULL,
    DATA_OB_DO datetime NULL,
    DATA_OB_OD datetime NULL,
    WERSJA integer NULL,
    available bit default(1) not null
)
ALTER TABLE dsg_ams_unit_budget
ADD CONSTRAINT PK_dsg_ams_unit_budget PRIMARY KEY(ID);

CREATE INDEX IDX_dsg_ams_unit_budget ON dsg_ams_unit_budget(ERP_ID);

CREATE VIEW dsg_ams_unit_budget_view AS
	SELECT
		ID AS id,
		IDM AS cn,
		NAZWA AS title,
		null AS refValue,
		null as centrum,
		available as available
	FROM dsg_ams_asset_operation_type
	
