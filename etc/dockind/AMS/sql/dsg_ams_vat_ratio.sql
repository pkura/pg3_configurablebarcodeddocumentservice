CREATE TABLE dsg_ams_vat_ratio (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 2),
	komorka_id numeric(18, 2),
	okrrozl_id numeric(18, 2),
	rok numeric(18, 0),
	wartosc numeric(18, 2),
)