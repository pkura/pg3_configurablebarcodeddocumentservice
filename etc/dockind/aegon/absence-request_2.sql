alter table ds_employee_card add FIRST_NAME varchar(65);
alter table ds_employee_card add LAST_NAME varchar(65);

DECLARE @login varchar(62)
DECLARE @firstName varchar(62)
DECLARE @lastName varchar(62)
DECLARE logins cursor for (select [USER_ID] from DS_EMPLOYEE_CARD)
OPEN logins 
WHILE @@FETCH_STATUS = 0
	BEGIN
		FETCH NEXT FROM logins INTO @login
		
		SELECT @lastName = null;
		SELECT @firstName = null;
		SELECT @firstName = u.FIRSTNAME, @lastName = u.LASTNAME FROM DS_USER u WHERE u.NAME = @login;
		
		UPDATE DS_EMPLOYEE_CARD SET FIRST_NAME = @firstName, LAST_NAME = @lastName WHERE [USER_ID] = @login;
	END
CLOSE logins
DEALLOCATE logins
GO