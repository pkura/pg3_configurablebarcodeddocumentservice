CREATE TABLE [DS_CALENDAR_FREE_DAY]
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DAY_DATE] [datetime] NOT NULL,
	[INFO] [text] NULL
);