CREATE TABLE dsg_kp_dzial
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_kp_departament
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_kp_stanowisko
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

alter table dsg_absence_request add kp_dzial numeric(18,0);
alter table dsg_absence_request add kp_departament numeric(18,0);
alter table dsg_absence_request add kp_stanowisko numeric(18,0);

alter table dsg_kp_dzial alter column cn varchar(100);
alter table dsg_kp_departament alter column cn varchar(100);
alter table dsg_kp_stanowisko alter column cn varchar(100);