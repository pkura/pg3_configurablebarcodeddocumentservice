DECLARE ids CURSOR FOR SELECT d.ID, id.original FROM DS_DOCUMENT d INNER JOIN DSG_DOCTYPE_1 n ON d.ID = n.DOCUMENT_ID LEFT JOIN DSO_IN_DOCUMENT id ON id.ID = d.ID;
DECLARE @currentId AS NUMERIC (18,0), @original AS smallint;
OPEN ids
FETCH NEXT FROM ids INTO @currentId, @original
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @original = 1
		UPDATE DSG_DOCTYPE_1 SET document_form = 0 WHERE DOCUMENT_ID = @currentId --oryginal
	ELSE
		UPDATE DSG_DOCTYPE_1 SET document_form = 1 WHERE DOCUMENT_ID = @currentId --kopia
		
	FETCH NEXT FROM ids INTO @currentId, @original
END
CLOSE ids
DEALLOCATE ids