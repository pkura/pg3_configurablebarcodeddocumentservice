DECLARE @lastID AS numeric(18,0), @login AS varchar(62);
DECLARE logins cursor for (select e.USER_ID FROM DS_EMPLOYEE_CARD e WHERE e.USER_ID IN ('adamczt', 'banachm', 'bialonm', 'czotyri', 'faronp ', 
	'filipid', 'klaczaw', 'osadnia', 'pantolm', 'paradas', 'pietrzw', 'przysza', 'stawict', 'widlint')
	GROUP BY e.USER_ID);
OPEN logins;
FETCH NEXT FROM logins INTO @login;
WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @login;
		SELECT TOP 1 @lastID = e.ID FROM DS_EMPLOYEE_CARD e WHERE e.USER_ID = @login ORDER BY e.ID DESC;
		
		INSERT INTO DS_ABSENCE (ABSENCE_TYPE, CTIME, DAYS_NUM, DOCUMENT_ID, EDITOR_NAME, EMPLOYEE_ID, END_DATE, 
						        INFO, MTIME, nr_zestawienia, START_DATE, USER_ID, YEAR) 
			SELECT a.ABSENCE_TYPE, a.CTIME, a.DAYS_NUM, a.DOCUMENT_ID, a.EDITOR_NAME, @lastID, a.END_DATE, 
				   a.INFO, a.MTIME, a.nr_zestawienia, a.START_DATE, a.USER_ID, a.ID
			FROM DS_ABSENCE a 
			WHERE 
				a.EMPLOYEE_ID = (SELECT TOP 1 e.ID FROM DS_EMPLOYEE_CARD e WHERE 
				e.ID < @lastID AND e.EMPLOYMENT_END_DATE IS NULL ORDER BY e.ID DESC)
				AND a.ABSENCE_TYPE NOT LIKE('WU%');
				
			FETCH NEXT FROM logins INTO @login;
	END
CLOSE logins
DEALLOCATE logins
GO