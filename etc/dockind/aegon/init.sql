CREATE TABLE [dsg_absence_request](
	[document_id] [numeric](19, 0) NOT NULL,
	[period_from] [datetime] NULL,
	[period_to] [datetime] NULL,
	[description] [text] NULL,
	[position] [varchar](50) NULL,
	[status] [int] NULL,
	[kind] [varchar](150) NULL,
	[working_days] [int] NULL,
	[available_days] [int] NULL,
	[remark] [varchar](500) NULL,
	[akceptacja_finalna] [tinyint] NULL,
	[username] [varchar](75) NULL,
	PRIMARY KEY ([document_id])
);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUN', 'Wymiar urlopu nale�ny', NULL, 2);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUZ', 'Wymiar urlopu zaleg�y', NULL, 2);


INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UW', 'Urlop wypoczynkowy', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('U�', 'Urlop na ��danie', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('188', 'Opieka nad dzieckiem 188 Kp', NULL, 0);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('OG', 'Odbi�r godzin', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UOK', 'Urlop okoliczno�ciowy', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UB', 'Urlop bezp�atny', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('INW', 'Urlop inwalidzki', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UM', 'Urlop macierzy�ski', NULL, 0);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UMD', 'Urlop macierzy�ski dodatkowy', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UJ', 'Urlop ojcowski', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NN', 'Nieobecno�� nieusprawiedliwiona', NULL, 0);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NUN', 'Nieobecno�� usprawiedliwiona niep�atna', NULL, 0);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NUP', 'Nieobecno�� usprawiedliwiona p�atna', NULL, 0);
     
update ds_absence_type set flag = 2 where code = 'INW';

insert into ds_division(GUID,NAME,DIVISIONTYPE,PARENT_ID,hidden) VALUES('AR_PERMISSIONS','WNIOSEK_UPRAWNIENIA','group',1,1);

-- dodanie pola oznaczajacego czy dodano wpis urlopowy
alter table dsg_absence_request add abs_charged tinyint;

-- dodanie kolumny EDITOR_NAME dla tabeli ds_absence 
alter table ds_absence add EDITOR_NAME varchar(75);

-- dodanie kolumny author do tabeli dsg_absence_request
alter table dsg_absence_request add author varchar(65);

-- dodanie kolumny okreslajacej czy wnioskodawca zlozyl wniosek na systemie zdalnym
alter table dsg_absence_request add remote_system tinyint;
