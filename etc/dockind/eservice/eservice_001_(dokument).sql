create table dsg_eservice
(
	[DOCUMENT_ID] numeric(19, 0) NOT NULL PRIMARY KEY,
	[klasa]       	smallint,
	[typ]         	smallint,
	[nr_umowy]		varchar(20),
	[klient]	numeric(18,0),
	[status] 		smallint
);


insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('ESERVICE_READ','eService - odczyt',null,'group',1,0);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('ESERVICE_MODIFY','eService - zapis',null,'group',1,0);

alter table dsg_eservice add  NR_SPRAWY varchar(50);
alter table dsg_eservice add  BARCODE varchar(50);
alter table dsg_eservice add  NR_POS_ID varchar(50);
alter table dsg_eservice add  ZAMAWIAJACY varchar(50); 
alter table dsg_eservice add  STATUS_ZAMOWIENIA integer;


CREATE TABLE dsg_STATUS_ZAMOWIENIA
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('ORDER_MANAGER', 'Zarządzanie zamówieniami','group',1,0);


alter table dsg_eservice add  NR_SPRAWY varchar(50);
alter table dsg_eservice add  BARCODE varchar(50);
alter table dsg_eservice add  NR_POS_ID varchar(50);>>>>>>> .merge-right.r697
