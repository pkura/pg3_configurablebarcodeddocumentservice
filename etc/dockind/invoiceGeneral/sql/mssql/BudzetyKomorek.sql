  CREATE TABLE [dbo].[dsg_budzetyKO_komorek](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[idm] [varchar](50) NULL,
	[nazwa] [varchar](255) NULL,
	[okres_id] [numeric](18, 0) NULL,
	[okres_idm] [varchar](50) NULL,
	[budzet_id] [numeric](18, 0) NULL,
	[budzet_idn] [varchar](50) NULL,
	[erpId] numeric(18,0) NULL,
	[czyAgregat] bit NULL
) ON [PRIMARY]

  create view dsg_invoice_general_budzet as 
  select budzet_id as id, budzet_id as cn, nazwa as title, okres_id as refValue, null as centrum, available from  dsg_budzetyKO_komorek
  
   -- create view [dbo].[dsg_budzetyKO_komorek_view] as 
 -- select budzet_id as id, idm as cn, nazwa as title, okres_id as refValue, null as centrum, 1 as available from  dsg_budzetyKO_komorek
  --do zamowienia publicznego
  --budzetyDoKomorki.sql najpierw
CREATE VIEW [dbo].[dsg_budzetyKO_komorek_tylko_kom]
AS
SELECT        bud.erpId AS id, bud.idm AS cn, { fn CONCAT(bud.budzet_idn, { fn CONCAT(' : ', bdk.nazwaKomorkiBudzetowej) }) } AS title, bud.okres_id AS refValue, NULL
                         AS centrum, bud.available
FROM            dbo.dsg_budzetyKO_komorek AS bud LEFT OUTER JOIN
                         dbo.dsg_budzetDokomorki AS bdk ON bud.budzet_id = bdk.budzet_id

create view dsg_budzetyKO_komorek_tylko_budzet as
SELECT        budzet_id AS id, budzet_idn AS cn, budzet_idn AS title, budzet_id AS refValue, NULL AS centrum, available
FROM            dbo.dsg_budzetyKO_komorek

alter table dsg_budzetyKO_komorek add available bit