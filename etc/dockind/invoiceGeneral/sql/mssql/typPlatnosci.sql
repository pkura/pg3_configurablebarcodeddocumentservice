CREATE TABLE [dbo].[dsg_payment_terms](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

--typ platnosci view 
create view [dbo].[simple_erp_per_docusafe_warpla_view] AS
SELECT DISTINCT id, cn, title, centrum, refValue, available
FROM            dbo.dsg_payment_terms where refValue='Zakup'