create table dsg_szablony_rozl_faktury(
id numeric(18,0) identity NOT NULL,
erpId numeric(18,0) NULL,
idm varchar(255) NULL,
nazwa varchar(400) NULL,
okres_rozl varchar(255) NULL,
okres_id numeric(18,0) NULL
)

create view dsg_szablony_rozl_faktury_view as
  SELECT        erpId AS id, CONCAT(okres_rozl, ':', idm) AS cn,  CONCAT(okres_rozl, ':', idm) AS title, okres_id AS refValue, NULL AS centrum, 1 AS available
FROM            dbo.dsg_szablony_rozl_faktury