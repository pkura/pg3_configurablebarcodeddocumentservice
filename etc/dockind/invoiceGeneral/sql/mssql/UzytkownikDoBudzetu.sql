CREATE VIEW [dbo].[INVOICE_GENERAL_UZYTKOWNIK_WYP_DEKRET]
AS
SELECT        b.id, users.NAME AS cn, users.FIRSTNAME + ' ' + users.LASTNAME AS title, p.erpId AS refvalue, NULL AS centrum, 1 AS available
FROM            dbo.DS_USER AS users LEFT OUTER JOIN
                         dbo.dsg_role_in_budget AS b ON b.pracownik_nrewid = users.pracow_nrewid_ERP LEFT OUTER JOIN
                         dbo.dsg_budzetyKO_komorek AS p ON p.erpId = b.bd_budzet_ko_id
WHERE        (b.bp_rola_idn = 'Spec. ds. a-f')

GO