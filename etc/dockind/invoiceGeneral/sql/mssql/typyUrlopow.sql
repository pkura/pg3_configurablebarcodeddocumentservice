CREATE TABLE dsg_typy_urlopow(
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	idm varchar(50) NULL,
	opis varchar(40) NULL,
	opis_dlugi varchar(60) NULL,
	erpId numeric(18,0)  NOT NULL
) ON [PRIMARY]

CREATE VIEW [dbo].[v_typy_urlopow]
AS
SELECT        id, idm AS CN, opis_dlugi AS TITLE, 1 AS available, NULL AS refValue, NULL AS centrum
FROM            dbo.dsg_typy_urlopow

GO
