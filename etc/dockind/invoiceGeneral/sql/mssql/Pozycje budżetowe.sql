create TABLE dbo.DSG_INVOICE_GENERAL_MPK(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[OKRESBUDZET] numeric (19,0) NULL,
	[BUDZETID] numeric (19,0) NULL,
	[POZYCJAID] numeric (19,0) NULL,
	[PROJEKT] numeric (19,0) NULL,
	[BUDZET_PROJ] numeric (19,0) NULL,
	[ETAP] numeric (19,0) NULL,
	[ZASOB] numeric (19,0) NULL,
	[KWOTANETTO] numeric (19,4) NULL,
	[KWOTAVAT] numeric (19,4) NULL,
	[DYSPONENT] numeric (19,0) NULL,
	[AKCEPTACJA] numeric(19,0) NULL
	)


CREATE VIEW [dbo].[POZYCJE_FAKTURY_BUDZETY]
AS
SELECT        poz.id, CAST(poz.id AS varchar(50)) AS cn, CAST(poz.id AS varchar(255)) AS title, 0 AS centrum, CAST(dic.DOCUMENT_ID AS varchar(20)) AS refValue, 
                         1 AS available
FROM            dbo.dsg_invoice_general_multiple AS dic INNER JOIN
                         dbo.DSG_INVOICE_GENERAL_MPK AS poz ON dic.FIELD_CN = 'MPK' AND poz.id = dic.FIELD_VAL

--grupy dysponent�w musz� si� nazywa� 'Dysponenci'
Create view dbo.INVOICE_GENERAL_DYSPONENCI 
AS
SELECT        users.ID, users.NAME AS cn, users.FIRSTNAME + ' ' + users.LASTNAME AS title, p.erpId AS refvalue, NULL AS centrum, b.available
FROM            dbo.DS_USER AS users LEFT OUTER JOIN
                         dbo.dsg_role_in_budget AS b ON b.pracownik_nrewid = users.pracow_nrewid_ERP LEFT OUTER JOIN
                         dbo.dsg_budzetyKO_komorek AS p ON p.erpId = b.bd_budzet_ko_id
WHERE        (b.bp_rola_idn = 'Dysponent')

--potrzebne projekty.sql i budzety.sql i budzetyKomorek i BudzetPozycje

--budzety
create VIEW dbo.dsg_invoice_general_budzet AS
SELECT        erpId AS id, erpId AS cn, nazwa AS title, okres_id AS refValue, NULL AS centrum, available
FROM            dbo.dsg_budzetyKO_komorek
--pozycja budzetu
create view dsg_invoice_general_budzet_poz as
select erpId as id, erpId as cn,nazwa as title, parent_id as refValue, available, null as centrum from dsg_invoice_general_budzety_pozycja_simple


alter table DSG_INVOICE_GENERAL_MPK add STAWKA_BUDZET  numeric (19,0)
alter table DSG_INVOICE_GENERAL_MPK add DATA_AKCEPT_DYSP [date] NULL
alter table DSG_INVOICE_GENERAL_MPK add DYSPONENT_PROJ numeric (19,0) NULL
 
