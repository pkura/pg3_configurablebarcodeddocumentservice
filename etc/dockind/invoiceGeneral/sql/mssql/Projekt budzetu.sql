CREATE TABLE dbo.dsg_projekt_budzetu (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 0),
	idm varchar(50),
	nazwa varchar(255),
	p_datako datetime,
	p_datapo datetime,
	parent varchar(255),
	parent_id numeric(18,0),
	parent_idm varchar(50),
	stan bit,
	status bit
)

alter table dsg_projekt_budzetu add available bit
 
create view dbo.simple_erp_per_budzet_projektu_view as
SELECT        erpId as id, idm AS cn, nazwa AS title, parent_id AS refValue, available, NULL AS centrum
FROM            dbo.dsg_projekt_budzetu

