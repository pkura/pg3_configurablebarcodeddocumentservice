CREATE TABLE dbo.dsg_projekt_budzet_etap (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 0),
	nazwa varchar(255),
	nrpoz int,
	p_datako datetime,
	p_datapo datetime,
	parent varchar(255),
	parent_id numeric(18,0),
	parent_idm varchar(50)
)

create view dbo.simple_erp_per_etap_projektu as
SELECT        erpId AS id, CONVERT(varchar(50), erpId) AS cn, nazwa AS title, parent_id AS refvalue, NULL AS centrum, available
FROM            dbo.dsg_projekt_budzet_etap

alter table dsg_projekt_budzet_etap add available bit