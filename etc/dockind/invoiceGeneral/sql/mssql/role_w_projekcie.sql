CREATE TABLE dsg_role_in_project (
	id numeric(18, 0) identity(1,1) NOT NULL,
	bp_rola_id numeric(18, 2),
	bp_rola_idn varchar(50),
	bp_rola_kontrakt_id numeric(18, 2),
	czy_blokada_prac bit,
	czy_blokada_roli bit,
	kontrakt_id numeric(18, 2),
	kontrakt_idm varchar(50),
	pracownik_id numeric(18, 2),
	pracownik_idn varchar(50),
	pracownik_nrewid int,
	osobaGuid varchar(150),
	osoba_id numeric(18,0)
)


alter table dsg_role_in_project add available bit

create view INVOICE_GENERAL_DYSPON_PROJ as
  SELECT        users.ID, users.NAME AS cn, users.FIRSTNAME + ' ' + users.LASTNAME AS title, p.erpId AS refvalue, NULL AS centrum, b.available
FROM            dbo.DS_USER AS users LEFT OUTER JOIN
                         dbo.dsg_role_in_project AS b ON b.pracownik_nrewid = users.pracow_nrewid_ERP LEFT OUTER JOIN
                         dbo.dsg_projekty AS p ON p.erpId = b.kontrakt_id
WHERE        (b.bp_rola_idn = 'Dysponent')

    


