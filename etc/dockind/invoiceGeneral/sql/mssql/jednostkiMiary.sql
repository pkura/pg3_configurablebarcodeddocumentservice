create table dbo.dsg_jednostki_miary (
    id bigint identity(1,1) not null,
    available int null,
	centrum int NULL,
	refValue varchar(20) NULL,
	cn varchar(50) null, 
    title varchar(200) null,
	jmtyp_idn varchar(50) null,
	precyzjajm int null,
    erpId numeric(18,0) null )

CREATE VIEW [dbo].[simple_erp_per_docusafe_jm_view]
AS
SELECT        id, cn, title, refValue, CENTRUM, available, precyzjajm, erpId, jmtyp_idn
FROM            dbo.dsg_jednostki_miary



