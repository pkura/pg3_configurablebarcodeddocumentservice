create table dbo.dsg_product (
    id bigint identity(1,1) not null,
    available int null,
	centrum int NULL,
	refValue varchar(20) NULL,
	cn varchar(50) null, 
    jmIdn varchar(50) null,
    klasyWytwIdn varchar(50) null,
    title varchar(200) null,
    rodzTowaru numeric(18,0) null,
    vatStawIds varchar(50) null,
    wytworId numeric(18,0) null )


create view [dbo].[simple_erp_per_docusafe_wytwory_zapdost_view]
AS
SELECT       id,cn,RTRIM(cn) +' : '+title as title, available,centrum,refvalue, jmIdn, klasyWytwIdn, rodzTowaru, vatStawIds, wytworId
FROM    dbo.dsg_product