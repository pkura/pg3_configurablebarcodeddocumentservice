USE [utp]
GO

/****** Object:  Table [dbo].[DS_UTP_ZAMPUB_POZ_BUD]    Script Date: 2014-03-27 12:45:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dsg_invoice_general_poz_zam](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[POZYCJAID_ZAM] [numeric](18, 0) NULL,
	[DOCUMENT_ID_ZAMOWIENIA] [numeric](18, 0) NULL,
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[ZAMPOZ_ID] [numeric](18,0) NULL,
	[OKRES_BUDZETOWY_ZAM] [numeric](18, 0) NULL,
	[KOMORKA_BUDZETOWA_ZAM] [numeric](18, 0) NULL,
	[IDENTYFIKATOR_PLANU_ZAM] [numeric](18, 0) NULL,
	[IDENTYFIKATOR_BUDZETU_ZAM] [numeric](18, 0) NULL,
	[POZYCJA_PLANU_ZAKUPOW_ZAM] [varchar](256) NULL,
	[POZYCJA_BUDZETU_ZAM] [varchar](256) NULL,
	[PROJEKT_ZAM] [numeric](18, 0) NULL,
	[PRODUKT_ZAM] [numeric](18, 0) NULL,
	[OPIS_ZAM] [varchar](250) NULL,
	[ILOSC_ZAM] [numeric](18, 4) NULL,
	[ILOSC_ZREAL_ZAM] [numeric](18, 4) NULL,
	[JM_ZAM] [numeric](18, 0) NULL,
	[STAWKA_VAT_ZAM] [numeric](18, 0) NULL,
	[NETTO_ZAM] [decimal](18, 4) NULL,
	[BRUTTO_ZAM] [decimal](18, 4) NULL,
	[UWAGI_ZAM] [varchar](250) NULL
) ON [PRIMARY]

GO
CREATE TABLE dsg_invoice_supplier_order_header (
	id numeric(18, 0) identity(1,1) NOT NULL,
	dat_dok datetime,
	dat_odb datetime,
	dostawca_id numeric(18, 2),
	dostawca_idn varchar(100),
	erpId numeric(18, 2),
	idm varchar(50),
	opis_wewn varchar(150),
	uwagi varchar(150),
	wartdok numeric(18, 2)
)


create view dsg_invoice_general_zamowienia as
SELECT        CONVERT(numeric(18, 0), id) AS id, CONVERT(numeric(18, 0), erpId) AS cn, idm AS title, CONVERT(numeric(18, 0), dostawca_id) AS refValue, NULL AS centrum, 
                         available
FROM            dbo.dsg_invoice_supplier_order_header

--pozycje z simpla
create table dsg_invoice_supplier_order_position
(
id numeric(18,0) identity,
supplier_order_id numeric(18,0),
supplier_order_idm varchar(100),
wytwor_idm varchar (100),
ilosc  numeric(18,4),
nrpoz integer,
cena numeric(18,4),
cenabrutto  numeric(18,4),
koszt  numeric(18,4),
kwotnett  numeric(18,4),
kwotvat  numeric(18,4)
);

alter table  dsg_invoice_supplier_order_position add zamdospoz_id numeric(18,0);


ALTER TABLE dsg_invoice_supplier_order_position ADD budzet_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD budzet_idm varchar(100);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_budzet_ko_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_budzet_ko_idm varchar(100);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_rodzaj_ko_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_plan_zam_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_plan_zam_idm varchar(100);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_rodzaj_plan_zam_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_rezerwacja_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_rezerwacja_idm varchar(100);
ALTER TABLE dsg_invoice_supplier_order_position ADD bd_rezerwacja_poz_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD kontrakt_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD projekt_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD etap_id numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD zasob_id numeric(18, 0);

ALTER TABLE dsg_invoice_supplier_order_position ADD vatStawId numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD walutaId numeric(18, 0);
ALTER TABLE dsg_invoice_supplier_order_position ADD iloscZreal numeric(18, 4);
ALTER TABLE dsg_invoice_supplier_order_position ADD komorkaOrganizacyjaERPId numeric(18, 0);

alter table dsg_invoice_supplier_order_header add available bit;


