CREATE TABLE [dbo].[dsg_document_types](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[available] [int] NOT NULL,
	[czywal] [bit] NULL,
	[symbol_waluty] [varchar](128) NULL,
	[nazwa] [varchar](128) NULL,
	[typdok_idn] [varchar](128) NULL,
	[typdok_ids] [varchar](128) NULL,
	[typdokzak_id] [numeric](19, 0) NULL
) ON [PRIMARY]

create view [dbo].[simple_erp_per_docusafe_typdokzak_pln_view] AS
SELECT        typdokzak_id as id, typdok_idn AS cn,RTRIM(typdok_idn)+': '+ nazwa AS title, '0' AS CENTRUM, NULL AS refValue,  available
FROM            dbo.dsg_document_types
WHERE        (czywal = 0)

create view [dbo].[simple_erp_per_docusafe_typdokzak_wal_view] AS
SELECT        typdokzak_id as id, typdok_idn AS cn,RTRIM(typdok_idn)+': '+ nazwa AS title, '0' AS CENTRUM, NULL AS refValue,  available
FROM            dbo.dsg_document_types
WHERE        (czywal = 1)

CREATE VIEW [dbo].[simple_erp_per_docusafe_typDokumentu_view]
AS
SELECT        typdokzak_id AS id, typdok_idn AS cn,RTRIM(typdok_idn)+': '+ nazwa AS title, '0' AS CENTRUM, NULL AS refValue, available
FROM            dbo.dsg_document_types