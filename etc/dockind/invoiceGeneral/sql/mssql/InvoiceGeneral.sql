create TABLE [dbo].[dsg_invoice_general](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[data_wplywu] [date] NULL,
	[adresat] [numeric](18, 0) NULL,
	[netto] [numeric](18, 2) NULL,
	[brutto] [numeric](18, 2) NULL,
	[vat] [numeric](18, 2) NULL,
	[waluta] [numeric](18, 0) NULL,
	[termin_platnosci] [date] NULL,
	[faktura_skan] [numeric](18, 0) NULL,
	[RODZAJFAKTURY] [numeric](18, 0) NULL,
	[FAKTKOSZTTRWALE] [bit] NULL,

	[FAKTNAWYDZIALE] [bit] NULL,
	[PELNOMOCNIK] [numeric](19, 0) NULL,
	[numer_faktury] [varchar](30) NULL,
	[BARCODE] [varchar](30) NULL,
	[wystawca_faktury] [numeric](18, 0) NULL,
	[data_wystawienia] [date] NULL,
	
	[kwotaWwalucie] [numeric](18, 2) NULL,
	[kurs] [numeric](18, 4) NULL,
	[typPlatnosci] [numeric](18, 0) NULL,
	[nazwaTowUsl] [varchar](4096) NULL,
	[DODATKOWEINFO] [varchar](4096) NULL,
	[WYDATEKSTRUKT] [bit] NULL,
	[WYDATKISTRUKTENUM] [numeric](18, 0) NULL,
	[SKIERUJUZUPOPIS] [bit] NULL,
	[OSOBAUZUPOPIS] [numeric](19, 0) NULL,
	[NUMERPOSTPRZETARG] [varchar](50) NULL,
	[TRYBPOSTEP] [numeric](18, 0) NULL,
	[WynikaZPostepPrzetarg] [numeric](18, 0) NULL,
	[PracKsiegowosc] [numeric](19, 0) NULL,
	[GlownyKsiegowy] [numeric](19, 0) NULL,
	[Dostawca] [numeric](18, 0) NULL,
	[Zamowienie] [numeric](18, 0) NULL,
	
	[Pracownik] [numeric](19, 0) NULL,
	[Zaliczka] [numeric](19, 0) NULL,
	[korekta] [bit] NULL,
	[korekta_faktura] [varchar](30) NULL,
	[STANOWISKO_DOSTAW] [numeric](18, 0) NULL,
	[opisMeryt] [varchar](4096) NULL,
	[PrzeznIUwagi] [varchar](4096) NULL,
	[KwotaPozost] [numeric](18,2) NULL,
	[potwOdZgZUmow] [bit] NULL,
	[potwOdZgZFakt] [bit] NULL,
	[typ_faktury_wal] [numeric](18, 0) NULL,
	[typ_faktury_pln] [numeric](18, 0) NULL,
	
		--nie wiadomo co z nimi/ brak na dokumencie google
		
	[nr_wniosku] [varchar](50) NULL,
	[PRACOWNIK_DELEGACJA] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

CREATE TABLE dbo.dsg_invoice_general_multiple (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

GO

CREATE TABLE dbo.dsg_invoice_general_export_status(
	id int NOT NULL IDENTITY (1, 1),
	document_id numeric(18, 0) NOT NULL,
	guid varchar(100) NOT NULL,
	data_utworzenia date NOT NULL
)  ON [PRIMARY];

GO

 alter table dbo.dsg_invoice_general add KWOTAPOZOSTNETTO numeric(19,2)
 alter table dbo.dsg_invoice_general add NRKONTAWYSTAWCY varchar(50)
 alter table dbo.dsg_invoice_general add ts varchar(50)
 alter table dbo.dsg_invoice_general add KwotaPozostVAT numeric(19,2)
 alter table dbo.dsg_invoice_general add sposobWyplaty int;

 alter table dbo.dsg_invoice_general add uzytkownik_wyp_dekret varchar(10)
 alter table dbo.dsg_invoice_general add czySzablon bit
 alter table dbo.dsg_invoice_general add czyZZamowienia bit
 alter table dbo.dsg_invoice_general add NRZAMOWIENIA numeric(19,0)
 alter table dbo.dsg_invoice_general add Szablon numeric(19,0)
 alter table dbo.dsg_invoice_general add ZAAKCEPTOWANOPOZZAM bit 
 alter table dbo.dsg_invoice_general add czyRozliczenieMediow bit
 alter table dbo.dsg_invoice_general add DOT_SEK_ROZL_PROJ bit
 alter table dbo.dsg_invoice_general add CZYZWNIOSKU bit
 alter table dbo.dsg_invoice_general add ZaakceptowanoWnioskiORez bit 
 alter table dsg_invoice_general add wygenerowanoMetryczke bit
 
  