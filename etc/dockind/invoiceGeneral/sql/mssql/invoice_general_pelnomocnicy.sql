--nalezy podac odpowiedni guid gdzie znajduja sie pelnomocnicy
-- tzn w organizacji musi byc grupa/dzial pelnomocnicy
--  w ktorego obrebie znajduja sie grupy w ktorym znajduja sie pelnomocnicy do ktorych beda szly faktury

Create view INVOICE_GENERAL_PELNOMOCNICY
AS
select divgrupyWlasciwe.id,divgrupyWlasciwe.guid as cn,divgrupyWlasciwe.name as title,NULL AS refvalue, NULL AS centrum, 1 AS available from DS_DIVISION divGroupaPeln
left join DS_DIVISION divgrupyWlasciwe on divgrupyWlasciwe.PARENT_ID=divGroupaPeln.ID
 where 
 (divGroupaPeln.GUID = guidWGrupieKtorejZnajdujaSieGrupyPelnomocnikowDlaPoszczegolnychDzialow) AND (divGroupaPeln.DIVISIONTYPE = 'group')