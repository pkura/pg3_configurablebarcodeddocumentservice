IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'ds_acceptance_division')
DROP TABLE ds_acceptance_division;

CREATE TABLE ds_acceptance_division(
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[GUID] [varchar](62) NOT NULL,
	[NAME] [varchar](62) NOT NULL,
	[CODE] [varchar](62) NULL,
	[DIVISIONTYPE] [varchar](20) NOT NULL,
	[PARENT_ID] [numeric](19, 0) NULL,
	[ogolna] [int] NULL
) ON [PRIMARY]
GO

SET IDENTITY_INSERT [ds_acceptance_division] ON 
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(1 AS Numeric(19, 0)), N'root', N'INS', N'root', N'ac.division', NULL, 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(10010 AS Numeric(19, 0)), N'xxx', N'Dział zakupów i zamówień publicznych', N'zakupyIZamowienia', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(10 AS Numeric(19, 0)), N'xxx', N'Pełnomocnik ds. Administracyjno-Finansowych', N'sprawyAdmFin', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(10011 AS Numeric(19, 0)), N'xxx', N'Sekcja rozliczeń projektów', N'sekcjaRozliczenProjektow', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(16 AS Numeric(19, 0)), N'xxx', N'Dysponent Środków', N'dysponentSrodkow', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(10012 AS Numeric(19, 0)), N'xxx', N'Kierownik Jednostki', N'kierownikJednostki', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(18 AS Numeric(19, 0)), N'xxx', N'Pełnomocnik ds. zamówień publicznych', N'zamowieniaPubliczne', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(13 AS Numeric(19, 0)), N'xxx', N'Wyznaczony pracownik jednostki samodzielnej finansowo', N'JSamFin', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
INSERT [ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(20 AS Numeric(19, 0)), N'xxx', N'Sekcja Finansowa', N'sekcjaFinansowa', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
SET IDENTITY_INSERT [ds_acceptance_division] OFF
