CREATE TABLE dsg_invoice_general_wn_o_rezer (
	id numeric(18, 0) identity(1,1) NOT NULL,
	bd_budzet_ko_id numeric(18, 0),
	bd_budzet_ko_idm varchar(100),
	bd_plan_zam_id numeric(18, 0),
	bd_plam_zam_idm varchar(100),
	bd_rezerwacja_id numeric(18, 0),
	bd_rezerwacja_idm varchar(100),
	bd_rezerwacja_poz_id numeric(18, 0),
	bd_rodzaj_ko_id numeric(18, 0),
	bd_rodzaj_plan_zam_id numeric(18, 0),
	bd_str_budzet_id numeric(18, 0),
	bd_typ_rezerwacja_idn varchar(100),
	bk_bd_okr_szablon_rel_id numeric(18, 0),
	bk_bd_szablon_poz_id numeric(18, 0),
	bk_nazwa varchar(150),
	bk_poz_nazwa varchar(150),
	bk_poz_nrpoz numeric(18, 0),
	bk_poz_p_koszt numeric(18, 4),
	bk_poz_p_koszt_zrodla numeric(18, 4),
	bk_poz_r_koszt numeric(18, 4),
	bk_poz_r_koszt_zrodla numeric(18, 4),
	bk_poz_rez_koszt numeric(18, 4),
	bk_poz_rez_koszt_zrodla numeric(18, 4),
	bk_wytwor_id numeric(18, 0),
	budzet_id numeric(18, 0),
	budzet_idm varchar(100),
	cena numeric(18, 4),
	czy_obsluga_pzp bit,
	ilosc numeric(18, 4),
	koszt numeric(18, 4),
	magazyn_id numeric(18, 0),
	nazwa varchar(150),
	nrpoz numeric(18, 0),
	okrrozl_id numeric(18, 0),
	pzp_bd_okr_szablon_rel_id numeric(18, 0),
	pzp_bd_szablon_poz_id numeric(18, 0),
	pzp_jm_id numeric(18, 0),
	pzp_jm_idn varchar(100),
	pzp_nazwa varchar(150),
	pzp_poz_nazwa varchar(150),
	pzp_poz_nrpoz numeric(18, 0),
	pzp_poz_p_ilosc numeric(18, 4),
	pzp_poz_p_koszt numeric(18, 4),
	pzp_poz_r_ilosc numeric(18, 4),
	pzp_poz_r_koszt numeric(18, 4),
	pzp_poz_rez_ilosc numeric(18, 4),
	pzp_poz_rez_koszt numeric(18, 4),
	pzp_wytwor_id numeric(18, 0),
	pzp_wytwor_idm varchar(100),
	rez_jm_id numeric(18, 0),
	rez_jm_idn varchar(100),
	rezerwacja_czy_archiwalny bit,
	rezerwacja_status numeric(18, 0),
	vatstaw_id numeric(18, 0),
	wytwor_edit_idm varchar(100),
	wytwor_edit_nazwa varchar(150),
	wytwor_id numeric(18, 0),
	zadanie_id numeric(18, 0),
	zadanie_idn varchar(100),
	zadanie_nazwa varchar(150),
	zrodlo_id numeric(18, 0),
	zrodlo_idn varchar(100),
	zrodlo_nazwa varchar(150)
)


CREATE TABLE dsg_invoice_gen_wn_o_rez_dict (
	id numeric(18, 0) identity(1,1) NOT NULL,
	rezerwacja_idm numeric(18, 0)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


--na kazde 100 000 000 rezerwacji jedno zniknie, ale dopiero po przekroczeniu tej liczby, wiec raczej nie uda sie tyle wypelnic
create view dsg_invoice_gen_wn_o_rez_view as  
SELECT        TOP (99.99999999) PERCENT id, bd_rezerwacja_idm AS cn, bd_rezerwacja_idm AS title, NULL AS centrum, NULL AS refValue, available
FROM            dbo.dsg_invoice_general_wn_o_rezer
WHERE        (rezerwacja_status = 3)
ORDER BY title

  CREATE TABLE dsg_invoice_gen_wn_o_rez_pozycje (
	id numeric(18, 0) identity(1,1) NOT NULL,
	pozycje_rez_rezerwacja_idm varchar(100),
	nr_poz_rez int,
	wytwor_rez varchar(100),
	wytwor_nazwa_rez varchar(150),
	ilosc_rez numeric(18, 4),
	cena_rez numeric(18, 4),
	cena_brutto_rez numeric(18, 4),
	koszt_rez numeric(18, 4),
	kwota_netto_rez numeric(18, 4),
	kwota_vat_rez numeric(18, 4)
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


alter table dsg_invoice_gen_wn_o_rez_pozycje add  rezerwacja_okres_budzet numeric(18,0)
alter table dsg_invoice_gen_wn_o_rez_pozycje add  rezerwacja_budzetID numeric(18,0)
alter table dsg_invoice_gen_wn_o_rez_pozycje add  rezerwacja_stawka numeric(18,0)
alter table dsg_invoice_gen_wn_o_rez_pozycje add  koszt_rez_brutto numeric(19,4)
alter table dsg_invoice_gen_wn_o_rez_pozycje add  rezerwacja_id numeric(18,0)
alter table dsg_invoice_gen_wn_o_rez_pozycje add  document_id numeric(18,0)
alter table dsg_invoice_general_wn_o_rezer add available bit

alter table dsg_invoice_gen_wn_o_rez_pozycje add ZRODLO_WNIOSEK numeric(18,0)