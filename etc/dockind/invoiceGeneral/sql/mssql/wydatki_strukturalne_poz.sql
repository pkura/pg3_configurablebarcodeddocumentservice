CREATE TABLE [dbo].[dsg_INVOICE_GENERAL_WYD_STR_POZ](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[WYDATEK_STRUKT] numeric(18,0) NULL,
	[KWOTA_BRUTTO_WYD] [numeric](19, 4) NULL,
	[OBSZAR] numeric(18,0) NULL,
	[KOD] numeric(18,0) NULL
) ON [PRIMARY]

--tabela z danymi dla wydatkow strukturalnych
CREATE TABLE [dbo].[dsg_INVOICE_GENERAL_WYD_STR](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[WYDATEK_STRUKT] numeric(18,0) NULL,
	[KWOTA_BRUTTO_WYD] [numeric](19, 4) NULL,
	[OBSZAR] numeric(18,0) NULL,
	[KOD] numeric(18,0) NULL
) ON [PRIMARY]
go

-- Tabela przechowuj�ca obszary dla wydatk�w strukturalnych
CREATE TABLE dsg_ig_wyd_str_obszar(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
 CONSTRAINT [PK_dsg_ig_wyd_str_obszar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

-- Obszary wydatk�w strukturalnych przekazane przez UTP
SET IDENTITY_INSERT [dbo].[dsg_ig_wyd_str_obszar] ON
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, 1, 'I', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, 2, 'II', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, 5, 'V', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, 12, 'XII', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, 13, 'XIII', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, 15, 'XV', 0, null, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_obszar] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, 17, 'XVII', 0, null, 1);
go
SET IDENTITY_INSERT [dbo].[dsg_ig_wyd_str_obszar] OFF

-- Tabela przechowuj�ca obszary dla wydatk�w strukturalnych
CREATE TABLE dsg_ig_wyd_str_kod(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [int] NULL,
	[available] [bit] NULL,
 CONSTRAINT [PK_dsg_ig_wyd_str_kod] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[dsg_ig_wyd_str_kod]  WITH CHECK ADD  CONSTRAINT [FK_dsg_ig_wyd_str_kod_dsg_ig_wyd_str_obszar] FOREIGN KEY([refValue])
REFERENCES [dbo].[dsg_ig_wyd_str_obszar] ([id])
ON UPDATE CASCADE
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[dsg_ig_wyd_str_kod] CHECK CONSTRAINT [FK_dsg_ig_wyd_str_kod_dsg_ig_wyd_str_obszar]
GO

-- Kody wydatk�w strukturalnych przekazane przez UTP. RefValue wskazuje na id z tabeli dsg_ig_wyd_str_obszar
SET IDENTITY_INSERT [dbo].[dsg_ig_wyd_str_kod] ON
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, 1, '1', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, 2, '2', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, 3, '3', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, 4, '4', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, 5, '5', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, 6, '6', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, 7, '7', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, 8, '8', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, 9, '9', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, 10, '10', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, 11, '11', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, 12, '12', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, 13, '13', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, 14, '14', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, 15, '15', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (47, 47, '47', 0, 5, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (51, 51, '51', 0, 5, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (53, 53, '53', 0, 5, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (54, 54, '54', 0, 5, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (72, 72, '72', 0, 12, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (73, 73, '73', 0, 12, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (74, 74, '74', 0, 12, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (75, 75, '75', 0, 13, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (79, 79, '79', 0, 13, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (81, 81, '81', 0, 15, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (85, 85, '85', 0, 17, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_kod] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (86, 86, '86', 0, 17, 1);
go
SET IDENTITY_INSERT [dbo].[dsg_ig_wyd_str_kod] OFF

-- Tabela przechowuj�ca nazwy dla wydatk�w strukturalnych
CREATE TABLE [dbo].[dsg_ig_wyd_str_nazwa](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [int] NULL,
	[available] [bit] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[dsg_ig_wyd_str_nazwa]  WITH CHECK ADD  CONSTRAINT [FK_dsg_ig_wyd_str_nazwa_dsg_ig_wyd_str_kod] FOREIGN KEY([refValue])
REFERENCES [dbo].[dsg_ig_wyd_str_kod] ([id])
ON UPDATE CASCADE
ON DELETE SET NULL
GO

ALTER TABLE [dbo].[dsg_ig_wyd_str_nazwa] CHECK CONSTRAINT [FK_dsg_ig_wyd_str_nazwa_dsg_ig_wyd_str_kod]
GO
-- Nazwy wydatk�w strukturalnych przekazane przez UTP. RefValue wskazuje na id z tabeli dsg_ig_wyd_str_kod
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (1, 'Dzia�alno�� B+RT prowadzona w o�rodkach badawczych', 0, 1, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (2, 'Infrastruktura B+RT', 0, 2, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (3, 'Transfer technologii i udoskonal. sieci wsp�pr. mi�dzy M�P', 0, 3, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (4, 'Wsparcie na rzecz rozwoju B+RT', 0, 4, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (5, 'Us�ugi w zakresie zaawansow. wsparcia przedsi�biorstw', 0, 5, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (6, 'Wsparcie na rzecz M�P w zakresie promocji produkt�w', 0, 6, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (7, 'Inwestycje przedsi�biorstw zw. z dziedz. bada� i innowacji', 0, 7, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (8, 'Inne inwestycje przedsi�biorstw', 0, 8, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (9, 'Inne dzia�ania naj�ce na celu pobudz. bada�, innow. przeds.', 0, 9, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (10, 'Infrastruktura telekomunikacyjna (w tym sieci szerokopasm.)', 0, 10, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (11, 'Technologie informacyjne i komunikacyjne - TIK', 0, 11, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (12, 'Technologie informacyjne i komunikacyjne (sieci TEN-ICT)', 0, 12, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (13, 'Us�ugi i aplikacje dla obywateli', 0, 13, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (14, 'Us�ugi i aplikacje dla M�P', 0, 14, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (15, 'Inne dzia�ania maj�ce na celu popraw� dost�pu M�P do TK', 0, 15, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (16, 'Jako�� powietrza', 0, 47, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (17, 'Promowanie bior�norodno�ci i ochrony przyrody (w tym NATURA 2000)', 0, 51, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (18, 'Zapobieganie zagro�eniom naturalnym i technologicznym (w tym opracowanie i wdra�anie plan�w i instrument�w zapobiegania i zarz�dzania zagro�eniami)', 0, 53, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (19, 'Inne dzia�ania na rzecz ochrony �rodowiska i zapobiegania zagro�eniom', 0, 54, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (20, 'Opracow., uruchom, i wdro�enie reform syst. kszta�c. i szkol', 0, 72, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (21, 'Dzia�ania na rzecz zwi�kszenia udz. w kszta�ceniu i szkol.', 0, 73, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (22, 'Rozw�j potencja�u ludzkiego w zakresie bada� i innowacji', 0, 74, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (23, 'Infrastruktura edukacji', 0, 75, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (24, 'Pozosta�a infrastruktura spo�eczna', 0, 79, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (25, 'Rozwi�zania na rzecz podn. jako�ci oprac., monit., ewaluacji', 0, 81, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (26, 'Przygotowanie, realizacja monitorowanie i kontrola', 0, 85, 1);
INSERT INTO [dbo].[dsg_ig_wyd_str_nazwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (27, 'Ocena, badania, ekspertyzy, informacja', 0, 86, 1);
go

CREATE VIEW [dbo].[dsg_invoice_general_wydPoz_wydStruct] AS
SELECT *
FROM dsg_ig_wyd_str_nazwa
GO

CREATE VIEW [dbo].[dsg_invoice_general_wydPoz_obszar] AS
SELECT *
FROM dsg_ig_wyd_str_obszar
GO

CREATE VIEW [dbo].[dsg_invoice_general_wydPoz_kod] AS
SELECT * 
FROM dsg_ig_wyd_str_kod
GO


