CREATE TABLE dbo.dsg_warehouse (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(250),
	title varchar(250),
	centrum int,
	refValue varchar(20),
	available bit,
	bilansowy bit,
) ON [PRIMARY]