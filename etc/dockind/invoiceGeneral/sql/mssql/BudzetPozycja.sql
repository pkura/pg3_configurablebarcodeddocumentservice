  create table dsg_invoice_general_budzety_pozycja_simple (
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[bd_nad_rodzaj_ko_id] [numeric](18,0) NULL,
	[czyAktywna] bit NULL,
	[erpId] [numeric](18, 0) NULL,
	[nazwa] [varchar](255) NULL,
	[nrpoz] [numeric](18, 0) NULL,
	[opis] [varchar](4000) NULL,
	[p_koszt] [numeric](18, 2) NULL,	
	[parent] [varchar](55) NULL,
	[parent_id] [numeric](18, 0) NULL,	
	[parent_idm] [varchar](50) NULL,
	[r_koszt] [numeric](18, 2) NULL,		
	[rez_koszt] [numeric](18, 2) NULL,
	[wytwor_id][numeric](18,0) NULL,
	[wytwor_idm][varchar](55) NULL

) ON [PRIMARY]

create view dsg_invoice_general_budzet_poz as
select erpId as id, erpId as cn,nazwa as title, parent_id as refValue,available, null as centrum from dsg_invoice_general_budzety_pozycja_simple


alter table dsg_invoice_general_budzety_pozycja_simple add available bit

 ---insert into dsg_invoice_general_budzety_pozycja_simple(erpId,nazwa) values(0,'Rozliczenie za media')