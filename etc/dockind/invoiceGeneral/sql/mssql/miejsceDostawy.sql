CREATE TABLE [dbo].[dsg_delivery_location](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

create view [dbo].[simple_erp_per_docusafe_stdost_view]
as
select id,cn,title,centrum, available,refvalue 
from [dsg_delivery_location]

