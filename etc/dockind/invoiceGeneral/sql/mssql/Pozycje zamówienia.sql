
CREATE TABLE [dbo].[DSG_INVOICE_WnioZapDoDost_Pozycje](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LP] [varchar](20) NULL,
	[POZYCJA_BUDZETU] [numeric](19, 0) NULL,
	[PRODUKT] [numeric](19, 0) NULL,
	[OPIS] [varchar](240) NULL,
	[ILOSC] [numeric](19, 3) NULL,
	[JM] [numeric](19, 0) NULL,
	[CENA] [numeric](19, 2) NULL,
	[CENA_BAZ] [numeric](19, 2) NULL,
	[NETTO] [numeric](19, 2) NULL,
	[NETTO_BAZ] [numeric](19, 2) NULL,
	[VAT] [numeric](19, 0) NULL,
	[DATAOCZEKIWANA] [date] NULL
) ON [PRIMARY]

GO

--widok do budzetow dal pozycji faktury, jesli bedzie potrzebne
CREATE VIEW [dbo].[INVOICE_GENERAL_BUDZETPOZ_view]
AS
SELECT        poz.ID, CAST(poz.ID AS varchar(50)) AS cn, CAST(poz.ID AS varchar(255)) AS title, 0 AS centrum, CAST(dic.DOCUMENT_ID AS varchar(20)) AS refValue, 
                         1 AS available
FROM            dsg_invoice_general_multiple AS dic INNER JOIN
                         [DSG_INVOICE_WnioZapDoDost_Pozycje] AS poz ON dic.FIELD_CN = 'BUDZET_POZ' AND poz.ID = dic.FIELD_VAL





