CREATE TABLE dbo.dsg_zlecenia_prod(
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available bit NULL,
	erpId numeric(18, 0),
)