\CREATE TABLE dbo.dsg_okresy_budzetowe(
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	idm varchar(50) NULL,
	nazwa varchar(255) NULL,
	wersja int NULL,
	czy_archiwalny bit NULL,
	Data_obow_do datetime NULL,
	Data_obow_od datetime NULL,
	erpId numeric(18,0) NOT NULL
) ON [PRIMARY]

-- okresy budzetowania view 
CREATE VIEW dbo.dsg_invoice_general_okresy_budz AS
SELECT        erpId AS id, idm AS cn, nazwa AS title, NULL AS refValue, NULL AS centrum, available
FROM            dbo.dsg_okresy_budzetowe


alter table dsg_okresy_budzetowe add available bit