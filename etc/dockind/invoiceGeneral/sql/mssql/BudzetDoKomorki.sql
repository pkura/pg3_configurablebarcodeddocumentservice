    CREATE TABLE [dbo].[dsg_budzetDokomorki](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[budzet_id] [numeric](18,0) NULL,
	[budzet_idn] [varchar](255) NULL,
	[budzet_nad_id] [numeric](18,0) NULL,
	[budzet_nad_idn] [varchar](255) NULL,
	[okres_id] [numeric](18, 0) NULL,
	[czyWiodaca] bit NULL,
	[komorka_id] [numeric](18, 0) NULL,
	[komorka_idn] [varchar](255) NULL,	
	[nazwaKomorkiBudzetowej] [varchar](400) NULL,
	[nazwaKomorkiPrganizacyjnej] [varchar](400) NULL	

) ON [PRIMARY]