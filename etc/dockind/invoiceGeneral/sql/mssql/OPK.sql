CREATE VIEW [dbo].[simple_erp_per_docusafe_OPK_view]
AS
SELECT dbo.ds_division_erp.erp_id AS id,
       dbo.ds_division_erp.erp_id AS cn,
       dbo.ds_division_erp.opk + ': ' + dbo.DS_DIVISION.NAME AS title,
       dbo.ds_division_erp.is_cost AS czy_kosztowa,
       NULL AS refValue,
       NULL AS centrum, 1 AS available,
       dbo.DS_DIVISION.CODE
FROM dbo.DS_DIVISION
INNER JOIN dbo.ds_division_erp ON dbo.DS_DIVISION.ID = dbo.ds_division_erp.ds_division_id
WHERE (dbo.ds_division_erp.is_cost = 1)

