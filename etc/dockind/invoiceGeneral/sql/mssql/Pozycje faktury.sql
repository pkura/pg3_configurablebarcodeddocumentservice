CREATE TABLE dbo.DSG_INVOICE_GENERAL_POZFAKT(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[POZYCJA_BUDZETU] numeric(19,0),
	[ZADANIA] numeric (19,0) NULL,
	[KWALIFIKACJA] numeric (19,0) NULL,
	[ZRODLO] numeric (19,0) NULL,
	[STAWKA] numeric (19,0) NULL,
	[KWOTANETTO] float NULL,
	[KWOTAVAT] float NULL,
	[KWOTABRUTTO] float NULL,
	[OPK] numeric(19,0) NULL,
	[POZFAKTURY] numeric(19,0) NULL,
	[PRZEZNACZENIE] numeric (19,0) NULL,
	[POZYCJA_PROJEKT] numeric (19,0) NULL
	)

	
	--slowniki dynamiczne
  --    alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO165_DOMY_STUDENTA numeric(19,0)
	--  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO166_KOSZTY_DS numeric(19,0)
	 -- alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO116_BEZPOSREDNIEPOSREDNIE numeric(19,0)
	--  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO115_SYSTEM_STUDIOW numeric(19,0)
   --  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO39_KOMORKA_KOSZTOWA numeric(19,0)
 --	  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO119_KWALIFIKOWALNENIEKWALIFIKOWALNE numeric(19,0)
--	  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO118_RODZAJ_KOSZTOW_ZW_Z_PR numeric(19,0)
 --	  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO173_ZADANIA_DO_PROJEKTOW numeric(19,0)
  --	  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO128_CZWORKA numeric(19,0)
    --  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO90_ZMNIEJSZENIA_ZFSS numeric(19,0)
	 -- alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO91_ZWIEKSZENIA_ZFSS numeric(19,0)
	--  alter table DSG_INVOICE_GENERAL_POZFAKT add DSD_REPO67_GUS numeric(19,0)
	  
	  
	  
	  alter table DSG_INVOICE_GENERAL_POZFAKT add pozycjaZamowieniaId numeric(18,0)
	  alter table DSG_INVOICE_GENERAL_POZFAKT add documentId numeric(18,0) -- uzywany tylko przy fakturze rozliczanej z zamowienia
	  alter table DSG_INVOICE_GENERAL_POZFAKT add wniosekORezerwacje numeric(18,0)
	  
	  alter table DSG_INVOICE_GENERAL_POZFAKT add POZYCJAFAKPOLEBUDZET numeric(18,0)
	  alter table DSG_INVOICE_GENERAL_POZFAKT add ZRODLOZAWPROJ numeric(18,0)
	  alter table DSG_INVOICE_GENERAL_POZFAKT add POZYCJAPROJDOZROD numeric(18,0)
	  

	  
	  