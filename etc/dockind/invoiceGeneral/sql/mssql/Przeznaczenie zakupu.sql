CREATE TABLE [dbo].[dsg_przez_zakup](
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available bit NULL
) 

insert into [dbo].[dsg_przez_zakup] values (0, 'Sprzedaż opodatkowana', null, null, 1)
insert into [dbo].[dsg_przez_zakup] values (1, 'Sprzedaż nieopodatkowana', null, null, 1)
insert into [dbo].[dsg_przez_zakup] values (2, 'Nieokreślone', null, null, 1)

create view dbo.simple_erp_per_docusafe_przezn_zak_view
AS
select id,cn,title,centrum,refValue,available from dsg_przez_zakup