CREATE TABLE dbo.dsg_project_budzet_zasob (
	id numeric(18, 0) identity(1,1) NOT NULL,
	czy_glowny bit,
	nazwa varchar(255),
	erpId numeric(18, 0),
	nrpoz int,
	p_cena numeric(18,1),
	p_ilosc numeric(18,1),
	p_wartosc numeric(18,1),
	parent varchar(255),
	parent_id numeric(18,0),
	r_ilosc numeric(18,1),
	r_wartosc numeric(18,1),
	rodzaj_zasobu int,
	zasob_glowny_id numeric(18,1)
)
alter table dsg_project_budzet_zasob add available bit


create view dbo.simple_erp_per_zasoby_projektu_view as
SELECT        CONVERT(numeric(18, 0), erpId) AS id, CONVERT(numeric(18, 0), erpId) AS cn, nazwa AS title, available, CONVERT(numeric(18, 0), parent_id) AS refValue, NULL 
                         AS centrum
FROM            dbo.dsg_project_budzet_zasob