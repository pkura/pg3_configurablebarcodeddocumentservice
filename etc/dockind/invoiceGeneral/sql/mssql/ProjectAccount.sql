USE [utp]
GO

/****** Object:  Table [dbo].[dsg_projectAccount]    Script Date: 2014-03-31 08:17:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[dsg_projectAccount](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[konto_ido] [varchar](255) NULL,
	[kontrakt_id] [numeric](18, 0) NULL,
	[kontrakt_idm] [varchar](255) NULL,
	[rep_atrybut_ido] [varchar](255) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
alter table dsg_projectAccount add available bit


