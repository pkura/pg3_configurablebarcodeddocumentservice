CREATE TABLE dbo.dsg_projekty (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available bit NULL,
	identyfikator_num numeric(18, 2),
	erpId numeric(18, 0),
	p_datako datetime,
	p_datapo datetime,
	typ_kontrakt_id numeric(18,2),
	typ_kontrakt_idn varchar(200)
)

create view dbo.simple_erp_per_projekty_view as
SELECT        CONVERT(numeric(18, 0), erpId) AS id, cn, title, centrum, available, refValue
FROM            dbo.dsg_projekty

insert into dsg_projekty(cn,title,available,erpId) values('niedostepna','Wybrano zlecenie - pozycja niedostÍpna',0,0)