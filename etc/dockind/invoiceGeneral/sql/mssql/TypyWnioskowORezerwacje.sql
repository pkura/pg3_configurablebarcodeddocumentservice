CREATE TABLE dbo.dsg_typy_wnioskow_o_rez(
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	idm varchar(50) NULL,
	nazwa varchar(255) NULL,
	czy_aktywny bit NULL,
	czy_kontrola_ceny bit NULL,
	czy_kontrola_ilosci bit NULL,
	czy_kontrola_wartosci bit NULL,
	czy_obsluga_pzp bit NULL,
	erpId numeric(18,0)  NOT NULL
) ON [PRIMARY]
