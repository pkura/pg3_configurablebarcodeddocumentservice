CREATE TABLE [dbo].[dsg_vat_rate](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[czy_zwolniona] [bit] NULL,
	[nie_podlega] [bit] NULL,
	[erp_id] [numeric](19, 0) NOT NULL,
	[idm] [varchar](10) NULL,
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

--vat 
create view [dbo].[simple_erp_per_docusafe_vatstaw_view] AS
SELECT        erp_id AS id, cn, title, NULL AS refValue, 0 AS CENTRUM, available
FROM            dbo.dsg_vat_rate