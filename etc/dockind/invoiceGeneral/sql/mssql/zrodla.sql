CREATE TABLE dbo.dsg_zrod_finan (
	id numeric(18, 0) identity(1,1) NOT NULL,
	erpId numeric(18, 0),
	identyfikator_num numeric(18, 1),
	idm varchar(100),
	zrodlo_nazwa varchar(100),
	available bit
)  



create view simple_erp_per_proj_po_zrodl_view as 
  SELECT        CONVERT(numeric(18, 0), proj.erpId) AS id, proj.cn, proj.title, centrum, proj.available, CONVERT(numeric(18,0),zrod.zrodlo_id) as refValue 
FROM          dbo.dsg_zrod_finan_proj as zrod
left join   dbo.dsg_projekty as proj on proj.erpId=zrod.kontrakt_id


CREATE VIEW simple_erp_per_docusafe_zrodla_f
AS
SELECT        CONVERT(numeric(18, 0), erpId) AS id, idm AS cn, zrodlo_nazwa AS title, NULL AS centrum, NULL AS refValue, available
FROM            dbo.dsg_zrod_finan
WHERE        (idm IS NOT NULL)