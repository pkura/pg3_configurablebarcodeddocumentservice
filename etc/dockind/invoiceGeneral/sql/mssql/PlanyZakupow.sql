CREATE TABLE dbo.dsg_plany_zakupow(
	id numeric(19,0) identity(1,1) NOT NULL,
	budzet_id numeric(18, 0) NULL,
	budzet_idn varchar(50) NULL,
	okres_id numeric(10, 0) NULL,
	okres_idm varchar(50) NULL,
	czyAgregat bit NULL,
--	czy_ceny_brutto bit NULL,
--	czy_kwoty_tys bit NULL,
	erpId numeric(18,0) NULL,
	idm varchar(50) NULL,
	nazwa varchar(40) NULL,
	--kwota_pzp numeric(18,2) NULL,
	--pozostalo_pzp numeric(18,2) NULL,
	--status int NULL
) ON [PRIMARY]

alter table dsg_plany_zakupow add available bit

create view [dsg_plany_zakupow_view] as 
SELECT	pz.erpId AS id, pz.idm AS cn, pz.nazwa AS title, bKOk.erpId AS refvalue, NULL AS centrum, pz.available
FROM	dbo.dsg_plany_zakupow AS pz LEFT OUTER JOIN
		dbo.dsg_budzetyKO_komorek AS bKOk ON bKOk.budzet_id = pz.budzet_id
WHERE	(czyAgregat = 0)

create view dsg_plany_zak_identyfikator_view as
SELECT        pz.erpId AS id, pz.idm AS cn, pz.idm AS title, bKOk.erpId AS refvalue, NULL AS centrum, pz.available
FROM            dbo.dsg_plany_zakupow AS pz LEFT OUTER JOIN
                         dbo.dsg_budzetyKO_komorek AS bKOk ON bKOk.budzet_id = pz.budzet_id
