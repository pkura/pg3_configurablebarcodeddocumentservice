create table dsg_pracownicyERP (
    id bigint identity(1,1) not null,
    czy_aktywny_osoba smallint null,
	guid varchar(50) NULL,
    imie varchar(20) null,
    nazwisko varchar(20) null,
    pesel varchar(20) null,
    pracownik_id numeric(18,0) null,
    pracownik_idn varchar(20) null,
    nrewid int null,
)

