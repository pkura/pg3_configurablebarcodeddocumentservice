CREATE TABLE dbo.dsg_zd_finansowe (
	id numeric(18, 0) identity(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available bit NULL,
	identyfikator_num numeric(18, 2),
	erpId numeric(18, 0)
)
create view dbo.simple_erp_per_docusafe_zadania
As
SELECT        zd.erpId AS id, zd.cn, zd.title, zd.centrum, proj.erpId AS refValue, zd.available
FROM            dbo.dsg_zd_finansowe AS zd LEFT OUTER JOIN
                         dbo.dsg_projekty AS proj ON SUBSTRING(zd.cn, 0, 10) = SUBSTRING(proj.cn, 0, 10)