CREATE TABLE dbo.dsg_zrod_finan_proj (
	id numeric(18, 0) identity(1,1) NOT NULL,
	refValue varchar(20) NULL,
	available bit NULL,
	kontrakt_id numeric(18, 1),
	erpId numeric(18, 0),
	kontrakt_idm varchar(200),
	rodzaj int,
	wsp_proc numeric(18, 1),
	zrodlo_id numeric(18, 1),
	zrodlo_identyfikator_num numeric(18, 1),
	zrodlo_idn varchar(100),
	zrodlo_nazwa varchar(100)
)

 create view dbo.simple_erp_per_docusafe_zrodla AS
SELECT        CONVERT(numeric(18, 0), erpId) AS id, zrodlo_idn AS cn, zrodlo_nazwa AS title, NULL AS centrum,  CONVERT(numeric(18, 0), kontrakt_id) AS refValue, available
                          FROM            dbo.dsg_zrod_finan_proj
                          WHERE        (zrodlo_idn IS NOT NULL)
                          
                          
insert into [dsg_zrod_finan_proj](zrodlo_id,zrodlo_Idn,zrodlo_nazwa,kontrakt_id,available) values (0,'pusty','--Pozycja niedostÍpna dla wybranego funduszu--',0,0)