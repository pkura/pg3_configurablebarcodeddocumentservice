CREATE TABLE [dbo].[dsg_currency](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]


create view dbo.simple_erp_per_docusafe_waluta_view as
SELECT        id, cn, title, NULL AS refValue, '0' AS CENTRUM, available
FROM            dbo.dsg_currency