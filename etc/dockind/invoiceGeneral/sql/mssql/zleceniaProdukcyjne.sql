

CREATE TABLE [dbo].[dsg_zlecenia_prod](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL
) ON [PRIMARY]

GO

  create view dbo.simple_erp_per_docusafe_zlecenia_prod as
  select erpId as id,cn,title,centrum,refValue,available from dsg_zlecenia_prod
  
  insert into dsg_projekty(cn,title,available,erpId) values('niedostepna','Wybrano zlecenie - pozycja niedostÍpna',0,0)

