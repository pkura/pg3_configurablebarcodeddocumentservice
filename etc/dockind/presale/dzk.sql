alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_presales_dzk
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	typdok numeric(18,0),
	dok_idm varchar (20),
	datdok date,
	datodb date,
	warplat numeric (18,0),
	zlecprod numeric (18,0),
	pracownik numeric (18,0),
	komorka numeric (18,0),
	uwagi varchar (4096),
	pozycje numeric (18,0),
);

CREATE TABLE dsg_presales_dzk_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE dsg_presales_dzk_poz
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	WYTWOR NUMERIC(18,0),
	ILOSC NUMERIC (18,3),
	CENA NUMERIC (18,2),
	JM NUMERIC (18,0),
	VATSTAW NUMERIC (18,0),
	KWOTABRUTTO NUMERIC (18,2),
	ZLECPROD NUMERIC (18,0),
	PRACOWNIK NUMERIC (18,0),
	KOMORKA NUMERIC (18,0),
	UWAGI VARCHAR (4096)
);


create view [dbo].ERP_typ_dokumentu_zakupu_view as
select 
	ID as ID,
	typdok_idn as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_typ_dokumentu_zakupu;

create view [dbo].ERP_WARPLAT_view as
select 
	ID as ID,
	warplat_idn as CN, 
	warplat_idn as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_WARPLAT;

create view [dbo].ERP_pp_zlecprod_view as
select 
	ID as ID,
	zlecprod_idm as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_pp_zlecprod;

create view [dbo].ERP_pracownik_view as
select 
	ID as ID,
	pracownik_idn as CN, 
	nazwisko + ' ' + imie as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_pracownik;

create view [dbo].ERP_komorka_view as
select 
	ID as ID,
	komorka_idn as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_komorka where czy_kosztowa = '1.0';

create view [dbo].ERP_jm_view as
select 
	ID as ID,
	jm_idn as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_jm;

create view [dbo].ERP_vatstaw_view as
select 
	ID as ID,
	vatstaw_ids as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_vatstaw;

create view [dbo].ERP_v_br_wytwor_view as
select 
	ID as ID,
	wytwor_idm as CN, 
	nazwa as title,
	null as refValue,
	null as centrum,
	1 as available
from ERP_v_br_wytwor;


update ERP_v_br_wytwor set nazwa = wytwor_idm where nazwa is null;