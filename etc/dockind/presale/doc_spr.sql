create table dsg_docspr
(
	document_id numeric(18,0),
	numer_zamowienia	varchar(50),
	klient numeric(18,0),
	Operator integer,
	TYP_ZAMOWIENIA integer,	
	OSWIADCZENIE_K integer,
	
	FK_SPRZEDAZY integer,	
	FK_SPR_numer_dokumentu		varchar(50),
	FK_SPR_DATA_POTWIERDZENIA 	datetime,
	FK_SPR_numer_segregatora 	varchar(50),
	FK_SPR_numer_w_segregatorze 	varchar(50),
	FK_SPR_OS_POTWIERDZAJACA integer,
	FK_SPR_DATA_WYSLANIA datetime,
	FK_SPR_rodzaj integer,
	
	WZ integer,	
	WZ_numer_dokumentu		varchar(50),
	WZ_DATA_POTWIERDZENIA 	datetime,
	WZ_numer_segregatora 	varchar(50),
	WZ_numer_w_segregatorze 	varchar(50),
	WZ_OS_POTWIERDZAJACA integer,
	WZ_DATA_WYSLANIA datetime,
	WZ_rodzaj integer,
	
	FK_Z_OSWIADCZENIAMI integer,	
	FK_Z_OSnumer_dokumentu		varchar(50),
	FK_Z_OSDATA_POTWIERDZENIA 	datetime,
	FK_Z_OSnumer_segregatora 	varchar(50),
	FK_Z_OSnumer_w_segregatorze 	varchar(50),
	FK_Z_OSOS_POTWIERDZAJACA integer,
	FK_Z_OSDATA_WYSLANIA datetime,
	FK_Z_OSrodzaj integer,
	
	FK_KORYGUJACE integer,	
	FK_KO_numer_dokumentu		varchar(50),
	FK_KO_DATA_POTWIERDZENIA 	datetime,
	FK_KO_numer_segregatora 	varchar(50),
	FK_KO_numer_w_segregatorze 	varchar(50),
	FK_KO_OS_POTWIERDZAJACA integer,
	FK_KO_DATA_WYSLANIA datetime,
	FK_KO_rodzaj integer
);

CREATE TABLE dsg_typ_zamowienia
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_operator
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
alter table dsg_docspr add NR_SEG integer;
alter table dsg_docspr add NR_W_SEG integer;