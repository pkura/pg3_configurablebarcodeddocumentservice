alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_presales_wizytowka
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	ILOSC numeric (18,0),
	DATA_WYSTAWIENIA date,
	DATA_PLATNOSCI date,
);