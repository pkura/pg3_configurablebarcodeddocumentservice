create table dsg_bisk_reklamacja
(
	document_id numeric(18,0) primary key,
	rodzaj	numeric(18,0), 
	nr_reklamacji integer,
	PODRODZAJ integer,
	data_zgloszenia datetime,
	termin_realizacji datetime,
	DANE_PRODUKTU varchar(300),
	NUMER_FABRYCZNY varchar(50),
	typ integer,
	INDEX_PRODUKTU varchar(50),
	DATA_ZAKUPU datetime,
	DOKUMENT_ZAKUPU integer,
	ILOSC integer,
	kontrahent numeric(18,0),
	OSOBA_PRZYJMUJACA numeric(18,0),
	OPIS_USTERKI integer,
	opis varchar(500),
	DATA_PRZYJECIA_MAGZAYN datetime,
	WOJEWODZTWO integer,
	SERWISANT integer,
	WYMIENIONE_CZESCI integer,
	CZESCI varchar(500),
	ILOSC_CZESCI integer,
	UWAGI_SERWISANTA varchar(500),
	OPIS_NAPRAWY varchar(500),
	DATA_NAPRAWY datetime,
	ROBOCIZNA float,
	WYSLANY smallint,
	DATA_WYSYLKI datetime,
	KOSZT_WYSYLKI float,
 	KILOMETRY float,
	SUMA float,
	status integer
);

ALTER TABLE dsg_bisk_reklamacja ADD CONSTRAINT dsg_bisk_reklamacja_docId FOREIGN KEY (document_id) REFERENCES ds_document (ID);

CREATE TABLE DSG_BISK_MULTI_VALUE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(20 ) NOT NULL,
    FIELD_VAL varchar(100) NULL
);


CREATE TABLE dsg_osoba
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_wojewodztwo
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_serwisant
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

insert into dsg_wojewodztwo (cn,title)values('D','dolno�l�skie');
insert into dsg_wojewodztwo (cn,title)values('C','kujawsko-pomorskie');
insert into dsg_wojewodztwo (cn,title)values('L','lubelskie');
insert into dsg_wojewodztwo (cn,title)values('F','lubuskie');
insert into dsg_wojewodztwo (cn,title)values('E','��dzkie');
insert into dsg_wojewodztwo (cn,title)values('K','ma�opolskie');
insert into dsg_wojewodztwo (cn,title)values('W','mazowieckie');
insert into dsg_wojewodztwo (cn,title)values('O','opolskie');
insert into dsg_wojewodztwo (cn,title)values('R','podkarpackie');
insert into dsg_wojewodztwo (cn,title)values('B','podlaskie');
insert into dsg_wojewodztwo (cn,title)values('G','pomorskie');
insert into dsg_wojewodztwo (cn,title)values('S','�l�skie');
insert into dsg_wojewodztwo (cn,title)values('T','�wi�tokrzyskie');
insert into dsg_wojewodztwo (cn,title)values('N','warmi�sko-mazurskie');
insert into dsg_wojewodztwo (cn,title)values('P','wielkopolskie');
insert into dsg_wojewodztwo (cn,title)values('Z','zachodniopomorskie');

alter table dsg_bisk_reklamacja add KOSZT_DOJAZDU numeric(16,2);
alter table dsg_bisk_reklamacja add KOSZT_ROBOCIZNY numeric(16,2);
alter table dsg_bisk_reklamacja add ILOSC_czesci integer;
alter table dsg_bisk_reklamacja add DOJAZD integer;