CREATE TABLE dsg_pmaterials
(
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    mpk smallint,
    rodzaj smallint,
    szczegoly smallint,
    sz_dostawy smallint,
    komentarz varchar(350),
    w_akcepaca smallint
);