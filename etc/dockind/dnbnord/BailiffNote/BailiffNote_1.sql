CREATE TABLE dsg_bailiff_note
(
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    klient numeric(18, 0),
    komornik numeric(18, 0),
    dane_uzupelnione smallint,
    klient_banku smallint,
);

CREATE TABLE dsg_bn_klient
(
    ID numeric(18,0) identity(1,1) NOT NULL,
    sygnatura	varchar(50),
	nazwa		varchar(50),
	imie		varchar(50),
	nazwisko	varchar(50),
	nip			varchar(10),
	regon		varchar(14),
	miejscowosc	varchar(50),
	kod			varchar(6),
	ulica		varchar(50),
	nr_domu		varchar(5),
	bankowy		smallint
);

CREATE TABLE dsg_bn_komornik
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	sad varchar(50),
	imie		varchar(50),
	nazwisko	varchar(50),
	miejscowosc	varchar(50),
	kod			varchar(6),
	ulica		varchar(50),
	nr_domu		varchar(5)
);