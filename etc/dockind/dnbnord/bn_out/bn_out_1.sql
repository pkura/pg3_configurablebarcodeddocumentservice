CREATE TABLE dsg_bn_out
(
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    typ smallint,
    klient numeric(18, 0),
    komornik numeric(18, 0),
    source numeric (18,0)
);