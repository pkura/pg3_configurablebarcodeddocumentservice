CREATE TABLE dsg_skarga (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	SPOSOB_ZLOZENIA 	int,
	KTORA_KASA 	        varchar(1000),
	DATA_WPLYWU 		date,
	DATA_ZDARZENIA 		date,
	MIEJSCE_ZDARZENIA 	varchar(200),
	NR_POCIAGU 	        varchar(50),
	RELACJA 	        varchar(100),
	GODZ_ODJAZDU 	    varchar(100),
	NR_KIEROWNIKA       varchar(100),
	TRESC_SKARGI        varchar(4096),
	WYJASNIENIE      	int,
	UZUP_OBCE_JEDN      int,
	OS_MERYTORYCZNE     int,
	KOM_MERYTORYCZNE    int,
	DANE_J_OBCEJ        varchar(350),
	ADD_INFO            varchar(4096)
);


ALTER TABLE dsg_skarga ADD PRIMARY KEY (document_id);
create index dsg_skarga_index on dsg_skarga (document_id);


CREATE TABLE dsg_skarga_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;


alter table dsg_skarga add TYP_DOK int;
alter table dsg_skarga add DATA_ZLOZENIA date;
alter table dsg_skarga add ADRES_ZLOZENIA varchar(1000);
alter table dsg_skarga add ADRES_EMAIL varchar(1000);
alter table dsg_skarga add TEL_TXT varchar(1000);
alter table dsg_skarga add OSOBISCIE_TXT varchar(1000);
alter table dsg_skarga add ODJAZD varchar(100);
alter table dsg_skarga add NR_KASY varchar(100);
alter table dsg_skarga add GODZ_ZDARZENIA varchar(100);
alter table dsg_skarga add MIEJSCOWOSC varchar(100);

alter table dsg_skarga add KATA SMALLINT;
alter table dsg_skarga add KATB SMALLINT;
alter table dsg_skarga add KATC SMALLINT;
alter table dsg_skarga add KATD SMALLINT;
alter table dsg_skarga add KATE SMALLINT;
alter table dsg_skarga add KATF SMALLINT;
alter table dsg_skarga add KATG SMALLINT;
alter table dsg_skarga add KATH SMALLINT;
alter table dsg_skarga add KATI SMALLINT;
alter table dsg_skarga add KATJ SMALLINT;
alter table dsg_skarga add KATK SMALLINT;
alter table dsg_skarga add KATL SMALLINT;
alter table dsg_skarga add KATM SMALLINT;

alter table dsg_skarga add ZASADNA int;
alter table dsg_skarga add STAN_MERYT varchar(4096);
alter table dsg_skarga add nr_skargi varchar (100);
alter table dsg_skarga add TERMIN_NA_ROZPATRZENIE date;
alter table dsg_skarga add DATA_PRZEKAZANIA date;
alter table dsg_skarga add DATA_ZAKONCZENIA date;
alter table dsg_skarga add DATA_WYSLANIA_ODP date;
