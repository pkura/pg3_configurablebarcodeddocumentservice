insert into dsd_km_cost values ('421','zu�ycie elektrycznej energii trakcyjnej',null,null,1);
insert into dsd_km_cost values ('501','koszty dzia�alno�ci podstawowej',null,null,1);
insert into dsd_km_cost values ('765','pozosta�e koszty operacyjne',null,null,1);
insert into dsd_km_cost values ('755','koszty finansowe',null,null,1);

insert into dsd_km_subject_kind values ('24','PKP Energetyka Sp. z o.o.',null,null,1);
insert into dsd_km_subject_kind values ('98','pozosta�e jednostki',null,null,1);

insert into dsd_km_analitycs values ('100','energia Trakcyjna',null,(select top 1 id from dsd_km_cost where cn='421'),1);
insert into dsd_km_analitycs values ('21100','energia-Energia Trakcyjna',null,(select top 1 id from dsd_km_cost where cn='501'),1);
insert into dsd_km_analitycs values ('50','koszty usuwania dewastacji-zg�oszone do ubezpieczenia',null,(select top 1 id from dsd_km_cost where cn='765'),1);
insert into dsd_km_analitycs values ('76','odsetki do emisji obligacji',null,(select top 1 id from dsd_km_cost where cn='755'),1);

insert into dsd_km_project values ('101000','Koszty prowadzenia ruchu',null,null,1);

insert into dsd_km_bp values ('K-309-13','K-309-13',null,null,1);

insert into dsd_km_vehicle values ('99','koszty wsp�lne dla wszystkich typ�w pojazd�w',null,null,1);

insert into dsd_km_action_kind values ('1101','dzia�alno�� przewozowa-wojew�dztwo mazowieckie-poci�gi handlowe',null,null,1);

insert into dsd_km_cost_kind values ('42','energia',null,null,1);

insert into dsd_km_tax values ('1','Stanowi�ce przych�d podatkowy',null,null,1);
insert into dsd_km_tax values ('2','Nie stanowi�ce przych�d podatkowy',null,null,1);

insert into dsd_km_completed values ('1','Zrealizowane',null,null,1);
insert into dsd_km_completed values ('2','Niezrealizowane',null,null,1);