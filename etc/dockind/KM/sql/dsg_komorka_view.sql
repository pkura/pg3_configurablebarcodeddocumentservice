create view dsg_komorka_view as
select ID as id,
GUID as cn,
NAME as title,
CODE as refValue,
null as centrum,
1 as available
from DS_DIVISION
where CODE in ('MEN', 'MEK','MZS');