
alter table dso_rwa add isactive smallint
update dso_rwa set isactive = 1

--05
UPDATE DSO_RWA SET DESCRIPTION = 'SKARGI, WNIOSKI I REKLAMACJE' WHERE ID=143
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Reklamacje podr�nych' WHERE ID=144
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Skargi, wnioski podr�nych' WHERE ID=145
UPDATE DSO_RWA SET ACHOME='B3', ACOTHER='Bc', DESCRIPTION = 'Skargi, wnioski pozosta�e' WHERE ID=150

--07
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'WSPӣPRACA Z KRAJOWYMI INSTYTUCJAMI' WHERE ID=205
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Z Samorz�dem Wojew�dztwa Mazowieckiego' WHERE ID=206
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Z innymi instytucjami samorz�du terytorialnego' WHERE ID=212
UPDATE DSO_RWA SET ACHOME='B10', ACOTHER='Bc', DESCRIPTION = 'Z innymi instytucjami, urz�dami i podmiotami' WHERE ID=221

INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 7, 3, 3, 'A', 'Bc', 'Z partiami politycznymi i organizacjami spo�ecznymi');
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 7, 4, 3, 'A', 'Bc', 'Wsp�praca z zak�adowymi organizacjami zwi�zkowymi');
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 7, 5, 3, 'A', 'Bc', 'Umowy zawierane z podmiotami gospodarczymi');
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 7, 6, 3, 'B5', 'Bc', 'Wsp�praca z organizacjami, kt�rych cz�onkiem jest Sp�ka "Koleje Mazowieckie - KM" sp. z o.o.');


--08 
UPDATE DSO_RWA SET DESCRIPTION = 'WSPӣPRACA Z ZAGRANIC�' WHERE ID=229
--080 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Wsp�praca z instytucjami transportu kolejowego w innych krajach' WHERE ID=230
--081 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Wyjazdy s�u�bowe za granic� pracownik�w Sp�ki' WHERE ID=231
--082 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Przyjazdy zagranicznych delegacji' WHERE ID=237
--083 
UPDATE DSO_RWA SET ACHOME='B10', ACOTHER='Bc', DESCRIPTION = 'Wsp�praca z plac�wkami obcych kolei w Polsce' WHERE ID=246

--09 
UPDATE DSO_RWA SET DESCRIPTION = 'KONTROLE I AUDYTY' WHERE ID=269
--090 
UPDATE DSO_RWA SET DESCRIPTION = 'Zasady i tryb przeprowadzania kontroli' WHERE ID=270
--0900 
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,DIGIT4,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 9, 0, 0, 4, 'A', 'Bc', 'Zasady i metodyka kontroli');
--0901
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Plany kontroli oraz sprawozdania z realizacji' WHERE ID=271
--091 
UPDATE DSO_RWA SET DESCRIPTION = 'Zasady i tryb przeprowadzania audytu' WHERE ID=274
--0910 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Zasady i metodyka audytu' WHERE ID=275
--0911 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Plany audytu oraz sprawozdania z realizacji' WHERE ID=276
--092 
UPDATE DSO_RWA SET DESCRIPTION = 'Kontrole zewn�trzne w Sp�ce' WHERE ID=280
--0920 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Kontrole kompleksowe i problemowe' WHERE ID=281
--0921 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Inne kontrole w sp�ce' WHERE ID=282
--093 
UPDATE DSO_RWA SET ACHOME='A', ACOTHER='Bc', DESCRIPTION = 'Kontrole zewn�trzne w jednostkach podleg�ych' WHERE ID=283
--094 
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 9, 4, 3, 'B5', 'Bc', 'Kontrole zewn�trzne w Sp�ce');
--095 
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 9, 5, 3, 'A', 'Bc', 'Audyt');
--096 
INSERT INTO DSO_RWA (ISACTIVE,DIGIT1,DIGIT2,DIGIT3,RWALEVEL,ACHOME,ACOTHER,DESCRIPTION) values (1, 0, 9, 6, 3, 'B5', 'Bc', 'Ksi��ko kontroli');

--23 
UPDATE DSO_RWA SET DESCRIPTION = 'Transport i ��czno��' WHERE ID=496
--230 
UPDATE DSO_RWA SET ACHOME='B5', ACOTHER='Bc', DESCRIPTION = 'Eksploatacja w�asnych �rodk�w transportu' WHERE ID=497
--231 
UPDATE DSO_RWA SET ACHOME='B5', ACOTHER='Bc', DESCRIPTION = 'Rycza�ty samochodowe' WHERE ID=498
--232 
UPDATE DSO_RWA SET ACHOME='B2', ACOTHER='Bc', DESCRIPTION = 'Eksploatacja �rodk�w ��czno�ci (telefon�w, telefaks�w, dalekopis�w) oraz terminali system�w informatycznych i innych' WHERE ID=503


