CREATE TABLE DSG_WNIOSEK_O_TELEFON(
	DOCUMENT_ID 		BIGINT NOT NULL,
	STATUS				BIGINT,
	OPIS				VARCHAR(4096),
	DATA_PISMA			DATE,
	LIMIT_KOSZTOW		VARCHAR(20),
	WNIOSKODAWCA 		NUMERIC(18,0),
	OSOBA 				NUMERIC(18,0),
);

CREATE TABLE DSG_USER_PRESENTATION(
	ID 					INT IDENTITY(1,1) NOT NULL,
	FIRSTNAME 			VARCHAR(62),
	LASTNAME 			VARCHAR(62),
	LIMIT_KOSZTOW 		VARCHAR(20),
	CENTRUM 			INT NULL,
	DIVISION			VARCHAR(75),
	PHONE_NUMBER		NUMERIC(18),
	POSITION			VARCHAR(100)
);

insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Barbara', 'Chmielewska');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Jan', 'Kowalski');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Joanna', 'Serzysko');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Krzysztof', 'Jasi�ski');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Janusz', 'Szyma�ski');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Marzena', 'Ochnik');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Tomasz', 'Jedynak');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Wojciech', 'G�rski');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Magdalena', 'Gajweska');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Marzena', 'Makowska');
insert into DSG_USER_PRESENTATION (firstname,lastname) values ('Roman', 'Borkowski');

update DSG_USER_PRESENTATION set POSITION='Rzecznik', LIMIT_KOSZTOW='250.00', DIVISION='Zesp� Organizacyjny' where id=1
update DSG_USER_PRESENTATION set POSITION='Dyrektor Generalny', LIMIT_KOSZTOW='Bez limitu', DIVISION='Prezes Zarz�du' where id=2
update DSG_USER_PRESENTATION set POSITION='Naczelnik', LIMIT_KOSZTOW='50.00', DIVISION='Zesp� Organizacyjny' where id=3
update DSG_USER_PRESENTATION set POSITION='Kontroler', LIMIT_KOSZTOW='50.00', DIVISION='Sekcja Obs�ugi Podr�nych' where id=4
update DSG_USER_PRESENTATION set POSITION='Dyrektor Biura', LIMIT_KOSZTOW='100.00', DIVISION='Biuro Administracyjne' where id=5
update DSG_USER_PRESENTATION set POSITION='Kasjerka', LIMIT_KOSZTOW='30.00', DIVISION='Sekcja Obs�ugi Podr�nych' where id=6
update DSG_USER_PRESENTATION set POSITION='Dyrektor Handlowy', LIMIT_KOSZTOW='100.00', DIVISION='Cz�onek Zarz�du' where id=7
update DSG_USER_PRESENTATION set POSITION='Dyrektor Biura Zarz�du', LIMIT_KOSZTOW='100.00', DIVISION='Biuro Zarz�du' where id=8
update DSG_USER_PRESENTATION set POSITION='Specjalista', LIMIT_KOSZTOW='50.00', DIVISION='Zesp� Skarg i Reklamacji' where id=9
update DSG_USER_PRESENTATION set POSITION='Referendarz', LIMIT_KOSZTOW='50.00', DIVISION='Zesp� Skarg i Reklamacji' where id=10
update DSG_USER_PRESENTATION set POSITION='Referendarz', LIMIT_KOSZTOW='50.00', DIVISION='Zesp� Skarg i Reklamacji' where id=11
