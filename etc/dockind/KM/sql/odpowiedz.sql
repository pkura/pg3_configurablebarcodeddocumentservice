CREATE TABLE dsg_odpowiedz (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	STAN_MERYT       	varchar(4096),
	SKARGA              int
);

ALTER TABLE dsg_odpowiedz ADD PRIMARY KEY (document_id);
create index dsg_odpowiedz_index on dsg_odpowiedz (document_id);


CREATE TABLE dsg_odpowiedz_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;

alter table  dsg_odpowiedz add TRESC_SKARGI      varchar(4096)      ;
alter table  dsg_odpowiedz add NR_SKARGI      varchar(100)      ;