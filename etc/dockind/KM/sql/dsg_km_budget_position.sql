create table dsg_km_budget_position (
id bigint identity(1,1) not null,
account_view varchar(255) null,
cost int null,
mpk int null,
subject_kind int null,
analitycs int null,
project int null,
tax int null,
bp int null,
vehicle int null,
action_kind int null,
cost_kind int null,
completed int null,
amount numeric(19,2) null
)