USE [km]
GO
/****** Object:  Table [dbo].[dsd_km_action_kind]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_action_kind](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_analitycs]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_analitycs](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_bp]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_bp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_completed]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_completed](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_cost]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_cost](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_cost_kind]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_cost_kind](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_mpk]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_mpk](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_project]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_project](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_subject_kind]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_subject_kind](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_tax]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_tax](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsd_km_vehicle]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsd_km_vehicle](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsg_km_costinvoice]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsg_km_costinvoice](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wystawienia] [datetime] NULL,
	[data_wplywu_faktury] [datetime] NULL,
	[data_operacji_gospodarczej] [datetime] NULL,
	[data_rejestracji] [datetime] NULL,
	[termin_platnosci] [datetime] NULL,
	[rodzaj_dokumentu] [int] NULL,
	[okres_sprawozdawczy] [varchar](30) NULL,
	[kwota_waluta] [int] NULL,
	[kwota_waluta_data] [datetime] NULL,
	[kwota_walutowa] [numeric](19, 2) NULL,
	[kwota_kurs] [float] NULL,
	[kwota_netto] [numeric](19, 2) NULL,
	[kwota_vat] [numeric](19, 2) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[sposob] [int] NULL,
	[sposob_zwrotu] [int] NULL,
	[pracownik_nr_konta] [varchar](100) NULL,
	[nazwisko_dla_zwrotu] [varchar](1024) NULL,
	[opis] [varchar](4096) NULL,
	[addinfo] [varchar](8000) NULL,
	[platnosc_szczegolna] [tinyint] NULL,
	[rodzaj_zamowienia] [int] NULL,
	[tryb_zamowienia] [int] NULL,
	[szczegoly_zamowienia] [int] NULL,
	[postepowanie_nr] [varchar](512) NULL,
	[postepowanie_data] [datetime] NULL,
	[kwota_pozostala_mpk] [numeric](19, 2) NULL,
	[korekta] [tinyint] NULL,
	[faktura_skan] [numeric](19, 2) NULL,
	[protokol_odbioru] [numeric](19, 2) NULL,
	[korekta_faktura] [int] NULL,
	[szablon] [int] NULL,
	[nr_wniosku_zakupowego] [varchar](250) NULL,
	[nr_umowy] [varchar](250) NULL,
	[pracownik_merytoryczny] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsg_km_costinvoice_document_types]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsg_km_costinvoice_document_types](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__dsg_km_costinvoice_document_types] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsg_km_costinvoice_multiple_value]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsg_km_costinvoice_multiple_value](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsg_km_costinvoice_tp]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsg_km_costinvoice_tp](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wystawienia] [datetime] NULL,
	[data_wplywu_faktury] [datetime] NULL,
	[data_operacji_gospodarczej] [datetime] NULL,
	[data_rejestracji] [datetime] NULL,
	[termin_platnosci] [datetime] NULL,
	[rodzaj_dokumentu] [int] NULL,
	[okres_sprawozdawczy] [varchar](30) NULL,
	[kwota_waluta] [int] NULL,
	[kwota_waluta_data] [datetime] NULL,
	[kwota_walutowa] [numeric](19, 2) NULL,
	[kwota_kurs] [float] NULL,
	[kwota_netto] [numeric](19, 2) NULL,
	[kwota_vat] [numeric](19, 2) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[sposob] [int] NULL,
	[sposob_zwrotu] [int] NULL,
	[pracownik_nr_konta] [varchar](100) NULL,
	[nazwisko_dla_zwrotu] [varchar](1024) NULL,
	[opis] [varchar](4096) NULL,
	[addinfo] [varchar](8000) NULL,
	[platnosc_szczegolna] [tinyint] NULL,
	[rodzaj_zamowienia] [int] NULL,
	[tryb_zamowienia] [int] NULL,
	[szczegoly_zamowienia] [int] NULL,
	[postepowanie_nr] [varchar](512) NULL,
	[postepowanie_data] [datetime] NULL,
	[kwota_pozostala_mpk] [numeric](19, 2) NULL,
	[korekta] [tinyint] NULL,
	[faktura_skan] [numeric](19, 2) NULL,
	[protokol_odbioru] [numeric](19, 2) NULL,
	[korekta_faktura] [int] NULL,
	[template_name] [varchar](100) NULL,
	[nr_umowy] [varchar](250) NULL,
	[nr_wniosku_zakupowego] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[dsg_km_currency]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dsg_km_currency](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__dsg_currency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[ds_document_internals]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ds_document_internals] as
select doc.id as document_id, doc.*,doc_out.OFFICENUMBER from ds_document doc
join dso_out_document doc_out on doc.ID=doc_out.ID
where doc.dockind_id=(select id from DS_DOCUMENT_KIND where cn='normal_int');
GO
/****** Object:  View [dbo].[dsd_km_mpk_mapped_to_user]    Script Date: 2013-07-31 11:54:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[dsd_km_mpk_mapped_to_user] as 
select
m.id as id,
m.cn as cn,
m.title as title,
ud.USER_ID as refValue,
null as centrum,
m.available as available
from dsd_km_mpk m
join ds_division d on SUBSTRING(m.title,1,3)=d.CODE
join DS_USER_TO_DIVISION ud on d.id=ud.DIVISION_ID
GO
SET IDENTITY_INSERT [dbo].[dsd_km_action_kind] ON 

GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, N'1101', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. MAZOWIECKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'1102', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. MAZOWIECKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'1103', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. MAZOWIECKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'1104', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. MAZOWIECKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'1111', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBELSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'1112', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBELSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'1113', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBELSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'1114', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBELSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'1121', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'1122', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'1123', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, N'1124', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, N'1131', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŁÓDZKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, N'1132', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŁÓDZKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (16, N'1133', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŁÓDZKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, N'1134', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ŁÓDZKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (18, N'1141', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. PODLASKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (19, N'1142', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. PODLASKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (20, N'1143', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. PODLASKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (21, N'1144', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. PODLASKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (22, N'1151', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (23, N'1152', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (24, N'1153', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (25, N'1154', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (26, N'1161', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. POMORSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (27, N'1162', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. POMORSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (28, N'1163', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. POMORSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (29, N'1164', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. POMORSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (30, N'1171', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (31, N'1172', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (32, N'1173', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (33, N'1174', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (34, N'1181', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBUSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (35, N'1182', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBUSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (36, N'1183', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBUSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (37, N'1184', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. LUBUSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (38, N'1191', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (39, N'1192', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (40, N'1193', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WIELKOPOLSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (41, N'1194', N'DZIAŁALNOŚĆ PRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (42, N'2101', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. MAZOWIECKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (43, N'2102', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. MAZOWIECKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (44, N'2103', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. MAZOWIECKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (45, N'2104', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. MAZOWIECKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (46, N'2111', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBELSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (47, N'2112', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBELSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (48, N'2113', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBELSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (49, N'2114', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBELSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (50, N'2121', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (51, N'2122', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (52, N'2123', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (53, N'2124', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŚWIĘTOKRZYSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (54, N'2131', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŁÓDZKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (55, N'2132', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŁÓDZKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (56, N'2133', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŁÓDZKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (57, N'2134', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ŁÓDZKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (58, N'2141', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. PODLASKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (59, N'2142', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. PODLASKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (60, N'2143', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. PODLASKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (61, N'2144', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. PODLASKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (62, N'2151', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (63, N'2152', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (64, N'2153', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (65, N'2154', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WARMIŃSKO - MAZURSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (66, N'2161', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. POMORSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (67, N'2162', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. POMORSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (68, N'2163', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. POMORSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (69, N'2164', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. POMORSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (70, N'2171', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (71, N'2172', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (72, N'2173', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (73, N'2174', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. ZACHODNIO - POMORSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (74, N'2181', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBUSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (75, N'2182', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBUSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (76, N'2183', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBUSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (77, N'2184', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. LUBUSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (78, N'2191', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIAGI HANDLOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (79, N'2192', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIĄGI SŁUŻBOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (80, N'2193', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WIELKOPOLSKIE,PRZEWÓZ RZECZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_action_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (81, N'2194', N'DZIAŁALNOŚC POZAPRZEWOZOWA,WOJ. WIELKOPOLSKIE,POCIĄGI BEZ PODZIAŁU', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_action_kind] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_analitycs] ON 

GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, N'100', N'ENERGIA TRAKCYJNA', NULL, N'5', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (31, N'10', N'ODSETKI OD POŻYCZEK', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'50', N'KOSZTY USUWANIA DEWASTACJI-ZGŁOSZONE DO UBEZPIECZENIA', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'76', N'ODSETKI DO EMISJI OBLIGACJI', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'201', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI ADMINISTRACYJNE - WŁASNE', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'202', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI ADMINISTRACYJNE - OBCE - ULEPSZENIE', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'203', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI EKSPLOATACYJNE - WŁASNE ', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, N'91XXX', N'AMORTYZACJA ŚRODKÓW TRWAŁYCH WG ANALITYKI DO KONTA 401', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, N'92XXX', N'AMORTYZACJA WNIP WG ANALITYKI DO KONTA 402', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (16, N'93XXX', N'AMORTYZACJA ŚRODKÓW TRWAŁYCH O NISKIEJ WARTOŚCI WG ANALITYKI DO KONTA 403', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, N'94XXX', N'AMORTYZACJA ŚRODKÓW TRWAŁYCH Z LEASINGU FINANSOWEGO WG ANALITYKI DO KONTA 404', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (18, N'11XXX', N'ZUŻYCIE PALIWA TRAKCYJNEGO WG ANALITYKI DOKONTA 411', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (19, N'12XXX', N'ZUZYCIE NIETRAKCYJNEGO PALIWA WG ANALITYKI DO KONTA 412', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (20, N'14XXX', N'MATERIAŁY TECHNICZNE WG ANALITYKI DO KONTA 414', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (21, N'15XXX', N'MATERIAŁY BIUROWE I ADMINISTRACYJNE WG ANALITYKI DO KONTA 415', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (22, N'16XXX ', N'ZUŻYCIE POZOSTAŁYCH MATERIAŁÓW WG ANALITYKI DO KONTA 416', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (23, N'17XXX ', N'MATERIAŁY REMONTOWE WG ANALITYKI DO KONTA 417', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (24, N'21XXX', N'ZUZYCIE ELEKTRYCZNEJ ENERGII TRAKCYJNEJ WG ANALITYKI KONTA 421', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (25, N'22XXX', N'ZUZYCIE POZOSTAŁEJ ENERGII ELEKTRYCZNEJ WG ANALITYKI KONTA 422', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (26, N'23XXX', N'ZUZYCIE ENERGII CIEPLNEJ WG ANALITYKI DO KONTA 423', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (27, N'24XXX', N'ZUZYCIE INNEJ ENERGI WG ANALITYKI DO KONTA 424', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (28, N'31XXX', N'UDOSTEPNIENIE LINII PRZEZ PLK WG ANALITYKI DO KONTA 433', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (29, N'32XXX', N'USŁUGI TRANSPORTOWE WG ANALITYKI DO KONTA 432', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (30, N'33XXX ', N'USŁUGI TELEKOMUNIKACYJNE WG ANALITYKI DO KONTA 433', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (32, N'11', N'ODSETKI OD KREDYTÓW', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (33, N'12', N'ODSETKI OD DEBETÓW I KREDYTÓW W RACHUNKU BIEŻĄCYM', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (34, N'13', N'ODSETKI OD ZOBOWIĄZAŃ BUDŻETOWYCH', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (35, N'14', N'ODSETKI OD ZOBOWIĄZAŃ POZOSTAŁYCH', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (36, N'15', N'ODSETKI - LEASING FINANSOWY', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (37, N'16', N'ODSETKI Z TYTUŁU ŚRODKÓW OTRZYMANYCH W TRAMACH LEASINGU FINANSOWEGO', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (38, N'17', N'INNE ODESETKI', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (39, N'18', N'UJEMNE RÓZNICE KURSOWE OD POZYCZEK OTRZYMANYCH', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (40, N'19', N'UJEMNE RÓŻNICE KURSOWE OD KREDYTÓW ', NULL, N'8', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (41, N'10', N'NALEŻNOŚCI PRZETERMINOWANE', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (42, N'11', N'NALEZNOŚĆI UMORZONE', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (43, N'12', N'NALEŻNOŚCI NIEŚCIĄGALNE', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (44, N'13', N'NALEZNOŚCI PRZEDAWNIONE', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (45, N'14', N'UJEMNE RÓŻNICE INWENTARYZACYJNE', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (46, N'15', N'KOREKTA VAT NALICZONEGO Z TYTUŁU NIEDOBORU', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (47, N'16', N'STRATA NA SPRZEDAŻY NALEŻNOŚCI', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (48, N'17', N'UMORZENIE NALEZNOŚCI OD PRACOWNIKÓW ', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (49, N'18', N'UMORZENIE NALEZNOŚCI POZOSTAŁYCH ', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (50, N'20', N'ODSZKODOWANIA Z TYTUŁU WYPADKÓW PRZY PRACY ', NULL, N'7', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'204', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI EKSPLOATACYJNE - OBCE  ULEPSZENIE  ', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'205', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI POZOSTAŁE - WŁASNE', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'206', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDYNKI POZOSTAŁE - OBCE- ULEPSZENIE', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'207', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDOWLA - WŁASNE', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'208', N'AMORTYZACJA ŚRODKI TRWAŁE - BUDOWLA - OBCA- ULEPRZENIA', NULL, N'47', 1)
GO
INSERT [dbo].[dsd_km_analitycs] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, N'220', N'OBIEKTY INŻYNIERII LĄDOWEJ I WODNEJ', NULL, N'47', 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_analitycs] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_bp] ON 

GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, N'K-309-13', N'K-309-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'K-5-13', N'K-5-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'K-6-13', N'K-6-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'K-7-13', N'K-7-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'K-8-13', N'K-8-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'K-9-13', N'K-9-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'K-10-13', N'K-10-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'K-11-13', N'K-11-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'K-12-13', N'K-12-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'K-13-13', N'K-13-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, N'K-14-13', N'K-14-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, N'K-15-13', N'K-15-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, N'K-16-13', N'K-16-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (16, N'K-17-13', N'K-17-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, N'K-18-13', N'K-18-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (18, N'K-19-13', N'K-19-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (19, N'K-20-13', N'K-20-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (20, N'K-21-13', N'K-21-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (21, N'K-22-13', N'K-22-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (22, N'K-23-13', N'K-23-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (23, N'K-24-13', N'K-24-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (24, N'K-25-13', N'K-25-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (25, N'K-26-13', N'K-26-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (26, N'K-27-13', N'K-27-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (27, N'K-28-13', N'K-28-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (28, N'K-29-13', N'K-29-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (29, N'K-30-13', N'K-30-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (30, N'K-31-13', N'K-31-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (31, N'K-32-13', N'K-32-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (32, N'K-33-13', N'K-33-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (33, N'K-34-13', N'K-34-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (34, N'K-35-13', N'K-35-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (35, N'K-36-13', N'K-36-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (36, N'K-37-13', N'K-37-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (37, N'K-38-13', N'K-38-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (38, N'K-39-13', N'K-39-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (39, N'K-40-13', N'K-40-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (40, N'K-41-13', N'K-41-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (41, N'K-42-13', N'K-42-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (42, N'K-43-13', N'K-43-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (43, N'K-44-13', N'K-44-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (44, N'K-45-13', N'K-45-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (45, N'K-46-13', N'K-46-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (46, N'K-47-13', N'K-47-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (47, N'K-48-13', N'K-48-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (48, N'K-49-13', N'K-49-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (49, N'K-50-13', N'K-50-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (50, N'K-51-13', N'K-51-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (51, N'K-52-13', N'K-52-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (52, N'K-53-13', N'K-53-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (53, N'K-54-13', N'K-54-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (54, N'K-55-13', N'K-55-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (55, N'K-56-13', N'K-56-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (56, N'K-57-13', N'K-57-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (57, N'K-58-13', N'K-58-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (58, N'K-59-13', N'K-59-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (59, N'K-60-13', N'K-60-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (60, N'K-61-13', N'K-61-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (61, N'K-62-13', N'K-62-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (62, N'K-63-13', N'K-63-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (63, N'K-64-13', N'K-64-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (64, N'K-65-13', N'K-65-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (65, N'K-66-13', N'K-66-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (66, N'K-67-13', N'K-67-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (67, N'K-68-13', N'K-68-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (68, N'K-69-13', N'K-69-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (69, N'K-70-13', N'K-70-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (70, N'K-71-13', N'K-71-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (71, N'K-72-13', N'K-72-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (72, N'K-73-13', N'K-73-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (73, N'K-74-13', N'K-74-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (74, N'K-75-13', N'K-75-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (75, N'K-76-13', N'K-76-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (76, N'K-77-13', N'K-77-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (77, N'K-78-13', N'K-78-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (78, N'K-79-13', N'K-79-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (79, N'K-80-13', N'K-80-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (80, N'K-81-13', N'K-81-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (81, N'K-82-13', N'K-82-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (82, N'K-83-13', N'K-83-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (83, N'K-84-13', N'K-84-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (84, N'K-85-13', N'K-85-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (85, N'K-86-13', N'K-86-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (86, N'K-87-13', N'K-87-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (87, N'K-88-13', N'K-88-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (88, N'K-89-13', N'K-89-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (89, N'K-90-13', N'K-90-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (90, N'K-91-13', N'K-91-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (91, N'K-92-13', N'K-92-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (92, N'K-93-13', N'K-93-13', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_bp] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (93, N'K-94-13', N'K-94-13', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_bp] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_completed] ON 

GO
INSERT [dbo].[dsd_km_completed] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'1', N'ZAPŁACONE / ZREALIZOWANE
', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_completed] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'2', N'NIEZAPŁACONE / NIE ZREALIZOWANE
', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_completed] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_cost] ON 

GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'501', N'KOSZTY DZIAŁALNOŚCI PODSTAWOWEJ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'765', N'POZOSTAŁE KOSZTY OPERACYJNE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'755', N'KOSZTY FINANSOWE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (47, N'401', N'AMORTYZACJA ŚRODKÓW TRWAŁYCH', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (48, N'402', N'AMORTYZACJA WNIP', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (49, N'403', N'AMORTYZACJA ŚRODKÓW TRWAŁYCH O NISKIEJ WARTOŚCI', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (50, N'404', N'AMORTYZACJA ŚT Z LEASINGU FINANSOWEGO', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (52, N'411', N'ZUŻYCIE PALIWA TRAKCYJNEGO ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (53, N'412', N'ZUZYCIE NIETRAKCYJNEGO PALIWA', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (54, N'414', N'MATERIAŁY TECHNICZNE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (55, N'415', N'MATERIAŁY BIUROWE I ADMINISTRACYJNE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (56, N'416', N'ZUZYCIE POZOSTAŁYCH MATERIAŁÓW ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (57, N'417', N'MATERIAŁY REMONTOWE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (59, N'421', N'ZUZYCIE ELEKTRYCZNEJ ENERGII TREKCYJNEJ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (60, N'422', N'ZUZYCIE POZOSTAŁEJ ENERGII ELEKTRYCZNEJ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (61, N'423', N'ZUZYCIE ENERGII CIEPLNEJ ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (62, N'424', N'ZUŻYCIE INNEJ ENERGII', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (64, N'431', N'UDOSTĘPNIENIE LINII PRZEZ PLK', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (65, N'432', N'USŁUGI TRANSPORTOWE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (66, N'433', N'USŁUGI TELEKOMUNIKACYJNE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (67, N'434', N'CZYNSZE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (68, N'435', N'CZYSZCZENIE TABORU I POZOSTAŁE PORZĄDKOWE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (69, N'436', N'USŁUGI AGENTÓW', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (70, N'437', N'USŁUGI REMONTOWE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (71, N'438', N'POZOSTAŁE USŁUGI OBCE', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (72, N'441', N'WYNAGRODZENIA', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (73, N'451', N'NARZUTY NA WYNAGRODZENIA', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (75, N'461', N'ODPIS PODSTAWOWY I ZWIĘKSZENIA NZ ZFŚS', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (76, N'462', N'ŚWIADCZENIA NA RZECZ PRACOWNIKÓW WYNIKAJĄCE Z PRZEPISÓW BHP ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (77, N'463', N'SZKOLENIA PRACOWNIKÓW ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (78, N'464', N'UBEZPIECZENIE PRACOWNIKÓW ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (79, N'465', N'INNE ŚWIADCZENIA NA RZECZ PRACOWNIKÓW ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (81, N'471', N'PODRÓŻE SŁÓŻBOWE ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (82, N'472', N'REKLAMA I PREZENTACJA ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (83, N'473', N'UBEZPIECZENIA MAJATKU ', NULL, N'6', 1)
GO
INSERT [dbo].[dsd_km_cost] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (84, N'474', N'POZOSTAŁE KOSZTY ', NULL, N'6', 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_cost] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_cost_kind] ON 

GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, N'40', N'AMORTYZACJA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'41', N'MATERIAŁY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'42', N'ENERGIA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'43', N'USLUGI OBCE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'44', N'WYNAGRODZENIA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'45', N'NARZUTY NA WYNAGRODZENIA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'46', N'ŚWIADCZENIA NA RZECZ PRZCOWNIKÓW', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'47', N'POZOSTAŁE USŁUGI RODZAJOWE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_cost_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'48', N'PODATKLI I OPŁATY ', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_cost_kind] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_mpk] ON 

GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, N'312000', N'MEN WYDZIAŁ NAPRAW I UTRZYMANIA TABORU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, N'313000', N'MET WYDZIAŁ TECHNIKI', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'316000', N'MEK WYDZIAŁ KONTROLI EKSPLOATACYJNO-TECHNICZNEJ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'206000', N'MHO SEKCJA OBSŁUGI PODRÓŻNYCH', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'126000', N'MZS ZESPÓŁ SKARG I REKLAMACJI ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'127000', N'MZO ZESPÓŁ ORGANIZACYJNY ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'131000', N'MPZ WYDZIAŁ ZATRUDNIENIA I FUNDUSZU WYNAGRODZEŃ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'132000', N'MPO WYDZIAŁ OGÓLNOPRACOWNICZY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'134000', N'MPR WYDZIAŁ ROZWOJU KADR I SPRAW SOCJALNYCH', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'135000', N'MPW ZESPÓŁ WSPÓŁPRACY ZE ZWIAZKAMI ZAWODOWYMI ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'151000', N'MAT WYDZIAŁ TELEINFORMATYKI', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'152000', N'MAN WYDZIAŁ NIERUCHOMOŚCI I LOGISTYKI ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, N'153000', N'MAK ZASPÓŁ KANCELARYJNY ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, N'154000', N'MAS ZESPÓŁ SYSTENÓW ZARZĄDZANIA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, N'155000', N'MZB ZESPÓŁ BHP ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (16, N'206000', N'MHO SEKCJA OBSŁUGI PODRÓŻNYCH ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, N'208000', N'MHD SEKCJA DRÓZYN KONDUKTORSKICH ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (18, N'208300', N'KONDUKTORZY  - SEKCJA DRUŻYN KONDUKTORSKICH ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (19, N'211000', N'MHS WYDZIAŁ SPRZEDAŻY ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (20, N'213000', N'MHK WYDZIAŁ KONTROLI HANDLOWO PRZEWOZOWEJ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (21, N'214000', N'MHT WYDZIAŁ PRZEPISÓW I TARYF', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (22, N'215000', N'MHP WYDZIAŁ PLANOWANIA PRZEWOZÓW ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (23, N'216101', N'K.W. WARSZAWA WSCHODNIA (SEK.WSCH)', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (24, N'216103', N'K.W. WARSZAWA STADION (SEK. WSCH)', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_mpk] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (25, N'216105', N'K.W. ŚRÓDMIEŚCIE (SEK. WSCH)', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_mpk] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_project] ON 

GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, N'100998', N'BEZ PODZIAŁU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'101000', N'BIEŻĄCA DZIAŁALNOŚC OPERACYJNA - KOSZTY DZIAŁALNOŚCI PRZEWOZOWEJ  - KOSZTY  PROWADZENIA RUCHU ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'102000', N'BIEŻĄCA DZIAŁALNOŚC OPERACYJNA - KOSZTY DZIAŁALNOŚCI PODSTAWOWEJ  - KOSZTY  UTRZYMANIA RUCHU ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'103000', N'BIEŻĄCA DZIAŁALNOŚC OPERACYJNA - KOSZTY DZIAŁALNOŚCI PODSTAWOWEJ - POZOSTAŁA DZIAŁALNOŚĆ  OPERACYJNA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'105000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSZTY DZIAŁALNOŚCI PODSTAWOWEJ - KOSZTY CZYSZCZENIA TABORU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'111000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSZTY SPRZEDAŻY - KOSZTY UTRZYMANIA KAS WŁASNYCH', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'112000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSZTY SPRZEDAŻY  - KOSZTY UTRZYMANIA KAS AGENCYJNYCH ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'113000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSZTY SPRZEDAŻY - KOSZTY WINDYKACJI NALEŻNOŚCI ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'114000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSDZTY SPRZEDAŻY - DZIAŁALNOŚC MARKETINGOWA ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_project] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'115000', N'BIEŻĄCA DZIAŁALNOŚĆ OPERACYJNA - KOSZTY SPRZEDAŻY- KOSZTY DZIAŁALNOŚCI HANDLOWEJ ODDZIAŁ MOU', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_project] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_subject_kind] ON 

GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'10', N'URZAD MARSZAŁKOWSKI ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'12', N'KOLEJE MAZOWIECKIE FINANCE AB', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'20', N'PKP POLSKIE LINIE KOLEJOWE S.A', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'21', N'PKP CARGO S.A.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'22', N'PKP NIERUCHOMOŚCI S.A.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'23', N'PKP INTERCITY SP. ZO.O.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'24', N'PKP ENERGETYKA SP. Z.O.O.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'25', N'TELEKOMUNIKACJA KOLEJOWA SP. Z.O.O.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'26', N'PKP INFORMATYKA SP. Z.O.O.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'27', N'KOLEJOWA MEDYCYNA PRACY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (13, N'28', N'PKP SZYBKA KOLEJ MIEJSKA W TRÓJMIEŚCIE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (14, N'29', N'PKP WARSZAWSKA KOLEJ DOJAZDOWA ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (15, N'30', N'PRZEWOZY REGIONALNE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (16, N'31', N'WFOŚIGW', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (17, N'32', N'MAZOWIECKI ZARZĄD NIERUCHOMOŻCI', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (18, N'33', N'MAZOWIECKA SPÓŁKA TABOROWA ', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (19, N'34', N'MAZOWIECKI TEATR MUZYCZNY IM JANA KIEPURY W WARSZAWIE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (20, N'54', N'KPTW NATURA TOUR SP. Z.O.O. GDAŃSK', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (21, N'56', N'DRUKARNIA KOLEJOWA W KRAKOWIE SP. Z.O.O.', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (22, N'60', N'MENNICA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (23, N'70', N'SKM WARSZAWA', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_subject_kind] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (24, N'98', N'POZOSTAŁE JEDNOSTKI ', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_subject_kind] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_tax] ON 

GO
INSERT [dbo].[dsd_km_tax] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'1', N'STANOWIĄCA PRZYCHÓD/KOSZT PODATKOWY', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_tax] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'2', N'NIESTANOWIĄCA PRZYCHODU/KOSZTU PODATKOWEGO TRWALE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_tax] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'3', N'NIESTANOWIĄCA PRZYCHODU PODATKOWEGO PRZEJŚCIOWO', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_tax] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'4', N'WOLNE OD PODATKU', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_tax] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'5', N'KONTO TECHNICZNE', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_tax] OFF
GO
SET IDENTITY_INSERT [dbo].[dsd_km_vehicle] ON 

GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (1, N'99', N'KOSZTY WSPOLNE DLA WSZYSTKICH TYPOW POJAZDOW', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (2, N'11', N'EZT EN 57', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (3, N'21', N'EZT EN 71', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (4, N'31', N'EZT EW 80', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (5, N'41', N'AUTOBUSY SZYNOWE VT 627/628', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (6, N'42', N'AUTOBUSY SZYNOWE SA 135', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (7, N'51', N'ER 75 FLIRT', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (8, N'52', N'EZT EN75 ELF', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (9, N'55', N'KONTO TECHNICZNE', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (10, N'61', N'BPZ PUSH-PULL', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (11, N'62', N'E-583 TRAXX PL', NULL, NULL, 1)
GO
INSERT [dbo].[dsd_km_vehicle] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (12, N'70', N'POJAZDY OBCE - POZOSTAŁE', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsd_km_vehicle] OFF
GO
INSERT [dbo].[dsg_km_costinvoice] ([document_id], [status], [nr_faktury], [data_wystawienia], [data_wplywu_faktury], [data_operacji_gospodarczej], [data_rejestracji], [termin_platnosci], [rodzaj_dokumentu], [okres_sprawozdawczy], [kwota_waluta], [kwota_waluta_data], [kwota_walutowa], [kwota_kurs], [kwota_netto], [kwota_vat], [kwota_brutto], [sposob], [sposob_zwrotu], [pracownik_nr_konta], [nazwisko_dla_zwrotu], [opis], [addinfo], [platnosc_szczegolna], [rodzaj_zamowienia], [tryb_zamowienia], [szczegoly_zamowienia], [postepowanie_nr], [postepowanie_data], [kwota_pozostala_mpk], [korekta], [faktura_skan], [protokol_odbioru], [korekta_faktura], [szablon], [nr_wniosku_zakupowego], [nr_umowy], [pracownik_merytoryczny]) VALUES (CAST(10140 AS Numeric(19, 0)), 110, N'123bc', CAST(0x0000A1F100000000 AS DateTime), CAST(0x0000A1F900000000 AS DateTime), CAST(0x0000A20600000000 AS DateTime), CAST(0x0000A20C00000000 AS DateTime), CAST(0x0000A20700000000 AS DateTime), 30, N'12321', 1, NULL, CAST(213.00 AS Numeric(19, 2)), 1, CAST(213.00 AS Numeric(19, 2)), CAST(0.00 AS Numeric(19, 2)), CAST(213.00 AS Numeric(19, 2)), NULL, NULL, NULL, NULL, N'asdas', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0.00 AS Numeric(19, 2)), NULL, CAST(10055.00 AS Numeric(19, 2)), NULL, NULL, NULL, NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[dsg_km_costinvoice_document_types] ON 

GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'AM', N'AMORTYZACJA ', NULL, NULL, 1, 17)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'BO', N'BILANS OTWARCIA', NULL, NULL, 1, 18)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'DEX', N'DOKUMENT EKSPORTOWY ', NULL, NULL, 1, 19)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'DIM', N'DOKUMENT IMPORTOWY', NULL, NULL, 1, 20)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'DP', N'DOKUMENT PROSTY', NULL, NULL, 1, 21)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'DPB', N'DOKUMENT PROSTY - ROZLICZENIE BILETÓW', NULL, NULL, 1, 22)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'DPZL', N'DOKUMENT PROSTY - ZALICZKI ', NULL, NULL, 1, 23)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKS', N'FAKTURA KORYGUJĄCA SPRZEDAŻ 2005 R', NULL, NULL, 1, 24)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSB', N'SPRZEDAŻ- KASY BILETOWE - KOREKTY', NULL, NULL, 1, 25)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSE', N'SPRZEDAZ- BIURO EKSPOLOATACYJNE - KOREKTY', NULL, NULL, 1, 26)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSH', N'SPRZEDAZ-BIURO HANDLOWE - KOREKTY', NULL, NULL, 1, 27)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSI', N'SPRZEDAZ POZOSTAŁA - KOREKTY', NULL, NULL, 1, 28)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSM', N'SPRZEDAZ MATERIAŁÓW - KOREKTY', NULL, NULL, 1, 29)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKSP', N'SPRZEDAZ USŁ. PRZEW. NA RZCZ INN. KOREKTY', NULL, NULL, 1, 30)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZ ', N'FAKTURA KORYGUJĄCA ZAKUP 2005', NULL, NULL, 1, 31)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZC', N'ZAKUP PALIW DO SAM. OSOB.-KOREKTY', NULL, NULL, 1, 32)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZG', N'ZAKUP DO SPÓŁEK GRUPY PKP-KOREKTY', NULL, NULL, 1, 33)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZM', N'ZAKUP TOWARÓW I MATERIAŁÓW - KOREKTY', NULL, NULL, 1, 34)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZO', N'ZAKUP PALIWA DO SAM. OSOB.-KOREKTY', NULL, NULL, 1, 35)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZT', N'ZAKUP ŚRODKI TRWAŁE I WNiP-KOREKTY', NULL, NULL, 1, 36)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FKZU', N'ZAKUP USŁUG - KOREKTY', NULL, NULL, 1, 37)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FSIU', N'SPRZEDAŻ USŁUG - IMPORT ', NULL, NULL, 1, 38)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVGN', N'ZAKUP - USŁUGI NOCLEGOWE, GASTRONOMIA', NULL, NULL, 1, 39)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVS', N'FASKTURA VAT SPRZEDAŻ 2005 r.', NULL, NULL, 1, 40)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVS6', N'FAKTURA VAT SPRZEDAŻ 2006 R.', NULL, NULL, 1, 41)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVSB', N'SPRZEDAZ - KASY BILETOWE', NULL, NULL, 1, 42)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVSE', N'SPRZEDAZ - BIURO EKSPLOATACYJNE ', NULL, NULL, 1, 43)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVSH', N'SPRZEDAZ- BIURO HANDLOWE', NULL, NULL, 1, 44)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVSI', N'SPRZEDAZ POZOSTAŁA - KOREKTY', NULL, NULL, 1, 45)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVSM', N'SPRZEDAZ MATERIAŁÓW ', NULL, NULL, 1, 46)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZ', N'FAKTURA VAT ZAKUP 2005 R.', NULL, NULL, 1, 47)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZ6', N'FAKTURA VAT ZAKUP 2006 R.', NULL, NULL, 1, 48)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZG', N'ZAKUP DO SPÓŁEK GRUPY PKP', NULL, NULL, 1, 49)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZL', N'ZAKUP USŁUG - LEASING SAM. OSOBOWYCH', NULL, NULL, 1, 50)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZM', N'ZAKUP TOWARÓW I MATERIAŁÓW', NULL, NULL, 1, 51)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZO', N'ZAKUP PALIWA DO SAM. OSOBOWYCH', NULL, NULL, 1, 52)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZS', N'ZAKUP - FUNDUSZ ŚWIADCZEŃ SOCJALNYCH ', NULL, NULL, 1, 53)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZT', N'ZAKUP - ŚRODKI TRWAŁE I WNIP', NULL, NULL, 1, 54)
GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'FVZU', N'ZAKUP USŁUG ', NULL, NULL, 1, 55)
GO
SET IDENTITY_INSERT [dbo].[dsg_km_costinvoice_document_types] OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_km_costinvoice_multiple_value] ON 

GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3651 AS Numeric(18, 0)), CAST(10087 AS Numeric(18, 0)), N'BP', N'3668')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3652 AS Numeric(18, 0)), CAST(10087 AS Numeric(18, 0)), N'BP', N'3669')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3653 AS Numeric(18, 0)), CAST(10094 AS Numeric(18, 0)), N'BP', N'3682')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3654 AS Numeric(18, 0)), CAST(10094 AS Numeric(18, 0)), N'BP', N'3683')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3655 AS Numeric(18, 0)), CAST(10095 AS Numeric(18, 0)), N'BP', N'3684')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3656 AS Numeric(18, 0)), CAST(10095 AS Numeric(18, 0)), N'BP', N'3685')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3657 AS Numeric(18, 0)), CAST(10096 AS Numeric(18, 0)), N'BP', N'3686')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3658 AS Numeric(18, 0)), CAST(10096 AS Numeric(18, 0)), N'BP', N'3687')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3665 AS Numeric(18, 0)), CAST(10097 AS Numeric(18, 0)), N'BP', N'3692')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3666 AS Numeric(18, 0)), CAST(10097 AS Numeric(18, 0)), N'BP', N'3693')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3667 AS Numeric(18, 0)), CAST(10099 AS Numeric(18, 0)), N'BP', N'3694')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3668 AS Numeric(18, 0)), CAST(10099 AS Numeric(18, 0)), N'BP', N'3695')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3673 AS Numeric(18, 0)), CAST(10103 AS Numeric(18, 0)), N'BP', N'3700')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3675 AS Numeric(18, 0)), CAST(10105 AS Numeric(18, 0)), N'BP', N'3702')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3686 AS Numeric(18, 0)), CAST(10134 AS Numeric(18, 0)), N'BP', N'3713')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(11 AS Numeric(18, 0)), CAST(10080 AS Numeric(18, 0)), N'BP', N'26')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(12 AS Numeric(18, 0)), CAST(10080 AS Numeric(18, 0)), N'BP', N'27')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(13 AS Numeric(18, 0)), CAST(10083 AS Numeric(18, 0)), N'BP', N'10')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(14 AS Numeric(18, 0)), CAST(10083 AS Numeric(18, 0)), N'BP', N'4')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3669 AS Numeric(18, 0)), CAST(10100 AS Numeric(18, 0)), N'BP', N'3696')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3670 AS Numeric(18, 0)), CAST(10100 AS Numeric(18, 0)), N'BP', N'3697')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3661 AS Numeric(18, 0)), CAST(10098 AS Numeric(18, 0)), N'BP', N'3690')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3662 AS Numeric(18, 0)), CAST(10098 AS Numeric(18, 0)), N'BP', N'3691')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3676 AS Numeric(18, 0)), CAST(10114 AS Numeric(18, 0)), N'BP', N'3703')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3677 AS Numeric(18, 0)), CAST(10114 AS Numeric(18, 0)), N'BP', N'3704')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3678 AS Numeric(18, 0)), CAST(10127 AS Numeric(18, 0)), N'BP', N'3705')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3680 AS Numeric(18, 0)), CAST(10129 AS Numeric(18, 0)), N'BP', N'3707')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3685 AS Numeric(18, 0)), CAST(10131 AS Numeric(18, 0)), N'BP', N'3709')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3687 AS Numeric(18, 0)), CAST(10136 AS Numeric(18, 0)), N'BP', N'3714')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3689 AS Numeric(18, 0)), CAST(10140 AS Numeric(18, 0)), N'BP', N'3716')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3690 AS Numeric(18, 0)), CAST(10140 AS Numeric(18, 0)), N'BP', N'3715')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3674 AS Numeric(18, 0)), CAST(10104 AS Numeric(18, 0)), N'BP', N'3701')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3679 AS Numeric(18, 0)), CAST(10128 AS Numeric(18, 0)), N'BP', N'3706')
GO
INSERT [dbo].[dsg_km_costinvoice_multiple_value] ([ID], [DOCUMENT_ID], [FIELD_CN], [FIELD_VAL]) VALUES (CAST(3682 AS Numeric(18, 0)), CAST(10130 AS Numeric(18, 0)), N'BP', N'3708')
GO
SET IDENTITY_INSERT [dbo].[dsg_km_costinvoice_multiple_value] OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_km_currency] ON 

GO
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'PLN ', N'polski złoty', NULL, NULL, 1, 1)
GO
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'EUR ', N'euro', NULL, NULL, 1, 2)
GO
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'USD ', N'dolar amerykański', NULL, NULL, 1, 3)
GO
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'CHF ', N'frank szwajcarski', NULL, NULL, 1, 4)
GO
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'GBP ', N'funt szterling', NULL, NULL, 1, 5)
GO
SET IDENTITY_INSERT [dbo].[dsg_km_currency] OFF
GO
ALTER TABLE [dbo].[dsd_km_action_kind] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_analitycs] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_bp] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_completed] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_cost] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_cost_kind] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_mpk] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_project] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_subject_kind] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_tax] ADD  DEFAULT ((1)) FOR [available]
GO
ALTER TABLE [dbo].[dsd_km_vehicle] ADD  DEFAULT ((1)) FOR [available]
GO
