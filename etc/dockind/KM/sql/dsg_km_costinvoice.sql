CREATE TABLE [dbo].[dsg_km_costinvoice](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wystawienia] [datetime] NULL,
	[data_wplywu_faktury] [datetime] NULL,
	[data_operacji_gospodarczej] [datetime] NULL,
	[data_rejestracji] [datetime] NULL,
	[termin_platnosci] [datetime] NULL,
	[rodzaj_dokumentu] [int] NULL,
	[okres_sprawozdawczy] [varchar](30) NULL,
	[kwota_waluta] [int] NULL,
	[kwota_waluta_data] [datetime] NULL,
	[kwota_walutowa] [numeric](19, 2) NULL,
	[kwota_kurs] [float] NULL,
	[kwota_netto] [numeric](19, 2) NULL,
	[kwota_vat] [numeric](19, 2) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[sposob] [int] NULL,
	[sposob_zwrotu] [int] NULL,
	[pracownik_nr_konta] [varchar](100) NULL,
	[nazwisko_dla_zwrotu] [varchar](1024) NULL,
	[opis] [varchar](4096) NULL,
	[addinfo] [varchar](8000) NULL,
	[platnosc_szczegolna] [tinyint] NULL,
	[rodzaj_zamowienia] [int] NULL,
	[tryb_zamowienia] [int] NULL,
	[szczegoly_zamowienia] [int] NULL,
	[postepowanie_nr] [varchar](512) NULL,
	[postepowanie_data] [datetime] NULL,
	[kwota_pozostala_mpk] [numeric](19, 2) NULL,
	[korekta] [tinyint] NULL,
	[faktura_skan] [numeric](19, 2) NULL,
	[protokol_odbioru] [numeric](19, 2) NULL,
	[korekta_faktura] [int] NULL,
	[szablon] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

create view ds_document_internals as
select doc.id as document_id, doc.*,doc_out.OFFICENUMBER from ds_document doc
join dso_out_document doc_out on doc.ID=doc_out.ID
where doc.dockind_id=(select id from DS_DOCUMENT_KIND where cn='normal_int');


CREATE TABLE [dbo].[dsg_km_costinvoice_document_types](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__dsg_km_costinvoice_document_types] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZU ', N'Faktura zakupu us�ug', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZT ', N'Faktura zakupu �rodk�w trwa�ych', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZM ', N'Faktura zakupu materia��w do magazynu', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZIM', N'Faktura zakupu materia��w z importu', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZUE', N'Faktura zakupu materia��w z UE', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FVZP ', N'Zakup towar�w i us�ug - krajowe', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'IU  ', N'Import us�ug - dostawcy zagraniczni', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PNZ ', N'Pozosta�e nabycia - dostawcy zagranicz', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FIP ', N'Zakupy inwestycyjne - PLN', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FIW ', N'Zakupy inwestycyjne w walucie obcej', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PZP ', N'Zaliczki pracownik na zakup tow. i us�', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PZW ', N'Zal. z dz.poz. zakup towar�w i us� W', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FPP ', N'Zakup towar�w i us�ug - pracownik PLN', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PZP ', N'Zaliczki pracownik na zakup towar�w i us', NULL, NULL, 1)
INSERT [dbo].[dsg_km_costinvoice_document_types] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZPP ', N'Zal. z poz. dz. zakup towar�w i us�ug', NULL, NULL, 1)

CREATE TABLE [dbo].[dsg_km_currency](
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK__dsg_currency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PLN ', N'polski z�oty', NULL, NULL, 1)
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'EUR ', N'euro', NULL, NULL, 1)
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'USD ', N'dolar ameryka�ski', NULL, NULL, 1)
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'CHF ', N'frank szwajcarski', NULL, NULL, 1)
INSERT [dbo].[dsg_km_currency] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'GBP ', N'funt szterling', NULL, NULL, 1)


CREATE TABLE dsg_km_costinvoice_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);