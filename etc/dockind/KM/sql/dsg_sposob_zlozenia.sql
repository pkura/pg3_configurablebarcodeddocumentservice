CREATE TABLE [dbo].[dsg_sposob_zlozenia](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_sposob_zlozenia ADD PRIMARY KEY (ID);
create index dsg_sposob_zlozenia_index on dsg_sposob_zlozenia (ID);

SET IDENTITY_INSERT [dsg_sposob_zlozenia] ON

insert into dsg_sposob_zlozenia (id, cn, title, centrum, refValue, available) values (1, 'LIST', 'Listownie', null, null, 1);
insert into dsg_sposob_zlozenia (id, cn, title, centrum, refValue, available) values (1, 'EMAIL', 'E-mail', null, null, 1);
insert into dsg_sposob_zlozenia (id, cn, title, centrum, refValue, available) values (1, 'TEL', 'Telefonicznie', null, null, 1);
insert into dsg_sposob_zlozenia (id, cn, title, centrum, refValue, available) values (1, 'KASA', 'W kasie biletowej', null, null, 1);
insert into dsg_sposob_zlozenia (id, cn, title, centrum, refValue, available) values (1, 'OSOB', 'Osobi�cie', null, null, 1);

SET IDENTITY_INSERT [dsg_sposob_zlozenia] OFF