CREATE TABLE [DSG_SANTANDER](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[data_wystawienia] [datetime] NULL,
	[data_dostawy] [datetime] NULL,
	[dostawca] [numeric](19, 0) NULL,
	[kwota] [float] NULL,
	[opis_zamowienia] [varchar](800) NULL,
	[nr_zamowienia] [numeric](19, 0) NULL,
	[centrum_kosztowe] [int]) NULL	
);

CREATE TABLE [DSG_SANTANDER_MULTIPLE_VALUE](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_CN] [varchar](20) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);