alter table DS_PG_ZADANIE_ODBIORCA ADD COLUMN USER_STATUS	NUMERIC(38,0) default 0;
ALTER TABLE DS_PG_ZADANIE_ODBIORCA ADD COLUMN USER_REALIZATION_DATE date;
ALTER TABLE DS_PG_ZADANIE_ODBIORCA ADD COLUMN  DOCUMENT_ID	numeric(19,0);
ALTER TABLE DS_PG_ZADANIE_ODBIORCA ADD COLUMN  DELEGATE_USER_ID	numeric(19,0);
ALTER TABLE DS_PG_ZADANIE_ODBIORCA ADD COLUMN  DELEGATE_MESSAGE	varchar (500);
ALTER TABLE DS_PG_ZADANIE_ODBIORCA ADD COLUMN USER_REALIZATION_DATE_CHANGED date;

CREATE OR REPLACE VIEW USERS_VIEV (ID, CN,TITLE,AVAILABLE,CENTRUM,REFVALUE) AS
SELECT ID,NAME, FIRSTNAME || ' ' || LASTNAME ||' (' || EXTERNALNAME || ')' ,1,0,null
FROM DS_USER where DELETED = 'f';