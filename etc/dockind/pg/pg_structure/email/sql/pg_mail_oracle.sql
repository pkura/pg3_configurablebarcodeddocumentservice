CREATE TABLE DOCUMENT_IN_EMAIL
(
	DOCUMENT_ID number(19, 0) NOT NULL,
	typ_slownika integer NULL,
	temat varchar2(50),
	tresc varchar2(50),
	wyslac integer NULL,
	typ_slownika_kopia integer NULL,
	typ_slownika_ukrytaKopia integer NULL,
	format varchar2(50) NULL,
	typ integer NULL,
	dostep integer NULL,
	wyslano integer NULL);
commit;

CREATE or Replace view V_DSO_PERSON
AS
SELECT     ID AS id, DISCRIMINATOR, TITLE, FIRSTNAME, LASTNAME, ORGANIZATION, ORGANIZATIONDIVISION, STREET, ZIP, PESEL, LOCATION, NIP, REGON, EMAIL, 
                      COUNTRY, REMARKS, FAX
FROM         DSO_PERSON
WHERE     (DISCRIMINATOR = 'person');

commit;