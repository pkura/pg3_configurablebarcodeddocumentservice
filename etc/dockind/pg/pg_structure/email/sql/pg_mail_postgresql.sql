CREATE TABLE DOCUMENT_IN_EMAIL
(
	DOCUMENT_ID NUMERIC(19, 0) NOT NULL,
	typ_slownika integer NULL,
	temat VARCHAR(50),
	tresc VARCHAR(50),
	wyslac integer NULL,
	typ_slownika_kopia integer NULL,
	typ_slownika_ukrytaKopia integer NULL,
	format VARCHAR(50) NULL,
	typ integer NULL,
	dostep integer NULL,
	wyslano integer NULL
);


CREATE or Replace view V_DSO_PERSON
AS
SELECT     ID AS id, DISCRIMINATOR, TITLE, FIRSTNAME, LASTNAME, ORGANIZATION, ORGANIZATIONDIVISION, STREET, ZIP, PESEL, LOCATION, NIP, REGON, EMAIL, 
                      COUNTRY, REMARKS, FAX
FROM         DSO_PERSON
WHERE     (DISCRIMINATOR = 'person');


CREATE TABLE ds_pg_dokuments_multi(
	ID SERIAL PRIMARY KEY NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

