
-----------------(686 <- 683)[migration from oracle 2 postgresql]--------------

DO $$
DECLARE 
  v_table1_exists bigint;  
  v_table2_exists bigint;  
  v_table3_exists bigint; 
  v_table4_exists bigint;
  v_table5_exists bigint;
BEGIN
  Select count(*) into v_table1_exists
    FROM pg_catalog.pg_tables where  tablename = 'ds_ar_brakowanie';
    
  if (v_table1_exists > 0) then
       alter table DS_AR_BRAKOWANIE add column typ INTEGER;
       alter table DS_AR_BRAKOWANIE add column MIESCEUTWORZENIA varchar(400);
       alter table DS_AR_BRAKOWANIE add column DODATKOWE_METADANE NUMERIC(18,0);
      
  end if;

  Select count(*) into v_table2_exists
    FROM pg_catalog.pg_tables  where  tablename = 'ds_ar_inventory_transf_and_rec';
  if (v_table2_exists > 0) then
       alter table ds_ar_inventory_transf_and_rec add column typ INTEGER;
       alter table ds_ar_inventory_transf_and_rec add column MIESCEUTWORZENIA varchar(400);
       alter table ds_ar_inventory_transf_and_rec add column DODATKOWE_METADANE NUMERIC(18,0);
      
  end if;

  Select count(*) into v_table3_exists
    FROM pg_catalog.pg_tables  where  tablename = 'DS_AR_SPIS_DOK_DO_AP';
  if (v_table3_exists > 0) then
       alter table DS_AR_SPIS_DOK_DO_AP add column typ INTEGER;
       alter table DS_AR_SPIS_DOK_DO_AP add column MIESCEUTWORZENIA varchar(400);
       alter table DS_AR_SPIS_DOK_DO_AP add column DODATKOWE_METADANE NUMERIC(18,0);
      
  end if;

  Select count(*) into v_table4_exists
    FROM pg_catalog.pg_tables  where  tablename = 'DS_AR_WNIOSEK_UDOSTEP';
  if (v_table4_exists > 0) then
       alter table DS_AR_WNIOSEK_UDOSTEP add column typ INTEGER;
       alter table DS_AR_WNIOSEK_UDOSTEP add column MIESCEUTWORZENIA varchar(400);
       alter table DS_AR_WNIOSEK_UDOSTEP add column DODATKOWE_METADANE NUMERIC(18,0);
      
  end if;

  Select count(*) into v_table5_exists
    FROM pg_catalog.pg_tables  where  tablename = 'DS_AR_WYCOFANIE';
  if (v_table5_exists > 0) then
       alter table DS_AR_WYCOFANIE add column typ INTEGER;
       alter table DS_AR_WYCOFANIE add column MIESCEUTWORZENIA varchar(400);
       alter table DS_AR_WYCOFANIE add column DODATKOWE_METADANE NUMERIC(18,0);
      
  end if;
END $$;




CREATE TABLE DS_PG_INT_DOC (
	DOCUMENT_ID NUMERIC(18, 0) NOT NULL,
	MIESCEUTWORZENIA varchar(400),
	DODATKOWE_METADANE NUMERIC(18,0)
);
	
--spis zdawczoodbiorczy

--BĹ�Ä„D: relacja "ds_ar_dodatkowe_metadane" nie istnieje

DROP TABLE IF EXISTS ds_ar_dodatkowe_metadane CASCADE;
CREATE TABLE ds_ar_dodatkowe_metadane (
  ID          SERIAL PRIMARY KEY NOT NULL,
  TEXTDATA    VARCHAR(4000)      NULL,
  NAZWAPOLA   VARCHAR(100)       NULL
);

--BĹ�Ä„D:  relacja "dsi_import_table" nie istnieje

DROP TABLE IF EXISTS DSI_IMPORT_TABLE CASCADE;
CREATE TABLE DSI_IMPORT_TABLE (
  IMPORT_ID   		SERIAL PRIMARY KEY NOT NULL,
  USER_IMPORT_ID    	VARCHAR(50)      	NULL,
  DOCUMENT_KIND   	VARCHAR(100)     	NULL,
  IMPORT_KIND   	NUMERIC(38, 0)     	DEFAULT 0,
  SUBMIT_DATE   	TIMESTAMP (6)     	NULL,
  IMPORT_STATUS   	NUMERIC(38, 0)     	DEFAULT 0,
  PROCESS_DATE   	TIMESTAMP (6)     	NULL,
  DOCUMENT_ID   	NUMERIC(38, 0)     	DEFAULT -1,
  ERR_CODE		   	VARCHAR(200)     	NULL,
  FILE_PATH		   	VARCHAR(512)     	NULL,
  BOX_CODE		   	VARCHAR(20)     	NULL,
  FEEDBACK_STATUS   NUMERIC(38, 0)     	DEFAULT 0,
  FEEDBACK_DATE   	TIMESTAMP (6)   	NULL,
  FEEDBACK_RESPONSE VARCHAR(100)     	NULL
);

create index dsi_import_table_1 on dsi_import_table (import_status);

--org.postgresql.util.PSQLException: BĹ�Ä„D: wartoĹ›Ä‡ zbyt dĹ‚uga dla typu znakowego zmiennego (20)

ALTER TABLE ds_document_kind ALTER COLUMN tablename TYPE varchar(40);
ALTER TABLE ds_document_kind ALTER COLUMN tablename SET NOT NULL;


----

create table dsg_pg_barcode_pool(
  id NUMERIC primary key,
  document_id NUMERIC,
  startval NUMERIC,
  endval NUMERIC,
  creationDate date,
  user_id NUMERIC,
  prefix VARCHAR(30)
);


CREATE sequence dsg_pg_barcode_pool_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;

--CREATE SEQUENCE dsg_pg_barcode_pool_id
--  START WITH 1
--  INCREMENT BY 1
--  CACHE 10000000000000;

  create table dsg_pg_barcode_generator(
  pointer_value NUMERIC
);

insert into dsg_pg_barcode_generator values (0);

insert into ds_acceptance_division (id, GUID, NAME, code, DIVISIONTYPE, ogolna) values (1, 'root', 'PG', 'root', 'ac.division', 't');


drop sequence dsg_pg_barcode_pool_id
CREATE sequence dsg_pg_barcode_pool_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;

drop   TABLE DSG_PG_BARCODE_POOL_PREFIX;
  CREATE TABLE DSG_PG_BARCODE_POOL_PREFIX
   (	PREFIX character varying(10)
   );
  drop  TABLE DSG_PG_BARCODES_CHECK; 
  
    CREATE TABLE DSG_PG_BARCODES_CHECK
   (	DOCUMENT_ID numeric (18,0), 
	TOCHECK integer
   );
 drop  TABLE DSG_PG_BARCODE_ORDER;   
   CREATE TABLE DSG_PG_BARCODE_ORDER
   (	DOCUMENT_ID numeric (18,0), 
	STATUS integer, 
	USER_AUTHOR numeric (18,0),
	ILOSC numeric (18,0), 
	CPP integer, 
	START_BARCODE character varying(50), 
	END_BARCODE character varying(50),
	ODBIOR_OSOBISTY boolean, 
	DOC_DATE DATE, 
	CPP_DIVISION numeric (18,0),
	START_BARCODE_CHECK character varying(50),
	BARCODES_CHECK boolean
   ) ;
   
   drop  TABLE DSG_PG_BARCODE_POOL;   
  CREATE TABLE DSG_PG_BARCODE_POOL 
   (	ID  numeric (18,0) PRIMARY KEY, 
	DOCUMENT_ID  numeric (18,0), 
	STARTVAL integer, 
	ENDVAL integer, 
	CREATIONDATE DATE, 
	USER_ID numeric (18,0),  
	PREFIX character varying(50), 
	START_BARCODE_CHECK character varying(50)
  );




DROP TABLE ds_pg_drag_doc;
CREATE TABLE ds_pg_drag_doc
(
  DOCUMENT_ID numeric(19,0) NOT NULL,
  FOLDER_ID numeric
)

ALTER TABLE ds_xes_log_data DROP COLUMN "ID";
ALTER TABLE ds_xes_log_data ADD COLUMN id numeric(19,0) NOT NULL;

CREATE sequence seq_dso_ds_xes_log_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 630
INCREMENT BY 1;


CREATE SEQUENCE SEQ_DSO_DS_XES_LOG_DATA_ID
MINVALUE 1 MAXVALUE 9999999999999999999999999999 
INCREMENT BY 1 

ALTER TABLE ds_attachment DROP COLUMN editable;
ALTER TABLE ds_attachment ADD COLUMN editable boolean;

ALTER TABLE ds_attachment_revision DROP COLUMN index_status_id;
ALTER TABLE ds_attachment_revision ADD COLUMN index_status_id numeric(19,0) NOT NULL DEFAULT 0;

ALTER TABLE ds_pg_modify_case_doc ADD COLUMN dsdivision numeric(19,0);

UPDATE ds_folder
   SET systemname='null'
 WHERE id=0;

ALTER TABLE ds_pg_in_doc
ADD COLUMN zablokuj boolean DEFAULT false;
update ds_folder_permission  set subject  = '*' ,name = 'read' , subjecttype='any'


update ds_folder_permission  set subject  = '*' ,name = 'read' , subjecttype='any';

DELETE FROM ds_folder_permission
;

INSERT INTO ds_folder_permission(
            folder_id, subject, subjecttype, name, negative)
    VALUES (0, '*', 'any', 'read', false);

INSERT INTO ds_folder_permission(
            folder_id, subject, subjecttype, name, negative)
    VALUES (1, '*', 'any', 'read', false);

    INSERT INTO ds_folder_permission(
            folder_id, subject, subjecttype, name, negative)
    VALUES (2, '*', 'any', 'read', false);

    INSERT INTO ds_folder_permission(
            folder_id, subject, subjecttype, name, negative)
    VALUES (12, '*', 'any', 'read', false);
	

    
 Create or replace view USER_TASKLIST_VIEV as 
  SELECT DISTINCT
    t.document_id as ID,
    t.assigned_resource AS USER_TASK ,
    t.document_type     AS DOC_TYPE,
    dc.name             AS DOC_KIND_NAME,
    t.document_id       AS DOC_ID,
    t.office_number     AS DOC_OFFICENUMBER,
    t.document_ctime    AS DOC_DATA,
    t.summary           AS DOC_DESCRIPTION,
    din.barcode || dout.barcode  AS DOC_BARCODE ,
    t.case_office_id    AS DOC_CASEDOCUMENTID,
    t.last_user         AS DOC_DEKRETUJACY ,
    t.document_author   AS DOC_AUTOR,
    din.sender::varchar || dout.sender::varchar AS DOC_SENDER ,  
    doc.dockind_id      AS DOC_KINDID,
    doc.inPackage       as DOC_IN_PACKAGE
  FROM dsw_jbpm_tasklist t
  LEFT JOIN ds_document doc
  ON (t.document_id = doc.id)
  LEFT JOIN dso_in_document din
  ON ( doc.id = din.id)
  LEFT JOIN dso_OUT_document dout
  ON ( doc.id = dout.id)
  LEFT JOIN ds_document_kind dc
  ON (doc.dockind_id = dc.id)
  ORDER BY t.document_id desc;

  
  ALTER TABLE ds_package_documents
   ADD COLUMN lokalizacja_paczki character varying(4000);
   
   ALTER TABLE ds_package_documents
   ADD COLUMN zablokuj boolean;

   ALTER TABLE ds_pg_in_doc
   drop COLUMN anonim; 
   
   ALTER TABLE ds_pg_in_doc
   ADD COLUMN anonim boolean;

   ALTER TABLE ds_pg_in_doc
   drop COLUMN zewnetrzny; 
   
   ALTER TABLE ds_pg_in_doc
   ADD COLUMN zewnetrzny boolean;

    ALTER TABLE DS_AR_SPIS_DOK_DO_AP
   drop COLUMN numer; 
   
   ALTER TABLE DS_AR_SPIS_DOK_DO_AP
   ADD COLUMN numer character varying(50);
   CREATE TABLE utp_search_filter(
	id SERIAL PRIMARY KEY NOT NULL,
	document_id int NULL,
	title varchar(50) NULL,
	description varchar (50) NULL,
	attribute varchar (50) NULL,
	author varchar(50) NULL,
	attachmentBarcode varchar(50) NULL,
	startCtime date NULL,
	endCtime date NULL,
	startMtime date NULL,
	endMtime date NULL,
	lastRemark varchar(50) NULL,
	accessedBy varchar(50) NULL,
	filterName varchar(50) NULL,
	documentKindIds varchar(50) NULL
);

--05.01.2015
DROP FUNCTION insert_to_ds_case_to_case() CASCADE;

CREATE OR REPLACE FUNCTION insert_to_ds_case_to_case()
  RETURNS trigger AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_CASE_TO_CASE_ID');--@ignore-semicolon
  END IF ;--@ignore-semicolon
 Return NEW;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION insert_to_ds_case_to_case()
  OWNER TO postgres;
  
 CREATE TRIGGER TRG_DS_CASE_TO_CASE
 BEFORE INSERT ON DS_CASE_TO_CASE
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_CASE_TO_CASE();

 
 create sequence DSI_IMPORT_TABLE_ID;
create table DSI_IMPORT_ATTRIBUTES 
(
  attribute_id numeric(18,0) primary key ,
  import_id integer,
  attribute_cn varchar(50),
  attribute_value varchar(1000),
  set_code varchar(100),
  line_no integer
);
create sequence DSI_IMPORT_ATTRIBUTES_ID;
create table DSI_IMPORT_BLOB
(
  attachment_id numeric(18,0) primary key ,
  import_id integer,
  attachment_name varchar(50) default 'skan',
  attachment bytea
);
 create table DSI_IMPORT_STATUS
 (
  import_status_id numeric(18,0) primary key,
  status_description varchar(100)
 );

 insert into DSI_IMPORT_STATUS values (0,'Status poczštkowy');
insert into DSI_IMPORT_STATUS values (1, 'Gotowy do importu');
insert into DSI_IMPORT_STATUS values (2, 'Zaimportowany');
insert into DSI_IMPORT_STATUS values (3, 'Do skasowania');
insert into DSI_IMPORT_STATUS values (-1, 'Odrzucony');

create index dsi_import_attributes_1 on dsi_import_attributes (import_id);
create index dsi_import_blob_1 on dsi_import_blob (import_id);

 --07.01.2014
 ALTER TABLE ds_pg_zadanie_odbiorca DROP COLUMN delegate_user_id;

ALTER TABLE ds_pg_zadanie_odbiorca ADD COLUMN numeric(19,0) character varying(62);

ALTER TABLE ds_pg_int_zadanie_doc
   ALTER COLUMN cykliczne SET DEFAULT false;
   
   update ds_pg_int_zadanie_doc set cykliczne = false;

   
    ALTER TABLE DS_PG_IN_DOC DROP COLUMN show_email;

ALTER TABLE DS_PG_IN_DOC ADD COLUMN show_email boolean default false;
  
 drop sequence dsg_pg_barcode_pool_id
CREATE sequence dsg_pg_barcode_pool_id
MINVALUE 1
MAXVALUE 999999999999999999
START WITH 1
INCREMENT BY 1;
 
   


CREATE TABLE DS_ATTACHMENT_SIGNATURE (
	ID              NUMERIC(18,0) NOT NULL SERIAL,
	REVISION_ID     NUMERIC(18,0) NOT NULL,
	CTIME           DATE NOT NULL,
	AUTHOR          character Varying (62) NOT NULL,
	CONTENTDATA     bytea
  );


 CREATE SEQUENCE ds_attachment_signature_id
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
drop   TABLE DSG_PG_BARCODE_POOL_PREFIX;
  CREATE TABLE DSG_PG_BARCODE_POOL_PREFIX
   (    PREFIX character varying(10)
   );
  drop  TABLE DSG_PG_BARCODES_CHECK;
    CREATE TABLE DSG_PG_BARCODES_CHECK
   (    DOCUMENT_ID numeric (18,0),
    TOCHECK integer
   );
drop  TABLE DSG_PG_BARCODE_ORDER;  
   CREATE TABLE DSG_PG_BARCODE_ORDER
   (    DOCUMENT_ID numeric (18,0),
    STATUS integer,
    USER_AUTHOR numeric (18,0),
    ILOSC numeric (18,0),
    CPP integer,
    START_BARCODE character varying(50),
    END_BARCODE character varying(50),
    ODBIOR_OSOBISTY boolean,
    DOC_DATE DATE,
    CPP_DIVISION numeric (18,0),
    START_BARCODE_CHECK character varying(50),
    BARCODES_CHECK boolean
   ) ;
  
   drop  TABLE DSG_PG_BARCODE_POOL;  
  CREATE TABLE DSG_PG_BARCODE_POOL
   (    ID  numeric (18,0) PRIMARY KEY,
    DOCUMENT_ID  numeric (18,0),
    STARTVAL integer,
    ENDVAL integer,
    CREATIONDATE DATE,
    USER_ID numeric (18,0), 
    PREFIX character varying(50),
    START_BARCODE_CHECK character varying(50)
  );
  
  CREATE TABLE DS_ATTACHMENT_SIGNATURE 
   (	
	ID numeric(19,0) NOT NULL , 
	REVISION_ID numeric(19,0) NOT NULL , 
	CTIME TIMESTAMP (6) NOT NULL , 
	AUTHOR VARCHAR(62) NOT NULL , 
	CORRECTLYVERIFIED boolean, 
	CONTENTDATA bytea
   );

--20.01.2014

ALTER TABLE ds_pg_in_doc
   ADD COLUMN "IDENTYFIKATOR_PACZKI" character varying(200);
ALTER TABLE ds_pg_in_doc
   ADD COLUMN "DOC_FROM_PACKAGE" boolean DEFAULT false;


 CREATE SEQUENCE ds_attachment_signature_id
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
  
alter table ds_document add column documentbarcode character varying(25);

--funkcja czywybrakowac dla PostgreSQL, 11-02-2015, Artur Gdula 

CREATE OR REPLACE FUNCTION czywybrakowac(in_date timestamp without time zone, archcat character varying)
  RETURNS integer AS
$BODY$
DECLARE
  rokarchiwizacji    INT;
  lataprzechowywania INT;
BEGIN
  rokarchiwizacji = date_part('year',  in_date);
  IF (LOWER(archcat) = 'b')
  THEN
    lataprzechowywania = 25;
  ELSIF REPLACE(lower(archcat), 'b', '') = 'c' OR REPLACE(lower(archcat), 'b', '') = 'e'
    THEN
      lataprzechowywania = 2;
  ELSE
    lataprzechowywania = CAST(REPLACE(REPLACE(lower(archcat), 'b', ''), 'e', '') AS INT);
  END IF;

  lataprzechowywania = ABS(lataprzechowywania);

  IF (date_part('year', current_date) >= rokarchiwizacji + 1 + lataprzechowywania)
  THEN RETURN 1;
  ELSE RETURN 0;
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION czywybrakowac(timestamp without time zone, character varying)
  OWNER TO pg;

  
--zmiana kolumn z bit na boolean w tabeli ds_ar_wniosek_udostep, 11-02-2015, Artur Gdula 
ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN is_udostepn_akt_na_miejscu;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN is_udostepn_akt_na_miejscu boolean;
ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN is_wypozyczenie_fizyczne;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN is_wypozyczenie_fizyczne boolean;
ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN is_odwzorowanie_cyfrowe, DROP COLUMN is_kserokopia;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN is_odwzorowanie_cyfrowe boolean,ADD COLUMN is_kserokopia boolean ;
ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN is_nadanie_upr_elektr, DROP COLUMN inne_ch, DROP COLUMN is_potwierdzony_zwrot;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN is_nadanie_upr_elektr boolean,ADD COLUMN inne_ch boolean,ADD COLUMN is_potwierdzony_zwrot boolean;

ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN is_wnioskodawca_zewnetrzny;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN is_wnioskodawca_zewnetrzny boolean;

--zmiana kolumn z smallint na boolean w tabeli ds_ar_wniosek_udostep, 11-02-2015, Artur Gdula 
ALTER TABLE ds_ar_wniosek_udostep DROP COLUMN wniosk_zew_os_praw;
ALTER TABLE ds_ar_wniosek_udostep ADD COLUMN wniosk_zew_os_praw boolean;

--zmiana typu kolumny z numeric na chracter varying
ALTER TABLE ds_ar_teczki_do_ap DROP COLUMN idteczki;
ALTER TABLE ds_ar_teczki_do_ap ADD COLUMN idteczki character varying(84);

--zmiana smallint na boolean
ALTER TABLE ds_ar_brakowanie_pozycja DROP COLUMN czy_wybrakowany, DROP COLUMN czy_usuniety;
ALTER TABLE ds_ar_brakowanie_pozycja ADD COLUMN czy_wybrakowany boolean,ADD COLUMN czy_usuniety boolean ;




--Grzesiek 02-12-2015

INSERT INTO ds_archives_team(
            id, cn, title, centrum, refvalue, available)
    VALUES (nextval('ds_archives_team_id_seq'), 1, 'Politechnika Gdańska', 1, null, true);
    
    


CREATE OR REPLACE VIEW v_dso_rwaa AS 
 SELECT dso_rwa.id,
    concat(concat(concat(concat(COALESCE(dso_rwa.digit1, ''::character varying), COALESCE(dso_rwa.digit2, ''::character varying)), COALESCE(dso_rwa.digit3, ''::character varying)), COALESCE(dso_rwa.digit4, ''::character varying)), COALESCE(dso_rwa.digit5, ''::character varying)) AS code,
    dso_rwa.achome,
    dso_rwa.acother,
    dso_rwa.description,
    dso_rwa.remarks
   FROM dso_rwa where isactive = true

   
   
CREATE OR REPLACE FUNCTION insert_to_ds_new_report()
  RETURNS trigger AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('ds_new_report_id');--@ignore-semicolon
  END IF ;--@ignore-semicolon
 Return NEW;--@ignore-semicolon
 END;--@ignore-semicolon
 $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

   
 CREATE TRIGGER TRG_DS_new_report
 BEFORE INSERT ON DS_NEW_REPORT
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_ds_new_report();

   
ALTER TABLE ds_electronic_signature DROP COLUMN fk_type_id;

ALTER TABLE ds_electronic_signature ADD COLUMN fk_type_id integer;

ALTER TABLE ds_electronic_signature DROP COLUMN persistence_type_id;

ALTER TABLE ds_electronic_signature ADD COLUMN persistence_type_id integer;


--Tomek P 16.02.2015
CREATE TABLE utp_search_filter
(
id SERIAL NOT NULL,
document_id numeric(18,0),
title character varying(50),
description character varying(50),
attribute character varying(50),
author character varying(50),
attachmentBarcode character varying(50),
startCtime timestamp without time zone,
endCtime timestamp without time zone,
startMtime timestamp without time zone,
endMtime timestamp without time zone,
lastRemark character varying(50) NULL,
accessedBy character varying(50) NULL,
filterName character varying(50) NULL,
documentKindIds character varying(50) NULL,
CONSTRAINT utp_search_filter_pkey PRIMARY KEY (id)
)

--2015-02-17 jpirog
alter table DS_AR_DOKUMENTY_PUBLICZNE alter column abstract type character varying(4000);

ALTER TABLE ds_pg_out_doc ADD COLUMN osoba_fizyczna boolean;

-- Grzesiek 20.02.2015

ALTER TABLE ds_pg_out_doc DROP COLUMN zablokuj;

ALTER TABLE ds_pg_out_doc ADD COLUMN zablokuj boolean;
ALTER TABLE ds_pg_out_doc DROP COLUMN zablokuj;
ALTER TABLE ds_pg_out_doc ADD COLUMN zablokuj boolean;

ALTER TABLE ds_electronic_signature DROP COLUMN signed_file;
ALTER TABLE ds_electronic_signature ADD COLUMN signed_file text;
ALTER TABLE ds_electronic_signature DROP COLUMN signature_file;
ALTER TABLE ds_electronic_signature ADD COLUMN signature_file text;

-- Grzesiek 02.03.2015

ALTER TABLE ds_pg_in_doc
   ADD COLUMN docautorexternalname character varying(200);

   
-- Grzesiek 13.03.2015
  
 ALTER TABLE dsw_jbpm_tasklist ADD COLUMN taskinpackage boolean default false;   
 ALTER TABLE dsw_jbpm_tasklist ADD COLUMN dockindid  numeric(18,0);   

 update dsw_jbpm_tasklist set taskinpackage = false; 
  update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='spiszdawczoodbiorczy') where dockind_name = 'Spis zdawczo-odbiorczy';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='ar_prot_brak') where dockind_name = 'Protokół brakowania';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='SpisDokDoAP') where dockind_name = 'Spis zdawczo-odbiorczy do AP';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='udostepnienie') where dockind_name = 'Wniosek o Udostępninie dokumenatcji';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='wycofanie') where dockind_name like 'Wniosek o wycofanie dokumenatcji%';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='normal_dragdrop') where dockind_name = 'Dokument zwykły';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='modifi_case') where dockind_name like 'Modyfikacja%sprawy%';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='pg_package') where dockind_name  like '%aczka%';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='normal_int') where dockind_name = 'Pismo wewnetrzne';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='normal') where dockind_name like '%ismo%przych%';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='normal_out') where dockind_name like '%Pismo wychodzące';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='email') where dockind_name = 'Wysyłanie maili';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='security') where dockind_name like 'security document';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='barcode_order') where dockind_name like 'Zamówienie puli barcodów' ;

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='zadanie') where dockind_name like 'Zadanie' ;

delete from dsw_jbpm_tasklist where dockind_name like '%aczka%' and document_type  = 'out';
update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='barcode_order') where dockind_name like 'Zamówienie puli barcodów';

update dsw_jbpm_tasklist set dockindid = (select id from ds_document_kind  where cn ='zadanie') where dockind_name like 'Zadanie';

delete from dsw_jbpm_tasklist where dockind_name like '%aczka%' and document_type  = 'out';
