----barcode_order------

create table dsg_pg_barcode_order(
  document_id NUMERIC,
  status NUMERIC,
  user_author NUMERIC,
  ilosc integer,
  cpp NUMERIC,
  start_barcode VARCHAR(50),
  end_barcode VARCHAR(50),
  odbior_osobisty NUMERIC(1),
  DOC_DATE date
);

create view dsr_printers_view as
  select id, userprintername as cn, userprintername as title, 0 as centrum, null as refValue, 1 as available from ds_printers order by userprintername;
  
  
CREATE OR REPLACE VIEW DSG_PG_CPP_VIEW (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) AS 
select id, guid as cn, name as title, 0 as centrum, null as refValue, 1 as available from ds_division div where divisiontype='division' and name like('%CPP%');

