

DECLARE
  v_table1_exists INTEGER;  
  v_table2_exists INTEGER;  
  v_table3_exists INTEGER; 
  v_table4_exists INTEGER;
  v_table5_exists INTEGER;
BEGIN
  Select count(*) into v_table1_exists
    from user_tables  where  table_name = 'DS_AR_BRAKOWANIE';
  if (v_table1_exists > 0) then
      execute immediate 'alter table DS_AR_BRAKOWANIE add (typ INTEGER)';
      execute immediate 'alter table DS_AR_BRAKOWANIE add (MIESCEUTWORZENIA VARCHAR2(400 CHAR))';
      execute immediate 'alter table DS_AR_BRAKOWANIE add (DODATKOWE_METADANE NUMBER(18,0))';
      COMMIT;
  end if;

  Select count(*) into v_table2_exists
    from user_tables  where  table_name = 'ds_ar_inventory_transf_and_rec';
  if (v_table2_exists > 0) then
      execute immediate 'alter table ds_ar_inventory_transf_and_rec add (typ INTEGER)';
      execute immediate 'alter table ds_ar_inventory_transf_and_rec add (MIESCEUTWORZENIA VARCHAR2(400 CHAR))';
      execute immediate 'alter table ds_ar_inventory_transf_and_rec add (DODATKOWE_METADANE NUMBER(18,0))';
      COMMIT;
  end if;

  Select count(*) into v_table3_exists
    from user_tables  where  table_name = 'DS_AR_SPIS_DOK_DO_AP';
  if (v_table3_exists > 0) then
      execute immediate 'alter table DS_AR_SPIS_DOK_DO_AP add (typ INTEGER)';
      execute immediate 'alter table DS_AR_SPIS_DOK_DO_AP add (MIESCEUTWORZENIA VARCHAR2(400 CHAR))';
      execute immediate 'alter table DS_AR_SPIS_DOK_DO_AP add (DODATKOWE_METADANE NUMBER(18,0))';
      COMMIT;
  end if;

  Select count(*) into v_table4_exists
    from user_tables  where  table_name = 'DS_AR_WNIOSEK_UDOSTEP';
  if (v_table4_exists > 0) then
      execute immediate 'alter table DS_AR_WNIOSEK_UDOSTEP add (typ INTEGER)';
      execute immediate 'alter table DS_AR_WNIOSEK_UDOSTEP add (MIESCEUTWORZENIA VARCHAR2(400 CHAR))';
      execute immediate 'alter table DS_AR_WNIOSEK_UDOSTEP add (DODATKOWE_METADANE NUMBER(18,0))';
      COMMIT;
  end if;

  Select count(*) into v_table5_exists
    from user_tables  where  table_name = 'DS_AR_WYCOFANIE';
  if (v_table5_exists > 0) then
      execute immediate 'alter table DS_AR_WYCOFANIE add (typ INTEGER)';
      execute immediate 'alter table DS_AR_WYCOFANIE add (MIESCEUTWORZENIA VARCHAR2(400 CHAR))';
      execute immediate 'alter table DS_AR_WYCOFANIE add (DODATKOWE_METADANE NUMBER(18,0))';
      COMMIT;
  end if;
end;




alter table DS_DOCUMENT add abstractdescription VARCHAR2(4000 char) NULL;
alter table DS_DOCUMENT add documentautordivision VARCHAR2(62 char) NULL;

CREATE TABLE ds_ar_dodatkowe_metadane
(
 id NUMBER (18,0),
 TEXTDATA varchar2(4000)
 NAZWAPOLA varchar2(100)
 );

COMMIT;

create sequence  DS_AR_DODATKOWE_METADANE_ID start with 1 nocache;
COMMIT;

create TRIGGER TRG_DS_AR_DODATK_METADANE_ID
before insert on DS_AR_DODATKOWE_METADANE
for each row
begin
  if :new.id is null then
  select DS_AR_DODATKOWE_METADANE_ID.nextval into :new.id from dual;
 end if;
end
commit;
 


CREATE TABLE DS_PG_IN_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0),
	ANONIM smallint
	
) ;
commit;

CREATE TABLE DS_PG_IN_DOC_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
commit;

create sequence  DS_PG_IN_DOC_MULTIPLE_ID start with 1 nocache;
commit;

create or replace trigger TRG_DS_PG_IN_DOC_MULTIPLE
before insert on DS_PG_IN_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_IN_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;


CREATE TABLE DS_PG_OUT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0),
	EPUAP INTEGER
	
);
commit;

CREATE TABLE DS_PG_OUT_DOC_MULTIPLE
   (	ID NUMBER(19,0) NOT NULL ENABLE, 
DOCUMENT_ID NUMBER(19,0) NOT NULL, 
FIELD_CN VARCHAR2(100 BYTE)NOT NULL , 
FIELD_VAL VARCHAR2(100 BYTE)
   );
   
commit;
create sequence  DS_PG_OUT_DOC_MULTIPLE_ID start with 1 nocache;
commit;

create  trigger TRG_DS_PG_OUT_DOC_MULTIPLE
before insert on DS_PG_OUT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_OUT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;




CREATE TABLE DS_PG_INT_DOC (
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0)
);
commit;
 

CREATE TABLE DS_PG_INT_DOC_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
    
);
create sequence  DS_PG_INT_DOC_MULTIPLE_ID start with 1 nocache;


create or replace trigger TRGDS_PG_INT_DOC_MULTIPLE
before insert on DS_PG_INT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_INT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;



alter table DS_UTP_DOC_INT add MIESCEUTWORZENIA VARCHAR2(100 char) NULL;
	
	
	
	
	

CREATE TABLE ds_pg_dokuments_multi(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
commit;

create sequence  ds_pg_dokuments_multi_ID start with 1 nocache;
commit;

create or replace trigger TRG_ds_pg_dokuments_multi
before insert on ds_pg_dokuments_multi
for each row
begin
  if :new.id is null then
 select ds_pg_dokuments_multi_ID.nextval into :new.id from dual;
  end if;
end
commit;


--2015-02-17 jpirog
alter table DS_AR_DOKUMENTY_PUBLICZNE alter column abstract varchar(4000);
commit;