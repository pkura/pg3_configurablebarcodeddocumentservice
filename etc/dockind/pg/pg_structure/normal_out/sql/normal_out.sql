EPUAP
--mssql
--oracle 

CREATE TABLE DS_PG_OUT_DOC_MULTIPLE
   (	ID NUMBER(19,0) NOT NULL ENABLE, 
DOCUMENT_ID NUMBER(19,0) NOT NULL, 
FIELD_CN VARCHAR2(100 BYTE)NOT NULL , 
FIELD_VAL VARCHAR2(100 BYTE)
   );
 commit;
 
 create sequence  DS_PG_OUT_DOC_MULTIPLE_ID start with 1 nocache;
 commit;
 
create or replace trigger TRG_DS_PG_OUT_DOC_MULTIPLE
before insert on DS_PG_OUT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
  select DS_PG_OUT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
 commit;

--drop table DS_PG_OUT_DOC;
CREATE TABLE DS_PG_OUT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
		EPUAP INTEGER 
	)	
drop table DS_ADM_INT_DOC;
CREATE TABLE DS_ADM_INT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	ANONIM smallint,
	DSUSER NUMBER(19, 0) not null,
	DSDIVISION NUMBER(19, 0) not null
)

alter table DSO_IN_DOCUMENT add contractId numeric (18,0);
alter table DS_ADM_IN_DOC add ANONIM smallint;




drop table DS_ADM_IN_DOC;
CREATE TABLE DS_ADM_IN_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	ANONIM smallint,
	DSUSER NUMBER(19, 0) not null,
	DSDIVISION NUMBER(19, 0) not null
) 