--normal--
DROP TABLE IF EXISTS DS_PG_IN_DOC CASCADE;
CREATE TABLE DS_PG_IN_DOC(
	DOCUMENT_ID NUMERIC(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR(400),
	DODATKOWE_METADANE NUMERIC(18,0),
	ANONIM smallint
	
);

DROP TABLE IF EXISTS DS_PG_IN_DOC_MULTIPLE CASCADE;
CREATE TABLE DS_PG_IN_DOC_MULTIPLE(
	ID SERIAL PRIMARY KEY NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
 -- ds_access_to_document nie istnieje
DROP TABLE IF EXISTS ds_access_to_document CASCADE;
CREATE TABLE ds_access_to_document 
(	ID SERIAL PRIMARY KEY NOT NULL, 
	POSN NUMERIC(18,0) NOT NULL, 
	NAME VARCHAR(40) NOT NULL 
);
INSERT INTO ds_access_to_document (POSN, NAME)
VALUES (1,'Publiczny – dostępny w całości');
INSERT INTO ds_access_to_document (POSN, NAME)
VALUES (2,'Niepubliczny');

alter table DS_PG_IN_DOC drop column ALFRESCO_IMPORT;
alter table DS_PG_IN_DOC add ALFRESCO_IMPORT boolean;
alter table DS_PG_IN_DOC add ZEWNETRZNY boolean;
-- email
alter table DS_PG_IN_DOC add ALFRESCO_UUID VARCHAR(300);
alter table DS_PG_IN_DOC add SHOW_EMAIL  boolean;
--epuap
alter table DS_PG_IN_DOC add DATA_EPUAP VARCHAR(30);

