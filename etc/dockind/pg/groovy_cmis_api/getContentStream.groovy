import org.apache.chemistry.opencmis.commons.*
import org.apache.chemistry.opencmis.commons.data.*
import org.apache.chemistry.opencmis.commons.enums.*
import org.apache.chemistry.opencmis.client.api.*

// variable 'session' is bound to the current OpenCMIS session

// print the repository name - Java style
println "Repository: " + session.getRepositoryInfo().getName()

// print the repository name - Groovy style
println "Repository: " + session.repositoryInfo.name


// get root folder
Folder root = session.getObjectByPath(path)
println "--- Root Folder: " + root.getName() + " ---"



// CMIS helper script
def cmis = new scripts.CMIS(session)


