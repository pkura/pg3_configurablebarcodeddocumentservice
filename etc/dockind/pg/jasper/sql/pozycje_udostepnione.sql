select TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD')	data_udostepnienia 
    ,b.OFFICEID
	  ,c.description
	  , REGEXP_REPLACE (b.OFFICEID,'[a-zA-Z._]') as rwa
    ,d.name
    ,e.rodzaj
	,c.achome

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID join DS_AR_UDOSTEPNIONE_ZASOBY e on b.id=e.zasob_id
where b.discriminator='CASE' 
and TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2015-10-22'