SELECT 
      TO_DATE (to_char (a.data_wycofania,
'YYYY-MON-DD'),'YYYY-MON-DD') data_wycofania
	  ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1)))
    as rwa
	,d.NAME
  FROM DS_AR_WYCOFANIE a join DSO_CONTAINER b 
  on b.id=a.sprawa_archiwista join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID
  where a.STATUSDOKUMENTU=5 and  TO_DATE (to_char (a.data_wycofania,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22' ;