select TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD')	data_udostepnienia 
    ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1)))
    as rwa,
    d.name

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID join DS_AR_UDOSTEPNIONE_ZASOBY e on b.id=e.zasob_id
where b.discriminator='FOLDER' 
and TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22'

select TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD')	data_udostepnienia 
    ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1)))
    as rwa,
    d.name

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID join DS_AR_UDOSTEPNIONE_ZASOBY e on b.id=e.zasob_id
where b.discriminator='CASE' 
and TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22'

select TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD')	data_udostepnienia 
    ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1))) as rwa,
    d.name, 
    f.officenumber AS ko_wyc,
    h.officenumber AS ko_in

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID join DSO_OUT_DOCUMENT f on b.id=f.CASE_ID 
join DSO_IN_DOCUMENT h on b.id=h.CASE_ID join DS_AR_UDOSTEPNIONE_ZASOBY e on f.id=e.zasob_id
join DS_AR_UDOSTEPNIONE_ZASOBY m on h.id=m.zasob_id
join DS_AR_UDOSTEPNIONE_ZASOBY m on f.id=m.zasob_id
where e.RODZAJ='document'  
and TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22' 

