select e.DATE_SHRED	 
    ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1)))
    as rwa,
    d.name

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID join DS_AR_INVENTORY_ITEMS e on b.id=e.folder_id
where b.archivestatus=4 
and b.discriminator='FOLDER' 
and TO_DATE (to_char (e.DATE_SHRED,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22'