/**Pozycje fizyczne przekzane do archiwum nie ma ich w stystemie **/
select d.folder_officeid,d.folder_title,d.category_arch, d.folder_count, d.folder_length_meter, TO_DATE (to_char (d.archive_date,'YYYY-MON-DD'),'YYYY-MON-DD') as data_przekazania
,REGEXP_REPLACE (d.folder_officeid,'[a-zA-Z._]') as rwa, g.name 
from ds_ar_inventory_transf_and_rec f,ds_division g, ds_ar_inventory_items d 
    full join ds_ar_inventory_items_multi  e 
        on d.id=e.field_val 
          where f.document_id=e.document_id
          and f.ds_division_id=g.id
          and ARCHIVE_STATUS=2 
          and folderid is null
			and TO_DATE (to_char (d.archive_date,'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2015-12-22' 	
        order by f.document_id