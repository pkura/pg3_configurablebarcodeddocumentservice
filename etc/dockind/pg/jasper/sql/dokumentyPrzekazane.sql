select b.archiveddate	 
    ,b.OFFICEID
	  ,c.description
	  ,(cast(c.digit1 as varchar2(1)))+(cast (c.digit2 as varchar2(1)))+(cast(c.digit3 as varchar2(1)))
    +(cast(c.digit4 as varchar2(1)))+(cast(c.digit5 as varchar2(1)))
    as rwa,
    d.name

from dso_container b join dso_rwa c on b.RWA_ID=c.id join DS_DIVISION d on d.GUID=b.DIVISIONGUID
where b.archivestatus=2 
and b.discriminator='CASE' 
and TO_DATE (to_char (b.archiveddate,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2013-10-22' 