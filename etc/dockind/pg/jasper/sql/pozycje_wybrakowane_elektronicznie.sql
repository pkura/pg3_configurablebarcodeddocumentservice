select TO_DATE (to_char (e.DATE_SHRED,
'YYYY-MON-DD'),'YYYY-MON-DD') as data_wybrakowanie	 
    ,b.OFFICEID
	  ,c.description
	  , REGEXP_REPLACE (b.OFFICEID,'[a-zA-Z._]') as rwa
    ,d.name
    ,c.achome
    ,e.folder_count
    ,e.folder_length_meter
	,TO_DATE(to_char (b.finishdate,
'YYYY-MON-DD'),'YYYY-MON-DD') as data_zakonczeniaS
	,TO_DATE(to_char (b.archiveddate,
'YYYY-MON-DD'),'YYYY-MON-DD') as data_przekazania

from DS_DIVISION d,ds_ar_inventory_items e ,dso_container b join dso_rwa c on b.RWA_ID=c.id 
where b.archivestatus=4
and b.discriminator='FOLDER' 
and TO_DATE (to_char (e.DATE_SHRED,
'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2015-12-22'
and b.divisionguid=d.guid
and b.id=e.folderid