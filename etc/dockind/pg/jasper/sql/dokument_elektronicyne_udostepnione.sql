select TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
'YYYY-MON-DD'),'YYYY-MON-DD')	data_udostepnienia 
    ,b.OFFICEID
	  ,c.description
	  , (cast(c.digit1 as varchar2(1)))||(cast (c.digit2 as varchar2(1)))||(cast(c.digit3 as varchar2(1)))
    ||(cast(c.digit4 as varchar2(1)))||(cast(c.digit5 as varchar2(1))) as rwa
    ,d.name
    ,e.rodzaj
	,c.achome
  ,g.id

from DS_DIVISION d,dso_in_document f,dso_out_document g,ds_ar_udostepnione_zasoby e ,dso_container b 
join dso_rwa c on b.RWA_ID=c.id 

	where e.rodzaj='Document'  and e.zasob_id=g.id   and  b.id=g.case_id
		and TO_DATE (to_char (e.DATA_UDOSTEPNIENIA,
			'YYYY-MON-DD'),'YYYY-MON-DD') between '2013-05-10' and '2015-10-22' and  d.GUID=b.DIVISIONGUID 