create or replace 
trigger "PGA".PublicDocument AFTER
  INSERT OR
  UPDATE ON DS_DOCUMENT FOR EACH ROW DECLARE id NUMBER;
  author              VARCHAR(100);
  login               VARCHAR(100);
  title               VARCHAR(100);
  ctime               TIMESTAMP;
  accesstodocument    NUMERIC (18,0);
  typid               NUMBER;
  typ                 VARCHAR(100);
  format              VARCHAR(100);
  typInt              VARCHAR(100);
  formatInt           VARCHAR(100);
  typOut              VARCHAR(100);
  formatOut           VARCHAR(100);
  idIn                SMALLINT;
  idInt               SMALLINT;
  idOut               SMALLINT;
  idPublic            SMALLINT;
  abstract            VARCHAR(255);
  organizational_unit VARCHAR(255);
  organizationId      NUMBER;
  locations           VARCHAR(255);
  BEGIN
    SELECT COUNT(document_id)
    INTO idInt
    FROM ds_utp_doc_int
    WHERE document_id=:new.id;
    SELECT COUNT(document_id)
    INTO idIn
    FROM ds_pg_in_doc
    WHERE document_id=:new.id;
    SELECT COUNT(document_id)
    INTO idOut
    FROM ds_pg_out_doc
    WHERE document_id=:new.id;
    SELECT COUNT(document_id)
    INTO idPublic
    FROM DS_AR_DOKUMENTY_PUBLICZNE
    WHERE document_id=:new.id;
    IF(idInt = 1) THEN
      SELECT format INTO formatInt FROM ds_utp_doc_int WHERE document_id=:new.id;
      SELECT abstract INTO abstract FROM ds_utp_doc_int WHERE document_id=:new.id;
      SELECT locations INTO locations FROM ds_utp_doc_int WHERE document_id=:new.id;
      SELECT organizational_unit
      INTO organizational_unit
      FROM ds_utp_doc_int
      WHERE document_id=:new.id;
      SELECT firstname || ' ' || lastname
      INTO login
      FROM ds_user
      WHERE name=:new.author;
      SELECT typ INTO typid FROM ds_utp_doc_int WHERE document_id=:new.id;
      IF(typid != 0) THEN
        SELECT title INTO typInt FROM ds_type WHERE id=typid;
      END IF;
      IF(idPublic = 0 AND :new.accesstodocument=1) THEN
        INSERT
        INTO DS_AR_DOKUMENTY_PUBLICZNE
          (
            DOCUMENT_ID,
            AUTHOR,
            TITLE,
            CTIME,
            DOSTEP,
            TYP_DOKUMENTU,
            FORMAT,
            ABSTRACT,
            locations,
            organizational_unit
          )
          VALUES
          (
            :new.id,
            login,
            :new.TITLE,
            :new.ctime,
            :new.accesstodocument,
            typInt,
            formatInt,
            abstract,
            locations,
            organizational_unit
          );
      elsif(idPublic = 1 AND :new.accesstodocument=1 ) THEN
        UPDATE DS_AR_DOKUMENTY_PUBLICZNE
        SET AUTHOR            =login,
          TITLE               =:new.TITLE,
          CTIME               =:new.ctime,
          DOSTEP              =:new.accesstodocument,
          TYP_DOKUMENTU       =typInt,
          FORMAT              =formatInt,
          abstract            =abstract,
          locations           =locations,
          organizational_unit = organizational_unit
        WHERE DOCUMENT_ID     =:new.id;
      elsif(idPublic = 1 AND :new.accesstodocument!=1) THEN
        DELETE FROM DS_AR_DOKUMENTY_PUBLICZNE WHERE DOCUMENT_ID=:new.id;
      END IF;
    elsif(idIn = 1) THEN
      SELECT format INTO format FROM ds_utp_doc_in WHERE document_id=:new.id;
      SELECT abstract INTO abstract FROM ds_utp_doc_in WHERE document_id=:new.id;
      SELECT locations INTO locations FROM ds_utp_doc_in WHERE document_id=:new.id;
      SELECT organizational_unit
      INTO organizational_unit
      FROM ds_pg_in_doc
      WHERE document_id=:new.id;
      SELECT firstname || ' ' || lastname
      INTO login
      FROM ds_user
      WHERE name=:new.author;
      SELECT typ INTO typid FROM ds_pg_in_doc WHERE document_id=:new.id;
      IF(typid != 0) THEN
        SELECT title INTO typ FROM ds_type WHERE id=typid;
      END IF;
      IF(idPublic = 0 AND :new.accesstodocument=1) THEN
        INSERT
        INTO DS_AR_DOKUMENTY_PUBLICZNE
          (
            DOCUMENT_ID,
            AUTHOR,
            TITLE,
            CTIME,
            DOSTEP,
            TYP_DOKUMENTU,
            FORMAT,
            ABSTRACT,
            locations,
            organizational_unit
          )
          VALUES
          (
            :new.id,
            login,
            :new.TITLE,
            :new.ctime,
            :new.accesstodocument,
            typ,
            format,
            abstract,
            locations,
            organizational_unit
          );
      elsif(idPublic = 1 AND :new.accesstodocument=1 ) THEN
        UPDATE DS_AR_DOKUMENTY_PUBLICZNE
        SET AUTHOR            =login,
          TITLE               =:new.TITLE,
          CTIME               =:new.ctime,
          DOSTEP              =:new.accesstodocument,
          TYP_DOKUMENTU       =typ,
          FORMAT              =format,
          abstract            =abstract,
          locations           =locations,
          organizational_unit = organizational_unit
        WHERE DOCUMENT_ID     =:new.id;
      elsif(idPublic          = 1 AND :new.accesstodocument!=1) THEN
        DELETE FROM DS_AR_DOKUMENTY_PUBLICZNE WHERE DOCUMENT_ID=:new.id;
      END IF;
    elsif(idOut = 1) THEN
      SELECT format INTO formatOut FROM ds_pg_out_doc WHERE document_id=:new.id;
      SELECT abstract INTO abstract FROM ds_pg_out_doc WHERE document_id=:new.id;
      SELECT locations INTO locations FROM ds_pg_out_doc WHERE document_id=:new.id;
      SELECT organizational_unit
      INTO organizational_unit
      FROM ds_pg_out_doc
      WHERE document_id=:new.id;
      SELECT firstname || ' ' || lastname
      INTO login
      FROM ds_user
      WHERE name=:new.author;
      SELECT typ INTO typid FROM ds_pg_out_doc WHERE document_id=:new.id;
      IF(typid != 0) THEN
        SELECT title INTO typOut FROM ds_type WHERE id=typid;
      END IF;
      IF(idPublic = 0 AND :new.accesstodocument=1) THEN
        INSERT
        INTO DS_AR_DOKUMENTY_PUBLICZNE
          (
            DOCUMENT_ID,
            AUTHOR,
            TITLE,
            CTIME,
            DOSTEP,
            TYP_DOKUMENTU,
            FORMAT,
            ABSTRACT,
            locations,
            organizational_unit
          )
          VALUES
          (
            :new.id,
            login,
            :new.TITLE,
            :new.ctime,
            :new.accesstodocument,
            typOut,
            formatOut,
            abstract,
            locations,
            organizational_unit
          );
      elsif(idPublic = 1 AND :new.accesstodocument=1 ) THEN
        UPDATE DS_AR_DOKUMENTY_PUBLICZNE
        SET AUTHOR            =login,
          TITLE               =:new.TITLE,
          CTIME               =:new.ctime,
          DOSTEP              =:new.accesstodocument,
          TYP_DOKUMENTU       =typOut,
          FORMAT              =formatOut,
          abstract            =abstract,
          locations           =locations,
          organizational_unit = organizational_unit
        WHERE DOCUMENT_ID     =:new.id;
      elsif(idPublic          = 1 AND :new.accesstodocument!=1) THEN
        DELETE FROM DS_AR_DOKUMENTY_PUBLICZNE WHERE DOCUMENT_ID=:new.id;
      END IF;
    END IF;
  END;