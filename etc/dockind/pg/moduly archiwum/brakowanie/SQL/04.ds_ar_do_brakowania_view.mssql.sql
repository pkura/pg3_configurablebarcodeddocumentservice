-- Wy�wietla pozycje ze spisu zdawczo odbiorczego ktore moga zostac wybrakowane
drop VIEW ds_ar_do_brakowania_view
CREATE VIEW ds_ar_do_brakowania_view
AS
select invt.DOCUMENT_ID, invt.ds_division_id, invi.CATEGORY_ARCH, invt.NUMBER_ITR,
invi.DATE_FROM, invi.DATE_TO, invi.FOLDER_COUNT, invi.FOLDER_LENGTH_METER, invi.FOLDER_TITLE, invi.FOLDER_OFFICEID, invi.FOLDERID as FOLDER_ID, teczka.archiveddate
from [docusafe].[dbo].[ds_ar_inventory_transf_and_rec] invt
left join [docusafe].[dbo].[DS_AR_INVENTORY_ITEMS_MULTI] invim on invt.DOCUMENT_ID = invim.DOCUMENT_ID
left join [docusafe].[dbo].[DS_AR_INVENTORY_ITEMS] invi on invim.FIELD_VAL = invi.ID
left join [docusafe].[dbo].[DSO_CONTAINER] teczka on invi.FOLDER_OFFICEID = teczka.OFFICEID
where teczka.ARCHIVESTATUS = 2 and  invi.CATEGORY_ARCH != 'A'
GO


