CREATE OR REPLACE VIEW "DS_AR_BRAKOWANIE_POZYCJA_VIEW" ("NUMBER_ITR", "ID_ITEM", "ID", "CN", "TITLE", "CASE_YEAR", "DATAOD", "DATADO", "RWATYTUL", "SYMBOL", "KATARCH", "LICZBATOMOW", "METRYBIEZACE", "REFVALUE", "CENTRUM", "AVAILABLE") AS 
  SELECT
  spis_zodb.NUMBER_ITR,
  aii.ID AS id_Item,
  con.ID,
  con.OFFICEID AS CN,
  con.OFFICEID AS TITLE,
  con.CASE_YEAR,
  aii.DATE_FROM AS DATAOD,
  aii.DATE_TO AS DATADO,
  rwa.description AS RWATYTUL,
  rwa.code AS SYMBOL,
  rwa.achome AS katArch, 
  aii.FOLDER_COUNT AS liczbaTomow,
  aii.FOLDER_LENGTH_METER AS metryBiezace,
  NULL AS refValue,
  0 AS CENTRUM,
  1 AS available
FROM DS_AR_INVENTORY_ITEMS aii
LEFT OUTER JOIN DSO_CONTAINER con ON aii.FOLDER_OFFICEID = con.OFFICEID
LEFT OUTER JOIN v_dso_rwaa rwa ON con.RWA_ID = rwa.ID
LEFT OUTER JOIN (
  SELECT
    PARENT_ID,
    NVL(MAX(ARCHIVESTATUS), 0)  as max
  FROM  DSO_CONTAINER concase
  WHERE (DISCRIMINATOR = 'CASE')
    AND (ID IN (
      SELECT DISTINCT outdok.CASE_ID
      FROM DS_DOCUMENT dok
      LEFT OUTER JOIN DSO_OUT_DOCUMENT outdok ON dok.ID = outdok.ID
      WHERE (outdok.CASE_ID IS NOT NULL)
      ))
    OR (ID IN (
      SELECT DISTINCT indok.CASE_ID
      FROM DS_DOCUMENT dok
      LEFT OUTER JOIN DSO_IN_DOCUMENT indok ON dok.ID = indok.ID 
      WHERE (indok.CASE_ID IS NOT NULL)
    ))
  GROUP BY PARENT_ID) kejsy ON kejsy.PARENT_ID = con.ID
JOIN DS_AR_INVENTORY_ITEMS_MULTI spis_zodb_m on aii.ID = spis_zodb_m.FIELD_VAL
JOIN ds_ar_inventory_transf_and_rec spis_zodb on spis_zodb_m.DOCUMENT_ID = spis_zodb.DOCUMENT_ID
WHERE (con.DISCRIMINATOR = 'FOLDER' and rwa.achome != 'A' and con.ARCHIVESTATUS = 2 and CZYWYBRAKOWAC(con.ARCHIVEDDATE, rwa.achome) = 1);
