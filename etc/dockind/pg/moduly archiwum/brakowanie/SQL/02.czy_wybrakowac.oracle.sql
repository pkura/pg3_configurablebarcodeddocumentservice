create or replace 
FUNCTION CZYWYBRAKOWAC (var_data varchar2, archcat  varchar2)
RETURN varchar
AS
     wybrakuj numeric;
     rokarchiwizacji numeric;
     lataprzechowywania numeric;
BEGIN
    rokarchiwizacji := extract(year from (cast(var_data as date)));
    if LOWER(archcat) = 'b' THEN
      lataprzechowywania := 25;
    elsif REPLACE(lower(archcat),'b','') = 'c' or REPLACE(lower(archcat),'b','') = 'e' THEN
      lataprzechowywania := 2;
    else 
      lataprzechowywania := cast(LTRIM(lower(archcat),'be') as numeric);
    END IF;
    
    lataprzechowywania := ABS(lataprzechowywania);
	
    if(extract(YEAR from sysdate) >= rokarchiwizacji+1+lataprzechowywania) THEN
      wybrakuj := 1;
    else 
      wybrakuj := 0;
    END IF;
    RETURN(wybrakuj);
END;