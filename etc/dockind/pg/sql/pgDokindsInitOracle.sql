

CREATE TABLE DS_PG_IN_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0),
	ANONIM smallint
	
);
commit;

CREATE TABLE DS_PG_IN_DOC_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
commit;

create sequence  DS_PG_IN_DOC_MULTIPLE_ID start with 1 nocache;
commit;

create or replace trigger TRG_DS_PG_IN_DOC_MULTIPLE
before insert on DS_PG_IN_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_IN_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;


CREATE TABLE DS_PG_OUT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0),
	EPUAP INTEGER
	
);
commit;

CREATE TABLE DS_PG_OUT_DOC_MULTIPLE
   (	ID NUMBER(19,0) NOT NULL ENABLE, 
DOCUMENT_ID NUMBER(19,0) NOT NULL, 
FIELD_CN VARCHAR2(100 BYTE)NOT NULL , 
FIELD_VAL VARCHAR2(100 BYTE)
   );
   
commit;
create sequence  DS_PG_OUT_DOC_MULTIPLE_ID start with 1 nocache;
commit;

create  trigger TRG_DS_PG_OUT_DOC_MULTIPLE
before insert on DS_PG_OUT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_OUT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;




CREATE TABLE DS_PG_INT_DOC (
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	MIESCEUTWORZENIA VARCHAR2(400 CHAR),
	DODATKOWE_METADANE NUMBER(18,0)
);
commit;
 

CREATE TABLE DS_PG_INT_DOC_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
    
);
create sequence  DS_PG_INT_DOC_MULTIPLE_ID start with 1 nocache;


create or replace trigger TRGDS_PG_INT_DOC_MULTIPLE
before insert on DS_PG_INT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
 select DS_PG_INT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end
commit;



alter table DS_UTP_DOC_INT add MIESCEUTWORZENIA VARCHAR2(100 char) NULL;
	
	
	
	
	

CREATE TABLE ds_pg_dokuments_multi(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
commit;

create sequence  ds_pg_dokuments_multi_ID start with 1 nocache;
commit;

create or replace trigger TRG_ds_pg_dokuments_multi
before insert on ds_pg_dokuments_multi
for each row
begin
  if :new.id is null then
 select ds_pg_dokuments_multi_ID.nextval into :new.id from dual;
  end if;
end
commit;

--
alter table DS_PG_IN_DOC add ALFRESCO_IMPORT NUMBER(1);
alter table DS_PG_IN_DOC add ALFRESCO_UUID VARCHAR2(300 CHAR);
alter table DS_PG_OUT_DOC add ALFRESCO_UUID VARCHAR2(300 CHAR);
alter table DS_PG_IN_DOC add ZEWNETRZNY NUMBER(1);
--
INSERT INTO DSA_MAPPED_FIELD (MAPPING_TYPE, DOCKIND_CN, DOCKIND_FIELD_CN, EXTERNAL_FIELD_CN)
VALUES ('ALFRESCO', 'normal', 'DOC_DESCRIPTION', 'name');

INSERT INTO DSA_MAPPED_FIELD (MAPPING_TYPE, DOCKIND_CN, DOCKIND_FIELD_CN, EXTERNAL_FIELD_CN)
VALUES ('ALFRESCO', 'normal_out', 'DOC_DESCRIPTION', 'name');

-- email
alter table DS_PG_IN_DOC add SHOW_EMAIL NUMBER(1);
--epuap
alter table DS_PG_IN_DOC add DATA_EPUAP VARCHAR2(30 CHAR );


alter table DS_PG_OUT_DOC add ZABLOKUJ NUMBER(1);
