create table dsg_pg_barcode_order(
  document_id number,
  status number,
  user_author number,
  ilosc integer,
  cpp number,
  start_barcode varchar2(50),
  end_barcode varchar2(50),
  odbior_osobisty number(1),
  DOC_DATE date
);

create table dsg_pg_barcode_pool(
  id number primary key,
  document_id number,
  startval number,
  endval number,
  creationDate date,
  user_id number,
  prefix varchar2(30)
);

CREATE SEQUENCE dsg_pg_barcode_pool_id
  START WITH 1
  INCREMENT BY 1
  CACHE 10000000000000;


create table dsg_pg_barcode_generator(
  pointer_value number
);

insert into dsg_pg_barcode_generator values (0);

create view dsr_printers_view as
  select id, userprintername as cn, userprintername as title, 0 as centrum, null as refValue, 1 as available from ds_printers order by userprintername;

insert into ds_acceptance_division (id, GUID, NAME, code, DIVISIONTYPE, ogolna) values (1, 'root', 'PG', 'root', 'ac.division', 1);

alter table DSW_JBPM_TASKLIST add resource_count number  default 1 not null;
ALTER TABLE DSW_JBPM_TASKLIST add markerClass varchar2(30);
ALTER table DS_ATTACHMENT add field_cn varchar2(200);