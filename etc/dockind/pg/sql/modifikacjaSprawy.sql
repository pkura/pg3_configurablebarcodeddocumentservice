CREATE OR REPLACE FORCE VIEW V_CASE_LIST ("ID", "OFFICEID", "TITLE", "DESCRIPTION", "CTIME", "FINISHDATE", "AUTOR", "CLERK", "DIVISIONNAME", "STATUS", "PRIORITY") AS 
  select  c.id ,c.OFFICEID , c.TITLE, c.DESCRIPTION,c.CTIME ,c.finishdate,
CONCAT(u.FIRSTNAME,' ' || + u.LASTNAME) as AUTOR ,CONCAT(u2.FIRSTNAME,' ' || + u2.LASTNAME) AS CLERK ,d.NAME as divisionName,cs.NAME as Status, cp.NAME as priority
from DSO_CONTAINER c left join DS_DIVISION d
on c.DIVISIONGUID = d.GUID  left join DS_USER u on c.AUTHOR = u.NAME  left join DS_user u2 on c.CLERK = u2.NAME 
right join DSO_CASE_STATUS cs on c.STATUS_ID = cs.ID 
right join DSO_CASE_PRIORITY cp on c.PRIORITY_ID =cp.ID
where c.DISCRIMINATOR = 'CASE';

  CREATE TABLE DS_PG_MODIFY_CASE_DOC 
   (	"DOCUMENT_ID" NUMBER(18,0) NOT NULL ENABLE, 
	"TITLE" VARCHAR2(400 CHAR), 
	"DESCRIPTION" VARCHAR2(400 CHAR), 
	"PLANINGENDTIME" DATE, 
	"CLERK" NUMBER(18,0), 
	"AUTOR" NUMBER(18,0), 
	"CASESTATUS" NUMBER(18,0), 
	"CASEPRIORITY" NUMBER(18,0), 
	"DODATKOWE_METADANE" VARCHAR2(400 CHAR), 
	"MIESCEUTWORZENIA" VARCHAR2(200 BYTE), 
	"CASES" NUMBER, 
	"STATUS" NUMBER, 
	"CASETITLE" VARCHAR2(200 BYTE), 
	"CASEDESCRIPTION" VARCHAR2(2000 BYTE)
   )