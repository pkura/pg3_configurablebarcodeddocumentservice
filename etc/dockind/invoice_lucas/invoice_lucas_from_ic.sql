CREATE TABLE [dbo].[DSG_INVOICE_LUCAS](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[data_wystawienia] [datetime] NULL,
	[data_platnosci] [datetime] NULL,
	[data_zaplaty] [datetime] NULL,
	[data_zaksieg] [datetime] NULL,
	[dostawca] [int] NULL,
	[kwota_brutto] [numeric](16, 2) NULL,
	[opis_towaru] [varchar](200) NULL,
	[nr_zestawienia] [numeric](19, 0) NULL,
	[nr_faktury] [varchar](30) NULL,
	[zaplacono] [smallint] NULL,
	[akceptacja_finalna] [smallint] NULL,
	[numer_rachunku_bankowego] [varchar](32) NULL,
	[rozliczono] [smallint] NULL,
	[kwota_netto] [numeric](16, 2) NULL,
	[vat] [numeric](16, 2) NULL,
	[lokalizacja] [int] NULL,
	[kod_ksiegowania] [int] NULL,
	[numer_ksiegowania] [varchar](50) NULL,
	[stanowisko] [int] NULL,
	[struktura] [int] NULL,
	[data_ksiegowania] [datetime] NULL,
	[opis_faktury] [int] NULL,
	[uwaga_opis] [varchar](200) NULL,
	[waluta] [int] NULL,
	[rodzaj_faktury] [int] NULL,
	[faktura_specjalna] [int] NULL,
	[nazwa_projektu] [int] NULL,
	[jbpm_process_id] [numeric](19, 0) NULL,
	[numer_nabycia] [varchar](50) NULL,
	[sposob_zaplaty] [int] NULL
)