

 CREATE TABLE [dbo].[dsg_holiday_request](
	[document_id] [numeric](19, 0) NOT NULL PRIMARY KEY ,
	[dsuser] [numeric](19, 0) NULL,
	[period_from] [datetime] NULL,
	[period_to] [datetime] NULL,
	[substitution] [numeric](19, 0) NULL,
	[description] [text] NULL,
	[requested] [bit] NULL,
	[recreational] [bit] NULL,
	[position] [varchar](50) NULL,
	[status] [integer] NULL
)
