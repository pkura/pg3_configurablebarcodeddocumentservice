create table dsg_polkomtel_invoice(
	document_id numeric(18, 0) NOT NULL,
	NUMER_ZAKLADU varchar(50),
	NUMER_LOKALIZACJI varchar(50),
	NR_PPE_Z_ZAKLADU varchar(50)
);

create table dsg_stacje(
	ID numeric(19, 0),
	NR_STACJI varchar(50),
	NAZWA_STACJI varchar(50),
	ADRES_STACJI varchar(50),
	KOD_POCZTOWY varchar(50),
	MIEJSCOWOSC varchar(50),
	NR_LICZNIKA varchar(50),
	NR_EWIDENCYJNY varchar(50),
	TARYFA_OSD varchar(50),
	MOC_UMOWNA varchar(50),
	ZABEZPIECZENIE_PRZEDLICZNIKOWE varchar(50)
);