CREATE TABLE [dbo].[dsg_umw_umowa_o_prace](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[STATUS] [int] NULL,
	[DATA_UMOWY] [date] NULL,
	[MIEJSCE_UMOWY] [varchar](100) NULL,
	[IMIE_PR] [varchar](50) NULL,
	[NAZWISKO_PR] [varchar](100) NULL,
	[ZIP_PR] [varchar](10) NULL,
	[MIEJSCOWOSC_PR] [varchar](100) NULL,
	[ULICA_PR] [varchar](100) NULL,
	[PESEL_PR] [varchar](11) NULL,
	[STANOWISKO] [varchar](200) NULL,
	[MIEJSCE_PRACY] [varchar](150) NULL,
	[OBOWIAZKI] [varchar](5000) NULL,
	[WYMIAR_ETATU] [int] NULL,
	[RODZAJ_UMOWY] [int] NULL,
	[DATA_ROZP] [date] NULL,
	[DATA_ZAKO] [date] NULL,
	[KWOTA_WYN] [numeric](18, 2) NULL,
	[NR_KONTA] [int] NULL,
	[OS_PROWADZACA] [int] NULL
	);


CREATE TABLE dsg_umw_umowa_o_prace_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);

Alter Table dsg_umw_umowa_o_prace ADD TERMIN_WYPL date;