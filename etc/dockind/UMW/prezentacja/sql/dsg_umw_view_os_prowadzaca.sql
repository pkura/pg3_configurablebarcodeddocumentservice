create view dsg_umw_view_os_prowadzaca as
SELECT
u.ID as id,
u.NAME as cn,
u.LASTNAME + ' ' + u.FIRSTNAME as title,
null as centrum,
null as refValue,
1 as available
FROM DS_DIVISION d
JOIN DS_USER_TO_DIVISION ud on d.ID = ud.DIVISION_ID
JOIN DS_USER u on ud.USER_ID = u.ID
WHERE d.GUID = 'BFA800C00042859E1437C43F3E533F4A4D2'
;