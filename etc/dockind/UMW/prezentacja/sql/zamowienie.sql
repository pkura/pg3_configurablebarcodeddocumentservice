ALTER TABLE dsg_umw_zamowienie ADD OS_ODPOWIEDZIALNA numeric(18,0);
ALTER TABLE dsg_umw_zamowienie ADD OS_KONTAKTOWA numeric(18,0);




/****** Object:  Table [dbo].[dsd_umw_currency]    Script Date: 2014-01-12 22:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsd_umw_currency](
	[id] [int] NOT NULL,
	[available] [bit] NOT NULL,
	[refValue] [int] NULL,
	[title] [varchar](50) NULL,
	[cn] [varchar](10) NULL,
	[centrum] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dsg_umw_account_number]    Script Date: 2014-01-12 22:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_umw_account_number](
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[dostawca_nazwa] [varchar](250) NULL,
	[dostawca_nip] [varchar](50) NULL,
	[dostawca_miasto] [varchar](100) NULL,
	[dostawca_kodpoczt] [varchar](50) NULL,
	[dostawca_ulica] [varchar](100) NULL,
	[dostawca_nrdomu] [varchar](10) NULL,
	[dostawca_nrmieszk] [varchar](10) NULL,
	[numer] [varchar](250) NULL,
	[waluta] [int] NULL,
	[officenumber_1] [varchar](150) NULL,
	[description_1] [varchar](2048) NULL,
	[kontrahent_id] [numeric](19, 0) NULL,
	[bank] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dsg_umw_contractor]    Script Date: 2014-01-12 22:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_umw_contractor](
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[dostawca_nazwa] [varchar](250) NULL,
	[dostawca_nip] [varchar](50) NULL,
	[dostawca_miasto] [varchar](100) NULL,
	[dostawca_kodpoczt] [varchar](50) NULL,
	[dostawca_ulica] [varchar](100) NULL,
	[dostawca_nrdomu] [varchar](10) NULL,
	[dostawca_nrmieszk] [varchar](10) NULL,
	[numer] [varchar](250) NULL,
	[waluta] [int] NULL,
	[kontrahent_id] [numeric](19, 0) NULL,
	[officenumber_1] [varchar](150) NULL,
	[description_1] [varchar](2048) NULL,
	[bank] [varchar](250) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[dsg_umw_zamowienie]    Script Date: 2014-01-12 22:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[dsg_umw_zamowienie](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[rodzaj_zamowienia] [int] NULL,
	[nazwa_zamowienia] [varchar](251) NULL,
	[nr_sprawy] [varchar](20) NULL,
	[realizacja] [datetime] NULL,
	[szczegolowy_opis] [varchar](8000) NULL,
	[uzasadnienie] [varchar](8000) NULL,
	[kwota_brutto] [numeric](19, 2) NULL,
	[tryb_zamowienia] [int] NULL,
	[szczegoly_zamowienia] [int] NULL,
	[contractor] [int] NULL,
	[konto_bankowe] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE dsg_umw_zamowienie_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[ds_document_internals]    Script Date: 2014-01-12 22:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ds_document_internals] as
select doc.id as document_id, doc.*,doc_out.OFFICENUMBER from ds_document doc
join dso_out_document doc_out on doc.ID=doc_out.ID
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (1, 1, 1, N'Polski złoty                            ', N'PLN', 1)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (2, 1, 1, N'Dolar australijski                      ', N'AUD', 2)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (3, 1, 1, N'Dolar kanadyjski                        ', N'CAD', 3)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (4, 1, 1, N'Frank szwajcarski                       ', N'CHF', 4)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (5, 1, 1, N'Korona duńska                           ', N'DKK', 5)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (6, 1, 1, N'Euro                                    ', N'EUR', 6)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (7, 1, 1, N'Funt szterling                          ', N'GBP', 7)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (8, 1, 1, N'Korona norweska                         ', N'NOK', 8)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (9, 1, 1, N'Korona szwedzka                         ', N'SEK', 9)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (10, 1, 1, N'Dolar amerykański                       ', N'USD', 10)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (11, 1, 1, N'Łat łotewski                            ', N'LVL', 11)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (12, 1, 1, N'Rubel rosyjski                          ', N'RUB', 12)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (13, 1, 1, N'Korona estońska                         ', N'EEK', 13)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (14, 1, 1, N'Yuan chiński                            ', N'CNY', 14)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (15, 1, 1, N'Lej rumuński                            ', N'RON', 15)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (16, 1, 1, N'Korona islandzka                        ', N'ISK', 16)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (17, 1, 1, N'Real brazylijski                        ', N'BRL', 17)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (18, 1, 1, N'Gulden antylski                         ', N'ANG', 18)
GO
INSERT [dbo].[dsd_umw_currency] ([id], [available], [refValue], [title], [cn], [centrum]) VALUES (19, 1, 1, N'Forint                                  ', N'HUF', 19)
GO
SET IDENTITY_INSERT [dbo].[dsg_umw_account_number] ON 

GO
INSERT [dbo].[dsg_umw_account_number] ([ID], [dostawca_nazwa], [dostawca_nip], [dostawca_miasto], [dostawca_kodpoczt], [dostawca_ulica], [dostawca_nrdomu], [dostawca_nrmieszk], [numer], [waluta], [officenumber_1], [description_1], [kontrahent_id], [bank]) VALUES (CAST(1 AS Numeric(19, 0)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'343', 2, NULL, NULL, CAST(1 AS Numeric(19, 0)), NULL)
GO
INSERT [dbo].[dsg_umw_account_number] ([ID], [dostawca_nazwa], [dostawca_nip], [dostawca_miasto], [dostawca_kodpoczt], [dostawca_ulica], [dostawca_nrdomu], [dostawca_nrmieszk], [numer], [waluta], [officenumber_1], [description_1], [kontrahent_id], [bank]) VALUES (CAST(2 AS Numeric(19, 0)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'3435', 2, NULL, NULL, CAST(1 AS Numeric(19, 0)), NULL)
GO
INSERT [dbo].[dsg_umw_account_number] ([ID], [dostawca_nazwa], [dostawca_nip], [dostawca_miasto], [dostawca_kodpoczt], [dostawca_ulica], [dostawca_nrdomu], [dostawca_nrmieszk], [numer], [waluta], [officenumber_1], [description_1], [kontrahent_id], [bank]) VALUES (CAST(3 AS Numeric(19, 0)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'3456789', 14, NULL, NULL, CAST(2 AS Numeric(19, 0)), NULL)
GO
SET IDENTITY_INSERT [dbo].[dsg_umw_account_number] OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_umw_contractor] ON 

GO
INSERT [dbo].[dsg_umw_contractor] ([ID], [dostawca_nazwa], [dostawca_nip], [dostawca_miasto], [dostawca_kodpoczt], [dostawca_ulica], [dostawca_nrdomu], [dostawca_nrmieszk], [numer], [waluta], [kontrahent_id], [officenumber_1], [description_1], [bank]) VALUES (CAST(1 AS Numeric(19, 0)), N'COM-PAN System', N'849-123-25-45', N'Warszawa', NULL, N'Pęcicka', N'42', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[dsg_umw_contractor] ([ID], [dostawca_nazwa], [dostawca_nip], [dostawca_miasto], [dostawca_kodpoczt], [dostawca_ulica], [dostawca_nrdomu], [dostawca_nrmieszk], [numer], [waluta], [kontrahent_id], [officenumber_1], [description_1], [bank]) VALUES (CAST(2 AS Numeric(19, 0)), N'UMW', N'849-123-25-45', N'Warszawa', NULL, N'Pęcicka', N'42', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[dsg_umw_contractor] OFF
GO
INSERT [dbo].[dsg_umw_zamowienie] ([document_id], [status], [rodzaj_zamowienia], [nazwa_zamowienia], [nr_sprawy], [realizacja], [szczegolowy_opis], [uzasadnienie], [kwota_brutto], [tryb_zamowienia], [szczegoly_zamowienia], [contractor], [konto_bankowe]) VALUES (CAST(11 AS Numeric(19, 0)), 110, 3, N'jkl', N'14-0001', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1)
GO

USE [umw]
GO
SET IDENTITY_INSERT [dbo].[ds_acceptance_condition] ON 

GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(5 AS Numeric(19, 0)), N'eco_center', NULL, N'alos', NULL, NULL)
GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(6 AS Numeric(19, 0)), N'eco_center', NULL, N'admin', NULL, NULL)
GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(7 AS Numeric(19, 0)), N'auth', N'1', N'mmalek', NULL, NULL)
GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(8 AS Numeric(19, 0)), N'auth', N'3', N'admin', NULL, NULL)
GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(9 AS Numeric(19, 0)), N'auth', N'3', N'bzaremba', NULL, NULL)
GO
INSERT [dbo].[ds_acceptance_condition] ([id], [cn], [fieldValue], [username], [divisionGuid], [maximumAmountForAcceptance]) VALUES (CAST(10 AS Numeric(19, 0)), N'auth', N'2', N'amucha', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[ds_acceptance_condition] OFF
GO
SET IDENTITY_INSERT [dbo].[ds_acceptance_division] ON 

GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(1 AS Numeric(19, 0)), N'root', N'UMW', N'root', N'ac.division', NULL, 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(3 AS Numeric(19, 0)), N'xxx', N'Autoryzacja wg rodzaju zamówienia', N'auth', N'ac.division', CAST(1 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(4 AS Numeric(19, 0)), N'xxx', N'Akcesoria komputerowe', N'akcesoria', N'ac.division', CAST(3 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(5 AS Numeric(19, 0)), N'xxx', N'Wartość', N'1', N'ac.fieldValue', CAST(4 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(6 AS Numeric(19, 0)), N'xxx', N'Osoby', N'auth', N'ac.acceptance', CAST(4 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(11 AS Numeric(19, 0)), N'xxx', N'Wartość', N'2', N'ac.fieldValue', CAST(8 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(12 AS Numeric(19, 0)), N'xxx', N'Monitory', N'monitory', N'ac.division', CAST(3 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(13 AS Numeric(19, 0)), N'xxx', N'Wartość', N'3', N'ac.fieldValue', CAST(12 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(14 AS Numeric(19, 0)), N'xxx', N'Osoby', N'auth', N'ac.acceptance', CAST(12 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(15 AS Numeric(19, 0)), N'xxx', N'Zaremba Bogdan', N'bzaremba', N'ds.user', CAST(14 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(16 AS Numeric(19, 0)), N'xxx', N'Systemu Administrator', N'admin', N'ds.user', CAST(14 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(18 AS Numeric(19, 0)), N'xxx', N'Los Agata', N'alos', N'ds.user', CAST(17 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(19 AS Numeric(19, 0)), N'xxx', N'Systemu Administrator', N'admin', N'ds.user', CAST(17 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(7 AS Numeric(19, 0)), N'xxx', N'Małek Maciej', N'mmalek', N'ds.user', CAST(6 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(8 AS Numeric(19, 0)), N'xxx', N'Oprogramowanie', N'oprogramowanie', N'ac.division', CAST(3 AS Numeric(19, 0)), 1)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(9 AS Numeric(19, 0)), N'xxx', N'Osoby', N'auth', N'ac.acceptance', CAST(8 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(10 AS Numeric(19, 0)), N'xxx', N'Mucha Alicja', N'amucha', N'ds.user', CAST(9 AS Numeric(19, 0)), 0)
GO
INSERT [dbo].[ds_acceptance_division] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [ogolna]) VALUES (CAST(17 AS Numeric(19, 0)), N'xxx', N'Akceptacja CG', N'eco_center', N'ac.acceptance', CAST(1 AS Numeric(19, 0)), 1)
GO
SET IDENTITY_INSERT [dbo].[ds_acceptance_division] OFF
GO
