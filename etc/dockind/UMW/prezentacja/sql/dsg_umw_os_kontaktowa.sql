CREATE TABLE [dbo].[dsg_umw_os_kontaktowa](
    [ID] numeric(18,0) identity(1,1),
	[NAZWA] [varchar](300) NULL,
	[TEL] [varchar](50) NULL,
	[EMAIL] [varchar](50) NULL,
	[KOMPETENCJE] [varchar](500) NULL,
	);
