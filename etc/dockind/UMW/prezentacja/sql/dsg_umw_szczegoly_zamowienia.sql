CREATE TABLE [dbo].[dsg_umw_szczegoly_zamowienia](
    [ID] numeric(18,0) identity(1,1),
	[NAZWA] [varchar](250) NULL,
	[MODEL] [varchar](100) NULL,
	[KOD_PRODUCENTA] [varchar](50) NULL,
	[ILOSC] [varchar](100) NULL,
	[CENA] [numeric](18, 2) NULL,
	[WERSJA] [varchar](100) NULL,
	[PLATFORMA] [varchar](100) NULL,
	);
