INSERT INTO DS_FOLDER (id,TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES (ds_folder_id.nextval,'Archiwum', 1, 0, current_timestamp, 'admin', 'businessarchive', 1);

INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_WSZYSCY', 'Wszyscy', 'group',0);
INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_WSZYSCY', 'Wszyscy', 'group',0)

INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_OGOLNE', 'Og�lne', 'group',0);
INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_OGOLNE', 'Og�lne', 'group',0)

INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_SWIADCZENIOWE', '�wiadczeniowe', 'group',0);
INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_SWIADCZENIOWE', '�wiadczeniowe', 'group',0)

INSERT INTO DS_DIVISION (id,PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (DS_DIVISION_id.nextval,1, 'NW_RAPORTY', 'Raporty', 'group',0);

insert into ds_division (id,guid, name, code,divisiontype, parent_id, hidden, description) values (DS_DIVISION_id.nextval,'NW_OGOLNE_EDYCJA', 'Og�lna - edycja',null,'group',1,0,null);

INSERT INTO DS_FOLDER (id,TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES) VALUES (ds_folder_id.nextval,'Archiwum', 0, 0, current_timestamp, 'admin', 'businessarchive', 1);

create table nw_perm (
    document_id     numeric(19,0),
    hash            numeric(19,0)
);
