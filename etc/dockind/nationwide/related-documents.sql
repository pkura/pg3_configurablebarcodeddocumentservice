CREATE TABLE dsg_nationwide_related_types(
	[type_id] [int] NOT NULL,
	[related_type_id] [int] NOT NULL
);

alter table dsg_nationwide_related_types add constraint PK_dsg_nrt primary key (type_id, related_type_id);