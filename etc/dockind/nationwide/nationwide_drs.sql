create table dsg_doctype_2 (
    document_id     numeric(19,0) not null,
    field_0         varchar(30),
    field_1         integer,
    field_2         integer,
    field_3         numeric(19,0),
    field_4         integer,
    field_5         integer
);

create index dsg_doctype_2_id on dsg_doctype_2(document_id);
create index dsg_doctype_2_0 on dsg_doctype_2(field_0);
create index dsg_doctype_2_1 on dsg_doctype_2(field_1);
create index dsg_doctype_2_2 on dsg_doctype_2(field_2);
create index dsg_doctype_2_3 on dsg_doctype_2(field_3);
create index dsg_doctype_2_4 on dsg_doctype_2(field_4);
create index dsg_doctype_2_5 on dsg_doctype_2(field_5);

insert into ds_folder (title, parent_id, hversion, ctime, author, systemname, attributes) values ('Rejestr szk�d', 1, 0, current_timestamp, 'admin', 'drs', 1);
insert into ds_division (guid, name, code, divisiontype, parent_id) values ('NW_DRS', 'Rejestr szk�d', 'DRS', 'group', 1);