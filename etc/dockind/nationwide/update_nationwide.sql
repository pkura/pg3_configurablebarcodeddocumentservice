-- Update potrzebny w momencie przejscia z doctype na dockind

alter table dsg_doctype_1 add kategoria int NULL;
alter table dsg_doctype_3 add kategoria int NULL;


update ds_document set dockind_id=NATIONWIDE_DOCKIND_ID where doctype=NATIONWIDE_DOCTYPE_ID;
update ds_document set dockind_id=DRS_DOCKIND_ID where doctype=DRS_DOCTYPE_ID;
update ds_document set dockind_id=DAA_DOCKIND_ID where doctype=DAA_DOCTYPE_ID;


update dsg_doctype_1 set kategoria=10 where field_3 in (20,23,30,40,60,70,73,75,78,80,100,140,150,170,190,200,220,230,245,250,260,270,280,290,310,315,320,330,335,345,350,365,370,380,390,400,410,425,430,440,445,455,460,470,510,520,530,540,550,560,570);
update dsg_doctype_1 set kategoria=20 where field_3 in (10,50,90,120,130,160,180,210,240,300,340,420,450,480,490,500);
update dsg_doctype_1 set kategoria=30 where field_3 in (110,360);

update dsg_doctype_3 set kategoria=10 where field_3 in (10,30,50,60,70,90,110,130,140,160,180,210,230,250,290,320,380,400,420,440,452,454,456,458,460,480,500,520,540,560,580,595);
update dsg_doctype_3 set kategoria=20 where field_3 in (20,40,80,100,120,150,170,190,202,204,220,240,260,300,330,390,410,430,450,470,490,510,530,550,570,590);
update dsg_doctype_3 set kategoria=30 where field_3 in (200,270,280,310,340,600);