CREATE TABLE DSG_DOCTYPE_1(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_0 varchar(50),
	FIELD_1 timestamp NULL,
	FIELD_2 integer NULL,
	FIELD_3 integer NULL,
	field_4 varchar(50),
	nw_class varchar(1),
	kategoria integer NULL
);

CREATE TABLE DS_NW_STATUS_TRACE (
    DOCUMENT_ID         numeric(18, 0) NOT NULL,
    STATUS              integer NOT NULL,
    STATUS_CN           varchar(20),
    MTIME               timestamp NOT NULL
);
