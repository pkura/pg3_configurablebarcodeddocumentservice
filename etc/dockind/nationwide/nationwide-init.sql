INSERT INTO DS_FOLDER (TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)
VALUES ('Archiwum', 1, 0, current_timestamp, 'admin', 'businessarchive', 1);

INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE) VALUES (1, 'NW_WSZYSCY', 'Wszyscy', 'group');
INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE) VALUES (1, 'NW_OGOLNE', 'Og�lne', 'group');
INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE) VALUES (1, 'NW_SWIADCZENIOWE', '�wiadczeniowe', 'group');
INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE) VALUES (1, 'NW_RAPORTY', 'Raporty', 'group');
insert into ds_division (guid, name, code,divisiontype, parent_id, hidden, description) values ('NW_OGOLNE_EDYCJA', 'Og�lna - edycja',null,'group',1,0,null);
INSERT INTO DS_FOLDER (TITLE, PARENT_ID, HVERSION, CTIME, AUTHOR, SYSTEMNAME,ATTRIBUTES)VALUES ('Archiwum', 1, 0, current_timestamp, 'admin', 'businessarchive', 1);
create table nw_perm (
    document_id     numeric(19,0),
    hash            numeric(19,0)
);