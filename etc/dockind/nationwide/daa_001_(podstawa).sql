CREATE TABLE [DSG_DOCTYPE_3](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[FIELD_0] [int] NULL,
	[FIELD_1] [int] NULL,
	[FIELD_2] [datetime] NULL,
	[FIELD_3] [int] NULL,
	[FIELD_4] [int] NULL,
	[FIELD_5] [int] NULL,
	[FIELD_6] [int] NULL,
	[kategoria] [int] NULL
);

CREATE TABLE [daa_agencja](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rodzaj] [int] NULL,
	[nazwa] [varchar](255) NULL,
	[nip] [varchar](20) NULL,
	[ulica] [varchar](255) NULL,
	[kod] [varchar](255) NULL,
	[miejscowosc] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[faks] [varchar](255) NULL,
	[telefon] [varchar](255) NULL,
	[fk_rodzaj_sieci] [int] NULL,
	[numer] [varchar](60) NULL,
	[compan_id] [int] NOT NULL DEFAULT ((-1)),
	PRIMARY KEY (id)
);

CREATE TABLE [daa_agent](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nazwisko] [varchar](255) NOT NULL,
	[imie] [varchar](255) NOT NULL,
	[fk_daa_agencja] [int] NULL,
	[numer] [varchar](60) NULL,
	[compan_id] [int] NOT NULL DEFAULT ((-1)),
	PRIMARY KEY (id)
);

