Subject: Zaleg�a korespondencja 

Cze�� ${imie}

W kancelarii znajduje si� ${n} dokument�w oczekuj�cych na Tw�j odbi�r.
Poni�ej znajduj� si� lista dokument�w:

${msg}

Zapraszamy po odbi�r korespondencji do Kancelarii Og�lnej w godz. 10.00 - 17.00. 
Mail wygenerowany automatycznie prosimy na niego nie odpowiada�.