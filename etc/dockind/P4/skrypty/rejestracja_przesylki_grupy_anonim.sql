CREATE TABLE DSG_GROUP (
	id INT IDENTITY,
	cn VARCHAR(50),
	title VARCHAR(255),
	centrum	INT,
	refValue VARCHAR(20),
	available BIT NOT NULL
);

INSERT INTO DSG_GROUP VALUES ('ka', 'kancelaria', null, null, 1);
INSERT INTO DSG_GROUP VALUES ('nd', 'n-dni�wki', null, null, 1);
INSERT INTO DSG_GROUP VALUES ('ndnieuznana', 'n-dni�wki - nieuznana', null, null, 1);
INSERT INTO DSG_GROUP VALUES ('ndreklamacje', 'n-dni�wki - reklamacje', null, null, 1);
INSERT INTO DSG_GROUP VALUES ('ndtrudnesprawy', 'n-dni�wki - trudne sprawy', null, null, 1);
INSERT INTO DSG_GROUP VALUES ('inne', 'inne', null, null, 1);

INSERT INTO DS_DIVISION VALUES ('ka', 'kancelaria', null, 'group', 1, null, 0, null);
INSERT INTO DS_DIVISION VALUES ('nd', 'n-dni�wki', null, 'group', 1, null, 0, null);
INSERT INTO DS_DIVISION VALUES ('ndnieuznana', 'n-dni�wki - nieuznana', null, 'group', 1, null, 0, null);
INSERT INTO DS_DIVISION VALUES ('ndreklamacje', 'n-dni�wki - reklamacje', null, 'group', 1, null, 0, null);
INSERT INTO DS_DIVISION VALUES ('ndtrudnesprawy', 'n-dni�wki - trudne sprawy', null, 'group', 1, null, 0, null);
INSERT INTO DS_DIVISION VALUES ('inne', 'inne', null, 'group', 1, null, 0, null);

CREATE TABLE DSG_P4_PARCEL_REGISTRY (
	DOCUMENT_ID NUMERIC(19,0),
	NR_MSISDN VARCHAR(15),
	NR_IMEI VARCHAR(15),
	REMARK VARCHAR(4000),
	DELIVERY_TYPE NUMERIC(18,0),
	GROUP_TYPE VARCHAR(64),
	STATUS VARCHAR(32)
);

INSERT INTO DSO_PERSON (DISCRIMINATOR, DICTIONARYTYPE, DICTIONARYGUID, ANONYMOUS, FIRSTNAME, LASTNAME) VALUES ('PERSON', 'sender', 'rootdivision', 1, NULL, NULL);