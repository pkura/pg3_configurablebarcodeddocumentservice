CREATE TABLE [DSG_ALDUMOWY] (
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
	[kategoria] int,
	[opis_gl] int,
	[nr_klienta] varchar(20),
	[nazwa_klienta] varchar(100),
	[data_podpisania] datetime,
	[numer_rejestracyjny] varchar(7),
	[nr_kontraktu] varchar(20),
);