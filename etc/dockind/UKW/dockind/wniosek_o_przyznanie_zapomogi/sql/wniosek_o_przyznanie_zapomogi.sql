create table dsg_wniosek_zapomoga 
(
	document_id numeric(19,0) not null,
	DATA_ZATRUDNIENIA date null,
	STATUS_PRACY numeric(18,0) null,
	PRAC_BRUTTO varchar(150) null,
	PRAC_BRUTTO_WSPOL varchar(150) null,
	ER_BRUTTO varchar(150) null,
	ER_BRUTTO_WSPOL varchar(150) null,
	OSOBY_NA_UTRZYMANIU int null,
	MIEJSCOWOSC varchar(150) null,
	KOD_POCZTOWY varchar(6) null,
	ULICA_NR varchar(150) null,
	UZASADNIENIE_WNIOSKU varchar(400) null,
	BYDGOSZCZ_DNIA date null,
	ILOSC_AKCEPTACJI INT NULL DEFAULT(0),
	ILOSC_ODRZUCEN INT NULL DEFAULT(0),
	STOPIEN_WIDOCZNOSCI INT NULL DEFAULT(0),
	STATUS_AKCEPTACJI varchar(50) null
);


