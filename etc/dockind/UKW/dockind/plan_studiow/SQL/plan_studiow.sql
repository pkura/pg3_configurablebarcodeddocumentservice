CREATE TABLE dsg_ukw_plan_studiow(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	rodzaj_int int NULL,
	send_kind int NULL,
	signature varchar(150) NULL,
	BARCODE varchar(50) NULL,
	format varchar(250) NULL,
	dostep int NULL,
	NIE_PELNE_CYFROWE smallint NULL,
	WERYFIKACJA_WG_JRWA smallint NULL,
	zalacznik_tok_studiow numeric(18,0) NULL,
	status numeric(18,0) NULL
) ON [PRIMARY]

CREATE TABLE dsg_ukw_plan_studiow_multi(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
)
