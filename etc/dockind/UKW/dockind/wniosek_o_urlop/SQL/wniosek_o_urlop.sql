CREATE TABLE DS_UKW_WNIOSEK_O_URLOP (
	DOCUMENT_ID	numeric (19,0) NOT NULL,
	typ_wniosku numeric(18,0) NULL,
	typ_wnioskuERP numeric(18,0) NULL,
	przelozony numeric(18,0) NULL,
	rok_urlopu date NULL,
	wymiar_dni numeric(18,0) NULL,
	data_urlopu_od date NULL,
	data_urlopu_do date NULL,
	zgoda int NULL,
	stanowisko varchar(20) NULL,
	data_urlopu_przelozony_od date NULL,
	data_urlopu_przelozony_do date NULL,
	nreewid varchar(20) NULL,
	zastepca varchar(20) NULL,
	dnia date NULL,
	ilosc_tygodni int NULL,
	macierzynski_od date NULL,
	macierzynski_do date NULL,
	imie_dziecka varchar(20) NULL,
	nazwisko_dziecka varchar(20) NULL,
	urodzone date NULL,
	urlop_wychowawczy_potw smallint NULL,
	urlop_wychowawczy_potw1 smallint NULL,
	wykorzystanie_urlopow varchar(20) NULL,
	nie_korzysta smallint NULL,
	uzyskanie_oswiadczenia smallint NULL,
	oswiadczenie_imie varchar(20) NULL,
	oswiadczenie_nazwisko varchar(20) NULL, 
	bedacego varchar(20) NULL,
	powod varchar(20) NULL,
	ograniczenie smallint NULL,
	wymaga_opieki smallint NULL,
	zalacznik_rezygnacja numeric(18,0) NULL,
	zalacznik_ograniczenie numeric(18,0) NULL,
	zalacznik_orzeczenie numeric(18,0) NULL,
	urlop_okolicznosciowy varchar(20) NULL,
	urlop_okolicznosciowy_podstawa varchar(500) NULL,
	urlop_okolicznosciowy_liczba int NULL,
	nadgodziny smallint NULL,
	nadgodziny_daty numeric(18,0) NULL,
	wolny_od_pracy smallint NULL,
	wolne_dni_daty numeric(18,0) NULL,
	liczba_dostepnych_dni int NULL,
	status numeric(18,0) NULL
) ON [PRIMARY]

alter table DS_UKW_WNIOSEK_O_URLOP add zalacznik_akt_urodzenia numeric(18,0) NULL;
alter table DS_UKW_WNIOSEK_O_URLOP add ilosc_tygodni_wyp int NULL;

CREATE TABLE dsg_ukw_wykorzystanie_urlopow(
	id numeric (19,0) IDENTITY(1,1) NOT NULL,
	od date NULL,
	do date NULL,
) ON [PRIMARY]

CREATE TABLE dsg_ukw_nadgodziny_daty(
	id numeric (19,0) IDENTITY(1,1) NOT NULL,
	dataod date NULL,
	do date NULL,
) ON [PRIMARY]

CREATE TABLE dsg_ukw_wolne_dni_daty(
	id numeric (19,0) IDENTITY(1,1) NOT NULL,
	dataod date NULL,
	do date NULL,
) ON [PRIMARY]

CREATE TABLE dsg_ukw_wniosek_urlop_multi(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
)

CREATE TABLE dsg_ukw_typy_wnioskow_urlopowych(
	id numeric(18,0) IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
) ON [PRIMARY]

insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('WniosekUrlopowy', 'Wniosek urlopowy');
insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('urlopNaZadanie167', 'Urlop na żądanie(na podstawie art.167 Kodeksu pracy)');
insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('urlopOkolicznosciowy', 'Wniosek o urlop okolicznościowy');
insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('udzielenieDniWolnychOdPracy', 'Wniosek o udzielenie dni wolnych od pracy');
insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('udzielenieUrlopuWychowawczego', 'Wniosek o udzielenie urlopu wychowawczego');
insert dsg_ukw_typy_wnioskow_urlopowych(cn, title) values ('udzielenieUrlopuOjcowskiego', 'Wniosek o udzielenie urlopu ojcowskiego');
