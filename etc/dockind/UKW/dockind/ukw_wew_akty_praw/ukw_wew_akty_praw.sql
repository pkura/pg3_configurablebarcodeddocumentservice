CREATE TABLE dsg_ukw_rodzaj_aktu (
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) DEFAULT 'pl' NOT NULL);
	
CREATE TABLE dsg_ukw_tagi (
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[language] [varchar](256) DEFAULT 'pl' NOT NULL);

CREATE TABLE DS_UKW_WEW_AKTY (
[DOCUMENT_ID]			[numeric] (19,0) NOT NULL,
[rodzajAktu]		[numeric](18, 0) NULL,
[slowaKluczowe]		[numeric](18, 0) NULL,
[numerAktu] 		[varchar](50) NULL,
[dataWejscia]		[date] NULL,
[uchylony] 			[bit] NULL,
[opisUchylenia] 		[varchar](400) NULL
) ON [PRIMARY];



INSERT INTO dsg_ukw_rodzaj_aktu (cn,title,centrum,available,language) VALUES ('1','Uchwa�y Senatu','0','1','pl');
INSERT INTO dsg_ukw_rodzaj_aktu (cn,title,centrum,available,language) VALUES ('2','Zarz�dzenia Rektora','0','1','pl');
INSERT INTO dsg_ukw_rodzaj_aktu (cn,title,centrum,available,language) VALUES ('3','Pisma Ok�lne Rektora','0','1','pl');
INSERT INTO dsg_ukw_rodzaj_aktu (cn,title,centrum,available,language) VALUES ('4','Zarz�dzenia Kanclerza','0','1','pl');
INSERT INTO dsg_ukw_rodzaj_aktu (cn,title,centrum,available,language) VALUES ('5','Pisma Ok�lne Kanclerza ','0','1','pl');

INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('1','Dydaktyka','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('2','Finanse','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('3','Jako�� Kszta�cenia','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('4','Kadra','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('5','Kierunki Studi�w,Specjalno�ci','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('6','Komisja','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('7','Nauka','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('8','Porozumienie/Umowa o wsp�pracy','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('9','Przetarg','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('10','Regulamin','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('11','Struktura,Organizacja','0','1','pl');
INSERT INTO dsg_ukw_tagi (cn,title,centrum,available,language) VALUES ('12','Inwestycje','0','1','pl');