Create view [DS_UKW_ZAMPUB_KIERJED_VIEW]
AS
select users.id, users.name as cn, users.firstname +' '+ users.lastname as title, null as refvalue, null as centrum, 1 as available from DS_USER users
left join DS_USER_TO_DIVISION utd on utd.USER_ID=users.id
left join DS_DIVISION div on div.ID=utd.DIVISION_ID
where div.GUID='AC0F949B001931DB1452123A556CDF5A943' and div.DIVISIONTYPE='group';


CREATE TABLE [DS_UKW_PODMIOTY_REALIZUJACE](
	[ID] [numeric](18, 0) identity(1,1) NOT NULL,
	[NAZWA_I_ADRES_WYKONAWCY] [varchar](255) NULL,
	[WYCENA_ZAM_BRUTTO] [decimal](18, 2) NULL,
	[UWAGI] [varchar](500) NULL,
	[MIEJSCE_REALIZACJI] [varchar](50) NULL,
	[GWARANCJA] [varchar](50) NULL
);


CREATE TABLE [DS_UKW_ZAMPUB_POZ_BUD](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[POZYCJAID] [numeric](18, 0) NULL,
	[OKRES_BUDZETOWY] [numeric](18, 0) NULL,
	[KOMORKA_BUDZETOWA] [numeric](18, 0) NULL,
	[IDENTYFIKATOR_PLANU] [numeric](18, 0) NULL,
	[IDENTYFIKATOR_BUDZETU] [numeric](18, 0) NULL,
	[POZYCJA_PLANU_ZAKUPOW] [varchar](256) NULL,
	[POZYCJA_BUDZETU] [varchar](256) NULL,
	[PRODUKT] [numeric](18, 0) NULL,
	[OPIS] [varchar](250) NULL,
	[ILOSC] [numeric](18, 0) NULL,
	[JM] [numeric](18, 0) NULL,
	[STAWKA_VAT] [numeric](18, 0) NULL,
	[NETTO] [decimal](18, 2) NULL,
	[BRUTTO] [decimal](18, 2) NULL,
	[UWAGI] [varchar](250) NULL
);


CREATE TABLE [DS_UKW_ZAMPUB_ZRODLA_FIN](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[POZYCJAID] [numeric](18, 0) NULL,
	[ZADANIE_FINANSOWE_PLANU_RAW] [varchar](250) NULL,
	[ZADANIE_FINANSOWE_BUDZETU_RAW] [varchar](250) NULL,
	[PROCENT] [decimal](18, 2) NULL,
	[BRUTTO] [decimal](18, 2) NULL
);


CREATE TABLE [DS_UKW_AKCEPTACJE](
	[ID] [numeric](18, 0)identity(1,1) NOT NULL,
	[OSOBA] [numeric](18, 0) NULL,
	[DATA] [date] NULL,
	[STATUS] [numeric](18, 0) NULL,
	[UWAGI] [varchar](250) NULL
);


CREATE TABLE [DS_UKW_KRYTERIA_DECYZJI](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[KRYTERIUM] [varchar](80) NULL,
	[WAGA] [numeric](18, 0) NULL
);


CREATE TABLE [DS_UKW_ZEBRANE_OFERTY](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NAZWA_WYKONAWCY] [varchar](255) NULL,
	[ADRES] [varchar](50) NULL,
	[NETTO] [decimal](18, 2) NULL,
	[INNE_KRYTERIA] [varchar](50) NULL,
	[UWAGI] [varchar](50) NULL,
	[MIEJSCE_REALIZACJI] [varchar](50) NULL,
	[OKRES_GWARANCJI] [varchar](50) NULL,
	[WARUNKI_PLATNOSCI] [varchar](50) NULL
);


CREATE TABLE [DS_UKW_ZAMPUB](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[STATUS] [numeric](18, 0) NULL,
	[ERP_STATUS] [numeric](18, 0) NULL,
	[RODZAJ_WNIOSKU] [numeric](18, 0) NULL,
	[RODZAJ_ZAMOWIENIA] [numeric](18, 0) NULL,
	[CZY_ROBOTA_BUDTELE] [tinyint] NULL,
	[OS_REJESTRUJACA] [numeric](18, 0) NULL,
	[OS_REJESTRUJACA_DZIAL] [numeric](18, 0) NULL,
	[ERP_DOCUMENT_IDM] [varchar](50) NULL,
	[NR_WNIOSKU] [varchar](50) NULL,
	[ROK_BUDZETOWY] [numeric](4, 0) NULL,
	[DATA_UTWORZENIA] [datetime] NULL,
	[NETTO] [decimal](18, 2) NULL,
	[BRUTTO] [decimal](18, 2) NULL,
	[NETTO_EURO] [decimal](18, 2) NULL,
	[KURS] [decimal](18, 6) NULL,
	[DATA_UST_WART_ZAM] [datetime] NULL,
	[OS_UST_WART_ZAM] [numeric](18, 0) NULL,
	[PODSTAWA_ZAM] [numeric](18, 0) NULL,
	[OPIS_PODSTAWA_ZAM_INNE] [varchar](350) NULL,
	[OSWIADCZENIE_NOTATKA] [tinyint] NULL,
	[MIEJSCE_REALIZACJ] [varchar](255) NULL,
	[GWARANCJA] [varchar](255) NULL,
	[TYP_REZERWACJI] [numeric](18, 0) NULL,
	[CZY_ART_6A] [tinyint] NULL,
	[CZY_ZAGROZENIE] [tinyint] NULL,
	[CZY_ZC] [tinyint] NULL,
	[CZY_INNE] [tinyint] NULL,
	[INNE_WYLACZENIA] [numeric](18, 0) NULL,
	[AKCEPTACJA_CZ] [numeric](18, 0) NULL,
	[AKCEPTACJE_DYS] [numeric](18, 0) NULL,
	[KJS_OR_KP] [numeric](18, 0) NULL,
	[TERMIN_WYKONANIA_ZAM] [datetime] NULL,
	[OKRES_GWARANCJI_REKOJMI] [varchar](50) NULL,
	[WARUNKI_PLATNOSCI] [varchar](50) NULL,
	[TERMIN_ZLOZENIA_OFERTY] [datetime] NULL,
	[W_MIEJSCU] [varchar](50) NULL,
	[DOPISEK_NA_KOPERTE] [varchar](50) NULL,
	[FAKSEM_NA] [numeric](18, 0) NULL,
	[MAILEM_NA] [varchar](50) NULL,
	[UZASADNIENIE_WYBORU] [varchar](50) NULL,
	[OSWIADCZENIE_ZAPROSZENIE] [tinyint] NULL,
	[ZALACZNIKI] [tinyint] NULL,
	[OSOBA_USTALAJACA] [numeric](18, 0) NULL,
	[DATA_USTALENIA] [datetime] NULL,
	[UJETO_W_PLANIE_POZ] [varchar](50) NULL,
	[NR_SYGNATURY] [varchar](50) NULL,
	[OSOBA_MERYTORYCZNA] [numeric](18, 0) NULL,
	[AKCEPTACJA_DZIALU] [numeric](18, 0) NULL,
	[OSOBA_SKLADAJACA_ZAM] [numeric](18, 0) NULL,
	[JEDNOSTKA_ZAMAWIAJACEGO] [numeric](18, 0) NULL,
	[UPOWAZNIONY_PRACOWNIK] [numeric](18, 0) NULL
);


ALTER TABLE [DS_UKW_ZAMPUB] ADD [VALID] [tinyint] NULL;
ALTER TABLE [DS_UKW_ZAMPUB] ADD [TYP_REZERWACJI_ERP] [numeric](18, 0) NULL;


CREATE TABLE [DS_UKW_ZAMPUB_MULTI](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](50) NOT NULL,
	[FIELD_VAL] [varchar](50) NOT NULL
);


create view [dsg_invoice_general_budzet_with_ref] AS
SELECT * 
FROM (
	SELECT CONVERT(numeric(19, 0), bd_budzet_id) AS id, id AS cn, (budzet_nazwa + ': ' + dok_kom_nazwa) AS title, ROW_NUMBER() OVER (PARTITION BY bd_budzet_id ORDER BY id) AS centrum, convert(numeric(19,0),budzet_id) AS refValue, available
	FROM dsg_budzety
	WHERE bd_rodzaj = 'BK'
) a
WHERE a.centrum = 1;


create table [dsg_budget_to_division] (
	id numeric(19,0) identity(1,1),
	[budzet_id] [numeric](10, 0) NOT NULL,
	[budzet_idm] [varchar](20) NOT NULL,
	[wersja] [int] NOT NULL,
	[nazwa] [varchar](128) NULL,
	[data_obow_od] [datetime] NULL,
	[data_obow_do] [datetime] NULL,
	[czy_archiwalny] [numeric](1, 0) NOT NULL,
	komorka_idn varchar(50) not null,
	komorka_id [numeric](19, 0) NOT NULL,
	division_id [numeric](19, 0) NOT NULL,
	division_guid varchar(50) NOT NULL
);


--dsg_budzetyKO_komorek etc\dockind\invoiceGeneral\sql\mssql\BudzetyKomorek.sql
--ten insert jest do sprawdzenia wywala się przez brak pola wersja w budzetyKO_komorek
insert into dsg_budget_to_division
select b.budzet_id as [budzet_id], b.idm as [budzet_idm], b.[wersja], b.[nazwa], null, null, 0, div.CODE as komorka_idn, cast(cast(d.erp_id as float) as bigint) as komorka_id, d.id as division_id, div.guid as division_guid
from dsg_budzetyKO_komorek b
join ds_division_erp d on b.erpId=d.erp_id 
join DS_DIVISION div on div.ID=d.ds_division_id;


--dsg_typy_wnioskow_o_rez etc\dockind\invoiceGeneral\sql\mssql\TypyWnioskowORezerwacje.sql
CREATE VIEW [typy_rezerwacji]
AS
SELECT erpId AS id, idm AS cn, nazwa AS title, czy_obsluga_pzp AS refValue, czy_aktywny AS available, NULL AS centrum, czy_obsluga_pzp
FROM dsg_typy_wnioskow_o_rez
GO


--dsg_plany_zakupow etc\dockind\invoiceGeneral\sql\mssql\PlanyZakupow.sql
create view [dsg_plany_zakupow_view] as
SELECT erpId AS id, idm AS cn, nazwa AS title, budzet_id AS refvalue, NULL AS centrum, 1 AS available
FROM dsg_plany_zakupow
WHERE (czyAgregat = 0);

ALTER TABLE DS_UKW_ZAMPUB_POZ_BUD ADD
	DYSPONENT numeric(18, 0) NULL,
	DATA_AKCEPTACJI date NULL,
	STATUS_AKCEPTACJI int NULL;