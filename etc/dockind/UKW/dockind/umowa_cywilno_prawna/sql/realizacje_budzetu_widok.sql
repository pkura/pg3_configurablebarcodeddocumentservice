-- 1. Widok nieodczytuje wst�pnych kwot, dla dokumentow o statusie 1405 i 2000, oznaczaj�cych odpowiednio Odrzucenie dokumentu oraz zaakceptowanie
-- 2. Widok obs�uguje 5 dockind�w, WZP,WZE,WMU,WKO,Costinvoice
-- 3. Widok potrzebuje utworzenia innego widoku: dsg_pcz_document_multiple_value_view, scalaj�cego dla wszystkich dokument�w tabele typu multiple_value
-- 4. Dla wszystkich status�w blokad, id oznaczaj�cy blokad� OSTATECZN� ma warto�� 16061

create view realizacje_budzetu_view as
select	realizacje.pozycja_id as id,
		realizacje.pozycja_id,
		realizacje.budzet,
		realizacje.pozycja,
		realizacje.planowana,
		realizacje.ostatecznie_zarezerwowana, 
		CAST(ISNULL(mpk.wstepna,0)+realizacje.ostatecznie_zarezerwowana as numeric(18,2)) as wstepnie_zarezerwowana, 
		realizacje.ostatecznie_wykonana, 
		realizacje.ostatecznie_wykonana+ISNULL(mpk_inv.wstepna,0) as wstepnie_wykonana, 
		realizacje.ostatecznie_pozostala, 
		CAST(realizacje.planowana-(realizacje.ostatecznie_wykonana+ISNULL(mpk_inv.wstepna,0)) as numeric(18,2)) as wstepnie_pozostala,
		ISNULL(mpk.wstepna,0) as wstepna_z_wnioskow,
		ISNULL(mpk_inv.wstepna,0) as wstepna_z_faktur
from(
	select bd_poz_budzet_id as pozycja_id, kom_bud_nazwa as budzet,bd_poz_budzet_nazwa as pozycja, max(bd_poz_p_koszt) as planowana, max(abs(bd_poz_rez_koszt)) as ostatecznie_zarezerwowana, max(bd_poz_r_koszt) as ostatecznie_wykonana, max(bd_poz_do_wyk_koszt) as ostatecznie_pozostala
	from ERP_DB.simpleERP.dbo.v_bd_budzetowanie_uczelni where zrodlo_nazwa is not null --klauzula where, dlatego bo widok z simple.erp jest s�aby!!!!
	group by bd_poz_budzet_id,kom_bud_nazwa,bd_poz_budzet_nazwa
) realizacje
FULL OUTER JOIN (
	select accountNumber, SUM(amount) as wstepna from dsg_pcz_document_multiple_value_view mul
	JOIN DSG_CENTRUM_KOSZTOW_FAKTURY mpk ON mul.FIELD_VAL=mpk.id and mul.document_id IN
	(
		select document_id from dsg_pcz_application_wzp where status NOT IN (2000,1405)
		UNION
		select document_id from dsg_pcz_application_wze where status NOT IN (2000,1405)
		UNION
		select document_id from dsg_pcz_application_wmu where status NOT IN (2000,1405)
		UNION
		select document_id from dsg_pcz_application_wko where status NOT IN (2000,1405)
	)
	and mul.FIELD_CN='MPK'
	where mpk.lokalizacja is not null and mpk.lokalizacja != '16061' -- 16061 id dla statusu blokady: OSTATECZNA
	group by accountNumber
) mpk ON mpk.accountNumber=realizacje.pozycja_id
FULL OUTER JOIN (
		select accountNumber, SUM(amount) as wstepna from dsg_pcz_document_multiple_value_view mul
		JOIN DSG_CENTRUM_KOSZTOW_FAKTURY mpk ON mul.FIELD_VAL=mpk.id and mul.document_id IN
		(
			select document_id from dsg_pcz_costinvoice where status NOT IN (2000,1405)
		)
		and mul.FIELD_CN='MPK'
		where accountNumber is not null
		group by accountNumber
) mpk_inv ON mpk_inv.accountNumber=realizacje.pozycja_id
where pozycja_id is not null