create table dsg_wniosek_pozyczka 
(
	document_id numeric(19,0) not null,
	OSOBA_Z_ZEWNATRZ bit null,
	NUMER_SPRAWY varchar(50) null,
	KWOTA_WNIOSKOWANA numeric(18,2) null,
	IMIE_ZEW varchar(150) null,
	NAZWISKO_ZEW varchar(150) null,
	OKRES_POZYCZKI int null,
	CEL_POZYCZKI numeric(19, 0) null,
	PRZEKAZANIE_NA numeric(19, 0) null,
	BYDGOSZCZ_DNIA date null,
	MIEJSCOWOSC varchar(150) null,
	KOD_POCZTOWY varchar(6) null,
	ULICA_NR varchar(150) null,
	WYZ_WYMIENIONY_JEST numeric(19, 0) null,
	WYZ_WYMIENIONY_CD varchar(150) null,
	KWALIFIKACJA_KOMISJI varchar(400) null,
	DATA_KWALIFIKACJI date null,
	PRZYZNAJE_POZYCZK_NA varchar(400) null,
	KWOTA_PRZYZNANA numeric(18, 2) null,
	KWOTA_PRZYZNANA_STR varchar(250) null,
	ODMOWA_POWOD varchar(400) null,
	POTW_DZIALU_KADR numeric(18, 0) null,
	DATA_POTWIERDZENIA date null,
	ATTACHEMENT_ID numeric(19,0) null,
	ILOSC_AKCEPTACJI INT NULL DEFAULT(0),
	ILOSC_ODRZUCEN INT NULL DEFAULT(0),
	STOPIEN_WIDOCZNOSCI INT NULL DEFAULT(0),
	STATUS_AKCEPTACJI varchar(50) null
);

create table ds_ukw_cel_pozyczki
(
	id numeric(18, 0) identity(1,1) NOT NULL, 
	cn varchar(250), 
	title varchar(250), 
	centrum int, 
	refValue varchar(20), 
	available bit
);

create table ds_ukw_przekazanie_na
(
	id numeric(18, 0) identity(1,1) NOT NULL, 
	cn varchar(250), 
	title varchar(250), 
	centrum int, 
	refValue varchar(20), 
	available bit
);



create table ds_ukw_wymieniony_jest
(
	id numeric(18, 0) identity(1,1) NOT NULL, 
	cn varchar(250), 
	title varchar(250), 
	centrum int, 
	refValue varchar(20), 
	available bit
);







