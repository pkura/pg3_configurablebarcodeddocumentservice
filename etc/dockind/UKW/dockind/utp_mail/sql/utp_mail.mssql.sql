CREATE TABLE [dbo].[DOCUMENT_IN_EMAIL](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[typ_slownika] [int] NULL,
	[temat] [varchar](50) NULL,
	[tresc] [varchar](50) NULL,
	[wyslac] [int] NULL,
	[typ_slownika_kopia] [int] NULL,
	[typ_slownika_ukrytaKopia] [int] NULL,
	[format] [varchar](50) NULL,
	[typ] [int] NULL,
	[dostep] [int] NULL,
	[wyslano] [int] NULL
) ON [PRIMARY]

CREATE VIEW [dbo].[V_DSO_PERSON]
AS
SELECT     ID AS id, DISCRIMINATOR, TITLE, FIRSTNAME, LASTNAME, ORGANIZATION, ORGANIZATIONDIVISION, STREET, ZIP, PESEL, LOCATION, NIP, REGON, EMAIL, 
                      COUNTRY, REMARKS, FAX
FROM         dbo.DSO_PERSON
WHERE     (DISCRIMINATOR = 'person')