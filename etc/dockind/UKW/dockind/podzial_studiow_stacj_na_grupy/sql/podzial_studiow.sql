
CREATE TABLE [DSG_UKW_PODZIAL_STUDIOW](
	[DOCUMENT_ID] [numeric](19, 0) NOT null,
	[rodzaj_int] [int] null,
	[send_kind] [int] null,
	[signature] [varchar](150) null,
	[BARCODE] [varchar](50) null,
	typ varchar(250) null,
	format varchar(250) null,
	dostep int null,
	NIE_PELNE_CYFROWE smallint,
	WERYFIKACJA_WG_JRWA smallint,
	numerKolejny int null,
	ATT_KOREKTA numeric(19,0) null,
	WIDOCZNOSC_ATT_KOR bit null
) ON [PRIMARY]




