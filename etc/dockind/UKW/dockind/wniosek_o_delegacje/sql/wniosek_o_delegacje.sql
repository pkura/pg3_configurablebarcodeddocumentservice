create table dsg_wniosek_delegacja
(
	document_id numeric(19,0) not null,
	PRZELOZONY numeric(18,0) null,
	OSOBA_DELEGUJACA numeric(18,0) null,
	SRODEK_LOKOMOCJI numeric(19, 0) null,
	RODZAJ_WYJAZDU numeric(18,0) null,
	WYMAGANA_ZALICZKA bit null,
	ATT_KARTA_PRZEBIEGU numeric(19,0) null,
	ATT_UMOWA_KILOMETR numeric(19,0) null,
	ATT_ZALICZKA numeric(19,0) null,
	WIDOCZNOSC_STATUSU bit null,
	STATUS_AKCEPTACJI varchar(50) null,
	RODZ_KOSZTOW_MULTI int null,
	KONTO_KOSZTOWE varchar(100) null,
	MIEJSCE_WYJAZDU varchar(150) null,
	CEL_POBYTU varchar(150) null,
	DATA_DELEGACJI_OD date null,
	DATA_DELEGACJI_DO date null,
	KWOTA_ZALICZKI numeric(18, 2) null,
	REQUIRED_KONTO_KOSZT bit null,
	PRACOWNIK_TYP numeric(19, 0) null
);


create table ds_ukw_srodek_lokomocji
(
	id numeric(18, 0) identity(1,1) NOT NULL, 
	cn varchar(250), 
	title varchar(250), 
	centrum int, 
	refValue varchar(20), 
	available bit
);


CREATE TABLE dsg_ukw_wniosek_delegacja_multi(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
)

CREATE TABLE dsg_ukw_rodzaj_kosztow(
	id numeric (19,0) IDENTITY NOT NULL,
	RODZAJ_KOSZTOW numeric(18,0) NULL,
	KOSZT numeric(18, 2) NULL,
) ON [PRIMARY]



------------------------ insert ---------------------------------

INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('SMP','samoch�d prywatny',0,NULL,1);

--powy�szy insert nale�y wykona� jako pierwszy poniewa� droolsy s� oparte o id tego pola (- -')
--je�eli przed wykonaniem tych insert�w w tabeli stnia�y jakie� rekordy, nale�y  je usun�� i wgra� powy�sze (id - identity(1,1))
 
INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('SMS','samoch�d s�u�bowy',0,NULL,1);
INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('PKS','pks',0,NULL,1);
INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('PKP','pkp (poci�g)',0,NULL,1);

-----------------------------------------------------------------

create table dsg_ukw_pracownik_typ
(
	id numeric(18, 0) identity(1,1) NOT NULL, 
	cn varchar(250), 
	title varchar(250), 
	centrum int, 
	refValue varchar(20), 
	available bit
);

------------------------ insert ---------------------------------
INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('ADM','administracyjny',0,NULL,1);
INSERT INTO [ds_ukw_srodek_lokomocji] ([cn],[title],[centrum],[refValue],[available]) VALUES('DYD','dydaktyczny',0,NULL,1);

