CREATE VIEW [dbo].[v_dso_container]
AS
SELECT     ID, PARENT_ID, DISCRIMINATOR, OFFICEID, OFFICEIDPREFIX, OFFICEIDMIDDLE, OFFICEIDSUFFIX, CLERK, AUTHOR, CTIME, SEQUENCEID, RWA_ID, CASE_YEAR, 
                      OPENDATE, FINISHDATE, description, TITLE
FROM         dbo.DSO_CONTAINER
WHERE     (DISCRIMINATOR = 'CASE')

GO

CREATE VIEW [dbo].[v_dso_rwaa]
AS
SELECT     ID, ISNULL(digit1, '') + ISNULL(digit2, '') + ISNULL(digit3, '') + ISNULL(digit4, '') + ISNULL(digit5, '') AS code, achome, acother, description, remarks
FROM         dbo.dso_rwa

GO

CREATE VIEW [dbo].[v_ds_division]
AS
SELECT     ID, NAME, CODE
FROM         dbo.DS_DIVISION
WHERE     (NOT (CODE IS NULL))

GO