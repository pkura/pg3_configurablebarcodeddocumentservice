CREATE TABLE [dbo].[DSG_KONTRAHENT](
   [id] [numeric](19,0) IDENTITY(1,1) NOT NULL,
    [imie] [varchar](50) NULL,
    [nazwisko] [varchar](50) NULL,
    [nazwa] [varchar](1000) NULL,
    [ulica] [varchar](1000) NULL,
    [nrDomu] [varchar](30) NULL,
    [nrMieszkania] [varchar](30) NULL,
    [kodpocztowy] [varchar](50) NULL,
    [miejscowosc] [varchar](1000) NULL,
    [nip] [varchar](50) NULL,
    [regon] [varchar](50) NULL,
    [kraj] [varchar](1000) NULL,
    [email] [varchar](256) NULL,
    [pesel] [varchar](20) NULL,
    [faks] [varchar](256) NULL,
    [telefon] [varchar](256) NULL,
    [nrKonta] [varchar](50) NULL,
    [erpId] [numeric](19,0) NULL,
    [nowy] [smallint] NULL
    [skrytkaPocztowa] [varchar](256) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[UTP_STUDENT](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [imie] [varchar](50) NULL,
    [nazwisko] [varchar](50) NULL,
    [nazwa] [varchar](1000) NULL,
    [ulica] [varchar](1000) NULL,
    [kodpocztowy] [varchar](50) NULL,
    [miejscowosc] [varchar](1000) NULL,
    [kraj] [varchar](1000) NULL,
    [email] [varchar](256) NULL,
    [pesel] [varchar](20) NULL,
    [telefon] [varchar](256) NULL,
    [nrAlbumu] [varchar](20) NULL,
    [kierunek] [varchar](100) NULL,
    [punktyECTS] [varchar](50) NULL,
    [skrytkaPocztowa] [varchar](256) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[UTP_KANDYDAT](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [imie] [varchar](50) NULL,
    [nazwisko] [varchar](50) NULL,
    [nazwa] [varchar](1000) NULL,
    [ulica] [varchar](1000) NULL,
    [kodpocztowy] [varchar](50) NULL,
    [miejscowosc] [varchar](1000) NULL,
    [kraj] [varchar](1000) NULL,
    [email] [varchar](256) NULL,
    [pesel] [varchar](20) NULL,
    [telefon] [varchar](256) NULL,
    [nrAlbumu] [varchar](20) NULL,
    [kierunek] [varchar](100) NULL,
    [nazwaUkonczonejSzkoly] [varchar](200) NULL,
    [rokMatury] [date] NULL,
    [oplataRekrutacyjna] [bit] NULL,
    [skrytkaPocztowa] [varchar](256) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[UTP_PRACOWNIK](
    [id] [int] IDENTITY(1,1) NOT NULL,
    [imie] [varchar](50) NULL,
    [nazwisko] [varchar](50) NULL,
    [nazwa] [varchar](1000) NULL,
    [ulica] [varchar](1000) NULL,
    [kodpocztowy] [varchar](50) NULL,
    [miejscowosc] [varchar](1000) NULL,
    [kraj] [varchar](1000) NULL,
    [email] [varchar](256) NULL,
    [pesel] [varchar](20) NULL,
    [telefon] [varchar](256) NULL,
    [skrytkaPocztowa] [varchar](256) NULL
) ON [PRIMARY]

CREATE TABLE [dbo].[UTP_POZOSTALI](
   [id] [int] IDENTITY(1,1) NOT NULL,
    [imie] [varchar](50) NULL,
    [nazwisko] [varchar](50) NULL,
    [nazwa] [varchar](1000) NULL,
    [ulica] [varchar](1000) NULL,
    [kodpocztowy] [varchar](50) NULL,
    [miejscowosc] [varchar](1000) NULL,
    [kraj] [varchar](1000) NULL,
    [email] [varchar](256) NULL,
    [pesel] [varchar](20) NULL,
    [faks] [varchar](256) NULL,
    [telefon] [varchar](256) NULL,
    [skrytkaPocztowa] [varchar](256) NULL
) ON [PRIMARY]