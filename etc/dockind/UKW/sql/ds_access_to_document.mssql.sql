create table dbo.DS_ACCESS_TO_DOCUMENT (
    ID                      int identity not null PRIMARY KEY,
    POSN                    smallint NOT NULL,
    NAME                    varchar(40) NOT NULL
);

insert into DS_ACCESS_TO_DOCUMENT (POSN, NAME) values  ( '1', 'Publiczny � dost�pny w ca�o�ci');
insert into DS_ACCESS_TO_DOCUMENT (POSN, NAME) values ( '2', 'Publiczny � dost�pny cz�ciowo');
insert into DS_ACCESS_TO_DOCUMENT (POSN, NAME) values ( '3', 'Niepubliczny');
