--sposob dostarczenia dla dokumentow przychodzacych
delete FROM dso_in_document_delivery 
	insert into dso_in_document_delivery values ('1','Zwyk�y');
	insert into dso_in_document_delivery values ('2','Rejestrowany');
	insert into dso_in_document_delivery values ('3','Rejestrowany+ZPO');
	insert into dso_in_document_delivery values ('4','Kurier');
	insert into dso_in_document_delivery values ('5','Fax');
	insert into dso_in_document_delivery values ('6','E-mail');
	insert into dso_in_document_delivery values ('7','esp-ePUAP');
	insert into dso_in_document_delivery values ('8', 'Osobi�cie');
	insert into dso_in_document_delivery values ('9', 'No�nik optyczny/pen-drive');

--sposob dostarczenia dla dokumentow wychodzacych	
 delete FROM dso_out_document_delivery 
	insert into dso_out_document_delivery values ('1','Krajowa ekonomiczna');
	insert into dso_out_document_delivery values ('2','Krajowa ekonomiczna polecona');
	insert into dso_out_document_delivery values ('3','Krajowa ekonomiczna polecona zpo');
	insert into dso_out_document_delivery values ('4','Krajowa priorytetowa ');
	insert into dso_out_document_delivery values ('5','Krajowa priorytetowa polecona');
	insert into dso_out_document_delivery values ('6','Krajowa priorytetowa polecona zpo');
	insert into dso_out_document_delivery values ('7','Osobi�cie');
	insert into dso_out_document_delivery values ('8','Paczka');	
	insert into dso_out_document_delivery values ('9','Zagraniczna ekonomiczna');
	insert into dso_out_document_delivery values ('10','Zagraniczna ekonomiczna polecona');
	insert into dso_out_document_delivery values ('11','Zagraniczna priorytetowa');
	insert into dso_out_document_delivery values ('12','Zagraniczna priorytetowa polecona');

	-- rodzaje wag kraj
CREATE TABLE [dbo].[DS_OUT_DELIVERY_COUNTRY_WEIGHT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '1', 'Do 350g', null ,1,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '2', 'Ponad 350g do 1000g', null , 1, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '3', 'Ponad 1000g do 2000g', null , 1, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '4', 'Do 350g', null ,2 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '5', 'Ponad 350g do 1000g', null ,2 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '6', 'Ponad 1000g do 2000g', null ,2 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '7', 'Do 350g', null ,3,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '8', 'Ponad 350g do 1000g', null , 3, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '9', 'Ponad 1000g do 2000g', null , 3, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '10', 'Do 350g', null ,4 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '11', 'Ponad 350g do 1000g', null ,4 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '12', 'Ponad 1000g do 2000g', null ,4 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '13', 'Do 350g', null ,5,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '14', 'Ponad 350g do 1000g', null , 5, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '15', 'Ponad 1000g do 2000g', null , 5, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '16', 'Do 350g', null ,6 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '17', 'Ponad 350g do 1000g', null ,6 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '18', 'Ponad 1000g do 2000g', null ,6 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '19', 'Do 1kg', null ,8,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '20', 'Ponad 1kg do 2kg', null , 8, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '21', 'Ponad 2kg do 5kg', null , 8, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '22', 'Ponad 5kg do 10kg', null ,8 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '23', 'Do 50g', null ,9,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '24', 'Ponad 50g do 100g', null ,9, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '25', 'Ponad 100g do 350g', null , 9, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '26', 'Ponad 350g do 500g', null ,9 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '27', 'Ponad 500g do 1kg', null , 9, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '28', 'Ponad 1kg do 2kg', null , 9, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '29', 'Do 50g', null ,10,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '30', 'Ponad 50g do 100g', null , 10, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '31', 'Ponad 100g do 350g', null , 10, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '32', 'Ponad 350g do 500g', null , 10,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '33', 'Ponad 500g do 1kg', null , 10, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '34', 'Ponad 1kg do 2kg', null , 10, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '35', 'Do 50g', null ,11,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '36', 'Ponad 50g do 100g', null , 11, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '37', 'Ponad 100g do 350g', null , 11, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '38', 'Ponad 350g do 500g', null ,11 ,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '39', 'Ponad 500g do 1kg', null , 11, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '40', 'Ponad 1kg do 2kg', null , 11, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '41', 'Do 50g', null ,12,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '42', 'Ponad 50g do 100g', null , 12, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '43', 'Ponad 100g do 350g', null , 12, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '44', 'Ponad 350g do 500g', null , 12,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '45', 'Ponad 500g do 1kg', null , 12, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '46', 'Ponad 1kg do 2kg', null , 12, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '47', 'Inna', null , 1, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '48', 'Inna', null , 2, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '49', 'Inna', null , 3, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '50', 'Inna', null , 4, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '51', 'Inna', null , 5, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '52', 'Inna', null , 6, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '53', 'Inna', null , 8, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '54', 'Inna', null , 9, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '55', 'Inna', null , 10, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '56', 'Inna', null , 11, 1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '57', 'Inna', null , 12, 1);

	drop table DS_OUT_DELIVERY_GABARYT;
	
	-- rodzaje Gabaryt�w 
	CREATE TABLE [dbo].[DS_OUT_DELIVERY_GABARYT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1,
	[kwota] numeric(6, 2)
	
) ON [PRIMARY]

--gabaryt i kwota list ekonomiczny 
insert into DS_OUT_DELIVERY_GABARYT values  ( '1', 'KRAJ - Gabaryt A', null, 1, 1, 1.75);
insert into DS_OUT_DELIVERY_GABARYT values  ( '2', 'KRAJ - Gabaryt A', null, 2, 1, 3.70);
insert into DS_OUT_DELIVERY_GABARYT values  ( '3', 'KRAJ - Gabaryt A', null, 3, 1, 6.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '4', 'KRAJ - Gabaryt B',null, 1, 1, 3.75);
insert into DS_OUT_DELIVERY_GABARYT values ( '5', 'KRAJ - Gabaryt B',null, 2, 1, 4.75);
insert into DS_OUT_DELIVERY_GABARYT values ( '6', 'KRAJ - Gabaryt B',null, 3, 1, 7.30);
--gabaryt i kwota list ekonomiczny polecony bez zpo
insert into DS_OUT_DELIVERY_GABARYT values  ( '7', 'KRAJ - Gabaryt A', null, 4, 1, 4.20);
insert into DS_OUT_DELIVERY_GABARYT values  ( '8', 'KRAJ - Gabaryt A', null, 5, 1, 5.90);
insert into DS_OUT_DELIVERY_GABARYT values  ( '9', 'KRAJ - Gabaryt A', null, 6, 1, 8.50);
insert into DS_OUT_DELIVERY_GABARYT values ( '10', 'KRAJ - Gabaryt B', null, 4, 1, 7.50);
insert into DS_OUT_DELIVERY_GABARYT values ( '11', 'KRAJ - Gabaryt B', null, 5, 1, 8.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '12', 'KRAJ - Gabaryt B', null, 6, 1, 9.50);
--gabaryt i kwota list ekonomiczny polecony z zpo
insert into DS_OUT_DELIVERY_GABARYT values  ( '13', 'KRAJ - Gabaryt A', null, 7, 1, 6.10);
insert into DS_OUT_DELIVERY_GABARYT values  ( '14', 'KRAJ - Gabaryt A', null, 8, 1, 7.80);
insert into DS_OUT_DELIVERY_GABARYT values  ( '15', 'KRAJ - Gabaryt A', null, 9, 1, 10.40);
insert into DS_OUT_DELIVERY_GABARYT values ( '16', 'KRAJ - Gabaryt B', null, 7, 1, 9.40);
insert into DS_OUT_DELIVERY_GABARYT values ( '17', 'KRAJ - Gabaryt B', null, 8, 1, 10.20);
insert into DS_OUT_DELIVERY_GABARYT values ( '18', 'KRAJ - Gabaryt B', null, 9, 1, 11.40);
--gabaryt i kwota list priorytetowy
insert into DS_OUT_DELIVERY_GABARYT values  ( '19', 'KRAJ - Gabaryt A', null, 10, 1, 2.35);
insert into DS_OUT_DELIVERY_GABARYT values  ( '20', 'KRAJ - Gabaryt A', null, 11, 1, 4.50);
insert into DS_OUT_DELIVERY_GABARYT values  ( '21', 'KRAJ - Gabaryt A', null, 12, 1, 8.80);
insert into DS_OUT_DELIVERY_GABARYT values ( '22', 'KRAJ - Gabaryt B', null, 10, 1, 5.10);
insert into DS_OUT_DELIVERY_GABARYT values ( '23', 'KRAJ - Gabaryt B', null, 11, 1, 7.10);
insert into DS_OUT_DELIVERY_GABARYT values ( '24', 'KRAJ - Gabaryt B', null, 12, 1, 10.90);
--gabaryt i kwota list priorytetowy polecony bez zpo
insert into DS_OUT_DELIVERY_GABARYT values  ( '25', 'KRAJ - Gabaryt A', null, 13, 1, 5.50);
insert into DS_OUT_DELIVERY_GABARYT values  ( '26', 'KRAJ - Gabaryt A', null, 14, 1, 7.20);
insert into DS_OUT_DELIVERY_GABARYT values  ( '27', 'KRAJ - Gabaryt A', null, 15, 1, 11.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '28', 'KRAJ - Gabaryt B', null, 13, 1, 8.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '29', 'KRAJ - Gabaryt B', null, 14, 1, 11.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '30', 'KRAJ - Gabaryt B', null, 15, 1, 14.50);
--gabaryt i kwota list priorytetowy polecony z zpo
insert into DS_OUT_DELIVERY_GABARYT values  ( '31', 'KRAJ - Gabaryt A', null, 16, 1, 7.40);
insert into DS_OUT_DELIVERY_GABARYT values  ( '32', 'KRAJ - Gabaryt A', null, 17, 1, 9.10);
insert into DS_OUT_DELIVERY_GABARYT values  ( '33', 'KRAJ - Gabaryt A', null, 18, 1, 12.90);
insert into DS_OUT_DELIVERY_GABARYT values ( '34', 'KRAJ - Gabaryt B', null, 16, 1, 10.20);
insert into DS_OUT_DELIVERY_GABARYT values ( '35', 'KRAJ - Gabaryt B', null, 17, 1, 12.90);
insert into DS_OUT_DELIVERY_GABARYT values ( '36', 'KRAJ - Gabaryt B', null, 18, 1, 16.40);

--gabaryt i kwota paczki
insert into DS_OUT_DELIVERY_GABARYT values  ( '37', 'KRAJ - Gabaryt A', null, 19, 1, 9.50);
insert into DS_OUT_DELIVERY_GABARYT values  ( '38', 'KRAJ - Gabaryt A', null, 20, 1, 11.00);
insert into DS_OUT_DELIVERY_GABARYT values  ( '39', 'KRAJ - Gabaryt A', null, 21, 1, 13.00);
insert into DS_OUT_DELIVERY_GABARYT values  ( '40', 'KRAJ - Gabaryt A', null, 22, 1, 18.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '41', 'KRAJ - Gabaryt B', null, 19, 1, 12.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '42', 'KRAJ - Gabaryt B', null, 20, 1, 14.50);
insert into DS_OUT_DELIVERY_GABARYT values ( '43', 'KRAJ - Gabaryt B', null, 21, 1, 17.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '44', 'KRAJ - Gabaryt B', null, 22, 1, 22.00);
--gabaryt i kwaota zagraniczna ekonomiczna
insert into DS_OUT_DELIVERY_GABARYT values ( '45', '�WIAT',null, 23, 1, 5.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '46', '�WIAT',null, 24, 1, 9.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '47', '�WIAT',null, 25, 1, 10.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '48', '�WIAT',null, 26, 1, 11.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '49', '�WIAT',null, 27, 1, 21.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '50', '�WIAT',null, 28, 1, 40.90);
--gabaryt i kwaota zagraniczna ekonomiczna polecona
insert into DS_OUT_DELIVERY_GABARYT values ( '51', '�WIAT', null, 29, 1, 5.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '52', '�WIAT', null, 30, 1, 9.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '53', '�WIAT', null, 31, 1, 10.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '54', '�WIAT', null, 32, 1, 11.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '55', '�WIAT', null, 33, 1, 21.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '56', '�WIAT', null, 34, 1, 40.90);
--gabaryt i kwaota zagraniczna priorytetowa
insert into DS_OUT_DELIVERY_GABARYT values ( '57', '�WIAT', null, 35, 1, 5.20);
insert into DS_OUT_DELIVERY_GABARYT values ( '58', '�WIAT', null, 36, 1, 11.50);
insert into DS_OUT_DELIVERY_GABARYT values ( '59', '�WIAT', null, 37, 1, 13.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '60', '�WIAT', null, 38, 1, 15.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '61', '�WIAT', null, 39, 1, 29.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '62', '�WIAT', null, 40, 1, 58.90);
--gabaryt i kwaota zagraniczna priorytetowa polecona
insert into DS_OUT_DELIVERY_GABARYT values ( '63', '�WIAT', null, 41, 1, 16.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '64', '�WIAT', null, 42, 1, 17.00);
insert into DS_OUT_DELIVERY_GABARYT values ( '65', '�WIAT', null, 43, 1, 18.30);
insert into DS_OUT_DELIVERY_GABARYT values ( '66', '�WIAT', null, 44, 1, 20.60);
insert into DS_OUT_DELIVERY_GABARYT values ( '67', '�WIAT', null, 45, 1, 34.60);
insert into DS_OUT_DELIVERY_GABARYT values ( '68', '�WIAT', null, 46, 1, 64.20);
--gabaryt i cena podana przez uzytkownika
insert into DS_OUT_DELIVERY_GABARYT values  ( '69', 'KRAJ - Gabaryt A', null, 47, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '70', 'KRAJ - Gabaryt A', null, 48, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '71', 'KRAJ - Gabaryt A', null, 49, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '72', 'KRAJ - Gabaryt A', null, 50, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '73', 'KRAJ - Gabaryt A', null, 51, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '74', 'KRAJ - Gabaryt A', null, 52, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values  ( '75', 'KRAJ - Gabaryt A', null, 53, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '76', 'KRAJ - Gabaryt B', null, 47, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '77', 'KRAJ - Gabaryt B', null, 48, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '78', 'KRAJ - Gabaryt B', null, 49, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '79', 'KRAJ - Gabaryt B', null, 50, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '80', 'KRAJ - Gabaryt B', null, 51, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '81', 'KRAJ - Gabaryt B', null, 52, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '82', 'KRAJ - Gabaryt B', null, 53, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '83', '�WIAT',null, 54, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '84', '�WIAT',null, 55, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '85', '�WIAT',null, 56, 1, null);
insert into DS_OUT_DELIVERY_GABARYT values ( '86', '�WIAT',null, 57, 1, null);

alter table DS_UTP_DOC_OUT add iloscPrzesylek int null;
alter table DS_UTP_DOC_OUT add WAGA_KRAJ int null;


drop table DS_OUT_DELIVERY_SERVICE

	--sposoby obs�ugi
	CREATE TABLE [dbo].[DS_OUT_DELIVERY_SERVICE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1 
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('1', 'Ostro�nie szk�o');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('2', 'Chroni� przed upadkiem');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('3', 'g�ra/d�');
insert into DS_OUT_DELIVERY_SERVICE (cn, title) values('4', 'Chroni� przed wilgoci�');

--typ
CREATE TABLE [dbo].[DS_UTP_TYPE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] int default 1 
	
) ON [PRIMARY]

insert into DS_UTP_TYPE (cn, title) values  ( '1', 'Nieuporz�dkowany zbi�r danych');
insert into DS_UTP_TYPE (cn, title) values ( '2', 'Uporz�dkowany zbi�r danych');
insert into DS_UTP_TYPE (cn, title) values ( '3', 'Obraz ruchomy');
insert into DS_UTP_TYPE (cn, title) values ( '4', 'Obiekt fizyczny');
insert into DS_UTP_TYPE (cn, title) values ( '5', 'Oprogramowanie');
insert into DS_UTP_TYPE (cn, title) values ( '6', 'D�wi�k');
insert into DS_UTP_TYPE (cn, title) values ( '7', 'Obraz nieruchomy');
insert into DS_UTP_TYPE (cn, title) values ( '8', 'Tekst');
