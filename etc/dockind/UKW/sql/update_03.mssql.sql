	
-- rodzaje pisma przychodzacego
CREATE TABLE [dbo].[dsg_rodzaj_ins_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL
) ON [PRIMARY]
	DELETE from dsg_rodzaj_ins_in
	insert into dsg_rodzaj_ins_in values ('pismo', 'Pismo', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('faktura', 'Faktura', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('notaKsiegowa', 'Nota księgowa', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('decyzjaAdministracyjna', 'Decyzja administracyjna', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('zawiadomienieOZajęciuWynagrodzenia', 'Zawiadomienie o zajęciu wynagrodzenia', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('wniosekOUdostępnienieInformacjiPublicznej', 'Wniosek o udostępnienie informacji publicznej', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('podanieOPracę', 'Podanie o pracę', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('skarga', 'Skarga', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('ofertaWTrybieZamówieniaPublicznego', 'Oferta w trybie zamówienia publicznego', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('podanieOPrzyjęcieNaStudia', 'Podanie o przyjęcie na studia', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('niezamawianeOfertyHandlowe', 'Niezamawiane oferty handlowe', null, null, 1);
	insert into dsg_rodzaj_ins_in values ('inne', 'Inne', null, null, 1);
	
-- rodzaje pisma wychodzacego	
CREATE TABLE [dbo].[dsg_rodzaj_ins_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL
) ON [PRIMARY]
    insert into dsg_rodzaj_ins_out values ('ESP/ePUAP', 'ESP/ePUAP', null, null, 1);
	insert into dsg_rodzaj_ins_out values ('fax', 'Fax', null, null, 1);
	insert into dsg_rodzaj_ins_out values ('email', 'Email', null, null, 1);

-- rodzaje pisma wewnetrznego
CREATE TABLE [dbo].[dsg_rodzaj_ins_int](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL
) ON [PRIMARY]
	insert into dsg_rodzaj_ins_int values ('pismo', 'Pismo', null, null, 1);
	insert into dsg_rodzaj_ins_int values ('notatka', 'Notatka', null, null, 1);
	insert into dsg_rodzaj_ins_int values ('prezentacja', 'Prezentacja', null, null, 1);
	
	
-- forma wysłania dokumentu 	
CREATE TABLE [utp_out_sent_form](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[CN] [varchar](255) NULL,
) ON [PRIMARY]

GO

INSERT INTO utp_out_sent_form(CN) values ('papier')
INSERT INTO utp_out_sent_form(CN) values ('e-mail')
INSERT INTO utp_out_sent_form(CN) values ('ePUAP')
