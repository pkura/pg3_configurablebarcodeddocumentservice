alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_serwis_realization
(
	DOCUMENT_ID numeric(18,0),	
	nr_zgloszenia varchar (50),
	device numeric(18,0),
	przyjmujacy numeric(18,0),
	typ numeric(18,0),
	opis_uwagi varchar (MAX),
	umowa int,
	gwarancja int,
	wyposazenie varchar (MAX),
	prze_termin_zakonczenia date,
	prze_koszt numeric (18,2),
	opis_naprawy varchar (MAX),
	pozycje_naprawy numeric(18,0),
	total_netto numeric (18,2),
	total_brutto numeric(18,2),
	status numeric(18,0)
);

CREATE TABLE DSG_POZYCJE_NAPRAWY
(
	ID IDENTITY(1,1) NOT NULL,
	towar_usluga numeric(18,0),
	ilosc numeric(18,2),
	jm numeric(18,0),
	netto numeric (18,2),
	vat numeric (18,0),
	brutto numeric (18,2),
	wartosc_brutto numeric (18,2)
);

CREATE TABLE [DSG_SERWIS_TOWAR](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer default 1
);

CREATE TABLE [DSG_SERWIS_JM](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer default 1
);

CREATE TABLE [DSG_SERWIS_VAT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer default 1
);



CREATE TABLE dsg_serwis_realization_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);