alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_serwis_device
(
	DOCUMENT_ID numeric(18,0),	
	grupa numeric(18,0),
	nazwa varchar (350),
	nr_seryjny varchar(150),
	data_produkcji date,
	data_sprzedazy date,
	okres_gwarancji numeric(18,0),
	data_ostatniego_przegladu date,
	czestotliwosc_przegladow numeric (18,0)
);

CREATE TABLE [dsg_serwis_grupa](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer default 1
);


CREATE TABLE dsg_serwis_device_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);