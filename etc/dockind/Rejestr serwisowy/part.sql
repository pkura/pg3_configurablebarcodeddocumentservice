alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_serwis_part
(
	DOCUMENT_ID numeric(18,0),	
	typ numeric(18,0),
	ilosc numeric(18,0),
	nazwa varchar (350),
	nr_seryjny varchar(150),
	data_produkcji date,
	urzadzenie numeric(18,0),
);

CREATE TABLE dsg_serwis_part_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);