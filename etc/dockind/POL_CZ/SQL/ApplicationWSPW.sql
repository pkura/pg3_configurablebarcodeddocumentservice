alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_pcz_application_wspw
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	nr_wniosek varchar (50),
	data_wypelnienia date,
	konto_zrodlowe varchar (100),
	konto_docelowe varchar (100),
	uzasadnienie varchar (2048)
);

CREATE TABLE dsg_pcz_application_wspw_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE dsg_pcz_application_wspw_students
(
	id numeric (18,0),
	nazwisko varchar (150),
	imie varchar (80),
	indeks numeric (18,0),
	rok numeric (4,0),
	semestr varchar(10),
	grupa varchar (10)
);
