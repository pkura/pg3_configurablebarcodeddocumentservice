CREATE TABLE dsg_pcz_application_wpz
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	wnioskodawca_stanowisko varchar(50),
	wnioskodawca_info varchar(500),
	uzasadnienie varchar(1000),
	typ_zapomogi numeric(18,0)
);