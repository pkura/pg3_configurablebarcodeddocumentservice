create table dsg_pcz_trip_diet(
	id numeric(18,0) identity(1,1) NOT NULL,
	currency numeric(18,0),
	currency_value float
);