alter table dsg_pcz_application_wps drop column wypoczynek_indywidualny;
alter table dsg_pcz_application_wps drop column pobyt_na_wczasach;
alter table dsg_pcz_application_wps drop column pobyt_dziecka;

alter table dsg_pcz_application_wps add typ_wniosku numeric(18,0) NULL;