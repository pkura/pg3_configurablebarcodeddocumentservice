DROP TABLE [dbo].[DS_ABSENCE_TYPE];

CREATE TABLE [dbo].[DS_ABSENCE_TYPE](
	[CODE] [varchar](20) NOT NULL,
	[NAME] [varchar](150) NOT NULL,
	[INFO] [text] NULL,
	[FLAG] [smallint] NOT NULL,
	[ID] [int] PRIMARY KEY IDENTITY );



INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('W', 'Urlop wypoczynkowy', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('V', 'Urlop wyp. na żądanie', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('O', 'Urlop okolicznościowy', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('A', 'Urlop dodatkowy', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('v', 'Urlop szkoleniowy', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('T', 'Opieka nad dzieckiem art.188Kp', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('G', 'Poszukiwanie pracy art.37Kp', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('D', 'Inne nieobecności uspr. płatne', NULL, 0);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('z', 'Urlop zdrowotny', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('y', 'Urlop naukowy', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('t', 'Urlop habilitacyjny', NULL, 1);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUN', 'Wymiar urlopu należnego', NULL, 2);
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUZ', 'Wymiar urlopu zaległego', NULL, 2);