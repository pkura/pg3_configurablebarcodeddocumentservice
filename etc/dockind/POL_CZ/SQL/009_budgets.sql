alter table ds_division alter column guid varchar (100);
alter table ds_division alter column name varchar (150);

alter table dsg_centrum_kosztow_faktury add descriptionAmount varchar(600);
alter table DSG_CENTRUM_KOSZTOW_FAKTURY add analytics_id int;

alter table dsg_pcz_application_wrs  add bd_rezerwacja_id int;
alter table dsg_pcz_application_ww  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wgz  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wkp  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wde  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wucp  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wzp  add bd_rezerwacja_id int;
alter table dsg_pcz_application_wze  add bd_rezerwacja_id int;