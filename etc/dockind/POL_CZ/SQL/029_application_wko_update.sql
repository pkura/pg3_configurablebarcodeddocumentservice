alter table dsg_pcz_application_wko add nr_konta_swift varchar(50);
alter table dsg_pcz_application_wko add publikacja_kwota numeric(18,2);
alter table dsg_pcz_application_wko add uczestnictwo_kwota numeric(18,2);
alter table dsg_pcz_application_wko drop column adres_beneficjenta;
alter table dsg_pcz_application_wko drop column nazwa_beneficjenta;