alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_pcz_application_wmt
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	nr_wniosek varchar (50),
	rok_budzetowy numeric(18,0),
	data_wypelnienia date,
	rodzaj numeric(18,0),
	temat varchar (350),
	netto numeric (18,2),
	brutto numeric (18,2),
	termin_realizacji date,
	centra_kosztow_dnb numeric(18,0),
	centra_kosztow_dd numeric(18,0),
	koordynator numeric(18,0),
	dzp_status numeric(18,0)
);

CREATE TABLE dsg_pcz_application_wmt_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

