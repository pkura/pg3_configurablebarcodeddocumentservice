CREATE TABLE [dbo].[dsg_pcz_application_wrs]
(
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[RODZAJ_WNIOSKU] [numeric](18, 0) NULL,
	[STATUS] [numeric](18, 0) NULL,
	[WNIOSKODAWCA] [numeric](18, 0) NULL,
	[WORKER_DIVISION] [numeric](18, 0) NULL,
	[nr_wniosek] [varchar](50) NULL,
	[rok_budzetowy] [numeric](18, 0) NULL,
	[data_wypelnienia] [date] NULL,
	[RODZAJ_ZAMOWIENIA] [numeric](18, 0) NULL,
	[temat] [varchar](350) NULL,
	[kwota] [numeric](18, 2) NULL,
	[netto] [numeric](18, 2) NULL,
	[brutto] [numeric](18, 2) NULL,
	[termin_realizacji] [date] NULL,
	[centra_kosztow_dnb] [numeric](18, 0) NULL,
	[centra_kosztow_dd] [numeric](18, 0) NULL,
	[koordynator] [numeric](18, 0) NULL
);

CREATE TABLE [dbo].[dsg_pcz_application_wrs_multiple_value]
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);