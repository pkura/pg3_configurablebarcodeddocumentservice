alter table dsg_pcz_application_wzp add simple_imported smallint default  0;
alter table dsg_pcz_application_wze add simple_imported smallint default  0;
alter table dsg_pcz_application_wmu add simple_imported smallint default  0;
alter table dsg_pcz_application_wko add simple_imported smallint default  0;
alter table dsg_pcz_application_wko add bd_rezerwacja_id numeric(18,0);
update dsg_pcz_application_wzp set simple_imported = 0;
update dsg_pcz_application_wze set simple_imported = 0;
update dsg_pcz_application_wko set simple_imported = 0;
update dsg_pcz_application_wmu set simple_imported = 0;

CREATE view [dbo].[rezerwacje_view]
as
select bd_rezerwacja_id, simple_imported from dsg_pcz_application_wzp
union
select bd_rezerwacja_id, simple_imported from dsg_pcz_application_wze
union
select bd_rezerwacja_id, simple_imported from dsg_pcz_application_wmu
union
select bd_rezerwacja_id, simple_imported from dsg_pcz_application_wko;