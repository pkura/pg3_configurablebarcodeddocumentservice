CREATE TABLE [dbo].[dsg_pcz_application_wmu](
	[status] [numeric](18, 0) NULL,
	[rozeznanie_attach] [numeric](18, 0) NULL,
	[wnioskodawca] [numeric](18, 0) NULL,
	[worker_division] [numeric](18, 0) NULL,
	[nr_wniosek] [varchar](50) NULL,
	[rok_budzetowy] [numeric](18, 0) NULL,
	[data_wypelnienia] [date] NULL,
	[rodzaj_zamowienia] [numeric](18, 0) NULL,
	[temat] [varchar](2048) NULL,
	[add_info] [varchar](2048) NULL,
	[contractor] [numeric](18, 0) NULL,
	[miejsce] [varchar](2048) NULL,
	[gwarancja] [varchar](2048) NULL,
	[kwota_brutto] [numeric](18, 2) NULL,
	[typ_terminu] [numeric](18, 0) NULL,
	[termin_od_podpisania] [numeric](18, 0) NULL,
	[termin_od_do] [numeric](18, 0) NULL,
	[termin_data_zakonczenia] [numeric](18, 0) NULL,
	[termin_inny] [varchar](2048) NULL,
	[koordynator] [numeric](18, 0) NULL,
	[wybrany_dzial] [numeric](18, 0) NULL,
	[DOCUMENT_ID] [numeric](18, 0) PRIMARY KEY);

CREATE INDEX pcz_wmu_index ON dsg_pcz_application_wmu (DOCUMENT_ID);

CREATE TABLE [dbo].[dsg_pcz_application_wmu_multiple_value](
	[ID] [numeric](18, 0) PRIMARY KEY IDENTITY,
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[FIELD_CN] [varchar](100) NULL,
	[FIELD_VAL] [varchar](100) NULL
);
