alter table dsg_pcz_application_wucp add grupa_umowa numeric (18,0);
alter table dsg_pcz_application_wucp add rodzaj_umowa numeric (18,0);
alter table dsg_pcz_application_wucp add umowa_dydaktyczna int default 0 not null;
alter table dsg_pcz_application_wucp add nr_projektu numeric (18,0);