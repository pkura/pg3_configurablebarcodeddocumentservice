
CREATE TABLE [dsg_nr_projektu_pcz](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] bit default 1 not null
);

alter table DSG_NORMAL_DOCKIND add docType integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add send_kind integer;
alter table DSG_NORMAL_DOCKIND add signature varchar(150);

CREATE TABLE [dsg_rodzaj_pcz_send_kind](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	
);

CREATE TABLE [dsg_rodzaj_pcz_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	
);

CREATE TABLE [dsg_rodzaj_pcz_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

CREATE TABLE [dsg_rodzaj_pcz_INT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

insert into dsg_rodzaj_pcz_in(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_pcz_in(cn,title,available)values('ZAMOWIENIA','Zamówienia usług',1);
insert into dsg_rodzaj_pcz_in(cn,title,available)values('ODPOWIEDZ','Odpowiedzi na zapytania ofertowe',1);
insert into dsg_rodzaj_pcz_in(cn,title,available)values('Z_JEDNOSTEK','Pisma z jednostek nadrzędnych (PAN, MNiSW)',1);
insert into dsg_rodzaj_pcz_in(cn,title,available)values('TAJNE','Pisma tajne',1);
insert into dsg_rodzaj_pcz_in(cn,title,available)values('INNE','Pozostałe pisma',1);

insert into dsg_rodzaj_pcz_out(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_pcz_out(cn,title,available)values('DO_JEDNOSTEK','Pisma do jednostek nadrzędnych (PAN, MNiSW',1);
insert into dsg_rodzaj_pcz_out(cn,title,available)values('DO_PARTNEROW','Pisma do Partnerów  projektów',1);
insert into dsg_rodzaj_pcz_out(cn,title,available)values('MONITY','Pisma, monity  do kontrahentów',1);
insert into dsg_rodzaj_pcz_out(cn,title,available)values('INNE','Pozostała  korespondencja ',1);

insert into dsg_rodzaj_pcz_int(cn,title,available)values('NOTATKA','Notatka służbowa',1);
insert into dsg_rodzaj_pcz_int(cn,title,available)values('PODANIA','Podania',1);

