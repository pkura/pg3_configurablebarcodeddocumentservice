
CREATE TABLE dsg_pcz_wni_urlop
(
	rodzaj_url numeric(18,0),
    DOCUMENT_ID numeric(18,0),
    nr_wniosku varchar(60),
    status numeric(18,0),
	wnioskodawca numeric(18,0),
    OSOBA_ZASTEPUJ numeric(18,0),
    URLOPRODZAJ numeric(18,0),
    tytul varchar(250),
    ctime datetime,
    stanowisko varchar(150),
    data_od datetime,
    data_do datetime,
    ILOSC_DNI numeric(18,0),  
    worker_division numeric(18,0)
    POZOSTALO_DNI numeric(18,0)

);

CREATE TABLE [dsg_pcz_nazwa_jedn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);

CREATE TABLE [dsg_pcz_osoba_zast](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);


alter table ds_employee_card add FIRST_NAME varchar(65);
alter table ds_employee_card add LAST_NAME varchar(65);
alter table ds_absence add DOCUMENT_ID numeric(19,0);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUN', 'Wymiar urlopu należny', NULL, 2);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUZ', 'Wymiar urlopu zaległy', NULL, 2);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UW', 'Urlop wypoczynkowy', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UŻ', 'Urlop na żądanie', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('188', 'Opieka nad dzieckiem', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UOK', 'Urlop okolicznościowy', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UB', 'Urlop bezpłatny', NULL, 1);     
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UI', 'Inny urlop', NULL, 0);

-- DODATKOWE URLOPY
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UJ', 'Urlop ojcowski', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UM', 'Urlop macierzyński', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UY', 'Urlop wychowawczy', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NP', 'Nieobecność pracownika', NULL, 1);

DECLARE @userID numeric(19,0)
DECLARE @employeeCardID numeric (18,0)
DECLARE @firstName varchar (65)
DECLARE @lastName varchar (65)
DECLARE @username varchar (65)
DECLARE ids cursor for (select ID from DS_USER)
OPEN ids 
WHILE @@FETCH_STATUS = 0
	BEGIN
		FETCH NEXT FROM ids INTO @userID
		
		SELECT @firstName = FIRSTNAME FROM DS_USER WHERE id = @userID
		SELECT @lastName = LASTNAME FROM DS_USER WHERE id = @userID
		SELECT @username = NAME FROM DS_USER WHERE id = @userID
		
		INSERT INTO DS_EMPLOYEE_CARD (KPX, COMPANY, DEPARTMENT, EMPLOYMENT_START_DATE, POSITION, [USER_ID])
			VALUES ('BD', 'PCZ', 'G��wny', CURRENT_TIMESTAMP, 'BD', @username)
		set @employeeCardID = @@IDENTITY
		
		INSERT INTO DS_ABSENCE ([YEAR], START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (2011, '01-01-2011', '12-31-2011', 26, CURRENT_TIMESTAMP, 'Urlop nale�ny', @employeeCardID, 'WUN') 
			
		INSERT INTO DS_ABSENCE ([YEAR], START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (2011, '01-01-2011', '12-31-2011', 0, CURRENT_TIMESTAMP, 'Urlop zaleg�y', @employeeCardID, 'WUZ')
			
	    UPDATE DS_EMPLOYEE_CARD SET FIRST_NAME = @firstName, LAST_NAME = @lastName where user_id = @username

	END
CLOSE ids
DEALLOCATE ids
GO


-- urlop inwalidzki
alter table DS_EMPLOYEE_CARD add IS_TINW tinyint;
alter table DS_EMPLOYEE_CARD add INV_ABS_DAYS_NUM numeric(18,0);


-- liczby dni urlopow T188 Kp i UJ
alter table DS_EMPLOYEE_CARD add T188KP_DAYS_NUM numeric(18,0);
alter table DS_EMPLOYEE_CARD add TUJ_DAYS_NUM numeric(18,0);

alter table ds_absence add EDITOR_NAME varchar(75);

CREATE TABLE dsg_absence_request
 (
	document_id numeric(19, 0) NOT NULL PRIMARY KEY ,
	dsuser numeric(19, 0) NULL,
	period_from datetime NULL,
	period_to datetime NULL,
	description varchar(400) NULL,
	position varchar(50) NULL,
	status integer NULL,
	kind varchar(150) NULL,
	working_days integer NULL,
	available_days integer NULL,
	substitution numeric(19, 0) NULL,
	author varchar(65),
	external_id numeric(19,0)
);
