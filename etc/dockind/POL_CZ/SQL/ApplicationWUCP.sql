CREATE TABLE [dbo].[dsg_pcz_application_wucp](
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[status] [numeric](18, 0) NULL,
	[wnioskodawca] [numeric](18, 0) NULL,
	[worker_division] [numeric](18, 0) NULL,
	[nr_wniosek] [varchar](50) NULL,
	[rok_budzetowy] [numeric](18, 0) NULL,
	[data_wypelnienia] [date] NULL,
	[przedmiot] [varchar](251) NULL,
	[kwota_umowy] [numeric](18, 2) NULL,
	[umowa_start_date] [date] NULL,
	[umowa_finish_date] [date] NULL,
	[grupa_umowa] [numeric](18, 0) NULL,
	[rodzaj_umowa] [numeric](18, 0) NULL,
	[nr_projektu] [numeric](18, 0) NULL,
	[obcy_nazwisko] [varchar](50) NULL,
	[wykonawca_obcy] [bit] NULL,
	[obcy_imie] [varchar](50) NULL,
	[obcy_adres] [varchar](50) NULL,
	[obcy_nip] [varchar](6) NULL,
	[obcy_pesel] [varchar](50) NULL,
	[recenzja] [bit] NULL,
	[bd_rezerwacja_id] [numeric](18, 0) PRIMARY KEY
);

CREATE INDEX pcz_wucp_index ON dsg_pcz_application_wucp (DOCUMENT_ID);

CREATE TABLE [dbo].[dsg_pcz_application_wucp_multiple_value](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);