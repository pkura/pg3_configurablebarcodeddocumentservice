CREATE TABLE dsg_pcz_contract
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	contract_nr varchar (50),
	zleceniobiorca varchar (150),
	przedmiot varchar (150),
	wartosc numeric(18,2),
	start_date date,
	finish_date date,
	srodki_badawcze int,
	centra_kosztow numeric(18,0),
	contractor numeric(18,0)
);

CREATE TABLE dsg_pcz_contract_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);