CREATE TABLE dsg_pcz_costinvoice
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	nr_faktury varchar (30),
	data_wystawienia date,
	termin_platnosci date,
	netto numeric (18,2),
	vat numeric (18,2),
	brutto numeric (18,2),
	sposob numeric (18,0),
	dzialalnosc numeric (18,0),
	opis varchar (4096),
	addinfo varchar (4096)
);

CREATE TABLE dsg_pcz_costinvoice_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

drop view DSG_CENTRUM_KOSZTOW_view;
create view DSG_CENTRUM_KOSZTOW_view
as
select id as ID, symbol as CN, name as title, 0 as centrum, '0' as refValue, 1 as available
from DSG_CENTRUM_KOSZTOW;

----- blokady!!!
--docuemnt id wniosku, nr wniosku, temat zamowienia, kwota blokady
alter view cost_invoice_app_blocades_view
as
-- wniosek wzp
select cf.id as id, m.DOCUMENT_ID, w.nr_wniosek, w.temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wzp_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wzp w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061 and w.status=185
union
-- wniosek wze
select cf.id as id,m.DOCUMENT_ID, w.nr_wniosek, w.temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wze_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wze w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061
union
-- wniosek wmu
select cf.id as id,m.DOCUMENT_ID, w.nr_wniosek, w.temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wmu_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wmu w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061
union
-- wniosek wko
select cf.id as id,m.DOCUMENT_ID, w.nr_wniosek, ISNULL(w.opis_uslugi,'Konferencja/Publikacja') as temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wko_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wko w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061
union
-- wniosek wgz
select cf.id as id,m.DOCUMENT_ID, w.nr_wniosek, w.temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wgz_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wgz w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061
union
-- wniosek wucp
select cf.id as id,m.DOCUMENT_ID, w.nr_wniosek, ISNULL(w.przedmiot,'Umowa cywilnoprawna') as temat, cf.amount, cf.analytics_id  from dsg_pcz_application_wucp_multiple_value m 
join DSG_CENTRUM_KOSZTOW_FAKTURY cf on m.FIELD_VAL = cf.id 
join dsg_pcz_application_wucp w on m.DOCUMENT_ID = w.DOCUMENT_ID where m.FIELD_CN = 'MPK' and cf.analytics_id is not null and cf.lokalizacja = 16061

