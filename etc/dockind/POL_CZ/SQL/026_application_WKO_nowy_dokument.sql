drop table dsg_pcz_application_wko;
drop table dsg_pcz_application_wko_multiple_value;

CREATE TABLE dsg_pcz_application_wko (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[wnioskodawca] [numeric](18, 0) NULL,
	[worker_division] [numeric](18, 0) NULL,
	[nr_wniosek] [varchar](80) NULL,
	[data_wypelnienia] [datetime] NULL,
	[rok_budzetowy] [numeric](18, 0) NULL,
	[typ] [numeric](18, 0) NULL,
	[typ_zagraniczna] [numeric](18, 0) NULL,
	[uczestnictwo] [bit] NULL,
	[publikacja] [bit] NULL,
	[nazwa_konferencji] [varchar](250) NULL,
	[rozpoczecie_konferencji] [datetime] NULL,
	[zakonczenie_konferencji] [datetime] NULL,
	[opis_uslugi] [varchar](250) NULL,
	[nazwa_organizatora] [varchar](250) NULL,
	[podatnik_vat] [numeric](18, 0) NULL,
	[nip_ue] [varchar](80) NULL,
	[stawka_vat] [numeric](18, 0) NULL,
	[uczestnictwo_waluta] [numeric](18, 0) NULL,
	[uczestnictwo_kurs] [numeric](18, 4) NULL,
	[uczestnictwo_kwota_pln] [numeric](18, 2) NULL,
	[publikacja_waluta] [numeric](18, 0) NULL,
	[publikacja_kurs] [numeric](18, 4) NULL,
	[publikacja_kwota_pln] [numeric](18, 2) NULL,
	[kwota_pln] [numeric](18, 2) NULL,
	[przelac_z_konta] [varchar](50) NULL,
	[przelac_na_konto] [varchar](50) NULL,
	[nazwa_beneficjenta] [varchar](50) NULL,
	[adres_beneficjenta] [varchar](251) NULL,
	[nazwa_banku] [varchar](50) NULL,
	[termin_platnosci] [datetime] NULL,
	[dodatkowe_informacje] [varchar](1000) NULL,
	[wybrany_dzial] [numeric](18, 0) NULL
)

CREATE TABLE dsg_pcz_application_wko_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);