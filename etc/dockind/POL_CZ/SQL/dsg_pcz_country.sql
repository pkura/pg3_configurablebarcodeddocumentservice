CREATE TABLE dsg_pcz_country(
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[country] [numeric](18, 0) NULL,
	[city] [varchar](50) NULL,
	[organization] [varchar](50) NULL,
	[destination] [numeric](18, 0) NULL
);