CREATE TABLE [dbo].[dsg_pcz_cpv](
	[ID] [numeric](18, 0) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NOT NULL
);