
create view [dbo].[simple_erp_per_docusafe_bd_pozycje_view] AS
SELECT 
distinct pozycje.bd_budzet_poz_id as ID,
pozycje.bd_budzet_poz_id as cn,
budzety.bd_budzet_id as refValue,
pozycje.nazwa as title,
'0' AS CENTRUM,
'1' as available
FROM         simpleERP.dbo.bd_budzet LEFT OUTER JOIN
                          (SELECT     1 AS rodzaj, bd_budzet_ko_id AS bd_budzet_id, budzet_id, bd_typ_budzet_ko_id AS bd_typ_budzet_id, bd_budzet_ko_idm AS bd_budzet_idm, nazwa, 
                                                   uklad, bd_str_budzet_id
                            FROM          simpleERP.dbo.bd_budzet_ko
                            UNION
                            SELECT     2 AS rodzaj, bd_budzet_pr_id AS bd_budzet_id, budzet_id, bd_typ_budzet_pr_id AS bd_typ_budzet_id, bd_budzet_pr_idm AS bd_budzet_idm, nazwa, 
                                                  uklad, bd_str_budzet_id
                            FROM         simpleERP.dbo.bd_budzet_pr
                            UNION
                            SELECT     3 AS rodzaj, bd_prf_id AS bd_budzet_id, budzet_id, bd_typ_prf_id AS bd_typ_budzet_id, bd_prf_idm AS bd_budzet_idm, nazwa, uklad, 
                                                  bd_str_budzet_id
                            FROM         simpleERP.dbo.bd_prf) AS budzety ON budzety.budzet_id = simpleERP.dbo.bd_budzet.budzet_id LEFT OUTER JOIN
                      simpleERP.dbo.bd_str_budzet ON simpleERP.dbo.bd_str_budzet.bd_str_budzet_id = budzety.bd_str_budzet_id LEFT OUTER JOIN
                          (SELECT     1 AS rodzaj, bd_budzet_ko_id AS bd_budzet_id, bd_rodzaj_ko_id AS bd_budzet_poz_id, nrpoz, nazwa, ISNULL(p_koszt, 0) AS p_koszt, ISNULL
                                                       ((SELECT     SUM(rp.koszt) AS EXPR1
                                                           FROM         simpleERP.dbo.bd_rezerwacja_poz AS rp INNER JOIN
                                                                                 simpleERP.dbo.bd_rezerwacja AS rn ON rp.bd_rezerwacja_id = rn.bd_rezerwacja_id
                                                           WHERE     (rp.bd_rodzaj_ko_id = simpleERP.dbo.bd_budzet_ko_rodzaj.bd_rodzaj_ko_id) AND (rn.status = 3)), 0) - ISNULL
                                                       ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                           FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                           WHERE     (bd_rodzaj_ko_id = simpleERP.dbo.bd_budzet_ko_rodzaj.bd_rodzaj_ko_id)), 0) AS rez_koszt, ISNULL
                                                       ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                           FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                           WHERE     (bd_rodzaj_ko_id = simpleERP.dbo.bd_budzet_ko_rodzaj.bd_rodzaj_ko_id)), 0) AS r_koszt, ISNULL(p_koszt, 0) - ISNULL
                                                       ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                           FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                           WHERE     (bd_rodzaj_ko_id = simpleERP.dbo.bd_budzet_ko_rodzaj.bd_rodzaj_ko_id)), 0) AS do_wyk_koszt
                            FROM          simpleERP.dbo.bd_budzet_ko_rodzaj
                            UNION
                            SELECT     2 AS rodzaj, bd_budzet_pr_id AS bd_budzet_id, bd_rodzaj_pr_id AS bd_budzet_poz_id, nrpoz, nazwa, ISNULL(p_koszt, 0) AS p_koszt, 0 AS rez_koszt, 
                                                  ISNULL(r_koszt, 0) AS r_koszt, ISNULL(p_koszt, 0) - ISNULL(r_koszt, 0) AS do_wyk_koszt
                            FROM         simpleERP.dbo.bd_budzet_pr_rodzaj
                            UNION
                            SELECT     3 AS rodzaj, bd_prf_id AS bd_budzet_id, bd_rodzaj_prf_id AS bd_budzet_poz_id, nrpoz, nazwa, ISNULL(p_koszt, 0) AS p_koszt, 0 AS rez_koszt, 
                                                  ISNULL
                                                      ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                          FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                          WHERE     (bd_rodzaj_ko_id IN
                                                                                    (SELECT     bd_rodzaj_ko_id
                                                                                      FROM          simpleERP.dbo.bd_prf_agregat
                                                                                      WHERE      (bd_rodzaj_prf_id = simpleERP.dbo.bd_prf_rodzaj.bd_rodzaj_prf_id) AND (operacja = 1)))), 0) - ISNULL
                                                      ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                          FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                          WHERE     (bd_rodzaj_ko_id IN
                                                                                    (SELECT     bd_rodzaj_ko_id
                                                                                      FROM          simpleERP.dbo.bd_prf_agregat AS bd_prf_agregat_3
                                                                                      WHERE      (bd_rodzaj_prf_id = simpleERP.dbo.bd_prf_rodzaj.bd_rodzaj_prf_id) AND (operacja = 2)))), 0) AS r_koszt, ISNULL(p_koszt, 0) 
                                                  - ISNULL(ISNULL
                                                      ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                          FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                          WHERE     (bd_rodzaj_ko_id IN
                                                                                    (SELECT     bd_rodzaj_ko_id
                                                                                      FROM          simpleERP.dbo.bd_prf_agregat AS bd_prf_agregat_2
                                                                                      WHERE      (bd_rodzaj_prf_id = simpleERP.dbo.bd_prf_rodzaj.bd_rodzaj_prf_id) AND (operacja = 1)))), 0) - ISNULL
                                                      ((SELECT     SUM(dok_poz_koszt) AS EXPR1
                                                          FROM         simpleERP.dbo.bd_dok_koszt AS rp
                                                          WHERE     (bd_rodzaj_ko_id IN
                                                                                    (SELECT     bd_rodzaj_ko_id
                                                                                      FROM          simpleERP.dbo.bd_prf_agregat AS bd_prf_agregat_1
                                                                                      WHERE      (bd_rodzaj_prf_id = simpleERP.dbo.bd_prf_rodzaj.bd_rodzaj_prf_id) AND (operacja = 2)))), 0), 0) AS do_wyk_koszt
                            FROM         simpleERP.dbo.bd_prf_rodzaj) AS pozycje ON pozycje.bd_budzet_id = budzety.bd_budzet_id AND pozycje.rodzaj = budzety.rodzaj LEFT OUTER JOIN
                          (SELECT     1 AS rodzaj, simpleERP.dbo.mtf_zrodlo.zrodlo_id, simpleERP.dbo.mtf_zrodlo.zrodlo_idn, simpleERP.dbo.mtf_zrodlo.zrodlo_nazwa, 
                                                   simpleERP.dbo.bd_budzet_ko_rodzaj_zrodla.bd_rodzaj_ko_id AS bd_budzet_poz_id, ISNULL(simpleERP.dbo.bd_budzet_ko_rodzaj_zrodla.p_koszt, 0) AS p_koszt, 
                                                   0 AS rez_koszt, 0 AS r_koszt
                            FROM          simpleERP.dbo.bd_budzet_ko_rodzaj_zrodla LEFT OUTER JOIN
                                                   simpleERP.dbo.mtf_zrodlo ON simpleERP.dbo.mtf_zrodlo.zrodlo_id = simpleERP.dbo.bd_budzet_ko_rodzaj_zrodla.zrodlo_id) AS zrodla ON 
                      zrodla.bd_budzet_poz_id = pozycje.bd_budzet_poz_id AND zrodla.rodzaj = pozycje.rodzaj
where zrodlo_id is not null;