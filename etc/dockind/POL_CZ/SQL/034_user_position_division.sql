ALTER VIEW [user_position_division] AS
SELECT us.LastName as LASTNAME, us.FirstName as FIRSTNAME, us.NAME as USERNAME, us.ID as USER_ID, div.GUID as DIVISION_GUID, div.NAME as DIVISION_NAME, us.MOBILE_PHONE_NUM as MOBILE_PHONE_NUM, ecard.position as POSITION, convert(numeric, (replicate('0', 7-datalength(CAST(us.ID AS varchar))) + CAST(us.ID AS varchar)) + (replicate('0', 7-datalength(CAST(div.ID AS varchar))) + CAST(div.ID AS varchar))) as id
FROM ds_user us
JOIN ds_employee_card ecard
ON us.name=ecard.user_id

JOIN ds_user_to_division usdiv
ON us.id=usdiv.user_id

JOIN DS_DIVISION div
ON div.id=usdiv.division_id and div.DIVISIONTYPE = (
													CASE 
														WHEN (SELECT count(1) from DS_USER_TO_DIVISION usdev join ds_division div on usdev.division_id=div.id and div.divisiontype='division' where usdev.USER_ID=us.ID) = 0 THEN 'position'
														ELSE 'division'
													END
													);