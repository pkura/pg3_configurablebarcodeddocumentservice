ALTER TABLE dsg_pcz_application_wucp add w_o_nazwisko varchar(50) null;
ALTER TABLE dsg_pcz_application_wucp add wykonawca_obcy bit;
ALTER TABLE dsg_pcz_application_wucp add w_o_imie varchar(50) null;
ALTER TABLE dsg_pcz_application_wucp add w_o_ulica varchar(50) null;
ALTER TABLE dsg_pcz_application_wucp add w_o_kod varchar(6) null;
ALTER TABLE dsg_pcz_application_wucp add w_o_miasto varchar(50) null;
ALTER TABLE dsg_pcz_application_wucp add w_o_pesel numeric(11, 0) null;
ALTER TABLE dsg_pcz_application_wucp add w_o_nip numeric(10, 0) null;
ALTER TABLE dsg_pcz_application_wucp add recenzja bit;