CREATE TABLE dsg_pcz_libraryinvoice
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	nr_faktury varchar (30),
	data_wystawienia date,
	termin_platnosci date,
	netto numeric (18,2),
	vat numeric (18,2),
	brutto numeric (18,2),
	sposob numeric (18,0),
	opis varchar (4096),
	addinfo varchar (4096),
	blokady numeric (18,0)
);

CREATE TABLE dsg_pcz_libraryinvoice_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
