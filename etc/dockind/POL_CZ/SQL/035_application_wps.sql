alter table dsg_pcz_application_wps
add nr_wniosek varchar(80);

alter table dsg_pcz_application_wps
add rok_budzetowy numeric(18,0);

alter table dsg_pcz_application_wps
add data_wypelnienia datetime;

alter table dsg_pcz_application_wps
add urlop_od datetime;

alter table dsg_pcz_application_wps
add urlop_do datetime;

alter table dsg_pcz_application_wps
add oswiadczenie numeric(18,0);

alter table dsg_pcz_application_wps
add dochod_roczny numeric(18,2);

alter table dsg_pcz_application_wps
drop column karta_urlopowa;

alter table dsg_pcz_application_wps
drop column karta_urlopowa_okres;

alter table dsg_pcz_application_wps
drop column karta_urlopowa_okres_attach;

alter table dsg_pcz_application_wps
drop column pobyt_dziecka_attach;

alter table dsg_pcz_application_wps
drop column pobyt_na_wczasach_attach;

alter table dsg_pcz_application_wps
drop column dzieci;

CREATE TABLE dsg_pcz_application_wps_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE dsg_pcz_osoby_wps (
	ID numeric(18,0) identity(1,1) NOT NULL,
	imie varchar(50),
	nazwisko varchar(50),
	data_urodzenia datetime,
	czy_uczacy numeric(18,0),
	nr_dokumentu varchar(50),
	nazwa_szkoly varchar(50)
);