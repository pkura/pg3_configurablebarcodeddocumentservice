alter table dsg_pcz_application_wde add WNIOSEK_WYJAZDOWY numeric (18,0);
alter table dsg_pcz_application_wde add WNIOSEK_ZALICZKA numeric (18,0);
alter table dsg_pcz_application_wde add KOSZTY_PODROZY numeric (18,0);
alter table dsg_pcz_application_wde add RYCZALT numeric (18,2);
alter table dsg_pcz_application_wde add SUMA_KOSZTOW_PODROZY numeric (18,2);
alter table dsg_pcz_application_wde add DIETY numeric (18,2);
alter table dsg_pcz_application_wde add NOCLEGI numeric (18,2);
alter table dsg_pcz_application_wde add NOCLEGI_RYCZALT numeric (18,2);
alter table dsg_pcz_application_wde add INNE_WYDATKI numeric (18,2);
alter table dsg_pcz_application_wde add SUMA numeric (18,2);
alter table dsg_pcz_application_wde add DO_WYPLATY numeric(18,2);
alter table dsg_pcz_application_wde add FORMA_ZWROTU numeric(18,0);
alter table dsg_pcz_application_wde add SRODEK_LOKOMOCJI varchar (100);
alter table dsg_pcz_application_wde add CEL_WYJAZDU varchar (300);
alter table dsg_pcz_application_wde add MIEJSCE_WYJAZDU varchar (150);

CREATE TABLE DSG_KOSZTY_PODROZY
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	FROM_PLACE varchar(50),
	FROM_DATE date,
	TO_PLACE varchar (50),
	TO_DATE date,
	TRANSPORT numeric (18,0),
	KOSZT numeric (18,2)
);