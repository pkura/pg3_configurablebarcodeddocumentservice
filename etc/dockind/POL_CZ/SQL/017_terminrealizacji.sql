create table dsg_pcz_termin_realizacji
(
ID numeric (18,0) identity (1,1) not null,
ile int,
jm numeric (18,0)
)


alter table dsg_pcz_termin_realizacji add od date;
alter table dsg_pcz_termin_realizacji add do date;
alter table dsg_pcz_termin_realizacji add INNY varchar(350);

alter table dsg_pcz_application_wzp add typ_terminu numeric (18,0);
alter table dsg_pcz_application_wzp add termin_od_podpisania numeric (18,0);
alter table dsg_pcz_application_wzp add termin_data_zakonczenia numeric (18,0);
alter table dsg_pcz_application_wzp add termin_od_do numeric (18,0);
alter table dsg_pcz_application_wzp add stawka_vat int;
alter table dsg_pcz_application_wzp add termin_inny numeric(18,0);

alter table dsg_pcz_application_wze add typ_terminu numeric (18,0);
alter table dsg_pcz_application_wze add termin_od_podpisania numeric (18,0);
alter table dsg_pcz_application_wze add termin_data_zakonczenia numeric (18,0);
alter table dsg_pcz_application_wze add termin_od_do numeric (18,0);
alter table dsg_pcz_application_wze add stawka_vat int;
alter table dsg_pcz_application_wze add termin_inny numeric(18,0);

alter table dsg_pcz_application_ww add waluta numeric (18,0);
alter table dsg_pcz_application_ww add kwota_w_walucie numeric (18,2);
alter table dsg_pcz_application_ww add kwota_pln numeric (18,2);
alter table dsg_pcz_application_ww add rate numeric (18,4);

alter table dsg_pcz_application_wze add rodzaj_proc_wew numeric (18,0);

alter table dsg_pcz_application_wgz add termin_od_do numeric (18,0);