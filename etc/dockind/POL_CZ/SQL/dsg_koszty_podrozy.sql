CREATE TABLE DSG_KOSZTY_PODROZY
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	FROM_PLACE varchar(50),
	FROM_DATE datetime,
	TO_PLACE varchar (50),
	TO_DATE datetime,
	TRANSPORT numeric (18,0),
	OPIS varchar (249),
	KOSZT numeric (18,2)
);

CREATE TABLE DSG_SRODKI_LOKOMOCJI
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);