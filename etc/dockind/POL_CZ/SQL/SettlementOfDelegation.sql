CREATE TABLE dsg_pcz_delegation
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	nr_wniosek varchar (50),
	rok_budzetowy numeric(18,0),
	data_wypelnienia date,
	delegacja numeric (18,0),
	centra_kosztow_dnb numeric(18,0),
	centra_kosztow_dd numeric(18,0)
);

CREATE TABLE dsg_pcz_delegation_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

create view dsg_pcz_deleagtions_view
as
select 
	wz.DOCUMENT_ID,
	wz.status,
	wz.wnioskodawca,
	wz.worker_division,
	wz.nr_wniosek,
	wz.rok_budzetowy,
	wz.data_wypelnienia,
	wz.temat,
	wz.kraj,
	wz.uczestnicy,
	wz.brutto,
	wz.netto,
	wz.termin_realizacji
from dsg_pcz_application_wz wz
union
select	
	wk.DOCUMENT_ID,
	wk.status,
	wk.wnioskodawca,
	wk.worker_division,
	wk.nr_wniosek,
	wk.rok_budzetowy,
	wk.data_wypelnienia,
	wk.temat,
	wk.kraj,
	wk.uczestnicy,
	wk.kwota as netto,
	wk.kwota as brutto,
	wk.termin_realizacji
from dsg_pcz_application_wk wk
union
select	
	wko.DOCUMENT_ID,
	wko.status,
	wko.wnioskodawca,
	wko.worker_division,
	wko.nr_wniosek,
	wko.rok_budzetowy,
	wko.data_wypelnienia,
	wko.temat,
	wko.kraj,
	wko.uczestnicy,
	wko.kwota as netto,
	wko.kwota as brutto,
	wko.termin_realizacji
from dsg_pcz_application_wko wko;