-- nr kont politechniki
create view simple_erp_per_docusafe_konta_bankowe_firmy_view
as
select 
	konto_id  as id, 
	konto_idm as cn, 
	konto_idm + ' ' + numer_konta as title,  
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available
-- baza testowa: SimpleERP_Comp
from ERP_DB_TEST.SimpleERP_Comp.dbo.konta_bankowe_firmy

-- typ dokumentu platnosci
create view simple_erp_per_docusafe_fk_typdokplat_view
as
select 
	typ_dokplat_id  as id, 
	typ_dokplat_idn as cn, 
	nazwa_typdokplat as title,  
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available
-- baza testowa: SimpleERP_Comp
from ERP_DB_TEST.SimpleERP_Comp.dbo.fk_typdokplat
where czy_rozrachunkowy = 1 and czy_aktywny = 1 and rodzaj_dok_plat = 1
and rodzaj_platn = 1

alter table dsg_pcz_application_wko add typdokplat numeric (18,0);
alter table dsg_pcz_application_wko alter column przelac_z_konta numeric (18,0);