alter table  dsg_pcz_costinvoice  add msg_library int default 0;
alter table  dsg_pcz_costinvoice  add nie_dzp int default 0;
alter table  dsg_pcz_costinvoice  add srodek_trwaly numeric (18,0);
alter table  dsg_pcz_costinvoice  add miejsce_uzytkowania varchar (249);
alter table  dsg_pcz_costinvoice  add korekta int default 0;
alter table  dsg_pcz_costinvoice  add korekta_faktura numeric (18,0);
alter table  dsg_pcz_costinvoice  add data_wplywu date;
alter table  dsg_pcz_costinvoice  add waluta numeric (18,0);
alter table  dsg_pcz_costinvoice  add kwotaWwalucie numeric (18,2);
alter table  dsg_pcz_costinvoice  add kurs numeric (18,4);
alter table dsg_pcz_costinvoice add wybrany_dzial numeric (18,0);
alter table DSG_CENTRUM_KOSZTOW_FAKTURY add analytics_id int;
alter table dsg_pcz_rachunki add snr_rachunku varchar (50)
alter table  dsg_pcz_costinvoice  add platnosc_szczegolna int default 0;

create table dsg_pcz_rachunki
(
	id numeric (18,0) identity(1,1) not null,
	nr_rachunku numeric (18,0),
	kwota numeric (18,2)
);