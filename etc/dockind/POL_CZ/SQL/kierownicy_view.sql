create view kierownicy_dzialow_view
as
select distinct(u.NAME) as cn, u.id as id, u.FIRSTNAME + ' ' + u.LASTNAME as title, 0 as centrum, '0' as refValue, 1 as available
from DS_USER as u, DS_USER_TO_DIVISION as t, DS_DIVISION as d, ds_acceptance_condition as a 
where a.divisionGuid=d.GUID and t.DIVISION_ID=d.ID and u.ID=t.USER_ID and a.cn='manager_of_department'

create view kierownicy_jednostek_view
as
select distinct(u.NAME) as cn, u.id as id, u.FIRSTNAME + ' ' + u.LASTNAME as title, 0 as centrum, '0' as refValue, 1 as available
from DS_USER as u, DS_USER_TO_DIVISION as t, DS_DIVISION as d, ds_acceptance_condition as a 
where a.divisionGuid=d.GUID and t.DIVISION_ID=d.ID and u.ID=t.USER_ID and a.cn='manager_of_unit'