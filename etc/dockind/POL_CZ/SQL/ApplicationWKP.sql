alter table dsg_pcz_application_wkp add OSOBA_2 numeric (18,0);
alter table dsg_pcz_application_wkp add WYMIAR_ETATU varchar (30);

CREATE TABLE [dbo].[dsg_pcz_application_wkp]
(
	[DOCUMENT_ID] [numeric](18, 0) NULL,
	[RODZAJ_WNIOSKU] [numeric](18, 0) NULL,
	[STATUS] [numeric](18, 0) NULL,
	[WNIOSKODAWCA] [numeric](18, 0) NULL,
	[WORKER_DIVISION] [numeric](18, 0) NULL,
	[data_wypelnienia] [date] NULL,
	[nr_wniosek] [varchar](50) NULL,
	[OSOBA] [numeric](18, 0) NULL,
	[stanowisko] [varchar](100) NULL,
	[nowe_stanowisko] [varchar](100) NULL,
	[stawka] [numeric](18, 2) NULL,
	[nowa_stawka] [numeric] (18, 2) NULL
);

CREATE TABLE [dbo].[dsg_pcz_application_wkp_multiple_value]
(
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
);