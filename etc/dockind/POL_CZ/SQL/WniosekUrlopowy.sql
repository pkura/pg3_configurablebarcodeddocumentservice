
CREATE TABLE dsg_pcz_wni_urlop
(
	rodzaj_url numeric(18,0),
    DOCUMENT_ID numeric(18,0),
    nr_wniosku varchar(60),
    status numeric(18,0),
	wnioskodawca numeric(18,0),
    OSOBA_ZASTEPUJ numeric(18,0),
    URLOPRODZAJ numeric(18,0),
    tytul varchar(250),
    ctime datetime,
    stanowisko varchar(150),
    data_od datetime,
    data_do datetime,
    ILOSC_DNI numeric(18,0),  
    worker_division numeric(18,0)
);

CREATE TABLE [dsg_pcz_nazwa_jedn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);

CREATE TABLE [dsg_pcz_osoba_zast](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);