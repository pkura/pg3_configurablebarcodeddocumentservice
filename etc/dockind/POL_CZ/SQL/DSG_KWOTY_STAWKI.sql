CREATE TABLE DSG_KWOTY_STAWKI
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	NETTO numeric(18,2),
	VAT numeric(18,0),
	BRUTTO numeric(18,2)
);