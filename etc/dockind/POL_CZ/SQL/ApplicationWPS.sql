CREATE TABLE dsg_pcz_application_wps
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	wnioskodawca_stanowisko varchar(50),
	wnioskodawca_info varchar(500),
	dzieci varchar(1000),
	wypoczynek_indywidualny bit,
	pobyt_na_wczasach bit,
	pobyt_dziecka bit,
	karta_urlopowa numeric(18,0),
	karta_urlopowa_okres varchar(50),
	karta_urlopowa_okres_attach numeric(18,0),
	pobyt_dziecka_attach numeric(18,0),
	pobyt_na_wczasach_attach numeric(18,0),
);