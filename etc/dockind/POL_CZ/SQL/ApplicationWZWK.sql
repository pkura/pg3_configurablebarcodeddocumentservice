alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

CREATE TABLE dsg_pcz_application_wzwk
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	data_wypelnienia date,
	nr_wniosek varchar (50),
	osoba numeric(18,0),
	stanowisko varchar (150),
	stawka numeric(18,2)
);

CREATE TABLE dsg_pcz_application_wzwk_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE dsg_pcz_application_wzwk_osoba (
	ID numeric(18,0) identity(1,1) NOT NULL,
    imie varchar (80),
    nazwisko varchar (180),
    ojciec varchar (80),
    matka varchar (80),
    urodzenie varchar (249),
    data_urodzenia date,
    pesel_nip varchar (249),
    ulica varchar (249),
    nr_domu varchar (40),
    nr_mieszkania varchar (40),
    gmina varchar (100),
    kod_pocztowy varchar (40),
    miasto varchar (249)
);

