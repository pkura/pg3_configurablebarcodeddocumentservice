
create table dsg_pcz_ryczalt(
	id numeric(18, 0) IDENTITY (1,1),
	typ numeric (18,0),
	opis varchar (100),
	currency numeric(18, 0),
	kurs numeric (18,4),
	pln_value numeric (18,2),
	country numeric(18, 0),
	currency_value float,
	value float,
	days integer,
	city varchar(100),
	enum numeric(18,0),
	enum2 numeric(18,0),
	rate float
);