alter table dsg_pcz_costinvoice add termin_platnosci_z_umowy date;

CREATE TABLE DSG_STAWKI_VAT_FAKTURY
(
	ID numeric(18,0) identity (1,1) not null,	
	pozycja numeric (18,0),
	blokada numeric(18,0),
	pozycja numeric(18,0),
	netto numeric(18,2),
	stawka numeric(18,0),
	kwota_vat numeric (18,2),
	dzialalnosc numeric (18,0),
	obciazenie numeric (18,2)
);

  CREATE TABLE simple_erp_per_docusafe_bd_pozycje_vat_view(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	);
	
alter table dsg_pcz_costinvoice add forma_rozliczenia varchar (4096);
alter table dsg_pcz_costinvoice add contract numeric (18,0);
alter table dsg_pcz_costinvoice add nr_umowy_rk varchar (50);
alter table dsg_pcz_costinvoice add nr_biblioteka varchar (50);
alter table dsg_pcz_costinvoice add faktura_skan numeric(18,0);
alter table dsg_centrum_kosztow_faktury add descriptionAmount varchar(600);


