  drop table dsg_pcz_mpk_manager_of_department
  CREATE TABLE dsg_pcz_mpk_manager_of_department(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	)
	  drop table dsg_pcz_mpk_financial_section
  CREATE TABLE dsg_pcz_mpk_financial_section(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	)
	  drop table dsg_pcz_mpk_manager_of_unit
  CREATE TABLE dsg_pcz_mpk_manager_of_unit(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	)	