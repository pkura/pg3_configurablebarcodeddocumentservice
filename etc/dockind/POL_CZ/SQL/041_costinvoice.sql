alter table dsg_pcz_costinvoice add nr_wniosku varchar(50);

CREATE TABLE dsg_ifpan_cpv
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null,
	kwota numeric(19, 2)
);


CREATE TABLE dsg_cpv_kod_nazwa
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);