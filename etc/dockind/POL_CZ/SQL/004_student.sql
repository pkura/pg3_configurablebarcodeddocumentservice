alter table dsg_pcz_application_wspw_students add haslo varchar(100);
alter table dsg_pcz_application_wspw_students add wydzial varchar(100);

ALTER TABLE dsg_pcz_application_wspw_students ALTER COLUMN indeks varchar(50);

ALTER TABLE dsg_pcz_application_wspw_students ADD UNIQUE(id);

INSERT INTO ds_division (guid,name,divisiontype,parent_id,hidden) VALUES ('STUDENCI', 'Studenci zaimportowani','group',1,0);
INSERT INTO ds_division (guid,name,divisiontype,parent_id,hidden) VALUES ('PRACOWNICY', 'Wszyscy pracownicy','group',1,0);