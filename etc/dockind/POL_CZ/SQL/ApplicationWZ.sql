alter table DS_DOCUMENT_KIND alter column tablename varchar (100);
alter table DS_DOCUMENT_KIND alter column name varchar (100);
alter table DS_DOCUMENT_KIND alter column cn varchar (100);

DROP TABLE IF EXISTS dsg_pcz_application_wz;


create table dsg_pcz_application_wz(
	document_id numeric(18, 0),
	status numeric(18, 0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18, 0),
	nr_wniosek varchar(50),
	rok_budzetowy integer,
	data_wypelnienia date,
	cel varchar(500),
	add_info varchar(500),
	transport varchar(500),
	wyjazd_miejsce varchar(100),
	wyjazd_date datetime,
	przyjazd_miejsce varchar(100),
	przyjazd_date datetime,
	kalkulacja_kwota float,
	zaliczka bit,
	forma_zaliczki numeric(18, 0),
	przelew_z varchar(100),
	przelew_na varchar(100),
	suma_pozycji float,
	wybrany_dzial numeric(18,0),
	kalkulacja_attach numeric(18,0),
	czas_podr_wyjazd_miasto varchar(100),
	czas_podr_wyjazd_data datetime,
	czas_podr_wyjazd_miasto_gr varchar(100),
	czas_podr_wyjazd_data_gr datetime,
	czas_podr_powrot_miasto varchar(100),
	czas_podr_powrot_data datetime,
	czas_podr_powrot_miasto_gr varchar(100),
	czas_podr_powrot_data_gr datetime,
	czas_podr_kraj varchar(100),
	czas_podr_zagr varchar(100),
	kwota_ogolem_wydana float,
	pobrana_zaliczka float,
	do_wyplaty float,
	forma_rozliczenia_delegacji numeric(18, 0),
	rozliczenie_przelew_z varchar(100),
	rozliczenie_przelew_na varchar(100),
	rozliczenie_do_zwrotu float,
	bd_rezerwacja_id numeric(18, 0)
);

CREATE TABLE dsg_pcz_application_wz_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);


DROP VIEW IF EXISTS [user_position_division];

CREATE VIEW [user_position_division] AS
SELECT us.LastName as LASTNAME, us.FirstName as FIRSTNAME, div.NAME as DIVISION_NAME, us.MOBILE_PHONE_NUM as MOBILE_PHONE_NUM, ecard.position as POSITION, convert(numeric, (replicate('0', 7-datalength(CAST(us.ID AS varchar))) + CAST(us.ID AS varchar)) + (replicate('0', 7-datalength(CAST(div.ID AS varchar))) + CAST(div.ID AS varchar))) as id
FROM ds_user us
JOIN ds_employee_card ecard
ON us.name=ecard.user_id

JOIN ds_user_to_division usdiv
ON us.id=usdiv.user_id

JOIN DS_DIVISION div
ON div.id=usdiv.division_id and div.DIVISIONTYPE = (
													CASE 
														WHEN (SELECT count(1) from DS_USER_TO_DIVISION usdev join ds_division div on usdev.division_id=div.id and div.divisiontype='division' where usdev.USER_ID=us.ID) = 0 THEN 'position'
														ELSE 'division'
													END
													);
