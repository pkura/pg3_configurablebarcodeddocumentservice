CREATE TABLE dsg_pcz_uczestnicy
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	IMIE_NAZWISKO varchar(150),
	KRAJ numeric (18,0),
	STAWKA_DZIENNA numeric (18,2),
	ILOSC_DNI numeric (18,0),
	KWOTA numeric (18,2)
);

alter table dsg_pcz_application_wgz add OS_TOWARZYSZACE varchar (1000);
alter table dsg_pcz_application_wgz add OS_PODEJMUJACA numeric (18,0);
alter table dsg_pcz_application_wgz add termin_od_do numeric (18,0);
alter table dsg_pcz_application_wgz add DANE_OSOBOWE numeric(18,0);
alter table dsg_pcz_application_wgz add PRO_REKTOR numeric (18,0);
alter table dsg_pcz_application_wgz add PRO_REKTOR_BOOL bit NULL;