alter table dsg_pcz_application_wzp add weryfikacja_merytoryczna numeric (18,0);
alter table dsg_pcz_application_wzp add brak_srodkow smallint default value 0;
CREATE TABLE dsg_pcz_add_werf_merytoryczna
(
	ID numeric(18,0) identity (1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18,0),
	refValue varchar(20),
	available numeric(18,0) default 1 not null
);