create view vice_kwestor_view
as
select distinct(u.NAME) as cn, u.ID as id, u.FIRSTNAME + ' ' + u.LASTNAME as title, 0 as centrum, '0' as refValue, 1 as available 
from DS_DIVISION d, DS_USER u, DS_USER_TO_DIVISION ud
where u.ID = ud.USER_ID and d.ID = ud.DIVISION_ID and d.NAME like '%zas%kwest%';