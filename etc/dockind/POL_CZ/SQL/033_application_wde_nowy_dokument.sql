alter table dsg_pcz_application_wde add PLATNOSC_SZCZEGOLNA smallint;
alter table dsg_pcz_application_wde add PRZELEW_Z_SZCZEG varchar (40);
alter table dsg_pcz_application_wde add ROZ_PLATNOSC_SZCZEGOLNA smallint;
alter table dsg_pcz_application_wde add ROZ_PRZELEW_Z_SZCZEG varchar (40);


drop table dsg_pcz_application_wde;
drop table dsg_pcz_application_wde_multiple_value;

CREATE TABLE dsg_pcz_application_wde
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	wnioskodawca numeric(18,0),
	worker_division numeric(18,0),
	nr_wniosek varchar (50),
	rok_budzetowy numeric(18,0),
	data_wypelnienia date,
	delegowany numeric (18,0),
	WNIOSEK_WYJAZDOWY numeric (18,0),
	CEL_WYJAZDU varchar (300),
	TERMIN_WYJAZDU date,
	TERMIN_POWROTU date,
	JEDNOSTKA_DELEGACJI varchar (150),
	MIEJSCOWOSC_WYJAZDU varchar (150),
	SRODEK_LOKOMOCJI varchar (100),
	PRZEWIDYWANY_KOSZT numeric (18,2),
	ZALICZKA smallint, 
	FORMA_ZALICZKI numeric (18,0),
	PRZELEW_Z numeric (18,0),
	PRZELEW_NA varchar (40),
	wybrany_dzial numeric (18,0),
	POTWIERDZENIE_POBYTU numeric (18,0),
	RYCZALT_COST numeric (18,2),
	SUMA_KOSZTOW_PODROZY numeric (18,2),
	DIETY numeric (18,2),
	NOCLEGI numeric (18,2),
	NOCLEGI_RYCZALT numeric (18,2),
	INNE_WYDATKI numeric (18,2),
	SUMA numeric (18,2),
	POBRANA_ZALICZKA numeric (18,2),
	DO_WYPLATY numeric (18,2),
	FORMA_ZWROTU numeric (18,0),
	ROZLICZENIE_PRZELEW_Z  numeric (18,0),
	ROZLICZENIE_PRZELEW_NA varchar (40),
	DO_ZWROTU numeric (18,2),
	bd_rezerwacja_id numeric (18,0),
	DODATKOWE_INFO varchar (350)
);

CREATE TABLE dsg_pcz_application_wde_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);