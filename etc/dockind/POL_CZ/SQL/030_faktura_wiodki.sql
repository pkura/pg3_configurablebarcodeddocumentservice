-- brutto dla slownika stawki vat
alter table DSG_STAWKI_VAT_FAKTURY add brutto numeric (18,2);

-- columna typ_faktury

alter table dsg_pcz_costinvoice add typ_faktury_wal numeric (18,0);
alter table dsg_pcz_costinvoice add typ_faktury_pln numeric (18,0);
-- select typdokzak_id  , typdok_idn , nazwa , * from typ_dokumentu_zakupu where czy_aktywny = 1 and czywal = 0 --plm
-- select typdokzak_id  , typdok_idn , nazwa , * from typ_dokumentu_zakupu where czy_aktywny = 1 and czywal = 1 --walutowe
-- dokumenty walutowe
create view simple_erp_per_docusafe_typdokzak_wal_view
as
select 
	typdokzak_id  as id, 
	typdok_idn as cn, 
	nazwa as title,  
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available
from ERP_DB_TEST.SimpleERP_LP5.dbo.typ_dokumentu_zakupu 
where czy_aktywny = 1 
and czywal = 1;
-- dokumenty pln
create view simple_erp_per_docusafe_typdokzak_pln_view
as
select 
	typdokzak_id  as id, 
	typdok_idn as cn, 
	nazwa as title,  
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available
from ERP_DB_TEST.SimpleERP_LP5.dbo.typ_dokumentu_zakupu 
where czy_aktywny = 1 
and czywal = 0;

-- typ platnosci 
create view simple_erp_per_docusafe_warpla_view
as
	select warplat_id as id, 
	warplat_idn   as cn,
	warplat_idn as title,
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available 
from ERP_DB_TEST.SimpleERP_LP5.dbo.warplat 
where warplat_id in (19 , 22 , 24  ) 
order by 2;

-- columna DSG_STAWKI_VAT_FAKTURY.pozycja
create view simple_erp_per_docusafe_wytwor_view
as
select 
	wytwor_id as id, 
	wytwor_idm as cn, 
	RTRIM(wytwor_idm) + ' : ' + nazwa  as title,
	'0' AS CENTRUM, 
	null AS refValue, 
	'1' as available
from ERP_DB_TEST.SimpleERP_LP5.dbo.wytwor 
where klaswytw_id = 8 
and wytwor_idm like '4%';

-- columna DSG_STAWKI_VAT_FAKTURY.stawka
create view simple_erp_per_docusafe_vatstaw_view
as
select 
	vatstaw_id as id, 
 	procent as cn,
	vatstaw_ids as title, 
	procent as refValue,
	'0' AS CENTRUM, 
	'1' as available
	from ERP_DB_TEST.SimpleERP_LP5.dbo.vatstaw
where data_obow_do is null or convert ( date , data_obow_do ) >= CONVERT ( date , getdate () );

-- columna WALUTA
create view simple_erp_per_docusafe_waluta_view
as
select 
	waluta_id as id, 
 	symbolwaluty as cn,
	symbolwaluty as title, 
	null as refValue,
	'0' AS CENTRUM, 
	'1' as available
from ERP_DB_TEST.SimpleERP_Comp.dbo.waluta;

alter table alter table DSG_STAWKI_VAT_FAKTURY add komorka numeric (18,0);
-- columna dsg_stawki_vat.komorka
create view simple_erp_per_docusafe_komorka_view
as
select 
	komorka_id as id, 
 	komorka_idk as cn,
	nazwa as title, 
	null as refValue,
	'0' AS CENTRUM, 
	'1' as available
from ERP_DB_TEST.SimpleERP_Comp.dbo.komorka;

alter table alter table DSG_STAWKI_VAT_FAKTURY add zlecenie numeric (18,0);
-- columna dsg_stawki_vat.zlecenie
create view simple_erp_per_docusafe_zlecprod_view
as
select 
	zlecprod_id as id, 
 	zlecprod_idm as cn,
	nazwa as title, 
	null as refValue,
	'0' AS CENTRUM, 
	'1' as available
from ERP_DB_TEST.SimpleERP_Comp.dbo.zlecprod;

-- kontrhenci
-- select dostawca_id , dostawca_idk , dostawca_idk , kontrahent.nazwa  , kodpoczt , miasto , ulica , nrdomu, nrmieszk ,  kraj_idn , nip
-- from dostawca join kontrahent on dostawca.kontrahent_id = kontrahent.kontrahent_id
-- join kraj on kontrahent.kraj_rejpod_id = kraj.kraj_id
--select 'PERSON' as DISCRIMINATOR, dostawca_id as wparam, kontrahent.nazwa as organization, kodpoczt as zip, miasto as location, 
--case
--when nrdomu is not null and nrmieszk is not null THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu) + '/' + RTRIM(nrmieszk)
--when nrdomu is not null and nrmieszk is null THEN RTRIM(ulica) + ' ' + RTRIM(nrdomu)
--when nrdomu is null and nrmieszk is null THEN ulica
--end 
--as street,
--RTRIM(kraj_idn),
--	CASE 
--      WHEN nip IS not null  THEN REPLACE(nip, '-', '')
--      else nip
--   END as nip
--from dostawca join kontrahent on dostawca.kontrahent_id = kontrahent.kontrahent_id
--join kraj on kontrahent.kraj_rejpod_id = kraj.kraj_id

-- warunki platnosci
--select warplat_id , warplat_idn!!!   from warplat where warplat_id in (19 , 22 , 24  )
-- order by 2

-- wskaznik
-- select wskaznik_id ,  rok , wartosc from sys_vat_wskaznik