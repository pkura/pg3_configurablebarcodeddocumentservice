alter table dsg_pcz_application_wzp add wybrany_dzial numeric (18,0);
alter table dsg_pcz_application_wze add wybrany_dzial numeric (18,0);

CREATE TABLE dsg_pcz_add_department
(
	ID numeric(18,0) identity (1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18,0),
	refValue varchar(20),
	available numeric(18,0) default 1 not null
);