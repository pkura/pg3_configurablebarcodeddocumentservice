CREATE TABLE dsg_pcz_application_ww
(
	DOCUMENT_ID numeric(18,0),
	RODZAJ_WNIOSKU numeric(18, 0),
	STATUS numeric(18,0),
	WNIOSKODAWCA numeric(18,0),
	WORKER_DIVISION numeric(18,0),
	nr_wniosek varchar (50),
	rok_budzetowy numeric(18,0),
	data_wypelnienia date,
	temat varchar (350),
	kraj numeric(18,0),
	uczestnicy numeric (18,0),
	netto numeric (18,2),
	brutto numeric (18,2),
	kwota numeric (18,2),
	termin_realizacji date,
	centra_kosztow_dnb numeric(18,0),
	centra_kosztow_dd numeric(18,0)
);

CREATE TABLE dsg_pcz_application_ww_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);