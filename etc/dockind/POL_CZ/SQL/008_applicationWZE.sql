alter table dsg_pcz_application_wze alter column termin_realizacji varchar(50);

alter table dsg_pcz_application_wze add DODATKOWE_INFO varchar(350);
alter table dsg_pcz_application_wze add MIEJSCE_REALIZACJI varchar(200);
alter table dsg_pcz_application_wze add SUMA_BRUTTO numeric(18,2);
alter table dsg_pcz_application_wze add termin_inny numeric(18,0);
alter table dsg_pcz_application_wze add GWARANCJA varchar (350);
alter table dsg_pcz_application_wze add VCE_KWESTOR numeric(18,0);
