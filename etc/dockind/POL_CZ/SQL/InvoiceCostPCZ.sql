CREATE TABLE dsg_pcz_invoicecost
(
	DOCUMENT_ID numeric(18,0),	
	status numeric(18,0),
	net numeric (18,2),
	gross numeric (18,2),
	invoice_date date,
	payment_date date,
	centra_kosztow numeric(18,0),
	contract_nr varchar(50),
	activity numeric(18,0)
);

CREATE TABLE [dsg_pcz_invoice_activity](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	available integer	
);

CREATE TABLE dsg_pcz_cost_category
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_pcz_invoice_cost_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

create view DSG_CENTRUM_KOSZTOW_VIEW as
select ck.id as ID, ck.symbol as CN, ck.name AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available
from DSG_CENTRUM_KOSZTOW as ck;

alter table ds_acceptance_division alter column CODE varchar (100);
alter table ds_acceptance_condition alter column cn varchar (100);

alter table dsg_pcz_invoicecost add rodzaj numeric(18,0);
alter table dsg_pcz_invoicecost add rodzaj_zakupu numeric(18,0);
alter table dsg_pcz_invoicecost add nr_srodka varchar(50);
alter table dsg_pcz_invoicecost add umorzenie_srodka numeric(18,0);