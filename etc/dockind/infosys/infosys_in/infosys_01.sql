CREATE TABLE [dsg_infosys] (
    [document_id]                   numeric(18,0) NULL,
    [assignment_code]               varchar(20) null,
    [assignment_name]               varchar(20) null,
    [cost_assessment]               varchar(20) null,
    [service_type]                  varchar(20) null,
    [requestors_country]            smallint null,
    [requestors_oru]                varchar(20) null,
    [related_finosp_oru]            varchar(20) null,
    [delivery_center]               varchar(20) null,
    [department]                    varchar(20) null,
    [finops_region]                 varchar(20) null,
    [finops_country]                smallint null,
    [billing_currency]              smallint null,
    [billing_region]                varchar(20) null,
    [fir_next_vol_reduction_date]   date null,
    [service_start_date]            date null,
    [service_end_date]              date null,
    [nr_service_months]             varchar(20) null,
    [billing_start_month]           smallint,
    [service_start_month]           smallint,
    [service_end_month]             smallint
    --IN_BILLING_CURRENCY
);

CREATE TABLE [dsg_infosys_ibc] (
    [id]                numeric(18, 0) IDENTITY(1,1) NOT NULL,
    oru                 varchar(20) null,
    task_cost_element   varchar(20) null,
    resource_unit       varchar(20) null,
    resource_unit_code  varchar(20) null,
    labor_number        varchar(20) null,
    labor_months_served varchar(20) null,
    labor_cost_fte      varchar(20) null,
    total_labor_cost    varchar(20) null,
    material_cost       varchar(20) null,
    travel_cost         varchar(20) null
);

CREATE TABLE [dsg_infosys_multiple_value](
	[DOCUMENT_ID]   [numeric](18, 0) NOT NULL,
	[FIELD_CN]      [varchar](100) NOT NULL,
	[FIELD_VAL]     [varchar](100) NULL
) ;