alter table dsg_infosys add delivery_center_i smallint;
alter table dsg_infosys add department_i smallint;
alter table dsg_infosys add finops_region_i smallint;
alter table dsg_infosys add billing_region_i  smallint;
alter table dsg_infosys add requestors_country_i smallint;
alter table dsg_infosys add finops_country_i smallint;
alter table dsg_infosys add service_type_i smallint;