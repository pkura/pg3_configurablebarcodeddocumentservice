alter table dsg_infosys alter column assignment_name varchar(251);
alter table dsg_infosys add billing_start_year varchar(4);
alter table dsg_infosys add service_start_year varchar(4);
alter table dsg_infosys add service_end_year varchar(4);