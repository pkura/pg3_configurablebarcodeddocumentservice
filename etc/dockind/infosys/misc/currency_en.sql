CREATE TABLE dsr_currency(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
    [available] smallint
);

--TOP10
insert into dsr_currency(title,cn,available) values('EUR','EUR',1);
insert into dsr_currency(title,cn,available) values('PLN','PLN',1);
insert into dsr_currency(title,cn,available) values('GBP','GBP',1);
insert into dsr_currency(title,cn,available) values('USD','USD',1);
insert into dsr_currency(title,cn,available) values('JPY','JPY',1);
insert into dsr_currency(title,cn,available) values('CHF','CHF',1);
insert into dsr_currency(title,cn,available) values('RUB','RUB',1);
insert into dsr_currency(title,cn,available) values('SEK','SEK',1);
insert into dsr_currency(title,cn,available) values('CNY','CNY',1);
insert into dsr_currency(title,cn,available) values('AED','AED',1);

--ALL
insert into dsr_currency(title,cn,available) values('AED','AED',1);
insert into dsr_currency(title,cn,available) values('AFN','AFN',1);
insert into dsr_currency(title,cn,available) values('ALL','ALL',1);
insert into dsr_currency(title,cn,available) values('AMD','AMD',1);
insert into dsr_currency(title,cn,available) values('ANG','ANG',1);
insert into dsr_currency(title,cn,available) values('AOA','AOA',1);
insert into dsr_currency(title,cn,available) values('ARS','ARS',1);
insert into dsr_currency(title,cn,available) values('AUD','AUD',1);
insert into dsr_currency(title,cn,available) values('AWG','AWG',1);
insert into dsr_currency(title,cn,available) values('AZN','AZN',1);
insert into dsr_currency(title,cn,available) values('BAM','BAM',1);
insert into dsr_currency(title,cn,available) values('BBD','BBD',1);
insert into dsr_currency(title,cn,available) values('BDT','BDT',1);
insert into dsr_currency(title,cn,available) values('BGN','BGN',1);
insert into dsr_currency(title,cn,available) values('BHD','BHD',1);
insert into dsr_currency(title,cn,available) values('BIF','BIF',1);
insert into dsr_currency(title,cn,available) values('BMD','BMD',1);
insert into dsr_currency(title,cn,available) values('BND','BND',1);
insert into dsr_currency(title,cn,available) values('BOB','BOB',1);
insert into dsr_currency(title,cn,available) values('BRL','BRL',1);
insert into dsr_currency(title,cn,available) values('BSD','BSD',1);
insert into dsr_currency(title,cn,available) values('BTN','BTN',1);
insert into dsr_currency(title,cn,available) values('BWP','BWP',1);
insert into dsr_currency(title,cn,available) values('BYR','BYR',1);
insert into dsr_currency(title,cn,available) values('BZD','BZD',1);
insert into dsr_currency(title,cn,available) values('CAD','CAD',1);
insert into dsr_currency(title,cn,available) values('CDF','CDF',1);
insert into dsr_currency(title,cn,available) values('CHF','CHF',1);
insert into dsr_currency(title,cn,available) values('CLP','CLP',1);
insert into dsr_currency(title,cn,available) values('CNY','CNY',1);
insert into dsr_currency(title,cn,available) values('COP','COP',1);
insert into dsr_currency(title,cn,available) values('CRC','CRC',1);
insert into dsr_currency(title,cn,available) values('CUC','CUC',1);
insert into dsr_currency(title,cn,available) values('CUP','CUP',1);
insert into dsr_currency(title,cn,available) values('CVE','CVE',1);
insert into dsr_currency(title,cn,available) values('CZK','CZK',1);
insert into dsr_currency(title,cn,available) values('DJF','DJF',1);
insert into dsr_currency(title,cn,available) values('DKK','DKK',1);
insert into dsr_currency(title,cn,available) values('DOP','DOP',1);
insert into dsr_currency(title,cn,available) values('DZD','DZD',1);
insert into dsr_currency(title,cn,available) values('EGP','EGP',1);
insert into dsr_currency(title,cn,available) values('ERN','ERN',1);
insert into dsr_currency(title,cn,available) values('ETB','ETB',1);
insert into dsr_currency(title,cn,available) values('EUR','EUR',1);
insert into dsr_currency(title,cn,available) values('FJD','FJD',1);
insert into dsr_currency(title,cn,available) values('FKP','FKP',1);
insert into dsr_currency(title,cn,available) values('GBP','GBP',1);
insert into dsr_currency(title,cn,available) values('GEL','GEL',1);
insert into dsr_currency(title,cn,available) values('GGP','GGP',1);
insert into dsr_currency(title,cn,available) values('GHS','GHS',1);
insert into dsr_currency(title,cn,available) values('GIP','GIP',1);
insert into dsr_currency(title,cn,available) values('GMD','GMD',1);
insert into dsr_currency(title,cn,available) values('GNF','GNF',1);
insert into dsr_currency(title,cn,available) values('GTQ','GTQ',1);
insert into dsr_currency(title,cn,available) values('GYD','GYD',1);
insert into dsr_currency(title,cn,available) values('HKD','HKD',1);
insert into dsr_currency(title,cn,available) values('HNL','HNL',1);
insert into dsr_currency(title,cn,available) values('HRK','HRK',1);
insert into dsr_currency(title,cn,available) values('HTG','HTG',1);
insert into dsr_currency(title,cn,available) values('HUF','HUF',1);
insert into dsr_currency(title,cn,available) values('IDR','IDR',1);
insert into dsr_currency(title,cn,available) values('ILS','ILS',1);
insert into dsr_currency(title,cn,available) values('IMP','IMP',1);
insert into dsr_currency(title,cn,available) values('INR','INR',1);
insert into dsr_currency(title,cn,available) values('IQD','IQD',1);
insert into dsr_currency(title,cn,available) values('IRR','IRR',1);
insert into dsr_currency(title,cn,available) values('ISK','ISK',1);
insert into dsr_currency(title,cn,available) values('JEP','JEP',1);
insert into dsr_currency(title,cn,available) values('JMD','JMD',1);
insert into dsr_currency(title,cn,available) values('JOD','JOD',1);
insert into dsr_currency(title,cn,available) values('JPY','JPY',1);
insert into dsr_currency(title,cn,available) values('KES','KES',1);
insert into dsr_currency(title,cn,available) values('KGS','KGS',1);
insert into dsr_currency(title,cn,available) values('KHR','KHR',1);
insert into dsr_currency(title,cn,available) values('KMF','KMF',1);
insert into dsr_currency(title,cn,available) values('KPW','KPW',1);
insert into dsr_currency(title,cn,available) values('KRW','KRW',1);
insert into dsr_currency(title,cn,available) values('KWD','KWD',1);
insert into dsr_currency(title,cn,available) values('KYD','KYD',1);
insert into dsr_currency(title,cn,available) values('KZT','KZT',1);
insert into dsr_currency(title,cn,available) values('LAK','LAK',1);
insert into dsr_currency(title,cn,available) values('LBP','LBP',1);
insert into dsr_currency(title,cn,available) values('LKR','LKR',1);
insert into dsr_currency(title,cn,available) values('LRD','LRD',1);
insert into dsr_currency(title,cn,available) values('LSL','LSL',1);
insert into dsr_currency(title,cn,available) values('LTL','LTL',1);
insert into dsr_currency(title,cn,available) values('LVL','LVL',1);
insert into dsr_currency(title,cn,available) values('LYD','LYD',1);
insert into dsr_currency(title,cn,available) values('MAD','MAD',1);
insert into dsr_currency(title,cn,available) values('MDL','MDL',1);
insert into dsr_currency(title,cn,available) values('MGA','MGA',1);
insert into dsr_currency(title,cn,available) values('MKD','MKD',1);
insert into dsr_currency(title,cn,available) values('MMK','MMK',1);
insert into dsr_currency(title,cn,available) values('MNT','MNT',1);
insert into dsr_currency(title,cn,available) values('MOP','MOP',1);
insert into dsr_currency(title,cn,available) values('MRO','MRO',1);
insert into dsr_currency(title,cn,available) values('MUR','MUR',1);
insert into dsr_currency(title,cn,available) values('MVR','MVR',1);
insert into dsr_currency(title,cn,available) values('MWK','MWK',1);
insert into dsr_currency(title,cn,available) values('MXN','MXN',1);
insert into dsr_currency(title,cn,available) values('MYR','MYR',1);
insert into dsr_currency(title,cn,available) values('MZN','MZN',1);
insert into dsr_currency(title,cn,available) values('NAD','NAD',1);
insert into dsr_currency(title,cn,available) values('NGN','NGN',1);
insert into dsr_currency(title,cn,available) values('NIO','NIO',1);
insert into dsr_currency(title,cn,available) values('NOK','NOK',1);
insert into dsr_currency(title,cn,available) values('NPR','NPR',1);
insert into dsr_currency(title,cn,available) values('NZD','NZD',1);
insert into dsr_currency(title,cn,available) values('OMR','OMR',1);
insert into dsr_currency(title,cn,available) values('PAB','PAB',1);
insert into dsr_currency(title,cn,available) values('PEN','PEN',1);
insert into dsr_currency(title,cn,available) values('PGK','PGK',1);
insert into dsr_currency(title,cn,available) values('PHP','PHP',1);
insert into dsr_currency(title,cn,available) values('PKR','PKR',1);
insert into dsr_currency(title,cn,available) values('PLN','PLN',1);
insert into dsr_currency(title,cn,available) values('PYG','PYG',1);
insert into dsr_currency(title,cn,available) values('QAR','QAR',1);
insert into dsr_currency(title,cn,available) values('RON','RON',1);
insert into dsr_currency(title,cn,available) values('RSD','RSD',1);
insert into dsr_currency(title,cn,available) values('RUB','RUB',1);
insert into dsr_currency(title,cn,available) values('RWF','RWF',1);
insert into dsr_currency(title,cn,available) values('SAR','SAR',1);
insert into dsr_currency(title,cn,available) values('SBD','SBD',1);
insert into dsr_currency(title,cn,available) values('SCR','SCR',1);
insert into dsr_currency(title,cn,available) values('SDG','SDG',1);
insert into dsr_currency(title,cn,available) values('SEK','SEK',1);
insert into dsr_currency(title,cn,available) values('SGD','SGD',1);
insert into dsr_currency(title,cn,available) values('SHP','SHP',1);
insert into dsr_currency(title,cn,available) values('SLL','SLL',1);
insert into dsr_currency(title,cn,available) values('SOS','SOS',1);
insert into dsr_currency(title,cn,available) values('SPL','SPL',1);
insert into dsr_currency(title,cn,available) values('SRD','SRD',1);
insert into dsr_currency(title,cn,available) values('STD','STD',1);
insert into dsr_currency(title,cn,available) values('SVC','SVC',1);
insert into dsr_currency(title,cn,available) values('SYP','SYP',1);
insert into dsr_currency(title,cn,available) values('SZL','SZL',1);
insert into dsr_currency(title,cn,available) values('THB','THB',1);
insert into dsr_currency(title,cn,available) values('TJS','TJS',1);
insert into dsr_currency(title,cn,available) values('TMT','TMT',1);
insert into dsr_currency(title,cn,available) values('TND','TND',1);
insert into dsr_currency(title,cn,available) values('TOP','TOP',1);
insert into dsr_currency(title,cn,available) values('TRY','TRY',1);
insert into dsr_currency(title,cn,available) values('TTD','TTD',1);
insert into dsr_currency(title,cn,available) values('TVD','TVD',1);
insert into dsr_currency(title,cn,available) values('TWD','TWD',1);
insert into dsr_currency(title,cn,available) values('TZS','TZS',1);
insert into dsr_currency(title,cn,available) values('UAH','UAH',1);
insert into dsr_currency(title,cn,available) values('UGX','UGX',1);
insert into dsr_currency(title,cn,available) values('USD','USD',1);
insert into dsr_currency(title,cn,available) values('UYU','UYU',1);
insert into dsr_currency(title,cn,available) values('UZS','UZS',1);
insert into dsr_currency(title,cn,available) values('VEF','VEF',1);
insert into dsr_currency(title,cn,available) values('VND','VND',1);
insert into dsr_currency(title,cn,available) values('VUV','VUV',1);
insert into dsr_currency(title,cn,available) values('WST','WST',1);
insert into dsr_currency(title,cn,available) values('XAF','XAF',1);
insert into dsr_currency(title,cn,available) values('XCD','XCD',1);
insert into dsr_currency(title,cn,available) values('XDR','XDR',1);
insert into dsr_currency(title,cn,available) values('XOF','XOF',1);
insert into dsr_currency(title,cn,available) values('XPF','XPF',1);
insert into dsr_currency(title,cn,available) values('YER','YER',1);
insert into dsr_currency(title,cn,available) values('ZAR','ZAR',1);
insert into dsr_currency(title,cn,available) values('ZMW','ZMW',1);
insert into dsr_currency(title,cn,available) values('ZWD','ZWD',1);