USE [ima]
GO
/****** Object:  Trigger [dbo].[t_ds_doccase_number_update]    Script Date: 10/18/2011 12:57:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[t_ds_doccase_number_update] 
    ON [dbo].[ds_doccase]
    AFTER INSERT
AS
BEGIN
	
	DECLARE @numberSet as Int
	
	SET @numberSet = (SELECT MAX(cast(number as int))+1 FROM [ima].[dbo].[ds_doccase]) 
	
    UPDATE ds_doccase set ds_doccase.number = ISNULL(@numberSet, 100)
    FROM INSERTED
    where INSERTED.id=ds_doccase.id

END