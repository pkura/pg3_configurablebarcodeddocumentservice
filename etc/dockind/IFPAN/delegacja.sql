CREATE TABLE dsg_ifpan_delegacja
(
    DOCUMENT_ID numeric(18,0) NOT NULL,
    STATUS numeric(18,0),
    WORKER numeric(18,0),
    worker_division numeric(18,0),
    country numeric(18,0),
    city varchar (150),
    organizer_name varchar(150),
    start_date date, 
    finish_date date,
    trip_purpose varchar (251),
    trip_type numeric(18,0),
    organizer_cost varchar (251),
    delegate_costs numeric(18,0),
    advance numeric(18,0),
    entry numeric(18,0),
    rozliczenie_sro_in numeric(18,0),
    rozliczenie_sro_abroad numeric(18,0),
    wydatkowane_srodki numeric(18,0),
    rozliczenie numeric(18,2),
    nadplata numeric(18,0),
    zwrot numeric(18,0)
);

create table dsg_ifpan_trip_type
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	typ numeric(18,0),
	days_amount numeric(18,0)
);

create table dsg_ifpan_delegate_costs
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	typ numeric(18,0),
	amount numeric(18,2),
	cost_amount numeric(18,2),
	platnik numeric(18,0)
);

create table dsg_ifpan_advance
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	amount numeric(18,2),
	waluta numeric(18,0),
	typ numeric(18,0),
	description varchar(251)
);

create table dsg_ifpan_srodki_internal
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	amount numeric(18,2),
	waluta numeric(18,0),
	kurs numeric(18,2),
	pln numeric(18,2)
);

create table dsg_ifpan_srodki_abroad
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	amount numeric(18,2),
	waluta numeric(18,0),
	kurs numeric(18,2),
	pln numeric(18,2)
);

create table dsg_ifpan_wydatkowane_srodki
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	amount numeric(18,2),
	waluta numeric(18,0),
	kurs numeric(18,2),
	pln numeric(18,2),
	typ numeric(18,0)
);

create table dsg_ifpan_delegacja_multiple_value
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	DOCUMENT_ID numeric(18,0) NOT NULL,
	FIELD_CN varchar(150),
	FIELD_VAL varchar (50)
);
