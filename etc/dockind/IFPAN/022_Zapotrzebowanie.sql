alter table dsg_ifpan_zapotrz add uzasadnienie varchar (4096);
alter table dsg_ifpan_zapotrz alter column remarks varchar (4096);
alter table dsg_ifpan_zapotrz alter column REASONNEGOTIAT varchar (4096);
alter table dsg_ifpan_zapotrz alter column ITEMDESCRIPTION varchar (4096);

alter table dsg_ifpan_zapotrz add odprawa bit;
alter table dsg_ifpan_zapotrz add realization date;

CREATE TABLE dsg_ifpan_rodzaj_majatku_trwalego
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

alter table DSG_CENTRUM_KOSZTOW_FAKTURY add inwentarz varchar(100);
alter table dsg_ifpan_coinvoice add waluta numeric(18,0);
alter table dsg_ifpan_coinvoice add kwotaWwalucie numeric(18,2);
alter table dsg_ifpan_coinvoice add kurs numeric(18,4);
alter table dsg_ifpan_coinvoice add sposob numeric(18,0);

alter table dsg_ifpan_coinvoice add addinfo varchar (4096);
alter table dsg_ifpan_coinvoice alter column description varchar (4096);

insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja Chemia/Pierwiastek','chemic','ac.acceptance', 1, 1);


CREATE TABLE [dbo].[dsg_ifpan_cost_category_poig](
	[ID] [numeric](18, 0) identity(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](500) NULL,
	[umowa] [datetime] NULL,
	[nrprojekt] [varchar](50) NULL,
	[projekt] [varchar](200) NULL
) 

alter table dsg_ifpan_zapotrz add chemia bit;
alter table dsg_ifpan_zapotrz add przetarg bit;