alter table dsr_project add projectType varchar(50);
alter table dsr_project_entry add settlementKind varchar(50);
alter table dsr_project_entry add currency varchar(5);
alter table dsr_project_entry add rate numeric(16,2);