alter table dsg_ifpan_coinvoice
add contractornationality numeric(18,0);
alter table dsg_ifpan_zapotrz
add contractornationality numeric(18,0);
alter table dsg_ifpan_zapotrz
add sug_contractornationality numeric(18,0);
alter table dsg_ifpan_contract
add contractornationality numeric(18,0);