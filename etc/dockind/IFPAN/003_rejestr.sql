	alter table dsr_project add contractNumber varchar(100) ;
	alter table dsr_project add remarks varchar(600) ;
	alter table dsr_project add fullGross varchar(50) ;
	alter table dsr_project add defaultCurrency varchar(10) ;
	alter table dsr_project add zalacznik numeric(18,0);
	alter table dsr_project_entry add accountingCostKind integer;
	alter table dsr_project add percentValue float;
	alter table dsr_project add mtime datetime;
	alter table dsr_project_entry add mtime datetime;
    