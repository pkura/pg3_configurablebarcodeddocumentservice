
CREATE TABLE dsg_nr_projektu_ifpan
(
	id numeric(18,0)  NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum numeric(18,0) NULL,
	refValue varchar2(20 char) NULL
);

create sequence dsg_nr_projektu_ifpan_id;


alter table dsg_nr_projektu_ifpan add available NUMBER(1) default 1 not null;
alter table dsg_rodzaj_ifpan add available NUMBER(1) default 1 not null;

alter table DSG_NORMAL_DOCKIND add docType integer;
alter table DSG_NORMAL_DOCKIND add prywatna NUMBER(6);
alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add nr_projektu integer;
alter table DSG_NORMAL_DOCKIND add nr_sprawy varchar2(50 char);

CREATE TABLE dsg_rodzaj_ifpan_in(
	id numeric(18,0) NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum integer	NULL,
	refValue varchar2(20 char) NULL
	,available integer	
);

create sequence dsg_rodzaj_ifpan_in_id;

CREATE TABLE dsg_rodzaj_ifpan_out(
	id numeric(18,0) NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum integer NULL,
	refValue varchar2(20 char) NULL
	,available integer
);

create sequence dsg_rodzaj_ifpan_out_id;

CREATE TABLE dsg_rodzaj_ifpan_INT(
	id numeric(18,0) NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum integer NULL,
	refValue varchar2(20 char) NULL
	,available integer
);

create sequence dsg_rodzaj_ifpan_INT_id;

insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'ZAMOWIENIA','Zamówienia usług',1);
insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'ODPOWIEDZ','Odpowiedzi na zapytania ofertowe',1);
insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'Z_JEDNOSTEK','Pisma z jednostek nadrzędnych (PAN, MNiSW)',1);
insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'TAJNE','Pisma tajne',1);
insert into dsg_rodzaj_ifpan_in(id,cn,title,available)values(dsg_rodzaj_ifpan_in_id.NEXTVAL,'INNE','Pozostałe pisma',1);

insert into dsg_rodzaj_ifpan_out(id,cn,title,available)values(dsg_rodzaj_ifpan_out_id.NEXTVAL,'FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_ifpan_out(id,cn,title,available)values(dsg_rodzaj_ifpan_out_id.NEXTVAL,'DO_JEDNOSTEK','Pisma do jednostek nadrzędnych (PAN, MNiSW',1);
insert into dsg_rodzaj_ifpan_out(id,cn,title,available)values(dsg_rodzaj_ifpan_out_id.NEXTVAL,'DO_PARTNEROW','Pisma do Partnerów  projektów',1);
insert into dsg_rodzaj_ifpan_out(id,cn,title,available)values(dsg_rodzaj_ifpan_out_id.NEXTVAL,'MONITY','Pisma, monity  do kontrahentów',1);
insert into dsg_rodzaj_ifpan_out(id,cn,title,available)values(dsg_rodzaj_ifpan_out_id.NEXTVAL,'INNE','Pozostała  korespondencja ',1);

insert into dsg_rodzaj_ifpan_int(id,cn,title,available)values(dsg_rodzaj_ifpan_int_id.NEXTVAL,'NOTATKA','Notatka służbowa',1);
insert into dsg_rodzaj_ifpan_int(id,cn,title,available)values(dsg_rodzaj_ifpan_int_id.NEXTVAL,'PODANIA','Podania',1);

