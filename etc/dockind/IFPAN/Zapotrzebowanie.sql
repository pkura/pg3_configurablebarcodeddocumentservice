CREATE TABLE dsg_ifpan_zapotrz
(
	contractor numeric(18,0),
    DOCUMENT_ID numeric(18,0),
    description varchar(251),
    centra_kosztow numeric(18,0),
    contract numeric(18,0),
    remarks varchar(50),
    gross_amount numeric(18,2),
    status numeric(18,0),
	orderkind numeric(18,0),
	procedurekind numeric(18,0),
	orderName varchar(50),
	contractZValue numeric(18,2),
	contractEValue numeric(18,2),
	overProvedure bit,
	duueNr varchar(50),
    itemDescription varchar(50),
	serviceKind numeric(18,0),
	reasonNegotiat varchar(250),
	country numeric(18,0),
	art6715 bit,
	art144 bit,
	art145 bit,
	orderSign numeric(18,0),
	accepted bit,
	cpvNr varchar(50),
	modeGiveOrder numeric(18,0)
);

alter table dsg_ifpan_zapotrz add  country_dostawcy numeric(18,0);
update dsg_ifpan_zapotrz set country_dostawcy = country;
alter table dsg_ifpan_zapotrz drop column country;


CREATE TABLE dsg_ifpan_order_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_proc_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);


CREATE TABLE dsg_ifpan_service_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_order_sign
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_order_mode
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);