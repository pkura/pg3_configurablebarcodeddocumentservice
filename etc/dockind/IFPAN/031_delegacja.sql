alter table dsg_ifpan_srodki_internal add SENDED_TO_ERP bit default 1;

alter table dsg_ifpan_delegacja add kompensata smallint default 1;
alter table dsg_ifpan_delegacja add delegation_nr varchar (50);
alter table dsg_ifpan_srodki_internal add typ numeric (18, 0);

-- TODO PODGRAC ten widok NA TESCIE !!!
create view dsg_ifpan_delegacja_bielety_view
as 
select * from dsg_ifpan_delegacja where STATUS <= 20;

CREATE view [dbo].[dsg_ifpan_founding_source_erp_view] as
select pr.id as ID, pr.nrIFPAN as CN, pr.nrIFPAN AS TITLE, '0' AS CENTRUM, e.zlec_prod_id AS refValue, '1' as available
from DSR_PROJECT as pr join dsr_ifpan_project_erp e on pr.ID = e.project_id;

alter table dsg_ifpan_delegacja add oldEntry numeric (18, 0);
update dsg_ifpan_delegacja set oldEntry = entry;
alter table dsg_ifpan_delegacja drop column entry;
alter table dsg_ifpan_delegacja add entry numeric (18, 0) default 1304;
update dsg_ifpan_delegacja set entry = oldEntry;
alter table dsg_ifpan_delegacja drop column oldEntry;

alter table dsg_ifpan_delegacja add old_form_Advance numeric (18);
update dsg_ifpan_delegacja set old_form_Advance = FORM_ADVANCE;
alter table dsg_ifpan_delegacja drop column FORM_ADVANCE;
alter table dsg_ifpan_delegacja add FORM_ADVANCE numeric (18) default 12040;
update dsg_ifpan_delegacja set FORM_ADVANCE = old_form_Advance;
alter table dsg_ifpan_delegacja drop column old_form_Advance;

update dsg_currency set available = 0 where cn not in 
(
'EUR','USD','GBP','PLN','CHF');
update dsg_currency set refValue = 9 where cn = 'EUR';
update dsg_currency set refValue = 3 where cn = 'USD';
update dsg_currency set refValue = 12 where cn = 'GBP';
update dsg_currency set refValue = 14 where cn = 'JPY';
update dsg_currency set refValue = 1 where cn = 'PLN';
update dsg_currency set refValue = 11 where cn = 'CHF';

alter table dsg_ifpan_delegacja add	[nadplata_waluta] [numeric](18, 0);
alter table dsg_ifpan_delegacja add	[nadplata_pln] [numeric](18, 0);
alter table dsg_ifpan_delegacja add	[rozliczenie_ifpan_pln] [numeric](18, 2);
alter table dsg_ifpan_delegacja add	[rozliczenie_ifpan_waluta] [numeric](18, 2);
alter table dsg_ifpan_delegacja add	[ddfin_id] [numeric](18, 0) ;
alter table dsg_ifpan_delegacja add	[kompensata] [smallint] ;
alter table dsg_ifpan_delegacja add	[zaproszenie] [numeric](18, 0);
alter table dsg_ifpan_delegacja add	[potwierdzenie] [numeric](18, 0) ;
alter table dsg_ifpan_delegacja add	[prefinansowanie] [numeric](18, 0);
alter table dsg_ifpan_delegacja add	[sprawozdanie] [numeric](18, 0) ;


	
alter table dsg_ifpan_wydatkowane_srodki add ilosc numeric (18,2);
update dsg_ifpan_delegacja set delegation_nr = CAST (DOCUMENT_ID AS VARCHAR) + '/' + CAST (YEAR((select CTIME from ds_document where ID = dsg_ifpan_delegacja.DOCUMENT_ID)) AS VARCHAR);
alter table dsg_ifpan_wydatkowane_srodki add typ numeric (18,0);
alter table dsg_ifpan_wydatkowane_srodki add stawka numeric (18,2);
alter table dsg_ifpan_wydatkowane_srodki add procent numeric (18,2);