USE [wb_test]
GO

/****** Object:  StoredProcedure [dbo].[up_ani_erp_import_kontrahent_dostawca_pub]    Script Date: 11/05/2012 15:27:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[up_ani_erp_import_kontrahent_dostawca_pub]
-------------------------------------------------------------------------------
-- Procedura  : erp_import_kontrahent_dostawca_pub 
-- Baza danych: MSSQL 
-- Argumenty  :  
-- Zwraca     : 
-- Stworzyl   : ws 
-- Opis       : 
--%%ver_begin%%-----------------------------------------------------------------
-- Historia zmian wersji 
-- 2011-01-03 15:18:12  ws    - #50572
-- 2010-12-29 11:17:49  ws    - #50462
-- 2010-12-16 10:56:20  maz   - #49860
-- 2010-03-18 13:23:12  maz   - dodanie trybu update
-- 2009-12-16 16:11:37  ws    - lista wartosci w cechach
-- 2009-11-10 17:04:15  ws    - testy abr
-- 2009-11-03 10:36:54  ws    - pierwsze uruchomienie
-- 2009-11-02 14:35:44  ws    - wersja inicjalna
--%%ver_end%%-------------------------------------------------------------------
    @dostawca_idn VARCHAR(15) ,
    @klasdost_idn VARCHAR(15) ,
    @powiat_idn VARCHAR(15) = NULL ,
    @nazwawoj CHAR(20) = NULL ,
    @kraj_rejpod_idn VARCHAR(15) = NULL ,
    @oddzial_idn VARCHAR(15) = NULL ,
    @imie CHAR(15) = NULL ,
    @nazwisko CHAR(35) = NULL ,
    @telefon1 VARCHAR(30) = NULL ,
    @telefon2 VARCHAR(30) = NULL ,
    @fax VARCHAR(30) = NULL ,
    @email VARCHAR(50) = NULL ,
    @stanowisko VARCHAR(40) = NULL ,
    @nip CHAR(13) = NULL ,
    @nazwa_us CHAR(40) = NULL ,
    @czyosfiz NUMERIC(10) = NULL ,
    @regon VARCHAR(14) = NULL ,
    @nazwa VARCHAR(60) = NULL ,
    @powiat CHAR(20) = NULL ,
    @gmina CHAR(20) = NULL ,
    @kodpoczt VARCHAR(10) = NULL ,
    @miasto VARCHAR(30) = NULL ,
    @poczta VARCHAR(30) = NULL ,
    @ulica CHAR(40) = NULL ,
    @nrdomu CHAR(10) = NULL ,
    @nrmieszk CHAR(6) = NULL ,
    @status INT = 1 ,
    @nazwa_rozw VARCHAR(80) = NULL ,
    @haslo CHAR(10) = NULL ,
    @uwagi VARCHAR(MAX) = NULL ,
    @identyfikator_num NUMERIC(9, 0) = NULL ,
    @czy_zgoda_pdo NUMERIC(10) = NULL ,
    @data_zgody_pdo DATETIME = NULL ,
    @odleglosc NUMERIC(10, 2) = NULL ,
    @czy_zweryfikowany_nip NUMERIC(10) = NULL ,
    @data_weryfikacji_nip DATETIME = NULL ,
    @zrodlo_weryfikacji_nip VARCHAR(40) = NULL ,
    @nazwa_koresp VARCHAR(256) = NULL ,
    @uzytk_nip_idn VARCHAR(15) = NULL ,
    @kraj_idn VARCHAR(15) = NULL ,
    @nazwa_adr VARCHAR(40) = 'Podstawowy' ,
    @warplat_idn VARCHAR(15) = NULL ,
    @branzysta_idn VARCHAR(15) = NULL ,
    @uzytk_idn VARCHAR(15) = NULL ,
    @atrybutvat_idn VARCHAR(15) = NULL ,
    @waluta_symbolwaluty VARCHAR(15) = NULL ,
    @bank_idn VARCHAR(15) = NULL ,
    @platnik_idn VARCHAR(15) = NULL ,
    @katmetdost_idn VARCHAR(15) = NULL ,
    @maxwartfakt MONEY = NULL ,
    @czy_zakpracchron NUMERIC(1, 0) = NULL ,
    @kredhandtyp INT = NULL ,
    @zammin MONEY = NULL ,
    @opusthand NUMERIC(5, 2) = NULL ,
    @histkal NUMERIC(1, 0) = NULL ,
    @histokr NUMERIC(1, 0) = NULL ,
    @histpod NUMERIC(1, 0) = NULL ,
    @margkonmies INT = NULL ,
    @czyzabl NUMERIC(1, 0) = 0 ,
    @kwotumo MONEY = NULL ,
    @kwota_kredytu MONEY = NULL ,
    @identyfikator_num_dst NUMERIC(9, 0) = NULL ,
    @czycert NUMERIC(1, 0) = NULL ,
    @datcertod DATETIME = NULL ,
    @datacertdo DATETIME = NULL ,
    @dostawca_idk CHAR(10) = NULL ,
    @podatnik_idn VARCHAR(15) = NULL ,
    @adrzak_nazwa VARCHAR(60) = NULL ,
    @adrodb_nazwa VARCHAR(60) = NULL ,
    @powiat_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @woj_nazwa_ovr_null NUMERIC(1, 0) = 0 ,
    @kraj_rejpod_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @oddzial_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @imie_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwisko_ovr_null NUMERIC(1, 0) = 0 ,
    @telefon1_ovr_null NUMERIC(1, 0) = 0 ,
    @telefon2_ovr_null NUMERIC(1, 0) = 0 ,
    @fax_ovr_null NUMERIC(1, 0) = 0 ,
    @email_ovr_null NUMERIC(1, 0) = 0 ,
    @stanowisko_ovr_null NUMERIC(1, 0) = 0 ,
    @nip_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_us_ovr_null NUMERIC(1, 0) = 0 ,
    @czyosfiz_ovr_null NUMERIC(1, 0) = 0 ,
    @regon_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_ovr_null NUMERIC(1, 0) = 0 ,
    @powiat_ovr_null NUMERIC(1, 0) = 0 ,
    @gmina_ovr_null NUMERIC(1, 0) = 0 ,
    @kodpoczt_ovr_null NUMERIC(1, 0) = 0 ,
    @miasto_ovr_null NUMERIC(1, 0) = 0 ,
    @poczta_ovr_null NUMERIC(1, 0) = 0 ,
    @ulica_ovr_null NUMERIC(1, 0) = 0 ,
    @nrdomu_ovr_null NUMERIC(1, 0) = 0 ,
    @nrmieszk_ovr_null NUMERIC(1, 0) = 0 ,
    @status_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_rozw_ovr_null NUMERIC(1, 0) = 0 ,
    @haslo_ovr_null NUMERIC(1, 0) = 0 ,
    @uwagi_ovr_null NUMERIC(1, 0) = 0 ,
    @identyfikator_num_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zgoda_pdo_ovr_null NUMERIC(1, 0) = 0 ,
    @data_zgody_pdo_ovr_null NUMERIC(1, 0) = 0 ,
    @odleglosc_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zweryfikowany_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @data_weryfikacji_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @zrodlo_weryfikacji_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_koresp_ovr_null NUMERIC(1, 0) = 0 ,
    @uzytk_nip_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @kraj_idn_ovr_null NUMERIC(1, 0) = 0 ,
--dostawca
    @warplat_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @klasdost_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @branzysta_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @uzytk_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @atrybutvat_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @waluta_symbolwaluty_ovr_null NUMERIC(1, 0) = 0 ,
    @bank_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @kontrahent_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @platnik_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @katmetdost_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @maxwartfakt_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zakpracchron_ovr_null NUMERIC(1, 0) = 0 ,
    @kredhandtyp_ovr_null NUMERIC(1, 0) = 0 ,
    @zammin_ovr_null NUMERIC(1, 0) = 0 ,
    @opusthand_ovr_null NUMERIC(1, 0) = 0 ,
    @histkal_ovr_null NUMERIC(1, 0) = 0 ,
    @histokr_ovr_null NUMERIC(1, 0) = 0 ,
    @histpod_ovr_null NUMERIC(1, 0) = 0 ,
    @margkonmies_ovr_null NUMERIC(1, 0) = 0 ,
    @czyzabl_ovr_null NUMERIC(1, 0) = 0 ,
    @kwotumo_ovr_null NUMERIC(1, 0) = 0 ,
    @kwota_kredytu_ovr_null NUMERIC(1, 0) = 0 ,
    @identyfikator_num_dst_ovr_null NUMERIC(1, 0) = 0 ,
    @dostawca_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @adrzak_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @adrodb_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @czycert_ovr_null NUMERIC(1, 0) = 0 ,
    @datcertod_ovr_null NUMERIC(1, 0) = 0 ,
    @datacertdo_ovr_null NUMERIC(1, 0) = 0 ,
    @dostawca_idk_ovr_null NUMERIC(1, 0) = 0 ,
    @podatnik_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @c_identyfikator_1 CHAR(15) = NULL ,
    @c_slownik_1 CHAR(1) = NULL ,
    @c_identyfikator_2 CHAR(15) = NULL ,
    @c_slownik_2 CHAR(1) = NULL ,
    @c_identyfikator_3 CHAR(15) = NULL ,
    @c_slownik_3 CHAR(1) = NULL ,
    @c_identyfikator_4 CHAR(15) = NULL ,
    @c_slownik_4 CHAR(1) = NULL ,
    @c_identyfikator_5 CHAR(15) = NULL ,
    @c_slownik_5 CHAR(1) = NULL ,
    @c_identyfikator_6 CHAR(15) = NULL ,
    @c_slownik_6 CHAR(1) = NULL ,
    @c_identyfikator_7 CHAR(15) = NULL ,
    @c_slownik_7 CHAR(1) = NULL ,
    @p_cecha_definiowalna_1 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_2 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_3 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_4 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_5 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_6 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_7 VARCHAR(40) = NULL ,
    @status_czy_update INT , 
    @wparam numeric (19,0) OUTPUT
    
    
AS 
    DECLARE @klasdost_id T_id ,
--kontrahent
        @powiat_id T_id ,
        @woj_id T_id ,
        @kraj_rejpod_id T_id ,
        @oddzial_id T_id ,
        @uzytk_nip_id T_id ,
        @kraj_id T_id ,
--dostawca
        @warplat_id T_id ,
        @branzysta_id T_id ,
        @uzytk_id T_id ,
        @atrybutvat_id T_id ,
        @waluta_id T_id ,
        @bank_id T_id ,
        @platnik_id T_id ,
        @katmetdost_id T_id ,
        @adrzak_id T_id ,
        @adrodb_id T_id ,
        @podatnik_id T_id ,
        @powiat_id_ovr_null NUMERIC(1, 0) ,
        @woj_id_ovr_null NUMERIC(1, 0) ,
        @kraj_rejpod_id_ovr_null NUMERIC(1, 0) ,
        @oddzial_id_ovr_null NUMERIC(1, 0) ,
        @uzytk_nip_id_ovr_null NUMERIC(1, 0) ,
        @kraj_id_ovr_null NUMERIC(1, 0) ,
--dostawca
        @warplat_id_ovr_null NUMERIC(1, 0) ,
        @klasdost_id_ovr_null NUMERIC(1, 0) ,
        @branzysta_id_ovr_null NUMERIC(1, 0) ,
        @uzytk_id_ovr_null NUMERIC(1, 0) ,
        @atrybutvat_id_ovr_null NUMERIC(1, 0) ,
        @waluta_id_ovr_null NUMERIC(1, 0) ,
        @bank_id_ovr_null NUMERIC(1, 0) ,
        @platnik_id_ovr_null NUMERIC(1, 0) ,
        @katmetdost_id_ovr_null NUMERIC(1, 0) ,
        @adrzak_id_ovr_null NUMERIC(1, 0) ,
        @adrodb_id_ovr_null NUMERIC(1, 0) ,
        @podatnik_id_ovr_null NUMERIC(1, 0) ,
        @katparam_id1 T_id ,
        @katparam_id2 T_id ,
        @katparam_id3 T_id ,
        @katparam_id4 T_id ,
        @katparam_id5 T_id ,
        @katparam_id6 T_id ,
        @katparam_id7 T_id ,
        @zakres [varchar](20)



--kontrahent
    DECLARE @datutw DATETIME ,
        @datmod DATETIME ,
        @dostawca_id T_id ,
        @adrkontr_id T_id ,
        @czy_auto_odbiorca INT ,
        @czy_idnum_automat INT ,
        @klasodb_id T_id ,
        @message VARCHAR(8000) ,
        @stat INT
    BEGIN
        IF NOT EXISTS ( SELECT  1
                        FROM    [dbo].[klasdost]
                        WHERE   [klasdost].[klasdost_idn] = @klasdost_idn ) 
            BEGIN
                EXEC [dbo].[sys_setstatus] -1
                SET @message = 'Podana klasa nadrzędna nie istnieje IDN: '
                    + ISNULL(@klasdost_idn, '')
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END
        SELECT  @klasdost_id = ( SELECT MAX([klasdost].[klasdost_id])
                                 FROM   [dbo].[klasdost]
                                 WHERE  [klasdost].[klasdost_idn] = @klasdost_idn
                               )

        IF ISNULL(@dostawca_idn, '') = '' 
            BEGIN
                EXEC [dbo].[sys_setstatus] -1
                SET @message = 'Identyfikator dostawcy nie może być pusty'
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END
	
        IF @powiat_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[powiat]
                                WHERE   [powiat].[powiat_idn] = @powiat_idn )
                    AND @powiat_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol powiatu nie istnieje IDN: '
                            + ISNULL(@powiat_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @powiat_id = ( SELECT   MIN([powiat].[powiat_id])
                                       FROM     [dbo].[powiat]
                                       WHERE    [powiat].[powiat_idn] = @powiat_idn
                                     )
                SELECT  @powiat_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @powiat_id_ovr_null = 1
                SELECT  @powiat_id = NULL
            END
        IF @woj_nazwa_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[wojewodztwo]
                                WHERE   [wojewodztwo].[nazwawoj] = @nazwawoj )
                    AND @woj_nazwa_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol województwa nie istnieje IDN: '
    + ISNULL(@nazwawoj, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @woj_id = ( SELECT  MIN([wojewodztwo].[woj_id])
                                    FROM    [dbo].[wojewodztwo]
                                    WHERE   [wojewodztwo].[nazwawoj] = @nazwawoj
                                  )
                SELECT  @woj_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @woj_id_ovr_null = 1
                SELECT  @woj_id = NULL
            END
        IF @kraj_rejpod_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[kraj]
                                WHERE   [kraj].[kraj_idn] = @kraj_rejpod_idn )
                    AND @kraj_rejpod_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol kraju rejestracji podatkowej nie istnieje IDN: '
                            + ISNULL(@kraj_rejpod_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @kraj_rejpod_id = ( SELECT  MIN([kraj].[kraj_id])
                                            FROM    [dbo].[kraj]
                                            WHERE   [kraj].[kraj_idn] = @kraj_rejpod_idn
                                          )
                SELECT  @kraj_rejpod_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @kraj_rejpod_id_ovr_null = 1
                SELECT  @kraj_rejpod_id = NULL
            END

        IF @kraj_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[kraj]
                                WHERE   [kraj].[kraj_idn] = @kraj_idn )
                    AND @kraj_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol kraju nie istnieje IDN: '
                            + ISNULL(@kraj_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @kraj_id = ( SELECT MIN([kraj].[kraj_id])
                                     FROM   [dbo].[kraj]
                                     WHERE  [kraj].[kraj_idn] = @kraj_idn
                                   )
                SELECT  @kraj_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @kraj_id_ovr_null = 1
                SELECT  @kraj_id = NULL
            END

        IF @oddzial_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[komorka]
                                WHERE   [komorka].[komorka_idn] = @oddzial_idn )
                    AND @oddzial_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol oddzialu nie istnieje IDN: '
                            + ISNULL(@oddzial_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @oddzial_id = ( SELECT  MIN([komorka].[oddzial_id])
                                        FROM    [dbo].[komorka]
                                        WHERE   [komorka].[komorka_idn] = @oddzial_idn
                                      )
                SELECT  @oddzial_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @oddzial_id_ovr_null = 1
                SELECT  @oddzial_id = NULL
            END
        IF @warplat_idn_ovr_null <> 1 
 BEGIN
 IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[warplat]
                                WHERE   [warplat].[warplat_idn] = @warplat_idn )
                    AND @warplat_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol warunków płatności nie istnieje IDN: '
                            + ISNULL(@warplat_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @warplat_id = ( SELECT  MIN([warplat].[warplat_id])
                                        FROM    [dbo].[warplat]
                                        WHERE   [warplat].[warplat_idn] = @warplat_idn
                                      )
                SELECT  @warplat_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @warplat_id_ovr_null = 1
                SELECT  @warplat_id = NULL
            END

        IF @branzysta_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[kp_pracownik]
                                WHERE   [kp_pracownik].[pracownik_idn] = @branzysta_idn )
                    AND @branzysta_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol branżysty nie istnieje IDN: '
                            + ISNULL(@branzysta_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @branzysta_id = ( SELECT    MIN([kp_pracownik].[pracownik_id])
                                          FROM      [dbo].[kp_pracownik]
                                          WHERE     [kp_pracownik].[pracownik_idn] = @branzysta_idn
                                        )
                SELECT  @branzysta_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @branzysta_id_ovr_null = 1
                SELECT  @branzysta_id = NULL
            END

        IF @uzytk_nip_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[uzs]
                                WHERE   [uzs].[uzytk_idn] = @uzytk_nip_idn )
                    AND @uzytk_nip_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol użytkownika (nip) nie istnieje IDN: '
                            + ISNULL(@uzytk_nip_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @uzytk_id = ( SELECT    MIN([uzs].[uzytk_id])
                                      FROM      [dbo].[uzs]
                                      WHERE     [uzs].[uzytk_idn] = @uzytk_nip_idn
                                    )
                SELECT  @uzytk_nip_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @uzytk_nip_id_ovr_null = 1
                SELECT  @uzytk_nip_id = NULL
            END

        IF @uzytk_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[uzs]
                                WHERE   [uzs].[uzytk_idn] = @uzytk_idn )
                    AND @uzytk_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol użytkownika nie istnieje IDN: '
                            + ISNULL(@uzytk_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
          SELECT  @uzytk_id = ( SELECT    MIN([uzs].[uzytk_id])
                                      FROM      [dbo].[uzs]
                                      WHERE     [uzs].[uzytk_idn] = @uzytk_idn
                                    )
                SELECT  @uzytk_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @uzytk_id_ovr_null = 1
                SELECT  @uzytk_id = NULL
            END

        IF @atrybutvat_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[sys_atrybut_vat]
                                WHERE   [sys_atrybut_vat].[atrybutvat_idn] = @atrybutvat_idn )
                    AND @atrybutvat_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol atrybutu VAT nie istnieje IDN: '
                            + ISNULL(@atrybutvat_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @atrybutvat_id = ( SELECT   MIN([sys_atrybut_vat].[atrybutvat_id])
                                           FROM     [dbo].[sys_atrybut_vat]
                                           WHERE    [sys_atrybut_vat].[atrybutvat_idn] = @atrybutvat_idn
                                         )
                SELECT  @atrybutvat_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @atrybutvat_id_ovr_null = 1
                SELECT  @atrybutvat_id = NULL
            END

        IF @waluta_symbolwaluty_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[waluta]
                                WHERE   [waluta].[symbolwaluty] = @waluta_symbolwaluty )
                    AND @waluta_symbolwaluty_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol waluty nie istnieje IDN: '
                            + ISNULL(@waluta_symbolwaluty, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @waluta_id = ( SELECT   MIN([waluta].[waluta_id])
                                       FROM     [dbo].[waluta]
                                       WHERE    [waluta].[symbolwaluty] = @waluta_symbolwaluty
                                     )
                SELECT  @waluta_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @waluta_id_ovr_null = 1
                SELECT  @waluta_id = NULL
            END
        IF @bank_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[bank]
                                WHERE   [bank].[bank_idn] = @bank_idn )
                    AND @bank_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol banku nie istnieje IDN: '
                            + ISNULL(@bank_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @bank_id = ( SELECT MIN([bank].[bank_id])
                                     FROM   [dbo].[bank]
                                     WHERE  [bank].[bank_idn] = @bank_idn
                                   )
                SELECT  @bank_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @bank_id_ovr_null = 1
                SELECT  @bank_id = NULL
            END

        IF @platnik_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[dostawca]
                           WHERE   [dostawca].[dostawca_idn] = @platnik_idn )
                    AND @platnik_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol płatnika nie istnieje IDN: '
                            + ISNULL(@platnik_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @platnik_id = ( SELECT  MIN([dostawca].[dostawca_id])
                                        FROM    [dbo].[dostawca]
                                        WHERE   [dostawca].[dostawca_idn] = @platnik_idn
                                      )
                SELECT  @platnik_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @platnik_id_ovr_null = 1
                SELECT  @platnik_id = NULL
            END
        IF @katmetdost_idn_ovr_null <> 1 
            BEGIN
                IF NOT EXISTS ( SELECT  1
                                FROM    [dbo].[katmetdost]
                                WHERE   [katmetdost].[katmetdost_idn] = @katmetdost_idn )
                    AND @katmetdost_idn_ovr_null = -1 
                    BEGIN
                        EXEC [dbo].[sys_setstatus] -1
                        SET @message = 'Podany symbol metody dostaw nie istnieje IDN: '
                            + ISNULL(@katmetdost_idn, '')
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                SELECT  @katmetdost_id = ( SELECT   MIN([katmetdost].[katmetdost_id])
                                           FROM     [dbo].[katmetdost]
                                           WHERE    [katmetdost].[katmetdost_idn] = @katmetdost_idn
                                         )
                SELECT  @katmetdost_id_ovr_null = 0
            END
        ELSE 
            BEGIN
                SELECT  @katmetdost_id_ovr_null = 1
                SELECT  @katmetdost_id = NULL
            END

	
        IF @c_identyfikator_1 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_1 ) 
            BEGIN
                IF @c_slownik_1 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_1,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 1 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -1
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id1 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id1 ,
                          @katparam_id1 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_1
                        )
            END
        SELECT  @katparam_id1 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                 WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_1
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id1
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_1 )
            AND @c_slownik_1 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id1 ,
                      @p_cecha_definiowalna_1 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id1 )
            AND @katparam_id1 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id1 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_1
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 1 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -1
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	
        IF @c_identyfikator_2 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_2 ) 
            BEGIN
                IF @c_slownik_2 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_2,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 2 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -1
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id2 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id2 ,
                          @katparam_id2 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_2
                        )
            END
        SELECT  @katparam_id2 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_2
                                )
        IF NOT EXISTS ( SELECT  *
     FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id2
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_2 )
            AND @c_slownik_2 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id2 ,
                      @p_cecha_definiowalna_2 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id2 )
            AND @katparam_id2 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id2 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_2
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 2 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -1
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;

        IF @c_identyfikator_3 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_3 ) 
            BEGIN
                IF @c_slownik_3 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_3,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 3 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -3
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id3 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id3 ,
                          @katparam_id3 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_3
                        )
            END
        SELECT  @katparam_id3 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_3
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id3
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_3 )
            AND @c_slownik_3 = 'T' 
      INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id3 ,
                      @p_cecha_definiowalna_3 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id3 )
            AND @katparam_id3 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id3 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_3
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 3 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -3
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	

        IF @c_identyfikator_4 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_4 ) 
            BEGIN
                IF @c_slownik_4 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_4,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 4 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -4
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id4 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id4 ,
                          @katparam_id4 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_4
                        )
            END
        SELECT  @katparam_id4 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_4
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id4
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_4 )
            AND @c_slownik_4 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id4 ,
                      @p_cecha_definiowalna_4 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id4 )
            AND @katparam_id4 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id4 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_4
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 4 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -4
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	
        IF @c_identyfikator_5 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_5 ) 
            BEGIN
                IF @c_slownik_5 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_5,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 5 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -5
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id5 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id5 ,
                          @katparam_id5 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_5
                        )
            END
        SELECT  @katparam_id5 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_5
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id5
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_5 )
            AND @c_slownik_5 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id5 ,
                      @p_cecha_definiowalna_5 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id5 )
            AND @katparam_id5 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id5 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_5
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 5 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -5
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	


        IF @c_identyfikator_6 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_6 ) 
            BEGIN
                IF @c_slownik_6 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_6,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 6 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -6
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id6 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id6 ,
                          @katparam_id6 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_6
                        )
            END
        SELECT  @katparam_id6 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_6
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id6
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_6 )
            AND @c_slownik_6 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id6 ,
                      @p_cecha_definiowalna_6 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id6 )
            AND @katparam_id6 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
[czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id6 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_6
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 6 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -6
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	

        IF @c_identyfikator_7 IS NOT NULL
            AND NOT EXISTS ( SELECT *
                             FROM   [dbo].[katparam]
                             WHERE  [katparam].[typ_obiektu] = 1
                                    AND [katparam].[katparam_idn] = @c_identyfikator_7 ) 
            BEGIN
                IF @c_slownik_7 = 'T' 
                    SELECT  @zakres = 'Lista'
                ELSE 
                    SELECT  @zakres = 'Dowolna'

                EXECUTE [dbo].[erp_import_prc_katparam_pub] @katparam_idn = @c_identyfikator_7,
                    @typ_obiektu = 'Dostawca', @typ = 'Tekst',
                    @zakres = @zakres
                EXEC [dbo].[sys_getstatus] @stat OUTPUT
                IF @stat < 0 
                    BEGIN	
                        SET @message = 'Błąd procedury założenia cechy 7 dla klasy '
                            + @klasdost_idn
                        EXEC [dbo].[sys_setstatus] -7
                        EXEC [dbo].[sys_setstring] @message
                        RETURN
                    END
                EXEC [dbo].[sys_getlong] @katparam_id7 OUT
                INSERT  INTO [dbo].[cechklasdst]
                        ( [katparam_id] ,
                          [klasdost_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru]
                        )
                VALUES  ( @katparam_id7 ,
                          @katparam_id7 ,
                          0 ,
                          0 ,
                          @p_cecha_definiowalna_7
                        )
            END
        SELECT  @katparam_id7 = ( SELECT    MIN([katparam].[katparam_id])
                                  FROM      [dbo].[katparam]
                                  WHERE     [katparam].[typ_obiektu] = 1
                                            AND [katparam].[katparam_idn] = @c_identyfikator_7
                                )
        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[katparampoz]
                        WHERE   [katparampoz].[katparam_id] = @katparam_id7
                                AND [katparampoz].[wartosc] = @p_cecha_definiowalna_7 )
            AND @c_slownik_7 = 'T' 
            INSERT  INTO [dbo].[katparampoz]
                    ( katparam_id ,
                      wartosc 
                    )
            VALUES  ( @katparam_id7 ,
                      @p_cecha_definiowalna_7 
                    )

        IF NOT EXISTS ( SELECT  *
                        FROM    [dbo].[cechklasdst]
                        WHERE   [cechklasdst].[klasdost_id] = @klasdost_id
                                AND [cechklasdst].[katparam_id] = @katparam_id7 )
            AND @katparam_id7 IS NOT NULL 
            BEGIN TRY
                INSERT  INTO [dbo].[cechklasdst]
                        ( [klasdost_id] ,
                          [katparam_id] ,
                          [czy_dziedziczy] ,
                          [czy_statyczny] ,
                          [wartoscparametru] 
                        )
                VALUES  ( @klasdost_id ,
                          @katparam_id7 ,
          0 ,
                          0 ,
                          @p_cecha_definiowalna_7
                        )
            END TRY
            BEGIN CATCH
                SET @message = 'Błąd dodania cechy 7 klasy dostawców: '
                    + @klasdost_idn + ' [' + ERROR_MESSAGE() + ']'
                EXEC [dbo].[sys_setstatus] -7
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END CATCH ;
	



	


        EXEC [dbo].[up_ani_erp_import_kontrahent_dostawca_priv] @dostawca_idn = @dostawca_idn,
            @klasdost_id = @klasdost_id,
			--kontrahent	=	--kontrahent	,
            @powiat_id = @powiat_id, @woj_id = @woj_id,
            @kraj_rejpod_id = @kraj_rejpod_id, @oddzial_id = @oddzial_id,
            @imie = @imie, @nazwisko = @nazwisko, @telefon1 = @telefon1,
            @telefon2 = @telefon2, @fax = @fax, @email = @email,
            @stanowisko = @stanowisko, @nip = @nip, @nazwa_us = @nazwa_us,
            @czyosfiz = @czyosfiz, @regon = @regon, @nazwa = @nazwa,
            @powiat = @powiat, @gmina = @gmina, @kodpoczt = @kodpoczt,
            @miasto = @miasto, @poczta = @poczta, @ulica = @ulica,
            @nrdomu = @nrdomu, @nrmieszk = @nrmieszk, @status = @status,
            @nazwa_rozw = @nazwa_rozw, @haslo = @haslo, @uwagi = @uwagi,
            @identyfikator_num = @identyfikator_num,
            @czy_zgoda_pdo = @czy_zgoda_pdo, @data_zgody_pdo = @data_zgody_pdo,
            @odleglosc = @odleglosc,
            @czy_zweryfikowany_nip = @czy_zweryfikowany_nip,
            @data_weryfikacji_nip = @data_weryfikacji_nip,
            @zrodlo_weryfikacji_nip = @zrodlo_weryfikacji_nip,
            @nazwa_koresp = @nazwa_koresp, @uzytk_nip_id = @uzytk_nip_id,
            @kraj_id = @kraj_id, @nazwa_adr = @nazwa_adr,
			--dostawca		--dostawca	,
            @warplat_id = @warplat_id, @branzysta_id = @branzysta_id,
            @uzytk_id = @uzytk_id, @atrybutvat_id = @atrybutvat_id,
            @waluta_id = @waluta_id, @bank_id = @bank_id,
            @platnik_id = @platnik_id, @katmetdost_id = @katmetdost_id,
            @maxwartfakt = @maxwartfakt, @czy_zakpracchron = @czy_zakpracchron,
            @kredhandtyp = @kredhandtyp, @zammin = @zammin,
            @opusthand = @opusthand, @histkal = @histkal, @histokr = @histokr,
            @histpod = @histpod, @margkonmies = @margkonmies,
            @czyzabl = @czyzabl, @kwotumo = @kwotumo,
            @kwota_kredytu = @kwota_kredytu,
            @identyfikator_num_dst = @identyfikator_num_dst,
            @adrzak_id = @adrzak_id, @adrodb_id = @adrodb_id,
            @czycert = @czycert, @datcertod = @datcertod,
            @datacertdo = @datacertdo, @dostawca_idk = @dostawca_idk,
            @podatnik_id = @podatnik_id,
			--kontrahent		--kontrahent	,
            @powiat_id_ovr_null = @powiat_id_ovr_null,
            @woj_id_ovr_null = @woj_id_ovr_null,
            @kraj_rejpod_id_ovr_null = @kraj_rejpod_id_ovr_null,
            @oddzial_id_ovr_null = @oddzial_id_ovr_null,
            @imie_ovr_null = @imie_ovr_null,
            @nazwisko_ovr_null = @nazwisko_ovr_null,
            @telefon1_ovr_null = @telefon1_ovr_null,
            @telefon2_ovr_null = @telefon2_ovr_null,
            @fax_ovr_null = @fax_ovr_null, @email_ovr_null = @email_ovr_null,
            @stanowisko_ovr_null = @stanowisko_ovr_null,
            @nip_ovr_null = @nip_ovr_null,
            @nazwa_us_ovr_null = @nazwa_us_ovr_null,
            @czyosfiz_ovr_null = @czyosfiz_ovr_null,
            @regon_ovr_null = @regon_ovr_null,
            @nazwa_ovr_null = @nazwa_ovr_null,
            @powiat_ovr_null = @powiat_ovr_null,
            @gmina_ovr_null = @gmina_ovr_null,
            @kodpoczt_ovr_null = @kodpoczt_ovr_null,
            @miasto_ovr_null = @miasto_ovr_null,
            @poczta_ovr_null = @poczta_ovr_null,
            @ulica_ovr_null = @ulica_ovr_null,
            @nrdomu_ovr_null = @nrdomu_ovr_null,
            @nrmieszk_ovr_null = @nrmieszk_ovr_null,
            @status_ovr_null = @status_ovr_null,
            @nazwa_rozw_ovr_null = @nazwa_rozw_ovr_null,
            @haslo_ovr_null = @haslo_ovr_null,
            @uwagi_ovr_null = @uwagi_ovr_null,
            @identyfikator_num_ovr_null = @identyfikator_num_ovr_null,
            @czy_zgoda_pdo_ovr_null = @czy_zgoda_pdo_ovr_null,
            @data_zgody_pdo_ovr_null = @data_zgody_pdo_ovr_null,
            @odleglosc_ovr_null = @odleglosc_ovr_null,
            @czy_zweryfikowany_nip_ovr_null = @czy_zweryfikowany_nip_ovr_null,
            @data_weryfikacji_nip_ovr_null = @data_weryfikacji_nip_ovr_null,
            @zrodlo_weryfikacji_nip_ovr_null = @zrodlo_weryfikacji_nip_ovr_null,
            @nazwa_koresp_ovr_null = @nazwa_koresp_ovr_null,
            @uzytk_nip_id_ovr_null = @uzytk_nip_id_ovr_null,
            @kraj_id_ovr_null = @kraj_id_ovr_null,
			--dostawca		--dostawca	,
            @warplat_id_ovr_null = @warplat_id_ovr_null,
            @klasdost_id_ovr_null = @klasdost_id_ovr_null,
            @branzysta_id_ovr_null = @branzysta_id_ovr_null,
            @uzytk_id_ovr_null = @uzytk_id_ovr_null,
            @atrybutvat_id_ovr_null = @atrybutvat_id_ovr_null,
            @waluta_id_ovr_null = @waluta_id_ovr_null,
            @bank_id_ovr_null = @bank_id_ovr_null,
            @platnik_id_ovr_null = @platnik_id_ovr_null,
            @katmetdost_id_ovr_null = @katmetdost_id_ovr_null,
            @maxwartfakt_ovr_null = @maxwartfakt_ovr_null,
            @czy_zakpracchron_ovr_null = @czy_zakpracchron_ovr_null,
            @kredhandtyp_ovr_null = @kredhandtyp_ovr_null,
            @zammin_ovr_null = @zammin_ovr_null,
            @opusthand_ovr_null = @opusthand_ovr_null,
            @histkal_ovr_null = @histkal_ovr_null,
            @histokr_ovr_null = @histokr_ovr_null,
            @histpod_ovr_null = @histpod_ovr_null,
            @margkonmies_ovr_null = @margkonmies_ovr_null,
            @czyzabl_ovr_null = @czyzabl_ovr_null,
            @kwotumo_ovr_null = @kwotumo_ovr_null,
            @kwota_kredytu_ovr_null = @kwota_kredytu_ovr_null,
            @identyfikator_num_dst_ovr_null = @identyfikator_num_dst_ovr_null,
            @dostawca_idn_ovr_null = @dostawca_idn_ovr_null,
            @adrzak_id_ovr_null = @adrzak_id_ovr_null,
            @adrodb_id_ovr_null = @adrodb_id_ovr_null,
            @czycert_ovr_null = @czycert_ovr_null,
            @datcertod_ovr_null = @datcertod_ovr_null,
            @datacertdo_ovr_null = @datacertdo_ovr_null,
            @dostawca_idk_ovr_null = @dostawca_idk_ovr_null,
            @podatnik_id_ovr_null = @podatnik_id_ovr_null,
            @p_cecha_definiowalna_1 = @p_cecha_definiowalna_1,
            @p_cecha_definiowalna_2 = @p_cecha_definiowalna_2,
            @p_cecha_definiowalna_3 = @p_cecha_definiowalna_3,
            @p_cecha_definiowalna_4 = @p_cecha_definiowalna_4,
            @p_cecha_definiowalna_5 = @p_cecha_definiowalna_5,
            @p_cecha_definiowalna_6 = @p_cecha_definiowalna_6,
            @p_cecha_definiowalna_7 = @p_cecha_definiowalna_7,
            @katparam_id1 = @katparam_id1, @katparam_id2 = @katparam_id2,
            @katparam_id3 = @katparam_id3, @katparam_id4 = @katparam_id4,
            @katparam_id5 = @katparam_id5, @katparam_id6 = @katparam_id6,
            @katparam_id7 = @katparam_id7,
            @status_czy_update = @status_czy_update


declare @status_imp int , 
        @erp_id int , 
        @opis_imp  varchar(256)

			SELECT  
                	@status_imp = [vzmienne].[status] ,
                    @opis_imp = [vzmienne].[wart_varchar] ,
                    @erp_id = [vzmienne].[typ_long]
            FROM    [dbo].[vzmienne]
			begin
-- ani 2012-02-24				
			-- select @wparam = kontrahent_id from dostawca where dostawca_id = @erp_id;
			--  print @wparam;			
			  select @wparam = dostawca_id 
			  from dostawca 
			  where dostawca_id = @erp_id
-- ani 2012-02-24						  
			end
    END



GO

