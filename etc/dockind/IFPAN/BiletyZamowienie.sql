CREATE TABLE dsg_ifpan_bilety
(
	DOCUMENT_ID numeric(18,0) NOT NULL,
    STATUS numeric(18,0),
    WORKER numeric(18,0),
    worker_division numeric(18,0),
    start_date date, 
    finish_date date,
    trip_purpose varchar (251),
    trasa varchar (249),
    koszt numeric(18,2),
    nr_rezerwacji varchar(50),
    purchase_date date,
    instr_wyjazdowa numeric(18,0),
    nr_zestawienia varchar(50), 
    data_potw_wykupu date, 
    data_zestawienia date
);

create table dsg_ifpan_bilety_costs
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	typ numeric(18,0),
	koszt numeric(18,2),
	platnik numeric(18,0)
);

create table dsg_ifpan_bilety_multiple_value
(
	DOCUMENT_ID numeric(18,0) NOT NULL,
	FIELD_CN varchar(150),
	FIELD_VAL varchar (50)
);

alter table dsg_ifpan_wydatkowane_srodki add bilet_id numeric (18,0);
alter table dsg_ifpan_delegate_costs add bilet_id numeric (18,0);