insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM','Przetarg nieograniczony (art. 39)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM1','Przetarg ograniczony (art. 47)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM2','Negocjacje z og�oszeniem (art. 54)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM3','Dialog konkurencyjny (art. 60)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM4','Negocjacje bez og�oszenia (art. 61)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM5','Zam�wienia z wolnej r�ki (art. 66)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM6','Zapytanie o cen� (art. 69)',1);
insert into dsg_ifpan_order_mode(cn,title,available) values('TRYBZAM7','Licytacja elektroniczna (art. 74)',1);

insert into dsg_ifpan_order_sign(cn,title,available) values('ZNZAM','CKP - Przyw�z',1);
insert into dsg_ifpan_order_sign(cn,title,available) values('ZNZAM1','CK - Import',1);
insert into dsg_ifpan_order_sign(cn,title,available) values('ZNZAM2','A - Aparatura krajowa i  materia�y laboratoryjne ( materia�y komputerowe)',1);
insert into dsg_ifpan_order_sign(cn,title,available) values('ZNZAM3','H - Materia�y technologiczne, sanitarne, remontowe itp�.',1);
insert into dsg_ifpan_order_sign(cn,title,available) values('ZNZAM4','C - Serwis i elektronika',1);

ALTER TABLE dsg_ifpan_zapotrz ADD caseNumber numeric(18,0);
ALTER TABLE dsg_ifpan_zapotrz ADD contractor numeric(18,0);