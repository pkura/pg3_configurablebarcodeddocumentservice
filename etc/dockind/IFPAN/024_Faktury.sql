CREATE TABLE dsg_ifpan_akceptacja_wydatkow
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

insert into dsg_ifpan_akceptacja_wydatkow (CN, TITLE, centrum, refValue, available)
values ('T_DIR', 'Dyrektor ds. Technicznych', 0, null, 1);
insert into dsg_ifpan_akceptacja_wydatkow (CN, TITLE, centrum, refValue, available)
values ('P_DIR', 'Dyrektor ds. Projekt�w Badawczych', 0, null, 1);
insert into dsg_ifpan_akceptacja_wydatkow (CN, TITLE, centrum, refValue, available)
values ('K_DIR', 'Dyrektor ds. Naukowych', 0, null, 1);

insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Dyrektor ds. Technicznych','T_DIR','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Dyrektor ds. Projekt�w Badawczych','P_DIR','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Dyrektor ds. Naukowych','K_DIR','ac.acceptance', 1, 1);

alter table dsg_ifpan_coinvoice add akcept_wydatkow numeric (18,0);