create view [dbo].[pozycje_budzetowe_doc_view] as

select mpk.id, CAST(mpk.id as varchar(50)) as cn, CAST(mpk.id as varchar(255)) as title, 0 as centrum, CAST(mul.DOCUMENT_ID as varchar(20)) as refValue, 1 as available

from [dbo].[dsg_ifpan_faktura_multiple_value] mul
JOIN [dbo].DSG_CENTRUM_KOSZTOW_FAKTURY mpk ON mul.FIELD_CN='DSG_CENTRUM_KOSZTOW_FAKTURY' and mpk.id=mul.FIELD_VAL;
GO


