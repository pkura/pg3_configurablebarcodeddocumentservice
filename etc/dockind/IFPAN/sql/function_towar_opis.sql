CREATE FUNCTION [dbo].[towarOpis] (@doc_id numeric(18,0))
	RETURNS varchar(6000)
AS
	BEGIN

	declare @description varchar (8000)
	declare @name varchar (4000)

	DECLARE db_cursor CURSOR FOR
	select ITEM_DESCRIPTION from dsg_ifpan_towar t join dsg_ifpan_zapotrz2_multiple_value zm on t.id = zm.field_val
	where field_cn='TOWAR' and document_id = @doc_id

	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @name

	WHILE @@FETCH_STATUS = 0
	BEGIN
       SET @description = ISNULL(@description, '') + ' ' + @name

       FETCH NEXT FROM db_cursor INTO @name
	END

	CLOSE db_cursor
	DEALLOCATE db_cursor

return @description
end
