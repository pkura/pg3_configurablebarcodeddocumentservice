alter table dsg_ifpan_cpv add column base integer;
alter table dsg_ifpan_cpv add column koszt_netto money;


----zmiana nazwy glownej kolumny, nie miala identity
sp_rename 'dsg_ifpan_cpv.id', 'id_old', 'column';
---- usuniecie kluczy
ALTER TABLE dsg_ifpan_cpv
DROP CONSTRAINT PK__dsg_ifpa__9DB7D2A48D580A76;
---- dodanie identity
ALTER TABLE dsg_ifpan_cpv ADD id numeric(18,0) PRIMARY KEY identity(1,1);
--- usuniecie niepotrzebnej kolumny
alter table dsg_ifpan_cpv drop column id_old;