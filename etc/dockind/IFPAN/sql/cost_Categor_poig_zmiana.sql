declare @cn varchar (50)
declare @title varchar (2048)
declare @umowa datetime
declare @nrprojekt varchar(100)
declare @projekt varchar(2048)
declare @ID numeric (18,0)
declare @tytu�zadania varchar (4096)
declare @podkategoria varchar (max)
declare @nr varchar (50)
declare @pozycjaid numeric(18,0)
declare @base smallint
DECLARE db_cursor CURSOR FOR
SELECT *
FROM [dsg_ifpan_cost_category_poig]


OPEN db_cursor
FETCH NEXT FROM db_cursor INTO @cn, @title, @umowa, @nrprojekt, @projekt, @ID, @tytu�zadania, @podkategoria, @nr, @pozycjaid, @base

WHILE @@FETCH_STATUS = 0
BEGIN
		SET IDENTITY_INSERT [dsg_ifpan_cost_category_poig_2] ON;
		insert into [dsg_ifpan_cost_category_poig_2] (cn, title, umowa, nrprojekt, projekt, ID, tytu�zadania, podkategoria, nr, pozycjaid, base)
		values (@cn, @title, @umowa, @nrprojekt, @projekt, @ID, @tytu�zadania, @podkategoria, @nr, @pozycjaid, @base)
		SET IDENTITY_INSERT [dsg_ifpan_cost_category_poig_2] OFF;

       FETCH NEXT FROM db_cursor INTO @cn, @title, @umowa, @nrprojekt, @projekt, @ID, @tytu�zadania, @podkategoria, @nr, @pozycjaid, @base
END

CLOSE db_cursor
DEALLOCATE db_cursor

drop table [dsg_ifpan_cost_category_poig];

GO
EXEC sp_rename 'dsg_ifpan_cost_category_poig_2','dsg_ifpan_cost_category_poig';
GO


alter table dsg_ifpan_cost_category_poig add KOD_PODATKU numeric (18,0);
alter table dsg_ifpan_cost_category_poig add TO_TEMPLATE int;