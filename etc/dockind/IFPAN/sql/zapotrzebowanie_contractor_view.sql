ALTER VIEW [dbo].[zapotrzebowanie_cotractor_view]
AS
select
d.CTIME as ctime,
zap.DOCUMENT_ID as document_id,
zap.caseNumber as caseNumber,
zap.orderSign as orderSign,
zap.description as description,
p.NIP as nip,
p.ORGANIZATION as organization
from ds_document d join dsg_ifpan_zapotrz zap on d.id=zap.DOCUMENT_ID left join dso_person p on zap.contractor = p.id

union


select

d.CTIME as ctime,
zap.DOCUMENT_ID as document_id,
zap.DEMAND_NUMBER2 as caseNumber,
null as orderSign,
[dbo].[towarOpis](zap.DOCUMENT_ID) as description,
p.NIP as nip,
p.ORGANIZATION as organization
from ds_document d join dsg_ifpan_zapotrz2 zap on d.id=zap.DOCUMENT_ID left join dso_person p on zap.SUG_CONTRACTOR = p.id
join dsg_ifpan_zapotrz2_multiple_value zapom on zapom.document_id = d.id
join dsg_ifpan_towar t on zapom.field_val = t.id
where field_cn='TOWAR'
group by zap.DOCUMENT_ID, d.CTIME, description, zap.DEMAND_NUMBER2, p.NIP, p.ORGANIZATION
