CREATE TABLE dsg_FinancingInstitutionKind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);


insert into dsg_FinancingInstitutionKind(title,cn,available) values('Ministerstwo Nauki i Szkolnictwa Wy�szego','MNiSW',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Narodowe Centrum Nauki','NCN',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Narodowe Centrum Bada� i Rozwoju','NCBiR',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Fundacja na rzecz Nauki Polskiej','FNP',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Komisja Europejska-Program Ramowy','KE-Program-Ramowy',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Komisja Europejska-fundusze strukturalne','KE-fundusze-strukturalne',1);
insert into dsg_FinancingInstitutionKind(title,cn,available) values('Inne','Inne',1);


alter table dsr_project add  clearance char(1);
alter table dsr_project add  finalReport char(1);
alter table dsr_project add  applicationNumber varchar(50);
alter table dsr_project add  financingInstitutionKind integer;