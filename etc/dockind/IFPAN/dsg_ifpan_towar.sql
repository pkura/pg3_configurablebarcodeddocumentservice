CREATE TABLE [dbo].[dsg_ifpan_towar](
	[ID] [numeric](18, 0) NOT NULL,
	lp numeric (18,0),
	ITEM_DESCRIPTION varchar (249),
	DETAIL_ITEM_DESCRIPTION varchar (1000),
	count numeric (18,2),
	price numeric (18,2),
	totalPrice numeric (18,2))