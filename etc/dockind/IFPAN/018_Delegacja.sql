create table dsg_ifpan_delegation_country
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	country numeric(18,0),
	from_date date,
	to_date date
);

alter table dsg_ifpan_srodki_internal add platnik numeric (18,0);
alter table dsg_ifpan_srodki_internal add nrBlocade varchar(50);
alter table dsg_ifpan_wydatkowane_srodki add description varchar(251);
alter table dsg_ifpan_srodki_abroad add description varchar(251);
EXEC sp_rename 'dsg_ifpan_srodki_internal.[PLN]', 'COST_PLN';

alter table dsg_ifpan_delegation_country alter column to_date datetime;
alter table dsg_ifpan_delegation_country alter column from_date datetime;
alter table dsg_ifpan_delegate_costs add rate numeric(18,4);
alter table dsg_ifpan_delegate_costs alter column AMOUNT numeric(18,2);

alter table dsg_ifpan_srodki_internal alter column kurs numeric(18,4);
alter table dsg_ifpan_srodki_abroad alter column kurs numeric(18,4);
alter table dsg_ifpan_wydatkowane_srodki alter column kurs numeric(18,4);

alter table dsg_ifpan_wydatkowane_srodki add wyd_platnik numeric (18,0);
alter table dsg_ifpan_srodki_internal add nrCost varchar(50);

alter table dsg_ifpan_delegation_country add city varchar (100);
alter table dsg_ifpan_delegation_country add organizer_name varchar (150);
alter table dsg_ifpan_delegation_country add organizer_cost varchar (251);