CREATE TABLE [dbo].[dsg_ifpan_zamowienia_umowa_pzp](
	[document_id] [numeric](19, 0) NULL,
	[status] [numeric](19, 0) NULL,
	[nr_sprawy] [varchar](50) NULL,
	[ZNACZNIK_ZAMOWIENIA] [numeric](19, 0) NULL,
	[CASE_NUMBER] [numeric](18, 0) NULL,
	[CONTRACTOR] [numeric](18, 0) NULL,
	[TRYB_POSTEPOWANIA] [numeric](18, 0) NULL,
	[RODZAJ_ZAMOWIENIA] [numeric](18, 0) NULL,
	[SZACOWANA_WARTOSC] [numeric](18, 2) NULL,
	[SUMA_FAKTUR] [numeric](18, 2) NULL,
	[PROG_KWOTOWY] [numeric](18, 2) NULL,
	[SZACOWANA_EURO] [numeric](18, 2) NULL,
	[PRZEKAZANIE] [bit] NULL,
	[ZRODLA_FINANSOWANIA] [varchar](1000) NULL
);

CREATE TABLE [dbo].[dsg_ifpan_zamowienia_umowa_pzp_multiple_value](
	[document_id] [numeric](18, 0) NULL,
	[field_cn] [varchar](150) NULL,
	[field_val] [varchar](50) NULL
); 

create view [dbo].[dsg_ifpan_zapotrz2_view] as
select * from dsg_ifpan_zapotrz2 where status=100 or status=110;