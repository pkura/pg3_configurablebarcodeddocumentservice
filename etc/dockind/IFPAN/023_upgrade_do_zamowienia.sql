alter table dsr_project_entry alter column gross numeric (18,2);
alter table dsr_project_entry alter column net numeric (18,2);
alter table dsr_project_entry alter column vat numeric (18,2);
alter table dsr_project_entry alter column docNr varchar(50);
alter table dsg_ifpan_zapotrz alter column uzasadnienie varchar (4096);
alter table dsg_ifpan_zapotrz alter column remarks varchar (4096);
alter table dsg_ifpan_zapotrz alter column REASONNEGOTIAT varchar (4096);
alter table dsg_ifpan_zapotrz alter column ITEMDESCRIPTION varchar (4096);
alter table dsg_ifpan_coinvoice alter column addinfo varchar (4096);
alter table dsg_ifpan_coinvoice alter column description varchar (4096);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja Chemia/Pierwiastek','chemic','ac.acceptance', 1, 1);

create view dsg_nr_projektu_ifpan_view
as 
select p.ID as id, p.nrIFPAN as cn, p.nrIFPAN as title, null as centrum, '0' as refValue, 1 as available from dsr_project p;
drop table dsg_ifpan_rodzaj_majatku_trwalego;

drop table dsg_ifpan_rodzaj_majatku_trwalego;
--najpierw import z bazy testowej, a potem to co poni�ej
alter table dsg_ifpan_rodzaj_majatku_trwalego drop column ID;
alter table dsg_ifpan_rodzaj_majatku_trwalego add ID numeric(18, 0) identity(1,1) NOT NULL;

drop table dsg_ifpan_cost_category_poig;
--najpierw import z bazy testowej, a potem to co poni�ej
alter table dsg_ifpan_cost_category_poig drop column ID, centrum, refValue,available;
alter table dsg_ifpan_cost_category_poig add ID numeric(18, 0) identity(1,1) NOT NULL;
alter table dsg_ifpan_cost_category_poig alter column title varchar (2048);
alter table dsg_ifpan_cost_category_poig alter column projekt varchar (2048);
alter table dsg_ifpan_cost_category_poig alter column nrprojekt varchar (100);
alter table dsg_ifpan_cost_category_poig alter column cn varchar (50);

insert into dsg_ifpan_faktura_multiple_value (FIELD_CN, DOCUMENT_ID, FIELD_VAL)
select 'ZAPOTRZEBOWANIE', DOCUMENT_ID, zapotrzebowanie  from dsg_ifpan_coinvoice where zapotrzebowanie is not null;
update dsg_ifpan_coinvoice set waluta = 313 where waluta is null;
update dsg_ifpan_coinvoice set kurs = 1;
update dsg_ifpan_coinvoice set kwotaWwalucie = net_amount;
update DSG_CENTRUM_KOSZTOW_FAKTURY set inwentarz = 'brak';

insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja AS','AS','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja CZ','CZ','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja D','D','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja PK','PK','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja ST','ST','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja WNID','WNID','ac.acceptance', 1, 1);
insert into ds_acceptance_division (GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID, ogolna) 
values ('xxx','Akceptacja WP','WP','ac.acceptance', 1, 1);