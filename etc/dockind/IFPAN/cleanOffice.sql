delete from ds_attachment_revision where attachment_id in (select att.id from ds_attachment att join dso_in_document doc on att.document_id=doc.id);
delete from ds_attachment where id in (select att.id from ds_attachment att join dso_in_document doc on att.document_id=doc.id);

delete from ds_attachment_revision where attachment_id in (select att.id from ds_attachment att join dso_out_document doc on att.document_id=doc.id);
delete from ds_attachment where id in (select att.id from ds_attachment att join dso_out_document doc on att.document_id=doc.id);

delete from dso_journal_entry;
delete from dso_journal where id > 3
delete from DSO_DOCUMENT_REMARKS;
delete from dso_document_asgn_history;
delete from dso_document_audit;
delete from ds_document_changelog;
delete from dso_in_document;
delete from dso_out_document;
delete from dsw_task_history_entry;
delete from ds_document where office=1;
delete from dso_person where discriminator = 'SENDER' or discriminator = 'RECIPIENT';
delete from dso_person;
update dso_journal set sequenceid = 1;

delete from DSW_JBPM_TASKLIST;

delete from JBPM4_VARIABLE;
delete from JBPM4_EXECUTION;
delete from JBPM4_PARTICIPATION;
delete from JBPM4_TASK;
delete from JBPM4_DEPLOYPROP;
delete from JBPM4_LOB;
delete from JBPM4_DEPLOYMENT;
delete from ds_document_to_jbpm4;

delete from DS_DOCUMENT_KIND where id not in (1,3,4);