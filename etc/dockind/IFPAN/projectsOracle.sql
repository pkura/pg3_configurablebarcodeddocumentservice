create table dsr_project
(
	ID numeric(18,0) NOT NULL,
	projectManager VARCHAR2(50),
	projectName VARCHAR2(150),
	nrIFPAN VARCHAR2(50),
	startDate TIMESTAMP,
	finishDate TIMESTAMP
);

create sequence dsr_project_id;

create table dsr_project_entry
(
	ID numeric(18,0) NOT NULL,
	projectId numeric(18,0),
	dateEntry TIMESTAMP,
	expenditureDate TIMESTAMP,
	costName VARCHAR(50),
	costKindId numeric(18,0),
	gross numeric(18,2),
	net numeric(18,18),
	vat numeric(18,0),
	entryType VARCHAR2(50),	
	docNr numeric(18,0),
	demandNr numeric(18,0),
	description VARCHAR2(255)
);

create sequence dsr_project_entry_id;

create table dsr_project_costkind
(
	ID numeric(18,0) NOT NULL,
	NAME VARCHAR2(40),
	CATEGORY VARCHAR2(40)
);

create sequence dsr_project_costkind_id;


insert into ds_division (guid,name,divisiontype,parent_id,hidden) values (dsr_country_id.NEXTVAL,'PROJECT_READ', 'Projekty - odczyt','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('PROJECT_MODIFY', 'Projekty - modyfikacja','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('KANCELARIA', 'Kancelaria','group',1,0);