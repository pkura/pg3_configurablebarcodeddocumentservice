USE [wb_test]
GO

/****** Object:  StoredProcedure [dbo].[up_ani_erp_import_kontrahent_dostawca_priv]    Script Date: 11/05/2012 15:27:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[up_ani_erp_import_kontrahent_dostawca_priv]
-------------------------------------------------------------------------------
-- Procedura  : erp_import_kontrahent_dostawca_priv 
-- Baza danych: MSSQL 
-- Argumenty  :  
-- Zwraca     : 
-- Stworzyl   : ws 
-- Opis       : 
--%%ver_begin%%-----------------------------------------------------------------
-- Historia zmian wersji 
-- 2011-01-03 15:18:12  ws    - #50572
-- 2010-12-16 10:55:47  maz   - #49860
-- 2010-03-18 13:22:37  maz   - dodanie trybu update
-- 2009-11-10 17:04:14  ws    - testy abr
-- 2009-11-02 13:41:40  ws    - wersja inicjalna
--%%ver_end%%-------------------------------------------------------------------
    @dostawca_idn VARCHAR(15) ,
    @klasdost_id T_id ,
--kontrahent
    @powiat_id T_id = NULL ,
    @woj_id T_id = NULL ,
    @kraj_rejpod_id T_id = NULL ,
    @oddzial_id T_id = NULL ,
    @imie CHAR(15) = NULL ,
    @nazwisko CHAR(35) = NULL ,
    @telefon1 VARCHAR(30) = NULL ,
    @telefon2 VARCHAR(30) = NULL ,
    @fax VARCHAR(30) = NULL ,
    @email VARCHAR(50) = NULL ,
    @stanowisko VARCHAR(40) = NULL ,
    @nip CHAR(13) = NULL ,
    @nazwa_us CHAR(40) = NULL ,
    @czyosfiz NUMERIC(10) = NULL ,
    @regon VARCHAR(14) = NULL ,
    @nazwa VARCHAR(60) = NULL ,
    @powiat CHAR(20) = NULL ,
    @gmina CHAR(20) = NULL ,
    @kodpoczt VARCHAR(10) = NULL ,
    @miasto VARCHAR(30) = NULL ,
    @poczta VARCHAR(30) = NULL ,
    @ulica CHAR(40) = NULL ,
    @nrdomu CHAR(10) = NULL ,
    @nrmieszk CHAR(6) = NULL ,
    @status INT = 1 ,
    @nazwa_rozw VARCHAR(80) = NULL ,
    @haslo CHAR(10) = NULL ,
    @uwagi VARCHAR(MAX) = NULL ,
    @identyfikator_num NUMERIC(9, 0) = NULL ,
    @czy_zgoda_pdo NUMERIC(10) = NULL ,
    @data_zgody_pdo DATETIME = NULL ,
    @odleglosc NUMERIC(10, 2) = NULL ,
    @czy_zweryfikowany_nip NUMERIC(10) = NULL ,
    @data_weryfikacji_nip DATETIME = NULL ,
    @zrodlo_weryfikacji_nip VARCHAR(40) = NULL ,
    @nazwa_koresp VARCHAR(256) = NULL ,
    @uzytk_nip_id T_id = NULL ,
    @kraj_id T_id = NULL ,
    @nazwa_adr VARCHAR(40) = 'Podstawowy' ,
--dostawca
    @warplat_id T_id = NULL ,
    @branzysta_id T_id = NULL ,
    @uzytk_id T_id = NULL ,
    @atrybutvat_id T_id = NULL ,
    @waluta_id T_id = NULL ,
    @bank_id T_id = NULL ,
    @kontrahent_id T_id = NULL ,
    @platnik_id T_id = NULL ,
    @katmetdost_id T_id = NULL ,
    @maxwartfakt MONEY = NULL ,
    @czy_zakpracchron NUMERIC(1, 0) = NULL ,
    @kredhandtyp INT = NULL ,
    @zammin MONEY = NULL ,
    @opusthand NUMERIC(5, 2) = NULL ,
    @histkal NUMERIC(1, 0) = NULL ,
    @histokr NUMERIC(1, 0) = NULL ,
    @histpod NUMERIC(1, 0) = NULL ,
    @margkonmies INT = NULL ,
    @czyzabl NUMERIC(1, 0) = 0 ,
    @kwotumo MONEY = NULL ,
    @kwota_kredytu MONEY = NULL ,
    @identyfikator_num_dst NUMERIC(9, 0) = NULL ,
    @adrzak_id T_id = NULL ,
    @adrodb_id T_id = NULL ,
    @czycert NUMERIC(1, 0) = NULL ,
    @datcertod DATETIME = NULL ,
    @datacertdo DATETIME = NULL ,
    @dostawca_idk CHAR(10) = NULL ,
    @podatnik_id T_id = NULL ,
--kontrahent
    @powiat_id_ovr_null NUMERIC(1, 0) = 0 ,
    @woj_id_ovr_null NUMERIC(1, 0) = 0 ,
    @kraj_rejpod_id_ovr_null NUMERIC(1, 0) = 0 ,
    @oddzial_id_ovr_null NUMERIC(1, 0) = 0 ,
    @imie_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwisko_ovr_null NUMERIC(1, 0) = 0 ,
    @telefon1_ovr_null NUMERIC(1, 0) = 0 ,
    @telefon2_ovr_null NUMERIC(1, 0) = 0 ,
    @fax_ovr_null NUMERIC(1, 0) = 0 ,
    @email_ovr_null NUMERIC(1, 0) = 0 ,
    @stanowisko_ovr_null NUMERIC(1, 0) = 0 ,
    @nip_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_us_ovr_null NUMERIC(1, 0) = 0 ,
    @czyosfiz_ovr_null NUMERIC(1, 0) = 0 ,
    @regon_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_ovr_null NUMERIC(1, 0) = 0 ,
    @powiat_ovr_null NUMERIC(1, 0) = 0 ,
    @gmina_ovr_null NUMERIC(1, 0) = 0 ,
    @kodpoczt_ovr_null NUMERIC(1, 0) = 0 ,
    @miasto_ovr_null NUMERIC(1, 0) = 0 ,
    @poczta_ovr_null NUMERIC(1, 0) = 0 ,
    @ulica_ovr_null NUMERIC(1, 0) = 0 ,
    @nrdomu_ovr_null NUMERIC(1, 0) = 0 ,
    @nrmieszk_ovr_null NUMERIC(1, 0) = 0 ,
    @status_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_rozw_ovr_null NUMERIC(1, 0) = 0 ,
    @haslo_ovr_null NUMERIC(1, 0) = 0 ,
    @uwagi_ovr_null NUMERIC(1, 0) = 0 ,
    @identyfikator_num_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zgoda_pdo_ovr_null NUMERIC(1, 0) = 0 ,
    @data_zgody_pdo_ovr_null NUMERIC(1, 0) = 0 ,
    @odleglosc_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zweryfikowany_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @data_weryfikacji_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @zrodlo_weryfikacji_nip_ovr_null NUMERIC(1, 0) = 0 ,
    @nazwa_koresp_ovr_null NUMERIC(1, 0) = 0 ,
    @uzytk_nip_id_ovr_null NUMERIC(1, 0) = 0 ,
    @kraj_id_ovr_null NUMERIC(1, 0) = 0 ,
--dostawca
    @warplat_id_ovr_null NUMERIC(1, 0) = 0 ,
    @klasdost_id_ovr_null NUMERIC(1, 0) = 0 ,
    @branzysta_id_ovr_null NUMERIC(1, 0) = 0 ,
    @uzytk_id_ovr_null NUMERIC(1, 0) = 0 ,
    @atrybutvat_id_ovr_null NUMERIC(1, 0) = 0 ,
    @waluta_id_ovr_null NUMERIC(1, 0) = 0 ,
    @bank_id_ovr_null NUMERIC(1, 0) = 0 ,
    @kontrahent_id_ovr_null NUMERIC(1, 0) = 0 ,
    @platnik_id_ovr_null NUMERIC(1, 0) = 0 ,
    @katmetdost_id_ovr_null NUMERIC(1, 0) = 0 ,
    @maxwartfakt_ovr_null NUMERIC(1, 0) = 0 ,
    @czy_zakpracchron_ovr_null NUMERIC(1, 0) = 0 ,
    @kredhandtyp_ovr_null NUMERIC(1, 0) = 0 ,
    @zammin_ovr_null NUMERIC(1, 0) = 0 ,
    @opusthand_ovr_null NUMERIC(1, 0) = 0 ,
    @histkal_ovr_null NUMERIC(1, 0) = 0 ,
    @histokr_ovr_null NUMERIC(1, 0) = 0 ,
    @histpod_ovr_null NUMERIC(1, 0) = 0 ,
    @margkonmies_ovr_null NUMERIC(1, 0) = 0 ,
    @czyzabl_ovr_null NUMERIC(1, 0) = 0 ,
    @kwotumo_ovr_null NUMERIC(1, 0) = 0 ,
    @kwota_kredytu_ovr_null NUMERIC(1, 0) = 0 ,
    @identyfikator_num_dst_ovr_null NUMERIC(1, 0) = 0 ,
    @dostawca_idn_ovr_null NUMERIC(1, 0) = 0 ,
    @adrzak_id_ovr_null NUMERIC(1, 0) = 0 ,
    @adrodb_id_ovr_null NUMERIC(1, 0) = 0 ,
    @czycert_ovr_null NUMERIC(1, 0) = 0 ,
    @datcertod_ovr_null NUMERIC(1, 0) = 0 ,
    @datacertdo_ovr_null NUMERIC(1, 0) = 0 ,
    @dostawca_idk_ovr_null NUMERIC(1, 0) = 0 ,
    @podatnik_id_ovr_null NUMERIC(1, 0) = 0 ,
    @p_cecha_definiowalna_1 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_2 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_3 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_4 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_5 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_6 VARCHAR(40) = NULL ,
    @p_cecha_definiowalna_7 VARCHAR(40) = NULL ,
    @katparam_id1 T_id = NULL ,
    @katparam_id2 T_id = NULL ,
    @katparam_id3 T_id = NULL ,
    @katparam_id4 T_id = NULL ,
    @katparam_id5 T_id = NULL ,
    @katparam_id6 T_id = NULL ,
    @katparam_id7 T_id = NULL ,
    @status_czy_update INT
AS 
    DECLARE @datutw DATETIME ,
        @datmod DATETIME ,
        @dostawca_id T_id ,
        @adrkontr_id T_id ,
        @czy_auto_odbiorca INT ,
        @czy_idnum_automat INT ,
        @klasodb_id T_id ,
        @message VARCHAR(8000) ,
        @stat INT ,
        @tryb_update INT
    BEGIN
        IF NOT EXISTS ( SELECT  1
                        FROM    [dbo].[klasdost]
                        WHERE   [klasdost].[klasdost_id] = @klasdost_id ) 
            BEGIN
                EXEC [dbo].[sys_setstatus] -1
                SET @message = 'Podana klasa nadrzędna nie istnieje ID: '
                    + ISNULL(@klasdost_id, '')
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END
        IF ISNULL(@dostawca_idn, '') = '' 
            BEGIN
                EXEC [dbo].[sys_setstatus] -1
                SET @message = 'Identyfikator dostawcy nie może być pusty'
                EXEC [dbo].[sys_setstring] @message
                RETURN
            END
	-- wartości domyśle i z klasy
     SELECT  @powiat_id = CASE WHEN @powiat_id_ovr_null = 1 THEN NULL
                                  ELSE @powiat_id
                             END ,
                @woj_id = CASE WHEN @woj_id_ovr_null = 1 THEN NULL
                               ELSE @woj_id
                          END ,
                @kraj_rejpod_id = CASE WHEN @kraj_rejpod_id_ovr_null = 1
                                       THEN NULL
                                       ELSE ISNULL(@kraj_rejpod_id,
                                                   [klasdost].[kraj_rejpod_id])
                                  END ,--
                @oddzial_id = CASE WHEN @oddzial_id_ovr_null = 1 THEN NULL
                                   ELSE @oddzial_id
                              END ,
                @imie = CASE WHEN @imie_ovr_null = 1 THEN NULL
                             ELSE @imie
                        END ,
                @nazwisko = CASE WHEN @nazwisko_ovr_null = 1 THEN NULL
                                 ELSE @nazwisko
                            END ,
                @telefon1 = CASE WHEN @telefon1_ovr_null = 1 THEN NULL
                                 ELSE @telefon1
                            END ,
                @telefon2 = CASE WHEN @telefon2_ovr_null = 1 THEN NULL
                                 ELSE @telefon2
                            END ,
                @fax = CASE WHEN @fax_ovr_null = 1 THEN NULL
                            ELSE @fax
                       END ,
                @email = CASE WHEN @email_ovr_null = 1 THEN NULL
                              ELSE @email
                         END ,
                @stanowisko = CASE WHEN @stanowisko_ovr_null = 1 THEN NULL
                                   ELSE @stanowisko
                              END ,
                @nip = CASE WHEN @nip_ovr_null = 1 THEN NULL
                            ELSE @nip
                       END ,
                @nazwa_us = CASE WHEN @nazwa_us_ovr_null = 1 THEN NULL
                                 ELSE @nazwa_us
                            END ,
                @czyosfiz = CASE WHEN @czyosfiz_ovr_null = 1 THEN NULL
                                 ELSE @czyosfiz
                            END ,
                @regon = CASE WHEN @regon_ovr_null = 1 THEN NULL
                              ELSE @regon
                         END ,
                @nazwa = CASE WHEN @nazwa_ovr_null = 1 THEN NULL
                              ELSE @nazwa
                         END ,
                @powiat = CASE WHEN @powiat_ovr_null = 1 THEN NULL
                               ELSE @powiat
                          END ,
                @gmina = CASE WHEN @gmina_ovr_null = 1 THEN NULL
                              ELSE @gmina
                         END ,
                @kodpoczt = CASE WHEN @kodpoczt_ovr_null = 1 THEN NULL
                                 ELSE @kodpoczt
                            END ,
                @miasto = CASE WHEN @miasto_ovr_null = 1 THEN NULL
                               ELSE @miasto
                          END ,
                @poczta = CASE WHEN @poczta_ovr_null = 1 THEN NULL
                               ELSE @poczta
                          END ,
                @ulica = CASE WHEN @ulica_ovr_null = 1 THEN NULL
                              ELSE @ulica
                         END ,
                @nrdomu = CASE WHEN @nrdomu_ovr_null = 1 THEN NULL
                               ELSE @nrdomu
                          END ,
                @nrmieszk = CASE WHEN @nrmieszk_ovr_null = 1 THEN NULL
                                 ELSE @nrmieszk
                            END ,
                @status = CASE WHEN @status_ovr_null = 1 THEN NULL
                               ELSE @status
                          END ,
                @nazwa_rozw = CASE WHEN @nazwa_rozw_ovr_null = 1 THEN NULL
                                   ELSE @nazwa_rozw
                              END ,
                @haslo = CASE WHEN @haslo_ovr_null = 1 THEN NULL
                              ELSE @haslo
                         END ,
                @uwagi = CASE WHEN @uwagi_ovr_null = 1 THEN NULL
                              ELSE @uwagi
                         END ,
                @identyfikator_num = CASE WHEN @identyfikator_num_ovr_null = 1
                                          THEN NULL
                                          ELSE ISNULL(@identyfikator_num,
                                                      [klasdost].[identyfikator_num])
                                     END ,--
                @czy_zgoda_pdo = CASE WHEN @czy_zgoda_pdo_ovr_null = 1
                                      THEN NULL
                                      ELSE @czy_zgoda_pdo
                                 END ,
                @data_zgody_pdo = CASE WHEN @data_zgody_pdo_ovr_null = 1
                                       THEN NULL
                                       ELSE @data_zgody_pdo
                                  END ,
                @odleglosc = CASE WHEN @odleglosc_ovr_null = 1 THEN NULL
                                  ELSE @odleglosc
                             END ,
                @czy_zweryfikowany_nip = CASE WHEN @czy_zweryfikowany_nip_ovr_null = 1
                                              THEN NULL
                                              ELSE @czy_zweryfikowany_nip
                                         END ,
                @data_weryfikacji_nip = CASE WHEN @data_weryfikacji_nip_ovr_null = 1
                                             THEN NULL
                                             ELSE @data_weryfikacji_nip
                                        END ,
                @zrodlo_weryfikacji_nip = CASE WHEN @zrodlo_weryfikacji_nip_ovr_null = 1
                                               THEN NULL
                                               ELSE @zrodlo_weryfikacji_nip
                                          END ,
                @nazwa_koresp = CASE WHEN @nazwa_koresp_ovr_null = 1 THEN NULL
                                     ELSE @nazwa_koresp
                                END ,
                @uzytk_nip_id = CASE WHEN @uzytk_nip_id_ovr_null = 1 THEN NULL
                                     ELSE @uzytk_nip_id
                                END ,
                @kraj_id = CASE WHEN @kraj_id_ovr_null = 1 THEN NULL
                                ELSE @kraj_id
                           END ,
		--dostawca
                @warplat_id = CASE WHEN @warplat_id_ovr_null = 1 THEN NULL
                                   ELSE ISNULL(@warplat_id, [klasdost].[warplat_id])
                              END ,
                @branzysta_id = CASE WHEN @branzysta_id_ovr_null = 1 THEN NULL
                                     ELSE ISNULL(@branzysta_id, [klasdost].[branzysta_id])
                                END ,
                @uzytk_id = CASE WHEN @uzytk_id_ovr_null = 1 THEN NULL
                                 ELSE ISNULL(@uzytk_id, @uzytk_id)
                            END ,
                @atrybutvat_id = CASE WHEN @atrybutvat_id_ovr_null = 1
                                      THEN NULL
                                      ELSE ISNULL(@atrybutvat_id,
                                                  [klasdost].[atrybutvat_id])
                                 END ,
                @waluta_id = CASE WHEN @waluta_id_ovr_null = 1 THEN NULL
                                  ELSE ISNULL(@waluta_id, [klasdost].[waluta_id])
                             END ,
                @bank_id = CASE WHEN @bank_id_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@bank_id, @bank_id)
                           END ,
                @platnik_id = CASE WHEN @platnik_id_ovr_null = 1 THEN NULL
                                   ELSE ISNULL(@platnik_id, @platnik_id)
                             END ,
                @katmetdost_id = CASE WHEN @katmetdost_id_ovr_null = 1
                                      THEN NULL
                                      ELSE ISNULL(@katmetdost_id,
                                                  [klasdost].[katmetdost_id])
                                 END ,
                @maxwartfakt = CASE WHEN @maxwartfakt_ovr_null = 1 THEN NULL
                                    ELSE ISNULL(@maxwartfakt, [klasdost].[maxwartfakt])
                               END ,
                @czy_zakpracchron = CASE WHEN @czy_zakpracchron_ovr_null = 1
                                         THEN NULL
                                         ELSE ISNULL(@czy_zakpracchron,
                                                     [klasdost].[czy_zakpracchron])
                                    END ,
                @kredhandtyp = CASE WHEN @kredhandtyp_ovr_null = 1 THEN NULL
                                    ELSE ISNULL(@kredhandtyp, [klasdost].[kredhandtyp])
                               END ,
                @zammin = CASE WHEN @zammin_ovr_null = 1 THEN NULL
                               ELSE ISNULL(@zammin, [klasdost].[zammin])
                          END ,
                @opusthand = CASE WHEN @opusthand_ovr_null = 1 THEN NULL
                                  ELSE ISNULL(@opusthand, [klasdost].[opusthand])
                             END ,
                @histkal = CASE WHEN @histkal_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@histkal, [klasdost].[histkal])
                           END ,
                @histokr = CASE WHEN @histokr_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@histokr, [klasdost].[histokr])
                           END ,
                @histpod = CASE WHEN @histpod_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@histpod, [klasdost].[histpod])
                           END ,
                @margkonmies = CASE WHEN @margkonmies_ovr_null = 1 THEN NULL
                                    ELSE ISNULL(@margkonmies, [klasdost].[margkonmies])
                               END ,
                @czyzabl = CASE WHEN @czyzabl_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@czyzabl, [klasdost].[czyzabl])
                           END ,
                @datutw = GETDATE() ,
                @datmod = GETDATE() ,
                @kwotumo = CASE WHEN @kwotumo_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@kwotumo, [klasdost].[kwotumo])
                           END ,
                @kwota_kredytu = CASE WHEN @kwota_kredytu_ovr_null = 1
                                      THEN NULL
                                      ELSE ISNULL(@kwota_kredytu,
                                                  [klasdost].[kwota_kredytu])
                                 END ,
                @identyfikator_num_dst = CASE WHEN @identyfikator_num_dst_ovr_null = 1
                                              THEN NULL
                                              ELSE ISNULL(@identyfikator_num_dst,
                                                          [klasdost].[identyfikator_num])
                                         END ,
                @czycert = CASE WHEN @czycert_ovr_null = 1 THEN NULL
                                ELSE ISNULL(@czycert, @czycert)
                           END ,
                @datcertod = CASE WHEN @datcertod_ovr_null = 1 THEN NULL
                                  ELSE ISNULL(@datcertod, @datcertod)
                             END ,
                @datacertdo = CASE WHEN @datacertdo_ovr_null = 1 THEN NULL
                                   ELSE ISNULL(@datacertdo, @datacertdo)
                              END ,
                @dostawca_idk = CASE WHEN @dostawca_idk_ovr_null = 1 THEN NULL
                                     ELSE ISNULL(@dostawca_idk, @dostawca_idk)
                                END ,
                @podatnik_id = CASE WHEN @podatnik_id_ovr_null = 1 THEN NULL
                                    ELSE ISNULL(@podatnik_id, @podatnik_id)
                               END ,
                @czy_auto_odbiorca = [klasdost].[czy_auto_odbiorca] ,
                @czy_idnum_automat = [klasdost].[czy_idnum_automat] ,
                @klasodb_id = [klasdost].[klasodb_id]
        FROM    [dbo].[klasdost]
        WHERE   [klasdost].[klasdost_id] = @klasdost_id

        IF EXISTS ( SELECT  1
                    FROM    [dbo].[dostawca]
                    WHERE   [dostawca].[dostawca_idn] = @dostawca_idn )
            AND @status_czy_update = 1 
            SELECT  @tryb_update = 1
        ELSE 
            SELECT  @tryb_update = 0

        IF @tryb_update = 0 
            BEGIN
                BEGIN TRY
                    INSERT  INTO [dbo].[kontrahent]
                            ( [powiat_id] ,
                              [woj_id] ,
                              [kraj_rejpod_id] ,
                              [oddzial_id] ,
                              [imie] ,
                              [nazwisko] ,
                              [telefon1] ,
                              [telefon2] ,
                              [fax] ,
                              [email] ,
                              [stanowisko] ,
                              [nip] ,
                              [nazwa_us] ,
                              [czyosfiz] ,
                              [regon] ,
                              [nazwa] ,
                              [powiat] ,
                              [gmina] ,
                              [kodpoczt] ,
                              [miasto] ,
                              [poczta] ,
                              [ulica] ,
                              [nrdomu] ,
                              [nrmieszk] ,
                              [status] ,
                              [nazwa_rozw] ,
                              [haslo] ,
                              [uwagi] ,
                              [identyfikator_num] ,
                              [czy_zgoda_pdo] ,
                              [data_zgody_pdo] ,
                              [odleglosc] ,
                              [czy_zweryfikowany_nip] ,
                              [data_weryfikacji_nip] ,
                              [zrodlo_weryfikacji_nip] ,
                              [nazwa_koresp] ,
                              [uzytk_nip_id] ,
                              [kraj_id]
                            )
                    VALUES  ( @powiat_id ,
                              @woj_id ,
                              @kraj_rejpod_id ,
                              @oddzial_id ,
                              @imie ,
                              @nazwisko ,
                              @telefon1 ,
                              @telefon2 ,
                              @fax ,
                              @email ,
                              @stanowisko ,
                              @nip ,
                              @nazwa_us ,
                              @czyosfiz ,
                              @regon ,
                              @nazwa ,
                              @powiat ,
                              @gmina ,
                              @kodpoczt ,
                              @miasto ,
                              @poczta ,
                              @ulica ,
                              @nrdomu ,
                              @nrmieszk ,
                              @status ,
                              @nazwa_rozw ,
                              @haslo ,
                              @uwagi ,
                              @identyfikator_num ,
                              @czy_zgoda_pdo ,
                              @data_zgody_pdo ,
                             @odleglosc ,
                              @czy_zweryfikowany_nip ,
                              @data_weryfikacji_nip ,
                              @zrodlo_weryfikacji_nip ,
                              @nazwa_koresp ,
                              @uzytk_nip_id ,
                              @kraj_id
                            )
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd dodania kontrahenta: '
                        + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;

                EXEC @kontrahent_id = [dbo].[sys_getinsertidentity] 'kontrahent'

                BEGIN TRY
                    INSERT  INTO [dbo].[dostawca]
                            ( [warplat_id] ,
                              [klasdost_id] ,
                              [branzysta_id] ,
                              [uzytk_id] ,
                              [atrybutvat_id] ,
                              [waluta_id] ,
                              [bank_id] ,
                              [kontrahent_id] ,
                              [platnik_id] ,
                              [katmetdost_id] ,
                              [maxwartfakt] ,
                              [czy_zakpracchron] ,
                              [kredhandtyp] ,
                              [zammin] ,
                              [opusthand] ,
                              [histkal] ,
                              [histokr] ,
                              [histpod] ,
                              [margkonmies] ,
                              [czyzabl] ,
                              [datutw] ,
                              [datmod] ,
                              [kwotumo] ,
                              [kwota_kredytu] ,
                              [identyfikator_num] ,
                              [dostawca_idn] ,
                              [czycert] ,
                              [datcertod] ,
                              [datacertdo] ,
                              [dostawca_idk] ,
                              [podatnik_id]
                            )
                    VALUES  ( @warplat_id ,
                              @klasdost_id ,
                              @branzysta_id ,
                              @uzytk_id ,
                              @atrybutvat_id ,
                              @waluta_id ,
                              @bank_id ,
                              @kontrahent_id ,
                              @platnik_id ,
                              @katmetdost_id ,
                              @maxwartfakt ,
                              @czy_zakpracchron ,
                              @kredhandtyp ,
                              @zammin ,
                              @opusthand ,
                              @histkal ,
                              @histokr ,
                              @histpod ,
                              @margkonmies ,
                              @czyzabl ,
                              @datutw ,
                              @datmod ,
                              @kwotumo ,
                              @kwota_kredytu ,
                              @identyfikator_num_dst ,
                              @dostawca_idn ,
                              @czycert ,
                              @datcertod ,
                              @datacertdo ,
                              @dostawca_idk ,
                              @podatnik_id
                            )
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd dodania dostawcy: ' + @dostawca_idn
                        + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;

                EXEC @dostawca_id = [dbo].[sys_getinsertidentity] 'dostawca'

                BEGIN TRY
                    INSERT  INTO [dbo].[adrkontr]
                            ( powiat_id ,
                              woj_id ,
                              kontrahent_id ,
                              kraj_id ,
                              nazwa ,
                              powiat ,
                              gmina ,
                              kodpoczt ,
                              miasto ,
                              poczta ,
                              ulica ,
                              nrdomu ,
                              nrmieszk ,
                              nazwa_koresp
                            )
                    VALUES  ( @powiat_id ,
                              @woj_id ,
                              @kontrahent_id ,
                              @kraj_id ,
                              @nazwa_adr ,
                              @powiat ,
                              @gmina ,
                              @kodpoczt ,
                              @miasto ,
                              @poczta ,
                              @ulica ,
                              @nrdomu ,
                              @nrmieszk ,
                              @nazwa_koresp
                            )
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd dodania adresu dostawcy: '
                        + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;
                EXEC @adrkontr_id = [dbo].[sys_getinsertidentity] 'adrkontr'

                UPDATE  [dbo].[kontrahent]
                SET     [kontrahent].[adrpod_id] = @adrkontr_id
                WHERE   [kontrahent].[kontrahent_id] = @kontrahent_id
			
                UPDATE  [dbo].[dostawca]
                SET     [dostawca].[adrzak_id] = @adrkontr_id ,
                        [dostawca].[adrodb_id] = @adrkontr_id
                WHERE   [dostawca].[dostawca_id] = @dostawca_id

		--cechy z klasy
                CREATE TABLE #cechydst
                    (
                      [klasdost_id] [numeric](10, 0) ,
                      [katparam_id] [numeric](10, 0) ,
                      [czy_statyczny] [numeric](1, 0) DEFAULT ( (0) ) ,
                      [wartoscparametru] [char](20) ,
                      [zakres] INT
                    ) 
                INSERT  INTO #cechydst
                        ( katparam_id ,
                          klasdost_id ,
                          czy_statyczny ,
                          wartoscparametru ,
                          zakres 
                        )
                        EXECUTE [dbo].[sp_dst_select_dla_ds_cech] @klasdost_id

                INSERT  INTO [dbo].[cechydst]
                        ( katparam_id ,
                          dostawca_id ,
                          klasdost_id ,
                          czy_statyczny ,
                          wartoscparametru
                        )
                        SELECT  katparam_id ,
                                @dostawca_id ,
                                klasdost_id ,
                                czy_statyczny ,
                                wartoscparametru
                        FROM    #cechydst
		
                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_1
                WHERE   [cechydst].[katparam_id] = @katparam_id1
                        AND [cechydst].[dostawca_id] = @dostawca_id

                UPDATE  [dbo].[cechydst]
         SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_2
                WHERE   [cechydst].[katparam_id] = @katparam_id2
                        AND [cechydst].[dostawca_id] = @dostawca_id
		
                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_3
                WHERE   [cechydst].[katparam_id] = @katparam_id3
                        AND [cechydst].[dostawca_id] = @dostawca_id

                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_4
                WHERE   [cechydst].[katparam_id] = @katparam_id4
                        AND [cechydst].[dostawca_id] = @dostawca_id

                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_5
                WHERE   [cechydst].[katparam_id] = @katparam_id5
                        AND [cechydst].[dostawca_id] = @dostawca_id

                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_6
                WHERE   [cechydst].[katparam_id] = @katparam_id6
                        AND [cechydst].[dostawca_id] = @dostawca_id

                UPDATE  [dbo].[cechydst]
                SET     [cechydst].[wartoscparametru] = @p_cecha_definiowalna_7
                WHERE   [cechydst].[katparam_id] = @katparam_id7
                        AND [cechydst].[dostawca_id] = @dostawca_id

                DROP TABLE #cechydst
                IF @czy_auto_odbiorca = 1 
                    BEGIN
                        BEGIN TRY
                            EXECUTE [dbo].[ot_dst_auto_odbiorca] @dostawca_id,
                                @klasodb_id, 0 
                        END TRY
                        BEGIN CATCH
                            SET @message = 'Błąd utworzenia skojarzonego odbiorcy: '
                                + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                            EXEC [dbo].[sys_setstatus] -1
                            EXEC [dbo].[sys_setstring] @message
                            RETURN
                        END CATCH	
                        EXEC [dbo].[sys_getstatus] @stat OUTPUT
                        IF @stat < 0 
                            BEGIN
                                EXEC [dbo].[sys_getmessage] @message OUTPUT
                                SELECT  @message = [messages].[msgtext]
                                FROM    [dbo].[messages]
                                WHERE   @message = [messages].[msgid]
                                SET @message = 'Błąd utworzenia skojarzonego odbiorcy: '
                                    + @dostawca_idn + ' [' + @message + ']'
                                EXEC [dbo].[sys_setstatus] -1
                                EXEC [dbo].[sys_setstring] @message
                                RETURN
                            END
                    END
            END 
        ELSE 
            BEGIN
                SELECT  @dostawca_id = [dostawca].[dostawca_id] ,
                        @kontrahent_id = [dostawca].[kontrahent_id]
                FROM    [dbo].[dostawca]
                WHERE   [dostawca].[dostawca_idn] = @dostawca_idn
                SELECT  @adrkontr_id = [kontrahent].[adrpod_id]
                FROM    [dbo].[kontrahent]
                WHERE   [kontrahent].[kontrahent_id] = @kontrahent_id
                BEGIN TRY
                    UPDATE  [dbo].[kontrahent]
                    SET     [kontrahent].[powiat_id] = @powiat_id ,
                            [kontrahent].[woj_id] = @woj_id ,
                            [kontrahent].[kraj_rejpod_id] = @kraj_rejpod_id ,
                            [kontrahent].[oddzial_id] = @oddzial_id ,
                            [kontrahent].[imie] = @imie ,
                            [kontrahent].[nazwisko] = @nazwisko ,
                     [kontrahent].[telefon1] = @telefon1 ,
                            [kontrahent].[telefon2] = @telefon2 ,
                            [kontrahent].[fax] = @fax ,
                            [kontrahent].[email] = @email ,
                            [kontrahent].[stanowisko] = @stanowisko ,
                            [kontrahent].[nip] = @nip ,
                            [kontrahent].[nazwa_us] = @nazwa_us ,
                            [kontrahent].[czyosfiz] = @czyosfiz ,
                            [kontrahent].[regon] = @regon ,
                            [kontrahent].[nazwa] = @nazwa ,
                            [kontrahent].[powiat] = @powiat ,
                            [kontrahent].[gmina] = @gmina ,
                            [kontrahent].[kodpoczt] = @kodpoczt ,
                            [kontrahent].[miasto] = @miasto ,
                            [kontrahent].[poczta] = @poczta ,
                            [kontrahent].[ulica] = @ulica ,
                            [kontrahent].[nrdomu] = @nrdomu ,
                            [kontrahent].[nrmieszk] = @nrmieszk ,
                            [kontrahent].[status] = @status ,
                            [kontrahent].[nazwa_rozw] = @nazwa_rozw ,
                            [kontrahent].[haslo] = @haslo ,
                            [kontrahent].[uwagi] = @uwagi
				--,[identyfikator_num] = @identyfikator_num
                            ,
                            [kontrahent].[czy_zgoda_pdo] = @czy_zgoda_pdo ,
                            [kontrahent].[data_zgody_pdo] = @data_zgody_pdo ,
                            [kontrahent].[odleglosc] = @odleglosc ,
                            [kontrahent].[czy_zweryfikowany_nip] = @czy_zweryfikowany_nip ,
                            [kontrahent].[data_weryfikacji_nip] = @data_weryfikacji_nip ,
                            [kontrahent].[zrodlo_weryfikacji_nip] = @zrodlo_weryfikacji_nip ,
                            [kontrahent].[nazwa_koresp] = @nazwa_koresp ,
                            [kontrahent].[uzytk_nip_id] = @uzytk_nip_id ,
                            [kontrahent].[kraj_id] = @kraj_id
                    WHERE   [kontrahent].[kontrahent_id] = @kontrahent_id
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd aktualizacji kontrahenta: '
                        + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;

                BEGIN TRY
                    UPDATE  [dbo].[dostawca]
                    SET     [dostawca].[warplat_id] = @warplat_id
				--,[klasdost_id] = @klasdost_id
                            ,
                            [dostawca].[branzysta_id] = @branzysta_id ,
                            [dostawca].[uzytk_id] = @uzytk_id
				--,[atrybutvat_id] = @atrybutvat_id
                            ,
                            [dostawca].[waluta_id] = @waluta_id ,
                            [dostawca].[bank_id] = @bank_id ,
                            [dostawca].[kontrahent_id] = @kontrahent_id ,
                            [dostawca].[platnik_id] = @platnik_id ,
                            [dostawca].[katmetdost_id] = @katmetdost_id ,
                            [dostawca].[maxwartfakt] = @maxwartfakt ,
                            [dostawca].[czy_zakpracchron] = @czy_zakpracchron ,
                            [dostawca].[kredhandtyp] = @kredhandtyp ,
                            [dostawca].[zammin] = @zammin ,
                            [dostawca].[opusthand] = @opusthand ,
                            [dostawca].[histkal] = @histkal ,
                            [dostawca].[histokr] = @histokr ,
                            [dostawca].[histpod] = @histpod ,
                            [dostawca].[margkonmies] = @margkonmies ,
                            [dostawca].[czyzabl] = @czyzabl ,
                            [dostawca].[datutw] = @datutw ,
                            [dostawca].[datmod] = @datmod ,
                            [dostawca].[kwotumo] = @kwotumo ,
                            [dostawca].[kwota_kredytu] = @kwota_kredytu
				--,[identyfikator_num] = @identyfikator_num_dst
				--,[dostawca_idn] = @dostawca_idn
                            ,
                            [dostawca].[czycert] = @czycert ,
                            [dostawca].[datcertod] = @datcertod ,
                            [dostawca].[datacertdo] = @datacertdo
				--,[dostawca_idk] = @dostawca_idk
                            ,
                            [dostawca].[podatnik_id] = @podatnik_id
                    WHERE   [dostawca].[dostawca_id] = @dostawca_id
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd aktualizacji dostawcy: '
                        + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;

                BEGIN TRY
                    UPDATE  [dbo].[adrkontr]
                    SET     [adrkontr].[powiat_id] = @powiat_id ,
                            [adrkontr].[woj_id] = @woj_id ,
                            [adrkontr].[kontrahent_id] = @kontrahent_id ,
                            [adrkontr].[kraj_id] = @kraj_id ,
                            [adrkontr].[nazwa] = @nazwa_adr ,
                            [adrkontr].[powiat] = @powiat ,
                            [adrkontr].[gmina] = @gmina ,
                            [adrkontr].[kodpoczt] = @kodpoczt ,
                            [adrkontr].[miasto] = @miasto ,
                            [adrkontr].[poczta] = @poczta ,
                            [adrkontr].[ulica] = @ulica ,
                            [adrkontr].[nrdomu] = @nrdomu ,
                            [adrkontr].[nrmieszk] = @nrmieszk ,
                            [adrkontr].[nazwa_koresp] = @nazwa_koresp
                    WHERE   [adrkontr].[adrkontr_id] = @adrkontr_id 
                END TRY
                BEGIN CATCH
                    SET @message = 'Błąd aktualizacji adresu dostawcy: '
                        + @dostawca_idn + ' [' + ERROR_MESSAGE() + ']'
                    EXEC [dbo].[sys_setstatus] -1
                    EXEC [dbo].[sys_setstring] @message
                    RETURN
                END CATCH ;
            END
        EXEC [dbo].[sys_setstatus] 0
        EXEC [dbo].[sys_setlong] @dostawca_id 
        
-- MPI #67071 -- dodanie numeru analitycznego
		if isnull(@dostawca_id,0) > 0 
		begin
		update dostawca set identyfikator_num = dostawca_idn 
		where ISNUMERIC(dostawca_idn) = 1 and identyfikator_num is null and dostawca_idn not in (select isnull(identyfikator_num,0) 
		                                                                                         from dostawca with(nolock)) and dostawca_id = @dostawca_id
		update dostawca set identyfikator_num = (select max(identyfikator_num) + 1 
		                                         from dostawca) 
		                                         where ISNUMERIC(dostawca_idn) = 1 and identyfikator_num is null and dostawca_idn in (select isnull(identyfikator_num,0) 
		                                                                                                                              from dostawca 
		                                                                                                                              with(nolock)) 
		                                                                           and dostawca_id = @dostawca_id
		end
-- MPI #67071 end        
    END


GO

