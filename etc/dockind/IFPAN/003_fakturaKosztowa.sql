ALTER TABLE dsg_ifpan_coinvoice ALTER COLUMN vat numeric(18,2);

ALTER TABLE dsg_ifpan_coinvoice ADD  zapotrzebowanie int; 

ALTER TABLE dsg_ifpan_faktura_multiple_value ALTER COLUMN FIELD_CN varchar(35);
