USE [Simple]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

alter procedure [dbo].[up_gsd_szukaj_dostawcy] @dostawcanazwa varchar(60), @nip varchar(14) , @kraj_nazwa varchar (40) , @wynik xml  out 

as 

begin


CREATE TABLE [dbo].[#wynik](
	[dostawca_nazwa] [varchar](60) NULL,
	[kraj_kod] [varchar](40) NULL,
	[ulica] [varchar](80) NULL,
	[zip] [varchar](40) NULL,
	[miasto] [varchar](40) NULL,
	[nip] [varchar](14) NULL,
	[dostawca_id] [varchar](50) NULL
) ON [PRIMARY]




	INSERT  [#wynik] 
           ([dostawca_nazwa]
           ,[kraj_kod]
           ,[ulica]
           ,[zip]
           ,[miasto]
           ,[nip]
           ,[dostawca_id])

	select  rtrim ( k.nazwa ) as dostawca_nazwa ,
		RTRIM ( kraj.kod ) as kraj_kod, 
		rtrim(k.ulica) as ulica,
		RTRIM(k.kodpoczt) as zip,
		RTRIM(k.miasto) as miasto,
		rtrim ( k.nip ) as nip , d.dostawca_id
	from kontrahent k join dostawca d on k.kontrahent_id = d.kontrahent_id 
	join kraj on k.kraj_rejpod_id = kraj.kraj_id
	where (@dostawcanazwa is  null or @dostawcanazwa = '' OR UPPER ( k.nazwa ) like '%' + UPPER ( @dostawcanazwa ) + '%' )
	 AND (@nip is  null or @nip = '' or REPLACE (k.nip , '-' , '' ) like '%' + @nip + '%')
	  AND (@kraj_nazwa is  null or @kraj_nazwa = '' or UPPER ( kraj.kod ) like '%' + UPPER ( @kraj_nazwa) + '%')


select @wynik = (
select [dostawca_nazwa],[kraj_kod],[ulica],[zip],[miasto],[nip],[dostawca_id]
from [#wynik] dostawca for xml auto, root )

drop table [#wynik]

end 


GO

