  CREATE TABLE dsg_ifpan_erp_person(
	id numeric(18,0) identity(1,1) NOT NULL,
	erp_id numeric(18,0) NOT NULL,
	organization varchar(255) NULL,
	street varchar(255) NULL,
	zip varchar(255) NULL,
	location varchar(255) NULL,
	nip varchar(255) NULL,
	country_id numeric(18,0) NULL
	)