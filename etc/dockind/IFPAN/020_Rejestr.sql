alter table dsr_project_entry alter column gross numeric (18,2);
alter table dsr_project_entry alter column net numeric (18,2);
alter table dsr_project_entry alter column vat numeric (18,2);
alter table dsr_project_entry alter column docNr varchar(50);