CREATE TABLE [dbo].[dsg_ifpan_coinvoice](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[status] [int] NULL,
	[invoice_kind] [int] NULL,
	[invoice_date] [datetime] NULL,
	[invoice_number] [varchar](50) NULL,
	[payment_date] [datetime] NULL,
	[net_amount] [numeric](18, 2) NULL,
	[vat] [numeric](18, 2) NULL,
	[gross_amount] [numeric](18, 2) NULL,
	[centra_kosztow] [int] NULL,
	[duuenr] [varchar](50) NULL,
	[sadNr] [varchar](50) NULL,
	[contract_order] [int] NULL,
	[buy_kind] [int] NULL,
	[cpvNr] [int] NULL,
	[description] [varchar](4096) NULL,
	[zapotrzebowanie] [int] NULL,
	[contractor] [numeric](18, 0) NULL,
	[acceptance] [numeric](18, 0) NULL,
	[waluta] [numeric](18, 0) NULL,
	[kwotaWwalucie] [numeric](18, 2) NULL,
	[kurs] [numeric](18, 4) NULL,
	[sposob] [numeric](18, 0) NULL,
	[addinfo] [varchar](4096) NULL,
	[chemia] [bit] NULL,
	[akcept_wydatkow] [numeric](18, 0) NULL,
	[uwagi] [varchar](4096) NULL,

);

	alter table dsg_ifpan_coinvoice add [contractornationality] [numeric](18, 0) NULL;
	alter table dsg_ifpan_coinvoice add [worker] [numeric](19, 0) NULL;
	alter table dsg_ifpan_coinvoice add [opis_towaru] [varchar](4096) NULL;
	alter table dsg_ifpan_coinvoice add [WSZYSTKIE_WYPELNIONE] [numeric](19, 0) NULL;
	alter table dsg_ifpan_coinvoice add [SEKRETARIAT] [numeric](19, 0) NULL;









CREATE TABLE dsg_ifpan_acc_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_cpv
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_invoice_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_buy_kind
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

CREATE TABLE dsg_ifpan_cost_category
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create view dsg_ifpan_founding_source as
select pr.id as ID, pr.nrIFPAN as CN, pr.nrIFPAN AS TITLE, '0' AS CENTRUM, NULL AS refValue, pr.active as available
from DSR_PROJECT as pr;
