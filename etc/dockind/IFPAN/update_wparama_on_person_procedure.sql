USE [docusafe_prod]
GO
/****** Object:  StoredProcedure [dbo].[update_wparam_on_dsoperson]    Script Date: 07/09/2012 10:30:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[update_wparam_on_dsoperson] 
@wynik numeric(10,0) out
 
as     
begin     
SET NOCOUNT ON
   
DECLARE @RC int
DECLARE @nip varchar(14)
DECLARE @result varchar
DECLARE @wparam numeric(19,0)
DECLARE @email varchar(50)
DECLARE @nazwa varchar(60)
DECLARE @nipout varchar(13)
DECLARE @regon varchar(14)
DECLARE @kodpoczt varchar(10)
DECLARE @miasto varchar(30)
DECLARE @ulica varchar(40)
DECLARE @kraj varchar(15)
DECLARE @counter int = 0

DECLARE kursor CURSOR FOR -- Deklarujemy kursor
--SELECT nip from dso_person where wparam is null and NIP is not null
(SELECT nip from DSO_PERSON where NIP is not null)
OPEN kursor 
FETCH NEXT FROM kursor
INTO @nip 
WHILE @@FETCH_STATUS = 0 
BEGIN
EXECUTE @RC = [simple].[dbo].[up_gsd_weryfikacja_dost] 
   @nip
  ,@result OUTPUT
  ,@wparam OUT
  ,@email OUTPUT
  ,@nazwa OUTPUT
  ,@nipout OUTPUT
  ,@regon OUTPUT
  ,@kodpoczt OUTPUT
  ,@miasto OUTPUT
  ,@ulica OUTPUT
  ,@kraj OUTPUT
  
if @wparam IS NOT NULL
 begin    
  UPDATE DSO_PERSON SET wparam=@wparam where NIP=@nip
  select @counter = @counter+1
end    
SET @wparam = NULL
  
FETCH NEXT FROM kursor into @nip
END
CLOSE kursor
DEALLOCATE kursor

select @wynik=@counter
    
end    
    