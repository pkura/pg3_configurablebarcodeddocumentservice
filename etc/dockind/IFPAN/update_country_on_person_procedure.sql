USE [docusafe_prod]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[update_person_country] 
@wynik numeric(10,0) out
 
as     
begin     
SET NOCOUNT ON
   
DECLARE @RC int
DECLARE @nip varchar(14)
DECLARE @result varchar
DECLARE @wparam numeric(19,0)
DECLARE @email varchar(50)
DECLARE @nazwa varchar(60)
DECLARE @nipout varchar(13)
DECLARE @regon varchar(14)
DECLARE @kodpoczt varchar(10)
DECLARE @miasto varchar(30)
DECLARE @ulica varchar(40)
DECLARE @kraj varchar(15)
DECLARE @counter int = 0
DECLARE @krajId numeric(19,0)

DECLARE kursor CURSOR FOR -- Deklarujemy kursor
SELECT nip from dso_person where NIP is not null
OPEN kursor -- otwieramy kursor
FETCH NEXT FROM kursor
INTO @nip -- umieszczamy kolejne miesiące do zmiennej @dana
WHILE @@FETCH_STATUS = 0 -- pętla kursor wykona się tyle, aż do wyczerpie wartości z zapytania
BEGIN
EXECUTE @RC = [simple].[dbo].[up_gsd_weryfikacja_dost] 
   @nip
  ,@result OUTPUT
  ,@wparam OUT
  ,@email OUTPUT
  ,@nazwa OUTPUT
  ,@nipout OUTPUT
  ,@regon OUTPUT
  ,@kodpoczt OUTPUT
  ,@miasto OUTPUT
  ,@ulica OUTPUT
  ,@kraj OUTPUT
  
if @kraj is not null   
	begin    
	set @krajId  = (select top 1 ID from dsr_country where cn=@kraj)
		UPDATE dso_person SET country=@krajId where NIP=@nip
		select @counter = @counter+1
	end    
SET @kraj = NULL
  
FETCH NEXT FROM kursor into @nip
END
CLOSE kursor
DEALLOCATE kursor

select @wynik=@counter
    
end    
    