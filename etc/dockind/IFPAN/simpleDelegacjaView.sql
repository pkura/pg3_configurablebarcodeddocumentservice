CREATE VIEW advances_view
AS 
 select m_del.DOCUMENT_ID AS DEL_ID, del_costs.CURRENCY_COST AS CURRENCY_AMOUNT,
		curr.CN AS CURRENCY
	from dsg_ifpan_delegacja_multiple_value m_del, dsg_ifpan_delegate_costs del_costs,
		 dsg_currency curr
	where del_costs.ID = m_del.FIELD_VAL AND m_del.FIELD_CN like 'DELEGATE_COSTS' AND
			curr.ID = del_costs.WALUTA
			 
CREATE VIEW project_view
AS 
select m_del.DOCUMENT_ID AS DEL_ID, del_found.CN AS CN, del_found.TITLE AS TITLE
	from dsg_ifpan_delegacja_multiple_value m_del, dsg_ifpan_delegate_costs del_costs, 
		 dsg_ifpan_founding_source del_found
	where del_costs.ID = m_del.FIELD_VAL AND m_del.FIELD_CN like 'DELEGATE_COSTS' AND
		del_found.ID = del_costs.platnik

<!-- przekazac info o znaczeniu ID TRIP_TYPE, ENTRY_PAY (Spos�b op�aty wpisowego), nadplata, zwrot -->
CREATE VIEW delegation_view
AS 
select del.DOCUMENT_ID AS ID, doc.CTIME AS CTIME, usr.FIRSTNAME + ' ' + usr.LASTNAME AS WORKER, div.NAME AS WORKER_DIVISION, 
		del.TRIP_TYPE AS TRIP_TYPE, ct.CN AS COUNTRY, del.CITY AS CITY, del.ORGANIZER_NAME AS ORGANIZER_NAME, 
		del.START_DATE AS START_DATE, del.FINISH_DATE AS FINISH_DATE, del.trip_purpose AS trip_purpose,
		del.entry AS ENTRY_PAY, del.POSIADANA AS TOTAL_AMOUNT, del.nadplata AS TURN_WAY, del.TRIP_TYPE AS LEAVE_TYPE,
		del.rozliczenie_ifpan AS TURN_AMOUNT, del.zwrot AS TURN_TO_WORKER_WAY, del.rozliczenie_delegowany AS TURN_TO_WORKER_AMOUNT
	from dsg_ifpan_delegacja del, ds_document doc, ds_user usr, DS_DIVISION div, DSR_COUNTRY ct
	where del.document_id = doc.id AND usr.ID = del.WORKER AND div.ID = del.WORKER_DIVISION  
			 AND del.COUNTRY = ct.ID

<!--
1.    Numer delegacji

2.    Data dokumentu

3.    Imi� i nazwisko delegowanego

4.    Dzia�/kom�rka DELEGOWANEGO

5.    Typ wyjazdu

6.    Kraj delegacji

7.    Miasto delegacji

8.    Nazwa firmy do kt�rej jest delegacja

9.    Termin wyjazdu

10.   Termin przyjazdu

11.   Cel wyjazdu

12.   Spos�b op�aty wpisowego (ENTRY_PAY)

13.   Ca�kowita kwota delegacji (jako pole: Kwota og�em �rodk�w dysponowanych TOTAL_AMOUNT) 

14.   Zwrot do IFPAN-forma rozliczenia (TURN_WAY)

15.   Kwota zwrotu (TURN_AMOUNT)

17.   Forma zwrotu delegowanemu (TURN_TO_WORKER_WAY)

18.   Kwota zwrotu delegowanemu (TURN_TO_WORKER_AMOUNT)

19.   Typ urlopu (LEAVE_TYPE)


Wysoko�� koszt�w pokrytych przez organizatora - brak takiej informacji
 Typ urlopu - pokrywa sie z typem wyjazdu 
 Projekt, kwota i waluta zaliczki jako osobny widok powiazany przed id -> document_id
-->
			 
