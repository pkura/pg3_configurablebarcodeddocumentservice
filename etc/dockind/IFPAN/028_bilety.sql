alter table dsg_ifpan_bilety_costs add WALUTA numeric(18,0);
alter table dsg_ifpan_bilety_costs add AMOUNT numeric(18,2);
alter table dsg_ifpan_bilety_costs add CURRENCY_COST numeric(18,2);
alter table dsg_ifpan_bilety_costs add COST_AMOUNT numeric(18,2);
alter table dsg_ifpan_bilety_costs drop column koszt;
alter table dsg_ifpan_bilety_costs add projectManag varchar(60);