USE [wb_test]
GO

/****** Object:  StoredProcedure [dbo].[up_gsd_DelegacjaPrzelew]    Script Date: 11/05/2012 15:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE procedure [dbo].[up_gsd_DelegacjaPrzelew]

@user_id  numeric(10,0) ,
@pracownik_nrewid numeric(10,0) ,
@numerdok char (20) ,
@data_rej date ,
@datadok date ,
@kwota money , 
@kwotawalbaz money ,
@kurs money ,
@waluta_id bigint ,
@czy_krajowa bigint ,
@opis varchar(60) ,
@result varchar(max) out


as 
begin 

-- @czy_krajowa = 1 - delegacja krajowa , 2 delegacja zagranicza

if @czy_krajowa not in (1,2)
begin 
	set @result = 'Parametr poza dopuszczalnym zakresem'
	return
end 

declare @podokrrozl_id  numeric(10,0)
declare @stronarozrach_id numeric(10,0) 
declare @ddfin numeric(10,0)
declare @ddfin_typ_id int 
declare @czy_walutowy int


if @czy_krajowa = 1
begin 
select @ddfin_typ_id = 3
select @czy_walutowy = 0
end 

if @czy_krajowa = 2
begin 
select @ddfin_typ_id = 4
select @czy_walutowy = 0
end 


select @stronarozrach_id = stronarozrach_id from fk_stronarozrach where rodzstrony_id = 4 and obiekt_idn  = @pracownik_nrewid


 --wyznaczam okres rozliczeniowy

exec sp_okr_getpodokr_id @data_rej , 2
select @podokrrozl_id = typ_long from vzmienne
select @user_id = 33


begin try

INSERT INTO [fk_ddfin]
           ([waluta_id] 
           ,[podokrrozl_id]
           ,[ddfin_typ_id]
           ,[uzytk_id]
           ,[tytul_id]
           ,[maska_id]
           ,[komorka_id]
           ,[ddfin_idm]
           ,[stronarozrach_id]
           ,[datdok]
           ,[datoper]
           ,[datpla]
           ,[rodzaj_operacji]
           ,[kwota]
           ,[kurs]
           ,[kwotawalbaz]
           ,[status]
           ,[czy_waluta]
           ,[sposzapl]
           ,[czy_zadekretowany]
           ,[dokobcy]
           ,[opis] )
     VALUES
           ( @waluta_id
           , @podokrrozl_id
           , @ddfin_typ_id
           , @user_id
           , 5
           , 113
           , 2
           , @numerdok
           , @stronarozrach_id
           , @datadok 
           , @data_rej
           , @datadok
           , 3
           , @kwota
           , @kurs
           , @kwotawalbaz
           , 0
           , @czy_walutowy
           , 1 
           , 0
           , @numerdok
           , @opis )
 
end try
           
         	BEGIN CATCH
			set @result = 'Błšd dodania dokumentu '+ '  ' + @numerdok +  '  ' +'['+ ERROR_MESSAGE() + ']'
					  +  convert (varchar (4) , ERROR_line()  )
			return
			END CATCH;  
           
           
select @ddfin = @@IDENTITY                      

begin try
INSERT INTO [fk_ddfin_poz]
           ([ddfin_id]
           ,[kwota]
           ,[kurs]
           ,[kwotwalbaz])
     VALUES
           ( @ddfin
           , @kwota
           , @kurs
           , @kwotawalbaz )
 
end try
           
         	BEGIN CATCH
			set @result = 'Błšd dodania dokumentu '+ '  ' + @numerdok +  '  ' +'['+ ERROR_MESSAGE() + ']'
					  +  convert (varchar (4) , ERROR_line()  )
			return
			END CATCH;  
           

end 




GO

