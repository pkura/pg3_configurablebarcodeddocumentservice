create view ds_document_acceptance_view
as 
select da.id as id, da.acceptanceCn as Cn, da.username as title, '0' AS CENTRUM, da.document_id as RefValue, '1' as available from DS_DOCUMENT_ACCEPTANCE as da;

alter table dsg_ifpan_delegacja add acceptance numeric(18,0);
alter table dsg_ifpan_coinvoice add acceptance numeric(18,0);
alter table dsg_ifpan_contract add acceptance numeric(18,0);
alter table dsg_ifpan_zapotrz add acceptance numeric(18,0);