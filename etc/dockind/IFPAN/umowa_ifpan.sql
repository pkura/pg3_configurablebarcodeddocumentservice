create table dbo.dsg_ifpan_dockind_umowa(
    ID numeric(18,0) identity(1,1) NOT NULL,
	DOCUMENT_ID numeric(18,0),
	status numeric(18,0),
	nr_sprawy varchar(30),
	znacznik numeric(18,0),
	zapotrzebowanie numeric(18,0),
	dostawca numeric(18,0),
	tryb_postepowania numeric(18,0),
	rodzaj_zamowienia numeric(18,0),
	wartosc_szacowana_pln numeric(18,2),
	suma_faktur numeric(18,2),
	prog_kwotowy numeric(18,2),
	wart_szacowana_eur numeric(18,2),
	przekazanie bit
);


