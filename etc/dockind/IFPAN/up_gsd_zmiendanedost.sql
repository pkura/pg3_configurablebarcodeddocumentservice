alter procedure up_gsd_zmiendanedost 
 
@wparam varchar(15)  
,@nazwa varchar(40)
,@ulica varchar(40)
,@kodpoczt varchar(10)
,@miasto varchar(40)
,@kraj_cn varchar(10)
,@result varchar(max) out
 
as 
begin
 
if not exists ( select top 1 * from kontrahent k join dostawca d on k.kontrahent_id = d.dostawca_id where dostawca_id = @wparam )
 
       begin 
             select  @result = 'Nie istnieje dostawca o takim ....'
             return 5
       end                                     
                                               
begin transaction;
--zmiana nazwy
if @nazwa is  null or @nazwa = ''
       begin 
             select  @result = 'Parametr @nazwa nie moze byc pusty '
             rollback;
             return 10
       end 
 
else 
 
begin
 
       begin try
       update kontrahent set nazwa = @nazwa
       from kontrahent k join dostawca d on k.kontrahent_id = d.kontrahent_id
       where dostawca_id = @wparam
       end try
       
       
       begin catch    
       select @result = 'B31d zmoany nazwy dostawcy '  +'['+ ERROR_MESSAGE() + ']'    
              + ' ' +   convert (varchar (4) , ERROR_line()  )    
       rollback;
       return 11  
       end catch ;   
 
end 
 
 
--zmiana adresu - ulicy
if @ulica is  null or @ulica = ''
       begin 
             select  @result = 'Parametr @ulica nie moze byc pusty '
             rollback;
             return 15
       end 
 
else 
 
begin
 
       begin try
             update kontrahent set ulica = @ulica
             from kontrahent k join dostawca d on k.kontrahent_id = d.kontrahent_id
             where dostawca_id = @wparam
       end try
 
       begin catch    
       select @result = 'B31d zmoany nazwy ulicy '  +'['+ ERROR_MESSAGE() + ']'    
              + ' ' +   convert (varchar (4) , ERROR_line()  ) 
       rollback;         
       return 16  
       end catch ;  
        
end
 
--zmiiana kod-poczt
if @kodpoczt is  null or @kodpoczt = ''
       begin 
             select  @result = 'Parametr @kodpoczt nie moze byc pusty '
             rollback;
             return 20
       end 
 
else 
 
       begin
 
             begin try
                    update kontrahent set kodpoczt = @kodpoczt
                    from kontrahent k join dostawca d on k.kontrahent_id = d.kontrahent_id
                    where dostawca_id = @wparam
             end try
 
             begin catch    
             select @result = 'B31d zmiany kodu pocztowe '  +'['+ ERROR_MESSAGE() + ']'    
                     + ' ' +   convert (varchar (4) , ERROR_line()  )    
             rollback;
             return 21  
             end catch ;   
       end
 


--zmiiana miasto
if @miasto is  null or @miasto = ''
       begin 
             select  @result = 'Parametr @miasto nie moze byc pusty '
             rollback;
             return 30
       end 
 
else 
 
 
             begin try
                    update kontrahent set miasto = @miasto
                    from kontrahent k join dostawca d on k.kontrahent_id = d.kontrahent_id
                    where dostawca_id = @wparam
             end try
 
             begin catch    
             select @result = 'B31d zmiany kodu pocztowe '  +'['+ ERROR_MESSAGE() + ']'    
                     + ' ' +   convert (varchar (4) , ERROR_line()  )    
             rollback;
             return 31  
             end catch ;   
 

--zmiiana kraj
if @kraj_cn is null or @kraj_cn = ''
       begin 
             select  @result = 'Parametr @miasto nie moze byc pusty '
             rollback;
             return 40
       end 
 
else 
 

             begin try
                    update kontrahent set kraj_rejpod_id = (select top 1 kraj_id from kraj where kod = @kraj_cn)
                    from kontrahent k 
                    join dostawca d on k.kontrahent_id = d.kontrahent_id
                    where dostawca_id = @wparam
             end try
 
             begin catch    
             select @result = 'B31d zmiany kodu pocztowe '  +'['+ ERROR_MESSAGE() + ']'    
                     + ' ' +   convert (varchar (4) , ERROR_line()  )    
             rollback;
             return 41  
             end catch ;   
             
commit;
end