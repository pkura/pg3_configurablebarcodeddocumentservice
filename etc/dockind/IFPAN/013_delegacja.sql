alter table dsg_ifpan_delegate_costs add waluta numeric(18,0);
alter table dsg_ifpan_delegate_costs add CURRENCY_COST numeric(18,2);
alter table dsg_ifpan_delegate_costs add projectManag varchar(60);

alter table dsg_ifpan_advance add ZL_AMOUNT numeric(18,2);
alter table dsg_ifpan_advance add RATE numeric(18,2);

alter table ds_acceptance_division alter column code varchar(50);
alter table ds_acceptance_condition alter column cn varchar(50);

alter table dsg_ifpan_delegate_costs add COST_PLN numeric(18,2);
alter table dsg_ifpan_delegate_costs add nrBlocade varchar(20);
alter table dsg_ifpan_delegate_costs add ADVANCE_KIND varchar(30);

alter table dsg_ifpan_delegacja add WYDANA numeric(18,2);
alter table dsg_ifpan_delegacja add POSIADANA numeric(18,2);

alter table ds_document_acceptance alter column acceptanceCn varchar(150); 