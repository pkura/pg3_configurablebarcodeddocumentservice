CREATE TABLE dsg_ifpan_contract
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	DOCUMENT_ID numeric(18,0),
	contract_nr varchar(50),
	status numeric(18,0),
	realizat_date datetime,
	contract_val numeric(18,2),
	centra_kosztow int,
	zapotrzebowanie int
)

CREATE TABLE dsg_ifpan_contract_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(20) NOT NULL,
    FIELD_VAL varchar(100) NULL
);