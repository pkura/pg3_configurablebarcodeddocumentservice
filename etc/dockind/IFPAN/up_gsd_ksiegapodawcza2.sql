USE [wb_test]
GO

/****** Object:  StoredProcedure [dbo].[up_gsd_ksiegapodawcza2]    Script Date: 11/05/2012 15:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





  
  
CREATE procedure [dbo].[up_gsd_ksiegapodawcza2]  
  
  
@dok_idm varchar (20) ,    
@dokobcy varchar (80) ,    
@dostawca_id numeric (10,0) ,    
@komorka numeric (10,0) ,    
@netto money ,    
@vat money ,    
@data_rej date ,    
@data_faktury date ,    
@termin_plat date ,    
@result varchar(max) out ,
--gsd 2012-05-29 - Bo tak
@symbolwaluty char(3) , --symbol waluty
@kurs_waluty money ,
@nettowalbaz money ,
@vatwalutbaz money 



    
as     
begin     
--wkładanie dokumnetu ze statusemm do edycji wymaga wartości null w poli zatw_id    
DECLARE @podokrrozl_id numeric(10,0)    
DECLARE @brutto money    
DECLARE @dzien datetime     
DECLARE @dostfakt_id numeric(10,0)    
DECLARE @kontrahent_id numeric(10,0)    
DECLARE @dostawca_idn  varchar (10)     
DECLARE @warplat numeric (10,0)     
DECLARE @adrkontr_id numeric (10,0)  
-- ani 2012-02-28
DECLARE @nip char (13)  
DECLARE @nazwa varchar (60)
DECLARE @gmina varchar (20)
DECLARE @kodpoczt varchar (10) 
DECLARE @miasto varchar (30) 
DECLARE @ulica varchar (40) 
DECLARE @nrdomu varchar (10)
DECLARE @nrmieszk varchar (6) 
DECLARE @kraj_id T_id
DECLARE @woj_id T_id
DECLARE @powiat char(20)
DECLARE @uzytk_id T_id
DECLARE @prev_dostfakt_id T_id
DECLARE @poczta varchar(30) 
-- ani 2012-02-28 
DECLARE  @rejdokprzych_id numeric (10) 
declare @warplatpoz int  
declare @tabkurs_id int 
declare @waluta_id int



select @uzytk_id = 41
    
if @dok_idm  is null or @dok_idm = ''    
 begin    
  set @result = 'Paramert @dok_idm nie może  być pusty '     
  return    
 end    
    
if @dokobcy  is null or @dokobcy = ''    
 begin    
  set @result = 'Paramert @dokobcy nie może  być pusty '     
  return    
 end    
    
if @dostawca_id  is null 
 begin    
  set @result = 'Paramert @dostawca_id nie może  być pusty '     
  return    
 end    
     
if @komorka  is null    
 begin    
  set @result = 'Paramert @komorka nie może  być pusty '     
  return    
 end    
    
if @netto  is null or @netto = ''    
 begin    
  set @result = 'Paramert @netto nie może  być pusty '     
  return    
 end    

if @data_faktury  is null or @data_faktury = ''    
 begin    
  set @result = 'Paramert @data_faktury nie może  być pusty '     
  return    
 end    
     
if @termin_plat  is null or @termin_plat = ''    
 begin    
  set @result = 'Paramert @termin_plat nie może  być pusty '     
  return    
 end    
   

if not exists (select  top 1 * from waluta where  symbolwaluty = @symbolwaluty ) --or @waluta_id  is null
 begin    
  set @result = 'Nie zostala zdefiniowala waluta o takim symbolu w systemie SIMPLE.ERP'    
  return    
 end    

select @waluta_id = waluta_id from waluta where symbolwaluty = @symbolwaluty


select @dostawca_idn = dostawca_idn
from dostawca   
where dostawca_id = @dostawca_id  
    
select @kontrahent_id = kontrahent_id
from dostawca   
where dostawca_id = @dostawca_id

-- ani 2012-02-28 
select @nip = nip,  
       @nazwa = nazwa, 
       @gmina = gmina,
       @kodpoczt = kodpoczt,
       @miasto =miasto,
       @ulica = ulica,
       @nrdomu = nrdomu,
       @nrmieszk = nrmieszk,
       @kraj_id = kraj_id,
       @woj_id = woj_id,
       @powiat = powiat,
       @poczta = poczta
from kontrahent
where kontrahent_id = @kontrahent_id

select @adrkontr_id = adrkontr_id
from adrkontr 
where kontrahent_id = @kontrahent_id 

exec sp_dzk_zapisz_dostfakt  @nip, @nazwa, @gmina, @kodpoczt, @miasto, @ulica, @nrdomu, @nrmieszk, @kraj_id, @woj_id, @powiat, 
                             @uzytk_id, @dostawca_id, @adrkontr_id, null , @dostawca_idn, @poczta
-- ani 2012-02-28 


-- brutto = netto + vat
    


    
--wyznaczam okres rozliczeniowy    
    
exec sp_okr_getpodokr_id @data_faktury ,2    
  
select @podokrrozl_id = typ_long   
from vzmienne    
    
--wyznaczam warunki płatności    
    
if DATEDIFF (DAY , @data_faktury , @termin_plat ) = 0     
begin    
 select @warplat = 1      
end    
else      
 select @warplat = 24      
    
--wyliczam liczbe dni 
    
DECLARE @liczbadni INT     
select  @liczbadni =  DATEDIFF (DAY , @data_faktury , @termin_plat )    

-- ani 2012-02-28     
select @dostfakt_id = dostfakt_id    
from dostfakt   
where dostawca_id = @dostawca_id    
-- ani 2012-02-28 
   
-- gsd 2012-05-29 dodaje obsług wielu walut - w tej skcji tylko PLN   
if @waluta_id = 1 
begin    
    
    select @brutto = @netto + @vat
    
    
	BEGIN TRY    
	INSERT INTO [ot_rejdokprzych]    
			   ([rodzstrony_id]    
			   ,[warplat_id]    
			   ,[uzytk_id]    
			   ,[last_uzytk_id]    
			   ,[akt_uzytk_id]    
			   ,[dostfakt_id]    
			   ,[adrkontr_id]    
			   ,[odpowiedzialny_id]    
			   ,[maska_id]    
			   ,[waluta_id]    
			   ,[typrejprzych_id]    
			   ,[komorka_id]    
			   ,[podokrrozl_id]    
			   ,[oddzial_id]    
			   ,[dok_idm]    
			   ,[dokobcy]    
			   ,[typdok]    
			   ,[datdok]    
			   ,[datoper]    
			   ,[datrej]    
			   ,[datutw]    
			   ,[datplatn]    
			   ,[status]    
			   ,[stan]    
			   ,[kurs]    
			   ,[netto]    
			   ,[vat]    
			   ,[brutto]    
			   ,[nettowalbaz]    
			   ,[vatwalbaz]    
			   ,[bruttowalbaz]    
			   ,[czykorekta]    
			   ,[strona_id]    
			   ,[pracownik_id] )    
		 VALUES (    
	-- ani 2012-02-24 było 1  
				 2    
	-- ani 2012-02-24              
			   , @warplat    
			   , @uzytk_id    
			   , @uzytk_id    
			   , @uzytk_id    
			   , @dostfakt_id    
			   , @adrkontr_id    
			   , @uzytk_id    
			   , 78 -- zmiennic na maskę z rejestru    
			   , 1  -- waluta_id  
			   , 1  -- typrejprzych_id z  ot_typ_rejdokprzych, MPI #59172  
			   , @komorka    
			   , @podokrrozl_id    
			   , 2    
			   , @dok_idm    
			   , @dokobcy    
			   , 5    
			   , @data_faktury    
			   , @data_rej    
			   , @data_rej    
			   , @data_rej    
			   , @termin_plat    
			   , 0    
			   , 0    
			   , 1.0    
			   , @netto     
			   , @vat    
			   , @brutto    
			   , @netto    
			   , @vat    
			   , @brutto    
			   , 0    
			   , @dostawca_id     
			   , @uzytk_id )    
	               
			 end try      
	               
	 BEGIN CATCH    
	  set @result = 'Błąd dodania dokumentu '+ '  ' + @dokobcy +  '  ' +'['+ ERROR_MESSAGE() + ']'    
		  +  convert (varchar (4) , ERROR_line()  )    
	  return    
	 END CATCH;    
	     
	--ddokładanie warunków płatności    
	--czytam id dodanedo dokumentu      
	     
	   
	select @rejdokprzych_id = @@IDENTITY    
	select @warplatpoz     

	BEGIN TRY    
	INSERT INTO [warplatrejdokprzych]    
			   ([rejdokprzych_id]    
			   ,[warplatpoz]    
			   ,[sposzapl]    
			   ,[kwota]    
			   ,[terplat]    
			   ,[typplat]    
			   ,[liczdni]    
			   ,[prockwot]    
			   ,[kurs]    
			   ,[kwotwalbaz]    
			   ,[czy_korekta_kursu]    
			   ,[czyproc] )    
	 select     
			 @rejdokprzych_id ,    
			 wp.warplatpoz_id ,     
			 wp.sposzaply ,    
			 @netto + @vat ,    
			 @termin_plat ,    
			 wp.typplat ,    
			 @liczbadni ,    
			 wp.prockwot ,    
			 1.0 ,     
			 0.0 ,    
			 0 ,    
			 wp.czyproc     
	 from warplat w join warplatpoz wp on w.warplat_id = wp.warplat_id    
	 where w.warplat_id = @warplat     
	    
	 end try  
	 
	               
	 BEGIN CATCH    
	  set @result = 'Błąd dodania dokumentu '+ '  ' + @dokobcy +  '  ' +'['+ ERROR_MESSAGE() + ']'    
		  +  convert (varchar (4) , ERROR_line()  )    
	  return    
	 END CATCH;    
    
end     

select @tabkurs_id = 1

--select @nettowalbaz = ROUND(  @netto * @kurs_waluty , 2 )
--select @vatwalutbaz = ROUND( @vat * @kurs_waluty, 2 )



select @brutto = @netto + @vat
     
if @waluta_id <> 1
begin 

	BEGIN TRY    
	INSERT INTO [ot_rejdokprzych]    
			   ([rodzstrony_id]    
			   ,[warplat_id]    
			   ,[uzytk_id]    
			   ,[last_uzytk_id]    
			   ,[akt_uzytk_id]    
			   ,[dostfakt_id]    
			   ,[adrkontr_id]    
			   ,[odpowiedzialny_id]    
			   ,[maska_id]    
			   ,[waluta_id]    
			   ,[typrejprzych_id]    
			   ,[komorka_id]    
			   ,[podokrrozl_id]    
			   ,[oddzial_id]    
			   ,[dok_idm]    
			   ,[dokobcy]    
			   ,[typdok]    
			   ,[datdok]    
			   ,[datoper]    
			   ,[datrej]    
			   ,[datutw]    
			   ,[datplatn]    
			   ,[status]    
			   ,[stan]    
			   ,[kurs]    
			   ,[netto]    
			   ,[vat]    
			   ,[brutto]    
			   ,[nettowalbaz]    
			   ,[vatwalbaz]    
			   ,[bruttowalbaz]    
			   ,[czykorekta]    
			   ,[strona_id]    
			   ,[pracownik_id]
			   ,[tabkurs_id])    
		 VALUES (    
	-- ani 2012-02-24 było 1  
				 2    
	-- ani 2012-02-24              
			   , @warplat    
			   , @uzytk_id    
			   , @uzytk_id    
			   , @uzytk_id    
			   , @dostfakt_id    
			   , @adrkontr_id    
			   , @uzytk_id    
			   , 78 -- zmiennic na maskę z rejestru    
			   , @waluta_id  -- waluta_id  
			   , 2  -- typrejprzych_id z  ot_typ_rejdokprzych, MPI #59172  
			   , @komorka    
			   , @podokrrozl_id    
			   , 2    
			   , @dok_idm    
			   , @dokobcy    
			   , 5    
			   , @data_faktury    
			   , @data_rej    
			   , @data_rej    
			   , @data_rej    
			   , @termin_plat    
			   , 0    
			   , 0    
			   , @kurs_waluty   
			   , @netto     
			   , @vat    
			   , @brutto    
			   , @nettowalbaz    
			   , @vatwalutbaz    
			   , round ( @nettowalbaz +  @vatwalutbaz , 2 )   
			   , 0    
			   , @dostawca_id     
			   , @uzytk_id 
			   , @tabkurs_id )    
	               
			 end try      
	               
	 BEGIN CATCH    
	  set @result = 'Błąd dodania dokumentu '+ '  ' + @dokobcy +  '  ' +'['+ ERROR_MESSAGE() + ']'    
		  +  convert (varchar (4) , ERROR_line()  )    
	  return    
	 END CATCH;    
	     
	--ddokładanie warunków płatności    
	--czytam id dodanedo dokumentu      
	     
	  
	select @rejdokprzych_id = @@IDENTITY     
	select @warplatpoz     

	BEGIN TRY    
	INSERT INTO [warplatrejdokprzych]    
			   ([rejdokprzych_id]    
			   ,[warplatpoz]    
			   ,[sposzapl]    
			   ,[kwota]    
			   ,[terplat]    
			   ,[typplat]    
			   ,[liczdni]    
			   ,[prockwot]    
			   ,[kurs]    
			   ,[kwotwalbaz]    
			   ,[czy_korekta_kursu]    
			   ,[czyproc] )    
	 select     
			 @rejdokprzych_id ,    
			 wp.warplatpoz_id ,     
			 wp.sposzaply ,    
			 round ( @nettowalbaz +  @vatwalutbaz , 2 )    ,    
			 @termin_plat ,    
			 wp.typplat ,    
			 @liczbadni ,    
			 wp.prockwot ,    
			 1.0 ,     
			 0.0 ,    
			 0 ,    
			 wp.czyproc     
	 from warplat w join warplatpoz wp on w.warplat_id = wp.warplat_id    
	 where w.warplat_id = @warplat     
	    
	 end try  
	 
	               
	 BEGIN CATCH    
	  set @result = 'Błąd dodania dokumentu '+ '  ' + @dokobcy +  '  ' +'['+ ERROR_MESSAGE() + ']'    
		  +  convert (varchar (4) , ERROR_line()  )    
	  return    
	 END CATCH;    


end      
     
     
     
     
     
end    
    




GO

