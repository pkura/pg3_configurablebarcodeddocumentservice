CREATE TABLE [dbo].[dsg_ifpan_zapotrz2](
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[STATUS] [numeric](19, 0) NULL,
	[DS_USER] [numeric](19, 0) NULL,
	[ITEM_DESCRIPTION] [varchar](1000) NULL,
	[DETAIL_ITEM_DESCRIPTION] [varchar](1000) NULL,
	[PURCHASE_REASON] [varchar](1000) NULL,
	[CONTRACTOR_REASON] [varchar](1000) NULL,
	[REMARKS] [varchar](1000) NULL,
	[CUSTOMS] [bit] NULL,
	[ELEMENT] [bit] NULL,
	[SUGG_BRUTTO] [numeric](18, 2) NULL,
	[SUG_CONTRACTOR] [numeric](19, 0) NULL,
	[CONTRACTORNATIONALITY] [numeric](19, 0) NULL,
	[centra_kosztow] [numeric](19, 0) NULL,
	[PURCHASE_TYPE] [numeric](19, 0) NULL,
	[CHEMIA] [bit] NULL,
	[ZRODLA_FIN] [varchar](250) NULL,
	[ACCEPTED] [bit] NULL,
); 

alter table dsg_ifpan_zapotrz2 add [CASENUMBER2] [numeric](19, 0) NULL;
alter table dsg_ifpan_zapotrz2 add [DAMEND_NUMBER2] [varchar](249) NULL;
alter table dsg_ifpan_zapotrz2 add [DEMAND_NUMBER2] [varchar](249) NULL;
alter table dsg_ifpan_zapotrz2 add [ACCEPTED1] [bit] NULL;
alter table dsg_ifpan_zapotrz2 add [DSG_CENTRUM_KOSZTOW_FAKTURY] [numeric](19, 0) NULL;

CREATE TABLE [dbo].[dsg_ifpan_zapotrz2_multiple_value](
	[document_id] [numeric](18, 0) NULL,
	[field_cn] [varchar](150) NULL,
	[field_val] [varchar](50) NULL
);