CREATE TABLE [dsg_ic_centra_report](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[centrum_id] [numeric](18, 0) NOT NULL,
	[centrum_name] [varchar](100) NOT NULL,
	[kwota_zaakceptowane] [float] NOT NULL,
	[kwota_niezaakceptowane] [float] NOT NULL,
	[waluta] [varchar](50) NOT NULL
);