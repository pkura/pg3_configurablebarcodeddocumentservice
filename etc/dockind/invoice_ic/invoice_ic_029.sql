
CREATE TABLE [dsg_vat_rate](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[value] [tinyint] NOT NULL,
	[symbol] [varchar](50) NOT NULL,
	[date_from] [datetime] NULL,
	[date_to] [datetime] NULL,

    CONSTRAINT [PK_dsg_vat_rate] PRIMARY KEY CLUSTERED ([id] ASC)
)

INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (22,	'A',	'1900-01-01 00:00:00.000',	'2010-12-31 00:00:00.000');
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (23,	'A',	'2011-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (7,	'B',	'1900-01-01 00:00:00.000',	'2010-12-31 00:00:00.000');
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (8,	'B',	'2011-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (0,	'C',	'1900-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (17,	'D',	'1900-01-01 00:00:00.000',	'2010-12-31 00:00:00.000');
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (5,	'D',	'2011-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (0,	'E',	'1900-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (3,	'F',	'1900-01-01 00:00:00.000',	'2010-12-31 00:00:00.000');
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (0,	'F',	'2011-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (0,	'N',	'1900-01-01 00:00:00.000',	NULL);
INSERT INTO DSG_VAT_RATE(value, symbol, date_from, date_to) values (0,	'G',	'1900-01-01 00:00:00.000',	NULL);

CREATE TABLE [dsg_vat_entry](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[vat_symbol] [varchar](10) NOT NULL,
	[vat] [int] NOT NULL,
	[vat_amount] [numeric](18, 2) NOT NULL,
	[netto_amount] [numeric](18, 2) NOT NULL,
	[costs_vat_amount] [numeric](18, 2) NOT NULL,
	[costs_netto_amount] [numeric](18, 2) NOT NULL,
	[investment] [bit] NOT NULL,

    CONSTRAINT [PK_dsg_vat_entry] PRIMARY KEY CLUSTERED ([id] ASC)
)
