CREATE TABLE [dsg_contract_ic](
	[document_id] [numeric](19, 0) NOT NULL,
	[type] [int] NULL,
	[subtype] [int] NULL,
    CONSTRAINT [PK_dsg_contract_ic] PRIMARY KEY CLUSTERED ([document_id] ASC)
 )

