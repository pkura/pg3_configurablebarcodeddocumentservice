create table [DSG_INVOICE_INFO](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[lineNumber] [int] NULL,
	[status] [int] NULL,
	[creatingUser] [varchar](62) NOT NULL,
	[ctime] [datetime] NOT NULL,
	[invoiceNumber] [varchar](50) NULL,
	[amountBrutto] [numeric](19, 0) NULL,
	[vat] [numeric](19, 0) NULL,
	[nip] [varchar](50) NULL,
	[origAmountBrutto] [numeric](19, 0) NULL,
	[errorInfo] [varchar](100) NULL,
	[documentId] [numeric](19, 0) NULL,
	[invDate] [datetime] NULL
);