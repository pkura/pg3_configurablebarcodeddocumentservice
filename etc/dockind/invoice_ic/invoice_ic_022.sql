alter TABLE DSG_INVOICE_IC
	ALTER COLUMN kwota_netto numeric(16,2);
alter TABLE DSG_INVOICE_IC
	ALTER COLUMN vat numeric(16,2);

alter TABLE DSG_SAD
	ALTER COLUMN kwota_netto numeric(16,2);
alter TABLE DSG_SAD
	ALTER COLUMN vat numeric(16,2);
alter TABLE DSG_SAD
	ALTER COLUMN kwota_brutto numeric(16,2);