CREATE TABLE [dsg_currency](
	[id] [int] PRIMARY KEY IDENTITY(10000,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] bit default 1
);

GO

SET IDENTITY_INSERT [dsg_currency] ON

INSERT INTO [dsg_currency] (id, cn, title) values(10, 'EUR','EUR - EU euro');
INSERT INTO [dsg_currency] (id, cn, title) values(50, 'GBP','GBP - Funt szterling');
INSERT INTO [dsg_currency] (id, cn, title) values(80, 'JPY','JPY - Japonia jen');
INSERT INTO [dsg_currency] (id, cn, title) values(290, 'PLN','PLN - Z�oty polski');
INSERT INTO [dsg_currency] (id, cn, title) values(30, 'USD','USD - Dolar ameryka�ski');
INSERT INTO [dsg_currency] (id, cn, title) values(330, 'SKK','SKK - Korona s�owacka');
INSERT INTO [dsg_currency] (id, cn, title) values(150, 'CZK','CZK - Korona czeska');
INSERT INTO [dsg_currency] (id, cn, title) values(350, 'UAH','UAH - Hrywna Ukraina');
INSERT INTO [dsg_currency] (id, cn, title) values(351, 'LTL','LTL - Lit Litwa');
INSERT INTO [dsg_currency] (id, cn, title) values(3510, 'LVL','LVL - �at �otwa');
INSERT INTO [dsg_currency] (id, cn, title) values(352, 'CNY','CNY - Juan Chiny');
INSERT INTO [dsg_currency] (id, cn, title) values(353, 'NOK','NOK - norweska korona');
INSERT INTO [dsg_currency] (id, cn, title) values(354, 'CHF','CHF - frank szwajcarski');
INSERT INTO [dsg_currency] (id, cn, title) values(355, 'KUNA','KUNA - Kuna chorwacka');
INSERT INTO [dsg_currency] (id, cn, title) values(356, 'BYR','BYR - Rubel Bia�oru�');
INSERT INTO [dsg_currency] (id, cn, title) values(357, 'BAM','BAM - Marka transferowa Bo�nia i Hercegowina');
INSERT INTO [dsg_currency] (id, cn, title) values(358, 'BGN','BGN - Lew Bu�garia');
INSERT INTO [dsg_currency] (id, cn, title) values(359, 'DKK','DKK - Korona du�ska');
INSERT INTO [dsg_currency] (id, cn, title) values(360, 'ISK','ISK - Korona islandzka');
INSERT INTO [dsg_currency] (id, cn, title) values(361, 'MKD','MKD - Denar macedo�ski');
INSERT INTO [dsg_currency] (id, cn, title) values(362, 'MDL','MDL - Lej mo�dawski');
INSERT INTO [dsg_currency] (id, cn, title) values(363, 'RUB','RUB - Rubel Rosja');
INSERT INTO [dsg_currency] (id, cn, title) values(364, 'RON','RON - Lej Rumunia');
INSERT INTO [dsg_currency] (id, cn, title) values(365, 'CSD','CSD - Dinar serbski');
INSERT INTO [dsg_currency] (id, cn, title) values(366, 'SEK','SEK - Korona szwedzka');
INSERT INTO [dsg_currency] (id, cn, title) values(367, 'HUF','HUF - Forint W�gry');


SET IDENTITY_INSERT [dsg_currency] OFF

