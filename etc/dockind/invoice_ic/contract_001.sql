alter table dsg_contract_ic add document_date datetime null;
alter table dsg_contract_ic add expiry_date datetime null;
alter table dsg_contract_ic add name varchar(250) null;
alter table dsg_contract_ic add document_number varchar(250) null;
alter table dsg_contract_ic add introduce_person [numeric](19, 0) NULL
alter table dsg_contract_ic add nip varchar(32) null;
alter table dsg_contract_ic add others varchar(250) null;
alter table dsg_contract_ic add company numeric(19, 0) NULL;