CREATE TABLE [dsg_enum5](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
);

insert into dsg_enum5 (cn,title,refValue)values('DO','Dzia� Operacyjny','1');
insert into dsg_enum5 (cn,title,refValue)values('BP','Biuro prawne','1');
insert into dsg_enum5 (cn,title,refValue)values('ICT','IC Travel','1');
insert into dsg_enum5 (cn,title,refValue)values('KF','Kontrola finansowa','2');
insert into dsg_enum5 (cn,title,refValue)values('KS','Ksi�gowo��','2');