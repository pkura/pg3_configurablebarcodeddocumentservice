CREATE TABLE [dsg_enum3](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
);
insert into dsg_enum3 (cn,title)values('ZKWLOD','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKWIND','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKUEK1','Korekty Naby� - skonta');
insert into dsg_enum3 (cn,title)values('ZKUEK','Korekty Naby�');
insert into dsg_enum3 (cn,title)values('ZKUE','Zakup towar�w w UE');
insert into dsg_enum3 (cn,title)values('ZKTXT','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKTXK','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKTXJ','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKTUS','Zakup cz�ci serwisowych');
insert into dsg_enum3 (cn,title)values('ZKTUE','Zakup towar�w z UE nabycie');
insert into dsg_enum3 (cn,title)values('ZKTRSU','Transport unijny (spedycja)');
insert into dsg_enum3 (cn,title)values('ZKTRSM','Transport mi�dzynarodowy (spedycja)');
insert into dsg_enum3 (cn,title)values('ZKTRSK','Transport krajowy (spedycja)');
insert into dsg_enum3 (cn,title)values('ZKTRNO','Transport (nabycie) stawka 0%');
insert into dsg_enum3 (cn,title)values('ZKTRNNW','Transport (nabycie) stawka NP WAL');
insert into dsg_enum3 (cn,title)values('ZKTRNN','Transport (nabycie) stawka NP');
insert into dsg_enum3 (cn,title)values('ZKTRN2','Transport (nabycie) stawka 22%');
insert into dsg_enum3 (cn,title)values('ZKTRINW','Transport (import) stawka NP WAL');
insert into dsg_enum3 (cn,title)values('ZKTRIN','Transport (import) stawka NP');
insert into dsg_enum3 (cn,title)values('ZKTRI2','Transport (import) stawka 22%');
insert into dsg_enum3 (cn,title)values('ZKTRI0','Transport (import) stawka 0%');
insert into dsg_enum3 (cn,title)values('ZKTRAK','Faktury transportowe');
insert into dsg_enum3 (cn,title)values('ZKTRA','Faktury transportowe');
insert into dsg_enum3 (cn,title)values('ZKTOW','Zakup towar�w handlowych krajowych');
insert into dsg_enum3 (cn,title)values('ZKTOP','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKTNZ','Zakup towar�w w tranzycie');
insert into dsg_enum3 (cn,title)values('ZKTIM','Zakup towar�w z importu (faktury)');
insert into dsg_enum3 (cn,title)values('ZKSZFLO','Zakup kosztowy - flota');
insert into dsg_enum3 (cn,title)values('ZKSRT','Zakup �rodk�w trwa�ych');
insert into dsg_enum3 (cn,title)values('ZKSPEWAL','Zakup spedycja walutowa');
insert into dsg_enum3 (cn,title)values('ZKSAD','Zakup towar�w z importu (SAD)');
insert into dsg_enum3 (cn,title)values('ZKRZR','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKPRE','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKPOB','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKOSRD','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKOLL','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKMPT','Zakup materia��w - przecena');
insert into dsg_enum3 (cn,title)values('ZKMPO','Zakup pozosta�ych materia��w');
insert into dsg_enum3 (cn,title)values('ZKMAT','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKKUSD','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKKSZW','Zakup kosztowy-waluty');
insert into dsg_enum3 (cn,title)values('ZKKSZP','Zakup kosztowy - pozosta�y');
insert into dsg_enum3 (cn,title)values('ZKKSZ','Zakup kosztowy');
insert into dsg_enum3 (cn,title)values('ZKKRP','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKKPP','Zakup kosztowy- Dokumenty pozosta�e');
insert into dsg_enum3 (cn,title)values('ZKKOB','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKKBL','Zakup kosztowy - Naprawy powypadkowe');
insert into dsg_enum3 (cn,title)values('ZKKAK','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKJAWD','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKINW','Zakup inwestycyjny');
insert into dsg_enum3 (cn,title)values('ZKINN','Pozosta�e');
insert into dsg_enum3 (cn,title)values('ZKIMP','Zakup towar�w z importu (SAD)');
insert into dsg_enum3 (cn,title)values('ZKGRUD','Rejestr zakupu - koszty windykacji');
insert into dsg_enum3 (cn,title)values('ZKFLO','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKFBL','Zakup towar�w Flota');
insert into dsg_enum3 (cn,title)values('ZKCZK','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKBIH','Zakup materia��w');
insert into dsg_enum3 (cn,title)values('ZKBBH','Zakup materia��w');
