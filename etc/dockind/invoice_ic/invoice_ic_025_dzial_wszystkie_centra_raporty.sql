DECLARE @TestVariable numeric(19, 0);
SET @TestVariable = (SELECT ID FROM DS_DIVISION where GUID = 'rootdivision');
INSERT INTO DS_DIVISION ([GUID],[NAME],[DIVISIONTYPE],[PARENT_ID],[HIDDEN])
VALUES('WSZYSTKIE_CENTRA_RAPORTY','Raporty - wszystkie centra','group',@TestVariable,0);
