update dsg_contract_ic
set company = (select top 1 ID from DF_DICINVOICE where DF_DICINVOICE.numerKontrahenta = dsg_contract_ic.document_number)
where company is null and exists
(select top 1 ID from DF_DICINVOICE where DF_DICINVOICE.numerKontrahenta = dsg_contract_ic.document_number)