SELECT [DSG_INVOICE_IC].[DOCUMENT_ID]
	  ,[count]
      ,[INVOICE_IC_ATRIB_COUNT].[data_wystawienia]
      ,[INVOICE_IC_ATRIB_COUNT].[kwota_brutto]
      ,[INVOICE_IC_ATRIB_COUNT].[stanowisko]
      ,[INVOICE_IC_ATRIB_COUNT].[nr_faktury]
      ,[INVOICE_IC_ATRIB_COUNT].[dostawca]
  FROM [INVOICE_IC_ATRIB_COUNT], [DSG_INVOICE_IC],DS_DOCUMENT
 WHERE [INVOICE_IC_ATRIB_COUNT].[data_wystawienia] = [DSG_INVOICE_IC].[data_wystawienia]
	  AND [DSG_INVOICE_IC].[DOCUMENT_ID] = DS_DOCUMENT.ID
      AND [INVOICE_IC_ATRIB_COUNT].[kwota_brutto] = [DSG_INVOICE_IC].[kwota_brutto]
      AND [INVOICE_IC_ATRIB_COUNT].[stanowisko] = [DSG_INVOICE_IC].[stanowisko]
      AND [INVOICE_IC_ATRIB_COUNT].[nr_faktury] = [DSG_INVOICE_IC].[nr_faktury]
      AND [INVOICE_IC_ATRIB_COUNT].[dostawca] = [DSG_INVOICE_IC].[dostawca]
	  AND [count] > 1
	 -- AND CTIME >= '2010-01-01'
ORDER BY [INVOICE_IC_ATRIB_COUNT].[nr_faktury] 