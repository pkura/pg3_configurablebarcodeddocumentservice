
alter table dsg_invoice_ic add payment_method_id integer null;
alter table dsg_invoice_ic add advance_taker varchar(255) null;
alter table dsg_invoice_ic add advance_number varchar(255) null;

CREATE TABLE [dsg_payment_method](
	[id]        [int] IDENTITY(1,1) NOT NULL,
	[cn]        [varchar](50) NULL,
	[title]     [varchar](255) NULL,
	[centrum]   [int] NULL,
	[refValue]  [varchar](20) NULL,
	[available] [bit] NOT NULL DEFAULT 1,

    CONSTRAINT [PK_payment_method] PRIMARY KEY ([id] ASC)
);


insert into dsg_payment_method (cn, title) values ('01','Got�wka');
insert into dsg_payment_method (cn, title) values ('02','Odroczona');
insert into dsg_payment_method (cn, title) values ('03','Karta/czek');
insert into dsg_payment_method (cn, title) values ('04','Kompensata');
insert into dsg_payment_method (cn, title) values ('05','Za pobraniem');
insert into dsg_payment_method (cn, title) values ('06','Przedp�ata');
insert into dsg_payment_method (cn, title) values ('07','Bezp�atne');



