

CREATE TABLE [ds_acceptance_mode_rule](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cost_center_id] [int] NOT NULL,
	[mode_id] [int] NOT NULL,
	[min_amount] [decimal](12, 2) NOT NULL,
	[mode_name] [varchar](254) NULL,
    CONSTRAINT [PK_ds_acceptance_mode_rule] PRIMARY KEY CLUSTERED ([id] ASC)
 )

 CREATE UNIQUE NONCLUSTERED INDEX [uq_center_mode] ON [ds_acceptance_mode_rule]
(
	[cost_center_id] ASC,
	[mode_id] ASC
)


