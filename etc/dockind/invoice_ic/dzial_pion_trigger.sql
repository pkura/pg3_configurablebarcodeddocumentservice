-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER UPDATE_PION 
   ON  DSG_ENUM5
   AFTER UPDATE
AS IF UPDATE(refValue)
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
	declare @pionid int;
	declare @dzialid int;

	select @pionid=i.refValue from inserted i;	
	select @dzialid=i.id from inserted i;	

	UPDATE DSG_INVOICE_IC SET struktura = @pionid WHERE stanowisko = @dzialid

END
GO
