CREATE OR REPLACE TRIGGER BECK_EMP 
BEFORE INSERT OR UPDATE ON DS_USER_TO_DIVISION DECLARE
  divName DS_Division.name%TYPE;
  divPosName DS_Division.name%TYPE;
BEGIN
select divD.name INTO divname from ds_division div, ds_user us, ds_user_to_division du, ds_division divD  where du.division_id = div.id and du.user_id = us.id and div.divisiontype = 'position' and div.parent_id = divd.id;
select div.name INTO divPosName from ds_division div, ds_user us, ds_user_to_division du  where du.division_id = div.id and du.user_id = us.id and div.divisiontype = 'position';
  update ds_employee_card set division = divname , position = divposname;
END;