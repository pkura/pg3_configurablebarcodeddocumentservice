DECLARE
  userID DS_USER.name%TYPE;
  employeeCardID DS_EMPLOYEE_CARD.ID%TYPE;
  CURSOR UserIdCursor IS (SELECT u.name FROM DS_USER u  where (select count(1) from DS_EMPLOYEE_CARD e where e.user_id = u.name ) < 1 ) FOR UPDATE;
BEGIN
  OPEN UserIdCursor;
  
  LOOP
    FETCH userIdCursor INTO userID;
    EXIT WHEN userIdCursor%NOTFOUND;
    
    SELECT SEQ_DS_EMPLOYEE_CARD_ID.NEXTVAL INTO employeeCardId FROM DUAL;
    
    INSERT INTO DS_EMPLOYEE_CARD ("ID", KPX, COMPANY, DEPARTMENT, EMPLOYMENT_START_DATE, POSITION, USER_ID)
			VALUES (employeeCardId, 'BD', 'Beck', 'G��wny', CURRENT_TIMESTAMP, 'BD', userID);
    
    INSERT INTO DS_ABSENCE ("ID", "YEAR", START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (SEQ_DS_ABSENCE_ID.NEXTVAL, 2013, to_date('2013-01-01', 'yyyy-mm-dd'), to_date('2013-12-31', 'yyyy-mm-dd'), 26, CURRENT_TIMESTAMP, 'Urlop nale�ny', employeeCardID, 'WUN');
			
		INSERT INTO DS_ABSENCE ("ID", "YEAR", START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (SEQ_DS_ABSENCE_ID.NEXTVAL, 2013, to_date('2013-01-01', 'yyyy-mm-dd'), to_date('2013-12-31', 'yyyy-mm-dd'), 0, CURRENT_TIMESTAMP, 'Urlop zaleg�y', employeeCardID, 'WUZ');
      
  END LOOP;
  
  CLOSE UserIdCursor;
END;

