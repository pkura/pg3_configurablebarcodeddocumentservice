alter table ds_employee_card add FIRST_NAME varchar(65);
alter table ds_employee_card add LAST_NAME varchar(65);
alter table ds_absence add DOCUMENT_ID numeric(19,0);

DECLARE
  login DS_USER.name%TYPE;
  firstName DS_USER.FIRSTNAME%TYPE;
  lastName DS_USER.LASTNAME%TYPE;
  CURSOR logins IS (SELECT USER_ID FROM DS_EMPLOYEE_CARD) FOR UPDATE;
BEGIN
  OPEN logins;
  
  LOOP
    FETCH logins INTO login;
    EXIT WHEN logins%NOTFOUND;
    
    firstName := null;
    lastName := null;
    SELECT u.FIRSTNAME INTO firstName FROM DS_USER u WHERE u.NAME = login;
    SELECT u.LASTNAME INTO lastName FROM DS_USER u WHERE u.NAME = login;
    
    UPDATE DS_EMPLOYEE_CARD SET FIRST_NAME = firstName, LAST_NAME = lastName where user_id = login;
  END LOOP;
  
  CLOSE logins;
END;