alter table DS_EMPLOYEE_CARD add DIVISION varchar(100);
alter table DS_EMPLOYEE_CARD add IS_T188KP tinyint;
alter table DS_EMPLOYEE_CARD add IS_TUJ tinyint;

-- urlop inwalidzki
alter table DS_EMPLOYEE_CARD add IS_TINW tinyint;
alter table DS_EMPLOYEE_CARD add INV_ABS_DAYS_NUM integer;


-- liczby dni urlopow T188 Kp i UJ
alter table DS_EMPLOYEE_CARD add T188KP_DAYS_NUM integer;
alter table DS_EMPLOYEE_CARD add TUJ_DAYS_NUM integer;

alter table ds_absence add EDITOR_NAME varchar(75);
alter table dsg_absence_request add author varchar(65);
alter table dsg_absence_request add external_id numeric(19,0);