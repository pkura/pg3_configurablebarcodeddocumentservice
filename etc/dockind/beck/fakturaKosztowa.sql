CREATE TABLE dsg_beck_coinvoice
(
    DOCUMENT_ID numeric(18, 0) NOT NULL ,
    status numeric(18, 0),
    invoice_kind numeric(18, 0),
    dostawca numeric(18, 0),
    invoice_date timestamp, 
    beck_zamowienie  numeric(18, 0),
    invoice_number varchar2(30),
    payment_date timestamp,
    net_amount numeric(18,2),  
    vat numeric(18,2),
    gross_amount numeric(18,2),  
    centra_kosztow numeric(18, 0)
);


CREATE TABLE dsg_beck_invoice_kind
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum numeric(18, 0),
	refValue varchar(20),
	available numeric(18, 0) default 1 not null
);

create sequence dsg_beck_invoice_kind_id start with 1 nocache;