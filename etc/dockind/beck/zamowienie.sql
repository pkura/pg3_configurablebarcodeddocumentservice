

CREATE TABLE dsg_beck_zamowie
(
	DOCUMENT_ID numeric(18, 0) NOT NULL ,
	category numeric(18, 0),
	dostawca numeric(18, 0),
	description varchar2(301 char),
	status numeric(18, 0),
	kwota_netto numeric(18, 2),
	rea_date timestamp NULL,
	centra_kosztow numeric(18, 0)
);


CREATE TABLE dsg_beck_category
(
	ID numeric(18,0) NOT NULL,
	CN varchar2(50),
	TITLE varchar2(255),
	centrum int,
	refValue varchar2(20),
	available int default 1 not null
);

CREATE SEQUENCE dsg_beck_category_id start with 1 nocache;

CREATE TABLE dsg_ifpan_cost_category
(
	ID numeric(18,0) NOT NULL,
	CN varchar2(50),
	TITLE varchar2(255),
	centrum numeric(18,0),
	refValue varchar2(20),
	available numeric(18,0) default 1 not null
);

CREATE SEQUENCE dsg_ifpan_cost_category_id start with 1 nocache;


CREATE TABLE dsg_ifpan_founding_source
(
	ID numeric(18,0) NOT NULL,
	CN varchar2(50),
	TITLE varchar2(255),
	centrum numeric(18,0),
	refValue varchar2(20),
	available numeric(18,0) default 1 not null
);

CREATE SEQUENCE dsg_ifpan_founding_source_id start with 1 nocache;


CREATE TABLE dsg_beck_cost
(
	ID numeric(18,0) NOT NULL,
	CN varchar2(50),
	TITLE varchar2(255),
	centrum numeric(18,0),
	refValue varchar2(20),
	available numeric(18,0) default 1 not null
);

CREATE SEQUENCE dsg_beck_cost_id start with 1 nocache;


ALTER TABLE DS_DOCUMENT ADD index_status_id SMALLINT DEFAULT 0 NOT NULL ;

CREATE TABLE dsg_beck_faktura_multiple(
	ID numeric(18,0),
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar2(40) NOT NULL,
    FIELD_VAL varchar2(100) NULL
);

CREATE SEQUENCE dsg_beck_faktura_multiple_id start with 1 nocache;



