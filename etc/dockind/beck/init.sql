

CREATE TABLE dsg_absence_request
 (
	document_id numeric(19, 0) NOT NULL PRIMARY KEY ,
	dsuser numeric(19, 0) NULL,
	period_from timestamp NULL,
	period_to timestamp NULL,
	description varchar(400 char) NULL,
	position varchar2(50 char) NULL,
	status integer NULL,
	kind varchar2(150 char) NULL,
	working_days integer NULL,
	available_days integer NULL,
	substitution numeric(19, 0) NULL
);

-- DZIA� PERSONALNY
INSERT INTO DS_DIVISION (id,GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,description,HIDDEN)VALUES (ds_division_id.nextval,'personal-division', 'Dzia� personalny', 'DP', 'group', 473, NULL, 1);

-- TYPY URLOP�W DLA BECK     

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUN', 'Wymiar urlopu nale�ny', NULL, 2);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUZ', 'Wymiar urlopu zaleg�y', NULL, 2);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UW', 'Urlop wypoczynkowy', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('U�', 'Urlop na ��danie', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('188', 'Opieka nad dzieckiem', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UOK', 'Urlop okoliczno�ciowy', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UB', 'Urlop bezp�atny', NULL, 1);     
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UI', 'Inny urlop', NULL, 0);

-- DODATKOWE URLOPY
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UJ', 'Urlop ojcowski', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UM', 'Urlop macierzy�ski', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UY', 'Urlop wychowawczy', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NP', 'Nieobecno�� pracownika', NULL, 1);

CREATE TABLE dsg_abs_without_pr
 (
	document_id numeric(19, 0) NOT NULL PRIMARY KEY ,
	dsuser numeric(19, 0) NULL,
	period_from timestamp NULL,
	period_to timestamp NULL,
	description varchar(400 char) NULL,
	position varchar2(50 char) NULL,
	status integer NULL,
	kind varchar2(150 char) NULL,
	working_days integer NULL,
	available_days integer NULL,
	substitution numeric(19, 0) NULL
);

-- urlop inwalidzki
alter table DS_EMPLOYEE_CARD add IS_TINW smallint;
alter table DS_EMPLOYEE_CARD add INV_ABS_DAYS_NUM integer;
update ds_absence_type set flag = 2 where code = 'INW';

-- liczby dni urlopow T188 Kp i UJ
alter table DS_EMPLOYEE_CARD add T188KP_DAYS_NUM integer;
alter table DS_EMPLOYEE_CARD add TUJ_DAYS_NUM integer;

alter table DS_EMPLOYEE_CARD add DIVISION varchar(100);
alter table DS_EMPLOYEE_CARD add IS_T188KP smallint;
alter table DS_EMPLOYEE_CARD add IS_TUJ smallint;

Aby zg�osi� b��d wy�lij maila : <a href="mailto:helpdesk@docusafe.pl">helpdesk@docusafe.pl</a> 

<div>

<p>LEGENDA KALENDARZA:</p>

<p><img width=12 height=12 src="/docusafe/img/lock_mini.gif" alt="*"/>
<span style='font:12.0pt "Times New Roman"'>&nbsp;K��dka - kalendarz jest zablokowany przed edycj� zmiany mo�e wprowadza� tylko uprawniona osoba</span>
</p>

<p><img width=12 height=12 src="/docusafe/img/gm_arrow_down.gif" alt="*"/>
<span style='font:12.0pt "Times New Roman"'>&nbsp;Strza�ka skierowana w d� - wyb�r koloru kalendarza</span>
</p>

<p><img width=14 height=14 src="/docusafe/img/all.gif" alt="*"/>
<span style='font:12.0pt "Times New Roman"'>&nbsp;Krzy�yk - usuni�cie wpisu ze swojej listy </span>
</p>

<p><img width=12 height=12 src="/docusafe/img/saveBtn.gif" alt="*"/>
<span style='font:12.0pt "Times New Roman"'>&nbsp;Ptaszek - potwierdzenie wzi�cia udzia�u w wydarzeniu.</span>
</p>

<p>?
<span style='font:12.0pt "Times New Roman"'>&nbsp;Znak zapytania przy imieniu i nazwisku - Oznacza �e osoba nie potwierdzi�a jeszcze zaproszenia, pytanie zosta�o zadane i oczekuje na odpowied�,</span>
</p>


