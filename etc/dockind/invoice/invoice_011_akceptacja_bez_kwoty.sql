ALTER TABLE DSG_INVOICE ADD akceptacjia_bez_kwoty smallint;
DECLARE @TestVariable numeric(19, 0);
SET @TestVariable = (SELECT ID FROM DS_DIVISION where GUID = 'INVOICE_ACCOUNTING');
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_AKCEPTACJA_BEZ_KWOTY', 'Akceptacja bez sprawdzenia kwoty','group', @TestVariable,0);