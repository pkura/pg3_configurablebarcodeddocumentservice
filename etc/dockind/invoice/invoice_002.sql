CREATE TABLE [DF_DICINVOICE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[numerKontrahenta] [varchar](255) NOT NULL,
	[numerKontaBankowego] [varchar](255) NULL,
	[imie] [varchar](255) NULL,
	[nazwisko] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[oldname] [varchar](255) NULL,
	[nip] [varchar](30) NULL,
	[regon] [varchar](30) NULL,
	[ulica] [varchar](255) NULL,
	[kod] [varchar](10) NULL,
	[miejscowosc] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[faks] [varchar](255) NULL,
	[telefon] [varchar](255) NULL,
	[kraj] [varchar](10) NULL,
	PRIMARY KEY (id)
);