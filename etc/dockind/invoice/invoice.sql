CREATE TABLE [DSG_INVOICE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL ,
    [status] [int] NULL ,
    [data_wystawienia] [datetime] NULL ,
    [data_platnosci] [datetime] NULL ,
    [data_zaplaty] [datetime] NULL ,
    [data_zaksieg] [datetime] NULL ,
    [dostawca] [numeric] (19,0) NULL,
    [kwota_brutto] [float] NULL,
    [opis_towaru] [varchar] (200) NULL,
    [nr_zestawienia] [numeric] (19,0) NULL,
    [nr_faktury] [varchar] (30) NULL,
    [zaplacono] [smallint] NULL,
    [akceptacja_finalna] [smallint] NULL,
	[numer_rachunku_bankowego] [varchar] (32) NULL,
);


CREATE TABLE [DSG_INVOICE_MULTIPLE_VALUE] (
    [DOCUMENT_ID] [numeric](19, 0) NOT NULL,
    [FIELD_CN] [varchar](20) NOT NULL,
    [FIELD_VAL] [varchar](100) NULL
);

insert into ds_box_line (line,name) values ('invoice','Faktury kosztowe');

insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_ACCOUNTING', 'Faktury kosztowe - księgowość','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_CENTRA', 'Faktury kosztowe','division',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_READ', 'Faktury kosztowe - odczyt','group',1,0);
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_MODIFY', 'Faktury kosztowe - modyfikacja','group',1,0);
