CREATE TABLE DSG_INVOICE (
    DOCUMENT_ID numeric(18, 0) NOT NULL ,
    status integer NULL ,
    data_wystawienia timestamp NULL ,
    data_platnosci timestamp NULL ,
    data_zaplaty timestamp NULL ,
    data_zaksieg timestamp NULL ,
    dostawca numeric (18,0) NULL,
    kwota_brutto float NULL,
    opis_towaru varchar (200 char) NULL,
    nr_zestawienia numeric (19,0) NULL,
    nr_faktury varchar (30 char) NULL,
    zaplacono smallint NULL,
    akceptacja_finalna smallint NULL
);


CREATE TABLE DSG_INVOICE_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(20 char) NOT NULL,
    FIELD_VAL varchar(100 char) NULL
);

insert into ds_box_line (id,line,name) values (ds_box_line_id.NEXTVAL,'invoice','Faktury kosztowe');

insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL,'INVOICE_ACCOUNTING', 'Faktury kosztowe - księgowość','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL,'INVOICE_CENTRA', 'Faktury kosztowe','division',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL,'INVOICE_READ', 'Faktury kosztowe - odczyt','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL,'INVOICE_MODIFY', 'Faktury kosztowe - modyfikacja','group',1,0);
