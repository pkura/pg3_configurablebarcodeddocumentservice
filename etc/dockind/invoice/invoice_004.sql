DECLARE @TestVariable numeric(19, 0);
SET @TestVariable = (SELECT ID FROM DS_DIVISION where GUID = 'INVOICE_ACCOUNTING');
insert into ds_division (guid,name,divisiontype,parent_id,hidden) values ('INVOICE_RACHUNEKBANKOWY', 'Rachunek bankowy - edycja','group', @TestVariable,0);