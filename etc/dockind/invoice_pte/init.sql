CREATE TABLE [dbo].[DS_CONSUMER_DURABLES](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[INVENTORY_NUM] [varchar](50) NOT NULL,
	[INFO] [text] NULL,
	[SERIAL_NUM] [varchar](50) NOT NULL,
	[LOCATION] [varchar](255) NOT NULL,
	[AVAILABLE] [tinyint] NULL,
	[CTIME] [datetime] DEFAULT (getDate())
);
insert into ds_division (guid, name, code, divisiontype, parent_id, hidden) values ('REPORT_DurablesReport','Raport �rodk�w trwa�ych - generowanie',null,'group',1,1);