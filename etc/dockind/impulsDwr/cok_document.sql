CREATE TABLE "DOCUMENT_IN_EMAIL"
(	"DOCUMENT_ID" NUMBER(19,0) NOT NULL ENABLE,
   "MESSAGE_ID" NUMBER(19,0),
   "ATTACHMENTID" NUMBER(19,0),
   "ATTACHMENT_DICTIONARY_ID" NUMBER(19,0),
   "CONTRACTOR_ID" NUMBER(19,0),
   "TASK_TYPE" VARCHAR2(64 CHAR),
   "STATUS" VARCHAR2(64 CHAR),
   "CASE_ID" NUMBER(19,0),
   "DICTIONARY_ID" NUMBER(10,0),
   "CONVERSATION_DESCRIPTION" VARCHAR2(512 CHAR),
   "DELIVERY_TYPE" NUMBER(19,0)
);
commit;

CREATE TABLE "DOCUMENT_IN_EMAIL_MULTIPLE"
(	"ID" NUMBER(19,0) NOT NULL ENABLE,
   "DOCUMENT_ID" NUMBER(19,0),
   "FIELD_CN" VARCHAR2(150 BYTE),
   "FIELD_VAL" VARCHAR2(50 BYTE)
);
commit;

create sequence DOCUMENT_IN_EMAIL_MULTIPLE_ID start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_DOCUMENT_IN_EMAIL_MULTIPLE
before insert on DOCUMENT_IN_EMAIL_MULTIPLE
for each row
  begin
    if :new.id is null then
      select DOCUMENT_IN_EMAIL_MULTIPLE_ID.nextval into :new.id from dual;
    end if;
  end
    commit;

CREATE TABLE "DSC_ILPOL_COK_CASE"
(	"ID" NUMBER(19,0),
   "INTERNAL_ID" VARCHAR2(5 CHAR),
   "CTIME" TIMESTAMP (6),
   "DESCRIPTION" VARCHAR2(512 CHAR),
   "NAME" VARCHAR2(50 CHAR),
   "FULLNAME" VARCHAR2(200 CHAR),
   "NIP" VARCHAR2(12 CHAR),
   "KRS" VARCHAR2(50 CHAR),
   "REGON" VARCHAR2(13 CHAR),
   "STREET" VARCHAR2(50 CHAR),
   "CODE" VARCHAR2(5 CHAR),
   "CITY" VARCHAR2(50 CHAR),
   "COUNTRY" VARCHAR2(50 CHAR),
   "REMARKS" VARCHAR2(200 CHAR),
   "CONTRACTORNO" VARCHAR2(50 CHAR),
   "PHONENUMBER" VARCHAR2(50 CHAR),
   "PHONENUMBER2" VARCHAR2(50 CHAR),
   "EMAIL" VARCHAR2(50 CHAR),
   "FAX" VARCHAR2(50 CHAR),
   "WWW" VARCHAR2(50 CHAR),
   "REGION" NUMBER(10,0),
   "DSUSER" VARCHAR2(50 CHAR),
   "OBROT" NUMBER(19,2),
   "FIRSTNAME_1" VARCHAR2(64 CHAR),
   "LASTNAME_1" VARCHAR2(64 CHAR),
   "EMAIL_1" VARCHAR2(64 CHAR),
   "FIRSTNAME" VARCHAR2(50 CHAR),
   "LASTNAME" VARCHAR2(50 CHAR),
   "DIVISION" VARCHAR2(50 CHAR),
   "AREA" NUMBER(19,0),
   "CATEGORY" NUMBER(19,0),
   "SUBCATEGORY" NUMBER(19,0)
);
commit;

CREATE SEQUENCE "DSC_ILPOL_COK_CASE_ID" INCREMENT BY 1 START WITH 1 NOCACHE;
commit;


create table DSC_ILPOL_COK_CASE_CATEGORY
(
  id number(19,0) not null,
  name varchar2(250),
  parent_id number(19,0) default null
);

create sequence DSC_ILPOL_COK_CASE_CATEGORY_ID start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_DSC_ILPOL_COK_CASE_CAT
before insert on DSC_ILPOL_COK_CASE_CATEGORY
for each row
  begin
    if :new.id is null then
      select DSC_ILPOL_COK_CASE_CATEGORY_ID.nextval into :new.id from dual;
    end if;
  end
    commit;

-- obszary
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Umowy aktywne');
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Zmiany i zako�czenia um�w');

-- kategorie
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values('Faktury', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values('Wnioski klienta', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values('Udzielenie informacji', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values('Wnioski klienta', 2);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values('Udzielenie informacji', 2);

-- podkategorie
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Duplikat faktury', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Wyja�nienie faktury', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('B��dnie wystawiona faktura', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Brak faktury', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Organy administracji', 3);

insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Harmonogram', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Opinia dot. wsp�pracy', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Saldo kapita�owo odsetkowe', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Zgoda na podnajem', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Upowa�nienie do korzystania z PU', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Zmiana daty p�atno�ci', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Zmiana danych Klienta', 4);

insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Zmiana osobowo�ci prawnej', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Cesja', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Restrukturyzacja', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Wcze�niejsze zako�czenie umowy', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY (name, parent_id) values ('Terminowe zako�czenie umowy', 6);


create or replace view DSC_ILPOL_COK_CASE_AREA as
  select area.id id, area.id cn, area.name title, '0' centrum, null refValue, '1' available
  from DSC_ILPOL_COK_CASE_CATEGORY area where parent_id is null
  order by name asc;

create or replace view DSC_ILPOL_COK_CASE_CAT as
  select cat.id id, cat.id cn, cat.name title, '0' centrum, cat.parent_id refValue, '1' available
  from DSC_ILPOL_COK_CASE_CATEGORY cat;

create or replace view DSC_ILPOL_COK_CASE_SUBCAT as
  select cat.id id, cat.id cn, cat.name title, '0' centrum, cat.parent_id refValue, '1' available
  from DSC_ILPOL_COK_CASE_CATEGORY cat;

commit;

CREATE OR REPLACE FORCE VIEW "DSC_ILPOL_UMOWA_DICTIONARY" ("ID", "CONTRACTOR_ID", "NRUMOWY", "DATAPODPISANIA", "DATAAKTYWACJI", "DATAZAKONCZENIA", "STATUS", "NAZWAPRZEDMIOTU", "TYPPRZEDMIOTU", "NRWNIOSKU")
AS
  SELECT DISTINCT(leo.idumowy) id,
                 c.idklienta contractor_id,
                 leo.nrumowy nrumowy,
                 leo.datapodpisania datapodpisania,
                 leo.dataaktywacji dataaktywacji,
                 leo.datazakonczenia datazakonczenia,
                 leo.status status,
                 leo.nazwaprzedmiotu nazwaprzedmiotu,
                 leo.typprzedmiotu typprzedmiotu,
                 leo.nrwniosku nrwniosku
  FROM sysdba.vumowy leo
    INNER JOIN dl_contract_dictionary c
      ON c.numerumowy=leo.nrumowy;

alter table document_in_email add sent_resp varchar2(4000);
commit;

create sequence DSC_ILPOL_COK_CASE_INTERNAL_ID start with 1 increment by 1 nocache;
commit;

alter table dsc_ilpol_cok_case modify internal_id VARCHAR2(17 CHAR);
commit;

CREATE TABLE DSC_ILPOL_COK_TASK_STATUS(
  ID	NUMBER(19),
  CN	NUMBER(19),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_COK_TASK_STATUS_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_TASK_STATUS
BEFORE INSERT ON DSC_ILPOL_COK_TASK_STATUS
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_TASK_STATUS_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (1, 'Nowe', 0, 1);
INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (2, 'Przyj�te do realizacji', 0, 1);
INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (3, 'W trakcie realizacji', 0, 1);
INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (4, 'Przekazane do realizacji', 0, 1);
INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (5, 'Zako�czone', 0, 1);
COMMIT;

ALTER TABLE DOCUMENT_IN_EMAIL ADD TASK_STATUS NUMBER(19,0);
COMMIT;

ALTER TABLE DOCUMENT_IN_EMAIL ADD COMPLAINT NUMBER(5,0);
COMMIT;
RENAME OLD_SEQUENCE TO NEW_SEQUENCE;
-- obszary
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Ubezpieczenia');
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Szkody');
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Rozrachunki');
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Windykacja');
insert into DSC_ILPOL_COK_CASE_CATEGORY (name) values('Inne');

INSERT INTO DSC_ILPOL_COK_TASK_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (6, 'Zg�oszenie nies�uszne', 0, 1);
COMMIT;

ALTER TABLE DOCUMENT_IN_EMAIL RENAME TO DSC_ILPOL_COK_DOCUMENT;
COMMIT;

ALTER TABLE DOCUMENT_IN_EMAIL_MULTIPLE RENAME TO DSC_ILPOL_COK_DOCUMENT_MLT;
COMMIT;

RENAME DOCUMENT_IN_EMAIL_MULTIPLE_ID TO DSC_ILPOL_COK_DOCUMENT_MLT_ID;
COMMIT;

DROP TRIGGER TRG_DOCUMENT_IN_EMAIL_MULTIPLE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_DOCUMENT_MLT
BEFORE INSERT ON DSC_ILPOL_COK_DOCUMENT_MLT
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_DOCUMENT_MLT_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

UPDATE DS_DOCUMENT_KIND SET TABLENAME = 'DSC_ILPOL_COK_DOCUMENT' WHERE TABLENAME = 'document_in_email';
COMMIT;

UPDATE DS_DOCUMENT_KIND SET CN = 'cok_document' WHERE CN = 'document_in_email';
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD DEALER NUMBER(5,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD TASK_SOLUTION VARCHAR2(512 BYTE);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD FINISH_DATE TIMESTAMP(6);
COMMIT;

drop table DSC_ILPOL_COK_CASE_CATEGORY;
create table DSC_ILPOL_COK_CASE_CATEGORY
(
  id number(19,0) not null unique,
  name varchar2(250),
  parent_id number(19,0) default null
);
commit;

insert into DSC_ILPOL_COK_CASE_CATEGORY values (1, 'Umowy aktywne', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (2, 'Zmiany i zako�czenia um�w', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (3, 'Ubezpieczenia', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (4, 'Szkody', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (5, 'Rozrachunki', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (6, 'Monitoring p�atno�ci', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (7, 'Windykacja', null);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (11, 'Faktury', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (12, 'Wnioski klienta', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (13, 'Portal klienta', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (14, 'Udzielenie informacji', 1);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (15, 'Inne', 1);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (21, 'Wnioski klienta', 2);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (22, 'Udzielenie informacji', 2);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (23, 'Inne', 2);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (31, 'Polisa ubezpieczeniowa', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (32, 'Ubezpieczenie obce', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (33, 'Wnioski klienta', 3);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (34, 'Inne', 3);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (41, 'Szkoda ca�kowita', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (42, 'Szkoda cz�ciowa', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (43, 'Kradzie�', 4);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (44, 'Wnioski Klienta', 4);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (51, 'Saldo nale�no�ci', 5);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (52, 'Wnioski Klienta', 5);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (53, 'Inne', 5);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (61, 'Wnioski Klienta', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (62, 'Sms', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (63, 'Monit', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (64, 'Wezwanie do zap�aty', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (65, 'Wnioski Klienta', 6);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (66, 'Inne', 6);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (71, 'Wypowiedzenie umowy', 7);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (72, 'Wnioski Klienta', 7);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (73, 'Rozliczenie umowy', 7);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (74, 'Inne', 7);


insert into DSC_ILPOL_COK_CASE_CATEGORY values (111, 'Duplikat faktury', 11);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (112, 'Wyja�nienie faktury', 11);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (113, 'B��dnie wystawiona faktura', 11);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (114, 'Brak faktury', 11);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (115, 'Organy administracji', 11);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (116, 'Inne', 11);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (121, 'Harmonogram', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (122, 'Opinia dot. wsp�pracy', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (123, 'Saldo kapita�owo odsetkowe', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (124, 'Zgoda na podnajem', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (125, 'Upowa�nienie do korzystania z PU', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (126, 'Zmiana daty p�atno�ci', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (127, 'Zmiana danych klienta', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (128, 'Inne', 12);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (131, 'Aktywacja konta', 13);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (132, 'Reset hasla', 13);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (133, 'Zmiana adresu mailowego', 13);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (134, 'Inne', 13);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (141, 'Udzielenie informacji', 14);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (151, 'Inne', 15);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (211, 'Zmiana osobowosci prawnej', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (212, 'Cesja', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (213, 'Restrukturyzacja', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (214, 'Wcze�niejsze zako�czenie umowy', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (215, 'Terminowe zako�czenie umowy', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (216, 'Inne', 21);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (221, 'Udzielenie informacji', 22);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (231, 'Inne', 23);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (311, 'Wyja�nienie faktury', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (312, 'Nota ubezpieczeniowa', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (313, 'Zwrot sk�adki ubezpieczeniowej', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (314, 'Wypowiedzenie polisy', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (315, 'Udzielenie informacji', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (316, 'Inne', 31);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (317, 'Cesja polisy', 31);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (321, 'Wniosek o polis� poza pakietem F', 32);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (322, 'Udzielenie informacji', 32);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (323, 'Zgoda na cesj�', 32);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (324, 'Inne', 32);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (331, 'Duplikat polisy', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (332, 'Dokumenty', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (333, 'Rozbicie sk�adki na raty', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (334, 'Rozsze�enie zakresu ubezpieczenia ', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (335, 'Oferta ubezpieczenia', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (336, 'Zielona karta', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (337, 'Inne', 33);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (341, 'Inne', 34);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (411, 'Zawiadomienie o szkodzie', 41);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (412, 'Udzielenie informacji', 41);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (413, 'Inne', 41);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (421, 'Pe�nomocnictwo do szkody', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (422, 'Zg�oszenie szkody', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (423, 'Udzielenie informacji', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (424, 'Inne', 42);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (431, 'Zg�oszenie szkody', 43);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (432, 'Udzielenie informacji', 43);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (433, 'Inne', 43);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (441, 'Cesja do dochodzenia roszcze�', 44);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (442, 'Przed�u�ona gwarancja - szkoda', 44);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (443, 'Inne', 44);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (511, 'Rozksi�gowanie nadp�aty', 51);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (512, 'Udzielenie informacji', 51);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (513, 'Inne', 51);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (521, 'Duplikat noty odsetkowej', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (522, 'Anulowanie noty odsetkowej', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (523, 'Saldo kapita�owo odsetkowe', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (524, 'Saldo nale�no�ci', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (525, 'Zwrot nadp�aty', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (526, 'Duplikat faktury', 52);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (527, 'Inne', 52);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (531, 'Inne', 53);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (611, 'Deklaracja sp�aty', 61);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (612, 'Potwierdzenie wp�aty', 61);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (613, 'Udzielenie informacjie', 61);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (614, 'Inne', 61);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (621, 'Deklaracja sp�aty', 62);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (622, 'Potwierdzenie wp�aty', 62);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (623, 'Udzielenie informacjie', 62);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (624, 'Inne', 62);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (631, 'Deklaracja sp�aty', 63);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (632, 'Potwierdzenie wp�aty', 63);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (633, 'Udzielenie informacjie', 63);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (634, 'Inne', 63);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (641, 'Deklaracja sp�aty', 64);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (642, 'Potwierdzenie wp�aty', 64);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (643, 'Udzielenie informacjie', 64);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (644, 'Inne', 64);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (651, 'Anulowanie op�aty za wezwanie', 65);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (652, 'Duplikat faktury', 65);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (653, 'Duplikat noty odsetkowej', 65);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (654, 'Inne', 65);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (661, 'Inne', 66);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (711, 'Wznowienie umowy', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (712, 'Deklaracja sp�aty', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (713, 'Potwierdzenie wp�aty', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (714, 'Udzielenie informacji', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (715, 'Agencje zewn�trzne', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (716, 'Zwrot przedmiotu', 71);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (717, 'Inne', 71);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (721, 'Zestawienie zaleg�o�ci', 72);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (722, 'Duplikat faktury', 72);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (723, 'Duplikat noty odsetkowej', 72);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (724, 'Inne', 72);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (731, 'Udzielenie informacji', 73);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (732, 'Inne', 73);

insert into DSC_ILPOL_COK_CASE_CATEGORY values (741, 'Inne', 74);


ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD SUBJECT VARCHAR2(256 BYTE);
COMMIT;

update dsc_ilpol_cok_task_status set title='Reklamacja nies�uszna' where id=6;
commit;


alter table dsc_ilpol_cok_document add conversation_description2 clob;
update dsc_ilpol_cok_document set conversation_description2=conversation_description where conversation_description is not null;
alter table dsc_ilpol_cok_document drop column conversation_description;
alter table dsc_ilpol_cok_document rename column conversation_description2 to conversation_description;
commit;

-- Umowy aktywne -> Wnioski Klienta
insert into DSC_ILPOL_COK_CASE_CATEGORY values (129, 'U�yczenie PU na osob� trzeci�', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1201, 'Wymiana dowodu tymczasowego', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1202, 'Wt�rnik tablic rejestracyjnych', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1203, 'Wt�rnik naklejki na szyb�', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1204, 'Wt�rnik dowodu rejestracyjnego', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1205, 'Zmiany w dowodzie rejestracyjnym', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1206, 'Odbi�r zatrzymanego dowodu rejestracyjnego', 12);

-- Podmiana 30.04.2013

alter table dsc_ilpol_cok_document add sent_resp2 clob;
update dsc_ilpol_cok_document set sent_resp2=sent_resp where sent_resp is not null;
alter table dsc_ilpol_cok_document drop column sent_resp;
alter table dsc_ilpol_cok_document rename column sent_resp2 to sent_resp;
commit;

alter table dsc_ilpol_cok_document add task_solution2 clob;
update dsc_ilpol_cok_document set task_solution2=task_solution where task_solution is not null;
alter table dsc_ilpol_cok_document drop column task_solution;
alter table dsc_ilpol_cok_document rename column task_solution2 to task_solution;
commit;

-- Kategorie wrzucone na produkcj� 14 maja 2013

insert into DSC_ILPOL_COK_CASE_CATEGORY values (8, 'Inne', null);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (81, 'Inne', 8);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (811, 'Inne', 81);
commit;

-- podmiana testu 15 maja 2013
-- podmiana produkcji 29 maja 2013
CREATE TABLE DSC_ILPOL_COK_MAIL_ATT(
  ID NUMBER(19,0),
  ATT NUMBER(19,0)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_COK_MAIL_ATT_ID START WITH 1 INCREMENT BY 1
NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_MAIL_ATT
BEFORE INSERT ON DSC_ILPOL_COK_MAIL_ATT
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_MAIL_ATT_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD FAX_SEND_DATETIME TIMESTAMP (6);

CREATE TABLE "DSC_ILPOL_FAX_ACCEPTED_NUMBERS"
( "ID" NUMBER(19,0),
  "FAX_ACCEPTED_NUMBER" VARCHAR2(32 CHAR)
);
commit;


CREATE TABLE DSC_ILPOL_COK_DOCUMENT_IMPORT
(
  ID NUMBER(19,0),
  DOCUMENT_ID NUMBER(19,0) NOT NULL,
  ATTACHED_IMPORTED_DOCUMENT NUMBER(19,0)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_COK_DOC_IMPORT_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_DOC_IMPORT
BEFORE INSERT ON DSC_ILPOL_COK_DOCUMENT_IMPORT
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_DOC_IMPORT_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

-- dokument sprawa
CREATE TABLE DSC_ILPOL_CASE_DOC
(
  ID NUMBER(19,0),
  DOCUMENT_ID NUMBER(19,0) NOT NULL,
  DESCRIPTION varchar2(4000)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_CASE_DOC_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_CASE_DOC
BEFORE INSERT ON DSC_ILPOL_CASE_DOC
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_CASE_DOC_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

-- dokument zadanie
CREATE TABLE DSC_ILPOL_TASK_DOC
(
  ID NUMBER(19,0),
  DOCUMENT_ID NUMBER(19,0) NOT NULL,
  DESCRIPTION varchar2(4000)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_TASK_DOC_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_TASK_DOC
BEFORE INSERT ON DSC_ILPOL_TASK_DOC
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_TASK_DOC_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

CREATE TABLE "DSC_ILPOL_CASE_MULTIPLE"
(
  "DOCUMENT_ID" NUMBER(19,0),
  "FIELD_CN" VARCHAR2(150 BYTE),
  "FIELD_VAL" VARCHAR2(50 BYTE)
);
commit;

alter table dsc_ilpol_case_doc add CTIME TIMESTAMP (6);
alter table dsc_ilpol_case_doc add "AREA" NUMBER(19,0);
alter table dsc_ilpol_case_doc add "CATEGORY" NUMBER(19,0);
alter table dsc_ilpol_case_doc add "SUBCATEGORY" NUMBER(19,0);
alter table dsc_ilpol_case_doc add "INTERNAL_ID" VARCHAR2(17 CHAR);
commit;

alter table dsc_ilpol_cok_document add new_task_description VARCHAR2(4000);
commit;

alter table dsc_ilpol_case_doc add new_task_description VARCHAR2(4000);
commit;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT_IMPORT ADD IMPORT_SUMMARY VARCHAR2(1024 CHAR);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD FAX_NUMBER VARCHAR2(64);
COMMIT;


-- Klient wyszukiwanie po umowie
CREATE OR REPLACE FORCE VIEW "DSC_CONTRACTOR_COK" ("ID", "NAME", "FULLNAME", "NIP", "REGON", "KRS", "STREET", "CODE", "CITY", "REMARKS", "FAX", "PHONENUMBER", "PHONENUMBER2", "REGION", "NRUMOWY", "EMAIL") AS
  SELECT
    con.ID ID,
    con.NAME NAME,
    con.FULLNAME FULLNAME,
    con.NIP NIP,
    con.REGON REGON,
    con.KRS KRS,
    con.STREET STREET,
    con.CODE CODE,
    con.CITY CITY,
    con.REMARKS REMARKS,
    con.FAX FAX,
    con.PHONENUMBER PHONENUMBER,
    con.PHONENUMBER2 PHONENUMBER2,
    con.REGION REGION,
    umowa.NRUMOWY NRUMOWY,
    con.EMAIL EMAIL
  FROM
      DSC_CONTRACTOR con LEFT OUTER JOIN DSC_ILPOL_UMOWA_DICTIONARY umowa ON con.ID = umowa.CONTRACTOR_ID;
commit;
------------


update dsw_jbpm_tasklist set dockind_name='Zg�oszenie COK' where dockind_name='COK';
commit;


--------- 04-07-2013 ---------
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD SUBCATEGORY NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_CREATION_TIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_DEADLINE_TIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD FINISH_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_SUBCATEGORY NUMBER(19,0);
COMMIT;



ALTER TABLE DSC_ILPOL_TASK_DOC ADD CREATION_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD DEADLINE_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD SENDER VARCHAR2(128);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD FINISH_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD CONTRACTOR_ID NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD SUBCATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_TASK_DOC ADD TASK_SOLUTION VARCHAR2(4000);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_TASK_CONTRACTOR_ID NUMBER(19,0);
COMMIT;


--------- --------- --------- 05-07-2013 --------- --------- ---------
ALTER TABLE DSC_ILPOL_COK_CASE RENAME COLUMN CTIME to CREATION_TIME;
COMMIT;

ALTER TABLE DSC_ILPOL_COK_CASE ADD FINISH_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_CASE RENAME COLUMN DESCRIPTION to CASE_SOLUTION_DESCRIPTION;
COMMIT;

ALTER TABLE DSC_ILPOL_COK_CASE MODIFY CASE_SOLUTION_DESCRIPTION VARCHAR2(4000 CHAR);
COMMIT;

CREATE TABLE DSC_ILPOL_COK_CASE_STATUS(
  ID NUMBER(19),
  CN VARCHAR2(50 BYTE),
  TITLE VARCHAR2(255 BYTE),
  CENTRUM NUMBER(38,0),
  REFVALUE VARCHAR2(20 BYTE),
  AVAILABLE NUMBER(38,0)
);
COMMIT;

INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (1, 'Nowa', 'Nowa', 0, NULL, 1);
COMMIT;

INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (2, 'Przyj�ta do realizacji', 'Przyj�ta do realizacji', 0, NULL, 1);
COMMIT;

INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (3, 'W trakcie Realizacji', 'W trakcie Realizacji', 0,  NULL, 1);
COMMIT;

INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (4, 'Reklamacja nies�uszna', 'Reklamacja nies�uszna', 0, NULL, 1);
COMMIT;

INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (5, 'Zako�czona', 'Zako�czona', 0, NULL, 1);
COMMIT;

-- Tworzy DSC_ILPOL_COK_FAX_ATT zawieraj�c� wszystkie za��czniki typu faks
create table DSC_ILPOL_COK_FAX_ATT as select mail_att.att
                                      from dsc_ilpol_cok_mail_att mail_att
                                        inner join ds_attachment ds_att on ds_att.id=mail_att.att
                                      where ds_att.cn ='fax';
commit;

-- kasujemy za��czniki typu faks z tabeli wys�anych za��cznik�w e-mail
delete from (select mail_att.id, mail_att.att
             from dsc_ilpol_cok_mail_att mail_att
               inner join ds_attachment ds_att on ds_att.id=mail_att.att
             where ds_att.cn ='fax');

alter table DSC_ILPOL_COK_FAX_ATT add ID NUMBER(19,0);
commit;

CREATE SEQUENCE DSC_ILPOL_COK_FAX_ATT_ID START WITH 1 INCREMENT BY 1
NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_FAX_ATT
BEFORE INSERT ON DSC_ILPOL_COK_FAX_ATT
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_FAX_ATT_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

-- wstawiamy brakuj�ce ID
update dsc_ilpol_cok_fax_att set id=DSC_ILPOL_COK_FAX_ATT_ID.NEXTVAL where id is null;
commit;


-------- Update do tabeli dockinda Sprawy COK ---------
ALTER TABLE DSC_ILPOL_CASE_DOC RENAME COLUMN CTIME to CREATION_DATETIME;
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD FINISH_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC RENAME COLUMN DESCRIPTION to CASE_SOLUTION_DESCRIPTION;
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC MODIFY CASE_SOLUTION_DESCRIPTION VARCHAR2(4000 CHAR);
COMMIT;


ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_CREATION_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_DEADLINE_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD FINISH_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_AREA NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_CATEGORY NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_SUBCATEGORY NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD NEW_TASK_CONTRACTOR_ID NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD COMPLAINT NUMBER(5,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD DEALER NUMBER(5,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD CONTRACTOR_ID NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_CASE_DOC ADD STATUS VARCHAR2(64 CHAR);
COMMIT;


------------------------- 09-07-2013 -------------------------

UPDATE DSC_ILPOL_COK_CASE_STATUS SET TITLE = 'W trakcie realizacji' WHERE TITLE = 'W trakcie Realizacji';
COMMIT;

UPDATE DSC_ILPOL_COK_TASK_STATUS SET AVAILABLE = 0 WHERE TITLE = 'Reklamacja nies�uszna';
COMMIT;

UPDATE DSC_ILPOL_COK_TASK_STATUS SET AVAILABLE = 0 WHERE TITLE = 'Przekazane do realizacji';
COMMIT;

ALTER TABLE DSC_ILPOL_TASK_DOC ADD STATUS VARCHAR2(64 CHAR);
COMMIT;

ALTER TABLE DSC_ILPOL_TASK_DOC ADD RECIPIENT VARCHAR2(128 CHAR);
COMMIT;

-- na produkcji 09-07-2013
-- Umowy aktywne -> Wnioski Klienta
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1207, 'Przedmiot leasingu', 12);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (1208, 'Skan dokument�w umowy', 12);
-- Umowy aktywne -> Faktury
insert into DSC_ILPOL_COK_CASE_CATEGORY values (107, 'Potwierdzenie numeru konta', 11);
-- Umowy aktywne -> Udzielenie informacji
insert into DSC_ILPOL_COK_CASE_CATEGORY values (142, 'Organy administracji', 14);

-- Zmiany i zako�czenia um�w -> Wnioski klienta
insert into DSC_ILPOL_COK_CASE_CATEGORY values (218, 'Duplikat faktury na wykup', 21);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (219, 'Przes�anie KRS', 21);

-- Ubezpieczenia -> Wnioski klienta
insert into DSC_ILPOL_COK_CASE_CATEGORY values (338, 'Pro�ba o skan dokument�w', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (339, 'Rezygnacja z DAS', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (3301, 'Rezygnacja z Assistance', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (3302, 'Rezygnacja z GAP', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (3303, 'Rezygnacja z GAP Faktura', 33);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (3304, 'Rezygnacja z CPI', 33);
-- Ubezpieczenia -> Polisa ubezpieczeniowa
insert into DSC_ILPOL_COK_CASE_CATEGORY values (318, 'Zmiana ubezpieczyciela', 31);

-- Szkody -> Kradzie�
insert into DSC_ILPOL_COK_CASE_CATEGORY values (434, 'Zapytanie o etap likwidacji', 43);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (435, 'Rozliczenie szkody', 43);
-- Szkody -> Szkoda cakowita
insert into DSC_ILPOL_COK_CASE_CATEGORY values (414, 'Zapytanie o etap likwidacji', 41);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (415, 'Pozosta�o�ci powypadkowe', 41);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (416, 'Rozliczenie szkody', 41);
-- Szkody -> Szkoda cz�ciowa
insert into DSC_ILPOL_COK_CASE_CATEGORY values (425, 'Zapytanie o etap likwidacji', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (426, 'Jak zg�osi� szkod�', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (427, 'Warsztat', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (428, 'InerProfmax', 42);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (429, 'Rozliczenie odszkodowania', 42);

-- Rozrachunki -> Saldo nale�no�ci
insert into DSC_ILPOL_COK_CASE_CATEGORY values (514, 'Wystawienie raportu/zestawienia', 51);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (515, 'Uzgodnienie salda', 51);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (516, 'Potwierdzenie wp�aty', 51);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (517, 'Rozliczenie wp�aty', 51);

-- Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY values (82, 'Policja', 8);
-- Inne -> Policja
insert into DSC_ILPOL_COK_CASE_CATEGORY values (821, 'Wniosek o informacje', 82);
insert into DSC_ILPOL_COK_CASE_CATEGORY values (822, 'Brak odpowiedzi na pismo', 82);
---

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_CONTRACTOR_ID NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_SUBCATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_CREATION_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_FINISH_DATETIME TIMESTAMP(6);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_STATUS NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_INTERNAL_ID VARCHAR2(17 CHAR);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_COMPLAINT NUMBER(5,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_DEALER NUMBER(5,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_SOLUTION_DESCRIPTION VARCHAR2(4000 CHAR);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD DOCUMENT_CREATION_DATETIME TIMESTAMP(6);
COMMIT;

CREATE OR REPLACE VIEW DSC_ILPOL_COK_CASE_COK_DOC AS
  SELECT DOC.DOCUMENT_ID DOCUMENT_ID, DOC.DELIVERY_TYPE DELIVERY_TYPE, DS_DOC.CTIME CTIME,
         CASE DOC.DELIVERY_TYPE
         WHEN 720 THEN DOC.FAX_NUMBER
         ELSE DS_MSG.SENDER
         END SENDER,
         CASE DOC.DELIVERY_TYPE
         WHEN 700 THEN DS_MSG.SUBJECT
         ELSE DOC.SUBJECT
         END SUBJECT,
         DS_USR.FIRSTNAME || ' ' || DS_USR.LASTNAME RECEIVER
  FROM DSC_ILPOL_COK_DOCUMENT DOC
    INNER JOIN DS_DOCUMENT DS_DOC ON DS_DOC.ID=DOC.DOCUMENT_ID
    INNER JOIN DS_USER DS_USR ON DS_USR.NAME=DS_DOC.AUTHOR
    LEFT JOIN DS_MESSAGE DS_MSG ON DS_MSG.ID=DOC.MESSAGE_ID
  ORDER BY DS_DOC.CTIME ASC;
COMMIT;



CREATE TABLE DSC_ILPOL_COK_IMPORTED_DOC
(
  ID NUMBER(19,0) NOT NULL ENABLE,
  DOCUMENT_ID NUMBER(19,0),
  ATTACHED_IMPORTED_DOCUMENT NUMBER(19,0),
  CONTRACTOR_ID NUMBER(10,0),
  STATUS VARCHAR2(64),
  DEADLINE_DATETIME TIMESTAMP(6),
  SUBJECT VARCHAR2(256),
  DESCRIPTION VARCHAR2(4000),
  AREA NUMBER(19,0),
  CATEGORY NUMBER(19,0),
  SUBCATEGORY NUMBER(19,0)
);
COMMIT;

CREATE SEQUENCE DSC_ILPOL_COK_IMPORTED_DOC_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_DSC_ILPOL_COK_IMPORTED_DOC
BEFORE INSERT ON DSC_ILPOL_COK_IMPORTED_DOC
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT DSC_ILPOL_COK_IMPORTED_DOC_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;


-- 16-07-2013 Importowanie dokument�w COK
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD CONVERSATION_DESCRIPTION CLOB;
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD SENT_RESP CLOB;
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD CASE_ID NUMBER(19,0);
COMMIT;

ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_DESCRIPTION VARCHAR2(4000);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_CREATION_TIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_DEADLINE_TIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_SUBCATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_TASK_CONTRACTOR_ID NUMBER(19,0);
COMMIT;


ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_CONTRACTOR_ID NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_AREA NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_CATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_SUBCATEGORY NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_CREATION_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_FINISH_DATETIME TIMESTAMP(6);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_STATUS NUMBER(19,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_INTERNAL_ID VARCHAR2(17 CHAR);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_COMPLAINT NUMBER(5,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_DEALER NUMBER(5,0);
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD NEW_CASE_SOLUTION_DESCRIPTION VARCHAR2(4000 CHAR);
COMMIT;


ALTER TABLE DSC_ILPOL_TASK_DOC ADD CASE_ID NUMBER(19,0);
ALTER TABLE DSC_ILPOL_TASK_DOC ADD COK_ID NUMBER(19,0);
COMMIT;

CREATE TABLE DSC_ILPOL_TASK_DOC_MLT (
  "DOCUMENT_ID" NUMBER(19,0),
  "FIELD_CN" VARCHAR2(150 BYTE),
  "FIELD_VAL" VARCHAR2(50 BYTE)
);
COMMIT;

-- kana� wp�ywu jako db-enum
CREATE TABLE DSC_ILPOL_COK_DELIVERY_TYPE (
  ID	NUMBER(19),
  CN VARCHAR2(50 BYTE),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;

Insert into DSC_ILPOL_COK_DELIVERY_TYPE (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values ('700','EMAIL','Email','0','NULL','1');
Insert into DSC_ILPOL_COK_DELIVERY_TYPE (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values ('710','TELEFON','Telefon','0','NULL','1');
Insert into DSC_ILPOL_COK_DELIVERY_TYPE (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values ('720','FAKS','Faks','0','NULL','1');
Insert into DSC_ILPOL_COK_DELIVERY_TYPE (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values ('730','IMPORT','Import','0','NULL','1');
Insert into DSC_ILPOL_COK_DELIVERY_TYPE (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values ('740','DOCUMENT_IN','Korespondencja','0','NULL','1');
commit;

-- status zadania
CREATE OR REPLACE VIEW DSC_ILPOL_COK_CASE_COK_DOC AS
  SELECT DOC.DOCUMENT_ID DOCUMENT_ID, DOC.DELIVERY_TYPE DELIVERY_TYPE, DS_DOC.CTIME CTIME, DOC.STATUS STATUS,
         CASE DOC.DELIVERY_TYPE
         WHEN 720 THEN DOC.FAX_NUMBER
         ELSE DS_MSG.SENDER
         END SENDER,
         CASE DOC.DELIVERY_TYPE
         WHEN 700 THEN DS_MSG.SUBJECT
         ELSE DOC.SUBJECT
         END SUBJECT,
         DS_USR.FIRSTNAME || ' ' || DS_USR.LASTNAME RECEIVER
  FROM DSC_ILPOL_COK_DOCUMENT DOC
    INNER JOIN DS_DOCUMENT DS_DOC ON DS_DOC.ID=DOC.DOCUMENT_ID
    INNER JOIN DS_USER DS_USR ON DS_USR.NAME=DS_DOC.AUTHOR
    LEFT JOIN DS_MESSAGE DS_MSG ON DS_MSG.ID=DOC.MESSAGE_ID
  ORDER BY DS_DOC.CTIME ASC;
COMMIT;

-- nie wszystkie zadania mia�y ustawiony status
UPDATE DSC_ILPOL_TASK_DOC set STATUS=1 where STATUS IS NULL;
COMMIT;

-- status sprawy zaczyna si� od przyj�ta do realizacji
delete from DSC_ILPOL_COK_CASE_STATUS where id=1;
commit;
update DSC_ILPOL_COK_CASE_STATUS set id=1 where id=2;
commit;


-- Klient wyszukiwanie po umowie
CREATE OR REPLACE FORCE VIEW "DSC_CONTRACTOR_COK" ("ID", "NAME", "FULLNAME", "FIRSTNAME", "LASTNAME", "NIP", "REGON", "KRS", "STREET", "CODE", "CITY", "REMARKS", "FAX", "PHONENUMBER", "PHONENUMBER2", "REGION", "NRUMOWY", "EMAIL") AS
  SELECT
    con.ID ID,
    con.NAME NAME,
    con.FULLNAME FULLNAME,
    con.FIRSTNAME FIRSTNAME,
    con.LASTNAME LASTNAME,
    con.NIP NIP,
    con.REGON REGON,
    con.KRS KRS,
    con.STREET STREET,
    con.CODE CODE,
    con.CITY CITY,
    con.REMARKS REMARKS,
    con.FAX FAX,
    con.PHONENUMBER PHONENUMBER,
    con.PHONENUMBER2 PHONENUMBER2,
    con.REGION REGION,
    umowa.NRUMOWY NRUMOWY,
    con.EMAIL EMAIL
  FROM
      DSC_CONTRACTOR con LEFT OUTER JOIN DSC_ILPOL_UMOWA_DICTIONARY umowa ON con.ID = umowa.CONTRACTOR_ID;
commit;


-- 08-10-2013
UPDATE DS_PREFS_VALUE SET PREF_VALUE = 'receiveDate,dockindBusinessAtr3,dockindBusinessAtr2,dockindBusinessAtr1' WHERE PREF_KEY like 'default-columns_watches';


-- 22-08-2013
alter table DSC_ILPOL_COK_DOCUMENT_IMPORT add IMPORT_SUMMARY2 clob;
update DSC_ILPOL_COK_DOCUMENT_IMPORT set IMPORT_SUMMARY2=IMPORT_SUMMARY where IMPORT_SUMMARY is not null;
alter table DSC_ILPOL_COK_DOCUMENT_IMPORT drop column IMPORT_SUMMARY;
alter table DSC_ILPOL_COK_DOCUMENT_IMPORT rename column IMPORT_SUMMARY2 to IMPORT_SUMMARY;
commit;

-- 26-08-2013
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD REMARK CLOB;
COMMIT;
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD INFORMATION VARCHAR2(64 CHAR);
COMMIT;


-- 23-10-2013 ZMIANY DOT. WEB SERWISU ILPOL CUSTOMER SERVICE (PORTAL DOKUMENTOW)

-- NOWY KANAL WPLYWU --
INSERT INTO DSC_ILPOL_COK_DELIVERY_TYPE (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES ('750', 'PORTAL_KLIENTA', 'Portal klienta', 0, 'NULL', 1);

-- OBSZARY --
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name) values(9, 'Portal Klienta - zapytanie');
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name) values(10, 'Portal Klienta - reklamacja');
-- "skonczyl sie" zakres id 1-10 stad kolejnym "bezpiecznym" wydaje sie id o wartosci 100000,
-- przyjmuje zalozenie ze kolejne obszary beda z zakresu 100000 - 100099 co powinno wystarczy na przyszle potrzeby systemu
-- przyjmuje rowniez koncepcje, ze dla zapisu zzyyxx (100000) xx oznacza id obszaru yy kategorii zz podkategorii
-- daje to sam w sumie 100 nowych obszarow, kazda obszar ma 0-99 kategorii, kazda kategoria ma 0-99 podkategorii
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name) values(100000, 'Portal Klienta - wnioski');
COMMIT;

-- KATEGORIE --
-- Portal Klienta - reklamacja -> Reklamacja
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(101, 'Reklamacja', 10);
COMMIT;
-- Portal Klienta - zapytanie -> Umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(91, 'Umowy', 9);
-- Portal Klienta - zapytanie -> Faktury
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(92, 'Faktury', 9);
-- Portal Klienta - zapytanie -> P�atno�ci
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(93, 'P�atno�ci', 9);
-- Portal Klienta - zapytanie -> Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(94, 'Inne', 9);
COMMIT;
-- Portal Klienta - wnioski -> Umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(100100, 'Umowy', 100000);
-- Portal Klienta - wnioski -> P�atno�ci
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(100200, 'P�atno�ci', 100000);
-- Portal Klienta - wnioski -> Firma
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(100300, 'Firma', 100000);
COMMIT;

-- PODKATEGORIE --
-- Portal Klienta - reklamacja -> Reklamacja -> Reklamacja
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(1011, 'Reklamacja', 101);
COMMIT;
-- Portal Klienta - zapytanie -> Umowy -> Udzielenie informacji
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(911, 'Udzielenie informacji', 91);
-- Portal Klienta - zapytanie -> Umowy -> Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(912, 'Inne', 91);
COMMIT;
-- Portal Klienta - zapytanie -> Faktury -> Wyja�nienie dotycz�ce faktury
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(921, 'Wyja�nienie dotycz�ce faktury', 92);
-- Portal Klienta - zapytanie -> Faktury -> Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(922, 'Inne', 92);
COMMIT;
-- Portal Klienta - zapytanie -> P�atno�ci -> Udzielenie informacji
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(931, 'Udzielenie informacji', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Deklaracja sp�aty
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(932, 'Deklaracja sp�aty', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Potwierdzenie wp�aty
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(933, 'Potwierdzenie wp�aty', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Monit
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(934, 'Monit', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Wezwanie do zap�aty
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(935, 'Wezwanie do zap�aty', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Nota odsetkowa
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(936, 'Nota odsetkowa', 93);
-- Portal Klienta - zapytanie -> P�atno�ci -> Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(937, 'Inne', 93);
COMMIT;
-- Portal Klienta - zapytanie -> Inne -> Inne
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(941, 'Inne', 94);
COMMIT;
-- Portal Klienta - wnioski -> Umowy -> Duplikat faktury
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(110100, 'Duplikat faktury', 100100);
-- Portal Klienta - wnioski -> Umowy -> Harmonogram
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(120100, 'Harmonogram', 100100);
-- Portal Klienta - wnioski -> Umowy -> Opinia dotycz�ca wsp�pracy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(130100, 'Opinia dotycz�ca wsp�pracy', 100100);
-- Portal Klienta - wnioski -> Umowy -> Zgoda na podnajem
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(140100, 'Zgoda na podnajem', 100100);
-- Portal Klienta - wnioski -> Umowy -> Upowa�nienie do korzystania z przedmiotu umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(150100, 'Upowa�nienie do korzystania z przedmiotu umowy', 100100);
-- Portal Klienta - wnioski -> Umowy -> Zmiana daty p�atno�ci
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(160100, 'Zmiana daty p�atno�ci', 100100);
-- Portal Klienta - wnioski -> Umowy -> U�yczenie przedmiotu umowy na osob� trzeci�
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(170100, 'U�yczenie przedmiotu umowy na osob� trzeci�', 100100);
-- Portal Klienta - wnioski -> Umowy -> Zmiana osobowo�ci prawnej
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(180100, 'Zmiana osobowo�ci prawnej', 100100);
-- Portal Klienta - wnioski -> Umowy -> Cesja
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(190100, 'Cesja', 100100);
-- Portal Klienta - wnioski -> Umowy -> Zmiana warunk�w umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(200100, 'Zmiana warunk�w umowy', 100100);
-- Portal Klienta - wnioski -> Umowy -> Wcze�niejsze zako�czenie umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(210100, 'Wcze�niejsze zako�czenie umowy', 100100);
-- Portal Klienta - wnioski -> Umowy -> Terminowe zako�czenie umowy
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(220100, 'Terminowe zako�czenie umowy', 100100);
COMMIT;
-- Portal Klienta - wnioski -> P�atno�ci -> Duplikat noty odsetkowej
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(110200, 'Duplikat noty odsetkowej', 100200);
-- Portal Klienta - wnioski -> P�atno�ci -> Saldo kapita�owo odsetkowe
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(120200, 'Saldo kapita�owo odsetkowe', 100200);
-- Portal Klienta - wnioski -> P�atno�ci -> Saldo nale�no�ci
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(130200, 'Saldo nale�no�ci', 100200);
-- Portal Klienta - wnioski -> P�atno�ci -> Zwrot nadp�aty
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(140200, 'Zwrot nadp�aty', 100200);
COMMIT;
-- Portal Klienta - wnioski -> Firma -> Zmiana danych Klienta
insert into DSC_ILPOL_COK_CASE_CATEGORY (id, name, parent_id) values(110300, 'Zmiana danych Klienta', 100300);
COMMIT;


CREATE OR REPLACE VIEW DSC_ILPOL_COK_APPLICATION (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) AS
  SELECT ROWNUM, ROWNUM, NRUMOWY, 0, CONTRACTOR_ID, 1 FROM DSC_ILPOL_UMOWA_DICTIONARY ORDER BY DATAPODPISANIA;


ALTER TABLE DSC_ILPOL_COK_DOCUMENT ADD NEW_CASE_CONTRACT_NUMBER_ID NUMBER(19,0);
ALTER TABLE DSC_ILPOL_CASE_DOC ADD CONTRACT_NUMBER_ID NUMBER(19,0);
COMMIT;

-- AKTUALIZACJA WIDOKU DLA ENUMA UMOW DLA BAZY TESTOWEJ (!!!!!!!!!!!!!!!)
CREATE OR REPLACE FORCE VIEW DSC_ILPOL_COK_APPLICATION ("ID", "CN", "TITLE", "CENTRUM", "REFVALUE", "AVAILABLE") AS
  SELECT leo.idumowy, leo.idumowy, leo.nrumowy, 0, c.idklienta, 1 FROM leonew.vumowy leo INNER JOIN dl_contract_dictionary c ON c.numerumowy=leo.nrumowy;
-- AKTUALIZACJA WIDOKU DLA ENUMA UMOW DLA BAZY PRODUKCYJNEJ (!!!!!!!!!!!!!!!)
CREATE OR REPLACE FORCE VIEW DSC_ILPOL_COK_APPLICATION ("ID", "CN", "TITLE", "CENTRUM", "REFVALUE", "AVAILABLE") AS
  SELECT leo.idumowy, leo.idumowy, leo.nrumowy, 0, c.idklienta, 1 FROM sysdba.vumowy leo INNER JOIN dl_contract_dictionary c ON c.numerumowy=leo.nrumowy;

COMMIT;


-- 22-11-2013 DODANIE PARAMETRYZACJI KONFIGURACJI KANALU EMAIL
-- informacje o obszarze, kategorii, podkategorii ktore maja zostac ustawione dla Zgloszenia ICS tworzonego z mailow otrzymanych z kanalu email
CREATE TABLE ILPL_COK_DOC_EMAIL_CHANNEL (
  ID NUMBER(19,0),
  EMAIL_CHANNEL_ID NUMBER(19,0),
  DIVISION_ID NUMBER(19,0),
  AREA_ID NUMBER(19,0),
  CATEGORY_ID	NUMBER(19,0),
  SUBCATEGORY_ID	NUMBER(19,0)
);
COMMIT;

CREATE SEQUENCE ILPL_COK_DOC_EMAIL_CHANNEL_ID  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE;
COMMIT;

CREATE OR REPLACE
TRIGGER TRG_ILPL_COK_DOC_EMAIL_CHANNEL
BEFORE INSERT ON ILPL_COK_DOC_EMAIL_CHANNEL
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT ILPL_COK_DOC_EMAIL_CHANNEL_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END;

COMMIT;

-- DODANIE TABELI Z INDYWIDUALNYMI STATUSAMI DLA DOKUMENTU IMPORT COK
-- AKTUALIZACJA STATUSU DOKUMENTOW O NIEDOSTEPNYCH JUZ WARTOSCIACH "Przyj�te do realizacji" oraz "Zako�czone" na "Zako�czone - zap�acone"
CREATE TABLE ILPL_COK_IMPORTED_DOC_STATUS (
  ID	NUMBER(19,0),
  CN	NUMBER(19,0),
  TITLE	VARCHAR2(250 BYTE),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64 BYTE),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;


INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('1','1','Nowe','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('2','2','Przyj�te do realizacji','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('3','3','W trakcie realizacji','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('4','4','Przekazane do realizacji','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('5','5','Zako�czone','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('6','6','Zako�czone - brak kontaktu','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('7','7','Zako�czone - do wypowiedzenia','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES ('8','8','Zako�czone - zap�acone','0',null,'1');
COMMIT;

UPDATE DSC_ILPOL_COK_IMPORTED_DOC SET STATUS = '8' WHERE STATUS IN ('2', '5');
COMMIT;


-- 26-11-2013
-- Aktualizacja widok�w o kolumn� numer rejestracyjny klienta w vumowy
-- vumowy dla testu
-- sysdba.vumowy dla produkcji
CREATE OR REPLACE FORCE VIEW DSC_ILPOL_UMOWA_DICTIONARY ("ID", "CONTRACTOR_ID", "NRUMOWY", "DATAPODPISANIA", "DATAAKTYWACJI", "DATAZAKONCZENIA", "STATUS", "NAZWAPRZEDMIOTU", "TYPPRZEDMIOTU", "NRWNIOSKU", "NUMERREJ") AS
  SELECT DISTINCT(leo.idumowy) id,
                 c.idklienta contractor_id,
                 leo.nrumowy nrumowy,
                 leo.datapodpisania datapodpisania,
                 leo.dataaktywacji dataaktywacji,
                 leo.datazakonczenia datazakonczenia,
                 leo.status status,
                 leo.nazwaprzedmiotu nazwaprzedmiotu,
                 leo.typprzedmiotu typprzedmiotu,
                 leo.nrwniosku nrwniosku,
                 leo.numerrej numerrej
  FROM vumowy leo
    INNER JOIN dl_contract_dictionary c
      ON c.numerumowy = leo.nrumowy
;

CREATE OR REPLACE FORCE VIEW DSC_CONTRACTOR_COK ("ID", "NAME", "FULLNAME", "NIP", "REGON", "KRS", "STREET", "CODE", "CITY", "REMARKS", "FAX", "PHONENUMBER", "PHONENUMBER2", "REGION", "NRUMOWY", "EMAIL", "NUMERREJ") AS
  SELECT
    con.ID ID,
    con.NAME NAME,
    con.FULLNAME FULLNAME,
    con.NIP NIP,
    con.REGON REGON,
    con.KRS KRS,
    con.STREET STREET,
    con.CODE CODE,
    con.CITY CITY,
    con.REMARKS REMARKS,
    con.FAX FAX,
    con.PHONENUMBER PHONENUMBER,
    con.PHONENUMBER2 PHONENUMBER2,
    con.REGION REGION,
    umowa.NRUMOWY NRUMOWY,
    con.EMAIL EMAIL,
    umowa.NUMERREJ NUMERREJ
  FROM
      DSC_CONTRACTOR con LEFT OUTER JOIN DSC_ILPOL_UMOWA_DICTIONARY umowa ON con.ID = umowa.CONTRACTOR_ID
;

-- Monitoring p�atno�� dodanie kolumny zaleg�o�� pierwotna
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD ORIGINAL_ARREAR NUMBER;
COMMIT;
-- Monitoring p�atno�� dodanie kolumny zaleg�o�� aktualna
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD CURRENT_ARREAR NUMBER;
COMMIT;

-- 29-11-2013
-- MONITORING PLATNOSC
-- SLOWNIK HISTORII KONTAKTOW Z KLIENTEM
-- ENUM RODZAJ KONTAKTU
-- ENUM STATUS KONTAKTU
CREATE TABLE ILPL_CLIENT_CONTACT_TYPE(
  ID	NUMBER(19),
  CN	NUMBER(19),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;

CREATE SEQUENCE ILPL_CLIENT_CONTACT_TYPE_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_ILPL_CLIENT_CONTACT_TYPE
BEFORE INSERT ON ILPL_CLIENT_CONTACT_TYPE
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT ILPL_CLIENT_CONTACT_TYPE_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

INSERT INTO ILPL_CLIENT_CONTACT_TYPE (CN, TITLE, CENTRUM, AVAILABLE) VALUES (1, 'Faks', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_TYPE (CN, TITLE, CENTRUM, AVAILABLE) VALUES (2, 'Mail', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_TYPE (CN, TITLE, CENTRUM, AVAILABLE) VALUES (3, 'Telefon', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_TYPE (CN, TITLE, CENTRUM, AVAILABLE) VALUES (4, 'Wizyta', 0, 1);
COMMIT;


CREATE TABLE ILPL_CLIENT_CONTACT_STATUS(
  ID	NUMBER(19),
  CN	NUMBER(19),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;

CREATE SEQUENCE ILPL_CLIENT_CONTACT_STATUS_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_ILPL_CLIENT_CONTACT_STATUS
BEFORE INSERT ON ILPL_CLIENT_CONTACT_STATUS
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT ILPL_CLIENT_CONTACT_STATUS_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (1, 'Nie ma takiego nr', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (2, 'Nie odbiera', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (3, 'Zaj�te', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (4, 'Poza zasi�giem', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (5, 'Wy��czony telefon', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (6, 'Poczta pro�ba o kontakt', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (7, 'Deklaracja sp�aty', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (8, 'Do wyja�nienia', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (9, 'Do wypowiedzenia', 0, 1);
INSERT INTO ILPL_CLIENT_CONTACT_STATUS (CN, TITLE, CENTRUM, AVAILABLE) VALUES (10, 'Zap�acone', 0, 1);
COMMIT;

CREATE TABLE ILPL_COK_CLIENT_CONTACT_DIC
(
  ID NUMBER(19,0) NOT NULL ENABLE,
  DOCUMENT_ID NUMBER(19,0),
  AUTHOR VARCHAR2(128),
  CONTACT_TYPE NUMBER(19,0),
  CONTACT_STATUS NUMBER(19,0),
  CONTACT_DATE TIMESTAMP(6),
  DESCRIPTION VARCHAR2(1024),
  REPAYMENT_DATE TIMESTAMP(6)
);
COMMIT;

CREATE SEQUENCE ILPL_COK_CLIENT_CONTACT_DIC_ID START WITH 1 INCREMENT BY 1 NOCACHE;
COMMIT;

CREATE OR REPLACE TRIGGER TRG_ILPL_COK_CLIENT_CONTACT
BEFORE INSERT ON ILPL_COK_CLIENT_CONTACT_DIC
FOR EACH ROW
  BEGIN
    IF :NEW.ID IS NULL THEN
      SELECT ILPL_COK_CLIENT_CONTACT_DIC_ID.NEXTVAL INTO :NEW.ID FROM DUAL;
    END IF;
  END
    COMMIT;

-- 12-03-2013
-- Monitoring Platnosci: dodanie kolumny przechowujacej temat wiadomosci przeslanej do klienta
ALTER TABLE DSC_ILPOL_COK_IMPORTED_DOC ADD RESPONSE_SUBJECT	VARCHAR2(256 BYTE);
COMMIT;


CREATE TABLE ILPL_COK_IMPORTED_DOC_STATUS
(	"ID" NUMBER(19,0),
   "CN" NUMBER(19,0),
   "TITLE" VARCHAR2(250 BYTE),
   "CENTRUM" NUMBER(5,0),
   "REFVALUE" VARCHAR2(64 BYTE),
   "AVAILABLE" NUMBER(5,0)
);
COMMIT;

INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('1','1','Nowe','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('2','2','Przyj�te do realizacji','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('3','3','W trakcie realizacji','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('4','4','Przekazane do realizacji','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('5','5','Zako�czone','0',null,'0');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('6','6','Zako�czone - brak kontaktu','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('7','7','Zako�czone - do wypowiedzenia','0',null,'1');
INSERT INTO ILPL_COK_IMPORTED_DOC_STATUS (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) VALUES('8','8','Zako�czone - zap�acone','0',null,'1');
COMMIT;

-- 06-12-2013
-- w zwi�zku ze zmian� nazwy u�ytkownika w dockindzie cok_imported_doc z Import COK na Monitoring P�atno�ci trzeba odpowiednio zaktualizowa� wpisy na li�cie zada� aby dokumenty o nieaktualnym typie Import COK wyswietlily sie w zakladce Monitoing P�atno�ci
UPDATE DSW_JBPM_TASKLIST SET DOCKIND_NAME = 'Monitoring P�atno�ci' WHERE DOCKIND_NAME = 'Import COK';
COMMIT;


-- 12-12-2013
-- Monitoring P�atno�ci - dodanie kolumny informuj�cej czy wpis do s�ownika historii kontakt�w z klientem zosta� pomy�lnie wprowadzony do bazy danych
-- informacja jest wykorzystywana z powodu zlozonego przypadku automatycznego dodawania wpisu do slownika gdy uzytkownik wysyla maila do kontrahenta
ALTER TABLE ILPL_COK_CLIENT_CONTACT_DIC ADD ALREADY_PERSISTED	NUMBER(1,0);
COMMIT;


-- 17-12-2013
-- nowe statusy dla sprawy
INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (6, 'Zako�czona Akceptacja', 'Zako�czona Akceptacja', 0, null, 1);
INSERT INTO DSC_ILPOL_COK_CASE_STATUS (ID, CN, TITLE, CENTRUM, REFVALUE, AVAILABLE) VALUES (7, 'Zako�czona Odmowa', 'Zako�czona Odmowa', 0, null, 1);
COMMIT;

-- kategoria do obszaru 'Ubezpieczenia'
INSERT INTO DSC_ILPOL_COK_CASE_CATEGORY VALUES (35, 'Odst�pstwa', 3);
COMMIT;
-- podkategorie do kategorii 'Odst�pstwa'
INSERT INTO DSC_ILPOL_COK_CASE_CATEGORY VALUES (350, 'Decyzja IIPOL', 35);
INSERT INTO DSC_ILPOL_COK_CASE_CATEGORY VALUES (351, 'Decyzja TU', 35);
COMMIT;
-- aktualizacja listy zada� z powodu zmiany nazwy typu dokumentu z 'Importowanie COK' na 'Import Monitoring P�atno�ci'
UPDATE DSW_JBPM_TASKLIST SET DOCKIND_NAME = 'Import Monitoring P�atno�ci' WHERE DOCKIND_NAME = 'Importowanie COK';
COMMIT;

-- z teczki mozna stworzy� zadanie
ALTER TABLE DSC_ILPOL_TASK_DOC ADD BINDER_ID NUMBER(19,0);
COMMIT;

-- 31-12-2013
-- usuni�cie ju� nie u�ywanej kolumny ze s�ownika historii kontakt�w z klientem (u�ywany w Monitoringu P�atno�ci)
ALTER TABLE ILPL_COK_CLIENT_CONTACT_DIC DROP COLUMN ALREADY_PERSISTED;
COMMIT;

-- 14-01-2014
-- dodanie pola przechowujacego date stworzenia wpisu (generowana automatycznie po zmianie wartosci ktoregokolwiek z pol w wierszu slownika, pod warunkiem ze data stworzenia jeszcze nie widnieje w wierszu) do slownika historii kontaktow z klientem
ALTER TABLE ILPL_COK_CLIENT_CONTACT_DIC ADD CREATION_DATE	TIMESTAMP(6);
COMMIT;