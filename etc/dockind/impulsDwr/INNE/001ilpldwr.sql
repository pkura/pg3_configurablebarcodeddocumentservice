--wykonac skrypty
--wgrac procesy
-- dodac na drzewie akceptacje DR oraz DO
-- zapisac akceptacje
-- zwiekszyc koluimne kwota w taasklist
-- wylaczyc migration, usunac z kodu przed produkcja wylaczenie jhistoriui dla migration 
--dodac dokindy


alter table dsw_jbpm_tasklist modify dockind_kwota number(36,2);
alter table dsg_leasing add leasing_doc integer;
alter table dsg_leasing add INIT integer;
ALTER TABLE dsg_dlbinder MODIFY workflow_place NUMERIC (18,0) Default NULL;
ALTER TABLE dsw_jbpm_tasklist ADD resource_count INTEGER DEFAULT 1 NOT NULL;
ALTER TABLE dsw_jbpm_tasklist ADD markerClass varchar(30 char);
alter table dsg_leasing add MASTER_DOC numeric(18,0);
ALTER TABLE dsw_jbpm_tasklist modify markerClass varchar(30 char);
ALTER TABLE dsw_jbpm_tasklist modify dockind_kwota numeric(30,2);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (1,'root','root','root','division',null,null);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (401,'xxx', 'Dzia� Sprzeda�y','DS','ac.acceptance',1,1);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (402,'xxx','Dzia� Operacyjny','DO','ac.acceptance',1,1);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (403,'xxx','Dzia� Ryzyka','DR','ac.acceptance',1,1); 

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (404,'xxx','Dzia� Ksi�gowego','DK','ac.acceptance',1,1);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (405,'DL_BINDER_DK','Teczka Dept. Finans�w i Ksi�gowo�ci','','ds.division',404,0);

insert into ds_acceptance_division (id, guid, name, code, divisiontype, parent_id, ogolna) 
	values (406,'DL_BINDER_DO','Teczka Dept. Operacyjny','','ds.division',402,0);


update ds_division set parent_id = 235 where id = 151;
update ds_division set parent_id = 235 where id = 171;
update ds_division set parent_id = 220 where id = 139;
update ds_division set parent_id = 220 where id = 294;
update ds_division set parent_id = 404 where id = 149;
update ds_division set parent_id = 404 where id = 432;
update ds_division set parent_id = 225 where id = 167;
update ds_division set parent_id = 225 where id = 148;
update ds_division set parent_id = 88 where id = 143  ;
update ds_division set parent_id = 88 where id =397;
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values(ds_division_id.nextval,'DV_1101','Region Mazowiecki-Zach�d','division',408,0);
update ds_division set parent_id = 408 where id = 168;
update ds_division set parent_id = 221 where id = 138;
update ds_division set parent_id = 221 where id = 145;
update ds_division set parent_id = 221 where id = 141;
update ds_division set parent_id = 222 where id = 146;
update ds_division set parent_id = 222 where id = 144;
update ds_division set parent_id = 224 where id = 170;
update ds_division set parent_id = 224 where id = 157;
update ds_division set parent_id = 223 where id = 213;
update ds_division set parent_id = 223 where id = 140;
update ds_division set parent_id = 164 where id = 238;
update ds_division set name = 'Region Mazowiecki-Centralny' where guid = 'DV_1082';
update ds_division set name = 'Region Lubuski' where guid = 'DV_171';
update ds_division set name = 'Region �l�ski-P�noc' where guid = 'DV_SLA';

update ds_division set parent_id = 201 where parent_id = 172;

update ds_division set hidden = 1 where id in(4,12,5,33);
update ds_division set divisiontype = 'division' where id in (161,162,163,164,207);
update ds_division set parent_id = 161 where parent_id = 4;
update ds_division set parent_id = 162 where parent_id = 12;
update ds_division set parent_id = 163 where parent_id = 5;
update ds_division set parent_id = 164 where parent_id = 33;
update ds_division set hidden = 1 where guid = 'AC100112011F84E7122C0B8B2BDA0F0FEFE';
update ds_division set divisiontype = 'division' where guid like 'DV_%';

update dsw_jbpm_tasklist set last_user = (select max (t2.dsw_process_context_param_val) from dsw_tasklist t2 where t2.dso_document_id = dsw_jbpm_tasklist.document_id );
update dsw_jbpm_tasklist set last_state_time = (select max (t2.dsw_activity_laststatetime) from dsw_tasklist t2 where t2.dso_document_id = dsw_jbpm_tasklist.document_id );
update dsw_jbpm_tasklist set incoming_date = (select max (t2.incomingdate) from dsw_tasklist t2 where t2.dso_document_id = dsw_jbpm_tasklist.document_id );
update dsw_jbpm_tasklist set case_id = (select max (t2.dsw_assignment_accepted) from dsw_tasklist t2 where t2.dso_document_id = dsw_jbpm_tasklist.document_id );
update ds_watchlist set urn = (select max('document:'||t.dso_document_id||',in,1') from dsw_tasklist t where ds_watchlist.urn  = 'newtask:internal,'||t.dsw_activity_activity_key);


-- do przetestowania , potrzeby bedzie potem jeszcze jakis zjoinowany update
--SELECT *  FROM    jbpm4_participation WHERE task_ IN 
--(SELECT DBID_ FROM jbpm4_task WHERE execution_ IN 
--(SELECT execution_ FROM jbpm4_variable WHERE key_='doc-id' AND long_value_ IN 
--(SELECT dso_document_id FROM dsw_tasklist WHERE dsw_assignment_accepted= 1)))
