create table dsc_patron
(
  id numeric(18,0),
  contractorId numeric(18,0),
  username varchar2(50),
  own varchar2(50)
);
alter table dsc_patron add POSN integer;
create sequence dsc_patron_id;

create table dsc_contractor_to_patron
(
  patronId numeric(18,0),
  contractorId numeric(18,0)
);