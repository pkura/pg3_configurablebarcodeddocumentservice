Pismo przychodz�ce (normal.xml) ma podane dzia� dekretacji domy�lne w polu DOC_TYPE jako parametr arg1="Nazwa dzia�u".
Nale�y zamieni� arg1="Nazwa dzia�u" na arg1=";d:GUID_DZIA�U", �eby DocuSafe nie musia� szuka� po nazwie dzia�u, tylko mia�a od razu podany GUID.
Do zamiany s�u�y skrypt "./INNE/zamiana dzia��w na guid w normalxml (x86).ps1" napisany w PowerShell 1 (wersja 32 bitowa).
Skrypt szuka dzia��w po nazwie, wyci�ga GUID i podmienia plik normal.xml.
Skrypt nale�y uruchamia� z katalogu "./INNE" - w pierwszych linijkach s� zmienne do po��czenia do bazy danych (url, username, password)
i nazwa zamienianego pliku xml.
