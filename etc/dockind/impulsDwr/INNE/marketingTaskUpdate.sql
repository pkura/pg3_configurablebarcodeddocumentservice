alter table DSC_Contractor add osob_odpowiedz integer;
create view dsg_impuls_object_type as select DL_OBJECT_TYPE.id as ID, DL_OBJECT_TYPE.ID as CN, DL_OBJECT_TYPE.NAME AS TITLE, 
	'0' AS CENTRUM, NULL AS refValue, '1' as available from DL_OBJECT_TYPE;

create view dsg_iimpuls_contact_kind as select DSC_contact_kind.id as ID, DSC_contact_kind.CN as CN, DSC_contact_kind.NAME AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available from DSC_contact_kind;
create view dsg_iimpuls_status_kind as select DSC_status_kind.id as ID, DSC_status_kind.CN as CN, DSC_status_kind.NAME AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available from DSC_status_kind;
create view dsg_iimpuls_region as select DSC_region.id as ID, DSC_region.CN as CN, DSC_region.NAME AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available from DSC_region;


create view dsg_iimpuls_typ3 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE from dsg_typ where refvalue = 3;
create view dsg_iimpuls_typ21 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 21;
create view dsg_iimpuls_typ22 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 22;
create view dsg_iimpuls_typ23 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 23;
create view dsg_iimpuls_typ24 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 24;
create view dsg_iimpuls_typ25 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 25;
create view dsg_iimpuls_typ26 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 26;
create view dsg_iimpuls_typ27 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 27;
create view dsg_iimpuls_typ28 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 28;
create view dsg_iimpuls_typ29 as select dsg_typ.id, dsg_typ.CN, dsg_typ.TITLE, dsg_typ.CENTRUM, dsg_typ.refValue, dsg_typ.AVAILABLE  from dsg_typ where refvalue = 29;
	
ALTER TABLE ds_document_acceptance ADD  automatic number(1) default 0 not null;	

  INSERT INTO ds_acceptance_division
           (ID
           ,GUID
           ,NAME
           ,CODE
           ,DIVISIONTYPE
           ,PARENT_ID
           ,ogolna)
     VALUES
           (ds_acceptance_division_id.nextval
           ,'root'
           ,'root'
           ,'root'
           ,'division'
           ,null
           ,null );      
	
--create view dsg_iimpuls_application as select DL_CONTRACT_DICTIONARY.id as ID, 
 -- DL_CONTRACT_DICTIONARY.numerUmowy as CN, 
  --'Nr: ' || DL_CONTRACT_DICTIONARY.numerUmowy || ' ' || DL_CONTRACT_DICTIONARY.opisUmowy AS TITLE, 
  --'0' AS CENTRUM, NULL AS refValue, '1' as available from DL_CONTRACT_DICTIONARY;

--create view dsg_iimpuls_contract as select DL_APPLICATION_DICTIONARY.id as ID, 
 -- DL_APPLICATION_DICTIONARY.numerWniosku as CN, 
  --'Nr: ' || DL_APPLICATION_DICTIONARY.numerWniosku || ' ' || DL_APPLICATION_DICTIONARY.opis AS TITLE, 
  --'0' AS CENTRUM, NULL AS refValue, '1' as available from DL_APPLICATION_DICTIONARY;
