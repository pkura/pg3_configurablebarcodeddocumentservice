﻿

$serverUrl = "127.0.0.1/ORCL"
$username = "ILPL2"
$password = "ilpl2"

 







$xmlFileName = '..\normal.xml'
$xmlOutFileName = "$pwd\..\out.xml"
$divisions = @{}
$fieldCns = @("ILPOL_KIND")


Add-Type -path ".\Oracle.ManagedDataAccess.dll"


#####################
Function GetGuids {
    param($con)
    
    $cmd = $con.CreateCommand()
    $cmd.CommandText = "select name,guid from ds_division where hidden=0";
    "Query: {0}" -f ` $cmd.CommandText

    if($con.State -ne 'Open') {
        $con.Open()
    }
    $reader = $cmd.ExecuteReader()
    

    while($reader.Read()) {
        $divisions.Add($reader.GetString(0), $reader.getString(1))
    }
    $con.Close()
}

Function UpdateDockindXML {
    $file = Get-Item $xmlFileName

    [xml] $xml = Get-Content $file

    foreach ($fieldCn in $fieldCns) {
        $enumItems = $xml.SelectNodes("//doctype/fields/field[@cn=`"$fieldCn`"]/type/enum-item[@arg1]")
        foreach ($enumItem in $enumItems) {
            if ($divisions.ContainsKey($enumItem.arg1)) {
                $divisionName = $enumItem.arg1
                $divisionGuid = ";d:" + $divisions.Item($divisionName)
                "Replacing $divisionName with $divisionGuid"
                $enumItem.arg1 = $divisionGuid
            } else {
                "Could not find division name {0}" -f $enumItem.arg1
            }
        }
    }

    
    $xml.Save($xmlOutFileName)

    "Saved to $xmlOutFileName"
}

#########
# MAIN

try {
    "Connecting to database $serverUrl by user $username"
    $con = New-Object Oracle.ManagedDataAccess.Client.OracleConnection("User Id=$username;Password=$password;Data Source=$serverUrl")
    $con.Open()
    "Connected to database: {0} running on host: {1} - Servicename: {2} - Subversion: {3}" -f `
    $con.DatabaseName, $con.HostName, $con.ServiceName, $con.ServerVersion
   
} catch {
    Write-Error("Cannot open connection: {0}`n{1}" -f `
    $con.ConnectionString, $_.Exception.ToString())
} finally {
    if($con.State -eq 'Open') {
        $con.Close()
    }
}
GetGuids($con)
UpdateDockindXML





