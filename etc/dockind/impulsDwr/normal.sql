alter table dsg_normal_dockind add ilpol_kind numeric(19,0);
commit;

CREATE TABLE DSC_ILPOL_NORMAL_DOCKIND_TYPE (
  ID	NUMBER(19),
  CN	NUMBER(19),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;

insert into dsc_ilpol_normal_dockind_type values(1, '1', 'FV Kosztowa', 0, NULL, 1);
insert into dsc_ilpol_normal_dockind_type values(2, '2', 'Reklamacja klienta', 0, NULL, 1);
insert into dsc_ilpol_normal_dockind_type values(3, '4', 'Nota koryguj�ca', 0, NULL, 1);
insert into dsc_ilpol_normal_dockind_type values(4, '4', 'Wznowienie UL', 0, NULL, 1);
commit;

insert into dsc_ilpol_normal_dockind_type values(5, '5', 'Zg�oszenie COK', 0, NULL, 1);
commit;

drop table DSC_ILPOL_NORMAL_DOCKIND_TYPE;
commit;

update dso_out_document_delivery set name='Pos�aniec' where lower(name) = 'kurier';
commit;

insert into dso_in_document_delivery (id, posn, name) values (9, 9, 'IN POST');
commit;

alter table dsg_normal_dockind add ilpol_client numeric(1,0);
commit;

-- Spos�b odbioru
CREATE TABLE DSC_NORMAL_DOCKIND_RECEIVE (
  ID	NUMBER(19),
  CN	NUMBER(19),
  TITLE	VARCHAR2(250),
  CENTRUM	NUMBER(5,0),
  REFVALUE	VARCHAR2(64),
  AVAILABLE	NUMBER(5,0)
);
COMMIT;
insert into DSC_NORMAL_DOCKIND_RECEIVE values(1, '1', 'Osobi�cie', 0, NULL, 1);
insert into DSC_NORMAL_DOCKIND_RECEIVE values(2, '2', 'Archiwizacja', 0, NULL, 1);
commit;

alter table dsg_normal_dockind add DOC_RECEIVE numeric(19,0);
alter table dsg_normal_dockind add BOX_NUMBER varchar2(50);
commit;

update dsg_normal_dockind set ilpol_client=0 where ilpol_client is null;
alter table dsg_normal_dockind modify ilpol_client default 0 not null;
commit;

create or replace trigger TRG_DSG_NORMAL_DOCKIND_CLIENT
before insert or update on DSG_NORMAL_DOCKIND
for each row
begin
  if :new.ilpol_client is null then
    :new.ilpol_client := 0;
  end if;
end;
commit;