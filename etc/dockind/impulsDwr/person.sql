-- scalanie dso_person z dso_contractor

-- kopia DSO_PERSON
create table dso_person_KOPIA as select * from dso_person;

-- tymczasowa tabela przechowująca znalezionych kontrahentów ze slownika person
create table tmp_person_contractor as
select person.id person_id, contractor.id contractor_id, person.street street, person.location location,
person.organization person_organization, contractor.fullname contractor_fullname, contractor.nip
from dso_person person
inner join dsc_contractor contractor on lower(person.street) = lower(contractor.street)
and lower(person.location) = lower(contractor.city)
and person.zip = contractor.code
and contractor.fullname is not null
order by contractor_id;
commit;

-- usuwamy wszystkie wpisy o takim samym person_id
delete from tmp_person_contractor
where person_id in(select person_id from tmp_person_contractor group by person_id having count(person_id) > 1);
commit;

-- ustawiamy NIP i nazwę w slowniku PERSON
merge into dso_person person
using tmp_person_contractor tmp
on (tmp.person_id = person.id)
when matched then
update set person.nip = tmp.nip, person.organization = tmp.contractor_fullname,
person.wparam = tmp.contractor_id;
commit;

-- kasujemy tymczasową tabelę
drop table tmp_person_contractor;
commit;





