
CREATE TABLE DS_ADM_OUT_DOC_MULTIPLE(
	ID bigint  NOT NULL,
    DOCUMENT_ID bigint NOT NULL,
    FIELD_CN character varying(100) NOT NULL,
    FIELD_VAL character varying(100) NULL
);
create sequence  DS_ADM_OUT_DOC_MULTIPLE_ID start with 1 ;




CREATE OR REPLACE FUNCTION insert_to_DS_ADM_OUT_DOC_MULTIPLE()
 RETURNS "trigger" AS
$BODY$
 BEGIN
 IF NEW.id IS NULL THEN
   New.id:=nextval('DS_ADM_OUT_DOC_MULTIPLE_ID');
   Return NEW;
 END IF ;
 END;
 $BODY$

 LANGUAGE 'plpgsql' VOLATILE;
commit;

 CREATE TRIGGER TRG_DS_ADM_OUT_DOC_MULTIPLE
 BEFORE INSERT
 ON DS_ADM_OUT_DOC_MULTIPLE
 FOR EACH ROW
 EXECUTE PROCEDURE insert_to_DS_ADM_OUT_DOC_MULTIPLE();
commit;




CREATE TABLE DS_ADM_IN_DOC(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	ANONIM boolean,
	DSUSER numeric(19, 0) not null,
	DSDIVISION numeric(19, 0) not null
) ;

CREATE TABLE DS_ADM_OUT_DOC(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
		ANONIM boolean,
		DSUSER numeric(19, 0) not null,
		DSDIVISION numeric(19, 0) not null
) ;

CREATE TABLE DS_ADM_INT_DOC(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	ANONIM boolean,
	DSUSER numeric(19, 0) not null,
	DSDIVISION numeric(19, 0) not null
);