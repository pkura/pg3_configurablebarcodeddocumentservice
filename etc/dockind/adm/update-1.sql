--mssql 
CREATE TABLE DS_ADM_OUT_DOC_MULTIPLE(
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE [DS_ADM_IN_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	
) ON [PRIMARY]
CREATE TABLE [DS_ADM_OUT_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	
) ON [PRIMARY]

CREATE TABLE [DS_ADM_INT_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	
) ON [PRIMARY]

alter table DSO_IN_DOCUMENT add contractId numeric (19,0);
alter table DS_ADM_IN_DOC add ANONIM smallint;

alter table DS_ADM_IN_DOC add DSUSER numeric(19, 0) not null;
alter table DS_ADM_IN_DOC add DSDIVISION numeric(19, 0) not null;


alter table DS_ADM_OUT_DOC add DSUSER numeric(19, 0) not null;
alter table DS_ADM_OUT_DOC add DSDIVISION numeric(19, 0) not null;

alter table DS_ADM_INT_DOC add DSUSER numeric(19, 0) not null;
alter table DS_ADM_INT_DOC add DSDIVISION numeric(19, 0) not null;


alter table DS_ADM_IN_DOC add ANONIM smallint;
alter table DS_ADM_OUT_DOC add ANONIM smallint;
alter table DS_ADM_INT_DOC add ANONIM smallint;
--oracle 

CREATE TABLE DS_ADM_OUT_DOC_MULTIPLE
   (	ID NUMBER(19,0) NOT NULL ENABLE, 
DOCUMENT_ID NUMBER(19,0) NOT NULL, 
FIELD_CN VARCHAR2(100 BYTE)NOT NULL , 
FIELD_VAL VARCHAR2(100 BYTE)
   );
   



create sequence DS_ADM_OUT_DOC_MULTIPLE_ID start with 1 increment by 1 nocache;


CREATE TABLE DS_ADM_OUT_DOC_MULTIPLE(
	ID int  NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
create or replace trigger TRG_DS_ADM_OUT_DOC_MULTIPLE
before insert on DS_ADM_OUT_DOC_MULTIPLE
for each row
begin
  if :new.id is null then
  select DS_ADM_OUT_DOC_MULTIPLE_ID.nextval into :new.id from dual;
  end if;
end


create sequence  DS_ADM_OUT_DOC_MULTIPLE_ID start with 1 nocache;
drop table DS_ADM_IN_DOC;
CREATE TABLE DS_ADM_IN_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	ANONIM smallint,
	DSUSER NUMBER(19, 0) not null,
	DSDIVISION NUMBER(19, 0) not null
) 
drop table DS_ADM_OUT_DOC;
CREATE TABLE DS_ADM_OUT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
		ANONIM smallint,
		DSUSER NUMBER(19, 0) not null,
		DSDIVISION NUMBER(19, 0) not null
) 
drop table DS_ADM_INT_DOC;
CREATE TABLE DS_ADM_INT_DOC(
	DOCUMENT_ID NUMBER(18, 0) NOT NULL,
	ANONIM smallint,
	DSUSER NUMBER(19, 0) not null,
	DSDIVISION NUMBER(19, 0) not null
)

alter table DSO_IN_DOCUMENT add contractId numeric (18,0);
alter table DS_ADM_IN_DOC add ANONIM smallint;


alter table DS_ADM_IN_DOC add DSUSER NUMBER(19, 0) not null;
alter table DS_ADM_IN_DOC add DSDIVISION NUMBER(19, 0) not null;
alter table DS_ADM_IN_DOC add REFERENT NUMBER(19, 0) not null;
alter table DS_ADM_OUT_DOC add DSUSER NUMBER(19, 0) not null;
alter table DS_ADM_OUT_DOC add DSDIVISION NUMBER(19, 0) not null;
alter table DS_ADM_INT_DOC add DSUSER NUMBER(19, 0) not null;
alter table DS_ADM_INT_DOC add DSDIVISION NUMBER(19, 0) not null;

