drop table dsg_pig_t_funding_source;
drop view dsg_pig_topic_f_source_view;

  CREATE TABLE dsg_pig_t_funding_source
   (	
	id NUMBER(18,0) NOT NULL ENABLE, 
	"POZYCJA_PLANU" VARCHAR2(150), 
	"KIEROWNIK_KOD" VARCHAR2(150), 
	"KIEROWNIK_IMIE" VARCHAR2(150), 
	"KIEROWNIK_NAZWISKO" VARCHAR2(150), 
	"KIEROWNIK_KONTO_ORACLE" VARCHAR2(150), 
	"OPIEKUN_KOD" VARCHAR2(150), 
	"OPIEKUN_IMIE" VARCHAR2(150), 
	"OPIEKUN_NAZWISKO" VARCHAR2(150), 
	"OPIEKUN_KONTO_ORACLE" VARCHAR2(150)
   );
  

CREATE VIEW dsg_pig_topic_f_source_view AS
select topic.id as ID, topic.pozycja_planu as CN, topic.pozycja_planu AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available
from dsg_pig_t_funding_source topic;