CREATE TABLE DSG_PIG_OFERTA (
    DOCUMENT_ID numeric(18, 0) NOT NULL ,
    nr_oferty varchar(50),
    name varchar(50),
    domain int,
    author int,
    funding_source int,
    for_services int,
    geological_unit int,
    chrono_geological_unit int,
    cost numeric(18,2),
    costs numeric(18,2),
    remarks varchar (255),
    points int,
    cecha int,
    charakter_badania int
);

create table dsg_pig_domain
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create sequence dsg_pig_domain_id;

create table dsg_pig_funding_source
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);
create sequence dsg_pig_funding_source_id;

create table dsg_pig_geological_unit
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);
create sequence dsg_pig_geological_unit_id;

create table dsg_pig_ch_geological_unit
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);
create sequence dsg_pig_ch_geological_unit_id;

create table dsg_pig_cecha
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);
create sequence dsg_pig_cecha_id;

create table dsg_pig_cecharakter_badania
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);
create sequence dsg_pig_cecharakter_badania_id;

CREATE TABLE DSG_PIG_OFERTA_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(20 char) NOT NULL,
    FIELD_VAL varchar(100 char) NULL
);

alter table DSG_PIG_OFERTA add  TYP INTEGER default 1 not null;
alter table DSG_PIG_OFERTA add  AUTHOR varchar(50) not null;
alter table DSG_PIG_OFERTA add  ACCEPTING_TYPE INTEGER null;
alter table DSG_PIG_OFERTA add  preparation varchar(50) null;

insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL, 'dpizu-division', 'Dzia Planowania i Zarządzania Umowami','division',0,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (ds_division_id.NEXTVAL, 'sszp-division', 'SSZP','division',0,0);
