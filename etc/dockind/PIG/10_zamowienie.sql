delete from dsg_pig_tryb;

Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (24,'NEG_BEZ_OGL','Negocjacje bez ogłoszenia',2402,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (1,'PRZ_NIEOG','Przetarg nieograniczony',2400,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (21,'PRZ_OGR ','Przetarg ograniczony',2401,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (22,'NEG_OGL','Negocjacje z ogłoszeniem',2402,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (27,'LIC_ELE','Licytacja elektroniczna',2405,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (23,'DIAL_KON','Dialog konkurencyjny',2402,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (25,'CENA','Zapytanie o cene',2404,null,1);
Insert into DSG_PIG_TRYB (ID,CN,TITLE,CENTRUM,REFVALUE,AVAILABLE) values (26,'ELEKTRO','Licytacja elektroniczna',2405,null,1);