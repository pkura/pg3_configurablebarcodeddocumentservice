
CREATE TABLE dsg_pig_wni_urlop
(
	rodzaj_url numeric(18,0),
    DOCUMENT_ID numeric(18,0),
    nr_wniosku varchar(60),
    status numeric(18,0),
	wnioskodawca numeric(18,0),
    OSOBA_ZASTEPUJ numeric(18,0),
    URLOPRODZAJ numeric(18,0),
    DATA_UTW date,
    data_od date,
    data_do date
);

CREATE TABLE [dsg_pcz_nazwa_jedn](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);

CREATE TABLE [dsg_pcz_osoba_zast](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);