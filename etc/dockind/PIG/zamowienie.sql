CREATE TABLE DSG_PIG_ZAMOWIENIE (
    DOCUMENT_ID numeric(18, 0) NOT NULL ,
    przedmiot varchar(150),
    uzasadnienie varchar(250),
    netto numeric(18,2),
    euro_netto numeric(18,2),
    vat int,
    brutto numeric(18,2),
    start_date date,
    finish_date date,
    topic_only int,
    topic_funding_source numeric(18,0),
    funding_source numeric(18,0),
    division int,
    description varchar (255),
    criterium varchar(50),
    tryb numeric(18,0),
    uzas_trybu varchar (255),
    osoby_merytoryczne numeric(18,0),
    wykonanie_tematu date,
    typ_limit_prac int,
    limit_prac varchar (250),
    typ_kosztu_prac numeric(18,0),
    koszt_prac numeric(18,2),
    zgodny_z_umowa int,
    cpv numeric(18,0),
    zgodny_plan int,
    pozycja_planu varchar(100),
    zabezpieczenie numeric(18,2),
    proc_realizacji int,
    wykonawca varchar (250),
    termin_wykonania date
);

create table dsg_pig_t_funding_source (
	id numeric(18,0) NOT NULL,
	funding_source numeric(18,0),
	topic_funding_source numeric(18,0),
	kierownik int
);

create sequence dsg_pig_t_funding_source_id;

create table dsg_pig_funding_source_t
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create sequence dsg_pig_funding_source_t_id;

create table dsg_pig_tryb
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create sequence dsg_pig_tryb_id;

create table dsg_pig_typKosztuPrac
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create sequence dsg_pig_typKosztuPrac_id;

create table dsg_pig_cpv
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	centrum int,
	refValue varchar(20),
	available int default 1 not null
);

create sequence dsg_pig_cpv_id;

alter table DSG_PIG_ZAMOWIENIE add  AUTHOR varchar(50) not null;
alter table DSG_PIG_ZAMOWIENIE add  wymagania varchar(250);
alter table DSG_PIG_ZAMOWIENIE add  STATUS varchar(100);