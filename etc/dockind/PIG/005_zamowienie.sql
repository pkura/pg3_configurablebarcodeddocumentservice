
create table dsg_pig_topic_funding_source
(
	ID numeric(18,0) NOT NULL,
	CN varchar(50),
	TITLE varchar(255),
	manager varchar(100),
	supervisor varchar(100)
);

CREATE VIEW dsg_pig_topic_f_source_view AS
select topic.id as ID, topic.cn as CN, topic.title AS TITLE, '0' AS CENTRUM, NULL AS refValue, '1' as available
from dsg_pig_topic_funding_source topic;