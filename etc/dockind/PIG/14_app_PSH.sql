alter table dsg_pig_psh add wycena_mwp numeric(18,2);
alter table dsg_pig_psh add wycena_cbdh numeric(18,2);
alter table dsg_pig_psh add wycena_pobory numeric(18,2);
alter table dsg_pig_psh add wycena_gzwp numeric(18,2);
alter table dsg_pig_psh add wycena_inne numeric(18,2);
alter table dsg_pig_psh add odbior_email_adres varchar(50);