alter table DSG_PIG_ZAMOWIENIE add na_dzien date;

CREATE VIEW dsg_pig_cpv_view AS
select cpv.id as ID, cpv.cn as CN, cpv.title AS TITLE, count(m.document_id) AS COUNT_ZAM
from dsg_pig_cpv cpv 
left join dsg_pig_oferta_multiple_value m on cpv.id = TO_NUMBER (m.field_val) 
GROUP BY cpv.id, cpv.cn, cpv.title;

INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'WOZN', 'Wynagrodzenia osobowe z narzutami', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'BFP', 'Bezosobowy fundusz p�ac', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'MIW', 'Materia�y i wyposa�enie', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'TRA', 'Transport', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'APS', 'Aparatura specjalna', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'OP', 'Oprogramowanie', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'DK', 'Delegacje krajowe', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'DZ', 'Delegacje zagraniczne', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'INNE', 'Inne', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'KOP', 'Kooperacja', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'KOPW', 'Kooperacja wspomagaj�ca badania', '0', NULL, '1');
INSERT INTO dsg_pig_typKosztuPrac (ID, CN, TITLE, centrum, refValue, available) VALUES (dsg_pig_typKosztuPrac_id.nextval, 'NI', 'Nak�ady inwestycyjne', '0', NULL, '1');

alter table DSG_PIG_ZAMOWIENIE add to_realization int; 