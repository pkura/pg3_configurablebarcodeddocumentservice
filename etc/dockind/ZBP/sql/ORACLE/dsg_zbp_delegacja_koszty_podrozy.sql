CREATE TABLE dsg_zbp_del_koszty_podrozy(
id number(18,0) not null,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);
commit;

alter table dsg_zbp_del_koszty_podrozy add constraint PK_koszty_podrozy primary key (id);
commit;

create sequence dsg_zbp_del_koszty_podrozy_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_KOSZTY_PODROZY
before insert on dsg_zbp_del_koszty_podrozy
for each row
begin
  if :new.id is null then
  select dsg_zbp_del_koszty_podrozy_ID.nextval into :new.id from dual;
  end if;
end
commit;


CREATE TABLE dsg_zbp_del_srod_lok(
id number(18,0) not null,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);
commit;

alter table dsg_zbp_del_srod_lok add constraint PK_srodki_lokomocji primary key (id);
commit;

create sequence dsg_zbp_del_srod_lok_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_SRODKI_LOKOMOCJI
before insert on dsg_zbp_del_srod_lok
for each row
begin
  if :new.id is null then
  select dsg_zbp_del_srod_lok_id.nextval into :new.id from dual;
  end if;
end
commit;


insert into dsg_zbp_del_srod_lok values (null,'CAR40', 'Samoch�d prywatny (40%)',0,0,1);  
insert into dsg_zbp_del_srod_lok values (null,'CAR100', 'Samoch�d prywatny (100%)',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'OS', 'Obni�ona stawka (B�dzi�)',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'AIR', 'Samolot',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'PKP', 'Poci�g',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'BUS', 'Autokar',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'BK', 'Brak koszt�w przejazdu',0,0,1);
insert into dsg_zbp_del_srod_lok values (null,'CAR', 'Samoch�d s�u�bowy',0,0,1);
commit;