CREATE TABLE DSG_zbp_FAKT_ZAKUP (
	document_id numeric (18,0) NOT NULL,
	status numeric(18, 0) NULL,
	typ_faktury numeric(18, 0) NULL,
	nr_faktury varchar(30) NULL,
	data_wplywu_faktury date NULL,
	data_wystawienia date NULL,
	termin_platnosci date NULL,
	waluta numeric(18, 0) NULL,
	kwota_netto numeric(18, 2) NULL,
	kwota_brutto numeric(18, 2) NULL,
	kwota_vat numeric(18, 2) NULL,
	sposob numeric(18, 0) NULL,
	opis char (4000) NULL,
	OSOBA_MERYTORYCZNA numeric(18, 0) NULL,
	NR_UMOWY varchar(30) NULL,
	F_PROFORMA numeric(18, 0) NULL,
	OKRES_FAKTURY varchar(30) NULL,
	ZARZAD numeric(18, 0) NULL,
  	MPK_CORRECTION smallint,
  	HIDE_MPK smallint,
  	constraint fk_document_id 
    	foreign key (document_id)
    	references ds_document(id)
);


create index faktura_zakupowa_index on dsg_zbp_faktura_zakup (document_id);


CREATE TABLE DSG_zbp_FAKT_ZAKUP_multiple (
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100) NULL
) ;


alter table DSG_zbp_FAKT_ZAKUP add proforma	CHAR(1 );
alter table DSG_zbp_FAKT_ZAKUP add mpk_was_changed CHAR(1 );


/* Kolumna mpk_was_changed okazala sie niepotrzebna */
alter table dsg_zbp_fakt_zakup drop column mpk_was_changed;


alter table dsg_zbp_fakt_zakup add duplikat CHAR(1 );


alter table dsg_zbp_fakt_zakup add f_faktura numeric(18, 0);


alter table dsg_zbp_fakt_zakup add korekta CHAR(1 );


alter table DSG_zbp_FAKT_ZAKUP add show_korekta_info CHAR(1 );


alter table dsg_zbp_fakt_zakup add show_f_proforma_info CHAR(1 );


alter table dsg_zbp_fakt_zakup modify NR_UMOWY char(4000);


alter table dsg_zbp_fakt_zakup drop column okres_faktury;


alter table dsg_zbp_fakt_zakup add okres_faktury_od date null;
alter table dsg_zbp_fakt_zakup add okres_faktury_do date null;
