
CREATE TABLE dsg_zbp_wniosek_urlopowy (
	DOCUMENT_ID number(19, 0) NOT NULL,
	period_from timestamp NULL,
	period_to timestamp NULL,
	rodzaj_INT int NULL,
	RECIPIENT number(18, 0) NULL,
	KIND number(18,0)NULL,
	available_days int NULL,
	DSUSER number(19, 0) null,
	GODZ_FROM timestamp NULL,
	GODZ_TO timestamp NULL,
	DSDIVISION number(19, 0) null,
	status number(18,0) null,
  WORKING_DAYS_CN number(18,0) null,
  OSOBA_ZASTEPUJ number (19,0) null,
  stampDate date
);
commit;

create index wniosek_urlop_index on dsg_zbp_wniosek_urlopowy (document_id);
commit;

alter table dsg_zbp_wniosek_urlopowy add constraint fk_wniosek_urlop_id foreign key(document_id)references ds_document(id);
commit;

CREATE TABLE dsg_zbp_wnios_urlop_multiple (
	DOCUMENT_ID number(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100) NULL
);
commit;

--------------------------------------------------

CREATE TABLE dsg_zbp_wniosek_zast (
  ID number(18,0) NOT NULL,
  title varchar (200) null,
  zast_period_from timestamp NULL,
  zast_period_to timestamp NULL 
);
commit;

create index dsg_zbp_urlop_zast_index on dsg_zbp_wniosek_zast(id);
commit;

create sequence dsg_zbp_wniosek_zast_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_zbp_wniosek_zast
before insert on dsg_zbp_wniosek_zast
for each row
begin
  if :new.id is null then
  select dsg_zbp_wniosek_zast_ID.nextval into :new.id from dual;
  end if;
end
commit;
 
 
-------------------------------------------------------------

/**
 * 
CREATE or replace view dsg_zbp_wniosek_user
AS
SELECT     U.ID, U.FIRSTNAME + ' ' + U.LASTNAME + ' ' + D.NAME AS title, 'D_'+U.NAME as cn,	NULL as	centrum ,	'' as refValue , 1 as available 
FROM       DS_USER U 
INNER JOIN DS_USER_TO_DIVISION U_D ON U_D.USER_ID = U.ID 
INNER JOIN DS_DIVISION D ON D.ID = U_D.DIVISION_ID 
WHERE      D.DIVISIONTYPE = 'division' OR D.DIVISIONTYPE = 'position';
commit;
 */