
CREATE TABLE dsg_zbp_delegacja_zespol(
id number(18,0) not null,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);
commit;

create index dsg_del_zesp_index on dsg_zbp_delegacja_zespol (id);
commit;

create sequence dsg_zbp_delegacja_zespol_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_zbp_del_zesp
before insert on dsg_zbp_delegacja_zespol
for each row
begin
  if :new.id is null then
  select dsg_zbp_delegacja_zespol_ID.nextval into :new.id from dual;
  end if;
end
commit;