

CREATE TABLE dsg_absence_request
 (
	document_id numeric(19, 0) NOT NULL PRIMARY KEY ,
	dsuser numeric(19, 0) NULL,
	period_from timestamp NULL,
	period_to timestamp NULL,
	description varchar(400 char) NULL,
	position varchar2(50 char) NULL,
	status integer NULL,
	kind varchar2(150 char) NULL,
	working_days integer NULL,
	available_days integer NULL,
	substitution numeric(19, 0) NULL
);

-- DZIA� PERSONALNY
INSERT INTO DS_DIVISION (id,GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,description,HIDDEN)VALUES (ds_division_id.nextval,'personal-division', 'Dzia� personalny', 'DP', 'group', 473, NULL, 1);

-- TYPY URLOP�W DLA BECK     

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUN', 'Wymiar urlopu nale�ny', NULL, 2);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('WUZ', 'Wymiar urlopu zaleg�y', NULL, 2);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UW', 'Urlop wypoczynkowy', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('U�', 'Urlop na ��danie', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('188', 'Opieka nad dzieckiem', NULL, 0);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UOK', 'Urlop okoliczno�ciowy', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UB', 'Urlop bezp�atny', NULL, 1);     
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UI', 'Inny urlop', NULL, 0);

-- DODATKOWE URLOPY
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UJ', 'Urlop ojcowski', NULL, 1);
     
INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UM', 'Urlop macierzy�ski', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('UY', 'Urlop wychowawczy', NULL, 1);

INSERT INTO DS_ABSENCE_TYPE (CODE,NAME,INFO,FLAG)VALUES ('NP', 'Nieobecno�� pracownika', NULL, 1);

DECLARE
  userID DS_USER.name%TYPE;
  employeeCardID DS_EMPLOYEE_CARD.ID%TYPE;
  CURSOR UserIdCursor IS (SELECT u.name FROM DS_USER u  where (select count(1) from DS_EMPLOYEE_CARD e where e.user_id = u.name ) < 1 ) FOR UPDATE;
BEGIN
  OPEN UserIdCursor;
  
  LOOP
    FETCH userIdCursor INTO userID;
    EXIT WHEN userIdCursor%NOTFOUND;
    
    SELECT SEQ_DS_EMPLOYEE_CARD_ID.NEXTVAL INTO employeeCardId FROM DUAL;
    
    INSERT INTO DS_EMPLOYEE_CARD ("ID", KPX, COMPANY, DEPARTMENT, EMPLOYMENT_START_DATE, POSITION, USER_ID)
			VALUES (employeeCardId, 'BD', 'Beck', 'G��wny', CURRENT_TIMESTAMP, 'BD', userID);
    
    INSERT INTO DS_ABSENCE ("ID", "YEAR", START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (SEQ_DS_ABSENCE_ID.NEXTVAL, 2013, to_date('2013-01-01', 'yyyy-mm-dd'), to_date('2013-12-31', 'yyyy-mm-dd'), 26, CURRENT_TIMESTAMP, 'Urlop nale�ny', employeeCardID, 'WUN');
			
		INSERT INTO DS_ABSENCE ("ID", "YEAR", START_DATE, END_DATE, DAYS_NUM, CTIME, INFO, EMPLOYEE_ID, ABSENCE_TYPE)
			VALUES (SEQ_DS_ABSENCE_ID.NEXTVAL, 2013, to_date('2013-01-01', 'yyyy-mm-dd'), to_date('2013-12-31', 'yyyy-mm-dd'), 0, CURRENT_TIMESTAMP, 'Urlop zaleg�y', employeeCardID, 'WUZ');
      
  END LOOP;
  
  CLOSE UserIdCursor;
END;

 UPDATE DS_EMPLOYEE_CARD SET FIRST_NAME = (select max(u.firstname) from ds_user u where DS_EMPLOYEE_CARD.user_id = u.name), 
 LAST_NAME = (select max(u.lastname) from ds_user u where DS_EMPLOYEE_CARD.user_id = u.name);