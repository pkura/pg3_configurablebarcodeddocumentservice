CREATE TABLE ds_document_termin_platnosci(
	document_id number (18,0) NOT NULL ENABLE,
	termin_platnosci date NOT NULL
);
commit;

create index ds_doc_term_plat_index on ds_document_termin_platnosci (document_id);
commit;