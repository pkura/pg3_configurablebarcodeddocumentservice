CREATE TABLE CZASOPISMO(
 	TITLE varchar(20) NULL,
 	ISSUE nchar(10) NULL,
 	document_id numeric(18, 0) NULL,
 	sender smallint NULL,
 	ATTACHMENTS_USERS numeric(19, 0) NULL,
 	status numeric(19, 0) NULL,
 	USER_ACCEPTING numeric(19, 0) NULL
 );
   commit;


 CREATE TABLE czasopismo_types(
 	ID numeric(19, 0) NOT NULL,
 	NAME varchar(256) NULL
 );
   commit;

 create sequence czasopismo_types_id start with 1 increment by 1 nocache;
  commit;
  
CREATE OR REPLACE TRIGGER czasopismo_types_trigger 
BEFORE INSERT ON czasopismo_types 
FOR EACH ROW
WHEN (new.id IS NULL)
BEGIN
  SELECT czasopismo_types_id.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

 CREATE TABLE czasopismo_users_multiple(
 	DOCUMENT_ID numeric(19, 0) NOT NULL,
 	FIELD_CN varchar(20) NOT NULL,
 	FIELD_VAL varchar(100) NULL
 );

 COMMIT;