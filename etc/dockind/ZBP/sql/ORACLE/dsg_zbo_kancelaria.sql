CREATE TABLE dsg_zbp_kancelaria(
id number(18,0) not null,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);
commit;

create index dsg_zbp_kanc_index on dsg_zbp_kancelaria (id);
commit;

create sequence dsg_zbp_kancelaria_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_DSG_ZBP_KANCELARIA
before insert on dsg_zbp_kancelaria
for each row
begin
  if :new.id is null then
  select DSG_ZBP_KANCELARIA_ID.nextval into :new.id from dual;
  end if;
end
commit;
