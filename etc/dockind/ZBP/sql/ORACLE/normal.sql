alter table DSG_NORMAL_DOCKIND add kancelaria integer;
alter table DSG_NORMAL_DOCKIND add NR_KODU_KRESKOWEGO varchar (150);
alter table DSG_NORMAL_DOCKIND add docType integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add send_kind integer;
alter table DSG_NORMAL_DOCKIND add signature varchar(150);
commit;



CREATE TABLE dsg_rodzaj_zbp_IN(
	id number(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available integer
);
commit;

create index dsr_rodzaj_IN_index on dsg_rodzaj_zbp_IN (id);
commit;

create sequence dsg_rodzaj_zbp_IN_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_rodzaj_zbp_IN
before insert on dsg_rodzaj_zbp_IN
for each row
begin
  if :new.id is null then
  select dsg_rodzaj_zbp_IN_ID.nextval into :new.id from dual;
  end if;
end
commit;

---------------------------------------------------------------

CREATE TABLE dsg_rodzaj_zbp_out(
	id number(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available integer
);
commit;

create index dsr_rodzaj_OUT_index on dsg_rodzaj_zbp_out (id);
commit;

create sequence dsg_rodzaj_zbp_out_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_rodzaj_zbp_out
before insert on dsg_rodzaj_zbp_out
for each row
begin
  if :new.id is null then
  select dsg_rodzaj_zbp_out_ID.nextval into :new.id from dual;
  end if;
end
commit;


---------------------------------------------------------------

CREATE TABLE dsg_rodzaj_zbp_INT(
	id number(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
  available integer
);
commit;

create index dsr_rodzaj_INT_index on dsg_rodzaj_zbp_INT (id);
commit;

create sequence dsg_rodzaj_zbp_INT_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_rodzaj_zbp_INT
before insert on dsg_rodzaj_zbp_INT
for each row
begin
  if :new.id is null then
  select dsg_rodzaj_zbp_INT_ID.nextval into :new.id from dual;
  end if;
end
commit;






insert into dsg_rodzaj_zbp_in(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('ZAMOWIENIA','Zamówienia usług',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('ODPOWIEDZ','Odpowiedzi na zapytania ofertowe',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('Z_JEDNOSTEK','Pisma z jednostek nadrzędnych (PAN, MNiSW)',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('TAJNE','Pisma tajne',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('INNE','Pozostałe pisma',1);

insert into dsg_rodzaj_zbp_out(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_zbp_out(cn,title,available)values('DO_JEDNOSTEK','Pisma do jednostek nadrzędnych (PAN, MNiSW',1);
insert into dsg_rodzaj_zbp_out(cn,title,available)values('DO_PARTNEROW','Pisma do Partnerów  projektów',1);
insert into dsg_rodzaj_zbp_out(cn,title,available)values('MONITY','Pisma, monity  do kontrahentów',1);
insert into dsg_rodzaj_zbp_out(cn,title,available)values('INNE','Pozostała  korespondencja ',1);

insert into dsg_rodzaj_zbp_int(cn,title,available)values('NOTATKA','Notatka służbowa',1);
insert into dsg_rodzaj_zbp_int(cn,title,available)values('PODANIA','Podania',1);
commit;

