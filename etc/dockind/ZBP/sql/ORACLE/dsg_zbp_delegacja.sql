create table dsg_zbp_delegacja(
document_id number(18,0) NOT NULL,
status number(18,0),
delegowany number(18,0),
cel_wyjazdu varchar(300),
termin_wyjazdu date,
termin_powrotu date,
miejsce_delegacji varchar(150),
przew_koszt_delegacji number(18,2),
srodek_lokomocji varchar(150),
zaliczka number(18,2),
zespol number(18,0),
forma_wspolpracy number(18,0),
numer_delegacji varchar(150),
rachunek number(18,0),
ryczalt number(18,2),
diety number(18,2),
noclegi_rach number(18,2),
noclegi_rycz number(18,2),
inne_wyd number(18,2)
);
commit;


create index dsg_zbp_delegacja_index on dsg_zbp_delegacja (document_id);
commit;

alter table dsg_zbp_delegacja add constraint fk_delegacja_document_id foreign key (document_id) references ds_document(id);
commit;

create sequence dsg_zbp_delegacja_id start with 1 increment by 1 nocache;
commit;  
  
alter table dsg_zbp_delegacja add suma number(18,2);
alter table dsg_zbp_delegacja add do_zwrotu number(18,2);
alter table dsg_zbp_delegacja add do_wyplaty number(18,2);
alter table dsg_zbp_delegacja add koszty_podrozy number(18,2);
alter table dsg_zbp_delegacja add INFORMACJE_DODATKOWE VARCHAR(300);
alter table dsg_zbp_delegacja add RAZEM_PRZYJAZDY_DOJAZDY VARCHAR(30);
commit;

CREATE TABLE dsg_zbp_delegacja_multiple (
	DOCUMENT_ID number(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100) NULL
);
commit;