CREATE TABLE dsg_currency(
  id number(18,0) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available smallint NULL
);
commit;

create index dsg_currency_index on dsg_currency (id);
commit;

create sequence dsg_currency_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_currency
before insert on dsg_currency
for each row
begin
  if :new.id is null then
  select dsg_currency_ID.nextval into :new.id from dual;
  end if;
end
commit;


INSERT INTO dsg_currency VALUES ('EUR', 'EUR - EU euro', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'USD', 'USD - Dolar ameryka�ski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'GBP', 'GBP - Funt szterling', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'JPY', 'JPY - Japonia jen', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'CZK', 'CZK - Korona czeska', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'PLN', 'PLN - Z�oty polski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'SKK', 'SKK - Korona s�owacka', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'UAH', 'UAH - Hrywna Ukraina', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'LTL', 'LTL - Lit Litwa', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'CNY', 'CNY - Juan Chiny', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'NOK', 'NOK - norweska korona', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'CHF', 'CHF - frank szwajcarski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'KUNA', 'KUNA - Kuna chorwacka', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'BYR', 'BYR - Rubel Bia�oru�', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'BAM', 'BAM - Marka transferowa Bo�nia i Hercegowina', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'BGN', 'BGN - Lew Bu�garia', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'DKK', 'DKK - Korona du�ska', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'ISK', 'ISK - Korona islandzka', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'MKD', 'MKD - Denar macedo�ski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'MDL', 'MDL - Lej mo�dawski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'RUB', 'RUB - Rubel Rosja', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'RON', 'RON - Lej Rumunia', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'CSD', 'CSD - Dinar serbski', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'SEK', 'SEK - Korona szwedzka', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'HUF', 'HUF - Forint W�gry', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'LVL', 'LVL - �at �otwa', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'CAD', 'CAD- Dolar kanadyjski', 0, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'ND', '-', NULL, NULL, 1);
INSERT INTO dsg_currency VALUES (null,'AUD', 'AUD - Dolar australijski', 0, NULL, 1);
commit;

