CREATE TABLE dsg_zbp_centra_kosztow(
id number(18,0) not null,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);
commit;

alter table dsg_zbp_centra_kosztow add data_akceptacji date;
alter table dsg_zbp_centra_kosztow add osoba_akceptujaca number(18,0);
alter table dsg_zbp_centra_kosztow add constraint PK_centra_koszt primary key (id);
commit;

create sequence dsg_zbp_centra_kosztow_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_CENTRA_KOSZT
before insert on dsg_zbp_centra_kosztow
for each row
begin
  if :new.id is null then
  select dsg_zbp_centra_kosztow_ID.nextval into :new.id from dual;
  end if;
end
commit;


INSERT INTO dsg_zbp_centra_kosztow VALUES (null,'CK', 'Centrum koszt�w', NULL, NULL, 1);