CREATE TABLE dsg_user_to_owner(
user_id number(19,0)null,
owner_id number(19,0) null
);

insert into dsg_user_to_owner (user_id, owner_id) values (9,5);
insert into dsg_user_to_owner (user_id, owner_id) values (7,4);
insert into dsg_user_to_owner (user_id, owner_id) values (10,3);
insert into dsg_user_to_owner (user_id, owner_id) values (11,3);
insert into dsg_user_to_owner (user_id, owner_id) values (12,3);
insert into dsg_user_to_owner (user_id, owner_id) values (13,5);
insert into dsg_user_to_owner (user_id, owner_id) values (14,4);
insert into dsg_user_to_owner (user_id, owner_id) values (15,3);
insert into dsg_user_to_owner (user_id, owner_id) values (16,2);
insert into dsg_user_to_owner (user_id, owner_id) values (18,2);
insert into dsg_user_to_owner (user_id, owner_id) values (19,4);
insert into dsg_user_to_owner (user_id, owner_id) values (20,4);
insert into dsg_user_to_owner (user_id, owner_id) values (21,5);
insert into dsg_user_to_owner (user_id, owner_id) values (23,5);
insert into dsg_user_to_owner (user_id, owner_id) values (24,4);
insert into dsg_user_to_owner (user_id, owner_id) values (25,2);
insert into dsg_user_to_owner (user_id, owner_id) values (26,5);
insert into dsg_user_to_owner (user_id, owner_id) values (27,2);
insert into dsg_user_to_owner (user_id, owner_id) values (28,2);
insert into dsg_user_to_owner (user_id, owner_id) values (29,2);
insert into dsg_user_to_owner (user_id, owner_id) values (31,4);
insert into dsg_user_to_owner (user_id, owner_id) values (32,5);
insert into dsg_user_to_owner (user_id, owner_id) values (34,2);
insert into dsg_user_to_owner (user_id, owner_id) values (35,2);
insert into dsg_user_to_owner (user_id, owner_id) values (36,5);
insert into dsg_user_to_owner (user_id, owner_id) values (38,3);
insert into dsg_user_to_owner (user_id, owner_id) values (39,2);
insert into dsg_user_to_owner (user_id, owner_id) values (41,4);
insert into dsg_user_to_owner (user_id, owner_id) values (3,2);
insert into dsg_user_to_owner (user_id, owner_id) values (43,3);
insert into dsg_user_to_owner (user_id, owner_id) values (44,2);
insert into dsg_user_to_owner (user_id, owner_id) values (46,5);
insert into dsg_user_to_owner (user_id, owner_id) values (47,2);
insert into dsg_user_to_owner (user_id, owner_id) values (48,4);
insert into dsg_user_to_owner (user_id, owner_id) values (50,5);
insert into dsg_user_to_owner (user_id, owner_id) values (51,3);
insert into dsg_user_to_owner (user_id, owner_id) values (53,2);
insert into dsg_user_to_owner (user_id, owner_id) values (55,5);
insert into dsg_user_to_owner (user_id, owner_id) values (57,4);
insert into dsg_user_to_owner (user_id, owner_id) values (58,4);
insert into dsg_user_to_owner (user_id, owner_id) values (62,4);
insert into dsg_user_to_owner (user_id, owner_id) values (64,5);
insert into dsg_user_to_owner (user_id, owner_id) values (65,5);
insert into dsg_user_to_owner (user_id, owner_id) values (66,3);
insert into dsg_user_to_owner (user_id, owner_id) values (68,2);
insert into dsg_user_to_owner (user_id, owner_id) values (69,5);
insert into dsg_user_to_owner (user_id, owner_id) values (70,4);
insert into dsg_user_to_owner (user_id, owner_id) values (4,2);
insert into dsg_user_to_owner (user_id, owner_id) values (72,5);
insert into dsg_user_to_owner (user_id, owner_id) values (73,5);
insert into dsg_user_to_owner (user_id, owner_id) values (5,2);
insert into dsg_user_to_owner (user_id, owner_id) values (74,2);
insert into dsg_user_to_owner (user_id, owner_id) values (8,5);
insert into dsg_user_to_owner (user_id, owner_id) values (30,5);
insert into dsg_user_to_owner (user_id, owner_id) values (33,5);
insert into dsg_user_to_owner (user_id, owner_id) values (37,3);
insert into dsg_user_to_owner (user_id, owner_id) values (40,2);
insert into dsg_user_to_owner (user_id, owner_id) values (42,4);
insert into dsg_user_to_owner (user_id, owner_id) values (45,3);
insert into dsg_user_to_owner (user_id, owner_id) values (49,2);
insert into dsg_user_to_owner (user_id, owner_id) values (52,4);
insert into dsg_user_to_owner (user_id, owner_id) values (54,2);
insert into dsg_user_to_owner (user_id, owner_id) values (56,4);
insert into dsg_user_to_owner (user_id, owner_id) values (60,2);
insert into dsg_user_to_owner (user_id, owner_id) values (63,3);
insert into dsg_user_to_owner (user_id, owner_id) values (67,2);
insert into dsg_user_to_owner (user_id, owner_id) values (71,5);
insert into dsg_user_to_owner (user_id, owner_id) values (61,2);

commit;