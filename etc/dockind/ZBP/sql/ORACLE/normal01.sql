delete from dsg_rodzaj_zbp_in where cn='Z_JEDNOSTEK';
delete from dsg_rodzaj_zbp_in where cn='FAKTURA';
delete from dsg_rodzaj_zbp_in where cn='TAJNE';

insert into dsg_rodzaj_zbp_in(cn,title,available)values('NOTA','Noty',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('UMOWY','Umowy',1);
insert into dsg_rodzaj_zbp_in(cn,title,available)values('WEZWANIA_DO_ZAPLATY','Wezwania do zap�aty',1);

alter table dsg_normal_dockind add kg_koszt number(18,0) null;
alter table dsg_normal_dockind add kw_koszt number(18,0) null;
commit;