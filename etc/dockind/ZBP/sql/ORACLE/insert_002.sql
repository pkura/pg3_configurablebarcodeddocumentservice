insert into dsg_zbp_delegacja_zespol values(1,'AMRON','System Analiz i Monitorowania Rynku Obrotu Nieruchomo�ciami','2',null,'1');
insert into dsg_zbp_delegacja_zespol values(2,'CEM','Centrum Edukacyjno-Marketingowe','4,5',null,'1');
insert into dsg_zbp_delegacja_zespol values(3,'CIG','Centrum Informacji Gospodarczej','5',null,'1');
insert into dsg_zbp_delegacja_zespol values(4,'PRAWNY','Zesp�l Prawny','1',null,'1');
insert into dsg_zbp_delegacja_zespol values(5,'SWD','System W�zla Dost�powego','3',null,'1');
insert into dsg_zbp_delegacja_zespol values(6,'WYDAWNICTWO','Wydawnictwo','5',null,'1');
insert into dsg_zbp_delegacja_zespol values(7,'ZARZAD','Zarz�d','1',null,'1');
insert into dsg_zbp_delegacja_zespol values(8,'ORGANIZACJA','Zesp�l Organizacji','1',null,'1');
insert into dsg_zbp_delegacja_zespol values(9,'KADRY_I_PLACE','Zesp�l Kadr i Plac','1',null,'1');
insert into dsg_zbp_delegacja_zespol values(10,'KSIEGOWOSC','Zesp�l Ksi�gowo�ci','1',null,'1');

drop table dsg_zbp_del_koszty_podrozy;

CREATE TABLE dsg_zbp_del_koszty_podrozy
(
	ID number(18,0) NOT NULL,
	FROM_PLACE varchar(50),
	FROM_DATE timestamp,
	TO_PLACE varchar (50),
	TO_DATE timestamp,
	TRANSPORT number (18,0),
	OPIS varchar (249),
	KOSZT number (18,2)
);

alter table dsg_zbp_del_koszty_podrozy add constraint PK_koszty_podrozy primary key (id);
commit;

create sequence dsg_zbp_del_koszty_podrozy_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_KOSZTY_PODROZY
before insert on dsg_zbp_del_koszty_podrozy
for each row
begin
  if :new.id is null then
  select dsg_zbp_del_koszty_podrozy_ID.nextval into :new.id from dual;
  end if;
end
commit;
