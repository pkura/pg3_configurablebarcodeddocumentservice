CREATE or replace view DSG_ZBP_BOARD_MEMBERS as
SELECT ID AS ID, NAME AS CN, FIRSTNAME + ' ' + LASTNAME AS TITLE, 1 AS AVAILABLE, null as REFVALUE, null AS CENTRUM
from ds_user;
commit;

CREATE TABLE dsg_zbp_board (
	id int NOT NULL,
	MEMBER int NULL 
);
commit;


create index dsg_zbp_board_index on dsg_zbp_board (id);
commit;

create sequence dsg_zbp_board_id start with 1 increment by 1 nocache;
commit;

create or replace trigger TRG_dsg_zbp_board
before insert on dsg_zbp_board
for each row
begin
  if :new.id is null then
  select dsg_zbp_board_ID.nextval into :new.id from dual;
  end if;
end
commit;