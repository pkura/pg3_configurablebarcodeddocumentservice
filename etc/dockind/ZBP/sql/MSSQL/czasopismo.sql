BEGIN TRANSACTION;
CREATE TABLE CZASOPISMO(
 	[TITLE] [varchar](20) NULL,
 	[ISSUE] [nchar](10) NULL,
 	[document_id] [numeric](18, 0) NULL,
 	[sender] [smallint] NULL,
 	[ATTACHMENTS_USERS] [numeric](19, 0) NULL,
 	[status] [numeric](19, 0) NULL,
 	[USER_ACCEPTING] [numeric](19, 0) NULL
 )

 CREATE TABLE czasopismo_types(
 	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
 	[NAME] [varchar](256) NULL
 )

 CREATE TABLE czasopismo_users_multiple(
 	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
 	[FIELD_CN] [varchar](20) NOT NULL,
 	[FIELD_VAL] [varchar](100) NULL
 )

 COMMIT;