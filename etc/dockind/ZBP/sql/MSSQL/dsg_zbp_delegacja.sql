
create table dbo.dsg_zbp_delegacja(
document_id numeric(18,0) primary key CLUSTERED,
[status] numeric(18,0),
delegowany numeric(18,0),
cel_wyjazdu varchar(300),
termin_wyjazdu date,
termin_powrotu date,
miejsce_delegacji varchar(150),
przew_koszt_delegacji money,
srodek_lokomocji varchar(150),
zaliczka money,
zespol numeric(18,0),
forma_wspolpracy numeric(18,0),
numer_delegacji varchar(150),
rachunek numeric(18,0),
ryczalt money,
diety money,
noclegi_rach money,
noclegi_rycz money,
inne_wyd money
);

create index dsg_zbp_delegacja_index on dsg_zbp_delegacja (document_id);

  
alter table [dsg_zbp_delegacja] add suma numeric(18,0);
alter table [dsg_zbp_delegacja] add do_zwrotu numeric(18,0);
alter table [dsg_zbp_delegacja] add do_wyplaty numeric(18,0);
alter table dsg_zbp_delegacja add koszty_podrozy numeric(18,0);

CREATE TABLE dsg_zbp_delegacja_multiple_value (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;