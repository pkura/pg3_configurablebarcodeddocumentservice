alter table dsg_zbp_faktura_zakupowa add MPK_CORRECTION smallint;
alter table dsg_zbp_faktura_zakupowa add HIDE_MPK smallint;

CREATE TABLE dsg_zbp_faktura_zakupowa (
	[document_id] numeric (18,0) IDENTITY(1,1) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[typ_faktury] [numeric](18, 0) NULL,
	[nr_faktury] [varchar](30) NULL,
	[data_wplywu_faktury] [date] NULL,
	[data_wystawienia] [date] NULL,
	[termin_platnosci] [date] NULL,
	[waluta] [numeric](18, 0) NULL,
	[kwota_netto] [numeric](18, 2) NULL,
	[kwota_brutto] [numeric](18, 2) NULL,
	[kwota_vat] [numeric](18, 2) NULL,
	[sposob] [numeric](18, 0) NULL,
	[opis] [varchar](4096) NULL,
	[OSOBA_MERYTORYCZNA] [numeric](18, 0) NULL,
	[NR_UMOWY] [varchar](30) NULL,
	[F_PROFORMA] [numeric](18, 0) NULL,
	[OKRES_FAKTURY] [varchar](30) NULL,
	[ZARZAD] [numeric](18, 0) NULL
);


ALTER TABLE dsg_zbp_faktura_zakupowa ADD PRIMARY KEY (document_id);
create index dsg_zbp_faktura_zakupowa_index on dsg_zbp_faktura_zakupowa (document_id);


CREATE TABLE dsg_zbp_faktura_zakupowa_multiple_value (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;

alter table DSG_ZBP_FAKT_ZAKUP add proforma	CHAR(1);