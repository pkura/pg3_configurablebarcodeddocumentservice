create table dsg_zbp_delegacja_zespol(
id numeric(18,0) primary key CLUSTERED,
cn varchar(50),
title varchar(255),
centrum integer,
refValue varchar(20),
available integer
);