CREATE TABLE [dbo].[dsg_zbp_centra_kosztow](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
 CONSTRAINT [PK__dsg_zbp_centra_kosztow] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[dsg_zbp_centra_kosztow] ON
INSERT [dbo].[dsg_zbp_centra_kosztow] ([cn], [title], [centrum], [refValue], [available], [id]) VALUES (N'CK', N'Centrum koszt�w', NULL, NULL, 1, 1)
SET IDENTITY_INSERT [dbo].[dsg_zbp_centra_kosztow] OFF
