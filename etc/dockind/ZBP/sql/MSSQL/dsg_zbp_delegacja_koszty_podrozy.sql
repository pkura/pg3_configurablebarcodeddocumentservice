CREATE TABLE dsg_zbp_delegacja_koszty_podrozy
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	FROM_PLACE varchar(50),
	FROM_DATE datetime,
	TO_PLACE varchar (50),
	TO_DATE datetime,
	TRANSPORT numeric (18,0),
	OPIS varchar (249),
	KOSZT numeric (18,2)
);

CREATE TABLE dsg_zbp_delegacja_srodki_lokomocji
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Samoch�d prywatny (40%)',0,0,1);  
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Samoch�d prywatny (100%)',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Obni�ona stawka (B�dzi�)',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Samolot',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Poci�g',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Autokar',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Brak koszt�w przejazdu',0,0,1);
insert into dsg_zbp_delegacja_srodki_lokomocji values (1, 'Samoch�d s�u�bowy',0,0,1);