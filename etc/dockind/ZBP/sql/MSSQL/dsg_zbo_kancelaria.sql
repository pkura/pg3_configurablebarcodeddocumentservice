CREATE TABLE dbo.dsg_zbp_kacelaria(
id numeric(18,0) primary key,
cn varchar(255),
title varchar(255),
refValue varchar(20),
centrum numeric(18,0),
available smallint
);