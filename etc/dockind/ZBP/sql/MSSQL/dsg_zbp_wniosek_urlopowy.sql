CREATE TABLE [dsg_zbp_wniosek_urlopowy](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[period_from] [datetime]NULL,
	[period_to] [datetime]NULL,
	[rodzaj_INT] [int]NULL,
	[RECIPIENT] [numeric](18, 0) NULL,
	[KIND] numeric(18,0)NULL,
	[available_days] [int]NULL,
	[DSUSER] [numeric](19, 0) null,
	[GODZ_FROM] [datetime] NULL,
	[GODZ_TO] [datetime] NULL,
	[DSDIVISION] [numeric](19, 0) null
	

) ON [PRIMARY]


alter table dsg_zbp_wniosek_urlopowy add [status] numeric(18,0)
alter table dsg_zbp_wniosek_urlopowy add [WORKING_DAYS_CN] numeric(18,0)
alter table dsg_zbp_wniosek_urlopowy add [OSOBA_ZASTEPUJ] NUMERIC (19,0)
alter table dsg_zbp_wniosek_urlopowy add [stampDate] date;

CREATE TABLE [dsg_zbp_wniosek_zast] (
ID numeric(18,0) identity(1,1) NOT NULL,
     [title] varchar (200) null,
     [zast_period_from] [datetime] NULL,
	 [zast_period_to] [datetime] NULL
	 
 )
   
GO

CREATE VIEW [dsg_zbp_wniosek_user]
AS
SELECT     U.ID, U.FIRSTNAME + ' ' + U.LASTNAME + ' ' + D.NAME AS title,
			'D_'+U.NAME as cn,
		NULL as	centrum ,
		'' as refValue ,
		1 as available 
FROM         dbo.DS_USER AS U INNER JOIN
                      dbo.DS_USER_TO_DIVISION AS U_D ON U_D.USER_ID = U.ID INNER JOIN
                      dbo.DS_DIVISION AS D ON D.ID = U_D.DIVISION_ID WHERE     (D.DIVISIONTYPE = 'division') OR
                      (D.DIVISIONTYPE = 'position')
