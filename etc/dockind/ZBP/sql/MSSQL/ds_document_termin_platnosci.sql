CREATE TABLE ds_document_termin_platnosci(
	[document_id] [numeric](18,0) PRIMARY KEY,
	[termin_platnosci] [date] NOT NULL
);

create index ds_document_termin_platnosci_index on ds_document_termin_platnosci (document_id);

