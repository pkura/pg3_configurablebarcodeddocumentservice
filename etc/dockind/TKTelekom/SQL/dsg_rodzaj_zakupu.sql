CREATE TABLE [dbo].[dsg_rodzaj_zakupu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_rodzaj_zakupu ADD PRIMARY KEY (ID);
create index dsg_rodzaj_zakupu_index on dsg_rodzaj_zakupu (ID);


INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'USL', N'Us�ugowa', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'MAG', N'Magazynowa', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'INW', N'Inwestycyjna', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ODS', N'Odsetki', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZFSS', N'ZF�S', NULL, NULL, 1);
INSERT [dbo].[dsg_rodzaj_zakupu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'INNE', N'Inne (np. kary, odszkodowania, kaucje, wadia, inne op�aty td.)', NULL, NULL, 1);


