CREATE TABLE [dbo].dsg_grupa_platnosci (
	[id] [bigint]  NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	)

insert into dsg_grupa_platnosci (id, cn, available) values (1, 'GSM-R KRAJ',1);
insert into dsg_grupa_platnosci (id, cn, available) values (2, 'GSM-R ODS KRAJ',1);
insert into dsg_grupa_platnosci (id, cn, available) values (3, 'GSM-R ODSTEN-T',1);
insert into dsg_grupa_platnosci (id, cn, available) values (4, 'GSM-R SR ZAST',1);
insert into dsg_grupa_platnosci (id, cn, available) values (5, 'GSM-R TEN-T',1);
insert into dsg_grupa_platnosci (id, cn, available) values (6, 'INTERCONNECT',1);
insert into dsg_grupa_platnosci (id, cn, available) values (7, 'KAUCJE I WADIA',1);
insert into dsg_grupa_platnosci (id, cn, available) values (8, 'PLACE',1);
insert into dsg_grupa_platnosci (id, cn, available) values (9, 'PODSTAWOWA',1);
insert into dsg_grupa_platnosci (id, cn, available) values (10, 'SITKOL KRAJ',1);
insert into dsg_grupa_platnosci (id, cn, available) values (11, 'SITKOL ODS KRAJ',1);
insert into dsg_grupa_platnosci (id, cn, available) values (12, 'SITKOL ODSTEN-T',1);
insert into dsg_grupa_platnosci (id, cn, available) values (13, 'SITKOL SR ZAST',1);
insert into dsg_grupa_platnosci (id, cn, available) values (14, 'SITKOL TEN-T',1);
insert into dsg_grupa_platnosci (id, cn, available) values (15, 'ZFSS',1);
insert into dsg_grupa_platnosci (id, cn, available) values (16, 'SRODKI BUDZETOWE',1);
insert into dsg_grupa_platnosci (id, cn, available) values (17, 'ALIOR PODST',1);
insert into dsg_grupa_platnosci (id, cn, available) values (18, 'ALIOR WPL MASOWE',1);
insert into dsg_grupa_platnosci (id, cn, available) values (19, 'ALIOR CESJE',1);
insert into dsg_grupa_platnosci (id, cn, available) values (20, 'DnB NORD PODST',1);

update dsg_grupa_platnosci set title = cn;
alter table dsg_faktura_zakupu add GRUPA_PLATNOSCI_LIST numeric (18, 0)