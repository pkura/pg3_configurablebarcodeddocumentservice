alter table dsg_faktura_zakupu add KIEROWNIK_LFR numeric (18, 0);
alter table dsg_faktura_zakupu add PRACOWNIK_LFR numeric (18, 0);

CREATE TABLE [dbo].[dsg_kierownik_lfr]
(
	[id] numeric(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL,
);

insert into [dsg_kierownik_lfr] (cn, title, centrum, refValue, available)
values ('LFR1', 'LFR 1 - Zesp� ds. rozlicze� maj�tku trwa�ego i sprawozdawczo�ci finansowej', null, null, 1);
insert into [dsg_kierownik_lfr] (cn, title, centrum, refValue, available)
values ('LFR2', 'LFR2 - Zesp� ds. rozlicze� podatkowych i ewidencji sprzeda�y', null, null, 1);
insert into [dsg_kierownik_lfr] (cn, title, centrum, refValue, available)
values ('LFR3', 'LFR3 - Zesp� ds. rozlicze�  finansowych', null, null, 1);
insert into [dsg_kierownik_lfr] (cn, title, centrum, refValue, available)
values ('LFR4', 'LFR4 - Zesp� ds. koszt�w i ewidencji zakupu', null, null, 1);


create view dsg_lfr_users_view as
select
 u.id,
 u.name as cn,
 u.firstname + ' ' + u.lastname as title,
null as centrum,
CASE
	WHEN d.code like 'LFR1' then (select id from dsg_kierownik_lfr k where k.cn = 'LFR1')
	WHEN d.code like 'LFR2' then (select id from dsg_kierownik_lfr k where k.cn = 'LFR2')
	WHEN d.code like 'LFR3' then (select id from dsg_kierownik_lfr k where k.cn = 'LFR3')
	WHEN d.code like 'LFR4' then (select id from dsg_kierownik_lfr k where k.cn = 'LFR4')
END as refValue,
 1 as available
 from ds_user u join ds_user_to_division ud on u.id = ud.user_id
join ds_Division d on ud.division_id = d.id where d.code in ('LFR1', 'LFR2', 'LFR3', 'LFR4');

insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'Akceptacja Kierownika LFR1', 'LFR1', 'ac.acceptance', 1, 1);
insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'J�DRAS EWA', 'jedras', 'ds.user', (select id from ds_acceptance_division where code = 'LFR1'), 1);

insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'Akceptacja Kierownika LFR2', 'LFR2', 'ac.acceptance', 1, 1);
insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'NALEWAJEK BO�ENA', 'nalewajek', 'ds.user', (select id from ds_acceptance_division where code = 'LFR2'), 1);

insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'Akceptacja Kierownika LFR3', 'LFR3', 'ac.acceptance', 1, 1);
insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'S�ABUSZEWSKA SYLWIA', 'slabuszewska', 'ds.user', (select id from ds_acceptance_division where code = 'LFR3'), 1);

insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'Akceptacja Kierownika LFR4', 'LFR4', 'ac.acceptance', 1, 1);
insert into ds_acceptance_division (guid, name, code, DIVISIONTYPE, PARENT_ID, ogolna)
values ('xxx', 'MOSAKOWSKA KRYSTYNA', 'mosakowska', 'ds.user', (select id from ds_acceptance_division where code = 'LFR4'), 1);