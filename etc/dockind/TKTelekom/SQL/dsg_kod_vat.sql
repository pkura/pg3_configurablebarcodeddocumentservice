CREATE TABLE [dbo].[dsg_kod_vat](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_kod_vat ADD PRIMARY KEY (ID);
create index dsg_kod_vat_index on dsg_kod_vat (ID);

INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT3', N'VAT 2%', NULL, NULL, 1);
INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT5', N'VAT 5%', NULL, NULL, 1);
INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT7', N'VAT 7%', NULL, NULL, 1);
INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT8', N'VAT 8%', NULL, NULL, 1);
INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT22', N'VAT 22%', NULL, NULL, 1);
INSERT [dbo].[dsg_kod_vat] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'VAT23', N'VAT 23%', NULL, NULL, 1);

CREATE TABLE [dbo].[dsg_mpk_kod_podatku](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

