CREATE TABLE [dbo].[dsg_komorka_osoby](
[id] [bigint] NOT NULL,
[cn] [varchar](50) NULL,
[title] [varchar](255) NULL,
[centrum] [int] NULL,
[refValue] [varchar](20) NULL,
[available] [bit] NULL
);

ALTER TABLE dsg_komorka_osoby ADD PRIMARY KEY (ID);
create index dsg_komorka_osoby_index on dsg_komorka_osoby (ID);


--  w INSERT'cie refValue to ID poszczeg�lnych pion�w z DS_DIVISION

--  Pion Operatorski
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOIW', N'Szadkowski Bartosz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOIS', N'Anio�kowski Ryszard',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOIT', N'Rakowski �ukasz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOIB', N'Grudnik Barbara',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOH', N'Pola�ski Marcin',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHB', N'Sierankowski Tomasz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHB2', N'Wyrzykowski Roman',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHS', N'Sielski Sebastian',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHS1', N'Dolna Katarzyna',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHS2', N'J�drzejczak Oskar',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHK', N'Auzbiter Jerzy',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHK1', N'Brz�ka�a Maciej',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHK2', N'Kukolus Monika',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHU', N'Puacz Konrad',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHUP', N'Cieciera Andrzej',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHO', N'Wo�ek Robert',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHO1', N'Zwoli�ski Piotr',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHO2', N'Szlagowski W�odzimierz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHO3', N'Olszewski Daniel',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHW', N'Pe�szy�ski Jacek',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHW1', N'Szyma�ski Krzysztof',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOHW2', N'Konieczka Aleksandra',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOT', N'R�lka Tomasz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA', N'Patoleta Marcin',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA1', N'Lankiewicz Andrzej',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA2', N'Orlikowski Zbigniew',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA3', N'Wawrzynowicz Zbigniew',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA4', N'Stawicki Jerzy',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTA5', N'Gozdan Jan',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTE', N'Kawi�ski Wojciech',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTE3', N'Rudnicki Ryszard',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTU', N'Nowak Jaros�aw',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTU1', N'Bujak Pawe�',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTS', N'Owczarek Bogdan',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTS1', N'Kosior Grzegorz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTS2', N'Krupa Justyna ',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTS2', N'Filipiuk Dorota',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTS3', N'Brodecki Grzegorz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOX', N'Tomczyk Anna',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOR1', N'Koszyk Andrzej',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORK1', N'Rembek �ukasz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOR2', N'Wendt Adam',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORK2', N'Gryszko Anna',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORS2', N'Tomczyk Janusz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOR3', N'Olszowy Sylwia',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTE2', N'Wawry� Zbigniew',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORK3', N'Szewczuk Mateusz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOR4', N'Sur�wka Dariusz',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORS4', N'P�a�y�ski Aleksander',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LOTE2', N'Patela W�adys�aw',null,331,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LORK4', N'Walak Zbigniew',null,331,1);



--  Pion Wsparcia

INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'L', N'Hamryszak Micha� ',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LR', N'Prokurat Marek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZARZAD', N'Kuna Przemys�aw',null,322,1);

--INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'', N'Kuna Przemys�aw',null,322,1);

INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LM', N'Kami�ska Monika',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LMOK', N'Dobrzy�ski S�awomir',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LMM', N'Po�lednik Ewa',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LMK', N'K�oda Zbigniew',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LMOP', N'Stefanek Jerzy',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LF', N'Gocel Artur',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LFR', N'Achcyng Joanna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LFC', N'Szymczuk Sylwester',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LFB', N'Szymczuk Martyna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LFP', N'Rak Katarzyna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LP', N'Solecka Katarzyna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LPW', N'Malesa Marzanna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LPD', N'Naciska�a Celina',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LPW', N'�wiercz Hanna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LA', N'Nowicki Andrzej',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAZ', N'Lorenc-Zaso� El�bieta',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAC', N'Chodkiewicz El�bieta',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAZ', N'Kuczy�ski Konrad',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAZ', N'�wik�a Bogus�aw',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAL', N'�o��dziewski Zbigniew',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALE', N'Janczarek Stanis�aw',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAL', N'Magruk Magdalena',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALG', N'Wiadrowski Sylwester',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAA', N'Seiler Marzanna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAAK', N'Dymek Beata',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Kowalski Marek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Kubik Andrzej',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Mazurkiewicz Anna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Kuczyk Anna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Mazur Anna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Ga��zka Artur',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'G�ska Andrzej',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Mrozek Dariusz',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Tomaszewicz Ma�gorzata',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Garus Jolanta',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Kopras Urszula',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Tarasiewicz Monika',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Meller Marek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Tworkowski Janusz',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'J�drzejak Anna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Przeniczna Ma�gorzata',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Dorobek Iwona',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Ku�kiewicz Lucyna ',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LAN', N'Siemaszko Irena',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL1', N'Madej Ma�gorzata',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL1', N'Uszy�ska Teresa',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL2', N'W�glowska Mieczys�awa',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL2', N'Sztacha�ska Maria',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL2', N'�wierczek Helena',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL3', N'Szyjka Zbigniew',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL3', N'Purol Marek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL4', N'Marulewska Krystyna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL4', N'Michor Maria',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL4', N'Chrz�szcz Jolanta',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LALL4', N'Ciszewska Gra�yna',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LC', N'W�jcik Agnieszka',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LC', N'Kr�l Kazimierz',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LU', N'Ko�odziejski Marek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LUI', N'Rubaj Jacek',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LUO', N'Lewandowska Iwona',null,322,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LUT', N'Zwierzchowski Krzysztof',null,322,1);


--  Pion Rob�t Telekomunikacyjnych

INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LB', N'Klimczak Micha�',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBT', N'Kluj Krzysztof',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBTB', N'Lenart Grzegorz',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBTR', N'Kaczmarczyk Kazimierz',null,318 ,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBT1', N'Bartnik Adam',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBTB3', N'Mus Marcin',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBTB3', N'Kluka El�bieta',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator', N'Janiszewski Witold',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator', N'Grzybowski Grzegorz',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator', N'Kami�ski Grzegorz',null,318,1);

--INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator Kontraktu Trakcja Tiltra-��d�', N'Janiszewski Witold',null,318,1);
--INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator Kotraktu Bochnia-Biadoliny', N'Grzybowski Grzegorz',null,318,1);
--INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Koordynator Kontraktu LCS Warszawa Zachodni-Skierniewice', N'Kami�ski Grzegorz',null,318,1);

INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBH1', N'Mira Daniel',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBH1', N'Jaranowski Pawe�',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBHH', N'Chmielowska Janina',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK1', N'Madej Dariusz',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK1.5', N'Romanowski Artur',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR2', N'Kry�ski S�awomir',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR3', N'Czerwi�ski Wojciech',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK3', N'Mazur Jan',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR4', N'Ignatiuk Wies�aw',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK4', N'�lizankiewicz Leszek',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR5', N'Tucki Bohdan',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK5', N'Bonk Grzegorz',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR6', N'Gregorasz Andrzej',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK 6', N'Czarny Sebastian',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR7', N'Maszner Wies�aw',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK7.4', N'Primel Walerian',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBR8', N'Krawiec Piotr',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK8', N'Smagur Grzegorz',null,318,1);
INSERT [dbo].[dsg_komorka_osoby] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LBRK8.2', N'Paw�owski Ryszard',null,318,1);
