CREATE TABLE [dbo].[dsg_normal_multiple](
 	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
 	[FIELD_CN] [varchar](100) NOT NULL,
 	[FIELD_VAL] [varchar](100) NULL
 )