CREATE TABLE [dbo].[dsg_mpk_usluga](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_usluga ADD PRIMARY KEY (ID);
create index dsg_mpk_usluga_index on dsg_mpk_usluga (ID);