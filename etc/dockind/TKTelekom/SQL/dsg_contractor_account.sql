CREATE TABLE [dbo].[dsg_contractor_account](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_kontrahenta] [numeric](19,0) NOT NULL,
	[nr_rachunku] [varchar](255) NULL,
	[nazwa_banku] [varchar](255) NULL,
);

ALTER TABLE dsg_contractor_account ADD PRIMARY KEY (ID);
create index dsg_contractor_account_index on dsg_contractor_account (ID);


CREATE TABLE [dbo].[dsg_contractor_account_ext](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_konta] [numeric](19,0) NOT NULL,
	[nr_rachunku] [varchar](255) NULL,
	[nazwa_banku] [varchar](255) NULL,
	[personID] [numeric] (19,0) NULL
);

ALTER TABLE dsg_contractor_account_ext ADD PRIMARY KEY (ID);
create index dsg_contractor_account_ext_index on dsg_contractor_account_ext (ID);


create view dsg_accounts_view as
select 
a.id as id, 
a.id_konta as cn,
a.nr_rachunku as title,
null as centrum,
1 as available,
p.id as refValue
from 
dso_person p
join dsg_contractor_account_ext a on p.wparam=a.personID;

