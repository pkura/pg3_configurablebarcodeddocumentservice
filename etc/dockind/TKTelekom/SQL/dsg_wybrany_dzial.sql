CREATE TABLE [dbo].[dsg_wybrany_dzial](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_wybrany_dzial ADD PRIMARY KEY (ID);
create index dsg_wybrany_dzial_index on dsg_wybrany_dzial (ID);

SET IDENTITY_INSERT [dsg_wybrany_dzial] ON
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (13, 'LOTS2', 'Zesp� Inwestycji i Nadzoru Koszt�w', null, null, 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630001, 'LUBLIN1', 'Lublin - Okopowa 5', null, 'figiel', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630011, 'WARSZAWA',	'Warszawa - Cha�ubi�skiego 4', null, 'domaniuk', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630012, 'BIALYSTOK',	'Bia�ystok - Narewska 4', null, 'dolinowska', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630013, 'LODZ',	'��d� - Unii Lubelskiej 3/5', null, 'krajewski', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630021, 'LUBLIN2',	'Lublin - Rataja 15', null, 'majewski_z', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630031, 'KRAKOW',	'Krak�w - Czechowicza 12', null, 'kwasny', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630032, 'SKARZYSKO_KAM',	'Skar�ysko Kamienna - Niepodleg�o�ci 96B', null, 'madej_m', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630033, 'RZESZOW',	'Rzesz�w - Batorego 24A', null, 'pieniazek', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630041, 'SOSNOWIEC',	'Sosnowiec - 3 Maja 28B', null, 'arendarczyk', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630042, 'GLIWICE',	'Gliwice - Bohater� Getta 12', null, 'skraba', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630043, 'CZESTOCHOWA',	'Cz�stochowa - Pi�sudskiego 2/6', null, 'kowalik', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630051, 'TORUN',	'Toru� - ��dzka 12', null, 'ciechorska', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630052, 'GDANSK',	'Gda�sk - Dyrekcyjna 2-4', null, 'struzik', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630053, 'OLSZTYN',	'Olsztyn - Konstytucji 3 Maja 1A', null, 'czaplinska', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630081, 'SZCZECIN',	'Szczecin - Korzeniowskiego 1', null, 'kulikowska', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630091, 'TCZEW',	'Tczew - Za Dworcem 8', null, 'muszynska', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630061, 'WROCLAW',	'Wroc�aw - Staszica 36a', null, 'walecki', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630062, 'OPOLE',	'Opole - Krakowska 48', null, 'bieganski', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630063, 'LEGNICA',	'Legnica - Scinawska 1', null, 'kiewro', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630071, 'POZNAN',	'Pozna� - Taczaka 10', null, 'malewicz', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630072, 'OSTROW_WLKP',	'Ostr�w Wielkopolski - Wolno�ci 30', null, 'augustyniak_w', 1)
insert into dsg_wybrany_dzial (id, cn, title, centrum, refValue, available) values (630073, 'ZIELONA_GORA',	'Zielona G�ra - Traugutta 10', null, 'staszak', 1)

SET IDENTITY_INSERT [dsg_wybrany_dzial] OFF