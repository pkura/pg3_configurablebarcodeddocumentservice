alter table DSG_NORMAL_DOCKIND add RODZAJ_IN integer;
alter table DSG_NORMAL_DOCKIND add RODZAJ_OUT integer;
alter table DSG_NORMAL_DOCKIND add RODZAJ_INT integer;
alter table DSG_NORMAL_DOCKIND add STATUS int;
alter table DSG_NORMAL_DOCKIND add DATA_GODZINA_WPLYWU datetime;
alter table DSG_NORMAL_DOCKIND add NR_DOKUMENTU varchar(30);
alter table DSG_NORMAL_DOCKIND add NR_KODU_KRESKOWEGO varchar(30);
alter table DSG_NORMAL_DOCKIND add MIEJSCE_PRZECHOWYWANIA varchar(30);
alter table DSG_NORMAL_DOCKIND add POTWIERDZENIE_ODBIORU smallint;
alter table DSG_NORMAL_DOCKIND add JBPM_PROCESS_ID numeric(19, 0); 
alter table DSW_JBPM_TASKLIST add resource_count int not null  default 1;
ALTER TABLE DSW_JBPM_TASKLIST add markerClass varchar(30);
ALTER table DS_ATTACHMENT add field_cn varchar(200);

DELETE DS_DOCUMENT_KIND where name='Zwyk�y dokument';


CREATE TABLE [DSG_RODZAJ_IN](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] int NULL,
	[REFVALUE] varchar(20) NULL, 
	AVAILABLE integer
);

insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('FAKS','Faks',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('EMAIL','E-mail',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('PISMO_PRZYCHODZACE','Pismo przychodz�ce',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('LISTY_IMIENNE','Listy imienne',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('PISMO_PRZYCHODZACE_DROGA_ELEKTRONICZNA','Pismo przychodz�ce drog� elektroniczn�',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('UMOWA','Umowa',1);
insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('DOKUMENTACJA_PRZETARGOWA','Dokumentacja przetargowa',1);

CREATE TABLE [DSG_RODZAJ_OUT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] int NULL,
	[REFVALUE] varchar(20) NULL, 
	AVAILABLE integer
);

insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('FAKS','Faks',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('EMAIL','E-mail',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('PISMO_WYCHODZACE','Pismo wychodz�ce',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('LISTY_IMIENNE','Listy imienne',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('PISMO_WYCHODZACE_DROGA_ELEKTRONICZNA','Pismo wychodz�ce drog� elektroniczn�',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('UMOWA','Umowa',1);
insert into DSG_RODZAJ_OUT(CN,TITLE,AVAILABLE)values('DOKUMENTACJA_PRZETARGOWA','Dokumentacja przetargowa (Po za obiegiem kancelaryjnym w recepcji)',1);

CREATE TABLE [DSG_RODZAJ_INT](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CN] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] int NULL,
	[REFVALUE] varchar(20) NULL, 
	AVAILABLE integer
);

update DSO_IN_DOCUMENT_DELIVERY set NAME = 'Faks' where id = 1;
update DSO_IN_DOCUMENT_DELIVERY set NAME = 'E-mail' where id = 2;
update DSO_IN_DOCUMENT_DELIVERY set NAME = 'Kurier' where id = 3;
insert into DSO_IN_DOCUMENT_DELIVERY(POSN,NAME)values(4,'Poczta');
insert into DSO_IN_DOCUMENT_DELIVERY(POSN,NAME)values(5,'Osobi�cie');
insert into DSO_IN_DOCUMENT_DELIVERY(POSN,NAME)values(6,'Wewn�trzne');

update DSO_OUT_DOCUMENT_DELIVERY set NAME = 'Kurier' where id = 1;
update DSO_OUT_DOCUMENT_DELIVERY set NAME = 'Zwykle' where id = 2;
insert into DSO_OUT_DOCUMENT_DELIVERY(POSN,NAME)values(3,'Priorytetowe');
insert into DSO_OUT_DOCUMENT_DELIVERY(POSN,NAME)values(4,'Polecony');
insert into DSO_OUT_DOCUMENT_DELIVERY(POSN,NAME)values(5,'Polecony priorytetowy');
insert into DSO_OUT_DOCUMENT_DELIVERY(POSN,NAME)values(6,'Za potwierdzeniem odbioru');