CREATE TABLE [dbo].[dsg_mpk_konto](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_konto ADD PRIMARY KEY (ID);
create index dsg_mpk_konto_index on dsg_mpk_konto (ID);

INSERT [dbo].[dsg_mpk_konto] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'01110000', N'Grunty w�asne', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_konto] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'01123000', N'Sp�dzielcze w�asno�ciowe prawo do lokalu mieszkalnego i u�ytkowego', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_konto] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'06110000', N'Grunty w�asne - odpisy aktualizuj�ce warto��', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_konto] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'06421000', N'Budynki i lokale - odpisy aktualizuj�ce warto��', NULL, NULL, 1);

CREATE TABLE [dbo].[dsg_metoda_platnosci](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);
