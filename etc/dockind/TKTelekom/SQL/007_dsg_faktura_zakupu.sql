create view [dbo].[dsg_logzam_view] as
select ID as id,
GUID as cn,
NAME as title,
CODE as refValue,
null as centrum,
1 as available
from DS_DIVISION
where CODE in ('LAZ', 'LALL1', 'LALL2', 'LALL3', 'LALL4');
GO

-- Przy wyborze w polu: Nazwa dokumentu, warto�ci: nota , pismo, rachunek
update dsg_nazwa_dokumentu set available = 1 where id in (1,5);
alter table dsg_faktura_Zakupu add kwota_brutto_z_dok numeric (18,2);

alter table dsg_faktura_zakupu add skan numeric (18);
alter table dsg_faktura_zakupu add skan_ot numeric (18);

alter table dsg_faktura_zakupu add SUMA_MPK numeric (18,2);

CREATE TABLE dsg_kierownicy_lfr
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	kierownik_lfr numeric(18,0)
);

ALTER TABLE dsg_kierownicy_lfr ADD PRIMARY KEY (ID);

alter table dsg_faktura_Zakupu add nie_podlega_vat smallint;