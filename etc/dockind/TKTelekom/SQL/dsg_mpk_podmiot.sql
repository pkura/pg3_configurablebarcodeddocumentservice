CREATE TABLE [dbo].[dsg_mpk_podmiot](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_podmiot ADD PRIMARY KEY (ID);
create index dsg_mpk_podmiot_index on dsg_mpk_podmiot (ID);

INSERT [dbo].[dsg_mpk_podmiot] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_podmiot] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0010', N'Dostawca - odzysk', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_podmiot] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0113', N'Telekom - EXATEL S.A.', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_podmiot] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0205', N'Telekom - INTERNETIA sp. z o.o.', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_podmiot] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0313', N'Kolej poza PKP - Pozostali', NULL, NULL, 1);