insert into DSG_RODZAJ_IN(CN,TITLE,AVAILABLE)values('LIST','List',1);

alter table dsg_normal_dockind add miejsce_przechowywania_list numeric (18,0);


CREATE TABLE dsg_miejsce_przechowywania (
	id numeric (18,0) identity (1,1),
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available integer default 1
);

insert into dsg_miejsce_przechowywania (cn, title, centrum, refValue, available)
values ('WAW_KIJOWSKA', "Warszawa, ul. Kijowska 10/12A", null, null, 1);