CREATE TABLE [dbo].[dsg_mpk_projekt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_projekt ADD PRIMARY KEY (ID);
create index dsg_mpk_projekt_index on dsg_mpk_projekt (ID);

INSERT [dbo].[dsg_mpk_projekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_projekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0101', N'Budowa/Rozbudowa infrastruktury serwerowej i system�w dyskowych', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_projekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0203', N'Urz�dzenia klimatyzacyjne', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_projekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'0405', N'Kanalizacja teletechniczna', NULL, NULL, 1);
