CREATE TABLE [dbo].[dsg_rodz_zak_inwest](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_rodz_zak_inwest ADD PRIMARY KEY (ID);
create index dsg_rodz_zak_inwest_index on dsg_rodz_zak_inwest (ID);

INSERT [dbo].[dsg_rodz_zak_inwest] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'NSTB', N'Nowe �rodki trwa�e w budowie', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_zak_inwest] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'GD', N'Gotowe dobro', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_zak_inwest] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'USTW', N'Ulepszenie �rodk�w trwa�ych w�asnych', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_zak_inwest] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'USTO', N'Ulepszenie �rodk�w trwa�ych obcych', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_zak_inwest] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ZUST', N'Zakup u�ywanych �rodk�w trwa�ych', NULL, NULL, 1);