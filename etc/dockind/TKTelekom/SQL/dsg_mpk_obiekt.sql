CREATE TABLE [dbo].[dsg_mpk_obiekt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_obiekt ADD PRIMARY KEY (ID);
create index dsg_mpk_obiekt_index on dsg_mpk_obiekt (ID);

INSERT [dbo].[dsg_mpk_obiekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_obiekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'001', N'Centrale Cyfrowe i elektroniczne abonenckie', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_obiekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'016', N'Urządzenia łaczności satelitarnej', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_obiekt] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'119', N'Urządzenia dostępowe GSM', NULL, NULL, 1);
