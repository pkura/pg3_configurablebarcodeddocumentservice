CREATE TABLE [dbo].[dsg_rodz_majatku_trwalego](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_rodz_majatku_trwalego ADD PRIMARY KEY (ID);
create index dsg_rodz_majatku_trwalego_index on dsg_rodz_majatku_trwalego (ID);

INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'WNiP_W', N'WNiP_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'WNiP_N', N'WNiP_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_1_W', N'ST_GRUPA_1_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_1_N', N'ST_GRUPA_1_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_2_W', N'ST_GRUPA_2_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_2_N', N'ST_GRUPA_2_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_3_W', N'ST_GRUPA_3_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_3_N', N'ST_GRUPA_3_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_4_W', N'ST_GRUPA_4_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_4_N', N'ST_GRUPA_4_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_5_W', N'ST_GRUPA_5_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_5_N', N'ST_GRUPA_5_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_6_W', N'ST_GRUPA_6_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_6_N', N'ST_GRUPA_6_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_7_W', N'ST_GRUPA_7_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_7_N', N'ST_GRUPA_7_N', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_8_W', N'ST_GRUPA_8_W', NULL, NULL, 1);
INSERT [dbo].[dsg_rodz_majatku_trwalego] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'ST_GRUPA_8_N', N'ST_GRUPA_8_N', NULL, NULL, 1);
