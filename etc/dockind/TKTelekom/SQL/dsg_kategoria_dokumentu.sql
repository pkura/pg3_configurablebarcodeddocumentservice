CREATE TABLE [dbo].[dsg_kategoria_dokumentu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_kategoria_dokumentu ADD PRIMARY KEY (ID);
create index dsg_kategoria_dokumentu_index on dsg_kategoria_dokumentu (ID);

insert into dsg_kategoria_dokumentu values ('ZALICZKA_PRAC', 'Zaliczki Pracownicze', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ROZR_PRAC',	'Pozosta�e rozrachunki z pracownikami', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PRZEDPLATA',	'Przedp�ata dla dostawcy', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ZALICZKA_KOM',	'Zaliczki komornicze', null, null, 1)
insert into dsg_kategoria_dokumentu values ('FAKT_VAT_KRAJ',	'Zobowi�zania wobec dostawc�w krajowych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('FAKT_VAT_ZAGR',	'Zobowi�zania wobec dostawc�w zagranicznych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('FAKT_KOR_VAT_KRAJ',	'Korekta zobowi�zania wobec dostawc�w krajowych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('FAKT_KOR_VAT_ZAGR',	'Korekta zobowi�zania wobec dostawc�w zagranicznych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('DOKUMENT_NIE_VAT',	'Inne dokumenty np. Rachunki, dyspozycje na pi�mie', null, null, 1)
insert into dsg_kategoria_dokumentu values ('KOREKTA_DOKUMENTU_NIE_VAT',	'Korekty innych dokument�w np.. Rachunki, dyspozycje na pi�mie', null, null, 1)
insert into dsg_kategoria_dokumentu values ('WYCENA_BILANSOWA_DODATNIA',	'Dokumenty zwi�zane z wycen� bilansow� dodatni�', null, null, 1)
insert into dsg_kategoria_dokumentu values ('WYCENA_BILANSOWA_UJEMNA',	'Dokumenty zwi�zane z wycen� bilansow� ujemn�', null, null, 1)
insert into dsg_kategoria_dokumentu values ('NOTA_ODSETKOWA',	'Noty odsetkowe', null, null, 1)
insert into dsg_kategoria_dokumentu values ('WADIA_KAUCJE_GWARANCJE',	'Dokumenty zwi�zane z ewidencj� wadi�w, kaucji i gwarancji', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ROZNICE_KURSOWE_DODATNIE',	'Dokumenty zwi�zane z ewidencj� zrealizowanych dodatnich r�nic kursowych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ROZNICE_KURSOWE_UJEMNE',	'Dokumenty zwi�zane z ewidencj� zrealizowanych ujemnych r�nic kursowych', null, null, 1)
insert into dsg_kategoria_dokumentu values ('LEASING',	'Dokumenty zwi�zane z ewidencj� leasingu', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PODATEK_NIER',	'Podatek od nieruchomo�ci', null, null, 1)
insert into dsg_kategoria_dokumentu values ('OPLATY_SKARBOWE',	'Op�aty skarbowe', null, null, 1)
insert into dsg_kategoria_dokumentu values ('INNE_PUBL_PRAW',	'Inne rozliczenia publiczno-prawne', null, null, 1)
insert into dsg_kategoria_dokumentu values ('BILANS_OTWARCIA',	'Bilans otwarcia', null, null, 1)
insert into dsg_kategoria_dokumentu values ('SPR_PLAT',	'P�atno�ci Zobowi�za� uregulowane przelewem', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ROZL_PLAT',	'P�atno�ci Zobowi�za� uregulowane rozliczeniem', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PLAT_ELEKTR',	'P�atno�ci Zobowi�za� uregulowane elektronicznie', null, null, 1)
insert into dsg_kategoria_dokumentu values ('ZOBOWIAZANIA',	'Sp�ata zwi�zana z p�atno�ci� dostawcy - Zobowi�zania', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PLAT_GOTOWKA',	'P�atno�ci got�wkowe', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PLAT_TECH',	'P�atno�ci techniczne', null, null, 1)
insert into dsg_kategoria_dokumentu values ('PLAT_KARTA',	'P�atno�ci kart�', null, null, 1)
