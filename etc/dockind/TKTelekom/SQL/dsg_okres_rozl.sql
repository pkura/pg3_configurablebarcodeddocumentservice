CREATE TABLE [dbo].[dsg_okres_rozl](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_okres_rozl ADD PRIMARY KEY (ID);
create index dsg_okres_rozl_index on dsg_okres_rozl (ID);

INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'STYCZEN', N'Stycze�', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LUTY', N'Luty', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'MARZEC', N'Marzec', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'KWIECIEN', N'Kwiecie�', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'MAJ', N'Maj', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'CZERWIEC', N'Czerwiec', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LIPIEC', N'Lipiec', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'SIERPIEN', N'Sierpie�', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'WRZESIEN', N'Wrzesie�', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PAZDZIERNIK', N'Pa�dziernik', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LISTOPAD', N'Listopad', NULL, NULL, 1);
INSERT [dbo].[dsg_okres_rozl] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'GRUDZIEN', N'Grudzie�', NULL, NULL, 1);