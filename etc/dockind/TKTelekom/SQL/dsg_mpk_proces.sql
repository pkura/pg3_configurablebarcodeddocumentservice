CREATE TABLE [dbo].[dsg_mpk_proces](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_proces ADD PRIMARY KEY (ID);
create index dsg_mpk_proces_index on dsg_mpk_proces (ID);

INSERT [dbo].[dsg_mpk_proces] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_proces] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'101', N'Reprezentacja', NULL, NULL, 1);
