CREATE TABLE [dbo].[dsg_mpk_rezerwa](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_rezerwa ADD PRIMARY KEY (ID);
create index dsg_mpk_rezerwa_index on dsg_mpk_rezerwa (ID);

INSERT [dbo].[dsg_mpk_rezerwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_rezerwa] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'101', N'Rezerwa', NULL, NULL, 1);