CREATE TABLE [dbo].[dsg_sposob_zaplaty](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_sposob_zaplaty ADD PRIMARY KEY (ID);
create index dsg_sposob_zaplaty_index on dsg_sposob_zaplaty (ID);

INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Karta', N'P�atno�� kart�', NULL, NULL, 1);
INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Przelew', N'P�atno�� przelewem', NULL, NULL, 1);
INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Rozliczenie', N'Rozliczanie p�atno�ci', NULL, NULL, 1);
INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Elektroniczne', N'Metoda p�atno�ci elektronicznej', NULL, NULL, 1);
INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Techniczna', N'P�atno�� techniczna', NULL, NULL, 1);
INSERT [dbo].[dsg_sposob_zaplaty] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'Got�wka', N'P�atno�� got�wk�', NULL, NULL, 1);