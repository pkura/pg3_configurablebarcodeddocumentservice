CREATE TABLE dsg_faktura_zakupu (
	DOCUMENT_ID 		bigint NOT NULL,
	STATUS 				bigint,
	RODZAJ_ZAKUPU 		int,
	DATA_WYSTAWIENIA 	date,
	DATA_WPLYWU 		date,
	NAZWA_DOKUMENTU 	int,
	NR_FAKTURY 			varchar(30),
	KONTRAHENT_NR_KONTA	int,
	PODSTAWA_ZAKUPU		varchar(4096),
	KOMORKA				bigint,
	KWOTA_NETTO			numeric(18,2),
	KWOTA_BRUTTO		numeric(18,2),
	KWOTA_VAT			numeric(18,2),
	OPIS_MERYTORYCZNY	varchar(4096),
	OKRES_ROZL			int,
	TERMIN_PLAT_Z_UMOWY	date,
	TERMIN_ZAPLAT_Z_FAKT date,
	KATEGORIA_DOKUMENTU	int,
	NR_REJESTRU_VAT		varchar(30),
	NR_IWT				varchar(30),
	RODZ_ZAK_INWEST		int,
	NR_MAGAZYNU			varchar(30),
	SPOSOB_ZAPLATY		int,
	DATA_KSIEGI_GL		date,
	NR_OEBS				int,
	WALUTA				int,
	BARCODE				varchar(30),
	TYP_DOKUMENTU		int,
	FAKT_ROZL			int,
	METODA_PLATNOSCI	varchar(30),
	GRUPA_PLATNOSCI		varchar(30),
	KURS_WYMIANY_WALUTY	int,
	DATA_KURSU_WYMIANY	date,
	OPIS_KSIEGOWY		varchar(4096),
	KOMORKA_OSOBY		int
);


ALTER TABLE dsg_faktura_zakupu ADD PRIMARY KEY (document_id);
create index dsg_faktura_zakupu_index on dsg_faktura_zakupu (document_id);


CREATE TABLE dsg_faktura_zakupu_multiple (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ;

