CREATE TABLE [dbo].[dsg_nazwa_dokumentu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_nazwa_dokumentu ADD PRIMARY KEY (ID);
create index dsg_nazwa_dokumentu_index on dsg_nazwa_dokumentu (ID);

INSERT [dbo].[dsg_nazwa_dokumentu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FAKT', N'Faktura', NULL, NULL, 1);
INSERT [dbo].[dsg_nazwa_dokumentu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'RACH', N'Rachunek', NULL, NULL, 1);
INSERT [dbo].[dsg_nazwa_dokumentu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'NOTA', N'Nota', NULL, NULL, 1);
INSERT [dbo].[dsg_nazwa_dokumentu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PISMO', N'Pismo', NULL, NULL, 1);
INSERT [dbo].[dsg_nazwa_dokumentu] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PP�AT', N'Przedp�ata', NULL, NULL, 1);
