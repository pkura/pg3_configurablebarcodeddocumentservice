CREATE TABLE dsg_os_merytoryczne
(
	ID numeric(18,0) identity(1,1) NOT NULL,
	OS_MERYTORYCZNA numeric(18,0)
);

ALTER TABLE dsg_os_merytoryczne ADD PRIMARY KEY (ID);
create index dsg_os_merytoryczne_index on dsg_os_merytoryczne (ID);