CREATE TABLE [dbo].[dsg_mpk_rpk](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

ALTER TABLE dsg_mpk_rpk ADD PRIMARY KEY (ID);
create index dsg_mpk_rpk_index on dsg_mpk_rpk (ID);

INSERT [dbo].[dsg_mpk_rpk] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'000', N'', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_rpk] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'101', N'Rada Nadzorcza', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_rpk] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'173', N'PW- Zesp� Logistyki LALL2 Gda�sk', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_rpk] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'213', N'PO- Zesp� Interkonektu', NULL, NULL, 1);
INSERT [dbo].[dsg_mpk_rpk] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'413', N'PRT- Zesp� wsp�pracy z Pionem Operatorskim', NULL, NULL, 1);