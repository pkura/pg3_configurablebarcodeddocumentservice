CREATE TABLE DSG_SWD_USER
(
    ID                      NUMERIC(18,0) IDENTITY NOT NULL,
    SWDUID                  NUMERIC(18,0),
    FIRST_NAME              VARCHAR(100),
    LAST_NAME               VARCHAR(100),
    STATUS_NOTE             VARCHAR(100)
);