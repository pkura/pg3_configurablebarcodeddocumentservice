CREATE TABLE [dbo].[DSG_SWD_CERTIFICATES](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL  PRIMARY KEY CLUSTERED ,
	[STATUS] [numeric](18, 0) NULL,
	[ORGANISATION_UNIT_ID] [numeric](18, 0) NULL,
	[DOCUMENT_KIND] [numeric](18, 0) NULL,
	[SYSTEM] [numeric](18, 0) NULL,
	[TEXT] [nvarchar](max) NULL,
	[DATE_IN] [date] NULL,
	[DATE_HANDLE] [date] NULL,
	[SCAN_ID] [numeric](18, 0) NULL,
	[OBCIAZENIE] [numeric](18, 0) NULL,
	[DODATKOWA_OBSLUGA] [numeric](18, 0) NULL,
)
CREATE TABLE DSG_SWD_CERTIFICATES_MULTIPLE_VALUE (
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN]    [varchar](100)   NOT NULL,
	[FIELD_VAL]   [varchar](100)   NULL
);
