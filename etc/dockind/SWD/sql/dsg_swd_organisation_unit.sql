CREATE TABLE [dbo].[DSG_SWD_ORGANISATION_UNIT](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[NAME] [nvarchar](100) NULL,
	[SWDID] [numeric](18, 0) NOT NULL
)


