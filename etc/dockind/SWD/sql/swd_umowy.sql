CREATE TABLE [dbo].[DSG_SWD_UMOWY](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL PRIMARY KEY CLUSTERED,
	[STATUS] [numeric](18, 0) NULL,
	[ORGANISATION_UNIT_ID] [numeric](18, 0) NULL,
	[DOCUMENT_KIND] [numeric](18, 0) NULL,
	[DOCUMENT_TYPE] [numeric](18, 0) NULL,
	[SYSTEM] [numeric](18, 0) NULL,
	[TEXT] [nvarchar](max) NULL,
	[DATE_IN] [date] NULL,
	[DATE_HANDLE] [date] NULL,
	[SCAN_ID] [numeric](18, 0) NULL,
)



CREATE TABLE [dbo].[DSG_SWD_UMOWY_MULTIPLE_VALUE](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
)