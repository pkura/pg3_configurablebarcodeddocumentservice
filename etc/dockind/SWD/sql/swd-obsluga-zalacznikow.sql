CREATE TABLE DSG_SWD_OBSLUGA_ZALACZNIKOW
(
    DOCUMENT_ID             NUMERIC(18,0)   NOT NULL  PRIMARY KEY CLUSTERED,
    STATUS                  NUMERIC(18,0),
	ORGANISATION_UNIT	    NUMERIC(18,0),
    OPERATION_TYPE          NUMERIC(18,0),
    SYSTEM                  NUMERIC(18,0),
    SCAN                    NUMERIC(18,0),
    MSG_TEXT                VARCHAR(1024),
    DATE_IN                 TIME,
    DATE_HANDLE             TIME
);

CREATE TABLE DSG_SWD_OBSLUGA_ZALACZNIKOW_MULTIPLE_VALUE
(
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN]    [varchar](100)   NOT NULL,
	[FIELD_VAL]   [varchar](100)   NULL
);
