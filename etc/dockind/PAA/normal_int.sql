-- wykaz jako dokument (dockind) PAA przychodzace dockind

CREATE TABLE [DS_PAA_INT_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_int] [int] NULL,
	[statusdokumentu] [int] NULL,
	[SENDER] [nvarchar](50) NULL,
	[realizationDate] [date] null,
	[RECIPIENT] [nvarchar](50) NULL
) ON [PRIMARY]

CREATE TABLE [DS_PAA_RODZAJ_INT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	)
	 ON [PRIMARY]
	 
	 INSERT INTO DS_PAA_RODZAJ_INT
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'Faktury',0,null,1),
           (2,'Umowy', 0,null,1),
           (3,'Skargi',0,null,1),
           (4,'Wnioski',0,null,1),
           (5,'Pisma inne',0,null,1);
