
alter table DS_PAA_OUT_DOC add GABARYT numeric(18, 0) null;
alter table DS_PAA_OUT_DOC add RODZAJ_PRZESY�KI_TYPE numeric(18, 0) null;
alter table DS_PAA_OUT_DOC add RODZAJ_PRZESY�KI_TYPE_WEIGHT_COUNTRY numeric(18, 0) null;
alter table DS_PAA_OUT_DOC add RODZAJ_PRZESY�KI_TYPE_WEIGHT_WORLD numeric(18, 0) null;
alter table DS_PAA_OUT_DOC add RODZAJ_PRZESY�KI numeric(18, 0) null;


drop table DS_OUT_DELIVERY_DOC_RODZAJ;
drop table DS_OUT_DELIVERY_GABARYT;
drop table DS_OUT_DELIVERY_DOC_RODZAJ_TYPE;
drop table DS_OUT_DELIVERY_COUNTRY_WEIGHT;
drop table DS_OUT_DELIVERY_WORLD_WEIGHT;



CREATE TABLE [dbo].[DS_OUT_DELIVERY_DOC_RODZAJ](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] not NULL,
	[poziomPrzesy�ki] [int] not NULL
) ON [PRIMARY]
-- rodzaje przesy�ek 
insert into DS_OUT_DELIVERY_DOC_RODZAJ values ( '1', 'Krajowa ekonomiczna' , null ,'1',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ values ( '2', 'Krajowa priorytetowa' , null ,'1',1,2);
insert into DS_OUT_DELIVERY_DOC_RODZAJ values ( '3', 'Zagraniczna ekonomiczna' , null ,'1',1,3);
insert into DS_OUT_DELIVERY_DOC_RODZAJ values ( '4', 'Zagraniczna priorytetowa' , null ,'1',1,4);


-- rodzaje Gabaryt�w 
CREATE TABLE [dbo].[DS_OUT_DELIVERY_GABARYT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[poziomGabarytu] [int] not NULL
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_GABARYT values ( '1', 'KRAJ - Gabaryt A', null ,'1',1,1);
insert into DS_OUT_DELIVERY_GABARYT values ( '2', 'KRAJ - Gabaryt B', null ,'1',1,2);
insert into DS_OUT_DELIVERY_GABARYT values ( '3', '�WIAT - Gabaryt A', null ,'1',1,3);
insert into DS_OUT_DELIVERY_GABARYT values ( '4', '�WIAT - Gabaryt B', null ,'1',1,4);
insert into DS_OUT_DELIVERY_GABARYT values ( '5', '�WIAT - Gabaryt C', null ,'1',1,5);
insert into DS_OUT_DELIVERY_GABARYT values ( '6', '�WIAT - Gabaryt D', null ,'1',1,6);

-- Typy Listu 
CREATE TABLE [dbo].[DS_OUT_DELIVERY_DOC_RODZAJ_TYPE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[poziomListu] [int] not NULL
	
) ON [PRIMARY]
--kraj
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '1', 'List Zwyk�y', null ,'1',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '2', 'List Polecony', null ,'1',1,2);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '3', 'List Polecony zpo', null ,'1',1,3);

insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '4', 'List Zwyk�y', null ,'2',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '5', 'List Polecony', null ,'2',1,2);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '6', 'List Polecony zpo', null ,'2',1,3);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '7', 'List Zwyk�y', null ,'3',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '8', 'List Polecony', null ,'3',1,2);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '9', 'List Zwyk�y', null ,'4',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '10', 'List Zwyk�y', null ,'5',1,1);
insert into DS_OUT_DELIVERY_DOC_RODZAJ_TYPE values ( '11', 'List Zwyk�y', null ,'6',1,1);



-- rodzaje wag kraj
CREATE TABLE [dbo].[DS_OUT_DELIVERY_COUNTRY_WEIGHT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[poziomWagi] [int] not NULL
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '1', 'Do 350 gram', null ,'1',1,3);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '2', 'Do 1000 gram', null ,'1',1,5);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '3', 'Do 2000 gram', null ,'1',1,6);

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '4', 'Do 350 gram', null ,'2',1,3);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '5', 'Do 1000 gram', null ,'2',1,5);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '6', 'Do 2000 gram', null ,'2',1,6);

-- rodzaje wag �wiat
CREATE TABLE [dbo].[DS_OUT_DELIVERY_WORLD_WEIGHT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [int] NULL,
	[poziomWagi] [int] not null
	
) ON [PRIMARY]

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '1', 'Do 50 gram' , null ,'3',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '2', 'Do 100 gram', null ,'3',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '3', 'Do 350 gram', null ,'3',1,3);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '4', 'Do 500 gram', null ,'3',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '5', 'Do 1000 gram', null ,'3',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '6', 'Do 2000 gram', null ,'3',1,6);

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '7', 'Do 50 gram' , null ,'4',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '8', 'Do 100 gram', null ,'4',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '9', 'Do 350 gram', null ,'4',1,3);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '10', 'Do 500 gram', null ,'4',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '11', 'Do 1000 gram', null ,'4',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '12', 'Do 2000 gram', null ,'4',1,6);

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '13', 'Do 50 gram' , null ,'5',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '14', 'Do 100 gram', null ,'5',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '15', 'Do 350 gram', null ,'5',1,3);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '16', 'Do 500 gram', null ,'5',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '17', 'Do 1000 gram', null ,'5',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '18', 'Do 2000 gram', null ,'5',1,6);


insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '19', 'Do 50 gram' , null ,'6',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '20', 'Do 100 gram', null ,'6',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '21', 'Do 350 gram', null ,'6',1,3);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '22', 'Do 500 gram', null ,'6',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '23', 'Do 1000 gram', null ,'6',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '24', 'Do 2000 gram', null ,'6',1,6);




drop table DS_STAWKI_OPLAT_POCZTOWYCH;

CREATE TABLE DS_STAWKI_OPLAT_POCZTOWYCH
(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
waga varchar(20) not null,
poziomPrzesy�ki smallint not null,
poziomGabarytu smallint not null,
poziomListu smallint not null,
poziomWagi smallint not null,
kwota numeric(6, 2) not null,
paczka smallint not null
);
-- kraj  economy
-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 
--3 , 5 ,6 

-- zwykly economy A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 1, 1, 3, 1.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 1, 1, 5, 3.70 ,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 1, 6, 6.30,0);

-- polecony A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 1, 2, 3, 3.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 1, 2, 5, 5.90,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 2, 6, 8.50,0);

-- polecony  zpo A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 1, 3, 3, 5.70,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 1, 3, 5, 7.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 3, 6, 10.40,0);



-- zwykly economy B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 2, 1, 3, 3.75,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 2, 1, 5, 4.75,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 2, 1, 6, 7.30,0);

-- zwykly polecony B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 2, 2, 3, 5.95,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 2, 2, 5, 6.95,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 2, 2, 6, 9.50,0);

--zwykly polecony  zpo B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 2, 3, 3, 7.85,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 2, 3, 5, 8.85,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 3, 6, 11.40,0);


-- kraj  priorytyet 
-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 

-- Priorytetowy  zwykly  A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 1, 1, 3, 2.35,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 1, 1, 5, 4.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 1, 1, 6, 8.80,0);

-- Priorytetowy polecony  A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 1, 2, 3, 4.55,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 1, 2, 5, 6.70,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 1, 2, 6, 11.00,0);

-- Priorytetowy polecony  zpo A kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 1, 3, 3, 6.45,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 1, 3, 5, 8.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 1, 3, 6, 12.90,0);



-- Priorytetowy zwykly B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 2, 1, 3, 5.10,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 2, 1, 5, 7.10,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 2, 1, 6, 10.90,0);

-- Priorytetowy polecony B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 2, 2, 3, 7.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 2, 2, 5, 9.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 2, 2, 6, 13.10,0);

-- Priorytetowy polecony  zpo B kraj
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    2, 2, 3, 3, 9.20,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   2, 2, 3, 5, 11.20,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   2, 1, 3, 6, 15.00,0);


-- �wiat  economy 

-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 

-- zwykly economy A �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     3, 3, 1, 1, 4.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    3, 3, 1, 2, 9.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    3, 3, 1, 3, 10.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    3, 3, 1, 4, 11.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   3, 3, 1, 5, 21.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   3, 3, 1, 6, 40.90,0);

-- polecony A �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     3, 3, 2, 1, 9.90,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    3, 3, 2, 2, 14.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    3, 3, 2, 3, 15.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    3, 3, 2, 4, 16.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   3, 3, 2, 5, 26.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   3, 3, 2, 6, 46.20,0);


-- zwykly economy B �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     3, 4, 1, 1, 4.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    3, 4, 1, 2, 9.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    3, 4, 1, 3, 10.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    3, 4, 1, 4, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   3, 4, 1, 5, 21.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   3, 4, 1, 6, 41.40,0);


-- zwykly economy C �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     3, 5, 1, 1, 4.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    3, 5, 1, 2, 9.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    3, 5, 1, 3, 10.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    3, 5, 1, 4, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   3, 5, 1, 5, 21.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   3, 5, 1, 6, 41.40,0);


-- zwykly economy D �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     3, 6, 1, 1, 4.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    3, 6, 1, 2, 9.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    3, 6, 1, 3, 10.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    3, 6, 1, 4, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   3, 6, 1, 5, 21.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   3, 6, 1, 6, 41.40,0);


--swiat prorytet
-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 

-- Priorytetowy zwykly economy A �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     4, 3, 1, 1, 5.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    4, 3, 1, 2, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    4, 3, 1, 3, 13.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    4, 3, 1, 4, 15.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   4, 3, 1, 5, 29.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   4, 3, 1, 6, 58.90,0);

-- Priorytetowy polecony A �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     4, 3, 2, 1, 10.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    4, 3, 2, 2, 16.80,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    4, 3, 2, 3, 18.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    4, 3, 2, 4, 20.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   4, 3, 2, 5, 34.60,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   4, 3, 2, 6, 64.20,0);


--Priorytetowy zwykly economy B �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     4, 4, 1, 1, 5.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    4, 4, 1, 2, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    4, 4, 1, 3, 13.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    4, 4, 1, 4, 19.30,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   4, 4, 1, 5, 38.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   4, 4, 1, 6, 77.00,0);


-- Priorytetowy zwykly economy C �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     4, 5, 1, 1, 5.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    4, 5, 1, 2, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    4, 5, 1, 3, 13.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    4, 5, 1, 4, 24.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   4, 5, 1, 5, 45.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   4, 5, 1, 6, 95.00,0);


-- Priorytetowy zwykly economy D �wiat
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     4, 6, 1, 1, 5.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    4, 6, 1, 2, 11.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    4, 6, 1, 3, 13.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    4, 6, 1, 4, 34.50,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   4, 6, 1, 5, 68.00,0);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   4, 6, 1, 6, 147.00,0);

CREATE TABLE DS_PRZESYLKI_POCZTOWE
(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
DOCUMENT_ID numeric(18, 0) not null,
DATA_OPERACJI date not null,
USER_ID (19, 0) not null,
przedzialWagowy varchar(50) null,
poziomPrzesy�ki smallint  null,
poziomGabarytu smallint  null,
poziomListu smallint  null,
poziomWagi smallint  null,
);
