-- wykaz jako dokument (dockind) PAA przychodzace dockind
CREATE TABLE [DS_PAA_IN_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[WYKAZ_NUMBER] [int] NULL,
	[rodzaj_in] [int] NULL,
	[send_kind] [int] NULL,
	[signature] [varchar](150) NULL,
	[realizationDate] [date] null,
	[RECIPIENT] [numeric](18, 0) NULL,
	[znak_pisma] [nvarchar](50) NULL
) ON [PRIMARY]

CREATE TABLE [DS_PAA_RODZAJ_IN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	)
	 ON [PRIMARY]


	 
CREATE TABLE DS_PAA_MULTIPLE (
ID numeric(18,0) identity(1,1) NOT NULL,
     DOCUMENT_ID numeric(18, 0) NOT NULL,
     FIELD_CN varchar(100) NOT NULL,
     FIELD_VAL varchar(100) NULL
);
 

 INSERT INTO DS_PAA_RODZAJ_IN
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'Faktury',0,null,1),
           (2,'Umowy', 0,null,1),
           (3,'Skargi',0,null,1),
           (4,'Wnioski',0,null,1),
           (5,'Pisma inne',0,null,1);
           
--create view [DS_PAA_V_USERDICT_AND_DIV] as 
--select U.id , U.name , U.firstname, U.lastname , D.NAME as division
--from DS_USER U
--join DS_USER_TO_DIVISION U_D on U_D.[USER_ID]= U.ID
--join DS_DIVISION D on D.ID = U_D.DIVISION_ID
--where DS_DIVISION.division

           GO
           
CREATE VIEW [DS_PAA_V_USERDICT_AND_DIV]
AS
SELECT     U.ID, U.NAME, U.FIRSTNAME, U.LASTNAME, D.NAME AS division, D.DIVISIONTYPE
FROM         dbo.DS_USER AS U INNER JOIN
                      dbo.DS_USER_TO_DIVISION AS U_D ON U_D.USER_ID = U.ID INNER JOIN
                      dbo.DS_DIVISION AS D ON D.ID = U_D.DIVISION_ID
WHERE     (D.DIVISIONTYPE = 'division') OR
                      (D.DIVISIONTYPE = 'position')
       