
CREATE TABLE [dbo].[DS_PAA_PLAN_PRACY](
	[DOCUMENT_ID] [numeric](19, 0) NULL
);

CREATE TABLE [dbo].[DS_PAA_PLAN_PRACY_ZAD](
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[TYP_OBIEKTU] [int] NULL,
	[TYP_ZADANIA] [int] NULL,
	[OPIS] [varchar](1024) NULL,
	[DATAOD] [datetime] NULL,
	[DATADO] [datetime] NULL,
	[STAN] [varchar](1024) NULL,
	[WYKONAWCA] [numeric](19, 0) NULL,
	[KOMORKA_WIODACA] [numeric](19, 0) NULL
);

CREATE TABLE [dbo].[DS_PAA_PLAN_PRACY_PODZAD](
	[DOCUMENT_ID] [numeric](19, 0) NULL,
	[TYP_ZADANIA] [int] NULL,
	[OPIS] [varchar](1024) NULL,
	[DATAOD] [datetime] NULL,
	[DATADO] [datetime] NULL,
	[STAN] [varchar](1024) NULL,
	[PROCENT] [smallint] NULL,
	[WYKONAWCA] [numeric](19, 0) NULL,
	[KOMORKA_WIODACA] [numeric](19, 0) NULL
);


CREATE TABLE DS_PAA_ZADANIA_MULTIPLE (
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);
