

-- dodanie kolumny do listy wyszukiwania
alter table DS_PAA_IN_DOC add PELENADRES varchar(200) null;
alter table DS_PAA_OUT_DOC add PELENADRES varchar(200) null;
alter table DS_PAA_INT_DOC add PELENADRES varchar(200) null;



CREATE TABLE DS_PAA_MULTIPLE_ODBIORCA(
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

alter table DS_PAA_OUT_DOC add PODZIELPISMA smallint null;
alter table DS_PAA_OUT_DOC add PODZIELONE smallint null;



alter table DS_PAA_ODBIORCA add RODZAJ_PRZESY�KI int  null;
alter table DS_PAA_ODBIORCA add KOSZT_PRZESYLKI int  null;
alter table DS_PAA_ODBIORCA add SPOSOB_DOSTARCZENIA int null;
alter table DS_PAA_ODBIORCA add KWOTA varchar(50) NULL;
