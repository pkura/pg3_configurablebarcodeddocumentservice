--rozszerzenie paczki pocztowe poczta 
alter table DS_PAA_OUT_DOC add paczka smallint;

alter table DS_STAWKI_OPLAT_POCZTOWYCH add paczka smallint;


insert into DS_OUT_DELIVERY_GABARYT values (7,'KRAJ - Paczka - Gab. A',null,1,1,1);
  
insert into DS_OUT_DELIVERY_GABARYT values (8,'KRAJ - Paczka - Gab. B',null,1,1,2);
  

insert into DS_OUT_DELIVERY_GABARYT values (9,'�wiat - Paczka ',null,1,1,3);

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '7', 'do 1 kg', null ,'7',1,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '8', 'ponad 1 kg do 2 kg', null ,'7',1,2);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '9', 'ponad 2 kg do 5 kg', null ,'7',1,3);

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '10', 'ponad 5 kg do 10 kg', null ,'7',1,4);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '11', 'ponad 10 kg do 15 kg', null ,'7',1,5);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '12', 'ponad 15 kg do 20 kg', null ,'7',1,6);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '13', 'ponad 20 kg do 30 kg', null ,'7',1,7);

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '14', 'do 1 kg', null ,'8',1,1);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '15', 'ponad 1 kg do 2 kg', null ,'8',1,2);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '16', 'ponad 2 kg do 5 kg', null ,'8',1,3);

insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '17', 'ponad 5 kg do 10 kg', null ,'8',1,4);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '18', 'ponad 10 kg do 15 kg', null ,'8',1,5);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '19', 'ponad 15 kg do 20 kg', null ,'8',1,6);
insert into DS_OUT_DELIVERY_COUNTRY_WEIGHT values ( '20', 'ponad 20 kg do 30 kg', null ,'8',1,7);


insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '25', 'do 1 kg', null ,'7',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '26', 'ponad 1 kg do 2 kg', null ,'7',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '27', 'ponad 2 kg do 5 kg', null ,'7',1,3);

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '28', 'ponad 5 kg do 10 kg', null ,'7',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '29', 'ponad 10 kg do 15 kg', null ,'7',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '30', 'ponad 15 kg do 20 kg', null ,'7',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '31', 'ponad 20 kg do 30 kg', null ,'7',1,7);

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '32', 'do 1 kg', null ,'8',1,1);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '33', 'ponad 1 kg do 2 kg', null ,'8',1,2);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '34', 'ponad 2 kg do 5 kg', null ,'8',1,3);

insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '35', 'ponad 5 kg do 10 kg', null ,'8',1,4);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '36', 'ponad 10 kg do 15 kg', null ,'8',1,5);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '37', 'ponad 15 kg do 20 kg', null ,'8',1,6);
insert into DS_OUT_DELIVERY_WORLD_WEIGHT values ( '38', 'ponad 20 kg do 30 kg', null ,'8',1,7);



--oplaty 


-- kraj  A
-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D, 7 - kraj Paczka gab A , 8- kraj Paczka gab B  
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 
--3 , 5 ,6 




--SET IDENTITY_INSERT DS_STAWKI_OPLAT_POCZTOWYCH  ON 
-- zwykly economy A kraj - PACZKA
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 7, 1, 1, 9.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 7, 1, 2, 11.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 7, 1, 3, 13.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 7, 1, 4, 18.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 7, 1, 5, 21.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 7, 1, 6, 30.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 7, 1, 7, 36.00,1);

--
-- zwykly economy A polecony kraj - PACZKA - to samo co zwykly economy A  bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 7, 2, 1, 9.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 7, 2, 2, 11.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 7, 2, 3, 13.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 7, 2, 4, 18.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 7, 2, 5, 21.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 7, 2, 6, 30.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 7, 2, 7, 36.00,1);

--
-- zwykly economy A polecony zpo kraj - PACZKA - to samo co zwykly economy A  bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 7, 3, 1, 9.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 7, 3, 2, 11.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 7, 3, 3, 13.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 7, 3, 4, 18.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 7, 3, 5, 21.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 7, 3, 6, 30.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 7, 3, 7, 36.00,1);


--  PRIIORYTETOWY zwykly A kraj - PACZKA - 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 7, 1, 1, 11.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 7, 1, 2, 13.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 7, 1, 3, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 7, 1, 4, 20.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 7, 1, 5, 25.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 7, 1, 6, 35.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 7, 1, 7, 43.00,1);


--  PRIIORYTETOWY polecony A kraj - PACZKA -  to samo co  PRIIORYTETOWY zwykly A kraj   bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 7, 2, 1, 11.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 7, 2, 2, 13.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 7, 2, 3, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 7, 2, 4, 20.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 7, 2, 5, 25.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 7, 2, 6, 35.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 7, 2, 7, 43.00,1);



--  PRIIORYTETOWY  polecony zpo kraj A kraj - PACZKA -  to samo co  PRIIORYTETOWY zwykly A kraj   bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 7, 3, 1, 11.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 7, 3, 2, 13.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 7, 3, 3, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 7, 3, 4, 20.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 7, 3, 5, 25.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 7, 3, 6, 35.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 7, 3, 7, 43.00,1);




-- gabaryt B 


-- kraj  B
-- waga 
--poziom przesylki 1- ekonomy 2- priorytetowy 3. zagranica economy  4 , zagranica priorytet
-- poziomGabarytu  1 - kraj A ,2 - kraj B  , 3 - swiat A , 4 swiat  B , 5 - swiat C , 6- swiat D, 7 - kraj Paczka gab A , 8- kraj Paczka gab B  
-- poziomlistu  zwykly 1, polecony 2 ,polecony zpo 3
--poziomWagi  1 - do 50g 2- do 100g  3 - do 350g  4 - do 500g ,5 -do 1000g, 6- do 2000g
-- kwota 

-- zwykly economy B kraj - PACZKA
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 8, 1, 1, 12.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 8, 1, 2, 14.50 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 8, 1, 3, 17.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 8, 1, 4, 22.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 8, 1, 5, 27.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 8, 1, 6, 40.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 8, 1, 7, 46.00,1);
--
-- zwykly economy B polecony kraj - PACZKA - to samo co zwykly economy B  bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 8, 2, 1, 12.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 8, 2, 2, 14.50 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 8, 2, 3, 17.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 8, 2, 4, 22.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 8, 2, 5, 27.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 8, 2, 6, 40.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 8, 2, 7, 46.00,1);


--
-- zwykly economy B polecony zpo kraj - PACZKA - to samo co zwykly economy B bo nei okreslono

insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  1, 8, 3, 1, 12.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  1, 8, 3, 2, 14.50 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  1, 8, 3, 3, 17.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    1, 8, 3, 4, 22.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   1, 8, 3, 5, 27.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   1, 8, 3, 6, 40.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   1, 8, 3, 7, 46.00,1);


--  PRIIORYTETOWY zwykly B kraj - PACZKA 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 8, 1, 1, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 8, 1, 2, 17.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 8, 1, 3, 19.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 8, 1, 4, 26.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 8, 1, 5, 32.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 8, 1, 6, 48.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 8, 1, 7, 53.00,1);



--  PRIIORYTETOWY polecony B kraj - PACZKA -  to samo co  PRIIORYTETOWY zwykly B kraj   bo nei okreslono
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 8, 2, 1, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 8, 2, 2, 17.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 8, 2, 3, 19.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 8, 2, 4, 26.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 8, 2, 5, 32.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 8, 2, 6, 48.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 8, 2, 7, 53.00,1);

--  PRIIORYTETOWY  polecony zpo kraj B kraj - PACZKA -  to samo co  PRIIORYTETOWY zwykly B kraj   bo nei okreslono

insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'do 1 kg',   			  2, 8, 3, 1, 14.50,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 1 kg do 2 kg', 	  2, 8, 3, 2, 17.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 2 kg do 5 kg', 	  2, 8, 3, 3, 19.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 5 kg do 10 kg',    2, 8, 3, 4, 26.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 10 kg do 15 kg',   2, 8, 3, 5, 32.00 ,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 15 kg do 20 kg',   2, 8, 3, 6, 48.00,1);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'ponad 20 kg do 30 kg',   2, 8, 3, 7, 53.00,1);





  