--init podstawowy--
country.sql

--przychodzace--
Nazwa kodowa			paadocin
Nazwa dla użytkownika	Pismo przychodzące
Tabela w bazie			DS_PAA_IN_DOC
Plik XML				etc\dockind\PAA\normal.xml
+sql normal.sql


--wychodzace--
Nazwa kodowa			paadocout
Nazwa dla użytkownika	Pismo wychodzące
Tabela w bazie			DS_PAA_OUT_DOC
Plik XML				etc\dockind\PAA\normal_out.xml
+sql normal_out.sql


--wewnetrzne--
Nazwa kodowa			paadocint
Nazwa dla użytkownika	Pismo wewnętrzne
Tabela w bazie			DS_PAA_INT_DOC
Plik XML				etc\dockind\PAA\normal_int.xml
+sql normal_int.sql


--wniosek urlopowy--
Nazwa kodowa			paa_wniosek_urlopowy
Nazwa dla użytkownika	Wniosek urlopowy
Tabela w bazie			DS_PAA_WNIOSEK_URLOPOWY
Plik XML				etc\dockind\PAA\paa_wniosek_urlopowy.xml
+sql normal_int.sql		paa_wniosek_urlopowy.sql


--plan pracy (main)--
Nazwa kodowa			paa_planpracy
Nazwa dla użytkownika	Plan pracy
Tabela w bazie			DS_PAA_PLAN_PRACY
Plik XML				etc\dockind\PAA\plan_pracy.xml
+sql normal_int.sql		plan_pracy.sql

--plan pracy (zadanie glowne/koordynatora)--
Nazwa kodowa			paa_planpracy_zad
Nazwa dla użytkownika	Plan pracy zadanie
Tabela w bazie			DS_PAA_PLAN_PRACY_ZAD
Plik XML				etc\dockind\PAA\plan_pracy_zadanie.xml
+sql normal_int.sql		plan_pracy.sql

--plan pracy (podzadanie pracownika)--
Nazwa kodowa			paa_planpracy_podzad
Nazwa dla użytkownika	Plan pracy podzadanie
Tabela w bazie			DS_PAA_PLAN_PRACY_PODZAD
Plik XML				etc\dockind\PAA\plan_pracy_podzadanie.xml
+sql normal_int.sql		plan_pracy.sql

