CREATE TABLE [dbo].[DS_PAA_ADRESAT](
	[ID] [varchar](50) NOT NULL,
	[FIRSTNAME] [varchar](50) NULL,
	[LASTNAME] [varchar](50) NULL,
	[DIVISION] [varchar](249) NULL
) ON [PRIMARY]

INSERT DS_PAA_ADRESAT
SELECT     U.ID, U.FIRSTNAME, U.LASTNAME, D.NAME AS division
FROM         dbo.DS_USER AS U INNER JOIN
                      dbo.DS_USER_TO_DIVISION AS U_D ON U_D.USER_ID = U.ID INNER JOIN
                      dbo.DS_DIVISION AS D ON D.ID = U_D.DIVISION_ID
WHERE     (D.DIVISIONTYPE = 'division') OR
                      (D.DIVISIONTYPE = 'position')