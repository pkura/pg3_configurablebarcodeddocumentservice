alter table[DS_PAA_IN_DOC] add dataPisma [date] null;



CREATE TABLE [dbo].[DS_PAA_DICTIONARY_SELECT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[available] [int] NULL,
	[DICTIONATY_ID] [numeric](18, 0) NULL,
	[DICTIONATY_TYPE] [varchar](64)NULL,
	[DICTIONATY_GUID] [varchar](126) NULL,
	
) ON [PRIMARY]


insert into DS_PAA_DICTIONARY_SELECT values ( '1', 'P-PAA', null		,1,1,DICID,dict_type, dict guid);
insert into DS_PAA_DICTIONARY_SELECT values ( '2', 'PAA/VP-GIDJ', null	,1,2);
insert into DS_PAA_DICTIONARY_SELECT values ( '3', 'PAA/GP', null		,1,3);
insert into DS_PAA_DICTIONARY_SELECT values ( '4', 'PAA/GP/WM', null 	,1,4);
insert into DS_PAA_DICTIONARY_SELECT values ( '5', 'PAA/GP/WP', null 	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '6', 'PAA/DG', null 		,1,6);
insert into DS_PAA_DICTIONARY_SELECT values ( '7', 'PAA/BDG', null 		,1,7);
insert into DS_PAA_DICTIONARY_SELECT values ( '8', 'PAA/AW', null 		,1,8); usunac 
insert into DS_PAA_DICTIONARY_SELECT values ( '9', 'PAA/CEZAR', null 	,1,9);
insert into DS_PAA_DICTIONARY_SELECT values ( '10', 'PAA/CEZAR/WMP', null ,1,10);
insert into DS_PAA_DICTIONARY_SELECT values ( '11', 'PAA/CEZAR/WZK', null ,1,11);
insert into DS_PAA_DICTIONARY_SELECT values ( '12', 'PAA/CEZAR/BOR', null ,1,12);
insert into DS_PAA_DICTIONARY_SELECT values ( '13', 'PAA/DBJ', null 	,1,13);
insert into DS_PAA_DICTIONARY_SELECT values ( '14', 'PAA/DBJ/WA', null	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '15', 'PAA/DBJ/WK', null	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '16', 'PAA/DBJ/WN', null 	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '17', 'PAA/DBJ/WO', null	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '18', 'PAA/DBJ/WT', null 	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '19', 'PAA/DEB', null		,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '20', 'PAA/DOR', null 	,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '21', 'PAA/DOR/CRD', null ,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '22', 'PAA/DP', null 		,1,5);
insert into DS_PAA_DICTIONARY_SELECT values ( '23', 'PAA/KT', null 		,1,5);



