ALTER TABLE [DS_PAA_OUT_DOC] ADD  [statusdokumentu] [int] NULL;
ALTER TABLE [DS_PAA_IN_DOC] ADD  [statusdokumentu] [int] NULL;
ALTER TABLE [DS_PAA_WNIOSEK_URLOPOWY] ADD  [statusdokumentu] [int] NULL;
alter table [PAA].[dbo].[DSO_PERSON] add PHONENUMBER varchar(10);
alter table [PAA].[dbo].[DSO_PERSON] add KOMNUMBER varchar(10);

drop VIEW [DS_PAA_WNIOSEK_USER];
GO
CREATE VIEW [DS_PAA_WNIOSEK_USER]

AS
SELECT     U.ID, U.FIRSTNAME + ' ' + U.LASTNAME + ' ' + D.NAME AS title,
            'D_'+U.NAME as cn,
        NULL as    centrum ,
        '' as refValue ,
        1 as available 
FROM         dbo.DS_USER AS U INNER JOIN
                      dbo.DS_USER_TO_DIVISION AS U_D ON U_D.USER_ID = U.ID INNER JOIN
                      dbo.DS_DIVISION AS D ON D.ID = U_D.DIVISION_ID WHERE     (D.DIVISIONTYPE = 'division') OR
                      (D.DIVISIONTYPE = 'position')