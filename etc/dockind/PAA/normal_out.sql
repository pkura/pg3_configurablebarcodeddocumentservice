-- wykaz jako dokument (dockind) PAA przychodzace dockind
CREATE TABLE [dbo].[DS_PAA_OUT_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_out] [int] NULL,
	[znak_pisma] [varchar](50) NULL,
	[signature] [varchar](150) NULL,
	[SENDER] [numeric](18, 0) NULL,
	[ZPO] [tinyint] NULL
) ON [PRIMARY]

CREATE TABLE [DS_PAA_RODZAJ_OUT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	)
	 ON [PRIMARY]

	  INSERT INTO DS_PAA_RODZAJ_OUT
           (cn,
           title
           ,centrum
           ,refValue
           ,available)
     VALUES
           (1,'Faktury',0,null,1),
           (2,'Umowy', 0,null,1),
           (3,'Skargi',0,null,1),
           (4,'Wnioski',0,null,1),
           (5,'Pisma inne',0,null,1);