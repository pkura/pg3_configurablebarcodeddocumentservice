
create table DSC_role (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);

create table ds_contractor_to_role (
	contractor_id		numeric(19,0) not null,
	role_id		numeric(19,0) not null
);

alter table ds_contractor_to_role add constraint ds_contractor_to_role_fk1
foreign key (contractor_id) references DSC_Contractor (id);

CREATE SEQUENCE   "DSC_ROLE_ID"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

insert into dsc_role(id,name,cn)values(dsc_role_id.nextval,'Odbiorca ','ODB');
insert into dsc_role(id,name,cn)values(dsc_role_id.nextval,'Dostawca ','DOS');
insert into dsc_role(id,name,cn)values(dsc_role_id.nextval,'Dealer ','DEA');
insert into dsc_role(id,name,cn)values(dsc_role_id.nextval,'Po�rednik ','POS');
insert into dsc_role(id,name,cn)values(dsc_role_id.nextval,'Nowy Kontakt ','NEW');

COMMIT;