alter table DSC_Contractor add region integer;
alter table DSC_VINDICATION add region varchar2(20 char);

create table DSC_region (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)

);
CREATE SEQUENCE   "DSC_REGION_ID"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 11270 NOCACHE  NOORDER  NOCYCLE;

insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'�wi�tokrzyski','SWI');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Wielkopolski','WIE');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'�l�ski','SLA');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Lubelski','LAB');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Vendorzy','VEN');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Podlaski','PODL');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Podkarpacki','PODK');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'��dzki','LOD');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Ma�opolski','MAL');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Centralny','CEN');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Kujawsko-Pomorski','KUPOM');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Pomorski','POM');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Wsparcie sprzeda�y','WS');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Zachodniopomorski','ZPOM');
insert into dsc_region(id,name,cn)values(dsc_region_id.nextval,'Dolno�l�ski','DOL');

COMMIT;