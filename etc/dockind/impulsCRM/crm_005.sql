alter table DSC_Contractor add dsUser varchar2(50 char);

alter table DSC_Contact add dataNastepnegoKontaktu timestamp;
alter table DSC_Contact add celKontaktu varchar2(600 char);

delete from DSO_LABEL;

drop SEQUENCE DSO_LABEL_ID; 

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(1, 'Przeterminowane', 0, 0, 0, 1, 0, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, DESCRIPTION, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(2, 'Dzisiaj', 'do realizacji dzisiaj', 0, 24, 0, 1, 1, 0, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, DESCRIPTION, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(3, 'Za jeden dzie�', 'do realizacji Jutro', 0, 24, 1, 2, 1, 1, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(4, 'Za dwa dni', 0, 24, 1, 3, 1, 1, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(5, 'Za trzy dni', 0, 24, 1, 4, 1, 1, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(6, 'Za cztery dni', 0, 24, 1, 5, 1, 1, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, INACTION_TIME, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(7, 'Za pi�� dni', 0, 24, 1, 6, 1, 1, 'label', '#system#', '#system#', '#none#');

insert into DSO_LABEL
   (ID, NAME, PARENT_ID, HIDDEN, ANCESTOR, MODIFIABLE, DELETES_AFTER, TYPE, READ_RIGHTS, WRITE_RIGHTS, MODIFY_RIGHTS)
values(8, 'Odleg�y termin', 0, 1, 0, 1, 0, 'label', '#system#', '#system#', '#none#');


CREATE SEQUENCE   DSO_LABEL_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 20 CACHE 20 NOORDER  NOCYCLE;

alter table DSC_Contractor add obrot float;

COMMIT;