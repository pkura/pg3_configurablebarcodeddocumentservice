create table ds_contractor_to_marka (
	contractor_id		numeric(19,0) not null,
	marka_id			numeric(19,0) not null
);

alter table ds_contractor_to_marka add constraint ds_contractor_to_marka_fk1
foreign key (contractor_id) references DSC_Contractor (id);