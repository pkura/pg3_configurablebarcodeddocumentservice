alter table DSC_Contact add status integer;

create table DSC_status_kind (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(50 char)
);

CREATE SEQUENCE   DSC_status_kind_ID  MINVALUE 150 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 150 NOCACHE  NOORDER  NOCYCLE;

insert into DSC_status_kind(id,cn,name)values(10,'PROBA_NAWIAZANIA_KONTAKTU','Proba nawiazania kontaktu');
insert into DSC_status_kind(id,cn,name)values(30,'PREZENTACJA_ILPOL','Prezentacja ILPOL dostarczona Klientowi');
insert into DSC_status_kind(id,cn,name)values(35,'SPOTKANIE_UMOWIONE','Spotkanie z firm� um�wione');
insert into DSC_status_kind(id,cn,name)values(40,'INFORMACJE_O_PROP','Informacje o prop. Wspolp. zebrane');
insert into DSC_status_kind(id,cn,name)values(50,'OFERTA_WYSLANA','Oferta wyslana');
insert into DSC_status_kind(id,cn,name)values(60,'NEGOCJOWANIE_OFERTY','Negocjowanie oferty');
insert into DSC_status_kind(id,cn,name)values(70,'OFERTA_ODRZUCONA','Oferta odrzucona');
insert into DSC_status_kind(id,cn,name)values(90,'UMOWA_ZAWARTA','Umowa zawarta');
insert into DSC_status_kind(id,cn,name)values(100,'UMOWAODRZUCONA','Umowa odrzucona');
insert into DSC_status_kind(id,cn,name)values(110,'BEZ_WSPOLPRACY','BEZ mozliwo�ci wspolpracy');
insert into DSC_status_kind(id,cn,name)values(115,'FIRMA_NIE_INSTNIEJE','Firma nie istnieje');
insert into DSC_status_kind(id,cn,name)values(120,'INNY','Inny');

update dsc_contact c2 set status = (select max(t.status_poczatkowy) from dsc_marketing_task t, dsg_leasing_multiple_value m 
where m.document_id = t.document_id and m.field_cn = 'KONTAKT' and m.field_val = c2.id);

delete from ds_contractor_to_role where role_id not in (5,7,8);
delete from dsc_role where id not in (5,7,8);

alter table DSC_marketing_TASK add kategoria integer;
alter table DSC_marketing_TASK add grupa integer;

CREATE TABLE dsg_grupa
(
	id numeric(18,0) NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum int NULL,
	refValue varchar2(20 char) NULL,
	available int default 1 
);

CREATE TABLE dsg_kategoria
(
	id numeric(18,0) NOT NULL,
	cn varchar2(50 char) NULL,
	title varchar2(255 char) NULL,
	centrum int NULL,
	refValue varchar2(20 char) NULL,
	available int default 1 
);

CREATE TABLE DL_OBJECT_TYPE
(
	id numeric(18,0) NOT NULL,
	name varchar2(50 char) NULL,
	leoId numeric(18,0) NOT NULL
);

alter table dl_application_dictionary add leasingObjectType integer;
alter table dl_contract_dictionary add leasingObjectType integer;

create sequence DL_OBJECT_TYPE_ID;
create sequence dsg_grupa_ID;
create sequence dsg_kategoria_ID;