create table DSG_COUNTY
(
	id					numeric(19,0) not null,
    name                varchar2(60 char),
	cn					varchar2(20 char),
	title 				varchar2(255 char) NULL,
	centrum 			numeric(19,0) NULL,
	refValue 			varchar2(20 char) NULL,
	available 			numeric(19,0)
);

insert into dsg_county (id,name,cn) values(10, 'dolno?l�skie','DS');
insert into dsg_county (id,name,cn) values(15, 'kujawsko-pomorskie','KP');
insert into dsg_county (id,name,cn) values(20, 'lubuskie','LUB');
insert into dsg_county (id,name,cn) values(25, '��dzkie','LOD');
insert into dsg_county (id,name,cn) values(30, 'lubelskie','LU');
insert into dsg_county (id,name,cn) values(35, 'mazowieckie','MAZ');
insert into dsg_county (id,name,cn) values(40, 'ma�opolskie','MAL');
insert into dsg_county (id,name,cn) values(45, 'opolskie','OPO');
insert into dsg_county (id,name,cn) values(50, 'podlaskie','PODL');
insert into dsg_county (id,name,cn) values(60, 'podkarpackie','PODK');
insert into dsg_county (id,name,cn) values(65, 'pomorskie','POM');
insert into dsg_county (id,name,cn) values(70, '?l�skie','SLA');
insert into dsg_county (id,name,cn) values(80, '?wi�tokrzyskie','SWI');
insert into dsg_county (id,name,cn) values(85, 'warmi�sko-mazurskie','WAMA');
insert into dsg_county (id,name,cn) values(90, 'wielkopolskie','WIE');
insert into dsg_county (id,name,cn) values(95, 'zachodniopomorskie','ZPOM');

alter table dsc_marketing_TASK add county integer;

COMMIT;

