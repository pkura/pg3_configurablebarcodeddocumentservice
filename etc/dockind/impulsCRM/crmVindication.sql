create table DSC_VINDICATION(
    document_id         	integer not null,
    contractor_number   	integer,
    worker_number       	integer,
	numer_wezwania			varchar2(50 char),
	data_wezwania			timestamp,
	numer_faktury			varchar2(50 char),
	naleznosc_pierwotna		float,
	naleznosc				float,
	wynik_kontaktu			integer,
	data_kolejnego_kontaktu	timestamp,
	data_kolejnej_akcji		timestamp,
	opis					varchar2(700 char),
	windykacja				number(1)
);
CREATE SEQUENCE DSC_VINDICATION_TASK_ID start with 1 nocache;