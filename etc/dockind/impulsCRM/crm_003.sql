create table DSC_Contact(
	id					integer not null,
	dataKontaktu		timestamp,
	osobaKontaktowa		integer,
	rodzajkontaktu		integer
);
CREATE SEQUENCE   DSC_contact_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;


create table DSC_contact_kind (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);
CREATE SEQUENCE   DSC_contact_kind_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table DSC_leaflet_kind (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);
CREATE SEQUENCE   DSC_leaflet_kind_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table ds_leaflet_to_contact (
	contact_id		numeric(19,0) not null, 
	leaflet_id		numeric(19,0) not null
);

alter table ds_leaflet_to_contact add constraint ds_leaflet_to_contact_fk1
foreign key (contact_id) references DSC_Contact (id);



insert into DSC_contact_kind(id,name,cn)values(DSC_contact_kind_ID.nextval,'Wizyta ','WIZ');
insert into DSC_contact_kind(id,name,cn)values(DSC_contact_kind_id.nextval,'Telefon ','TEL');
insert into DSC_contact_kind(id,name,cn)values(DSC_contact_kind_id.nextval,'Faks ','FAX');
insert into DSC_contact_kind(id,name,cn)values(DSC_contact_kind_id.nextval,'Mail ','MAL');

insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Kubki','KUB');
insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'No�yki','NOZ');
insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Smycze','SMY');
insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Parasole','PAR');
insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Zestawy do golfa','ZDG');
insert into DSC_leaflet_kind(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Kalendarze','KAL');
COMMIT;