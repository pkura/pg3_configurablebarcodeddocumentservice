create table DSC_marketing_TASK (
    document_id         integer not null,
    status_poczatkowy	integer,
    status_koncowy      integer,
    contractor_number   integer,    
    opis                varchar2(600 char),
	uzytkownik          varchar2(20 char),
    data_kontaktu       timestamp
);

CREATE SEQUENCE DSC_marketing_TASK_ID start with 1 nocache;