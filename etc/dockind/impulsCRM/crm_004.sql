create table DSC_Competition(
	id 					integer not null,
	competitionName		integer,
	competitionKind		integer,
	oficjalna			number(1),
	marzaOd				float,
	marzaDo				float,
	prowizjaSalon		float,
	prowizjaHandlowiec  float,
	nagrodaHandlowiec	float,
	wysokoscSprzedazy	float,
	opis				varchar2(600 char),
	okres				integer,
	contractorId		integer,
	dataDodania			timestamp,
	posn				integer
);
CREATE SEQUENCE   DSC_Competition_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table DSC_CooperationTerms(
	id 					integer not null,
	marzaOd				float,
	marzaDo				float,
	prowizjaSalonOd		float,
	prowizjaSalonDo		float,
	prowizjaHandlowiec	float,
	nagrodaHandlowiec	float,
	opis				varchar2(600 char),
	contractorId		integer,
	dataDodania			timestamp,
	posn				integer
);
CREATE SEQUENCE   DSC_CooperationTerms_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table DSC_Competition_Kind (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);
CREATE SEQUENCE   DSC_Competition_Kind_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table DSC_Competition_Name (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);

CREATE SEQUENCE   DSC_Competition_Name_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

create table DSC_marka (
    id                  integer not null,
    name                varchar2(60 char),
	cn					varchar2(20 char)
);

CREATE SEQUENCE   DSC_marka_ID  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE;

insert into DSC_Competition_Kind(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Leasing','WIZ');
insert into DSC_Competition_Kind(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Kredyt','TEL');
insert into DSC_Competition_Kind(id,name,cn)values(DSC_contact_kind_id.nextval,'Inny','FAX');

insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'VB ','VB');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'EFL','EFL');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Raiffeisen ','RAF');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'ING','ING');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'BRE','BRE');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Bankowy','BAN');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Pekao','PEK');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'GE Money ','GEM');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'GMAC','GMAC');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Santander Cosumer Bank, ','SCB');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Lukas Bank, ','LB');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Volkswagen Bank,','VB');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Fiat Bank, ','FB');
insert into DSC_Competition_Name(id,name,cn)values(DSC_Competition_Kind_ID.nextval,'Toyota Bank','TB');


alter table dsc_contractor add marka integer;

insert into DSC_marka(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Marka 1','WIZ');
insert into DSC_marka(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Marka 2','TEL');
insert into DSC_marka(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Marka 3','FAX');
insert into DSC_marka(id,name,cn)values(DSC_leaflet_kind_id.nextval,'Marka 4','MAL');

COMMIT;