--KO, data wprowadzenia, skr�cony opis, pud�o, nadawca, odbiorca
Create view plg_report as
SELECT DOCUMENT_ID 'id', j.SEQUENCEID 'KO', plg.typ 'typ_id', d.CTIME 'ctime',
plg.KROTKIOPIS 'opis', box.NAME 'pudlo', nad.NAZWA 'nadawca', od.NAZWA 'odbiorca', usr.LASTNAME + ' ' + usr.FIRSTNAME 'odbiorca_uzytkownik' 
FROM DSG_PLG plg
left join DSO_JOURNAL_ENTRY j ON j.DOCUMENTID = plg.DOCUMENT_ID
left join DS_DOCUMENT d ON d.ID = plg.DOCUMENT_ID
left join DS_BOX box ON box.ID = d.BOX_ID
left join DSG_PLG_ODBIORCA od ON od.id = plg.ODBIORCA
LEFT join DS_USER usr ON usr.ID = plg.ODBIORCA_UZYT
left join DSG_PLG_NADAWCA nad ON nad.ID = plg.NADAWCA 
where d.dockind_id = 4

Create view plg_out_report as
SELECT DOCUMENT_ID 'id', j.SEQUENCEID 'KO', plg.typ 'typ_id', d.CTIME 'ctime',
plg.KROTKIOPIS 'opis', box.NAME 'pudlo', plg.TYP_PRZESYLKI 'typ_przesylki',
nad.NAZWA 'nadawca', od.NAZWA 'odbiorca'
FROM DSG_PLG_OUT plg
left join DSO_JOURNAL_ENTRY j ON j.DOCUMENTID = plg.DOCUMENT_ID
left join DS_DOCUMENT d ON d.ID = plg.DOCUMENT_ID
left join DS_BOX box ON box.ID = d.BOX_ID
left join DSG_PLG_ODBIORCA od ON od.id = plg.ODBIORCA
left join DSG_PLG_NADAWCA nad ON nad.ID = plg.NADAWCA
where d.dockind_id = 5