insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_DELETE','Dokumenty zwykły - usuwanie','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_ATT_MODIFY','Dokumenty zwykły zalacznik - modyfikacja','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ','Dokumenty zwykły- odczyt','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_MODIFY','Dokumenty zwykły - modyfikacja','brak','group',         1,1);
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('DOCUMENT_READ_ATT_READ','Dokumenty zwykły zalacznik - odczyt','brak','group',         1,1);
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(7 AS Numeric(19, 0)), N'PLG_READ_DELETE', N'Korespondencja przychodząca - usuwanie', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(8 AS Numeric(19, 0)), N'PLG_READ_ATT_READ', N'Korespondencja przychodząca zalacznik - odczyt', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(9 AS Numeric(19, 0)), N'PLG_READ', N'Korespondencja przychodząca - odczyt', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(10 AS Numeric(19, 0)), N'PLG_READ_MODIFY', N'Korespondencja przychodząca - modyfikacja', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(11 AS Numeric(19, 0)), N'PLG_READ_ATT_MODIFY', N'Korespondencja przychodząca zalacznik - modyfikacja', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(12 AS Numeric(19, 0)), N'PLG_OUT_READ_ATT_READ', N'Korespondencja wychodząca zalacznik - odczyt', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(13 AS Numeric(19, 0)), N'PLG_OUT_READ', N'Korespondencja wychodząca - odczyt', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(14 AS Numeric(19, 0)), N'PLG_OUT_READ_MODIFY', N'Korespondencja wychodząca - modyfikacja', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(15 AS Numeric(19, 0)), N'PLG_OUT_READ_DELETE', N'Korespondencja wychodząca - usuwanie', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)
INSERT [DS_DIVISION] ([ID], [GUID], [NAME], [CODE], [DIVISIONTYPE], [PARENT_ID], [description], [HIDDEN], [EXTERNALID]) VALUES (CAST(16 AS Numeric(19, 0)), N'PLG_OUT_READ_ATT_MODIFY', N'Korespondencja wychodząca zalacznik - modyfikacja', NULL, N'group', CAST(1 AS Numeric(19, 0)), NULL, 1, NULL)

create table DSG_PLG(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP int,
	RODZAJ int,
	DATA datetime,
	NADAWCA int,
	ODBIORCA int,
	UZYTKOWNIK int,
	ZOSTAJE tinyint,
	KROTKIOPIS varchar(256),
	GENERUJ tinyint,
	AKCEPTACJA_FINALNA tinyint,
	TYP_PRZESYLKI int,
	FORMA_GRZECZNOSCI int,
	PUDLO_ID int,
	DESCRIPTION varchar(2000),
	STATUS int
)

create table DSG_PLG_NADAWCA(
	[ID] [int] IDENTITY(1,1) NOT NULL primary key,
	[NAZWA] [varchar](100) NULL,
	[POCZTA] [varchar](20) NULL,
	[MIASTO] [varchar](50) NULL,
	[ULICA] [varchar](50) NULL, 
)

create table DSG_PLG_ODBIORCA(
	[ID] [int] IDENTITY(1,1) NOT NULL primary key,
	[NAZWA] [varchar](100) NULL,
	[POCZTA] [varchar](20) NULL,
	[MIASTO] [varchar](50) NULL,
	[ULICA] [varchar](50) NULL, 
)

CREATE VIEW DSG_PLG_V_USERDICT
as
select id, name, firstname, lastname, email from ds_user;

/** tabela zawiera formy grzeczności */
CREATE TABLE DSG_PLG_FORMA_GRZECZ(
	id numeric(5,0) IDENTITY(1,1) NOT NULL primary key,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int NULL
);

create table DSG_PLG_PUDLO(
	[ID] [int] IDENTITY(1,1) NOT NULL primary key,
	[NUMER] [varchar](100) NULL
)

--UPDATE 001
alter table DSG_PLG add STATUS int;

--UPDATE 002 
insert into DS_DIVISION(GUID,NAME,CODE,DIVISIONTYPE,PARENT_ID,HIDDEN)values('ZARZAD','Zarząd',null,'division',1,0);
CREATE TABLE DSG_PLG_MULTI(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	FIELD_CN varchar(50) NOT NULL,
	FIELD_VAL varchar(50) NOT NULL,
	ID numeric(5,0) IDENTITY(1,1) NOT NULL
) ON PRIMARY
GO

--update 003
alter table DSG_PLG_NADAWCA add external_id int;

SELECT id,nazwa,miejscowosc 'miasto',
CASE WHEN numerMieszk is not null AND DATALENGTH(numerMieszk) > 0 THEN
	ulica + ' ' + numerDomu + '/' + numerMieszk
	ELSE ulica + ' ' + numerDomu
END 'ulica'
,kod 

FROM SSCommon.fk_kontrahenci_tmp

--update 004

alter table DSG_PLG add DESCRIPTION varchar(2000);

-- update 005

create table DSG_PLG_OUT(
	DOCUMENT_ID numeric(19, 0) NOT NULL,
	TYP int,
	RODZAJ int,
	DATA datetime,
	NADAWCA int,
	ODBIORCA int,
	UZYTKOWNIK int,
	ZOSTAJE tinyint,
	KROTKIOPIS varchar(256),
	GENERUJ tinyint,
	AKCEPTACJA_FINALNA tinyint,
	TYP_PRZESYLKI int,
	FORMA_GRZECZNOSCI int,
	PUDLO_ID int,
	DESCRIPTION varchar(2000),
	STATUS int
)

--update 006
alter table DSG_PLG add ODBIORCA_UZYT int;


