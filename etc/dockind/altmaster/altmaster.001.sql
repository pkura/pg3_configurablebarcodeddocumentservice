create table dsg_ALTMASTER
(
	document_id numeric(18,0),
	rodzaj	integer,
	typ		integer,
	podtyp	integer,
	tytul	varchar(50),
	nr_dokumentu	varchar(50),
	status	integer,
	numer	varchar(50),
	numer_wersji integer,
	indeks	integer,
	nazwa_produktu	integer,
	os_nadzorujaca numeric(18,0),
	kontrahent	numeric(18,0),
	pracownik	varchar(50),
	wydzial		varchar(50),
	opis		varchar(600),
	NUMER_DOKUMENTU_ZWIAZANEGO varchar(50)
);


CREATE TABLE DSG_MULTIPLE_VALUE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(20 ) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

CREATE TABLE [DF_DICINVOICE](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[numerKontrahenta] [varchar](255) NOT NULL,
	[numerKontaBankowego] [varchar](255) NULL,
	[imie] [varchar](255) NULL,
	[nazwisko] [varchar](255) NULL,
	[name] [varchar](255) NULL,
	[oldname] [varchar](255) NULL,
	[nip] [varchar](30) NULL,
	[regon] [varchar](30) NULL,
	[ulica] [varchar](255) NULL,
	[kod] [varchar](10) NULL,
	[miejscowosc] [varchar](255) NULL,
	[email] [varchar](255) NULL,
	[faks] [varchar](255) NULL,
	[telefon] [varchar](255) NULL,
	[kraj] [varchar](10) NULL
);

CREATE TABLE dsg_rodzaj
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
CREATE TABLE dsg_typ
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
CREATE TABLE dsg_podtyp
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
CREATE TABLE dsg_obiekt
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
CREATE TABLE dsg_indeks
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);
CREATE TABLE dsg_nazwa
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);