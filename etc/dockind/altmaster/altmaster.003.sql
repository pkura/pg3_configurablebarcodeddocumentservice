CREATE TABLE dsg_wydzial
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

CREATE TABLE dsg_pracownik
(
	id int IDENTITY(1,1) NOT NULL,
	cn varchar(50) NULL,
	title varchar(255) NULL,
	centrum int NULL,
	refValue varchar(20) NULL,
	available int default 1 
);

alter table dsg_altmaster add lokalizacja varchar(100);