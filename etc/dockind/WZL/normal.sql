CREATE TABLE [dsg_rodzaj_bial_IN](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer	
);

CREATE TABLE [dsg_rodzaj_bial_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_OUT integer;
alter table DSG_NORMAL_DOCKIND add doc_number varchar(150);
alter table DSG_NORMAL_DOCKIND add storage_place varchar(250);
