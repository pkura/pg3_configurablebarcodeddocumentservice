CREATE TABLE dsg_cbk_order
(
    DOCUMENT_ID numeric(18,0) NOT NULL,
    status numeric(18,0),
    nrwniosku varchar(20),
    nazwa_przedmiotu varchar(40),
    itemDescription varchar(4096),
    producent varchar (4096),
    uzasadnienie varchar(4096),
    remarks varchar (4096), 
    sug_contractor numeric(18,0),
    contractor numeric(18,0),
    realization date,
    last_realization date,
    gross_amount numeric(18,2),
    nrprzetargu varchar(100),
);

CREATE TABLE dsg_cbk_order_multiple_value (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);