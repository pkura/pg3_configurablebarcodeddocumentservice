CREATE TABLE dsg_cbk_invoiceorder
(
    DOCUMENT_ID numeric(18,0) NOT NULL,
    status numeric(18,0),
    duuenr varchar(50),
    sadNr varchar(50),
    invoice_kind numeric (18,0),
    invoice_date date,
    invoice_number varchar (50),
    payment_date date,
    waluta numeric (18,0),
    kwotaWwalucie numeric (18,2),
    kurs numeric (18,4),
    sposob numeric (18,0),
    net_amount numeric (18,2),
    vat numeric (18,2),
    gross_amount numeric (18,2),
    description varchar (4096),
    buy_kind numeric (18,0),
    addinfo varchar (4096),
    
);

CREATE TABLE dsg_cbk_invoiceorder_multiple_value (
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);