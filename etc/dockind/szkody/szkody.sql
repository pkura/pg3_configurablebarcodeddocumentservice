CREATE TABLE [dbo].[DSG_HARM](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[rodzaj_procedury] [int] NULL,
	[numer_zgloszenia] [varchar](50) NULL,
	[data_zgloszenia] [datetime] NULL,
	[data_kolizji] [datetime] NULL,
	[wartosc_pojazdu] [numeric](18, 2) NULL,
	[wartosc_szkod] [numeric](18, 2) NULL,
	[insurance_participant_id] [int] NULL,
	[POLICJA] [bit] NULL,
	[policja_info] [varchar](50) NULL,
	[postepowanie_karne] [int] NULL,
	[teren_zabudowany] [bit] NULL,
	[dopuszczalna_predkosc] [int] NULL,
	[miejscowosc] [varchar](50) NULL,
	[ulica] [varchar](50) NULL,
	[najblizsza_miejscowosc] [varchar](50) NULL,
	[odleglosc] [int] NULL,
	[trasa] [varchar](200) NULL,
	[rodzaj_nawierzchni] [int] NULL,
	[rodzaj_nawierzchni_jaka] [varchar](50) NULL,
	[stan_nawierzchni] [int] NULL,
	[przebieg_zdarzenia] [text] NULL,
	[status] [int] NULL,
	[wysokosc_odszkodowania] [numeric](18, 2) NULL,
	[decyzja] [int] NULL,
	[szkody_poza_pojazdem] [bit] NULL CONSTRAINT [DF_DSG_HARM_szkody_poza_pojazdem]  DEFAULT ((0)),
	[szkody_poza_pojazdem_opis] [text] NULL,
	[ranni] [bit] NULL CONSTRAINT [DF_DSG_HARM_ranni]  DEFAULT ((0)),
	[zabici] [bit] NULL CONSTRAINT [DF_DSG_HARM_zabici]  DEFAULT ((0)),
	[swiadkowie_zdarzenia] [text] NULL,
	[pojazd_miejsce] [text] NULL,
	[data_ogledzin] [datetime] NULL,
	[miejsce_ogledzin] [varchar](50) NULL,
	[wykonal] [int] NULL,
	[godzina_ogledzin] [varchar](50) NULL,
	[czas_ogledzin] [varchar](50) NULL,
);

CREATE TABLE [dbo].[dsg_insurance_participant](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](40) NOT NULL,
	[surname] [varchar](40) NOT NULL,
	[email] [varchar](50) NULL,
	[pesel] [varchar](50) NULL,
	[ac] [varchar](50) NULL,
	[oc] [varchar](50) NULL
);

INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (%root_id%, 'RZECZOZNAWCA', 'Rzeczoznawca', 'group',0);
INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (%root_id%, 'KSIEGOWA', 'Ksiegowa', 'group',0);
INSERT INTO DS_DIVISION (PARENT_ID, GUID, NAME, DIVISIONTYPE,HIDDEN) VALUES (%root_id%, 'LIKWIDATOR', 'Likwidator', 'group',0);
