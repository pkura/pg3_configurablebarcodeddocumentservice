CREATE TABLE dsg_pz_typy_dokumentow_raw(
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [int] NULL,
	[available] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

insert into dsg_pz_typy_dokumentow_raw (cn,title,available)
select ltrim(rtrim(cn)),ltrim(rtrim(title)),1 from dsg_pz_typy_dokumentow_porthol
union
select ltrim(rtrim(cn)),ltrim(rtrim(title)),1 from dsg_pz_typy_dokumentow_project
union
select ltrim(rtrim(cn)),ltrim(rtrim(title)),1 from dsg_pz_typy_dokumentow_project_gd
union
select ltrim(rtrim(cn)),ltrim(rtrim(title)),1 from dsg_pz_typy_dokumentow_wuz

alter view dsg_pz_typy_dokumentow as
select id,cn,title,available,centrum,1 as refValue
from dsg_pz_typy_dokumentow_raw
where cn in ('KZ1','KZ2','KZ3','KZ4','KZ5','KZ6','KZV2','KZV6','V','V2','VI1','VI2','VI3','VI4','VV','Z1','Z2','Z3','Z4','Z5','ZZBO','ZZBO_W')
union all
select id,cn,title,available,centrum,2 as refValue
from dsg_pz_typy_dokumentow_raw
where cn in ('KZ1','KZ2','KZ3','KZ4','KZ5','KZ6','KZV2','KZV6','V','V2','VI1','VI2','VI3','VI4','VV','Z1','Z2','Z4','Z5','ZZBO','ZZBO_W')
union all
select id,cn,title,available,centrum,3 as refValue
from dsg_pz_typy_dokumentow_raw
where cn in ('KZ1','KZ2','KZ3','KZ4','KZ5','KZ6','KZV2','KZV6','V','V2','VI1','VI2','VI3','VI4','VV','Z1','Z2','Z4','Z5','ZZBO','ZZBO_W')
union all
select id,cn,title,available,centrum,4 as refValue
from dsg_pz_typy_dokumentow_raw
where cn in ('KZ1','KZ2','KZ3','KZ4','KZ5','KZ6','KZV2','KZV6','V','V2','VI1','VI2','VI3','VI4','VV','Z1','Z2','Z3','Z4','Z5','ZZBO','ZZBO_W')
union all
select id,cn,title,available,centrum,5 as refValue
from dsg_pz_typy_dokumentow_raw
where cn in ('KZ1','KZ2','KZ3','KZ4','KZ5','KZ6','KZV2','KZV6','V','V2','VI1','VI2','VI3','VI4','VV','Z1','Z2','Z3','Z4','Z5','ZZBO','ZZBO_W')





