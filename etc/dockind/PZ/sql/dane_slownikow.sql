SET IDENTITY_INSERT [dsg_pz_komorka_raw] ON 

GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(19, 0)), N'520-1          ', N'Projekt 1', NULL, 1, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(19, 0)), N'520-2          ', N'Projekt 2', NULL, 1, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(19, 0)), N'540', N'Koszty jednostek', NULL, 1, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(19, 0)), N'550', N'Koszty Zarządu', NULL, 1, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(19, 0)), N'520-1', N'Projekt 1', NULL, 2, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(19, 0)), N'520-2', N'Projekt 2', NULL, 2, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(19, 0)), N'540', N'Koszty crew jednostek', NULL, 2, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(19, 0)), N'541', N'Koszty zarządu technicznego jednostek', NULL, 2, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(19, 0)), N'550', N'Koszty Zarządu', NULL, 2, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(19, 0)), N'520-1', N'Projekt 1', NULL, 3, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(19, 0)), N'520-2', N'Projekt 2', NULL, 3, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(12 AS Numeric(19, 0)), N'540', N'Koszty crew jednostek', NULL, 3, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(13 AS Numeric(19, 0)), N'541', N'Koszty zarządu technicznego jednostek', NULL, 3, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(14 AS Numeric(19, 0)), N'550', N'Koszty Zarządu', NULL, 3, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(15 AS Numeric(19, 0)), N'520-1          ', N'Projekt 1', NULL, 4, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(16 AS Numeric(19, 0)), N'520-2          ', N'Projekt 2', NULL, 4, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(17 AS Numeric(19, 0)), N'540', N'Koszty jednostek', NULL, 4, 1)
GO
INSERT [dsg_pz_komorka_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(18 AS Numeric(19, 0)), N'550', N'Koszty Zarządu', NULL, 4, 1)
GO
SET IDENTITY_INSERT [dsg_pz_komorka_raw] OFF
GO
SET IDENTITY_INSERT [dsg_pz_mpk_raw] ON 

GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(19, 0)), N'000                 ', N'Bez podziału na holowniki', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(19, 0)), N'001                 ', N'Holownik SERWAL (PŻ)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(19, 0)), N'002                 ', N'FAIRPLAY VII (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(19, 0)), N'004                 ', N'Holownik SERWAL-3 (Multra)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(19, 0)), N'007                 ', N'FAIRPLAY XI  (FP)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(19, 0)), N'009                 ', N'FAIRPLAY IV (FP)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(19, 0)), N'010                 ', N'FAIRPLAY I (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(19, 0)), N'012                 ', N'FAIRPLAY XII (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(19, 0)), N'014                 ', N'FAIRPLAY 14 (FP)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(19, 0)), N'015                 ', N'FAIRPLAY XV (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(19, 0)), N'017                 ', N'FAIRPLAY XVII (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(12 AS Numeric(19, 0)), N'023                 ', N'FAIRPLAY 23 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(13 AS Numeric(19, 0)), N'026                 ', N'FAIRPLAY 26 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(14 AS Numeric(19, 0)), N'027                 ', N'FAIRPLAY 27 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(15 AS Numeric(19, 0)), N'029                 ', N'FAIRPLAY 29 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(16 AS Numeric(19, 0)), N'030                 ', N'FAIRPLAY 30 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(17 AS Numeric(19, 0)), N'031                 ', N'Holownik URAN (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(18 AS Numeric(19, 0)), N'032                 ', N'Holownik ARGUS (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(19 AS Numeric(19, 0)), N'033                 ', N'Holownik ATLANT (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(20 AS Numeric(19, 0)), N'035                 ', N'Holownik STRALSUND (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(21 AS Numeric(19, 0)), N'041                 ', N'FAIRPLAY 31 (FH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(22 AS Numeric(19, 0)), N'061                 ', N'Centaur II', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(23 AS Numeric(19, 0)), N'062                 ', N'Heros', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(24 AS Numeric(19, 0)), N'063                 ', N'Odys', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(25 AS Numeric(19, 0)), N'064                 ', N'Odyseusz', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(26 AS Numeric(19, 0)), N'065                 ', N'Mocarz', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(27 AS Numeric(19, 0)), N'066                 ', N'Hektor', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(28 AS Numeric(19, 0)), N'067                 ', N'Herakles', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(29 AS Numeric(19, 0)), N'068                 ', N'Kronos', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(30 AS Numeric(19, 0)), N'069                 ', N'Mars', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(31 AS Numeric(19, 0)), N'101                 ', N'Barka pchana BA STOCIN-2', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(32 AS Numeric(19, 0)), N'104                 ', N'Barka paliwowa PŻ-3 GEDANIA (Lotos)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(33 AS Numeric(19, 0)), N'105                 ', N'Barka paliwowa PŻ-4 BALTICA (Lotos)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(34 AS Numeric(19, 0)), N'203                 ', N'Pilot 9', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(35 AS Numeric(19, 0)), N'204                 ', N'Motorówka Magdusia', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(36 AS Numeric(19, 0)), N'205                 ', N'Motorówka Jagna', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(37 AS Numeric(19, 0)), N'302                 ', N'PN 20', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(38 AS Numeric(19, 0)), N'303                 ', N'PN 21', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(39 AS Numeric(19, 0)), N'700                 ', N'Pchacz WEZYR 1 (FP)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(40 AS Numeric(19, 0)), N'701                 ', N'Pchacz WEZYR 2 (FP)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(41 AS Numeric(19, 0)), N'900                 ', N'Ponton DP 12 (PZ)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(42 AS Numeric(19, 0)), N'902                 ', N'DP-6 (PH)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(43 AS Numeric(19, 0)), N'903                 ', N'DP-7 (Port)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(44 AS Numeric(19, 0)), N'911                 ', N'STRAŻAK 25 (Port)', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(45 AS Numeric(19, 0)), N'999                 ', N'Pozostały drobny sprzęt pływający', NULL, 1, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(46 AS Numeric(19, 0)), N'000                 ', N'Bez podziału na holowniki', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(47 AS Numeric(19, 0)), N'001                 ', N'(PŻ) Holownik SERWAL-1 (PŻ)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(48 AS Numeric(19, 0)), N'002                 ', N'FAIRPLAY VII', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(49 AS Numeric(19, 0)), N'003                 ', N'NIEAUKTUALNY SERWAL 2', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(50 AS Numeric(19, 0)), N'004                 ', N'(PŻ) SERWAL 3 (PŻ T/C Z MULTRA)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(51 AS Numeric(19, 0)), N'007                 ', N'FAIRPLAY XI (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(52 AS Numeric(19, 0)), N'008                 ', N'FAIRPLAY III', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(53 AS Numeric(19, 0)), N'009                 ', N'(PŻ) FAIRPLAY IV (PŻ T/C Z FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(54 AS Numeric(19, 0)), N'010                 ', N'FAIRPLAY I (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(55 AS Numeric(19, 0)), N'011                 ', N'FAIRPLAY 31 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(56 AS Numeric(19, 0)), N'012                 ', N'FAIRPLAY 12 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(57 AS Numeric(19, 0)), N'014                 ', N'FAIRPLAY 14 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(58 AS Numeric(19, 0)), N'015                 ', N'FAIRPLAY XV (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(59 AS Numeric(19, 0)), N'017                 ', N'FAIRPLAY XVII (FH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(60 AS Numeric(19, 0)), N'023                 ', N'FAIRPLAY 23 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(61 AS Numeric(19, 0)), N'026                 ', N'FAIRPLAY 26 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(62 AS Numeric(19, 0)), N'027                 ', N'FAIRPLAY 27 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(63 AS Numeric(19, 0)), N'029                 ', N'FAIRPLAY 29 (FH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(64 AS Numeric(19, 0)), N'030                 ', N'FAIRPLAY 30 (FH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(65 AS Numeric(19, 0)), N'031                 ', N'Holownik URAN (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(66 AS Numeric(19, 0)), N'032                 ', N'Holownik ARGUS (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(67 AS Numeric(19, 0)), N'033                 ', N'Holownik ATLANT (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(68 AS Numeric(19, 0)), N'034                 ', N'(ZLIKWIDOWANY) Holownik SAMSON (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(69 AS Numeric(19, 0)), N'035                 ', N'Holownik STRALSUND (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(70 AS Numeric(19, 0)), N'041                 ', N'FAIRPLAY 33 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(71 AS Numeric(19, 0)), N'061                 ', N'Centaur II', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(72 AS Numeric(19, 0)), N'062                 ', N'Heros', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(73 AS Numeric(19, 0)), N'063                 ', N'Odys', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(74 AS Numeric(19, 0)), N'064                 ', N'Odyseusz', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(75 AS Numeric(19, 0)), N'065                 ', N'Mocarz', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(76 AS Numeric(19, 0)), N'066                 ', N'Hektor', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(77 AS Numeric(19, 0)), N'067                 ', N'Herakles', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(78 AS Numeric(19, 0)), N'068                 ', N'Kronos', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(79 AS Numeric(19, 0)), N'069                 ', N'Mars', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(80 AS Numeric(19, 0)), N'100                 ', N'WYCOFANA Barka pchana BA STOCIN-1', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(81 AS Numeric(19, 0)), N'101                 ', N'Barka pchana BA STOCIN-2', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(82 AS Numeric(19, 0)), N'102                 ', N'WYCOFANA Barka pchana STOCIN-3', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(83 AS Numeric(19, 0)), N'104                 ', N'Barka paliwowa PŻ-3 GEDANIA (Lotos)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(84 AS Numeric(19, 0)), N'105                 ', N'Barka paliwowa PŻ-4 BALTICA (Lotos)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(85 AS Numeric(19, 0)), N'200                 ', N'Motorówka LM-STOCIN 1', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(86 AS Numeric(19, 0)), N'201                 ', N'WYCOFANA Motorówka LM-STOCIN 5', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(87 AS Numeric(19, 0)), N'202                 ', N'WYCOFANA Motorówka AGATH', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(88 AS Numeric(19, 0)), N'203                 ', N'Pilot 9', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(89 AS Numeric(19, 0)), N'204                 ', N'Motorówka Magdusia', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(90 AS Numeric(19, 0)), N'205                 ', N'Motorówka Jagna', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(91 AS Numeric(19, 0)), N'300                 ', N'Ponton PN STOCIN 2', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(92 AS Numeric(19, 0)), N'301                 ', N'Ponton do zapory PN-PŻ-01', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(93 AS Numeric(19, 0)), N'302                 ', N'PN 20', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(94 AS Numeric(19, 0)), N'303                 ', N'PN 21', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(95 AS Numeric(19, 0)), N'400                 ', N'Łódź wiosłowa STOCIN 1', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(96 AS Numeric(19, 0)), N'401                 ', N'Łódź wiosłowa STOCIN 2', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(97 AS Numeric(19, 0)), N'700                 ', N'Pchacz WEZYR 1 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(98 AS Numeric(19, 0)), N'701                 ', N'Pchacz WEZYR 2 (FP)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(99 AS Numeric(19, 0)), N'900                 ', N'(PŻ) Ponton DP 12 (PZ)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(100 AS Numeric(19, 0)), N'901                 ', N'Ponton P-32', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(101 AS Numeric(19, 0)), N'902                 ', N'DP-6 (PH)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(102 AS Numeric(19, 0)), N'903                 ', N'DP-7 (Port)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(103 AS Numeric(19, 0)), N'910                 ', N'Bunkierka BUNKIER STOCIN 1', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(104 AS Numeric(19, 0)), N'911                 ', N'STRAŻAK 25 (Port)', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(105 AS Numeric(19, 0)), N'991                 ', N'WYCOFANY Hangar pływający', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(106 AS Numeric(19, 0)), N'999                 ', N'Pozostały drobny sprzęt pływający', NULL, 2, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(107 AS Numeric(19, 0)), N'000                 ', N'Bez podziału na holowniki', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(108 AS Numeric(19, 0)), N'001                 ', N'(PŻ) Holownik SERWAL-1 (PŻ)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(109 AS Numeric(19, 0)), N'002                 ', N'FAIRPLAY VII', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(110 AS Numeric(19, 0)), N'003                 ', N'NIEAUKTUALNY SERWAL 2', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(111 AS Numeric(19, 0)), N'004                 ', N'(PŻ) SERWAL 3 (PŻ T/C Z MULTRA)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(112 AS Numeric(19, 0)), N'007                 ', N'FAIRPLAY XI (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(113 AS Numeric(19, 0)), N'008                 ', N'FAIRPLAY III', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(114 AS Numeric(19, 0)), N'009                 ', N'(PŻ) FAIRPLAY IV (PŻ T/C Z FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(115 AS Numeric(19, 0)), N'010                 ', N'FAIRPLAY I (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(116 AS Numeric(19, 0)), N'011                 ', N'FAIRPLAY 31 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(117 AS Numeric(19, 0)), N'012                 ', N'FAIRPLAY 12 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(118 AS Numeric(19, 0)), N'014                 ', N'FAIRPLAY 14 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(119 AS Numeric(19, 0)), N'015                 ', N'FAIRPLAY XV (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(120 AS Numeric(19, 0)), N'017                 ', N'FAIRPLAY XVII (FH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(121 AS Numeric(19, 0)), N'023                 ', N'FAIRPLAY 23 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(122 AS Numeric(19, 0)), N'026                 ', N'FAIRPLAY 26 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(123 AS Numeric(19, 0)), N'027                 ', N'FAIRPLAY 27 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(124 AS Numeric(19, 0)), N'029                 ', N'FAIRPLAY 29 (FH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(125 AS Numeric(19, 0)), N'030                 ', N'FAIRPLAY 30 (FH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(126 AS Numeric(19, 0)), N'031                 ', N'Holownik URAN (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(127 AS Numeric(19, 0)), N'032                 ', N'Holownik ARGUS (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(128 AS Numeric(19, 0)), N'033                 ', N'Holownik ATLANT (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(129 AS Numeric(19, 0)), N'034                 ', N'(ZLIKWIDOWANY) Holownik SAMSON (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(130 AS Numeric(19, 0)), N'035                 ', N'Holownik STRALSUND (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(131 AS Numeric(19, 0)), N'041                 ', N'FAIRPLAY 33 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(132 AS Numeric(19, 0)), N'061                 ', N'Centaur II', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(133 AS Numeric(19, 0)), N'062                 ', N'Heros', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(134 AS Numeric(19, 0)), N'063                 ', N'Odys', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(135 AS Numeric(19, 0)), N'064                 ', N'Odyseusz', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(136 AS Numeric(19, 0)), N'065                 ', N'Mocarz', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(137 AS Numeric(19, 0)), N'066                 ', N'Hektor', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(138 AS Numeric(19, 0)), N'067                 ', N'Herakles', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(139 AS Numeric(19, 0)), N'068                 ', N'Kronos', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(140 AS Numeric(19, 0)), N'069                 ', N'Mars', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(141 AS Numeric(19, 0)), N'100                 ', N'WYCOFANA Barka pchana BA STOCIN-1', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(142 AS Numeric(19, 0)), N'101                 ', N'Barka pchana BA STOCIN-2', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(143 AS Numeric(19, 0)), N'102                 ', N'WYCOFANA Barka pchana STOCIN-3', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(144 AS Numeric(19, 0)), N'104                 ', N'Barka paliwowa PŻ-3 GEDANIA (Lotos)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(145 AS Numeric(19, 0)), N'105                 ', N'Barka paliwowa PŻ-4 BALTICA (Lotos)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(146 AS Numeric(19, 0)), N'200                 ', N'Motorówka LM-STOCIN 1', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(147 AS Numeric(19, 0)), N'201                 ', N'WYCOFANA Motorówka LM-STOCIN 5', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(148 AS Numeric(19, 0)), N'202                 ', N'WYCOFANA Motorówka AGATH', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(149 AS Numeric(19, 0)), N'203                 ', N'Pilot 9', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(150 AS Numeric(19, 0)), N'204                 ', N'Motorówka Magdusia', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(151 AS Numeric(19, 0)), N'205                 ', N'Motorówka Jagna', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(152 AS Numeric(19, 0)), N'300                 ', N'Ponton PN STOCIN 2', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(153 AS Numeric(19, 0)), N'301                 ', N'Ponton do zapory PN-PŻ-01', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(154 AS Numeric(19, 0)), N'302                 ', N'PN 20', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(155 AS Numeric(19, 0)), N'303                 ', N'PN 21', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(156 AS Numeric(19, 0)), N'400                 ', N'Łódź wiosłowa STOCIN 1', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(157 AS Numeric(19, 0)), N'401                 ', N'Łódź wiosłowa STOCIN 2', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(158 AS Numeric(19, 0)), N'700                 ', N'Pchacz WEZYR 1 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(159 AS Numeric(19, 0)), N'701                 ', N'Pchacz WEZYR 2 (FP)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(160 AS Numeric(19, 0)), N'800                 ', N'Cuma', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(161 AS Numeric(19, 0)), N'900                 ', N'(PŻ) Ponton DP 12 (PZ)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(162 AS Numeric(19, 0)), N'901                 ', N'Ponton P-32', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(163 AS Numeric(19, 0)), N'902                 ', N'DP-6 (PH)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(164 AS Numeric(19, 0)), N'903                 ', N'DP-7 (Port)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(165 AS Numeric(19, 0)), N'910                 ', N'Bunkierka BUNKIER STOCIN 1', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(166 AS Numeric(19, 0)), N'911                 ', N'STRAŻAK 25 (Port)', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(167 AS Numeric(19, 0)), N'991                 ', N'WYCOFANY Hangar pływający', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(168 AS Numeric(19, 0)), N'999                 ', N'Pozostały drobny sprzęt pływający', NULL, 3, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(169 AS Numeric(19, 0)), N'000                 ', N'Bez podziału na holowniki', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(170 AS Numeric(19, 0)), N'001                 ', N'Holownik SERWAL (PŻ)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(171 AS Numeric(19, 0)), N'002                 ', N'FAIRPLAY VII (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(172 AS Numeric(19, 0)), N'004                 ', N'Holownik SERWAL-3 (Multra)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(173 AS Numeric(19, 0)), N'007                 ', N'FAIRPLAY XI  (FP)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(174 AS Numeric(19, 0)), N'009                 ', N'FAIRPLAY IV (FP)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(175 AS Numeric(19, 0)), N'010                 ', N'FAIRPLAY I (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(176 AS Numeric(19, 0)), N'012                 ', N'FAIRPLAY XII (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(177 AS Numeric(19, 0)), N'014                 ', N'FAIRPLAY 14 (FP)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(178 AS Numeric(19, 0)), N'015                 ', N'FAIRPLAY XV (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(179 AS Numeric(19, 0)), N'017                 ', N'FAIRPLAY XVII (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(180 AS Numeric(19, 0)), N'023                 ', N'FAIRPLAY 23 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(181 AS Numeric(19, 0)), N'026                 ', N'FAIRPLAY 26 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(182 AS Numeric(19, 0)), N'027                 ', N'FAIRPLAY 27 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(183 AS Numeric(19, 0)), N'029                 ', N'FAIRPLAY 29 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(184 AS Numeric(19, 0)), N'030                 ', N'FAIRPLAY 30 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(185 AS Numeric(19, 0)), N'031                 ', N'Holownik URAN (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(186 AS Numeric(19, 0)), N'032                 ', N'Holownik ARGUS (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(187 AS Numeric(19, 0)), N'033                 ', N'Holownik ATLANT (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(188 AS Numeric(19, 0)), N'035                 ', N'Holownik STRALSUND (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(189 AS Numeric(19, 0)), N'041                 ', N'FAIRPLAY 31 (FH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(190 AS Numeric(19, 0)), N'061                 ', N'Centaur II', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(191 AS Numeric(19, 0)), N'062                 ', N'Heros', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(192 AS Numeric(19, 0)), N'063                 ', N'Odys', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(193 AS Numeric(19, 0)), N'064                 ', N'Odyseusz', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(194 AS Numeric(19, 0)), N'065                 ', N'Mocarz', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(195 AS Numeric(19, 0)), N'066                 ', N'Hektor', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(196 AS Numeric(19, 0)), N'067                 ', N'Herakles', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(197 AS Numeric(19, 0)), N'068                 ', N'Kronos', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(198 AS Numeric(19, 0)), N'069                 ', N'Mars', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(199 AS Numeric(19, 0)), N'104                 ', N'Barka paliwowa PŻ-3 GEDANIA (Lotos)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(200 AS Numeric(19, 0)), N'105                 ', N'Barka paliwowa PŻ-4 BALTICA (Lotos)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(201 AS Numeric(19, 0)), N'203                 ', N'Pilot 9', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(202 AS Numeric(19, 0)), N'204                 ', N'Motorówka Magdusia', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(203 AS Numeric(19, 0)), N'205                 ', N'Motorówka Jagna', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(204 AS Numeric(19, 0)), N'302                 ', N'PN 20', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(205 AS Numeric(19, 0)), N'303                 ', N'PN 21', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(206 AS Numeric(19, 0)), N'700                 ', N'Pchacz WEZYR 1 (FP)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(207 AS Numeric(19, 0)), N'701                 ', N'Pchacz WEZYR 2 (FP)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(208 AS Numeric(19, 0)), N'800                 ', N'Cuma', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(209 AS Numeric(19, 0)), N'900                 ', N'Ponton DP 12 (PZ)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(210 AS Numeric(19, 0)), N'902                 ', N'DP-6 (PH)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(211 AS Numeric(19, 0)), N'903                 ', N'DP-7 (Port)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(212 AS Numeric(19, 0)), N'911                 ', N'STRAŻAK 25 (Port)', NULL, 4, 1)
GO
INSERT [dsg_pz_mpk_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(213 AS Numeric(19, 0)), N'999                 ', N'Pozostały drobny sprzęt pływający', NULL, 4, 1)
GO
SET IDENTITY_INSERT [dsg_pz_mpk_raw] OFF
GO
SET IDENTITY_INSERT [dsg_pz_odbiorca_refaktury_raw] ON 

GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(1 AS Numeric(19, 0)), N' Hans Lehmann', N' m/v"Anna Lehmann" c/o Hans Lehmann KG', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(2 AS Numeric(19, 0)), N' JAN', N'JAN STĘPNIEWSKI I SKA', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(3 AS Numeric(19, 0)), N'AAFSLEF', N'PER AARSLEFF A/S', NULL, 1, 1, N'Abyhoj')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(4 AS Numeric(19, 0)), N'AAFSLEF', N'PER AARSLEFF A/S', NULL, 2, 1, N'Abyhoj')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(5 AS Numeric(19, 0)), N'AAFSLEF', N'PER AARSLEFF A/S', NULL, 4, 1, N'Abyhoj')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(6 AS Numeric(19, 0)), N'AARSLEF POL', N'Per Aarsleff A/S Oddział w Polsce', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(7 AS Numeric(19, 0)), N'AARSLEFF BILFIN', N'Aarsleff Bilfinger Berger JV', NULL, 1, 1, N'10 Grey Coat Place            ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(8 AS Numeric(19, 0)), N'Aarsleff Polska', N'Per Aarsleff A/S Oddział w Polsce', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(9 AS Numeric(19, 0)), N'Aarsleff Polska', N'Per Aarsleff A/S Oddział w Polsce', NULL, 2, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(10 AS Numeric(19, 0)), N'AASSEN', N'Aasen Chartering AS', NULL, 4, 1, N'Mosterhamn, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(11 AS Numeric(19, 0)), N'aassen charte', N'Aasen Chartering AS', NULL, 1, 1, N'Mosterhamn, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(12 AS Numeric(19, 0)), N'ace shipping', N'Ace Shipping A/S ', NULL, 1, 1, N'Hellerup , Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(13 AS Numeric(19, 0)), N'ACIRIC C.V.', N'ACIRIC C.V.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(14 AS Numeric(19, 0)), N'Ahlmann Zersse', N'Ahlmann-Zerssen Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(15 AS Numeric(19, 0)), N'AHLMANN ZERSSER', N'Ahlmann-Zerssen Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(16 AS Numeric(19, 0)), N'AIDA', N'AIDA Cruises,Am Strande 3d,18055 Rostock,Germany', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(17 AS Numeric(19, 0)), N'AIP III', N'Arbeitsgemeinschaft Ingenieurbau Pier III', NULL, 1, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(18 AS Numeric(19, 0)), N'AIP III', N'Arbeitsgemeinschaft Ingenieurbau Pier III', NULL, 2, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(19 AS Numeric(19, 0)), N'AIP III', N'Arbeitsgemeinschaft Ingenieurbau Pier III', NULL, 4, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(20 AS Numeric(19, 0)), N'AKADEMIA', N'AKADEMIA MORSKA W GDYNI', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(21 AS Numeric(19, 0)), N'akademia', N'Akademia Morska w Szczecinie', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(22 AS Numeric(19, 0)), N'Alameda Mamoré', N'Alameda Mamoré ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(23 AS Numeric(19, 0)), N'ALBA HOUSE', N'Alba House,2 Central Avenue,Northern Marine Management Ltd.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(24 AS Numeric(19, 0)), N'ALBERS', N'ALBERS-HANSEN', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(25 AS Numeric(19, 0)), N'albers hansen', N'Master / Owner of m/v Blue Note', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(26 AS Numeric(19, 0)), N'ALEXANDER', N'ALEXANDER MARITIME LTD.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(27 AS Numeric(19, 0)), N'Allied', N'Allied Maritime Inc', NULL, 1, 1, N'c/o 3 Drossini Street')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(28 AS Numeric(19, 0)), N'ALLIED', N'Allied Maritime Inc', NULL, 4, 1, N'c/o 3 Drossini Street')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(29 AS Numeric(19, 0)), N'ALMEX', N'ALMEX Spółka z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(30 AS Numeric(19, 0)), N'ALMEX', N'ALMEX Spółka z o.o.', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(31 AS Numeric(19, 0)), N'ALMEX', N'ALMEX Spółka z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(32 AS Numeric(19, 0)), N'alpha shipp', N'T/C Owners of mv "Yarrawonga"', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(33 AS Numeric(19, 0)), N'ALPHA SHIPPING', N'T/C Owners of mv" Mangarella" (Transbulk 1904 AB, Goeteborg)', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(34 AS Numeric(19, 0)), N'ALUMARE', N'ALUMARE Sp. z o.o.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(35 AS Numeric(19, 0)), N'Amasus ', N'Amasus Fleet B.V.Zijlvest 26, ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(36 AS Numeric(19, 0)), N'AMASUS FLEET', N'AMASUS SHIPPING B.V.', NULL, 1, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(37 AS Numeric(19, 0)), N'AMERICAN', N'AMERICAN PRESIDENT LINES, LTD', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(38 AS Numeric(19, 0)), N'AMERICAN000', N'AMERICAN PRESIDENT LINES, LTD', NULL, 4, 1, N'Scottsdale')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(39 AS Numeric(19, 0)), N'ANCHOR', N' ANCHOR AGENTS & SHIPBROKERS SP. Z O.O.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(40 AS Numeric(19, 0)), N'ANCHOR', N'Anchor Agents & Shipbrokers Sp z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(41 AS Numeric(19, 0)), N'Andreas', N'ANDREAS Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(42 AS Numeric(19, 0)), N'ANTARES', N'Antares Shipping Agency', NULL, 4, 1, N'St.Petersburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(43 AS Numeric(19, 0)), N'ARAMIS', N'ARAMIS SHIPPING AGENCY', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(44 AS Numeric(19, 0)), N'Aramis Shipp', N'Owners mt SCORPIUS', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(45 AS Numeric(19, 0)), N'ARAMIS SHIPPING', N'Owners mt SCORPIUS', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(46 AS Numeric(19, 0)), N'ARKLOW', N'ARKLOW SHIPPING Ltd North Quay,Arklow,Co Wicklow', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(47 AS Numeric(19, 0)), N'Arklow Ned.', N'Arklow Shipping Nederland BV', NULL, 4, 1, N'Rotterdam/ The Nederland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(48 AS Numeric(19, 0)), N'Arklow Shipping', N'Arklow Shipping Ltd North Quay,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(49 AS Numeric(19, 0)), N'ART', N'Agencja Obsługi Statków Art', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(50 AS Numeric(19, 0)), N'ART', N'Agencja Obsługi Statków Art', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(51 AS Numeric(19, 0)), N'ART', N'Agencja Obsługi Statków Art', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(52 AS Numeric(19, 0)), N'B + K', N'B + K   AGENCY', NULL, 4, 1, N'Sopot')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(53 AS Numeric(19, 0)), N'BAGS', N'BAG''S SPÓŁKA  Z  O.O.', NULL, 4, 1, N'BIAŁYSTOK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(54 AS Numeric(19, 0)), N'BALT - T', N'BALT - T  AGENCY SPÓŁKA  Z O.O. ', NULL, 4, 1, N'Sopot')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(55 AS Numeric(19, 0)), N'BALTA', N'BALTA S.A.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(56 AS Numeric(19, 0)), N'BALTEX', N'BALTEX ENERGIA I GÓRNICTWO MORSKIE Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(57 AS Numeric(19, 0)), N'BALTEX', N'BALTEX TRANSPORT MORSKI   Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(58 AS Numeric(19, 0)), N'Baltex Energia', N'Baltex Energia i Górnictwo Morskie Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(59 AS Numeric(19, 0)), N'Baltic G', N'Baltic Gateway Poland ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(60 AS Numeric(19, 0)), N'Balticus', N'Master / Owner of mv RUSICH 8', NULL, 1, 1, N'Kliniska Wielkie')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(61 AS Numeric(19, 0)), N'BALTRAMP', N'Baltramp Shipping Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(62 AS Numeric(19, 0)), N'BARANOWSKI', N'BARANOWSKI KAZIMIERZ', NULL, 4, 1, N'ŁEBNO 9')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(63 AS Numeric(19, 0)), N'BARANOWSKI DARI', N'BARANOWSKI DARIUSZ', NULL, 4, 1, N'STAROGARD GDAŃSKI')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(64 AS Numeric(19, 0)), N'BBB', N'BBB Schlepp-und Hafendienst GmbH', NULL, 1, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(65 AS Numeric(19, 0)), N'BBB', N'BBB Schlepp-und Hafendienst GmbH', NULL, 2, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(66 AS Numeric(19, 0)), N'BBB', N'BBB Schlepp-und Hafendienst GmbH', NULL, 3, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(67 AS Numeric(19, 0)), N'BBB', N'BBB Schlepp-und Hafendienst GmbH', NULL, 4, 1, N'Rostock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(68 AS Numeric(19, 0)), N'BBC', N'BBC CHARTERING & LOGISTIC GmbH & Co. KG', NULL, 4, 1, N'Leer')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(69 AS Numeric(19, 0)), N'BEMO MOTORS', N'BEMO MOTORS SP. Z O.O.', NULL, 1, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(70 AS Numeric(19, 0)), N'BFL', N'B.F.L. Baltic Ferries Lines Limited', NULL, 1, 1, N'Limassol, Cyprus')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(71 AS Numeric(19, 0)), N'BFL', N'B.F.L. Baltic Ferries Lines Limited', NULL, 4, 1, N'P.C. 3106 Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(72 AS Numeric(19, 0)), N'bfl c/o', N'B.F.L. Baltic Ferries Lines Limited', NULL, 1, 1, N'c/o Polska Żegluga Morska PP')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(73 AS Numeric(19, 0)), N'BIELUSZEWSKI ', N'BIELUSZEWSKI ANDRZEJ', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(74 AS Numeric(19, 0)), N'Biglift', N'Biglift Shipping B.V.Amsterdam', NULL, 4, 1, N'Amsterdam,The Netherlands')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(75 AS Numeric(19, 0)), N'BIL', N'BIL ZDZISŁAW EUGENIUSZ', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(76 AS Numeric(19, 0)), N'Blue Star', N'Blue Star Chemikal Carriers Ltd.', NULL, 4, 1, N'Valletta VLT 1171')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(77 AS Numeric(19, 0)), N'BMS', N'BALTIC MARINE SURVEYORS  Spółka z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(78 AS Numeric(19, 0)), N'BOA TUGS', N'Boa Tugs AB', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(79 AS Numeric(19, 0)), N'boskalis', N'Boskalis International B.V.', NULL, 1, 1, N'Papendrecht')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(80 AS Numeric(19, 0)), N'BOSKALIS NL', N'Baggermaaschappij  Boskalis BV ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(81 AS Numeric(19, 0)), N'BP SHIPPING', N'BP Shipping Ltd. c/o Cory Hub Services Ltd, ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(82 AS Numeric(19, 0)), N'BRE LEASING', N'BRE LEASING SP. Z O.O.', NULL, 1, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(83 AS Numeric(19, 0)), N'BRE LEASING', N'BRE LEASING SP. Z O.O.', NULL, 2, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(84 AS Numeric(19, 0)), N'BRE LEASING', N'BRE LEASING SP. Z O.O.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(85 AS Numeric(19, 0)), N'BREDO', N'Bredo - Bremerhaven Dock GmbH', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(86 AS Numeric(19, 0)), N'BRENTON RIDGE', N'BRETTON RIDGE SHIPPING CPOMNPANY', NULL, 4, 1, N'FREDRIKSTAD NORWAY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(87 AS Numeric(19, 0)), N'BRIESE', N'BRIESE SHIPPING AGENCY LTD SP. Z O.O', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(88 AS Numeric(19, 0)), N'BRIESE', N'Briese Shipping Agency Ltd Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(89 AS Numeric(19, 0)), N'BROSTORM', N'Brostrom Tankers AB', NULL, 4, 1, N'Goteborg, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(90 AS Numeric(19, 0)), N'brostrom', N'Brostrom AB', NULL, 1, 1, N'Goteborg, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(91 AS Numeric(19, 0)), N'Brostrom A/S', N'Brostrom A/S', NULL, 1, 1, N'Kobenhavn K, Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(92 AS Numeric(19, 0)), N'Brovig', N'M/T Brovig Vindur c/o Brovigtank AS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(93 AS Numeric(19, 0)), N'Bryggen', N'Bryggen Shipping International AS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(94 AS Numeric(19, 0)), N'BRZESKI MIROSŁA', N'BRZESKI MIROSŁAW', NULL, 4, 1, N'REKOWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(95 AS Numeric(19, 0)), N'BSA', N'Baltic Shipping Agency LTD Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(96 AS Numeric(19, 0)), N'BSA', N'Baltic Shipping Agency LTD Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(97 AS Numeric(19, 0)), N'bsc', N'Baltic Stevedoring Company ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(98 AS Numeric(19, 0)), N'BTD', N'BAŁTYCKI TERMINAL DROBNICOWY', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(99 AS Numeric(19, 0)), N'BTDG', N'BTDG-Bałtycki Terminal Drobnicowy Gdynia  Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(100 AS Numeric(19, 0)), N'BTZ', N'BAŁTYCKI TERMINAL ZBOŻOWY', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(101 AS Numeric(19, 0)), N'BULKCARGO', N'Bulk Cargo-Port Szczecin Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(102 AS Numeric(19, 0)), N'BUNGE', N'BUNGE  SA - Freight', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(103 AS Numeric(19, 0)), N'burger', N'Burger Poland Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(104 AS Numeric(19, 0)), N'BURGER POLAND', N'Burger Poland  Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(105 AS Numeric(19, 0)), N'BUTER', N'BUTER HEBETECHNIK GmbH', NULL, 1, 1, N'Meppen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(106 AS Numeric(19, 0)), N'BUTER', N'BUTER HEBETECHNIK GmbH', NULL, 2, 1, N'Meppen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(107 AS Numeric(19, 0)), N'BUTER', N'BUTER HEBETECHNIK GmbH', NULL, 4, 1, N'Meppen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(108 AS Numeric(19, 0)), N'BW rock group', N'BW ROCK GROUP ŚWINOUJŚCIE S.C.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(109 AS Numeric(19, 0)), N'BW ROCK GROUP', N'BW ROCK GROUP ŚWINOUJŚCIE S.C.', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(110 AS Numeric(19, 0)), N'C&T', N'C & T  MARINE CONSULTANTS  CO  LTD', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(111 AS Numeric(19, 0)), N'C.Hartwig', N' C.HARTWIG  Gdynia S.A.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(112 AS Numeric(19, 0)), N'C.V.  S.O. LADY', N'C.V.   S.O. LADY AMALIA', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(113 AS Numeric(19, 0)), N'Canada Moon', N'Canada Moon Shipping Co.Ltd ', NULL, 1, 1, N'CY-4002 Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(114 AS Numeric(19, 0)), N'capt', N'Capt. a/o Owners mv Tama Hope', NULL, 1, 1, N'AW Groningen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(115 AS Numeric(19, 0)), N'Captain', N'Captain/Owners m/v  "MAGDA D.', NULL, 4, 1, N' Lubeck')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(116 AS Numeric(19, 0)), N'cargill', N'Cargill International SA ', NULL, 1, 1, N'Geneva,  SWITZERLAND ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(117 AS Numeric(19, 0)), N'CARGILL', N'CARGILL INTERNATIONAL SA', NULL, 4, 1, N'Geneva,Switzerland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(118 AS Numeric(19, 0)), N'Cargill', N'Cargill Poland Sp. z o.o.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(119 AS Numeric(19, 0)), N'Cargill(Polska)', N'Cargill International SA', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(120 AS Numeric(19, 0)), N'Carisbroke', N'MV Geja C York Shipping Limited', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(121 AS Numeric(19, 0)), N'CAVERAL', N'Caveral Spółka Jawna', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(122 AS Numeric(19, 0)), N'Cdt.', N'Cdt.Commandant Blaison', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(123 AS Numeric(19, 0)), N'cecilia', N'CECILIA MARITIME S.A.', NULL, 1, 1, N' Panama City, PANAMA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(124 AS Numeric(19, 0)), N'cemex', N'CEMEX Polska Sp. z o.o.', NULL, 1, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(125 AS Numeric(19, 0)), N'CENTRUM WYCHOWA', N'Centrum Wychowania Morskiego ZHP', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(126 AS Numeric(19, 0)), N'CENTRUM WYCHOWA', N'Centrum Wychowania Morskiego ZHP', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(127 AS Numeric(19, 0)), N'cg falochron', N'CG FALOCHRON SWINOUJŚCIE S.C.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(128 AS Numeric(19, 0)), N'CHEŁMICKI', N'JAN CHEŁMICKI', NULL, 1, 1, N'Mirosławiec')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(129 AS Numeric(19, 0)), N'CHEŁMICKI', N'JAN CHEŁMICKI', NULL, 2, 1, N'Mirosławiec')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(130 AS Numeric(19, 0)), N'CHEŁMICKI', N'JAN CHEŁMICKI', NULL, 4, 1, N'Mirosławiec')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(131 AS Numeric(19, 0)), N'chem gas', N'Chemgas Shipping', NULL, 1, 1, N'Be Rotterdam, Holland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(132 AS Numeric(19, 0)), N'CHINESE', N'CHINESE-POLISH JOINT STOCK SHIPPING CO. BRANCH OFFICE', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(133 AS Numeric(19, 0)), N'CHINESE-POLISH', N'CHINESE- POLISH JOINT STOCK SHIPPING COMPANY', NULL, 4, 1, N'SHANGHAI')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(134 AS Numeric(19, 0)), N'CHMIELEWSKA', N'CHMIELEWSKA MAŁGORZATA', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(135 AS Numeric(19, 0)), N'CIECILIA', N'CECILIA MARITIME S.A.', NULL, 4, 1, N'GIJON, SPAIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(136 AS Numeric(19, 0)), N'CIESIELSKI', N'Ciesielski Waldemar', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(137 AS Numeric(19, 0)), N'CIESIELSKI', N'Ciesielski Waldemar', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(138 AS Numeric(19, 0)), N'CIESIELSKI', N'Ciesielski Waldemar', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(139 AS Numeric(19, 0)), N'CIESIELSKI A', N'CIESIELSKI ARKADIUSZ', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(140 AS Numeric(19, 0)), N'Clipper Bulk ', N'Clipper Bulk Transportes Marítimos Ltda.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(141 AS Numeric(19, 0)), N'CMA', N'CMA-CGM S.A.', NULL, 4, 1, N'MARSEILLE CEDEX 2')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(142 AS Numeric(19, 0)), N'CMA-CGM', N'CMA - CGM S.A. ', NULL, 1, 1, N'Marseille')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(143 AS Numeric(19, 0)), N'CMN internation', N'CMN International S.A. ', NULL, 1, 1, N'Majuro, Marshall Islands')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(144 AS Numeric(19, 0)), N'colcrete', N'Colcrete von Essen GmbH & Co Kg', NULL, 1, 1, N'Rastede, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(145 AS Numeric(19, 0)), N'COLCRETE', N'Colcrete von Essen GmbH & Co Kg', NULL, 4, 1, N'Rastede, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(146 AS Numeric(19, 0)), N'cosco', N'COSCOL (H.K.) INVESTMENT & DEVELOPMENT CO., LTD.', NULL, 1, 1, N'ROAD CENTRAL, HONG KONG ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(147 AS Numeric(19, 0)), N'Crist S.A.', N'Crist S.A.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(148 AS Numeric(19, 0)), N'Crystal', N'Crystal Pool AS ', NULL, 4, 1, N'Bergen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(149 AS Numeric(19, 0)), N'CSL EUROPE', N'CSL EUROPE Ltd Number One Windsor', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(150 AS Numeric(19, 0)), N'CST COMET', N'Master Owners of m/v "Setlark" ', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(151 AS Numeric(19, 0)), N'cst comet', N'Master Owners of m/v ULRIKE G', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(152 AS Numeric(19, 0)), N'CV  SO  ROBIJN', N'CV  SO  ROBIJN  c/o  Wagenborg Shipping BV', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(153 AS Numeric(19, 0)), N'CV SO LADY ANNE', N'CV  SO  LADY  ANNEKE', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(154 AS Numeric(19, 0)), N'CZAJA MAREK', N'CZAJA MAREK', NULL, 4, 1, N'BOLSZEWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(155 AS Numeric(19, 0)), N'CZAPP JERZY', N'CZAPP JERZY', NULL, 4, 1, N'ŻELISTRZEWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(156 AS Numeric(19, 0)), N'D', N'Detal', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(157 AS Numeric(19, 0)), N'D', N'Detal', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(158 AS Numeric(19, 0)), N'D', N'Detal', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(159 AS Numeric(19, 0)), N'DA-desk FZE', N'DA-DESK FZE', NULL, 1, 1, N'UNITED ARAB EMIRATES')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(160 AS Numeric(19, 0)), N'Dalmor', N'DALMOR Spółka Akcyjna', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(161 AS Numeric(19, 0)), N'Damen', N'Damen Shipyards Gdynia Spółka Akcyjna', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(162 AS Numeric(19, 0)), N'DAMPC', N'DAMPC EDWARD', NULL, 4, 1, N'WEJHEROWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(163 AS Numeric(19, 0)), N'DAN SHIPPING', N'DAN SHIPPING AND TRADING', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(164 AS Numeric(19, 0)), N'DAN SHIPPING', N'DAN SHIPPING AND TRADING  CO.LTD.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(165 AS Numeric(19, 0)), N'Danbor AS', N'Danbor A/S', NULL, 1, 1, N'Esbjerg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(166 AS Numeric(19, 0)), N'DAWIDOWSKI JÓZE', N'DAWIDOWSKI JÓZEF', NULL, 4, 1, N'STAWKI 78')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(167 AS Numeric(19, 0)), N'DB PORT', N'DB Port Szczecin Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(168 AS Numeric(19, 0)), N'Delta', N'Delta Products LLC, Marshall Islands, c/o Delta Products SIA', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(169 AS Numeric(19, 0)), N'DEME', N'DEME - Belgium', NULL, 1, 1, N'Zwijndrecht')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(170 AS Numeric(19, 0)), N'DEODAR', N'DEODAR SHIPPING LTD. ', NULL, 1, 1, N'NO.18/2 , KAT:2 , B BLOK      ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(171 AS Numeric(19, 0)), N'Detlef Hegeman', N'Detlef Hegemann GmbH', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(172 AS Numeric(19, 0)), N'DIVERS', N'DIVERS S.C.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(173 AS Numeric(19, 0)), N'DIVERS', N'DIVERS S.C.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(174 AS Numeric(19, 0)), N'DOKER', N'Przedsiębiorstwo Robót Portowych DOKER Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(175 AS Numeric(19, 0)), N'DOKER', N'Przedsiębiorstwo Robót Portowych DOKER Sp. z o.o. ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(176 AS Numeric(19, 0)), N'Dolphin', N'Dolphin Maritime LLC', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(177 AS Numeric(19, 0)), N'DOSZ', N'ZAKŁAD USŁUGOWO-HANDLOWY', NULL, 4, 1, N'SZEMUD')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(178 AS Numeric(19, 0)), N'Drabert', N'Drabert Schiffahrts GmbH ', NULL, 1, 1, N'Oldenburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(179 AS Numeric(19, 0)), N'EAST-NAV', N'East-Nav Spółka z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(180 AS Numeric(19, 0)), N'ECS', N'Express-Interfracht Polska Sp. z o.o.', NULL, 1, 1, N'Będzin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(181 AS Numeric(19, 0)), N'ECS', N'Express-Interfracht Polska Sp. z o.o.', NULL, 4, 1, N'Będzin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(182 AS Numeric(19, 0)), N'ED LINE', N'Reederei Ed Line GmbH', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(183 AS Numeric(19, 0)), N'EFSA', N'Euro Forwarding and Shipping Agency Sp. z o.o.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(184 AS Numeric(19, 0)), N'Eides', N'Eides Rederi A/S', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(185 AS Numeric(19, 0)), N'Eimskip', N'EIMSKIP - CTG AS', NULL, 1, 1, N'SORTLAND, NORWAY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(186 AS Numeric(19, 0)), N'EITZEN', N'EITZEN CHEMICAL INVEST (SINGAPORE)', NULL, 1, 1, N'Lavallois Perret, France')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(187 AS Numeric(19, 0)), N'Ek tank', N'EKTANK AB', NULL, 1, 1, N'Gothenburg, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(188 AS Numeric(19, 0)), N'EKTANK', N'EKTANK AB', NULL, 4, 1, N'Gothenburg, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(189 AS Numeric(19, 0)), N'ENERGO', N'Energomontaż- Północ Gdynia', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(190 AS Numeric(19, 0)), N'Epidnav', N'Epidnav Marine Company LTD', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(191 AS Numeric(19, 0)), N'Er Denizcilik', N'Er Denizcilik San Nak ve Tic A.S.', NULL, 1, 1, N'Istanbul')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(192 AS Numeric(19, 0)), N'ESL ', N'ESL Shipping Ltd', NULL, 1, 1, N'Helsinki ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(193 AS Numeric(19, 0)), N'ESL', N'ESL Shipping Oy', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(194 AS Numeric(19, 0)), N'Euroafrica', N'Euroafrica Services Limited', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(195 AS Numeric(19, 0)), N'Euroafrica', N'Euroafrica Services Limited', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(196 AS Numeric(19, 0)), N'EUROAFRICA', N'EUROAFRICA SHIPPING LINES  LIMITED', NULL, 4, 1, N'Limassol, CYPRUS')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(197 AS Numeric(19, 0)), N'EUROAFRICA', N'EUROAFRICA SHIPPING LINES CYPRUS LIMITED', NULL, 2, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(198 AS Numeric(19, 0)), N'EUROAFRICA', N'EUROAFRICA SHIPPING LINES LIMITED', NULL, 1, 1, N'Limassol, CYPRUS')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(199 AS Numeric(19, 0)), N'euroafrica c/o', N'EUROAFRICA SHIPPING LINES CYPRUS LIMITED', NULL, 1, 1, N'Polsce')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(200 AS Numeric(19, 0)), N'EUROAFRICA SERV', N'Euroafrica Services Limited', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(201 AS Numeric(19, 0)), N'EUROCARGO', N'EUROCARGO GDYNIA ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(202 AS Numeric(19, 0)), N'Euromos', N'Euromos Spółka z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(203 AS Numeric(19, 0)), N'FAIRPLAY 2', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 1, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(204 AS Numeric(19, 0)), N'FAIRPLAY 2', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 2, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(205 AS Numeric(19, 0)), N'FAIRPLAY 2', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 3, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(206 AS Numeric(19, 0)), N'FAIRPLAY 2', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(207 AS Numeric(19, 0)), N'FAIRPLAY ARGE', N'SCHLEPPER-ARGE MV', NULL, 1, 1, N'ROSTOCK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(208 AS Numeric(19, 0)), N'FAIRPLAY ARGE', N'SCHLEPPER-ARGE MV', NULL, 3, 1, N'ROSTOCK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(209 AS Numeric(19, 0)), N'FAIRPLAY ARGE', N'SCHLEPPER-ARGE MV', NULL, 4, 1, N'ROSTOCK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(210 AS Numeric(19, 0)), N'FAIRPLAY B.V.', N'FAIRPLAY B.V. c/o Fairplay Schleppdampfschiffs-Reederei', NULL, 1, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(211 AS Numeric(19, 0)), N'FAIRPLAY B.V.', N'FAIRPLAY B.V. c/o Fairplay Schleppdampfschiffs-Reederei', NULL, 3, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(212 AS Numeric(19, 0)), N'FAIRPLAY B.V.', N'FAIRPLAY B.V. c/o Fairplay Schleppdampfschiffs-Reederei', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(213 AS Numeric(19, 0)), N'FAIRPLAY B.V.', N'Master/Owner Fairplay 32', NULL, 2, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(214 AS Numeric(19, 0)), N'FAIRPLAY HAMBUR', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 1, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(215 AS Numeric(19, 0)), N'FAIRPLAY HAMBUR', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 2, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(216 AS Numeric(19, 0)), N'FAIRPLAY HAMBUR', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 3, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(217 AS Numeric(19, 0)), N'FAIRPLAY HAMBUR', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(218 AS Numeric(19, 0)), N'FAIRPLAY NL', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 1, 1, N'Rotterdam')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(219 AS Numeric(19, 0)), N'FAIRPLAY NL', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 2, 1, N'Rotterdam')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(220 AS Numeric(19, 0)), N'FAIRPLAY NL', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 3, 1, N'Rotterdam')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(221 AS Numeric(19, 0)), N'FAIRPLAY NL', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 4, 1, N'Rotterdam')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(222 AS Numeric(19, 0)), N'FAIRPLAY O POL', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(223 AS Numeric(19, 0)), N'FAIRPLAY ODDZIA', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(224 AS Numeric(19, 0)), N'FAIRPLAY ODDZIA', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH ', NULL, 3, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(225 AS Numeric(19, 0)), N'FAIRPLAY ODDZIA', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH ', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(226 AS Numeric(19, 0)), N'Fairplay Rostoc', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 1, 1, N'Rostock ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(227 AS Numeric(19, 0)), N'Fairplay Rostoc', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 3, 1, N'Rostock ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(228 AS Numeric(19, 0)), N'Fairplay Rostoc', N'Fairplay Schleppdampfschiffs-Reederei Richard Borchard GmbH', NULL, 4, 1, N'Rostock ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(229 AS Numeric(19, 0)), N'FAIRPLAY ROTTER', N'FAIRPLAY B.V.', NULL, 1, 1, N'ROTTERDAM')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(230 AS Numeric(19, 0)), N'FAIRPLAY ROTTER', N'FAIRPLAY B.V.', NULL, 2, 1, N'ROTTERDAM')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(231 AS Numeric(19, 0)), N'FAIRPLAY ROTTER', N'FAIRPLAY B.V.', NULL, 3, 1, N'ROTTERDAM')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(232 AS Numeric(19, 0)), N'FAIRPLAY ROTTER', N'FAIRPLAY B.V.', NULL, 4, 1, N'ROTTERDAM')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(233 AS Numeric(19, 0)), N'FAIRPLAY SZCZ.', N'Fairplay Polska Sp. z o.o.', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(234 AS Numeric(19, 0)), N'FAIRPLAY SZCZEC', N'Fairplay Towage Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(235 AS Numeric(19, 0)), N'FAIRPLAY SZCZEC', N'Fairplay Towage Sp. z o.o.', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(236 AS Numeric(19, 0)), N'FAIRPLAY SZCZEC', N'Fairplay Towage Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(237 AS Numeric(19, 0)), N'FAIRPLAY Z.O.O', N'Fairplay Polska Spółka z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(238 AS Numeric(19, 0)), N'FAIRPLAY Z.O.O', N'Fairplay Polska Spółka z o.o.', NULL, 3, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(239 AS Numeric(19, 0)), N'FAIRPLAY Z.O.O', N'Fairplay Polska Spółka z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(240 AS Numeric(19, 0)), N'FAIRPLAY1      ', N'SCHLEPPER-ARGE MV', NULL, 2, 1, N'ROSTOCK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(241 AS Numeric(19, 0)), N'fast baltic', N'Fast Baltic sp. z o.o.', NULL, 1, 1, N' Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(242 AS Numeric(19, 0)), N'fast lines', N'Fast Lines Belgium N.V. ', NULL, 1, 1, N' Antwerp / Belgium')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(243 AS Numeric(19, 0)), N'FINN', N'FINNLINES PLC', NULL, 4, 1, N'HELSINKI')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(244 AS Numeric(19, 0)), N'Finomar', N'Finomar  Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(245 AS Numeric(19, 0)), N'First ', N'First Quality Cruises Inc.,', NULL, 4, 1, N'Republic od Panama')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(246 AS Numeric(19, 0)), N'Flinter', N'Flinter Shipping B.V.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(247 AS Numeric(19, 0)), N'FORKOR', N'Forkor Spółka z ograniczoną odpowiedzialnością Sp. k.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(248 AS Numeric(19, 0)), N'FORTUNA', N'FORTUNA SHIPPING APS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(249 AS Numeric(19, 0)), N'FOSS', N'm/v Foss, Lorentzens Rederi AS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(250 AS Numeric(19, 0)), N'Frank Dahl', N'Reederei Frank Dahl m/s "DANIO"', NULL, 1, 1, N'Cuxhaven')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(251 AS Numeric(19, 0)), N'FRUKACZ', N'FRUKACZ CONSULTING ', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(252 AS Numeric(19, 0)), N'FRUKACZ', N'FRUKACZ CONSULTING ', NULL, 2, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(253 AS Numeric(19, 0)), N'FRUKACZ', N'FRUKACZ CONSULTING ', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(254 AS Numeric(19, 0)), N'FUNDACJA', N'FUNDACJA HARCERSTWA', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(255 AS Numeric(19, 0)), N'GAC', N'GAC (Poland) Ltd.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(256 AS Numeric(19, 0)), N'GAC', N'GAC (POLAND) Spółka z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(257 AS Numeric(19, 0)), N'GCT', N'Gdynia Container Terminal', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(258 AS Numeric(19, 0)), N'Gearbulk Norway', N'Gearbulk Norway AS', NULL, 1, 1, N'N-5160 Laksevaeg    ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(259 AS Numeric(19, 0)), N'GEARBULK NORWAY', N'Gearbulk Norway AS', NULL, 4, 1, N'N-5160 Laksevaeg    ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(260 AS Numeric(19, 0)), N'Gec', N'Krzysztof Gec', NULL, 1, 1, N'Dębnica Kaszubska')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(261 AS Numeric(19, 0)), N'Geja C', N'Owner of m/v GEJA C c/o Carisbroke Shipping Ltd', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(262 AS Numeric(19, 0)), N'General ', N'General Shipping Services Ltd.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(263 AS Numeric(19, 0)), N'GERDES', N'Gerdes Shipping & Chartering GmbH & Co KG', NULL, 1, 1, N'D-25404 Pinneberg, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(264 AS Numeric(19, 0)), N'global hestia', N'global hestia', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(265 AS Numeric(19, 0)), N'GLOBAL HESTIA', N'global hestia', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(266 AS Numeric(19, 0)), N'GMINA SZCZECIN', N'GMINA MIASTO SZCZECIN', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(267 AS Numeric(19, 0)), N'GMINA SZCZECIN', N'GMINA MIASTO SZCZECIN', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(268 AS Numeric(19, 0)), N'GMINA SZCZECIN', N'GMINA MIASTO SZCZECIN', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(269 AS Numeric(19, 0)), N'GMINA ŚWINOUJŚC', N'GMINA MIASTO ŚWINOUJŚCIE', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(270 AS Numeric(19, 0)), N'Golden', N'Golden Laurel Maritime INC.Owners of mv Princess Daphne', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(271 AS Numeric(19, 0)), N'Golden000', N'Golden Laurel Maritime Inc.', NULL, 4, 1, N'Panama City,Panama')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(272 AS Numeric(19, 0)), N'Gran Fish', N'Przetwórstwo Rybne Granfish s.c. M.W. i M. Kłos', NULL, 1, 1, N'Ustka')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(273 AS Numeric(19, 0)), N'green charterin', N'Green Chartering AS', NULL, 1, 1, N'Bergen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(274 AS Numeric(19, 0)), N'GREEN CHATERING', N'Green Chartering AS', NULL, 4, 1, N'Bergen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(275 AS Numeric(19, 0)), N'green refers', N'Euro Forwarding and Shipping Agency Sp. z o.o.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(276 AS Numeric(19, 0)), N'GREEN REFERS', N'Green Reefers Polska Sp. z o.o.', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(277 AS Numeric(19, 0)), N'GRIFFIN ODBIORC', N'IMS-GRIFFIN SP. Z O.O. ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(278 AS Numeric(19, 0)), N'GROS STEFAN', N'GROS STEFAN', NULL, 4, 1, N'ŻUKOWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(279 AS Numeric(19, 0)), N'GRUPA LOTOS', N'Grupa LOTOS S.A.', NULL, 1, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(280 AS Numeric(19, 0)), N'GRUPA LOTOS', N'Grupa LOTOS S.A.', NULL, 2, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(281 AS Numeric(19, 0)), N'GRUPA LOTOS', N'Grupa LOTOS S.A.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(282 AS Numeric(19, 0)), N'Gryfia', N'Szczecińska Stocznia Remontowa GRYFIA SA', NULL, 1, 1, N'Szczecin, PL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(283 AS Numeric(19, 0)), N'GRYFIA', N'Szczecińska Stocznia Remontowa GRYFIA SA', NULL, 4, 1, N'Szczecin, PL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(284 AS Numeric(19, 0)), N'GSR', N'Gdańska Stocznia Remontowa im.Józefa Piłsudskiego S.A. ', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(285 AS Numeric(19, 0)), N'HAGEMANN', N'Detlef Hegemann GmbH', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(286 AS Numeric(19, 0)), N'HAGLAND', N'HAGLAND  SAGA', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(287 AS Numeric(19, 0)), N'HAGLAND', N'Hagland Bulk Transport KS ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(288 AS Numeric(19, 0)), N'Hagland bulk', N'Hagland Bulk II KS', NULL, 1, 1, N'Haugesund, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(289 AS Numeric(19, 0)), N'HANJIN', N'Owners of mv "Keoyang Noble"', NULL, 4, 1, N'SEOUL, KOREA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(290 AS Numeric(19, 0)), N'HANJIN DE', N'Hanjin Shipping Co. Ltd.', NULL, 4, 1, N'Hamburg, GERMANY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(291 AS Numeric(19, 0)), N'Hanjin Germany', N'Hanjin Shipping Co. Ltd.', NULL, 1, 1, N'Hamburg, GERMANY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(292 AS Numeric(19, 0)), N'Hanjin Seoul', N'Hanjin Shipping Seoul / Panamax Team', NULL, 1, 1, N'Seoul, KOREA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(293 AS Numeric(19, 0)), N'HANJIN SEOUL', N'Hanjin Shipping Seoul / Panamax Team', NULL, 4, 1, N'Seoul, KOREA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(294 AS Numeric(19, 0)), N'HANSA', N'HANSA SHIPPING LTD, MALTA,', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(295 AS Numeric(19, 0)), N'Hapag-Lloyd', N'Hapag-Lloyd Polska Sp.z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(296 AS Numeric(19, 0)), N'Hapag-Lloyd AG', N'Hapag-Lloyd AG', NULL, 4, 1, N'Hamburg 20095')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(297 AS Numeric(19, 0)), N'HECKSHER', N'HECKSHER POLSKA SP. Z O.O.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(298 AS Numeric(19, 0)), N'HERDZIK JERZY', N'HERDZIK JERZY', NULL, 4, 1, N'81-226 GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(299 AS Numeric(19, 0)), N'HERKULES', N'HERKULES S.A.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(300 AS Numeric(19, 0)), N'HEWELT ZDZISŁAW', N'HEWELT ZDZISŁAW', NULL, 4, 1, N'MRZEZINO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(301 AS Numeric(19, 0)), N'HICOPY ODBIORCA', N'HICOPY SZCZECIN S.C. J. DANKIEWICZ, S. BEREŚ', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(302 AS Numeric(19, 0)), N'HINCKE ANDRZEJ', N'HINCKE ANDRZEJ', NULL, 4, 1, N'WEJHEROWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(303 AS Numeric(19, 0)), N'HOOYERPOOL', N'HOOYERPOOL  AS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(304 AS Numeric(19, 0)), N'HS Schiffahrts ', N'HS Schiffahrts GmbH & Co. KG MS "MARIA SCHEPERS",', NULL, 4, 1, N'Haren,  Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(305 AS Numeric(19, 0)), N'HS SHIFFAHRTS', N'HS  SHIFFAHRTS GmbH & Co. KG MS "Jan"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(306 AS Numeric(19, 0)), N'Hullkon', N'HULLKON', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(307 AS Numeric(19, 0)), N'HULLKON', N'HULLKON', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(308 AS Numeric(19, 0)), N'Hydrobudowa', N'Hydrobudowa Gdańsk S.A.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(309 AS Numeric(19, 0)), N'ICE CONDOR', N'Ice Condor Inc.', NULL, 1, 1, N'Glyfada 16674, Greece')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(310 AS Numeric(19, 0)), N'ILE DE ISCHIA', N'MV "Onego Ponza" MS "Ile de Ischia" GmbH & Co. KG', NULL, 1, 1, N'St-Nr.: 43/220/39153')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(311 AS Numeric(19, 0)), N'IMS-GRIFFIN', N'IMS-GRIFFIN SP. Z O.O.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(312 AS Numeric(19, 0)), N'INCHCAPE', N'INCHCAPE SHIPPING SERVICES POLAND', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(313 AS Numeric(19, 0)), N'INTER', N'INTER BALT SP. Z O.O', NULL, 4, 1, N'GDAŃSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(314 AS Numeric(19, 0)), N'INTER FREIGHT', N'Oceania Cruises', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(315 AS Numeric(19, 0)), N'INTER FREIGHT1', N'Inter Freight  Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(316 AS Numeric(19, 0)), N'INTER MARINE', N'Inter Marine Spółka z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(317 AS Numeric(19, 0)), N'Interagent', N'Interagent Spółka z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(318 AS Numeric(19, 0)), N'Interbalt', N'Inter Balt Sp. z o.o.', NULL, 1, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(319 AS Numeric(19, 0)), N'Interfracht', N'Interfracht Gmbh,Bergiusstrasse 1,D-28816,Stuhr/Bremen', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(320 AS Numeric(19, 0)), N'Intermare', N'Intermare Transport GmbH', NULL, 4, 1, N' Hamburg,Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(321 AS Numeric(19, 0)), N'INTERNATIONAL  ', N'INTERNATIONAL SHIPBROKERS LIMITED', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(322 AS Numeric(19, 0)), N'INTERORIENT', N'INTERORIENT MARINE SERVICES LTD ', NULL, 1, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(323 AS Numeric(19, 0)), N'INTERORIENT', N'INTERORIENT MARINE SERVICES LTD ', NULL, 2, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(324 AS Numeric(19, 0)), N'INTERORIENT', N'INTERORIENT MARINE SERVICES LTD ', NULL, 4, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(325 AS Numeric(19, 0)), N'IRIS 1', N'CAPTAIN/OWNERS: MV IRIS 1', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(326 AS Numeric(19, 0)), N'ISS', N'Owner of m/f HAMBURG c/o  ', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(327 AS Numeric(19, 0)), N'ISTANBUL', N'I"STANBUL DENIZCILIK GEMI INSA SAN TIC A.S."', NULL, 4, 1, N'ISTANBUL,TURKEY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(328 AS Numeric(19, 0)), N'J/S North', N'J/S North-Western Shipping Company', NULL, 4, 1, N'St.Petersburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(329 AS Numeric(19, 0)), N'JAKUBIŃSKI', N'JAKUBIŃSKI PIOTR', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(330 AS Numeric(19, 0)), N'Jan de nul', N'JAN DE NUL NV', NULL, 1, 1, N'HOFSTADE- AALST , BELGIUM')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(331 AS Numeric(19, 0)), N'Jan Miotke', N'Skup Metali Nieżelaznych  Jan Miotke', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(332 AS Numeric(19, 0)), N'JAN STEPNIEWSKI', N'JAN STEPNIEWSKI I S-KA ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(333 AS Numeric(19, 0)), N'JAN STĘPNIEWSKI', N'JAN STĘPNIEWSKI I SKA Spółka z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(334 AS Numeric(19, 0)), N'JANAR', N'JANAR Baran Jan, Brudnicki Artur Sp. Jawna', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(335 AS Numeric(19, 0)), N'JANAR', N'JANAR Baran Jan, Brudnicki Artur Sp. Jawna', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(336 AS Numeric(19, 0)), N'JANAR', N'JANAR Baran Jan, Brudnicki Artur Sp. Jawna', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(337 AS Numeric(19, 0)), N'JANIK', N'JANIK BEATA', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(338 AS Numeric(19, 0)), N'JĘDRUSIAK', N'JĘDRUSIAK JAROSŁAW', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(339 AS Numeric(19, 0)), N'JOINT STOCK ', N'JOINT STOCK COMPANY "NORTHERN SHIPPING COMPANY"', NULL, 4, 1, N'ARKHANGELSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(340 AS Numeric(19, 0)), N'JS', N'JS"North-Western Shipping Company"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(341 AS Numeric(19, 0)), N'JS North-west', N'JS "NORTH-WESTERN SHIPPING COMPANY"', NULL, 1, 1, N'ST. Petersburg, Russia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(342 AS Numeric(19, 0)), N'JSC', N'JSC "Northern Shipping Company"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(343 AS Numeric(19, 0)), N'JSC', N'JSC “Northern Shipping Company”', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(344 AS Numeric(19, 0)), N'JUL TRUCK', N'Jul-Truck Żaneta Bukowska', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(345 AS Numeric(19, 0)), N'JUL TRUCK', N'Jul-Truck Żaneta Bukowska', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(346 AS Numeric(19, 0)), N'JUL TRUCK', N'Jul-Truck Żaneta Bukowska', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(347 AS Numeric(19, 0)), N'JUPITER', N'JUPITER SHIPPING SERVICES SPÓŁKA Z O.O.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(348 AS Numeric(19, 0)), N'KABAŁA', N'KABAŁA DARIUSZ', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(349 AS Numeric(19, 0)), N'KACPRZAK JERZY', N'KACPRZAK JERZY', NULL, 4, 1, N'84-200 WEJHEROWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(350 AS Numeric(19, 0)), N'KAMIŃSKI', N'KAMIŃSKI JERZY', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(351 AS Numeric(19, 0)), N'KARAŚ', N'KARAŚ ŁUKASZ', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(352 AS Numeric(19, 0)), N'Karl Geuther', N'Karl Geuther Gmbh,Martinistrasse 58,D-28195 Bremen', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(353 AS Numeric(19, 0)), N'KARSZNIA', N'KARSZNIA RYSZARD', NULL, 4, 1, N'DOMATOWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(354 AS Numeric(19, 0)), N'KARYSTOS', N'KARYSTOS SHIPPING SA TRUST COMPANY COMPLEX', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(355 AS Numeric(19, 0)), N'KAS', N'KAS TANKER Co. Ltd.', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(356 AS Numeric(19, 0)), N'KASTOR', N'MS"Kastor" Schiffahrts GmbH & Co.KG ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(357 AS Numeric(19, 0)), N'K-cCHARTERING', N'K-cCHARTERING AB', NULL, 1, 1, N'SIGTUNA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(358 AS Numeric(19, 0)), N'Kent Line ', N'Kent Line International Ltd.', NULL, 4, 1, N'Saint John,New Brunswick')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(359 AS Numeric(19, 0)), N'KEYTRADE', N'Keytrade Polska Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(360 AS Numeric(19, 0)), N'Keytrade', N'Keytrade Polska Sp. z o.o.', NULL, 1, 1, N'Szczecin, POLAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(361 AS Numeric(19, 0)), N'KĘDZIERSKI', N'KĘDZIERSKI KRZYSZTOF', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(362 AS Numeric(19, 0)), N'KIL', N'KIL Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(363 AS Numeric(19, 0)), N'KIL', N'KIL Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(364 AS Numeric(19, 0)), N'KONKEL', N'KONKEL JAN', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(365 AS Numeric(19, 0)), N'KONSIDA', N'KONSIDA Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(366 AS Numeric(19, 0)), N'KONSORCJUM MPB', N'KONSORCJUM MPB JETTY SC', NULL, 1, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(367 AS Numeric(19, 0)), N'KONSORCJUM MPB', N'KONSORCJUM MPB JETTY SC', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(368 AS Numeric(19, 0)), N'Konstal', N'Stocznia Konstal Sp. J. ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(369 AS Numeric(19, 0)), N'Konstal', N'Stocznia Konstal Sp. J. ', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(370 AS Numeric(19, 0)), N'Konstal', N'Stocznia Konstal Sp. J. ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(371 AS Numeric(19, 0)), N'KONTRAHENT', N'Kontrahent', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(372 AS Numeric(19, 0)), N'KONTRAHENT', N'Kontrahent', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(373 AS Numeric(19, 0)), N'KONTRAHENT', N'Kontrahent', NULL, 3, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(374 AS Numeric(19, 0)), N'KONTRAHENT', N'KONTRAHENT (systemowy SIMPLE, NIE USUWAĆ!)', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(375 AS Numeric(19, 0)), N'koppers', N'Koppers Denmark A/S', NULL, 1, 1, N'Nyborg, DENMARK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(376 AS Numeric(19, 0)), N'KOPPERS', N'Koppers Denmark A/S', NULL, 4, 1, N'Nyborg, DENMARK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(377 AS Numeric(19, 0)), N'KOPPERS B.V.', N'KOPPERS INTERNATIONAL B.V.', NULL, 1, 1, N'5800 NYBORG                   ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(378 AS Numeric(19, 0)), N'KOSIOR', N'KOSIOR SHIPPING COMPANY', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(379 AS Numeric(19, 0)), N'kosior', N'Kosior Shipping Company Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(380 AS Numeric(19, 0)), N'KROGSTADT', N' Owners / Operators of mt LADY HILDE', NULL, 4, 1, N'Oslo, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(381 AS Numeric(19, 0)), N'Krogstadt', N' Owners / Operators of mv ELLEN KNUTSEN ', NULL, 1, 1, N'Oslo, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(382 AS Numeric(19, 0)), N'Krono chem', N'Krono-Chem Sp. z o.o.', NULL, 1, 1, N'Szczecinek')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(383 AS Numeric(19, 0)), N'KRUZA', N'AUTO-LUK', NULL, 4, 1, N'Szemud')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(384 AS Numeric(19, 0)), N'Kubanek', N'Artur Kubanek', NULL, 1, 1, N'Świonoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(385 AS Numeric(19, 0)), N'KUPCZAK', N'KUPCZAK RYSZARD', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(386 AS Numeric(19, 0)), N'Lauritzen', N'Lauritzen Bulkers A/S', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(387 AS Numeric(19, 0)), N'Lavinia', N'Owners m/v LAVINIA",Lupin Shipping Ltd.,St.Vincent', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(388 AS Numeric(19, 0)), N'LAVINIA', N'Owners mv "LAVINIA"', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(389 AS Numeric(19, 0)), N'LEHMANN', N'MS Heike Lehmann Schifffahrts GmbH & Co. KG', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(390 AS Numeric(19, 0)), N'LEMAŃSKI', N'Marek Lemański', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(391 AS Numeric(19, 0)), N'LEMAŃSKI', N'Marek Lemański', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(392 AS Numeric(19, 0)), N'LEMAŃSKI', N'Marek Lemański', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(393 AS Numeric(19, 0)), N'LENE D', N'Captain/owners m/v "Lene D"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(394 AS Numeric(19, 0)), N'LESIAK', N'LESIAK ADAM', NULL, 4, 1, N'Nowy Dwór Gdański')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(395 AS Numeric(19, 0)), N'LESZKA', N'LESZKA CZESLAW . ', NULL, 4, 1, N'810-178 GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(396 AS Numeric(19, 0)), N'LIAN', N'Owners on m/v LIAN,Lupin Ltd.St.Vincent', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(397 AS Numeric(19, 0)), N'LILIANA', N'LILIANA SHIPPING LTD.,ST. VINCENT&GRENADINES', NULL, 4, 1, N'koege')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(398 AS Numeric(19, 0)), N'LIND bretton', N'BRETTON RIDGE SHIPPING CPOMNPANY', NULL, 1, 1, N'FREDRIKSTAD NORWAY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(399 AS Numeric(19, 0)), N'LITWIN', N'LITWIN KAZIMIERZ', NULL, 4, 1, N'KARTUZY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(400 AS Numeric(19, 0)), N'LJL', N'Akcine Bendrove "Lietuvos Juru Laivininkyste" ', NULL, 2, 1, N'Klaipeda, Lithuania')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(401 AS Numeric(19, 0)), N'LJL', N'Akcine Bendrove "Lietuvos Juru Laivininkyste" ', NULL, 4, 1, N'Klaipeda, Lithuania')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(402 AS Numeric(19, 0)), N'lorian shipping', N'Lorain Shipping S.A. - Panama', NULL, 1, 1, N'Lugano, Switzerland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(403 AS Numeric(19, 0)), N'LOTOS ASFALT', N'LOTOS ASFALT SP. Z O.O.', NULL, 1, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(404 AS Numeric(19, 0)), N'LOTOS ASFALT', N'LOTOS ASFALT SP. Z O.O.', NULL, 2, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(405 AS Numeric(19, 0)), N'LOTOS ASFALT', N'LOTOS ASFALT SP. Z O.O.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(406 AS Numeric(19, 0)), N'LSC', N'Lithauanian Shipping Company LTD', NULL, 1, 1, N'Klaipeda, Lithuania')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(407 AS Numeric(19, 0)), N'Lubecker', N'Capitan / Owners m/v Lene D c/o Lubecker Schiffahrtsges.mbH', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(408 AS Numeric(19, 0)), N'LYULIN', N'LYULIN MARITIME LTD. ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(409 AS Numeric(19, 0)), N'm/v "TINA"', N'M/V "TINA" Tina Shipping BV Daltonstraat 33', NULL, 4, 1, N'DK-5700 Svendborg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(410 AS Numeric(19, 0)), N'm/v Federal ', N'm/v Federal Severn, " Oldendorff Luebeck"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(411 AS Numeric(19, 0)), N'm/v Lona', N'Owners of m/v LONA  Lupin Shipping Ltd.,St.Vincent', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(412 AS Numeric(19, 0)), N'm/v Uphusen', N'm/v Uphusen', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(413 AS Numeric(19, 0)), N'MAC', N'MACANDREWS & CO.LTD. ', NULL, 4, 1, N'London EC4N  7BE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(414 AS Numeric(19, 0)), N'MAERSK BROKER', N'MAERSK BROKER AGENCY', NULL, 4, 1, N'Copenhagen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(415 AS Numeric(19, 0)), N'maersk broker', N'Maersk Broker Agency ', NULL, 1, 1, N'Copenhagen, Denmark  ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(416 AS Numeric(19, 0)), N'MAG', N'Master / Owner of m/v City Of Barcelona', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(417 AS Numeric(19, 0)), N'MAG', N'Morska Agencja Gdynia Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(418 AS Numeric(19, 0)), N'MAGEMAR', N'Magemar Polska Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(419 AS Numeric(19, 0)), N'MAKLER MG', N'MIROSŁAW GÓRNY Makler Żeglugowy', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(420 AS Numeric(19, 0)), N'MAKLER MG', N'MIROSŁAW GÓRNY Makler Żeglugowy', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(421 AS Numeric(19, 0)), N'MAKLER MG', N'MIROSŁAW GÓRNY Makler Żeglugowy', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(422 AS Numeric(19, 0)), N'MAKLER ŻEGLUGOW', N'MIROSŁAW WIATER Makler Żeglugowy', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(423 AS Numeric(19, 0)), N'MAKLER ŻEGLUGOW', N'MIROSŁAW WIATER Makler Żeglugowy', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(424 AS Numeric(19, 0)), N'MAKLER ŻEGLUGOW', N'MIROSŁAW WIATER Makler Żeglugowy', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(425 AS Numeric(19, 0)), N'MAKLERZY MWMG', N'MAKLERZY ŻEGLUGOWI M.Wiater, M. Górny', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(426 AS Numeric(19, 0)), N'MAKLERZY MWMG', N'MAKLERZY ŻEGLUGOWI M.Wiater, M. Górny', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(427 AS Numeric(19, 0)), N'MAKLERZY MWMG', N'MAKLERZY ŻEGLUGOWI M.Wiater, M. Górny', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(428 AS Numeric(19, 0)), N'MAKOWSKI', N'MAKOWSKI JACEK', NULL, 4, 1, N'GDAŃSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(429 AS Numeric(19, 0)), N'MANN LINES', N'Mann Lines Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(430 AS Numeric(19, 0)), N'MAR SERVICE', N'MAR-SERVICE Marcin Biedrzycki', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(431 AS Numeric(19, 0)), N'MAR SERVICE', N'MAR-SERVICE Marcin Biedrzycki', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(432 AS Numeric(19, 0)), N'Marine crane', N'Marine Crane AB', NULL, 1, 1, N'Pitea, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(433 AS Numeric(19, 0)), N'MARINE PROJECTS', N'Marine Projects Ltd. Spółka z o.o.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(434 AS Numeric(19, 0)), N'MARITIM', N'MARITIM-SHIPYARD ', NULL, 4, 1, N'GDAŃSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(435 AS Numeric(19, 0)), N'MARLENE', N'MV Marlene C/O', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(436 AS Numeric(19, 0)), N'MARSZEWSKI', N'MARSZEWSKI WOJCIECH', NULL, 4, 1, N'GDANSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(437 AS Numeric(19, 0)), N'MARTHA', N'Captain owners m/v "MARTHA"', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(438 AS Numeric(19, 0)), N'MAR-YARD', N'MAR-YARD LTD. SPÓŁKA Z O.O.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(439 AS Numeric(19, 0)), N'MAS', N'Morska Agencja Szczecin Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(440 AS Numeric(19, 0)), N'MAS', N'Morska Agencja Szczecin Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(441 AS Numeric(19, 0)), N'Mate Maritime', N'Mate Maritime Agency-Agencja Żeglugowa ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(442 AS Numeric(19, 0)), N'Meerpahl', N'MEERPAHL & MEYER (GmbH) Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(443 AS Numeric(19, 0)), N'Meerphal ', N'Meerphal & Meyer GmbH Sp. z o.o. Oddział w Polsce', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(444 AS Numeric(19, 0)), N'MICHALSKA', N'Krystyna Michalska-Brodzik', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(445 AS Numeric(19, 0)), N'MICHALSKA', N'Krystyna Michalska-Brodzik', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(446 AS Numeric(19, 0)), N'MICHALSKA', N'Krystyna Michalska-Brodzik', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(447 AS Numeric(19, 0)), N'milestone', N'Milestone Maritime A/S', NULL, 1, 1, N'Holbaek, Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(448 AS Numeric(19, 0)), N'MILESTONE', N'Milestone Maritime A/S', NULL, 4, 1, N'Holbaek, Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(449 AS Numeric(19, 0)), N'Miliana', N'Miliana Shipmanagement Limited Diagoras House, 7th floor ', NULL, 4, 1, N'Nicosia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(450 AS Numeric(19, 0)), N'Mirand', N'Mirand Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(451 AS Numeric(19, 0)), N'MIRIAM BV', N'CS Miriam BV', NULL, 1, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(452 AS Numeric(19, 0)), N'MIRIAM BV', N'CS Miriam BV', NULL, 2, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(453 AS Numeric(19, 0)), N'MIRIAM BV', N'CS Miriam BV', NULL, 4, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(454 AS Numeric(19, 0)), N'misje bulk', N'M/V ANMI ', NULL, 1, 1, N'5817 Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(455 AS Numeric(19, 0)), N'MISJE BULK', N'Misje Bulk A/S', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(456 AS Numeric(19, 0)), N'Mitsui  OSK', N'Mitsui OSK Bulk Shipping ( Europe ) Ltd ', NULL, 4, 1, N'London EC3N 4JR')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(457 AS Numeric(19, 0)), N'MLB DENMARK', N'M/V DAN FIGHTER', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(458 AS Numeric(19, 0)), N'MOBIUS 2', N'JOSEF MOBIUS BAU - GmbH', NULL, 1, 1, N'HAMBURG')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(459 AS Numeric(19, 0)), N'MOBIUS 2', N'JOSEF MOBIUS BAU-AKTIENGESELLSCHAFT', NULL, 4, 1, N'HAMBURG')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(460 AS Numeric(19, 0)), N'mobius de', N'JOSEF MOBIUS BAU GmbH', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(461 AS Numeric(19, 0)), N'MOBIUS DE', N'JOSEF MOBIUS BAU-AKTIENGESELLSCHAFT', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(462 AS Numeric(19, 0)), N'MOEBIUS', N'Moebius - Bau Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(463 AS Numeric(19, 0)), N'MOEBIUS', N'Moebius - Bau Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(464 AS Numeric(19, 0)), N'MOEBIUS', N'MOEBIUS-BAU SP. Z O.O.', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(465 AS Numeric(19, 0)), N'MOLAT', N'OWNER OF M/V MOLAT C/O', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(466 AS Numeric(19, 0)), N'MONTOSTAL', N'MONTOSTAL Sp. z o.o.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(467 AS Numeric(19, 0)), N'MOSTOSTAL', N'Mostostal  Warszawa S.A.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(468 AS Numeric(19, 0)), N'motoarmia', N'Moto-Armia SC Jakub Siemieński, Marcin Lenarciak ', NULL, 1, 1, N'Wałcz')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(469 AS Numeric(19, 0)), N'MOWINCEL', N'Mowinckel Ship Management AS', NULL, 4, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(470 AS Numeric(19, 0)), N'Mowinckel', N'Mowinckel Ship Management AS', NULL, 1, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(471 AS Numeric(19, 0)), N'MP BOLAGEN', N'MP Bolagen Skogsindustri AB', NULL, 1, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(472 AS Numeric(19, 0)), N'Mrc Emirhan', N'Mrc Emirhan Shipping Corp.Trust Company Complex,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(473 AS Numeric(19, 0)), N'MS Margrete C', N'MS Margrete C', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(474 AS Numeric(19, 0)), N'MS PERU', N'MS PERU River Liner GmbH& Co. KG', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(475 AS Numeric(19, 0)), N'MSC', N'MSC POLAND SP. Z O.O', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(476 AS Numeric(19, 0)), N'MSC / intermari', N'MSC CROCIERE S.A. ', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(477 AS Numeric(19, 0)), N'MSC CROCIERE', N'MSC CROCIERE S.A.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(478 AS Numeric(19, 0)), N'msr', N'Morska Stocznia Remontowa S.A.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(479 AS Numeric(19, 0)), N'MSR', N'Morska Stocznia Remontowa S.A.', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(480 AS Numeric(19, 0)), N'MT Baltic Sun', N'MT Baltic Sun ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(481 AS Numeric(19, 0)), N'MTMG', N'MTMG-MORSKI TERMINAL MASOWY GDYNIA', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(482 AS Numeric(19, 0)), N'MTS', N'MTS Morskie Techniczne Serwisy Sp. z o.o.', NULL, 1, 1, N'Szczecin, Poland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(483 AS Numeric(19, 0)), N'MUR shipping', N'MUR SHIPPING B.V.', NULL, 1, 1, N'RUNGSTED KYST, DENMARK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(484 AS Numeric(19, 0)), N'Murmansk', N'Murmansk Shipping Company', NULL, 1, 1, N'Murmansk, RUSSIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(485 AS Numeric(19, 0)), N'MURMANSK SHIPP', N'Murmansk Shipping Company', NULL, 4, 1, N'Murmansk, RUSSIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(486 AS Numeric(19, 0)), N'National Oilwel', N'National Oilwell Varco Molde', NULL, 1, 1, N'???')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(487 AS Numeric(19, 0)), N'NAUTA', N'STOCZNIA REMONTOWA NAUTA   S.A.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(488 AS Numeric(19, 0)), N'NAUTA HULL', N'NAUTA HULL SP. Z O.O', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(489 AS Numeric(19, 0)), N'NAUTICA', N'Prestige Cruise Holdings Inc.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(490 AS Numeric(19, 0)), N'navalis', N'NAVALIS SHIPPINGGmbH & CO KG', NULL, 1, 1, N'Oldendorf, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(491 AS Numeric(19, 0)), N'navenna', N'RUBICONE DI NAVIGAZIONE srl ', NULL, 1, 1, N'Ravenna , ITALY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(492 AS Numeric(19, 0)), N'NAVENNA', N'RUBICONE DI NAVIGAZIONE srl ', NULL, 4, 1, N'Ravenna , ITALY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(493 AS Numeric(19, 0)), N'Navibul pers ve', N'Veliko Tirnovo Shipping LTD, 60 South Str., Valletta, MALTA', NULL, 1, 1, N'VARNA, BULGARIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(494 AS Numeric(19, 0)), N'Navibulgar', N'PERSENK Maritime LTD, 60 South Street, Valetta, Malta', NULL, 1, 1, N'9000 Varna')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(495 AS Numeric(19, 0)), N'NAVIBULGAR', N'PLOVDIV MARITIME LTD MARSHALL ISLANDS c/o NAVIBULGAR', NULL, 4, 1, N'AJELTAKE ISLAND, MAJURO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(496 AS Numeric(19, 0)), N'Navibulgar Doll', N'Dolly Maritime LTD c/o NAVIBULGAR', NULL, 1, 1, N'Valletta, MALTA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(497 AS Numeric(19, 0)), N'NAVIBULGAR DOLL', N'Dolly Maritime LTD c/o NAVIBULGAR', NULL, 4, 1, N'Valletta, MALTA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(498 AS Numeric(19, 0)), N'Navibulgar plov', N'PLOVDIV MARITIME LTD MARSHALL ISLANDS c/o NAVIBULGAR', NULL, 1, 1, N'AJELTAKE ISLAND, MAJURO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(499 AS Numeric(19, 0)), N'Naviera Viscain', N'Naviera Vizcaina, S.A.', NULL, 1, 1, N'Madrid, SPAIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(500 AS Numeric(19, 0)), N'NAVIERA VIZCAIN', N'Naviera Vizcaina, S.A.', NULL, 4, 1, N'Madrid, SPAIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(501 AS Numeric(19, 0)), N'NAVIGA', N'NAVIGA KRZYSZTOF MAKOWSKI', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(502 AS Numeric(19, 0)), N'NAVIGAR', N'NAVIGAR Deneko-Garbień Sp.J', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(503 AS Numeric(19, 0)), N'NAVIGAR', N'NAVIGAR Deneko-Garbień Sp.J', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(504 AS Numeric(19, 0)), N'Naviglobe NV', N'Naviglobe NV', NULL, 1, 1, N'Antwerpen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(505 AS Numeric(19, 0)), N'navikon', N'Navikon Sry Sp. z o.o.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(506 AS Numeric(19, 0)), N'navikon', N'Navikon Sry Sp. z o.o.', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(507 AS Numeric(19, 0)), N'NAVIKON-STAL', N'NAVIKON STAL Sp. z o o', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(508 AS Numeric(19, 0)), N'Navimor', N'Navimor International  Com  Sp. z o.o.', NULL, 4, 1, N'Sopot')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(509 AS Numeric(19, 0)), N'Navitech', N'Navitech', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(510 AS Numeric(19, 0)), N'NAWIGATOR', N'NAWIGATOR SHIPPING Spółka z o.o.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(511 AS Numeric(19, 0)), N'NAWISHIP ', N'NAWISHIP Jan Wiciński', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(512 AS Numeric(19, 0)), N'Nednor', N'NEDNOR BV', NULL, 1, 1, N'AH GRONINGEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(513 AS Numeric(19, 0)), N'NEDNOR  ', N'NEDNOR BV', NULL, 4, 1, N'GRONINGEN/THE NETHERLANDS')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(514 AS Numeric(19, 0)), N'NEPTUNE AG', N'Neptune Agency Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(515 AS Numeric(19, 0)), N'Neptune age', N'Neptune Agency Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(516 AS Numeric(19, 0)), N'Neste', N'Neste Shipping Oy', NULL, 1, 1, N'Neste Oil, Finland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(517 AS Numeric(19, 0)), N'nimmrich', N'Nimmrich & Prahm Reederei GmbH & Co. KG', NULL, 1, 1, N'LEER, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(518 AS Numeric(19, 0)), N'NIMMRICH', N'Nimmrich & Prahm Reederei GmbH & Co. KG', NULL, 4, 1, N'LEER, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(519 AS Numeric(19, 0)), N'Niw shipping', N'NIW Shipping ', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(520 AS Numeric(19, 0)), N'Noble', N'NOBLE CHARTERING INC BVI', NULL, 1, 1, N'TORTOLA,BRITISH VIRGIN ISLANDS')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(521 AS Numeric(19, 0)), N'NORBROKER', N'NORBROKER SHIPPING & TRADING A/S', NULL, 4, 1, N'Flekkefjord,Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(522 AS Numeric(19, 0)), N'Norddeutsche', N'Norddeutsche Seekabelwerke GmbH', NULL, 1, 1, N'26954 Nordenham')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(523 AS Numeric(19, 0)), N'NORDKALK', N'Owner of m/v Ostanhav ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(524 AS Numeric(19, 0)), N'north west', N'NORTH WESTERN SHIPPING COMPANY', NULL, 1, 1, N' St. Petersburg, Russia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(525 AS Numeric(19, 0)), N'NORTHERN', N'NORTHERN MARINE MANAGEMENT LIMITED ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(526 AS Numeric(19, 0)), N'Norwind', N'Owners of Norwind ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(527 AS Numeric(19, 0)), N'nss', N'NSS Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(528 AS Numeric(19, 0)), N'NYNAS', N'NYNAS Sp. z o.o.', NULL, 1, 1, N'Szczecin, Poland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(529 AS Numeric(19, 0)), N'ODRA-REJS', N'ODRA-REJS S.C. export-import', NULL, 1, 1, N'Gryfów Śl.')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(530 AS Numeric(19, 0)), N'ODRA-REJS', N'ODRA-REJS S.C. export-import', NULL, 4, 1, N'Gryfów Śl.')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(531 AS Numeric(19, 0)), N'odratrans', N'Odratrans S.A.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(532 AS Numeric(19, 0)), N'ODRATRANS', N'Odratrans S.A.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(533 AS Numeric(19, 0)), N'OILER', N'OILER S.A.', NULL, 1, 1, N'Tczew')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(534 AS Numeric(19, 0)), N'OILER', N'OILER S.A.', NULL, 2, 1, N'Tczew')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(535 AS Numeric(19, 0)), N'OILER', N'OILER Sp. z o.o.', NULL, 4, 1, N'Tczew')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(536 AS Numeric(19, 0)), N'Okarmus', N'Okarmus Janusz', NULL, 4, 1, N'Bojano')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(537 AS Numeric(19, 0)), N'OKMARIT ', N'Okmarit Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(538 AS Numeric(19, 0)), N'OKMARIT ', N'Okmarit Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(539 AS Numeric(19, 0)), N'oktan - sinbad', N'Sinbad Chartering Company AB Flottbrovaegen 27', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(540 AS Numeric(19, 0)), N'Oktan Energy', N'OKTAN ENERGY & V/L SERVICE SP. Z O.O. ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(541 AS Numeric(19, 0)), N'OLD SEAS', N'OLD SEAS  SPÓŁKA   Z  O.O.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(542 AS Numeric(19, 0)), N'OLDENDORFF', N'OLDENDORFF Carriers GmbH & Co KG', NULL, 1, 1, N'Lübeck')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(543 AS Numeric(19, 0)), N'OLIMPIC', N'P.U.H. " Olimpic " Spółka z o.o.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(544 AS Numeric(19, 0)), N'ONEGO SHIPPING', N'ONEGO SHIPPING & CHARTERING B.V.', NULL, 1, 1, N'SPUI 24')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(545 AS Numeric(19, 0)), N'ONEGO SHIPPING', N'ONEGO SHIPPING & CHARTERING B.V.', NULL, 4, 1, N'SPUI 24')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(546 AS Numeric(19, 0)), N'ONEGO SHIPPING', N'ONEGO SHIPPING CHARTERING B.V.', NULL, 2, 1, N'SPUI 24')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(547 AS Numeric(19, 0)), N'OOCL', N'OOCL /EUROPE/  LIMITED.', NULL, 4, 1, N'SUFFOLK/LEVINGTON')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(548 AS Numeric(19, 0)), N'OOCL (Europe)', N'OOCL (Europe) Limited, ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(549 AS Numeric(19, 0)), N'OP SVENSSON', N'OP SVENSSON SHIPPING AB', NULL, 4, 1, N'Gothenburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(550 AS Numeric(19, 0)), N'orient', N'ORIENT SHIPPING ROTTERDAM B.V.', NULL, 1, 1, N'VA CAPELLE A/D IJSSEL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(551 AS Numeric(19, 0)), N'Orient', N'Orient Shipping Rotterdam BV', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(552 AS Numeric(19, 0)), N'ORLEN GAZ', N'Orlen Gaz Sp. z o.o.', NULL, 4, 1, N'Płock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(553 AS Numeric(19, 0)), N'Orlen GAZZ', N'Orlen Gaz Sp. z o.o.', NULL, 1, 1, N'Płock')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(554 AS Numeric(19, 0)), N'osterstroms', N'ÖSTERSTRÖMS Spolka z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(555 AS Numeric(19, 0)), N'osterstroms SE', N'Rederi AB TransAtlantic /Ősterströms', NULL, 1, 1, N'Göteborg, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(556 AS Numeric(19, 0)), N'OSZZGM', N'Ośrodek Szkolenia Zawodowego Gospodarki Morskiej', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(557 AS Numeric(19, 0)), N'otto a muller', N'M/V ALWIS', NULL, 1, 1, N'Hamburg, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(558 AS Numeric(19, 0)), N'OTTO A MULLER', N'M/V ALWIS', NULL, 4, 1, N'Hamburg, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(559 AS Numeric(19, 0)), N'OTTO WULF', N'Otto Wulf GmbH & Co. KG', NULL, 1, 1, N'Alter Hafen Nord 210')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(560 AS Numeric(19, 0)), N'OTTO WULF', N'Otto Wulf GmbH & Co. KG', NULL, 4, 1, N'Alter Hafen Nord 210')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(561 AS Numeric(19, 0)), N'OW TANKERS', N'O.W. TANKERS A/S', NULL, 1, 1, N'9400 Noerresundby, Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(562 AS Numeric(19, 0)), N'OW Tankers', N'O.W. TANKERS A/S ', NULL, 1, 1, N'Noerresundby')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(563 AS Numeric(19, 0)), N'OY KRAFTLINE', N'Oy Kraftline  Ab', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(564 AS Numeric(19, 0)), N'P.P.O  Holm', N'P.P.O  Holm  Construction  LTD Sp. z o.o.', NULL, 4, 1, N'Gdańsk ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(565 AS Numeric(19, 0)), N'Pacific Basin', N'PACIFIC BASIN SHIPPING (HK) LIMITED', NULL, 1, 1, N'10, HARCOURT RD,')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(566 AS Numeric(19, 0)), N'PACIFIC BASIN', N'PACIFIC BASIN SHIPPING (HK) LIMITED', NULL, 4, 1, N'10, HARCOURT RD,')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(567 AS Numeric(19, 0)), N'Pacific carrier', N'PCL SHIPPING PTE LTD', NULL, 1, 1, N' Great World City')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(568 AS Numeric(19, 0)), N'PACIFIC CARRIER', N'PCL SHIPPING PTE LTD', NULL, 4, 1, N' Great World City')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(569 AS Numeric(19, 0)), N'PALMALI', N'PALMALI SHIPPING SA', NULL, 1, 1, N'ISLAND OF NEVIS, WEST INDIES')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(570 AS Numeric(19, 0)), N'PARTNER STOCZNI', N'Partner Stocznia Sp. z o.o.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(571 AS Numeric(19, 0)), N'PARTNER STOCZNI', N'Partner Stocznia Sp. z o.o.', NULL, 1, 1, N'Police')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(572 AS Numeric(19, 0)), N'PEAK', N'Peak Shipping AS Litleaasveien 49,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(573 AS Numeric(19, 0)), N'Peak Shipping', N'PEAK SHIPPING AS', NULL, 1, 1, N'Nyborg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(574 AS Numeric(19, 0)), N'PERLE', N'MS "PERLE" River Liner GmbH & Co. KG', NULL, 4, 1, N'Haren')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(575 AS Numeric(19, 0)), N'PJM', N'PJM Shipping and Trading ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(576 AS Numeric(19, 0)), N'PJM', N'PJM Shipping and Trading', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(577 AS Numeric(19, 0)), N'PK dry cargo', N'PK DRYCARGO SRL', NULL, 1, 1, N'RAVENNA (ITALY)')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(578 AS Numeric(19, 0)), N'PLCC', N'Port Logics Cargo and Container Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(579 AS Numeric(19, 0)), N'PLF SHIPP', N'mt FM PROSPERITY', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(580 AS Numeric(19, 0)), N'PLF SHipp', N'Owners MEGA MOTTI', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(581 AS Numeric(19, 0)), N'POA1', N'POL-AGENT SPÓŁKA Z O.O.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(582 AS Numeric(19, 0)), N'POL FIN', N'POL- FIN Shipping Co. Ltd', NULL, 1, 1, N'Limassol, Cyprus')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(583 AS Numeric(19, 0)), N'Polagent', N'Master / Owners of m/v ZILLERTAL', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(584 AS Numeric(19, 0)), N'POLBROK', N'POLBROK Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(585 AS Numeric(19, 0)), N'Polfracht', N'Polfracht Agencja Żeglugowa Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(586 AS Numeric(19, 0)), N'POLFRACHT', N'Polfracht Agencja Żeglugowa Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(587 AS Numeric(19, 0)), N'pol-levant', N'POL-LEVANT SHIPPING LINES LTD.', NULL, 1, 1, N'GDYNIA, POLAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(588 AS Numeric(19, 0)), N'POL-LEVANT', N'POL-LEVANT SHIPPING LINES LTD.', NULL, 4, 1, N'GDYNIA, POLAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(589 AS Numeric(19, 0)), N'POL-MARE', N'POL-MARE Spółka z ograniczoną odpowiedzialnością', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(590 AS Numeric(19, 0)), N'polmariner', N'Master / Owner of m/v BOTNIA ', NULL, 1, 1, N'Miedzyzdroje')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(591 AS Numeric(19, 0)), N'POLMARINER', N'Master / Owner of m/v PYRGOS  ', NULL, 4, 1, N'Miedzyzdroje')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(592 AS Numeric(19, 0)), N'Pol-melita', N'POL - MELITA SHIPPING LIMITE', NULL, 1, 1, N'SLIEMA , MALTA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(593 AS Numeric(19, 0)), N'POL-MELITA', N'POL - MELITA SHIPPING LIMITE', NULL, 4, 1, N'SLIEMA , MALTA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(594 AS Numeric(19, 0)), N'POLSTEAM', N'Polsteam Shipping Agency Sp. z o.o.', NULL, 1, 1, N'Pl. Rodła 8')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(595 AS Numeric(19, 0)), N'POLSTEAM', N'Polsteam Shipping Agency Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(596 AS Numeric(19, 0)), N'Polsteam ship', N'Polsteam Shipping Company Ltd   ', NULL, 1, 1, N'Limassol Cyprus ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(597 AS Numeric(19, 0)), N'POLSTEAMCY', N'POLSTEAM SHIPPING COMPANY LTD', NULL, 4, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(598 AS Numeric(19, 0)), N'POLTRAMP YARD', N'Poltramp Yard Sp. z o.o.', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(599 AS Numeric(19, 0)), N'POŁOM', N'POŁOM MARIUSZ', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(600 AS Numeric(19, 0)), N'PORT CONSULTANT', N'Port Consultants Sp.J. ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(601 AS Numeric(19, 0)), N'PORT HANDLOWY Ś', N'PORT HANDLOWY ŚWINOUJŚCIE Sp. z o.o.', NULL, 1, 1, N'ŚWINOUJŚCIE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(602 AS Numeric(19, 0)), N'PORT HANDLOWY Ś', N'PORT HANDLOWY ŚWINOUJŚCIE Sp. z o.o.', NULL, 4, 1, N'ŚWINOUJŚCIE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(603 AS Numeric(19, 0)), N'PORT HOL       ', N'Fairplay Polska spółka z ograniczoną odpowiedzialnością &', NULL, 3, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(604 AS Numeric(19, 0)), N'PORT HOL       ', N'Fairplay Polska spółka z ograniczoną odpowiedzialnością &', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(605 AS Numeric(19, 0)), N'PORT HOL ', N'Fairplay Polska Spółka z ograniczoną odpowiedzialnością & Co', NULL, 2, 1, N'Świnoujście ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(606 AS Numeric(19, 0)), N'PORTA ŻEGLUGA', N'Porta Żegluga w upadłości', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(607 AS Numeric(19, 0)), N'PORTA ŻEGLUGA', N'Porta Żegluga w upadłości', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(608 AS Numeric(19, 0)), N'PORTA ŻEGLUGA', N'Porta Żegluga w upadłości', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(609 AS Numeric(19, 0)), N'POSEJDON', N'POSEIDON & FRACHTCONTOR JUNGE Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(610 AS Numeric(19, 0)), N'Posejdon PFJ000', N'Master / Owners of mv VIKINGDIEP', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(611 AS Numeric(19, 0)), N'PPG COATINGS', N'PPG COATINGS SPRL/BVBA', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(612 AS Numeric(19, 0)), N'PRCZIP', N'PRZEDSIĘBIORSTWO ROBÓR CZERPALNYCH I PODWODNYCH SP. Z O.O.', NULL, 1, 1, N'GDAŃSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(613 AS Numeric(19, 0)), N'PRCZIP', N'PRZEDSIĘBIORSTWO ROBÓR CZERPALNYCH I PODWODNYCH SP. Z O.O.', NULL, 4, 1, N'GDAŃSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(614 AS Numeric(19, 0)), N'precious Shippi', N'M/s. Precious Shipping PCL', NULL, 1, 1, N'Bangkok , THAILAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(615 AS Numeric(19, 0)), N'PRECIOUS SHIPPI', N'M/s. Precious Shipping PCL', NULL, 4, 1, N'Bangkok , THAILAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(616 AS Numeric(19, 0)), N'PRESTIGE', N'Prestige Cruise Holdings Inc.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(617 AS Numeric(19, 0)), N'PRIMA ', N'Prima Shipping Oy Ab', NULL, 1, 1, N'Porvoo')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(618 AS Numeric(19, 0)), N'PRIMA SHIPPING', N'PRIMA SHIPPING OY AB', NULL, 4, 1, N'Provo')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(619 AS Numeric(19, 0)), N'PRINCESS ', N'PRINCESS CRUISE LINES', NULL, 4, 1, N'Santa Clarita')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(620 AS Numeric(19, 0)), N'PRINSENDAM', N'm/v PRINSENDAM  c/o', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(621 AS Numeric(19, 0)), N'PRO', N'Polskie Ratownictwo Okrętowe w prywatyzacji', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(622 AS Numeric(19, 0)), N'PRO LINE', N'PRO LINE Shipping GmbH', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(623 AS Numeric(19, 0)), N'PTS', N'PTS Sp. zo.o.', NULL, 1, 1, N'Szczecin, Polska')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(624 AS Numeric(19, 0)), N'PTS', N'PTS Sp. zo.o.', NULL, 4, 1, N'Szczecin, Polska')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(625 AS Numeric(19, 0)), N'PZKB', N'CMA CGM POLSKA Spółka z o.o.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(626 AS Numeric(19, 0)), N'PZKB', N'Polska Zjednoczona Korporacja Bałtycka Sp. z o.o.', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(627 AS Numeric(19, 0)), N'PŻ', N'PROJECT ŻEGLUGA SP. Z O.O.', NULL, 1, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(628 AS Numeric(19, 0)), N'PŻ', N'PROJECT ŻEGLUGA SP. Z O.O.', NULL, 3, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(629 AS Numeric(19, 0)), N'PŻ', N'PROJECT ŻEGLUGA SP. Z O.O.', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(630 AS Numeric(19, 0)), N'PŻ GDYNIA', N'Project Żegluga Gdynia Sp. z o.o.', NULL, 2, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(631 AS Numeric(19, 0)), N'PŻ GDYNIA', N'PROJECT ŻEGLUGA GDYNIA SP. Z O.O.', NULL, 3, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(632 AS Numeric(19, 0)), N'PŻ GDYNIA', N'PROJECT ŻEGLUGA GDYNIA SP. Z O.O.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(633 AS Numeric(19, 0)), N'PŻB', N'Polska Żegluga Bałtycka S.A.', NULL, 1, 1, N'Kołobrzeg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(634 AS Numeric(19, 0)), N'PŻB', N'POLSKA ŻEGLUGA BAŁTYCKA S.A.', NULL, 4, 1, N'Kołobrzeg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(635 AS Numeric(19, 0)), N'PŻM', N'POLSKA ZEGLUGA MORSKA P.P.', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(636 AS Numeric(19, 0)), N'PŻM', N'Polska Żegluga Morska P.P. ', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(637 AS Numeric(19, 0)), N'RADZYMINSKI', N'RADZYMINSKI ROMAN', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(638 AS Numeric(19, 0)), N'RATOWNICTWO ', N'POLSKIE RATOWNICTWO OKRĘTOWE w prywatyzacji', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(639 AS Numeric(19, 0)), N'Rederi AB', N'Rederi AB Nordo-Link/ c Finnlines Plc P.O.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(640 AS Numeric(19, 0)), N'rederiet c.ro', N'REDERIET C.ROUSING A/S', NULL, 1, 1, N'FREDERIKSVAERK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(641 AS Numeric(19, 0)), N'REDERIET C.ROUS', N'REDERIET C.ROUSING A/S', NULL, 4, 1, N'FREDERIKSVAERK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(642 AS Numeric(19, 0)), N'REDERIJ', N'Rederij K@T Holland CV X Hogelandsterweg 14,9936 BH Farsum ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(643 AS Numeric(19, 0)), N'Reederei', N'Reederei F.Laeisz Gm.b.H', NULL, 4, 1, N'Rostock/Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(644 AS Numeric(19, 0)), N'REEDEREI', N'REEDEREI STRAHLMANN BRANCH', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(645 AS Numeric(19, 0)), N'REINING', N'Reining Invest OU', NULL, 1, 1, N'Tartu')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(646 AS Numeric(19, 0)), N'REINING', N'Reining Invest OU', NULL, 2, 1, N'Tartu')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(647 AS Numeric(19, 0)), N'REINING', N'Reining Invest OU', NULL, 4, 1, N'Tartu')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(648 AS Numeric(19, 0)), N'REKA', N'Reka Shipping & Chartering GmbH', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(649 AS Numeric(19, 0)), N'REMSHIP DOSTAWC', N'REMSHIP OZYGAŁA, BOGUCKI SP. J.', NULL, 1, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(650 AS Numeric(19, 0)), N'rentrans', N'RENTRANS CARGO SP. Z O.O.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(651 AS Numeric(19, 0)), N'RETRANS', N'RENTRANS CARGO SP. Z O.O.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(652 AS Numeric(19, 0)), N'Rhenus', N'Master / Owner of m/v RUSICH 11', NULL, 1, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(653 AS Numeric(19, 0)), N'RHENUS', N'RHENUS PORT LOGISTICS SP. Z O.O.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(654 AS Numeric(19, 0)), N'RHENUS M/O EDGA', N'Master / Owner of m/v Edgar Lehmann', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(655 AS Numeric(19, 0)), N'RICHERT ANDRZEJ', N'RICHERT ANDRZEJ', NULL, 4, 1, N'80-299 GDANSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(656 AS Numeric(19, 0)), N'RIGEL GmbH', N'Rigel Bereederungs GmbH & Co. KG ', NULL, 1, 1, N'A/S Ahlgade ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(657 AS Numeric(19, 0)), N'Rigel schiffahr', N'RIGEL SCHIFFAHRTS GmbH +Co. KG mt RHONESTERN', NULL, 1, 1, N'Bremen, DEUTSCHLAND')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(658 AS Numeric(19, 0)), N'RINCA JAN', N'RINCA JAN', NULL, 4, 1, N'80-376 GDANSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(659 AS Numeric(19, 0)), N'ROBAC', N'Przedsiębiorstwo Wielobranżowe ROBAC ', NULL, 1, 1, N'Bydgoszcz')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(660 AS Numeric(19, 0)), N'ROBAC', N'Przedsiębiorstwo Wielobranżowe ROBAC ', NULL, 2, 1, N'Bydgoszcz')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(661 AS Numeric(19, 0)), N'ROBAC', N'Przedsiębiorstwo Wielobranżowe ROBAC ', NULL, 4, 1, N'Bydgoszcz')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(662 AS Numeric(19, 0)), N'ROBERTSON GROUP', N'Robertson Group Lltd', NULL, 1, 1, N'Road Town, Tortola VG 1110')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(663 AS Numeric(19, 0)), N'Rohde Nielsen', N'Rohde-Nielsen Polska', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(664 AS Numeric(19, 0)), N'ROHDE ZBIGNIEW', N'ROHDE ZBIGNIEW', NULL, 4, 1, N'84-207 KOLECZKOWO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(665 AS Numeric(19, 0)), N'ROLLS-ROYCE', N'ROLLS-ROYCE MARINE BENELUX BV', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(666 AS Numeric(19, 0)), N'ROMANKIEWICZ KR', N'ROMANKIEWICZ KRZYSZTOF', NULL, 4, 1, N'81-740 SOPOT')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(667 AS Numeric(19, 0)), N'Romy Shipping', N'Romy Shipping AS ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(668 AS Numeric(19, 0)), N'RUBO', N'Maintpartner RO Sp. z o.o.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(669 AS Numeric(19, 0)), N'Rusiecki', N'Przedsiębiorstwo Usług Hydrotechnicznych RUSIECKI', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(670 AS Numeric(19, 0)), N'RUSKIE', N'RUSKIE', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(671 AS Numeric(19, 0)), N'RUSKIE', N'RUSKIE', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(672 AS Numeric(19, 0)), N'RUSKIE', N'RUSKIE', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(673 AS Numeric(19, 0)), N'RYSMANOWSKI JAC', N'RYSMANOWSKI  JACEK', NULL, 4, 1, N'RYKOWO GÓRNE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(674 AS Numeric(19, 0)), N'RYŻ', N'ARKADIUSZ RYŻ', NULL, 1, 1, N'Police')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(675 AS Numeric(19, 0)), N'RYŻ', N'ARKADIUSZ RYŻ', NULL, 2, 1, N'Police')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(676 AS Numeric(19, 0)), N'RYŻ', N'ARKADIUSZ RYŻ', NULL, 4, 1, N'Police')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(677 AS Numeric(19, 0)), N'S.F.  KADR', N'STUDIO FILMOWE   "KADR"', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(678 AS Numeric(19, 0)), N'SAB', N'ZAKŁAD HANDLOWO-USŁUGOWY "SAB"', NULL, 4, 1, N'Nowa Wieś Lęborska')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(679 AS Numeric(19, 0)), N'SAFE1', N'SAFE SHIPPING SP.Z O.O.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(680 AS Numeric(19, 0)), N'Sagittarius', N'Sagittarius Shipping Company Ltd', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(681 AS Numeric(19, 0)), N'Samherji hf', N'Samherji hf.', NULL, 1, 1, N'Akureyri, Iceland')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(682 AS Numeric(19, 0)), N'SANDFRAKT AS', N'SANDFRAKT AS ', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(683 AS Numeric(19, 0)), N'SĄD APELACYJNY', N'Sąd Apelacyjny I Wydział Cywilny', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(684 AS Numeric(19, 0)), N'SĄD APELACYJNY', N'Sąd Apelacyjny I Wydział Cywilny', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(685 AS Numeric(19, 0)), N'SĄD APELACYJNY', N'Sąd Apelacyjny I Wydział Cywilny', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(686 AS Numeric(19, 0)), N'SCAN', N'SCAN SHIPPING  POL SP.Z O.O ', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(687 AS Numeric(19, 0)), N'SCANDILINES', N'Scandlines Deutchland GmbH', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(688 AS Numeric(19, 0)), N'SCANDILINES', N'Scandlines Deutchland GmbH', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(689 AS Numeric(19, 0)), N'SCANDLINES A/S', N'SCANDLINES BALTIC A/S', NULL, 4, 1, N'm/v URD')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(690 AS Numeric(19, 0)), N'SCANDLINES A/S', N'SCANDLINES BALTIC A/S - m/v ASK', NULL, 1, 1, N'COPENHAGEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(691 AS Numeric(19, 0)), N'Schiffahrts', N' HS Schiffahrts GmbH & Co.KG MS "Jan"', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(692 AS Numeric(19, 0)), N'Schiffahrtsgese', N'MV Alsterdiep c/o Pool-Carriers Schiffahrtsgeselischaft mbH ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(693 AS Numeric(19, 0)), N'SCHLEIF TRANSPO', N'SCHLEIF TRANSPORT GMBH', NULL, 1, 1, N'BREMEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(694 AS Numeric(19, 0)), N'SCHLEIF TRANSPO', N'SCHLEIF TRANSPORT GMBH', NULL, 2, 1, N'BREMEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(695 AS Numeric(19, 0)), N'SCHLEIF TRANSPO', N'SCHLEIF TRANSPORT GMBH', NULL, 4, 1, N'BREMEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(696 AS Numeric(19, 0)), N'SCHULTZ', N'SCHULTZ SHIPPING  SP. Z  O.O.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(697 AS Numeric(19, 0)), N'SEA TECH', N'SEA-TECH POLAND S.C.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(698 AS Numeric(19, 0)), N'SEARENERGY', N'SeaRenergy Offshore Holding GmbH & Cie. KG', NULL, 1, 1, N'22763 Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(699 AS Numeric(19, 0)), N'SEARENERGY PROJ', N'SeaRenergy Offshore Projects GmbH & Cie. KG', NULL, 1, 1, N'22763 Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(700 AS Numeric(19, 0)), N'SEASHORE', N'Seashore Maritime Ltd.', NULL, 4, 1, N'Piraeus, Greece')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(701 AS Numeric(19, 0)), N'Seashore marit', N'Seashore Maritime Ltd.', NULL, 1, 1, N'Piraeus, Greece')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(702 AS Numeric(19, 0)), N'Seatrade', N'SEATRADE GROUP N.V.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(703 AS Numeric(19, 0)), N'SEATRADE', N'Seatrade Group NV ', NULL, 1, 1, N'2030 Antwerp')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(704 AS Numeric(19, 0)), N'SERKOWSKI PIOTR', N'SERKOWSKI PIOTR', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(705 AS Numeric(19, 0)), N'sermacom', N'SERMACOM BREST CTMPB-CC 27', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(706 AS Numeric(19, 0)), N'silver green', N'SilverGreen TC AS', NULL, 1, 1, N'Nesttun Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(707 AS Numeric(19, 0)), N'sinbad', N'Sinbad Chartering Company AB', NULL, 1, 1, N'Stockholm, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(708 AS Numeric(19, 0)), N'SINDBAD', N'Sinbad Chartering Company AB', NULL, 4, 1, N'Stockholm, Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(709 AS Numeric(19, 0)), N'SINDBAD-OKTAN', N'Sinbad Chartering Company AB Flottbrovaegen 27', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(710 AS Numeric(19, 0)), N'SIRIUS', N'Sirius Chartering AB ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(711 AS Numeric(19, 0)), N'Sirius', N'Sirius Chartering AB,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(712 AS Numeric(19, 0)), N'SISU', N'mv SISU CASTOR', NULL, 4, 1, N', Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(713 AS Numeric(19, 0)), N'SIWIELA', N'SIWIELA ZBIGNIEW', NULL, 4, 1, N'REDA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(714 AS Numeric(19, 0)), N'SKS tankers', N'SKS Tankers', NULL, 1, 1, N'Fyllingsdalen, Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(715 AS Numeric(19, 0)), N'SLING', N'“SLING” Wanda Lisiecka i Sławomir Lisiecki Sp. j.', NULL, 1, 1, N'Pruszcz Gdański')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(716 AS Numeric(19, 0)), N'SLING', N'Sling Wanda Lisiecka i Sławomir Lisiecki Spółka Jawna ', NULL, 4, 1, N'Pruszcz Gdański-Radunica')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(717 AS Numeric(19, 0)), N'SLOMAN', N'SLOMAN  NEPTUN Schiffahrts-Aktiengesselschaft', NULL, 1, 1, N'Bremen, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(718 AS Numeric(19, 0)), N'SMS', N'Master / Owner of m/v Sichem Eagle', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(719 AS Numeric(19, 0)), N'SMS', N'Owner of m/v SC ASTREA', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(720 AS Numeric(19, 0)), N'SMW', N'STOCZNIA MARYNARKI WOJENNEJ S.A. w upadłości likwidacyjnej', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(721 AS Numeric(19, 0)), N'SOBCZYK', N'SOBCZYK ZBIGNIEW', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(722 AS Numeric(19, 0)), N'SOCHON', N'SOCHON MAREK', NULL, 4, 1, N'DARLOWP PL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(723 AS Numeric(19, 0)), N'sodra', N'Sodra Cell International AB', NULL, 1, 1, N'Sweden')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(724 AS Numeric(19, 0)), N'SP/F Nolsoe', N'SP/F Nolsoe Shipping - Fure Sun', NULL, 1, 1, N'Torshavn, Faroe Islands')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(725 AS Numeric(19, 0)), N'spavalda', N'm/v Spavalda', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(726 AS Numeric(19, 0)), N'SPAVALDA', N'm/v Spavalda', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(727 AS Numeric(19, 0)), N'Spedrapid', N'Spedrapid Szczecin Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(728 AS Numeric(19, 0)), N'Speed', N'SPEED  Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(729 AS Numeric(19, 0)), N'SPIETSHOFFS', N'Spliethoff''s Bevrachtingskantoor B.V.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(730 AS Numeric(19, 0)), N'SPITSBERGEN', N'SPITSBERGEN INC PANAMA Tore Universal c/o ROSWELL TANKERS', NULL, 1, 1, N'REPUBLIC OF PANAMA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(731 AS Numeric(19, 0)), N'Spliethoff', N'Spliethoff''s Bevrachtingskantoor B.V.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(732 AS Numeric(19, 0)), N'SŚZU', N'STOWARZYSZENIE ŚWIATOWY ZJAZD UCZELNI', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(733 AS Numeric(19, 0)), N'st management', N'ST MANAGAMENT', NULL, 1, 1, N'MERIGNAC, FRANCE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(734 AS Numeric(19, 0)), N'ST MENAGEMENT', N'ST MANAGAMENT', NULL, 4, 1, N'MERIGNAC, FRANCE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(735 AS Numeric(19, 0)), N'STAL COMPLEX', N'STAL COMPLEX', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(736 AS Numeric(19, 0)), N'Stan', N'Stan Shipping Agency LTD.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(737 AS Numeric(19, 0)), N'STAŃCZYK S', N'STANISŁAWA STAŃCZYK', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(738 AS Numeric(19, 0)), N'STARBOARD ', N'STARBOARD SHIPPING ', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(739 AS Numeric(19, 0)), N'Stargate', N'Stargate Shipmanagement Gmbh ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(740 AS Numeric(19, 0)), N'STATOIL', N'LADY ELENA, STATOIL', NULL, 4, 1, N'GRANGEMOUTH UK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(741 AS Numeric(19, 0)), N'statoil', N'm/t MARIANNE, STATOIL', NULL, 1, 1, N'GRANGEMOUTH UK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(742 AS Numeric(19, 0)), N'stemag', N'Stemag Marine Group', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(743 AS Numeric(19, 0)), N'STENA LINE', N'STENA LINE SCANDINAVIA  AB     ', NULL, 4, 1, N'Gotenborg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(744 AS Numeric(19, 0)), N'STENSHIP', N'Stenship KS - Org. no 989 224 875', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(745 AS Numeric(19, 0)), N'Stentank AS', N'Stentank AS', NULL, 1, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(746 AS Numeric(19, 0)), N'STENTANK AS', N'Stentank AS Org number 976 726 340', NULL, 4, 1, N'Lysaker, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(747 AS Numeric(19, 0)), N'Stentank Org', N'Stentank – Org.no 976 726 340 ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(748 AS Numeric(19, 0)), N'STER', N'PPUH  "STER"  Spółka z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(749 AS Numeric(19, 0)), N'STOCZNIA POŁUDN', N'STOCZNIA POŁUDNIE S.C. BOLESŁAW DANILEWICZ, EUGENIUSZ ŁAWICK', NULL, 1, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(750 AS Numeric(19, 0)), N'Strahlman', N'Reederei Strahlmann Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(751 AS Numeric(19, 0)), N'stt', N'Master / Owner of m/v Eidholm', NULL, 1, 1, N'Szczecin, PL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(752 AS Numeric(19, 0)), N'STTC', N'Ship-Trans-Tourist Consulting Sp.J. A. Małdyk', NULL, 4, 1, N'Szczecin, PL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(753 AS Numeric(19, 0)), N'SUZAN BV', N'CS Suzan BV', NULL, 1, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(754 AS Numeric(19, 0)), N'SUZAN BV', N'CS Suzan BV', NULL, 2, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(755 AS Numeric(19, 0)), N'SUZAN BV', N'CS Suzan BV', NULL, 4, 1, N'Rotterdam ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(756 AS Numeric(19, 0)), N'Svane', N'Svane Shipping A/S', NULL, 4, 1, N'Kolding')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(757 AS Numeric(19, 0)), N'Svendborg bug', N'SVENDBORG BUGSER A/S', NULL, 1, 1, N'Svendborg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(758 AS Numeric(19, 0)), N'SVENSKA', N'SVENSKA ORIENT LINEN AB', NULL, 4, 1, N'Gothenburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(759 AS Numeric(19, 0)), N'SWEDE CHEM', N'SWEDE CHEM TANKERS AB', NULL, 1, 1, N'STOCKHOLM, SWEDEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(760 AS Numeric(19, 0)), N'SWISSMARINE', N'SwissMarine Services SA', NULL, 1, 1, N'Geneva ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(761 AS Numeric(19, 0)), N'SYCHOWSKI', N'SYCHOWSKI ANDRZEJ', NULL, 4, 1, N'SZEMUD')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(762 AS Numeric(19, 0)), N'SZCZECIN PILOT', N'Szczecin-Pilot Spółka z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(763 AS Numeric(19, 0)), N'SZCZECIN PILOT', N'Szczecin-Pilot Spółka z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(764 AS Numeric(19, 0)), N'SZCZODROWSKI', N'STANISLAW SZCZODROWSKI', NULL, 4, 1, N'GDANSK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(765 AS Numeric(19, 0)), N'Szleszyński', N'Szleszyński Zbigniew', NULL, 4, 1, N'Pogórze Górne')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(766 AS Numeric(19, 0)), N'SZORNAK', N'SZORNAK JÓZEF', NULL, 4, 1, N'Reda')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(767 AS Numeric(19, 0)), N'SZUTENBERG', N'ROMAN SZUTENBERG', NULL, 4, 1, N'KARTUZY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(768 AS Numeric(19, 0)), N'SZUTENBERG ROMA', N'SZUTENBERG ROMAN', NULL, 4, 1, N'83-300 KARTUZY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(769 AS Numeric(19, 0)), N'Szymysł', N'Szymysł Andrzej', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(770 AS Numeric(19, 0)), N'Szymysł', N'Szymysł Andrzej', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(771 AS Numeric(19, 0)), N'SZYMYSŁ', N'Szymysł Andrzej', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(772 AS Numeric(19, 0)), N'Tarntank', N'Tärntank Ship Management', NULL, 1, 1, N'Donsö, SWEDEN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(773 AS Numeric(19, 0)), N'TBS shipp', N'TBS SHIPPING SERVICES INC.', NULL, 1, 1, N'New York, Yonkers ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(774 AS Numeric(19, 0)), N'TEAM LINES', N'Team Lines Deutschland GmbH&Co. KG ', NULL, 4, 1, N'Hamburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(775 AS Numeric(19, 0)), N'TEAM LINES', N'Team Lines Deutschland GmbH&Co. KG ', NULL, 1, 1, N'Hamburg, Germany')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(776 AS Numeric(19, 0)), N'TERMAR', N'TERMAR', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(777 AS Numeric(19, 0)), N'thames', N'INDEPENDENT SHIPPING LIMITED, SHOREHAM', NULL, 1, 1, N'GRIMSBY; NE LINCS DN31 3LW    ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(778 AS Numeric(19, 0)), N'THAMES', N'INDEPENDENT SHIPPING LIMITED, SHOREHAM', NULL, 4, 1, N'GRIMSBY; NE LINCS DN31 3LW    ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(779 AS Numeric(19, 0)), N'TKACZYK ANDRZEJ', N'TKACZYK ANDRZEJ', NULL, 4, 1, N'RUMIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(780 AS Numeric(19, 0)), N'TKB SHIPPING', N'T.K.B. SHIPPING A/S', NULL, 1, 1, N'DK - 2900 Hellerup')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(781 AS Numeric(19, 0)), N'TNT', N'TNT Express Worldwide Poland Sp.z o.o.', NULL, 1, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(782 AS Numeric(19, 0)), N'TNT', N'TNT Express Worldwide Poland Sp.z o.o.', NULL, 2, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(783 AS Numeric(19, 0)), N'TNT', N'TNT Express Worldwide Poland Sp.z o.o.', NULL, 4, 1, N'Warszawa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(784 AS Numeric(19, 0)), N'Tomkowiak', N'Tomkowiak Ireneusz PPSW', NULL, 1, 1, N'Obrzycko')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(785 AS Numeric(19, 0)), N'TOMKOWIAK', N'Tomkowiak Ireneusz PPSW', NULL, 4, 1, N'Obrzycko')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(786 AS Numeric(19, 0)), N'trader', N'm/v TRADER"', NULL, 4, 1, N'Gothenburg')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(787 AS Numeric(19, 0)), N'Trampbalt', N'TRAMPBALT OVERSEAS  Sp.z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(788 AS Numeric(19, 0)), N'Trans Baltic', N'Trans Baltic S.C.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(789 AS Numeric(19, 0)), N'TRANSATLANTIC', N'REDERI  AB TRANSATLANTIC ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(790 AS Numeric(19, 0)), N'Transbaltic', N'Trans Baltic s.c.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(791 AS Numeric(19, 0)), N'TRANSBULK', N'Messrs TRANSBULK 1904 AB,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(792 AS Numeric(19, 0)), N'TRANSFENICA', N'TRANSFENNICA LTD.', NULL, 4, 1, N'HELSINKI')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(793 AS Numeric(19, 0)), N'TRANSFENICA POL', N'TRANSFENNICA POLSKA Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(794 AS Numeric(19, 0)), N'transfennica', N'Owner of MV Anjeliersgracht', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(795 AS Numeric(19, 0)), N'TRANSTECK', N'TRANSTECK SHIPPING LTD.,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(796 AS Numeric(19, 0)), N'TRANSTECK000', N'Transteck Shipping Ltd.,', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(797 AS Numeric(19, 0)), N'TRANTER', N'TRANTER INTERNATIONAL AB', NULL, 4, 1, N'VANESBORG')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(798 AS Numeric(19, 0)), N'Trend Projekt', N'TREND Projekt Sp. z o.o.', NULL, 4, 1, N'Pruszcz Gdański')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(799 AS Numeric(19, 0)), N'Tri Frakt As', N'Tri Frakt As', NULL, 4, 1, N'Austrheim')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(800 AS Numeric(19, 0)), N'TRI STAR', N'mv "TRI STAR"', NULL, 4, 1, N'Aabenraa')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(801 AS Numeric(19, 0)), N'TYMOSZUK', N'HENRYK TYMOSZUK', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(802 AS Numeric(19, 0)), N'TYMOSZUK', N'HENRYK TYMOSZUK', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(803 AS Numeric(19, 0)), N'TYMOSZUK', N'HENRYK TYMOSZUK', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(804 AS Numeric(19, 0)), N'TZAREVETZ ', N'PERSENK MARITIME LTD c/o NAVIGATION MARITIME BULGARE ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(805 AS Numeric(19, 0)), N'U A Roselius AB', N'U A Roselius AB', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(806 AS Numeric(19, 0)), N'U.STUDIO', N'U.STUDIO Paulina Miziewicz', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(807 AS Numeric(19, 0)), N'U.STUDIO', N'U.STUDIO Wojciech Miziewicz', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(808 AS Numeric(19, 0)), N'UAB', N'UAB "Juru agentura FORSA"', NULL, 4, 1, N'Klaipeda')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(809 AS Numeric(19, 0)), N'UAB AMBER BAY', N'UAB "Amber Bay" ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(810 AS Numeric(19, 0)), N'ULRABULK S.A.', N'Ultrabulk S.A.   Av.Ei Bosque Norte 500-20th Floor', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(811 AS Numeric(19, 0)), N'Ultrabulk', N'Ultrabulk A/S', NULL, 1, 1, N'Copenhagen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(812 AS Numeric(19, 0)), N'ULTRABULK ', N'ULTRABULK A/S', NULL, 4, 1, N'Copenhagen')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(813 AS Numeric(19, 0)), N'ULTRABULK  S.A.', N'ULTRABULK  S.A.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(814 AS Numeric(19, 0)), N'Ultrabulk 3', N'Flavia Raguel Bohm Operations Manager ', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(815 AS Numeric(19, 0)), N'UMS', N'Owners of m/v "Oraluna"  c/o  UMS  MARBALCO Sp. Z O.O.', NULL, 4, 1, N'Sopot')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(816 AS Numeric(19, 0)), N'UMS', N'Urząd Morski w Szczecinie', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(817 AS Numeric(19, 0)), N'UNI', N'UNI-CHARTERING A/S', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(818 AS Numeric(19, 0)), N'Unibaltic', N'Unibaltic Shipping Ltd.', NULL, 1, 1, N'Limassol')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(819 AS Numeric(19, 0)), N'UNIBALTIC', N'Unibaltic Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(820 AS Numeric(19, 0)), N'UNIBALTIC CY', N'UNIBALTIC SHIPPING LTD', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(821 AS Numeric(19, 0)), N'Unibaltic PL', N'Unibaltic Ltd.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(822 AS Numeric(19, 0)), N'UNIBALTIC RUBIN', N'M/T RUBINO C/O UNIBALTIC SHIPPING', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(823 AS Numeric(19, 0)), N'UNIFEEDER', N'UNIFEEDER  A/S', NULL, 4, 1, N'AARHUS C    ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(824 AS Numeric(19, 0)), N'UNIFEEDER', N'Unifeeder A/S', NULL, 1, 1, N'AARHUS C / DENMARK')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(825 AS Numeric(19, 0)), N'UNIFEEDER 2', N' UNIFEEDER A/S  S.A.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(826 AS Numeric(19, 0)), N'UNION TRANSPORT', N'UNION TRANSPORT GROUP Plc', NULL, 4, 1, N'Bromley, Kent. BR1 1SJ')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(827 AS Numeric(19, 0)), N'UNISTEEL', N'Master/ Owners of mv VISNES', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(828 AS Numeric(19, 0)), N'Uni-Tankers', N'Uni-Tankers m/t "Great Swan" APS', NULL, 4, 1, N' Middelfart')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(829 AS Numeric(19, 0)), N'UNI-TANKERS', N'UNI-TANKERS MT ERRIA SWAN APS', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(830 AS Numeric(19, 0)), N'UNITY LINE', N'c/o Unity Line Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(831 AS Numeric(19, 0)), N'UNITY LINE', N'c/o Unity Line Sp. z o.o.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(832 AS Numeric(19, 0)), N'URBANEK WŁODZIM', N'URBANEK WŁODZIMIERZ', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(833 AS Numeric(19, 0)), N'utkilen', N'Utkilen AS', NULL, 1, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(834 AS Numeric(19, 0)), N'UTKILEN', N'Utkilen AS  Strandgaten  197', NULL, 4, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(835 AS Numeric(19, 0)), N'Uwe Meier', N'SCHULSCHIFFVEREIN " GROSSHERZOGIN ELISABETH"', NULL, 1, 1, N'Delmenhorst GERMANY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(836 AS Numeric(19, 0)), N'UWE MEIER', N'SCHULSCHIFFVEREIN " GROSSHERZOGIN ELISABETH"', NULL, 4, 1, N'Delmenhorst GERMANY')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(837 AS Numeric(19, 0)), N'Vadero', N'Vadero Shipping Ltd', NULL, 4, 1, N'457 72 Grebbestad')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(838 AS Numeric(19, 0)), N'Van den Herik', N'Van den Herik Kust - en Oeverwerken B.V.', NULL, 1, 1, N'HJ Sliedrecht, Holandia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(839 AS Numeric(19, 0)), N'VEGAMAR', N'VEGAMAR SP. Z O.O.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(840 AS Numeric(19, 0)), N'VEGAMAR', N'VEGAMAR SP. Z O.O.', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(841 AS Numeric(19, 0)), N'VEGAMAR', N'VEGAMAR SP. Z O.O.', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(842 AS Numeric(19, 0)), N'VELIKO', N'Veliko Tirnovo Shipping LTD, 60 South Str., Valletta, MALTA', NULL, 4, 1, N'VARNA, BULGARIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(843 AS Numeric(19, 0)), N'VETRO', N' Izabela Tymińska - Vetro', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(844 AS Numeric(19, 0)), N'Vetro', N'Vetro - Izabela Tyminska', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(845 AS Numeric(19, 0)), N'VISTAL', N'VISTAL GDYNIA S.A.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(846 AS Numeric(19, 0)), N'VISTAL OFFSHORE', N'VISTAL OFFSHORE ', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(847 AS Numeric(19, 0)), N'VISTAL OLVIT', N'VISTAL OLVIT Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(848 AS Numeric(19, 0)), N'VISTAL SHIPYARD', N'VISTAL SHIPYARD  Sp.  z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(849 AS Numeric(19, 0)), N'Volkmar', N'Volkmar Baume Poland Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(850 AS Numeric(19, 0)), N'Vopak', N'Vopak Agencies Rotterdam B.V.', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(851 AS Numeric(19, 0)), N'Vt shipping', N'VT Shipping A/S', NULL, 1, 1, N'Copenhagen SV, Denmark')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(852 AS Numeric(19, 0)), N'wagenborg', N'Wagenborg Shipping B.V.', NULL, 1, 1, N'Delfzijl')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(853 AS Numeric(19, 0)), N'WAGENBORG', N'WAGENBORG SHIPPING BV', NULL, 4, 1, N'Delfzijl /The Netherlands')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(854 AS Numeric(19, 0)), N'Warta', N'TU Warta', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(855 AS Numeric(19, 0)), N'Warta', N'TU Warta', NULL, 2, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(856 AS Numeric(19, 0)), N'WARTA', N'TU Warta', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(857 AS Numeric(19, 0)), N'WASKI.NET', N'WASKI.NET', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(858 AS Numeric(19, 0)), N'WASKI.NET', N'WASKI.NET', NULL, 2, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(859 AS Numeric(19, 0)), N'WASKI.NET', N'WASKI.NET', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(860 AS Numeric(19, 0)), N'Wenskowski', N'Wenskowski Tomasz', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(861 AS Numeric(19, 0)), N'WERSE', N'WERSE CHARTERING GMBH', NULL, 1, 1, N'MUENSTER')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(862 AS Numeric(19, 0)), N'WERSE', N'WERSE CHARTERING GMBH', NULL, 2, 1, N'MUENSTER')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(863 AS Numeric(19, 0)), N'WERSE', N'WERSE CHARTERING GMBH', NULL, 4, 1, N'MUENSTER')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(864 AS Numeric(19, 0)), N'western', N'Western Bulk Carriers', NULL, 1, 1, N'Oslo, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(865 AS Numeric(19, 0)), N'WĘGLOKOKS', N'WĘGLOKOKS S.A.', NULL, 4, 1, N' Katowice')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(866 AS Numeric(19, 0)), N'WIATER', N'MIROSŁAW WIATER', NULL, 2, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(867 AS Numeric(19, 0)), N'wilhelmsen', N'Master / Owner of  m/v Northern Lynx', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(868 AS Numeric(19, 0)), N'WILHELMSEN', N'Owners of  "ZAPADNYY "', NULL, 4, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(869 AS Numeric(19, 0)), N'Wilson', N'WILSON ASA', NULL, 1, 1, N'Bergen, Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(870 AS Numeric(19, 0)), N'WILSON', N'WILSON EUROCARRIERS AS', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(871 AS Numeric(19, 0)), N'WILSON EUROCAR', N'WILSON EUROCARRIERS AS', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(872 AS Numeric(19, 0)), N'WILSONE', N'WILSON  EUROCARRIERS AS  ', NULL, 4, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(873 AS Numeric(19, 0)), N'WILSONS', N'WILSON SHIP MANAGEMENT AS', NULL, 4, 1, N'Bergen,Norway')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(874 AS Numeric(19, 0)), N'Wisła', N'Stocznia Wisła Sp  z o.o.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(875 AS Numeric(19, 0)), N'Wolniak', N'Wolniak Marcin', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(876 AS Numeric(19, 0)), N'WÓJCIK ADAM', N'ADAM WÓJCIK', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(877 AS Numeric(19, 0)), N'WÓJCIK ADAM', N'ADAM WÓJCIK', NULL, 2, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(878 AS Numeric(19, 0)), N'WÓJCIK ADAM', N'ADAM WÓJCIK', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(879 AS Numeric(19, 0)), N'WShipping', N'WShipping Ltd.', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(880 AS Numeric(19, 0)), N'wulkan', N'Wulkan Sp. z o.o.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(881 AS Numeric(19, 0)), N'WUŻ', N'WUŻ Przedsiębiorstwo Usług Żeglugowych i Portowych Gdynia', NULL, 1, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(882 AS Numeric(19, 0)), N'WUŻ GDYNIA', N'WUŻ Przedsiębiorstwo Usług Żeglugowych i Portowych Gdynia ', NULL, 2, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(883 AS Numeric(19, 0)), N'WUŻ GDYNIA', N'WUŻ PRZEDSIĘBIORSTWO USŁUG ŻEGLUGOWYCH I PORTOWYCH GDYNIA', NULL, 3, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(884 AS Numeric(19, 0)), N'WUŻ PORT', N'WUŻ PORT AND MARITIME SERVICES Ltd.Sp.z o.o.', NULL, 4, 1, N'Gdańsk')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(885 AS Numeric(19, 0)), N'WYSŁOUCH', N'WYSŁOUCH ANDRZEJ', NULL, 4, 1, N'SMOLNO')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(886 AS Numeric(19, 0)), N'XO Shipping', N'XO Shipping A/S ', NULL, 1, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(887 AS Numeric(19, 0)), N'Yara', N'Yara Balderton Ltd c/o', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(888 AS Numeric(19, 0)), N'z', N'z', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(889 AS Numeric(19, 0)), N'z1', N'z1', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(890 AS Numeric(19, 0)), N'Z4', N'Z4', NULL, 4, 1, N'NULL')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(891 AS Numeric(19, 0)), N'Zagaj Marek', N'Zagaj Marek', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(892 AS Numeric(19, 0)), N'ZAKŁ USŁ NIEAKT', N'ZAKŁAD USŁUG ZAKRZEWSKI (NIEAKTYWNY)', NULL, 1, 1, N'ŚWINOUJŚCIE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(893 AS Numeric(19, 0)), N'ZAKŁ USŁ NIEAKT', N'ZAKŁAD USŁUG ZAKRZEWSKI (NIEAKTYWNY)', NULL, 4, 1, N'ŚWINOUJŚCIE')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(894 AS Numeric(19, 0)), N'ZAKŁAD', N'z', NULL, 1, 1, NULL)
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(895 AS Numeric(19, 0)), N'ZAKŁAD INSTALAT', N'ZAKŁAD INSTALATORSTWA SANITARNEGO I GAZ. ', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(896 AS Numeric(19, 0)), N'ZAKŁAD INSTALAT', N'ZAKŁAD INSTALATORSTWA SANITARNEGO I GAZ. ', NULL, 4, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(897 AS Numeric(19, 0)), N'ZIĘBA', N'ZIĘBA IRENEUSZ', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(898 AS Numeric(19, 0)), N'ZMP GDYNIA', N'ZARZĄD MORSKIEGO PORTU GDYNIA S. A.', NULL, 4, 1, N'GDYNIA')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(899 AS Numeric(19, 0)), N'ZMPSŚ', N'ZARZĄD MORSKICH PORTÓW SZCZECIN  I ŚWINOUJŚCIE S.A', NULL, 1, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(900 AS Numeric(19, 0)), N'ZMPSŚ', N'ZARZĄD MORSKICH PORTÓW SZCZECIN ŚWINOUJŚCIE S.A', NULL, 4, 1, N'SZCZECIN')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(901 AS Numeric(19, 0)), N'ZRUO', N'ZRUO Sp. z o.o.', NULL, 4, 1, N'Gdynia')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(902 AS Numeric(19, 0)), N'zuz', N'Zakład Usług Żeglugowych Sp. z o.o. & Co. Sp. k.', NULL, 1, 1, N'Szczecin')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(903 AS Numeric(19, 0)), N'Żegluga Przybrz', N'Żegluga Przybrzeżna, Marek Sochoń', NULL, 4, 1, N'Darłowo')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(904 AS Numeric(19, 0)), N'Żegluga Świn', N'Żegluga Świnoujska', NULL, 1, 1, N'Świnoujście')
GO
INSERT [dsg_pz_odbiorca_refaktury_raw] ([id], [cn], [title], [centrum], [refValue], [available], [miasto]) VALUES (CAST(905 AS Numeric(19, 0)), N'Żegluga Wiślana', N'Żegluga Wiślana, Rafał Błocki', NULL, 4, 1, N'Zielonka')
GO
SET IDENTITY_INSERT [dsg_pz_odbiorca_refaktury_raw] OFF
GO
SET IDENTITY_INSERT [dsg_pz_pozostali_raw] ON 

GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(19, 0)), N'210-01         ', N'MC Mirosław Wiater', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(19, 0)), N'210-02         ', N'MC Mirosław Górny', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(19, 0)), N'210-03         ', N'Visa Przemek Góralski', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(19, 0)), N'210-04         ', N'Visa Krzysztof Ciesielczyk', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(19, 0)), N'210-05         ', N'Visa Henryka Judkowiak', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(19, 0)), N'210-06         ', N'Visa Małgorzata Urbańska', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(19, 0)), N'210-07         ', N'Visa Grzegorz Wójcik', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(19, 0)), N'210-08         ', N'Visa Rafał Jacoszek', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(19, 0)), N'210-09         ', N'Visa Konrad Majcher', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(19, 0)), N'210-10         ', N'Visa Tomasz Jesionka', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(19, 0)), N'210-11         ', N'Visa Agnieszka Janczyńska', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(12 AS Numeric(19, 0)), N'210-12         ', N'Visa Arkadiusz Ryż', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(13 AS Numeric(19, 0)), N'210-13         ', N'MC Michał Dudek', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(14 AS Numeric(19, 0)), N'210-14         ', N'Visa Beata Łazor', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(15 AS Numeric(19, 0)), N'210-15         ', N'MC Artur Schneider', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(16 AS Numeric(19, 0)), N'210-16         ', N'MC Danuta Adamiak-Kosy', NULL, 1, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(17 AS Numeric(19, 0)), N'210-01         ', N'MC Mirosław Wiater', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(18 AS Numeric(19, 0)), N'210-02         ', N'MC Mirosław Górny', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(19 AS Numeric(19, 0)), N'210-03         ', N'Visa Przemek Góralski', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(20 AS Numeric(19, 0)), N'210-04         ', N'Visa Krzysztof Ciesielczyk', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(21 AS Numeric(19, 0)), N'210-05         ', N'Visa Henryka Judkowiak', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(22 AS Numeric(19, 0)), N'210-06         ', N'Visa Małgorzata Urbańska', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(23 AS Numeric(19, 0)), N'210-07         ', N'Visa Grzegorz Wójcik', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(24 AS Numeric(19, 0)), N'210-08         ', N'Visa Rafał Jacoszek', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(25 AS Numeric(19, 0)), N'210-09         ', N'Visa Konrad Majcher', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(26 AS Numeric(19, 0)), N'210-10         ', N'Visa Agnieszka Janczyńska', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(27 AS Numeric(19, 0)), N'210-11         ', N'MC Jola Klimek', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(28 AS Numeric(19, 0)), N'210-12         ', N'MC Arek Ryż', NULL, 2, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(29 AS Numeric(19, 0)), N'210-01         ', N'MC Mirosław Wiater', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(30 AS Numeric(19, 0)), N'210-02         ', N'MC Mirosław Górny', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(31 AS Numeric(19, 0)), N'210-03         ', N'Visa Przemek Góralski', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(32 AS Numeric(19, 0)), N'210-04         ', N'Visa Krzysztof Ciesielczyk', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(33 AS Numeric(19, 0)), N'210-05         ', N'Visa Henryka Judkowiak', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(34 AS Numeric(19, 0)), N'210-06         ', N'Visa Małgorzata Urbańska', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(35 AS Numeric(19, 0)), N'210-07         ', N'Visa Grzegorz Wójcik', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(36 AS Numeric(19, 0)), N'210-08         ', N'Visa Rafał Jacoszek', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(37 AS Numeric(19, 0)), N'210-09         ', N'Visa Konrad Majcher', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(38 AS Numeric(19, 0)), N'210-10         ', N'Visa Agnieszka Janczyńska', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(39 AS Numeric(19, 0)), N'210-11         ', N'MC Jola Klimek', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(40 AS Numeric(19, 0)), N'210-12         ', N'MC Arek Ryż', NULL, 3, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(41 AS Numeric(19, 0)), N'210-01         ', N'MC Mirosław Wiater', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(42 AS Numeric(19, 0)), N'210-02         ', N'MC Wiesław Michulec', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(43 AS Numeric(19, 0)), N'210-03         ', N'MC Henryk Kruza', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(44 AS Numeric(19, 0)), N'210-04         ', N'MC Mirosław Szalczyński', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(45 AS Numeric(19, 0)), N'210-05         ', N'MC Ryszard Baczewski', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(46 AS Numeric(19, 0)), N'210-06         ', N'MC Michał Ciszewski', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(47 AS Numeric(19, 0)), N'210-07         ', N'MC Aleksandra Peplińska', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(48 AS Numeric(19, 0)), N'210-08         ', N'Visa Rafał Jacoszek', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(49 AS Numeric(19, 0)), N'210-09         ', N'Visa Konrad Majcher', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(50 AS Numeric(19, 0)), N'210-10         ', N'Visa Tomasz Jesionka', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(51 AS Numeric(19, 0)), N'210-11         ', N'Visa Agnieszka Janczyńska', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(52 AS Numeric(19, 0)), N'210-12         ', N'Visa Arkadiusz Ryż', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(53 AS Numeric(19, 0)), N'210-13         ', N'MC Michał Dudek', NULL, 4, 1)
GO
INSERT [dsg_pz_pozostali_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(54 AS Numeric(19, 0)), N'210-14         ', N'MC Mirosław Górny', NULL, 4, 1)
GO
SET IDENTITY_INSERT [dsg_pz_pozostali_raw] OFF
GO
SET IDENTITY_INSERT [dsg_pz_produkt_raw] ON 

GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(19, 0)), N'010-1', N'010-1', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(19, 0)), N'010-2', N'010-2', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(19, 0)), N'010-4', N'010-4', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(19, 0)), N'010-7', N'010-7', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(19, 0)), N'010-8', N'010-8', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(19, 0)), N'011-2', N'011-2', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(19, 0)), N'011-4', N'011-4', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(19, 0)), N'011-7', N'011-7', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(19, 0)), N'011-8', N'011-8', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(19, 0)), N'020-1', N'020-1', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(19, 0)), N'020-2', N'020-2', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(12 AS Numeric(19, 0)), N'080-801', N'080-801', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(13 AS Numeric(19, 0)), N'080-802', N'080-802', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(14 AS Numeric(19, 0)), N'401P0100', N'401P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(15 AS Numeric(19, 0)), N'401P0200', N'401P0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(16 AS Numeric(19, 0)), N'401Z0100', N'401Z0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(17 AS Numeric(19, 0)), N'401Z0200', N'401Z0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(18 AS Numeric(19, 0)), N'402P0100', N'402P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(19 AS Numeric(19, 0)), N'402P0200', N'402P0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(20 AS Numeric(19, 0)), N'402P0300', N'402P0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(21 AS Numeric(19, 0)), N'402P0400', N'402P0400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(22 AS Numeric(19, 0)), N'402P0500', N'402P0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(23 AS Numeric(19, 0)), N'402P0501', N'402P0501', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(24 AS Numeric(19, 0)), N'402P0600', N'402P0600', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(25 AS Numeric(19, 0)), N'402P0601', N'402P0601', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(26 AS Numeric(19, 0)), N'402P0700', N'402P0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(27 AS Numeric(19, 0)), N'402P0800', N'402P0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(28 AS Numeric(19, 0)), N'402P0900', N'402P0900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(29 AS Numeric(19, 0)), N'402P9900', N'402P9900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(30 AS Numeric(19, 0)), N'402Z0101', N'402Z0101', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(31 AS Numeric(19, 0)), N'402Z0102', N'402Z0102', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(32 AS Numeric(19, 0)), N'402Z0103', N'402Z0103', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(33 AS Numeric(19, 0)), N'402Z0104', N'402Z0104', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(34 AS Numeric(19, 0)), N'402Z0105', N'402Z0105', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(35 AS Numeric(19, 0)), N'402Z0106', N'402Z0106', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(36 AS Numeric(19, 0)), N'402Z0107', N'402Z0107', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(37 AS Numeric(19, 0)), N'402Z0108', N'402Z0108', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(38 AS Numeric(19, 0)), N'402Z0109', N'402Z0109', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(39 AS Numeric(19, 0)), N'402Z0110', N'402Z0110', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(40 AS Numeric(19, 0)), N'402Z0111', N'402Z0111', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(41 AS Numeric(19, 0)), N'402Z0112', N'402Z0112', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(42 AS Numeric(19, 0)), N'402Z0113', N'402Z0113', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(43 AS Numeric(19, 0)), N'402Z0114', N'402Z0114', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(44 AS Numeric(19, 0)), N'402Z0115', N'402Z0115', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(45 AS Numeric(19, 0)), N'402Z0119', N'402Z0119', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(46 AS Numeric(19, 0)), N'402Z0201', N'402Z0201', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(47 AS Numeric(19, 0)), N'402Z0202', N'402Z0202', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(48 AS Numeric(19, 0)), N'402Z0203', N'402Z0203', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(49 AS Numeric(19, 0)), N'402Z0204', N'402Z0204', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(50 AS Numeric(19, 0)), N'402Z0205', N'402Z0205', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(51 AS Numeric(19, 0)), N'402Z0206', N'402Z0206', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(52 AS Numeric(19, 0)), N'402Z0207', N'402Z0207', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(53 AS Numeric(19, 0)), N'402Z0208', N'402Z0208', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(54 AS Numeric(19, 0)), N'402Z0209', N'402Z0209', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(55 AS Numeric(19, 0)), N'402Z0210', N'402Z0210', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(56 AS Numeric(19, 0)), N'402Z0211', N'402Z0211', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(57 AS Numeric(19, 0)), N'402Z0212', N'402Z0212', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(58 AS Numeric(19, 0)), N'402Z0213', N'402Z0213', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(59 AS Numeric(19, 0)), N'402Z0214', N'402Z0214', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(60 AS Numeric(19, 0)), N'402Z0215', N'402Z0215', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(61 AS Numeric(19, 0)), N'402Z0219', N'402Z0219', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(62 AS Numeric(19, 0)), N'402Z0301', N'402Z0301', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(63 AS Numeric(19, 0)), N'402Z0302', N'402Z0302', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(64 AS Numeric(19, 0)), N'402Z0399', N'402Z0399', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(65 AS Numeric(19, 0)), N'403P0101', N'403P0101', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(66 AS Numeric(19, 0)), N'403P0102', N'403P0102', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(67 AS Numeric(19, 0)), N'403P0201', N'403P0201', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(68 AS Numeric(19, 0)), N'403P0202', N'403P0202', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(69 AS Numeric(19, 0)), N'403P0203', N'403P0203', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(70 AS Numeric(19, 0)), N'403P0204', N'403P0204', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(71 AS Numeric(19, 0)), N'403P0205', N'403P0205', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(72 AS Numeric(19, 0)), N'403P0206', N'403P0206', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(73 AS Numeric(19, 0)), N'403P0207', N'403P0207', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(74 AS Numeric(19, 0)), N'403P0208', N'403P0208', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(75 AS Numeric(19, 0)), N'403P0211', N'403P0211', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(76 AS Numeric(19, 0)), N'403P0212', N'403P0212', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(77 AS Numeric(19, 0)), N'403P0213', N'403P0213', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(78 AS Numeric(19, 0)), N'403P0214', N'403P0214', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(79 AS Numeric(19, 0)), N'403P0301', N'403P0301', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(80 AS Numeric(19, 0)), N'403P0302', N'403P0302', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(81 AS Numeric(19, 0)), N'403P0303', N'403P0303', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(82 AS Numeric(19, 0)), N'403P0304', N'403P0304', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(83 AS Numeric(19, 0)), N'403P0305', N'403P0305', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(84 AS Numeric(19, 0)), N'403P0399', N'403P0399', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(85 AS Numeric(19, 0)), N'403P0401', N'403P0401', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(86 AS Numeric(19, 0)), N'403P0402', N'403P0402', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(87 AS Numeric(19, 0)), N'403P0403', N'403P0403', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(88 AS Numeric(19, 0)), N'403P0404', N'403P0404', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(89 AS Numeric(19, 0)), N'403P0405', N'403P0405', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(90 AS Numeric(19, 0)), N'403P0406', N'403P0406', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(91 AS Numeric(19, 0)), N'403P0601', N'403P0601', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(92 AS Numeric(19, 0)), N'403P0602', N'403P0602', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(93 AS Numeric(19, 0)), N'403P0700', N'403P0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(94 AS Numeric(19, 0)), N'403P0800', N'403P0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(95 AS Numeric(19, 0)), N'403Z0100', N'403Z0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(96 AS Numeric(19, 0)), N'403Z0101', N'403Z0101', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(97 AS Numeric(19, 0)), N'403Z0102', N'403Z0102', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(98 AS Numeric(19, 0)), N'403Z0103', N'403Z0103', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(99 AS Numeric(19, 0)), N'403Z0201-do2010', N'403Z0201-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(100 AS Numeric(19, 0)), N'403Z0202-do2010', N'403Z0202-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(101 AS Numeric(19, 0)), N'403Z0202', N'403Z0202', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(102 AS Numeric(19, 0)), N'403Z0203-do2010', N'403Z0203-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(103 AS Numeric(19, 0)), N'403Z0203', N'403Z0203', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(104 AS Numeric(19, 0)), N'403Z0204-do2010', N'403Z0204-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(105 AS Numeric(19, 0)), N'403Z0204', N'403Z0204', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(106 AS Numeric(19, 0)), N'403Z0205-do2010', N'403Z0205-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(107 AS Numeric(19, 0)), N'403Z0205', N'403Z0205', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(108 AS Numeric(19, 0)), N'403Z0206-do2010', N'403Z0206-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(109 AS Numeric(19, 0)), N'403Z0206', N'403Z0206', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(110 AS Numeric(19, 0)), N'403Z0207-do2010', N'403Z0207-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(111 AS Numeric(19, 0)), N'403Z0207', N'403Z0207', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(112 AS Numeric(19, 0)), N'403Z0208-do2010', N'403Z0208-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(113 AS Numeric(19, 0)), N'403Z0208', N'403Z0208', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(114 AS Numeric(19, 0)), N'403Z0209-do2010', N'403Z0209-do2010', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(115 AS Numeric(19, 0)), N'403Z0212', N'403Z0212', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(116 AS Numeric(19, 0)), N'403Z0214', N'403Z0214', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(117 AS Numeric(19, 0)), N'403Z0215', N'403Z0215', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(118 AS Numeric(19, 0)), N'403Z0219', N'403Z0219', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(119 AS Numeric(19, 0)), N'403Z0300', N'403Z0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(120 AS Numeric(19, 0)), N'403Z0500', N'403Z0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(121 AS Numeric(19, 0)), N'403Z0600', N'403Z0600', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(122 AS Numeric(19, 0)), N'403Z0700', N'403Z0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(123 AS Numeric(19, 0)), N'403Z0800', N'403Z0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(124 AS Numeric(19, 0)), N'403Z0900', N'403Z0900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(125 AS Numeric(19, 0)), N'403Z1000', N'403Z1000', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(126 AS Numeric(19, 0)), N'403Z1100', N'403Z1100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(127 AS Numeric(19, 0)), N'403Z1201', N'403Z1201', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(128 AS Numeric(19, 0)), N'403Z1202', N'403Z1202', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(129 AS Numeric(19, 0)), N'403Z1203', N'403Z1203', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(130 AS Numeric(19, 0)), N'403Z1204', N'403Z1204', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(131 AS Numeric(19, 0)), N'403Z1301', N'403Z1301', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(132 AS Numeric(19, 0)), N'403Z1302', N'403Z1302', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(133 AS Numeric(19, 0)), N'403Z1303', N'403Z1303', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(134 AS Numeric(19, 0)), N'403Z1304', N'403Z1304', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(135 AS Numeric(19, 0)), N'403Z1305', N'403Z1305', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(136 AS Numeric(19, 0)), N'403Z1306', N'403Z1306', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(137 AS Numeric(19, 0)), N'403Z1307', N'403Z1307', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(138 AS Numeric(19, 0)), N'403Z1308', N'403Z1308', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(139 AS Numeric(19, 0)), N'403Z1309', N'403Z1309', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(140 AS Numeric(19, 0)), N'403Z1310', N'403Z1310', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(141 AS Numeric(19, 0)), N'403Z1311', N'403Z1311', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(142 AS Numeric(19, 0)), N'403Z1312', N'403Z1312', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(143 AS Numeric(19, 0)), N'403Z1313', N'403Z1313', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(144 AS Numeric(19, 0)), N'403Z1314', N'403Z1314', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(145 AS Numeric(19, 0)), N'403Z1315', N'403Z1315', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(146 AS Numeric(19, 0)), N'403Z1319', N'403Z1319', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(147 AS Numeric(19, 0)), N'403Z1400', N'403Z1400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(148 AS Numeric(19, 0)), N'403Z9900', N'403Z9900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(149 AS Numeric(19, 0)), N'404P0100', N'404P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(150 AS Numeric(19, 0)), N'404P0200', N'404P0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(151 AS Numeric(19, 0)), N'404P0300', N'404P0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(152 AS Numeric(19, 0)), N'404P9900', N'404P9900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(153 AS Numeric(19, 0)), N'404Z9900', N'404Z9900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(154 AS Numeric(19, 0)), N'405P0100', N'405P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(155 AS Numeric(19, 0)), N'406P0100', N'406P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(156 AS Numeric(19, 0)), N'406P0400', N'406P0400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(157 AS Numeric(19, 0)), N'406P0500', N'406P0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(158 AS Numeric(19, 0)), N'406P0600', N'406P0600', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(159 AS Numeric(19, 0)), N'406Z0100', N'406Z0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(160 AS Numeric(19, 0)), N'406Z0500', N'406Z0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(161 AS Numeric(19, 0)), N'406Z9900', N'406Z9900', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(162 AS Numeric(19, 0)), N'407P0100', N'407P0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(163 AS Numeric(19, 0)), N'407Z0100', N'407Z0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(164 AS Numeric(19, 0)), N'408P0101', N'408P0101', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(165 AS Numeric(19, 0)), N'408P0199', N'408P0199', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(166 AS Numeric(19, 0)), N'408P0201', N'408P0201', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(167 AS Numeric(19, 0)), N'408P0202', N'408P0202', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(168 AS Numeric(19, 0)), N'408P0300', N'408P0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(169 AS Numeric(19, 0)), N'408Z0100', N'408Z0100', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(170 AS Numeric(19, 0)), N'408Z0201', N'408Z0201', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(171 AS Numeric(19, 0)), N'408Z0300', N'408Z0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(172 AS Numeric(19, 0)), N'408Z0400', N'408Z0400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(173 AS Numeric(19, 0)), N'409P0200', N'409P0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(174 AS Numeric(19, 0)), N'409P0300', N'409P0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(175 AS Numeric(19, 0)), N'409P0400', N'409P0400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(176 AS Numeric(19, 0)), N'409P0401', N'409P0401', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(177 AS Numeric(19, 0)), N'409P0500', N'409P0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(178 AS Numeric(19, 0)), N'409P0600', N'409P0600', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(179 AS Numeric(19, 0)), N'409P0700', N'409P0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(180 AS Numeric(19, 0)), N'409P0800', N'409P0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(181 AS Numeric(19, 0)), N'409Z0200', N'409Z0200', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(182 AS Numeric(19, 0)), N'409Z0300', N'409Z0300', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(183 AS Numeric(19, 0)), N'409Z0400', N'409Z0400', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(184 AS Numeric(19, 0)), N'409Z0401', N'409Z0401', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(185 AS Numeric(19, 0)), N'409Z0500', N'409Z0500', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(186 AS Numeric(19, 0)), N'409Z0600', N'409Z0600', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(187 AS Numeric(19, 0)), N'409Z0700', N'409Z0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(188 AS Numeric(19, 0)), N'409Z0800', N'409Z0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(189 AS Numeric(19, 0)), N'649-101', N'649-101', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(190 AS Numeric(19, 0)), N'649-102', N'649-102', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(191 AS Numeric(19, 0)), N'649-103', N'649-103', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(192 AS Numeric(19, 0)), N'649-104', N'649-104', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(193 AS Numeric(19, 0)), N'649-105', N'649-105', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(194 AS Numeric(19, 0)), N'649-106', N'649-106', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(195 AS Numeric(19, 0)), N'649-107', N'649-107', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(196 AS Numeric(19, 0)), N'649-108', N'649-108', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(197 AS Numeric(19, 0)), N'649-109', N'649-109', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(198 AS Numeric(19, 0)), N'649-110', N'649-110', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(199 AS Numeric(19, 0)), N'649-111', N'649-111', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(200 AS Numeric(19, 0)), N'649-112', N'649-112', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(201 AS Numeric(19, 0)), N'649-113', N'649-113', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(202 AS Numeric(19, 0)), N'649-114', N'649-114', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(203 AS Numeric(19, 0)), N'649-115', N'649-115', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(204 AS Numeric(19, 0)), N'649-116', N'649-116', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(205 AS Numeric(19, 0)), N'649-117', N'649-117', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(206 AS Numeric(19, 0)), N'649-118', N'649-118', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(207 AS Numeric(19, 0)), N'649-119', N'649-119', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(208 AS Numeric(19, 0)), N'649-120', N'649-120', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(209 AS Numeric(19, 0)), N'649-121', N'649-121', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(210 AS Numeric(19, 0)), N'649-122', N'649-122', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(211 AS Numeric(19, 0)), N'649-123', N'649-123', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(212 AS Numeric(19, 0)), N'649-124', N'649-124', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(213 AS Numeric(19, 0)), N'649-125', N'649-125', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(214 AS Numeric(19, 0)), N'649-126', N'649-126', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(215 AS Numeric(19, 0)), N'649-127', N'649-127', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(216 AS Numeric(19, 0)), N'649-128', N'649-128', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(217 AS Numeric(19, 0)), N'649-129', N'649-129', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(218 AS Numeric(19, 0)), N'649-130', N'649-130', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(219 AS Numeric(19, 0)), N'649-131', N'649-131', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(220 AS Numeric(19, 0)), N'649-132', N'649-132', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(221 AS Numeric(19, 0)), N'649-133', N'649-133', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(222 AS Numeric(19, 0)), N'649-134', N'649-134', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(223 AS Numeric(19, 0)), N'649-135', N'649-135', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(224 AS Numeric(19, 0)), N'649-136', N'649-136', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(225 AS Numeric(19, 0)), N'649-137', N'649-137', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(226 AS Numeric(19, 0)), N'649-138', N'649-138', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(227 AS Numeric(19, 0)), N'649-139', N'649-139', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(228 AS Numeric(19, 0)), N'649-140', N'649-140', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(229 AS Numeric(19, 0)), N'649-141', N'649-141', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(230 AS Numeric(19, 0)), N'649-142', N'649-142', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(231 AS Numeric(19, 0)), N'649-143', N'649-143', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(232 AS Numeric(19, 0)), N'649-144', N'649-144', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(233 AS Numeric(19, 0)), N'649-145', N'649-145', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(234 AS Numeric(19, 0)), N'649-146', N'649-146', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(235 AS Numeric(19, 0)), N'649-147', N'649-147', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(236 AS Numeric(19, 0)), N'649-148', N'649-148', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(237 AS Numeric(19, 0)), N'649-149', N'649-149', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(238 AS Numeric(19, 0)), N'649-150', N'649-150', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(239 AS Numeric(19, 0)), N'649-151', N'649-151', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(240 AS Numeric(19, 0)), N'649-152', N'649-152', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(241 AS Numeric(19, 0)), N'649-153', N'649-153', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(242 AS Numeric(19, 0)), N'649-154', N'649-154', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(243 AS Numeric(19, 0)), N'649-155', N'649-155', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(244 AS Numeric(19, 0)), N'649-156', N'649-156', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(245 AS Numeric(19, 0)), N'649-157', N'649-157', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(246 AS Numeric(19, 0)), N'649-158', N'649-158', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(247 AS Numeric(19, 0)), N'649-159', N'649-159', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(248 AS Numeric(19, 0)), N'649-160', N'649-160', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(249 AS Numeric(19, 0)), N'649-161', N'649-161', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(250 AS Numeric(19, 0)), N'649-162', N'649-162', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(251 AS Numeric(19, 0)), N'649-163', N'649-163', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(252 AS Numeric(19, 0)), N'649-164', N'649-164', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(253 AS Numeric(19, 0)), N'649-165', N'649-165', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(254 AS Numeric(19, 0)), N'649-166', N'649-166', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(255 AS Numeric(19, 0)), N'649-167', N'649-167', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(256 AS Numeric(19, 0)), N'649-168', N'649-168', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(257 AS Numeric(19, 0)), N'649-169', N'649-169', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(258 AS Numeric(19, 0)), N'649-170', N'649-170', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(259 AS Numeric(19, 0)), N'649-171', N'649-171', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(260 AS Numeric(19, 0)), N'649-172', N'649-172', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(261 AS Numeric(19, 0)), N'649-173', N'649-173', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(262 AS Numeric(19, 0)), N'649-174', N'649-174', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(263 AS Numeric(19, 0)), N'649-175', N'649-175', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(264 AS Numeric(19, 0)), N'649-176', N'649-176', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(265 AS Numeric(19, 0)), N'649-177', N'649-177', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(266 AS Numeric(19, 0)), N'649-178', N'649-178', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(267 AS Numeric(19, 0)), N'649-179', N'649-179', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(268 AS Numeric(19, 0)), N'649-180', N'649-180', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(269 AS Numeric(19, 0)), N'649-181', N'649-181', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(270 AS Numeric(19, 0)), N'649-182', N'649-182', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(271 AS Numeric(19, 0)), N'649-183', N'649-183', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(272 AS Numeric(19, 0)), N'649-184', N'649-184', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(273 AS Numeric(19, 0)), N'649-185', N'649-185', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(274 AS Numeric(19, 0)), N'649-186', N'649-186', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(275 AS Numeric(19, 0)), N'649-187', N'649-187', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(276 AS Numeric(19, 0)), N'649-188', N'649-188', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(277 AS Numeric(19, 0)), N'649-189', N'649-189', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(278 AS Numeric(19, 0)), N'649-190', N'649-190', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(279 AS Numeric(19, 0)), N'649-191', N'649-191', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(280 AS Numeric(19, 0)), N'649-192', N'649-192', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(281 AS Numeric(19, 0)), N'649-193', N'649-193', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(282 AS Numeric(19, 0)), N'649-194', N'649-194', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(283 AS Numeric(19, 0)), N'649-195', N'649-195', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(284 AS Numeric(19, 0)), N'649-196', N'649-196', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(285 AS Numeric(19, 0)), N'649-197', N'649-197', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(286 AS Numeric(19, 0)), N'649-198', N'649-198', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(287 AS Numeric(19, 0)), N'649-199', N'649-199', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(288 AS Numeric(19, 0)), N'650-402P0700', N'650-402P0700', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(289 AS Numeric(19, 0)), N'650-402P0800', N'650-402P0800', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(290 AS Numeric(19, 0)), N'650-403P0205', N'650-403P0205', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(291 AS Numeric(19, 0)), N'650-403P0206', N'650-403P0206', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(292 AS Numeric(19, 0)), N'650-403P0207', N'650-403P0207', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(293 AS Numeric(19, 0)), N'650-403P0208', N'650-403P0208', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(294 AS Numeric(19, 0)), N'746', N'746', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(295 AS Numeric(19, 0)), N'769-1', N'769-1', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(296 AS Numeric(19, 0)), N'840-1', N'840-1', NULL, 1, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(297 AS Numeric(19, 0)), N'010-1', N'010-1', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(298 AS Numeric(19, 0)), N'010-2', N'010-2', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(299 AS Numeric(19, 0)), N'010-4', N'010-4', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(300 AS Numeric(19, 0)), N'010-7', N'010-7', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(301 AS Numeric(19, 0)), N'010-8', N'010-8', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(302 AS Numeric(19, 0)), N'011-2', N'011-2', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(303 AS Numeric(19, 0)), N'011-4', N'011-4', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(304 AS Numeric(19, 0)), N'011-7', N'011-7', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(305 AS Numeric(19, 0)), N'011-8', N'011-8', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(306 AS Numeric(19, 0)), N'020-1', N'020-1', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(307 AS Numeric(19, 0)), N'020-2', N'020-2', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(308 AS Numeric(19, 0)), N'080-801', N'080-801', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(309 AS Numeric(19, 0)), N'080-802', N'080-802', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(310 AS Numeric(19, 0)), N'401P0100', N'401P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(311 AS Numeric(19, 0)), N'401P0200', N'401P0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(312 AS Numeric(19, 0)), N'401Z0100', N'401Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(313 AS Numeric(19, 0)), N'401Z0200', N'401Z0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(314 AS Numeric(19, 0)), N'402P0100', N'402P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(315 AS Numeric(19, 0)), N'402P0200', N'402P0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(316 AS Numeric(19, 0)), N'402P0300', N'402P0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(317 AS Numeric(19, 0)), N'402P0400', N'402P0400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(318 AS Numeric(19, 0)), N'402P0500', N'402P0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(319 AS Numeric(19, 0)), N'402P0600', N'402P0600', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(320 AS Numeric(19, 0)), N'402P0700', N'402P0700', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(321 AS Numeric(19, 0)), N'402P0800', N'402P0800', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(322 AS Numeric(19, 0)), N'402P0900', N'402P0900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(323 AS Numeric(19, 0)), N'402P9900', N'402P9900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(324 AS Numeric(19, 0)), N'402Z0101', N'402Z0101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(325 AS Numeric(19, 0)), N'402Z0102', N'402Z0102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(326 AS Numeric(19, 0)), N'402Z0103', N'402Z0103', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(327 AS Numeric(19, 0)), N'402Z0104', N'402Z0104', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(328 AS Numeric(19, 0)), N'402Z0105', N'402Z0105', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(329 AS Numeric(19, 0)), N'402Z0106', N'402Z0106', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(330 AS Numeric(19, 0)), N'402Z0107', N'402Z0107', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(331 AS Numeric(19, 0)), N'402Z0108', N'402Z0108', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(332 AS Numeric(19, 0)), N'402Z0109', N'402Z0109', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(333 AS Numeric(19, 0)), N'402Z0110', N'402Z0110', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(334 AS Numeric(19, 0)), N'402Z0111', N'402Z0111', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(335 AS Numeric(19, 0)), N'402Z0112', N'402Z0112', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(336 AS Numeric(19, 0)), N'402Z0113', N'402Z0113', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(337 AS Numeric(19, 0)), N'402Z0114', N'402Z0114', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(338 AS Numeric(19, 0)), N'402Z0115', N'402Z0115', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(339 AS Numeric(19, 0)), N'402Z0120', N'402Z0120', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(340 AS Numeric(19, 0)), N'402Z0121', N'402Z0121', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(341 AS Numeric(19, 0)), N'402Z0122', N'402Z0122', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(342 AS Numeric(19, 0)), N'402Z0123', N'402Z0123', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(343 AS Numeric(19, 0)), N'402Z0201', N'402Z0201', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(344 AS Numeric(19, 0)), N'402Z0202', N'402Z0202', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(345 AS Numeric(19, 0)), N'402Z0203', N'402Z0203', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(346 AS Numeric(19, 0)), N'402Z0204', N'402Z0204', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(347 AS Numeric(19, 0)), N'402Z0205', N'402Z0205', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(348 AS Numeric(19, 0)), N'402Z0206', N'402Z0206', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(349 AS Numeric(19, 0)), N'402Z0207', N'402Z0207', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(350 AS Numeric(19, 0)), N'402Z0208', N'402Z0208', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(351 AS Numeric(19, 0)), N'402Z0209', N'402Z0209', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(352 AS Numeric(19, 0)), N'402Z0210', N'402Z0210', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(353 AS Numeric(19, 0)), N'402Z0211', N'402Z0211', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(354 AS Numeric(19, 0)), N'402Z0212', N'402Z0212', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(355 AS Numeric(19, 0)), N'402Z0213', N'402Z0213', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(356 AS Numeric(19, 0)), N'402Z0214', N'402Z0214', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(357 AS Numeric(19, 0)), N'402Z0215', N'402Z0215', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(358 AS Numeric(19, 0)), N'402Z0220', N'402Z0220', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(359 AS Numeric(19, 0)), N'402Z0221', N'402Z0221', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(360 AS Numeric(19, 0)), N'402Z0222', N'402Z0222', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(361 AS Numeric(19, 0)), N'402Z0223', N'402Z0223', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(362 AS Numeric(19, 0)), N'402Z0301', N'402Z0301', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(363 AS Numeric(19, 0)), N'402Z0302', N'402Z0302', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(364 AS Numeric(19, 0)), N'402Z0399', N'402Z0399', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(365 AS Numeric(19, 0)), N'403P0101', N'403P0101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(366 AS Numeric(19, 0)), N'403P0102', N'403P0102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(367 AS Numeric(19, 0)), N'403P0201', N'403P0201', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(368 AS Numeric(19, 0)), N'403P0202', N'403P0202', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(369 AS Numeric(19, 0)), N'403P0203', N'403P0203', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(370 AS Numeric(19, 0)), N'403P0204', N'403P0204', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(371 AS Numeric(19, 0)), N'403P0205', N'403P0205', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(372 AS Numeric(19, 0)), N'403P0206', N'403P0206', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(373 AS Numeric(19, 0)), N'403P0207', N'403P0207', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(374 AS Numeric(19, 0)), N'403P0208', N'403P0208', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(375 AS Numeric(19, 0)), N'403P0301', N'403P0301', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(376 AS Numeric(19, 0)), N'403P0302', N'403P0302', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(377 AS Numeric(19, 0)), N'403P0303', N'403P0303', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(378 AS Numeric(19, 0)), N'403P0304', N'403P0304', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(379 AS Numeric(19, 0)), N'403P0305', N'403P0305', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(380 AS Numeric(19, 0)), N'403P0399', N'403P0399', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(381 AS Numeric(19, 0)), N'403P0401', N'403P0401', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(382 AS Numeric(19, 0)), N'403P0402', N'403P0402', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(383 AS Numeric(19, 0)), N'403P0403', N'403P0403', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(384 AS Numeric(19, 0)), N'403P0404', N'403P0404', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(385 AS Numeric(19, 0)), N'403P0405', N'403P0405', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(386 AS Numeric(19, 0)), N'403P0406', N'403P0406', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(387 AS Numeric(19, 0)), N'403P0601', N'403P0601', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(388 AS Numeric(19, 0)), N'403P0602', N'403P0602', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(389 AS Numeric(19, 0)), N'403P0700', N'403P0700', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(390 AS Numeric(19, 0)), N'403P0800', N'403P0800', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(391 AS Numeric(19, 0)), N'403Z0100', N'403Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(392 AS Numeric(19, 0)), N'403Z0101', N'403Z0101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(393 AS Numeric(19, 0)), N'403Z0102', N'403Z0102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(394 AS Numeric(19, 0)), N'403Z0103', N'403Z0103', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(395 AS Numeric(19, 0)), N'403Z0201-do2010', N'403Z0201-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(396 AS Numeric(19, 0)), N'403Z0202-do2010', N'403Z0202-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(397 AS Numeric(19, 0)), N'403Z0202', N'403Z0202', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(398 AS Numeric(19, 0)), N'403Z0203-do2010', N'403Z0203-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(399 AS Numeric(19, 0)), N'403Z0203', N'403Z0203', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(400 AS Numeric(19, 0)), N'403Z0204-do2010', N'403Z0204-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(401 AS Numeric(19, 0)), N'403Z0204', N'403Z0204', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(402 AS Numeric(19, 0)), N'403Z0205-do2010', N'403Z0205-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(403 AS Numeric(19, 0)), N'403Z0205', N'403Z0205', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(404 AS Numeric(19, 0)), N'403Z0206-do2010', N'403Z0206-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(405 AS Numeric(19, 0)), N'403Z0206', N'403Z0206', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(406 AS Numeric(19, 0)), N'403Z0207-do2010', N'403Z0207-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(407 AS Numeric(19, 0)), N'403Z0207', N'403Z0207', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(408 AS Numeric(19, 0)), N'403Z0208-do2010', N'403Z0208-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(409 AS Numeric(19, 0)), N'403Z0208', N'403Z0208', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(410 AS Numeric(19, 0)), N'403Z0209-do2010', N'403Z0209-do2010', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(411 AS Numeric(19, 0)), N'403Z0212', N'403Z0212', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(412 AS Numeric(19, 0)), N'403Z0214', N'403Z0214', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(413 AS Numeric(19, 0)), N'403Z0215', N'403Z0215', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(414 AS Numeric(19, 0)), N'403Z0220', N'403Z0220', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(415 AS Numeric(19, 0)), N'403Z0221', N'403Z0221', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(416 AS Numeric(19, 0)), N'403Z0222', N'403Z0222', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(417 AS Numeric(19, 0)), N'403Z0223', N'403Z0223', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(418 AS Numeric(19, 0)), N'403Z0300', N'403Z0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(419 AS Numeric(19, 0)), N'403Z0500', N'403Z0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(420 AS Numeric(19, 0)), N'403Z0600', N'403Z0600', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(421 AS Numeric(19, 0)), N'403Z0700', N'403Z0700', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(422 AS Numeric(19, 0)), N'403Z0800', N'403Z0800', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(423 AS Numeric(19, 0)), N'403Z0900', N'403Z0900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(424 AS Numeric(19, 0)), N'403Z1000', N'403Z1000', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(425 AS Numeric(19, 0)), N'403Z1100', N'403Z1100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(426 AS Numeric(19, 0)), N'403Z1201', N'403Z1201', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(427 AS Numeric(19, 0)), N'403Z1202', N'403Z1202', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(428 AS Numeric(19, 0)), N'403Z1203', N'403Z1203', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(429 AS Numeric(19, 0)), N'403Z1301', N'403Z1301', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(430 AS Numeric(19, 0)), N'403Z1302', N'403Z1302', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(431 AS Numeric(19, 0)), N'403Z1303', N'403Z1303', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(432 AS Numeric(19, 0)), N'403Z1304', N'403Z1304', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(433 AS Numeric(19, 0)), N'403Z1305', N'403Z1305', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(434 AS Numeric(19, 0)), N'403Z1306', N'403Z1306', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(435 AS Numeric(19, 0)), N'403Z1307', N'403Z1307', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(436 AS Numeric(19, 0)), N'403Z1308', N'403Z1308', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(437 AS Numeric(19, 0)), N'403Z1309', N'403Z1309', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(438 AS Numeric(19, 0)), N'403Z1310', N'403Z1310', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(439 AS Numeric(19, 0)), N'403Z1311', N'403Z1311', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(440 AS Numeric(19, 0)), N'403Z1312', N'403Z1312', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(441 AS Numeric(19, 0)), N'403Z1313', N'403Z1313', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(442 AS Numeric(19, 0)), N'403Z1314', N'403Z1314', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(443 AS Numeric(19, 0)), N'403Z1315', N'403Z1315', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(444 AS Numeric(19, 0)), N'403Z1320', N'403Z1320', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(445 AS Numeric(19, 0)), N'403Z1321', N'403Z1321', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(446 AS Numeric(19, 0)), N'403Z1322', N'403Z1322', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(447 AS Numeric(19, 0)), N'403Z1323', N'403Z1323', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(448 AS Numeric(19, 0)), N'403Z1400', N'403Z1400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(449 AS Numeric(19, 0)), N'403Z9900', N'403Z9900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(450 AS Numeric(19, 0)), N'404P0100', N'404P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(451 AS Numeric(19, 0)), N'404P0200', N'404P0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(452 AS Numeric(19, 0)), N'404P0300', N'404P0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(453 AS Numeric(19, 0)), N'404P9900', N'404P9900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(454 AS Numeric(19, 0)), N'404Z9900', N'404Z9900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(455 AS Numeric(19, 0)), N'405P0100', N'405P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(456 AS Numeric(19, 0)), N'405P0101', N'405P0101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(457 AS Numeric(19, 0)), N'405P0102', N'405P0102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(458 AS Numeric(19, 0)), N'405Z0100', N'405Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(459 AS Numeric(19, 0)), N'405Z0102', N'405Z0102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(460 AS Numeric(19, 0)), N'406P0100', N'406P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(461 AS Numeric(19, 0)), N'406P0400', N'406P0400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(462 AS Numeric(19, 0)), N'406P0500', N'406P0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(463 AS Numeric(19, 0)), N'406P0600', N'406P0600', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(464 AS Numeric(19, 0)), N'406Z0100', N'406Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(465 AS Numeric(19, 0)), N'406Z0500', N'406Z0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(466 AS Numeric(19, 0)), N'406Z9900', N'406Z9900', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(467 AS Numeric(19, 0)), N'407P0100', N'407P0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(468 AS Numeric(19, 0)), N'407Z0100', N'407Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(469 AS Numeric(19, 0)), N'408P0101', N'408P0101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(470 AS Numeric(19, 0)), N'408P0199', N'408P0199', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(471 AS Numeric(19, 0)), N'408P0201', N'408P0201', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(472 AS Numeric(19, 0)), N'408P0202', N'408P0202', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(473 AS Numeric(19, 0)), N'408P0300', N'408P0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(474 AS Numeric(19, 0)), N'408Z0100', N'408Z0100', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(475 AS Numeric(19, 0)), N'408Z0201', N'408Z0201', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(476 AS Numeric(19, 0)), N'408Z0300', N'408Z0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(477 AS Numeric(19, 0)), N'408Z0400', N'408Z0400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(478 AS Numeric(19, 0)), N'409P0200', N'409P0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(479 AS Numeric(19, 0)), N'409P0300', N'409P0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(480 AS Numeric(19, 0)), N'409P0400', N'409P0400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(481 AS Numeric(19, 0)), N'409P0401', N'409P0401', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(482 AS Numeric(19, 0)), N'409P0500', N'409P0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(483 AS Numeric(19, 0)), N'409P0600', N'409P0600', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(484 AS Numeric(19, 0)), N'409P0700', N'409P0700', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(485 AS Numeric(19, 0)), N'409P0800', N'409P0800', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(486 AS Numeric(19, 0)), N'409Z0200', N'409Z0200', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(487 AS Numeric(19, 0)), N'409Z0300', N'409Z0300', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(488 AS Numeric(19, 0)), N'409Z0400', N'409Z0400', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(489 AS Numeric(19, 0)), N'409Z0401', N'409Z0401', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(490 AS Numeric(19, 0)), N'409Z0500', N'409Z0500', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(491 AS Numeric(19, 0)), N'409Z0600', N'409Z0600', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(492 AS Numeric(19, 0)), N'409Z0700', N'409Z0700', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(493 AS Numeric(19, 0)), N'409Z0800', N'409Z0800', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(494 AS Numeric(19, 0)), N'649-101', N'649-101', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(495 AS Numeric(19, 0)), N'649-102', N'649-102', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(496 AS Numeric(19, 0)), N'649-103', N'649-103', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(497 AS Numeric(19, 0)), N'649-104', N'649-104', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(498 AS Numeric(19, 0)), N'649-105', N'649-105', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(499 AS Numeric(19, 0)), N'649-106', N'649-106', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(500 AS Numeric(19, 0)), N'649-107', N'649-107', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(501 AS Numeric(19, 0)), N'649-108', N'649-108', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(502 AS Numeric(19, 0)), N'649-109', N'649-109', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(503 AS Numeric(19, 0)), N'649-110', N'649-110', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(504 AS Numeric(19, 0)), N'649-111', N'649-111', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(505 AS Numeric(19, 0)), N'649-112', N'649-112', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(506 AS Numeric(19, 0)), N'649-113', N'649-113', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(507 AS Numeric(19, 0)), N'649-114', N'649-114', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(508 AS Numeric(19, 0)), N'649-115', N'649-115', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(509 AS Numeric(19, 0)), N'649-116', N'649-116', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(510 AS Numeric(19, 0)), N'649-117', N'649-117', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(511 AS Numeric(19, 0)), N'649-118', N'649-118', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(512 AS Numeric(19, 0)), N'649-119', N'649-119', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(513 AS Numeric(19, 0)), N'649-120', N'649-120', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(514 AS Numeric(19, 0)), N'649-121', N'649-121', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(515 AS Numeric(19, 0)), N'649-122', N'649-122', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(516 AS Numeric(19, 0)), N'649-123', N'649-123', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(517 AS Numeric(19, 0)), N'649-124', N'649-124', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(518 AS Numeric(19, 0)), N'649-125', N'649-125', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(519 AS Numeric(19, 0)), N'649-126', N'649-126', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(520 AS Numeric(19, 0)), N'649-127', N'649-127', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(521 AS Numeric(19, 0)), N'649-128', N'649-128', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(522 AS Numeric(19, 0)), N'649-129', N'649-129', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(523 AS Numeric(19, 0)), N'649-130', N'649-130', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(524 AS Numeric(19, 0)), N'649-131', N'649-131', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(525 AS Numeric(19, 0)), N'649-132', N'649-132', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(526 AS Numeric(19, 0)), N'649-133', N'649-133', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(527 AS Numeric(19, 0)), N'649-134', N'649-134', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(528 AS Numeric(19, 0)), N'649-135', N'649-135', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(529 AS Numeric(19, 0)), N'649-136', N'649-136', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(530 AS Numeric(19, 0)), N'649-137', N'649-137', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(531 AS Numeric(19, 0)), N'649-138', N'649-138', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(532 AS Numeric(19, 0)), N'649-139', N'649-139', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(533 AS Numeric(19, 0)), N'649-140', N'649-140', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(534 AS Numeric(19, 0)), N'649-141', N'649-141', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(535 AS Numeric(19, 0)), N'649-142', N'649-142', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(536 AS Numeric(19, 0)), N'649-143', N'649-143', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(537 AS Numeric(19, 0)), N'649-144', N'649-144', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(538 AS Numeric(19, 0)), N'649-145', N'649-145', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(539 AS Numeric(19, 0)), N'649-146', N'649-146', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(540 AS Numeric(19, 0)), N'649-147', N'649-147', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(541 AS Numeric(19, 0)), N'649-148', N'649-148', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(542 AS Numeric(19, 0)), N'649-149', N'649-149', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(543 AS Numeric(19, 0)), N'649-150', N'649-150', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(544 AS Numeric(19, 0)), N'649-151', N'649-151', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(545 AS Numeric(19, 0)), N'649-152', N'649-152', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(546 AS Numeric(19, 0)), N'649-153', N'649-153', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(547 AS Numeric(19, 0)), N'649-154', N'649-154', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(548 AS Numeric(19, 0)), N'649-155', N'649-155', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(549 AS Numeric(19, 0)), N'649-156', N'649-156', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(550 AS Numeric(19, 0)), N'649-157', N'649-157', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(551 AS Numeric(19, 0)), N'649-158', N'649-158', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(552 AS Numeric(19, 0)), N'649-159', N'649-159', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(553 AS Numeric(19, 0)), N'649-160', N'649-160', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(554 AS Numeric(19, 0)), N'649-161', N'649-161', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(555 AS Numeric(19, 0)), N'649-162', N'649-162', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(556 AS Numeric(19, 0)), N'649-163', N'649-163', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(557 AS Numeric(19, 0)), N'649-164', N'649-164', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(558 AS Numeric(19, 0)), N'649-165', N'649-165', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(559 AS Numeric(19, 0)), N'649-166', N'649-166', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(560 AS Numeric(19, 0)), N'649-167', N'649-167', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(561 AS Numeric(19, 0)), N'649-168', N'649-168', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(562 AS Numeric(19, 0)), N'649-169', N'649-169', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(563 AS Numeric(19, 0)), N'649-170', N'649-170', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(564 AS Numeric(19, 0)), N'649-171', N'649-171', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(565 AS Numeric(19, 0)), N'649-172', N'649-172', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(566 AS Numeric(19, 0)), N'649-173', N'649-173', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(567 AS Numeric(19, 0)), N'649-174', N'649-174', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(568 AS Numeric(19, 0)), N'649-175', N'649-175', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(569 AS Numeric(19, 0)), N'649-176', N'649-176', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(570 AS Numeric(19, 0)), N'649-177', N'649-177', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(571 AS Numeric(19, 0)), N'649-178', N'649-178', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(572 AS Numeric(19, 0)), N'649-179', N'649-179', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(573 AS Numeric(19, 0)), N'649-180', N'649-180', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(574 AS Numeric(19, 0)), N'649-181', N'649-181', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(575 AS Numeric(19, 0)), N'649-182', N'649-182', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(576 AS Numeric(19, 0)), N'649-183', N'649-183', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(577 AS Numeric(19, 0)), N'649-184', N'649-184', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(578 AS Numeric(19, 0)), N'649-185', N'649-185', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(579 AS Numeric(19, 0)), N'649-186', N'649-186', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(580 AS Numeric(19, 0)), N'649-187', N'649-187', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(581 AS Numeric(19, 0)), N'649-188', N'649-188', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(582 AS Numeric(19, 0)), N'649-189', N'649-189', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(583 AS Numeric(19, 0)), N'649-190', N'649-190', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(584 AS Numeric(19, 0)), N'649-191', N'649-191', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(585 AS Numeric(19, 0)), N'649-192', N'649-192', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(586 AS Numeric(19, 0)), N'649-193', N'649-193', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(587 AS Numeric(19, 0)), N'649-194', N'649-194', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(588 AS Numeric(19, 0)), N'649-195', N'649-195', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(589 AS Numeric(19, 0)), N'649-196', N'649-196', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(590 AS Numeric(19, 0)), N'649-197', N'649-197', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(591 AS Numeric(19, 0)), N'649-198', N'649-198', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(592 AS Numeric(19, 0)), N'649-199', N'649-199', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(593 AS Numeric(19, 0)), N'746', N'746', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(594 AS Numeric(19, 0)), N'769-1', N'769-1', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(595 AS Numeric(19, 0)), N'840-1', N'840-1', NULL, 2, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(596 AS Numeric(19, 0)), N'010-1', N'010-1', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(597 AS Numeric(19, 0)), N'010-2', N'010-2', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(598 AS Numeric(19, 0)), N'010-4', N'010-4', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(599 AS Numeric(19, 0)), N'010-7', N'010-7', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(600 AS Numeric(19, 0)), N'010-8', N'010-8', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(601 AS Numeric(19, 0)), N'011-2', N'011-2', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(602 AS Numeric(19, 0)), N'011-4', N'011-4', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(603 AS Numeric(19, 0)), N'011-7', N'011-7', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(604 AS Numeric(19, 0)), N'011-8', N'011-8', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(605 AS Numeric(19, 0)), N'020-1', N'020-1', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(606 AS Numeric(19, 0)), N'020-2', N'020-2', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(607 AS Numeric(19, 0)), N'080-801', N'080-801', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(608 AS Numeric(19, 0)), N'080-802', N'080-802', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(609 AS Numeric(19, 0)), N'401P0100', N'401P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(610 AS Numeric(19, 0)), N'401P0200', N'401P0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(611 AS Numeric(19, 0)), N'401Z0100', N'401Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(612 AS Numeric(19, 0)), N'401Z0200', N'401Z0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(613 AS Numeric(19, 0)), N'402P0100', N'402P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(614 AS Numeric(19, 0)), N'402P0200', N'402P0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(615 AS Numeric(19, 0)), N'402P0300', N'402P0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(616 AS Numeric(19, 0)), N'402P0400', N'402P0400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(617 AS Numeric(19, 0)), N'402P0500', N'402P0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(618 AS Numeric(19, 0)), N'402P0600', N'402P0600', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(619 AS Numeric(19, 0)), N'402P0700', N'402P0700', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(620 AS Numeric(19, 0)), N'402P0800', N'402P0800', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(621 AS Numeric(19, 0)), N'402P0900', N'402P0900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(622 AS Numeric(19, 0)), N'402P9900', N'402P9900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(623 AS Numeric(19, 0)), N'402Z0101', N'402Z0101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(624 AS Numeric(19, 0)), N'402Z0102', N'402Z0102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(625 AS Numeric(19, 0)), N'402Z0103', N'402Z0103', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(626 AS Numeric(19, 0)), N'402Z0104', N'402Z0104', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(627 AS Numeric(19, 0)), N'402Z0105', N'402Z0105', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(628 AS Numeric(19, 0)), N'402Z0106', N'402Z0106', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(629 AS Numeric(19, 0)), N'402Z0107', N'402Z0107', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(630 AS Numeric(19, 0)), N'402Z0108', N'402Z0108', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(631 AS Numeric(19, 0)), N'402Z0109', N'402Z0109', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(632 AS Numeric(19, 0)), N'402Z0110', N'402Z0110', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(633 AS Numeric(19, 0)), N'402Z0111', N'402Z0111', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(634 AS Numeric(19, 0)), N'402Z0112', N'402Z0112', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(635 AS Numeric(19, 0)), N'402Z0113', N'402Z0113', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(636 AS Numeric(19, 0)), N'402Z0114', N'402Z0114', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(637 AS Numeric(19, 0)), N'402Z0115', N'402Z0115', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(638 AS Numeric(19, 0)), N'402Z0120', N'402Z0120', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(639 AS Numeric(19, 0)), N'402Z0121', N'402Z0121', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(640 AS Numeric(19, 0)), N'402Z0122', N'402Z0122', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(641 AS Numeric(19, 0)), N'402Z0201', N'402Z0201', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(642 AS Numeric(19, 0)), N'402Z0202', N'402Z0202', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(643 AS Numeric(19, 0)), N'402Z0203', N'402Z0203', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(644 AS Numeric(19, 0)), N'402Z0204', N'402Z0204', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(645 AS Numeric(19, 0)), N'402Z0205', N'402Z0205', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(646 AS Numeric(19, 0)), N'402Z0206', N'402Z0206', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(647 AS Numeric(19, 0)), N'402Z0207', N'402Z0207', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(648 AS Numeric(19, 0)), N'402Z0208', N'402Z0208', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(649 AS Numeric(19, 0)), N'402Z0209', N'402Z0209', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(650 AS Numeric(19, 0)), N'402Z0210', N'402Z0210', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(651 AS Numeric(19, 0)), N'402Z0211', N'402Z0211', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(652 AS Numeric(19, 0)), N'402Z0212', N'402Z0212', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(653 AS Numeric(19, 0)), N'402Z0213', N'402Z0213', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(654 AS Numeric(19, 0)), N'402Z0214', N'402Z0214', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(655 AS Numeric(19, 0)), N'402Z0215', N'402Z0215', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(656 AS Numeric(19, 0)), N'402Z0220', N'402Z0220', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(657 AS Numeric(19, 0)), N'402Z0221', N'402Z0221', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(658 AS Numeric(19, 0)), N'402Z0222', N'402Z0222', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(659 AS Numeric(19, 0)), N'402Z0301', N'402Z0301', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(660 AS Numeric(19, 0)), N'402Z0302', N'402Z0302', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(661 AS Numeric(19, 0)), N'402Z0399', N'402Z0399', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(662 AS Numeric(19, 0)), N'403P0101', N'403P0101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(663 AS Numeric(19, 0)), N'403P0102', N'403P0102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(664 AS Numeric(19, 0)), N'403P0201', N'403P0201', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(665 AS Numeric(19, 0)), N'403P0202', N'403P0202', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(666 AS Numeric(19, 0)), N'403P0203', N'403P0203', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(667 AS Numeric(19, 0)), N'403P0204', N'403P0204', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(668 AS Numeric(19, 0)), N'403P0205', N'403P0205', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(669 AS Numeric(19, 0)), N'403P0206', N'403P0206', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(670 AS Numeric(19, 0)), N'403P0207', N'403P0207', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(671 AS Numeric(19, 0)), N'403P0208', N'403P0208', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(672 AS Numeric(19, 0)), N'403P0301', N'403P0301', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(673 AS Numeric(19, 0)), N'403P0302', N'403P0302', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(674 AS Numeric(19, 0)), N'403P0303', N'403P0303', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(675 AS Numeric(19, 0)), N'403P0304', N'403P0304', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(676 AS Numeric(19, 0)), N'403P0305', N'403P0305', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(677 AS Numeric(19, 0)), N'403P0399', N'403P0399', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(678 AS Numeric(19, 0)), N'403P0401', N'403P0401', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(679 AS Numeric(19, 0)), N'403P0402', N'403P0402', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(680 AS Numeric(19, 0)), N'403P0403', N'403P0403', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(681 AS Numeric(19, 0)), N'403P0404', N'403P0404', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(682 AS Numeric(19, 0)), N'403P0405', N'403P0405', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(683 AS Numeric(19, 0)), N'403P0406', N'403P0406', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(684 AS Numeric(19, 0)), N'403P0601', N'403P0601', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(685 AS Numeric(19, 0)), N'403P0602', N'403P0602', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(686 AS Numeric(19, 0)), N'403P0700', N'403P0700', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(687 AS Numeric(19, 0)), N'403P0800', N'403P0800', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(688 AS Numeric(19, 0)), N'403Z0100', N'403Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(689 AS Numeric(19, 0)), N'403Z0101', N'403Z0101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(690 AS Numeric(19, 0)), N'403Z0102', N'403Z0102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(691 AS Numeric(19, 0)), N'403Z0103', N'403Z0103', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(692 AS Numeric(19, 0)), N'403Z0201-do2010', N'403Z0201-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(693 AS Numeric(19, 0)), N'403Z0202-do2010', N'403Z0202-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(694 AS Numeric(19, 0)), N'403Z0202', N'403Z0202', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(695 AS Numeric(19, 0)), N'403Z0203-do2010', N'403Z0203-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(696 AS Numeric(19, 0)), N'403Z0203', N'403Z0203', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(697 AS Numeric(19, 0)), N'403Z0204-do2010', N'403Z0204-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(698 AS Numeric(19, 0)), N'403Z0204', N'403Z0204', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(699 AS Numeric(19, 0)), N'403Z0205-do2010', N'403Z0205-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(700 AS Numeric(19, 0)), N'403Z0205', N'403Z0205', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(701 AS Numeric(19, 0)), N'403Z0206-do2010', N'403Z0206-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(702 AS Numeric(19, 0)), N'403Z0206', N'403Z0206', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(703 AS Numeric(19, 0)), N'403Z0207-do2010', N'403Z0207-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(704 AS Numeric(19, 0)), N'403Z0207', N'403Z0207', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(705 AS Numeric(19, 0)), N'403Z0208-do2010', N'403Z0208-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(706 AS Numeric(19, 0)), N'403Z0208', N'403Z0208', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(707 AS Numeric(19, 0)), N'403Z0209-do2010', N'403Z0209-do2010', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(708 AS Numeric(19, 0)), N'403Z0212', N'403Z0212', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(709 AS Numeric(19, 0)), N'403Z0214', N'403Z0214', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(710 AS Numeric(19, 0)), N'403Z0215', N'403Z0215', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(711 AS Numeric(19, 0)), N'403Z0220', N'403Z0220', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(712 AS Numeric(19, 0)), N'403Z0221', N'403Z0221', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(713 AS Numeric(19, 0)), N'403Z0222', N'403Z0222', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(714 AS Numeric(19, 0)), N'403Z0300', N'403Z0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(715 AS Numeric(19, 0)), N'403Z0500', N'403Z0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(716 AS Numeric(19, 0)), N'403Z0600', N'403Z0600', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(717 AS Numeric(19, 0)), N'403Z0700', N'403Z0700', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(718 AS Numeric(19, 0)), N'403Z0800', N'403Z0800', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(719 AS Numeric(19, 0)), N'403Z0900', N'403Z0900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(720 AS Numeric(19, 0)), N'403Z1000', N'403Z1000', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(721 AS Numeric(19, 0)), N'403Z1100', N'403Z1100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(722 AS Numeric(19, 0)), N'403Z1201', N'403Z1201', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(723 AS Numeric(19, 0)), N'403Z1202', N'403Z1202', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(724 AS Numeric(19, 0)), N'403Z1203', N'403Z1203', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(725 AS Numeric(19, 0)), N'403Z1301', N'403Z1301', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(726 AS Numeric(19, 0)), N'403Z1302', N'403Z1302', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(727 AS Numeric(19, 0)), N'403Z1303', N'403Z1303', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(728 AS Numeric(19, 0)), N'403Z1304', N'403Z1304', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(729 AS Numeric(19, 0)), N'403Z1305', N'403Z1305', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(730 AS Numeric(19, 0)), N'403Z1306', N'403Z1306', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(731 AS Numeric(19, 0)), N'403Z1307', N'403Z1307', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(732 AS Numeric(19, 0)), N'403Z1308', N'403Z1308', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(733 AS Numeric(19, 0)), N'403Z1309', N'403Z1309', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(734 AS Numeric(19, 0)), N'403Z1310', N'403Z1310', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(735 AS Numeric(19, 0)), N'403Z1311', N'403Z1311', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(736 AS Numeric(19, 0)), N'403Z1312', N'403Z1312', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(737 AS Numeric(19, 0)), N'403Z1313', N'403Z1313', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(738 AS Numeric(19, 0)), N'403Z1314', N'403Z1314', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(739 AS Numeric(19, 0)), N'403Z1315', N'403Z1315', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(740 AS Numeric(19, 0)), N'403Z1320', N'403Z1320', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(741 AS Numeric(19, 0)), N'403Z1321', N'403Z1321', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(742 AS Numeric(19, 0)), N'403Z1322', N'403Z1322', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(743 AS Numeric(19, 0)), N'403Z1400', N'403Z1400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(744 AS Numeric(19, 0)), N'403Z9900', N'403Z9900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(745 AS Numeric(19, 0)), N'404P0100', N'404P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(746 AS Numeric(19, 0)), N'404P0200', N'404P0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(747 AS Numeric(19, 0)), N'404P0300', N'404P0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(748 AS Numeric(19, 0)), N'404P9900', N'404P9900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(749 AS Numeric(19, 0)), N'404Z9900', N'404Z9900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(750 AS Numeric(19, 0)), N'405P0100', N'405P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(751 AS Numeric(19, 0)), N'405P0101', N'405P0101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(752 AS Numeric(19, 0)), N'405P0102', N'405P0102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(753 AS Numeric(19, 0)), N'405Z0100', N'405Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(754 AS Numeric(19, 0)), N'405Z0102', N'405Z0102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(755 AS Numeric(19, 0)), N'406P0100', N'406P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(756 AS Numeric(19, 0)), N'406P0400', N'406P0400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(757 AS Numeric(19, 0)), N'406P0500', N'406P0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(758 AS Numeric(19, 0)), N'406P0600', N'406P0600', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(759 AS Numeric(19, 0)), N'406Z0100', N'406Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(760 AS Numeric(19, 0)), N'406Z0500', N'406Z0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(761 AS Numeric(19, 0)), N'406Z9900', N'406Z9900', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(762 AS Numeric(19, 0)), N'407P0100', N'407P0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(763 AS Numeric(19, 0)), N'407Z0100', N'407Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(764 AS Numeric(19, 0)), N'408P0101', N'408P0101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(765 AS Numeric(19, 0)), N'408P0199', N'408P0199', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(766 AS Numeric(19, 0)), N'408P0201', N'408P0201', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(767 AS Numeric(19, 0)), N'408P0202', N'408P0202', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(768 AS Numeric(19, 0)), N'408P0300', N'408P0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(769 AS Numeric(19, 0)), N'408Z0100', N'408Z0100', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(770 AS Numeric(19, 0)), N'408Z0201', N'408Z0201', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(771 AS Numeric(19, 0)), N'408Z0300', N'408Z0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(772 AS Numeric(19, 0)), N'408Z0400', N'408Z0400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(773 AS Numeric(19, 0)), N'409P0200', N'409P0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(774 AS Numeric(19, 0)), N'409P0300', N'409P0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(775 AS Numeric(19, 0)), N'409P0400', N'409P0400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(776 AS Numeric(19, 0)), N'409P0401', N'409P0401', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(777 AS Numeric(19, 0)), N'409P0500', N'409P0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(778 AS Numeric(19, 0)), N'409P0600', N'409P0600', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(779 AS Numeric(19, 0)), N'409P0700', N'409P0700', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(780 AS Numeric(19, 0)), N'409P0800', N'409P0800', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(781 AS Numeric(19, 0)), N'409Z0200', N'409Z0200', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(782 AS Numeric(19, 0)), N'409Z0300', N'409Z0300', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(783 AS Numeric(19, 0)), N'409Z0400', N'409Z0400', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(784 AS Numeric(19, 0)), N'409Z0401', N'409Z0401', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(785 AS Numeric(19, 0)), N'409Z0500', N'409Z0500', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(786 AS Numeric(19, 0)), N'409Z0600', N'409Z0600', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(787 AS Numeric(19, 0)), N'409Z0700', N'409Z0700', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(788 AS Numeric(19, 0)), N'409Z0800', N'409Z0800', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(789 AS Numeric(19, 0)), N'649-101', N'649-101', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(790 AS Numeric(19, 0)), N'649-102', N'649-102', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(791 AS Numeric(19, 0)), N'649-103', N'649-103', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(792 AS Numeric(19, 0)), N'649-104', N'649-104', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(793 AS Numeric(19, 0)), N'649-105', N'649-105', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(794 AS Numeric(19, 0)), N'649-106', N'649-106', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(795 AS Numeric(19, 0)), N'649-107', N'649-107', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(796 AS Numeric(19, 0)), N'649-108', N'649-108', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(797 AS Numeric(19, 0)), N'649-109', N'649-109', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(798 AS Numeric(19, 0)), N'649-110', N'649-110', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(799 AS Numeric(19, 0)), N'649-111', N'649-111', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(800 AS Numeric(19, 0)), N'649-112', N'649-112', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(801 AS Numeric(19, 0)), N'649-113', N'649-113', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(802 AS Numeric(19, 0)), N'649-114', N'649-114', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(803 AS Numeric(19, 0)), N'649-115', N'649-115', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(804 AS Numeric(19, 0)), N'649-116', N'649-116', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(805 AS Numeric(19, 0)), N'649-117', N'649-117', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(806 AS Numeric(19, 0)), N'649-118', N'649-118', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(807 AS Numeric(19, 0)), N'649-119', N'649-119', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(808 AS Numeric(19, 0)), N'649-120', N'649-120', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(809 AS Numeric(19, 0)), N'649-121', N'649-121', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(810 AS Numeric(19, 0)), N'649-122', N'649-122', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(811 AS Numeric(19, 0)), N'649-123', N'649-123', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(812 AS Numeric(19, 0)), N'649-124', N'649-124', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(813 AS Numeric(19, 0)), N'649-125', N'649-125', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(814 AS Numeric(19, 0)), N'649-126', N'649-126', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(815 AS Numeric(19, 0)), N'649-127', N'649-127', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(816 AS Numeric(19, 0)), N'649-128', N'649-128', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(817 AS Numeric(19, 0)), N'649-129', N'649-129', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(818 AS Numeric(19, 0)), N'649-130', N'649-130', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(819 AS Numeric(19, 0)), N'649-131', N'649-131', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(820 AS Numeric(19, 0)), N'649-132', N'649-132', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(821 AS Numeric(19, 0)), N'649-133', N'649-133', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(822 AS Numeric(19, 0)), N'649-134', N'649-134', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(823 AS Numeric(19, 0)), N'649-135', N'649-135', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(824 AS Numeric(19, 0)), N'649-136', N'649-136', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(825 AS Numeric(19, 0)), N'649-137', N'649-137', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(826 AS Numeric(19, 0)), N'649-138', N'649-138', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(827 AS Numeric(19, 0)), N'649-139', N'649-139', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(828 AS Numeric(19, 0)), N'649-140', N'649-140', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(829 AS Numeric(19, 0)), N'649-141', N'649-141', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(830 AS Numeric(19, 0)), N'649-142', N'649-142', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(831 AS Numeric(19, 0)), N'649-143', N'649-143', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(832 AS Numeric(19, 0)), N'649-144', N'649-144', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(833 AS Numeric(19, 0)), N'649-145', N'649-145', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(834 AS Numeric(19, 0)), N'649-146', N'649-146', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(835 AS Numeric(19, 0)), N'649-147', N'649-147', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(836 AS Numeric(19, 0)), N'649-148', N'649-148', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(837 AS Numeric(19, 0)), N'649-149', N'649-149', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(838 AS Numeric(19, 0)), N'649-150', N'649-150', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(839 AS Numeric(19, 0)), N'649-151', N'649-151', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(840 AS Numeric(19, 0)), N'649-152', N'649-152', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(841 AS Numeric(19, 0)), N'649-153', N'649-153', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(842 AS Numeric(19, 0)), N'649-154', N'649-154', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(843 AS Numeric(19, 0)), N'649-155', N'649-155', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(844 AS Numeric(19, 0)), N'649-156', N'649-156', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(845 AS Numeric(19, 0)), N'649-157', N'649-157', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(846 AS Numeric(19, 0)), N'649-158', N'649-158', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(847 AS Numeric(19, 0)), N'649-159', N'649-159', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(848 AS Numeric(19, 0)), N'649-160', N'649-160', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(849 AS Numeric(19, 0)), N'649-161', N'649-161', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(850 AS Numeric(19, 0)), N'649-162', N'649-162', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(851 AS Numeric(19, 0)), N'649-163', N'649-163', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(852 AS Numeric(19, 0)), N'649-164', N'649-164', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(853 AS Numeric(19, 0)), N'649-165', N'649-165', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(854 AS Numeric(19, 0)), N'649-166', N'649-166', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(855 AS Numeric(19, 0)), N'649-167', N'649-167', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(856 AS Numeric(19, 0)), N'649-168', N'649-168', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(857 AS Numeric(19, 0)), N'649-169', N'649-169', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(858 AS Numeric(19, 0)), N'649-170', N'649-170', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(859 AS Numeric(19, 0)), N'649-171', N'649-171', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(860 AS Numeric(19, 0)), N'649-172', N'649-172', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(861 AS Numeric(19, 0)), N'649-173', N'649-173', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(862 AS Numeric(19, 0)), N'649-174', N'649-174', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(863 AS Numeric(19, 0)), N'649-175', N'649-175', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(864 AS Numeric(19, 0)), N'649-176', N'649-176', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(865 AS Numeric(19, 0)), N'649-177', N'649-177', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(866 AS Numeric(19, 0)), N'649-178', N'649-178', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(867 AS Numeric(19, 0)), N'649-179', N'649-179', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(868 AS Numeric(19, 0)), N'649-180', N'649-180', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(869 AS Numeric(19, 0)), N'649-181', N'649-181', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(870 AS Numeric(19, 0)), N'649-182', N'649-182', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(871 AS Numeric(19, 0)), N'649-183', N'649-183', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(872 AS Numeric(19, 0)), N'649-184', N'649-184', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(873 AS Numeric(19, 0)), N'649-185', N'649-185', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(874 AS Numeric(19, 0)), N'649-186', N'649-186', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(875 AS Numeric(19, 0)), N'649-187', N'649-187', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(876 AS Numeric(19, 0)), N'649-188', N'649-188', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(877 AS Numeric(19, 0)), N'649-189', N'649-189', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(878 AS Numeric(19, 0)), N'649-190', N'649-190', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(879 AS Numeric(19, 0)), N'649-191', N'649-191', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(880 AS Numeric(19, 0)), N'649-192', N'649-192', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(881 AS Numeric(19, 0)), N'649-193', N'649-193', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(882 AS Numeric(19, 0)), N'649-194', N'649-194', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(883 AS Numeric(19, 0)), N'649-195', N'649-195', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(884 AS Numeric(19, 0)), N'649-196', N'649-196', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(885 AS Numeric(19, 0)), N'649-197', N'649-197', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(886 AS Numeric(19, 0)), N'649-198', N'649-198', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(887 AS Numeric(19, 0)), N'649-199', N'649-199', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(888 AS Numeric(19, 0)), N'746', N'746', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(889 AS Numeric(19, 0)), N'769-1', N'769-1', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(890 AS Numeric(19, 0)), N'840-1', N'840-1', NULL, 3, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(891 AS Numeric(19, 0)), N'010-1', N'010-1', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(892 AS Numeric(19, 0)), N'010-2', N'010-2', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(893 AS Numeric(19, 0)), N'010-4', N'010-4', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(894 AS Numeric(19, 0)), N'010-7', N'010-7', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(895 AS Numeric(19, 0)), N'010-8', N'010-8', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(896 AS Numeric(19, 0)), N'011-2', N'011-2', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(897 AS Numeric(19, 0)), N'011-4', N'011-4', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(898 AS Numeric(19, 0)), N'011-7', N'011-7', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(899 AS Numeric(19, 0)), N'011-8', N'011-8', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(900 AS Numeric(19, 0)), N'020-1', N'020-1', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(901 AS Numeric(19, 0)), N'020-2', N'020-2', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(902 AS Numeric(19, 0)), N'080-801', N'080-801', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(903 AS Numeric(19, 0)), N'080-802', N'080-802', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(904 AS Numeric(19, 0)), N'080-803', N'080-803', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(905 AS Numeric(19, 0)), N'080-804', N'080-804', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(906 AS Numeric(19, 0)), N'080-805', N'080-805', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(907 AS Numeric(19, 0)), N'301', N'301', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(908 AS Numeric(19, 0)), N'401P0100', N'401P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(909 AS Numeric(19, 0)), N'401P0200', N'401P0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(910 AS Numeric(19, 0)), N'401Z0100', N'401Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(911 AS Numeric(19, 0)), N'401Z0200', N'401Z0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(912 AS Numeric(19, 0)), N'402P0100', N'402P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(913 AS Numeric(19, 0)), N'402P0101', N'402P0101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(914 AS Numeric(19, 0)), N'402P0102', N'402P0102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(915 AS Numeric(19, 0)), N'402P0200', N'402P0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(916 AS Numeric(19, 0)), N'402P0300', N'402P0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(917 AS Numeric(19, 0)), N'402P0400', N'402P0400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(918 AS Numeric(19, 0)), N'402P0500', N'402P0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(919 AS Numeric(19, 0)), N'402P0501', N'402P0501', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(920 AS Numeric(19, 0)), N'402P0600', N'402P0600', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(921 AS Numeric(19, 0)), N'402P0601', N'402P0601', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(922 AS Numeric(19, 0)), N'402P0700', N'402P0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(923 AS Numeric(19, 0)), N'402P0800', N'402P0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(924 AS Numeric(19, 0)), N'402P0900', N'402P0900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(925 AS Numeric(19, 0)), N'402P1001', N'402P1001', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(926 AS Numeric(19, 0)), N'402P1002', N'402P1002', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(927 AS Numeric(19, 0)), N'402P9900', N'402P9900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(928 AS Numeric(19, 0)), N'402Z0107', N'402Z0107', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(929 AS Numeric(19, 0)), N'402Z0108', N'402Z0108', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(930 AS Numeric(19, 0)), N'402Z0109', N'402Z0109', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(931 AS Numeric(19, 0)), N'402Z0110', N'402Z0110', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(932 AS Numeric(19, 0)), N'402Z0111', N'402Z0111', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(933 AS Numeric(19, 0)), N'402Z0112', N'402Z0112', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(934 AS Numeric(19, 0)), N'402Z0113', N'402Z0113', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(935 AS Numeric(19, 0)), N'402Z0114', N'402Z0114', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(936 AS Numeric(19, 0)), N'402Z0115', N'402Z0115', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(937 AS Numeric(19, 0)), N'402Z0116', N'402Z0116', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(938 AS Numeric(19, 0)), N'402Z0117', N'402Z0117', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(939 AS Numeric(19, 0)), N'402Z0118', N'402Z0118', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(940 AS Numeric(19, 0)), N'402Z0201', N'402Z0201', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(941 AS Numeric(19, 0)), N'402Z0202', N'402Z0202', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(942 AS Numeric(19, 0)), N'402Z0203', N'402Z0203', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(943 AS Numeric(19, 0)), N'402Z0204', N'402Z0204', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(944 AS Numeric(19, 0)), N'402Z0205', N'402Z0205', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(945 AS Numeric(19, 0)), N'402Z0206', N'402Z0206', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(946 AS Numeric(19, 0)), N'402Z0207', N'402Z0207', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(947 AS Numeric(19, 0)), N'402Z0208', N'402Z0208', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(948 AS Numeric(19, 0)), N'402Z0209', N'402Z0209', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(949 AS Numeric(19, 0)), N'402Z0210', N'402Z0210', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(950 AS Numeric(19, 0)), N'402Z0211', N'402Z0211', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(951 AS Numeric(19, 0)), N'402Z0212', N'402Z0212', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(952 AS Numeric(19, 0)), N'402Z0213', N'402Z0213', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(953 AS Numeric(19, 0)), N'402Z0214', N'402Z0214', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(954 AS Numeric(19, 0)), N'402Z0215', N'402Z0215', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(955 AS Numeric(19, 0)), N'402Z0216', N'402Z0216', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(956 AS Numeric(19, 0)), N'402Z0217', N'402Z0217', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(957 AS Numeric(19, 0)), N'402Z0218', N'402Z0218', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(958 AS Numeric(19, 0)), N'402Z0301', N'402Z0301', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(959 AS Numeric(19, 0)), N'402Z0302', N'402Z0302', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(960 AS Numeric(19, 0)), N'402Z0399', N'402Z0399', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(961 AS Numeric(19, 0)), N'403P0101', N'403P0101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(962 AS Numeric(19, 0)), N'403P0102', N'403P0102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(963 AS Numeric(19, 0)), N'403P0201', N'403P0201', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(964 AS Numeric(19, 0)), N'403P0202', N'403P0202', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(965 AS Numeric(19, 0)), N'403P0203', N'403P0203', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(966 AS Numeric(19, 0)), N'403P0204', N'403P0204', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(967 AS Numeric(19, 0)), N'403P0205', N'403P0205', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(968 AS Numeric(19, 0)), N'403P0206', N'403P0206', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(969 AS Numeric(19, 0)), N'403P0207', N'403P0207', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(970 AS Numeric(19, 0)), N'403P0208', N'403P0208', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(971 AS Numeric(19, 0)), N'403P0211', N'403P0211', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(972 AS Numeric(19, 0)), N'403P0212', N'403P0212', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(973 AS Numeric(19, 0)), N'403P0213', N'403P0213', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(974 AS Numeric(19, 0)), N'403P0214', N'403P0214', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(975 AS Numeric(19, 0)), N'403P0301', N'403P0301', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(976 AS Numeric(19, 0)), N'403P0302', N'403P0302', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(977 AS Numeric(19, 0)), N'403P0303', N'403P0303', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(978 AS Numeric(19, 0)), N'403P0304', N'403P0304', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(979 AS Numeric(19, 0)), N'403P0305', N'403P0305', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(980 AS Numeric(19, 0)), N'403P0399', N'403P0399', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(981 AS Numeric(19, 0)), N'403P0401', N'403P0401', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(982 AS Numeric(19, 0)), N'403P0402', N'403P0402', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(983 AS Numeric(19, 0)), N'403P0403', N'403P0403', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(984 AS Numeric(19, 0)), N'403P0404', N'403P0404', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(985 AS Numeric(19, 0)), N'403P0405', N'403P0405', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(986 AS Numeric(19, 0)), N'403P0406', N'403P0406', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(987 AS Numeric(19, 0)), N'403P0601', N'403P0601', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(988 AS Numeric(19, 0)), N'403P0602', N'403P0602', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(989 AS Numeric(19, 0)), N'403P0700', N'403P0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(990 AS Numeric(19, 0)), N'403P0800', N'403P0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(991 AS Numeric(19, 0)), N'403P0901', N'403P0901', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(992 AS Numeric(19, 0)), N'403P0902', N'403P0902', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(993 AS Numeric(19, 0)), N'403Z0100', N'403Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(994 AS Numeric(19, 0)), N'403Z0101', N'403Z0101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(995 AS Numeric(19, 0)), N'403Z0102', N'403Z0102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(996 AS Numeric(19, 0)), N'403Z0202', N'403Z0202', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(997 AS Numeric(19, 0)), N'403Z0203', N'403Z0203', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(998 AS Numeric(19, 0)), N'403Z0204', N'403Z0204', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(999 AS Numeric(19, 0)), N'403Z0205', N'403Z0205', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1000 AS Numeric(19, 0)), N'403Z0206', N'403Z0206', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1001 AS Numeric(19, 0)), N'403Z0207', N'403Z0207', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1002 AS Numeric(19, 0)), N'403Z0208', N'403Z0208', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1003 AS Numeric(19, 0)), N'403Z0212', N'403Z0212', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1004 AS Numeric(19, 0)), N'403Z0214', N'403Z0214', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1005 AS Numeric(19, 0)), N'403Z0215', N'403Z0215', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1006 AS Numeric(19, 0)), N'403Z0216', N'403Z0216', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1007 AS Numeric(19, 0)), N'403Z0217', N'403Z0217', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1008 AS Numeric(19, 0)), N'403Z0218', N'403Z0218', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1009 AS Numeric(19, 0)), N'403Z0300', N'403Z0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1010 AS Numeric(19, 0)), N'403Z0500', N'403Z0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1011 AS Numeric(19, 0)), N'403Z0600', N'403Z0600', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1012 AS Numeric(19, 0)), N'403Z0700', N'403Z0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1013 AS Numeric(19, 0)), N'403Z0800', N'403Z0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1014 AS Numeric(19, 0)), N'403Z0900', N'403Z0900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1015 AS Numeric(19, 0)), N'403Z1000', N'403Z1000', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1016 AS Numeric(19, 0)), N'403Z1100', N'403Z1100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1017 AS Numeric(19, 0)), N'403Z1201', N'403Z1201', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1018 AS Numeric(19, 0)), N'403Z1202', N'403Z1202', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1019 AS Numeric(19, 0)), N'403Z1203', N'403Z1203', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1020 AS Numeric(19, 0)), N'403Z1204', N'403Z1204', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1021 AS Numeric(19, 0)), N'403Z1301', N'403Z1301', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1022 AS Numeric(19, 0)), N'403Z1302', N'403Z1302', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1023 AS Numeric(19, 0)), N'403Z1303', N'403Z1303', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1024 AS Numeric(19, 0)), N'403Z1304', N'403Z1304', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1025 AS Numeric(19, 0)), N'403Z1305', N'403Z1305', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1026 AS Numeric(19, 0)), N'403Z1306', N'403Z1306', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1027 AS Numeric(19, 0)), N'403Z1307', N'403Z1307', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1028 AS Numeric(19, 0)), N'403Z1308', N'403Z1308', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1029 AS Numeric(19, 0)), N'403Z1309', N'403Z1309', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1030 AS Numeric(19, 0)), N'403Z1310', N'403Z1310', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1031 AS Numeric(19, 0)), N'403Z1311', N'403Z1311', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1032 AS Numeric(19, 0)), N'403Z1312', N'403Z1312', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1033 AS Numeric(19, 0)), N'403Z1313', N'403Z1313', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1034 AS Numeric(19, 0)), N'403Z1314', N'403Z1314', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1035 AS Numeric(19, 0)), N'403Z1315', N'403Z1315', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1036 AS Numeric(19, 0)), N'403Z1316', N'403Z1316', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1037 AS Numeric(19, 0)), N'403Z1317', N'403Z1317', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1038 AS Numeric(19, 0)), N'403Z1318', N'403Z1318', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1039 AS Numeric(19, 0)), N'403Z1400', N'403Z1400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1040 AS Numeric(19, 0)), N'403Z9900', N'403Z9900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1041 AS Numeric(19, 0)), N'404P0100', N'404P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1042 AS Numeric(19, 0)), N'404P0200', N'404P0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1043 AS Numeric(19, 0)), N'404P0300', N'404P0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1044 AS Numeric(19, 0)), N'404P9900', N'404P9900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1045 AS Numeric(19, 0)), N'404Z9900', N'404Z9900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1046 AS Numeric(19, 0)), N'405P0100', N'405P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1047 AS Numeric(19, 0)), N'405P0101', N'405P0101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1048 AS Numeric(19, 0)), N'405P0102', N'405P0102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1049 AS Numeric(19, 0)), N'405Z0100', N'405Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1050 AS Numeric(19, 0)), N'405Z0102', N'405Z0102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1051 AS Numeric(19, 0)), N'406P0100', N'406P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1052 AS Numeric(19, 0)), N'406P0400', N'406P0400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1053 AS Numeric(19, 0)), N'406P0500', N'406P0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1054 AS Numeric(19, 0)), N'406P0600', N'406P0600', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1055 AS Numeric(19, 0)), N'406P0700', N'406P0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1056 AS Numeric(19, 0)), N'406Z0100', N'406Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1057 AS Numeric(19, 0)), N'406Z0500', N'406Z0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1058 AS Numeric(19, 0)), N'406Z9900', N'406Z9900', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1059 AS Numeric(19, 0)), N'407P0100', N'407P0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1060 AS Numeric(19, 0)), N'407Z0100', N'407Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1061 AS Numeric(19, 0)), N'408P0101', N'408P0101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1062 AS Numeric(19, 0)), N'408P0199', N'408P0199', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1063 AS Numeric(19, 0)), N'408P0201', N'408P0201', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1064 AS Numeric(19, 0)), N'408P0202', N'408P0202', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1065 AS Numeric(19, 0)), N'408P0300', N'408P0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1066 AS Numeric(19, 0)), N'408Z0100', N'408Z0100', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1067 AS Numeric(19, 0)), N'408Z0201', N'408Z0201', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1068 AS Numeric(19, 0)), N'408Z0300', N'408Z0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1069 AS Numeric(19, 0)), N'408Z0400', N'408Z0400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1070 AS Numeric(19, 0)), N'409P0200', N'409P0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1071 AS Numeric(19, 0)), N'409P0300', N'409P0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1072 AS Numeric(19, 0)), N'409P0400', N'409P0400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1073 AS Numeric(19, 0)), N'409P0401', N'409P0401', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1074 AS Numeric(19, 0)), N'409P0500', N'409P0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1075 AS Numeric(19, 0)), N'409P0600', N'409P0600', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1076 AS Numeric(19, 0)), N'409P0700', N'409P0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1077 AS Numeric(19, 0)), N'409P0800', N'409P0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1078 AS Numeric(19, 0)), N'409Z0200', N'409Z0200', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1079 AS Numeric(19, 0)), N'409Z0300', N'409Z0300', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1080 AS Numeric(19, 0)), N'409Z0400', N'409Z0400', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1081 AS Numeric(19, 0)), N'409Z0401', N'409Z0401', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1082 AS Numeric(19, 0)), N'409Z0500', N'409Z0500', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1083 AS Numeric(19, 0)), N'409Z0600', N'409Z0600', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1084 AS Numeric(19, 0)), N'409Z0700', N'409Z0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1085 AS Numeric(19, 0)), N'409Z0800', N'409Z0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1086 AS Numeric(19, 0)), N'649-101', N'649-101', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1087 AS Numeric(19, 0)), N'649-102', N'649-102', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1088 AS Numeric(19, 0)), N'649-103', N'649-103', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1089 AS Numeric(19, 0)), N'649-104', N'649-104', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1090 AS Numeric(19, 0)), N'649-105', N'649-105', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1091 AS Numeric(19, 0)), N'649-106', N'649-106', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1092 AS Numeric(19, 0)), N'649-107', N'649-107', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1093 AS Numeric(19, 0)), N'649-108', N'649-108', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1094 AS Numeric(19, 0)), N'649-109', N'649-109', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1095 AS Numeric(19, 0)), N'649-110', N'649-110', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1096 AS Numeric(19, 0)), N'649-111', N'649-111', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1097 AS Numeric(19, 0)), N'649-112', N'649-112', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1098 AS Numeric(19, 0)), N'649-113', N'649-113', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1099 AS Numeric(19, 0)), N'649-114', N'649-114', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1100 AS Numeric(19, 0)), N'649-115', N'649-115', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1101 AS Numeric(19, 0)), N'649-116', N'649-116', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1102 AS Numeric(19, 0)), N'649-117', N'649-117', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1103 AS Numeric(19, 0)), N'649-118', N'649-118', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1104 AS Numeric(19, 0)), N'649-119', N'649-119', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1105 AS Numeric(19, 0)), N'649-120', N'649-120', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1106 AS Numeric(19, 0)), N'649-121', N'649-121', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1107 AS Numeric(19, 0)), N'649-122', N'649-122', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1108 AS Numeric(19, 0)), N'649-123', N'649-123', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1109 AS Numeric(19, 0)), N'649-124', N'649-124', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1110 AS Numeric(19, 0)), N'649-125', N'649-125', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1111 AS Numeric(19, 0)), N'649-126', N'649-126', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1112 AS Numeric(19, 0)), N'649-127', N'649-127', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1113 AS Numeric(19, 0)), N'649-128', N'649-128', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1114 AS Numeric(19, 0)), N'649-129', N'649-129', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1115 AS Numeric(19, 0)), N'649-130', N'649-130', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1116 AS Numeric(19, 0)), N'649-131', N'649-131', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1117 AS Numeric(19, 0)), N'649-132', N'649-132', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1118 AS Numeric(19, 0)), N'649-133', N'649-133', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1119 AS Numeric(19, 0)), N'649-134', N'649-134', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1120 AS Numeric(19, 0)), N'649-135', N'649-135', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1121 AS Numeric(19, 0)), N'649-136', N'649-136', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1122 AS Numeric(19, 0)), N'649-137', N'649-137', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1123 AS Numeric(19, 0)), N'649-138', N'649-138', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1124 AS Numeric(19, 0)), N'649-139', N'649-139', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1125 AS Numeric(19, 0)), N'649-140', N'649-140', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1126 AS Numeric(19, 0)), N'649-141', N'649-141', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1127 AS Numeric(19, 0)), N'649-142', N'649-142', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1128 AS Numeric(19, 0)), N'649-143', N'649-143', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1129 AS Numeric(19, 0)), N'649-144', N'649-144', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1130 AS Numeric(19, 0)), N'649-145', N'649-145', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1131 AS Numeric(19, 0)), N'649-146', N'649-146', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1132 AS Numeric(19, 0)), N'649-147', N'649-147', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1133 AS Numeric(19, 0)), N'649-148', N'649-148', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1134 AS Numeric(19, 0)), N'649-149', N'649-149', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1135 AS Numeric(19, 0)), N'649-150', N'649-150', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1136 AS Numeric(19, 0)), N'649-151', N'649-151', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1137 AS Numeric(19, 0)), N'649-152', N'649-152', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1138 AS Numeric(19, 0)), N'649-153', N'649-153', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1139 AS Numeric(19, 0)), N'649-154', N'649-154', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1140 AS Numeric(19, 0)), N'649-155', N'649-155', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1141 AS Numeric(19, 0)), N'649-156', N'649-156', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1142 AS Numeric(19, 0)), N'649-157', N'649-157', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1143 AS Numeric(19, 0)), N'649-158', N'649-158', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1144 AS Numeric(19, 0)), N'649-159', N'649-159', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1145 AS Numeric(19, 0)), N'649-160', N'649-160', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1146 AS Numeric(19, 0)), N'649-161', N'649-161', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1147 AS Numeric(19, 0)), N'649-162', N'649-162', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1148 AS Numeric(19, 0)), N'649-163', N'649-163', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1149 AS Numeric(19, 0)), N'649-164', N'649-164', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1150 AS Numeric(19, 0)), N'649-165', N'649-165', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1151 AS Numeric(19, 0)), N'649-166', N'649-166', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1152 AS Numeric(19, 0)), N'649-167', N'649-167', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1153 AS Numeric(19, 0)), N'649-168', N'649-168', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1154 AS Numeric(19, 0)), N'649-169', N'649-169', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1155 AS Numeric(19, 0)), N'649-170', N'649-170', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1156 AS Numeric(19, 0)), N'649-171', N'649-171', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1157 AS Numeric(19, 0)), N'649-172', N'649-172', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1158 AS Numeric(19, 0)), N'649-173', N'649-173', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1159 AS Numeric(19, 0)), N'649-174', N'649-174', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1160 AS Numeric(19, 0)), N'649-175', N'649-175', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1161 AS Numeric(19, 0)), N'649-176', N'649-176', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1162 AS Numeric(19, 0)), N'649-177', N'649-177', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1163 AS Numeric(19, 0)), N'649-178', N'649-178', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1164 AS Numeric(19, 0)), N'649-179', N'649-179', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1165 AS Numeric(19, 0)), N'649-180', N'649-180', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1166 AS Numeric(19, 0)), N'649-181', N'649-181', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1167 AS Numeric(19, 0)), N'649-182', N'649-182', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1168 AS Numeric(19, 0)), N'649-183', N'649-183', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1169 AS Numeric(19, 0)), N'649-184', N'649-184', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1170 AS Numeric(19, 0)), N'649-185', N'649-185', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1171 AS Numeric(19, 0)), N'649-186', N'649-186', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1172 AS Numeric(19, 0)), N'649-187', N'649-187', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1173 AS Numeric(19, 0)), N'649-188', N'649-188', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1174 AS Numeric(19, 0)), N'649-189', N'649-189', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1175 AS Numeric(19, 0)), N'649-190', N'649-190', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1176 AS Numeric(19, 0)), N'649-191', N'649-191', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1177 AS Numeric(19, 0)), N'649-192', N'649-192', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1178 AS Numeric(19, 0)), N'649-193', N'649-193', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1179 AS Numeric(19, 0)), N'649-194', N'649-194', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1180 AS Numeric(19, 0)), N'649-195', N'649-195', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1181 AS Numeric(19, 0)), N'649-196', N'649-196', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1182 AS Numeric(19, 0)), N'649-197', N'649-197', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1183 AS Numeric(19, 0)), N'649-198', N'649-198', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1184 AS Numeric(19, 0)), N'649-199', N'649-199', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1185 AS Numeric(19, 0)), N'650-402P0700', N'650-402P0700', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1186 AS Numeric(19, 0)), N'650-402P0800', N'650-402P0800', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1187 AS Numeric(19, 0)), N'650-403P0205', N'650-403P0205', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1188 AS Numeric(19, 0)), N'650-403P0206', N'650-403P0206', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1189 AS Numeric(19, 0)), N'650-403P0207', N'650-403P0207', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1190 AS Numeric(19, 0)), N'650-403P0208', N'650-403P0208', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1191 AS Numeric(19, 0)), N'746', N'746', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1192 AS Numeric(19, 0)), N'769-1', N'769-1', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1193 AS Numeric(19, 0)), N'840-1', N'840-1', NULL, 4, 1)
GO
INSERT [dsg_pz_produkt_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1194 AS Numeric(19, 0)), N'840-1R', N'840-1R', NULL, 4, 1)
GO
SET IDENTITY_INSERT [dsg_pz_produkt_raw] OFF
GO
SET IDENTITY_INSERT [dsg_pz_typy_dokumentow_raw] ON 

GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(1 AS Numeric(19, 0)), N'KZ1', N'234 - Pracownik - Gotówka VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(2 AS Numeric(19, 0)), N'KZ2', N'234 - Pracownik - Gotówka Rachunek - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(3 AS Numeric(19, 0)), N'KZ3', N'202 - Dostawca - Gotówka VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(4 AS Numeric(19, 0)), N'KZ4', N'202 - Dostawca - Gotówka Rachunek - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(5 AS Numeric(19, 0)), N'KZ5', N'150 - Bez Zaliczki - Gotówka VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(6 AS Numeric(19, 0)), N'KZ6', N'150 - Bez Zaliczki - Gotówka Rach. - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(7 AS Numeric(19, 0)), N'KZV2', N'235 - Pracownik - Gotówka Rachunek - Wal', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(8 AS Numeric(19, 0)), N'KZV6', N'152 - Bez Zaliczki - Gotówka Rac - Walut', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(9 AS Numeric(19, 0)), N'V', N'204 - Dostawca - Waluta', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(10 AS Numeric(19, 0)), N'V2', N'235 - Pracownik - Waluta', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(11 AS Numeric(19, 0)), N'VI1', N'210 - Karta - Rachunek - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(12 AS Numeric(19, 0)), N'VI2', N'210 - Karta - Faktura VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(13 AS Numeric(19, 0)), N'VI3', N'210 - Karta - Waluta', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(14 AS Numeric(19, 0)), N'VI4', N'210 - Karta - Faktura NPD - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(15 AS Numeric(19, 0)), N'VV', N'204 - Dostawca - Waluta - z VAT', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(16 AS Numeric(19, 0)), N'Z1', N'202 - Dostawca - Faktura VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(17 AS Numeric(19, 0)), N'Z2', N'202 - Dostawca - Rachunek - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(18 AS Numeric(19, 0)), N'Z3', N'202 - Naprawa powyp. - Faktura VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(19 AS Numeric(19, 0)), N'Z4', N'234 - Pracownik - Faktura VAT - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(20 AS Numeric(19, 0)), N'Z5', N'234 - Pracownik - Rachunek - PLN', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(21 AS Numeric(19, 0)), N'ZZBO', N'BO techniczne dla korekt', NULL, NULL, 1)
GO
INSERT [dsg_pz_typy_dokumentow_raw] ([id], [cn], [title], [centrum], [refValue], [available]) VALUES (CAST(22 AS Numeric(19, 0)), N'ZZBO_W', N'BO techniczne dla korekt', NULL, NULL, 1)
GO
SET IDENTITY_INSERT [dsg_pz_typy_dokumentow_raw] OFF
GO
