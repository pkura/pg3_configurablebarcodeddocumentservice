CREATE TABLE dsg_pz_komorka_raw(
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [int] NULL,
	[available] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

insert into dsg_pz_komorka_raw (cn,title,refValue,available)
select cn,title,1,1 from dsg_pz_komorka_porthol
union all
select cn,title,2,1 from dsg_pz_komorka_project
union all
select cn,title,3,1 from dsg_pz_komorka_project_gd
union all
select cn,title,4,1 from dsg_pz_komorka_wuz

alter view dsg_pz_komorka as
select id,cn,title,available,centrum,refValue
from dsg_pz_komorka_raw
union all
select id,cn,title,available,centrum,5
from dsg_pz_komorka_raw
where refValue=1