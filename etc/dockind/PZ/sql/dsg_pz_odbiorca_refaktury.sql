CREATE TABLE dsg_pz_odbiorca_refaktury_raw(
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](255) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [int] NULL,
	[available] [int] NULL,
	[miasto] varchar(50) null,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

insert into dsg_pz_odbiorca_refaktury_raw (cn,title,miasto,refValue,available)
select cn,title,miasto,1,1 from dsg_pz_odbiorca_refaktury_porthol
union all
select cn,title,miasto,2,1 from dsg_pz_odbiorca_refaktury_project
union all
select cn,title,miasto,3,1 from dsg_pz_odbiorca_refaktury_project_gd
union all
select cn,title,miasto,4,1 from dsg_pz_odbiorca_refaktury_wuz

alter view dsg_pz_odbiorca_refaktury as
select id,cn,title,available,centrum,refValue
from dsg_pz_odbiorca_refaktury_raw
union all
select id,cn,title,available,centrum,5
from dsg_pz_odbiorca_refaktury_raw
where refValue=1