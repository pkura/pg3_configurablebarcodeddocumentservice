create view dsg_pz_dzial_operatora as
select
id,
guid as cn,
name as title,
null as refValue,
null as centrum,
1-hidden as available
from ds_division
where code in ('O','T','P','A')