CREATE TABLE dsg_pz_faktura_zakupowa (
document_id numeric(18, 0),
status numeric(18, 0),
erp numeric(18, 0),
firma numeric(18, 0),
nr_umowy varchar(50),
typ_faktury numeric(18, 0),
nr_dokumentu varchar(50),
data_wystawienia date,
nr_zamowienia varchar(50),
data_platnosci date,
kwota_netto numeric(18, 2),
kwota_brutto numeric(18, 2),
kwota_vat numeric(18, 2),
zaplacono numeric(18, 0),
tresc_faktury numeric(18, 0),
do_zaplaty_pozostaje numeric(18, 2),
powody_zatrzymania_zaplaty varchar(4096),
czlonek_zarzadu numeric(18, 0),
dzial_operatora numeric(18, 0),
dzial_operatora_pracownik numeric(18, 0),
numer_dekretu varchar(50),
pozostali numeric(18, 0),
pracownik_porthol numeric(18, 0),
pracownik_project numeric(18, 0),
pracownik_project_gd numeric(18, 0),
pracownik_wuz numeric(18, 0),
grupa numeric(18, 0),
koszty_do_rozpisania numeric(19,2)
)

CREATE TABLE dsg_pz_podzial_kosztow_dict (
id numeric (18,0) IDENTITY(1,1) NOT NULL,
dzialalnosc numeric(19, 0),
mpk numeric(19, 0),
rodzaj_kosztu numeric(19, 0),
odbiorca_refaktury numeric(19, 0),
--dzialalnosc_porthol numeric(18, 0),
--dzialalnosc_project numeric(18, 0),
--dzialalnosc_project_gd numeric(18, 0),
--dzialalnosc_wuz numeric(18, 0),
--mpk_porthol numeric(18, 0),
--mpk_project numeric(18, 0),
--mpk_project_gd numeric(18, 0),
--mpk_wuz numeric(18, 0),
--rodzaj_kosztu_porthol numeric(18, 0),
--rodzaj_kosztu_project numeric(18, 0),
--rodzaj_kosztu_project_gd numeric(18, 0),
--rodzaj_kosztu_wuz numeric(18, 0),
--odbiorca_refaktury_porthol numeric(18, 0),
--odbiorca_refaktury_project numeric(18, 0),
--odbiorca_refaktury_project_gd numeric(18, 0),
--odbiorca_refaktury_wuz numeric(18, 0),
kwota numeric(18, 2),
procent numeric(18, 0),
stawka_vat numeric(18, 0),
opis varchar(4096)
)

--CREATE TABLE dsg_pz_odbiorcy_refaktury_dict(
--id numeric (18,0) IDENTITY(1,1) NOT NULL,
--odbiorca numeric(18, 0),
--)

CREATE TABLE dsg_pz_tresc_faktury_dict(
id numeric (18,0) IDENTITY(1,1) NOT NULL,
opis_faktury varchar(4096)
)

CREATE TABLE dsg_pz_dokonane_platnosci_dict(
id numeric (18,0) IDENTITY (1,1) NOT NULL,
data date,
kwota numeric(18, 2)
)

CREATE TABLE dsg_pz_faktura_zakupowa_multiple(
	DOCUMENT_ID numeric(18, 0) NOT NULL,
	FIELD_CN varchar(100) NOT NULL,
	FIELD_VAL varchar(100) NULL
)