CREATE TABLE [dbo].[dsg_pz_archiwum](
	[document_id] [numeric](19, 0) NOT NULL,
	[sygnatura] [varchar](50) NULL,
	[status_archiwum] [int] NULL,
	[nazwa_dokumentu] [varchar](50) NULL,
	[opis] [varchar](8000) NULL,
	[osoba_odpowiedzialna] [int] NULL,
	[osoba_zstepujaca] [int] NULL,
	[data_powiadomienia] [datetime] NULL,
	[powiadomienie_uruchomione] [bit],
	[typ] [int] NULL,
	[spolka] [int] NULL,
	[data_zawarcia] [datetime] NULL,
	[miejsce_przechowywania] [int] NULL,
	[repertorium] [varchar](250) NULL,
	[umowa_firma_z_grupy] [int] NULL,
	[data_waznosci] [datetime] NULL,
	[data_wprowadzenia] [datetime] NULL,
	[jednsotka] [int] NULL,
	[umowa_firma_spoza_grupy] [int] NULL,
	[holowanie_morskie] [int] NULL,
	[holowanie_portowe] [int] NULL,
	[temat_kontroli] [varchar](250) NULL,
	[nr_kontroli] [varchar](250) NULL,
	[contractor] [numeric](19, 0) NULL,
	[data_obowiazywania_od] [datetime] NULL,
    [data_obowiazywania_do] [datetime] NULL,
    [okres_wypowiedzenie] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

