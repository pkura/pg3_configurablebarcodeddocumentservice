CREATE TABLE [dbo].[dso_mail_weight] (
  [id]       [numeric](19, 0) NOT NULL,
  [cn]       [varchar](30)    NOT NULL,
  [name]     [varchar](30)    NOT NULL,
  [weightG]  [smallint]       NULL,
  [weightKg] [smallint]       NULL,
  PRIMARY KEY CLUSTERED (
    [id] ASC
  )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
    ON [PRIMARY]
) ON [PRIMARY]

GO

INSERT INTO dso_mail_weight (id, cn, name, weightG) VALUES
(1, 'weight_50', 'Do 50g', 50),
(2, 'weight_50-100', '51g - 100g', 100),
(3, 'weight_100-350', '101g - 350g', 350),
(4, 'weight_350-500', '351g - 500g', 500);

INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(5, 'weight_500-1000', '501g - 1000g', 1000, 1),
(6, 'weight_0-350', 'Do 350g', 350, 0),
(7, 'weight_350-1000', '351g - 1000g', 1000, 1),
(8, 'weight_1000-2000', '1001g - 2000g', 2000, 2);


INSERT INTO dso_mail_weight (id, cn, name, weightG, weightKg) VALUES
(9, 'weight_2000-2500', '2001g - 2500g', 2500, 2),
(10, 'weight_2500-3000', '2501g - 3000g', 3000, 3),
(11, 'weight_3000-3500', '3001g - 3500g', 3500, 3),
(12, 'weight_3500-4000', '3501g - 4000g', 4000, 4);
(13, 'weight_4000-4500', '4001g - 4500g', 4500, 4),
(14, 'weight_4500-5000', '4501g - 5000g', 5000, 5);
