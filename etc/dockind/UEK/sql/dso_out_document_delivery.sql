update DSO_OUT_DOCUMENT set delivery=null
DELETE FROM dso_out_document_delivery;

SET IDENTITY_INSERT [dbo].[dso_out_document_delivery] ON

-- Do kolumny POSN wstawiamy odpowiedni poziomListu (patrz DS_STAWKI_OPLAT_POCZTOWYCH.sql) --
INSERT INTO dso_out_document_delivery (id, posn, name) VALUES
(1, 1, 'Krajowy zwyk�y ekonomiczny'),
(2, 1, 'Krajowy zwyk�y priorytet'),
(3, 4, 'Krajowy polecony ekonomiczny'),
(4, 4, 'Krajowy polecony priorytet'),
(5, 3, 'Przesy�ki reklamowe'),
(6, 2, 'Zagraniczny zwyk�y ekonomiczny'),
(7, 2, 'Zagraniczny zwyk�y priorytet'),
--(8, 5, 'Zagraniczny polecony ekonomiczny'),
(9, 5, 'Zagraniczny polecony priorytet'),
(10, 6, 'Paczka'),
(11, 7, 'ePUAP'),
(12, 7, 'Kurier'),
(13, 8, 'Anulowano'),
(14, 9, 'Kartka pocztowa ekonomiczna'),
(15, 9, 'Kartka pocztowa priorytet'),
(16, 10, 'Przesy�ka pobraniowa')

SET IDENTITY_INSERT [dbo].[dso_out_document_delivery] OFF


delete from dso_out_document_delivery ehre id = 8;

