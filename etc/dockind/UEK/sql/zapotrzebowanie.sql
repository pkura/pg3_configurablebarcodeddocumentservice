
CREATE TABLE [dbo].[dsg_uek_zapotrzebowanie](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[RODZAJ_WNIOSKU] [numeric](18, 0) NULL,
	[TRYB_ZAMOWIENIA] [numeric](18, 0) NULL,
	[WS_KOD] [varchar](250) NULL,
	[WS_OBSZAR] [varchar](250) NULL,
	[WS_KWOTA] [numeric](18, 2) NULL,
	[WYCENA] [numeric](18, 2) NULL,
	[OPIS_ZAMOWIENIA] [varchar](1024) NULL,
	[JEDNOSTKA_ZGLASZAJACA] [numeric](18, 0) NULL,
	[WNIOSKODAWCA] [numeric](18, 0) NULL,
	[PRZEZNACZENIE_ZAKUPU] [numeric](18, 0) NULL,
	[WARTOSC_NETTO_TOTAL] [numeric](18, 2) NULL,
	[UZASADNIENIE_ZAKUPU] [varchar](1024) NULL,
	[TO_WINWEST] [numeric](19, 0) NULL,
	[BUDGET_KIND] [numeric](18, 0) NULL,
	[BUDGET] [numeric](19, 0) NULL,
	[POSITION] [numeric](19, 0) NULL,
	[SOURCE] [numeric](19, 0) NULL,
	[CONTRACT] [numeric](19, 0) NULL,
	[CONTRACT_STAGE] [numeric](19, 0) NULL,
	[FUND] [numeric](19, 0) NULL,
	[PROJECT_BUDGET] [numeric](19, 0) NULL,
	[PRODUKTY] [numeric](19, 0) NULL,
	[NAZWA_PRZEDMIOTU] [varchar](250) NULL,
	[ILOSC] [int] NULL,
	[WARTOSC_NETTO] [numeric](18, 2) NULL,
	[RODZAJ_DZIALALNOSCI] [numeric](19, 0) NULL,
	[DATA_POZYSKANIA] [datetime] NULL,
	[ENTRY_ID] [numeric](18, 0) NULL,
	[SRODKI_BOOL] [bit] NULL,
	[SRODKI_STRING] [varchar](250) NULL,
	[FINANCIAL_TASK] [numeric](19, 0) NULL,
	[BUDGET_KO] [numeric](19, 0) NULL,
	[ERP_DOCUMENT_IDM] [varchar](250) NULL,
    [ERP_STATUS] [int] NULL,
 CONSTRAINT [PK_dsg_uek_zapotrzebowanie] PRIMARY KEY CLUSTERED
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE dsg_uek_zapotrzebowanie
ADD ERP_STATUS INTEGER NULL,
erp_document_idm VARCHAR(50) NULL

GO


---zapotrzebowanie tabela multi-
CREATE TABLE [dbo].[dsg_uek_zapotrzebowanie_multiple_value](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

GO

---s�ownik przedmiot�w zapotzrebowania
CREATE TABLE [dbo].[dsg_uek_slownik_przedmiotow_zapotrzebowania](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[PRODUKTY] [numeric](19, 0) NULL,
	[NAZWA_PRZEDMIOTU] [varchar](250) NULL,
	[ILOSC] [int] NULL,
	[WARTOSC_NETTO] [numeric](18, 2) NULL,
	[RODZAJ_DZIALALNOSCI] [numeric](19, 0) NULL,
	[DATA_POZYSKANIA] [datetime] NULL,
	[PRZEZNACZENIE_ZAKUPU] [numeric](18, 0) NULL,
	[SRODKI_BOOL] [bit] NULL,
	[SRODKI_STRING] [varchar](250) NULL,
	[DESCRIPTION] [varchar](2000) NULL,
	[ZRODLO_ID] [numeric](19, 0) NULL,
PRIMARY KEY CLUSTERED
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


-- tabele Views

CREATE VIEW dsg_uek_purchase_document_type AS
	SELECT
		id,
        idm AS cn,
        name AS title,
        convert(bit, czywal) AS centrum,
        CAST(erpId as int) AS refValue,
        available
	FROM dsd_services_purchase_document_type

GO


CREATE VIEW dsg_uek_budget AS
  SELECT budzetId as id,
         available AS available,
         budzetIdm AS cn,
         name AS title,
         erpId AS refValue,
         bdStrBudzetId AS centrum
  FROM dsd_services_budget


GO


CREATE VIEW dsg_uek_budget_position AS
  SELECT bdStrBudzetId AS id,
         1 AS available,
         bdStrBudzetIdn AS cn,
         bdStrBudzetIdn AS title,
         null AS refValue,
         0 AS centrum
  FROM dsd_services_budget


GO


CREATE VIEW dsg_uek_project_budget AS
  SELECT erpId AS id,
         available AS available,
         idm AS cn,
         name AS title,
         kontraktId AS refValue,
         null AS centrum
  FROM dsd_services_contract_budget;


GO


CREATE VIEW dsg_uek_fund AS
  SELECT idn AS id,
         available AS available,
         idm AS cn,
         name AS title,
         null AS refValue,
         null AS centrum
  FROM dsd_services_contract;


GO


CREATE VIEW dsg_uek_contract AS
  SELECT typKontraktId AS id,
         available AS available,
         typKontraktIdn AS cn,
         typKontraktIdn AS title,
         erpId AS refValue,
         null AS centrum
  FROM dsd_services_contract;


GO


CREATE VIEW [dbo].[dsg_uek_contract_stage] AS
  SELECT id AS id,
         available AS available,
         idm AS cn,
         name AS title,
         kierownikId AS refValue,
         erpId AS centrum
  FROM dsd_services_contract;




GO

CREATE VIEW dsg_uek_sources AS
  SELECT idn as id,
         available AS available,
         idm AS cn,
         name AS title,
         erpId AS refValue,
         null AS centrum
  FROM dsd_services_source



CREATE VIEW [dbo].[dsg_uek_financial_task] AS
  select  erpId as id,
          available,
          idm as cn,
          sourceName as title,
          idn as refValue,
          null AS centrum
  from dsd_services_financial_task


GO

CREATE VIEW [dbo].[dsg_uek_product] AS
  select  erpId as id,
          available,
          idm as cn,
          name as title,
          klaswytwIdn as refValue,
          null AS centrum
  from dsd_services_product


GO



CREATE VIEW [dbo].[dsg_uek_budget_ko] AS
  select	erpId as id,
    available,
    idm as cn,
    name as title,
    budzetId as refValue,
    null AS centrum
  from dsd_services_unitbudgetko



GO


CREATE VIEW [dbo].[dsg_uek_prodution_order] AS
  select  erpId as id,
          available,
          idm as cn,
          name as title,
          null as refValue,
          null AS centrum
  from dsd_services_production_order



GO



