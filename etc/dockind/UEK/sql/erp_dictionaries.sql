CREATE TABLE dsd_services_vat_rate(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[czyZwolniona] [numeric](18, 0) NULL,
	[dataObowDo] [datetime] NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[niePodlega] [numeric](18, 0) NULL,
	[procent] [numeric](18, 2) NULL
)


GO

CREATE TABLE dsd_services_source(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idn] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
)

GO

CREATE TABLE dsd_services_purchase_document_type (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[czyWal] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](150) NULL,
	[currencySymbol] [varchar](10) NULL,
	[documentTypeIds] [varchar](50) NULL
)

CREATE TABLE [dbo].[dsd_services_project](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[bprodzajwersjiid] [numeric](18, 0) NULL,
	[identyfikatornum] [numeric](18, 0) NULL,
	[idm] [varchar](250) NULL,
	[nazwa] [varchar](250) NULL,
	[pdatako] [date] NULL,
	[pdatapo] [date] NULL,
	[typkontraktid] [numeric](18, 0) NULL,
	[typkontraktidn] [varchar](250) NULL,

)
GO

CREATE TABLE dsd_services_currency_exchange_table (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[czasOgloszenia] [datetime] NULL,
	[dataDo] [datetime] NULL,
	[dataOd] [datetime] NULL,
	[tolerancja] [numeric](18, 0) NULL,
	[uwagi] [varchar](254) NULL
)

GO

CREATE TABLE dsd_services_currency (
	[id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL
)

GO

CREATE TABLE [dbo].[dsd_services_supplier](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[idn] [numeric](18, 0) NULL,
	[klasdostId] [numeric](18, 0) NULL,
	[klasdostIdn] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[nip] [varchar](50) NULL,
	[miasto] [varchar](50) NULL,
	[kodPocztowy] [varchar](50) NULL,
	[ulica] [varchar](50) NULL,
	[nrDomu] [varchar](50) NULL,
	[nrMieszkania] [varchar](50) NULL,
)

GO

CREATE TABLE dsd_services_contract_budget(
	[id] [int] PRIMARY KEY IDENTITY(1,1),
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[kontraktId] [numeric](18, 0) NULL,
	[name] [varchar](128) NULL,
	[pDatako] [datetime] NULL,
	[pDatapo] [datetime] NULL,
	[stan] [int] NULL,
	[status] [int] NULL
)

GO

CREATE TABLE [dbo].[dsd_services_unitbudgetko](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[bdStrBudzetId] [numeric](18, 0) NULL,
	[bdStrBudzetIdn] [varchar](128) NULL,
	[budzetId] [numeric](18, 0) NULL,
	[budzetIdm] [varchar](128) NULL,
	[czyAgregat] [int] NULL,
	[wersja] [int] NULL
)

GO

CREATE TABLE [dbo].[dsd_services_unit_of_measure](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[typIdn] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[precyzjaJm] [int] NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_organizational_unit](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[czyKadrowa] [bit] NULL,
	[czyKosztowa] [bit] NULL,
	[czyOddzial] [bit] NULL,
	[guidOsoba] [varchar](128) NULL,
	[idm] [varchar](50) NULL,
	[idn] [numeric](18, 0) NULL,
	[name] [varchar](128) NULL,
	[komorkaNadIdn] [varchar](50) NULL,
	[komorkaNadNazwa] [varchar](128) NULL,
	[osobaId] [numeric](18, 0) NULL,
	[pracownikId] [numeric](18, 0) NULL,
	[pracownikIdn] [varchar](50) NULL,
	[pracownikNrewid] [int] NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_product](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[jmIdn] [varchar](50) NULL,
	[klaswytwIdn] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[kind] [int] NULL,
	[vatRateId] [varchar](50) NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_production_order](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_unit_budget](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idm] [varchar](50) NULL,
	[name] [varchar](128) NULL,
	[wersja] [int] NULL,
	[dataObowDo] [datetime] NULL,
	[dataObowOd] [datetime] NULL,
	[czyArchiwalny] [bit] NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_financial_task](
	[id] [numeric](18, 0) IDENTITY(1,1) PRIMARY KEY,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[idn] [varchar](50) NULL,
	[idm] [varchar](50) NULL,
	[sourceName] [varchar](128) NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_project_budget_resource](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [available] [bit] NULL,
  [erpId] [numeric](18, 0) NULL,
  [name] [varchar](128) NULL,
  [nrPoz] [int] NULL,
  [etapId] [numeric](18, 0) NULL,
  [pCena] [numeric](18, 2) NULL,
  [pIlosc] [numeric](18, 0) NULL,
  [pWartosc] [numeric](18, 0) NULL,
  [rIlosc] [numeric](18, 0) NULL,
  [rWartosc] [numeric](18, 2) NULL,
  [zasobGlownyId] [int] NULL,
  [czyGlowny] [bit] NULL,
  [rodzajZasobu] [int] NULL,
)

 GO

CREATE TABLE [dbo].[dsd_services_project_budget_item](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [available] [bit] NULL,
  [erpId] [numeric](19, 0) NULL,
  [name] [varchar](128) NULL,
  [nrPoz] [int] NULL,
  [budzetId] [numeric](19, 0) NULL,
  [pDatako] [datetime] NULL,
  [pDatapo] [datetime] NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_project_budget_resource](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [available] [bit] NULL,
  [erpId] [numeric](18, 0) NULL,
  [name] [varchar](128) NULL,
  [nrPoz] [int] NULL,
  [etapId] [numeric](18, 0) NULL,
  [pCena] [numeric](18, 2) NULL,
  [pIlosc] [numeric](18, 0) NULL,
  [pWartosc] [numeric](18, 0) NULL,
  [rIlosc] [numeric](18, 0) NULL,
  [rWartosc] [numeric](18, 2) NULL,
  [zasobGlownyId] [int] NULL,
  [czyGlowny] [bit] NULL,
  [rodzajZasobu] [int] NULL,
)

GO

CREATE TABLE [dbo].[dsd_services_unit_budget_ko_kind](
  [id] [numeric](19, 0) IDENTITY(1,1) primary key ,
  [available] [bit] NULL,
  [erpId] [numeric](18, 0) NULL,
  [name] [varchar](128) NULL,
  [bdBudzetKoIdm] [varchar](50) NULL,
  [bdBudzetKoId] [numeric](18, 0) NULL,
  [bdNadRodzajKoId] [numeric](18, 0) NULL,
  [nrPoz] [int] NULL,
  [opis] [varchar](500) NULL,
  [pKoszt] [numeric](18, 0) NULL,
  [pKosztLimit] [numeric](18, 0) NULL,
  [rKoszt] [numeric](18, 0) NULL,
  [wytworId] [numeric](18, 0) NULL,
  [wytworIdm] [varchar](50) NULL,
)

GO


CREATE TABLE [dbo].[dsd_services_roles_In_projects](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[available] [bit] NULL,
	[erpId] [numeric](18, 0) NULL,
	[bprolaidn] [varchar](250) NULL,
	[bproalkontraktid] [numeric](18, 0) NULL,
	[czyblokadaprac] [numeric](18, 0) NULL,
	[czyblokadaroli] [numeric](18, 0) NULL,
	[kontraktid] [numeric](18, 0) NULL,

	[kontraktidm] [varchar](250) NULL,
	[osobaguid] [varchar](250) NULL,
	[osobaid] [numeric](18, 0) NULL,
	[pracownikid] [numeric](18, 0) NULL,
	[pracownikidn] [varchar](128) NULL,
	[pracowniknrewid] [int] NULL,


)
GO




------- VIEWS
------- NARAZIE WERSJA - DO POPRAWY


IF OBJECT_ID('dbo.dsg_uek_unit_of_measure') IS NOT NULL
  DROP VIEW dbo.dsg_uek_unit_of_measure
GO

create view dsg_uek_unit_of_measure as
  select erpId as id,
    available,
         idm as cn,
         name as title,
         null as refValue,
         null as centrum
  from dsd_services_unit_of_measure

go

IF OBJECT_ID('dbo.dsg_uek_purchase_document_type') IS NOT NULL
  DROP VIEW dbo.dsg_uek_purchase_document_type
GO

CREATE VIEW dsg_uek_purchase_document_type AS
  SELECT
    erpid as id,
    idm AS cn,
    name AS title,
    convert(bit, czywal) AS centrum,
    CAST(erpId as int) AS refValue,
    available
  FROM dsd_services_purchase_document_type

GO

IF OBJECT_ID('dbo.dsg_uek_currency') IS NOT NULL
  DROP VIEW dbo.dsg_uek_currency
GO

CREATE VIEW [dbo].[dsg_uek_currency] AS
  SELECT erpId AS id,
         available AS available,
         idm AS cn,
         idm+ ' - ' + name AS title,
         null AS refValue,
         null AS centrum
  FROM dsd_services_currency;


GO

IF OBJECT_ID('dbo.[dsg_uek_product]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_product]
GO

CREATE VIEW [dbo].[dsg_uek_product] AS
  SELECT erpId as id,
    available,
         idm as cn,
         name as title,
         null as refValue,
         null as centrum
  FROM dsd_services_product
  where klaswytwIdn = 'z_ZAKUPOWE'

GO

IF OBJECT_ID('dbo.dsg_uek_production_order') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_production_order]
GO

CREATE VIEW [dbo].[dsg_uek_production_order] AS
  SELECT erpId as id,
    available,
         idm as cn,
         name as title,
         null as refValue,
         null as centrum
  FROM dsd_services_production_order

GO

IF OBJECT_ID('dbo.[dsg_uek_financial_task]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_financial_task]
GO

CREATE VIEW [dbo].[dsg_uek_financial_task] AS
  SELECT erpId as id,
    available,
         idm as cn,
         sourceName as title,
         null as refValue,
         null as centrum
  FROM dsd_services_financial_task

GO

IF OBJECT_ID('dbo.[dsg_uek_budget]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_budget]
GO

CREATE VIEW [dbo].[dsg_uek_budget] AS
  SELECT erpId as id,
    available,
         idm AS cn,
         name AS title,
         null AS refValue,
         null AS centrum
  from dsd_services_unit_budget

GO

IF OBJECT_ID('dbo.[dsg_uek_budget_ko]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_budget_ko]
GO

CREATE VIEW [dbo].[dsg_uek_budget_ko] AS
  select	erpId as id,
    available,
          idm as cn,
          name as title,
          budzetId as refValue,
          null AS centrum
  from dsd_services_unitbudgetko

GO

IF OBJECT_ID('dbo.[dsg_uek_budget_position]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_budget_position]
GO

CREATE VIEW [dbo].[dsg_uek_budget_position] AS
  select	erpid as id,
    available,
          nrPoz as cn,
          name as title,
          bdBudzetKoId as refValue,
          null as centrum
  from dsd_services_unit_budget_ko_kind


GO

IF OBJECT_ID('dbo.[dsg_uek_project_budget]') IS NOT NULL
  DROP VIEW dbo.dsg_uek_project_budget
GO

CREATE VIEW dsg_uek_project_budget AS
  SELECT erpId AS id,
         available AS available,
         idm AS cn,
         name AS title,
         kontraktId AS refValue,
         null AS centrum
  FROM dsd_services_contract_budget;


GO

IF OBJECT_ID('dbo.[dsg_uek_fund]') IS NOT NULL
  DROP VIEW dbo.dsg_uek_fund
GO

CREATE VIEW dsg_uek_fund AS
  SELECT erpId AS id,
         available AS available,
         nrPoz AS cn,
         name AS title,
         etapId AS refValue,
         null AS centrum
  FROM dsd_services_project_budget_resource;


GO

IF OBJECT_ID('dbo.[dsg_uek_contract]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_contract]
GO


CREATE VIEW [dbo].[dsg_uek_contract] AS
  select  erpId as id,
    available,
          idm as cn,
          nazwa as title,
          typKontraktId as refValue,
          null AS centrum
  from dsd_services_project


GO
// zmiana  dsg_uek_contract zaci�ganny z dsg_uek_project
ALTER VIEW [dbo].[dsg_uek_contract] AS
  select  erpId as id,
          available,
          idm as cn,
          nazwa as title,
          typKontraktId as refValue,
          null AS centrum
  from dsd_services_project


GO

IF OBJECT_ID('dbo.[dsg_uek_contract_stage]') IS NOT NULL
  DROP VIEW dbo.dsg_uek_contract_stage
GO

CREATE VIEW dsg_uek_contract_stage AS
  SELECT erpId AS id,
         available AS available,
         nrPoz AS cn,
         name AS title,
         budzetId AS refValue,
         null AS centrum
  FROM dsd_services_project_budget_item;


GO

IF OBJECT_ID('dbo.[dsg_uek_sources]') IS NOT NULL
  DROP VIEW dbo.dsg_uek_sources
GO


CREATE VIEW dsg_uek_sources AS
  SELECT idn as id,
         available AS available,
         idm AS cn,
         name AS title,
         erpId AS refValue,
         null AS centrum
  FROM dsd_services_source


GO

IF OBJECT_ID('dbo.[dsg_uek_vat_rates]') IS NOT NULL
  DROP VIEW dbo.[dsg_uek_vat_rates]
GO

CREATE view [dbo].[dsg_uek_vat_rates] as
  select
    id as id,
    available as available,
    idm as cn,
    name as title,
    erpId as centrum,
    null as refValue
  from dsd_services_vat_rate;


GO


IF OBJECT_ID('dbo.dsg_uek_mpk') IS NOT NULL
  DROP VIEW dbo.dsg_uek_mpk
GO

create view dsg_uek_mpk as
  select id,
    iif(hidden = 0,1,0) as available,
    EXTERNALID as cn,
    CODE as title,
    null AS refValue,
    null AS centrum
  from  DS_DIVISION
  where DIVISIONTYPE = 'division' and EXTERNALID is not null

GO

-----------S�OWNIKI Z REPOZYTORIUM ------
create table dsd_dictionary (
  id int identity(1,1) not null,
  deleted numeric(1,0) default 0,
  code varchar(64),
  tablename varchar(64),
  dockind_cn varchar(50) not null,
  dict_source int,
  cn varchar(64),
  ref_dict int,
  is_control_dict numeric(1,0) default 0,
  PRIMARY KEY (id),
  CONSTRAINT REFERENCE_DICT FOREIGN KEY (ref_dict) REFERENCES dsd_dictionary (id)
);
CREATE UNIQUE INDEX dsd_dictionary_id
ON dsd_dictionary (id);

GO

create table dsd_simple_repository_attribute (
  id int identity(1,1) not null,
  available numeric(1,0) not null default(1),
  used numeric(1,0) not null default(1),
  table_name varchar(128) null,
  context_id int null,
  objectid int null,
  czy_aktywny int null,
  czy_blokada int null,
  nazwa varchar(128) null,
  obiekt_systemu_id bigint null,
  rep_atrybut_id bigint null,
  rep_atrybut_ido varchar(128) null,
  rep_disp_atr_id bigint null,
  rep_klasa_id bigint null,
  rep_typ_id bigint null,
  rep_typ_prosty_ido varchar(128) null,
  rep_wartosc_id bigint null,
  rodzaj int null,
  rodzaj_typu int null,
  rodzaj_wartosci int null,
  typ_dlugosc int null,
  typ_format varchar(128) null,
  typ_wartosci int null,
  wartosc_date datetime null,
  wartosc_decimal numeric(18,6) null,
  wartosc_instancja_id bigint null,
  wartosc_long int null,
  wartosc_string varchar(128) null
);
CREATE UNIQUE INDEX dsd_simple_repository_attribute_id
ON dsd_simple_repository_attribute (id);

GO

-----------VIEWS DO S�OWNIK�W Z REPOZYTORIUM ----


-----------VIEWS DO S�OWNIK�W Z REPOZYTORIUM ----
IF OBJECT_ID('dbo.dsg_repo77_rodzaj_przedsiewziecia') IS NOT NULL
  DROP VIEW dbo.dsg_repo77_rodzaj_przedsiewziecia
GO

CREATE VIEW dsg_repo77_rodzaj_przedsiewziecia
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo77_rodzaj_przedsiewziecia as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

IF OBJECT_ID('dbo.dsg_repo89_przedsiewziecia_dydaktyczne') IS NOT NULL
  DROP VIEW dbo.dsg_repo89_przedsiewziecia_dydaktyczne
GO

CREATE VIEW dsg_repo89_przedsiewziecia_dydaktyczne
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo89_przedsiewziecia_dydaktyczne as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id


GO

IF OBJECT_ID('dbo.dsg_repo95_rodzaj_studiow') IS NOT NULL
  DROP VIEW dbo.dsg_repo95_rodzaj_studiow
GO

CREATE VIEW dsg_repo95_rodzaj_studiow
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo95_rodzaj_studiow as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

IF OBJECT_ID('dbo.dsg_repo88_budynki') IS NOT NULL
  DROP VIEW dbo.dsg_repo88_budynki
GO

CREATE VIEW dsg_repo88_budynki
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo88_budynki as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

IF OBJECT_ID('dbo.dsg_repo97_ds') IS NOT NULL
  DROP VIEW dbo.dsg_repo97_ds
GO

CREATE VIEW dsg_repo97_ds
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo97_ds as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

IF OBJECT_ID('dbo.dsg_repo98_dotacja') IS NOT NULL
  DROP VIEW dbo.dsg_repo98_dotacja
GO

CREATE VIEW dsg_repo98_dotacja
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo98_dotacja as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

IF OBJECT_ID('dbo.dsg_repo119_kpzf') IS NOT NULL
  DROP VIEW dbo.dsg_repo119_kpzf
GO


CREATE VIEW dsg_repo119_kpzf
AS
  SELECT  dic.id,
    dic.available,
    dic.cn,
    dic.title,
    atr.objectid as refValue,
    dic.centrum
  FROM dsd_repo119_kpzf as dic join dsd_simple_repository_attribute as atr
      on  dic.refValue = atr.rep_atrybut_id

GO

