ALTER TABLE dsg_normal_dockind add zpo smallint;
ALTER TABLE dsg_normal_dockind add DOCUMENT_OUT_OFFICE_KIND numeric (18,0);
ALTER TABLE dsg_normal_dockind add DOC_MAIL_ZONE numeric (18,0);
ALTER TABLE dsg_normal_dockind add doc_return smallint;
ALTER TABLE dsg_normal_dockind add doc_return_cost numeric (6,2);
ALTER TABLE dsg_normal_dockind add status numeric (18,0);
ALTER TABLE dsg_normal_dockind add DOC_OUT numeric (18,0);
ALTER TABLE dsg_normal_dockind add doc_number_r_out varchar (100);
ALTER TABLE dsg_normal_dockind add EPUAP numeric (18,0);
ALTER TABLE dsg_normal_dockind add identifikatorEpuap varchar (50);
ALTER TABLE dsg_normal_dockind add adresSkrytkiEpuap varchar (50);
ALTER TABLE dsg_normal_dockind add ZABLOKUJ bit);
ALTER TABLE dsg_normal_dockind add CZY_MAIL bit;


DELETE FROM dso_document_out_gauge WHERE id in (3,4);

CREATE TABLE [dbo].[dsg_normal_multiple](
   	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
   	[FIELD_CN] [varchar](100) NOT NULL,
   	[FIELD_VAL] [varchar](100) NULL
   )
   
   CREATE VIEW NORMAL_OUT_VIEW 
 AS select outDoc.ID, outDoc.summary, outDoc.POSTALREGNUMBER from dso_out_document outDoc, dsg_normal_dockind normal where outDoc.ID = normal.DOCUMENT_ID;