CREATE TABLE [dbo].[dsg_uek_wniosek_inwestycyjny_multiple](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[dsg_uek_wniosek_inwestycyjny](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [numeric](18, 0) NULL,
	[JEDNOSTKA_ZGLASZAJACA] [numeric](18, 0) NULL,
	[RODZAJ_WNIOSKU] [numeric](18, 0) NULL,
	[WYCENA] [numeric](18, 2) NULL,
	[TRYB_ZAMOWIENIA] [numeric](18, 0) NULL,
	[WNIOSKODAWCA] [numeric](18, 0) NULL,
	[TYP_ZADANIA] [varchar](250) NULL,
	[Z_GLOWNE] [bit] NULL,
	[Z_KOMPLEMENTARNE] [bit] NULL,
	[D_TECHNICZNA] [bit] NULL,
	[BUDOWLANA] [bit] NULL,
	[PRZEBUDOWA] [bit] NULL,
	[ADAPTACJA] [bit] NULL,
	[REMONT] [bit] NULL,
	[ENTRY_ID] [numeric](18, 0) NULL,
	[SOURCE] [numeric](18, 0) NULL,
	[CONTRACT_STAGE] [numeric](18, 0) NULL,
	[CONTRACT] [numeric](18, 0) NULL,
	[FUND] [numeric](18, 0) NULL,
	[PROJECT_BUDGET] [numeric](18, 0) NULL,
	[POSITION] [numeric](18, 0) NULL,
	[BUDGET] [numeric](18, 0) NULL,
	[MODERNIZACJA] [bit] NULL,
	[TERMIN] [date] NULL,
	[CZY_POPRAWNA] [bit] NULL,
	[CZY_SPRAWDZONY] [bit] NULL,
	[WYMAGANE_PROCEDURY] [bit] NULL,
	[OPIS_CZY_POPRAWNA] [varchar](1024) NULL,
	[OPIS_CZY_SPRAWDZONY] [varchar](1024) NULL,
	[OPIS_WYMAGANE_PROCEDURY] [varchar](1024) NULL,
	[KWOTA_NETTO] [numeric](18, 2) NULL,
	[KWOTA_BRUTTO] [numeric](18, 2) NULL,
	[BUDGET_KIND] [numeric](18, 0) NULL,
	[TO_ZAPOTRZEBOWANIE] [numeric](19, 0) NULL,
	[FINANCIAL_TASK] [numeric](19, 0) NULL,
	[BUDGET_KO] [numeric](19, 0) NULL,
	[OPIS] [bit] NULL,
	[ZRODLO_DESCRIPTION] [varchar](2000) NULL,
 CONSTRAINT [PK_dsg_uek_wniosek_zakupowy] PRIMARY KEY CLUSTERED
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
-------------S�OWNIKI DOKUMENTU -----------------
CREATE TABLE [dbo].[dsg_uek_opis_przedmiotu](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[PURPOSE] [numeric](18, 0) NULL,
	[OPIS_ZAMOWIENIA] [varchar](1024) NULL,)

---------------------------------------------------------------

CREATE TABLE [dbo].[dsg_uek_wymogi_ddt](
	[id] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[WYMAGANA] [bit] NULL,
	[POSIADANA] [bit] NULL,
	[DOKUMENT] [numeric](18, 0) NULL,)



CREATE TABLE [dbo].[dsg_uek_ocena_projektu](
	[id] [numeric](18, 0) NOT NULL,
	[PURPOSE] [numeric](18, 0) NULL,
	[OPIS_ZAMOWIENIA] [varchar](1024) NULL,
 ) ON [PRIMARY]

-------------------------------------------------------------------

------------ ZR�DLO FINANSOWANIA ---------------

CREATE TABLE [dbo].[dsg_uek_zrodlo_finansowania](
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[BUDGET_KIND] [numeric](19, 0) NULL,
	[BUDGET_KO] [numeric](19, 0) NULL,
	[BUDGET] [numeric](19, 0) NULL,
	[POSITION] [numeric](19, 0) NULL,
	[FINANCIAL_TASK] [numeric](19, 0) NULL,
	[CONTRACT] [numeric](19, 0) NULL,
	[CONTRACT_STAGE] [numeric](19, 0) NULL,
	[PROJECT_BUDGET] [numeric](19, 0) NULL,
	[FUND] [numeric](19, 0) NULL,
	[SOURCE] [numeric](19, 0) NULL,
	[ZRODLO_DESCRIPTION] [varchar](2000) NULL,
)

GO



------- VIEWS (wymagane s� najpierw tabele z erp_dictionaries.sql)


CREATE VIEW dsg_uek_purchase_document_type AS
	SELECT
		id,
        idm AS cn,
        name AS title,
        convert(bit, czywal) AS centrum,
        CAST(erpId as int) AS refValue,
        available
	FROM dsd_services_purchase_document_type

GO


CREATE VIEW dsg_uek_budget AS
  SELECT budzetId as id,
         available AS available,
         budzetIdm AS cn,
         name AS title,
         erpId AS refValue,
         bdStrBudzetId AS centrum
  FROM dsd_services_budget


GO


CREATE VIEW dsg_uek_budget_position AS
  SELECT bdStrBudzetId AS id,
         1 AS available,
         bdStrBudzetIdn AS cn,
         bdStrBudzetIdn AS title,
         null AS refValue,
         0 AS centrum
  FROM dsd_services_budget


GO


CREATE VIEW dsg_uek_project_budget AS
  SELECT erpId AS id,
         available AS available,
         idm AS cn,
         name AS title,
         kontraktId AS refValue,
         null AS centrum
  FROM dsd_services_contract_budget;


GO


CREATE VIEW dsg_uek_fund AS
  SELECT idn AS id,
         available AS available,
         idm AS cn,
         name AS title,
         null AS refValue,
         null AS centrum
  FROM dsd_services_contract;


GO


CREATE VIEW dsg_uek_contract AS
  SELECT typKontraktId AS id,
         available AS available,
         typKontraktIdn AS cn,
         typKontraktIdn AS title,
         erpId AS refValue,
         null AS centrum
  FROM dsd_services_contract;


GO


CREATE VIEW dsg_uek_contract_stage AS
  SELECT id AS id,
         available AS available,
         idm AS cn,
         name AS title,
         kierownikId AS refValue,
         erpId AS centrum
  FROM dsd_services_contract;


GO

CREATE VIEW dsg_uek_sources AS
  SELECT idn as id,
         available AS available,
         idm AS cn,
         name AS title,
         erpId AS refValue,
         null AS centrum
  FROM dsd_services_source


GO









