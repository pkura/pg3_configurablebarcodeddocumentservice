CREATE TABLE [dbo].[dsg_uek_wnios_urlop_multiple](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

GO


CREATE TABLE [dbo].[dsg_uek_wniosek_urlopowy](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[DATA_OD] [datetime] NULL,
	[DATA_DO] [datetime] NULL,
	[RODZAJ_URLOPU] [numeric](18, 0) NULL,
	[ILOSC_DNI_ROBOCZYCH] [int] NULL,
	[ILOSC_GODZIN_ROBOCZYCH] [int] NULL,
	[status] [numeric](18, 0) NULL,
	[WNIOSKUJACY] [numeric](18, 0) NULL,
	[WNIOSKUJACY_JEDNOSTKA] [numeric](18, 0) NULL,
	[UWAGI] [varchar](1000) NULL,
	[NAUKOWY] [numeric](18, 0) NULL,
	[CZY_ADMINISTR] [bit] NULL
) ON [PRIMARY]

GO


