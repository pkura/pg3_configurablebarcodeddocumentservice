CREATE TABLE [dbo].dsg_uek_out_office_kind (
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
	)
	
ALTER TABLE dsg_uek_out_office_kind ADD PRIMARY KEY (ID);
create index dsg_uek_out_office_kind_index on dsg_uek_out_office_kind (ID);	

INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'FAKS', N'Faks', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'EMAIL', N'e-mail', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'OUT', N'Pismo wychodz�ce', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'LISTY', N'Listy imienne', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'PPDE', N'Pismo przychodz�ce drog� elektroniczn�', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'UMOWA', N'Umowa', NULL, NULL, 1);
INSERT [dbo].[dsg_uek_out_office_kind] ([cn], [title], [centrum], [refValue], [available]) VALUES (N'DP', N'Dokumentacja przetargowa', NULL, NULL, 1);