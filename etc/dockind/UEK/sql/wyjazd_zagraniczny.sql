CREATE TABLE [dbo].[dsg_uek_delegacja_zagraniczna](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	
	[NUMER_WNIOSKU] varchar(255) NULL,
	[DATA_WNIOSKU] date NULL,
	[DELEGOWANY] numeric(18,0) NULL,
	[STATUS_DELEGOWANEGO] numeric(18,0) NULL,
	[STANOWISKO] varchar(255) NULL,
	[TELEFON] varchar(255) NULL,
	[PESEL] varchar(255) NULL,
	[WYDZIAL] varchar(255) NULL,
	[SPECJALNOSC] varchar(255) NULL,
	[STOPIEN] varchar(255) NULL,
	[ROK] varchar(255) NULL,
	[ADRES_ZAMIESZKANIA] varchar(255) NULL,
	[EMAIL] varchar(255) NULL,
	[MIEJSCE] varchar(255) NULL,
	[DATA_OD] date NULL,
	[DATA_DO] date NULL,
	[LICZBA_DNI] numeric(18,0) NULL,
	[CEL_WYJAZDU] numeric(18,0) NULL,
	[OPIS_CELU] varchar(750) NULL,
	[UZASADNIENIE_WYJAZDU] varchar(750) NULL,
	[ZALATWIENIE_SPRAW] numeric(18,0) NULL,
	[ZALICZKA] numeric(18,0) NULL,
	[SPOSOB_WYPLATY] numeric(18,0) NULL,
	[KWOTA_ZALICZKI] numeric(18,2) NULL,
	[NUMER_KONTA] varchar(255) NULL,
	[DATA_PRZELEWU] date NULL,
	[KOSZTY_PRZEJAZU_SAM_P] numeric(18,0) NULL,
	[KOSZTY_PRZEJAZU_ROZLICZENIE] numeric(18,0) NULL,
	[FORMA_REGULOWANIA] numeric(18,0) NULL,
	[NUMER_KONTA_SEC] varchar(255) NULL,
	
	[BUDGET_KIND] numeric(18,0) NULL,
	[SOURCE] numeric(18,0) NULL,
	[BUDGET_DATE] numeric(18,0) NULL,
	[BUDGET] numeric(18,0) NULL,
	[BUDGET_KO] numeric(18,0) NULL,
	[POSITION] numeric(18,0) NULL,
	[FINANCIAL_TASK] numeric(18,0) NULL,
	[PROJECT_BUDGET] numeric(18,0) NULL,
	[CONTRACT] numeric(18,0) NULL,
	[CONTRACT_STAGE] numeric(18,0) NULL,
	[FUND] numeric(18,0) NULL,
	[ENTRY_ID] varchar(50) NULL,
	
	[ZALACZNIK] numeric(18,0) NULL,
	[DANE_WNIOSKODAWCY] numeric(18,0) NULL,
	[KOSZTY_WYJAZDU_PLN] numeric(18,0) NULL,
	[CZY_FINANSOWANY] smallint NULL,
	[SUPERVISOR_DECISION] smallint NULL,
	[UWAGI_KOSZTY] smallint NULL,
	[USASADNIENIE_SHOW_OR_HIDE] smallint NULL
	
	
	
	
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[dsg_uek_delegacja_zagraniczna_multiple_value](
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
) ON [PRIMARY]

CREATE TABLE [DSG_STATUS_DELEGOWANEGO] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

delete from DSG_STATUS_DELEGOWANEGO;
insert into DSG_STATUS_DELEGOWANEGO values ('PRACOWNIK','Pracownik',NULL,'MIN.guid',1);
insert into DSG_STATUS_DELEGOWANEGO values ('STUDENT','Student',NULL,'EBZ.guid',1);
insert into DSG_STATUS_DELEGOWANEGO values ('DOKTORANT','Doktorant',NULL,'MIN.guid',1);
insert into DSG_STATUS_DELEGOWANEGO values ('INNY','Inny',NULL,'MIN.guid',1);

CREATE TABLE [dsg_trip_purpose] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_trip_purpose values ('KONFERENCJA_NAUKOWO_BADAWCZA','Konferencja naukowo badawcza',NULL,NULL,1);
insert into dsg_trip_purpose values ('KONFERENCJA_DYDAKTYCZNA','Konferencja dydaktyczna',NULL,NULL,1);
insert into dsg_trip_purpose values ('WYJAZD_SZKOLENIOWY','Wyjazd szkoleniowy',NULL,NULL,1);
insert into dsg_trip_purpose values ('STAZ_NAUKOWY','Sta� naukowy',NULL,NULL,1);
insert into dsg_trip_purpose values ('DZIALALNOSC_DYDAKTYCZNA','Dzia�alno�� dydaktyczna',NULL,NULL,1);
insert into dsg_trip_purpose values ('WIZYTA_STUDYJNA','Wizyta studyjna',NULL,NULL,1);
insert into dsg_trip_purpose values ('DZIALALNOSC_NAUKOWO_BADAWCZA','Dzia�alno�� naukowo-badawcza',NULL,NULL,1);
insert into dsg_trip_purpose values ('WYJAZD_ORGANIZACYJNY','Wyjazd organizacyjny',NULL,NULL,1);
insert into dsg_trip_purpose values ('SZKOLA_LETNIA','Szko�a letnia',NULL,NULL,1);
insert into dsg_trip_purpose values ('INNY','Inny',NULL,NULL,1);

CREATE TABLE [dsg_vehicle] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_vehicle values ('POCIAG','Poci�g',NULL,NULL,1);
insert into dsg_vehicle values ('SAMOLOT','Samolot',NULL,NULL,1);
insert into dsg_vehicle values ('SAMOCHOD_SLUZBOWY','Samoch�d s�u�bowy',NULL,NULL,1);
insert into dsg_vehicle values ('AUTOKAR','Autokar',NULL,NULL,1);
insert into dsg_vehicle values ('SAMOCHOD_NIE','Samoch�d nie b�d�cy w�asno�ci� pracodawcy',NULL,NULL,1);

CREATE TABLE [dsg_other_delegation_cost] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_other_delegation_cost values ('OPLATA_AUTOSTRADOWA','Op�ata autostradowa',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('PARKING','Parking',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('WIZA','Wiza',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('OPLATA_KONFERENCYJNA','Op�ata konferencyjna',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('DOJAZD_KOMUNIKACJA','Dojazd na lotnisko w karaju (komunikacja publiczna)',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('DOJAZD_SAMOCHOD','Dojazd na lotnisko w karaju (samoch�d s�u�bowy)',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('DOJAZD_SAMOCHOD_NIE','Dojazd na lotnisko w karaju (samoch�d nieb�d�cy w�asno�ci� pracodawcy)',NULL,NULL,1);
insert into dsg_other_delegation_cost values ('INNE','Inne',NULL,NULL,1);

CREATE TABLE [dbo].[dsg_delegation_abroad_dic](
	[id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	[enum] [numeric](19, 0) NULL,
	[value] [numeric](19, 2) NULL,
	[rate] [numeric](19, 2) NULL,
	[add_rate] [numeric](19, 2) NULL,
	[description] [varchar](500) NULL,
	[money_value] [numeric](18, 2) NULL,
	[money_value_pln] [numeric](18, 2) NULL,
	[currency] [int] NULL,
	[currency_rate] [numeric](18, 4) NULL,
	[currency_rate_date] [datetime] NULL,
	[bool1] [bit] NULL,
	[bool2] [bit] NULL,
	[bool3] [bit] NULL,
	[bool4] [bit] NULL,
	[country] [int] NULL,
	[add_money_value] [numeric](19, 2) NULL,
	[add_money_value_pln] [numeric](19, 2) NULL,
	[add_enum] [numeric](19, 0) NULL,
	[int1] [int] NULL,
	[int2] [int] NULL,
	[int3] [int] NULL,
	[int4] [int] NULL,
	[int5] [int] NULL,
) ON [PRIMARY]

CREATE TABLE [dsg_cases] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_cases values ('REZERWACJA_HOTELU','Rezerwacja hotelu',NULL,NULL,1);
insert into dsg_cases values ('PRZELEW_OPLATY','Przelew op�aty konferencyjnej',NULL,NULL,1);
insert into dsg_cases values ('PRZELEW_STYPENDIUM','Przelew stypendium',NULL,NULL,1);
insert into dsg_cases values ('ORGANIZACJA_PODROZY','Organizacja podr�y',NULL,NULL,1);

CREATE TABLE [dsg_forma_regulowania] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_forma_regulowania values ('KASA','Wp�ata / Wyp�ata w kasie',NULL,NULL,1);
insert into dsg_forma_regulowania values ('POTRACENIE','Potr�cenie z wynagrodzenia',NULL,NULL,1);
insert into dsg_forma_regulowania values ('WALUTA','Waluta',NULL,NULL,1);
insert into dsg_forma_regulowania values ('PRZELEW','Przelew na konto',NULL,NULL,1);

CREATE TABLE [dsg_coutry_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[COUNTRY] NUMERIC(18,0) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_city_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[CITY] [varchar](255) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_trip_purpose_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[CEL_WYJAZDU] NUMERIC(18,0) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_journey_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[SRODEK_LOKOMOCJI] NUMERIC(18,0) NULL,
	[OPIS] varchar(255) NULL,
	[WARTOSC] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_other_delegation_cost_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[OTHER] NUMERIC(18,0) NULL,
	[OPIS] varchar(255) NULL,
	[WARTOSC] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_car_p_delegation_cost_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[FROM] varchar(255) NULL,
	[TO] varchar(255) NULL,
	[PRZEKROCZENIA_GRANICY] varchar(255) NULL,
	[ILOSC_KM] NUMERIC(18,0) NULL,
	[POT_ILOSC_KM] NUMERIC(18,0) NULL,
	[STAWKA_KM] NUMERIC(18,2) NULL,
	[WARTOSC] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_delegation_cost_summary_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[MIEJSCOWOSC_WYJAZDU] varchar(255) NULL,
	[DATA_WYJAZDU] datetime NULL,
	[MIEJSCOWOSC_PRZYJAZDU] varchar(255) NULL,
	[DATA_PRZYJAZDU] datetime NULL,
	[SRODEK_LOKOMOCJI] NUMERIC(18,0) NULL,
	[OPIS] varchar(255) NULL,
	[WARTOSC] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_koszty_wyjazdu_summary] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[CURRENCY] NUMERIC(18,0) NULL,
	[KWOTA] NUMERIC(18,2) NULL,
	[WARTOSC_PLN] NUMERIC(18,2) NULL,
	[PROPOZYCJA_ZMIAN] varchar(255) NULL
	PRIMARY KEY (ID)
);
CREATE SEQUENCE SEQ_KOSZTY_WYJAZDU_ID
    START WITH 1
    INCREMENT BY 1 ;
GO


CREATE TABLE [dsg_other_delegation_cost_summary_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[OTHER] NUMERIC(18,0) NULL,
	[WARTOSC] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_zaliczki_mv] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[SPOSOB_WYPLATY] int NULL,
	[MONEY_VALUE] NUMERIC(18,2) NULL,
	[CURRENCY] NUMERIC(18,0) NULL,
	[CURRENCY_RATE_DATE] date NULL,
	[CURRENCY_RATE] NUMERIC(18,4) NULL,
	[MONEY_VALUE_PLN] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE SEQUENCE SEQ_CURRENCY_DEPOSIT_ID
    START WITH 1
    INCREMENT BY 1 ;
GO


CREATE TABLE [dsg_uek_uzasadnienie_przejazdu] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[SRODEK_LOKOMOCJI] NUMERIC(18,0) NULL,
	[OPIS] varchar(255) NULL,
	[MARKA] varchar(255) NULL,
	[NR_REJESTRACYJNY] varchar(255) NULL,
	[POJEMNOSC_SILNIKA] NUMERIC(18,2) NULL,
	[WYNOSI_KM] NUMERIC(18,2) NULL,
	[STAWKA_KM] NUMERIC(18,2) NULL,
	[KOSZT_STAWKA] NUMERIC(18,2) NULL,
	[POCIAG_KLASA] varchar(255) NULL,
	[KOSZT_OSOBA] NUMERIC(18,2) NULL,
	[KOSZT_AUTOBUS] NUMERIC(18,2) NULL,
	[KOSZT_SAMOLOT] NUMERIC(18,2) NULL,
	[SAMOCHOD_NIE_ILOSC] NUMERIC(18,2) NULL,
	[SAMOCHOD_NIE_KOSZT] NUMERIC(18,2) NULL,
	[UWARUNKOWANIA] varchar(350) NULL,
	[INNE_UWARUNKOWANIA] varchar(350) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_uek_dane_wnioskodawcy] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[DELEGOWANY] numeric(18,0) NULL,
	[STATUS_DELEGOWANEGO] numeric(18,0) NULL,
	[STANOWISKO] varchar(255) NULL,
	[TELEFON] varchar(255) NULL,
	[PESEL] varchar(255) NULL,
	[WYDZIAL] varchar(255) NULL,
	[SPECJALNOSC] varchar(255) NULL,
	[STOPIEN] varchar(255) NULL,
	[ROK] varchar(255) NULL,
	[ADRES_ZAMIESZKANIA] varchar(255) NULL,
	[EMAIL] varchar(255) NULL
	PRIMARY KEY (ID)
);

