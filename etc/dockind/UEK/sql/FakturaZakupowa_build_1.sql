------WYMAGANE RÓWNIEŻ:
--------- dsg_uek_zrodla_finansowania.sql
--------- erp_dictionaries.sql



IF OBJECT_ID('dbo.dsg_uek_faktura_zakupowa') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_faktura_zakupowa]
GO



CREATE TABLE [dbo].[dsg_uek_faktura_zakupowa](
  [document_id] [numeric](19, 0) NOT NULL,
  [status] [numeric](18, 0) NULL,
  [OSTATNIA_FAKTURA_BOOL] [bit] NULL,
  [BRAK_KONTRAHENTA] [bit] NULL,
  [CONTRACTOR] [numeric](19, 0) NULL,
  [NR_FAKTURY] [varchar](150) NULL,
  [TYP_FAKTURY] [numeric](19, 0) NULL,
  [DATA_WYSTAWIENIA] [datetime] NULL,
  [DATA_WPLYWU] [datetime] NULL,
  [SPOSOB_PLATNOSCI] [numeric](19, 0) NULL,
  [SPOSOB_ZWROTU] [numeric](19, 0) NULL,
  [KWOTA_NETTO] [numeric](18, 2) NULL,
  [KWOTA_VAT] [numeric](18, 2) NULL,
  [KWOTA_BRUTTO] [numeric](18, 2) NULL,
  [KWOTA_WYPLATY] [numeric](18, 2) NULL,
  [NKUP] [numeric](19, 0) NULL,
  [DATA_ZAMOWIENIA] [datetime] NULL,
  [TRYB_ZAMOWIENIA] [numeric](19, 0) NULL,
  [SZCZEGOLOWY_TRYB] [numeric](19, 0) NULL,
  [NUMER_ZAMOWIENIA] [numeric](19, 0) NULL,
  [NUMER_BZP] [numeric](19, 0) NULL,
  [WALUTA] [numeric](19, 0) NULL,
  [KOD_USLUGI] [numeric](19, 0) NULL,
  [KURS] [numeric](18, 2) NULL,
  [OPIS_FAKTURY] [numeric](19, 0) NULL,
  [OPIS_MERYTORYCZNY] [varchar](4096) NULL,
  [WAS_PROFORMA] [numeric](19, 0) NULL,
  [PROFORMA] [numeric](19, 0) NULL,
  [WNIOSEK_O_REZERWACJE_BOOL] [bit] NULL,
  [WNIOSEK_O_REZERWACJE] [numeric](19, 0) NULL,
  [OPLATA_ADMINISTRACYJNA] [bit] NULL,
  [UMOWA] [bit] NULL,
  [OPIS_OPLATY] [varchar](512) NULL,
  [NUMER_UMOWY] [varchar](50) NULL,
  [CZY_MAGAZYNOWA] [numeric](19, 0) NULL,
  [PRODUCT] [numeric](19, 0) NULL,
  [PRODUCTION_ORDER] [numeric](19, 0) NULL,
  [MPK] [numeric](19, 0) NULL,
  [PRZED_WYKONANIEM] [numeric](19, 0) NULL,
  [DATA_WYKONANIA] [datetime] NULL,
  [RODZAJ_ZAMOWIENIA] [numeric](19, 0) NULL,
  [QUANTITY] [int] NULL,
  [UNIT_OF_MEASURE] [numeric](19, 0) NULL,
  [BUDGET_KIND] [numeric](19, 0) NULL,
  [SOURCE] [numeric](19, 0) NULL,
  [BUDGET] [numeric](19, 0) NULL,
  [BUDGET_KO] [numeric](19, 0) NULL,
  [POSITION] [numeric](19, 0) NULL,
  [FINANCIAL_TASK] [numeric](19, 0) NULL,
  [PROJECT_BUDGET] [numeric](19, 0) NULL,
  [CONTRACT] [numeric](19, 0) NULL,
  [CONTRACT_STAGE] [numeric](19, 0) NULL,
  [FUND] [numeric](19, 0) NULL,
  [WS_KOD] [varchar](250) NULL,
  [WS_OBSZAR] [varchar](250) NULL,
  [WS_KWOTA] [numeric](18, 2) NULL,
  [ENTRY_ID] [varchar](50) NULL,
  [DSD_REPO89_PRZEDSIEWZIECIA_DYDAKTYCZNE] [numeric](19, 0) NULL,
  [DSD_REPO95_RODZAJ_STUDIOW] [numeric](19, 0) NULL,
  [DSD_REPO77_RODZAJ_PRZEDSIEWZIECIA] [numeric](19, 0) NULL,
  [DSD_REPO88_BUDYNKI] [numeric](19, 0) NULL,
  [DSD_REPO97_DS] [numeric](19, 0) NULL,
  [DSD_REPO98_DOTACJA] [numeric](19, 0) NULL,
  [DSD_REPO119_KPZF] [numeric](19, 0) NULL,
  [ERP_STATUS] [int] NULL,
  [ERP_DOCUMENT_IDM] [varchar](50) NULL,

  PRIMARY KEY CLUSTERED
    (
      [document_id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF OBJECT_ID('dbo.dsg_uek_faktura_zakupowa_multiple_value') IS NOT NULL
  DROP TABLE [dbo].[dsg_uek_faktura_zakupowa_multiple_value]
GO

CREATE TABLE dsg_uek_faktura_zakupowa_multiple_value(
	[DOCUMENT_ID] [numeric](18, 0) NOT NULL ,
	[FIELD_CN] [varchar](100) NOT NULL,
	[FIELD_VAL] [varchar](100) NULL
)

GO



------------ POZYCJE FAKTURY ---------------
IF OBJECT_ID('dbo.dsg_uek_stawki_vat') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_stawki_vat]
GO

CREATE TABLE [dbo].[dsg_uek_stawki_vat](
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[NET_AMOUNT] [numeric](18, 2) NULL,
	[VAT_RATE] [numeric](19, 0) NULL,
	[GROSS_AMOUNT] [numeric](18, 2) NULL,
	[PURPOSE] [numeric](19, 0) NULL,
	[VAT_AMOUNT] [numeric](18, 2) NULL,
	[POSITION_NUMBER] int NULL,
)

GO

ALTER TABLE dsg_uek_stawki_vat
ADD ENTRY_NO INT NULL

GO

IF OBJECT_ID('dbo.dsg_uek_wnioskujacy') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_wnioskujacy]
GO

CREATE TABLE [dbo].[dsg_uek_wnioskujacy](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [WN_USER] [numeric](18, 0) NULL,
  [WN_DIVISION] [numeric](18, 0) NULL,
  [DATA_ODBIORU] [datetime] NULL,
)

GO

IF OBJECT_ID('dbo.dsg_uek_invoice_entry') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_invoice_entry]
GO


CREATE TABLE [dbo].[dsg_uek_invoice_entry](
  [ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
  [CZY_MAGAZYNOWA] [numeric](19, 0) NULL,
  [PRODUCT] [numeric](19, 0) NULL,
  [PRODUCTION_ORDER] [numeric](19, 0) NULL,
  [MPK] [numeric](19, 0) NULL,
  [PRZED_WYKONANIEM] [numeric](19, 0) NULL,
  [DATA_WYKONANIA] [datetime] NULL,
  [RODZAJ_ZAMOWIENIA] [numeric](19, 0) NULL,
  [QUANTITY] [int] NULL,
  [UNIT_OF_MEASURE] [numeric](19, 0) NULL,
  [WS_KOD] [varchar](250) NULL,
  [WS_OBSZAR] [varchar](250) NULL,
  [WS_KWOTA] [numeric](18, 2) NULL,
  [ZRODLO_ID] [numeric](19, 0) NULL,
  [DESCRIPTION] [varchar](2000) NULL,
  [dsd_repo77_rodzaj_przedsiewziecia] [int] NULL,
  [dsd_repo88_budynki] [int] NULL,
  [dsd_repo95_rodzaj_studiow] [int] NULL,
  [dsd_repo119_kpzf] [int] NULL,
  [dsd_repo89_przedsiewziecia_dydaktyczne] [int] NULL,
  [dsd_repo98_dotacja] [int] NULL,
  [dsd_repo97_ds] [int] NULL,
  [REPO_DICT_ENTRY_ID] [numeric](19, 0) NULL,
  [ERP_DOCUMENT_IDM] [varchar](50) NULL,

  PRIMARY KEY CLUSTERED
    (
      [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF OBJECT_ID('dbo.dsg_uek_repository_dict_entry') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_repository_dict_entry]
GO


CREATE TABLE [dbo].[dsg_uek_repository_dict_entry](
  [id] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
  [DSD_REPO89_PRZEDSIEWZIECIA_DYDAKTYCZNE] [numeric](19, 0) NULL,
  [DSD_REPO95_RODZAJ_STUDIOW] [numeric](19, 0) NULL,
  [DSD_REPO77_RODZAJ_PRZEDSIEWZIECIA] [numeric](19, 0) NULL,
  [DSD_REPO88_BUDYNKI] [numeric](19, 0) NULL,
  [DSD_REPO97_DS] [numeric](19, 0) NULL,
  [DSD_REPO98_DOTACJA] [numeric](19, 0) NULL,
  [DSD_REPO119_KPZF] [numeric](19, 0) NULL,
  CONSTRAINT [PK_dsg_uek_repository_dict_entry] PRIMARY KEY CLUSTERED
    (
      [id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

IF OBJECT_ID('dbo.dsg_uek_invoice_to_vat') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_invoice_to_vat]
GO

CREATE TABLE [dbo].[dsg_uek_invoice_to_vat](
  [id] [bigint] IDENTITY(1,1) PRIMARY KEY,
  [INVOICE_ENTRY_ID] [numeric](19, 0) NULL,
  [STAWKA_ID] [numeric](19, 0) NULL,
)

GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_vat]  WITH CHECK ADD  CONSTRAINT [FK_dsg_uek_invoice_to_vat_dsg_uek_invoice_entry] FOREIGN KEY([INVOICE_ENTRY_ID])
REFERENCES [dbo].[dsg_uek_invoice_entry] ([ID])
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_vat] CHECK CONSTRAINT [FK_dsg_uek_invoice_to_vat_dsg_uek_invoice_entry]
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_vat]  WITH CHECK ADD  CONSTRAINT [FK_dsg_uek_invoice_to_vat_dsg_uek_stawki_vat] FOREIGN KEY([STAWKA_ID])
REFERENCES [dbo].[dsg_uek_stawki_vat] ([id])
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_vat] CHECK CONSTRAINT [FK_dsg_uek_invoice_to_vat_dsg_uek_stawki_vat]
GO




IF OBJECT_ID('dbo.dsg_uek_invoice_to_wnioskujacy') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy]
GO

CREATE TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [INVOICE_ENTRY_ID] [numeric](19, 0) NULL,
  [WNIOSKUJACY_ID] [numeric](19, 0) NULL,
)
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy]  WITH CHECK ADD  CONSTRAINT [FK_dsg_uek_invoice_to_wnioskujacy_dsg_uek_invoice_entry] FOREIGN KEY([INVOICE_ENTRY_ID])
REFERENCES [dbo].[dsg_uek_invoice_entry] ([ID])
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy] CHECK CONSTRAINT [FK_dsg_uek_invoice_to_wnioskujacy_dsg_uek_invoice_entry]
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy]  WITH CHECK ADD  CONSTRAINT [FK_dsg_uek_invoice_to_wnioskujacy_dsg_uek_wnioskujacy] FOREIGN KEY([WNIOSKUJACY_ID])
REFERENCES [dbo].[dsg_uek_wnioskujacy] ([id])
GO

ALTER TABLE [dbo].[dsg_uek_invoice_to_wnioskujacy] CHECK CONSTRAINT [FK_dsg_uek_invoice_to_wnioskujacy_dsg_uek_wnioskujacy]
GO

-------------SŁOWNIKI DOKUMENTU -----------------


IF OBJECT_ID('dbo.dsg_uek_international_service_code') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_international_service_code]
GO

CREATE TABLE dsg_uek_international_service_code(
  [id] [numeric](18, 0) PRIMARY KEY IDENTITY(1,1),
  [cn] [varchar](250) NULL,
  [title] [varchar](250) NULL,
  [centrum] [int] NULL,
  [refValue] [varchar](20) NULL,
  [available] [bit] NULL
)

GO


IF OBJECT_ID('dbo.dsg_uek_opis_faktury') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_opis_faktury]
GO

CREATE TABLE dsg_uek_opis_faktury(
  [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),
  [cn] [varchar](100) NULL,
  [title] [varchar](510) NULL,
  [available] [bit] NULL,
  [centrum] [int] NULL,
  [refValue] [varchar](20) NULL,
)

GO

-- VIEW WNIOSKÓW ZAKUPOWYCH

IF OBJECT_ID('dbo.dsg_uek_view_wnioski') IS NOT NULL
DROP VIEW [dbo].[dsg_uek_view_wnioski]
GO

CREATE VIEW [dbo].dsg_uek_view_wnioski AS
  select null as type, null as documentId, null as NR_WNIOSKU, null as erp_document_idm
  ----select 0 as type, document_id, numer_wniosku as NR_WNIOSKU, erp_document_idm from dsg_uek_wniosek_zakupowy zak
  ----where erp_document_idm is not null and ERP_STATUS = 3
  ----union all
    --select 1 as type, DOCUMENT_ID, OPIS_ZAMOWIENIA as NR_WNIOSKU, erp_document_idm from dsg_uek_zapotrzebowanie zp
    --where erp_document_idm is not null and ERP_STATUS = 3
    --union all
    --select 2 as type, document_id, CAST(NR_POSTEPOWANIA AS varchar(1024)) as NR_WNIOSKU, erp_document_idm from DSG_UEK_ZAM_PUB zapo
    --where erp_document_idm is not null and ERP_STATUS = 3