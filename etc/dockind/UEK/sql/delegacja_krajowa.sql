CREATE TABLE [dbo].[dsg_uek_delegacja_krajowa](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[DELEGOWANY] [int] NULL,
	[STANOWISKO] [varchar](255) NULL,
	[MIEJSCOWOSC] [varchar](50) NULL,
	[TERMIN_WYJAZDU] [datetime] NULL,
	[TERMIN_POWROTU] [datetime] NULL,
	[DIETA_KRAJOWA_MONEY] [numeric](19, 2) NULL,
	[CEL_WYJAZDU] [varchar](150) NULL,
	[KOSZT_NOCLEGU] [numeric](19, 2) NULL,
	[ZALICZKA] [tinyint] NULL,
	[forma_zaliczki] [int] NULL,
	[DATA_PRZELEWU] [datetime] NULL,
	[ZALICZKA_KWOTA] [numeric](19, 2) NULL,
	[ADRES_ZAMIESZKANIA_DELEGOWANEGO] [varchar](50) NULL,
	[NIE_PONOSILEM_KOSZTOW_NOCLEGU] [tinyint] NULL,
	[dieta_krajowa_ilosc] [numeric](19, 2) NULL,
	[dieta_krajowa_money_to_sum] [numeric](19, 2) NULL,
	[DOJAZD_RYCZALT_ILOSC] [numeric](19, 2) NULL,
	[DOJAZD_RYCZALT_COST] [numeric](19, 2) NULL,
	[NOCLEG_WG_RACHUN] [numeric](19, 2) NULL,
	[NOCLEGI_RYCZALT_ILOSC] [numeric](19, 2) NULL,
	[NOCLEGI_RYCZALT] [numeric](19, 2) NULL,
	[INNE_WYDATKI] [numeric](19, 2) NULL,
	[SUMA] [numeric](19, 2) NULL,
	[DO_WYPLATY_ZWROTU] [numeric](19, 2) NULL,
	[FORMA_ROZLICZENIA] [int] NULL,
	[NUMER_KONTA_ZALICZKA] [varchar](50) NULL,
	[NUMER_KONTA_ROZLICZENIE] [varchar](50) NULL,
	[BUDGET_KIND] [int] NULL,
	[SOURCE] [int] NULL,
	[BUDGET] [int] NULL,
	[BUDGET_KO] [int] NULL,
	[POSITION] [int] NULL,
	[FINANCIAL_TASK] [int] NULL,
	[PROJECT_BUDGET] [int] NULL,
	[CONTRACT] [int] NULL,
	[CONTRACT_STAGE] [int] NULL,
	[FUND] [int] NULL,
	[ENTRY_ID] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[document_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dsg_dk_forma_regulowania_koncowa] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_dk_forma_regulowania_koncowa values ('KASA','Wp³ata / Wyp³ata w kasie',NULL,NULL,1);
insert into dsg_dk_forma_regulowania_koncowa values ('POTRACENIE','Potr±cenie z wynagrodzenia',NULL,NULL,1);
insert into dsg_dk_forma_regulowania_koncowa values ('PRZELEW','Przelew na konto',NULL,NULL,1);

CREATE TABLE [dsg_dk_forma_regulowania_zaliczki] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[TITLE] [varchar](255) NULL,
	[CENTRUM] NUMERIC(18,0) NULL,
	[REFVALUE] varchar(20) NULL,
	[AVAILABLE] bit default 1 not null,
	PRIMARY KEY (ID)
);

insert into dsg_dk_forma_regulowania_zaliczki values ('KASA','Wp³ata / Wyp³ata w kasie',NULL,NULL,1);
insert into dsg_dk_forma_regulowania_zaliczki values ('PRZELEW','Przelew na konto',NULL,NULL,1);

--TODO
CREATE TABLE [dbo].[dsg_uek_dk_jednostka_realizujaca](
  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
  [JEDNOSTKI_REALI] [numeric](18, 0) NULL,
  [JEDNOSTKA_ORGANI] [numeric](18, 0) NULL,
)

CREATE TABLE [dsg_uek_dk_liczb_zapewnionych_posilkow] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[SNIADANIE] NUMERIC(18,0) NULL,
	[OBIADY] NUMERIC(18,0) NULL,
	[KOLACJE] NUMERIC(18,0) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_uek_dk_inne_koszty] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[RODZAJ_INNYCH_KOSZTOW_1] NUMERIC(18,0) NULL,
	[OPIS_INNYCH_KOSZTOW_1] varchar(50) NULL,
	[WARTOSC_INNYCH_KOSZTOW_1] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_uek_dk_przejazd] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[RODZAJ_PRZEJ_1] NUMERIC(18,0) NULL,
	[KWOTA_PRZEJ_1] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_uek_dk_poniesione_koszty] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[MIEJSCOWOSC_WYJAZDU_1] varchar(50) NULL,
	[DATA_WYJAZDU_1] DATE NULL,
	[MIEJSCOWOSC_PRZYJAZDU_1] varchar(50) NULL,
	[DATA_PRZYJAZDU_1] DATE NULL,
	[SRODEK_LOKOMOCJI_1] NUMERIC(18,2) NULL,
	[OPIS_1] varchar(50) NULL,
	[WARTOSC_1] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);

CREATE TABLE [dsg_uek_dk_koszty_przejazdu_sam] (
	[ID] NUMERIC(18,0) IDENTITY(1,1) NOT NULL,
	[SKAD_1] varchar(50) NULL,
	[DOKAD_1] varchar(50) NULL,
	[ILOSC_KM_1] NUMERIC(18,2) NULL,
	[POTWIERDZENIE_ILOSCI_KM_1] NUMERIC(18,2) NULL,
	[STAWKA_1] NUMERIC(18,4) NULL,
	[WARTOSC_1] NUMERIC(18,2) NULL,
	PRIMARY KEY (ID)
);
