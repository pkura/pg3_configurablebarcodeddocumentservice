--ALTER TABLE DSO_IN_DOCUMENT
--DROP constraint dso_in_document_delivery_fk;

--drop table dso_in_document_delivery;

--create table dso_in_document_delivery (
--    ID                      int identity not null PRIMARY KEY,
--    POSN                    smallint NOT NULL,
--    NAME                    varchar(40) NOT NULL
--);

insert into dso_in_document_delivery values 
(1,'Poczta'),
(2,'Kurier -DHL'),
(3,'Kurier - si�demka'),
(4,'Kurier - UPS'),
(5,'Kurier - GLS'),
(6,'Kurier - PDP'),
(7,'Kurier - TNT'),
(8,'Kurier - Viva'),
(9,'Kurier - express logistic'),
(10,'Faks'),
(11,'E-mail'),
(12,'Osobi�cie'),
(13,'ePUAP');

--alter table dso_in_document add constraint dso_in_document_delivery_fk foreign key (delivery) references dso_in_document_delivery (id);

select * from dso_in_document_delivery;
