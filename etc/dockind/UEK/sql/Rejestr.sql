CREATE TABLE [dbo].[dsg_uek_rejestr](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[DATA_KONCA] [datetime] NULL,
	[DATA_DOK] [datetime] NULL,
	[RODZAJ_DOKUMENTU] [numeric](18, 0) NULL,
	[PODTYP] [numeric](18, 0) NULL,
	[KWOTA] [numeric](18, 2) NULL,
	[OPIS] [varchar](1000) NULL,
	[CONTRACTOR] [numeric](19, 0) NULL,
	[UMOWA_ERP] [numeric](18, 0) NULL,
	[TO_ZAMOWIENIE_PUBLICZNE] [numeric](19, 0) NULL
) ON [PRIMARY]

GO

