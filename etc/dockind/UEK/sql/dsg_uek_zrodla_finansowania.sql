IF OBJECT_ID('dbo.dsg_uek_zrodlo_finansowania') IS NOT NULL
DROP TABLE [dbo].[dsg_uek_zrodlo_finansowania]
GO

CREATE TABLE [dbo].[dsg_uek_zrodlo_finansowania](
	[id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
	[BUDGET_KIND] [numeric](19, 0) NULL,
	[BUDGET_KO] [numeric](19, 0) NULL,
	[BUDGET] [numeric](19, 0) NULL,
	[POSITION] [numeric](19, 0) NULL,
	[FINANCIAL_TASK] [numeric](19, 0) NULL,
	[CONTRACT] [numeric](19, 0) NULL,
	[CONTRACT_STAGE] [numeric](19, 0) NULL,
	[PROJECT_BUDGET] [numeric](19, 0) NULL,
	[FUND] [numeric](19, 0) NULL,
	[SOURCE] [numeric](19, 0) NULL,
	[ZRODLO_DESCRIPTION] [varchar](2000) NULL,
	[POSITION_NUMBER] INT NULL
)

GO
