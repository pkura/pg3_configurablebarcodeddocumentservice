create table DSG_UEK_ZAM_PUB (
	[DOCUMENT_ID]                   [numeric](19, 0)  PRIMARY KEY,
	[STATUS]                        [numeric](18, 0)  NULL,

    [JEDNOSTKA_REALIZUJACA]         [numeric](18, 0)  NULL,
    [PRZEDMIOT_ZAMOWIENIA]          [varchar](255)    NULL,
    [RODZAJ_ZAMOWIENIA]             [numeric](18, 0)  NULL,
    [KURS_EURO]                     [numeric](18, 2)  NULL,
    [MODULY_ZAMOWIENIA_REALIZACJA]  [bit]             NULL,
    [MODUL_SUM_WARTOSC_NETTO]       [numeric](18,2)   NULL,
    [MODUL_SUM_WARTOSC_NETTO_EURO]  [numeric](18,2)   NULL,
    [MODUL_SUM_WARTOSC_BRUTTO]      [numeric](18,2)   NULL,
    [MODUL_SUM_FINANSOWANIE_BRUTTO] [numeric](18,2)   NULL,
--  [CZESC_WIEKSZEGO_ZAMOWIENIA]    [numeric](18, 0)  NULL,
    [CZESC_ZAMOWIENIA_WARTOSC]      [numeric](18,2)   NULL,
    [CZESC_ZAMOWIENIA_WARTOSC_EUR]  [numeric](18,2)   NULL,
    [WARTOSCI_ZAM_DATA_USTALENIA]   [datetime]        NULL,
    [WARTOSCI_ZAM_PODSTAWA_USTALENIA] [varchar](255)  NULL,
    [WARTOSCI_ZAM_OS_USTALAJACA]    [numeric](18, 0)  NULL,
    [WSPOLFINANSOWANIE_UE]          [bit]             NULL,
    [SRODKI_UE_UDZAL_PROC]          [numeric](18, 2)  NULL,
    [NAZWA_PROJEKTU]                [varchar](255)    NULL,
    [TRYB_ZAM]                      [numeric](18, 0)  NULL,
    [UMOWA_RAMOWA]                  [numeric](18, 0)  NULL,
    [PODSTAWA_PRAWNA]               [numeric](18, 0)  NULL,
    [UZASADNIENIE_TRYBU]            [varchar](255)    NULL,
    [CHARAKTERYSTYKA_RYNKU]         [varchar](255)    NULL,
    [OS_PROWADZACA_SPRAWE]          [numeric](18, 0)  NULL,
--  [WNIOSEK_INWESTYCYJNY]          [numeric](18, 0)  NULL,
    [PRZEKAZANIE_PO_KOM_PRZET]      [numeric](18, 0)  NULL,
    [PRZEKAZANIE_PO_ZATW_TRYBU]     [numeric](18, 0)  NULL,
    [UWAGI_KOM_PRZET]               [varchar](255)    NULL,
    [SKAN_PROP_TRYB_POST]           [numeric](18, 0)  NULL,
    [UWAGI_DO_TRYBU]                [varchar](255)    NULL,
    [PRZEDMIOT_DZIALALNOSCI]		[numeric](18, 0)  NULL,
	[NR_POSTEPOWANIA] 				[int]             NULL,

	[TO_REJESTR] 					[numeric](19, 0)  NULL,
	[DATA_KONCA] 					[datetime]        NULL,
	[DATA_DOK] 						[datetime]        NULL,
	[RODZAJ_DOKUMENTU] 				[numeric](18, 0)  NULL,
	[PODTYP] 						[numeric](18, 0)  NULL,
	[KWOTA]							[numeric](18, 2)  NULL,
	[OPIS] 							[varchar](100)    NULL,
	[CONTRACTOR]					[numeric](19, 0)  NULL,
	[UMOWA_ERP]						[numeric](18, 0)  NULL,
	[TO_ZAMOWIENIE_PUBLICZNE]		[numeric](19, 0)  NULL,
    [NR_OGLOSZENIA]                 [varchar](100)    NULL,
	[KRAJ_POCHODZENIA]              [varchar](2)      NULL,
    [FINANCIAL_ID]		            [VARCHAR](50)     NULL,
    [BUDGET_KIND]		            [numeric](18, 0)  NULL,
    [SOURCE]		                [numeric](18, 0)  NULL,
    [FINANCIAL_AMOUNT]		        [numeric](18, 2)  NULL,
    [TZ_ZAMOWIENIE]                 [bit]             NULL,
    [TZ_UMOWY_RAMOWE]               [bit]             NULL,


    [BUDGET_KO]		                [numeric](18, 0)  NULL,
    [BUDGET]		                [numeric](18, 0)  NULL,
    [POSITION]		                [numeric](18, 0)  NULL,
    [FINANCIAL_TASK]		        [numeric](18, 0)  NULL,
    [PROJECT_BUDGET]		        [numeric](18, 0)  NULL,
    [CONTRACT]		                [numeric](18, 0)  NULL,
    [CONTRACT_STAGE]		        [numeric](18, 0)  NULL,
    [FUND]		                    [numeric](18, 0)  NULL,

    [POPUP_MODUL_ID]		            [VARCHAR](50)     NULL,
    [POPUP_MODUL_NAZWA]		            [VARCHAR](100)    NULL,
    [POPUP_MODUL_KODY_CPV_MAIN]		    [numeric](18, 0)  NULL,
    [POPUP_MODUL_KODY_CPV_TEXT]		    [varchar](1024)   NULL,
    [POPUP_MODUL_WARTOSC_NETTO]		    [numeric](18, 2)  NULL,
    [POPUP_MODUL_WARTOSC_NETTO_EURO]    [numeric](18, 2)  NULL,
    [POPUP_MODUL_WARTOSC_BRUTTO]	    [numeric](18, 2)  NULL,
    [POPUP_MODUL_STAWKA_VAT]		    [numeric](18, 0)  NULL,
    [POPUP_MODUL_FINANS_BRUTTO]	        [numeric](18, 2)  NULL,
    [POPUP_MODUL_REALIZACJA_OD]		    [datetime]        NULL,
    [POPUP_MODUL_REALIZACJA_DO]		    [datetime]        NULL
)

GO

ALTER TABLE DSG_UEK_ZAM_PUB
ADD ERP_STATUS INTEGER NULL,
erp_document_idm VARCHAR(50) NULL

GO


CREATE TABLE DSG_UEK_ZAM_PUB_MULTIPLE_VALUE(
	[DOCUMENT_ID]                   [numeric](18, 0) NOT NULL,
	[FIELD_CN]                      [varchar](100)   NOT NULL,
	[FIELD_VAL]                     [varchar](100)   NULL
)

-- TABELE DLA DOKUMENTU

CREATE TABLE DSG_UEK_ZAM_PUB_ZAPOTRZEBOWANIA(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

    [ZAPOTRZEBOWANIE]               [numeric](18,2)  NULL
)
CREATE TABLE DSG_UEK_ZAM_PUB_MODULY(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

	[POPUP_MODUL_NAZWA]                         [varchar](100)   NULL,
	[POPUP_MODUL_KODY_CPV_TEXT]                 [varchar](1024)  NULL,
	[POPUP_MODUL_KODY_CPV_MAIN]                 [numeric](18,0)  NULL,
	[POPUP_MODUL_WARTOSC_NETTO]                 [numeric](18,2)  NULL,
	[POPUP_MODUL_WARTOSC_NETTO_EURO]            [numeric](18,2)  NULL,
	[POPUP_MODUL_WARTOSC_BRUTTO]                [numeric](18,2)  NULL,
	[POPUP_MODUL_FINANS_BRUTTO]                 [numeric](18,2)  NULL,
	[POPUP_MODUL_STAWKA_VAT]                    [numeric](18,0)  NULL,
	[POPUP_MODUL_REALIZACJA_OD]                 [datetime]       NULL,
	[POPUP_MODUL_REALIZACJA_DO]                 [datetime]       NULL
)
CREATE TABLE DSG_UEK_ZAM_PUB_ZDARZENIA(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

	[ENTRYID]                                   [numeric](18,0)  NULL,
	[DATE]                                      [datetime]       NULL,
	[DESCRIPTION]                               [varchar](1024)  NULL
)
CREATE TABLE DSG_UEK_ZAM_PUB_ZR_FINANS(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

    [FINANCIAL_ID]		            [VARCHAR](50)    NULL,

    [FINANCIAL_AMOUNT]              [numeric](18,0)  NULL,
    [IDENTYFIKATOR_PLANU]           [numeric](18,0)  NULL,
    [POZYCJA_PLANU]                 [numeric](18,0)  NULL,

    [DESCRIPTION]		            [VARCHAR](2000)  NULL,
    [ZRODLO_ID]                     [numeric](18,0)  NULL
)
CREATE TABLE DSG_UEK_ZAM_PUB_CZESC_WIEKSZEGO_ZAM(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

    [WARTOSC]               [numeric](18,2)  NULL,
    [WARTOSC_EUR]           [numeric](18,2)  NULL
)
CREATE TABLE DSG_UEK_ZAM_PUB_KRYTERIA_OCENY(
    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),

    [OPIS]                  [varchar](150)   NULL,
    [PROCENT]               [numeric](18,2)  NULL
)


CREATE TABLE [DBO].[DSG_UEK_ZAM_PUB_TO_CPV](
  [ID]                      [NUMERIC](18, 0) IDENTITY(1,1) PRIMARY KEY,
  [MODULE_ID]               [NUMERIC](18, 0) NULL,
  [CPV_ID]                  [NUMERIC](18, 0) NULL,
)
ALTER TABLE [DBO].[DSG_UEK_ZAM_PUB_TO_CPV]  WITH CHECK ADD  CONSTRAINT [FK_DSG_UEK__CPV__TO__ZAM_PUB_ENTRY] FOREIGN KEY([MODULE_ID]) REFERENCES [DBO].[DSG_UEK_CPV] ([ID])
ALTER TABLE [DBO].[DSG_UEK_ZAM_PUB_TO_CPV] CHECK CONSTRAINT [FK_DSG_UEK__CPV__TO__ZAM_PUB_ENTRY]

-- TABELE SLOWNIKOWE

CREATE TABLE DSG_UEK_DICT_TRYB_ZAM_PUB(
  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
  [cn]                      [varchar](100)   NULL,
  [title]                   [varchar](510)   NULL,
  [available]               [bit]            NULL,
  [centrum]                 [int]            NULL,
  [refValue]                [varchar](20)    NULL
)

CREATE TABLE DSG_UEK_DICT_TRYB_UMOWY_RAMOWE(
  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
  [cn]                      [varchar](100)   NULL,
  [title]                   [varchar](510)   NULL,
  [available]               [bit]            NULL,
  [centrum]                 [int]            NULL,
  [refValue]                [varchar](20)    NULL
)

CREATE TABLE DSG_UEK_DICT_PODSTAWA_PRAWNA(
  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
  [cn]                      [varchar](100)   NULL,
  [title]                   [varchar](510)   NULL,
  [available]               [bit]            NULL,
  [centrum]                 [int]            NULL,
  [refValue]                [varchar](20)    NULL
)
CREATE TABLE DSG_UEK_CPV(
  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
  [cn]                      [varchar](100)   NULL,
  [title]                   [varchar](510)   NULL,
  [available]               [bit]            NULL,
  [centrum]                 [int]            NULL,
  [refValue]                [varchar](20)    NULL
)


INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'WylArt4',N'Wy��czenia art. 4',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Zamobdyn',N'Zam�wienia obj�te dynamicznym systemem zakup�w udzielone z odpowiednim stosowaniem trybu przetargu nieograniczonego',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'PrzNogr',N'Przetarg nieograniczony',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'PrzOgr',N'Przetarg ograniczony',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'NegZZ',N'Negocjacje bez og�oszenia',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'NegZZ',N'Negocjacje z og�oszeniem',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'ZapOO',N'Zapytanie o cen�',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'ZamZWR',N'Zam�wienie z wolnej r�ki',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'LicEl',N'Licytacja elektroniczna',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'DialKonk',N'Dialog konkurencyjny',1,NULL,NULL);


INSERT INTO DSG_UEK_DICT_TRYB_UMOWY_RAMOWE (cn, title,available,centrum,refValue)VALUES (N'PrzNogr',N'Przetarg nieograniczony',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_UMOWY_RAMOWE (cn, title,available,centrum,refValue) VALUES (N'PrzOgr',N'Przetarg ograniczony',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_TRYB_UMOWY_RAMOWE (cn, title,available,centrum,refValue) VALUES (N'NegZZ',N'Negocjacje z og�oszeniem',1,NULL,NULL);






INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt1a',N'pkt 1a. Szczeg�lnej procedury organizacji mi�dzynarodowej odmiennej od okre�lonej w ustawie',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt1b',N'pkt 1b. Um�w mi�dzynarodowych, kt�rych stron� jest Rzeczpospolita Polska, dotycz�cych stacjonowania wojsk, je�eli umowy te przewiduj� inne ni� ustawa procedury udzielania zam�wie�,',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt1c',N'pkt 1c. Umowy mi�dzynarodowej zawartej mi�dzy Rzecz�pospolit� Polsk� a jednym lub wieloma pa�stwami nieb�d�cymi cz�onkami Unii Europejskiej, dotycz�cej wdro�enia lub realizacji przedsi�wzi�cia przez strony tej umowy, je�eli umowa ta przewiduje inne ni� ustawa procedury udzielania zam�wie�;',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3a',N'pkt 3a. Us�ugi arbitra�owe lub pojednawcze',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3b',N'pkt 3b. Us�ugi Narodowego Banku Polskiego',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3e',N'pkt 3e. Us�ugi w zakresie bada� naukowych i prac rozwojowych oraz �wiadcze� us�ug badawczych...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3g',N'pkt 3g. Nabycie, przygotowanie, produkcja lub koprodukcja materia��w programowanych przeznaczonych do emisji w mediach',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3h',N'pkt 3h. Zakup czasu antenowego',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3i',N'pkt 3i. Nabycie w�asno�ci nieruchomo�ci oraz innych praw do nieruchomo�ci, w szczeg�lno�ci dzier�awy i najmu',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt3j',N'pkt 3j. Us�ugi finansowe zwi�zane z emisj�, sprzeda��, kupnem lub transferem papier�w warto�ciowych lub innych instr...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt4',N'pkt 4. Um�w z zakresu prawa pracy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt5',N'pkt 5. Zam�wie�, kt�rym nadano klauzul� "tajne" lub "�ci�le tajne" zgodnie z przepisami o ochronie informacji niejawnych...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt6',N'pkt 6. zam�wie� na us�ugi udzielane innemu zamawiaj�cemu, o kt�rym mowa w art. 3 ust. 1 pkt 1-3a, kt�remu przyznano, w drodze ustawy lub decyzji administracyjnej, wy��czne prawo do �wiadczenia tych us�ug;',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt7',N'pkt 7. Przyznanie dotacji ze �rodk�w publicznych, je�eli dotacje te s� przyznawane na podstawie ustaw',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt8',N'pkt 8. Zam�wie� lub konkurs�w, kt�rych warto�� nie przekracza wyra�onej w z�otych r�wnowarto�ci kwoty 30.000 euro',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt11',N'pkt 11. Nabywanie dostaw, us�ug, rob�t budowlanych od centralnego zamawiaj�cego lub od wykonawc�w wybranych przez centra...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt12',N'pkt 12. Koncesji na roboty budowlane oraz koncesji na us�ugi w rozumieniu ustawy z dn. 9.01.2009 o koncesji na roboty...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt13',N'pkt 13. Zam�wienia Banku Gospodarstwa Krajowego, o kt�rych mowa w art. 4 pkt 2a ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt14',N'pkt 14. Zam�wienia, kt�rych przedmiotem s� us�ugi Narodowego Banku Polskiego',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt15',N'pkt 15. Zam�wienia, kt�rych przedmiotem s� us�ugi Banku Gospodarstwa Krajowego, o kt�rych mowa w art. 4 pkt 3 lit. l ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt16',N'pkt 16. Zam�wienia, kt�rych przedmiotem s� dostawy uprawnie� do emisji do powietrza gaz�w cieplarnianych i innych substancji, jednostek po�wiadczonej redukcji emisji oraz jednostek redukcji emisji...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt17',N'pkt 17. zam�wienia, kt�rych g��wnym celem jest pozwolenie zamawiaj�cym na oddanie do dyspozycji publicznej sieci telekomunikacyjnej, eksploatacja publicznej sieci telekomunikacyjnej, lub �wiadczenie publicznie dost�pnych us�ug telekomunikacyjnych za pomoc� publicznej sieci telekomunikacyjnej',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt18',N'pkt 18. Koncesje na us�ugi w rozumieniu ustawy z dnia 9 stycznia 2009 r. o koncesji na roboty budowlane lub us�ugi',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt19',N'pkt 19. zam�wienia udzielone instytucji gospodarki bud�etowej przez organ w�adzy publicznej wykonuj�cy funkcje organu za�o�ycielskiego tej instytucji, w przypadku spe�nienia warunk�w, o kt�rych mowa w art. 4 pkt 13 ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt20',N'pkt 20. zam�wienia sektorowe na dostawy, us�ugi lub roboty budowlane, kt�re zosta�y udzielone podmiotom powi�zanym z zamawiaj�cym w spos�b, o kt�rym mowa w art. 136 ust. 1 ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt21',N'pkt 21. zam�wienia sektorowe na us�ugi lub roboty budowlane udzielone przez podmiot utworzony przez zamawiaj�cych w celu wsp�lnego wykonywania dzia�alno�ci, o kt�rej mowa w art. 132 ustawy, jednemu z tych zamawiaj�cych lub podmiotowi powi�zanemu z jednym z tych zamawiaj�cych, w spos�b, o kt�rym mowa w art. 136 ust. 1 ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt22',N'pkt 22. zam�wienia sektorowe udzielone podmiotowi utworzonemu przez zamawiaj�cych w celu wsp�lnego wykonywania dzia�alno�ci, o kt�rej mowaw art. 132 ustawy, przez jednego z tych zamawiaj�cych, o ile podmiot ten zosta� utworzony na okres co najmniej 3 lat, a z dokumentu, na podstawie kt�rego zosta� utworzony, wynika, �e zamawiaj�cy pozostan� jego cz�onkami w tym okresi',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt23',N'pkt 23. zam�wienia sektorowe udzielone w celu wykonywania dzia�alno�ci polegaj�cej na dostarczaniu gazu do sieci przeznaczonych do �wiadczeniapublicznych us�ug zwi�zanych z produkcj�, przesy�aniem lub dystrybucj� gazu, o ile produkcja gazu stanowi niezb�dn� konsekwencj�prowadzenia dzia�alno�ci innej ni� dzia�alno��, o kt�rej mowa w art. 132 ustawy, oraz ...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt24',N'pkt 24. zam�wienia sektorowe udzielone w celu wykonywania dzia�alno�ci polegaj�cej na dostarczaniu ciep�a do sieci przeznaczonych do �wiadczeniapublicznych us�ug zwi�zanych z produkcj�, przesy�aniem lub dystrybucj� ciep�a, o ile produkcja ciep�a stanowi niezb�dn� konsekwencj�prowadzenia dzia�alno�ci innej ni� dzia�alno��, o kt�rej mowa w art. 132 ustawy,...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt25',N'pkt 25. zam�wienia sektorowe udzielone w celu wykonywania dzia�alno�ci polegaj�cej na dostarczaniu energii elektrycznej do sieci przeznaczonych do�wiadczenia publicznych us�ug zwi�zanych z produkcj�, przesy�aniem lub dystrybucj� energii elektrycznej, o ile produkcja energii elektrycznejjest niezb�dna do prowadzenia dzia�alno�ci innej ni� dzia�alno��,...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt26',N'pkt 26. zam�wienia sektorowe udzielone w celu wykonywania dzia�alno�ci polegaj�cej na dostarczaniu energii elektrycznej do sieci przeznaczonych do�wiadczenia publicznych us�ug zwi�zanych z produkcj�, przesy�aniem lub dystrybucj� energii elektrycznej, o ile produkcja energii elektrycznej jest niezb�dna do prowadzenia dzia�alno�ci innej ni� dzia�alno��...',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt27',N'pkt 27. zam�wienia sektorowe udzielone w celu odsprzeda�y lub wynajmu przedmiotu zam�wienia osobom trzecim, o ile zamawiaj�cy nie posiada szczeg�lnego lub wy��cznego prawa do sprzeda�y lub wynajmu przedmiotu zam�wienia, a inne podmioty mog� przedmiot zam�wienia bezogranicze� sprzedawa� lub wynajmowa� na tych samych warunkach co zamawiaj�cy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt28',N'pkt 28. zam�wienia sektorowe maj�ce na celu udzielenie koncesji na roboty budowlane, o ile koncesje takie s� udzielane w celu wykonywania dzia�alno�ci, o kt�rej mowa w art. 132 ustawy',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt29',N'pkt 29. zam�wienia sektorowe udzielone w celu wykonywania dzia�alno�ci, o kt�rej mowa w art. 132 ustawy, poza obszarem Unii Europejskiej, o ile do jej wykonywania nie jest wykorzystywana sie� znajduj�ca si� na obszarze Unii Europejskiej lub obszar Unii Europejskiej',1,NULL,NULL);
INSERT INTO DSG_UEK_DICT_PODSTAWA_PRAWNA (cn, title,available,centrum,refValue) VALUES (N'pkt30',N'pkt 30. zam�wienia, kt�rych warto�� nie przekracza wyra�onej w z�otych r�wnowarto�ci kwoty, o kt�rej mowa w art. 4 pkt 8 ustawy',1,NULL,NULL);




CREATE VIEW DSG_UEK_CPV_MAIN AS
    SELECT
        id AS 'id',
        cn AS 'cn',
        title AS 'title',
        available AS 'available',
        null AS 'centrum',
        null AS 'refValue'
    FROM DSG_UEK_CPV where refValue = 'MAIN'
CREATE VIEW DSG_UEK_CPV_EXTRA AS
    SELECT
        id AS 'id',
        cn AS 'cn',
        title AS 'title',
        available AS 'available',
        null AS 'centrum',
        null AS 'refValue'
    FROM DSG_UEK_CPV where refValue = 'EXTRA'

--

CREATE PROCEDURE PROCEDURE_UPDATE_EURO_IN_MODULES
		@ModuleId numeric(18,0),
		@EuroRate numeric(18,2)
	AS
	BEGIN
		UPDATE DSG_UEK_ZAM_PUB_MODULY SET POPUP_MODUL_WARTOSC_NETTO_EURO = (POPUP_MODUL_WARTOSC_NETTO * @EuroRate) WHERE ID = @ModuleId
	END


--insert into DSG_UEK_ZAM_PUB_MODULY (POPUP_MODUL_WARTOSC_NETTO) values (100)
--exec PROCEDURE_UPDATE_EURO_IN_MODULES 2, 4.25
--select * from DSG_UEK_ZAM_PUB_MODULY





--TYMCZASOWE, DLA TESTU
--INSERT INTO DSG_UEK_CPV (cn, title,available,centrum,refValue) VALUES (N'TestMain',N'Test Kod CPV g��wny',1,NULL,'MAIN);
--INSERT INTO DSG_UEK_CPV (cn, title,available,centrum,refValue) VALUES (N'TestExtra',N'Test Kod CPV dodatkowy',1,NULL,'EXTRA');




















--
----------------
----------------
----------------
----------------
----------------
----------------      STARE
----------------
----------------
----------------
----------------
----------------
--
--CREATE TABLE DSG_UEK_ZAM_PUB_WARTOSC_SZAC(
--    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),
--
--    [NETTO]                 [numeric](18,2)  NULL,
--    [BRUTTO]                [numeric](18,2)  NULL,
--    [NETTO_EUR]             [numeric](18,2)  NULL,
--    [BRUTTO_EUR]            [numeric](18,2)  NULL,
--    [DATA_USTALENIA]        [datetime]       NULL,
--    [OS_USTALAJACA]         [numeric](18,0)  NULL,
--    [PODSTAWA]              [varchar](150)   NULL
--)
----CREATE TABLE DSG_UEK_ZAM_PUB_ZR_FINANS(
----    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),
----
----    [ZR_FINANS]             [numeric](18,0)  NULL
----)
--CREATE TABLE DSG_UEK_ZAM_PUB_KODY_CPV(
--    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),
--
--    [KOD_CPV_1]             [numeric](18,0)  NULL
--)
--CREATE TABLE DSG_UEK_ZAM_PUB_ZDARZENIA(
--    [id] [numeric](18,0) PRIMARY KEY IDENTITY(1,1),
--
--    [DATA]                  [datetime]       NULL,
--    [OPIS]                  [varchar](150)   NULL
--)
--
--
--
--CREATE TABLE DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB(
--  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
--  [cn]                      [varchar](100)   NULL,
--  [title]                   [varchar](510)   NULL,
--  [available]               [bit]            NULL,
--  [centrum]                 [int]            NULL,
--  [refValue]                [varchar](20)    NULL
--)
--CREATE TABLE DSG_UEK_DICT_PROTOTYP_ZAM_PUB(
--  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
--  [cn]                      [varchar](100)   NULL,
--  [title]                   [varchar](510)   NULL,
--  [available]               [bit]            NULL,
--  [centrum]                 [int]            NULL,
--  [refValue]                [varchar](20)    NULL
--)
--
----CREATE TABLE [dbo].[dsg_uek_zrodlo_finansowania](
----  [id] [numeric](19, 0) IDENTITY(1,1) PRIMARY KEY,
----  [BUDGET_KIND] [numeric](18, 0) NULL,
----  [BUDGET] [numeric](19, 0) NULL,
----  [POSITION] [numeric](19, 0) NULL,
----  [SOURCE] [numeric](19, 0) NULL,
----  [CONTRACT] [numeric](19, 0) NULL,
----  [CONTRACT_STAGE] [numeric](19, 0) NULL,
----  [FUND] [numeric](19, 0) NULL,
----  [ZRODLO_DESCRIPTION] [varchar](2000) NULL,
----  [PROJECT_BUDGET] [numeric](19, 0) NULL,
----)
--CREATE VIEW DSG_UEK_DICT_ZR_FINANS_ZAM_PUB AS
--    SELECT
--        f.id AS 'id',
--        convert(varchar(50), id) AS 'cn',
--        f.ZRODLO_DESCRIPTION AS 'title',
--        1 AS 'available',
--        null AS 'centrum',
--        null AS 'refValue'
--    FROM DSG_UEK_ZRODLO_FINANSOWANIA f
----CREATE TABLE DSG_UEK_DICT_ZR_FINANS_ZAM_PUB(
----  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
----  [cn]                      [varchar](100)   NULL,
----  [title]                   [varchar](510)   NULL,
----  [available]               [bit]            NULL,
----  [centrum]                 [int]            NULL,
----  [refValue]                [varchar](20)    NULL
----)
--CREATE TABLE DSG_UEK_DICT_POZYCJA_PLANU_ZAM_PUB(
--  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
--  [cn]                      [varchar](100)   NULL,
--  [title]                   [varchar](510)   NULL,
--  [available]               [bit]            NULL,
--  [centrum]                 [int]            NULL,
--  [refValue]                [varchar](20)    NULL
--)
--CREATE TABLE DSG_UEK_DICT_KOD_CPV_ZAM_PUB(
--  [id]                      [numeric](18,0)  PRIMARY KEY IDENTITY(1,1),
--  [cn]                      [varchar](100)   NULL,
--  [title]                   [varchar](510)   NULL,
--  [available]               [bit]            NULL,
--  [centrum]                 [int]            NULL,
--  [refValue]                [varchar](20)    NULL
--)
--
--
--INSERT INTO DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Wn',N'Wniosek',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'DOZW',N'Decyzja o zatwierdzenie wniosku',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Spec','Specyfikacja',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'DZP',N'Dokumentacja z post�powania',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_RODZAJ_DOK_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Um',N'Umowy',1,NULL,NULL);
--
--
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'PZP',N'Protok� z post�powania',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'OOZP',N'Og�oszenie o zam�wieniu publicznym',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Ofer',N'Oferty',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'WnP',N'Wynik post�powania',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Pyt',N'Pytania',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Odp',N'Odpowiedzi',1,NULL,NULL);
--INSERT INTO DSG_UEK_DICT_PROTOTYP_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'Odw',N'Odwo�ania',1,NULL,NULL);
--
----INSERT INTO DSG_UEK_DICT_POZYCJA_PLANU_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'tst1',N'...test... pozycja planu zam�wienia publicznego 1',1,NULL,NULL);
--
----INSERT INTO DSG_UEK_DICT_TRYB_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'tst1',N'...test... tryb zam�wienia publicznego 1',1,NULL,NULL);
--
----INSERT INTO DSG_UEK_DICT_KOD_CPV_ZAM_PUB (cn, title,available,centrum,refValue) VALUES (N'tst1',N'...test... kod cpv1',1,NULL,NULL);