-- waga
-- zpo  			1 - zpo, 0 - brak zpo;
-- poziomGabarytu   1 - A, 2 - B, 3 - strefa A, 4- strefa B, 5 - strefa C, 6 - strefa D, 7 - europejskie, 8 - pozaeuropejskie;
-- poziomListu      1 - zwyk�y krajowy ekonomiczny, 2 - krajowy zwykly priorytet, 3 - krajowy polecony ekonomiczny, 4 - polecony krajowy priorytet,
--					5 - przesylki reklamowe, 6 - zagraniczny zwykly ekonomiczny, 7 - zagraniczny zwykly priorytet, 8 - zagraniczny polecony ekonomiczny,
--					9 - zagraniczny polecony priorytet, 10 - Paczka, 11 - ePUAP, 12 - kurier, 13 - anulowano, 14 - kartka pocztowa EKO, 15 - kartka pocztowa prio; 
-- poziomWagi       1 - do 50g 2 - do 100g  3 - do 350g  4 - do 500g ,5 - do 1000g, 6 - do 2000g, 7 - do 2500g, 8 - do 3000g, 9 - do 3500g, 10 - do 4000g, 11 - do 5000g;
-- kwota
CREATE TABLE DS_STAWKI_OPLAT_POCZTOWYCH
(
ID numeric(18, 0) IDENTITY(1,1) NOT NULL,
waga varchar(20) not null,
zpo smallint not null default 0,
poziomGabarytu smallint not null,
poziomListu smallint not null,
poziomWagi smallint not null,
kwota numeric(6, 2) not null
);

-- EKONOMICZNE KRAJOWE (LISTY ZWYKLE)-- GABARYT A -- WAGA 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 1, 1, 6, 1.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 1, 1, 7, 3.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 1, 1, 8, 6.30);

-- EKONOMICZNE KRAJOWE (LISTY POLECONE)-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 1, 3, 6, 3.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 1, 3, 7, 5.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 1, 3, 8, 8.50);

-- EKONOMICZNE KRAJOWE (LISTY POLECONE ZPO)-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 1, 3, 6, 5.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 1, 3, 7, 7.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 3, 8, 10.40);

----------------------------------------------------------------------------------

-- PRIORYTETOWE KRAJOWE (LISTY ZWYKLE)-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 1, 2, 6, 2.35);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 1, 2, 7, 4.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 1, 2, 8, 8.80);

-- PRIORYTETOWE KRAJOWE (LISTY POLECONE)-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 1, 4, 6, 4.55);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 1, 4, 7, 7.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 1, 4, 8, 11.00);

-- PRIORYTETOWE KRAJOWE (LISTY POLECONE ZPO)-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 1, 4, 6, 6.45);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 1, 4, 7, 9.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 1, 4, 8, 12.90);

----------------------------------------------------------------------------------------

-- EKONOMICZNE KRAJOWE (LISTY ZWYKLE)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 2, 1, 6, 3.75);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 2, 1, 7, 4.75);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 2, 1, 8, 7.30);

-- EKONOMICZNE KRAJOWE (LISTY POLECONE)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 2, 3, 6, 7.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 2, 3, 7, 8.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 2, 3, 8, 9.50);

-- EKONOMICZNE KRAJOWE (LISTY POLECONE ZPO)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 2, 3, 6, 9.40);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 2, 3, 7, 10.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 2, 3, 8, 11.40);

--------------------------------------------------------------------------------------

-- PRIORYTETOWE KRAJOWE (LISTY ZWYKLE)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 2, 2, 6, 5.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 2, 2, 7, 7.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 2, 2, 8, 10.90);

-- PRIORYTETOWE KRAJOWE (LISTY POLECONE)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 2, 4, 6, 8.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 2, 4, 7, 11.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 2, 4, 8, 14.50);

-- PRIORYTETOWE KRAJOWE (LISTY POLECONE ZPO)-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 2, 4, 6, 10.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 2, 4, 7, 12.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 2, 4, 8, 16.40);

------------------------------------------------------------------------------------


-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)-- STREFA A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 3, 7, 1, 5.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 3, 7, 2, 11.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 3, 7, 3, 13.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 3, 7, 4, 15.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 3, 7, 5, 29.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 3, 7, 8, 58.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 3, 7, 9, 75.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 3, 7, 10, 92.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 3, 7, 11, 109.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 3, 7, 12, 126.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 3, 7, 13, 143.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 3, 7, 14, 160.90);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)-- STREFA A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 3, 9, 1, 10.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 3, 9, 2, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 3, 9, 3, 18.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 3, 9, 4, 20.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 3, 9, 5, 34.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 3, 9, 8, 64.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 3, 9, 9, 81.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 3, 9, 10, 98.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 3, 9, 11, 115.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 3, 9, 12, 132.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 3, 9, 13, 149.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 3, 9, 14, 166.20);

-------------------------------------------------------------------------------------

-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)---- STREFA B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 4, 7, 1, 5.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 4, 7, 2, 11.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 4, 7, 3, 13.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 4, 7, 4, 19.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 4, 7, 5, 38.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 4, 7, 8, 77.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 4, 7, 9, 99.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 4, 7, 10, 121.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 4, 7, 11, 143.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 4, 7, 12, 165.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 4, 7, 13, 187.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 4, 7, 14, 209.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)---- STREFA B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 4, 9, 1, 10.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 4, 9, 2, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 4, 9, 3, 18.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 4, 9, 4, 24.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 4, 9, 5, 43.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 4, 9, 8, 82.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 4, 9, 9, 104.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 4, 9, 10, 126.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 4, 9, 11, 148.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 4, 9, 12, 170.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 4, 9, 13, 192.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 4, 9, 14, 214.30);

--------------------------------------------------------------------------------------

-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)---- STREFA C --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 5, 7, 1, 5.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 5, 7, 2, 11.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 5, 7, 3, 13.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 5, 7, 4, 24.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 5, 7, 5, 45.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 5, 7, 8, 95.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 5, 7, 9, 122.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 5, 7, 10, 149.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 5, 7, 11, 176.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 5, 7, 12, 203.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 5, 7, 13, 230.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 5, 7, 14, 257.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)--- STREFA C --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 5, 9, 1, 16.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 5, 9, 2, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 5, 9, 3, 18.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 5, 9, 4, 29.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 5, 9, 5, 50.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 5, 9, 8, 100.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 5, 9, 9, 127.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 5, 9, 10, 154.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 5, 9, 11, 181.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 5, 9, 12, 208.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 5, 9, 13, 235.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 5, 9, 14, 262.30);

----------------------------------------------------------------------------------------

-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)--- STREFA D -- 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 6, 7, 1, 5.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 6, 7, 2, 11.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 6, 7, 3, 13.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 6, 7, 4, 34.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 6, 7, 5, 68.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 6, 7, 8, 147.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 6, 7, 9, 189.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 6, 7, 10, 231.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 6, 7, 11, 273.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 6, 7, 12, 315.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 6, 7, 13, 357.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 6, 7, 14, 399.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)--- STREFA D -- 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 6, 9, 1, 16.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 6, 9, 2, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 6, 9, 3, 18.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 6, 9, 4, 39.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 6, 9, 5, 73.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 6, 9, 8, 152.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   0, 5, 9, 9, 194.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   0, 5, 9, 10, 236.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   0, 5, 9, 11, 278.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   0, 5, 9, 12, 320.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   0, 5, 9, 13, 362.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   0, 5, 9, 14, 404.30);

------------------------------------------------------------------------------------


-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)-- STREFA A -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 3, 7, 1, 8.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 3, 7, 2, 14.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 3, 7, 3, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 3, 7, 4, 18.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 3, 7, 5, 32.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 3, 7, 8, 61.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 3, 7, 9, 78.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 3, 7, 10, 95.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 3, 7, 11, 112.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 3, 7, 12, 129.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 3, 7, 13, 146.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 3, 7, 14, 163.90);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)-- STREFA A -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 3, 9, 1, 13.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 3, 9, 2, 20.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 3, 9, 3, 21.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 3, 9, 4, 23.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 3, 9, 5, 37.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 3, 9, 8, 67.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 3, 9, 9, 84.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 3, 9, 10, 101.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 3, 9, 11, 118.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 3, 9, 12, 135.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 3, 9, 13, 152.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 3, 9, 14, 169.20);

-------------------------------------------------------------------------------------

-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)---- STREFA B -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 4, 7, 1, 8.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 4, 7, 2, 14.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 4, 7, 3, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 4, 7, 4, 22.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 4, 7, 5, 41.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 4, 7, 8, 80.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 4, 7, 9, 102.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 4, 7, 10, 124.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 4, 7, 11, 147.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 4, 7, 12, 168.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 4, 7, 13, 190.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 4, 7, 14, 212.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)---- STREFA B -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 4, 9, 1, 13.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 4, 9, 2, 20.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 4, 9, 3, 21.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 4, 9, 4, 27.60);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 4, 9, 5, 46.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 4, 9, 8, 85.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 4, 9, 9, 107.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 4, 9, 10, 129.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 4, 9, 11, 151.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 4, 9, 12, 173.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 4, 9, 13, 195.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 4, 9, 14, 217.30);

--------------------------------------------------------------------------------------

-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)---- STREFA C -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 5, 7, 1, 8.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 5, 7, 2, 14.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 5, 7, 3, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 5, 7, 4, 27.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 5, 7, 5, 48.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 5, 7, 8, 98.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 5, 7, 9, 125.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 5, 7, 10, 152.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 5, 7, 11, 179.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 5, 7, 12, 206.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 5, 7, 13, 233.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 5, 7, 14, 260.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)--- STREFA C -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 5, 9, 1, 19.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 5, 9, 2, 20.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 5, 9, 3, 21.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 5, 9, 4, 32.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 5, 9, 5, 53.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 5, 9, 8, 103.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 5, 9, 9, 130.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 5, 9, 10, 157.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 5, 9, 11, 184.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 5, 9, 12, 211.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 5, 9, 13, 238.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 5, 9, 14, 265.30);

----------------------------------------------------------------------------------------



-- PRIORYTETOWE ZAGRANICZNE (LISTY ZWYKLE)--- STREFA D -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 6, 7, 1, 8.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 6, 7, 2, 14.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 6, 7, 3, 17.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 6, 7, 4, 37.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 6, 7, 5, 71.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 6, 7, 8, 150.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 6, 7, 9, 192.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 6, 7, 10, 234.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 6, 7, 11, 276.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 6, 7, 12, 318.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 6, 7, 13, 360.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 6, 7, 14, 402.00);

-- PRIORYTETOWE ZAGRANICZNE (LISTY POLECONE)--- STREFA D -- ZPO
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     1, 6, 9, 1, 19.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    1, 6, 9, 2, 20.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    1, 6, 9, 3, 21.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    1, 6, 9, 4, 42.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   1, 6, 9, 5, 76.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   1, 6, 9, 8, 155.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2500 g',   1, 6, 9, 9, 197.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3000 g',   1, 6, 9, 10, 239.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 3500 g',   1, 6, 9, 11, 281.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4000 g',   1, 6, 9, 12, 323.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 4500 g',   1, 6, 9, 13, 365.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 5000 g',   1, 6, 9, 14, 407.30);

------------------------------------------------------------------------------------

-- ZWYKLE EKONOMICZNE ZAGRANICZNE --
-- EUROPEJSKIE --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 7, 6, 1, 5.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 7, 6, 2, 9.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 7, 6, 3, 10.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 7, 6, 4, 11.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 7, 6, 5, 21.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 7, 6, 8, 40.90);

-- POZAEUROPEJSKIE --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 8, 6, 1, 5.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 8, 6, 2, 9.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 8, 6, 3, 10.00);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 8, 6, 4, 11.50);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 8, 6, 5, 21.80);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 8, 6, 8, 41.40);

-- PRZESY�KI REKLAMOWE --
-- GABARYT A --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 1, 3, 1, 1.30);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 1, 3, 2, 1.40);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 1, 3, 3, 1.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 1, 3, 4, 2.10);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 1, 3, 5, 3.20);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 1, 3, 6, 4.50);

-- GABARYT B --
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 50 g',     0, 2, 3, 1, 1.55);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 100 g',    0, 2, 3, 2, 1.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 350 g',    0, 2, 3, 3, 1.90);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 500 g',    0, 2, 3, 4, 2.70);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 1000 g',   0, 2, 3, 5, 5.40);
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'Do 2000 g',   0, 2, 3, 6, 7.00);

-- KARTKA POCZTOWA EKONOMICZNA -- 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'brak',    0, 0, 14, 0, 1.60);
-- KARTKA POCZTOWA PRIORYTET -- 
insert into DS_STAWKI_OPLAT_POCZTOWYCH values ( 'brak',    0, 0, 15, 0, 2.35);