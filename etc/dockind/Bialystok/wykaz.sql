
-- polaczenie wykazu ze sprawami + pola dodatkowe dla spraw
CREATE TABLE [ds_WYKAZ_CASES](
	[ID] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
	
	[CASE_ID] [numeric](19, 0) NOT NULL,
	[C_WYKAZ_NUMBER] [int] NOT NULL,
	-- liczba teczek
	-- miejsce
	-- data zniszczenia
	[LP] [int] NOT NULL,
	[ZNAK] varchar(255) NOT NULL,
	[TYTUL] varchar(255) NOT NULL,
	[DATA_OD] varchar(50) NOT NULL,
	[DATA_DO] varchar(50) NOT NULL,
	[KAT_AKT] varchar(255) NOT NULL

) ON [PRIMARY]
GO


-- wykaz jako dokument (dockind)
CREATE TABLE [ds_WYKAZ_DOC](
	[DOCUMENT_ID] [numeric](19, 0) NOT NULL,
	[WYKAZ_NUMBER] [int] NOT NULL
) ON [PRIMARY]
GO


CREATE TABLE DS_WYKAZ_MULTIPLE (
    DOCUMENT_ID numeric(19, 0) NOT NULL,
    FIELD_CN varchar(20 ) NOT NULL,
    FIELD_VAL varchar(100) NULL
);