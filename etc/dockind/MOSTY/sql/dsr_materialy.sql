CREATE TABLE dsr_materialy(
	ID numeric(18,0) identity(1,1) NOT NULL,
	PRZEDMIOT_ZAMOWIENIA varchar (200),
	JEDNOSTKA_MIARY numeric (18,0),
	ILOSC numeric (18,0),
	TERMIN_REALIZACJI date,
	ILOSC_W_MG numeric (18, 0),
	DATA_WYDANIA date
);
