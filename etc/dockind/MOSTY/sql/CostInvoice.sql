IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'dsg_mosty_costinvoice') DROP TABLE dsg_mosty_costinvoice;
create table dsg_mosty_costinvoice(
	document_id numeric(18, 0),
	status numeric(18, 0),
	rodzaj_faktury numeric(18,0),
	invoice_number varchar(50),
	data_wystawienia date,
	data_wplywu date,
	data_platnosci date,
	netto float,
	vat float,
	brutto float,
);

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'dsg_mosty_costinvoice_multiple_value') DROP TABLE dsg_mosty_costinvoice_multiple_value;
create table dsg_mosty_costinvoice_multiple_value(
	id numeric(18, 0),
	document_id numeric(18, 0),
	field_cn varchar(100),
	field_val varchar(100)
);

IF EXISTS(SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'dsg_mosty_mpk_user') DROP TABLE dsg_mosty_mpk_user;
CREATE TABLE [dbo].[dsg_pcz_mpk_financial_section](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] [int] NULL,
	[refValue] [varchar](20) NULL,
	[available] [bit] NULL
);

alter table dsg_mosty_costinvoice add TYP_DOKUMENTU numeric(18,0);
alter table dsg_mosty_costinvoice add BUDOWA numeric(18,0);
alter table dsg_mosty_costinvoice add ETAP numeric(18,0);
alter table dsg_mosty_costinvoice add KATALOG numeric(18,0);
alter table dsg_mosty_costinvoice add STATUS_REALIZACJI numeric(18,0);
alter table dsg_mosty_costinvoice add TERMIN_REALIZACJI date;
alter table dsg_mosty_costinvoice add pz_number varchar(45);
alter table dsg_mosty_costinvoice add user_mpk numeric(19, 0);
alter table dsg_mosty_costinvoice add lokalizacja_1 numeric(19, 0);
alter table dsg_mosty_costinvoice add rodzaj_IN numeric(19, 0);
alter table dsg_mosty_costinvoice add skan_faktury numeric(19, 0);
alter table dsg_mosty_costinvoice add status_budowy numeric(19, 0);
alter table dso_person alter column organization varchar(1024);


insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('1','501 Koszty produkcji podstawowej',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('2','530-1 Koszty us�ug sprz�towych',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('3','530-2 Koszty us�ug transportowych',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('4','531-1 Warsztat',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('5','531-2 Wytw�rnia Kontrukcji Stalowej',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('6','531-3 Produkcja rolna',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('7','531-4 Wytw�rnia prefabrykat�w',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('8','531-5 Elektrycy',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('9','531-6 Pracownia projektowa',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('10','535 Koszty wydzia�owe us�ug produk',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('11','550-1 Koszty og�lnozak�adowe',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('12','550-2 Koszty Dzia�u Finansowego',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('13','550-3 Koszty Dzia�u Kadr i P�ac',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('14','550-4 Koszty Techn. Projekt.',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('15','550-5 Koszty Dzia�u przygotowa� Produkcji',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('16','550-6 Koszty Dzia�u Rozwoju Biznesu',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('17','550-7 Koszty Dzia�u Logistyki',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('18','550-8 Koszty Dzia�u Ofertowania',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('19','550-9 Koszty Dzia�u Geodezji',0,0,1);
insert into dsg_mosty_kod_mpk(cn,title,refValue,available) values ('20','550-10 Koszty Dzia�u Laboratorium',0,0,1);

create view dsg_mosty_kierownicy_budow as
select 
us.id as id, 
us.id as cn, 
us.lastname+' ' + us.firstname as title, 
0 as centrum, 
0 as refValue, 
1 as available, 
div.division_id , 
di.guid from ds_user us 
join ds_user_to_division div on div.user_id=us.id 
join ds_division di on di.id=div.division_id 
where di.guid='kierownicy_budow';

CREATE view dsg_mosty_dzialy as
select div.id as id, div.id as cn, div.name as title, 0 as centrum, 0 as refValue, 1 as available from ds_division div where divisiontype='division' or divisiontype='position'
union 
select -5 as id , '-5' as cn, 'Nie dotyczy' as title, 0 as centrum, 0 as refValue, 1 as available;