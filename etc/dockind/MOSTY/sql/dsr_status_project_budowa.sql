alter table dsg_mosty_sprawa add status_projekt_budowa numeric (18,0);
alter table dsg_mosty_costinvoice add status_projekt_budowa numeric (18,0);
alter table dsg_mosty_zamowienia add status_projekt_budowa numeric (18,0);
alter table DSG_NORMAL_DOCKIND add status_projekt_budowa numeric (18,0);
-- 

CREATE TABLE dsr_status_projekt_budowa(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] bit default 1 not null
);

-- zakonczone z gwarancja
-- w trakcie realizacji
-- archiwum

insert into dsr_projekt_budowa(id, cn, title, centrum, refValue, available) values(1,'REALIZACJA', 'Realizowane', 1, NULL,1);
insert into dsr_projekt_budowa(id, cn, title, centrum, refValue, available) values(2,'GWARANCJA', 'Zakończone z gwarancją', 1, NULL,1);
insert into dsr_projekt_budowa(id, cn, title, centrum, refValue, available) values(3,'ARCHIWUM', 'Archiwum', 1, NULL,1);