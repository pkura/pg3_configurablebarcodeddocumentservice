alter table dsg_mosty_zamowienia add TYP_DOKUMENTU numeric(18,0);
alter table dsg_mosty_zamowienia add LAB smallint;
alter table dsg_mosty_zamowienia add DP smallint;

CREATE TABLE dsg_mosty_zamowienia
(
	DOCUMENT_ID numeric(18,0),	
	STATUS numeric(18,0),
	BUDOWA numeric(18,0),
	ETAP numeric (18,0),
	KATALOG numeric (18,0),
	TERMIN_DOSTAWY date,
	TERMIN_REALIZACJI date,
	PRZEZNACZENIE varchar (350),
	UWAGI varchar (350),
	AKCEPTACJA_INWESTORA smallint,
	MIEJSCE_DOSTAWY varchar (350),
	DPP smallint,
	WKS smallint,
	DOSTARCZONE smallint,
	WYBOR_DOSTAWCY smallint,
	PREZES smallint,
	STATUS_REALIZACJI numeric (18,0)
);

CREATE TABLE dsg_mosty_zamowienia_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);