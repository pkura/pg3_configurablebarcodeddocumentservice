alter table DSG_NORMAL_DOCKIND add rodzaj_in integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_out integer;
alter table DSG_NORMAL_DOCKIND add rodzaj_int integer;
alter table DSG_NORMAL_DOCKIND add TYP_DOKUMENTU numeric(18,0);
alter table DSG_NORMAL_DOCKIND add BUDOWA numeric (18,0);
alter table DSG_NORMAL_DOCKIND add ETAP numeric (18,0);
alter table DSG_NORMAL_DOCKIND add KATALOG numeric (18,0);
alter table DSG_NORMAL_DOCKIND add STATUS_REALIZACJI numeric (18,0);
alter table DSG_NORMAL_DOCKIND add TYP_INT numeric (18,0);
alter table DSG_NORMAL_DOCKIND add sygnatura varchar(50);
alter table DSG_NORMAL_DOCKIND add termin_realizacji date;
alter table DSG_NORMAL_DOCKIND add skan_dokumentu numeric(19, 0);
alter table dsg_normal_dockind add ZALACZNIK numeric(19, 0);

CREATE TABLE [dsg_rodzaj_in](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
	
);

CREATE TABLE [dsg_rodzaj_out](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

CREATE TABLE [dsg_rodzaj_INT](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL
	,available integer
);

  insert into dso_in_document_delivery(posn, name) values(4, 'FAX');
  insert into dso_in_document_delivery(posn, name) values(5, 'E-mail');

  insert into dso_out_document_delivery(posn, name) values(3, 'FAX');
  insert into dso_out_document_delivery(posn, name) values(4, 'E-mail');


insert into dsg_rodzaj_in(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_in(cn,title,available)values('ZAMOWIENIA','Zamówienia usług',1);
insert into dsg_rodzaj_in(cn,title,available)values('ODPOWIEDZ','Odpowiedzi na zapytania ofertowe',1);
insert into dsg_rodzaj_in(cn,title,available)values('Z_JEDNOSTEK','Pisma z jednostek nadrzędnych (PAN, MNiSW)',1);
insert into dsg_rodzaj_in(cn,title,available)values('TAJNE','Pisma tajne',1);
insert into dsg_rodzaj_in(cn,title,available)values('INNE','Pozostałe pisma',1);

insert into dsg_rodzaj_out(cn,title,available)values('FAKTURA','Faktury handlowe, korekty faktur, noty',1);
insert into dsg_rodzaj_out(cn,title,available)values('DO_JEDNOSTEK','Pisma do jednostek nadrzędnych (PAN, MNiSW',1);
insert into dsg_rodzaj_out(cn,title,available)values('DO_PARTNEROW','Pisma do Partnerów  projektów',1);
insert into dsg_rodzaj_out(cn,title,available)values('MONITY','Pisma, monity  do kontrahentów',1);
insert into dsg_rodzaj_out(cn,title,available)values('INNE','Pozostała  korespondencja ',1);

insert into dsg_rodzaj_int(cn,title,available)values('NOTATKA','Notatka służbowa',1);
insert into dsg_rodzaj_int(cn,title,available)values('PODANIA','Podania',1);

ALTER TABLE DSG_NORMAL_DOCKIND ADD do_prezesa bit;