CREATE TABLE dsr_projekt_budowa(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[cn] [varchar](50) NULL,
	[title] [varchar](255) NULL,
	[centrum] int NULL,
	[refValue] varchar(20) NULL,
	[available] bit default 1 not null
);






insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('NIEPORET', 'Niepor�t', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('TUNEL_SREDNICOWY', 'Tunel �rednicowy', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('SOCHACZEW', 'Sochaczew Koz��w Szlachecki', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('PIASECZNO_JESOWKA', 'Piaseczno Jes�wka � �abieniec', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('RADZYMIN', 'Radzymin', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ZIELONKA', 'Zielonka', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('MURY_OPOROWE', 'Mury oporowe GUS', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('BARIERY_CYTADELA', 'Bariery � Cytadela wyb. Gdy�skie', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('NOWA_SUCHA', 'Nowa Sucha (Nida)', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('UTRZYMANIE_S8', 'Utrzymanie S8', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('TOPOLOWA_KOPIJNIKOW', 'Topolowa, Kopijnik�w � przepusty', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('EKRANY_GROJEC', 'Ekrany obwodnica Gr�jec', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('KLADKA_FOSA', 'K�adka Fosa', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('BARIERY_LAZIENKOWSKA', 'Bariery Trasy �azienkowej � Paryska', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('KLADKI_TARGOWEK', 'K�adki targ�wek', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('MLAWA_2', 'M�awa II', 1, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ROBOTY_UTRZYMANIOWE', 'Roboty utrzymaniowe 2013', 1, NULL,1);

insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ELEKTROWNIA_WIATROWA', 'Elektrownia Wiatrowa EOL Sp. z o.o.', 0, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ELEKTROWNIA_WIATROWA_BIS', 'Elektrownia Wiatrowa EOL Bis Sp. z o.o.', 0, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ELEKTROWNIA_WIATROWA_A', 'Bariery Trasy �azienkowej � Paryska', 0, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ELEKTROWNIA_WIATROWA_B', 'K�adki targ�wek', 0, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('EKO_GOSPODARSTWO', 'M�awa II', 0, NULL,1);
insert into dsr_projekt_budowa(cn, title, centrum, refValue, available) values('ELEKTROWNIA_WIATROWA_ZONDA', 'Roboty utrzymaniowe 2013', 0, NULL,1);


1. Elektrownia Wiatrowa EOL Sp. z o.o.
2. Elektrownia Wiatrowa EOL Bis Sp. z o.o.
3. Elektrownia Wiatrowa A 2MW Sp. z o.o.
4. Elektrownia Wiatrowa B 2MW Sp. z o.o.
5. Ekologiczne Gospodarstwo Rolne MOSTY Sp. z o.o.
6. Elektrownia Wiatrowa ZONDA Sp. z o.o.