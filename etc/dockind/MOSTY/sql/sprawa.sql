CREATE TABLE [dbo].[dsg_mosty_sprawa](
	[document_id] [numeric](19, 0) NOT NULL,
	[status] [int] NULL,
	[BUDOWA] [int] NULL,
	[ETAP] [int] NULL,
	[KATALOG] [int] NULL,
	[OPIS] [varchar](350) NULL,
	[STATUS_REALIZACJI] [int] NULL,
	[TYP_DOKUMENTU] [numeric](18, 0) NULL,
	[DOKUMENTY] [numeric](18, 0) NULL,
	[KOORDYNATOR] [numeric] (18,0) NULL
);

CREATE TABLE dsg_mosty_sprawa_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);



CREATE VIEW dsg_mosty_documents
as
SELECT doc.ID, dsg.DOCUMENT_ID, dsg.STATUS_BUDOWY, dsg.BUDOWA, dsg.ETAP, dsg.KATALOG, dsg.STATUS, dsg.TYP_DOKUMENTU
FROM dsg_mosty_zamowienia dsg join ds_document doc on dsg.DOCUMENT_ID = doc.ID
union
SELECT doc.ID, dsg.DOCUMENT_ID, dsg.STATUS_BUDOWY, dsg.BUDOWA, dsg.ETAP, dsg.KATALOG, dsg.STATUS, dsg.TYP_DOKUMENTU
FROM dsg_mosty_costinvoice dsg join ds_document doc on dsg.DOCUMENT_ID = doc.ID
union
SELECT doc.ID, dsg.DOCUMENT_ID, dsg.STATUS_BUDOWY, dsg.BUDOWA, dsg.ETAP, dsg.KATALOG, dsg.STATUS, dsg.TYP_DOKUMENTU
FROM dsg_mosty_salesinvoice dsg join ds_document doc on dsg.DOCUMENT_ID = doc.ID
union
SELECT doc.ID, dsg.DOCUMENT_ID, dsg.STATUS_BUDOWY, dsg.BUDOWA, dsg.ETAP, dsg.KATALOG, dsg.STATUS, dsg.TYP_DOKUMENTU
FROM DSG_NORMAL_DOCKIND dsg join ds_document doc on dsg.DOCUMENT_ID = doc.ID