
create table dsg_mosty_salesinvoice(
	document_id numeric(18, 0),
	status numeric(18, 0),
	TYP_DOKUMENTU numeric(18,0),
 	BUDOWA numeric(18,0),
 	ETAP numeric(18,0),
	KATALOG numeric(18,0),
	STATUS_REALIZACJI numeric(18,0),
	TERMIN_REALIZACJI date,
	OPIS varchar (350),
	brutto numeric (18,2),
	invoice_number varchar(50),
	NR_KONTA varchar (100)
);

CREATE TABLE dsg_mosty_salesinvoice_multiple_value (
	ID numeric(18,0) identity(1,1) NOT NULL,
    DOCUMENT_ID numeric(18, 0) NOT NULL,
    FIELD_CN varchar(100) NOT NULL,
    FIELD_VAL varchar(100) NULL
);

alter table dsg_mosty_salesinvoice add OBMIAR_ATTACHMENT numeric(18,0);