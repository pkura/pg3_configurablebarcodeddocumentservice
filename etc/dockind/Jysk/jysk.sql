CREATE TABLE DSG_JYSK (
	document_id		integer,
	kategoria		integer,
	id_pracownika	integer
);

CREATE TABLE DJ_PERSON_DICTIONARY(
	id			int identity(1,1) not null,
	nazwisko 	varchar(50),
	imie		varchar(50),
	pesel		varchar(20),
	nrsklepu	varchar(20),
	pracuje		char(1),
	idPracownika varchar(50)	
);
