CREATE TABLE DSR_REJESTR_SKARG (
	ENTRY_ID numeric(18,0) NOT NULL,
    PRZEDMIOT_SKARGI varchar(200),
	DATA_ZLECENIA timestamp,
	REALIZUJACY_SKARGE varchar(100),
    TERMIN_ZALATWIENIA timestamp,
    DATA_PO_ZALATW timestamp,
	SPOSOB_ZALATW varchar(50), 
    DATA_ZAWIADOMIENIA timestamp,
	ZAWIADOMIENI varchar(100),
    primary key(ENTRY_ID)
);

insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (gen_id(ds_division_id,1),'REJESTR_SKARG_READ', 'Rejestr skarg i wniosk�w - odczyt','group',1,0);
insert into ds_division (id,guid,name,divisiontype,parent_id,hidden) values (gen_id(ds_division_id,1),'REJESTR_SKARG_MODIFY', 'Rejestr skarg i wniosk�w - modyfikacja','group',1,0);
