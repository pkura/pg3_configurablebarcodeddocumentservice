// SdocCtl.cpp : Implementation of the CSdocCtrl ActiveX Control class.

#include "stdafx.h"
#include "sdoc.h"
#include "SdocCtl.h"
#include "SdocPpg.h"
#include "PinDlg.h"
#include "SSLCon.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define HTTPBUFLEN    512 // Size of HTTP Buffer...


IMPLEMENT_DYNCREATE(CSdocCtrl, COleControl)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CSdocCtrl, COleControl)
	//{{AFX_MSG_MAP(CSdocCtrl)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CSdocCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CSdocCtrl)
	DISP_FUNCTION(CSdocCtrl, "DownloadFile", DownloadFile, VT_BSTR, VTS_BSTR)
	DISP_FUNCTION(CSdocCtrl, "UploadFile", UploadFile, VT_EMPTY, VTS_BSTR VTS_BSTR VTS_BSTR)
	DISP_FUNCTION(CSdocCtrl, "DeleteLocalFile", DeleteLocalFile, VT_EMPTY, VTS_BSTR)
	//}}AFX_DISPATCH_MAP
	DISP_FUNCTION_ID(CSdocCtrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CSdocCtrl, COleControl)
	//{{AFX_EVENT_MAP(CSdocCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CSdocCtrl, 1)
	PROPPAGEID(CSdocPropPage::guid)
END_PROPPAGEIDS(CSdocCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CSdocCtrl, "SDOC.SdocCtrl.1",
	0x38552364, 0x433, 0x4f7f, 0x91, 0xc, 0x79, 0x25, 0x95, 0x7e, 0xf3, 0xa1)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CSdocCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DSdoc =
		{ 0x4083139a, 0xacdb, 0x4427, { 0xba, 0x5d, 0x2, 0x12, 0xff, 0x2e, 0xe1, 0x13 } };
const IID BASED_CODE IID_DSdocEvents =
		{ 0x9a83e9eb, 0x92a4, 0x494b, { 0x92, 0x2d, 0x67, 0xc5, 0x48, 0xaa, 0xc2, 0x8f } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwSdocOleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CSdocCtrl, IDS_SDOC, _dwSdocOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::CSdocCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CSdocCtrl

BOOL CSdocCtrl::CSdocCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_SDOC,
			IDB_SDOC,
			afxRegApartmentThreading,
			_dwSdocOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::CSdocCtrl - Constructor

CSdocCtrl::CSdocCtrl()
{
	InitializeIIDs(&IID_DSdoc, &IID_DSdocEvents);

	// TODO: Initialize your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::~CSdocCtrl - Destructor

CSdocCtrl::~CSdocCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::OnDraw - Drawing function

void CSdocCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	// TODO: Replace the following code with your own drawing code.
    COLORREF bckgrnd = TranslateColor(AmbientBackColor());
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(bckgrnd)));
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::DoPropExchange - Persistence support

void CSdocCtrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Call PX_ functions for each persistent custom property.
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::OnResetState - Reset control to default state

void CSdocCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl::AboutBox - Display an "About" box to the user

void CSdocCtrl::AboutBox()
{
	CDialog dlgAbout(IDD_ABOUTBOX_SDOC);
	dlgAbout.DoModal();
}


/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl message handlers


BSTR CSdocCtrl::DownloadFile(LPCTSTR A_inputURL) 
{
	CString strResultFName;
	// TODO: Add your dispatch handler code here

    char httpbuff[HTTPBUFLEN];
    CString strFileName;
    CInternetSession session;
    CStdioFile *remotefile=NULL;
    CString strExt;
    char winDir[MAX_PATH];
    GetTempPath(MAX_PATH, winDir);
    CString strURL(A_inputURL);

    CTime curtime = CTime::GetCurrentTime();
    int idx = strURL.ReverseFind('.');
    if( idx != -1)
    {
        strExt += strURL.Right(strURL.GetLength() - idx);
    }    
    strResultFName.Format("%s\\%d%d%d%d%d%d%s", winDir, curtime.GetYear(), curtime.GetMonth(), 
                                      curtime.GetDay(), curtime.GetHour(), 
                                      curtime.GetMinute(), curtime.GetSecond(), strExt );
    try
    {        
        remotefile = session.OpenURL(A_inputURL,1,INTERNET_FLAG_TRANSFER_BINARY|INTERNET_FLAG_RELOAD);
        CFile file(strResultFName, CFile::modeCreate|CFile::modeWrite|CFile::typeBinary);
        int numbytes;
        while (numbytes = remotefile->Read(httpbuff, HTTPBUFLEN))
        {
            file.Write(httpbuff, numbytes);
        }
        file.Close();
    }
    catch(CInternetException*)
    {          
    }

    if(remotefile != NULL)
    {
        remotefile->Close();
        delete remotefile;
    }
    session.Close();

	return strResultFName.AllocSysString();
}

void CSdocCtrl::UploadFile(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName) 
{
	// TODO: Add your dispatch handler code here
    CString strServer, strObject, strFileName;
    INTERNET_PORT nPort;
    DWORD dwServiceType;
    
    AfxParseURL(A_pathServlet, dwServiceType, strServer, strObject, nPort);
    if(dwServiceType == AFX_INET_SERVICE_HTTP)
    {
        UploadHTTP(A_pathFile, A_pathServlet, A_outFileName);
    }
    else if(dwServiceType == AFX_INET_SERVICE_HTTPS)
    {
        UploadHTTPS(A_pathFile, A_pathServlet, A_outFileName);
    }
}

void CSdocCtrl::DeleteLocalFile(LPCTSTR A_pathFile) 
{
	// TODO: Add your dispatch handler code here
    CFile::Remove( A_pathFile );
}

////////////////////////////////
//     private functions
////////////////////////////////

void CSdocCtrl::UploadHTTPS(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName) 
{
    char httpbuff[HTTPBUFLEN];
    CString strServer, strObject, strFileName;
    CString strData1, strData2, strData3;
    CString szFormData;
    INTERNET_PORT nPort;
    DWORD dwServiceType;
    

    AfxParseURL(A_pathServlet, dwServiceType, strServer, strObject, nPort);
    strFileName = GetFileName(A_outFileName);    
    
    strData1 = _T("Content-Disposition: form-data; name=\"targetFilename\"");
    strData2 = _T("Content-Disposition: form-data; name=\"doUploadToFile\"");
    strData3.Format(_T("Content-Disposition: form-data; name=\"file\"; filename=\"%s\""), strFileName);

    szFormData = "--913114112\r\n";
    szFormData += strData1;
    szFormData += "\r\n\r\n";
    szFormData += A_outFileName;
    szFormData += "\r\n";

    szFormData += "--913114112\r\n";
    szFormData += strData2;
    szFormData += "\r\n\r\n";
    szFormData += "true";
    szFormData += "\r\n";

    szFormData += "--913114112\r\n";
    szFormData += strData3;
    szFormData += "\r\n\r\n";

    CFile file(A_pathFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone);
    int numbytes;
    while ( numbytes = file.Read(httpbuff, HTTPBUFLEN) )
        szFormData.Append(httpbuff, numbytes);

    szFormData += "\r\n";
    szFormData += "--913114112--";
    szFormData += "\r\n";


    CSslConnection inetSec;
    string sServerName((LPCTSTR)strServer);
    string sObjectName((LPCTSTR)strObject);
    string strVerb = "POST";

    
    inetSec.SetServerName(sServerName);
    inetSec.SetPort(nPort);    
    inetSec.SetObjectName(sObjectName);
    inetSec.SetRequestID(0);

    string strHeaderPost = "Content-Type: multipart/form-data; boundary=913114112";    
    string strPostData = (LPCTSTR)szFormData;

    if (!inetSec.ConnectToHttpsServer(strVerb))
    {
		MessageBox("Can't connect to the server");
        return;
    } 

    if (!inetSec.SendHttpsRequest(strHeaderPost, strPostData)) 
    {
        CString strMess = "The data hasn't been sent to the server.\n";
        strMess += inetSec.GetLastErrorString().c_str();
        MessageBox(strMess);
        return;
	}
    
    //string response = inetSec.GetRequestResult();
    //MessageBox(response.c_str());
}


void CSdocCtrl::UploadHTTP(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName) 
{
    char httpbuff[HTTPBUFLEN];
    CString szFormData;
    CInternetSession session;
    CHttpConnection* pHttpConnection = NULL;
    CString strHeader1 = _T("Content-Type: multipart/form-data; boundary=913114112");
    CString strData1, strData2, strData3;
    CString strServer, strObject, strFileName;
    INTERNET_PORT nPort;
    DWORD dwServiceType;


    AfxParseURL(A_pathServlet, dwServiceType, strServer, strObject, nPort);
    strFileName = GetFileName(A_outFileName);

    strData1 = _T("Content-Disposition: form-data; name=\"targetFilename\"");
    strData2 = _T("Content-Disposition: form-data; name=\"doUploadToFile\"");
    strData3.Format(_T("Content-Disposition: form-data; name=\"file\"; filename=\"%s\""), strFileName);

    szFormData = "--913114112\r\n";
    szFormData += strData1;
    szFormData += "\r\n\r\n";
    szFormData += A_outFileName;
    szFormData += "\r\n";

    szFormData += "--913114112\r\n";
    szFormData += strData2;
    szFormData += "\r\n\r\n";
    szFormData += "true";
    szFormData += "\r\n";

    szFormData += "--913114112\r\n";
    szFormData += strData3;
    szFormData += "\r\n\r\n";

    CFile file(A_pathFile, CFile::modeRead|CFile::typeBinary|CFile::shareDenyNone);
    int numbytes;
    while ( numbytes = file.Read(httpbuff, HTTPBUFLEN) )
        szFormData.Append(httpbuff, numbytes);

    szFormData += "\r\n";
    szFormData += "--913114112--";
    szFormData += "\r\n";

    pHttpConnection = session.GetHttpConnection( strServer, nPort );
    CHttpFile *pHttpFile = pHttpConnection->OpenRequest(CHttpConnection::HTTP_VERB_POST, strObject);
    BOOL ret = pHttpFile->SendRequest(strHeader1, (LPVOID)(LPCSTR)szFormData, szFormData.GetLength());
    if(ret == FALSE)
    {
        ;
    }
    
    CString msg;
    DWORD retCode;
    ret = pHttpFile->QueryInfoStatusCode(retCode);
    if(ret == FALSE)
    {
        msg.Format("Error - nb: %d", retCode);
        MessageBox(msg);
    }
    else if( HTTP_STATUS_OK != retCode )
    {
        msg.Format("Error - nb: %d", retCode);
        MessageBox(msg);
    }

	char buf[2];
	int bytesRead;
	CString resultString = "";
	
	while( (bytesRead = pHttpFile->Read(buf, 1)) > 0 )
	{
		resultString += buf[0];
	}
    //MessageBox(resultString); 

    if(pHttpFile != NULL)
    {
        pHttpFile->Close();
        delete pHttpFile;
    }

    if(pHttpConnection != NULL)
    {
        pHttpConnection->Close();
        delete pHttpConnection;
    }

    session.Close();
}

CString CSdocCtrl::GetFileName(LPCTSTR A_pathFile)
{
    CString fileName(A_pathFile);

    int idx = fileName.ReverseFind('/');
    fileName.Delete(0, idx+1);

    return fileName;
}
