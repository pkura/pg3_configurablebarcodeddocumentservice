// PinDlg.cpp : implementation file
//

#include "stdafx.h"
#include "sdoc.h"
#include "PinDlg.h"

// Uzycie dialogu
/*
    CPinDlg dlg;
    dlg.SetDocFile(A_pathFile); // �cie�ka do pliku
    if(dlg.DoModal() == IDOK)
    {
        dlg.m_strPin; // zawiera wpisany przez uzytkownika pin
    }
*/

// CPinDlg dialog

IMPLEMENT_DYNAMIC(CPinDlg, CDialog)
CPinDlg::CPinDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPinDlg::IDD, pParent)
{
    m_strPin = _T("");
}

CPinDlg::~CPinDlg()
{
}

void CPinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_PIN, m_strPin);
}


BEGIN_MESSAGE_MAP(CPinDlg, CDialog)
    ON_BN_CLICKED(IDC_VIEW_DOC, OnBnClickedViewDoc)
END_MESSAGE_MAP()


// CPinDlg message handlers

BOOL CPinDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
    
    GetDlgItem( IDC_PIN )->SetFocus();
    
    return FALSE;
}

void CPinDlg::SetDocFile(LPCTSTR A_pathFile)
{
    m_strPathFile = A_pathFile;
}

void CPinDlg::OnBnClickedViewDoc()
{
    // TODO: Add your control notification handler code here
    ShellExecute(NULL, "open", m_strPathFile, NULL, NULL, SW_SHOW);
}
