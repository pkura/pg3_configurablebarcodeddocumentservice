#pragma once


// CPinDlg dialog

class CPinDlg : public CDialog
{
	DECLARE_DYNAMIC(CPinDlg)

public:
	CPinDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPinDlg();

    void SetDocFile(LPCTSTR A_pathFile);

// Dialog Data
	enum { IDD = IDD_INPUT_PIN };
    CString	m_strPin; 

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

protected:
    CString m_strPathFile;

public:
    afx_msg void OnBnClickedViewDoc();
};
