#if !defined(AFX_SDOC_H__83D797CA_378E_4982_9D51_592D76374E9C__INCLUDED_)
#define AFX_SDOC_H__83D797CA_378E_4982_9D51_592D76374E9C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// sdoc.h : main header file for SDOC.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CSdocApp : See sdoc.cpp for implementation.

class CSdocApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDOC_H__83D797CA_378E_4982_9D51_592D76374E9C__INCLUDED)
