#if !defined(AFX_SDOCCTL_H__08DC6DF9_C7AF_4D2D_93B8_98B385409C73__INCLUDED_)
#define AFX_SDOCCTL_H__08DC6DF9_C7AF_4D2D_93B8_98B385409C73__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// SdocCtl.h : Declaration of the CSdocCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CSdocCtrl : See SdocCtl.cpp for implementation.

class CSdocCtrl : public COleControl
{
	DECLARE_DYNCREATE(CSdocCtrl)

// Constructor
public:
	CSdocCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSdocCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CSdocCtrl();

	DECLARE_OLECREATE_EX(CSdocCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CSdocCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CSdocCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CSdocCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CSdocCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CSdocCtrl)
	afx_msg BSTR DownloadFile(LPCTSTR A_inputURL);
	afx_msg void UploadFile(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName);    
	afx_msg void DeleteLocalFile(LPCTSTR A_pathFile);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CSdocCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

protected:
    // functions
    void UploadHTTP(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName);
    void UploadHTTPS(LPCTSTR A_pathFile, LPCTSTR A_pathServlet, LPCTSTR A_outFileName);
    CString GetFileName(LPCTSTR A_pathFile);

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CSdocCtrl)
	dispidDownloadFile = 1L,
	dispidUploadFile = 2L,
	dispidDeleteLocalFile = 3L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDOCCTL_H__08DC6DF9_C7AF_4D2D_93B8_98B385409C73__INCLUDED)
