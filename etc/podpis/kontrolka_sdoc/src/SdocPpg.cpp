// SdocPpg.cpp : Implementation of the CSdocPropPage property page class.

#include "stdafx.h"
#include "sdoc.h"
#include "SdocPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CSdocPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CSdocPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CSdocPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CSdocPropPage, "SDOC.SdocPropPage.1",
	0xcc8a4179, 0xa4b3, 0x4ef5, 0xb3, 0x53, 0x86, 0x75, 0xef, 0x7a, 0x25, 0xa3)


/////////////////////////////////////////////////////////////////////////////
// CSdocPropPage::CSdocPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CSdocPropPage

BOOL CSdocPropPage::CSdocPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_SDOC_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CSdocPropPage::CSdocPropPage - Constructor

CSdocPropPage::CSdocPropPage() :
	COlePropertyPage(IDD, IDS_SDOC_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CSdocPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CSdocPropPage::DoDataExchange - Moves data between page and properties

void CSdocPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CSdocPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CSdocPropPage message handlers
