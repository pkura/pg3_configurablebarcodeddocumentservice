#if !defined(AFX_SDOCPPG_H__61779560_B34E_46D7_AC88_B7384BCC29BE__INCLUDED_)
#define AFX_SDOCPPG_H__61779560_B34E_46D7_AC88_B7384BCC29BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// SdocPpg.h : Declaration of the CSdocPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CSdocPropPage : See SdocPpg.cpp.cpp for implementation.

class CSdocPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CSdocPropPage)
	DECLARE_OLECREATE_EX(CSdocPropPage)

// Constructor
public:
	CSdocPropPage();

// Dialog Data
	//{{AFX_DATA(CSdocPropPage)
	enum { IDD = IDD_PROPPAGE_SDOC };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CSdocPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SDOCPPG_H__61779560_B34E_46D7_AC88_B7384BCC29BE__INCLUDED)
