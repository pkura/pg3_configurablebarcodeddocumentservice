CREATE TABLE [ds_nr_to_barcode] (
	id numeric(19,0) identity(1,1) not null,
	[nrPaczki] [varchar](30) NOT NULL,
	[nrDokumentu] [varchar](30) NOT NULL,
	[dataWprowadzeniaNumeru] datetime,
	[idLokalizacji] [int],
	[uzytkownik1Numer] [varchar](20),
	[state] [smallint] NOT NULL,
	[barkod] [varchar](30),
	[dataPrzydzieleniaBarkodu] datetime,
	[uzytkownik2Barkod] [varchar](20),
	[first_in_package] [smallint] NOT NULL,
    primary key (id)
);