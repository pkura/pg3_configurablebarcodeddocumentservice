import sys, xreadlines, string, base64, jarray
from java.lang import String
from java.text import SimpleDateFormat

"""
Odczytuje plik LDIF zawieraj�cy nazwy u�ytkownik�w i dzia��w Docusafe,
a nast�pnie tworzy na jego podstawie plik SQL umieszczaj�cy u�ytkownik�w
i dzia�y w bazie Firebird.

Dodatkowe czynno�ci, jakie nale�y wykona� w celu migracji u�ytkownik�w
i dzia��w z LDAP do SQL:
- dodanie parametru "user.factory = sql" do pliku HOME/docusafe.config
- zmiana nazwy klasy JndiLoginModule na DSLoginModule w HOME/login.config
"""

class GenId:
    """Generator kolejnych identyfikator�w.
    """
    def __init__(self):
        self.nextid = 1
    def next(self):
        id = self.nextid
        self.nextid += 1
        return id

class LdifFile:
    def __init__(self, file):
        self.__readLdif(file)

    def __readLdif(self, file):
        ver = file.readline()
        if ver.find('version:') != 0:
            raise LdifFormatException, "Brak numeru wersji"
        self.version = int(ver[len('version:'):])
        if self.version != 1:
            raise LdifFormatException, "Nieznany numer wersji: " + `self.version`

        # lista obiekt�w LdifRecord
        self.ldifrecords = []
        record = []
        in_record = 0

        saved_line = ""
        for line in xreadlines.xreadlines(file):
            line = line.rstrip()
            if len(line) > 0 and line[0] == '#': continue

            # separator rekord�w
            if len(line) == 0:
                record.append(saved_line) 
                if len(record) > 0:
                    self.ldifrecords.append(LdifRecord(record))
                record = []
                in_record = 0
                saved_line = ""
                continue

            # kontynuacja poprzedniej linii
            if in_record > 0 and line[0] == ' ':
                saved_line = saved_line + line[1:]
                continue

            in_record = 1
            if len(saved_line) > 0:
                record.append(saved_line) 
            saved_line = line

        # je�eli po ostatnim rekordzie nie by�o pustej linii,
        # jest to wymagane, bo powy�sza p�tla nie do��czy go do kolekcji
        if len(record) > 0:
            self.ldifrecords.append(LdifRecord(record))

    def records(self):
        return self.ldifrecords
                
class LdifRecord:
    """Klasa reprezentuj�ca jeden rekord z pliku LDIF.
    """
    def __init__(self, lines):
        """Konstruktor przyjmuj�cy list� linii sk�adaj�cych si� na rekord.
        """
        self.values = {}
        for line in lines:
            attr, val = self.__parse(line)
            if self.values.has_key(attr):
                self.values[attr].append(val)
            else:
                self.values[attr] = [ val ]
            
    def __repr__(self):
        return repr(self.values)

    def attr_values(self, attr):
        """Lista wszystkich warto�ci atrybutu (mo�e by� pusta, je�eli atrybut nie istnieje).
        """
        if self.values.has_key(attr.lower()):
            return self.values[attr]
        else:
            return []

    def attr_value(self, attr):
        """Pierwsza warto�� atrybutu lub None, je�eli atrybut nie istnieje.
        """
        attrs = self.attr_values(attr)
        if len(attrs) > 0: return attrs[0]
        else: return None

    def __parse(self, line):
        """Zwraca krotk� sk�adaj�c� si� z nazwy atrybutu (ma�e litery) i jego warto�ci
        """
        cc = line.find('::')
        if cc > 0:
            attr = line[0:cc].lower()
            s = base64.decodestring(line[cc+3:])
            val = String(jarray.array([ord(c) for c in s], 'b'), 'utf-8').toString()
        else:
            c = line.find(':')
            if c > 0:
                attr = line[0:c].lower()
                val = line[c+2:]
            else:
                raise LdifFormatException, "W linii brakuje dwukropka: " + line

        return (attr, val)

class Exception:
    def __init__(self, msg):
        self.message = msg
    def __repr__(self):
        return self.message

class LdifFormatException(Exception): pass  

def varchar(s):
    if s is None: return 'NULL'
    else: return "'%s'" % s.replace('\'', '\\\'')

# data w formacie SQL
sqlDateSdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

def date(s):
    if s is None: return 'NULL'
    else: return "'%s'" % sqlDateSdf.format(s)

def numeric(i):
    if i is None: return 'NULL'
    else: return str(i)

class User:
    idgen = GenId()

    def __init__(self, ldifAttrs):
        self.id = User.idgen.next()
        self.name = ldifAttrs.attr_value('cn')
        self.firstname = ldifAttrs.attr_value('givenname')
        self.lastname = ldifAttrs.attr_value('sn')
        self.email = ldifAttrs.attr_value('mail')
        self.identifier = ldifAttrs.attr_value('docusafeuseridentifier')
        self.hashedPassword = ldifAttrs.attr_value('userpassword')
        self.deleted = ldifAttrs.attr_value('docusafedeleted') == 'true'
        self.loginDisabled = ldifAttrs.attr_value('docusafelogindisabled') == 'true'
        self.isAdmin = 'admin' in ldifAttrs.attr_values('docusaferole')
        self.divisionGuids = ldifAttrs.attr_values('docusafememberofdivision')

        sdf = SimpleDateFormat('dd-MM-yyyy HH:mm:ss')

        lsf = ldifAttrs.attr_value('docusafelastsuccessfullogin')
        if lsf is not None:
            self.lastSuccessfulLogin = sdf.parse(lsf)
        else:
            self.lastSuccessfulLogin = None
        
        luf = ldifAttrs.attr_value('docusafelastunsuccessfullogin')
        if luf is not None:
            self.lastUnsuccessfulLogin = sdf.parse(luf)
        else:
            self.lastUnsuccessfulLogin = None

        lpc = ldifAttrs.attr_value('docusafelastpasswordchange')
        if lpc is not None:
            self.lastPasswordChange = sdf.parse(lpc)
        else:
            self.lastPasswordChange = None

        st = ldifAttrs.attr_value('docusafesubstitutedthrough')
        if st is not None:
            self.substitutedThrough = sdf.parse(st)
        else:
            self.substitutedThrough = None

    def update_division(self, divisions):
        self.divisions = filter(lambda div, guids=self.divisionGuids: div.guid in guids, divisions)

    def __repr__(self):
        return self.name + self.firstname + ' ' + self.lastname

    def sql(self):
        insertUser = "INSERT INTO DS_USER (ID, NAME, " \
        "FIRSTNAME, LASTNAME, EMAIL, IDENTIFIER, LOGINDISABLED, " \
        "USERROLES, LASTSUCCESSFULLOGIN, LASTUNSUCCESSFULLOGIN, SUBSTITUTEDFROM, SUBSTITUTEDTHROUGH, " \
        "DELETED, LASTPASSWORDCHANGE, HASHEDPASSWORD) VALUES (%d, %s, " \
        "%s, %s, %s, %s, %d, " \
        "%s, %s, %s, %s, %s, " \
        "%d, %s, %s);"

        insertJoin = "INSERT INTO DS_USER_TO_DIVISION (USER_ID, DIVISION_ID) " \
            "VALUES (%d, %d);"

        if self.isAdmin: userRoles = 'admin'
        else: userRoles = None

        sql = []

        sql.append(insertUser % (self.id, varchar(self.name), \
            varchar(self.firstname), varchar(self.lastname), varchar(self.email), varchar(self.identifier), self.loginDisabled, \
            varchar(userRoles), date(self.lastSuccessfulLogin), date(self.lastUnsuccessfulLogin), varchar(None), date(self.substitutedThrough), \
            self.deleted, date(self.lastPasswordChange), varchar(self.hashedPassword)))

        for i in self.divisions:
            sql.append(insertJoin % (self.id, i.id))

        return string.join(sql, "\n")

class Division:
    idgen = GenId()

    def __init__(self, ldifAttrs):
        self.id = Division.idgen.next()
        self.guid = ldifAttrs.attr_value('docusafeguid')
        self.name = ldifAttrs.attr_value('docusafedivisionname')
        self.type = ldifAttrs.attr_value('docusafedivisiontype')
        self.code = ldifAttrs.attr_value('docusafedivisioncode')
        self.parentGuid = ldifAttrs.attr_value('docusafeparentguid')
        self.parent = None
        self.children = []
        self.uniq = 1

    def update_parent(self, divisions):
        if self.parentGuid is None: return

        # poszukiwanie dzia�u nadrz�dnego
        parents = filter(lambda div, pg=self.parentGuid: div.guid == pg, divisions)
        if len(parents) > 1:
            raise "Za wielu rodzicow"
        elif len(parents) == 1:
            self.parent = parents[0]
            self.parent.children.append(self)
            # je�eli w kolekcji parent.children s� co najmniej dwa elementy,
            # sprawdzanie duplikuj�cych si� nazw
            if len(self.parent.children) > 1:
                self.parent.children.sort(lambda x, y: x.name < y.name)
                for i in xrange(1, len(self.parent.children)):
                    if self.parent.children[i].name == self.parent.children[i-1].name:
                        self.parent.children[i].name = self.parent.children[i].name + (" (%d)" % self.uniq)
                        self.uniq += 1

    def sql(self):
        insert = "INSERT INTO DS_DIVISION (ID, GUID, NAME, CODE, DIVISIONTYPE, PARENT_ID) " \
        "VALUES (%d, %s, %s, %s, %s, %s);"

        if self.parent is not None:
            parent_id = self.parent.id
        else:
            parent_id = None

        return insert % (self.id, varchar(self.guid), varchar(self.name), varchar(self.code), \
            varchar(self.type), numeric(parent_id))

    def __repr__(self):
        if self.parent: pg = self.parent.guid
        else: pg = None
        return "guid=%s pg=%s" % (self.guid, pg)


if __name__ == '__main__':
    lf = LdifFile(open(sys.argv[1], 'r'))
    divId = GenId()
    userId = GenId()

    divisions = []
    users = []

    for rec in lf.records():
        if 'docusafedivision' in [x.lower() for x in rec.attr_values('objectclass')]:
            divisions.append(Division(rec))
        if 'docusafeperson' in [x.lower() for x in rec.attr_values('objectclass')]:
            users.append(User(rec))

    for div in divisions:
        div.update_parent(divisions)

    for div in divisions:
        print div.sql()

    print "SET GENERATOR DS_DIVISION_ID TO %d;" % (len(divisions)+1)

    for us in users:
        us.update_division(divisions)

    for us in users:
        print us.sql()
        
    print "SET GENERATOR DS_USER_ID TO %d;" % (len(users)+1)
