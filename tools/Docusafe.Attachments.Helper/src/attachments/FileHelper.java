/*
 * FileHelper.java
 *
 * Created on 16 maj 2007, 10:51
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package attachments;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class FileHelper extends Thread{
    
    public static boolean shouldStop = false;
    
    public final static SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    public final static SimpleDateFormat dayFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public forma form;
    
    FileHelper(forma arg){
        this.form = arg;
    }
    
    public void run()
    {        
        long id;
        BufferedInputStream is;
        Date date;
        File file;
        BufferedOutputStream os;
        byte[] buf;
        ResultSet rs;
        int size;
        PreparedStatement ps,ps2;
        
        
        try 
        {
            Connection con = form.connection;            
            con.setAutoCommit(true);
            //Zrobmy inaczej
            Statement stm = con.createStatement();
            rs = stm.executeQuery("select  id from ds_attachment_revision where (onDisc=0 or onDisc is null)");            
            while(rs.next()) 
            {
                if(shouldStop)
                {
                    form.getStartButton().setVisible(true);
                    form.getErrorLogger().setText("");
                    shouldStop = false;
                    form.stop = System.currentTimeMillis();
                    break;
                }
                id = rs.getLong(1);
                ResultSet rsBlob;
                ps = con.prepareStatement("select id,ctime, contentdata  from ds_attachment_revision where id = ?");                        
                ps.setLong(1,id);
                rsBlob = ps.executeQuery();
                if (rsBlob.next())
                {
                    date = rsBlob.getDate(2);
                    if (rsBlob.getBlob(3)!=null)
                    {
                        is = new BufferedInputStream(rsBlob.getBinaryStream(3));
                        os = new BufferedOutputStream(new FileOutputStream(getFile(id, date)));
                        buf = new byte[16384];               
                        while((size=is.read(buf))>0)
                        {  os.write(buf,0,size); }
                        os.flush();
                        is.close();
                        os.close();                
                //Connection conn = form.getSimpleConnection();                        
                        if(form.getCheckbox().isSelected())
                        { ps2 = con.prepareStatement("update ds_attachment_revision set onDisc=1, contentdata=null where id=?");}
                        else
                        { ps2 = con.prepareStatement("update ds_attachment_revision set onDisc=1 where id=?");}                        
                        ps2.setLong(1,id);
                        ps2.execute();
                        ps2.close();                        
                    }
                }       
                rsBlob.close();
                ps.close();
                form.moved++;
                form.getProgressBar().setValue(form.moved); 
                form.getRestFilesLogger().setText("Plikow do przeniesienia z bazy jest: "+String.valueOf(form.countFiles-form.moved));
                long X =  ((System.currentTimeMillis() - form.milis) *(form.countFiles-form.moved)/form.moved)/1000;
                if(X>60)
                    form.getTimer().setText("Szacowany pozosta�y czas: "+X/60+" minut " + (X-((X/60)*60))/5*5 + " sekund" );
                else
                    form.getTimer().setText("Szacowany pozosta�y czas: "+(X/5)*5+" sekund");
            }
            rs.close();
            stm.close();
            form.connection.close();
            
        } catch (Exception ex) 
        {
            form.getErrorLogger().setText(ex.getMessage());
        }
        finally{
            form.getStartButton().setVisible(true);
            form.getStopButton().setVisible(false);
        }
    }
    
    private File getFile(Long id, Date date) throws IOException {
        String s = form.pathToAttachments;
        StringBuffer path = new StringBuffer(s);
        path.append(File.separator);
        String filename = Long.toHexString(id);
        int len = filename.length();
        filename = ("00000000".concat(filename)).substring(len);
        if(form.pathToAttachmentsWithDate.equals("true")) {
            path.append(yearFormat.format(date));
            path.append(File.separator);
            path.append(dayFormat.format(date));
        } else {
            path.append(filename.substring(0,2));
            path.append(File.separator);
            path.append(filename.substring(2,5));
        }
        path.append(File.separator);
        
        (new File(path.toString().toUpperCase())).mkdirs();
        path.append(filename);
        path.append(".DAT");
        
        File file = new File(path.toString().toUpperCase());
        if(file.exists())
            file.delete();
        file.createNewFile();
        
        return file;
    }
}
