﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Irony.Parsing;
using System.Globalization;

namespace DSNet
{
    public class DSParser
    {
        public String toFT(string question)
        {
            SearchGrammar gram = new SearchGrammar();
            Parser parser = new Parser(gram);
            ParsingContext context = new ParsingContext(parser);
            ParseTree tree = null;
            tree = parser.Parse(question);
            SearchGrammar sg = new SearchGrammar();
            string ftString = SearchGrammar.ConvertQuery(tree.Root);
            return ftString;
        }
    }
}
