import net.sf.jni4net.Bridge;

import java.io.IOException;

import dsnet.ParsQuery;
import dsnet.DSParser;

public class DSQueryToFT {
    public static void main(String arsg[]) throws IOException {
        Bridge.init();
        Bridge.LoadAndRegisterAssemblyFrom(new java.io.File("DSParser.j4n.dll"));

        DSParser calc = new ParsQuery();
        String result = calc.toFT("Answer to the Ultimate Question of Life, the Universe, and Everything");

        System.out.printf("Answer to the Ultimate Question is : " + result);
    }
}
