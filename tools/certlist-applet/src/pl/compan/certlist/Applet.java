package pl.compan.certlist;

import java.io.PrintStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;
import javax.swing.JApplet;

/**
 *
 * @author Michał Sankowski <michal.sankowski@docusafe.pl>
 */
public class Applet extends JApplet {

	private final static char[] HEXDIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	private final static PrintStream log = System.out;
	private String jsontable = "'error'";

	public static KeyStore getMSKeyStore()
			throws KeyStoreException {
		if (!isWindowsPlatform()) {
			throw new KeyStoreException("Microsoft Key Store is only available on Windows platform.");
		} 
		try {
			//log.println("Using windows platform keystore");
			KeyStore MSKeyStore = KeyStore.getInstance("Windows-MY");
			MSKeyStore.load(null, null);
			return MSKeyStore;
		} catch (Exception e) {
			throw new KeyStoreException("Unable to open the Windows KeyStore. " + e.getMessage(), e);
		}
	}

	public static boolean isWindowsPlatform() {
		String os = System.getProperty("os.name");
		return (os != null) && (os.startsWith("Windows"));
	}

	@Override
	public void init() {
		super.init();
		//log.println("INIT");
	}

	@Override
	public void start() {
		super.start();
		//log.println("START");
		try {
			StringBuilder json = new StringBuilder();
			json.append("[");
			KeyStore ks = getMSKeyStore();
			List<String> aliases = Collections.list(ks.aliases());
			for (String alias : aliases) {
				processAlias(ks, alias, json);
			}
			if(json.indexOf(",") > 0){
				json.deleteCharAt(json.length()-1);
			}
			json.append("]");
			log.println("json = " + json);
			jsontable = json.toString();
		} catch (Exception ex) {
			log.format("exception: %s", ex.getMessage());
		}
	}

	public String getCertsAsJson(){
		return jsontable;
	}

	private void processAlias(KeyStore ks, String alias, StringBuilder json) throws CertificateException, KeyStoreException {
		if (ks.isKeyEntry(alias)) {
			json.append("{");
			Certificate cert = ks.getCertificate(alias);
			if (!(cert instanceof X509Certificate)) {
			   throw new CertificateException("Certificate entry should be an X509Certificate");
			}
			X509Certificate x509cert = (X509Certificate)cert;
			
			json.append("'alias':'").append(toJsonString(alias)).append("'");
			json.append(",'expirationDate':'").append(new SimpleDateFormat("yyyy-MM-dd")
					.format(x509cert.getNotAfter())).append("'");
			json.append(",'sha1':'").append(toSha1(x509cert)).append("'");
			json.append("},");
		}
	}

	private static String toJsonString(String string){
		return string.replace("\\", "\\\\").replace("\"", "\\\"");// \ -> \\  " -> \"
	}

	public String toSha1(X509Certificate cert) {
		try {
			byte[] derCert = cert.getEncoded();
			MessageDigest md;
			md = MessageDigest.getInstance("SHA1");
			byte[] digest = md.digest(derCert);
			return hexify(digest);
		} catch (Exception ex) {
			return "error : " + ex.getMessage();
		}
	}

	public static String hexify (byte bytes[]) {
        StringBuilder buf = new StringBuilder(bytes.length * 2);

        for (int i = 0; i < bytes.length; ++i) {
            buf.append(HEXDIGITS[(bytes[i] & 0xf0) >> 4]);
            buf.append(HEXDIGITS[bytes[i] & 0x0f]);
        }

        return buf.toString();
    }
}
