-- skrypt do czyszczenia bazy MS SQL przed wdrozeniem

-- czyscimy dokumenty

delete from ds_attachment_signature;
delete from ds_attachment_revision;


delete from ds_attachment;

delete from dsg_doctype_1;
delete from dsg_doctype_2;
delete from dsg_doctype_3;

delete from ds_document_changelog;
delete from ds_document_flags;
delete from ds_document_lock;
delete from ds_document_permission;
delete from ds_document_signature;
delete from ds_document_user_flags;
delete from ds_document_watch;

delete from ds_imported_document_info;
delete from ds_imported_file_info;
delete from ds_message_attachment;
delete from ds_message;

delete from ds_nw_Status_Trace;

delete from ds_report;

delete from ds_retained_attachment;
delete from ds_retained_object_props;
delete from ds_retained_object;

delete from ds_subst_hist;

delete from dso_assignment_objective;
delete from dso_collective_assignment;
delete from dso_container_audit;
delete from dso_container_remarks;
delete from dso_document_asgn_history;
delete from dso_document_audit;
delete from dso_document_remarks;
delete from dso_container;

delete from dso_in_document;
delete from dso_journal_entry
delete from dso_journal where closed=1 or ownerguid is not null;
update dso_journal set sequenceid = 1;
delete from dso_order;
delete from dso_person;
delete from dso_out_document;

--delete from dso_role_permissions;
--delete from dso_role_divisions;
--delete from dso_role_usernames;
--delete from dso_role;

delete from dso_school;
delete from dsw_activity_context;
delete from dsw_assignment;
delete from dsw_participant_mapping;
delete from dsw_process_context;
delete from dsw_process_Activities;
delete from dsw_activity;
delete from dsw_process;
delete from dsw_process_reminder;
delete from dsw_tasklist;
delete  from fax_temporary;

delete from ds_resource;
delete from dsw_jbpm_process_definition;
delete from dsw_jbpm_swimlane_mapping;
delete from dsw_jbpm_task_definition;

delete  from nw_import;
delete  from nw_perm;

delete from ds_document;
delete from ds_folder_permission where folder_id >3;
--delete from ds_folder where id >3;;


delete from ds_user_certificate;
delete from ds_user_cert;
delete from ds_user_passwd_history;
--delete from ds_user_to_division;
delete from ds_watchlist;
delete from ds_document_watch;
delete from dso_document_remarks;


--delete from ds_division where id > 1;
--delete from ds_user where id>1;


--delete from ds_prefs_value;
--delete from ds_prefs_node;