﻿<#
.SYNOPSIS
    Automatic dockind xml updating script.
.DESCRIPTION
    Watches for changes in xml file and upload to DocuSafe.
.NOTES
    File Name: dockind watcher.ps1
    Author: Kamil Omelańczuk
    Requires: PowerShell V2
.EXAMPLE
    Watches for changes in file C:\ds_svn\ilpl\etc\dockind\impulsDwr\cok_imported_doc.xml and upload as dockind codename 'cok_imported_doc'
    & '.\dockind watcher.ps1' -file "C:\ds_svn\ilpl\etc\dockind\impulsDwr\cok_imported_doc.xml" -url "http://localhost:8082/docusafe/" -username admin -password admin -dockindCn "cok_imported_doc"
#>

Param(
[Parameter(Mandatory=$true)]
[string]$url,
[Parameter(Mandatory=$true)]
[string]$username,
[Parameter(Mandatory=$true)]
[string]$password,
[Parameter(Mandatory=$true)]
[string]$file,
[Parameter(Mandatory=$true)]
[string]$dockindCn
)
[void] [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
#[Reflection.Assembly]::LoadFrom(".\Microsoft.WindowsAPICodePack.Shell.Dll")
Add-Type -path ".\Microsoft.WindowsAPICodePack.Shell.Dll"

$url = $url -replace "/$", ""
$uri = "$url/login.jsp?username=$username&password=$password"
$updateUri = "$url/admin/document-kinds.action?doCreate=true&cn=$dockindCn"

$folder = Split-Path $file
$filename = Split-Path $file -Leaf
$fullpath = $file
$session = $null

"Watching file $file"

$watcher = New-Object System.IO.FileSystemWatcher
$watcher.Path = $folder
$watcher.Filter = $filename
$watcher.IncludeSubdirectories = $false
$watcher.EnableRaisingEvents = $false
$watcher.NotifyFilter = [System.IO.NotifyFilters]::LastWrite -bor [System.IO.NotifyFilters]::FileName

$taskbar = [Microsoft.WindowsAPICodePack.TaskBar.TaskBarManager]::Instance

if($objNotifyIcon)
{
    $objNotifyIcon.Visible = $false
    $objNotifyIcon.Dispose
}
$objNotifyIcon = New-Object System.Windows.Forms.NotifyIcon
$objNotifyIcon.Icon = Convert-Path -Path "..\..\web\img\favicon.ico"

function IsoToUni([string]$isoString)
{
    [System.Text.Encoding]$isoEncoding = [System.Text.Encoding]::GetEncoding('ISO-8859-2')
    [System.Text.Encoding]$uniEncoding = [System.Text.Encoding]::Unicode
    $isoBytes = $isoEncoding.GetBytes($isoString)
    $uniBytes = [System.Text.Encoding]::Convert($isoEncoding, $uniEncoding, $isoBytes)
    $uniString = $uniEncoding.GetString($uniBytes)

    return $uniString
}


function UpdateDockind()
{
    
    $session = $null
    "Login as $username with password $password"
    $taskbar.SetProgressState("Normal")
    $taskbar.SetProgressValue(50, 100)
    $result = Invoke-WebRequest -Uri $uri -Method Get -SessionVariable session -UseBasicParsing

    #invalid password or username
    if($result.Content.Contains('logout.do') -eq $false)
    {
        $resp = $result.Content
        $err = $resp -match '(?<=<b\sclass="warning">).+(?=</b>)'
        if($Matches)
        {
            $err = ""
            $Matches.GetEnumerator() | % { $err += $_.value + ". " }
            $err = IsoToUni $err
            Write-Error $err
        }
        ShowNotification "Login failed" $err $true
        return
    }
    
    $cookie = $session.Cookies.GetCookies($uri)

    
    "Updating dockind $dockindCn"
    $webClient = New-Object System.Net.WebClient
    $webClient.Headers.Add("Cookie", "JSESSIONID=" + $cookie.Value)
    
    $response = $webClient.UploadFile($updateUri, $fullpath)
    $resp = [System.Text.Encoding]::ASCII.GetString($response)

    if($resp.Contains("Zapisano podany rodzaj dokumentu"))
    {
        Write-Host "$(Get-Date) Dockind updated" -BackgroundColor DarkGreen -ForegroundColor White  
        ShowNotification "Dockind watcher" "$(Get-Date) $dockindCn updated"
        $taskbar.SetProgressValue(100, 100)
    }
    else
    {
        "Error:"
        $err = $resp -match '(?<=<li>).+(?=</li>)'
        $errorMessage = "Unknown error"
        if($Matches)
        {
            $err = ""
            $Matches.GetEnumerator() | % { $err += $_.value + ". " }
            $err = IsoToUni($err)
            Write-Error $err
        }
        
        ShowNotification "Dockind watcher error" $err $true
    }
}

function ShowNotification($title, $text, $error=$false)
{
    $objNotifyIcon.Visible = $false
    if($error)
    {
        $objNotifyIcon.BalloonTipIcon = [System.Windows.Forms.ToolTipIcon]::Error
        $taskbar.SetProgressState("Error")
    }
    else
    {
        $objNotifyIcon.BalloonTipIcon = [System.Windows.Forms.ToolTipIcon]::Info
    }
    $objNotifyIcon.BalloonTipText = $text
    $objNotifyIcon.BalloonTipTitle = $title
    $objNotifyIcon.Visible = $true
    $objNotifyIcon.ShowBalloonTip(1000)
}



Write-Host "Press Q to stop"
while($true)
{
    if ($Host.UI.RawUI.KeyAvailable -and ("q" -eq $Host.UI.RawUI.ReadKey("IncludeKeyUp,NoEcho").Character)) {
        Write-Host "Exiting now, don't try to stop me...." -Background DarkRed
        $objNotifyIcon.Visible = $false
        $taskbar.SetProgressState("NoProgress")
        break;
    }
    $result = $watcher.WaitForChanged([System.IO.WatcherChangeTypes]::Changed -bor [System.IO.WatcherChangeTypes]::Created, 500)
    if($result.TimedOut)
    {
        $taskbar.SetProgressState("NoProgress")
        continue
    }

    Write-Host "Change in " $result.Name
    UpdateDockind  
       
}