Skrypt do automatycznego podgrywania zmian w dockindzie. 
Skrypt monitoruje plik i je�li nast�pi� zmiany to wgrywa zmieniony pliku xml w DocuSafe (tj. Administracja/Rodzaje dokument�w).
Do uruchomienia potrzebny jest PowerShell 4.0(nie dzia�a dla wersji 2.0):

1. Uruchamiamy konsol� PowerShell.
2. Przechodzimy do katalogu w kt�rym znajduje si� ten plik ("README.txt"), np.: cd 'C:\ds_svn\ilpl\tools\Dockind watcher'
3. Uruchamiamy skrypt z parametrami: 
& '.\dockind watcher.ps1' -file "C:\ds_svn\ilpl\etc\dockind\impulsDwr\cok_imported_doc.xml" -url "http://localhost:8082/docusafe/" -username admin -password admin -dockindCn "cok_imported_doc"

gdzie:
-file "C:\ds_svn\ilpl\etc\dockind\impulsDwr\cok_imported_doc.xml" - plik dockind xml, kt�ry b�dzie monitorowany
-url "http://localhost:8082/docusafe/" - pe�na �cie�ka do uruchomionego DocuSafe
-username admin - login u�ytkownika na kt�ry zaloguje si� skrypt
-password admin - has�o u�ytkownika na kt�re zaloguje si� skrypt
-dockindCn "cok_imported_doc" - nazwa kodowa dockinda


Po wgraniu dockinda pokazuje si� komunikat (powiadomienie systemowe w zasobniku system) z informacj� o podgraniu xmla, ewentualnie komunikat o b��dach.
Aby zatrzyma� skrypt naciskamy "q" (w konsoli PowerShell).
B��dy/pomys�y zg�asza� na bugzill� - Kamil Omela�czuk
