/**
 * SprawaWychodzaca.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class SprawaWychodzaca  implements java.io.Serializable {
    private java.lang.String znakSprawy;

    private java.lang.String nrKancelaryjny;

    private java.lang.String barcode;

    private java.lang.String kor_id;

    private java.lang.String spr_id;  // attribute

    public SprawaWychodzaca() {
    }

    public SprawaWychodzaca(
           java.lang.String znakSprawy,
           java.lang.String nrKancelaryjny,
           java.lang.String barcode,
           java.lang.String kor_id,
           java.lang.String spr_id) {
           this.znakSprawy = znakSprawy;
           this.nrKancelaryjny = nrKancelaryjny;
           this.barcode = barcode;
           this.kor_id = kor_id;
           this.spr_id = spr_id;
    }


    /**
     * Gets the znakSprawy value for this SprawaWychodzaca.
     * 
     * @return znakSprawy
     */
    public java.lang.String getZnakSprawy() {
        return znakSprawy;
    }


    /**
     * Sets the znakSprawy value for this SprawaWychodzaca.
     * 
     * @param znakSprawy
     */
    public void setZnakSprawy(java.lang.String znakSprawy) {
        this.znakSprawy = znakSprawy;
    }


    /**
     * Gets the nrKancelaryjny value for this SprawaWychodzaca.
     * 
     * @return nrKancelaryjny
     */
    public java.lang.String getNrKancelaryjny() {
        return nrKancelaryjny;
    }


    /**
     * Sets the nrKancelaryjny value for this SprawaWychodzaca.
     * 
     * @param nrKancelaryjny
     */
    public void setNrKancelaryjny(java.lang.String nrKancelaryjny) {
        this.nrKancelaryjny = nrKancelaryjny;
    }


    /**
     * Gets the barcode value for this SprawaWychodzaca.
     * 
     * @return barcode
     */
    public java.lang.String getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this SprawaWychodzaca.
     * 
     * @param barcode
     */
    public void setBarcode(java.lang.String barcode) {
        this.barcode = barcode;
    }


    /**
     * Gets the kor_id value for this SprawaWychodzaca.
     * 
     * @return kor_id
     */
    public java.lang.String getKor_id() {
        return kor_id;
    }


    /**
     * Sets the kor_id value for this SprawaWychodzaca.
     * 
     * @param kor_id
     */
    public void setKor_id(java.lang.String kor_id) {
        this.kor_id = kor_id;
    }


    /**
     * Gets the spr_id value for this SprawaWychodzaca.
     * 
     * @return spr_id
     */
    public java.lang.String getSpr_id() {
        return spr_id;
    }


    /**
     * Sets the spr_id value for this SprawaWychodzaca.
     * 
     * @param spr_id
     */
    public void setSpr_id(java.lang.String spr_id) {
        this.spr_id = spr_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SprawaWychodzaca)) return false;
        SprawaWychodzaca other = (SprawaWychodzaca) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.znakSprawy==null && other.getZnakSprawy()==null) || 
             (this.znakSprawy!=null &&
              this.znakSprawy.equals(other.getZnakSprawy()))) &&
            ((this.nrKancelaryjny==null && other.getNrKancelaryjny()==null) || 
             (this.nrKancelaryjny!=null &&
              this.nrKancelaryjny.equals(other.getNrKancelaryjny()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              this.barcode.equals(other.getBarcode()))) &&
            ((this.kor_id==null && other.getKor_id()==null) || 
             (this.kor_id!=null &&
              this.kor_id.equals(other.getKor_id()))) &&
            ((this.spr_id==null && other.getSpr_id()==null) || 
             (this.spr_id!=null &&
              this.spr_id.equals(other.getSpr_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getZnakSprawy() != null) {
            _hashCode += getZnakSprawy().hashCode();
        }
        if (getNrKancelaryjny() != null) {
            _hashCode += getNrKancelaryjny().hashCode();
        }
        if (getBarcode() != null) {
            _hashCode += getBarcode().hashCode();
        }
        if (getKor_id() != null) {
            _hashCode += getKor_id().hashCode();
        }
        if (getSpr_id() != null) {
            _hashCode += getSpr_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SprawaWychodzaca.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "SprawaWychodzaca"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("spr_id");
        attrField.setXmlName(new javax.xml.namespace.QName("", "spr_id"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("znakSprawy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "znakSprawy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nrKancelaryjny");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "nrKancelaryjny"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
