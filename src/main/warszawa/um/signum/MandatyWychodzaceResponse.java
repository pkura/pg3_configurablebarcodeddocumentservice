/**
 * MandatyWychodzaceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class MandatyWychodzaceResponse  implements java.io.Serializable {
    private java.lang.String mandatyWychodzaceResult;

    public MandatyWychodzaceResponse() {
    }

    public MandatyWychodzaceResponse(
           java.lang.String mandatyWychodzaceResult) {
           this.mandatyWychodzaceResult = mandatyWychodzaceResult;
    }


    /**
     * Gets the mandatyWychodzaceResult value for this MandatyWychodzaceResponse.
     * 
     * @return mandatyWychodzaceResult
     */
    public java.lang.String getMandatyWychodzaceResult() {
        return mandatyWychodzaceResult;
    }


    /**
     * Sets the mandatyWychodzaceResult value for this MandatyWychodzaceResponse.
     * 
     * @param mandatyWychodzaceResult
     */
    public void setMandatyWychodzaceResult(java.lang.String mandatyWychodzaceResult) {
        this.mandatyWychodzaceResult = mandatyWychodzaceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MandatyWychodzaceResponse)) return false;
        MandatyWychodzaceResponse other = (MandatyWychodzaceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.mandatyWychodzaceResult==null && other.getMandatyWychodzaceResult()==null) || 
             (this.mandatyWychodzaceResult!=null &&
              this.mandatyWychodzaceResult.equals(other.getMandatyWychodzaceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMandatyWychodzaceResult() != null) {
            _hashCode += getMandatyWychodzaceResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MandatyWychodzaceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">MandatyWychodzaceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mandatyWychodzaceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "MandatyWychodzaceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
