/**
 * Test_SprawaWychResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class Test_SprawaWychResponse  implements java.io.Serializable {
    private warszawa.um.signum.SprawaWychodzaca sprawa;

    public Test_SprawaWychResponse() {
    }

    public Test_SprawaWychResponse(
           warszawa.um.signum.SprawaWychodzaca sprawa) {
           this.sprawa = sprawa;
    }


    /**
     * Gets the sprawa value for this Test_SprawaWychResponse.
     * 
     * @return sprawa
     */
    public warszawa.um.signum.SprawaWychodzaca getSprawa() {
        return sprawa;
    }


    /**
     * Sets the sprawa value for this Test_SprawaWychResponse.
     * 
     * @param sprawa
     */
    public void setSprawa(warszawa.um.signum.SprawaWychodzaca sprawa) {
        this.sprawa = sprawa;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Test_SprawaWychResponse)) return false;
        Test_SprawaWychResponse other = (Test_SprawaWychResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sprawa==null && other.getSprawa()==null) || 
             (this.sprawa!=null &&
              this.sprawa.equals(other.getSprawa())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSprawa() != null) {
            _hashCode += getSprawa().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Test_SprawaWychResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">test_SprawaWychResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sprawa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "sprawa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "SprawaWychodzaca"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
