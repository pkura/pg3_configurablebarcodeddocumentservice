/**
 * Korespondent2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class Korespondent2  implements java.io.Serializable {
    private java.lang.String nazwisko;

    private java.lang.String imie;

    private java.lang.String nazwaFirmy;

    private java.lang.String miasto;

    private java.lang.String kod;

    private java.lang.String ulica;

    private java.lang.String dom;

    private java.lang.String lokal;

    private java.lang.String regon;

    private java.lang.String nip;

    private java.lang.String kor_id;  // attribute

    public Korespondent2() {
    }

    public Korespondent2(
           java.lang.String nazwisko,
           java.lang.String imie,
           java.lang.String nazwaFirmy,
           java.lang.String miasto,
           java.lang.String kod,
           java.lang.String ulica,
           java.lang.String dom,
           java.lang.String lokal,
           java.lang.String regon,
           java.lang.String nip,
           java.lang.String kor_id) {
           this.nazwisko = nazwisko;
           this.imie = imie;
           this.nazwaFirmy = nazwaFirmy;
           this.miasto = miasto;
           this.kod = kod;
           this.ulica = ulica;
           this.dom = dom;
           this.lokal = lokal;
           this.regon = regon;
           this.nip = nip;
           this.kor_id = kor_id;
    }


    /**
     * Gets the nazwisko value for this Korespondent2.
     * 
     * @return nazwisko
     */
    public java.lang.String getNazwisko() {
        return nazwisko;
    }


    /**
     * Sets the nazwisko value for this Korespondent2.
     * 
     * @param nazwisko
     */
    public void setNazwisko(java.lang.String nazwisko) {
        this.nazwisko = nazwisko;
    }


    /**
     * Gets the imie value for this Korespondent2.
     * 
     * @return imie
     */
    public java.lang.String getImie() {
        return imie;
    }


    /**
     * Sets the imie value for this Korespondent2.
     * 
     * @param imie
     */
    public void setImie(java.lang.String imie) {
        this.imie = imie;
    }


    /**
     * Gets the nazwaFirmy value for this Korespondent2.
     * 
     * @return nazwaFirmy
     */
    public java.lang.String getNazwaFirmy() {
        return nazwaFirmy;
    }


    /**
     * Sets the nazwaFirmy value for this Korespondent2.
     * 
     * @param nazwaFirmy
     */
    public void setNazwaFirmy(java.lang.String nazwaFirmy) {
        this.nazwaFirmy = nazwaFirmy;
    }


    /**
     * Gets the miasto value for this Korespondent2.
     * 
     * @return miasto
     */
    public java.lang.String getMiasto() {
        return miasto;
    }


    /**
     * Sets the miasto value for this Korespondent2.
     * 
     * @param miasto
     */
    public void setMiasto(java.lang.String miasto) {
        this.miasto = miasto;
    }


    /**
     * Gets the kod value for this Korespondent2.
     * 
     * @return kod
     */
    public java.lang.String getKod() {
        return kod;
    }


    /**
     * Sets the kod value for this Korespondent2.
     * 
     * @param kod
     */
    public void setKod(java.lang.String kod) {
        this.kod = kod;
    }


    /**
     * Gets the ulica value for this Korespondent2.
     * 
     * @return ulica
     */
    public java.lang.String getUlica() {
        return ulica;
    }


    /**
     * Sets the ulica value for this Korespondent2.
     * 
     * @param ulica
     */
    public void setUlica(java.lang.String ulica) {
        this.ulica = ulica;
    }


    /**
     * Gets the dom value for this Korespondent2.
     * 
     * @return dom
     */
    public java.lang.String getDom() {
        return dom;
    }


    /**
     * Sets the dom value for this Korespondent2.
     * 
     * @param dom
     */
    public void setDom(java.lang.String dom) {
        this.dom = dom;
    }


    /**
     * Gets the lokal value for this Korespondent2.
     * 
     * @return lokal
     */
    public java.lang.String getLokal() {
        return lokal;
    }


    /**
     * Sets the lokal value for this Korespondent2.
     * 
     * @param lokal
     */
    public void setLokal(java.lang.String lokal) {
        this.lokal = lokal;
    }


    /**
     * Gets the regon value for this Korespondent2.
     * 
     * @return regon
     */
    public java.lang.String getRegon() {
        return regon;
    }


    /**
     * Sets the regon value for this Korespondent2.
     * 
     * @param regon
     */
    public void setRegon(java.lang.String regon) {
        this.regon = regon;
    }


    /**
     * Gets the nip value for this Korespondent2.
     * 
     * @return nip
     */
    public java.lang.String getNip() {
        return nip;
    }


    /**
     * Sets the nip value for this Korespondent2.
     * 
     * @param nip
     */
    public void setNip(java.lang.String nip) {
        this.nip = nip;
    }


    /**
     * Gets the kor_id value for this Korespondent2.
     * 
     * @return kor_id
     */
    public java.lang.String getKor_id() {
        return kor_id;
    }


    /**
     * Sets the kor_id value for this Korespondent2.
     * 
     * @param kor_id
     */
    public void setKor_id(java.lang.String kor_id) {
        this.kor_id = kor_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Korespondent2)) return false;
        Korespondent2 other = (Korespondent2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nazwisko==null && other.getNazwisko()==null) || 
             (this.nazwisko!=null &&
              this.nazwisko.equals(other.getNazwisko()))) &&
            ((this.imie==null && other.getImie()==null) || 
             (this.imie!=null &&
              this.imie.equals(other.getImie()))) &&
            ((this.nazwaFirmy==null && other.getNazwaFirmy()==null) || 
             (this.nazwaFirmy!=null &&
              this.nazwaFirmy.equals(other.getNazwaFirmy()))) &&
            ((this.miasto==null && other.getMiasto()==null) || 
             (this.miasto!=null &&
              this.miasto.equals(other.getMiasto()))) &&
            ((this.kod==null && other.getKod()==null) || 
             (this.kod!=null &&
              this.kod.equals(other.getKod()))) &&
            ((this.ulica==null && other.getUlica()==null) || 
             (this.ulica!=null &&
              this.ulica.equals(other.getUlica()))) &&
            ((this.dom==null && other.getDom()==null) || 
             (this.dom!=null &&
              this.dom.equals(other.getDom()))) &&
            ((this.lokal==null && other.getLokal()==null) || 
             (this.lokal!=null &&
              this.lokal.equals(other.getLokal()))) &&
            ((this.regon==null && other.getRegon()==null) || 
             (this.regon!=null &&
              this.regon.equals(other.getRegon()))) &&
            ((this.nip==null && other.getNip()==null) || 
             (this.nip!=null &&
              this.nip.equals(other.getNip()))) &&
            ((this.kor_id==null && other.getKor_id()==null) || 
             (this.kor_id!=null &&
              this.kor_id.equals(other.getKor_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNazwisko() != null) {
            _hashCode += getNazwisko().hashCode();
        }
        if (getImie() != null) {
            _hashCode += getImie().hashCode();
        }
        if (getNazwaFirmy() != null) {
            _hashCode += getNazwaFirmy().hashCode();
        }
        if (getMiasto() != null) {
            _hashCode += getMiasto().hashCode();
        }
        if (getKod() != null) {
            _hashCode += getKod().hashCode();
        }
        if (getUlica() != null) {
            _hashCode += getUlica().hashCode();
        }
        if (getDom() != null) {
            _hashCode += getDom().hashCode();
        }
        if (getLokal() != null) {
            _hashCode += getLokal().hashCode();
        }
        if (getRegon() != null) {
            _hashCode += getRegon().hashCode();
        }
        if (getNip() != null) {
            _hashCode += getNip().hashCode();
        }
        if (getKor_id() != null) {
            _hashCode += getKor_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Korespondent2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Korespondent2"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("kor_id");
        attrField.setXmlName(new javax.xml.namespace.QName("", "kor_id"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwisko");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "nazwisko"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "imie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwaFirmy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "nazwaFirmy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("miasto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "miasto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ulica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "ulica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "dom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lokal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "lokal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regon");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "regon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "nip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
