/**
 * GetSprawaResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetSprawaResponse  implements java.io.Serializable {
    private warszawa.um.signum.Sprawa getSprawaResult;

    public GetSprawaResponse() {
    }

    public GetSprawaResponse(
           warszawa.um.signum.Sprawa getSprawaResult) {
           this.getSprawaResult = getSprawaResult;
    }


    /**
     * Gets the getSprawaResult value for this GetSprawaResponse.
     * 
     * @return getSprawaResult
     */
    public warszawa.um.signum.Sprawa getGetSprawaResult() {
        return getSprawaResult;
    }


    /**
     * Sets the getSprawaResult value for this GetSprawaResponse.
     * 
     * @param getSprawaResult
     */
    public void setGetSprawaResult(warszawa.um.signum.Sprawa getSprawaResult) {
        this.getSprawaResult = getSprawaResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSprawaResponse)) return false;
        GetSprawaResponse other = (GetSprawaResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getSprawaResult==null && other.getGetSprawaResult()==null) || 
             (this.getSprawaResult!=null &&
              this.getSprawaResult.equals(other.getGetSprawaResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetSprawaResult() != null) {
            _hashCode += getGetSprawaResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSprawaResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetSprawaResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getSprawaResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "GetSprawaResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawa"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
