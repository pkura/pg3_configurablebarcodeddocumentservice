/**
 * GetIdSprawPrzychodzacychResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetIdSprawPrzychodzacychResponse  implements java.io.Serializable {
    private java.lang.String[] getIdSprawPrzychodzacychResult;

    public GetIdSprawPrzychodzacychResponse() {
    }

    public GetIdSprawPrzychodzacychResponse(
           java.lang.String[] getIdSprawPrzychodzacychResult) {
           this.getIdSprawPrzychodzacychResult = getIdSprawPrzychodzacychResult;
    }


    /**
     * Gets the getIdSprawPrzychodzacychResult value for this GetIdSprawPrzychodzacychResponse.
     * 
     * @return getIdSprawPrzychodzacychResult
     */
    public java.lang.String[] getGetIdSprawPrzychodzacychResult() {
        return getIdSprawPrzychodzacychResult;
    }


    /**
     * Sets the getIdSprawPrzychodzacychResult value for this GetIdSprawPrzychodzacychResponse.
     * 
     * @param getIdSprawPrzychodzacychResult
     */
    public void setGetIdSprawPrzychodzacychResult(java.lang.String[] getIdSprawPrzychodzacychResult) {
        this.getIdSprawPrzychodzacychResult = getIdSprawPrzychodzacychResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIdSprawPrzychodzacychResponse)) return false;
        GetIdSprawPrzychodzacychResponse other = (GetIdSprawPrzychodzacychResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getIdSprawPrzychodzacychResult==null && other.getGetIdSprawPrzychodzacychResult()==null) || 
             (this.getIdSprawPrzychodzacychResult!=null &&
              java.util.Arrays.equals(this.getIdSprawPrzychodzacychResult, other.getGetIdSprawPrzychodzacychResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetIdSprawPrzychodzacychResult() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetIdSprawPrzychodzacychResult());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetIdSprawPrzychodzacychResult(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetIdSprawPrzychodzacychResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetIdSprawPrzychodzacychResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getIdSprawPrzychodzacychResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "GetIdSprawPrzychodzacychResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "spr_id"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
