/**
 * Service1Soap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public interface Service1Soap extends java.rmi.Remote {

    /**
     * Metoda testowa.
     */
    public java.lang.String helloWorld() throws java.rmi.RemoteException;

    /**
     * Zwraca wszystkie sprawy w zakładce w realizacji u uzytkownika.
     */
    public warszawa.um.signum.TSprawy getSprawyWRealizacjiRWA(java.lang.String urz_id, java.lang.String rwa, java.lang.String pis_dataWplywu, java.lang.String login, java.lang.String haslo) throws java.rmi.RemoteException;

    /**
     * Import danych Ogolny
     */
    public warszawa.um.signum.TWrzuconeSprawy wrzucSprawyDoSignum(java.lang.String sXML, java.lang.String login, java.lang.String haslo) throws java.rmi.RemoteException;

    /**
     * Zwraca informacje o sprawie na podstawie jej barcode. np: 5008070113023961827
     */
    public warszawa.um.signum.Sprawa getSprawaZaSkan(java.lang.String barcode, java.lang.String skanLink, java.lang.String login, java.lang.String haslo) throws java.rmi.RemoteException;
}
