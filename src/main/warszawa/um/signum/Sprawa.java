/**
 * Sprawa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class Sprawa  implements java.io.Serializable {
    private java.lang.String spr_id;

    private java.lang.String pis_tresc;

    private java.lang.String pis_dataWplywu;

    private java.lang.String kor_id;

    private java.lang.String kor_kodA;

    private java.lang.String kor_kodB;

    private java.lang.String kor_miasto;

    private java.lang.String kor_ulica;

    private java.lang.String kor_dom;

    private java.lang.String kor_lokal;

    private java.lang.String kor_nazwaFirmy;

    private java.lang.String kor_nazwisko;

    private java.lang.String kor_imie;

    private java.lang.String kor_osobaFirma;

    private java.lang.String pis_nrKancelaryjny;

    private java.lang.String kor_mail;

    private java.lang.String kor_nip;

    private java.lang.String error;

    public Sprawa() {
    }

    public Sprawa(
           java.lang.String spr_id,
           java.lang.String pis_tresc,
           java.lang.String pis_dataWplywu,
           java.lang.String kor_id,
           java.lang.String kor_kodA,
           java.lang.String kor_kodB,
           java.lang.String kor_miasto,
           java.lang.String kor_ulica,
           java.lang.String kor_dom,
           java.lang.String kor_lokal,
           java.lang.String kor_nazwaFirmy,
           java.lang.String kor_nazwisko,
           java.lang.String kor_imie,
           java.lang.String kor_osobaFirma,
           java.lang.String pis_nrKancelaryjny,
           java.lang.String kor_mail,
           java.lang.String kor_nip,
           java.lang.String error) {
           this.spr_id = spr_id;
           this.pis_tresc = pis_tresc;
           this.pis_dataWplywu = pis_dataWplywu;
           this.kor_id = kor_id;
           this.kor_kodA = kor_kodA;
           this.kor_kodB = kor_kodB;
           this.kor_miasto = kor_miasto;
           this.kor_ulica = kor_ulica;
           this.kor_dom = kor_dom;
           this.kor_lokal = kor_lokal;
           this.kor_nazwaFirmy = kor_nazwaFirmy;
           this.kor_nazwisko = kor_nazwisko;
           this.kor_imie = kor_imie;
           this.kor_osobaFirma = kor_osobaFirma;
           this.pis_nrKancelaryjny = pis_nrKancelaryjny;
           this.kor_mail = kor_mail;
           this.kor_nip = kor_nip;
           this.error = error;
    }


    /**
     * Gets the spr_id value for this Sprawa.
     * 
     * @return spr_id
     */
    public java.lang.String getSpr_id() {
        return spr_id;
    }


    /**
     * Sets the spr_id value for this Sprawa.
     * 
     * @param spr_id
     */
    public void setSpr_id(java.lang.String spr_id) {
        this.spr_id = spr_id;
    }


    /**
     * Gets the pis_tresc value for this Sprawa.
     * 
     * @return pis_tresc
     */
    public java.lang.String getPis_tresc() {
        return pis_tresc;
    }


    /**
     * Sets the pis_tresc value for this Sprawa.
     * 
     * @param pis_tresc
     */
    public void setPis_tresc(java.lang.String pis_tresc) {
        this.pis_tresc = pis_tresc;
    }


    /**
     * Gets the pis_dataWplywu value for this Sprawa.
     * 
     * @return pis_dataWplywu
     */
    public java.lang.String getPis_dataWplywu() {
        return pis_dataWplywu;
    }


    /**
     * Sets the pis_dataWplywu value for this Sprawa.
     * 
     * @param pis_dataWplywu
     */
    public void setPis_dataWplywu(java.lang.String pis_dataWplywu) {
        this.pis_dataWplywu = pis_dataWplywu;
    }


    /**
     * Gets the kor_id value for this Sprawa.
     * 
     * @return kor_id
     */
    public java.lang.String getKor_id() {
        return kor_id;
    }


    /**
     * Sets the kor_id value for this Sprawa.
     * 
     * @param kor_id
     */
    public void setKor_id(java.lang.String kor_id) {
        this.kor_id = kor_id;
    }


    /**
     * Gets the kor_kodA value for this Sprawa.
     * 
     * @return kor_kodA
     */
    public java.lang.String getKor_kodA() {
        return kor_kodA;
    }


    /**
     * Sets the kor_kodA value for this Sprawa.
     * 
     * @param kor_kodA
     */
    public void setKor_kodA(java.lang.String kor_kodA) {
        this.kor_kodA = kor_kodA;
    }


    /**
     * Gets the kor_kodB value for this Sprawa.
     * 
     * @return kor_kodB
     */
    public java.lang.String getKor_kodB() {
        return kor_kodB;
    }


    /**
     * Sets the kor_kodB value for this Sprawa.
     * 
     * @param kor_kodB
     */
    public void setKor_kodB(java.lang.String kor_kodB) {
        this.kor_kodB = kor_kodB;
    }


    /**
     * Gets the kor_miasto value for this Sprawa.
     * 
     * @return kor_miasto
     */
    public java.lang.String getKor_miasto() {
        return kor_miasto;
    }


    /**
     * Sets the kor_miasto value for this Sprawa.
     * 
     * @param kor_miasto
     */
    public void setKor_miasto(java.lang.String kor_miasto) {
        this.kor_miasto = kor_miasto;
    }


    /**
     * Gets the kor_ulica value for this Sprawa.
     * 
     * @return kor_ulica
     */
    public java.lang.String getKor_ulica() {
        return kor_ulica;
    }


    /**
     * Sets the kor_ulica value for this Sprawa.
     * 
     * @param kor_ulica
     */
    public void setKor_ulica(java.lang.String kor_ulica) {
        this.kor_ulica = kor_ulica;
    }


    /**
     * Gets the kor_dom value for this Sprawa.
     * 
     * @return kor_dom
     */
    public java.lang.String getKor_dom() {
        return kor_dom;
    }


    /**
     * Sets the kor_dom value for this Sprawa.
     * 
     * @param kor_dom
     */
    public void setKor_dom(java.lang.String kor_dom) {
        this.kor_dom = kor_dom;
    }


    /**
     * Gets the kor_lokal value for this Sprawa.
     * 
     * @return kor_lokal
     */
    public java.lang.String getKor_lokal() {
        return kor_lokal;
    }


    /**
     * Sets the kor_lokal value for this Sprawa.
     * 
     * @param kor_lokal
     */
    public void setKor_lokal(java.lang.String kor_lokal) {
        this.kor_lokal = kor_lokal;
    }


    /**
     * Gets the kor_nazwaFirmy value for this Sprawa.
     * 
     * @return kor_nazwaFirmy
     */
    public java.lang.String getKor_nazwaFirmy() {
        return kor_nazwaFirmy;
    }


    /**
     * Sets the kor_nazwaFirmy value for this Sprawa.
     * 
     * @param kor_nazwaFirmy
     */
    public void setKor_nazwaFirmy(java.lang.String kor_nazwaFirmy) {
        this.kor_nazwaFirmy = kor_nazwaFirmy;
    }


    /**
     * Gets the kor_nazwisko value for this Sprawa.
     * 
     * @return kor_nazwisko
     */
    public java.lang.String getKor_nazwisko() {
        return kor_nazwisko;
    }


    /**
     * Sets the kor_nazwisko value for this Sprawa.
     * 
     * @param kor_nazwisko
     */
    public void setKor_nazwisko(java.lang.String kor_nazwisko) {
        this.kor_nazwisko = kor_nazwisko;
    }


    /**
     * Gets the kor_imie value for this Sprawa.
     * 
     * @return kor_imie
     */
    public java.lang.String getKor_imie() {
        return kor_imie;
    }


    /**
     * Sets the kor_imie value for this Sprawa.
     * 
     * @param kor_imie
     */
    public void setKor_imie(java.lang.String kor_imie) {
        this.kor_imie = kor_imie;
    }


    /**
     * Gets the kor_osobaFirma value for this Sprawa.
     * 
     * @return kor_osobaFirma
     */
    public java.lang.String getKor_osobaFirma() {
        return kor_osobaFirma;
    }


    /**
     * Sets the kor_osobaFirma value for this Sprawa.
     * 
     * @param kor_osobaFirma
     */
    public void setKor_osobaFirma(java.lang.String kor_osobaFirma) {
        this.kor_osobaFirma = kor_osobaFirma;
    }


    /**
     * Gets the pis_nrKancelaryjny value for this Sprawa.
     * 
     * @return pis_nrKancelaryjny
     */
    public java.lang.String getPis_nrKancelaryjny() {
        return pis_nrKancelaryjny;
    }


    /**
     * Sets the pis_nrKancelaryjny value for this Sprawa.
     * 
     * @param pis_nrKancelaryjny
     */
    public void setPis_nrKancelaryjny(java.lang.String pis_nrKancelaryjny) {
        this.pis_nrKancelaryjny = pis_nrKancelaryjny;
    }


    /**
     * Gets the kor_mail value for this Sprawa.
     * 
     * @return kor_mail
     */
    public java.lang.String getKor_mail() {
        return kor_mail;
    }


    /**
     * Sets the kor_mail value for this Sprawa.
     * 
     * @param kor_mail
     */
    public void setKor_mail(java.lang.String kor_mail) {
        this.kor_mail = kor_mail;
    }


    /**
     * Gets the kor_nip value for this Sprawa.
     * 
     * @return kor_nip
     */
    public java.lang.String getKor_nip() {
        return kor_nip;
    }


    /**
     * Sets the kor_nip value for this Sprawa.
     * 
     * @param kor_nip
     */
    public void setKor_nip(java.lang.String kor_nip) {
        this.kor_nip = kor_nip;
    }


    /**
     * Gets the error value for this Sprawa.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this Sprawa.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Sprawa)) return false;
        Sprawa other = (Sprawa) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.spr_id==null && other.getSpr_id()==null) || 
             (this.spr_id!=null &&
              this.spr_id.equals(other.getSpr_id()))) &&
            ((this.pis_tresc==null && other.getPis_tresc()==null) || 
             (this.pis_tresc!=null &&
              this.pis_tresc.equals(other.getPis_tresc()))) &&
            ((this.pis_dataWplywu==null && other.getPis_dataWplywu()==null) || 
             (this.pis_dataWplywu!=null &&
              this.pis_dataWplywu.equals(other.getPis_dataWplywu()))) &&
            ((this.kor_id==null && other.getKor_id()==null) || 
             (this.kor_id!=null &&
              this.kor_id.equals(other.getKor_id()))) &&
            ((this.kor_kodA==null && other.getKor_kodA()==null) || 
             (this.kor_kodA!=null &&
              this.kor_kodA.equals(other.getKor_kodA()))) &&
            ((this.kor_kodB==null && other.getKor_kodB()==null) || 
             (this.kor_kodB!=null &&
              this.kor_kodB.equals(other.getKor_kodB()))) &&
            ((this.kor_miasto==null && other.getKor_miasto()==null) || 
             (this.kor_miasto!=null &&
              this.kor_miasto.equals(other.getKor_miasto()))) &&
            ((this.kor_ulica==null && other.getKor_ulica()==null) || 
             (this.kor_ulica!=null &&
              this.kor_ulica.equals(other.getKor_ulica()))) &&
            ((this.kor_dom==null && other.getKor_dom()==null) || 
             (this.kor_dom!=null &&
              this.kor_dom.equals(other.getKor_dom()))) &&
            ((this.kor_lokal==null && other.getKor_lokal()==null) || 
             (this.kor_lokal!=null &&
              this.kor_lokal.equals(other.getKor_lokal()))) &&
            ((this.kor_nazwaFirmy==null && other.getKor_nazwaFirmy()==null) || 
             (this.kor_nazwaFirmy!=null &&
              this.kor_nazwaFirmy.equals(other.getKor_nazwaFirmy()))) &&
            ((this.kor_nazwisko==null && other.getKor_nazwisko()==null) || 
             (this.kor_nazwisko!=null &&
              this.kor_nazwisko.equals(other.getKor_nazwisko()))) &&
            ((this.kor_imie==null && other.getKor_imie()==null) || 
             (this.kor_imie!=null &&
              this.kor_imie.equals(other.getKor_imie()))) &&
            ((this.kor_osobaFirma==null && other.getKor_osobaFirma()==null) || 
             (this.kor_osobaFirma!=null &&
              this.kor_osobaFirma.equals(other.getKor_osobaFirma()))) &&
            ((this.pis_nrKancelaryjny==null && other.getPis_nrKancelaryjny()==null) || 
             (this.pis_nrKancelaryjny!=null &&
              this.pis_nrKancelaryjny.equals(other.getPis_nrKancelaryjny()))) &&
            ((this.kor_mail==null && other.getKor_mail()==null) || 
             (this.kor_mail!=null &&
              this.kor_mail.equals(other.getKor_mail()))) &&
            ((this.kor_nip==null && other.getKor_nip()==null) || 
             (this.kor_nip!=null &&
              this.kor_nip.equals(other.getKor_nip()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSpr_id() != null) {
            _hashCode += getSpr_id().hashCode();
        }
        if (getPis_tresc() != null) {
            _hashCode += getPis_tresc().hashCode();
        }
        if (getPis_dataWplywu() != null) {
            _hashCode += getPis_dataWplywu().hashCode();
        }
        if (getKor_id() != null) {
            _hashCode += getKor_id().hashCode();
        }
        if (getKor_kodA() != null) {
            _hashCode += getKor_kodA().hashCode();
        }
        if (getKor_kodB() != null) {
            _hashCode += getKor_kodB().hashCode();
        }
        if (getKor_miasto() != null) {
            _hashCode += getKor_miasto().hashCode();
        }
        if (getKor_ulica() != null) {
            _hashCode += getKor_ulica().hashCode();
        }
        if (getKor_dom() != null) {
            _hashCode += getKor_dom().hashCode();
        }
        if (getKor_lokal() != null) {
            _hashCode += getKor_lokal().hashCode();
        }
        if (getKor_nazwaFirmy() != null) {
            _hashCode += getKor_nazwaFirmy().hashCode();
        }
        if (getKor_nazwisko() != null) {
            _hashCode += getKor_nazwisko().hashCode();
        }
        if (getKor_imie() != null) {
            _hashCode += getKor_imie().hashCode();
        }
        if (getKor_osobaFirma() != null) {
            _hashCode += getKor_osobaFirma().hashCode();
        }
        if (getPis_nrKancelaryjny() != null) {
            _hashCode += getPis_nrKancelaryjny().hashCode();
        }
        if (getKor_mail() != null) {
            _hashCode += getKor_mail().hashCode();
        }
        if (getKor_nip() != null) {
            _hashCode += getKor_nip().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Sprawa.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawa"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("spr_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "spr_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pis_tresc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "pis_tresc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pis_dataWplywu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "pis_dataWplywu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_kodA");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_kodA"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_kodB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_kodB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_miasto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_miasto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_ulica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_ulica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_dom");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_dom"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_lokal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_lokal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_nazwaFirmy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_nazwaFirmy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_nazwisko");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_nazwisko"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_imie");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_imie"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_osobaFirma");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_osobaFirma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pis_nrKancelaryjny");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "pis_nrKancelaryjny"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_mail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_mail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kor_nip");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kor_nip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
