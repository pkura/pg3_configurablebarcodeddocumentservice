/**
 * Aaa.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class Aaa  implements java.io.Serializable {
    private java.lang.String literalOrderID;

    private java.lang.String param;  // attribute

    public Aaa() {
    }

    public Aaa(
           java.lang.String literalOrderID,
           java.lang.String param) {
           this.literalOrderID = literalOrderID;
           this.param = param;
    }


    /**
     * Gets the literalOrderID value for this Aaa.
     * 
     * @return literalOrderID
     */
    public java.lang.String getLiteralOrderID() {
        return literalOrderID;
    }


    /**
     * Sets the literalOrderID value for this Aaa.
     * 
     * @param literalOrderID
     */
    public void setLiteralOrderID(java.lang.String literalOrderID) {
        this.literalOrderID = literalOrderID;
    }


    /**
     * Gets the param value for this Aaa.
     * 
     * @return param
     */
    public java.lang.String getParam() {
        return param;
    }


    /**
     * Sets the param value for this Aaa.
     * 
     * @param param
     */
    public void setParam(java.lang.String param) {
        this.param = param;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Aaa)) return false;
        Aaa other = (Aaa) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.literalOrderID==null && other.getLiteralOrderID()==null) || 
             (this.literalOrderID!=null &&
              this.literalOrderID.equals(other.getLiteralOrderID()))) &&
            ((this.param==null && other.getParam()==null) || 
             (this.param!=null &&
              this.param.equals(other.getParam())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLiteralOrderID() != null) {
            _hashCode += getLiteralOrderID().hashCode();
        }
        if (getParam() != null) {
            _hashCode += getParam().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Aaa.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "aaa"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("param");
        attrField.setXmlName(new javax.xml.namespace.QName("", "param"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("literalOrderID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "LiteralOrderID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
