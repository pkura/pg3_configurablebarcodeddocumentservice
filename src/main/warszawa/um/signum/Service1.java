/**
 * Service1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public interface Service1 extends javax.xml.rpc.Service {
    public java.lang.String getService1Soap12Address();

    public warszawa.um.signum.Service1Soap getService1Soap12() throws javax.xml.rpc.ServiceException;

    public warszawa.um.signum.Service1Soap getService1Soap12(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getService1SoapAddress();

    public warszawa.um.signum.Service1Soap getService1Soap() throws javax.xml.rpc.ServiceException;

    public warszawa.um.signum.Service1Soap getService1Soap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
