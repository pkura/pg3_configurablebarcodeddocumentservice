/**
 * SprawaWychodzaca2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class SprawaWychodzaca2  implements java.io.Serializable {
    private java.lang.String znakSprawy;

    private java.lang.String nrKancelaryjny;

    private java.lang.String barcode;

    private warszawa.um.signum.Korespondent2 korespondent;

    private java.lang.String temat;

    private java.lang.String informacjaDodatkowa;

    private java.lang.String spr_id;  // attribute

    public SprawaWychodzaca2() {
    }

    public SprawaWychodzaca2(
           java.lang.String znakSprawy,
           java.lang.String nrKancelaryjny,
           java.lang.String barcode,
           warszawa.um.signum.Korespondent2 korespondent,
           java.lang.String temat,
           java.lang.String informacjaDodatkowa,
           java.lang.String spr_id) {
           this.znakSprawy = znakSprawy;
           this.nrKancelaryjny = nrKancelaryjny;
           this.barcode = barcode;
           this.korespondent = korespondent;
           this.temat = temat;
           this.informacjaDodatkowa = informacjaDodatkowa;
           this.spr_id = spr_id;
    }


    /**
     * Gets the znakSprawy value for this SprawaWychodzaca2.
     * 
     * @return znakSprawy
     */
    public java.lang.String getZnakSprawy() {
        return znakSprawy;
    }


    /**
     * Sets the znakSprawy value for this SprawaWychodzaca2.
     * 
     * @param znakSprawy
     */
    public void setZnakSprawy(java.lang.String znakSprawy) {
        this.znakSprawy = znakSprawy;
    }


    /**
     * Gets the nrKancelaryjny value for this SprawaWychodzaca2.
     * 
     * @return nrKancelaryjny
     */
    public java.lang.String getNrKancelaryjny() {
        return nrKancelaryjny;
    }


    /**
     * Sets the nrKancelaryjny value for this SprawaWychodzaca2.
     * 
     * @param nrKancelaryjny
     */
    public void setNrKancelaryjny(java.lang.String nrKancelaryjny) {
        this.nrKancelaryjny = nrKancelaryjny;
    }


    /**
     * Gets the barcode value for this SprawaWychodzaca2.
     * 
     * @return barcode
     */
    public java.lang.String getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this SprawaWychodzaca2.
     * 
     * @param barcode
     */
    public void setBarcode(java.lang.String barcode) {
        this.barcode = barcode;
    }


    /**
     * Gets the korespondent value for this SprawaWychodzaca2.
     * 
     * @return korespondent
     */
    public warszawa.um.signum.Korespondent2 getKorespondent() {
        return korespondent;
    }


    /**
     * Sets the korespondent value for this SprawaWychodzaca2.
     * 
     * @param korespondent
     */
    public void setKorespondent(warszawa.um.signum.Korespondent2 korespondent) {
        this.korespondent = korespondent;
    }


    /**
     * Gets the temat value for this SprawaWychodzaca2.
     * 
     * @return temat
     */
    public java.lang.String getTemat() {
        return temat;
    }


    /**
     * Sets the temat value for this SprawaWychodzaca2.
     * 
     * @param temat
     */
    public void setTemat(java.lang.String temat) {
        this.temat = temat;
    }


    /**
     * Gets the informacjaDodatkowa value for this SprawaWychodzaca2.
     * 
     * @return informacjaDodatkowa
     */
    public java.lang.String getInformacjaDodatkowa() {
        return informacjaDodatkowa;
    }


    /**
     * Sets the informacjaDodatkowa value for this SprawaWychodzaca2.
     * 
     * @param informacjaDodatkowa
     */
    public void setInformacjaDodatkowa(java.lang.String informacjaDodatkowa) {
        this.informacjaDodatkowa = informacjaDodatkowa;
    }


    /**
     * Gets the spr_id value for this SprawaWychodzaca2.
     * 
     * @return spr_id
     */
    public java.lang.String getSpr_id() {
        return spr_id;
    }


    /**
     * Sets the spr_id value for this SprawaWychodzaca2.
     * 
     * @param spr_id
     */
    public void setSpr_id(java.lang.String spr_id) {
        this.spr_id = spr_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SprawaWychodzaca2)) return false;
        SprawaWychodzaca2 other = (SprawaWychodzaca2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.znakSprawy==null && other.getZnakSprawy()==null) || 
             (this.znakSprawy!=null &&
              this.znakSprawy.equals(other.getZnakSprawy()))) &&
            ((this.nrKancelaryjny==null && other.getNrKancelaryjny()==null) || 
             (this.nrKancelaryjny!=null &&
              this.nrKancelaryjny.equals(other.getNrKancelaryjny()))) &&
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              this.barcode.equals(other.getBarcode()))) &&
            ((this.korespondent==null && other.getKorespondent()==null) || 
             (this.korespondent!=null &&
              this.korespondent.equals(other.getKorespondent()))) &&
            ((this.temat==null && other.getTemat()==null) || 
             (this.temat!=null &&
              this.temat.equals(other.getTemat()))) &&
            ((this.informacjaDodatkowa==null && other.getInformacjaDodatkowa()==null) || 
             (this.informacjaDodatkowa!=null &&
              this.informacjaDodatkowa.equals(other.getInformacjaDodatkowa()))) &&
            ((this.spr_id==null && other.getSpr_id()==null) || 
             (this.spr_id!=null &&
              this.spr_id.equals(other.getSpr_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getZnakSprawy() != null) {
            _hashCode += getZnakSprawy().hashCode();
        }
        if (getNrKancelaryjny() != null) {
            _hashCode += getNrKancelaryjny().hashCode();
        }
        if (getBarcode() != null) {
            _hashCode += getBarcode().hashCode();
        }
        if (getKorespondent() != null) {
            _hashCode += getKorespondent().hashCode();
        }
        if (getTemat() != null) {
            _hashCode += getTemat().hashCode();
        }
        if (getInformacjaDodatkowa() != null) {
            _hashCode += getInformacjaDodatkowa().hashCode();
        }
        if (getSpr_id() != null) {
            _hashCode += getSpr_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SprawaWychodzaca2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "SprawaWychodzaca2"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("spr_id");
        attrField.setXmlName(new javax.xml.namespace.QName("", "spr_id"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("znakSprawy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "znakSprawy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nrKancelaryjny");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "nrKancelaryjny"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("korespondent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "korespondent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Korespondent2"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temat");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "temat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informacjaDodatkowa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "informacjaDodatkowa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
