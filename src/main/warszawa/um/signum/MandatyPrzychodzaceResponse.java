/**
 * MandatyPrzychodzaceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class MandatyPrzychodzaceResponse  implements java.io.Serializable {
    private java.lang.String mandatyPrzychodzaceResult;

    public MandatyPrzychodzaceResponse() {
    }

    public MandatyPrzychodzaceResponse(
           java.lang.String mandatyPrzychodzaceResult) {
           this.mandatyPrzychodzaceResult = mandatyPrzychodzaceResult;
    }


    /**
     * Gets the mandatyPrzychodzaceResult value for this MandatyPrzychodzaceResponse.
     * 
     * @return mandatyPrzychodzaceResult
     */
    public java.lang.String getMandatyPrzychodzaceResult() {
        return mandatyPrzychodzaceResult;
    }


    /**
     * Sets the mandatyPrzychodzaceResult value for this MandatyPrzychodzaceResponse.
     * 
     * @param mandatyPrzychodzaceResult
     */
    public void setMandatyPrzychodzaceResult(java.lang.String mandatyPrzychodzaceResult) {
        this.mandatyPrzychodzaceResult = mandatyPrzychodzaceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof MandatyPrzychodzaceResponse)) return false;
        MandatyPrzychodzaceResponse other = (MandatyPrzychodzaceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.mandatyPrzychodzaceResult==null && other.getMandatyPrzychodzaceResult()==null) || 
             (this.mandatyPrzychodzaceResult!=null &&
              this.mandatyPrzychodzaceResult.equals(other.getMandatyPrzychodzaceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMandatyPrzychodzaceResult() != null) {
            _hashCode += getMandatyPrzychodzaceResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MandatyPrzychodzaceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">MandatyPrzychodzaceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mandatyPrzychodzaceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "MandatyPrzychodzaceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
