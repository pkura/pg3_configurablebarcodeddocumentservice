/**
 * TSprawy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class TSprawy  implements java.io.Serializable {
    private warszawa.um.signum.Sprawa[] sprawy;

    public TSprawy() {
    }

    public TSprawy(
           warszawa.um.signum.Sprawa[] sprawy) {
           this.sprawy = sprawy;
    }


    /**
     * Gets the sprawy value for this TSprawy.
     * 
     * @return sprawy
     */
    public warszawa.um.signum.Sprawa[] getSprawy() {
        return sprawy;
    }


    /**
     * Sets the sprawy value for this TSprawy.
     * 
     * @param sprawy
     */
    public void setSprawy(warszawa.um.signum.Sprawa[] sprawy) {
        this.sprawy = sprawy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TSprawy)) return false;
        TSprawy other = (TSprawy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sprawy==null && other.getSprawy()==null) || 
             (this.sprawy!=null &&
              java.util.Arrays.equals(this.sprawy, other.getSprawy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSprawy() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSprawy());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSprawy(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TSprawy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "TSprawy"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sprawy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawa"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawa"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
