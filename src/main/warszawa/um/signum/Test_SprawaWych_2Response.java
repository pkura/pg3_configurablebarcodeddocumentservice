/**
 * Test_SprawaWych_2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class Test_SprawaWych_2Response  implements java.io.Serializable {
    private warszawa.um.signum.SprawaWychodzaca2 sprawa2;

    public Test_SprawaWych_2Response() {
    }

    public Test_SprawaWych_2Response(
           warszawa.um.signum.SprawaWychodzaca2 sprawa2) {
           this.sprawa2 = sprawa2;
    }


    /**
     * Gets the sprawa2 value for this Test_SprawaWych_2Response.
     * 
     * @return sprawa2
     */
    public warszawa.um.signum.SprawaWychodzaca2 getSprawa2() {
        return sprawa2;
    }


    /**
     * Sets the sprawa2 value for this Test_SprawaWych_2Response.
     * 
     * @param sprawa2
     */
    public void setSprawa2(warszawa.um.signum.SprawaWychodzaca2 sprawa2) {
        this.sprawa2 = sprawa2;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Test_SprawaWych_2Response)) return false;
        Test_SprawaWych_2Response other = (Test_SprawaWych_2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sprawa2==null && other.getSprawa2()==null) || 
             (this.sprawa2!=null &&
              this.sprawa2.equals(other.getSprawa2())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSprawa2() != null) {
            _hashCode += getSprawa2().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Test_SprawaWych_2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">test_SprawaWych_2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sprawa2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "sprawa2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "SprawaWychodzaca2"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
