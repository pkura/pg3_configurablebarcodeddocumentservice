/**
 * GetIloscSprawPrzychodzacych.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetIloscSprawPrzychodzacych  implements java.io.Serializable {
    private java.lang.String urz_id;

    private java.lang.String kow_id;

    public GetIloscSprawPrzychodzacych() {
    }

    public GetIloscSprawPrzychodzacych(
           java.lang.String urz_id,
           java.lang.String kow_id) {
           this.urz_id = urz_id;
           this.kow_id = kow_id;
    }


    /**
     * Gets the urz_id value for this GetIloscSprawPrzychodzacych.
     * 
     * @return urz_id
     */
    public java.lang.String getUrz_id() {
        return urz_id;
    }


    /**
     * Sets the urz_id value for this GetIloscSprawPrzychodzacych.
     * 
     * @param urz_id
     */
    public void setUrz_id(java.lang.String urz_id) {
        this.urz_id = urz_id;
    }


    /**
     * Gets the kow_id value for this GetIloscSprawPrzychodzacych.
     * 
     * @return kow_id
     */
    public java.lang.String getKow_id() {
        return kow_id;
    }


    /**
     * Sets the kow_id value for this GetIloscSprawPrzychodzacych.
     * 
     * @param kow_id
     */
    public void setKow_id(java.lang.String kow_id) {
        this.kow_id = kow_id;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIloscSprawPrzychodzacych)) return false;
        GetIloscSprawPrzychodzacych other = (GetIloscSprawPrzychodzacych) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.urz_id==null && other.getUrz_id()==null) || 
             (this.urz_id!=null &&
              this.urz_id.equals(other.getUrz_id()))) &&
            ((this.kow_id==null && other.getKow_id()==null) || 
             (this.kow_id!=null &&
              this.kow_id.equals(other.getKow_id())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUrz_id() != null) {
            _hashCode += getUrz_id().hashCode();
        }
        if (getKow_id() != null) {
            _hashCode += getKow_id().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetIloscSprawPrzychodzacych.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetIloscSprawPrzychodzacych"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urz_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "urz_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kow_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kow_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
