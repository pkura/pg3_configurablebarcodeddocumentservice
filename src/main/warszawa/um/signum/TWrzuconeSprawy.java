/**
 * TWrzuconeSprawy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class TWrzuconeSprawy  implements java.io.Serializable {
    private java.lang.String reuslt;

    private java.lang.Object[] znakiSpraw;

    public TWrzuconeSprawy() {
    }

    public TWrzuconeSprawy(
           java.lang.String reuslt,
           java.lang.Object[] znakiSpraw) {
           this.reuslt = reuslt;
           this.znakiSpraw = znakiSpraw;
    }


    /**
     * Gets the reuslt value for this TWrzuconeSprawy.
     * 
     * @return reuslt
     */
    public java.lang.String getReuslt() {
        return reuslt;
    }


    /**
     * Sets the reuslt value for this TWrzuconeSprawy.
     * 
     * @param reuslt
     */
    public void setReuslt(java.lang.String reuslt) {
        this.reuslt = reuslt;
    }


    /**
     * Gets the znakiSpraw value for this TWrzuconeSprawy.
     * 
     * @return znakiSpraw
     */
    public java.lang.Object[] getZnakiSpraw() {
        return znakiSpraw;
    }


    /**
     * Sets the znakiSpraw value for this TWrzuconeSprawy.
     * 
     * @param znakiSpraw
     */
    public void setZnakiSpraw(java.lang.Object[] znakiSpraw) {
        this.znakiSpraw = znakiSpraw;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TWrzuconeSprawy)) return false;
        TWrzuconeSprawy other = (TWrzuconeSprawy) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.reuslt==null && other.getReuslt()==null) || 
             (this.reuslt!=null &&
              this.reuslt.equals(other.getReuslt()))) &&
            ((this.znakiSpraw==null && other.getZnakiSpraw()==null) || 
             (this.znakiSpraw!=null &&
              java.util.Arrays.equals(this.znakiSpraw, other.getZnakiSpraw())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReuslt() != null) {
            _hashCode += getReuslt().hashCode();
        }
        if (getZnakiSpraw() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getZnakiSpraw());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getZnakiSpraw(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TWrzuconeSprawy.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "TWrzuconeSprawy"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reuslt");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "reuslt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("znakiSpraw");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "ZnakiSpraw"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "anyType"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
