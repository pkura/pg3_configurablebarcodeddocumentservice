/**
 * GetSprawyWRealizacjiRWA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetSprawyWRealizacjiRWA  implements java.io.Serializable {
    private java.lang.String urz_id;

    private java.lang.String rwa;

    private java.lang.String pis_dataWplywu;

    private java.lang.String login;

    private java.lang.String haslo;

    public GetSprawyWRealizacjiRWA() {
    }

    public GetSprawyWRealizacjiRWA(
           java.lang.String urz_id,
           java.lang.String rwa,
           java.lang.String pis_dataWplywu,
           java.lang.String login,
           java.lang.String haslo) {
           this.urz_id = urz_id;
           this.rwa = rwa;
           this.pis_dataWplywu = pis_dataWplywu;
           this.login = login;
           this.haslo = haslo;
    }


    /**
     * Gets the urz_id value for this GetSprawyWRealizacjiRWA.
     * 
     * @return urz_id
     */
    public java.lang.String getUrz_id() {
        return urz_id;
    }


    /**
     * Sets the urz_id value for this GetSprawyWRealizacjiRWA.
     * 
     * @param urz_id
     */
    public void setUrz_id(java.lang.String urz_id) {
        this.urz_id = urz_id;
    }


    /**
     * Gets the rwa value for this GetSprawyWRealizacjiRWA.
     * 
     * @return rwa
     */
    public java.lang.String getRwa() {
        return rwa;
    }


    /**
     * Sets the rwa value for this GetSprawyWRealizacjiRWA.
     * 
     * @param rwa
     */
    public void setRwa(java.lang.String rwa) {
        this.rwa = rwa;
    }


    /**
     * Gets the pis_dataWplywu value for this GetSprawyWRealizacjiRWA.
     * 
     * @return pis_dataWplywu
     */
    public java.lang.String getPis_dataWplywu() {
        return pis_dataWplywu;
    }


    /**
     * Sets the pis_dataWplywu value for this GetSprawyWRealizacjiRWA.
     * 
     * @param pis_dataWplywu
     */
    public void setPis_dataWplywu(java.lang.String pis_dataWplywu) {
        this.pis_dataWplywu = pis_dataWplywu;
    }


    /**
     * Gets the login value for this GetSprawyWRealizacjiRWA.
     * 
     * @return login
     */
    public java.lang.String getLogin() {
        return login;
    }


    /**
     * Sets the login value for this GetSprawyWRealizacjiRWA.
     * 
     * @param login
     */
    public void setLogin(java.lang.String login) {
        this.login = login;
    }


    /**
     * Gets the haslo value for this GetSprawyWRealizacjiRWA.
     * 
     * @return haslo
     */
    public java.lang.String getHaslo() {
        return haslo;
    }


    /**
     * Sets the haslo value for this GetSprawyWRealizacjiRWA.
     * 
     * @param haslo
     */
    public void setHaslo(java.lang.String haslo) {
        this.haslo = haslo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSprawyWRealizacjiRWA)) return false;
        GetSprawyWRealizacjiRWA other = (GetSprawyWRealizacjiRWA) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.urz_id==null && other.getUrz_id()==null) || 
             (this.urz_id!=null &&
              this.urz_id.equals(other.getUrz_id()))) &&
            ((this.rwa==null && other.getRwa()==null) || 
             (this.rwa!=null &&
              this.rwa.equals(other.getRwa()))) &&
            ((this.pis_dataWplywu==null && other.getPis_dataWplywu()==null) || 
             (this.pis_dataWplywu!=null &&
              this.pis_dataWplywu.equals(other.getPis_dataWplywu()))) &&
            ((this.login==null && other.getLogin()==null) || 
             (this.login!=null &&
              this.login.equals(other.getLogin()))) &&
            ((this.haslo==null && other.getHaslo()==null) || 
             (this.haslo!=null &&
              this.haslo.equals(other.getHaslo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUrz_id() != null) {
            _hashCode += getUrz_id().hashCode();
        }
        if (getRwa() != null) {
            _hashCode += getRwa().hashCode();
        }
        if (getPis_dataWplywu() != null) {
            _hashCode += getPis_dataWplywu().hashCode();
        }
        if (getLogin() != null) {
            _hashCode += getLogin().hashCode();
        }
        if (getHaslo() != null) {
            _hashCode += getHaslo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSprawyWRealizacjiRWA.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetSprawyWRealizacjiRWA"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urz_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "urz_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rwa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "rwa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pis_dataWplywu");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "pis_dataWplywu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("login");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "login"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("haslo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "haslo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
