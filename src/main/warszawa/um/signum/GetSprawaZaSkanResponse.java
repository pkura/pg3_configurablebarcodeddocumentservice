/**
 * GetSprawaZaSkanResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetSprawaZaSkanResponse  implements java.io.Serializable {
    private warszawa.um.signum.Sprawa getSprawaZaSkanResult;

    public GetSprawaZaSkanResponse() {
    }

    public GetSprawaZaSkanResponse(
           warszawa.um.signum.Sprawa getSprawaZaSkanResult) {
           this.getSprawaZaSkanResult = getSprawaZaSkanResult;
    }


    /**
     * Gets the getSprawaZaSkanResult value for this GetSprawaZaSkanResponse.
     * 
     * @return getSprawaZaSkanResult
     */
    public warszawa.um.signum.Sprawa getGetSprawaZaSkanResult() {
        return getSprawaZaSkanResult;
    }


    /**
     * Sets the getSprawaZaSkanResult value for this GetSprawaZaSkanResponse.
     * 
     * @param getSprawaZaSkanResult
     */
    public void setGetSprawaZaSkanResult(warszawa.um.signum.Sprawa getSprawaZaSkanResult) {
        this.getSprawaZaSkanResult = getSprawaZaSkanResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSprawaZaSkanResponse)) return false;
        GetSprawaZaSkanResponse other = (GetSprawaZaSkanResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getSprawaZaSkanResult==null && other.getGetSprawaZaSkanResult()==null) || 
             (this.getSprawaZaSkanResult!=null &&
              this.getSprawaZaSkanResult.equals(other.getGetSprawaZaSkanResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetSprawaZaSkanResult() != null) {
            _hashCode += getGetSprawaZaSkanResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSprawaZaSkanResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetSprawaZaSkanResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getSprawaZaSkanResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "GetSprawaZaSkanResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "Sprawa"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
