/**
 * GetIdSprawPrzychodzacych.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetIdSprawPrzychodzacych  implements java.io.Serializable {
    private java.lang.String urz_id;

    private java.lang.String kow_id;

    private java.lang.String wtk_dataUtworzenia_od;

    public GetIdSprawPrzychodzacych() {
    }

    public GetIdSprawPrzychodzacych(
           java.lang.String urz_id,
           java.lang.String kow_id,
           java.lang.String wtk_dataUtworzenia_od) {
           this.urz_id = urz_id;
           this.kow_id = kow_id;
           this.wtk_dataUtworzenia_od = wtk_dataUtworzenia_od;
    }


    /**
     * Gets the urz_id value for this GetIdSprawPrzychodzacych.
     * 
     * @return urz_id
     */
    public java.lang.String getUrz_id() {
        return urz_id;
    }


    /**
     * Sets the urz_id value for this GetIdSprawPrzychodzacych.
     * 
     * @param urz_id
     */
    public void setUrz_id(java.lang.String urz_id) {
        this.urz_id = urz_id;
    }


    /**
     * Gets the kow_id value for this GetIdSprawPrzychodzacych.
     * 
     * @return kow_id
     */
    public java.lang.String getKow_id() {
        return kow_id;
    }


    /**
     * Sets the kow_id value for this GetIdSprawPrzychodzacych.
     * 
     * @param kow_id
     */
    public void setKow_id(java.lang.String kow_id) {
        this.kow_id = kow_id;
    }


    /**
     * Gets the wtk_dataUtworzenia_od value for this GetIdSprawPrzychodzacych.
     * 
     * @return wtk_dataUtworzenia_od
     */
    public java.lang.String getWtk_dataUtworzenia_od() {
        return wtk_dataUtworzenia_od;
    }


    /**
     * Sets the wtk_dataUtworzenia_od value for this GetIdSprawPrzychodzacych.
     * 
     * @param wtk_dataUtworzenia_od
     */
    public void setWtk_dataUtworzenia_od(java.lang.String wtk_dataUtworzenia_od) {
        this.wtk_dataUtworzenia_od = wtk_dataUtworzenia_od;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetIdSprawPrzychodzacych)) return false;
        GetIdSprawPrzychodzacych other = (GetIdSprawPrzychodzacych) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.urz_id==null && other.getUrz_id()==null) || 
             (this.urz_id!=null &&
              this.urz_id.equals(other.getUrz_id()))) &&
            ((this.kow_id==null && other.getKow_id()==null) || 
             (this.kow_id!=null &&
              this.kow_id.equals(other.getKow_id()))) &&
            ((this.wtk_dataUtworzenia_od==null && other.getWtk_dataUtworzenia_od()==null) || 
             (this.wtk_dataUtworzenia_od!=null &&
              this.wtk_dataUtworzenia_od.equals(other.getWtk_dataUtworzenia_od())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUrz_id() != null) {
            _hashCode += getUrz_id().hashCode();
        }
        if (getKow_id() != null) {
            _hashCode += getKow_id().hashCode();
        }
        if (getWtk_dataUtworzenia_od() != null) {
            _hashCode += getWtk_dataUtworzenia_od().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetIdSprawPrzychodzacych.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetIdSprawPrzychodzacych"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urz_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "urz_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kow_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "kow_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("wtk_dataUtworzenia_od");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "wtk_dataUtworzenia_od"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
