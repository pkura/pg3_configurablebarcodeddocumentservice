/**
 * TestttResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class TestttResponse  implements java.io.Serializable {
    private warszawa.um.signum.Aaa rrrrot;

    public TestttResponse() {
    }

    public TestttResponse(
           warszawa.um.signum.Aaa rrrrot) {
           this.rrrrot = rrrrot;
    }


    /**
     * Gets the rrrrot value for this TestttResponse.
     * 
     * @return rrrrot
     */
    public warszawa.um.signum.Aaa getRrrrot() {
        return rrrrot;
    }


    /**
     * Sets the rrrrot value for this TestttResponse.
     * 
     * @param rrrrot
     */
    public void setRrrrot(warszawa.um.signum.Aaa rrrrot) {
        this.rrrrot = rrrrot;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TestttResponse)) return false;
        TestttResponse other = (TestttResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rrrrot==null && other.getRrrrot()==null) || 
             (this.rrrrot!=null &&
              this.rrrrot.equals(other.getRrrrot())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRrrrot() != null) {
            _hashCode += getRrrrot().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TestttResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">TestttResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rrrrot");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "rrrrot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", "aaa"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
