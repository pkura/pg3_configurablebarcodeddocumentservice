/**
 * GetSprawaZaSkan.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package warszawa.um.signum;

public class GetSprawaZaSkan  implements java.io.Serializable {
    private java.lang.String barcode;

    private java.lang.String skanLink;

    private java.lang.String login;

    private java.lang.String haslo;

    public GetSprawaZaSkan() {
    }

    public GetSprawaZaSkan(
           java.lang.String barcode,
           java.lang.String skanLink,
           java.lang.String login,
           java.lang.String haslo) {
           this.barcode = barcode;
           this.skanLink = skanLink;
           this.login = login;
           this.haslo = haslo;
    }


    /**
     * Gets the barcode value for this GetSprawaZaSkan.
     * 
     * @return barcode
     */
    public java.lang.String getBarcode() {
        return barcode;
    }


    /**
     * Sets the barcode value for this GetSprawaZaSkan.
     * 
     * @param barcode
     */
    public void setBarcode(java.lang.String barcode) {
        this.barcode = barcode;
    }


    /**
     * Gets the skanLink value for this GetSprawaZaSkan.
     * 
     * @return skanLink
     */
    public java.lang.String getSkanLink() {
        return skanLink;
    }


    /**
     * Sets the skanLink value for this GetSprawaZaSkan.
     * 
     * @param skanLink
     */
    public void setSkanLink(java.lang.String skanLink) {
        this.skanLink = skanLink;
    }


    /**
     * Gets the login value for this GetSprawaZaSkan.
     * 
     * @return login
     */
    public java.lang.String getLogin() {
        return login;
    }


    /**
     * Sets the login value for this GetSprawaZaSkan.
     * 
     * @param login
     */
    public void setLogin(java.lang.String login) {
        this.login = login;
    }


    /**
     * Gets the haslo value for this GetSprawaZaSkan.
     * 
     * @return haslo
     */
    public java.lang.String getHaslo() {
        return haslo;
    }


    /**
     * Sets the haslo value for this GetSprawaZaSkan.
     * 
     * @param haslo
     */
    public void setHaslo(java.lang.String haslo) {
        this.haslo = haslo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetSprawaZaSkan)) return false;
        GetSprawaZaSkan other = (GetSprawaZaSkan) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.barcode==null && other.getBarcode()==null) || 
             (this.barcode!=null &&
              this.barcode.equals(other.getBarcode()))) &&
            ((this.skanLink==null && other.getSkanLink()==null) || 
             (this.skanLink!=null &&
              this.skanLink.equals(other.getSkanLink()))) &&
            ((this.login==null && other.getLogin()==null) || 
             (this.login!=null &&
              this.login.equals(other.getLogin()))) &&
            ((this.haslo==null && other.getHaslo()==null) || 
             (this.haslo!=null &&
              this.haslo.equals(other.getHaslo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBarcode() != null) {
            _hashCode += getBarcode().hashCode();
        }
        if (getSkanLink() != null) {
            _hashCode += getSkanLink().hashCode();
        }
        if (getLogin() != null) {
            _hashCode += getLogin().hashCode();
        }
        if (getHaslo() != null) {
            _hashCode += getHaslo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetSprawaZaSkan.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://signum.um.warszawa/", ">GetSprawaZaSkan"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("barcode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "barcode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skanLink");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "skanLink"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("login");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "login"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("haslo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://signum.um.warszawa/", "haslo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
