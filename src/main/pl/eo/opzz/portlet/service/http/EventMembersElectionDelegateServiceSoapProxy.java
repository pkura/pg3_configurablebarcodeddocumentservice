package pl.eo.opzz.portlet.service.http;

public class EventMembersElectionDelegateServiceSoapProxy implements pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap {
  private String _endpoint = null;
  private pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap eventMembersElectionDelegateServiceSoap = null;
  
  public EventMembersElectionDelegateServiceSoapProxy() {
    _initEventMembersElectionDelegateServiceSoapProxy();
  }
  
  public EventMembersElectionDelegateServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initEventMembersElectionDelegateServiceSoapProxy();
  }
  
  private void _initEventMembersElectionDelegateServiceSoapProxy() {
    try {
      eventMembersElectionDelegateServiceSoap = (new pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoapServiceLocator()).getPlugin_EO_Calendar_EventMembersElectionDelegateService();
      if (eventMembersElectionDelegateServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eventMembersElectionDelegateServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eventMembersElectionDelegateServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eventMembersElectionDelegateServiceSoap != null)
      ((javax.xml.rpc.Stub)eventMembersElectionDelegateServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap getEventMembersElectionDelegateServiceSoap() {
    if (eventMembersElectionDelegateServiceSoap == null)
      _initEventMembersElectionDelegateServiceSoapProxy();
    return eventMembersElectionDelegateServiceSoap;
  }
  
  public pl.eo.opzz.portlet.service.dto.WsResponseDto addDelegates(pl.eo.opzz.portlet.service.dto.AddDelegatesDto addDelegates) throws java.rmi.RemoteException{
    if (eventMembersElectionDelegateServiceSoap == null)
      _initEventMembersElectionDelegateServiceSoapProxy();
    return eventMembersElectionDelegateServiceSoap.addDelegates(addDelegates);
  }
  
  public pl.eo.opzz.portlet.service.dto.GetOrganizationsResponseDto getOrganizationsForUser(pl.eo.opzz.portlet.service.dto.GetOrganizationsDto input) throws java.rmi.RemoteException{
    if (eventMembersElectionDelegateServiceSoap == null)
      _initEventMembersElectionDelegateServiceSoapProxy();
    return eventMembersElectionDelegateServiceSoap.getOrganizationsForUser(input);
  }
  
  public pl.eo.opzz.portlet.service.dto.WsResponseDto removeDelegates(pl.eo.opzz.portlet.service.dto.RemoveDelegatesDto removeDelegates) throws java.rmi.RemoteException{
    if (eventMembersElectionDelegateServiceSoap == null)
      _initEventMembersElectionDelegateServiceSoapProxy();
    return eventMembersElectionDelegateServiceSoap.removeDelegates(removeDelegates);
  }
  
  
}