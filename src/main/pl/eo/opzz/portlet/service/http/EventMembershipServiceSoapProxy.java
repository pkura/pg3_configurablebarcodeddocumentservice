package pl.eo.opzz.portlet.service.http;

public class EventMembershipServiceSoapProxy implements pl.eo.opzz.portlet.service.http.EventMembershipServiceSoap {
  private String _endpoint = null;
  private pl.eo.opzz.portlet.service.http.EventMembershipServiceSoap eventMembershipServiceSoap = null;
  
  public EventMembershipServiceSoapProxy() {
    _initEventMembershipServiceSoapProxy();
  }
  
  public EventMembershipServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initEventMembershipServiceSoapProxy();
  }
  
  private void _initEventMembershipServiceSoapProxy() {
    try {
      eventMembershipServiceSoap = (new pl.eo.opzz.portlet.service.http.EventMembershipServiceSoapServiceLocator()).getPlugin_EO_Calendar_EventMembershipService();
      if (eventMembershipServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eventMembershipServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eventMembershipServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eventMembershipServiceSoap != null)
      ((javax.xml.rpc.Stub)eventMembershipServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.eo.opzz.portlet.service.http.EventMembershipServiceSoap getEventMembershipServiceSoap() {
    if (eventMembershipServiceSoap == null)
      _initEventMembershipServiceSoapProxy();
    return eventMembershipServiceSoap;
  }
  
  public pl.eo.opzz.portlet.service.dto.AddCalEventResponseDto addMembersElectionEvent(pl.eo.opzz.portlet.service.dto.CalEventDto calEvent) throws java.rmi.RemoteException{
    if (eventMembershipServiceSoap == null)
      _initEventMembershipServiceSoapProxy();
    return eventMembershipServiceSoap.addMembersElectionEvent(calEvent);
  }
  
  public pl.eo.opzz.portlet.service.dto.WsResponseDto updateMembersElectionEvent(pl.eo.opzz.portlet.service.dto.CalEventDto calEvent) throws java.rmi.RemoteException{
    if (eventMembershipServiceSoap == null)
      _initEventMembershipServiceSoapProxy();
    return eventMembershipServiceSoap.updateMembersElectionEvent(calEvent);
  }
  
  
}