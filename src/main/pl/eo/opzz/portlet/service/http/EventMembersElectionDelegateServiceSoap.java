/**
 * EventMembersElectionDelegateServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public interface EventMembersElectionDelegateServiceSoap extends java.rmi.Remote {
    public pl.eo.opzz.portlet.service.dto.WsResponseDto addDelegates(pl.eo.opzz.portlet.service.dto.AddDelegatesDto addDelegates) throws java.rmi.RemoteException;
    public pl.eo.opzz.portlet.service.dto.GetOrganizationsResponseDto getOrganizationsForUser(pl.eo.opzz.portlet.service.dto.GetOrganizationsDto input) throws java.rmi.RemoteException;
    public pl.eo.opzz.portlet.service.dto.WsResponseDto removeDelegates(pl.eo.opzz.portlet.service.dto.RemoveDelegatesDto removeDelegates) throws java.rmi.RemoteException;
}
