/**
 * EventMembersElectionDelegateServiceSoapServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public class EventMembersElectionDelegateServiceSoapServiceLocator extends org.apache.axis.client.Service implements pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoapService {

    public EventMembersElectionDelegateServiceSoapServiceLocator() {
    }


    public EventMembersElectionDelegateServiceSoapServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EventMembersElectionDelegateServiceSoapServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Plugin_EO_Calendar_EventMembersElectionDelegateService
    private java.lang.String Plugin_EO_Calendar_EventMembersElectionDelegateService_address = "http://opzz-dev.demo.eo.pl/opzz-portal-calendar-events-portlet/axis/Plugin_EO_Calendar_EventMembersElectionDelegateService";

    public java.lang.String getPlugin_EO_Calendar_EventMembersElectionDelegateServiceAddress() {
        return Plugin_EO_Calendar_EventMembersElectionDelegateService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Plugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName = "Plugin_EO_Calendar_EventMembersElectionDelegateService";

    public java.lang.String getPlugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName() {
        return Plugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName;
    }

    public void setPlugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName(java.lang.String name) {
        Plugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName = name;
    }

    public pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap getPlugin_EO_Calendar_EventMembersElectionDelegateService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Plugin_EO_Calendar_EventMembersElectionDelegateService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPlugin_EO_Calendar_EventMembersElectionDelegateService(endpoint);
    }

    public pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap getPlugin_EO_Calendar_EventMembersElectionDelegateService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            pl.eo.opzz.portlet.service.http.Plugin_EO_Calendar_EventMembersElectionDelegateServiceSoapBindingStub _stub = new pl.eo.opzz.portlet.service.http.Plugin_EO_Calendar_EventMembersElectionDelegateServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getPlugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPlugin_EO_Calendar_EventMembersElectionDelegateServiceEndpointAddress(java.lang.String address) {
        Plugin_EO_Calendar_EventMembersElectionDelegateService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                pl.eo.opzz.portlet.service.http.Plugin_EO_Calendar_EventMembersElectionDelegateServiceSoapBindingStub _stub = new pl.eo.opzz.portlet.service.http.Plugin_EO_Calendar_EventMembersElectionDelegateServiceSoapBindingStub(new java.net.URL(Plugin_EO_Calendar_EventMembersElectionDelegateService_address), this);
                _stub.setPortName(getPlugin_EO_Calendar_EventMembersElectionDelegateServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("Plugin_EO_Calendar_EventMembersElectionDelegateService".equals(inputPortName)) {
            return getPlugin_EO_Calendar_EventMembersElectionDelegateService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:http.service.portlet.opzz.eo.pl", "EventMembersElectionDelegateServiceSoapService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:http.service.portlet.opzz.eo.pl", "Plugin_EO_Calendar_EventMembersElectionDelegateService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Plugin_EO_Calendar_EventMembersElectionDelegateService".equals(portName)) {
            setPlugin_EO_Calendar_EventMembersElectionDelegateServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
