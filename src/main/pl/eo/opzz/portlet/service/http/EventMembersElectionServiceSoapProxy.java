package pl.eo.opzz.portlet.service.http;

public class EventMembersElectionServiceSoapProxy implements pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoap {
  private String _endpoint = null;
  private pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoap eventMembersElectionServiceSoap = null;
  
  public EventMembersElectionServiceSoapProxy() {
    _initEventMembersElectionServiceSoapProxy();
  }
  
  public EventMembersElectionServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initEventMembersElectionServiceSoapProxy();
  }
  
  private void _initEventMembersElectionServiceSoapProxy() {
    try {
      eventMembersElectionServiceSoap = (new pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoapServiceLocator()).getPlugin_EO_Calendar_EventMembersElectionService();
      if (eventMembersElectionServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)eventMembersElectionServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)eventMembersElectionServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (eventMembersElectionServiceSoap != null)
      ((javax.xml.rpc.Stub)eventMembersElectionServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoap getEventMembersElectionServiceSoap() {
    if (eventMembersElectionServiceSoap == null)
      _initEventMembersElectionServiceSoapProxy();
    return eventMembersElectionServiceSoap;
  }
  
  public pl.eo.opzz.portlet.service.dto.WsResponseDto addCandidates(pl.eo.opzz.portlet.service.dto.AddCandidatesDto addCandidatesDto) throws java.rmi.RemoteException{
    if (eventMembersElectionServiceSoap == null)
      _initEventMembersElectionServiceSoapProxy();
    return eventMembersElectionServiceSoap.addCandidates(addCandidatesDto);
  }
  
  public pl.eo.opzz.portlet.service.dto.WsResponseDto removeCandidates(pl.eo.opzz.portlet.service.dto.RemoveCandidatesDto removeCandidatesDto) throws java.rmi.RemoteException{
    if (eventMembersElectionServiceSoap == null)
      _initEventMembersElectionServiceSoapProxy();
    return eventMembersElectionServiceSoap.removeCandidates(removeCandidatesDto);
  }
  
  
}