/**
 * EventMembersElectionServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public interface EventMembersElectionServiceSoap extends java.rmi.Remote {
    public pl.eo.opzz.portlet.service.dto.WsResponseDto addCandidates(pl.eo.opzz.portlet.service.dto.AddCandidatesDto addCandidatesDto) throws java.rmi.RemoteException;
    public pl.eo.opzz.portlet.service.dto.WsResponseDto removeCandidates(pl.eo.opzz.portlet.service.dto.RemoveCandidatesDto removeCandidatesDto) throws java.rmi.RemoteException;
}
