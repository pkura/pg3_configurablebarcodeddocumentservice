/**
 * EventMembersElectionDelegateServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public interface EventMembersElectionDelegateServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getPlugin_EO_Calendar_EventMembersElectionDelegateServiceAddress();

    public pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap getPlugin_EO_Calendar_EventMembersElectionDelegateService() throws javax.xml.rpc.ServiceException;

    public pl.eo.opzz.portlet.service.http.EventMembersElectionDelegateServiceSoap getPlugin_EO_Calendar_EventMembersElectionDelegateService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
