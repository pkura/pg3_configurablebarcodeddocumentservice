/**
 * EventMembershipServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public interface EventMembershipServiceSoap extends java.rmi.Remote {
    public pl.eo.opzz.portlet.service.dto.AddCalEventResponseDto addMembersElectionEvent(pl.eo.opzz.portlet.service.dto.CalEventDto calEvent) throws java.rmi.RemoteException;
    public pl.eo.opzz.portlet.service.dto.WsResponseDto updateMembersElectionEvent(pl.eo.opzz.portlet.service.dto.CalEventDto calEvent) throws java.rmi.RemoteException;
}
