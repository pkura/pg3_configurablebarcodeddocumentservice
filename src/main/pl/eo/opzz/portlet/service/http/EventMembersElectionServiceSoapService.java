/**
 * EventMembersElectionServiceSoapService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.http;

public interface EventMembersElectionServiceSoapService extends javax.xml.rpc.Service {
    public java.lang.String getPlugin_EO_Calendar_EventMembersElectionServiceAddress();

    public pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoap getPlugin_EO_Calendar_EventMembersElectionService() throws javax.xml.rpc.ServiceException;

    public pl.eo.opzz.portlet.service.http.EventMembersElectionServiceSoap getPlugin_EO_Calendar_EventMembersElectionService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
