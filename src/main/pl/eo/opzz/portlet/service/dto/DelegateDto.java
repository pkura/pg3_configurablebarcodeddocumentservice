/**
 * DelegateDto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.dto;

public class DelegateDto  implements java.io.Serializable {
    private java.lang.String delegate;

    private java.lang.Long organizationId;

    public DelegateDto() {
    }

    public DelegateDto(
           java.lang.String delegate,
           java.lang.Long organizationId) {
           this.delegate = delegate;
           this.organizationId = organizationId;
    }


    /**
     * Gets the delegate value for this DelegateDto.
     * 
     * @return delegate
     */
    public java.lang.String getDelegate() {
        return delegate;
    }


    /**
     * Sets the delegate value for this DelegateDto.
     * 
     * @param delegate
     */
    public void setDelegate(java.lang.String delegate) {
        this.delegate = delegate;
    }


    /**
     * Gets the organizationId value for this DelegateDto.
     * 
     * @return organizationId
     */
    public java.lang.Long getOrganizationId() {
        return organizationId;
    }


    /**
     * Sets the organizationId value for this DelegateDto.
     * 
     * @param organizationId
     */
    public void setOrganizationId(java.lang.Long organizationId) {
        this.organizationId = organizationId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DelegateDto)) return false;
        DelegateDto other = (DelegateDto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.delegate==null && other.getDelegate()==null) || 
             (this.delegate!=null &&
              this.delegate.equals(other.getDelegate()))) &&
            ((this.organizationId==null && other.getOrganizationId()==null) || 
             (this.organizationId!=null &&
              this.organizationId.equals(other.getOrganizationId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDelegate() != null) {
            _hashCode += getDelegate().hashCode();
        }
        if (getOrganizationId() != null) {
            _hashCode += getOrganizationId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DelegateDto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://dto.service.portlet.opzz.eo.pl", "DelegateDto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delegate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "delegate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "organizationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
