/**
 * AddDelegatesDto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.dto;

public class AddDelegatesDto  implements java.io.Serializable {
    private pl.eo.opzz.portlet.service.dto.DelegateDto[] delegates;

    private java.lang.Long eventId;

    private java.lang.String user;

    public AddDelegatesDto() {
    }

    public AddDelegatesDto(
           pl.eo.opzz.portlet.service.dto.DelegateDto[] delegates,
           java.lang.Long eventId,
           java.lang.String user) {
           this.delegates = delegates;
           this.eventId = eventId;
           this.user = user;
    }


    /**
     * Gets the delegates value for this AddDelegatesDto.
     * 
     * @return delegates
     */
    public pl.eo.opzz.portlet.service.dto.DelegateDto[] getDelegates() {
        return delegates;
    }


    /**
     * Sets the delegates value for this AddDelegatesDto.
     * 
     * @param delegates
     */
    public void setDelegates(pl.eo.opzz.portlet.service.dto.DelegateDto[] delegates) {
        this.delegates = delegates;
    }


    /**
     * Gets the eventId value for this AddDelegatesDto.
     * 
     * @return eventId
     */
    public java.lang.Long getEventId() {
        return eventId;
    }


    /**
     * Sets the eventId value for this AddDelegatesDto.
     * 
     * @param eventId
     */
    public void setEventId(java.lang.Long eventId) {
        this.eventId = eventId;
    }


    /**
     * Gets the user value for this AddDelegatesDto.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this AddDelegatesDto.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddDelegatesDto)) return false;
        AddDelegatesDto other = (AddDelegatesDto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.delegates==null && other.getDelegates()==null) || 
             (this.delegates!=null &&
              java.util.Arrays.equals(this.delegates, other.getDelegates()))) &&
            ((this.eventId==null && other.getEventId()==null) || 
             (this.eventId!=null &&
              this.eventId.equals(other.getEventId()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDelegates() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDelegates());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDelegates(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEventId() != null) {
            _hashCode += getEventId().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddDelegatesDto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://dto.service.portlet.opzz.eo.pl", "AddDelegatesDto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delegates");
        elemField.setXmlName(new javax.xml.namespace.QName("", "delegates"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://dto.service.portlet.opzz.eo.pl", "DelegateDto"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eventId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
