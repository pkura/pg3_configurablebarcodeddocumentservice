/**
 * CalEventDto.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.eo.opzz.portlet.service.dto;

public class CalEventDto  implements java.io.Serializable {
    private java.lang.Boolean allDay;

    private java.lang.String description;

    private java.lang.Integer durationHour;

    private java.lang.Integer durationMinute;

    private java.util.Calendar endDate;

    private java.lang.Long eventId;

    private java.lang.String location;

    private java.util.Calendar startDate;

    private java.lang.String title;

    private java.lang.String user;

    public CalEventDto() {
    }

    public CalEventDto(
           java.lang.Boolean allDay,
           java.lang.String description,
           java.lang.Integer durationHour,
           java.lang.Integer durationMinute,
           java.util.Calendar endDate,
           java.lang.Long eventId,
           java.lang.String location,
           java.util.Calendar startDate,
           java.lang.String title,
           java.lang.String user) {
           this.allDay = allDay;
           this.description = description;
           this.durationHour = durationHour;
           this.durationMinute = durationMinute;
           this.endDate = endDate;
           this.eventId = eventId;
           this.location = location;
           this.startDate = startDate;
           this.title = title;
           this.user = user;
    }


    /**
     * Gets the allDay value for this CalEventDto.
     * 
     * @return allDay
     */
    public java.lang.Boolean getAllDay() {
        return allDay;
    }


    /**
     * Sets the allDay value for this CalEventDto.
     * 
     * @param allDay
     */
    public void setAllDay(java.lang.Boolean allDay) {
        this.allDay = allDay;
    }


    /**
     * Gets the description value for this CalEventDto.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this CalEventDto.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the durationHour value for this CalEventDto.
     * 
     * @return durationHour
     */
    public java.lang.Integer getDurationHour() {
        return durationHour;
    }


    /**
     * Sets the durationHour value for this CalEventDto.
     * 
     * @param durationHour
     */
    public void setDurationHour(java.lang.Integer durationHour) {
        this.durationHour = durationHour;
    }


    /**
     * Gets the durationMinute value for this CalEventDto.
     * 
     * @return durationMinute
     */
    public java.lang.Integer getDurationMinute() {
        return durationMinute;
    }


    /**
     * Sets the durationMinute value for this CalEventDto.
     * 
     * @param durationMinute
     */
    public void setDurationMinute(java.lang.Integer durationMinute) {
        this.durationMinute = durationMinute;
    }


    /**
     * Gets the endDate value for this CalEventDto.
     * 
     * @return endDate
     */
    public java.util.Calendar getEndDate() {
        return endDate;
    }


    /**
     * Sets the endDate value for this CalEventDto.
     * 
     * @param endDate
     */
    public void setEndDate(java.util.Calendar endDate) {
        this.endDate = endDate;
    }


    /**
     * Gets the eventId value for this CalEventDto.
     * 
     * @return eventId
     */
    public java.lang.Long getEventId() {
        return eventId;
    }


    /**
     * Sets the eventId value for this CalEventDto.
     * 
     * @param eventId
     */
    public void setEventId(java.lang.Long eventId) {
        this.eventId = eventId;
    }


    /**
     * Gets the location value for this CalEventDto.
     * 
     * @return location
     */
    public java.lang.String getLocation() {
        return location;
    }


    /**
     * Sets the location value for this CalEventDto.
     * 
     * @param location
     */
    public void setLocation(java.lang.String location) {
        this.location = location;
    }


    /**
     * Gets the startDate value for this CalEventDto.
     * 
     * @return startDate
     */
    public java.util.Calendar getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this CalEventDto.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Calendar startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the title value for this CalEventDto.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this CalEventDto.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the user value for this CalEventDto.
     * 
     * @return user
     */
    public java.lang.String getUser() {
        return user;
    }


    /**
     * Sets the user value for this CalEventDto.
     * 
     * @param user
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalEventDto)) return false;
        CalEventDto other = (CalEventDto) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.allDay==null && other.getAllDay()==null) || 
             (this.allDay!=null &&
              this.allDay.equals(other.getAllDay()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.durationHour==null && other.getDurationHour()==null) || 
             (this.durationHour!=null &&
              this.durationHour.equals(other.getDurationHour()))) &&
            ((this.durationMinute==null && other.getDurationMinute()==null) || 
             (this.durationMinute!=null &&
              this.durationMinute.equals(other.getDurationMinute()))) &&
            ((this.endDate==null && other.getEndDate()==null) || 
             (this.endDate!=null &&
              this.endDate.equals(other.getEndDate()))) &&
            ((this.eventId==null && other.getEventId()==null) || 
             (this.eventId!=null &&
              this.eventId.equals(other.getEventId()))) &&
            ((this.location==null && other.getLocation()==null) || 
             (this.location!=null &&
              this.location.equals(other.getLocation()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAllDay() != null) {
            _hashCode += getAllDay().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getDurationHour() != null) {
            _hashCode += getDurationHour().hashCode();
        }
        if (getDurationMinute() != null) {
            _hashCode += getDurationMinute().hashCode();
        }
        if (getEndDate() != null) {
            _hashCode += getEndDate().hashCode();
        }
        if (getEventId() != null) {
            _hashCode += getEventId().hashCode();
        }
        if (getLocation() != null) {
            _hashCode += getLocation().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalEventDto.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://dto.service.portlet.opzz.eo.pl", "CalEventDto"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allDay");
        elemField.setXmlName(new javax.xml.namespace.QName("", "allDay"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "boolean"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("durationHour");
        elemField.setXmlName(new javax.xml.namespace.QName("", "durationHour"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("durationMinute");
        elemField.setXmlName(new javax.xml.namespace.QName("", "durationMinute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "int"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eventId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "long"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location");
        elemField.setXmlName(new javax.xml.namespace.QName("", "location"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://schemas.xmlsoap.org/soap/encoding/", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
