/**
 * SMSObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.inteliso;

public class SMSObject  implements java.io.Serializable {
    private java.lang.String smsid;

    private java.lang.String from;

    private java.lang.String to;

    private java.lang.String txt;

    private java.lang.String delivdate;

    public SMSObject() {
    }

    public SMSObject(
           java.lang.String smsid,
           java.lang.String from,
           java.lang.String to,
           java.lang.String txt,
           java.lang.String delivdate) {
           this.smsid = smsid;
           this.from = from;
           this.to = to;
           this.txt = txt;
           this.delivdate = delivdate;
    }


    /**
     * Gets the smsid value for this SMSObject.
     * 
     * @return smsid
     */
    public java.lang.String getSmsid() {
        return smsid;
    }


    /**
     * Sets the smsid value for this SMSObject.
     * 
     * @param smsid
     */
    public void setSmsid(java.lang.String smsid) {
        this.smsid = smsid;
    }


    /**
     * Gets the from value for this SMSObject.
     * 
     * @return from
     */
    public java.lang.String getFrom() {
        return from;
    }


    /**
     * Sets the from value for this SMSObject.
     * 
     * @param from
     */
    public void setFrom(java.lang.String from) {
        this.from = from;
    }


    /**
     * Gets the to value for this SMSObject.
     * 
     * @return to
     */
    public java.lang.String getTo() {
        return to;
    }


    /**
     * Sets the to value for this SMSObject.
     * 
     * @param to
     */
    public void setTo(java.lang.String to) {
        this.to = to;
    }


    /**
     * Gets the txt value for this SMSObject.
     * 
     * @return txt
     */
    public java.lang.String getTxt() {
        return txt;
    }


    /**
     * Sets the txt value for this SMSObject.
     * 
     * @param txt
     */
    public void setTxt(java.lang.String txt) {
        this.txt = txt;
    }


    /**
     * Gets the delivdate value for this SMSObject.
     * 
     * @return delivdate
     */
    public java.lang.String getDelivdate() {
        return delivdate;
    }


    /**
     * Sets the delivdate value for this SMSObject.
     * 
     * @param delivdate
     */
    public void setDelivdate(java.lang.String delivdate) {
        this.delivdate = delivdate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSObject)) return false;
        SMSObject other = (SMSObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.smsid==null && other.getSmsid()==null) || 
             (this.smsid!=null &&
              this.smsid.equals(other.getSmsid()))) &&
            ((this.from==null && other.getFrom()==null) || 
             (this.from!=null &&
              this.from.equals(other.getFrom()))) &&
            ((this.to==null && other.getTo()==null) || 
             (this.to!=null &&
              this.to.equals(other.getTo()))) &&
            ((this.txt==null && other.getTxt()==null) || 
             (this.txt!=null &&
              this.txt.equals(other.getTxt()))) &&
            ((this.delivdate==null && other.getDelivdate()==null) || 
             (this.delivdate!=null &&
              this.delivdate.equals(other.getDelivdate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSmsid() != null) {
            _hashCode += getSmsid().hashCode();
        }
        if (getFrom() != null) {
            _hashCode += getFrom().hashCode();
        }
        if (getTo() != null) {
            _hashCode += getTo().hashCode();
        }
        if (getTxt() != null) {
            _hashCode += getTxt().hashCode();
        }
        if (getDelivdate() != null) {
            _hashCode += getDelivdate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://inteliso.pl", "SMSObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("smsid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "smsid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("from");
        elemField.setXmlName(new javax.xml.namespace.QName("", "from"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("to");
        elemField.setXmlName(new javax.xml.namespace.QName("", "to"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("txt");
        elemField.setXmlName(new javax.xml.namespace.QName("", "txt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("delivdate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "delivdate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
