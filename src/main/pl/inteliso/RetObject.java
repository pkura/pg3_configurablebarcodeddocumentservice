/**
 * RetObject.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.inteliso;

public class RetObject  implements java.io.Serializable {
    private int status;

    private java.lang.String description;

    private java.lang.String smsid;

    private java.lang.String refid;

    public RetObject() {
    }

    public RetObject(
           int status,
           java.lang.String description,
           java.lang.String smsid,
           java.lang.String refid) {
           this.status = status;
           this.description = description;
           this.smsid = smsid;
           this.refid = refid;
    }


    /**
     * Gets the status value for this RetObject.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this RetObject.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }


    /**
     * Gets the description value for this RetObject.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this RetObject.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the smsid value for this RetObject.
     * 
     * @return smsid
     */
    public java.lang.String getSmsid() {
        return smsid;
    }


    /**
     * Sets the smsid value for this RetObject.
     * 
     * @param smsid
     */
    public void setSmsid(java.lang.String smsid) {
        this.smsid = smsid;
    }


    /**
     * Gets the refid value for this RetObject.
     * 
     * @return refid
     */
    public java.lang.String getRefid() {
        return refid;
    }


    /**
     * Sets the refid value for this RetObject.
     * 
     * @param refid
     */
    public void setRefid(java.lang.String refid) {
        this.refid = refid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RetObject)) return false;
        RetObject other = (RetObject) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.status == other.getStatus() &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.smsid==null && other.getSmsid()==null) || 
             (this.smsid!=null &&
              this.smsid.equals(other.getSmsid()))) &&
            ((this.refid==null && other.getRefid()==null) || 
             (this.refid!=null &&
              this.refid.equals(other.getRefid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getStatus();
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getSmsid() != null) {
            _hashCode += getSmsid().hashCode();
        }
        if (getRefid() != null) {
            _hashCode += getRefid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RetObject.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://inteliso.pl", "RetObject"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("smsid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "smsid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
