/**
 * SMSSenderPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package pl.inteliso;

public interface SMSSenderPortType extends java.rmi.Remote {

    /**
     * Send a VCard SMS
     */
    public pl.inteliso.RetObject VCardSend(java.lang.String username, java.lang.String password, java.lang.String from, java.lang.String to, java.lang.String txt) throws java.rmi.RemoteException;

    /**
     * Send a SMS
     */
    public pl.inteliso.RetObject SMSSend(java.lang.String username, java.lang.String password, java.lang.String from, java.lang.String to, java.lang.String txt) throws java.rmi.RemoteException;

    /**
     * Send a SMSObject
     */
    public pl.inteliso.RetObject SMSSendObject(java.lang.String username, java.lang.String password, pl.inteliso.SMSObject sms) throws java.rmi.RemoteException;

    /**
     * Send a SMSObject list
     */
    public pl.inteliso.RetObject[] SMSSendObjectArray(java.lang.String username, java.lang.String password, pl.inteliso.SMSObject[] sms) throws java.rmi.RemoteException;

    /**
     * Check a balance
     */
    public pl.inteliso.GenericRetObject balance(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;

    /**
     * Check a SMS status
     */
    public pl.inteliso.GenericRetObject SMSStatus(java.lang.String username, java.lang.String password, java.lang.String refid) throws java.rmi.RemoteException;

    /**
     * Get a SMS status report summary
     */
    public pl.inteliso.Row[] reportGetSummary(java.lang.String username, java.lang.String password, java.lang.String date_from, java.lang.String date_to) throws java.rmi.RemoteException;

    /**
     * Get a SMS report detailed
     */
    public pl.inteliso.Row[] reportGet(java.lang.String username, java.lang.String password, java.lang.String date_from, java.lang.String date_to, int offset, int limit) throws java.rmi.RemoteException;
}
