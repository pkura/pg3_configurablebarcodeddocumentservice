//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.28 at 03:21:23 PM CEST 
//


package pl.gov.mswia.standardy.ndap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TypDaty.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TypDaty">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="dostepnyPo"/>
 *     &lt;enumeration value="opublikowany"/>
 *     &lt;enumeration value="stworzony"/>
 *     &lt;enumeration value="uzyskany"/>
 *     &lt;enumeration value="otrzymany"/>
 *     &lt;enumeration value="wyslany"/>
 *     &lt;enumeration value="zaakceptowany"/>
 *     &lt;enumeration value="zatwierdzony"/>
 *     &lt;enumeration value="zmodyfikowany"/>
 *     &lt;enumeration value="daty skrajne"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TypDaty")
@XmlEnum
public enum TypDaty {

    @XmlEnumValue("dostepnyPo")
    DOSTEPNY_PO("dostepnyPo"),
    @XmlEnumValue("opublikowany")
    OPUBLIKOWANY("opublikowany"),
    @XmlEnumValue("stworzony")
    STWORZONY("stworzony"),
    @XmlEnumValue("uzyskany")
    UZYSKANY("uzyskany"),
    @XmlEnumValue("otrzymany")
    OTRZYMANY("otrzymany"),
    @XmlEnumValue("wyslany")
    WYSLANY("wyslany"),
    @XmlEnumValue("zaakceptowany")
    ZAAKCEPTOWANY("zaakceptowany"),
    @XmlEnumValue("zatwierdzony")
    ZATWIERDZONY("zatwierdzony"),
    @XmlEnumValue("zmodyfikowany")
    ZMODYFIKOWANY("zmodyfikowany"),
    @XmlEnumValue("daty skrajne")
    DATY_SKRAJNE("daty skrajne");
    private final String value;

    TypDaty(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypDaty fromValue(String v) {
        for (TypDaty c: TypDaty.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
