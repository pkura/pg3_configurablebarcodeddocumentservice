//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.28 at 03:21:23 PM CEST 
//


package pl.gov.mswia.standardy.ndap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Adres complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Adres">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="kod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poczta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="miejscowosc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ulica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="budynek" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lokal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="skrytkapocztowa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uwagi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="kraj" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Adres", propOrder = {
    "kod",
    "poczta",
    "miejscowosc",
    "ulica",
    "budynek",
    "lokal",
    "skrytkapocztowa",
    "uwagi",
    "kraj"
})
public class Adres {

    protected String kod;
    protected String poczta;
    @XmlElement(required = true)
    protected String miejscowosc;
    protected String ulica;
    protected String budynek;
    protected String lokal;
    protected String skrytkapocztowa;
    protected String uwagi;
    @XmlElement(required = true, defaultValue = "PL")
    protected String kraj;

    /**
     * Gets the value of the kod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKod() {
        return kod;
    }

    /**
     * Sets the value of the kod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKod(String value) {
        this.kod = value;
    }

    /**
     * Gets the value of the poczta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoczta() {
        return poczta;
    }

    /**
     * Sets the value of the poczta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoczta(String value) {
        this.poczta = value;
    }

    /**
     * Gets the value of the miejscowosc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiejscowosc() {
        return miejscowosc;
    }

    /**
     * Sets the value of the miejscowosc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiejscowosc(String value) {
        this.miejscowosc = value;
    }

    /**
     * Gets the value of the ulica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUlica() {
        return ulica;
    }

    /**
     * Sets the value of the ulica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUlica(String value) {
        this.ulica = value;
    }

    /**
     * Gets the value of the budynek property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBudynek() {
        return budynek;
    }

    /**
     * Sets the value of the budynek property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBudynek(String value) {
        this.budynek = value;
    }

    /**
     * Gets the value of the lokal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLokal() {
        return lokal;
    }

    /**
     * Sets the value of the lokal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLokal(String value) {
        this.lokal = value;
    }

    /**
     * Gets the value of the skrytkapocztowa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSkrytkapocztowa() {
        return skrytkapocztowa;
    }

    /**
     * Sets the value of the skrytkapocztowa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSkrytkapocztowa(String value) {
        this.skrytkapocztowa = value;
    }

    /**
     * Gets the value of the uwagi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUwagi() {
        return uwagi;
    }

    /**
     * Sets the value of the uwagi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUwagi(String value) {
        this.uwagi = value;
    }

    /**
     * Gets the value of the kraj property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKraj() {
        return kraj;
    }

    /**
     * Sets the value of the kraj property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKraj(String value) {
        this.kraj = value;
    }

}
