//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.08.28 at 03:21:23 PM CEST 
//


package pl.gov.mswia.standardy.ndap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Tworca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Tworca">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="funkcja">
 *           &lt;simpleType>
 *             &lt;union memberTypes=" {http://www.w3.org/2001/XMLSchema}string">
 *               &lt;simpleType>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                   &lt;enumeration value="stworzyl"/>
 *                   &lt;enumeration value="modyfikowal"/>
 *                   &lt;enumeration value="zatwierdzil"/>
 *                 &lt;/restriction>
 *               &lt;/simpleType>
 *             &lt;/union>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="podmiot" type="{http://www.mswia.gov.pl/standardy/ndap}Podmiot"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tworca", propOrder = {
    "funkcja",
    "podmiot"
})
public class Tworca {
    /**
     * STA�E DLA OBIEKTU TW�RCY
     */
    public static final String STWORZYL = "stworzyl";
    public static final String MODYFIKOWAL = "modyfikowal";
    public static final String ZATWIERDZIL = "zatwierdzil";

    @XmlElement(required = true)
    protected String funkcja;
    @XmlElement(required = true)
    protected Podmiot podmiot;

    /**
     * Gets the value of the funkcja property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFunkcja() {
        return funkcja;
    }

    /**
     * Sets the value of the funkcja property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFunkcja(String value) {
        this.funkcja = value;
    }

    /**
     * Gets the value of the podmiot property.
     * 
     * @return
     *     possible object is
     *     {@link Podmiot }
     *     
     */
    public Podmiot getPodmiot() {
        return podmiot;
    }

    /**
     * Sets the value of the podmiot property.
     * 
     * @param value
     *     allowed object is
     *     {@link Podmiot }
     *     
     */
    public void setPodmiot(Podmiot value) {
        this.podmiot = value;
    }

}
