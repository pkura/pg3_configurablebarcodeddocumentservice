package pl.gov.mswia.standardy.ndap;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <pre>
 *         &lt;element name="dostepnosc">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="wszystko"/>
 *               &lt;enumeration value="metadane"/>
 *               &lt;enumeration value="niedostepne"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 * </pre>
 * 
 */
public enum TypDostepnosci {

    WSZYSTKO("wszystko"),
    METADANE("metadane"),
    NIEDOSTEPNE("niedostepne");

    private final String value;

    TypDostepnosci(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TypDostepnosci fromValue(String v) {
        for (TypDostepnosci c: TypDostepnosci.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
