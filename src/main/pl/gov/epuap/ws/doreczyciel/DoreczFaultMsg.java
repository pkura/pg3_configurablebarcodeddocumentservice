
/**
 * DoreczFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.gov.epuap.ws.doreczyciel;

public class DoreczFaultMsg extends java.lang.Exception{
    
    private pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek faultMessage;

    
        public DoreczFaultMsg() {
            super("DoreczFaultMsg");
        }

        public DoreczFaultMsg(java.lang.String s) {
           super(s);
        }

        public DoreczFaultMsg(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public DoreczFaultMsg(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek msg){
       faultMessage = msg;
    }
    
    public pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek getFaultMessage(){
       return faultMessage;
    }
}
    