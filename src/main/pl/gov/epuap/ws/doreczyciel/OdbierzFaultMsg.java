
/**
 * OdbierzFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.gov.epuap.ws.doreczyciel;

public class OdbierzFaultMsg extends java.lang.Exception{
    
    private pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek faultMessage;

    
        public OdbierzFaultMsg() {
            super("OdbierzFaultMsg");
        }

        public OdbierzFaultMsg(java.lang.String s) {
           super(s);
        }

        public OdbierzFaultMsg(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public OdbierzFaultMsg(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek msg){
       faultMessage = msg;
    }
    
    public pl.gov.epuap.ws.doreczyciel.PkDoreczycielServiceStub.Wyjatek getFaultMessage(){
       return faultMessage;
    }
}
    