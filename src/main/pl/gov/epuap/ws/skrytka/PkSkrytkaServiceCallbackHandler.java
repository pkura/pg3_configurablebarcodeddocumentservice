
/**
 * PkSkrytkaServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package pl.gov.epuap.ws.skrytka;

    /**
     *  PkSkrytkaServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class PkSkrytkaServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public PkSkrytkaServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public PkSkrytkaServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for nadaj method
            * override this method for handling normal response from nadaj operation
            */
           public void receiveResultnadaj(
                    pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.OdpowiedzSkrytki result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from nadaj operation
           */
            public void receiveErrornadaj(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for nadajAny method
            * override this method for handling normal response from nadajAny operation
            */
           public void receiveResultnadajAny(
                    pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.OdpowiedzSkrytki result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from nadajAny operation
           */
            public void receiveErrornadajAny(java.lang.Exception e) {
            }
                


    }
    