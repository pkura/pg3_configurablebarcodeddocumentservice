
/**
 * NadajFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.gov.epuap.ws.skrytka;

public class NadajFaultMsg extends java.lang.Exception{
    
    private pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.Wyjatek faultMessage;

    
        public NadajFaultMsg() {
            super("NadajFaultMsg");
        }

        public NadajFaultMsg(java.lang.String s) {
           super(s);
        }

        public NadajFaultMsg(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public NadajFaultMsg(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.Wyjatek msg){
       faultMessage = msg;
    }
    
    public pl.gov.epuap.ws.skrytka.PkSkrytkaServiceStub.Wyjatek getFaultMessage(){
       return faultMessage;
    }
}
    