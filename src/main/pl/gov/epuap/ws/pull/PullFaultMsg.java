
/**
 * PullFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.gov.epuap.ws.pull;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class PullFaultMsg extends Exception
{    
	
	static final long serialVersionUID = 23223;
    private static final Logger log = LoggerFactory.getLogger(EdmException.class);
     
    public PullFaultMsg(String message)
    {
        super(message);
        log.debug(message, this);
    }

    public PullFaultMsg(String message, boolean noLogging)
    {
        super(message);
        if (!noLogging)
            log.debug(message, this);
    }
    
    
    public PullFaultMsg(String message, Throwable cause)
    {
        super(message, cause);
        log.debug(message, this);
    }

    public PullFaultMsg(Throwable cause)
    {
        super(cause);
        log.debug("", this);
    }
	
    
	private pl.gov.epuap.ws.pull.PkPullServiceStub.Wyjatek faultMessage;
    
    public PullFaultMsg() {
        super();
    }
    
    public synchronized Throwable fillInStackTrace()
    {
    	return this.getCause();
    }
    

    public void setFaultMessage(pl.gov.epuap.ws.pull.PkPullServiceStub.Wyjatek msg){
       faultMessage = msg;
    }
    
    public pl.gov.epuap.ws.pull.PkPullServiceStub.Wyjatek getFaultMessage(){
       return faultMessage;
    }
}
    