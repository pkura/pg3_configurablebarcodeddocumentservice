package pl.gov.epuap.ws.pull;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;

/**
 * Klasa helper.java
 * @author <a href="mailto:mariusz.kiljanczyk@docusafe.pl">Mariusz Kilja�czyk</a>
 */
public class helper {

	
public static void toFile(String s,String filename)
{
	File outFile = new File(filename);
    FileWriter out;
	try {
		out = new FileWriter(outFile);
	    out.write(s);
	    out.close();
	} catch (IOException e) {
		e.printStackTrace();
	}

}
	
	
	public static void print(XMLStreamReader parser) {

		try {

			int inHeader = 0;
			for (int event = parser.next(); event != XMLStreamConstants.END_DOCUMENT; event = parser
					.next()) {
				switch (event) {
				case XMLStreamConstants.START_ELEMENT:
					if (isHeader(parser.getLocalName())) {
						inHeader++;
					}
					break;
				case XMLStreamConstants.END_ELEMENT:
					if (isHeader(parser.getLocalName())) {
						inHeader--;
						if (inHeader == 0)
							System.out.println();
					}
					break;
				case XMLStreamConstants.CHARACTERS:
					if (inHeader > 0)
						System.out.print(parser.getText());
					break;
				case XMLStreamConstants.CDATA:
					if (inHeader > 0)
						System.out.print(parser.getText());
					break;
				} // end switch
			} // end while
			parser.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
	
	
	   /**
	    * Determine if this is an XHTML heading element or not
	    * @param  name tag name
	    * @return boolean true if this is h1, h2, h3, h4, h5, or h6; 
	    *                 false otherwise
	    */
	    private static boolean isHeader(String name) {
	      if (name.equals("h1")) return true;
	      if (name.equals("h2")) return true;
	      if (name.equals("h3")) return true;
	      if (name.equals("h4")) return true;
	      if (name.equals("h5")) return true;
	      if (name.equals("h6")) return true;
	      return false;
	    }
}
