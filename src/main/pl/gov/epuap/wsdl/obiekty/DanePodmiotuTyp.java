/**
 * DanePodmiotuTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class DanePodmiotuTyp  implements java.io.Serializable {
    /* Identyfikator podmiotu, w imieniu którego działa użytkownik */
    private java.lang.String identyfikator;

    /* P - prawna, F - osoba fizyczna */
    private java.lang.String typOsoby;

    private java.lang.String imieSkrot;

    private java.lang.String nazwiskoNazwa;

    private java.lang.String nip;

    private java.lang.String pesel;

    private java.lang.String regon;

    /* Czy jest zgoda na otrzymywanie korespondencji drogą elektroniczną? */
    private boolean zgoda;

    public DanePodmiotuTyp() {
    }

    public DanePodmiotuTyp(
           java.lang.String identyfikator,
           java.lang.String typOsoby,
           java.lang.String imieSkrot,
           java.lang.String nazwiskoNazwa,
           java.lang.String nip,
           java.lang.String pesel,
           java.lang.String regon,
           boolean zgoda) {
           this.identyfikator = identyfikator;
           this.typOsoby = typOsoby;
           this.imieSkrot = imieSkrot;
           this.nazwiskoNazwa = nazwiskoNazwa;
           this.nip = nip;
           this.pesel = pesel;
           this.regon = regon;
           this.zgoda = zgoda;
    }


    /**
     * Gets the identyfikator value for this DanePodmiotuTyp.
     * 
     * @return identyfikator   * Identyfikator podmiotu, w imieniu którego działa użytkownik
     */
    public java.lang.String getIdentyfikator() {
        return identyfikator;
    }


    /**
     * Sets the identyfikator value for this DanePodmiotuTyp.
     * 
     * @param identyfikator   * Identyfikator podmiotu, w imieniu którego działa użytkownik
     */
    public void setIdentyfikator(java.lang.String identyfikator) {
        this.identyfikator = identyfikator;
    }


    /**
     * Gets the typOsoby value for this DanePodmiotuTyp.
     * 
     * @return typOsoby   * P - prawna, F - osoba fizyczna
     */
    public java.lang.String getTypOsoby() {
        return typOsoby;
    }


    /**
     * Sets the typOsoby value for this DanePodmiotuTyp.
     * 
     * @param typOsoby   * P - prawna, F - osoba fizyczna
     */
    public void setTypOsoby(java.lang.String typOsoby) {
        this.typOsoby = typOsoby;
    }


    /**
     * Gets the imieSkrot value for this DanePodmiotuTyp.
     * 
     * @return imieSkrot
     */
    public java.lang.String getImieSkrot() {
        return imieSkrot;
    }


    /**
     * Sets the imieSkrot value for this DanePodmiotuTyp.
     * 
     * @param imieSkrot
     */
    public void setImieSkrot(java.lang.String imieSkrot) {
        this.imieSkrot = imieSkrot;
    }


    /**
     * Gets the nazwiskoNazwa value for this DanePodmiotuTyp.
     * 
     * @return nazwiskoNazwa
     */
    public java.lang.String getNazwiskoNazwa() {
        return nazwiskoNazwa;
    }


    /**
     * Sets the nazwiskoNazwa value for this DanePodmiotuTyp.
     * 
     * @param nazwiskoNazwa
     */
    public void setNazwiskoNazwa(java.lang.String nazwiskoNazwa) {
        this.nazwiskoNazwa = nazwiskoNazwa;
    }


    /**
     * Gets the nip value for this DanePodmiotuTyp.
     * 
     * @return nip
     */
    public java.lang.String getNip() {
        return nip;
    }


    /**
     * Sets the nip value for this DanePodmiotuTyp.
     * 
     * @param nip
     */
    public void setNip(java.lang.String nip) {
        this.nip = nip;
    }


    /**
     * Gets the pesel value for this DanePodmiotuTyp.
     * 
     * @return pesel
     */
    public java.lang.String getPesel() {
        return pesel;
    }


    /**
     * Sets the pesel value for this DanePodmiotuTyp.
     * 
     * @param pesel
     */
    public void setPesel(java.lang.String pesel) {
        this.pesel = pesel;
    }


    /**
     * Gets the regon value for this DanePodmiotuTyp.
     * 
     * @return regon
     */
    public java.lang.String getRegon() {
        return regon;
    }


    /**
     * Sets the regon value for this DanePodmiotuTyp.
     * 
     * @param regon
     */
    public void setRegon(java.lang.String regon) {
        this.regon = regon;
    }


    /**
     * Gets the zgoda value for this DanePodmiotuTyp.
     * 
     * @return zgoda   * Czy jest zgoda na otrzymywanie korespondencji drogą elektroniczną?
     */
    public boolean isZgoda() {
        return zgoda;
    }


    /**
     * Sets the zgoda value for this DanePodmiotuTyp.
     * 
     * @param zgoda   * Czy jest zgoda na otrzymywanie korespondencji drogą elektroniczną?
     */
    public void setZgoda(boolean zgoda) {
        this.zgoda = zgoda;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DanePodmiotuTyp)) return false;
        DanePodmiotuTyp other = (DanePodmiotuTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.identyfikator==null && other.getIdentyfikator()==null) || 
             (this.identyfikator!=null &&
              this.identyfikator.equals(other.getIdentyfikator()))) &&
            ((this.typOsoby==null && other.getTypOsoby()==null) || 
             (this.typOsoby!=null &&
              this.typOsoby.equals(other.getTypOsoby()))) &&
            ((this.imieSkrot==null && other.getImieSkrot()==null) || 
             (this.imieSkrot!=null &&
              this.imieSkrot.equals(other.getImieSkrot()))) &&
            ((this.nazwiskoNazwa==null && other.getNazwiskoNazwa()==null) || 
             (this.nazwiskoNazwa!=null &&
              this.nazwiskoNazwa.equals(other.getNazwiskoNazwa()))) &&
            ((this.nip==null && other.getNip()==null) || 
             (this.nip!=null &&
              this.nip.equals(other.getNip()))) &&
            ((this.pesel==null && other.getPesel()==null) || 
             (this.pesel!=null &&
              this.pesel.equals(other.getPesel()))) &&
            ((this.regon==null && other.getRegon()==null) || 
             (this.regon!=null &&
              this.regon.equals(other.getRegon()))) &&
            this.zgoda == other.isZgoda();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdentyfikator() != null) {
            _hashCode += getIdentyfikator().hashCode();
        }
        if (getTypOsoby() != null) {
            _hashCode += getTypOsoby().hashCode();
        }
        if (getImieSkrot() != null) {
            _hashCode += getImieSkrot().hashCode();
        }
        if (getNazwiskoNazwa() != null) {
            _hashCode += getNazwiskoNazwa().hashCode();
        }
        if (getNip() != null) {
            _hashCode += getNip().hashCode();
        }
        if (getPesel() != null) {
            _hashCode += getPesel().hashCode();
        }
        if (getRegon() != null) {
            _hashCode += getRegon().hashCode();
        }
        _hashCode += (isZgoda() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DanePodmiotuTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DanePodmiotuTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identyfikator");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identyfikator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typOsoby");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typOsoby"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("imieSkrot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "imieSkrot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwiskoNazwa");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nazwiskoNazwa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nip");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pesel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pesel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("regon");
        elemField.setXmlName(new javax.xml.namespace.QName("", "regon"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zgoda");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zgoda"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
