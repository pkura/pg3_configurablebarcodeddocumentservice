/**
 * OdpowiedzPullPobierzTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class OdpowiedzPullPobierzTyp  extends pl.gov.epuap.wsdl.obiekty.OdpowiedzPullTyp  implements java.io.Serializable {
    private pl.gov.epuap.wsdl.obiekty.DanePodmiotuTyp danePodmiotu;

    private pl.gov.epuap.wsdl.obiekty.DaneNadawcyTyp daneNadawcy;

    private java.util.Calendar dataNadania;

    private java.lang.String nazwaSkrytki;

    private java.lang.String adresSkrytki;

    private java.lang.String adresOdpowiedzi;

    private boolean czyTestowe;

    private byte[] daneDodatkowe;

    private pl.gov.epuap.wsdl.obiekty.DokumentTyp dokument;

    public OdpowiedzPullPobierzTyp() {
    }

    public OdpowiedzPullPobierzTyp(
           pl.gov.epuap.wsdl.obiekty.StatusTyp status,
           pl.gov.epuap.wsdl.obiekty.DanePodmiotuTyp danePodmiotu,
           pl.gov.epuap.wsdl.obiekty.DaneNadawcyTyp daneNadawcy,
           java.util.Calendar dataNadania,
           java.lang.String nazwaSkrytki,
           java.lang.String adresSkrytki,
           java.lang.String adresOdpowiedzi,
           boolean czyTestowe,
           byte[] daneDodatkowe,
           pl.gov.epuap.wsdl.obiekty.DokumentTyp dokument) {
        super(
            status);
        this.danePodmiotu = danePodmiotu;
        this.daneNadawcy = daneNadawcy;
        this.dataNadania = dataNadania;
        this.nazwaSkrytki = nazwaSkrytki;
        this.adresSkrytki = adresSkrytki;
        this.adresOdpowiedzi = adresOdpowiedzi;
        this.czyTestowe = czyTestowe;
        this.daneDodatkowe = daneDodatkowe;
        this.dokument = dokument;
    }


    /**
     * Gets the danePodmiotu value for this OdpowiedzPullPobierzTyp.
     * 
     * @return danePodmiotu
     */
    public pl.gov.epuap.wsdl.obiekty.DanePodmiotuTyp getDanePodmiotu() {
        return danePodmiotu;
    }


    /**
     * Sets the danePodmiotu value for this OdpowiedzPullPobierzTyp.
     * 
     * @param danePodmiotu
     */
    public void setDanePodmiotu(pl.gov.epuap.wsdl.obiekty.DanePodmiotuTyp danePodmiotu) {
        this.danePodmiotu = danePodmiotu;
    }


    /**
     * Gets the daneNadawcy value for this OdpowiedzPullPobierzTyp.
     * 
     * @return daneNadawcy
     */
    public pl.gov.epuap.wsdl.obiekty.DaneNadawcyTyp getDaneNadawcy() {
        return daneNadawcy;
    }


    /**
     * Sets the daneNadawcy value for this OdpowiedzPullPobierzTyp.
     * 
     * @param daneNadawcy
     */
    public void setDaneNadawcy(pl.gov.epuap.wsdl.obiekty.DaneNadawcyTyp daneNadawcy) {
        this.daneNadawcy = daneNadawcy;
    }


    /**
     * Gets the dataNadania value for this OdpowiedzPullPobierzTyp.
     * 
     * @return dataNadania
     */
    public java.util.Calendar getDataNadania() {
        return dataNadania;
    }


    /**
     * Sets the dataNadania value for this OdpowiedzPullPobierzTyp.
     * 
     * @param dataNadania
     */
    public void setDataNadania(java.util.Calendar dataNadania) {
        this.dataNadania = dataNadania;
    }


    /**
     * Gets the nazwaSkrytki value for this OdpowiedzPullPobierzTyp.
     * 
     * @return nazwaSkrytki
     */
    public java.lang.String getNazwaSkrytki() {
        return nazwaSkrytki;
    }


    /**
     * Sets the nazwaSkrytki value for this OdpowiedzPullPobierzTyp.
     * 
     * @param nazwaSkrytki
     */
    public void setNazwaSkrytki(java.lang.String nazwaSkrytki) {
        this.nazwaSkrytki = nazwaSkrytki;
    }


    /**
     * Gets the adresSkrytki value for this OdpowiedzPullPobierzTyp.
     * 
     * @return adresSkrytki
     */
    public java.lang.String getAdresSkrytki() {
        return adresSkrytki;
    }


    /**
     * Sets the adresSkrytki value for this OdpowiedzPullPobierzTyp.
     * 
     * @param adresSkrytki
     */
    public void setAdresSkrytki(java.lang.String adresSkrytki) {
        this.adresSkrytki = adresSkrytki;
    }


    /**
     * Gets the adresOdpowiedzi value for this OdpowiedzPullPobierzTyp.
     * 
     * @return adresOdpowiedzi
     */
    public java.lang.String getAdresOdpowiedzi() {
        return adresOdpowiedzi;
    }


    /**
     * Sets the adresOdpowiedzi value for this OdpowiedzPullPobierzTyp.
     * 
     * @param adresOdpowiedzi
     */
    public void setAdresOdpowiedzi(java.lang.String adresOdpowiedzi) {
        this.adresOdpowiedzi = adresOdpowiedzi;
    }


    /**
     * Gets the czyTestowe value for this OdpowiedzPullPobierzTyp.
     * 
     * @return czyTestowe
     */
    public boolean isCzyTestowe() {
        return czyTestowe;
    }


    /**
     * Sets the czyTestowe value for this OdpowiedzPullPobierzTyp.
     * 
     * @param czyTestowe
     */
    public void setCzyTestowe(boolean czyTestowe) {
        this.czyTestowe = czyTestowe;
    }


    /**
     * Gets the daneDodatkowe value for this OdpowiedzPullPobierzTyp.
     * 
     * @return daneDodatkowe
     */
    public byte[] getDaneDodatkowe() {
        return daneDodatkowe;
    }


    /**
     * Sets the daneDodatkowe value for this OdpowiedzPullPobierzTyp.
     * 
     * @param daneDodatkowe
     */
    public void setDaneDodatkowe(byte[] daneDodatkowe) {
        this.daneDodatkowe = daneDodatkowe;
    }


    /**
     * Gets the dokument value for this OdpowiedzPullPobierzTyp.
     * 
     * @return dokument
     */
    public pl.gov.epuap.wsdl.obiekty.DokumentTyp getDokument() {
        return dokument;
    }


    /**
     * Sets the dokument value for this OdpowiedzPullPobierzTyp.
     * 
     * @param dokument
     */
    public void setDokument(pl.gov.epuap.wsdl.obiekty.DokumentTyp dokument) {
        this.dokument = dokument;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OdpowiedzPullPobierzTyp)) return false;
        OdpowiedzPullPobierzTyp other = (OdpowiedzPullPobierzTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.danePodmiotu==null && other.getDanePodmiotu()==null) || 
             (this.danePodmiotu!=null &&
              this.danePodmiotu.equals(other.getDanePodmiotu()))) &&
            ((this.daneNadawcy==null && other.getDaneNadawcy()==null) || 
             (this.daneNadawcy!=null &&
              this.daneNadawcy.equals(other.getDaneNadawcy()))) &&
            ((this.dataNadania==null && other.getDataNadania()==null) || 
             (this.dataNadania!=null &&
              this.dataNadania.equals(other.getDataNadania()))) &&
            ((this.nazwaSkrytki==null && other.getNazwaSkrytki()==null) || 
             (this.nazwaSkrytki!=null &&
              this.nazwaSkrytki.equals(other.getNazwaSkrytki()))) &&
            ((this.adresSkrytki==null && other.getAdresSkrytki()==null) || 
             (this.adresSkrytki!=null &&
              this.adresSkrytki.equals(other.getAdresSkrytki()))) &&
            ((this.adresOdpowiedzi==null && other.getAdresOdpowiedzi()==null) || 
             (this.adresOdpowiedzi!=null &&
              this.adresOdpowiedzi.equals(other.getAdresOdpowiedzi()))) &&
            this.czyTestowe == other.isCzyTestowe() &&
            ((this.daneDodatkowe==null && other.getDaneDodatkowe()==null) || 
             (this.daneDodatkowe!=null &&
              java.util.Arrays.equals(this.daneDodatkowe, other.getDaneDodatkowe()))) &&
            ((this.dokument==null && other.getDokument()==null) || 
             (this.dokument!=null &&
              this.dokument.equals(other.getDokument())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getDanePodmiotu() != null) {
            _hashCode += getDanePodmiotu().hashCode();
        }
        if (getDaneNadawcy() != null) {
            _hashCode += getDaneNadawcy().hashCode();
        }
        if (getDataNadania() != null) {
            _hashCode += getDataNadania().hashCode();
        }
        if (getNazwaSkrytki() != null) {
            _hashCode += getNazwaSkrytki().hashCode();
        }
        if (getAdresSkrytki() != null) {
            _hashCode += getAdresSkrytki().hashCode();
        }
        if (getAdresOdpowiedzi() != null) {
            _hashCode += getAdresOdpowiedzi().hashCode();
        }
        _hashCode += (isCzyTestowe() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getDaneDodatkowe() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDaneDodatkowe());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDaneDodatkowe(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDokument() != null) {
            _hashCode += getDokument().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OdpowiedzPullPobierzTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "OdpowiedzPullPobierzTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("danePodmiotu");
        elemField.setXmlName(new javax.xml.namespace.QName("", "danePodmiotu"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DanePodmiotuTyp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("daneNadawcy");
        elemField.setXmlName(new javax.xml.namespace.QName("", "daneNadawcy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DaneNadawcyTyp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dataNadania");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dataNadania"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwaSkrytki");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nazwaSkrytki"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresSkrytki");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresSkrytki"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresOdpowiedzi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresOdpowiedzi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("czyTestowe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "czyTestowe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("daneDodatkowe");
        elemField.setXmlName(new javax.xml.namespace.QName("", "daneDodatkowe"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dokument");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dokument"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DokumentTyp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
