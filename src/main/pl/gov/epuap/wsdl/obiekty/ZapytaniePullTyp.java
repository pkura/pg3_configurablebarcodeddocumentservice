/**
 * ZapytaniePullTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;


/**
 * Ogólny, abstrakcyjny typ zapytania metod PULL
 *         		Poszczególne zapytania metod PULL dziedziczą z tego typu.
 */
public class ZapytaniePullTyp  implements java.io.Serializable {
    private java.lang.String podmiot;

    private java.lang.String nazwaSkrytki;

    private java.lang.String adresSkrytki;

    public ZapytaniePullTyp() {
    }

    public ZapytaniePullTyp(
           java.lang.String podmiot,
           java.lang.String nazwaSkrytki,
           java.lang.String adresSkrytki) {
           this.podmiot = podmiot;
           this.nazwaSkrytki = nazwaSkrytki;
           this.adresSkrytki = adresSkrytki;

    }


    /**
     * Gets the podmiot value for this ZapytaniePullTyp.
     * 
     * @return podmiot
     */
    public java.lang.String getPodmiot() {
        return podmiot;
    }


    /**
     * Sets the podmiot value for this ZapytaniePullTyp.
     * 
     * @param podmiot
     */
    public void setPodmiot(java.lang.String podmiot) {
        this.podmiot = podmiot;
    }


    /**
     * Gets the nazwaSkrytki value for this ZapytaniePullTyp.
     * 
     * @return nazwaSkrytki
     */
    public java.lang.String getNazwaSkrytki() {
        return nazwaSkrytki;
    }


    /**
     * Sets the nazwaSkrytki value for this ZapytaniePullTyp.
     * 
     * @param nazwaSkrytki
     */
    public void setNazwaSkrytki(java.lang.String nazwaSkrytki) {
        this.nazwaSkrytki = nazwaSkrytki;
    }


    /**
     * Gets the adresSkrytki value for this ZapytaniePullTyp.
     * 
     * @return adresSkrytki
     */
    public java.lang.String getAdresSkrytki() {
        return adresSkrytki;
    }


    /**
     * Sets the adresSkrytki value for this ZapytaniePullTyp.
     * 
     * @param adresSkrytki
     */
    public void setAdresSkrytki(java.lang.String adresSkrytki) {
        this.adresSkrytki = adresSkrytki;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ZapytaniePullTyp)) return false;
        ZapytaniePullTyp other = (ZapytaniePullTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.podmiot==null && other.getPodmiot()==null) || 
             (this.podmiot!=null &&
              this.podmiot.equals(other.getPodmiot()))) &&
            ((this.nazwaSkrytki==null && other.getNazwaSkrytki()==null) || 
             (this.nazwaSkrytki!=null &&
              this.nazwaSkrytki.equals(other.getNazwaSkrytki()))) &&
            ((this.adresSkrytki==null && other.getAdresSkrytki()==null) || 
             (this.adresSkrytki!=null &&
              this.adresSkrytki.equals(other.getAdresSkrytki())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPodmiot() != null) {
            _hashCode += getPodmiot().hashCode();
        }
        if (getNazwaSkrytki() != null) {
            _hashCode += getNazwaSkrytki().hashCode();
        }
        if (getAdresSkrytki() != null) {
            _hashCode += getAdresSkrytki().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ZapytaniePullTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "ZapytaniePullTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("podmiot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "podmiot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwaSkrytki");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nazwaSkrytki"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adresSkrytki");
        elemField.setXmlName(new javax.xml.namespace.QName("", "adresSkrytki"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
