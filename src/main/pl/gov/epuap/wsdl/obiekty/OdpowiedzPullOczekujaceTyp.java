/**
 * OdpowiedzPullOczekujaceTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class OdpowiedzPullOczekujaceTyp  extends pl.gov.epuap.wsdl.obiekty.OdpowiedzPullTyp  implements java.io.Serializable {
    private int oczekujace;

    public OdpowiedzPullOczekujaceTyp() {
    }

    public OdpowiedzPullOczekujaceTyp(
           pl.gov.epuap.wsdl.obiekty.StatusTyp status,
           int oczekujace) {
        super(
            status);
        this.oczekujace = oczekujace;
    }


    /**
     * Gets the oczekujace value for this OdpowiedzPullOczekujaceTyp.
     * 
     * @return oczekujace
     */
    public int getOczekujace() {
        return oczekujace;
    }


    /**
     * Sets the oczekujace value for this OdpowiedzPullOczekujaceTyp.
     * 
     * @param oczekujace
     */
    public void setOczekujace(int oczekujace) {
        this.oczekujace = oczekujace;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OdpowiedzPullOczekujaceTyp)) return false;
        OdpowiedzPullOczekujaceTyp other = (OdpowiedzPullOczekujaceTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.oczekujace == other.getOczekujace();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getOczekujace();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OdpowiedzPullOczekujaceTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "OdpowiedzPullOczekujaceTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oczekujace");
        elemField.setXmlName(new javax.xml.namespace.QName("", "oczekujace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
