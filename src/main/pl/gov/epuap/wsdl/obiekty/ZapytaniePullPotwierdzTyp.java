/**
 * ZapytaniePullPotwierdzTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class ZapytaniePullPotwierdzTyp  extends pl.gov.epuap.wsdl.obiekty.ZapytaniePullTyp  implements java.io.Serializable {
    private java.lang.String skrot;

    public ZapytaniePullPotwierdzTyp() {
    }

    public ZapytaniePullPotwierdzTyp(
           java.lang.String podmiot,
           java.lang.String nazwaSkrytki,
           java.lang.String adresSkrytki,
           java.lang.String skrot) {
        super(
            podmiot,
            nazwaSkrytki,
            adresSkrytki);
        this.skrot = skrot;
    }


    /**
     * Gets the skrot value for this ZapytaniePullPotwierdzTyp.
     * 
     * @return skrot
     */
    public java.lang.String getSkrot() {
        return skrot;
    }


    /**
     * Sets the skrot value for this ZapytaniePullPotwierdzTyp.
     * 
     * @param skrot
     */
    public void setSkrot(java.lang.String skrot) {
        this.skrot = skrot;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ZapytaniePullPotwierdzTyp)) return false;
        ZapytaniePullPotwierdzTyp other = (ZapytaniePullPotwierdzTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.skrot==null && other.getSkrot()==null) || 
             (this.skrot!=null &&
              this.skrot.equals(other.getSkrot())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getSkrot() != null) {
            _hashCode += getSkrot().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ZapytaniePullPotwierdzTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "ZapytaniePullPotwierdzTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skrot");
        elemField.setXmlName(new javax.xml.namespace.QName("", "skrot"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
