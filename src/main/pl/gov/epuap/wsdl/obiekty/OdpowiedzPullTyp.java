/**
 * OdpowiedzPullTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;


/**
 * Ogólny, abstrakcyjny typ odpowiedzi metod PULL
 *         		Poszczególne odpowiedzi metod PULL dziedziczą z	tego typu.
 */
public class OdpowiedzPullTyp  implements java.io.Serializable {
    private pl.gov.epuap.wsdl.obiekty.StatusTyp status;

    public OdpowiedzPullTyp() {
    }

    public OdpowiedzPullTyp(
           pl.gov.epuap.wsdl.obiekty.StatusTyp status) {
           this.status = status;
    }


    /**
     * Gets the status value for this OdpowiedzPullTyp.
     * 
     * @return status
     */
    public pl.gov.epuap.wsdl.obiekty.StatusTyp getStatus() {
        return status;
    }


    /**
     * Sets the status value for this OdpowiedzPullTyp.
     * 
     * @param status
     */
    public void setStatus(pl.gov.epuap.wsdl.obiekty.StatusTyp status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OdpowiedzPullTyp)) return false;
        OdpowiedzPullTyp other = (OdpowiedzPullTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OdpowiedzPullTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "OdpowiedzPullTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "StatusTyp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
