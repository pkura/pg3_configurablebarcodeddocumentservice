/**
 * StatusTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class StatusTyp  implements java.io.Serializable {
    private int kod;

    private java.lang.String komunikat;

    public StatusTyp() {
    }

    public StatusTyp(
           int kod,
           java.lang.String komunikat) {
           this.kod = kod;
           this.komunikat = komunikat;
    }


    /**
     * Gets the kod value for this StatusTyp.
     * 
     * @return kod
     */
    public int getKod() {
        return kod;
    }


    /**
     * Sets the kod value for this StatusTyp.
     * 
     * @param kod
     */
    public void setKod(int kod) {
        this.kod = kod;
    }


    /**
     * Gets the komunikat value for this StatusTyp.
     * 
     * @return komunikat
     */
    public java.lang.String getKomunikat() {
        return komunikat;
    }


    /**
     * Sets the komunikat value for this StatusTyp.
     * 
     * @param komunikat
     */
    public void setKomunikat(java.lang.String komunikat) {
        this.komunikat = komunikat;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatusTyp)) return false;
        StatusTyp other = (StatusTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.kod == other.getKod() &&
            ((this.komunikat==null && other.getKomunikat()==null) || 
             (this.komunikat!=null &&
              this.komunikat.equals(other.getKomunikat())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getKod();
        if (getKomunikat() != null) {
            _hashCode += getKomunikat().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatusTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "StatusTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kod");
        elemField.setXmlName(new javax.xml.namespace.QName("", "kod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("komunikat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "komunikat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
