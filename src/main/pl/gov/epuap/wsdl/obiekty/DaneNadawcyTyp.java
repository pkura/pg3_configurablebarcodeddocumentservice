/**
 * DaneNadawcyTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class DaneNadawcyTyp  implements java.io.Serializable {
    /* Identyfikator (login) użytkownika */
    private java.lang.String uzytkownik;

    /* Identyfikator systemu */
    private java.lang.String system;

    public DaneNadawcyTyp() {
    }

    public DaneNadawcyTyp(
           java.lang.String uzytkownik,
           java.lang.String system) {
           this.uzytkownik = uzytkownik;
           this.system = system;
    }


    /**
     * Gets the uzytkownik value for this DaneNadawcyTyp.
     * 
     * @return uzytkownik   * Identyfikator (login) użytkownika
     */
    public java.lang.String getUzytkownik() {
        return uzytkownik;
    }


    /**
     * Sets the uzytkownik value for this DaneNadawcyTyp.
     * 
     * @param uzytkownik   * Identyfikator (login) użytkownika
     */
    public void setUzytkownik(java.lang.String uzytkownik) {
        this.uzytkownik = uzytkownik;
    }


    /**
     * Gets the system value for this DaneNadawcyTyp.
     * 
     * @return system   * Identyfikator systemu
     */
    public java.lang.String getSystem() {
        return system;
    }


    /**
     * Sets the system value for this DaneNadawcyTyp.
     * 
     * @param system   * Identyfikator systemu
     */
    public void setSystem(java.lang.String system) {
        this.system = system;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DaneNadawcyTyp)) return false;
        DaneNadawcyTyp other = (DaneNadawcyTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uzytkownik==null && other.getUzytkownik()==null) || 
             (this.uzytkownik!=null &&
              this.uzytkownik.equals(other.getUzytkownik()))) &&
            ((this.system==null && other.getSystem()==null) || 
             (this.system!=null &&
              this.system.equals(other.getSystem())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUzytkownik() != null) {
            _hashCode += getUzytkownik().hashCode();
        }
        if (getSystem() != null) {
            _hashCode += getSystem().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DaneNadawcyTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DaneNadawcyTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uzytkownik");
        elemField.setXmlName(new javax.xml.namespace.QName("", "uzytkownik"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("system");
        elemField.setXmlName(new javax.xml.namespace.QName("", "system"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
