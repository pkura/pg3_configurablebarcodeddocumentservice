/**
 * DokumentTyp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Nov 19, 2006 (02:31:34 GMT+00:00) WSDL2Java emitter.
 */

package pl.gov.epuap.wsdl.obiekty;

public class DokumentTyp  implements java.io.Serializable {
    private java.lang.String nazwaPliku;

    private java.lang.String typPliku;

    private byte[] zawartosc;

    public DokumentTyp() {
    }

    public DokumentTyp(
           java.lang.String nazwaPliku,
           java.lang.String typPliku,
           byte[] zawartosc) {
           this.nazwaPliku = nazwaPliku;
           this.typPliku = typPliku;
           this.zawartosc = zawartosc;
    }


    /**
     * Gets the nazwaPliku value for this DokumentTyp.
     * 
     * @return nazwaPliku
     */
    public java.lang.String getNazwaPliku() {
        return nazwaPliku;
    }


    /**
     * Sets the nazwaPliku value for this DokumentTyp.
     * 
     * @param nazwaPliku
     */
    public void setNazwaPliku(java.lang.String nazwaPliku) {
        this.nazwaPliku = nazwaPliku;
    }


    /**
     * Gets the typPliku value for this DokumentTyp.
     * 
     * @return typPliku
     */
    public java.lang.String getTypPliku() {
        return typPliku;
    }


    /**
     * Sets the typPliku value for this DokumentTyp.
     * 
     * @param typPliku
     */
    public void setTypPliku(java.lang.String typPliku) {
        this.typPliku = typPliku;
    }


    /**
     * Gets the zawartosc value for this DokumentTyp.
     * 
     * @return zawartosc
     */
    public byte[] getZawartosc() {
        return zawartosc;
    }


    /**
     * Sets the zawartosc value for this DokumentTyp.
     * 
     * @param zawartosc
     */
    public void setZawartosc(byte[] zawartosc) {
        this.zawartosc = zawartosc;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DokumentTyp)) return false;
        DokumentTyp other = (DokumentTyp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nazwaPliku==null && other.getNazwaPliku()==null) || 
             (this.nazwaPliku!=null &&
              this.nazwaPliku.equals(other.getNazwaPliku()))) &&
            ((this.typPliku==null && other.getTypPliku()==null) || 
             (this.typPliku!=null &&
              this.typPliku.equals(other.getTypPliku()))) &&
            ((this.zawartosc==null && other.getZawartosc()==null) || 
             (this.zawartosc!=null &&
              java.util.Arrays.equals(this.zawartosc, other.getZawartosc())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNazwaPliku() != null) {
            _hashCode += getNazwaPliku().hashCode();
        }
        if (getTypPliku() != null) {
            _hashCode += getTypPliku().hashCode();
        }
        if (getZawartosc() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getZawartosc());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getZawartosc(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DokumentTyp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://wsdl.epuap.gov.pl/obiekty/", "DokumentTyp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nazwaPliku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nazwaPliku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typPliku");
        elemField.setXmlName(new javax.xml.namespace.QName("", "typPliku"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zawartosc");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zawartosc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
