
/**
 * DictionaryServicesCostBudgetCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package pl.compan.docusafe.ws.ul;

    /**
     *  DictionaryServicesCostBudgetCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesCostBudgetCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesCostBudgetCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesCostBudgetCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getCostBudgets method
            * override this method for handling normal response from getCostBudgets operation
            */
           public void receiveResultgetCostBudgets(
                    pl.compan.docusafe.ws.ul.DictionaryServicesCostBudgetStub.GetCostBudgetsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostBudgets operation
           */
            public void receiveErrorgetCostBudgets(java.lang.Exception e) {
            }
                


    }
    