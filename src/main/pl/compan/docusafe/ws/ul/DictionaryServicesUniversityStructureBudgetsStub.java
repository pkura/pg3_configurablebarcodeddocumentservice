
/**
 * DictionaryServicesUniversityStructureBudgetsStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */
        package pl.compan.docusafe.ws.ul;

        

        /*
        *  DictionaryServicesUniversityStructureBudgetsStub java implementation
        */

        
        public class DictionaryServicesUniversityStructureBudgetsStub extends org.apache.axis2.client.Stub
        {
        protected org.apache.axis2.description.AxisOperation[] _operations;

        //hashmaps to keep the fault mapping
        private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
        private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
        private java.util.HashMap faultMessageMap = new java.util.HashMap();

        private static int counter = 0;

        private static synchronized java.lang.String getUniqueSuffix(){
            // reset the counter if it is greater than 99999
            if (counter > 99999){
                counter = 0;
            }
            counter = counter + 1; 
            return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
        }

    
    private void populateAxisService() throws org.apache.axis2.AxisFault {

     //creating the Service with a unique name
     _service = new org.apache.axis2.description.AxisService("DictionaryServicesUniversityStructureBudgets" + getUniqueSuffix());
     addAnonymousOperations();

        //creating the operations
        org.apache.axis2.description.AxisOperation __operation;

        _operations = new org.apache.axis2.description.AxisOperation[1];
        
                   __operation = new org.apache.axis2.description.OutInAxisOperation();
                

            __operation.setName(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl", "getUniversityStructureBudgets"));
	    _service.addOperation(__operation);
	    

	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_OUT_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"UTOverTransport\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Lax /></wsp:Policy></sp:Layout><sp:IncludeTimestamp /></wsp:Policy></sp:TransportBinding><sp:SignedSupportingTokens xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:UsernameToken xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\" sp:IncludeToken=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/AlwaysToRecipient\" /></wsp:Policy></sp:SignedSupportingTokens></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    (__operation).getMessage(org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE).getPolicySubject().attachPolicy(getPolicy("<wsp:Policy wsu:Id=\"UTOverTransport\" xmlns:wsp=\"http://schemas.xmlsoap.org/ws/2004/09/policy\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"><wsp:ExactlyOne><wsp:All><sp:TransportBinding xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:TransportToken><wsp:Policy><sp:HttpsToken RequireClientCertificate=\"false\" /></wsp:Policy></sp:TransportToken><sp:AlgorithmSuite xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:Basic256 /></wsp:Policy></sp:AlgorithmSuite><sp:Layout><wsp:Policy><sp:Lax /></wsp:Policy></sp:Layout><sp:IncludeTimestamp /></wsp:Policy></sp:TransportBinding><sp:SignedSupportingTokens xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\"><wsp:Policy><sp:UsernameToken xmlns:sp=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy\" sp:IncludeToken=\"http://schemas.xmlsoap.org/ws/2005/07/securitypolicy/IncludeToken/AlwaysToRecipient\" /></wsp:Policy></sp:SignedSupportingTokens></wsp:All></wsp:ExactlyOne></wsp:Policy>"));
	    
	    
            _operations[0]=__operation;
            
        
        }

    //populates the faults
    private void populateFaults(){
         


    }

    /**
      *Constructor that takes in a configContext
      */

    public DictionaryServicesUniversityStructureBudgetsStub(org.apache.axis2.context.ConfigurationContext configurationContext,
       java.lang.String targetEndpoint)
       throws org.apache.axis2.AxisFault {
         this(configurationContext,targetEndpoint,false);
   }


   /**
     * Constructor that takes in a configContext  and useseperate listner
     */
   public DictionaryServicesUniversityStructureBudgetsStub(org.apache.axis2.context.ConfigurationContext configurationContext,
        java.lang.String targetEndpoint, boolean useSeparateListener)
        throws org.apache.axis2.AxisFault {
         //To populate AxisService
         populateAxisService();
         populateFaults();

        _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext,_service);
        
        _service.applyPolicy();
        
	
        _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
                targetEndpoint));
        _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
        
            //Set the soap version
            _serviceClient.getOptions().setSoapVersionURI(org.apache.axiom.soap.SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
        
    
    }

    /**
     * Default Constructor
     */
    public DictionaryServicesUniversityStructureBudgetsStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
        
                    this(configurationContext,"https://172.18.0.30:8243/services/DictionaryServicesUniversityStructureBudgets.DictionaryServicesUniversityStructureBudgetsHttpsSoap12Endpoint" );
                
    }

    /**
     * Default Constructor
     */
    public DictionaryServicesUniversityStructureBudgetsStub() throws org.apache.axis2.AxisFault {
        
                    this("https://172.18.0.30:8243/services/DictionaryServicesUniversityStructureBudgets.DictionaryServicesUniversityStructureBudgetsHttpsSoap12Endpoint" );
                
    }

    /**
     * Constructor taking the target endpoint
     */
    public DictionaryServicesUniversityStructureBudgetsStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
        this(null,targetEndpoint);
    }



        
                    /**
                     * Auto generated method signature
                     * 
                     * @see pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgets#getUniversityStructureBudgets
                     * @param getUniversityStructureBudgets0
                    
                     */

                    

                            public  pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse getUniversityStructureBudgets(

                            pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets getUniversityStructureBudgets0)
                        

                    throws java.rmi.RemoteException
                    
                    {
              org.apache.axis2.context.MessageContext _messageContext = null;
              try{
               org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
              _operationClient.getOptions().setAction("urn:getUniversityStructureBudgets");
              _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              

              // create a message context
              _messageContext = new org.apache.axis2.context.MessageContext();

              

              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env = null;
                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getUniversityStructureBudgets0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                    "getUniversityStructureBudgets")));
                                                
        //adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // set the message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message contxt to the operation client
        _operationClient.addMessageContext(_messageContext);

        //execute the operation client
        _operationClient.execute(true);

         
               org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
                                           org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
                org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
                
                
                                java.lang.Object object = fromOM(
                                             _returnEnv.getBody().getFirstElement() ,
                                             pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse.class,
                                              getEnvelopeNamespaces(_returnEnv));

                               
                                        return (pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse)object;
                                   
         }catch(org.apache.axis2.AxisFault f){

            org.apache.axiom.om.OMElement faultElt = f.getDetail();
            if (faultElt!=null){
                if (faultExceptionNameMap.containsKey(faultElt.getQName())){
                    //make the fault by reflection
                    try{
                        java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
                        java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                        java.lang.Exception ex=
                                (java.lang.Exception) exceptionClass.newInstance();
                        //message class
                        java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
                        java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                        java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
                        java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                                   new java.lang.Class[]{messageClass});
                        m.invoke(ex,new java.lang.Object[]{messageObject});
                        

                        throw new java.rmi.RemoteException(ex.getMessage(), ex);
                    }catch(java.lang.ClassCastException e){
                       // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.ClassNotFoundException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }catch (java.lang.NoSuchMethodException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    } catch (java.lang.reflect.InvocationTargetException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }  catch (java.lang.IllegalAccessException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }   catch (java.lang.InstantiationException e) {
                        // we cannot intantiate the class - throw the original Axis fault
                        throw f;
                    }
                }else{
                    throw f;
                }
            }else{
                throw f;
            }
            } finally {
                if (_messageContext.getTransportOut() != null) {
                      _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                }
            }
        }
            
                /**
                * Auto generated method signature for Asynchronous Invocations
                * 
                * @see pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgets#startgetUniversityStructureBudgets
                    * @param getUniversityStructureBudgets0
                
                */
                public  void startgetUniversityStructureBudgets(

                 pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets getUniversityStructureBudgets0,

                  final pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsCallbackHandler callback)

                throws java.rmi.RemoteException{

              org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
             _operationClient.getOptions().setAction("urn:getUniversityStructureBudgets");
             _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);

              
              
                  addPropertyToOperationClient(_operationClient,org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR,"&");
              


              // create SOAP envelope with that payload
              org.apache.axiom.soap.SOAPEnvelope env=null;
              final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();

                    
                                    //Style is Doc.
                                    
                                                    
                                                    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
                                                    getUniversityStructureBudgets0,
                                                    optimizeContent(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                    "getUniversityStructureBudgets")));
                                                
        // adding SOAP soap_headers
         _serviceClient.addHeadersToEnvelope(env);
        // create message context with that soap envelope
        _messageContext.setEnvelope(env);

        // add the message context to the operation client
        _operationClient.addMessageContext(_messageContext);


                    
                        _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
                            public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
                            try {
                                org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
                                
                                        java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
                                                                         pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse.class,
                                                                         getEnvelopeNamespaces(resultEnv));
                                        callback.receiveResultgetUniversityStructureBudgets(
                                        (pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse)object);
                                        
                            } catch (org.apache.axis2.AxisFault e) {
                                callback.receiveErrorgetUniversityStructureBudgets(e);
                            }
                            }

                            public void onError(java.lang.Exception error) {
								if (error instanceof org.apache.axis2.AxisFault) {
									org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
									org.apache.axiom.om.OMElement faultElt = f.getDetail();
									if (faultElt!=null){
										if (faultExceptionNameMap.containsKey(faultElt.getQName())){
											//make the fault by reflection
											try{
													java.lang.String exceptionClassName = (java.lang.String)faultExceptionClassNameMap.get(faultElt.getQName());
													java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
													java.lang.Exception ex=
														(java.lang.Exception) exceptionClass.newInstance();
													//message class
													java.lang.String messageClassName = (java.lang.String)faultMessageMap.get(faultElt.getQName());
														java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
													java.lang.Object messageObject = fromOM(faultElt,messageClass,null);
													java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
															new java.lang.Class[]{messageClass});
													m.invoke(ex,new java.lang.Object[]{messageObject});
													
					
										            callback.receiveErrorgetUniversityStructureBudgets(new java.rmi.RemoteException(ex.getMessage(), ex));
                                            } catch(java.lang.ClassCastException e){
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (java.lang.ClassNotFoundException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (java.lang.NoSuchMethodException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (java.lang.reflect.InvocationTargetException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (java.lang.IllegalAccessException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (java.lang.InstantiationException e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            } catch (org.apache.axis2.AxisFault e) {
                                                // we cannot intantiate the class - throw the original Axis fault
                                                callback.receiveErrorgetUniversityStructureBudgets(f);
                                            }
									    } else {
										    callback.receiveErrorgetUniversityStructureBudgets(f);
									    }
									} else {
									    callback.receiveErrorgetUniversityStructureBudgets(f);
									}
								} else {
								    callback.receiveErrorgetUniversityStructureBudgets(error);
								}
                            }

                            public void onFault(org.apache.axis2.context.MessageContext faultContext) {
                                org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
                                onError(fault);
                            }

                            public void onComplete() {
                                try {
                                    _messageContext.getTransportOut().getSender().cleanup(_messageContext);
                                } catch (org.apache.axis2.AxisFault axisFault) {
                                    callback.receiveErrorgetUniversityStructureBudgets(axisFault);
                                }
                            }
                });
                        

          org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
        if ( _operations[0].getMessageReceiver()==null &&  _operationClient.getOptions().isUseSeparateListener()) {
           _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
          _operations[0].setMessageReceiver(
                    _callbackReceiver);
        }

           //execute the operation client
           _operationClient.execute(false);

                    }
                


       /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
       private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
       return returnMap;
    }

    
    ////////////////////////////////////////////////////////////////////////
    
    private static org.apache.neethi.Policy getPolicy (java.lang.String policyString) {
    	java.io.ByteArrayInputStream bais = new java.io.ByteArrayInputStream(policyString.getBytes());
    	return org.apache.neethi.PolicyEngine.getPolicy(bais);
    }
    
    /////////////////////////////////////////////////////////////////////////

    
    
    private javax.xml.namespace.QName[] opNameArray = null;
    private boolean optimizeContent(javax.xml.namespace.QName opName) {
        

        if (opNameArray == null) {
            return false;
        }
        for (int i = 0; i < opNameArray.length; i++) {
            if (opName.equals(opNameArray[i])) {
                return true;   
            }
        }
        return false;
    }
     //https://172.18.0.30:8243/services/DictionaryServicesUniversityStructureBudgets.DictionaryServicesUniversityStructureBudgetsHttpsSoap12Endpoint
        public static class UniversityStructureBudget
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = UniversityStructureBudget
                Namespace URI = http://universityStructureBudgets.esb.simple.com.pl/xsd
                Namespace Prefix = ns1
                */
            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://universityStructureBudgets.esb.simple.com.pl/xsd")){
                return "ns1";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Bd_budzet_id
                        */

                        
                                    protected long localBd_budzet_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_budzet_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBd_budzet_id(){
                               return localBd_budzet_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_budzet_id
                               */
                               public void setBd_budzet_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBd_budzet_idTracker = true;
                                              
                                       } else {
                                          localBd_budzet_idTracker = true;
                                       }
                                   
                                            this.localBd_budzet_id=param;
                                    

                               }
                            

                        /**
                        * field for Bd_budzet_idm
                        */

                        
                                    protected java.lang.String localBd_budzet_idm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_budzet_idmTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBd_budzet_idm(){
                               return localBd_budzet_idm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_budzet_idm
                               */
                               public void setBd_budzet_idm(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBd_budzet_idmTracker = true;
                                       } else {
                                          localBd_budzet_idmTracker = true;
                                              
                                       }
                                   
                                            this.localBd_budzet_idm=param;
                                    

                               }
                            

                        /**
                        * field for Bd_budzet_rodzaj_id
                        */

                        
                                    protected long localBd_budzet_rodzaj_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_budzet_rodzaj_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBd_budzet_rodzaj_id(){
                               return localBd_budzet_rodzaj_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_budzet_rodzaj_id
                               */
                               public void setBd_budzet_rodzaj_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBd_budzet_rodzaj_idTracker = true;
                                              
                                       } else {
                                          localBd_budzet_rodzaj_idTracker = true;
                                       }
                                   
                                            this.localBd_budzet_rodzaj_id=param;
                                    

                               }
                            

                        /**
                        * field for Bd_budzet_rodzaj_zrodla_id
                        */

                        
                                    protected long localBd_budzet_rodzaj_zrodla_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_budzet_rodzaj_zrodla_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBd_budzet_rodzaj_zrodla_id(){
                               return localBd_budzet_rodzaj_zrodla_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_budzet_rodzaj_zrodla_id
                               */
                               public void setBd_budzet_rodzaj_zrodla_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBd_budzet_rodzaj_zrodla_idTracker = true;
                                              
                                       } else {
                                          localBd_budzet_rodzaj_zrodla_idTracker = true;
                                       }
                                   
                                            this.localBd_budzet_rodzaj_zrodla_id=param;
                                    

                               }
                            

                        /**
                        * field for Bd_grupa_rodzaj_id
                        */

                        
                                    protected long localBd_grupa_rodzaj_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_grupa_rodzaj_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBd_grupa_rodzaj_id(){
                               return localBd_grupa_rodzaj_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_grupa_rodzaj_id
                               */
                               public void setBd_grupa_rodzaj_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBd_grupa_rodzaj_idTracker = true;
                                              
                                       } else {
                                          localBd_grupa_rodzaj_idTracker = true;
                                       }
                                   
                                            this.localBd_grupa_rodzaj_id=param;
                                    

                               }
                            

                        /**
                        * field for Bd_rodzaj
                        */

                        
                                    protected java.lang.String localBd_rodzaj ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_rodzajTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBd_rodzaj(){
                               return localBd_rodzaj;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_rodzaj
                               */
                               public void setBd_rodzaj(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBd_rodzajTracker = true;
                                       } else {
                                          localBd_rodzajTracker = true;
                                              
                                       }
                                   
                                            this.localBd_rodzaj=param;
                                    

                               }
                            

                        /**
                        * field for Bd_str_budzet_idn
                        */

                        
                                    protected java.lang.String localBd_str_budzet_idn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_str_budzet_idnTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBd_str_budzet_idn(){
                               return localBd_str_budzet_idn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_str_budzet_idn
                               */
                               public void setBd_str_budzet_idn(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBd_str_budzet_idnTracker = true;
                                       } else {
                                          localBd_str_budzet_idnTracker = true;
                                              
                                       }
                                   
                                            this.localBd_str_budzet_idn=param;
                                    

                               }
                            

                        /**
                        * field for Bd_str_budzet_ids
                        */

                        
                                    protected java.lang.String localBd_str_budzet_ids ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_str_budzet_idsTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBd_str_budzet_ids(){
                               return localBd_str_budzet_ids;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_str_budzet_ids
                               */
                               public void setBd_str_budzet_ids(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBd_str_budzet_idsTracker = true;
                                       } else {
                                          localBd_str_budzet_idsTracker = true;
                                              
                                       }
                                   
                                            this.localBd_str_budzet_ids=param;
                                    

                               }
                            

                        /**
                        * field for Bd_szablon_poz_id
                        */

                        
                                    protected long localBd_szablon_poz_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBd_szablon_poz_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBd_szablon_poz_id(){
                               return localBd_szablon_poz_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bd_szablon_poz_id
                               */
                               public void setBd_szablon_poz_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBd_szablon_poz_idTracker = true;
                                              
                                       } else {
                                          localBd_szablon_poz_idTracker = true;
                                       }
                                   
                                            this.localBd_szablon_poz_id=param;
                                    

                               }
                            

                        /**
                        * field for Bud_w_okr_nazwa
                        */

                        
                                    protected java.lang.String localBud_w_okr_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBud_w_okr_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBud_w_okr_nazwa(){
                               return localBud_w_okr_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Bud_w_okr_nazwa
                               */
                               public void setBud_w_okr_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBud_w_okr_nazwaTracker = true;
                                       } else {
                                          localBud_w_okr_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localBud_w_okr_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Budzet_id
                        */

                        
                                    protected long localBudzet_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBudzet_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getBudzet_id(){
                               return localBudzet_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Budzet_id
                               */
                               public void setBudzet_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localBudzet_idTracker = true;
                                              
                                       } else {
                                          localBudzet_idTracker = true;
                                       }
                                   
                                            this.localBudzet_id=param;
                                    

                               }
                            

                        /**
                        * field for Budzet_idm
                        */

                        
                                    protected java.lang.String localBudzet_idm ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBudzet_idmTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBudzet_idm(){
                               return localBudzet_idm;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Budzet_idm
                               */
                               public void setBudzet_idm(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBudzet_idmTracker = true;
                                       } else {
                                          localBudzet_idmTracker = true;
                                              
                                       }
                                   
                                            this.localBudzet_idm=param;
                                    

                               }
                            

                        /**
                        * field for Budzet_ids
                        */

                        
                                    protected java.lang.String localBudzet_ids ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBudzet_idsTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBudzet_ids(){
                               return localBudzet_ids;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Budzet_ids
                               */
                               public void setBudzet_ids(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBudzet_idsTracker = true;
                                       } else {
                                          localBudzet_idsTracker = true;
                                              
                                       }
                                   
                                            this.localBudzet_ids=param;
                                    

                               }
                            

                        /**
                        * field for Budzet_nazwa
                        */

                        
                                    protected java.lang.String localBudzet_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localBudzet_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getBudzet_nazwa(){
                               return localBudzet_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Budzet_nazwa
                               */
                               public void setBudzet_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localBudzet_nazwaTracker = true;
                                       } else {
                                          localBudzet_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localBudzet_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Czy_aktywna
                        */

                        
                                    protected int localCzy_aktywna ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCzy_aktywnaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getCzy_aktywna(){
                               return localCzy_aktywna;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Czy_aktywna
                               */
                               public void setCzy_aktywna(int param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Integer.MIN_VALUE) {
                                           localCzy_aktywnaTracker = true;
                                              
                                       } else {
                                          localCzy_aktywnaTracker = true;
                                       }
                                   
                                            this.localCzy_aktywna=param;
                                    

                               }
                            

                        /**
                        * field for Dok_bd_str_budzet_idn
                        */

                        
                                    protected java.lang.String localDok_bd_str_budzet_idn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_bd_str_budzet_idnTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDok_bd_str_budzet_idn(){
                               return localDok_bd_str_budzet_idn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_bd_str_budzet_idn
                               */
                               public void setDok_bd_str_budzet_idn(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_bd_str_budzet_idnTracker = true;
                                       } else {
                                          localDok_bd_str_budzet_idnTracker = true;
                                              
                                       }
                                   
                                            this.localDok_bd_str_budzet_idn=param;
                                    

                               }
                            

                        /**
                        * field for Dok_kom_nazwa
                        */

                        
                                    protected java.lang.String localDok_kom_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_kom_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDok_kom_nazwa(){
                               return localDok_kom_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_kom_nazwa
                               */
                               public void setDok_kom_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_kom_nazwaTracker = true;
                                       } else {
                                          localDok_kom_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localDok_kom_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_limit
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_limit ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_limitTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_limit(){
                               return localDok_poz_limit;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_limit
                               */
                               public void setDok_poz_limit(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_limitTracker = true;
                                       } else {
                                          localDok_poz_limitTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_limit=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_nazwa
                        */

                        
                                    protected java.lang.String localDok_poz_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDok_poz_nazwa(){
                               return localDok_poz_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_nazwa
                               */
                               public void setDok_poz_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_nazwaTracker = true;
                                       } else {
                                          localDok_poz_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_nrpoz
                        */

                        
                                    protected int localDok_poz_nrpoz ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_nrpozTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getDok_poz_nrpoz(){
                               return localDok_poz_nrpoz;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_nrpoz
                               */
                               public void setDok_poz_nrpoz(int param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Integer.MIN_VALUE) {
                                           localDok_poz_nrpozTracker = true;
                                              
                                       } else {
                                          localDok_poz_nrpozTracker = true;
                                       }
                                   
                                            this.localDok_poz_nrpoz=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_p_ilosc
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_p_ilosc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_p_iloscTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_p_ilosc(){
                               return localDok_poz_p_ilosc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_p_ilosc
                               */
                               public void setDok_poz_p_ilosc(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_p_iloscTracker = true;
                                       } else {
                                          localDok_poz_p_iloscTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_p_ilosc=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_p_kosz
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_p_kosz ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_p_koszTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_p_kosz(){
                               return localDok_poz_p_kosz;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_p_kosz
                               */
                               public void setDok_poz_p_kosz(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_p_koszTracker = true;
                                       } else {
                                          localDok_poz_p_koszTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_p_kosz=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_r_ilosc
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_r_ilosc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_r_iloscTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_r_ilosc(){
                               return localDok_poz_r_ilosc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_r_ilosc
                               */
                               public void setDok_poz_r_ilosc(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_r_iloscTracker = true;
                                       } else {
                                          localDok_poz_r_iloscTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_r_ilosc=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_r_koszt
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_r_koszt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_r_kosztTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_r_koszt(){
                               return localDok_poz_r_koszt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_r_koszt
                               */
                               public void setDok_poz_r_koszt(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_r_kosztTracker = true;
                                       } else {
                                          localDok_poz_r_kosztTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_r_koszt=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_rez_ilosc
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_rez_ilosc ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_rez_iloscTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_rez_ilosc(){
                               return localDok_poz_rez_ilosc;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_rez_ilosc
                               */
                               public void setDok_poz_rez_ilosc(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_rez_iloscTracker = true;
                                       } else {
                                          localDok_poz_rez_iloscTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_rez_ilosc=param;
                                    

                               }
                            

                        /**
                        * field for Dok_poz_rez_koszt
                        */

                        
                                    protected java.math.BigDecimal localDok_poz_rez_koszt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDok_poz_rez_kosztTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getDok_poz_rez_koszt(){
                               return localDok_poz_rez_koszt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Dok_poz_rez_koszt
                               */
                               public void setDok_poz_rez_koszt(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localDok_poz_rez_kosztTracker = true;
                                       } else {
                                          localDok_poz_rez_kosztTracker = true;
                                              
                                       }
                                   
                                            this.localDok_poz_rez_koszt=param;
                                    

                               }
                            

                        /**
                        * field for Okrrozl_id
                        */

                        
                                    protected long localOkrrozl_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localOkrrozl_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getOkrrozl_id(){
                               return localOkrrozl_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Okrrozl_id
                               */
                               public void setOkrrozl_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localOkrrozl_idTracker = true;
                                              
                                       } else {
                                          localOkrrozl_idTracker = true;
                                       }
                                   
                                            this.localOkrrozl_id=param;
                                    

                               }
                            

                        /**
                        * field for Pozycja_podrzedna_id
                        */

                        
                                    protected long localPozycja_podrzedna_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPozycja_podrzedna_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPozycja_podrzedna_id(){
                               return localPozycja_podrzedna_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Pozycja_podrzedna_id
                               */
                               public void setPozycja_podrzedna_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localPozycja_podrzedna_idTracker = true;
                                              
                                       } else {
                                          localPozycja_podrzedna_idTracker = true;
                                       }
                                   
                                            this.localPozycja_podrzedna_id=param;
                                    

                               }
                            

                        /**
                        * field for Str_bud_nazwa
                        */

                        
                                    protected java.lang.String localStr_bud_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localStr_bud_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getStr_bud_nazwa(){
                               return localStr_bud_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Str_bud_nazwa
                               */
                               public void setStr_bud_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localStr_bud_nazwaTracker = true;
                                       } else {
                                          localStr_bud_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localStr_bud_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Wspolczynnik_pozycji
                        */

                        
                                    protected int localWspolczynnik_pozycji ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWspolczynnik_pozycjiTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return int
                           */
                           public  int getWspolczynnik_pozycji(){
                               return localWspolczynnik_pozycji;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Wspolczynnik_pozycji
                               */
                               public void setWspolczynnik_pozycji(int param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Integer.MIN_VALUE) {
                                           localWspolczynnik_pozycjiTracker = true;
                                              
                                       } else {
                                          localWspolczynnik_pozycjiTracker = true;
                                       }
                                   
                                            this.localWspolczynnik_pozycji=param;
                                    

                               }
                            

                        /**
                        * field for Wytwor_id
                        */

                        
                                    protected long localWytwor_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localWytwor_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getWytwor_id(){
                               return localWytwor_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Wytwor_id
                               */
                               public void setWytwor_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localWytwor_idTracker = true;
                                              
                                       } else {
                                          localWytwor_idTracker = true;
                                       }
                                   
                                            this.localWytwor_id=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_id
                        */

                        
                                    protected long localZrodlo_id ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_idTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getZrodlo_id(){
                               return localZrodlo_id;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_id
                               */
                               public void setZrodlo_id(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localZrodlo_idTracker = true;
                                              
                                       } else {
                                          localZrodlo_idTracker = true;
                                       }
                                   
                                            this.localZrodlo_id=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_idn
                        */

                        
                                    protected java.lang.String localZrodlo_idn ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_idnTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getZrodlo_idn(){
                               return localZrodlo_idn;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_idn
                               */
                               public void setZrodlo_idn(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localZrodlo_idnTracker = true;
                                       } else {
                                          localZrodlo_idnTracker = true;
                                              
                                       }
                                   
                                            this.localZrodlo_idn=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_nazwa
                        */

                        
                                    protected java.lang.String localZrodlo_nazwa ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_nazwaTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getZrodlo_nazwa(){
                               return localZrodlo_nazwa;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_nazwa
                               */
                               public void setZrodlo_nazwa(java.lang.String param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localZrodlo_nazwaTracker = true;
                                       } else {
                                          localZrodlo_nazwaTracker = true;
                                              
                                       }
                                   
                                            this.localZrodlo_nazwa=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_p_koszt
                        */

                        
                                    protected java.math.BigDecimal localZrodlo_p_koszt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_p_kosztTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getZrodlo_p_koszt(){
                               return localZrodlo_p_koszt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_p_koszt
                               */
                               public void setZrodlo_p_koszt(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localZrodlo_p_kosztTracker = true;
                                       } else {
                                          localZrodlo_p_kosztTracker = true;
                                              
                                       }
                                   
                                            this.localZrodlo_p_koszt=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_r_koszt
                        */

                        
                                    protected java.math.BigDecimal localZrodlo_r_koszt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_r_kosztTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getZrodlo_r_koszt(){
                               return localZrodlo_r_koszt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_r_koszt
                               */
                               public void setZrodlo_r_koszt(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localZrodlo_r_kosztTracker = true;
                                       } else {
                                          localZrodlo_r_kosztTracker = true;
                                              
                                       }
                                   
                                            this.localZrodlo_r_koszt=param;
                                    

                               }
                            

                        /**
                        * field for Zrodlo_rez_koszt
                        */

                        
                                    protected java.math.BigDecimal localZrodlo_rez_koszt ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localZrodlo_rez_kosztTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return java.math.BigDecimal
                           */
                           public  java.math.BigDecimal getZrodlo_rez_koszt(){
                               return localZrodlo_rez_koszt;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Zrodlo_rez_koszt
                               */
                               public void setZrodlo_rez_koszt(java.math.BigDecimal param){
                            
                                       if (param != null){
                                          //update the setting tracker
                                          localZrodlo_rez_kosztTracker = true;
                                       } else {
                                          localZrodlo_rez_kosztTracker = true;
                                              
                                       }
                                   
                                            this.localZrodlo_rez_koszt=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,parentQName){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       UniversityStructureBudget.this.serialize(parentQName,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               parentQName,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://universityStructureBudgets.esb.simple.com.pl/xsd");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":UniversityStructureBudget",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "UniversityStructureBudget",
                           xmlWriter);
                   }

               
                   }
                if (localBd_budzet_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_budzet_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_budzet_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_budzet_id");
                                    }
                                
                                               if (localBd_budzet_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_budzet_idmTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_budzet_idm", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_budzet_idm");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_budzet_idm");
                                    }
                                

                                          if (localBd_budzet_idm==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBd_budzet_idm);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_budzet_rodzaj_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_budzet_rodzaj_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_budzet_rodzaj_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_budzet_rodzaj_id");
                                    }
                                
                                               if (localBd_budzet_rodzaj_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_rodzaj_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_budzet_rodzaj_zrodla_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_budzet_rodzaj_zrodla_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_budzet_rodzaj_zrodla_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_budzet_rodzaj_zrodla_id");
                                    }
                                
                                               if (localBd_budzet_rodzaj_zrodla_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_rodzaj_zrodla_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_grupa_rodzaj_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_grupa_rodzaj_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_grupa_rodzaj_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_grupa_rodzaj_id");
                                    }
                                
                                               if (localBd_grupa_rodzaj_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_grupa_rodzaj_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_rodzajTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_rodzaj", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_rodzaj");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_rodzaj");
                                    }
                                

                                          if (localBd_rodzaj==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBd_rodzaj);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_str_budzet_idnTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_str_budzet_idn", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_str_budzet_idn");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_str_budzet_idn");
                                    }
                                

                                          if (localBd_str_budzet_idn==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBd_str_budzet_idn);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_str_budzet_idsTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_str_budzet_ids", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_str_budzet_ids");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_str_budzet_ids");
                                    }
                                

                                          if (localBd_str_budzet_ids==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBd_str_budzet_ids);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBd_szablon_poz_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bd_szablon_poz_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bd_szablon_poz_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bd_szablon_poz_id");
                                    }
                                
                                               if (localBd_szablon_poz_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_szablon_poz_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBud_w_okr_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"bud_w_okr_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"bud_w_okr_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("bud_w_okr_nazwa");
                                    }
                                

                                          if (localBud_w_okr_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBud_w_okr_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBudzet_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"budzet_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"budzet_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("budzet_id");
                                    }
                                
                                               if (localBudzet_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBudzet_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBudzet_idmTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"budzet_idm", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"budzet_idm");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("budzet_idm");
                                    }
                                

                                          if (localBudzet_idm==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBudzet_idm);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBudzet_idsTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"budzet_ids", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"budzet_ids");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("budzet_ids");
                                    }
                                

                                          if (localBudzet_ids==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBudzet_ids);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localBudzet_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"budzet_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"budzet_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("budzet_nazwa");
                                    }
                                

                                          if (localBudzet_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localBudzet_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCzy_aktywnaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"czy_aktywna", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"czy_aktywna");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("czy_aktywna");
                                    }
                                
                                               if (localCzy_aktywna==java.lang.Integer.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCzy_aktywna));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_bd_str_budzet_idnTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_bd_str_budzet_idn", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_bd_str_budzet_idn");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_bd_str_budzet_idn");
                                    }
                                

                                          if (localDok_bd_str_budzet_idn==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDok_bd_str_budzet_idn);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_kom_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_kom_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_kom_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_kom_nazwa");
                                    }
                                

                                          if (localDok_kom_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDok_kom_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_limitTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_limit", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_limit");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_limit");
                                    }
                                

                                          if (localDok_poz_limit==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_limit));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_nazwa");
                                    }
                                

                                          if (localDok_poz_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDok_poz_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_nrpozTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_nrpoz", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_nrpoz");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_nrpoz");
                                    }
                                
                                               if (localDok_poz_nrpoz==java.lang.Integer.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_nrpoz));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_p_iloscTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_p_ilosc", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_p_ilosc");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_p_ilosc");
                                    }
                                

                                          if (localDok_poz_p_ilosc==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_p_ilosc));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_p_koszTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_p_kosz", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_p_kosz");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_p_kosz");
                                    }
                                

                                          if (localDok_poz_p_kosz==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_p_kosz));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_r_iloscTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_r_ilosc", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_r_ilosc");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_r_ilosc");
                                    }
                                

                                          if (localDok_poz_r_ilosc==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_r_ilosc));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_r_kosztTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_r_koszt", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_r_koszt");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_r_koszt");
                                    }
                                

                                          if (localDok_poz_r_koszt==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_r_koszt));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_rez_iloscTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_rez_ilosc", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_rez_ilosc");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_rez_ilosc");
                                    }
                                

                                          if (localDok_poz_rez_ilosc==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_rez_ilosc));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDok_poz_rez_kosztTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"dok_poz_rez_koszt", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"dok_poz_rez_koszt");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("dok_poz_rez_koszt");
                                    }
                                

                                          if (localDok_poz_rez_koszt==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_rez_koszt));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localOkrrozl_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"okrrozl_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"okrrozl_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("okrrozl_id");
                                    }
                                
                                               if (localOkrrozl_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOkrrozl_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPozycja_podrzedna_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"pozycja_podrzedna_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"pozycja_podrzedna_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("pozycja_podrzedna_id");
                                    }
                                
                                               if (localPozycja_podrzedna_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPozycja_podrzedna_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localStr_bud_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"str_bud_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"str_bud_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("str_bud_nazwa");
                                    }
                                

                                          if (localStr_bud_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localStr_bud_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWspolczynnik_pozycjiTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"wspolczynnik_pozycji", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"wspolczynnik_pozycji");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("wspolczynnik_pozycji");
                                    }
                                
                                               if (localWspolczynnik_pozycji==java.lang.Integer.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWspolczynnik_pozycji));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localWytwor_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"wytwor_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"wytwor_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("wytwor_id");
                                    }
                                
                                               if (localWytwor_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWytwor_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_idTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_id", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_id");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_id");
                                    }
                                
                                               if (localZrodlo_id==java.lang.Long.MIN_VALUE) {
                                           
                                                         writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_id));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_idnTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_idn", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_idn");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_idn");
                                    }
                                

                                          if (localZrodlo_idn==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localZrodlo_idn);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_nazwaTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_nazwa", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_nazwa");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_nazwa");
                                    }
                                

                                          if (localZrodlo_nazwa==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localZrodlo_nazwa);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_p_kosztTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_p_koszt", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_p_koszt");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_p_koszt");
                                    }
                                

                                          if (localZrodlo_p_koszt==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_p_koszt));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_r_kosztTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_r_koszt", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_r_koszt");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_r_koszt");
                                    }
                                

                                          if (localZrodlo_r_koszt==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_r_koszt));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localZrodlo_rez_kosztTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl/xsd";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"zrodlo_rez_koszt", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"zrodlo_rez_koszt");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("zrodlo_rez_koszt");
                                    }
                                

                                          if (localZrodlo_rez_koszt==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_rez_koszt));
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localBd_budzet_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_budzet_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_id));
                            } if (localBd_budzet_idmTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_budzet_idm"));
                                 
                                         elementList.add(localBd_budzet_idm==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_idm));
                                    } if (localBd_budzet_rodzaj_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_budzet_rodzaj_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_rodzaj_id));
                            } if (localBd_budzet_rodzaj_zrodla_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_budzet_rodzaj_zrodla_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_budzet_rodzaj_zrodla_id));
                            } if (localBd_grupa_rodzaj_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_grupa_rodzaj_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_grupa_rodzaj_id));
                            } if (localBd_rodzajTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_rodzaj"));
                                 
                                         elementList.add(localBd_rodzaj==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_rodzaj));
                                    } if (localBd_str_budzet_idnTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_str_budzet_idn"));
                                 
                                         elementList.add(localBd_str_budzet_idn==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_str_budzet_idn));
                                    } if (localBd_str_budzet_idsTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_str_budzet_ids"));
                                 
                                         elementList.add(localBd_str_budzet_ids==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_str_budzet_ids));
                                    } if (localBd_szablon_poz_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bd_szablon_poz_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBd_szablon_poz_id));
                            } if (localBud_w_okr_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "bud_w_okr_nazwa"));
                                 
                                         elementList.add(localBud_w_okr_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBud_w_okr_nazwa));
                                    } if (localBudzet_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "budzet_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBudzet_id));
                            } if (localBudzet_idmTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "budzet_idm"));
                                 
                                         elementList.add(localBudzet_idm==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBudzet_idm));
                                    } if (localBudzet_idsTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "budzet_ids"));
                                 
                                         elementList.add(localBudzet_ids==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBudzet_ids));
                                    } if (localBudzet_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "budzet_nazwa"));
                                 
                                         elementList.add(localBudzet_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localBudzet_nazwa));
                                    } if (localCzy_aktywnaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "czy_aktywna"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localCzy_aktywna));
                            } if (localDok_bd_str_budzet_idnTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_bd_str_budzet_idn"));
                                 
                                         elementList.add(localDok_bd_str_budzet_idn==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_bd_str_budzet_idn));
                                    } if (localDok_kom_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_kom_nazwa"));
                                 
                                         elementList.add(localDok_kom_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_kom_nazwa));
                                    } if (localDok_poz_limitTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_limit"));
                                 
                                         elementList.add(localDok_poz_limit==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_limit));
                                    } if (localDok_poz_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_nazwa"));
                                 
                                         elementList.add(localDok_poz_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_nazwa));
                                    } if (localDok_poz_nrpozTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_nrpoz"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_nrpoz));
                            } if (localDok_poz_p_iloscTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_p_ilosc"));
                                 
                                         elementList.add(localDok_poz_p_ilosc==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_p_ilosc));
                                    } if (localDok_poz_p_koszTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_p_kosz"));
                                 
                                         elementList.add(localDok_poz_p_kosz==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_p_kosz));
                                    } if (localDok_poz_r_iloscTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_r_ilosc"));
                                 
                                         elementList.add(localDok_poz_r_ilosc==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_r_ilosc));
                                    } if (localDok_poz_r_kosztTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_r_koszt"));
                                 
                                         elementList.add(localDok_poz_r_koszt==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_r_koszt));
                                    } if (localDok_poz_rez_iloscTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_rez_ilosc"));
                                 
                                         elementList.add(localDok_poz_rez_ilosc==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_rez_ilosc));
                                    } if (localDok_poz_rez_kosztTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "dok_poz_rez_koszt"));
                                 
                                         elementList.add(localDok_poz_rez_koszt==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localDok_poz_rez_koszt));
                                    } if (localOkrrozl_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "okrrozl_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localOkrrozl_id));
                            } if (localPozycja_podrzedna_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "pozycja_podrzedna_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPozycja_podrzedna_id));
                            } if (localStr_bud_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "str_bud_nazwa"));
                                 
                                         elementList.add(localStr_bud_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localStr_bud_nazwa));
                                    } if (localWspolczynnik_pozycjiTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "wspolczynnik_pozycji"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWspolczynnik_pozycji));
                            } if (localWytwor_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "wytwor_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localWytwor_id));
                            } if (localZrodlo_idTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_id"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_id));
                            } if (localZrodlo_idnTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_idn"));
                                 
                                         elementList.add(localZrodlo_idn==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_idn));
                                    } if (localZrodlo_nazwaTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_nazwa"));
                                 
                                         elementList.add(localZrodlo_nazwa==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_nazwa));
                                    } if (localZrodlo_p_kosztTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_p_koszt"));
                                 
                                         elementList.add(localZrodlo_p_koszt==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_p_koszt));
                                    } if (localZrodlo_r_kosztTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_r_koszt"));
                                 
                                         elementList.add(localZrodlo_r_koszt==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_r_koszt));
                                    } if (localZrodlo_rez_kosztTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd",
                                                                      "zrodlo_rez_koszt"));
                                 
                                         elementList.add(localZrodlo_rez_koszt==null?null:
                                         org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localZrodlo_rez_koszt));
                                    }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static UniversityStructureBudget parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            UniversityStructureBudget object =
                new UniversityStructureBudget();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"UniversityStructureBudget".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (UniversityStructureBudget)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_budzet_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_budzet_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBd_budzet_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBd_budzet_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_budzet_idm").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_budzet_idm(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_budzet_rodzaj_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_budzet_rodzaj_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBd_budzet_rodzaj_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBd_budzet_rodzaj_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_budzet_rodzaj_zrodla_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_budzet_rodzaj_zrodla_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBd_budzet_rodzaj_zrodla_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBd_budzet_rodzaj_zrodla_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_grupa_rodzaj_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_grupa_rodzaj_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBd_grupa_rodzaj_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBd_grupa_rodzaj_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_rodzaj").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_rodzaj(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_str_budzet_idn").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_str_budzet_idn(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_str_budzet_ids").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_str_budzet_ids(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bd_szablon_poz_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBd_szablon_poz_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBd_szablon_poz_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBd_szablon_poz_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","bud_w_okr_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBud_w_okr_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","budzet_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBudzet_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setBudzet_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setBudzet_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","budzet_idm").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBudzet_idm(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","budzet_ids").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBudzet_ids(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","budzet_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setBudzet_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","czy_aktywna").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCzy_aktywna(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setCzy_aktywna(java.lang.Integer.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setCzy_aktywna(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_bd_str_budzet_idn").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_bd_str_budzet_idn(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_kom_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_kom_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_limit").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_limit(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_nrpoz").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_nrpoz(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setDok_poz_nrpoz(java.lang.Integer.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setDok_poz_nrpoz(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_p_ilosc").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_p_ilosc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_p_kosz").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_p_kosz(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_r_ilosc").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_r_ilosc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_r_koszt").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_r_koszt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_rez_ilosc").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_rez_ilosc(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","dok_poz_rez_koszt").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDok_poz_rez_koszt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","okrrozl_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setOkrrozl_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setOkrrozl_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setOkrrozl_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","pozycja_podrzedna_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPozycja_podrzedna_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setPozycja_podrzedna_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPozycja_podrzedna_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","str_bud_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setStr_bud_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","wspolczynnik_pozycji").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setWspolczynnik_pozycji(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToInt(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setWspolczynnik_pozycji(java.lang.Integer.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setWspolczynnik_pozycji(java.lang.Integer.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","wytwor_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setWytwor_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setWytwor_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setWytwor_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_id").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_id(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                            
                                       } else {
                                           
                                           
                                                   object.setZrodlo_id(java.lang.Long.MIN_VALUE);
                                               
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setZrodlo_id(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_idn").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_idn(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_nazwa").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_nazwa(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_p_koszt").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_p_koszt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_r_koszt").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_r_koszt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl/xsd","zrodlo_rez_koszt").equals(reader.getName())){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setZrodlo_rez_koszt(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToDecimal(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://universityStructureBudgets.esb.simple.com.pl/xsd".equals(namespaceURI) &&
                  "UniversityStructureBudget".equals(typeName)){
                   
                            return  UniversityStructureBudget.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    
        public static class GetUniversityStructureBudgetsResponse
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://universityStructureBudgets.esb.simple.com.pl",
                "getUniversityStructureBudgetsResponse",
                "ns2");

            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://universityStructureBudgets.esb.simple.com.pl")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for _return
                        * This was an Array!
                        */

                        
                                    protected UniversityStructureBudget[] local_return ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean local_returnTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return UniversityStructureBudget[]
                           */
                           public  UniversityStructureBudget[] get_return(){
                               return local_return;
                           }

                           
                        


                               
                              /**
                               * validate the array for _return
                               */
                              protected void validate_return(UniversityStructureBudget[] param){
                             
                              }


                             /**
                              * Auto generated setter method
                              * @param param _return
                              */
                              public void set_return(UniversityStructureBudget[] param){
                              
                                   validate_return(param);

                               
                                          if (param != null){
                                             //update the setting tracker
                                             local_returnTracker = true;
                                          } else {
                                             local_returnTracker = true;
                                                 
                                          }
                                      
                                      this.local_return=param;
                              }

                               
                             
                             /**
                             * Auto generated add method for the array for convenience
                             * @param param UniversityStructureBudget
                             */
                             public void add_return(UniversityStructureBudget param){
                                   if (local_return == null){
                                   local_return = new UniversityStructureBudget[]{};
                                   }

                            
                                 //update the setting tracker
                                local_returnTracker = true;
                            

                               java.util.List list =
                            org.apache.axis2.databinding.utils.ConverterUtil.toList(local_return);
                               list.add(param);
                               this.local_return =
                             (UniversityStructureBudget[])list.toArray(
                            new UniversityStructureBudget[list.size()]);

                             }
                             

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
                org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       GetUniversityStructureBudgetsResponse.this.serialize(MY_QNAME,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               MY_QNAME,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://universityStructureBudgets.esb.simple.com.pl");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":getUniversityStructureBudgetsResponse",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "getUniversityStructureBudgetsResponse",
                           xmlWriter);
                   }

               
                   }
                if (local_returnTracker){
                                       if (local_return!=null){
                                            for (int i = 0;i < local_return.length;i++){
                                                if (local_return[i] != null){
                                                 local_return[i].serialize(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl","return"),
                                                           factory,xmlWriter);
                                                } else {
                                                   
                                                            // write null attribute
                                                            java.lang.String namespace2 = "http://universityStructureBudgets.esb.simple.com.pl";
                                                            if (! namespace2.equals("")) {
                                                                java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                                if (prefix2 == null) {
                                                                    prefix2 = generatePrefix(namespace2);

                                                                    xmlWriter.writeStartElement(prefix2,"return", namespace2);
                                                                    xmlWriter.writeNamespace(prefix2, namespace2);
                                                                    xmlWriter.setPrefix(prefix2, namespace2);

                                                                } else {
                                                                    xmlWriter.writeStartElement(namespace2,"return");
                                                                }

                                                            } else {
                                                                xmlWriter.writeStartElement("return");
                                                            }

                                                           // write the nil attribute
                                                           writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                           xmlWriter.writeEndElement();
                                                    
                                                }

                                            }
                                     } else {
                                        
                                                // write null attribute
                                                java.lang.String namespace2 = "http://universityStructureBudgets.esb.simple.com.pl";
                                                if (! namespace2.equals("")) {
                                                    java.lang.String prefix2 = xmlWriter.getPrefix(namespace2);

                                                    if (prefix2 == null) {
                                                        prefix2 = generatePrefix(namespace2);

                                                        xmlWriter.writeStartElement(prefix2,"return", namespace2);
                                                        xmlWriter.writeNamespace(prefix2, namespace2);
                                                        xmlWriter.setPrefix(prefix2, namespace2);

                                                    } else {
                                                        xmlWriter.writeStartElement(namespace2,"return");
                                                    }

                                                } else {
                                                    xmlWriter.writeStartElement("return");
                                                }

                                               // write the nil attribute
                                               writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                               xmlWriter.writeEndElement();
                                        
                                    }
                                 }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (local_returnTracker){
                             if (local_return!=null) {
                                 for (int i = 0;i < local_return.length;i++){

                                    if (local_return[i] != null){
                                         elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                                          "return"));
                                         elementList.add(local_return[i]);
                                    } else {
                                        
                                                elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                                          "return"));
                                                elementList.add(null);
                                            
                                    }

                                 }
                             } else {
                                 
                                        elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                                          "return"));
                                        elementList.add(local_return);
                                    
                             }

                        }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static GetUniversityStructureBudgetsResponse parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            GetUniversityStructureBudgetsResponse object =
                new GetUniversityStructureBudgetsResponse();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"getUniversityStructureBudgetsResponse".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetUniversityStructureBudgetsResponse)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                        java.util.ArrayList list1 = new java.util.ArrayList();
                    
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl","return").equals(reader.getName())){
                                
                                    
                                    
                                    // Process the array and step past its final element's end.
                                    
                                                          nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                          if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                              list1.add(null);
                                                              reader.next();
                                                          } else {
                                                        list1.add(UniversityStructureBudget.Factory.parse(reader));
                                                                }
                                                        //loop until we find a start element that is not part of this array
                                                        boolean loopDone1 = false;
                                                        while(!loopDone1){
                                                            // We should be at the end element, but make sure
                                                            while (!reader.isEndElement())
                                                                reader.next();
                                                            // Step out of this element
                                                            reader.next();
                                                            // Step to next element event.
                                                            while (!reader.isStartElement() && !reader.isEndElement())
                                                                reader.next();
                                                            if (reader.isEndElement()){
                                                                //two continuous end elements means we are exiting the xml structure
                                                                loopDone1 = true;
                                                            } else {
                                                                if (new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl","return").equals(reader.getName())){
                                                                    
                                                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                                                          list1.add(null);
                                                                          reader.next();
                                                                      } else {
                                                                    list1.add(UniversityStructureBudget.Factory.parse(reader));
                                                                        }
                                                                }else{
                                                                    loopDone1 = true;
                                                                }
                                                            }
                                                        }
                                                        // call the converter utility  to convert and set the array
                                                        
                                                        object.set_return((UniversityStructureBudget[])
                                                            org.apache.axis2.databinding.utils.ConverterUtil.convertToArray(
                                                                UniversityStructureBudget.class,
                                                                list1));
                                                            
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
        public static class GetUniversityStructureBudgets
        implements org.apache.axis2.databinding.ADBBean{
        
                public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName(
                "http://universityStructureBudgets.esb.simple.com.pl",
                "getUniversityStructureBudgets",
                "ns2");

            

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://universityStructureBudgets.esb.simple.com.pl")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        

                        /**
                        * field for Page
                        */

                        
                                    protected long localPage ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPageTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPage(){
                               return localPage;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Page
                               */
                               public void setPage(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localPageTracker = false;
                                              
                                       } else {
                                          localPageTracker = true;
                                       }
                                   
                                            this.localPage=param;
                                    

                               }
                            

                        /**
                        * field for PageSize
                        */

                        
                                    protected long localPageSize ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPageSizeTracker = false ;
                           

                           /**
                           * Auto generated getter method
                           * @return long
                           */
                           public  long getPageSize(){
                               return localPageSize;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PageSize
                               */
                               public void setPageSize(long param){
                            
                                       // setting primitive attribute tracker to true
                                       
                                               if (param==java.lang.Long.MIN_VALUE) {
                                           localPageSizeTracker = false;
                                              
                                       } else {
                                          localPageSizeTracker = true;
                                       }
                                   
                                            this.localPageSize=param;
                                    

                               }
                            

     /**
     * isReaderMTOMAware
     * @return true if the reader supports MTOM
     */
   public static boolean isReaderMTOMAware(javax.xml.stream.XMLStreamReader reader) {
        boolean isReaderMTOMAware = false;
        
        try{
          isReaderMTOMAware = java.lang.Boolean.TRUE.equals(reader.getProperty(org.apache.axiom.om.OMConstants.IS_DATA_HANDLERS_AWARE));
        }catch(java.lang.IllegalArgumentException e){
          isReaderMTOMAware = false;
        }
        return isReaderMTOMAware;
   }
     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
                org.apache.axiom.om.OMDataSource dataSource =
                       new org.apache.axis2.databinding.ADBDataSource(this,MY_QNAME){

                 public void serialize(org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
                       GetUniversityStructureBudgets.this.serialize(MY_QNAME,factory,xmlWriter);
                 }
               };
               return new org.apache.axiom.om.impl.llom.OMSourcedElementImpl(
               MY_QNAME,factory,dataSource);
            
       }

         public void serialize(final javax.xml.namespace.QName parentQName,
                                       final org.apache.axiom.om.OMFactory factory,
                                       org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,factory,xmlWriter,false);
         }

         public void serialize(final javax.xml.namespace.QName parentQName,
                               final org.apache.axiom.om.OMFactory factory,
                               org.apache.axis2.databinding.utils.writer.MTOMAwareXMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();

                    if ((namespace != null) && (namespace.trim().length() > 0)) {
                        java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
                        if (writerPrefix != null) {
                            xmlWriter.writeStartElement(namespace, parentQName.getLocalPart());
                        } else {
                            if (prefix == null) {
                                prefix = generatePrefix(namespace);
                            }

                            xmlWriter.writeStartElement(prefix, parentQName.getLocalPart(), namespace);
                            xmlWriter.writeNamespace(prefix, namespace);
                            xmlWriter.setPrefix(prefix, namespace);
                        }
                    } else {
                        xmlWriter.writeStartElement(parentQName.getLocalPart());
                    }
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://universityStructureBudgets.esb.simple.com.pl");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":getUniversityStructureBudgets",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "getUniversityStructureBudgets",
                           xmlWriter);
                   }

               
                   }
                if (localPageTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"page", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"page");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("page");
                                    }
                                
                                               if (localPage==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("page cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPage));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPageSizeTracker){
                                    namespace = "http://universityStructureBudgets.esb.simple.com.pl";
                                    if (! namespace.equals("")) {
                                        prefix = xmlWriter.getPrefix(namespace);

                                        if (prefix == null) {
                                            prefix = generatePrefix(namespace);

                                            xmlWriter.writeStartElement(prefix,"pageSize", namespace);
                                            xmlWriter.writeNamespace(prefix, namespace);
                                            xmlWriter.setPrefix(prefix, namespace);

                                        } else {
                                            xmlWriter.writeStartElement(namespace,"pageSize");
                                        }

                                    } else {
                                        xmlWriter.writeStartElement("pageSize");
                                    }
                                
                                               if (localPageSize==java.lang.Long.MIN_VALUE) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("pageSize cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPageSize));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             }
                    xmlWriter.writeEndElement();
               

        }

         /**
          * Util method to write an attribute with the ns prefix
          */
          private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
              if (xmlWriter.getPrefix(namespace) == null) {
                       xmlWriter.writeNamespace(prefix, namespace);
                       xmlWriter.setPrefix(prefix, namespace);

              }

              xmlWriter.writeAttribute(namespace,attName,attValue);

         }

        /**
          * Util method to write an attribute without the ns prefix
          */
          private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                      java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
                if (namespace.equals(""))
              {
                  xmlWriter.writeAttribute(attName,attValue);
              }
              else
              {
                  registerPrefix(xmlWriter, namespace);
                  xmlWriter.writeAttribute(namespace,attName,attValue);
              }
          }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


         /**
         * Register a namespace prefix
         */
         private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
                java.lang.String prefix = xmlWriter.getPrefix(namespace);

                if (prefix == null) {
                    prefix = generatePrefix(namespace);

                    while (xmlWriter.getNamespaceContext().getNamespaceURI(prefix) != null) {
                        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                    }

                    xmlWriter.writeNamespace(prefix, namespace);
                    xmlWriter.setPrefix(prefix, namespace);
                }

                return prefix;
            }


  
        /**
        * databinding method to get an XML representation of this object
        *
        */
        public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
                    throws org.apache.axis2.databinding.ADBException{


        
                 java.util.ArrayList elementList = new java.util.ArrayList();
                 java.util.ArrayList attribList = new java.util.ArrayList();

                 if (localPageTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                                      "page"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPage));
                            } if (localPageSizeTracker){
                                      elementList.add(new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl",
                                                                      "pageSize"));
                                 
                                elementList.add(
                                   org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPageSize));
                            }

                return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
            
            

        }

  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static GetUniversityStructureBudgets parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            GetUniversityStructureBudgets object =
                new GetUniversityStructureBudgets();

            int event;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"getUniversityStructureBudgets".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (GetUniversityStructureBudgets)ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                 
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl","page").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPage(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPage(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://universityStructureBudgets.esb.simple.com.pl","pageSize").equals(reader.getName())){
                                
                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPageSize(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToLong(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                               object.setPageSize(java.lang.Long.MIN_VALUE);
                                           
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // A start element we are not expecting indicates a trailing invalid property
                                throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getLocalName());
                            



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
          
            private  org.apache.axiom.om.OMElement  toOM(pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
            private  org.apache.axiom.om.OMElement  toOM(pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                                    
                                        private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets param, boolean optimizeContent)
                                        throws org.apache.axis2.AxisFault{

                                             
                                                    try{

                                                            org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                                                            emptyEnvelope.getBody().addChild(param.getOMElement(pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets.MY_QNAME,factory));
                                                            return emptyEnvelope;
                                                        } catch(org.apache.axis2.databinding.ADBException e){
                                                            throw org.apache.axis2.AxisFault.makeFault(e);
                                                        }
                                                

                                        }
                                
                             
                             /* methods to provide back word compatibility */

                             


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets.class.equals(type)){
                
                           return pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgets.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse.class.equals(type)){
                
                           return pl.compan.docusafe.ws.ul.DictionaryServicesUniversityStructureBudgetsStub.GetUniversityStructureBudgetsResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    
   }
   