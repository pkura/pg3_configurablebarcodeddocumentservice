
/**
 * DictionaryServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.ul;

    /**
     *  DictionaryServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemCountByParentID method
            * override this method for handling normal response from getProjectBudgetItemCountByParentID operation
            */
           public void receiveResultgetProjectBudgetItemCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemCountByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCount method
            * override this method for handling normal response from getProjectFundingSourceCount operation
            */
           public void receiveResultgetProjectFundingSourceCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCount operation
           */
            public void receiveErrorgetProjectFundingSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCountByParentID method
            * override this method for handling normal response from getProjectBugdetCountByParentID operation
            */
           public void receiveResultgetProjectBugdetCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCountByParentID operation
           */
            public void receiveErrorgetProjectBugdetCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByIDM method
            * override this method for handling normal response from getProjectBugdetByIDM operation
            */
           public void receiveResultgetProjectBugdetByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByIDM operation
           */
            public void receiveErrorgetProjectBugdetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCountByParentIDM method
            * override this method for handling normal response from getProjectFundingSourceCountByParentIDM operation
            */
           public void receiveResultgetProjectFundingSourceCountByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCountByParentIDM operation
           */
            public void receiveErrorgetProjectFundingSourceCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCountByParentIDM method
            * override this method for handling normal response from getProjectBugdetCountByParentIDM operation
            */
           public void receiveResultgetProjectBugdetCountByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCountByParentIDM operation
           */
            public void receiveErrorgetProjectBugdetCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKind method
            * override this method for handling normal response from getPurchasePlanKind operation
            */
           public void receiveResultgetPurchasePlanKind(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKind operation
           */
            public void receiveErrorgetPurchasePlanKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationType method
            * override this method for handling normal response from getApplicationForReservationType operation
            */
           public void receiveResultgetApplicationForReservationType(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetApplicationForReservationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationType operation
           */
            public void receiveErrorgetApplicationForReservationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByParentID method
            * override this method for handling normal response from getPurchasePlanKindByParentID operation
            */
           public void receiveResultgetPurchasePlanKindByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskCountByParentID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskCountByParentID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindFinTaskCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskCountByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSource method
            * override this method for handling normal response from getUnitBudgetKoKindFundSource operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSource(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSource operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByIDM method
            * override this method for handling normal response from getUnitBudgetKoByIDM operation
            */
           public void receiveResultgetUnitBudgetKoByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByIDM operation
           */
            public void receiveErrorgetUnitBudgetKoByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCount method
            * override this method for handling normal response from getPurchasePlanKindCount operation
            */
           public void receiveResultgetPurchasePlanKindCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCount operation
           */
            public void receiveErrorgetPurchasePlanKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskCount method
            * override this method for handling normal response from getPurchasePlanKindFinTaskCount operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindFinTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskCount operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByIDM method
            * override this method for handling normal response from getApplicationForReservationTypeByIDM operation
            */
           public void receiveResultgetApplicationForReservationTypeByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetApplicationForReservationTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByIDM operation
           */
            public void receiveErrorgetApplicationForReservationTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskCount method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskCount operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSource method
            * override this method for handling normal response from getProjectFundingSource operation
            */
           public void receiveResultgetProjectFundingSource(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSource operation
           */
            public void receiveErrorgetProjectFundingSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTask method
            * override this method for handling normal response from getUnitBudgetKoKindFinTask operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTask(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTask operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByID method
            * override this method for handling normal response from getUnitBudgetKoKindByID operation
            */
           public void receiveResultgetUnitBudgetKoKindByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemCount method
            * override this method for handling normal response from getProjectBudgetItemCount operation
            */
           public void receiveResultgetProjectBudgetItemCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemCount operation
           */
            public void receiveErrorgetProjectBudgetItemCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoCount method
            * override this method for handling normal response from getUnitBudgetKoCount operation
            */
           public void receiveResultgetUnitBudgetKoCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoCount operation
           */
            public void receiveErrorgetUnitBudgetKoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatio method
            * override this method for handling normal response from getVatRatio operation
            */
           public void receiveResultgetVatRatio(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetVatRatioResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatio operation
           */
            public void receiveErrorgetVatRatio(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItem method
            * override this method for handling normal response from getProjectBudgetItem operation
            */
           public void receiveResultgetProjectBudgetItem(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItem operation
           */
            public void receiveErrorgetProjectBudgetItem(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskByID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskByID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCountByParentID method
            * override this method for handling normal response from getPurchasePlanKindCountByParentID operation
            */
           public void receiveResultgetPurchasePlanKindCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCountByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTask method
            * override this method for handling normal response from getPurchasePlanKindFinTask operation
            */
           public void receiveResultgetPurchasePlanKindFinTask(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindFinTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTask operation
           */
            public void receiveErrorgetPurchasePlanKindFinTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectCount method
            * override this method for handling normal response from getProjectCount operation
            */
           public void receiveResultgetProjectCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectCount operation
           */
            public void receiveErrorgetProjectCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByParentID method
            * override this method for handling normal response from getProjectFundingSourceByParentID operation
            */
           public void receiveResultgetProjectFundingSourceByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByParentID operation
           */
            public void receiveErrorgetProjectFundingSourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCount method
            * override this method for handling normal response from getUnitBudgetKoKindCount operation
            */
           public void receiveResultgetUnitBudgetKoKindCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByParentIDM method
            * override this method for handling normal response from getProjectFundingSourceByParentIDM operation
            */
           public void receiveResultgetProjectFundingSourceByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByParentIDM operation
           */
            public void receiveErrorgetProjectFundingSourceByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByParentID method
            * override this method for handling normal response from getProjectBugdetByParentID operation
            */
           public void receiveResultgetProjectBugdetByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByParentID operation
           */
            public void receiveErrorgetProjectBugdetByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeCount method
            * override this method for handling normal response from getApplicationForReservationTypeCount operation
            */
           public void receiveResultgetApplicationForReservationTypeCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetApplicationForReservationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeCount operation
           */
            public void receiveErrorgetApplicationForReservationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceByParentID method
            * override this method for handling normal response from getProjectBudgetItemResourceByParentID operation
            */
           public void receiveResultgetProjectBudgetItemResourceByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudget method
            * override this method for handling normal response from getUnitBudget operation
            */
           public void receiveResultgetUnitBudget(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudget operation
           */
            public void receiveErrorgetUnitBudget(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCountByParentID method
            * override this method for handling normal response from getProjectFundingSourceCountByParentID operation
            */
           public void receiveResultgetProjectFundingSourceCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCountByParentID operation
           */
            public void receiveErrorgetProjectFundingSourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByParentIDM method
            * override this method for handling normal response from getProjectBugdetByParentIDM operation
            */
           public void receiveResultgetProjectBugdetByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByParentIDM operation
           */
            public void receiveErrorgetProjectBugdetByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCount method
            * override this method for handling normal response from getProjectBugdetCount operation
            */
           public void receiveResultgetProjectBugdetCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCount operation
           */
            public void receiveErrorgetProjectBugdetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByParentIDM method
            * override this method for handling normal response from getUnitBudgetKoKindByParentIDM operation
            */
           public void receiveResultgetUnitBudgetKoKindByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByParentIDM operation
           */
            public void receiveErrorgetUnitBudgetKoKindByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByID method
            * override this method for handling normal response from getPurchasePlanKindByID operation
            */
           public void receiveResultgetPurchasePlanKindByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByID operation
           */
            public void receiveErrorgetPurchasePlanKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatioCount method
            * override this method for handling normal response from getVatRatioCount operation
            */
           public void receiveResultgetVatRatioCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetVatRatioCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatioCount operation
           */
            public void receiveErrorgetVatRatioCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskByID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskByID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindFinTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskByID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatioByID method
            * override this method for handling normal response from getVatRatioByID operation
            */
           public void receiveResultgetVatRatioByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetVatRatioByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatioByID operation
           */
            public void receiveErrorgetVatRatioByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByID method
            * override this method for handling normal response from getApplicationForReservationTypeByID operation
            */
           public void receiveResultgetApplicationForReservationTypeByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetApplicationForReservationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByID operation
           */
            public void receiveErrorgetApplicationForReservationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByID method
            * override this method for handling normal response from getPurchasePlanByID operation
            */
           public void receiveResultgetPurchasePlanByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByID operation
           */
            public void receiveErrorgetPurchasePlanByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByID method
            * override this method for handling normal response from getProjectBugdetByID operation
            */
           public void receiveResultgetProjectBugdetByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByID operation
           */
            public void receiveErrorgetProjectBugdetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCountByParentIDM method
            * override this method for handling normal response from getUnitBudgetKoKindCountByParentIDM operation
            */
           public void receiveResultgetUnitBudgetKoKindCountByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCountByParentIDM operation
           */
            public void receiveErrorgetUnitBudgetKoKindCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKo method
            * override this method for handling normal response from getUnitBudgetKo operation
            */
           public void receiveResultgetUnitBudgetKo(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKo operation
           */
            public void receiveErrorgetUnitBudgetKo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceCount method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceCount operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetUnitOrganizationUnit method
            * override this method for handling normal response from getBudgetUnitOrganizationUnit operation
            */
           public void receiveResultgetBudgetUnitOrganizationUnit(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetBudgetUnitOrganizationUnitResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetUnitOrganizationUnit operation
           */
            public void receiveErrorgetBudgetUnitOrganizationUnit(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemByID method
            * override this method for handling normal response from getProjectBudgetItemByID operation
            */
           public void receiveResultgetProjectBudgetItemByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemByID operation
           */
            public void receiveErrorgetProjectBudgetItemByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceByID method
            * override this method for handling normal response from getProjectBudgetItemResourceByID operation
            */
           public void receiveResultgetProjectBudgetItemResourceByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceByID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByParentIDM method
            * override this method for handling normal response from getPurchasePlanKindByParentIDM operation
            */
           public void receiveResultgetPurchasePlanKindByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByParentIDM operation
           */
            public void receiveErrorgetPurchasePlanKindByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceByID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceByID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceCountByParentID method
            * override this method for handling normal response from getProjectBudgetItemResourceCountByParentID operation
            */
           public void receiveResultgetProjectBudgetItemResourceCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceCountByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetUnitOrganizationUnitCount method
            * override this method for handling normal response from getBudgetUnitOrganizationUnitCount operation
            */
           public void receiveResultgetBudgetUnitOrganizationUnitCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetBudgetUnitOrganizationUnitCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetUnitOrganizationUnitCount operation
           */
            public void receiveErrorgetBudgetUnitOrganizationUnitCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlan method
            * override this method for handling normal response from getPurchasePlan operation
            */
           public void receiveResultgetPurchasePlan(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlan operation
           */
            public void receiveErrorgetPurchasePlan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceCountByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanCount method
            * override this method for handling normal response from getPurchasePlanCount operation
            */
           public void receiveResultgetPurchasePlanCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanCount operation
           */
            public void receiveErrorgetPurchasePlanCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByID method
            * override this method for handling normal response from getProjectFundingSourceByID operation
            */
           public void receiveResultgetProjectFundingSourceByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectFundingSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByID operation
           */
            public void receiveErrorgetProjectFundingSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByIDM method
            * override this method for handling normal response from getUnitBudgetByIDM operation
            */
           public void receiveResultgetUnitBudgetByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByIDM operation
           */
            public void receiveErrorgetUnitBudgetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKind method
            * override this method for handling normal response from getUnitBudgetKoKind operation
            */
           public void receiveResultgetUnitBudgetKoKind(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKind operation
           */
            public void receiveErrorgetUnitBudgetKoKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProject method
            * override this method for handling normal response from getProject operation
            */
           public void receiveResultgetProject(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProject operation
           */
            public void receiveErrorgetProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByID method
            * override this method for handling normal response from getUnitBudgetKoByID operation
            */
           public void receiveResultgetUnitBudgetKoByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByID operation
           */
            public void receiveErrorgetUnitBudgetKoByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdet method
            * override this method for handling normal response from getProjectBugdet operation
            */
           public void receiveResultgetProjectBugdet(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBugdetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdet operation
           */
            public void receiveErrorgetProjectBugdet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByID method
            * override this method for handling normal response from getProjectByID operation
            */
           public void receiveResultgetProjectByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByID operation
           */
            public void receiveErrorgetProjectByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCountByParentIDM method
            * override this method for handling normal response from getPurchasePlanKindCountByParentIDM operation
            */
           public void receiveResultgetPurchasePlanKindCountByParentIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCountByParentIDM operation
           */
            public void receiveErrorgetPurchasePlanKindCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getData method
            * override this method for handling normal response from getData operation
            */
           public void receiveResultgetData(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getData operation
           */
            public void receiveErrorgetData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCount method
            * override this method for handling normal response from getCount operation
            */
           public void receiveResultgetCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCount operation
           */
            public void receiveErrorgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceCount method
            * override this method for handling normal response from getProjectBudgetItemResourceCount operation
            */
           public void receiveResultgetProjectBudgetItemResourceCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceCount operation
           */
            public void receiveErrorgetProjectBudgetItemResourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResource method
            * override this method for handling normal response from getProjectBudgetItemResource operation
            */
           public void receiveResultgetProjectBudgetItemResource(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemResourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResource operation
           */
            public void receiveErrorgetProjectBudgetItemResource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByIDM method
            * override this method for handling normal response from getPurchasePlanByIDM operation
            */
           public void receiveResultgetPurchasePlanByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByIDM operation
           */
            public void receiveErrorgetPurchasePlanByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByIDM method
            * override this method for handling normal response from getProjectByIDM operation
            */
           public void receiveResultgetProjectByIDM(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByIDM operation
           */
            public void receiveErrorgetProjectByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByID method
            * override this method for handling normal response from getUnitBudgetByID operation
            */
           public void receiveResultgetUnitBudgetByID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByID operation
           */
            public void receiveErrorgetUnitBudgetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskByParentID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskByParentID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetPurchasePlanKindFinTaskByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemByParentID method
            * override this method for handling normal response from getProjectBudgetItemByParentID operation
            */
           public void receiveResultgetProjectBudgetItemByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetProjectBudgetItemByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetCount method
            * override this method for handling normal response from getUnitBudgetCount operation
            */
           public void receiveResultgetUnitBudgetCount(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetCount operation
           */
            public void receiveErrorgetUnitBudgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindByParentID(
                    pl.compan.docusafe.ws.ul.DictionaryServiceStub.GetUnitBudgetKoKindByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindByParentID(java.lang.Exception e) {
            }
                


    }
    