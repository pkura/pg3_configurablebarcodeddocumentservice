
/**
 * DictionaryServiceIllegalArgumentExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

package pl.compan.docusafe.ws.ul;

public class DictionaryServiceIllegalArgumentExceptionException extends java.lang.Exception{
    
    private pl.compan.docusafe.ws.ul.DictionaryServiceStub.DictionaryServiceIllegalArgumentException faultMessage;

    
        public DictionaryServiceIllegalArgumentExceptionException() {
            super("DictionaryServiceIllegalArgumentExceptionException");
        }

        public DictionaryServiceIllegalArgumentExceptionException(java.lang.String s) {
           super(s);
        }

        public DictionaryServiceIllegalArgumentExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public DictionaryServiceIllegalArgumentExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.compan.docusafe.ws.ul.DictionaryServiceStub.DictionaryServiceIllegalArgumentException msg){
       faultMessage = msg;
    }
    
    public pl.compan.docusafe.ws.ul.DictionaryServiceStub.DictionaryServiceIllegalArgumentException getFaultMessage(){
       return faultMessage;
    }
}
    