
/**
 * DictionaryServicesProjectsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.ul;

    /**
     *  DictionaryServicesProjectsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesProjectsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesProjectsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesProjectsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getProjectFundSourcesPagged method
            * override this method for handling normal response from getProjectFundSourcesPagged operation
            */
           public void receiveResultgetProjectFundSourcesPagged(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectFundSourcesPaggedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundSourcesPagged operation
           */
            public void receiveErrorgetProjectFundSourcesPagged(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgets method
            * override this method for handling normal response from getProjectBudgets operation
            */
           public void receiveResultgetProjectBudgets(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgets operation
           */
            public void receiveErrorgetProjectBudgets(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetPositionResourcesPagged method
            * override this method for handling normal response from getProjectBudgetPositionResourcesPagged operation
            */
           public void receiveResultgetProjectBudgetPositionResourcesPagged(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourcesPaggedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetPositionResourcesPagged operation
           */
            public void receiveErrorgetProjectBudgetPositionResourcesPagged(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundSources method
            * override this method for handling normal response from getFundSources operation
            */
           public void receiveResultgetFundSources(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetFundSourcesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundSources operation
           */
            public void receiveErrorgetFundSources(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetPositionResourceFundSources method
            * override this method for handling normal response from getProjectBudgetPositionResourceFundSources operation
            */
           public void receiveResultgetProjectBudgetPositionResourceFundSources(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourceFundSourcesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetPositionResourceFundSources operation
           */
            public void receiveErrorgetProjectBudgetPositionResourceFundSources(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetPositionResourceFundSourcesPagged method
            * override this method for handling normal response from getProjectBudgetPositionResourceFundSourcesPagged operation
            */
           public void receiveResultgetProjectBudgetPositionResourceFundSourcesPagged(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourceFundSourcesPaggedResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetPositionResourceFundSourcesPagged operation
           */
            public void receiveErrorgetProjectBudgetPositionResourceFundSourcesPagged(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetPositionResourcesPagedFiltered method
            * override this method for handling normal response from getProjectBudgetPositionResourcesPagedFiltered operation
            */
           public void receiveResultgetProjectBudgetPositionResourcesPagedFiltered(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionResourcesPagedFilteredResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetPositionResourcesPagedFiltered operation
           */
            public void receiveErrorgetProjectBudgetPositionResourcesPagedFiltered(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjects method
            * override this method for handling normal response from getProjects operation
            */
           public void receiveResultgetProjects(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjects operation
           */
            public void receiveErrorgetProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetPositions method
            * override this method for handling normal response from getProjectBudgetPositions operation
            */
           public void receiveResultgetProjectBudgetPositions(
                    pl.compan.docusafe.ws.ul.DictionaryServicesProjectsStub.GetProjectBudgetPositionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetPositions operation
           */
            public void receiveErrorgetProjectBudgetPositions(java.lang.Exception e) {
            }
                


    }
    