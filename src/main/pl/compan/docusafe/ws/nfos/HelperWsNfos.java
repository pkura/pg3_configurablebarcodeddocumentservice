package pl.compan.docusafe.ws.nfos;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.parametrization.nfos.NfosLogicable;
/**
 * Klasa pomocnik�w wykorzystywana w ws.nfos i ws.journal.
 * Nie zaleca si� u�ywania w innych pakietach.
 *
 */
public final class HelperWsNfos {

	private HelperWsNfos(){}

	public static void sprawdzDokument(OutOfficeDocument document) throws RDEFault {
		sprawdzDokument(document, false);
	}

	public static void sprawdzDokument(OutOfficeDocument document, boolean przedUsunieciem) throws RDEFault {
		if (!przedUsunieciem) {
			if (document.getDocumentKind().getCn().equals(NfosLogicable.NORMAL_OUT))
				throw new RDEFault("ERR-008", "Numer kancelaryjny zosta� ju� wykorzystany");
		}
		if (document.getDocumentKind().getCn().equals(NfosLogicable.BLANK) &&
				document.getTitle().equalsIgnoreCase("numer KO anulowany")){
			throw new RDEFault("ERR-010", "Zarezerwowany dokument zosta� ju� anulowany. " + document.getDescription());
		} else if (!(document.getDocumentKind().getCn().equals(NfosLogicable.BLANK) &&
				document.getTitle().equalsIgnoreCase("Rezerwacja numeru KO"))){
			if (document.getDocumentKind().getCn().equals(NfosLogicable.BLANK))
				throw new RDEFault("ERR-012", "Dokument nie zosta� jeszcze utworzony.");			
		}
	}
	
	public static void sprawdzPrzedAnulacja(OutOfficeDocument document) throws RDEFault {
		if(document.getDocumentKind().getCn().equals(NfosLogicable.BLANK)){
			
		} else {
			throw new RDEFault("ERR-008", "Numer kancelaryjny zosta� ju� wykorzystany");
		}
	}
	
	/**
	 * Document jest usuni�ty je�li status dokumentu r�wny NfosLogicable.USUNIETY
	 * @param document
	 * @return
	 */
	public static boolean czyUsuniety(OutOfficeDocument document) {
		try {
			FieldsManager fm = document.getFieldsManager();
			int statusDokumentu = fm.getIntegerKey(NfosLogicable.STATUS_CN);
			if(statusDokumentu == NfosLogicable.USUNIETY) return true;
		} catch (EdmException e) {
			
		}
		return false;
	}
	
	/**
	 * Dokument jest anulowny je�li jego tytu� jest r�wny "numer KO anulowany".
	 * FIXME Nalezy to zmienic poniewa� jest to chwilowe i z�e rozwi�zanie.
	 * @param document
	 * @return 
	 */
	public static boolean czyAnulowany(OutOfficeDocument document) {
		String tytul = document.getTitle();
		if(document.getDocumentKind().getCn().equals(NfosLogicable.BLANK) &&
				"numer KO anulowany".equals(tytul)) return true;
		return false;
	}
	
	/**
	 * Zwraca podmiot z adresu ePUAP-u. Adres w formie "/{podmiot}/{urzad}".
	 * 
	 * @param adresSkrytki
	 * @return
	 */
	public static String podmiotFromAdres(String adresSkrytki) {
		String[] podmiot = adresSkrytki.split("/");
		
		return podmiot[1];
	}
	
	public static boolean isSetAtrybut(Atrybut[] atrybuty, String szukanyAtrybut){
		for (Atrybut atrybut : atrybuty) {
			if (szukanyAtrybut.equals(atrybut.getNazwa()))
				return true;
		}
		return false;
	}
}
