package pl.compan.docusafe.ws.nfos;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import de.schlichtherle.io.swing.JFileChooser;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Attachment;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.Recipient;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.office.RwaNotFoundException;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.nfos.NfosLogicable;
import pl.compan.docusafe.service.epuap.EpuapExportDocument;
import pl.compan.docusafe.service.epuap.EpuapExportManager;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;
import system.Array;

/**
 * Us�uga komunikatora dla NFOS
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class KomunikatorService implements Serializable
{
	private final static Logger log = LoggerFactory.getLogger(KomunikatorService.class);
	private final static int _wymaganaIloscParametrow = 5;
	private DocumentKind documentKind = new DocumentKind();
	private Long documentId = new Long(0);
	
	/**
	 * @param rodzajKomunikatu
	 * @param trescBinarna Base64
	 * @param atrybuty
	 * @return
	 * @throws AccessDeniedException, Exception 
	 */
	public String query(String rodzajKomunikatu, String trescBinarna, Atrybut[] atrybuty) throws RDEFault
	{
		// przyjmujemy dwa komunikaty dokumentWychodz�cy oraz usuniecieDokumentu
		try {
			AbstractAuthenticator.openContext();
			DSApi.context().begin();
			if ("dokumentWychodzacy".equalsIgnoreCase(rodzajKomunikatu)) {
				zmienDokumentWychodzacy(atrybuty, trescBinarna);
			} else if ("usuniecieDokumentu".equalsIgnoreCase(rodzajKomunikatu)) {
				usunDokument(atrybuty);
			} else throw new RDEFault("ERR-007", "B��dny rodzajKomunikatu lub jego brak");
			DSApi.context().commit();
		} catch (AccessDeniedException e) {
			log.debug(e.getMessage());
			DSApi.context()._rollback();
			throw new RDEFault("ERR-001", "Brak dost�pu");
		} catch (RDEFault e){
			log.debug("", e.getMessage());
			DSApi.context()._rollback();
			return e.getMessage();
		} catch (Exception e) {
			log.debug(e.getMessage());
			DSApi.context()._rollback();
		} finally {
			DSApi._close();
			AbstractAuthenticator._closeContext();
		}
		
		return "OK";
	}
	
	private void usunDokument(Atrybut[] atrybuty) throws EdmException, RDEFault {
		try {
			OutOfficeDocument outDocument = null;
			outDocument = getOutDocumentByOfficeNumber(atrybuty[0].getWartosc());
			HelperWsNfos.sprawdzDokument(outDocument, true);
			outDocument.getDocumentKind().logic().globalActionDocument(outDocument, 15, "removeDocument");	
		} catch (EdmException e) {
			log.debug(e.getMessage());
			throw new RDEFault("ERR-011", e.getMessage());
		}
	}
	
	private void zmienDokumentWychodzacy(Atrybut[] atrybuty, String zalacznik) throws Exception, EdmException, ParseException, RDEFault {
		OutOfficeDocument outDocument = new OutOfficeDocument();
		
		if (atrybuty.length == 4 && !HelperWsNfos.isSetAtrybut(atrybuty, "ePUAPOdbiorca")){
			documentKind = DocumentKind.findByCn(NfosLogicable.NORMAL_OUT);
			Map<String, Object> values = przygotujAtrybuty(atrybuty);
			values.put(NfosLogicable.STATUS_CN, NfosLogicable.ARCHIWALNY);
			outDocument = getOutDocumentByOfficeNumber("numerKancelaryjny", values);
			HelperWsNfos.sprawdzDokument(outDocument);
			documentId = createOutOfficeDocument(outDocument, documentKind, values, zalacznik);
		} else if (atrybuty.length == _wymaganaIloscParametrow) {
			documentKind = DocumentKind.findByCn(NfosLogicable.NORMAL_OUT);
			Map<String, Object> values = przygotujAtrybuty(atrybuty);
			outDocument = getOutDocumentByOfficeNumber("numerKancelaryjny", values);
			HelperWsNfos.sprawdzDokument(outDocument);
			documentId = createOutOfficeDocument(outDocument, documentKind, values, zalacznik);
			EpuapExportManager.sendToEpuap(documentId, true, EpuapExportDocument.USLUGA_SKRYTKA);
		} else {
			throw new RDEFault("ERR-003", "Brak wymaganych parametr�w");
		}
	}
		
	/**
	 * Przygotowuje atrybuty odebrane przez Web Service.
	 * 
	 * @param atrybuty
	 * @return
	 * @throws ParseException 
	 * @throws RDEFault 
	 * @throws EdmException 
	 * @throws RwaNotFoundException 
	 */
	private Map<String, Object> przygotujAtrybuty(Atrybut[] atrybuty) throws ParseException, RDEFault, RwaNotFoundException, EdmException {
		Map<String, Object> preparedValues = new HashMap<String, Object>();
		Date dataDoc = null;
		
		for(Atrybut value : atrybuty){
			if(value.getWartosc() == null || value.getWartosc().equals("")){
				throw new RDEFault("ERR-005", "Brak warto�ci w atrybucie");
			} else if ("numerKancelaryjny".equals(value.getNazwa())){ // numerKancelaryjny = nr KO = numer dziennika.
				preparedValues.put(value.getNazwa(), value.getWartosc());
			} else if ("kategoriaDokumentu".equals(value.getNazwa())){ // Kategoria RWA dokumentu np. 0101
				Rwa tempRwa = Rwa.findByCode((String)value.getWartosc());
				preparedValues.put("KATEGORIAARCHIWALNA", tempRwa.getId());
			} else if ("kategoriaArchiwalne".equals(value.getNazwa())){
//				preparedValues.put("KATEGORIAARCHIWALNA", value.getWartosc()); //np. B-10, B5
			} else if ("ePUAPOdbiorca".equals(value.getNazwa())){
				preparedValues.put("adresSkrytki", value.getWartosc());
			} else if ("dataDokumentu".equals(value.getNazwa())){
				dataDoc = new SimpleDateFormat("yyyy-MM-dd").parse(value.getWartosc());
				preparedValues.put("DOC_DATE", dataDoc); // data zapisywana jest w tabeli DSO_OUT_DOCUMENT kolumna DOCUMENTDATE
			} else {
				throw new RDEFault("ERR-004", "Nieznany atrybut");
			}
		}
		return preparedValues;
	}
	
	private OutOfficeDocument getOutDocumentByOfficeNumber(String numerKo) throws RDEFault{
		return this.getOutDocumentByOfficeNumber(numerKo, null);
	}
	
	private OutOfficeDocument getOutDocumentByOfficeNumber(String numerKo, Map<String, Object> values) throws RDEFault{
        OutOfficeDocument document = null;
        String numerKancelaryjny;
        try {
        	if(values == null){
        		numerKancelaryjny = numerKo;
        	} else {
        		numerKancelaryjny = (String)values.get(numerKo);
			}
			List<OutOfficeDocument> listaDocumentow = OutOfficeDocument.findByOfficeNumber(Integer.parseInt(numerKancelaryjny));
			if(listaDocumentow.size() == 0) throw new RDEFault("ERR-006", "B��dny numer kancelaryjny");
			for (OutOfficeDocument outOfficeDocument : listaDocumentow) {
				Journal j = outOfficeDocument.getJournal();
				if(j.getJournalType().equals(Journal.OUTGOING))
					document = outOfficeDocument;
			}
		} catch (NumberFormatException e) {
			log.debug(e);
		} catch (EdmException e) {
			log.error("", e.getMessage());
		}
		
		return document;
	}
	
	/*private OutOfficeDocument getOutDocumentByOfficeNumber(String numerKo){
        OutOfficeDocument document = null;        
        try {
			List<OutOfficeDocument> listaDocumentow = OutOfficeDocument.findByOfficeNumber(Integer.parseInt(numerKo));    
			for (OutOfficeDocument outOfficeDocument : listaDocumentow) {
				Journal j = outOfficeDocument.getJournal();
				if(j.getJournalType().equals(Journal.OUTGOING))
					document = outOfficeDocument;
			}
		} catch (NumberFormatException e) {
			log.debug(e);
			e.printStackTrace();
		} catch (EdmException e) {
			log.error("", e.getMessage());
			e.printStackTrace();
		}
		
		return document;
	}*/
	
	private Long createOutOfficeDocument(OutOfficeDocument document, DocumentKind documentKind, Map<String, Object> values, String zalacznik) throws Exception
	{		
		document.setPermissionsOn(false);
		setUpDocumentBeforeCreate(document, values);
		
		/* Atrybuty office */ 
		document.setSummary(documentKind.getName());
		document.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
		document.setDivisionGuid(DSDivision.ROOT_GUID);
		document.setCurrentAssignmentAccepted(Boolean.FALSE);
		document.setCreatingUser(DSApi.context().getPrincipalName());
		document.setDocumentDate((Date) values.get("DOC_DATE"));

		// Dodanie do dokumentu adresu skrytki i identyfikatora podmiotu
		if (values.get("adresSkrytki") != null) {
			Recipient rec = new Recipient();
			rec.create();
			rec.setAdresSkrytkiEpuap((String) values.get("adresSkrytki"));
			rec.setIdentifikatorEpuap(Helpers.podmiotFromAdres((String) values.get("adresSkrytki")));
			document.addRecipient(rec);
		}

		document.setForceArchivePermissions(false);
		document.setAssignedDivision(DSDivision.ROOT_GUID);
		document.setClerk(DSApi.context().getPrincipalName());
		
		//document.create();
		
        setUpDocumentAfterCreate(document);
        /*String desc = "Dokument zmieniony. Po wywo�aniu serwisu.";
        document.setDescription(desc);*/
        
        //dodawanie tresci binarnej do dokumentu
//        StandardAttachmentRevision stdAttRev = new StandardAttachmentRevision();
        JFileChooser filechooser = new JFileChooser();
        java.io.File file = FileUtils.decodeByBase64ToFile(zalacznik);
        Attachment attachment = new Attachment(filechooser.getName(file));
        document.createAttachment(attachment);
        attachment.createRevision(file).setOriginalFilename(file.getName());
//        stdAttRev
        //end_dodawanie
        
//      ustawienie systemu na dokumencie, na podstawie systemu, kt�ry wywo�a� Web Serwis
        values.put(NfosLogicable.SYSTEM_CN, 1 /*Id Systemu dziedzinowego*/ ); 
//        DSApi.context().getDSUser().getId()
//      end
        
        document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(AssignmentHistoryEntry.IMPORT, DSApi.context().getPrincipalName(), "x","x","x","x", false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
        
        WorkflowFactory.createNewProcess(document, true, "Document accepted");
		
        document.getDocumentKind().setOnly(document.getId(), values);
		document.getDocumentKind().logic().onStartProcess(document);
		
		
		return document.getId();
	}
	
	private void setUpDocumentBeforeCreate(OutOfficeDocument document, Map<String, Object> values) throws Exception {
		// analiza parametr 3
		documentKind.logic().correctImportValues(document, values, new StringBuilder());
		document.setDocumentKind(documentKind);
	}
	
	private void setUpDocumentAfterCreate(OutOfficeDocument document) throws EdmException {
		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_OUT_OFFICE);
		documentKind.logic().documentPermissions(document);
	}
	
	private static class Helpers {
		/**
		 * Zwraca podmiot z adresu. Adres w formie "/{podmiot}/{urzad}".
		 * 
		 * @param adresSkrytki
		 * @return
		 */
		public static String podmiotFromAdres(String adresSkrytki) {
			String[] podmiot = adresSkrytki.split("/");
			
			return podmiot[1];
		}
	}
}
