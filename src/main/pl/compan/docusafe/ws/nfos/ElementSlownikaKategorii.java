package pl.compan.docusafe.ws.nfos;

import java.io.Serializable;

/**
 * Element slownika kategorii
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class ElementSlownikaKategorii implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	private String system;
	
	private String dzial;
	
	private String kategoriaDokumentu; //id;digits;nazwa;kategoriaRWA
	
	private String kategoriaArchiwalna; //np. b-5

	public String getSystem()
	{
		return system;
	}

	public void setSystem(String system)
	{
		this.system = system;
	}

	public String getDzial()
	{
		return dzial;
	}

	public void setDzial(String dzial)
	{
		this.dzial = dzial;
	}

	public String getKategoriaDokumentu()
	{
		return kategoriaDokumentu;
	}

	public void setKategoriaDokumentu(String kategoriaDokumentu)
	{
		this.kategoriaDokumentu = kategoriaDokumentu;
	}

	public String getKategoriaArchiwalna()
	{
		return kategoriaArchiwalna;
	}

	public void setKategoriaArchiwalna(String kategoriaArchiwalna)
	{
		this.kategoriaArchiwalna = kategoriaArchiwalna;
	}

	public static long getSerialversionuid()
	{
		return serialVersionUID;
	}
	
	
}
