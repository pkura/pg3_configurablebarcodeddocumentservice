package pl.compan.docusafe.ws.nfos;

/**
 * Globalny komunikat b��du Dla NFOS
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class RDEFault extends Exception
{
	private String komunikat;
	private String kod;
	
	public RDEFault()
	{

		this.komunikat = "Nieznany b��d!";
		this.kod = "ERR-000";
	}
	
	public RDEFault(String kod, String komunikat)
	{
		super(komunikat);
		this.komunikat =komunikat;
		this.kod = kod;
	}
	
	
}
