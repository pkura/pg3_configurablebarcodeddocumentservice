package pl.compan.docusafe.ws.nfos;

import java.io.Serializable;

/**
 * Atrybuty dla Serwisu Komuniaktora NFOS
 * 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class Atrybut implements Serializable
{
	private static final long serialVersionUID = 145656456L;

	private String nazwa;
	
	private String wartosc;

	public String getNazwa()
	{
		return nazwa;
	}

	public void setNazwa(String nazwa)
	{
		this.nazwa = nazwa;
	}

	public String getWartosc()
	{
		return wartosc;
	}

	public void setWartosc(String wartosc)
	{
		this.wartosc = wartosc;
	}
	
	
}
