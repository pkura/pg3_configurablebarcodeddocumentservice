package pl.compan.docusafe.ws.nfos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.office.Rwa;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * Us�uga zwraca dane s�ownikowe udost�pnia jedn� metod� kategorie. Metoda ta s�u�y do udost�pnienia s�ownika kategorii
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 */
public class SlownikService implements Serializable
{
	private final static Logger log = LoggerFactory.getLogger(SlownikService.class);
	boolean showActive = true;
	Integer id = 20;
	
	public ElementSlownikaKategorii[] kategorie(String system, String dzial) throws RDEFault
	{
		ElementSlownikaKategorii[] esk;
		try {
			AbstractAuthenticator.openContext();
			List<Rwa> listOfRwa = new ArrayList<Rwa>();
			esk = testRWA(listOfRwa, system, dzial);
		} catch (AccessDeniedException e) {
			throw new RDEFault("ERR-002", "Brak dost�pu");
		} catch (Exception e) {
			throw new RDEFault();
		} finally {
			AbstractAuthenticator._closeContext();
		}
		
		return esk;
	}
	
	private ElementSlownikaKategorii[] testRWA(List<Rwa> listOfRwa, final String system, final String dzial) throws RDEFault{
		Collection<ElementSlownikaKategorii> slownik;
		try{
			listOfRwa = Rwa.list(showActive);
			/*Rwa pobraneRwaById = Rwa.find(id);
			Rwa zListy = listOfRwa.get(4);
			String rwaToString = zListy.getRwa();*/
			slownik = Collections2.transform(listOfRwa, new Function<Rwa, ElementSlownikaKategorii>(){

				@Override
				public ElementSlownikaKategorii apply(Rwa arg0) {
					ElementSlownikaKategorii esk = new ElementSlownikaKategorii();
					StringBuilder kategoriaRWA = new StringBuilder();
					kategoriaRWA.append(arg0.getRwa() + ";");
					kategoriaRWA.append(arg0.getDescription());
					
					esk.setSystem(system);
					esk.setDzial(dzial);
					esk.setKategoriaDokumentu(kategoriaRWA.toString());
					esk.setKategoriaArchiwalna(arg0.getAcHome());
					
					return esk;
				}
			});
		} catch (EdmException e){
			log.error("", e);
			throw new RDEFault("ERR-???", "Nie pobra�em listy");
		}
		return slownik.toArray(new ElementSlownikaKategorii[slownik.size()]);
	}
}