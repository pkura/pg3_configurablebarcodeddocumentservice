package pl.compan.docusafe.ws.test;

public class TestServiceRequest 
{
	private String param;
	private String param1;

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}
	
}
