
/**
 * DictionaryServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.client.simple.general;

    /**
     *  DictionaryServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitCount method
            * override this method for handling normal response from getOrganizationalUnitCount operation
            */
           public void receiveResultgetOrganizationalUnitCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitCount operation
           */
            public void receiveErrorgetOrganizationalUnitCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetSettlementSubPeriod method
            * override this method for handling normal response from getAssetSettlementSubPeriod operation
            */
           public void receiveResultgetAssetSettlementSubPeriod(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetSettlementSubPeriodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetSettlementSubPeriod operation
           */
            public void receiveErrorgetAssetSettlementSubPeriod(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInProjectsCount method
            * override this method for handling normal response from getRolesInProjectsCount operation
            */
           public void receiveResultgetRolesInProjectsCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjectsCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInProjectsCount operation
           */
            public void receiveErrorgetRolesInProjectsCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTerms method
            * override this method for handling normal response from getPaymentTerms operation
            */
           public void receiveResultgetPaymentTerms(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTerms operation
           */
            public void receiveErrorgetPaymentTerms(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFundingByID method
            * override this method for handling normal response from getSourcesOfProjectFundingByID operation
            */
           public void receiveResultgetSourcesOfProjectFundingByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFundingByID operation
           */
            public void receiveErrorgetSourcesOfProjectFundingByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskCount method
            * override this method for handling normal response from getFinancialTaskCount operation
            */
           public void receiveResultgetFinancialTaskCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskCount operation
           */
            public void receiveErrorgetFinancialTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemCountByParentID method
            * override this method for handling normal response from getProjectBudgetItemCountByParentID operation
            */
           public void receiveResultgetProjectBudgetItemCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemCountByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeByID method
            * override this method for handling normal response from getAssetOperationTypeByID operation
            */
           public void receiveResultgetAssetOperationTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOperationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeByID operation
           */
            public void receiveErrorgetAssetOperationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployee method
            * override this method for handling normal response from getEmployee operation
            */
           public void receiveResultgetEmployee(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployee operation
           */
            public void receiveErrorgetEmployee(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAbsenceType method
            * override this method for handling normal response from getAbsenceType operation
            */
           public void receiveResultgetAbsenceType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAbsenceType operation
           */
            public void receiveErrorgetAbsenceType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPriceTypeCount method
            * override this method for handling normal response from getPriceTypeCount operation
            */
           public void receiveResultgetPriceTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPriceTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPriceTypeCount operation
           */
            public void receiveErrorgetPriceTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCountByParentID method
            * override this method for handling normal response from getProjectBugdetCountByParentID operation
            */
           public void receiveResultgetProjectBugdetCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCountByParentID operation
           */
            public void receiveErrorgetProjectBugdetCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountByID method
            * override this method for handling normal response from getCompanyBankAccountByID operation
            */
           public void receiveResultgetCompanyBankAccountByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCompanyBankAccountByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountByID operation
           */
            public void receiveErrorgetCompanyBankAccountByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByIDM method
            * override this method for handling normal response from getProjectBugdetByIDM operation
            */
           public void receiveResultgetProjectBugdetByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByIDM operation
           */
            public void receiveErrorgetProjectBugdetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCount method
            * override this method for handling normal response from getProjectFundingSourceCount operation
            */
           public void receiveResultgetProjectFundingSourceCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCount operation
           */
            public void receiveErrorgetProjectFundingSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentByID method
            * override this method for handling normal response from getFinancialDocumentByID operation
            */
           public void receiveResultgetFinancialDocumentByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialDocumentByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentByID operation
           */
            public void receiveErrorgetFinancialDocumentByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReason method
            * override this method for handling normal response from getAssetLiquidationReason operation
            */
           public void receiveResultgetAssetLiquidationReason(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetLiquidationReasonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReason operation
           */
            public void receiveErrorgetAssetLiquidationReason(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRateType method
            * override this method for handling normal response from getCurrencyRateType operation
            */
           public void receiveResultgetCurrencyRateType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRateType operation
           */
            public void receiveErrorgetCurrencyRateType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCountByParentIDM method
            * override this method for handling normal response from getProjectFundingSourceCountByParentIDM operation
            */
           public void receiveResultgetProjectFundingSourceCountByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCountByParentIDM operation
           */
            public void receiveErrorgetProjectFundingSourceCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetSettlementSubPeriodByIDM method
            * override this method for handling normal response from getAssetSettlementSubPeriodByIDM operation
            */
           public void receiveResultgetAssetSettlementSubPeriodByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetSettlementSubPeriodByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetSettlementSubPeriodByIDM operation
           */
            public void receiveErrorgetAssetSettlementSubPeriodByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKind method
            * override this method for handling normal response from getPurchasePlanKind operation
            */
           public void receiveResultgetPurchasePlanKind(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKind operation
           */
            public void receiveErrorgetPurchasePlanKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureByID method
            * override this method for handling normal response from getUnitOfMeasureByID operation
            */
           public void receiveResultgetUnitOfMeasureByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureByID operation
           */
            public void receiveErrorgetUnitOfMeasureByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRequestForReservation method
            * override this method for handling normal response from getRequestForReservation operation
            */
           public void receiveResultgetRequestForReservation(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRequestForReservation operation
           */
            public void receiveErrorgetRequestForReservation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductByIDM method
            * override this method for handling normal response from getCostInvoiceProductByIDM operation
            */
           public void receiveResultgetCostInvoiceProductByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductByIDM operation
           */
            public void receiveErrorgetCostInvoiceProductByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUser method
            * override this method for handling normal response from getErpUser operation
            */
           public void receiveResultgetErpUser(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetErpUserResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUser operation
           */
            public void receiveErrorgetErpUser(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectAccount method
            * override this method for handling normal response from getProjectAccount operation
            */
           public void receiveResultgetProjectAccount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectAccount operation
           */
            public void receiveErrorgetProjectAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView2Count method
            * override this method for handling normal response from getBudgetView2Count operation
            */
           public void receiveResultgetBudgetView2Count(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetView2CountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView2Count operation
           */
            public void receiveErrorgetBudgetView2Count(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSource method
            * override this method for handling normal response from getFundingSource operation
            */
           public void receiveResultgetFundingSource(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSource operation
           */
            public void receiveErrorgetFundingSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetByIDM method
            * override this method for handling normal response from getContractBogdetByIDM operation
            */
           public void receiveResultgetContractBogdetByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractBogdetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetByIDM operation
           */
            public void receiveErrorgetContractBogdetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeByIDM method
            * override this method for handling normal response from getAssetOperationTypeByIDM operation
            */
           public void receiveResultgetAssetOperationTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOperationTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeByIDM operation
           */
            public void receiveErrorgetAssetOperationTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCostCenterCount method
            * override this method for handling normal response from getAssetCostCenterCount operation
            */
           public void receiveResultgetAssetCostCenterCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCostCenterCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCostCenterCount operation
           */
            public void receiveErrorgetAssetCostCenterCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseByID method
            * override this method for handling normal response from getWarehouseByID operation
            */
           public void receiveResultgetWarehouseByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetWarehouseByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseByID operation
           */
            public void receiveErrorgetWarehouseByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodCount method
            * override this method for handling normal response from getAccountingPeriodCount operation
            */
           public void receiveResultgetAccountingPeriodCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAccountingPeriodCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodCount operation
           */
            public void receiveErrorgetAccountingPeriodCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCountByParentIDM method
            * override this method for handling normal response from getProjectBugdetCountByParentIDM operation
            */
           public void receiveResultgetProjectBugdetCountByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCountByParentIDM operation
           */
            public void receiveErrorgetProjectBugdetCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationType method
            * override this method for handling normal response from getApplicationForReservationType operation
            */
           public void receiveResultgetApplicationForReservationType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationType operation
           */
            public void receiveErrorgetApplicationForReservationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeByIDM method
            * override this method for handling normal response from getEmployeeByIDM operation
            */
           public void receiveResultgetEmployeeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeByIDM operation
           */
            public void receiveErrorgetEmployeeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderByID method
            * override this method for handling normal response from getProductionOrderByID operation
            */
           public void receiveResultgetProductionOrderByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderByID operation
           */
            public void receiveErrorgetProductionOrderByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetResponsiblePersonCount method
            * override this method for handling normal response from getAssetResponsiblePersonCount operation
            */
           public void receiveResultgetAssetResponsiblePersonCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetResponsiblePersonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetResponsiblePersonCount operation
           */
            public void receiveErrorgetAssetResponsiblePersonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodByID method
            * override this method for handling normal response from getAccountingPeriodByID operation
            */
           public void receiveResultgetAccountingPeriodByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAccountingPeriodByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodByID operation
           */
            public void receiveErrorgetAccountingPeriodByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByParentID method
            * override this method for handling normal response from getPurchasePlanKindByParentID operation
            */
           public void receiveResultgetPurchasePlanKindByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonCount method
            * override this method for handling normal response from getPersonCount operation
            */
           public void receiveResultgetPersonCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonCount operation
           */
            public void receiveErrorgetPersonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOwnershipFormCount method
            * override this method for handling normal response from getAssetOwnershipFormCount operation
            */
           public void receiveResultgetAssetOwnershipFormCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOwnershipFormCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOwnershipFormCount operation
           */
            public void receiveErrorgetAssetOwnershipFormCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCount method
            * override this method for handling normal response from getPurchasePlanKindCount operation
            */
           public void receiveResultgetPurchasePlanKindCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCount operation
           */
            public void receiveErrorgetPurchasePlanKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractType method
            * override this method for handling normal response from getContractType operation
            */
           public void receiveResultgetContractType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractType operation
           */
            public void receiveErrorgetContractType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskCount method
            * override this method for handling normal response from getPurchasePlanKindFinTaskCount operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindFinTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskCount operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationByIDM method
            * override this method for handling normal response from getDeliveryLocationByIDM operation
            */
           public void receiveResultgetDeliveryLocationByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationByIDM operation
           */
            public void receiveErrorgetDeliveryLocationByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getExternalDecreeTypeCount method
            * override this method for handling normal response from getExternalDecreeTypeCount operation
            */
           public void receiveResultgetExternalDecreeTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetExternalDecreeTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getExternalDecreeTypeCount operation
           */
            public void receiveErrorgetExternalDecreeTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskCountByParentID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskCountByParentID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindFinTaskCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskCountByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPositionOfSaleByIDM method
            * override this method for handling normal response from getPositionOfSaleByIDM operation
            */
           public void receiveResultgetPositionOfSaleByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPositionOfSaleByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPositionOfSaleByIDM operation
           */
            public void receiveErrorgetPositionOfSaleByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriodByID method
            * override this method for handling normal response from getAssetAccountingPeriodByID operation
            */
           public void receiveResultgetAssetAccountingPeriodByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetAccountingPeriodByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriodByID operation
           */
            public void receiveErrorgetAssetAccountingPeriodByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeByID method
            * override this method for handling normal response from getEmployeeByID operation
            */
           public void receiveResultgetEmployeeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeByID operation
           */
            public void receiveErrorgetEmployeeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonByIDM method
            * override this method for handling normal response from getPersonByIDM operation
            */
           public void receiveResultgetPersonByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonByIDM operation
           */
            public void receiveErrorgetPersonByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContract method
            * override this method for handling normal response from getContract operation
            */
           public void receiveResultgetContract(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContract operation
           */
            public void receiveErrorgetContract(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTask method
            * override this method for handling normal response from getFinancialTask operation
            */
           public void receiveResultgetFinancialTask(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTask operation
           */
            public void receiveErrorgetFinancialTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetSettlementSubPeriodCount method
            * override this method for handling normal response from getAssetSettlementSubPeriodCount operation
            */
           public void receiveResultgetAssetSettlementSubPeriodCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetSettlementSubPeriodCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetSettlementSubPeriodCount operation
           */
            public void receiveErrorgetAssetSettlementSubPeriodCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByIDM method
            * override this method for handling normal response from getUnitBudgetKoByIDM operation
            */
           public void receiveResultgetUnitBudgetKoByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByIDM operation
           */
            public void receiveErrorgetUnitBudgetKoByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSource method
            * override this method for handling normal response from getUnitBudgetKoKindFundSource operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSource(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSource operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentByIDM method
            * override this method for handling normal response from getFinancialDocumentByIDM operation
            */
           public void receiveResultgetFinancialDocumentByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialDocumentByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentByIDM operation
           */
            public void receiveErrorgetFinancialDocumentByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAbsenceTypeByID method
            * override this method for handling normal response from getAbsenceTypeByID operation
            */
           public void receiveResultgetAbsenceTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAbsenceTypeByID operation
           */
            public void receiveErrorgetAbsenceTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseCount method
            * override this method for handling normal response from getWarehouseCount operation
            */
           public void receiveResultgetWarehouseCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetWarehouseCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseCount operation
           */
            public void receiveErrorgetWarehouseCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeByIDM method
            * override this method for handling normal response from getPurchaseDocumentTypeByIDM operation
            */
           public void receiveResultgetPurchaseDocumentTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeByIDM operation
           */
            public void receiveErrorgetPurchaseDocumentTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetPurposeByIDM method
            * override this method for handling normal response from getAssetPurposeByIDM operation
            */
           public void receiveResultgetAssetPurposeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetPurposeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetPurposeByIDM operation
           */
            public void receiveErrorgetAssetPurposeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRateCount method
            * override this method for handling normal response from getCurrencyRateCount operation
            */
           public void receiveResultgetCurrencyRateCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRateCount operation
           */
            public void receiveErrorgetCurrencyRateCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementByIDM method
            * override this method for handling normal response from getPersonAgreementByIDM operation
            */
           public void receiveResultgetPersonAgreementByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementByIDM operation
           */
            public void receiveErrorgetPersonAgreementByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateCount method
            * override this method for handling normal response from getVatRateCount operation
            */
           public void receiveResultgetVatRateCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateCount operation
           */
            public void receiveErrorgetVatRateCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureCount method
            * override this method for handling normal response from getUnitOfMeasureCount operation
            */
           public void receiveResultgetUnitOfMeasureCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureCount operation
           */
            public void receiveErrorgetUnitOfMeasureCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSource method
            * override this method for handling normal response from getProjectFundingSource operation
            */
           public void receiveResultgetProjectFundingSource(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSource operation
           */
            public void receiveErrorgetProjectFundingSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateByID method
            * override this method for handling normal response from getVatRateByID operation
            */
           public void receiveResultgetVatRateByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateByID operation
           */
            public void receiveErrorgetVatRateByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatios method
            * override this method for handling normal response from getVatRatios operation
            */
           public void receiveResultgetVatRatios(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRatiosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatios operation
           */
            public void receiveErrorgetVatRatios(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetResponsiblePersonByIDM method
            * override this method for handling normal response from getAssetResponsiblePersonByIDM operation
            */
           public void receiveResultgetAssetResponsiblePersonByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetResponsiblePersonByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetResponsiblePersonByIDM operation
           */
            public void receiveErrorgetAssetResponsiblePersonByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationTypeByID method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationTypeByID operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationTypeByID operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTask method
            * override this method for handling normal response from getUnitBudgetKoKindFinTask operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTask(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTask operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementCount method
            * override this method for handling normal response from getPersonAgreementCount operation
            */
           public void receiveResultgetPersonAgreementCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementCount operation
           */
            public void receiveErrorgetPersonAgreementCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskCount method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskCount operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeByIDM method
            * override this method for handling normal response from getContractTypeByIDM operation
            */
           public void receiveResultgetContractTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeByIDM operation
           */
            public void receiveErrorgetContractTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriod method
            * override this method for handling normal response from getAssetAccountingPeriod operation
            */
           public void receiveResultgetAssetAccountingPeriod(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetAccountingPeriodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriod operation
           */
            public void receiveErrorgetAssetAccountingPeriod(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBookkeepingSystemByIDM method
            * override this method for handling normal response from getBookkeepingSystemByIDM operation
            */
           public void receiveResultgetBookkeepingSystemByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBookkeepingSystemByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBookkeepingSystemByIDM operation
           */
            public void receiveErrorgetBookkeepingSystemByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByIDM method
            * override this method for handling normal response from getApplicationForReservationTypeByIDM operation
            */
           public void receiveResultgetApplicationForReservationTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByIDM operation
           */
            public void receiveErrorgetApplicationForReservationTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesCount method
            * override this method for handling normal response from getDeliveryDocumentTypesCount operation
            */
           public void receiveResultgetDeliveryDocumentTypesCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryDocumentTypesCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesCount operation
           */
            public void receiveErrorgetDeliveryDocumentTypesCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdet method
            * override this method for handling normal response from getContractBogdet operation
            */
           public void receiveResultgetContractBogdet(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractBogdetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdet operation
           */
            public void receiveErrorgetContractBogdet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountByID method
            * override this method for handling normal response from getSupplierBankAccountByID operation
            */
           public void receiveResultgetSupplierBankAccountByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountByID operation
           */
            public void receiveErrorgetSupplierBankAccountByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRate method
            * override this method for handling normal response from getCurrencyRate operation
            */
           public void receiveResultgetCurrencyRate(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRate operation
           */
            public void receiveErrorgetCurrencyRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrency method
            * override this method for handling normal response from getCurrency operation
            */
           public void receiveResultgetCurrency(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrency operation
           */
            public void receiveErrorgetCurrency(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemCount method
            * override this method for handling normal response from getProjectBudgetItemCount operation
            */
           public void receiveResultgetProjectBudgetItemCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemCount operation
           */
            public void receiveErrorgetProjectBudgetItemCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoCount method
            * override this method for handling normal response from getUnitBudgetKoCount operation
            */
           public void receiveResultgetUnitBudgetKoCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoCount operation
           */
            public void receiveErrorgetUnitBudgetKoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOwnershipFormByIDM method
            * override this method for handling normal response from getAssetOwnershipFormByIDM operation
            */
           public void receiveResultgetAssetOwnershipFormByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOwnershipFormByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOwnershipFormByIDM operation
           */
            public void receiveErrorgetAssetOwnershipFormByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBookkeepingSystemByID method
            * override this method for handling normal response from getBookkeepingSystemByID operation
            */
           public void receiveResultgetBookkeepingSystemByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBookkeepingSystemByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBookkeepingSystemByID operation
           */
            public void receiveErrorgetBookkeepingSystemByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRateTypeByID method
            * override this method for handling normal response from getCurrencyRateTypeByID operation
            */
           public void receiveResultgetCurrencyRateTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRateTypeByID operation
           */
            public void receiveErrorgetCurrencyRateTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoByIDM method
            * override this method for handling normal response from getAssetCardInfoByIDM operation
            */
           public void receiveResultgetAssetCardInfoByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoByIDM operation
           */
            public void receiveErrorgetAssetCardInfoByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationCount method
            * override this method for handling normal response from getDeliveryLocationCount operation
            */
           public void receiveResultgetDeliveryLocationCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationCount operation
           */
            public void receiveErrorgetDeliveryLocationCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetObtainmentForm method
            * override this method for handling normal response from getAssetObtainmentForm operation
            */
           public void receiveResultgetAssetObtainmentForm(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetObtainmentFormResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetObtainmentForm operation
           */
            public void receiveErrorgetAssetObtainmentForm(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceByID method
            * override this method for handling normal response from getFundingSourceByID operation
            */
           public void receiveResultgetFundingSourceByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceByID operation
           */
            public void receiveErrorgetFundingSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskByIDM method
            * override this method for handling normal response from getFinancialTaskByIDM operation
            */
           public void receiveResultgetFinancialTaskByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskByIDM operation
           */
            public void receiveErrorgetFinancialTaskByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationType method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationType operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationType operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAbsenceTypeCount method
            * override this method for handling normal response from getAbsenceTypeCount operation
            */
           public void receiveResultgetAbsenceTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAbsenceTypeCount operation
           */
            public void receiveErrorgetAbsenceTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByID method
            * override this method for handling normal response from getUnitBudgetKoKindByID operation
            */
           public void receiveResultgetUnitBudgetKoKindByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItem method
            * override this method for handling normal response from getProjectBudgetItem operation
            */
           public void receiveResultgetProjectBudgetItem(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItem operation
           */
            public void receiveErrorgetProjectBudgetItem(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPositionOfSaleCount method
            * override this method for handling normal response from getPositionOfSaleCount operation
            */
           public void receiveResultgetPositionOfSaleCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPositionOfSaleCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPositionOfSaleCount operation
           */
            public void receiveErrorgetPositionOfSaleCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView method
            * override this method for handling normal response from getBudgetView operation
            */
           public void receiveResultgetBudgetView(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetViewResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView operation
           */
            public void receiveErrorgetBudgetView(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetPurpose method
            * override this method for handling normal response from getAssetPurpose operation
            */
           public void receiveResultgetAssetPurpose(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetPurposeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetPurpose operation
           */
            public void receiveErrorgetAssetPurpose(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeCount method
            * override this method for handling normal response from getAssetOperationTypeCount operation
            */
           public void receiveResultgetAssetOperationTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOperationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeCount operation
           */
            public void receiveErrorgetAssetOperationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPositionOfSale method
            * override this method for handling normal response from getPositionOfSale operation
            */
           public void receiveResultgetPositionOfSale(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPositionOfSaleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPositionOfSale operation
           */
            public void receiveErrorgetPositionOfSale(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetCount method
            * override this method for handling normal response from getContractBogdetCount operation
            */
           public void receiveResultgetContractBogdetCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractBogdetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetCount operation
           */
            public void receiveErrorgetContractBogdetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskByID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskByID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderByID method
            * override this method for handling normal response from getSupplierOrderHeaderByID operation
            */
           public void receiveResultgetSupplierOrderHeaderByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderByID operation
           */
            public void receiveErrorgetSupplierOrderHeaderByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCountByParentID method
            * override this method for handling normal response from getPurchasePlanKindCountByParentID operation
            */
           public void receiveResultgetPurchasePlanKindCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCountByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRoles method
            * override this method for handling normal response from getProjectRoles operation
            */
           public void receiveResultgetProjectRoles(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectRolesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRoles operation
           */
            public void receiveErrorgetProjectRoles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeader method
            * override this method for handling normal response from getSupplierOrderHeader operation
            */
           public void receiveResultgetSupplierOrderHeader(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeader operation
           */
            public void receiveErrorgetSupplierOrderHeader(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentByID method
            * override this method for handling normal response from getDepartmentByID operation
            */
           public void receiveResultgetDepartmentByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDepartmentByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentByID operation
           */
            public void receiveErrorgetDepartmentByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryByID method
            * override this method for handling normal response from getCountryByID operation
            */
           public void receiveResultgetCountryByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCountryByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryByID operation
           */
            public void receiveErrorgetCountryByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserByIDM method
            * override this method for handling normal response from getErpUserByIDM operation
            */
           public void receiveResultgetErpUserByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetErpUserByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserByIDM operation
           */
            public void receiveErrorgetErpUserByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationByID method
            * override this method for handling normal response from getDeliveryLocationByID operation
            */
           public void receiveResultgetDeliveryLocationByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationByID operation
           */
            public void receiveErrorgetDeliveryLocationByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTask method
            * override this method for handling normal response from getPurchasePlanKindFinTask operation
            */
           public void receiveResultgetPurchasePlanKindFinTask(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindFinTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTask operation
           */
            public void receiveErrorgetPurchasePlanKindFinTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectCount method
            * override this method for handling normal response from getProjectCount operation
            */
           public void receiveResultgetProjectCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectCount operation
           */
            public void receiveErrorgetProjectCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView2 method
            * override this method for handling normal response from getBudgetView2 operation
            */
           public void receiveResultgetBudgetView2(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetView2Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView2 operation
           */
            public void receiveErrorgetBudgetView2(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByParentID method
            * override this method for handling normal response from getProjectFundingSourceByParentID operation
            */
           public void receiveResultgetProjectFundingSourceByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByParentID operation
           */
            public void receiveErrorgetProjectFundingSourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesByID method
            * override this method for handling normal response from getProjectRolesByID operation
            */
           public void receiveResultgetProjectRolesByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectRolesByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesByID operation
           */
            public void receiveErrorgetProjectRolesByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountCount method
            * override this method for handling normal response from getSupplierBankAccountCount operation
            */
           public void receiveResultgetSupplierBankAccountCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountCount operation
           */
            public void receiveErrorgetSupplierBankAccountCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInUnitBudgetKo method
            * override this method for handling normal response from getRolesInUnitBudgetKo operation
            */
           public void receiveResultgetRolesInUnitBudgetKo(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInUnitBudgetKo operation
           */
            public void receiveErrorgetRolesInUnitBudgetKo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsCount method
            * override this method for handling normal response from getPaymentTermsCount operation
            */
           public void receiveResultgetPaymentTermsCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsCount operation
           */
            public void receiveErrorgetPaymentTermsCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderByIDM method
            * override this method for handling normal response from getProductionOrderByIDM operation
            */
           public void receiveResultgetProductionOrderByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderByIDM operation
           */
            public void receiveErrorgetProductionOrderByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCount method
            * override this method for handling normal response from getUnitBudgetKoKindCount operation
            */
           public void receiveResultgetUnitBudgetKoKindCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoResponsiblePerson method
            * override this method for handling normal response from getAssetCardInfoResponsiblePerson operation
            */
           public void receiveResultgetAssetCardInfoResponsiblePerson(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoResponsiblePersonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoResponsiblePerson operation
           */
            public void receiveErrorgetAssetCardInfoResponsiblePerson(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharing method
            * override this method for handling normal response from getPatternsOfCostSharing operation
            */
           public void receiveResultgetPatternsOfCostSharing(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharing operation
           */
            public void receiveErrorgetPatternsOfCostSharing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountry method
            * override this method for handling normal response from getCountry operation
            */
           public void receiveResultgetCountry(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCountryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountry operation
           */
            public void receiveErrorgetCountry(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyExchangeTable method
            * override this method for handling normal response from getCurrencyExchangeTable operation
            */
           public void receiveResultgetCurrencyExchangeTable(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyExchangeTableResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyExchangeTable operation
           */
            public void receiveErrorgetCurrencyExchangeTable(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeCount method
            * override this method for handling normal response from getApplicationForReservationTypeCount operation
            */
           public void receiveResultgetApplicationForReservationTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeCount operation
           */
            public void receiveErrorgetApplicationForReservationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetObtainmentFormCount method
            * override this method for handling normal response from getAssetObtainmentFormCount operation
            */
           public void receiveResultgetAssetObtainmentFormCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetObtainmentFormCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetObtainmentFormCount operation
           */
            public void receiveErrorgetAssetObtainmentFormCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentCount method
            * override this method for handling normal response from getFinancialDocumentCount operation
            */
           public void receiveResultgetFinancialDocumentCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialDocumentCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentCount operation
           */
            public void receiveErrorgetFinancialDocumentCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocument method
            * override this method for handling normal response from getFinancialDocument operation
            */
           public void receiveResultgetFinancialDocument(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocument operation
           */
            public void receiveErrorgetFinancialDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRateTypeByIDM method
            * override this method for handling normal response from getCurrencyRateTypeByIDM operation
            */
           public void receiveResultgetCurrencyRateTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRateTypeByIDM operation
           */
            public void receiveErrorgetCurrencyRateTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateByIDM method
            * override this method for handling normal response from getVatRateByIDM operation
            */
           public void receiveResultgetVatRateByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateByIDM operation
           */
            public void receiveErrorgetVatRateByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByParentIDM method
            * override this method for handling normal response from getProjectFundingSourceByParentIDM operation
            */
           public void receiveResultgetProjectFundingSourceByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByParentIDM operation
           */
            public void receiveErrorgetProjectFundingSourceByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByParentID method
            * override this method for handling normal response from getProjectBugdetByParentID operation
            */
           public void receiveResultgetProjectBugdetByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByParentID operation
           */
            public void receiveErrorgetProjectBugdetByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceByParentID method
            * override this method for handling normal response from getProjectBudgetItemResourceByParentID operation
            */
           public void receiveResultgetProjectBudgetItemResourceByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInUnitBudgetKoCount method
            * override this method for handling normal response from getRolesInUnitBudgetKoCount operation
            */
           public void receiveResultgetRolesInUnitBudgetKoCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInUnitBudgetKoCount operation
           */
            public void receiveErrorgetRolesInUnitBudgetKoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemType method
            * override this method for handling normal response from getAssetRecordSystemType operation
            */
           public void receiveResultgetAssetRecordSystemType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemType operation
           */
            public void receiveErrorgetAssetRecordSystemType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOwnershipFormByID method
            * override this method for handling normal response from getAssetOwnershipFormByID operation
            */
           public void receiveResultgetAssetOwnershipFormByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOwnershipFormByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOwnershipFormByID operation
           */
            public void receiveErrorgetAssetOwnershipFormByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskByID method
            * override this method for handling normal response from getFinancialTaskByID operation
            */
           public void receiveResultgetFinancialTaskByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskByID operation
           */
            public void receiveErrorgetFinancialTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeFolder method
            * override this method for handling normal response from getDecreeFolder operation
            */
           public void receiveResultgetDecreeFolder(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeFolderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeFolder operation
           */
            public void receiveErrorgetDecreeFolder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountByIDM method
            * override this method for handling normal response from getSupplierBankAccountByIDM operation
            */
           public void receiveResultgetSupplierBankAccountByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountByIDM operation
           */
            public void receiveErrorgetSupplierBankAccountByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeFolderByIDM method
            * override this method for handling normal response from getDecreeFolderByIDM operation
            */
           public void receiveResultgetDecreeFolderByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeFolderByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeFolderByIDM operation
           */
            public void receiveErrorgetDecreeFolderByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudget method
            * override this method for handling normal response from getUnitBudget operation
            */
           public void receiveResultgetUnitBudget(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudget operation
           */
            public void receiveErrorgetUnitBudget(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeType method
            * override this method for handling normal response from getDecreeType operation
            */
           public void receiveResultgetDecreeType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeType operation
           */
            public void receiveErrorgetDecreeType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierCount method
            * override this method for handling normal response from getSupplierCount operation
            */
           public void receiveResultgetSupplierCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierCount operation
           */
            public void receiveErrorgetSupplierCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getExternalDecreeTypeByID method
            * override this method for handling normal response from getExternalDecreeTypeByID operation
            */
           public void receiveResultgetExternalDecreeTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetExternalDecreeTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getExternalDecreeTypeByID operation
           */
            public void receiveErrorgetExternalDecreeTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCount method
            * override this method for handling normal response from getProjectBugdetCount operation
            */
           public void receiveResultgetProjectBugdetCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCount operation
           */
            public void receiveErrorgetProjectBugdetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceCountByParentID method
            * override this method for handling normal response from getProjectFundingSourceCountByParentID operation
            */
           public void receiveResultgetProjectFundingSourceCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceCountByParentID operation
           */
            public void receiveErrorgetProjectFundingSourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPerson method
            * override this method for handling normal response from getPerson operation
            */
           public void receiveResultgetPerson(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPerson operation
           */
            public void receiveErrorgetPerson(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderCount method
            * override this method for handling normal response from getSupplierOrderHeaderCount operation
            */
           public void receiveResultgetSupplierOrderHeaderCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderCount operation
           */
            public void receiveErrorgetSupplierOrderHeaderCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoByID method
            * override this method for handling normal response from getAssetCardInfoByID operation
            */
           public void receiveResultgetAssetCardInfoByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoByID operation
           */
            public void receiveErrorgetAssetCardInfoByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInProjects method
            * override this method for handling normal response from getRolesInProjects operation
            */
           public void receiveResultgetRolesInProjects(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInProjects operation
           */
            public void receiveErrorgetRolesInProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreement method
            * override this method for handling normal response from getPersonAgreement operation
            */
           public void receiveResultgetPersonAgreement(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreement operation
           */
            public void receiveErrorgetPersonAgreement(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesCount method
            * override this method for handling normal response from getProjectRolesCount operation
            */
           public void receiveResultgetProjectRolesCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectRolesCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesCount operation
           */
            public void receiveErrorgetProjectRolesCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByParentIDM method
            * override this method for handling normal response from getProjectBugdetByParentIDM operation
            */
           public void receiveResultgetProjectBugdetByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByParentIDM operation
           */
            public void receiveErrorgetProjectBugdetByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsByID method
            * override this method for handling normal response from getPaymentTermsByID operation
            */
           public void receiveResultgetPaymentTermsByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsByID operation
           */
            public void receiveErrorgetPaymentTermsByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentByIDM method
            * override this method for handling normal response from getDepartmentByIDM operation
            */
           public void receiveResultgetDepartmentByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDepartmentByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentByIDM operation
           */
            public void receiveErrorgetDepartmentByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskByID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskByID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindFinTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskByID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByParentIDM method
            * override this method for handling normal response from getUnitBudgetKoKindByParentIDM operation
            */
           public void receiveResultgetUnitBudgetKoKindByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByParentIDM operation
           */
            public void receiveErrorgetUnitBudgetKoKindByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentType method
            * override this method for handling normal response from getPurchaseDocumentType operation
            */
           public void receiveResultgetPurchaseDocumentType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentType operation
           */
            public void receiveErrorgetPurchaseDocumentType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByID method
            * override this method for handling normal response from getPurchasePlanKindByID operation
            */
           public void receiveResultgetPurchasePlanKindByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByID operation
           */
            public void receiveErrorgetPurchasePlanKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasure method
            * override this method for handling normal response from getUnitOfMeasure operation
            */
           public void receiveResultgetUnitOfMeasure(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasure operation
           */
            public void receiveErrorgetUnitOfMeasure(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFinTaskCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFinTaskCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFinTaskCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFinTaskCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFinTaskCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFinTaskCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderCount method
            * override this method for handling normal response from getProductionOrderCount operation
            */
           public void receiveResultgetProductionOrderCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderCount operation
           */
            public void receiveErrorgetProductionOrderCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatAttributeByID method
            * override this method for handling normal response from getVatAttributeByID operation
            */
           public void receiveResultgetVatAttributeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatAttributeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatAttributeByID operation
           */
            public void receiveErrorgetVatAttributeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseByIDM method
            * override this method for handling normal response from getWarehouseByIDM operation
            */
           public void receiveResultgetWarehouseByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetWarehouseByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseByIDM operation
           */
            public void receiveErrorgetWarehouseByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierByID method
            * override this method for handling normal response from getSupplierByID operation
            */
           public void receiveResultgetSupplierByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierByID operation
           */
            public void receiveErrorgetSupplierByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByID method
            * override this method for handling normal response from getApplicationForReservationTypeByID operation
            */
           public void receiveResultgetApplicationForReservationTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByID operation
           */
            public void receiveErrorgetApplicationForReservationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeCount method
            * override this method for handling normal response from getContractTypeCount operation
            */
           public void receiveResultgetContractTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeCount operation
           */
            public void receiveErrorgetContractTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartment method
            * override this method for handling normal response from getDepartment operation
            */
           public void receiveResultgetDepartment(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDepartmentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartment operation
           */
            public void receiveErrorgetDepartment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetByID method
            * override this method for handling normal response from getContractBogdetByID operation
            */
           public void receiveResultgetContractBogdetByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractBogdetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetByID operation
           */
            public void receiveErrorgetContractBogdetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoResponsiblePersonCount method
            * override this method for handling normal response from getAssetCardInfoResponsiblePersonCount operation
            */
           public void receiveResultgetAssetCardInfoResponsiblePersonCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoResponsiblePersonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoResponsiblePersonCount operation
           */
            public void receiveErrorgetAssetCardInfoResponsiblePersonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatiosByID method
            * override this method for handling normal response from getVatRatiosByID operation
            */
           public void receiveResultgetVatRatiosByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRatiosByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatiosByID operation
           */
            public void receiveErrorgetVatRatiosByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetPurposeByID method
            * override this method for handling normal response from getAssetPurposeByID operation
            */
           public void receiveResultgetAssetPurposeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetPurposeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetPurposeByID operation
           */
            public void receiveErrorgetAssetPurposeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCostCenterByIDM method
            * override this method for handling normal response from getAssetCostCenterByIDM operation
            */
           public void receiveResultgetAssetCostCenterByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCostCenterByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCostCenterByIDM operation
           */
            public void receiveErrorgetAssetCostCenterByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCountByParentIDM method
            * override this method for handling normal response from getUnitBudgetKoKindCountByParentIDM operation
            */
           public void receiveResultgetUnitBudgetKoKindCountByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCountByParentIDM operation
           */
            public void receiveErrorgetUnitBudgetKoKindCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyRateTypeCount method
            * override this method for handling normal response from getCurrencyRateTypeCount operation
            */
           public void receiveResultgetCurrencyRateTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyRateTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyRateTypeCount operation
           */
            public void receiveErrorgetCurrencyRateTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeByID method
            * override this method for handling normal response from getPurchaseDocumentTypeByID operation
            */
           public void receiveResultgetPurchaseDocumentTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeByID operation
           */
            public void receiveErrorgetPurchaseDocumentTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByID method
            * override this method for handling normal response from getProjectBugdetByID operation
            */
           public void receiveResultgetProjectBugdetByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByID operation
           */
            public void receiveErrorgetProjectBugdetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeTypeByID method
            * override this method for handling normal response from getDecreeTypeByID operation
            */
           public void receiveResultgetDecreeTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeTypeByID operation
           */
            public void receiveErrorgetDecreeTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatAttribute method
            * override this method for handling normal response from getVatAttribute operation
            */
           public void receiveResultgetVatAttribute(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatAttributeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatAttribute operation
           */
            public void receiveErrorgetVatAttribute(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonCount method
            * override this method for handling normal response from getAssetLiquidationReasonCount operation
            */
           public void receiveResultgetAssetLiquidationReasonCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetLiquidationReasonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonCount operation
           */
            public void receiveErrorgetAssetLiquidationReasonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetViewCount method
            * override this method for handling normal response from getBudgetViewCount operation
            */
           public void receiveResultgetBudgetViewCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetViewCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetViewCount operation
           */
            public void receiveErrorgetBudgetViewCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationType method
            * override this method for handling normal response from getAssetOperationType operation
            */
           public void receiveResultgetAssetOperationType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOperationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationType operation
           */
            public void receiveErrorgetAssetOperationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierByIDM method
            * override this method for handling normal response from getSupplierByIDM operation
            */
           public void receiveResultgetSupplierByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierByIDM operation
           */
            public void receiveErrorgetSupplierByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetClassificationCount method
            * override this method for handling normal response from getAssetClassificationCount operation
            */
           public void receiveResultgetAssetClassificationCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetClassificationCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetClassificationCount operation
           */
            public void receiveErrorgetAssetClassificationCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractByID method
            * override this method for handling normal response from getContractByID operation
            */
           public void receiveResultgetContractByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractByID operation
           */
            public void receiveErrorgetContractByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFundingCount method
            * override this method for handling normal response from getSourcesOfProjectFundingCount operation
            */
           public void receiveResultgetSourcesOfProjectFundingCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFundingCount operation
           */
            public void receiveErrorgetSourcesOfProjectFundingCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnit method
            * override this method for handling normal response from getOrganizationalUnit operation
            */
           public void receiveResultgetOrganizationalUnit(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnit operation
           */
            public void receiveErrorgetOrganizationalUnit(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesByIDM method
            * override this method for handling normal response from getProjectRolesByIDM operation
            */
           public void receiveResultgetProjectRolesByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectRolesByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesByIDM operation
           */
            public void receiveErrorgetProjectRolesByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeCount method
            * override this method for handling normal response from getAssetRecordSystemTypeCount operation
            */
           public void receiveResultgetAssetRecordSystemTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeCount operation
           */
            public void receiveErrorgetAssetRecordSystemTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyCount method
            * override this method for handling normal response from getCurrencyCount operation
            */
           public void receiveResultgetCurrencyCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyCount operation
           */
            public void receiveErrorgetCurrencyCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesByID method
            * override this method for handling normal response from getDeliveryDocumentTypesByID operation
            */
           public void receiveResultgetDeliveryDocumentTypesByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryDocumentTypesByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesByID operation
           */
            public void receiveErrorgetDeliveryDocumentTypesByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonByID method
            * override this method for handling normal response from getPersonByID operation
            */
           public void receiveResultgetPersonByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonByID operation
           */
            public void receiveErrorgetPersonByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriod method
            * override this method for handling normal response from getAccountingPeriod operation
            */
           public void receiveResultgetAccountingPeriod(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAccountingPeriodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriod operation
           */
            public void receiveErrorgetAccountingPeriod(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRatiosCount method
            * override this method for handling normal response from getVatRatiosCount operation
            */
           public void receiveResultgetVatRatiosCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRatiosCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRatiosCount operation
           */
            public void receiveErrorgetVatRatiosCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByID method
            * override this method for handling normal response from getPurchasePlanByID operation
            */
           public void receiveResultgetPurchasePlanByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByID operation
           */
            public void receiveErrorgetPurchasePlanByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetClassificationByIDM method
            * override this method for handling normal response from getAssetClassificationByIDM operation
            */
           public void receiveResultgetAssetClassificationByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetClassificationByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetClassificationByIDM operation
           */
            public void receiveErrorgetAssetClassificationByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPositionOfSaleByID method
            * override this method for handling normal response from getPositionOfSaleByID operation
            */
           public void receiveResultgetPositionOfSaleByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPositionOfSaleByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPositionOfSaleByID operation
           */
            public void receiveErrorgetPositionOfSaleByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitByID method
            * override this method for handling normal response from getOrganizationalUnitByID operation
            */
           public void receiveResultgetOrganizationalUnitByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitByID operation
           */
            public void receiveErrorgetOrganizationalUnitByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeTypeCount method
            * override this method for handling normal response from getDecreeTypeCount operation
            */
           public void receiveResultgetDecreeTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeTypeCount operation
           */
            public void receiveErrorgetDecreeTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemByID method
            * override this method for handling normal response from getProjectBudgetItemByID operation
            */
           public void receiveResultgetProjectBudgetItemByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemByID operation
           */
            public void receiveErrorgetProjectBudgetItemByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCostCenterByID method
            * override this method for handling normal response from getAssetCostCenterByID operation
            */
           public void receiveResultgetAssetCostCenterByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCostCenterByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCostCenterByID operation
           */
            public void receiveErrorgetAssetCostCenterByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetClassification method
            * override this method for handling normal response from getAssetClassification operation
            */
           public void receiveResultgetAssetClassification(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetClassificationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetClassification operation
           */
            public void receiveErrorgetAssetClassification(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKo method
            * override this method for handling normal response from getUnitBudgetKo operation
            */
           public void receiveResultgetUnitBudgetKo(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKo operation
           */
            public void receiveErrorgetUnitBudgetKo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCostCenter method
            * override this method for handling normal response from getAssetCostCenter operation
            */
           public void receiveResultgetAssetCostCenter(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCostCenterResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCostCenter operation
           */
            public void receiveErrorgetAssetCostCenter(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesByIDM method
            * override this method for handling normal response from getDeliveryDocumentTypesByIDM operation
            */
           public void receiveResultgetDeliveryDocumentTypesByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryDocumentTypesByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesByIDM operation
           */
            public void receiveErrorgetDeliveryDocumentTypesByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeCount method
            * override this method for handling normal response from getEmployeeCount operation
            */
           public void receiveResultgetEmployeeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeCount operation
           */
            public void receiveErrorgetEmployeeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetResponsiblePersonByID method
            * override this method for handling normal response from getAssetResponsiblePersonByID operation
            */
           public void receiveResultgetAssetResponsiblePersonByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetResponsiblePersonByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetResponsiblePersonByID operation
           */
            public void receiveErrorgetAssetResponsiblePersonByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetUnitOrganizationUnit method
            * override this method for handling normal response from getBudgetUnitOrganizationUnit operation
            */
           public void receiveResultgetBudgetUnitOrganizationUnit(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnitResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetUnitOrganizationUnit operation
           */
            public void receiveErrorgetBudgetUnitOrganizationUnit(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatAttributeCount method
            * override this method for handling normal response from getVatAttributeCount operation
            */
           public void receiveResultgetVatAttributeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatAttributeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatAttributeCount operation
           */
            public void receiveErrorgetVatAttributeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceByID method
            * override this method for handling normal response from getProjectBudgetItemResourceByID operation
            */
           public void receiveResultgetProjectBudgetItemResourceByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceByID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceCount method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceCount operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentCount method
            * override this method for handling normal response from getDepartmentCount operation
            */
           public void receiveResultgetDepartmentCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDepartmentCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentCount operation
           */
            public void receiveErrorgetDepartmentCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfo method
            * override this method for handling normal response from getAssetCardInfo operation
            */
           public void receiveResultgetAssetCardInfo(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfo operation
           */
            public void receiveErrorgetAssetCardInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypes method
            * override this method for handling normal response from getDeliveryDocumentTypes operation
            */
           public void receiveResultgetDeliveryDocumentTypes(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryDocumentTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypes operation
           */
            public void receiveErrorgetDeliveryDocumentTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceCountByParentID method
            * override this method for handling normal response from getProjectBudgetItemResourceCountByParentID operation
            */
           public void receiveResultgetProjectBudgetItemResourceCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceCountByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeFolderByID method
            * override this method for handling normal response from getDecreeFolderByID operation
            */
           public void receiveResultgetDecreeFolderByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeFolderByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeFolderByID operation
           */
            public void receiveErrorgetDecreeFolderByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlan method
            * override this method for handling normal response from getPurchasePlan operation
            */
           public void receiveResultgetPurchasePlan(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlan operation
           */
            public void receiveErrorgetPurchasePlan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOwnershipForm method
            * override this method for handling normal response from getAssetOwnershipForm operation
            */
           public void receiveResultgetAssetOwnershipForm(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetOwnershipFormResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOwnershipForm operation
           */
            public void receiveErrorgetAssetOwnershipForm(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyByIDM method
            * override this method for handling normal response from getCurrencyByIDM operation
            */
           public void receiveResultgetCurrencyByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyByIDM operation
           */
            public void receiveErrorgetCurrencyByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductCount method
            * override this method for handling normal response from getCostInvoiceProductCount operation
            */
           public void receiveResultgetCostInvoiceProductCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductCount operation
           */
            public void receiveErrorgetCostInvoiceProductCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByParentIDM method
            * override this method for handling normal response from getPurchasePlanKindByParentIDM operation
            */
           public void receiveResultgetPurchasePlanKindByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByParentIDM operation
           */
            public void receiveErrorgetPurchasePlanKindByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSellerByIDM method
            * override this method for handling normal response from getSellerByIDM operation
            */
           public void receiveResultgetSellerByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSellerByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSellerByIDM operation
           */
            public void receiveErrorgetSellerByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetResponsiblePerson method
            * override this method for handling normal response from getAssetResponsiblePerson operation
            */
           public void receiveResultgetAssetResponsiblePerson(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetResponsiblePersonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetResponsiblePerson operation
           */
            public void receiveErrorgetAssetResponsiblePerson(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetObtainmentFormByIDM method
            * override this method for handling normal response from getAssetObtainmentFormByIDM operation
            */
           public void receiveResultgetAssetObtainmentFormByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetObtainmentFormByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetObtainmentFormByIDM operation
           */
            public void receiveErrorgetAssetObtainmentFormByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProduct method
            * override this method for handling normal response from getCostInvoiceProduct operation
            */
           public void receiveResultgetCostInvoiceProduct(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProduct operation
           */
            public void receiveErrorgetCostInvoiceProduct(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetUnitOrganizationUnitCount method
            * override this method for handling normal response from getBudgetUnitOrganizationUnitCount operation
            */
           public void receiveResultgetBudgetUnitOrganizationUnitCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnitCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetUnitOrganizationUnitCount operation
           */
            public void receiveErrorgetBudgetUnitOrganizationUnitCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceByID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceByID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPriceTypeByID method
            * override this method for handling normal response from getPriceTypeByID operation
            */
           public void receiveResultgetPriceTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPriceTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPriceTypeByID operation
           */
            public void receiveErrorgetPriceTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDecreeFolderCount method
            * override this method for handling normal response from getDecreeFolderCount operation
            */
           public void receiveResultgetDecreeFolderCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDecreeFolderCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDecreeFolderCount operation
           */
            public void receiveErrorgetDecreeFolderCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryByIDM method
            * override this method for handling normal response from getCountryByIDM operation
            */
           public void receiveResultgetCountryByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCountryByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryByIDM operation
           */
            public void receiveErrorgetCountryByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanCount method
            * override this method for handling normal response from getPurchasePlanCount operation
            */
           public void receiveResultgetPurchasePlanCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanCount operation
           */
            public void receiveErrorgetPurchasePlanCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyExchangeTableByID method
            * override this method for handling normal response from getCurrencyExchangeTableByID operation
            */
           public void receiveResultgetCurrencyExchangeTableByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyExchangeTableByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyExchangeTableByID operation
           */
            public void receiveErrorgetCurrencyExchangeTableByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonByID method
            * override this method for handling normal response from getAssetLiquidationReasonByID operation
            */
           public void receiveResultgetAssetLiquidationReasonByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetLiquidationReasonByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonByID operation
           */
            public void receiveErrorgetAssetLiquidationReasonByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoCount method
            * override this method for handling normal response from getAssetCardInfoCount operation
            */
           public void receiveResultgetAssetCardInfoCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetCardInfoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoCount operation
           */
            public void receiveErrorgetAssetCardInfoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectAccountCount method
            * override this method for handling normal response from getProjectAccountCount operation
            */
           public void receiveResultgetProjectAccountCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccountCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectAccountCount operation
           */
            public void receiveErrorgetProjectAccountCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetPurposeCount method
            * override this method for handling normal response from getAssetPurposeCount operation
            */
           public void receiveResultgetAssetPurposeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetPurposeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetPurposeCount operation
           */
            public void receiveErrorgetAssetPurposeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriodCount method
            * override this method for handling normal response from getAssetAccountingPeriodCount operation
            */
           public void receiveResultgetAssetAccountingPeriodCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetAccountingPeriodCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriodCount operation
           */
            public void receiveErrorgetAssetAccountingPeriodCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountByIDM method
            * override this method for handling normal response from getCompanyBankAccountByIDM operation
            */
           public void receiveResultgetCompanyBankAccountByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCompanyBankAccountByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountByIDM operation
           */
            public void receiveErrorgetCompanyBankAccountByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindFundSourceCountByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindFundSourceCountByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindFundSourceCountByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindFundSourceCountByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindFundSourceCountByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindFundSourceCountByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSeller method
            * override this method for handling normal response from getSeller operation
            */
           public void receiveResultgetSeller(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSellerResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSeller operation
           */
            public void receiveErrorgetSeller(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouse method
            * override this method for handling normal response from getWarehouse operation
            */
           public void receiveResultgetWarehouse(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetWarehouseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouse operation
           */
            public void receiveErrorgetWarehouse(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetSettlementSubPeriodByID method
            * override this method for handling normal response from getAssetSettlementSubPeriodByID operation
            */
           public void receiveResultgetAssetSettlementSubPeriodByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetSettlementSubPeriodByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetSettlementSubPeriodByID operation
           */
            public void receiveErrorgetAssetSettlementSubPeriodByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAbsenceTypeByIDM method
            * override this method for handling normal response from getAbsenceTypeByIDM operation
            */
           public void receiveResultgetAbsenceTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAbsenceTypeByIDM operation
           */
            public void receiveErrorgetAbsenceTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFunding method
            * override this method for handling normal response from getSourcesOfProjectFunding operation
            */
           public void receiveResultgetSourcesOfProjectFunding(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFunding operation
           */
            public void receiveErrorgetSourcesOfProjectFunding(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonByIDM method
            * override this method for handling normal response from getAssetLiquidationReasonByIDM operation
            */
           public void receiveResultgetAssetLiquidationReasonByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetLiquidationReasonByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonByIDM operation
           */
            public void receiveErrorgetAssetLiquidationReasonByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByIDM method
            * override this method for handling normal response from getUnitBudgetByIDM operation
            */
           public void receiveResultgetUnitBudgetByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByIDM operation
           */
            public void receiveErrorgetUnitBudgetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRate method
            * override this method for handling normal response from getVatRate operation
            */
           public void receiveResultgetVatRate(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRate operation
           */
            public void receiveErrorgetVatRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractByIDM method
            * override this method for handling normal response from getContractByIDM operation
            */
           public void receiveResultgetContractByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractByIDM operation
           */
            public void receiveErrorgetContractByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureByIDM method
            * override this method for handling normal response from getUnitOfMeasureByIDM operation
            */
           public void receiveResultgetUnitOfMeasureByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureByIDM operation
           */
            public void receiveErrorgetUnitOfMeasureByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKind method
            * override this method for handling normal response from getUnitBudgetKoKind operation
            */
           public void receiveResultgetUnitBudgetKoKind(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKind operation
           */
            public void receiveErrorgetUnitBudgetKoKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByID method
            * override this method for handling normal response from getUnitBudgetKoByID operation
            */
           public void receiveResultgetUnitBudgetKoByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByID operation
           */
            public void receiveErrorgetUnitBudgetKoByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdet method
            * override this method for handling normal response from getProjectBugdet operation
            */
           public void receiveResultgetProjectBugdet(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdet operation
           */
            public void receiveErrorgetProjectBugdet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeByID method
            * override this method for handling normal response from getAssetRecordSystemTypeByID operation
            */
           public void receiveResultgetAssetRecordSystemTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeByID operation
           */
            public void receiveErrorgetAssetRecordSystemTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyExchangeTableByIDM method
            * override this method for handling normal response from getCurrencyExchangeTableByIDM operation
            */
           public void receiveResultgetCurrencyExchangeTableByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyExchangeTableByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyExchangeTableByIDM operation
           */
            public void receiveErrorgetCurrencyExchangeTableByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsByIDM method
            * override this method for handling normal response from getPaymentTermsByIDM operation
            */
           public void receiveResultgetPaymentTermsByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsByIDM operation
           */
            public void receiveErrorgetPaymentTermsByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserByID method
            * override this method for handling normal response from getErpUserByID operation
            */
           public void receiveResultgetErpUserByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetErpUserByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserByID operation
           */
            public void receiveErrorgetErpUserByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFundingSourceByID method
            * override this method for handling normal response from getProjectFundingSourceByID operation
            */
           public void receiveResultgetProjectFundingSourceByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectFundingSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFundingSourceByID operation
           */
            public void receiveErrorgetProjectFundingSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderByIDM method
            * override this method for handling normal response from getSupplierOrderHeaderByIDM operation
            */
           public void receiveResultgetSupplierOrderHeaderByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderByIDM operation
           */
            public void receiveErrorgetSupplierOrderHeaderByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationTypeCount method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationTypeCount operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationTypeCount operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitByIDM method
            * override this method for handling normal response from getOrganizationalUnitByIDM operation
            */
           public void receiveResultgetOrganizationalUnitByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitByIDM operation
           */
            public void receiveErrorgetOrganizationalUnitByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProject method
            * override this method for handling normal response from getProject operation
            */
           public void receiveResultgetProject(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProject operation
           */
            public void receiveErrorgetProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getExternalDecreeType method
            * override this method for handling normal response from getExternalDecreeType operation
            */
           public void receiveResultgetExternalDecreeType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetExternalDecreeTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getExternalDecreeType operation
           */
            public void receiveErrorgetExternalDecreeType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharingCount method
            * override this method for handling normal response from getPatternsOfCostSharingCount operation
            */
           public void receiveResultgetPatternsOfCostSharingCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharingCount operation
           */
            public void receiveErrorgetPatternsOfCostSharingCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetClassificationByID method
            * override this method for handling normal response from getAssetClassificationByID operation
            */
           public void receiveResultgetAssetClassificationByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetClassificationByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetClassificationByID operation
           */
            public void receiveErrorgetAssetClassificationByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByID method
            * override this method for handling normal response from getProjectByID operation
            */
           public void receiveResultgetProjectByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByID operation
           */
            public void receiveErrorgetProjectByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCountByParentIDM method
            * override this method for handling normal response from getPurchasePlanKindCountByParentIDM operation
            */
           public void receiveResultgetPurchasePlanKindCountByParentIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindCountByParentIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCountByParentIDM operation
           */
            public void receiveErrorgetPurchasePlanKindCountByParentIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractCount method
            * override this method for handling normal response from getContractCount operation
            */
           public void receiveResultgetContractCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractCount operation
           */
            public void receiveErrorgetContractCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getData method
            * override this method for handling normal response from getData operation
            */
           public void receiveResultgetData(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getData operation
           */
            public void receiveErrorgetData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetObtainmentFormByID method
            * override this method for handling normal response from getAssetObtainmentFormByID operation
            */
           public void receiveResultgetAssetObtainmentFormByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetObtainmentFormByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetObtainmentFormByID operation
           */
            public void receiveErrorgetAssetObtainmentFormByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatAttributeByIDM method
            * override this method for handling normal response from getVatAttributeByIDM operation
            */
           public void receiveResultgetVatAttributeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatAttributeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatAttributeByIDM operation
           */
            public void receiveErrorgetVatAttributeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResource method
            * override this method for handling normal response from getProjectBudgetItemResource operation
            */
           public void receiveResultgetProjectBudgetItemResource(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResource operation
           */
            public void receiveErrorgetProjectBudgetItemResource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharingByID method
            * override this method for handling normal response from getPatternsOfCostSharingByID operation
            */
           public void receiveResultgetPatternsOfCostSharingByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharingByID operation
           */
            public void receiveErrorgetPatternsOfCostSharingByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByID method
            * override this method for handling normal response from getUnitBudgetByID operation
            */
           public void receiveResultgetUnitBudgetByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByID operation
           */
            public void receiveErrorgetUnitBudgetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByIDM method
            * override this method for handling normal response from getPurchasePlanByIDM operation
            */
           public void receiveResultgetPurchasePlanByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByIDM operation
           */
            public void receiveErrorgetPurchasePlanByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSellerCount method
            * override this method for handling normal response from getSellerCount operation
            */
           public void receiveResultgetSellerCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSellerCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSellerCount operation
           */
            public void receiveErrorgetSellerCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementByID method
            * override this method for handling normal response from getPersonAgreementByID operation
            */
           public void receiveResultgetPersonAgreementByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementByID operation
           */
            public void receiveErrorgetPersonAgreementByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeByID method
            * override this method for handling normal response from getContractTypeByID operation
            */
           public void receiveResultgetContractTypeByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetContractTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeByID operation
           */
            public void receiveErrorgetContractTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccount method
            * override this method for handling normal response from getCompanyBankAccount operation
            */
           public void receiveResultgetCompanyBankAccount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCompanyBankAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccount operation
           */
            public void receiveErrorgetCompanyBankAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCount method
            * override this method for handling normal response from getCount operation
            */
           public void receiveResultgetCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCount operation
           */
            public void receiveErrorgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryCount method
            * override this method for handling normal response from getCountryCount operation
            */
           public void receiveResultgetCountryCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCountryCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryCount operation
           */
            public void receiveErrorgetCountryCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrder method
            * override this method for handling normal response from getProductionOrder operation
            */
           public void receiveResultgetProductionOrder(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrder operation
           */
            public void receiveErrorgetProductionOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyByID method
            * override this method for handling normal response from getCurrencyByID operation
            */
           public void receiveResultgetCurrencyByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyByID operation
           */
            public void receiveErrorgetCurrencyByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSellerByID method
            * override this method for handling normal response from getSellerByID operation
            */
           public void receiveResultgetSellerByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSellerByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSellerByID operation
           */
            public void receiveErrorgetSellerByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplier method
            * override this method for handling normal response from getSupplier operation
            */
           public void receiveResultgetSupplier(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplier operation
           */
            public void receiveErrorgetSupplier(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserCount method
            * override this method for handling normal response from getErpUserCount operation
            */
           public void receiveResultgetErpUserCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetErpUserCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserCount operation
           */
            public void receiveErrorgetErpUserCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceCount method
            * override this method for handling normal response from getProjectBudgetItemResourceCount operation
            */
           public void receiveResultgetProjectBudgetItemResourceCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceCount operation
           */
            public void receiveErrorgetProjectBudgetItemResourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByIDM method
            * override this method for handling normal response from getProjectByIDM operation
            */
           public void receiveResultgetProjectByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByIDM operation
           */
            public void receiveErrorgetProjectByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountCount method
            * override this method for handling normal response from getCompanyBankAccountCount operation
            */
           public void receiveResultgetCompanyBankAccountCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCompanyBankAccountCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountCount operation
           */
            public void receiveErrorgetCompanyBankAccountCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyExchangeTableCount method
            * override this method for handling normal response from getCurrencyExchangeTableCount operation
            */
           public void receiveResultgetCurrencyExchangeTableCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyExchangeTableCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyExchangeTableCount operation
           */
            public void receiveErrorgetCurrencyExchangeTableCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindFinTaskByParentID method
            * override this method for handling normal response from getPurchasePlanKindFinTaskByParentID operation
            */
           public void receiveResultgetPurchasePlanKindFinTaskByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindFinTaskByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindFinTaskByParentID operation
           */
            public void receiveErrorgetPurchasePlanKindFinTaskByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBookkeepingSystemCount method
            * override this method for handling normal response from getBookkeepingSystemCount operation
            */
           public void receiveResultgetBookkeepingSystemCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBookkeepingSystemCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBookkeepingSystemCount operation
           */
            public void receiveErrorgetBookkeepingSystemCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPriceType method
            * override this method for handling normal response from getPriceType operation
            */
           public void receiveResultgetPriceType(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPriceTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPriceType operation
           */
            public void receiveErrorgetPriceType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeCount method
            * override this method for handling normal response from getPurchaseDocumentTypeCount operation
            */
           public void receiveResultgetPurchaseDocumentTypeCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeCount operation
           */
            public void receiveErrorgetPurchaseDocumentTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocation method
            * override this method for handling normal response from getDeliveryLocation operation
            */
           public void receiveResultgetDeliveryLocation(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocation operation
           */
            public void receiveErrorgetDeliveryLocation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPriceTypeByIDM method
            * override this method for handling normal response from getPriceTypeByIDM operation
            */
           public void receiveResultgetPriceTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPriceTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPriceTypeByIDM operation
           */
            public void receiveErrorgetPriceTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBookkeepingSystem method
            * override this method for handling normal response from getBookkeepingSystem operation
            */
           public void receiveResultgetBookkeepingSystem(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBookkeepingSystemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBookkeepingSystem operation
           */
            public void receiveErrorgetBookkeepingSystem(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceCount method
            * override this method for handling normal response from getFundingSourceCount operation
            */
           public void receiveResultgetFundingSourceCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceCount operation
           */
            public void receiveErrorgetFundingSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemByParentID method
            * override this method for handling normal response from getProjectBudgetItemByParentID operation
            */
           public void receiveResultgetProjectBudgetItemByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemByParentID operation
           */
            public void receiveErrorgetProjectBudgetItemByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceByIDM method
            * override this method for handling normal response from getFundingSourceByIDM operation
            */
           public void receiveResultgetFundingSourceByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceByIDM operation
           */
            public void receiveErrorgetFundingSourceByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByParentID method
            * override this method for handling normal response from getUnitBudgetKoKindByParentID operation
            */
           public void receiveResultgetUnitBudgetKoKindByParentID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindByParentIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByParentID operation
           */
            public void receiveErrorgetUnitBudgetKoKindByParentID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccount method
            * override this method for handling normal response from getSupplierBankAccount operation
            */
           public void receiveResultgetSupplierBankAccount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccount operation
           */
            public void receiveErrorgetSupplierBankAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodByIDM method
            * override this method for handling normal response from getAccountingPeriodByIDM operation
            */
           public void receiveResultgetAccountingPeriodByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAccountingPeriodByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodByIDM operation
           */
            public void receiveErrorgetAccountingPeriodByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeByIDM method
            * override this method for handling normal response from getAssetRecordSystemTypeByIDM operation
            */
           public void receiveResultgetAssetRecordSystemTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAssetRecordSystemTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeByIDM operation
           */
            public void receiveErrorgetAssetRecordSystemTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetCount method
            * override this method for handling normal response from getUnitBudgetCount operation
            */
           public void receiveResultgetUnitBudgetCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetCount operation
           */
            public void receiveErrorgetUnitBudgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getExternalDecreeTypeByIDM method
            * override this method for handling normal response from getExternalDecreeTypeByIDM operation
            */
           public void receiveResultgetExternalDecreeTypeByIDM(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetExternalDecreeTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getExternalDecreeTypeByIDM operation
           */
            public void receiveErrorgetExternalDecreeTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRequestForReservationCount method
            * override this method for handling normal response from getRequestForReservationCount operation
            */
           public void receiveResultgetRequestForReservationCount(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservationCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRequestForReservationCount operation
           */
            public void receiveErrorgetRequestForReservationCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductByID method
            * override this method for handling normal response from getCostInvoiceProductByID operation
            */
           public void receiveResultgetCostInvoiceProductByID(
                    pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductByID operation
           */
            public void receiveErrorgetCostInvoiceProductByID(java.lang.Exception e) {
            }
                


    }
    