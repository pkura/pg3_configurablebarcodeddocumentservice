package pl.compan.docusafe.ws.client.simple.general;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.builder.SAXOMBuilder;
import org.apache.axis2.AxisFault;
import org.apache.log4j.Logger;

import pl.compan.docusafe.ws.simple.general.dokzak.DokzakTYPE;
import pl.compan.docusafe.ws.simple.general.dokzak.ObjectFactory;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

public class ExportCostInvoiceFactory {
	private CostInvoiceCreateProxyStub stub;
	private final String SERVICE_PATH = "/services/CostInvoiceCreateProxy";
	protected static Logger log;
		
	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(ExportCostInvoiceFactory.class);
		}
		return log;
	}

	private void initConfiguration() throws Exception {
		String axis_endpoint_path = "axis.exportFakturyKosztowej.endpoint";
		String endpoint = Docusafe.getAdditionProperty(axis_endpoint_path);			
		AxisClientConfigurator conf = new AxisClientConfigurator();
		stub = new CostInvoiceCreateProxyStub(conf.getAxisConfigurationContext(),endpoint);
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public ExportCostInvoiceFactory() {

	}

	/**
	 * Metoda statyczna do pobierania zainicjowanego kontekstu us�ugi. Kontekst
	 * jest ju� zainicjowany parametrami konfiguracyjnymi Axis'a.
	 * 
	 * @return
	 */
	private CostInvoiceCreateProxyStub getInstance() {
		try {
			initConfiguration();
		} catch (Exception e) {
			getLogger().debug(e);
		}

		return stub;
	}
	
	/**
	 * Metoda wysyla fakture kosztowa podana w parametrze
	 * 
	 * @param dokzak
	 * @return guid
	 * @throws JAXBException
	 * @throws AxisFault
	 */
	public String sendInvoice(DokzakTYPE dokzak) throws AxisFault, JAXBException{
		String guid = null;
		
		OMElement response = getInstance()._getServiceClient().sendReceive(getXmlFromDokzak(dokzak));

		guid = response.getFirstElement().getText();
		
		return guid;
	}
	
	private OMElement getXmlFromDokzak(DokzakTYPE dokzak) throws JAXBException{
		ObjectFactory factory = new ObjectFactory();
		JAXBContext jaxbContext = JAXBContext.newInstance("pl.compan.docusafe.ws.simple.general.dokzak");
		Marshaller marshaller = jaxbContext.createMarshaller();
		
		SAXOMBuilder builder = new SAXOMBuilder();

        marshaller.marshal(factory.createDokzak(dokzak), builder);
        
        OMElement xmlElement = builder.getRootElement();

		return xmlElement;
	}
}
