package pl.compan.docusafe.ws.client.simple.general;

import org.apache.log4j.Logger;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;

public class GenericDocumentServiceStubFactory {
	
	private static GenericDocumentServiceStub stub;
	private final static String SERVICE_PATH = "/services/GenericDocumentService";
	protected static Logger log;

	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(DictionaryServiceFactory.class);
		}
		return log;
	}

	private static void initConfiguration() throws Exception {
		String axis_endpoint_path = "axisGD.endpoint";
		String endpoint = Docusafe.getAdditionProperty(axis_endpoint_path);			
		AxisClientConfigurator conf = new AxisClientConfigurator();
		stub = new GenericDocumentServiceStub(conf.getAxisConfigurationContext(),endpoint);
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public GenericDocumentServiceStubFactory() {

	}

	/**
	 * Metoda statyczna do pobierania zainicjowanego kontekstu us�ugi. Kontekst
	 * jest ju� zainicjowany parametrami konfiguracyjnymi Axis'a.
	 * 
	 * @return
	 */
	public static GenericDocumentServiceStub getInstance() {
		try {
			if(stub==null){
				initConfiguration();
			}
			else return stub;

		} catch (Exception e) {
			log.debug(e);
		}

		return stub;
	}
}
