package pl.compan.docusafe.ws.client.simple.general;

import java.rmi.RemoteException;

import org.apache.log4j.Logger;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceStub.GetStatusForGuid;
import pl.compan.docusafe.ws.client.simple.general.RequestServiceStub.RequestStatus;

public class RequestServiceFactory {
	private RequestServiceStub stub;
	private final String SERVICE_PATH = "/services/RequestService";
	protected static Logger log;
		
	public enum SimpleErpStatusFaktury{
		NEW, 			//Komunikat gotowy do pobrania przez szyn�.
		PROCESSING, 	//Komunikat jest przetwarzany przez szyn� danych
		FINISHED, 		//Komunikat zosta� poprawnie dodany do Simple.ERP
		INVALID,		//Komunikat nie przeszed� wst�pnej walidacji XSD
		FAULT;			//Komunikat nie zosta� dodany do Simple.ERP � system zg�osi� kod
	}
	
	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(RequestServiceFactory.class);
		}
		return log;
	}

	private void initConfiguration() throws Exception {
		String axis_endpoint_path = "axis.sprawdzStanFaktury.endpoint";
		String endpoint = Docusafe.getAdditionProperty(axis_endpoint_path);			
		AxisClientConfigurator conf = new AxisClientConfigurator();
		stub = new RequestServiceStub(conf.getAxisConfigurationContext(),endpoint);
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public RequestServiceFactory() {

	}

	/**
	 * Metoda statyczna do pobierania zainicjowanego kontekstu us�ugi. Kontekst
	 * jest ju� zainicjowany parametrami konfiguracyjnymi Axis'a.
	 * 
	 * @return
	 */
	private RequestServiceStub getInstance() {
		try {
			initConfiguration();
		} catch (Exception e) {
			getLogger().debug(e);
		}

		return stub;
	}
	
	public SimpleErpStatusFaktury getStatusForGuid(String guid) throws RemoteException{
		String wynik = null;
		
		GetStatusForGuid getStatusForGuid = new GetStatusForGuid();
		getStatusForGuid.setGuid(guid);
		
		RequestServiceStub.GetStatusForGuidResponse response;
		response = getInstance().getStatusForGuid(getStatusForGuid);
		
		RequestStatus requestStatus = response.get_return();
		
		wynik = requestStatus.getStateName();
		
		return SimpleErpStatusFaktury.valueOf(wynik);
	}

    public RequestStatus getRequestStatusForGuid(String guid) throws RemoteException{
        String wynik = null;

        GetStatusForGuid getStatusForGuid = new GetStatusForGuid();
        getStatusForGuid.setGuid(guid);

        RequestServiceStub.GetStatusForGuidResponse response;
        response = getInstance().getStatusForGuid(getStatusForGuid);

        return response.get_return();
    }
}
