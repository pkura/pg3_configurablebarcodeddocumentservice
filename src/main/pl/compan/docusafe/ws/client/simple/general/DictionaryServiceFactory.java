package pl.compan.docusafe.ws.client.simple.general;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.axis.AxisClientConfigurator;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.AbsenceType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ApplicationForReservationType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetUnitOrganizationUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.BudgetView;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.CostInvoiceProduct;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Currency;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.DeliveryLocation;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Employee;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FinancialTask;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.FundingSource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetAbsenceTypeResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetApplicationForReservationTypeResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnitCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnitCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetUnitOrganizationUnitResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetView;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetViewCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetViewCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetBudgetViewResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProduct;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCostInvoiceProductResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrency;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetCurrencyResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocation;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetDeliveryLocationResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployee;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetEmployeeResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTask;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFinancialTaskResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetFundingSourceResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetOrganizationalUnitResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharing;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPatternsOfCostSharingResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTerms;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPaymentTermsResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPerson;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreement;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonAgreementResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPersonResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrder;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProductionOrderResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProject;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccountCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccountCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectAccountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItem;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResourceResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBudgetItemResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdet;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectBugdetResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetProjectResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchaseDocumentTypeResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlan;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindByID;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindByIDResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanKindResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetPurchasePlanResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservation;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservationCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservationCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRequestForReservationResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjects;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjectsCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjectsCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInProjectsResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKoCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKoCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetRolesInUnitBudgetKoResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFunding;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSourcesOfProjectFundingResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplier;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierBankAccountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeader;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierOrderHeaderResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetSupplierResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudget;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoKindResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetKoResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitBudgetResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasure;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetUnitOfMeasureResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRate;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateCount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateCountResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.GetVatRateResponse;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.OrganizationalUnit;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PatternsOfCostSharing;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PaymentTerms;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Person;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PersonAgreement;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProductionOrder;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Project;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItem;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBudgetItemResource;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.ProjectBugdet;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchaseDocumentType;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlan;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.PurchasePlanKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RequestForReservation;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInProjects;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.RolesInUnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SourcesOfProjectFunding;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.Supplier;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierBankAccount;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.SupplierOrderHeader;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudget;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKo;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitBudgetKoKind;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.UnitOfMeasure;
import pl.compan.docusafe.ws.client.simple.general.DictionaryServiceStub.VatRate;

public class DictionaryServiceFactory {

	private DictionaryServiceStub stub;
	private final String SERVICE_PATH = "/services/DictionaryService";
	protected static Logger log;

	private Logger getLogger() {
		if (log == null) {
			log = Logger.getLogger(DictionaryServiceFactory.class);
		}
		return log;
	}

	private void initConfiguration() throws Exception {
		String axis_endpoint_path = "axis.endpoint";
		String endpoint = Docusafe.getAdditionProperty(axis_endpoint_path);			
		AxisClientConfigurator conf = new AxisClientConfigurator();
		stub = new DictionaryServiceStub(conf.getAxisConfigurationContext(),endpoint);
		conf.setUpHttpParameters(stub, SERVICE_PATH);
	}

	public DictionaryServiceFactory() {

	}

	/**
	 * Metoda statyczna do pobierania zainicjowanego kontekstu us�ugi. Kontekst
	 * jest ju� zainicjowany parametrami konfiguracyjnymi Axis'a.
	 * 
	 * @return
	 */
	private DictionaryServiceStub getInstance() {
		try {
			initConfiguration();
		} catch (Exception e) {
			log.debug(e);
		}

		return stub;
	}

	/**
	 * Metoda zwraca liczb� jednostek organizacyjnych >=0 - liczba wierszy -1 -
	 * b��d
	 * 
	 * @return
	 */
	private long getOrganizationUnitCount() {
		GetOrganizationalUnitCount gouc = new GetOrganizationalUnitCount();

		try {
			GetOrganizationalUnitCountResponse result = getInstance()
					.getOrganizationalUnitCount(gouc);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}

	/**
	 * Metoda zwraca OrganizationUnits w liczbie nie wi�kszej, ni� za�o�ona jako
	 * parametr numberOfObjects
	 * 
	 * @param numberOfObjects
	 * @return List<OrganizationalUnit> - lista wynik�w
	 * @return null - wyt�pi� b��d podczas wczytywania
	 */
	public List<OrganizationalUnit> getOrganizationUnits() {
		// Wywo�ywany serwis dzieli wyniki wed�ug: ilo�� na stronie, liczba
		// stron
		// Masymalny rozmiar na stronie = 1000,
		// Strony zaczynaj� si� od 1
		// I tak �eby zwr�ci� np. 14000 pozycji, nale�y pobra� strony od 1 do 14
		// z rozmiarem maksymalnym 1000 na stron�.
		// Metoda robi za u�ytkownika paginowanie i zwraca komplet danych

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getOrganizationUnitCount());

		List<OrganizationalUnit> result = new ArrayList<OrganizationalUnit>();

		try {

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetOrganizationalUnit params = new GetOrganizationalUnit();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetOrganizationalUnitResponse response = getInstance()
						.getOrganizationalUnit(params);

				for (OrganizationalUnit _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}

		return null;
	}


	// Waluta
	private long getCurrencyCount() throws RemoteException {
		GetCurrencyCount gouc = new GetCurrencyCount();
			GetCurrencyCountResponse result = getInstance()
					.getCurrencyCount(gouc);
			return result.get_return();
	}

	public List<Currency> getCurrency() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getCurrencyCount());

		List<Currency> result = new ArrayList<Currency>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetCurrency params = new GetCurrency();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetCurrencyResponse response = getInstance()
						.getCurrency(params);

				for (Currency _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
	}


	// Typ p�atno�ci
	private long getPaymentTermsCount() throws RemoteException {
		GetPaymentTermsCount gouc = new GetPaymentTermsCount();

			GetPaymentTermsCountResponse result = getInstance()
					.getPaymentTermsCount(gouc);
			return result.get_return();
	}

	public List<PaymentTerms> getPaymentTerms() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getPaymentTermsCount());

		List<PaymentTerms> result = new ArrayList<PaymentTerms>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetPaymentTerms params = new GetPaymentTerms();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetPaymentTermsResponse response = getInstance()
						.getPaymentTerms(params);

				for (PaymentTerms _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;

	}

	// Kontrahenci
	private long getSuppilierCount() {
		GetSupplierCount gouc = new GetSupplierCount();

		try {
			GetSupplierCountResponse result = getInstance()
					.getSupplierCount(gouc);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}

	public List<Supplier> getSupplier() {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getSuppilierCount());

		List<Supplier> result = new ArrayList<Supplier>();

		try {

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetSupplier params = new GetSupplier();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetSupplierResponse response = getInstance()
						.getSupplier(params);

				for (Supplier _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}

		return null;
	}

	// miejsce dostawy
	private long getDeliveryLocationCount() throws RemoteException {
		GetDeliveryLocationCount gouc = new GetDeliveryLocationCount();


			GetDeliveryLocationCountResponse result = getInstance()
					.getDeliveryLocationCount(gouc);
			return result.get_return();

	}

	public List<DeliveryLocation> getDeliveryLocation() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getDeliveryLocationCount());

		List<DeliveryLocation> result = new ArrayList<DeliveryLocation>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetDeliveryLocation params = new GetDeliveryLocation();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetDeliveryLocationResponse response = getInstance()
						.getDeliveryLocation(params);

				for (DeliveryLocation _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;

	}

	// typy dokument�w
	private long getPurchaseDocumentTypeCount() throws RemoteException {
		GetPurchaseDocumentTypeCount gouc = new GetPurchaseDocumentTypeCount();

			GetPurchaseDocumentTypeCountResponse result = getInstance()
					.getPurchaseDocumentTypeCount(gouc);
			return result.get_return();
	}

	public List<PurchaseDocumentType> getPurchaseDocumentType() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getPurchaseDocumentTypeCount());

		List<PurchaseDocumentType> result = new ArrayList<PurchaseDocumentType>();
			for (int i = 1; i <= liczbaIteracji; i++) {
				GetPurchaseDocumentType params = new GetPurchaseDocumentType();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetPurchaseDocumentTypeResponse response = getInstance()
						.getPurchaseDocumentType(params);

				for (PurchaseDocumentType _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
	}

	// stawki vat
	private long getVatRateCount() throws RemoteException {
		GetVatRateCount gouc = new GetVatRateCount();
			GetVatRateCountResponse result = getInstance()
					.getVatRateCount(gouc);
			return result.get_return();
	}

	public List<VatRate> getVatRate() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getVatRateCount());

		List<VatRate> result = new ArrayList<VatRate>();
			for (int i = 1; i <= liczbaIteracji; i++) {
				GetVatRate params = new GetVatRate();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetVatRateResponse response = getInstance()
						.getVatRate(params);

				for (VatRate _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
	}

	// produkty
	private long getCostInvoiceProductCount() throws RemoteException {
		GetCostInvoiceProductCount gouc = new GetCostInvoiceProductCount();

			GetCostInvoiceProductCountResponse result = getInstance()
					.getCostInvoiceProductCount(gouc);
			return result.get_return();
	}

	public List<CostInvoiceProduct> getCostInvoiceProduct() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getCostInvoiceProductCount());

		List<CostInvoiceProduct> result = new ArrayList<CostInvoiceProduct>();


			for (int i = 1; i <= liczbaIteracji; i++) {
				GetCostInvoiceProduct params = new GetCostInvoiceProduct();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetCostInvoiceProductResponse response = getInstance()
						.getCostInvoiceProduct(params);

				for (CostInvoiceProduct _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
	}

	// jednostki miary
	private long getUnitOfMeasureCount() throws RemoteException {
		GetUnitOfMeasureCount gouc = new GetUnitOfMeasureCount();

			GetUnitOfMeasureCountResponse result = getInstance()
					.getUnitOfMeasureCount(gouc);
			return result.get_return();
	}

	public List<UnitOfMeasure> getUnitOfMeasure() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getUnitOfMeasureCount());

		List<UnitOfMeasure> result = new ArrayList<UnitOfMeasure>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetUnitOfMeasure params = new GetUnitOfMeasure();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetUnitOfMeasureResponse response = getInstance()
						.getUnitOfMeasure(params);

				for (UnitOfMeasure _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;

	}
	/*------------------------------- EMPLOYEE ----------------------------------------------*/
	// Pracownik - employee

	/**
	 * Metoda zwraca liczb� jednostek wszystkich u�ytkownik�w  >=0 - liczba wierszy -1 -
	 * b��d
	 * 
	 * @return
	 */
	private long getEmployeeCount() {
		GetEmployeeCount count = new GetEmployeeCount();

		try {
			GetEmployeeCountResponse result = getInstance().getEmployeeCount(count);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}	

	/**
	 * Metoda zwraca Employee w liczbie nie wi�kszej, ni� za�o�ona jako
	 * parametr numberOfObjects
	 * 
	 * @param numberOfObjects
	 * @return List<Employee> - lista wynik�w
	 * @return null - wyt�pi� b��d podczas wczytywania
	 */
	public List<Employee> getEmployees() {
		// Wywo�ywany serwis dzieli wyniki wed�ug: ilo�� na stronie, liczba
		// stron
		// Masymalny rozmiar na stronie = 1000,
		// Strony zaczynaj� si� od 1
		// I tak �eby zwr�ci� np. 14000 pozycji, nale�y pobra� strony od 1 do 14
		// z rozmiarem maksymalnym 1000 na stron�.
		// Metoda robi za u�ytkownika paginowanie i zwraca komplet danych

		int liczbaIteracji = DictionaryUtils.GetIterationCount(getEmployeeCount());

		//int liczbaIteracji = 1;

		List<Employee> result = new ArrayList<Employee>();

		try {

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetEmployee params = new GetEmployee();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				//params.setPageSize(100);
				params.setPage(i);

				GetEmployeeResponse response = getInstance()
						.getEmployee(params);

				for (Employee _unit : response.get_return()) {
					result.add(_unit);
				}

			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}
		return null;
	}



	/*----------------------------- PERSON -> ZMIANA PO EMPLOYEE ------------------------------*/
	// Person

	/**
	 * Metoda zwraca liczb� jednostek wszystkich obiekt�w Person  >=0 - liczba wierszy 
	 * -1 b��d 
	 * @return
	 */
	private long getPersonCount() {
		GetPersonCount count = new GetPersonCount();

		try {
			GetPersonCountResponse result = getInstance().getPersonCount(count);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}

	/**
	 * Metoda zwraca Person w liczbie nie wi�kszej, ni� za�o�ona jako
	 * parametr numberOfObjects
	 * 
	 * @param numberOfObjects
	 * @return List<Person> - lista wynik�w
	 * @return null - wyt�pi� b��d podczas wczytywania
	 */
	public List<Person> getPersons() {
		// Wywo�ywany serwis dzieli wyniki wed�ug: ilo�� na stronie, liczba
		// stron
		// Masymalny rozmiar na stronie = 1000,
		// Strony zaczynaj� si� od 1
		// I tak �eby zwr�ci� np. 14000 pozycji, nale�y pobra� strony od 1 do 14
		// z rozmiarem maksymalnym 1000 na stron�.
		// Metoda robi za u�ytkownika paginowanie i zwraca komplet danych

		int liczbaIteracji = DictionaryUtils.GetIterationCount(getPersonCount());
		List<Person> result = new ArrayList<Person>();

		try {
			for (int i = 1; i <= liczbaIteracji; i++) {
				GetPerson params = new GetPerson();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetPersonResponse response = getInstance().getPerson(params);

				boolean importujTylkoAktywneUmowa = importujTylkoAktywnaUmowa();
				boolean importujTylkoAktywneOsoba = importujTylkoAktywnaOsoba();

				for (Person _unit : response.get_return()) {

					boolean dodaj = true;

					if (importujTylkoAktywneUmowa) {
						if (_unit.getCzy_aktywny_umowa() != 1.0) {
							dodaj = false;
						}
					}

					if (dodaj && importujTylkoAktywneOsoba) {
						if (_unit.getCzy_aktywny_osoba() != 1.0) {
							dodaj = false;
						}
					}
					if (dodaj){
						result.add(_unit);
					}
				}
			}
			return result;
		} catch (RemoteException e) {
			log.error("", e);
		}
		return null;
	}

    private boolean importujTylkoAktywnaOsoba() {
        String value = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_ERP_PERSON_TYLKO_AKTYWNY_OSOBA");

        boolean ok = false;
        if (value!=null && value.equals("true"))
        {
            ok = true;
        }
        return ok;
    }

    private boolean importujTylkoAktywnaUmowa() {
        String value = Docusafe.getAdditionProperty("IMPORT_PRACOWNIKOW_ERP_PERSON_TYLKO_AKTYWNY_UMOWA");
        boolean ok = false;
        if (value!=null && value.equals("true"))
        {
            ok = true;
        }
        return ok;
    }


	/*----------------------------- PERSON AGREEMENT ------------------------------*/

	/**\
	 * Metoda zwraca liczb� jednostek wszystkich obiekt�w PersonAgreement  >=0 - liczba wierszy 
	 * -1 b��d 
	 * @return
	 */
	private long getPersonAgreementsCount() {
		GetPersonAgreementCount count = new GetPersonAgreementCount();

		try {
			GetPersonAgreementCountResponse result = getInstance().getPersonAgreementCount(count);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}

	/**
	 * Metoda zwraca PersonAgreement w liczbie nie wi�kszej, ni� za�o�ona jako
	 * parametr numberOfObjects
	 * 
	 * @param numberOfObjects
	 * @return List<PersonAgreement> - lista wynik�w
	 * @return null - wyt�pi� b��d podczas wczytywania
	 */
	public List<PersonAgreement> getPersonAgreements() {
		// Wywo�ywany serwis dzieli wyniki wed�ug: ilo�� na stronie, liczba
		// stron
		// Masymalny rozmiar na stronie = 1000,
		// Strony zaczynaj� si� od 1
		// I tak �eby zwr�ci� np. 14000 pozycji, nale�y pobra� strony od 1 do 14
		// z rozmiarem maksymalnym 1000 na stron�.
		// Metoda robi za u�ytkownika paginowanie i zwraca komplet danych

		int liczbaIteracji = DictionaryUtils.GetIterationCount(getPersonAgreementsCount());

		//int liczbaIteracji = 1;

		List<PersonAgreement> result = new ArrayList<PersonAgreement>();

		try {

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetPersonAgreement params = new GetPersonAgreement();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				//params.setPageSize(100);
				params.setPage(i);

				GetPersonAgreementResponse response = getInstance()
						.getPersonAgreement(params);

				for (PersonAgreement _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}
		return null;
	}


	/*------------------------------- ZAM�WIENIE DO DOSTAWCY -----------------------------*/	

	// zamowienia dostawcow
	private long getSupplierOrderHeaderCount() throws RemoteException {
		GetSupplierOrderHeaderCount gouc = new GetSupplierOrderHeaderCount();

			GetSupplierOrderHeaderCountResponse result = getInstance()
					.getSupplierOrderHeaderCount(gouc);
			return result.get_return();

	}

	public List<SupplierOrderHeader> getSupplierOrderHeader() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getSupplierOrderHeaderCount());

		List<SupplierOrderHeader> result = new ArrayList<SupplierOrderHeader>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetSupplierOrderHeader params = new GetSupplierOrderHeader();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetSupplierOrderHeaderResponse response = getInstance()
						.getSupplierOrderHeader(params);

				for (SupplierOrderHeader _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// budzety
	private long getBudgetViewCount() {
		GetBudgetViewCount gouc = new GetBudgetViewCount();

		try {
			GetBudgetViewCountResponse result = getInstance()
					.getBudgetViewCount(gouc);
			return result.get_return();
		} catch (RemoteException e) {
			log.error(e);
		}
		return -1;
	}

	public List<BudgetView> getBudgetView() {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getBudgetViewCount());

		List<BudgetView> result = new ArrayList<BudgetView>();

		try {

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetBudgetView params = new GetBudgetView();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetBudgetViewResponse response = getInstance()
						.getBudgetView(params);

				for (BudgetView _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}

		return null;
	}

	// zadania finansowe
	private long getFinancialTaskCount() throws RemoteException {
		GetFinancialTaskCount gouc = new GetFinancialTaskCount();

			GetFinancialTaskCountResponse result = getInstance()
					.getFinancialTaskCount(gouc);
			return result.get_return();
	}

	public List<FinancialTask> getFinancialTask() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getFinancialTaskCount());

		List<FinancialTask> result = new ArrayList<FinancialTask>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetFinancialTask params = new GetFinancialTask();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetFinancialTaskResponse response = getInstance()
						.getFinancialTask(params);

				for (FinancialTask _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// projekty
	private long getProjectCount() throws RemoteException {
		GetProjectCount gouc = new GetProjectCount();

			GetProjectCountResponse result = getInstance()
					.getProjectCount(gouc);
			return result.get_return();
	}

	public List<Project> getProject() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getProjectCount());

		List<Project> result = new ArrayList<Project>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetProject params = new GetProject();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProjectResponse response = getInstance()
						.getProject(params);

				for (Project _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// projekty budzetow etapy
	private long getProjectBudgetItemCount() throws RemoteException {
		GetProjectBudgetItemCount gouc = new GetProjectBudgetItemCount();

			GetProjectBudgetItemCountResponse result = getInstance()
					.getProjectBudgetItemCount(gouc);
			return result.get_return();

	}

	public List<ProjectBudgetItem> getProjectBudgetItem() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getProjectBudgetItemCount());

		List<ProjectBudgetItem> result = new ArrayList<ProjectBudgetItem>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetProjectBudgetItem params = new GetProjectBudgetItem();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProjectBudgetItemResponse response = getInstance()
						.getProjectBudgetItem(params);

				for (ProjectBudgetItem _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// projekty budzetow
	private long getProjectBugdetCount() throws RemoteException {
		GetProjectBugdetCount gouc = new GetProjectBugdetCount();

			GetProjectBugdetCountResponse result = getInstance()
					.getProjectBugdetCount(gouc);
			return result.get_return();

	}

	public List<ProjectBugdet> getProjectBugdet() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getProjectBugdetCount());

		List<ProjectBugdet> result = new ArrayList<ProjectBugdet>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetProjectBugdet params = new GetProjectBugdet();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProjectBugdetResponse response = getInstance()
						.getProjectBugdet(params);

				for (ProjectBugdet _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// projekty budzetow zasoby
	private long getProjectBudgetItemResourceCount() throws RemoteException {
		GetProjectBudgetItemResourceCount gouc = new GetProjectBudgetItemResourceCount();

			GetProjectBudgetItemResourceCountResponse result = getInstance()
					.getProjectBudgetItemResourceCount(gouc);
			return result.get_return();
	}

	public List<ProjectBudgetItemResource> getProjectBudgetItemResource() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getProjectBudgetItemResourceCount());

		List<ProjectBudgetItemResource> result = new ArrayList<ProjectBudgetItemResource>();
			for (int i = 1; i <= liczbaIteracji; i++) {
				GetProjectBudgetItemResource params = new GetProjectBudgetItemResource();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProjectBudgetItemResourceResponse response = getInstance()
						.getProjectBudgetItemResource(params);

				for (ProjectBudgetItemResource _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// projekty budzetow zasoby
	private long getSourcesOfProjectFundingCount() throws RemoteException {
		GetSourcesOfProjectFundingCount gouc = new GetSourcesOfProjectFundingCount();

			GetSourcesOfProjectFundingCountResponse result = getInstance()
					.getSourcesOfProjectFundingCount(gouc);
			return result.get_return();

	}

	public List<SourcesOfProjectFunding> getSourcesOfProjectFunding() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getSourcesOfProjectFundingCount());

		List<SourcesOfProjectFunding> result = new ArrayList<SourcesOfProjectFunding>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetSourcesOfProjectFunding params = new GetSourcesOfProjectFunding();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetSourcesOfProjectFundingResponse response = getInstance()
						.getSourcesOfProjectFunding(params);

				for (SourcesOfProjectFunding _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	// zlecenia produkcyjne
	private long getProductionOrderCount() throws RemoteException {
		GetProductionOrderCount gouc = new GetProductionOrderCount();
			GetProductionOrderCountResponse result = getInstance()
					.getProductionOrderCount(gouc);
			return result.get_return();

	}

	public List<ProductionOrder> getProductionOrder() throws RemoteException {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getProductionOrderCount());

		List<ProductionOrder> result = new ArrayList<ProductionOrder>();

			for (int i = 1; i <= liczbaIteracji; i++) {
				GetProductionOrder params = new GetProductionOrder();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProductionOrderResponse response = getInstance()
						.getProductionOrder(params);

				for (ProductionOrder _unit : response.get_return()) {
					result.add(_unit);
				}
			}
			return result;
	}

	//konta bankowe dostawcow
	private long getSupplierBankAccountCount() {
		GetSupplierBankAccountCount gsbac = new GetSupplierBankAccountCount();
		try{
			GetSupplierBankAccountCountResponse result = getInstance()
					.getSupplierBankAccountCount(gsbac);
			return result.get_return();
		}catch (RemoteException e){
			getLogger().error(e);
		}
		return -1;
	}

	public List<SupplierBankAccount> getSupplierBankAccount() {

		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getSupplierBankAccountCount());

		List<SupplierBankAccount> result = new ArrayList<SupplierBankAccount>();

		try {

			for(int i=1;i<= liczbaIteracji; i++){
				GetSupplierBankAccount params = new GetSupplierBankAccount();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetSupplierBankAccountResponse response = getInstance()
						.getSupplierBankAccount(params);
				for(SupplierBankAccount sba : response.get_return()) {
					result.add(sba);
				}
			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}
		return null;
	}

	private long getUnitBudgetKoCount() throws RemoteException {
		GetUnitBudgetKoCount gouc = new GetUnitBudgetKoCount();

			GetUnitBudgetKoCountResponse result = getInstance()
					.getUnitBudgetKoCount(gouc);
			return result.get_return();

	}

	public List<UnitBudgetKo> getUnitBudgetKo() throws RemoteException {
		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getUnitBudgetKoCount());

		List<UnitBudgetKo> result = new ArrayList<UnitBudgetKo>();

			for(int i=1;i<= liczbaIteracji; i++){
				GetUnitBudgetKo params = new GetUnitBudgetKo();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetUnitBudgetKoResponse response = getInstance()
						.getUnitBudgetKo(params);
				for(UnitBudgetKo sba : response.get_return()) {
					result.add(sba);
				}
			}
			return result;
	}
	//rodzaje planow zakupu
	public long getPurchasePlanKindCount() throws RemoteException{
		GetPurchasePlanKindCount gouc = new GetPurchasePlanKindCount();

		GetPurchasePlanKindCountResponse result = getInstance()
				.getPurchasePlanKindCount(gouc);
		return result.get_return();
	}

	public List<PurchasePlanKind> getPurchasePlanKind() throws RemoteException {
		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getPurchasePlanKindCount());

		List<PurchasePlanKind> result = new ArrayList<PurchasePlanKind>();

		for(int i=1;i<= liczbaIteracji; i++){
			GetPurchasePlanKind params = new GetPurchasePlanKind();
			params.setPageSize(DictionaryConst.dictionaryPageSize);
			params.setPage(i);

			GetPurchasePlanKindResponse response = getInstance()
					.getPurchasePlanKind(params);
			for(PurchasePlanKind sba : response.get_return()) {
				result.add(sba);
			}
		}
		return result;
	}
	//plan zakupu
	public long getPurchasePlanCount() throws RemoteException{
		GetPurchasePlanCount gouc = new GetPurchasePlanCount();
			GetPurchasePlanCountResponse result = getInstance()
					.getPurchasePlanCount(gouc);
			return result.get_return();
	}
	public List<PurchasePlan> getPurchasePlan() throws RemoteException {
		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getPurchasePlanCount());

		List<PurchasePlan> result = new ArrayList<PurchasePlan>();

			for(int i=1;i<= liczbaIteracji; i++){
				GetPurchasePlan params = new GetPurchasePlan();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetPurchasePlanResponse response = getInstance()
						.getPurchasePlan(params);
				for(PurchasePlan sba : response.get_return()) {
					result.add(sba);
				}
			}
			return result;
	}

	public long getUnitBudgetCount() throws RemoteException {
		GetUnitBudgetCount gouc = new GetUnitBudgetCount();

			GetUnitBudgetCountResponse result = getInstance()
					.getUnitBudgetCount(gouc);
			return result.get_return();

	}


	public List<UnitBudget> getUnitBudget() throws RemoteException {
		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getUnitBudgetCount());

		List<UnitBudget> result = new ArrayList<UnitBudget>();

			for(int i=1;i<= liczbaIteracji; i++){
				GetUnitBudget params = new GetUnitBudget();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetUnitBudgetResponse response = getInstance()
						.getUnitBudget(params);
				for(UnitBudget sba : response.get_return()) {
					result.add(sba);
				}
			}
			return result;
	}

	public long getApplicationForReservationTypeCount() {
		GetApplicationForReservationTypeCount gouc = new GetApplicationForReservationTypeCount();

		try {
			GetApplicationForReservationTypeCountResponse result = getInstance()
					.getApplicationForReservationTypeCount(gouc);
			return result.get_return();
		} catch (RemoteException e) {
			getLogger().error(e);
		}
		return -1;
	}

	public List<ApplicationForReservationType> getApplicationForReservationType() {
		int liczbaIteracji = DictionaryUtils
				.GetIterationCount(getApplicationForReservationTypeCount());

		List<ApplicationForReservationType> result = new ArrayList<ApplicationForReservationType>();

		try {

			for(int i=1;i<= liczbaIteracji; i++){
				GetApplicationForReservationType params = new GetApplicationForReservationType();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetApplicationForReservationTypeResponse response = getInstance()
						.getApplicationForReservationType(params);
				for(ApplicationForReservationType sba : response.get_return()) {
					result.add(sba);
				}
			}
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}		
		return null;
	}


	public PurchasePlanKind getPurchasePlanKindById(Long id){
		PurchasePlanKind result = null;
		
		GetPurchasePlanKindByID params = new GetPurchasePlanKindByID();
		params.setId(id);
		
		try {
			GetPurchasePlanKindByIDResponse response= getInstance().getPurchasePlanKindByID(params);
			result=response.get_return();
			return result;
		} catch (RemoteException e) {
			log.error(e);
		}
		return null;
	}
	
	//role w projekcie
	public long getRoleInProjectsCount() throws RemoteException{
		GetRolesInProjectsCount rol = new GetRolesInProjectsCount();
			GetRolesInProjectsCountResponse res = getInstance()
					.getRolesInProjectsCount(rol);
			return res.get_return();
	}
	
	public List<RolesInProjects> getRoleInProject() throws RemoteException {
		List<RolesInProjects> result = new ArrayList<RolesInProjects>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getRoleInProjectsCount());
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetRolesInProjects param = new GetRolesInProjects();
				param.setPageSize(DictionaryConst.dictionaryPageSize);
				param.setPage(i);

				GetRolesInProjectsResponse res = getInstance()
						.getRolesInProjects(param);
				for(RolesInProjects r : res.get_return())
					result.add(r);
			}
			return result;
	}
	
	//role w budzecie
	public long getRolesInUnitBudgetKoCount() throws RemoteException {
		GetRolesInUnitBudgetKoCount rol = new GetRolesInUnitBudgetKoCount();
			GetRolesInUnitBudgetKoCountResponse res = getInstance()
					.getRolesInUnitBudgetKoCount(rol);
			return res.get_return();
	}

	public List<RolesInUnitBudgetKo> getRolesInUnitBudgetKo() throws RemoteException {
		List<RolesInUnitBudgetKo> result = new ArrayList<RolesInUnitBudgetKo>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getRolesInUnitBudgetKoCount());
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetRolesInUnitBudgetKo param = new GetRolesInUnitBudgetKo();
				param.setPageSize(DictionaryConst.dictionaryPageSize);
				param.setPage(i);

				GetRolesInUnitBudgetKoResponse res = getInstance()
						.getRolesInUnitBudgetKo(param);
				for(RolesInUnitBudgetKo r : res.get_return())
					result.add(r);
			}
			return result;
	}
	//budzet pozycje 
	public long getUnitBudgetKoKindCount() throws RemoteException {
		GetUnitBudgetKoKindCount rol = new GetUnitBudgetKoKindCount();
			GetUnitBudgetKoKindCountResponse res = getInstance()
					.getUnitBudgetKoKindCount(rol);
			return res.get_return();

	}
	
	public List<UnitBudgetKoKind> getUnitBudgetKoKind() throws RemoteException{
		List<UnitBudgetKoKind> result = new ArrayList<UnitBudgetKoKind>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getUnitBudgetKoKindCount());
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetUnitBudgetKoKind params = new GetUnitBudgetKoKind();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetUnitBudgetKoKindResponse res = getInstance()
						.getUnitBudgetKoKind(params);
				for(UnitBudgetKoKind r : res.get_return())
					result.add(r);
			}
			return result;

	}
	//budzety do komorek
	public long getBudgetUnitOrganizationUnitCount() {
		GetBudgetUnitOrganizationUnitCount rol = new GetBudgetUnitOrganizationUnitCount();
		try{
			GetBudgetUnitOrganizationUnitCountResponse res = getInstance()
					.getBudgetUnitOrganizationUnitCount(rol);
			return res.get_return();
		} catch(Exception e) {
			getLogger().error(e);
		}
		return -1;
	}
	
	
	public List<BudgetUnitOrganizationUnit> getBudgetUnitOrganizationUnit() {
		List<BudgetUnitOrganizationUnit> result = new ArrayList<BudgetUnitOrganizationUnit>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getBudgetUnitOrganizationUnitCount());
		try{
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetBudgetUnitOrganizationUnit params = new GetBudgetUnitOrganizationUnit();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetBudgetUnitOrganizationUnitResponse res = getInstance()
						.getBudgetUnitOrganizationUnit(params);
				for(BudgetUnitOrganizationUnit r : res.get_return())
					result.add(r);
			}
			return result;
		}catch(Exception e){
			log.error(e);
		}
		return null;
	}

	
	//szablony faktur
	public long getPatternsOfCostSharingCount() {
		GetPatternsOfCostSharingCount rol = new GetPatternsOfCostSharingCount();
		try{
			GetPatternsOfCostSharingCountResponse res = getInstance()
					.getPatternsOfCostSharingCount(rol);
			return res.get_return();
		} catch(Exception e) {
			getLogger().error(e);
		}
		return -1;
	}		
	
	public List<PatternsOfCostSharing> getPatternsOfCostSharing() {
		List<PatternsOfCostSharing> result = new ArrayList<PatternsOfCostSharing>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getPatternsOfCostSharingCount());
		try{
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetPatternsOfCostSharing params = new GetPatternsOfCostSharing();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetPatternsOfCostSharingResponse res = getInstance()
						.getPatternsOfCostSharing(params);
				for(PatternsOfCostSharing r : res.get_return())
					result.add(r);
			}
			return result;
		}catch(Exception e){
			log.error(e);
		}
		return null;
	}
	//piatki
	public long getProjectAccountCount() throws RemoteException {
		GetProjectAccountCount rol = new GetProjectAccountCount();
			GetProjectAccountCountResponse res = getInstance()
					.getProjectAccountCount(rol);
			return res.get_return();
	}		
	
	public List<ProjectAccount> getProjectAccount() throws RemoteException{
		List<ProjectAccount> result = new ArrayList<ProjectAccount>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getProjectAccountCount());
			for(int i=1;i<=liczbaIteracji;i++)
			{
				GetProjectAccount params = new GetProjectAccount();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetProjectAccountResponse res = getInstance()
						.getProjectAccount(params);
				for(ProjectAccount r : res.get_return())
					result.add(r);
			}
			return result;
	}
	
	//typy urlopow
	public long getAbsenceTypeCount() {
		GetAbsenceTypeCount absence = new GetAbsenceTypeCount();
		try{
			GetAbsenceTypeCountResponse res = getInstance()
					.getAbsenceTypeCount(absence);
			return res.get_return();
		}catch(Exception e){
			log.error(e);
		}
		return -1;
	}

	public List<AbsenceType> getAbsenceType(){
		List<AbsenceType> result = new ArrayList<AbsenceType>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getAbsenceTypeCount());
		try{
			for(int i=1;i<=liczbaIteracji;i++){
				GetAbsenceType params = new GetAbsenceType();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetAbsenceTypeResponse res = getInstance()
						.getAbsenceType(params);
				for(AbsenceType at: res.get_return())
					result.add(at);
			}
			return result;
		} catch(Exception e){
			log.error(e);
		}
		return null;
	}

	
	public long getRequestForReservationCount() throws RemoteException {
		GetRequestForReservationCount reservation = new GetRequestForReservationCount();
			GetRequestForReservationCountResponse res = getInstance()
					.getRequestForReservationCount(reservation);
			return res.get_return();
	}
	
	public List<RequestForReservation> getRequestForReservation() throws RemoteException {
		List<RequestForReservation> result = new ArrayList<RequestForReservation>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getRequestForReservationCount());
			for(int i=1;i<=liczbaIteracji;i++){
				GetRequestForReservation params = new GetRequestForReservation();
				params.setPageSize(DictionaryConst.dictionaryPageSize);
				params.setPage(i);

				GetRequestForReservationResponse res = getInstance()
						.getRequestForReservation(params);
				for(RequestForReservation at: res.get_return())
					result.add(at);
			}
			return result;
	}
	//zrodla 
	public long getFundingSourceCount() throws RemoteException {
		GetFundingSourceCount fundingSource = new GetFundingSourceCount();
		GetFundingSourceCountResponse res = getInstance()
				.getFundingSourceCount(fundingSource);
		return res.get_return();
	}

	public List<FundingSource> getFundingSource() throws RemoteException {
		List<FundingSource> result = new ArrayList<FundingSource>();
		int liczbaIteracji = DictionaryUtils.GetIterationCount(getFundingSourceCount());
		for(int i=1;i<=liczbaIteracji;i++){
			GetFundingSource params = new GetFundingSource();
			params.setPageSize(DictionaryConst.dictionaryPageSize);
			params.setPage(i);

			GetFundingSourceResponse res = getInstance()
					.getFundingSource(params);
			for(FundingSource at: res.get_return())
				result.add(at);
		}
		return result;

	}	
}
