package pl.compan.docusafe.ws.client.simple.general;


public class DictionaryUtils {

	/**
	 * Metoda pobiera liczb� wierszy i ustala ilo�� niezb�dnych iteracji przy
	 * za�o�eniu, �e rozmiarem strony jest pageSize;
	 * 
	 * @param rowCounts
	 * @return
	 */
	public static int GetIterationCount(long rowCounts) {
		int real = (int) (rowCounts / DictionaryConst.dictionaryPageSize);
		float imag = rowCounts % DictionaryConst.dictionaryPageSize;

		int iterationNumber = real;
		if (imag > 0) {
			iterationNumber++;
		}
		return iterationNumber;
	}
}
