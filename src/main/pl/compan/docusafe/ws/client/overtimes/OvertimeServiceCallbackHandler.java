
/**
 * OvertimeServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package pl.compan.docusafe.ws.client.overtimes;

    /**
     *  OvertimeServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class OvertimeServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public OvertimeServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public OvertimeServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getReport method
            * override this method for handling normal response from getReport operation
            */
           public void receiveResultgetReport(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getReport operation
           */
            public void receiveErrorgetReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeOvertimes method
            * override this method for handling normal response from getEmployeeOvertimes operation
            */
           public void receiveResultgetEmployeeOvertimes(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetEmployeeOvertimesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeOvertimes operation
           */
            public void receiveErrorgetEmployeeOvertimes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMonthEmployeeOvertimes method
            * override this method for handling normal response from getMonthEmployeeOvertimes operation
            */
           public void receiveResultgetMonthEmployeeOvertimes(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetMonthEmployeeOvertimesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMonthEmployeeOvertimes operation
           */
            public void receiveErrorgetMonthEmployeeOvertimes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for chargeOvertimes method
            * override this method for handling normal response from chargeOvertimes operation
            */
           public void receiveResultchargeOvertimes(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.ChargeOvertimesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from chargeOvertimes operation
           */
            public void receiveErrorchargeOvertimes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getIsFreeDay method
            * override this method for handling normal response from getIsFreeDay operation
            */
           public void receiveResultgetIsFreeDay(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetIsFreeDayResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getIsFreeDay operation
           */
            public void receiveErrorgetIsFreeDay(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUserInfo method
            * override this method for handling normal response from getUserInfo operation
            */
           public void receiveResultgetUserInfo(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.GetUserInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUserInfo operation
           */
            public void receiveErrorgetUserInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for unchargeOvertimes method
            * override this method for handling normal response from unchargeOvertimes operation
            */
           public void receiveResultunchargeOvertimes(
                    pl.compan.docusafe.ws.client.overtimes.OvertimeServiceStub.UnchargeOvertimesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from unchargeOvertimes operation
           */
            public void receiveErrorunchargeOvertimes(java.lang.Exception e) {
            }
                


    }
    