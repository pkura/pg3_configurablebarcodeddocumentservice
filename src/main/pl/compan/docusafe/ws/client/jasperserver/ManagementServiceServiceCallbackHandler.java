
/**
 * ManagementServiceServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.client.jasperserver;

    /**
     *  ManagementServiceServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ManagementServiceServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ManagementServiceServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ManagementServiceServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for move method
            * override this method for handling normal response from move operation
            */
           public void receiveResultmove(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.MoveResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from move operation
           */
            public void receiveErrormove(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for runReport method
            * override this method for handling normal response from runReport operation
            */
           public void receiveResultrunReport(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.RunReportResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from runReport operation
           */
            public void receiveErrorrunReport(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for get method
            * override this method for handling normal response from get operation
            */
           public void receiveResultget(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.GetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from get operation
           */
            public void receiveErrorget(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for delete method
            * override this method for handling normal response from delete operation
            */
           public void receiveResultdelete(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.DeleteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from delete operation
           */
            public void receiveErrordelete(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for put method
            * override this method for handling normal response from put operation
            */
           public void receiveResultput(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.PutResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from put operation
           */
            public void receiveErrorput(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for list method
            * override this method for handling normal response from list operation
            */
           public void receiveResultlist(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.ListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from list operation
           */
            public void receiveErrorlist(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for copy method
            * override this method for handling normal response from copy operation
            */
           public void receiveResultcopy(
                    pl.compan.docusafe.ws.client.jasperserver.ManagementServiceServiceStub.CopyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from copy operation
           */
            public void receiveErrorcopy(java.lang.Exception e) {
            }
                


    }
    