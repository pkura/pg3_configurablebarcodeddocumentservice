
/**
 * PullFaultMsg.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

package pl.compan.docusafe.ws.client.ePUAP;

public class PullFaultMsg extends java.lang.Exception{
    
    private pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.Wyjatek faultMessage;
    
    public PullFaultMsg() {
        super("PullFaultMsg");
    }
           
    public PullFaultMsg(java.lang.String s) {
       super(s);
    }
    
    public PullFaultMsg(java.lang.String s, java.lang.Throwable ex) {
      super(s, ex);
    }
    
    public void setFaultMessage(pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.Wyjatek msg){
       faultMessage = msg;
    }
    
    public pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.Wyjatek getFaultMessage(){
       return faultMessage;
    }
}
    