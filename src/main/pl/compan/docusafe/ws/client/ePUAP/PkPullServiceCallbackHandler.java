
/**
 * PkPullServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */

    package pl.compan.docusafe.ws.client.ePUAP;

    /**
     *  PkPullServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class PkPullServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public PkPullServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public PkPullServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for pobierzNastepny method
            * override this method for handling normal response from pobierzNastepny operation
            */
           public void receiveResultpobierzNastepny(
                    pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.OdpowiedzPullPobierz result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pobierzNastepny operation
           */
            public void receiveErrorpobierzNastepny(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for oczekujaceDokumenty method
            * override this method for handling normal response from oczekujaceDokumenty operation
            */
           public void receiveResultoczekujaceDokumenty(
                    pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.OdpowiedzPullOczekujace result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from oczekujaceDokumenty operation
           */
            public void receiveErroroczekujaceDokumenty(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for potwierdzOdebranie method
            * override this method for handling normal response from potwierdzOdebranie operation
            */
           public void receiveResultpotwierdzOdebranie(
                    pl.compan.docusafe.ws.client.ePUAP.PkPullServiceStub.OdpowiedzPullPotwierdz result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from potwierdzOdebranie operation
           */
            public void receiveErrorpotwierdzOdebranie(java.lang.Exception e) {
            }
                


    }
    