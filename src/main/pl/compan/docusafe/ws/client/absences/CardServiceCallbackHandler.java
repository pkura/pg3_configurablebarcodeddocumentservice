
/**
 * CardServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package pl.compan.docusafe.ws.client.absences;

    /**
     *  CardServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class CardServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public CardServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public CardServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for chargeAbsence method
            * override this method for handling normal response from chargeAbsence operation
            */
           public void receiveResultchargeAbsence(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.ChargeAbsenceResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from chargeAbsence operation
           */
            public void receiveErrorchargeAbsence(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPdfCard method
            * override this method for handling normal response from getPdfCard operation
            */
           public void receiveResultgetPdfCard(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.GetPdfCardResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPdfCard operation
           */
            public void receiveErrorgetPdfCard(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCard method
            * override this method for handling normal response from getCard operation
            */
           public void receiveResultgetCard(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.GetCardResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCard operation
           */
            public void receiveErrorgetCard(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFreeDays method
            * override this method for handling normal response from getFreeDays operation
            */
           public void receiveResultgetFreeDays(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.GetFreeDaysResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFreeDays operation
           */
            public void receiveErrorgetFreeDays(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for unchargeLastAbsence method
            * override this method for handling normal response from unchargeLastAbsence operation
            */
           public void receiveResultunchargeLastAbsence(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.UnchargeLastAbsenceResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from unchargeLastAbsence operation
           */
            public void receiveErrorunchargeLastAbsence(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUserInfo method
            * override this method for handling normal response from getUserInfo operation
            */
           public void receiveResultgetUserInfo(
                    pl.compan.docusafe.ws.client.absences.CardServiceStub.GetUserInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUserInfo operation
           */
            public void receiveErrorgetUserInfo(java.lang.Exception e) {
            }
                


    }
    