package pl.compan.docusafe.ws.tickets;

public class AnonymousTicketRequest 
{
	/**
	 * znacznik czasowy. Zawiera czas uzyty w wyliczaniu verifyCode. Ma posatc yyyyMMddHHmm
	 */
	private String timestamp;
	
	/**
	 * identyfikator bazodanowy dokumentu do ktorego zadany jest dostep
	 */
	private Long documentId;
	
	/** 
	 * kod weryfikujacy wyliczany poprzez SHA2(iddokumentu+znacznikczasowy+SALT)
	 */
	private String verifyCode;
	
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getVerifyCode() {
		return verifyCode;
	}
	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}
	
	
}
