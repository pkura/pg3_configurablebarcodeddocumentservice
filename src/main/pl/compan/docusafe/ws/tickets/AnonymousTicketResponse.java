package pl.compan.docusafe.ws.tickets;

import java.util.Calendar;
import java.util.Date;

public class AnonymousTicketResponse 
{
	/**
	 * bilet ktory musi byc przekazany do akcji zeby wyswietlila dokument
	 */
	private String ticket;
	
	/**
	 * okres waznosci biletu
	 */
	private Calendar validTill;
	
	/**
	 * maksymalna liczba dostepow z uzyciem biletu
	 */
	private Integer retriesCount;
	
	/** 
	 * status wbesrevicu
	 */
	private String status;
	
	/**
	 * szczegoly statusu
	 */
	private String statusDescription;
	
	
	public String getTicket() {
		return ticket;
	}
	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	public Calendar getValidTill() {
		return validTill;
	}
	public void setValidTill(Calendar validTill) {
		this.validTill = validTill;
	}
	public Integer getRetriesCount() {
		return retriesCount;
	}
	public void setRetriesCount(Integer retriesCount) {
		this.retriesCount = retriesCount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	
	
}
