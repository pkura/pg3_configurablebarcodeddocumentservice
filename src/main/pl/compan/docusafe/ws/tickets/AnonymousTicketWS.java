package pl.compan.docusafe.ws.tickets;

import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.AvailabilityManager;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.SearchResults;
import pl.compan.docusafe.core.archive.AnonymousAccessTicket;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DockindQuery;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.DocumentKindsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.im.DocuSafeUploadServiceUtils;

public class AnonymousTicketWS 
{

	private static final Logger log = LoggerFactory.getLogger(AnonymousTicketWS.class);
	
	private static Integer MAX_ACCESS = 3;
	private static Long TIMESTAMP_TOLERANCE_MILISECONDS = 120*DateUtils.MINUTE;
	private static Long ACCESS_VALID_PERIOD = 1*DateUtils.HOUR;
	
	private static final String OK = "SUCCESS";
	private static final String ERROR = "ERROR";
	
	//private static String SALT = "a co Ty zrobiles dla kraju!?";
	
	
	public AnonymousTicketResponse AcquireTicket(AnonymousTicketRequest request)
	{
		AnonymousTicketResponse atr = new AnonymousTicketResponse();
		Integer maxAccess;
		Long tolerance;
		Long validPeriod;
		String SALT;
		try
		{
			if(Docusafe.getAdditionProperty("ticketWS.SALT")==null)
			{
				throw new EdmException("Nie znaleziono parametru SALT");
			}
			
			SALT = Docusafe.getAdditionProperty("ticketWS.SALT");
			
			try
			{
				maxAccess = Integer.parseInt(Docusafe.getAdditionProperty("ticketWS.MAX_ACCESS"));
			}
			catch (Exception e) 
			{
				maxAccess = MAX_ACCESS;
			}
			
			try
			{
				tolerance = Long.parseLong(Docusafe.getAdditionProperty("ticketWS.TIMESTAMP_TOLERANCE_MINUTES"))*DateUtils.MINUTE;
			}
			catch (Exception e) 
			{
				tolerance = TIMESTAMP_TOLERANCE_MILISECONDS;
			}
			
			try
			{
				validPeriod = Long.parseLong(Docusafe.getAdditionProperty("ticketWS.ACCESS_VALID_PERIOD_MINUTES"))*DateUtils.MINUTE;
			}
			catch (Exception e) 
			{
				validPeriod = ACCESS_VALID_PERIOD;
			}
			
			
			DSApi.openAdmin();
			if(request.getDocumentId()==null)
			{
				throw new EdmException("Brak id dokumentu");
			}
			if(request.getTimestamp()==null)
			{
				throw new EdmException("Brak znacznika czasu");
			}
			if(request.getVerifyCode()==null)
			{
				throw new EdmException("Brak kodu weryfikujacego");
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
			Date receivedTime = sdf.parse(request.getTimestamp());
			
			if(Math.abs(receivedTime.getTime() - (new Date().getTime())) > tolerance)
			{
				throw new EdmException("Uplynal limit czasu waznosci zadania");
			}

			String base = request.getDocumentId() + request.getTimestamp() + SALT;
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(base.getBytes());
			String code = DocuSafeUploadServiceUtils.toHexString(md.digest());
			
			if(!code.equals(request.getVerifyCode()))
			{
				//System.out.println(code);
				//System.out.println(request.getVerifyCode());
				throw new EdmException("Nieporawny kod weryfikujacy");
			}
			
			
			//Sprawdzam czy dokument istnieje
			DocumentKind kind = DocumentKind.findByCn(DocumentLogicLoader.PROSIKA);
			DockindQuery query = new DockindQuery(0, 1000);
			query.setDocumentKind(kind);
			query.field(kind.getFieldByCn("ID_DOK"), request.getDocumentId().toString());
			SearchResults<Document> results = DocumentKindsManager.search(query);
			if(results.totalCount()<1)
				throw new EdmException("Brak dokumentów o podanym ID");
			
			Document doc = results.next();
			
			String accessCode;
			md.update(new Date().toString().getBytes());
			accessCode = DocuSafeUploadServiceUtils.toHexString(md.digest());
			
			DSApi.context().begin();
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			
			AnonymousAccessTicket aat = new AnonymousAccessTicket();
			aat.setAccessCount(0);
			aat.setCreator("webservice");
			aat.setCtime(new Date());
			aat.setDocumentId(doc.getId());
			aat.setMaxAccess(maxAccess);
			aat.setReturnMethod("inline");
			cal.add(Calendar.MILLISECOND, validPeriod.intValue());
			aat.setValidTill(cal.getTime());
			aat.setCode(accessCode);
			DSApi.context().session().save(aat);
			DSApi.context().commit();
			
			
			atr.setStatus(OK);
			atr.setValidTill(cal);
			atr.setRetriesCount(maxAccess);
			atr.setTicket(accessCode);
			
		}
		catch(Exception e)
		{
			DSApi.context().setRollbackOnly();
			atr.setStatus(ERROR);
			atr.setStatusDescription(e.getMessage());
			log.error(e.getMessage(),e);
		}
		finally
		{
			DSApi._close();
		}
		return atr;
	}
}
