package pl.compan.docusafe.ws.wssk;

import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_DIVISION_GUID_PARAM;
import static pl.compan.docusafe.core.jbpm4.ProcessParamsAssignmentHandler.ASSIGN_USER_PARAM;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.impl.llom.OMElementImpl;

import com.google.common.collect.Maps;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.base.Document;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.core.office.InOfficeDocument;
import pl.compan.docusafe.core.office.OfficeDocument;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.TaskSnapshot;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserFactory;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.sql.DivisionImpl;
import pl.compan.docusafe.parametrization.wssk.OfficeUtils;
import pl.compan.docusafe.parametrization.wssk.WSSK_CN;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class WsskService {

	private final static Logger log = LoggerFactory
			.getLogger(WsskService.class);

	/*
	 * public CreateDocumentResponse CreateDocument(CreateDocumentRequest
	 * request) {
	 * 
	 * CreateDocumentResponse response = new CreateDocumentResponse();
	 * 
	 * // DSApi.openAdmin();
	 * 
	 * // TODO Utworzenie dokumentu na podstawie parametr�w
	 * 
	 * return response; }
	 */

	public DyspozycjaWystawieniaFaktyrResponse NowaDyspozycjaWystawieniaFaktury(
			DyspozycjaWystawieniaFakturyRequest request) {

		DyspozycjaWystawieniaFaktyrResponse response = new DyspozycjaWystawieniaFaktyrResponse();
		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces wystawienia faktury dla nieubezpieczonego pacjenta.");
		response.setIsError(new Boolean(false));

		// Pacjent nazwa
		if (!IsValidStringField(request.getPacjentNazwa())) {
			response.setErrorMessage("Pole Nazwa Pacjenta jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pacjent Pesel
		if (!IsValidStringField(request.getPacjentPesel())) {
			response.setErrorMessage("Pesel jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pacjent Ulica
		if (!IsValidStringField(request.getPacjentUlica())) {
			response.setErrorMessage("Ulica jest wymagana");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pacjent numer domu
		if (!IsValidStringField(request.getPacjentNrDomu())) {
			response.setErrorMessage("Numer domu jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pacjent numer mieszkania
		if (!IsValidStringField(request.getPacjentNrMieszkania())) {
			response.setErrorMessage("Numer mieszkania jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Kod pocztowy
		if (!IsValidStringField(request.getPacjentKodPocztowy())) {
			response.setErrorMessage("Kod pocztowy jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pacjent miasto
		if (!IsValidStringField(request.getPacjentMiasto())) {
			response.setErrorMessage("Miejscowo�� jest wymagana");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Data pobytu od
		if (request.getDataPobytuOd() == null) {
			response.setErrorMessage("Data pobytu od jest wymagana");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Data pobytu do
		if (request.getDataPobytuDo() == null) {
			response.setErrorMessage("Data pobytu do jest wymagana");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d po��czenia bazy danych systemu");
			response.setIsError(new Boolean(true));
			log.debug("B��d pod��czenia do bazy danych. " + exc.getMessage());
			return response;
		}

		try {
			// Utworzenie dokumentu - Zapoznanie si� z dokumentami
			String summary = "Dokument wygenerowany automatycznie - Dyspozycja wystawienia faktury";

			String uzytkownikLogin = "";

			if (Docusafe
					.getAdditionProperty("WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_UZYTKOWNIK_LOGIN") == null) {
				response.setErrorMessage("Brak ustawionej w�a�ciwo�ci WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_UZYTKOWNIK_LOGIN");
				response.setIsError(new Boolean(true));
				return response;
			}

			uzytkownikLogin = Docusafe
					.getAdditionProperty("WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_UZYTKOWNIK_LOGIN");

			String organizacjaGuid = "";

			if (Docusafe
					.getAdditionProperty("WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_ORGANIZACJA_GUID") == null) {
				response.setErrorMessage("Brak ustawionej w�a�ciwo�ci WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_ORGANIZACJA_GUID");
				response.setIsError(new Boolean(true));
				return response;
			}

			organizacjaGuid = Docusafe
					.getAdditionProperty("WSSK_DYSPOZYCJA_WYSTAWIENIA_FAKTURY_ORGANIZACJA_GUID");

			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					organizacjaGuid, uzytkownikLogin, organizacjaGuid,
					uzytkownikLogin, null, summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_PACJENT_NIEUBEZP); // Dyspozycja
																			// wystawienia
																			// faktury
																			// dla
																			// pacjenta
																			// nieubezpieczonego
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());
			dockindKeys.put("odbiorca_nazwa", request.getPacjentNazwa());
			dockindKeys.put("odbiorca_pesel", request.getPacjentPesel());
			dockindKeys.put("odbiorca_ulica", request.getPacjentUlica());
			dockindKeys.put("odbiorca_nrdomu", request.getPacjentNrDomu());
			dockindKeys.put("odbiorca_nrmieszk",
					request.getPacjentNrMieszkania());
			dockindKeys.put("odbiorca_kodpoczt",
					request.getPacjentKodPocztowy());
			dockindKeys.put("odbiorca_miasto", request.getPacjentMiasto());
			dockindKeys.put("Data_od", request.getDataPobytuOd());
			dockindKeys.put("Data_do", request.getDataPobytuDo());

			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(uzytkownikLogin);

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, uzytkownikLogin);
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			/*
			 * JBPMSnapshotProvider jbpm = new JBPMSnapshotProvider();
			 * jbpm.updateTaskList(new Session(), doc.getId(), doc.getType(),
			 * doc.getType(), request.getUsername());
			 */

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();
		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Utworzenie Dyspozycji do Wystawienia Faktury");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}

		return response;

	}

	/**
	 * Us�uga Webservice. Pozwala na dodanie nowego u�ytkownika do systemu.
	 * Wymagane parametry: firstName lastName email organizationGuid
	 * supervisorUsername username
	 * 
	 * @param request
	 * @return
	 */
	public ZatrudnienieNowegoPracownikaResponse ZatrudnienieNowegoPracownika(
			ZatrudnienieNowegoPracownikaRequest request) {
		ZatrudnienieNowegoPracownikaResponse response = new ZatrudnienieNowegoPracownikaResponse();

		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces zatrudniania nowego pracownika");
		response.setIsError(new Boolean(false));

		// Sprawdzenie, czy wszystkie wymagane parametry zosta�y przekazane

		// Login u�ytkownika
		if (!IsValidStringField(request.getLogin())) {
			response.setErrorMessage("Login u�ytkownika jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Imi�
		if (!IsValidStringField(request.getImie())) {
			response.setErrorMessage("Pole imi� jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Nazwisko
		if (!IsValidStringField(request.getNazwisko())) {
			response.setErrorMessage("Pole Nazwisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Email
		if (!IsValidStringField(request.getAdresEmail())) {
			response.setErrorMessage("Pole Adres Email jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// GUID jednostki organizacyjnej
		if (!IsValidStringField(request.getJednostkaOrganizacyjnaGuid())) {
			response.setErrorMessage("Pole GUID jednostki organizacyjnej jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Has�o tymczasowe
		if (!IsValidStringField(request.getHasloTymczasowe())) {
			response.setErrorMessage("Pole Has�o Tymczasowe jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Nazwa jednostki organizacyjnej
		if (!IsValidStringField(request.getJednostkaOrganizacyjnaNazwa())) {
			response.setErrorMessage("Pole Nazwa Jednostki Organizacyjnej jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Login prze�o�onego
		if (!IsValidStringField(request.getLoginPrzelozonego())) {
			response.setErrorMessage("Pole Login U�ytkownika jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Stanowisko
		if (!IsValidStringField(request.getStanowisko())) {
			response.setErrorMessage("Pole Stanowisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Miejsce pracy
		if (!IsValidStringField(request.getMiejscePracy())) {
			response.setErrorMessage("Pole Miejsce Pracy jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Pesel
		if (!IsValidStringField(request.getPesel())) {
			response.setErrorMessage("Pole Pesel jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Tytu� naukowy
		if (!IsValidStringField(request.getTytulNaukowy())) {
			response.setErrorMessage("Pole Tytu� Naukowy jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d po��czenia bazy danych systemu");
			response.setIsError(new Boolean(true));
			log.debug("B��d pod��czenia do bazy danych. " + exc.getMessage());
			return response;
		}

		DSUser pracownikSzukany = null;
		try {
			// Sprawdzenie, czy u�ytkownik ju� istnieje
			pracownikSzukany = UserFactory.getInstance().findByUsername(
					request.getLogin());
		} catch (UserNotFoundException unfe) {
			pracownikSzukany = null;
		} catch (EdmException exc) {
			response.setErrorMessage("B�ad podczas dodawania u�ytkownika");
			response.setIsError(new Boolean(true));
			log.debug("B��d podczas wyszukiwania u�ytkownika"
					+ exc.getMessage());
			return response;
		}
		if (pracownikSzukany != null) {
			response.setErrorMessage("Pracownik o podanym identyfikatorze ju� istnieje w systemie");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			// Utworzenie nowego pracownika w systemie
			DSUser nowyPracownik = UserFactory.getInstance().createUser(
					request.getLogin(), request.getImie(),
					request.getNazwisko());
			nowyPracownik.setEmail(request.getAdresEmail());
			nowyPracownik.setCtime(new Timestamp(new Date().getTime()));
			// Has�em domy�lnym jest adres e-mail
			nowyPracownik.setPassword(request.getAdresEmail(), true);

			// Dodanie prze�o�onego
			DSUser przelozony = null;
			try {
				// Sprawdzenie, czy taki przelozony istnieje
				przelozony = UserFactory.getInstance().findByUsername(
						request.getLoginPrzelozonego());
			} catch (UserNotFoundException unfe) {
				response.setErrorMessage("Nie znaleziono podanego prze�o�onego");
				response.setIsError(new Boolean(true));
				log.debug("Nie znaleziono podanego przelozonego"
						+ unfe.getMessage());
				return response;
			} catch (EdmException exc) {
				response.setErrorMessage("B��d podczas wyszukiwania prze�o�onego");
				response.setIsError(new Boolean(true));
				log.debug("Blad podczas wyszukiwania przelozonego"
						+ exc.getMessage());
				return response;
			}
			nowyPracownik.setSupervisor(przelozony);

			// Pobranie grupy
			DSDivision group = DSDivision.find(request
					.getJednostkaOrganizacyjnaGuid());

			if (group == null) {
				response.setErrorMessage("Grupa o podanym GUID nie istnieje");
				response.setIsError(new Boolean(true));
				log.debug("Grupa o podanym GUID nie istnieje");
				return response;
			}

			group.addUser(nowyPracownik);

			DSApi.context().commit();

			// Przypisze u�ytkownika do struktury organizacyjnej,
			// Utworzy nowy dokument
			// "Zapoznanie Nowego Pracownika z Dokumentami" (w kodzie jest to
			// "Nowy Pracownik"),
			// Prze�le dokument na list� Nowego Pracownika

			// Utworzenie dokumentu - Zapoznanie si� z dokumentami
			String summary = "Dokument wygenerowany automatycznie do zapoznania si� dla Nowego Pracownika";
			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					request.getJednostkaOrganizacyjnaGuid(),
					request.getLogin(),
					request.getJednostkaOrganizacyjnaGuid(),
					request.getLogin(), null, summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_NOWY_PRACOWNIK);
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());

			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(request.getLogin());

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, request.getLogin());
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
			map.put(WSSK_CN.NEWEE_PVAR_employee, request.getLogin());
			map.put(WSSK_CN.NEWEE_PVAR_nrUprZaw,
					request.getNumerUprawnienWykonywaniaZawodu());
			map.put(WSSK_CN.NEWEE_PVAR_pesel, request.getPesel());
			map.put(WSSK_CN.NEWEE_PVAR_tytul, request.getTytulNaukowy());
			map.put(WSSK_CN.NEWEE_PVAR_stanowisko, request.getStanowisko());
			map.put(WSSK_CN.NEWEE_PVAR_miejscePracy, request.getMiejscePracy());

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			/*
			 * JBPMSnapshotProvider jbpm = new JBPMSnapshotProvider();
			 * jbpm.updateTaskList(new Session(), doc.getId(), doc.getType(),
			 * doc.getType(), request.getUsername());
			 */

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();

			// doc.getDocumentKind().getDockindInfo().getProcessesDeclarations().getProcess("manual").getLogic().startProcess(doc,
			// Collections.<String,
			// Object>singletonMap(Jbpm4Constants.READ_ONLY_ASSIGNEE_PROPERTY,doc.getAuthor()))

		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Nowy Pracownik");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}
		return response;
	}

	/**
	 * Us�uga Webservice. Tworzy w systemie nowy dokument
	 * "Zwolnienie pracownika"
	 * 
	 * @param request
	 * @return
	 */
	public ZwolnieniePracownikaResponse ZwolnieniePracownika(
			ZwolnieniePracownikaRequest request) {

		ZwolnieniePracownikaResponse response = new ZwolnieniePracownikaResponse();

		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces zwalniania pracownika");
		response.setIsError(new Boolean(false));

		// Sprawdzenie, czy wszystkie wymagane parametry zosta�y przekazane

		// Login u�ytkownika
		if (!IsValidStringField(request.getLogin())) {
			response.setErrorMessage("Login u�ytkownika jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Imi�
		if (!IsValidStringField(request.getImie())) {
			response.setErrorMessage("Pole imi� jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Nazwisko
		if (!IsValidStringField(request.getNazwisko())) {
			response.setErrorMessage("Pole Nazwisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Email
		if (!IsValidStringField(request.getAdresEmail())) {
			response.setErrorMessage("Pole Adres Email jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Stanowisko
		if (!IsValidStringField(request.getStanowisko())) {
			response.setErrorMessage("Pole Stanowisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Miejsce pracy
		if (!IsValidStringField(request.getMiejscePracy())) {
			response.setErrorMessage("Pole Miejsce Pracy jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Data zwolnienia
		if (!IsValidDateField(request.getDataZwolnienia())) {
			response.setErrorMessage("Pole Data Zwolnienia jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d po��czenia bazy danych systemu");
			response.setIsError(new Boolean(true));
			log.debug("B��d pod��czenia do bazy danych. " + exc.getMessage());
			return response;
		}

		try {
			// Utworzenie dokumentu - Zapoznanie si� z dokumentami
			String summary = "Dokument wygenerowany automatycznie - Zwolnienie Pracownika";

			String loginyUzytkownikow = "";

			if (Docusafe
					.getAdditionProperty("WSSK_ZWOLNIENIE_PRACOWNIKA_UZYTKOWNIK_LOGINY") == null) {
				response.setErrorMessage("Brak ustawionej w�a�ciwo�ci WSSK_ZWOLNIENIE_PRACOWNIKA_UZYTKOWNIK_LOGINY");
				response.setIsError(new Boolean(true));
				return response;
			}

			loginyUzytkownikow = Docusafe
					.getAdditionProperty("WSSK_ZWOLNIENIE_PRACOWNIKA_UZYTKOWNIK_LOGINY");

			// Loginy uzytkownik�w s� podawana po przecinku np.:
			// jlaszcz,lwozniak,rodoj
			String[] loginy = loginyUzytkownikow.split(",");

			DSUser pracownikSzukany = null;
			try {
				// Sprawdzenie, czy u�ytkownik ju� istnieje
				pracownikSzukany = UserFactory.getInstance().findByUsername(
						"admin");
			} catch (UserNotFoundException unfe) {
				pracownikSzukany = null;
			} catch (EdmException exc) {
				response.setErrorMessage("B�ad podczas wyszukiwania u�ytkownika: "
						+ "admin");
				response.setIsError(new Boolean(true));
				log.debug("B��d podczas wyszukiwania u�ytkownika: " + "admin"
						+ exc.getMessage());
				return response;
			}
			if (pracownikSzukany == null) {
				response.setErrorMessage("W pliku addds'�w zosta�y ustawione loginy, kt�rych nie ma w bazie: "
						+ "admin");
				response.setIsError(new Boolean(true));
				return response;
			}

			// Utworzenie dokumentu - Zapoznanie si� z dokumentami

			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					"rootdivision", request.getLogin(), "rootdivision",
					request.getLogin(), null, summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_ZWOLNIONY_PRACOWNIK);

			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());
			dockindKeys.put("NAZWA_WNIOSKU", "Zwolnienie Pracownika");
			dockindKeys.put("FIRSTNAME", request.getImie());
			dockindKeys.put("LASTNAME", request.getNazwisko());
			dockindKeys.put("POSITION", request.getStanowisko());
			dockindKeys.put("TITLE", request.getStanowisko());
			dockindKeys.put("DATA_ZWOLNIENIA", request.getDataZwolnienia());
			dockindKeys.put("WORKPLACE", request.getMiejscePracy());

			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor("admin");

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, "admin");
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			/*
			 * JBPMSnapshotProvider jbpm = new JBPMSnapshotProvider();
			 * jbpm.updateTaskList(new Session(), doc.getId(), doc.getType(),
			 * doc.getType(), request.getUsername());
			 */

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();

		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Zwolnienie Pracownika");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}

		return response;
	}

	/**
	 * Metoda wywo�ywana w momencie aktualizacji danych kadrowych pracownika
	 * 
	 * @param request
	 * @return
	 */
	public ZmianaKadrowaPracownikaResponse ZmianaKadrowaPracownika(
			ZmianaKadrowaPracownikaRequest request) {

		ZmianaKadrowaPracownikaResponse response = new ZmianaKadrowaPracownikaResponse();

		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces zmiany kadrowej pracownika");
		response.setIsError(new Boolean(false));

		// Sprawdzenie, czy wszystkie wymagane parametry zosta�y przekazane

		// Login u�ytkownika
		if (!IsValidStringField(request.getLogin())) {
			response.setErrorMessage("Login u�ytkownika jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Imi�
		if (!IsValidStringField(request.getImie())) {
			response.setErrorMessage("Pole imi� jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Nazwisko
		if (!IsValidStringField(request.getNazwisko())) {
			response.setErrorMessage("Pole Nazwisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Email
		if (!IsValidStringField(request.getAdresEmail())) {
			response.setErrorMessage("Pole Adres Email jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Stanowisko
		if (!IsValidStringField(request.getStanowisko())) {
			response.setErrorMessage("Pole Stanowisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Miejsce pracy
		if (!IsValidStringField(request.getMiejscePracy())) {
			response.setErrorMessage("Pole Miejsce Pracy jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d po��czenia bazy danych systemu");
			response.setIsError(new Boolean(true));
			log.debug("B��d pod��czenia do bazy danych. " + exc.getMessage());
			return response;
		}

		try {
			// Utworzenie dokumentu - Zapoznanie si� z dokumentami
			String summary = "Dokument wygenerowany automatycznie - Zmiana Kadrowa Pracownika";

			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					"rootdivision", "admin", "rootdivision", "admin", null,
					summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_ZMIANA_KADROWA_PRACOWNIKA);

			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());
			dockindKeys.put("NAZWA_WNIOSKU", "Zwolnienie Pracownika");
			dockindKeys.put("FIRSTNAME", request.getImie());
			dockindKeys.put("LASTNAME", request.getNazwisko());
			dockindKeys.put("POSITION", request.getStanowisko());
			dockindKeys.put("TITLE", request.getStanowisko());
			dockindKeys.put("WORKPLACE", request.getMiejscePracy());

			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor("admin");

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, "admin");
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			/*
			 * JBPMSnapshotProvider jbpm = new JBPMSnapshotProvider();
			 * jbpm.updateTaskList(new Session(), doc.getId(), doc.getType(),
			 * doc.getType(), request.getUsername());
			 */

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();

		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Zmiana Kadrowa Pracownika");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}

		return response;
	}

	/**
	 * Metoda wywo�ywana w momencie ustanowienia d�ugiego urlopu pracownika
	 * 
	 * @param request
	 * @return
	 */
	public UrlopPracownikaResponse UrlopPracownika(
			UrlopPracownikaRequest request) {

		UrlopPracownikaResponse response = new UrlopPracownikaResponse();

		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces urlopu pracownika");
		response.setIsError(new Boolean(false));

		// Sprawdzenie, czy wszystkie wymagane parametry zosta�y przekazane

		// Login u�ytkownika
		if (!IsValidStringField(request.getLogin())) {
			response.setErrorMessage("Login u�ytkownika jest wymagany");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Imi�
		if (!IsValidStringField(request.getImie())) {
			response.setErrorMessage("Pole imi� jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Nazwisko
		if (!IsValidStringField(request.getNazwisko())) {
			response.setErrorMessage("Pole Nazwisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		// Email
		if (!IsValidStringField(request.getAdresEmail())) {
			response.setErrorMessage("Pole Adres Email jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Stanowisko
		if (!IsValidStringField(request.getStanowisko())) {
			response.setErrorMessage("Pole Stanowisko jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Miejsce pracy
		if (!IsValidStringField(request.getMiejscePracy())) {
			response.setErrorMessage("Pole Miejsce Pracy jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Data urlopu od
		if (!IsValidDateField(request.getDataUrlopuOd())) {
			response.setErrorMessage("Pole data urlopu od jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Data urlopu od
		if (!IsValidDateField(request.getDataUrlopuDo())) {
			response.setErrorMessage("Pole data urlopu do jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d po��czenia bazy danych systemu");
			response.setIsError(new Boolean(true));
			log.debug("B��d pod��czenia do bazy danych. " + exc.getMessage());
			return response;
		}

		try {
			// Utworzenie dokumentu - ustanowienie urlopu
			String summary = "Dokument wygenerowany automatycznie - Urlop Pracownika";

			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					"rootdivision", "admin", "rootdivision", "admin", null,
					summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_URLOP_PRACOWNIKA);

			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());
			dockindKeys.put("NAZWA_WNIOSKU", "Zwolnienie Pracownika");
			dockindKeys.put("FIRSTNAME", request.getImie());
			dockindKeys.put("LASTNAME", request.getNazwisko());
			dockindKeys.put("POSITION", request.getStanowisko());
			dockindKeys.put("TITLE", request.getStanowisko());
			dockindKeys.put("WORKPLACE", request.getMiejscePracy());
			dockindKeys.put("DATA_URLOPU_OD", request.getDataUrlopuOd());
			dockindKeys.put("DATA_URLOPU_DO", request.getDataUrlopuDo());

			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor("admin");

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, "admin");
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();

		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Zmiana Kadrowa Pracownika");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}

		return response;
	}

	public UtworzFaktureResponse UtworzFakture(UtworzFaktureRequest request) {

		UtworzFaktureResponse response = new UtworzFaktureResponse();

		// Ustawienie parametr�w odpowiedzi
		response.setErrorMessage("Uruchomiono proces utworzenia faktury w systemie EOD.");
		response.setIsError(new Boolean(false));

		// Nazwa Wniosku
		/*if (!IsValidStringField(request.getNazwaWniosku())) {
			response.setErrorMessage("Pole Nazwa Wniosku jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}*/

		// Sprzedawca
		if (!IsValidStringField(request.getSprzedawca())) {
			response.setErrorMessage("Pole Sprzedawca jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		// Numer Faktury
		if (!IsValidStringField(request.getNumerFaktury())) {
			response.setErrorMessage("Pole Numer Faktury jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}

		
		
		// Data wystawienia
		if (!IsValidDateField(request.getDataWystawienia())) {
			response.setErrorMessage("Pole Data Wystawienia jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}		
		
		// NIP
		if (!IsValidStringField(request.getNip())) {
			response.setErrorMessage("Pole NIP jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}	
		
		// Miejscowo��
		if (!IsValidStringField(request.getMiejscowosc())) {
			response.setErrorMessage("Pole Miejscowo�� jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}	
		/*
		 * W pole miejscowosc moze byc podany caly zagregowany adres z APTEKI
		 * dlatego sprwadzane jest czy pola kod pocztowy i ulica s� puste.
		 * Je�li choc jedno z nich zawiera jakas wartosc to przeprowadzana jest normalna walidacja kazdego pola
		 */
		if(IsValidStringField(request.getKodPocztowy()) || IsValidStringField(request.getUlica())){
			// Kod pocztowy
			if (!IsValidStringField(request.getKodPocztowy())) {
				response.setErrorMessage("Pole Kod Pocztowy jest wymagane");
				response.setIsError(new Boolean(true));
				return response;
			}	

			// Ulica
			if (!IsValidStringField(request.getUlica())) {
				response.setErrorMessage("Pole Ulica jest wymagane");
				response.setIsError(new Boolean(true));
				return response;
			}			
		}
		
	
		// Numer konta
		if (!IsValidStringField(request.getNumerKonta())) {
			response.setErrorMessage("Pole Numer Konta jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}	
	
		// Kwota netto
		if (!IsValidAmountField(request.getKwotaNetto())) {
			response.setErrorMessage("Pole Kwota Netto jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}	
		
		// Kwota brutto
		if (!IsValidAmountField(request.getKwotaBrutto())) {
			response.setErrorMessage("Pole Kwota Brutto jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}	
		
		
				
		try {
			DSApi.openAdmin();
			DSApi.context().begin();
		} catch (Exception exc) {
			response.setErrorMessage("B��d systemu EOD - brak po��czenia z baz� danych");
			response.setIsError(new Boolean(true));
			log.debug("B��d systemu EOD - brak po��czenia z baz� danych. " + exc.getMessage());
			return response;
		}

		try {
			// Utworzenie dokumentu - Faktura
			String summary = "Dokument wygenerowany automatycznie - Faktura";

			String uzytkownikLogin = "";
			
		
			OutOfficeDocument doc = OfficeUtils.newOutOfficeDocument(
					"rootdivision", "admin", "rootdivision",
					"admin", null, summary, true);

			doc.create();

			DocumentKind documentKind = DocumentKind
					.findByCn(DocumentLogicLoader.WSSK_FAKTURA); // Faktura
						
			if (documentKind == null){
				response.setErrorMessage("Nie wczytano logiki dokumentu WSSK_FAKTURA. Sprawd�, czy wgrano dockind do aplikacji.");
				response.setIsError(new Boolean(true));
				return response;	
			}
			
			
			
			Map<String, Object> dockindKeys = new HashMap<String, Object>();
			
			//dockindKeys.put("NAZWA_WNIOSKU", request.getNazwaWniosku());
			
			dockindKeys.put("SPRZEDAWCA", request.getSprzedawca());
			dockindKeys.put("NR_FAKTURY", request.getNumerFaktury());
			dockindKeys.put("KOD_POCZTOWY", request.getKodPocztowy());
			dockindKeys.put("NIP", request.getNip());
			dockindKeys.put("DATA_WYSTAWIENIA", request.getDataWystawienia());
			dockindKeys.put("MIEJSCOWOSC", request.getMiejscowosc());
			dockindKeys.put("ULICA", request.getUlica());
			dockindKeys.put("NR_KONTA", request.getNumerKonta());
			dockindKeys.put("KWOTA_NETTO", request.getKwotaNetto());
			dockindKeys.put("KWOTA_BRUTTO", request.getKwotaBrutto());
			dockindKeys.put("DOC_DESCRIPTION", summary);
			dockindKeys.put("DOC_DATE", new Date());
			dockindKeys.put("GUID_JEDNOSTKA_DO_PRZEKAZANIA", request.getGuidJednostkiDoPrzekazania());
		
			doc.setDocumentKind(documentKind);

			Long newDocumentId = doc.getId();

			documentKind.set(newDocumentId, dockindKeys);

			doc.getDocumentKind().logic().archiveActions(doc, 0);
			doc.getDocumentKind().logic().documentPermissions(doc);
			doc.setAuthor(uzytkownikLogin);

			DSApi.context().session().save(doc);

			Map<String, Object> map = Maps.newHashMap();

			map.put(ASSIGN_USER_PARAM, "admin");
			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");

			if (!DSApi.context().inTransaction())
				DSApi.context().begin();
			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
					.onStart(doc, map);
			DSApi.context().commit();

			/*
			 * JBPMSnapshotProvider jbpm = new JBPMSnapshotProvider();
			 * jbpm.updateTaskList(new Session(), doc.getId(), doc.getType(),
			 * doc.getType(), request.getUsername());
			 */

			DSApi.context().begin();

			TaskSnapshot.updateByDocument(doc);

			DSApi.context().commit();
		} catch (Exception exc) {
			response.setErrorMessage("B��d podczas wywo�ywania procesu Utworzenie faktury.");
			response.setIsError(new Boolean(true));
			log.debug(exc);
			return response;
		}

		return response;

	}
	
	public ImportujFaktureResponse ImportujFakture(ImportujFaktureRequest request) {
		ImportujFaktureResponse response = new ImportujFaktureResponse();
		response.setErrorMessage("Uruchomiono proces importowania faktury w systemie EOD.");
		response.setIsError(new Boolean(false));
		
		// plik tekstowy
		if(!IsValidStringField(request.getMetryka())){
			response.setErrorMessage("Pole metryka jest wymagane");
			response.setIsError(new Boolean(true));
			return response;
		}
		
		Scanner sc;
		UtworzFaktureRequest faktura = new UtworzFaktureRequest();
	    sc = new Scanner(request.getMetryka());
		String linia = "";
		boolean czyFakturaXML = false;
		StringBuilder fakturaXML = new StringBuilder();
		String[] value = null;
		while(sc.hasNext()){
			linia = sc.nextLine();
			if(!czyFakturaXML){
				if(linia.contains("<_Sprzedawca>")){
					value = linia.split("<[^>]*>");
					faktura.setSprzedawca(value[1]);
				}
				if(linia.contains("<_NR_FAKTURY>")){
					value = linia.split("<[^>]*>");
					faktura.setNumerFaktury(value[1]); 
				}
				if(linia.contains("<_Kod_pocztowy>")){
					value = linia.split("<[^>]*>");
					faktura.setKodPocztowy(value[1]);
				}
				if(linia.contains("<_NIP>")){
					value = linia.split("<[^>]*>");
					faktura.setNip(value[1]);	 
				}
				if(linia.contains("<_Data_Wystawienia>")){
					value = linia.split("<[^>]*>");
					DateFormat dateFrm = new SimpleDateFormat("yyyy-MM-dd");
					try{
						Date data = dateFrm.parse(value[1]);
						faktura.setDataWystawienia(data);
					} catch (Exception e) {
						response.setErrorMessage("B��d podczas parsowania pliku. Pole DATA WYSTAWIENIA jest w z�ym formacie. Poprawny format: YYYY-MM-DD. B��d:" + e);
					}	
				}
				if(linia.contains("<_Miejscowosc>")){
					value = linia.split("<[^>]*>");
					faktura.setMiejscowosc(value[1]);	 
				}
				if(linia.contains("<_Ulica>")){
					value = linia.split("<[^>]*>");
					faktura.setUlica(value[1]);	 
				}
				if(linia.contains("<_Nr_konta>")){
					value = linia.split("<[^>]*>");
					faktura.setNumerKonta(value[1]);
				}
				if(linia.contains("<_Netto>")){
					value = linia.split("<[^>]*>");
					BigDecimal netto;
					try {
						netto = new BigDecimal(value[1]);
			    		faktura.setKwotaNetto(netto);	 
					} catch (NumberFormatException e) {
						response.setErrorMessage("B��d podczas parsowania pliku. Pole KWOTA NETTO jest podana w z�ym formacie (przyk�ad poprawnego formatu = 12345.04). " + e);
						response.setIsError(new Boolean(true));
					}
				}
				if(linia.contains("<_Brutto>")){
					value = linia.split("<[^>]*>");
					BigDecimal brutto;
					try {
						brutto = new BigDecimal(value[1]);
			    		faktura.setKwotaBrutto(brutto);	 
					} catch (NumberFormatException e) {
						response.setErrorMessage("B��d podczas parsowania pliku. Pole KWOTA BRUTTO jest podana w z�ym formacie (przyk�ad poprawnego formatu = 12345.04). " + e);
						response.setIsError(new Boolean(true));
					}
				}
				if(linia.contains("<_Pochodzenie>")){
					value = linia.split("<[^>]*>");
					faktura.setPochodzenie(value[1]);
				}
				if(linia.contains("<_FakturaXML>")){
					if(linia.contains("</_FakturaXML>")){
						linia = linia.replaceFirst("<_FakturaXML>", "");
						linia = linia.replaceFirst("</_FakturaXML>", "");
						linia = linia.trim();
						fakturaXML.append(linia);
					} else {
						czyFakturaXML = true;
						linia = linia.trim();
						if(!linia.equals("<_FakturaXML>")){
							linia = linia.replaceFirst("<_FakturaXML>", "");
							linia = linia.trim();
							fakturaXML.append(linia+"\n");
						}
					}
				}
			} else {
				if(linia.contains("</_FakturaXML>")){
					czyFakturaXML = false;
				}
				linia = linia.trim();
				if(!linia.equals("</_FakturaXML>")){
					linia = linia.replaceFirst("</_FakturaXML>", "");
					fakturaXML.append(linia+"\n");
				}
			}
		}
		faktura.setNazwaWniosku("Faktura");
		faktura.setFakturaXML(fakturaXML.toString());
		UtworzFaktureResponse r1 = UtworzFakture(faktura);
		if(r1.getIsError()){
			response.setErrorMessage(r1.getErrorMessage());
			response.setIsError(r1.getIsError());
		}
		return response;
	}
	
	public ZapotrzebowanieWewAktualizacjaResponse ZapotrzebowanieWewAktualizacja(ZapotrzebowanieWewAktualizacjaRequest request){
		
		ZapotrzebowanieWewAktualizacjaResponse response = new ZapotrzebowanieWewAktualizacjaResponse();
		response.setErrorMessage("Uruchomiono proces aktualizacji zapotrzebowania wewn�trznego w systemie EOD.");
		response.setIsError(new Boolean(false));
		
		//GUID
		if(!(IsValidStringField(request.getGUID()))){
			response.setErrorMessage("Pole GUID jest wymagane.");
			response.setIsError(new Boolean(true));
		}
		
		//idProduktuZPlanuZakupu dla wszystkich pozycji
		for(PozycjeZapotrzebowaniaWew poz: request.getPozycja()){
			if(poz.getIdProduktuZPlanuZakupu()==null){
				response.setErrorMessage("Przynajmniej jedna z pozycji nie ma podanego identyfikatora.");
				response.setIsError(new Boolean(true));
			}
		}
		
		//ArrayList lista = request.getPozycje();
		
//		try {
//			DSApi.openAdmin();
//			DSApi.context().begin();
//		} catch (Exception exc) {
//			response.setErrorMessage("B��d systemu EOD - brak po��czenia z baz� danych");
//			response.setIsError(new Boolean(true));
//			log.debug("B��d systemu EOD - brak po��czenia z baz� danych. " + exc.getMessage());
//			return response;
//		}
//		
//		try {
//			
//			OfficeDocument doc = OfficeDocument.find(request.getId());
//			
//			DocumentKind documentKind = DocumentKind
//					.findByCn(DocumentLogicLoader.WSSK_ZAPOTRZEBOWANIE_WEW); // Zapotrzebowanie wewnetrzne
//						
//			if (documentKind == null){
//				response.setErrorMessage("Nie wczytano logiki dokumentu WSSK_ZAPOTRZEBOWANIE_WEW. Sprawd�, czy wgrano dockind do aplikacji.");
//				response.setIsError(new Boolean(true));
//				return response;
//			}
//			//FieldsManager fm = new FieldsManager(documentId, documentKind);
//				
//			Map<String, Object> dockindKeys = new HashMap<String, Object>();
//			
//			dockindKeys.put("NAZWA_WNIOSKU", request.getNazwaWniosku());
//			dockindKeys.put("ZAPOTRZEBOWANIE_DICT", request.getArtykuly());
//			dockindKeys.put("STATUSDOKUMENTU", request.getStatus());
//			
//			/*doc.setDocumentKind(documentKind);
//
//			Long newDocumentId = doc.getId();
//
//			documentKind.set(newDocumentId, dockindKeys);
//
//			doc.getDocumentKind().logic().archiveActions(doc, 0);
//			doc.getDocumentKind().logic().documentPermissions(doc);
//			doc.setAuthor(uzytkownikLogin);*/
//
//			DSApi.context().session().save(doc);
//
//			Map<String, Object> map = Maps.newHashMap();
//
//			map.put(ASSIGN_USER_PARAM, "admin");
//			map.put(ASSIGN_DIVISION_GUID_PARAM, "rootdivision");
//
//			if (!DSApi.context().inTransaction())
//				DSApi.context().begin();
//			doc.getDocumentKind().getDockindInfo().getProcessesDeclarations()
//					.onStart(doc, map);
//			DSApi.context().commit();
//			
//	
//			DSApi.context().begin();
//
//			TaskSnapshot.updateByDocument(doc);
//
//			DSApi.context().commit();
//		} catch (Exception exc) {
//			response.setErrorMessage("B��d podczas wywo�ywania procesu AKTUALIZACJA ZAPOTRZEBOWANIA WEWNETRZNEGO.");
//			response.setIsError(new Boolean(true));
//			log.debug(exc);
//			return response;
//		}
		
		
		return response;
	}

	private boolean IsValidStringField(String fieldValue) {

		if (fieldValue == null) {
			return false;
		} else {
			return true;
		}
	}

	private boolean IsValidDateField(Date fieldValue) {

		if (fieldValue == null) {
			return false;
		} else {
			return true;
		}
	}
	
	private boolean IsValidAmountField(BigDecimal fieldValue) {

		if (fieldValue == null) {
			return false;
		} else {
			return true;
		}
	}
	
	private boolean IsValidLongField(Long fieldValue){
		if (fieldValue == null){
			return false;
		} else {
			return true;
		}
	}

	
	/**
	 * Klasa wyj�tku us�ugi WsskService
	 * 
	 * @author DELL
	 * 
	 */
	class WsskServiceException extends Exception {
		String mistake;

		public WsskServiceException() {
			super();
			mistake = "nieznany b��d";
		}

		public WsskServiceException(String err) {
			super(err);
			mistake = err;
		}

		public String getError() {
			return mistake;
		}
	}

}
