package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;
import java.util.Date;

public class DyspozycjaWystawieniaFakturyRequest  implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3856825354038794783L;
	public String getPacjentNazwa() {
		return pacjentNazwa;
	}
	public void setPacjentNazwa(String pacjentNazwa) {
		this.pacjentNazwa = pacjentNazwa;
	}
	public String getPacjentPesel() {
		return pacjentPesel;
	}
	public void setPacjentPesel(String pacjentPesel) {
		this.pacjentPesel = pacjentPesel;
	}
	public String getPacjentUlica() {
		return pacjentUlica;
	}
	public void setPacjentUlica(String pacjentUlica) {
		this.pacjentUlica = pacjentUlica;
	}
	public String getPacjentNrDomu() {
		return pacjentNrDomu;
	}
	public void setPacjentNrDomu(String pacjentNrDomu) {
		this.pacjentNrDomu = pacjentNrDomu;
	}
	public String getPacjentNrMieszkania() {
		return pacjentNrMieszkania;
	}
	public void setPacjentNrMieszkania(String pacjentNrMieszkania) {
		this.pacjentNrMieszkania = pacjentNrMieszkania;
	}
	public String getPacjentKodPocztowy() {
		return pacjentKodPocztowy;
	}
	public void setPacjentKodPocztowy(String pacjentKodPocztowy) {
		this.pacjentKodPocztowy = pacjentKodPocztowy;
	}
	public String getPacjentMiasto() {
		return pacjentMiasto;
	}
	public void setPacjentMiasto(String pacjentMiasto) {
		this.pacjentMiasto = pacjentMiasto;
	}
	public Date getDataPobytuOd() {
		return dataPobytuOd;
	}
	public void setDataPobytuOd(Date dataPobytuOd) {
		this.dataPobytuOd = dataPobytuOd;
	}
	public Date getDataPobytuDo() {
		return dataPobytuDo;
	}
	public void setDataPobytuDo(Date dataPobytuDo) {
		this.dataPobytuDo = dataPobytuDo;
	}
	private String pacjentNazwa;
	private String pacjentPesel;
	private String pacjentUlica;
	private String pacjentNrDomu;
	private String pacjentNrMieszkania;
	private String pacjentKodPocztowy;
	private String pacjentMiasto;
	private Date dataPobytuOd;
	private Date dataPobytuDo;
	
}
