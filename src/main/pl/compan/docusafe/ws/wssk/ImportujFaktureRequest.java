package pl.compan.docusafe.ws.wssk;

import java.io.File;
import java.io.Serializable;

public class ImportujFaktureRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5163642217049660265L;
	
	private String metryka;

	
	public String getMetryka() {
		return metryka;
	}
	public void setMetryka(String metryka) {
		this.metryka = metryka;
	}
	
}
