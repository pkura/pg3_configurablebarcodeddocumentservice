package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;
import java.util.Date;

public class UrlopPracownikaRequest  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1682753168762107952L;
	/**
	 * Imi� nowego pracownika
	 */
	private String imie;
	/**
	 * Nazwisko nowego pracownika
	 */
	private String nazwisko;
	/**
	 * Adres e-mail
	 */
	private String adresEmail;
	/**
	 * Login nowego pracownika  
	 */
	private String login;
	/**
	 * Stanowisko nowego pracownika
	 */
	private String stanowisko;
	/**
	 * Miejsce pracy nowego pracownika
	 */
	private String miejscePracy;	
	/**
	 * Data rozpocz�cia urlopu
	 */
	private Date dataUrlopuOd;
	/**
	 * Data zako�czenia urlopu
	 */
	private Date dataUrlopuDo;
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getAdresEmail() {
		return adresEmail;
	}
	public void setAdresEmail(String adresEmail) {
		this.adresEmail = adresEmail;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getStanowisko() {
		return stanowisko;
	}
	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}
	public String getMiejscePracy() {
		return miejscePracy;
	}
	public void setMiejscePracy(String miejscePracy) {
		this.miejscePracy = miejscePracy;
	}
	public Date getDataUrlopuOd() {
		return dataUrlopuOd;
	}
	public void setDataUrlopuOd(Date dataUrlopuOd) {
		this.dataUrlopuOd = dataUrlopuOd;
	}
	public Date getDataUrlopuDo() {
		return dataUrlopuDo;
	}
	public void setDataUrlopuDo(Date dataUrlopuDo) {
		this.dataUrlopuDo = dataUrlopuDo;
	}
	
	
}
