package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;

public class ZapotrzebowanieWewAktualizacjaResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4279618832139070025L;

	private String errorMessage;
	private Boolean isError;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

}
