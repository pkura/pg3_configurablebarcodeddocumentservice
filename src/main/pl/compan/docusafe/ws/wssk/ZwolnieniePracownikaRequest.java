package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;
import java.util.Date;

public class ZwolnieniePracownikaRequest implements Serializable{

	/**
	 * Wygenerowany serial
	 */
	private static final long serialVersionUID = -6127355279450340616L;	
	/**
	 * Imi� nowego pracownika
	 */
	private String imie;
	/**
	 * Nazwisko nowego pracownika
	 */
	private String nazwisko;
	/**
	 * Adres e-mail
	 */
	private String adresEmail;
	/**
	 * Login nowego pracownika  
	 */
	private String login;
	/**
	 * Stanowisko nowego pracownika
	 */
	private String stanowisko;
	/**
	 * Miejsce pracy nowego pracownika
	 */
	private String miejscePracy;	
	/**
	 * Data obowi�zywania zwolnienia
	 */
	private Date dataZwolnienia;
	
	
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getAdresEmail() {
		return adresEmail;
	}
	public void setAdresEmail(String adresEmail) {
		this.adresEmail = adresEmail;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getStanowisko() {
		return stanowisko;
	}
	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}
	public String getMiejscePracy() {
		return miejscePracy;
	}
	public void setMiejscePracy(String miejscePracy) {
		this.miejscePracy = miejscePracy;
	}
	public Date getDataZwolnienia() {
		return dataZwolnienia;
	}
	public void setDataZwolnienia(Date dataZwolnienia) {
		this.dataZwolnienia = dataZwolnienia;
	}
	
}
