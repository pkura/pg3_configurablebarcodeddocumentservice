package pl.compan.docusafe.ws.wssk;

public class ZatrudnienieNowegoPracownikaResponse {

	private String errorMessage;
	private Boolean isError;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}
}
