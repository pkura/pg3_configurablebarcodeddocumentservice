package pl.compan.docusafe.ws.wssk;

public class PozycjeZapotrzebowaniaWew {
	private Long idProduktuZPlanuZakupu;
	private double iloscZamowiona;
	private double iloscZaakceptowana;
	private double iloscZrealizowana;
	private String status;
	
	
	public Long getIdProduktuZPlanuZakupu() {
		return idProduktuZPlanuZakupu;
	}
	public void setIdProduktuZPlanuZakupu(Long idProduktuZPlanuZakupu) {
		this.idProduktuZPlanuZakupu = idProduktuZPlanuZakupu;
	}
	
	public double getIloscZaakceptowana() {
		return iloscZaakceptowana;
	}
	public void setIloscZaakceptowana(double iloscZaakceptowana) {
		this.iloscZaakceptowana = iloscZaakceptowana;
	}
	public double getIloscZrealizowana() {
		return iloscZrealizowana;
	}
	public void setIloscZrealizowana(double iloscZrealizowana) {
		this.iloscZrealizowana = iloscZrealizowana;
	}
	public double getIloscZamowiona(){
		return iloscZamowiona;
	}
	public void setIloscZamowiona(double iloscZamowiona) {
		this.iloscZamowiona = iloscZamowiona;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
