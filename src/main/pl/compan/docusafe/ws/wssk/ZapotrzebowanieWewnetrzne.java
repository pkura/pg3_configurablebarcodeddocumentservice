package pl.compan.docusafe.ws.wssk;

import java.util.Date;
import java.util.List;

import pl.compan.docusafe.core.users.DSUser;

public class ZapotrzebowanieWewnetrzne {
	private long id;
	private String nazwaWniosku;
	private Date dataZapotrzebowania;
	private DSUser osobaZglaszajaca;
	private List<PozycjeZapotrzebowaniaWew> artykuly;
	private String status;
	
	public Date getDataZapotrzebowania() {
		return dataZapotrzebowania;
	}
	public void setDataZapotrzebowania(Date dataZapotrzebowania) {
		this.dataZapotrzebowania = dataZapotrzebowania;
	}
	public DSUser getOsobaZglaszajaca() {
		return osobaZglaszajaca;
	}
	public void setOsobaZglaszajaca(DSUser osobaZglaszajaca) {
		this.osobaZglaszajaca = osobaZglaszajaca;
	}
	public List<PozycjeZapotrzebowaniaWew> getArtykuly() {
		return artykuly;
	}
	public void setArtykuly(List<PozycjeZapotrzebowaniaWew> artykuly) {
		this.artykuly = artykuly;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNazwaWniosku() {
		return nazwaWniosku;
	}
	public void setNazwaWniosku(String nazwaWniosku) {
		this.nazwaWniosku = nazwaWniosku;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	

}
