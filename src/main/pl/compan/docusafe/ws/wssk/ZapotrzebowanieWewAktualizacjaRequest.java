package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;

public class ZapotrzebowanieWewAktualizacjaRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6407644643415336269L;
	private String GUID;
	private PozycjeZapotrzebowaniaWew[] pozycja;

	public PozycjeZapotrzebowaniaWew[] getPozycja() {
		return pozycja;
	}

	public void setPozycja(PozycjeZapotrzebowaniaWew[] pozycja) {
		this.pozycja = pozycja;
	}

	public String getGUID() {
		return GUID;
	}

	public void setGUID(String GUID) {
		this.GUID = GUID;
	}
}
