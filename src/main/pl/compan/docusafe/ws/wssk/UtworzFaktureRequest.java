package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class UtworzFaktureRequest implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1048189022310522557L;

	private String nazwaWniosku;
	
	private String sprzedawca;
	
	private String numerFaktury;
	
	private String kodPocztowy;
	
	private Date dataWystawienia;
	
	private String nip;
	
	private String miejscowosc;
	
	private String ulica;
	
	private String numerKonta;
	
	private BigDecimal kwotaNetto;
	
	private BigDecimal kwotaBrutto;
	
	private String guidJednostkiDoPrzekazania;
	/**
	 * Jest to odpowiednik stanowiska dostaw ze schemy SIMPLE'owej
	 */
	private String pochodzenie;
	/**
	 * W przypadku gdy faktura jest wysy�ana z Apteki przesy�any jest tak�e plik xml faktury zakupowej.
	 * Charakteryzuje si� tym ze ma zagregowany adres w jednym pole, kt�ry b�dzie musia� by� p�niej rodzielony na psozczeg�lne pola 
	 */
	private String fakturaXML;

	public String getPochodzenie() {
		return pochodzenie;
	}

	public void setPochodzenie(String pochodzenie) {
		this.pochodzenie = pochodzenie;
	}

	public String getFakturaXML() {
		return fakturaXML;
	}

	public void setFakturaXML(String fakturaXML) {
		this.fakturaXML = fakturaXML;
	}

	public String getGuidJednostkiDoPrzekazania() {
		return guidJednostkiDoPrzekazania;
	}

	public void setGuidJednostkiDoPrzekazania(String guidJednostkiDoPrzekazania) {
		this.guidJednostkiDoPrzekazania = guidJednostkiDoPrzekazania;
	}

	public String getNazwaWniosku() {
		return nazwaWniosku;
	}

	public void setNazwaWniosku(String nazwaWniosku) {
		this.nazwaWniosku = nazwaWniosku;
	}

	public String getSprzedawca() {
		return sprzedawca;
	}

	public void setSprzedawca(String sprzedawca) {
		this.sprzedawca = sprzedawca;
	}

	public String getNumerFaktury() {
		return numerFaktury;
	}

	public void setNumerFaktury(String numerFaktury) {
		this.numerFaktury = numerFaktury;
	}

	public String getKodPocztowy() {
		return kodPocztowy;
	}

	public void setKodPocztowy(String kodPocztowy) {
		this.kodPocztowy = kodPocztowy;
	}

	public Date getDataWystawienia() {
		return dataWystawienia;
	}

	public void setDataWystawienia(Date dataWystawienia) {
		this.dataWystawienia = dataWystawienia;
	}

	public String getNip() {
		return nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getMiejscowosc() {
		return miejscowosc;
	}

	public void setMiejscowosc(String miejscowosc) {
		this.miejscowosc = miejscowosc;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getNumerKonta() {
		return numerKonta;
	}

	public void setNumerKonta(String numerKonta) {
		this.numerKonta = numerKonta;
	}

	public BigDecimal getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(BigDecimal kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public BigDecimal getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(BigDecimal kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}
		
}
