package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;
import java.util.Date;

public class CreateDocumentRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2439627526785060921L;
	/**
	 * Rodzaj dokumentu
	 */
	private String type;
	/**
	 * Opis dokumentu
	 */
	private String description;
	/**
	 * Data wystawienia
	 */
	private Date issedOnDate;
	/**
	 * Numer dokumentu
	 */
	private String number;
	/**
	 * Numer listu poleconego - R
	 */
	private String numberOfRegistered;
	/**
	 * Data wpłynięcia
	 */
	private String incomingDate;
	
}
