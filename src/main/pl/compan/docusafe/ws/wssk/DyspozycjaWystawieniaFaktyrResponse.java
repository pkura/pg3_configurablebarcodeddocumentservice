package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;

public class DyspozycjaWystawieniaFaktyrResponse   implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -692671776646584246L;
	private String errorMessage;
	private Boolean isError;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Boolean getIsError() {
		return isError;
	}
	public void setIsError(Boolean isError) {
		this.isError = isError;
	}

}
