package pl.compan.docusafe.ws.wssk;

import java.io.Serializable;

public class ZatrudnienieNowegoPracownikaRequest implements Serializable {
	
	/**
	 * Wygenerowany unikalny serialVersionUID
	 */
	private static final long serialVersionUID = -3320237091253722598L;
	/**
	 * Imi� nowego pracownika
	 */
	private String imie;
	/**
	 * Nazwisko nowego pracownika
	 */
	private String nazwisko;
	/**
	 * Adres e-mail
	 */
	private String adresEmail;
	/**
	 * Has�o tymczasowe dla u�ytkownika
	 */
	private String hasloTymczasowe;
	/**
	 * Nazwa jednostki organizacyjnej
	 */
	private String jednostkaOrganizacyjnaNazwa;
	/**
	 * Guid jednostki organizacyjnej
	 */
	private String jednostkaOrganizacyjnaGuid;	
	/**
	 * Login prze�o�onego
	 */
	private String loginPrzelozonego;
	/**
	 * Login nowego pracownika  
	 */
	private String login;
	/**
	 * Stanowisko nowego pracownika
	 */
	private String stanowisko;
	/**
	 * Miejsce pracy nowego pracownika
	 */
	private String miejscePracy;	
	/**
	 * Numer pesel nowego pracownika
	 */
	private String pesel;
	/**
	 * Tytu� naukowy nowego pracownika
	 */
	private String tytulNaukowy;
	/**
	 * Numer uprawnie� nowego pracownika do wykonywania zawodu
	 */
	private String numerUprawnienWykonywaniaZawodu;
	public String getImie() {
		return imie;
	}
	public void setImie(String imie) {
		this.imie = imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getAdresEmail() {
		return adresEmail;
	}
	public void setAdresEmail(String adresEmail) {
		this.adresEmail = adresEmail;
	}
	public String getHasloTymczasowe() {
		return hasloTymczasowe;
	}
	public void setHasloTymczasowe(String hasloTymczasowe) {
		this.hasloTymczasowe = hasloTymczasowe;
	}
	public String getJednostkaOrganizacyjnaNazwa() {
		return jednostkaOrganizacyjnaNazwa;
	}
	public void setJednostkaOrganizacyjnaNazwa(String jednostkaOrganizacyjnaNazwa) {
		this.jednostkaOrganizacyjnaNazwa = jednostkaOrganizacyjnaNazwa;
	}
	public String getJednostkaOrganizacyjnaGuid() {
		return jednostkaOrganizacyjnaGuid;
	}
	public void setJednostkaOrganizacyjnaGuid(String jednostkaOrganizacyjnaGuid) {
		this.jednostkaOrganizacyjnaGuid = jednostkaOrganizacyjnaGuid;
	}
	public String getLoginPrzelozonego() {
		return loginPrzelozonego;
	}
	public void setLoginPrzelozonego(String loginPrzelozonego) {
		this.loginPrzelozonego = loginPrzelozonego;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getStanowisko() {
		return stanowisko;
	}
	public void setStanowisko(String stanowisko) {
		this.stanowisko = stanowisko;
	}
	public String getMiejscePracy() {
		return miejscePracy;
	}
	public void setMiejscePracy(String miejscePracy) {
		this.miejscePracy = miejscePracy;
	}
	public String getPesel() {
		return pesel;
	}
	public void setPesel(String pesel) {
		this.pesel = pesel;
	}
	public String getTytulNaukowy() {
		return tytulNaukowy;
	}
	public void setTytulNaukowy(String tytulNaukowy) {
		this.tytulNaukowy = tytulNaukowy;
	}
	public String getNumerUprawnienWykonywaniaZawodu() {
		return numerUprawnienWykonywaniaZawodu;
	}
	public void setNumerUprawnienWykonywaniaZawodu(
			String numerUprawnienWykonywaniaZawodu) {
		this.numerUprawnienWykonywaniaZawodu = numerUprawnienWykonywaniaZawodu;
	}
	
}
