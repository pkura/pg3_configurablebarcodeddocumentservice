
/**
 * DictionaryServicesEmployeeCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServicesEmployeeCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesEmployeeCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesEmployeeCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesEmployeeCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getEmployeeByNrewid method
            * override this method for handling normal response from getEmployeeByNrewid operation
            */
           public void receiveResultgetEmployeeByNrewid(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesEmployeeStub.GetEmployeeByNrewidResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeByNrewid operation
           */
            public void receiveErrorgetEmployeeByNrewid(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeesCount method
            * override this method for handling normal response from getEmployeesCount operation
            */
           public void receiveResultgetEmployeesCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesEmployeeStub.GetEmployeesCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeesCount operation
           */
            public void receiveErrorgetEmployeesCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployees method
            * override this method for handling normal response from getEmployees operation
            */
           public void receiveResultgetEmployees(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesEmployeeStub.GetEmployeesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployees operation
           */
            public void receiveErrorgetEmployees(java.lang.Exception e) {
            }
                


    }
    