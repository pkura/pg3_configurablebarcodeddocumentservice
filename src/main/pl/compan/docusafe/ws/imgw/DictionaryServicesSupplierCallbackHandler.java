
/**
 * DictionaryServicesSupplierCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServicesSupplierCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesSupplierCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesSupplierCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesSupplierCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getSuppliersCount method
            * override this method for handling normal response from getSuppliersCount operation
            */
           public void receiveResultgetSuppliersCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub.GetSuppliersCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSuppliersCount operation
           */
            public void receiveErrorgetSuppliersCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierByIdn method
            * override this method for handling normal response from getSupplierByIdn operation
            */
           public void receiveResultgetSupplierByIdn(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub.GetSupplierByIdnResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierByIdn operation
           */
            public void receiveErrorgetSupplierByIdn(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSuppliers method
            * override this method for handling normal response from getSuppliers operation
            */
           public void receiveResultgetSuppliers(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesSupplierStub.GetSuppliersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSuppliers operation
           */
            public void receiveErrorgetSuppliers(java.lang.Exception e) {
            }
                


    }
    