
/**
 * DictionaryServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitCount method
            * override this method for handling normal response from getOrganizationalUnitCount operation
            */
           public void receiveResultgetOrganizationalUnitCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetOrganizationalUnitCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitCount operation
           */
            public void receiveErrorgetOrganizationalUnitCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInProjectsCount method
            * override this method for handling normal response from getRolesInProjectsCount operation
            */
           public void receiveResultgetRolesInProjectsCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetRolesInProjectsCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInProjectsCount operation
           */
            public void receiveErrorgetRolesInProjectsCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTerms method
            * override this method for handling normal response from getPaymentTerms operation
            */
           public void receiveResultgetPaymentTerms(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPaymentTermsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTerms operation
           */
            public void receiveErrorgetPaymentTerms(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountByID method
            * override this method for handling normal response from getCompanyBankAccountByID operation
            */
           public void receiveResultgetCompanyBankAccountByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCompanyBankAccountByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountByID operation
           */
            public void receiveErrorgetCompanyBankAccountByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFundingByID method
            * override this method for handling normal response from getSourcesOfProjectFundingByID operation
            */
           public void receiveResultgetSourcesOfProjectFundingByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSourcesOfProjectFundingByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFundingByID operation
           */
            public void receiveErrorgetSourcesOfProjectFundingByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByIDM method
            * override this method for handling normal response from getProjectBugdetByIDM operation
            */
           public void receiveResultgetProjectBugdetByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBugdetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByIDM operation
           */
            public void receiveErrorgetProjectBugdetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskCount method
            * override this method for handling normal response from getFinancialTaskCount operation
            */
           public void receiveResultgetFinancialTaskCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialTaskCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskCount operation
           */
            public void receiveErrorgetFinancialTaskCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeByID method
            * override this method for handling normal response from getAssetOperationTypeByID operation
            */
           public void receiveResultgetAssetOperationTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetOperationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeByID operation
           */
            public void receiveErrorgetAssetOperationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentByID method
            * override this method for handling normal response from getFinancialDocumentByID operation
            */
           public void receiveResultgetFinancialDocumentByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialDocumentByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentByID operation
           */
            public void receiveErrorgetFinancialDocumentByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReason method
            * override this method for handling normal response from getAssetLiquidationReason operation
            */
           public void receiveResultgetAssetLiquidationReason(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetLiquidationReasonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReason operation
           */
            public void receiveErrorgetAssetLiquidationReason(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployee method
            * override this method for handling normal response from getEmployee operation
            */
           public void receiveResultgetEmployee(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetEmployeeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployee operation
           */
            public void receiveErrorgetEmployee(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKind method
            * override this method for handling normal response from getPurchasePlanKind operation
            */
           public void receiveResultgetPurchasePlanKind(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKind operation
           */
            public void receiveErrorgetPurchasePlanKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureByID method
            * override this method for handling normal response from getUnitOfMeasureByID operation
            */
           public void receiveResultgetUnitOfMeasureByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitOfMeasureByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureByID operation
           */
            public void receiveErrorgetUnitOfMeasureByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductByIDM method
            * override this method for handling normal response from getCostInvoiceProductByIDM operation
            */
           public void receiveResultgetCostInvoiceProductByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCostInvoiceProductByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductByIDM operation
           */
            public void receiveErrorgetCostInvoiceProductByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUser method
            * override this method for handling normal response from getErpUser operation
            */
           public void receiveResultgetErpUser(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetErpUserResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUser operation
           */
            public void receiveErrorgetErpUser(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView2Count method
            * override this method for handling normal response from getBudgetView2Count operation
            */
           public void receiveResultgetBudgetView2Count(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetBudgetView2CountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView2Count operation
           */
            public void receiveErrorgetBudgetView2Count(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSource method
            * override this method for handling normal response from getFundingSource operation
            */
           public void receiveResultgetFundingSource(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFundingSourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSource operation
           */
            public void receiveErrorgetFundingSource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetByIDM method
            * override this method for handling normal response from getContractBogdetByIDM operation
            */
           public void receiveResultgetContractBogdetByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractBogdetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetByIDM operation
           */
            public void receiveErrorgetContractBogdetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeByIDM method
            * override this method for handling normal response from getAssetOperationTypeByIDM operation
            */
           public void receiveResultgetAssetOperationTypeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetOperationTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeByIDM operation
           */
            public void receiveErrorgetAssetOperationTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseByID method
            * override this method for handling normal response from getWarehouseByID operation
            */
           public void receiveResultgetWarehouseByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetWarehouseByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseByID operation
           */
            public void receiveErrorgetWarehouseByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodCount method
            * override this method for handling normal response from getAccountingPeriodCount operation
            */
           public void receiveResultgetAccountingPeriodCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAccountingPeriodCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodCount operation
           */
            public void receiveErrorgetAccountingPeriodCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationType method
            * override this method for handling normal response from getApplicationForReservationType operation
            */
           public void receiveResultgetApplicationForReservationType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetApplicationForReservationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationType operation
           */
            public void receiveErrorgetApplicationForReservationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeByIDM method
            * override this method for handling normal response from getEmployeeByIDM operation
            */
           public void receiveResultgetEmployeeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetEmployeeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeByIDM operation
           */
            public void receiveErrorgetEmployeeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderByID method
            * override this method for handling normal response from getProductionOrderByID operation
            */
           public void receiveResultgetProductionOrderByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProductionOrderByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderByID operation
           */
            public void receiveErrorgetProductionOrderByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodByID method
            * override this method for handling normal response from getAccountingPeriodByID operation
            */
           public void receiveResultgetAccountingPeriodByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAccountingPeriodByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodByID operation
           */
            public void receiveErrorgetAccountingPeriodByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonCount method
            * override this method for handling normal response from getPersonCount operation
            */
           public void receiveResultgetPersonCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonCount operation
           */
            public void receiveErrorgetPersonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindCount method
            * override this method for handling normal response from getPurchasePlanKindCount operation
            */
           public void receiveResultgetPurchasePlanKindCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindCount operation
           */
            public void receiveErrorgetPurchasePlanKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractType method
            * override this method for handling normal response from getContractType operation
            */
           public void receiveResultgetContractType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractType operation
           */
            public void receiveErrorgetContractType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationByIDM method
            * override this method for handling normal response from getDeliveryLocationByIDM operation
            */
           public void receiveResultgetDeliveryLocationByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryLocationByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationByIDM operation
           */
            public void receiveErrorgetDeliveryLocationByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriodByID method
            * override this method for handling normal response from getAssetAccountingPeriodByID operation
            */
           public void receiveResultgetAssetAccountingPeriodByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetAccountingPeriodByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriodByID operation
           */
            public void receiveErrorgetAssetAccountingPeriodByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeByID method
            * override this method for handling normal response from getEmployeeByID operation
            */
           public void receiveResultgetEmployeeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetEmployeeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeByID operation
           */
            public void receiveErrorgetEmployeeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonByIDM method
            * override this method for handling normal response from getPersonByIDM operation
            */
           public void receiveResultgetPersonByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonByIDM operation
           */
            public void receiveErrorgetPersonByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContract method
            * override this method for handling normal response from getContract operation
            */
           public void receiveResultgetContract(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContract operation
           */
            public void receiveErrorgetContract(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTask method
            * override this method for handling normal response from getFinancialTask operation
            */
           public void receiveResultgetFinancialTask(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialTaskResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTask operation
           */
            public void receiveErrorgetFinancialTask(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByIDM method
            * override this method for handling normal response from getUnitBudgetKoByIDM operation
            */
           public void receiveResultgetUnitBudgetKoByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByIDM operation
           */
            public void receiveErrorgetUnitBudgetKoByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentByIDM method
            * override this method for handling normal response from getFinancialDocumentByIDM operation
            */
           public void receiveResultgetFinancialDocumentByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialDocumentByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentByIDM operation
           */
            public void receiveErrorgetFinancialDocumentByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseCount method
            * override this method for handling normal response from getWarehouseCount operation
            */
           public void receiveResultgetWarehouseCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetWarehouseCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseCount operation
           */
            public void receiveErrorgetWarehouseCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeByIDM method
            * override this method for handling normal response from getPurchaseDocumentTypeByIDM operation
            */
           public void receiveResultgetPurchaseDocumentTypeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchaseDocumentTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeByIDM operation
           */
            public void receiveErrorgetPurchaseDocumentTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementByIDM method
            * override this method for handling normal response from getPersonAgreementByIDM operation
            */
           public void receiveResultgetPersonAgreementByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreementByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementByIDM operation
           */
            public void receiveErrorgetPersonAgreementByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateCount method
            * override this method for handling normal response from getVatRateCount operation
            */
           public void receiveResultgetVatRateCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetVatRateCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateCount operation
           */
            public void receiveErrorgetVatRateCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureCount method
            * override this method for handling normal response from getUnitOfMeasureCount operation
            */
           public void receiveResultgetUnitOfMeasureCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitOfMeasureCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureCount operation
           */
            public void receiveErrorgetUnitOfMeasureCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeByIDM method
            * override this method for handling normal response from getContractTypeByIDM operation
            */
           public void receiveResultgetContractTypeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeByIDM operation
           */
            public void receiveErrorgetContractTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateByID method
            * override this method for handling normal response from getVatRateByID operation
            */
           public void receiveResultgetVatRateByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetVatRateByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateByID operation
           */
            public void receiveErrorgetVatRateByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriod method
            * override this method for handling normal response from getAssetAccountingPeriod operation
            */
           public void receiveResultgetAssetAccountingPeriod(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetAccountingPeriodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriod operation
           */
            public void receiveErrorgetAssetAccountingPeriod(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationTypeByID method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationTypeByID operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationTypeByID operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementCount method
            * override this method for handling normal response from getPersonAgreementCount operation
            */
           public void receiveResultgetPersonAgreementCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreementCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementCount operation
           */
            public void receiveErrorgetPersonAgreementCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByIDM method
            * override this method for handling normal response from getApplicationForReservationTypeByIDM operation
            */
           public void receiveResultgetApplicationForReservationTypeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetApplicationForReservationTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByIDM operation
           */
            public void receiveErrorgetApplicationForReservationTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesCount method
            * override this method for handling normal response from getDeliveryDocumentTypesCount operation
            */
           public void receiveResultgetDeliveryDocumentTypesCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryDocumentTypesCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesCount operation
           */
            public void receiveErrorgetDeliveryDocumentTypesCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdet method
            * override this method for handling normal response from getContractBogdet operation
            */
           public void receiveResultgetContractBogdet(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractBogdetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdet operation
           */
            public void receiveErrorgetContractBogdet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountByID method
            * override this method for handling normal response from getSupplierBankAccountByID operation
            */
           public void receiveResultgetSupplierBankAccountByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierBankAccountByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountByID operation
           */
            public void receiveErrorgetSupplierBankAccountByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrency method
            * override this method for handling normal response from getCurrency operation
            */
           public void receiveResultgetCurrency(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCurrencyResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrency operation
           */
            public void receiveErrorgetCurrency(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemCount method
            * override this method for handling normal response from getProjectBudgetItemCount operation
            */
           public void receiveResultgetProjectBudgetItemCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBudgetItemCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemCount operation
           */
            public void receiveErrorgetProjectBudgetItemCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoCount method
            * override this method for handling normal response from getUnitBudgetKoCount operation
            */
           public void receiveResultgetUnitBudgetKoCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoCount operation
           */
            public void receiveErrorgetUnitBudgetKoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationCount method
            * override this method for handling normal response from getDeliveryLocationCount operation
            */
           public void receiveResultgetDeliveryLocationCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryLocationCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationCount operation
           */
            public void receiveErrorgetDeliveryLocationCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoByIDM method
            * override this method for handling normal response from getAssetCardInfoByIDM operation
            */
           public void receiveResultgetAssetCardInfoByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetCardInfoByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoByIDM operation
           */
            public void receiveErrorgetAssetCardInfoByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceByID method
            * override this method for handling normal response from getFundingSourceByID operation
            */
           public void receiveResultgetFundingSourceByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFundingSourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceByID operation
           */
            public void receiveErrorgetFundingSourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskByIDM method
            * override this method for handling normal response from getFinancialTaskByIDM operation
            */
           public void receiveResultgetFinancialTaskByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialTaskByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskByIDM operation
           */
            public void receiveErrorgetFinancialTaskByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationType method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationType operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationType operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindByID method
            * override this method for handling normal response from getUnitBudgetKoKindByID operation
            */
           public void receiveResultgetUnitBudgetKoKindByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindByID operation
           */
            public void receiveErrorgetUnitBudgetKoKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItem method
            * override this method for handling normal response from getProjectBudgetItem operation
            */
           public void receiveResultgetProjectBudgetItem(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBudgetItemResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItem operation
           */
            public void receiveErrorgetProjectBudgetItem(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView method
            * override this method for handling normal response from getBudgetView operation
            */
           public void receiveResultgetBudgetView(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetBudgetViewResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView operation
           */
            public void receiveErrorgetBudgetView(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationTypeCount method
            * override this method for handling normal response from getAssetOperationTypeCount operation
            */
           public void receiveResultgetAssetOperationTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetOperationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationTypeCount operation
           */
            public void receiveErrorgetAssetOperationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetCount method
            * override this method for handling normal response from getContractBogdetCount operation
            */
           public void receiveResultgetContractBogdetCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractBogdetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetCount operation
           */
            public void receiveErrorgetContractBogdetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderByID method
            * override this method for handling normal response from getSupplierOrderHeaderByID operation
            */
           public void receiveResultgetSupplierOrderHeaderByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierOrderHeaderByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderByID operation
           */
            public void receiveErrorgetSupplierOrderHeaderByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRoles method
            * override this method for handling normal response from getProjectRoles operation
            */
           public void receiveResultgetProjectRoles(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectRolesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRoles operation
           */
            public void receiveErrorgetProjectRoles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeader method
            * override this method for handling normal response from getSupplierOrderHeader operation
            */
           public void receiveResultgetSupplierOrderHeader(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierOrderHeaderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeader operation
           */
            public void receiveErrorgetSupplierOrderHeader(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentByID method
            * override this method for handling normal response from getDepartmentByID operation
            */
           public void receiveResultgetDepartmentByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDepartmentByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentByID operation
           */
            public void receiveErrorgetDepartmentByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryByID method
            * override this method for handling normal response from getCountryByID operation
            */
           public void receiveResultgetCountryByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCountryByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryByID operation
           */
            public void receiveErrorgetCountryByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserByIDM method
            * override this method for handling normal response from getErpUserByIDM operation
            */
           public void receiveResultgetErpUserByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetErpUserByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserByIDM operation
           */
            public void receiveErrorgetErpUserByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocationByID method
            * override this method for handling normal response from getDeliveryLocationByID operation
            */
           public void receiveResultgetDeliveryLocationByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryLocationByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocationByID operation
           */
            public void receiveErrorgetDeliveryLocationByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectCount method
            * override this method for handling normal response from getProjectCount operation
            */
           public void receiveResultgetProjectCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectCount operation
           */
            public void receiveErrorgetProjectCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetView2 method
            * override this method for handling normal response from getBudgetView2 operation
            */
           public void receiveResultgetBudgetView2(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetBudgetView2Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetView2 operation
           */
            public void receiveErrorgetBudgetView2(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesByID method
            * override this method for handling normal response from getProjectRolesByID operation
            */
           public void receiveResultgetProjectRolesByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectRolesByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesByID operation
           */
            public void receiveErrorgetProjectRolesByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountCount method
            * override this method for handling normal response from getSupplierBankAccountCount operation
            */
           public void receiveResultgetSupplierBankAccountCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierBankAccountCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountCount operation
           */
            public void receiveErrorgetSupplierBankAccountCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsCount method
            * override this method for handling normal response from getPaymentTermsCount operation
            */
           public void receiveResultgetPaymentTermsCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPaymentTermsCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsCount operation
           */
            public void receiveErrorgetPaymentTermsCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderByIDM method
            * override this method for handling normal response from getProductionOrderByIDM operation
            */
           public void receiveResultgetProductionOrderByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProductionOrderByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderByIDM operation
           */
            public void receiveErrorgetProductionOrderByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKindCount method
            * override this method for handling normal response from getUnitBudgetKoKindCount operation
            */
           public void receiveResultgetUnitBudgetKoKindCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoKindCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKindCount operation
           */
            public void receiveErrorgetUnitBudgetKoKindCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharing method
            * override this method for handling normal response from getPatternsOfCostSharing operation
            */
           public void receiveResultgetPatternsOfCostSharing(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPatternsOfCostSharingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharing operation
           */
            public void receiveErrorgetPatternsOfCostSharing(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountry method
            * override this method for handling normal response from getCountry operation
            */
           public void receiveResultgetCountry(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCountryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountry operation
           */
            public void receiveErrorgetCountry(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeCount method
            * override this method for handling normal response from getApplicationForReservationTypeCount operation
            */
           public void receiveResultgetApplicationForReservationTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetApplicationForReservationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeCount operation
           */
            public void receiveErrorgetApplicationForReservationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocumentCount method
            * override this method for handling normal response from getFinancialDocumentCount operation
            */
           public void receiveResultgetFinancialDocumentCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialDocumentCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocumentCount operation
           */
            public void receiveErrorgetFinancialDocumentCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialDocument method
            * override this method for handling normal response from getFinancialDocument operation
            */
           public void receiveResultgetFinancialDocument(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialDocument operation
           */
            public void receiveErrorgetFinancialDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFinancialTaskByID method
            * override this method for handling normal response from getFinancialTaskByID operation
            */
           public void receiveResultgetFinancialTaskByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFinancialTaskByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFinancialTaskByID operation
           */
            public void receiveErrorgetFinancialTaskByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRateByIDM method
            * override this method for handling normal response from getVatRateByIDM operation
            */
           public void receiveResultgetVatRateByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetVatRateByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRateByIDM operation
           */
            public void receiveErrorgetVatRateByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccountByIDM method
            * override this method for handling normal response from getSupplierBankAccountByIDM operation
            */
           public void receiveResultgetSupplierBankAccountByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierBankAccountByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccountByIDM operation
           */
            public void receiveErrorgetSupplierBankAccountByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudget method
            * override this method for handling normal response from getUnitBudget operation
            */
           public void receiveResultgetUnitBudget(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudget operation
           */
            public void receiveErrorgetUnitBudget(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemType method
            * override this method for handling normal response from getAssetRecordSystemType operation
            */
           public void receiveResultgetAssetRecordSystemType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemType operation
           */
            public void receiveErrorgetAssetRecordSystemType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierCount method
            * override this method for handling normal response from getSupplierCount operation
            */
           public void receiveResultgetSupplierCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierCount operation
           */
            public void receiveErrorgetSupplierCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetCount method
            * override this method for handling normal response from getProjectBugdetCount operation
            */
           public void receiveResultgetProjectBugdetCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBugdetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetCount operation
           */
            public void receiveErrorgetProjectBugdetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPerson method
            * override this method for handling normal response from getPerson operation
            */
           public void receiveResultgetPerson(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPerson operation
           */
            public void receiveErrorgetPerson(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharingByIDM method
            * override this method for handling normal response from getPatternsOfCostSharingByIDM operation
            */
           public void receiveResultgetPatternsOfCostSharingByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPatternsOfCostSharingByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharingByIDM operation
           */
            public void receiveErrorgetPatternsOfCostSharingByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderCount method
            * override this method for handling normal response from getSupplierOrderHeaderCount operation
            */
           public void receiveResultgetSupplierOrderHeaderCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierOrderHeaderCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderCount operation
           */
            public void receiveErrorgetSupplierOrderHeaderCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoByID method
            * override this method for handling normal response from getAssetCardInfoByID operation
            */
           public void receiveResultgetAssetCardInfoByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetCardInfoByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoByID operation
           */
            public void receiveErrorgetAssetCardInfoByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesInProjects method
            * override this method for handling normal response from getRolesInProjects operation
            */
           public void receiveResultgetRolesInProjects(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetRolesInProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesInProjects operation
           */
            public void receiveErrorgetRolesInProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreement method
            * override this method for handling normal response from getPersonAgreement operation
            */
           public void receiveResultgetPersonAgreement(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreementResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreement operation
           */
            public void receiveErrorgetPersonAgreement(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesCount method
            * override this method for handling normal response from getProjectRolesCount operation
            */
           public void receiveResultgetProjectRolesCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectRolesCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesCount operation
           */
            public void receiveErrorgetProjectRolesCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsByID method
            * override this method for handling normal response from getPaymentTermsByID operation
            */
           public void receiveResultgetPaymentTermsByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPaymentTermsByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsByID operation
           */
            public void receiveErrorgetPaymentTermsByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentByIDM method
            * override this method for handling normal response from getDepartmentByIDM operation
            */
           public void receiveResultgetDepartmentByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDepartmentByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentByIDM operation
           */
            public void receiveErrorgetDepartmentByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrderCount method
            * override this method for handling normal response from getProductionOrderCount operation
            */
           public void receiveResultgetProductionOrderCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProductionOrderCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrderCount operation
           */
            public void receiveErrorgetProductionOrderCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouseByIDM method
            * override this method for handling normal response from getWarehouseByIDM operation
            */
           public void receiveResultgetWarehouseByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetWarehouseByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouseByIDM operation
           */
            public void receiveErrorgetWarehouseByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierByID method
            * override this method for handling normal response from getSupplierByID operation
            */
           public void receiveResultgetSupplierByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierByID operation
           */
            public void receiveErrorgetSupplierByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getApplicationForReservationTypeByID method
            * override this method for handling normal response from getApplicationForReservationTypeByID operation
            */
           public void receiveResultgetApplicationForReservationTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetApplicationForReservationTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getApplicationForReservationTypeByID operation
           */
            public void receiveErrorgetApplicationForReservationTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeCount method
            * override this method for handling normal response from getContractTypeCount operation
            */
           public void receiveResultgetContractTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeCount operation
           */
            public void receiveErrorgetContractTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartment method
            * override this method for handling normal response from getDepartment operation
            */
           public void receiveResultgetDepartment(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDepartmentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartment operation
           */
            public void receiveErrorgetDepartment(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentType method
            * override this method for handling normal response from getPurchaseDocumentType operation
            */
           public void receiveResultgetPurchaseDocumentType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchaseDocumentTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentType operation
           */
            public void receiveErrorgetPurchaseDocumentType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanKindByID method
            * override this method for handling normal response from getPurchasePlanKindByID operation
            */
           public void receiveResultgetPurchasePlanKindByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanKindByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanKindByID operation
           */
            public void receiveErrorgetPurchasePlanKindByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractBogdetByID method
            * override this method for handling normal response from getContractBogdetByID operation
            */
           public void receiveResultgetContractBogdetByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractBogdetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractBogdetByID operation
           */
            public void receiveErrorgetContractBogdetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasure method
            * override this method for handling normal response from getUnitOfMeasure operation
            */
           public void receiveResultgetUnitOfMeasure(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitOfMeasureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasure operation
           */
            public void receiveErrorgetUnitOfMeasure(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractByID method
            * override this method for handling normal response from getContractByID operation
            */
           public void receiveResultgetContractByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractByID operation
           */
            public void receiveErrorgetContractByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFundingCount method
            * override this method for handling normal response from getSourcesOfProjectFundingCount operation
            */
           public void receiveResultgetSourcesOfProjectFundingCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSourcesOfProjectFundingCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFundingCount operation
           */
            public void receiveErrorgetSourcesOfProjectFundingCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnit method
            * override this method for handling normal response from getOrganizationalUnit operation
            */
           public void receiveResultgetOrganizationalUnit(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetOrganizationalUnitResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnit operation
           */
            public void receiveErrorgetOrganizationalUnit(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectRolesByIDM method
            * override this method for handling normal response from getProjectRolesByIDM operation
            */
           public void receiveResultgetProjectRolesByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectRolesByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectRolesByIDM operation
           */
            public void receiveErrorgetProjectRolesByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeCount method
            * override this method for handling normal response from getAssetRecordSystemTypeCount operation
            */
           public void receiveResultgetAssetRecordSystemTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeCount operation
           */
            public void receiveErrorgetAssetRecordSystemTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeByID method
            * override this method for handling normal response from getPurchaseDocumentTypeByID operation
            */
           public void receiveResultgetPurchaseDocumentTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchaseDocumentTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeByID operation
           */
            public void receiveErrorgetPurchaseDocumentTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdetByID method
            * override this method for handling normal response from getProjectBugdetByID operation
            */
           public void receiveResultgetProjectBugdetByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBugdetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdetByID operation
           */
            public void receiveErrorgetProjectBugdetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyCount method
            * override this method for handling normal response from getCurrencyCount operation
            */
           public void receiveResultgetCurrencyCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCurrencyCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyCount operation
           */
            public void receiveErrorgetCurrencyCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesByID method
            * override this method for handling normal response from getDeliveryDocumentTypesByID operation
            */
           public void receiveResultgetDeliveryDocumentTypesByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryDocumentTypesByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesByID operation
           */
            public void receiveErrorgetDeliveryDocumentTypesByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonByID method
            * override this method for handling normal response from getPersonByID operation
            */
           public void receiveResultgetPersonByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonByID operation
           */
            public void receiveErrorgetPersonByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriod method
            * override this method for handling normal response from getAccountingPeriod operation
            */
           public void receiveResultgetAccountingPeriod(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAccountingPeriodResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriod operation
           */
            public void receiveErrorgetAccountingPeriod(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonCount method
            * override this method for handling normal response from getAssetLiquidationReasonCount operation
            */
           public void receiveResultgetAssetLiquidationReasonCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetLiquidationReasonCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonCount operation
           */
            public void receiveErrorgetAssetLiquidationReasonCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetViewCount method
            * override this method for handling normal response from getBudgetViewCount operation
            */
           public void receiveResultgetBudgetViewCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetBudgetViewCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetViewCount operation
           */
            public void receiveErrorgetBudgetViewCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByID method
            * override this method for handling normal response from getPurchasePlanByID operation
            */
           public void receiveResultgetPurchasePlanByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByID operation
           */
            public void receiveErrorgetPurchasePlanByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetOperationType method
            * override this method for handling normal response from getAssetOperationType operation
            */
           public void receiveResultgetAssetOperationType(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetOperationTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetOperationType operation
           */
            public void receiveErrorgetAssetOperationType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierByIDM method
            * override this method for handling normal response from getSupplierByIDM operation
            */
           public void receiveResultgetSupplierByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierByIDM operation
           */
            public void receiveErrorgetSupplierByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitByID method
            * override this method for handling normal response from getOrganizationalUnitByID operation
            */
           public void receiveResultgetOrganizationalUnitByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetOrganizationalUnitByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitByID operation
           */
            public void receiveErrorgetOrganizationalUnitByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceByID method
            * override this method for handling normal response from getProjectBudgetItemResourceByID operation
            */
           public void receiveResultgetProjectBudgetItemResourceByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBudgetItemResourceByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceByID operation
           */
            public void receiveErrorgetProjectBudgetItemResourceByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKo method
            * override this method for handling normal response from getUnitBudgetKo operation
            */
           public void receiveResultgetUnitBudgetKo(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKo operation
           */
            public void receiveErrorgetUnitBudgetKo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDepartmentCount method
            * override this method for handling normal response from getDepartmentCount operation
            */
           public void receiveResultgetDepartmentCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDepartmentCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDepartmentCount operation
           */
            public void receiveErrorgetDepartmentCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfo method
            * override this method for handling normal response from getAssetCardInfo operation
            */
           public void receiveResultgetAssetCardInfo(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetCardInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfo operation
           */
            public void receiveErrorgetAssetCardInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypesByIDM method
            * override this method for handling normal response from getDeliveryDocumentTypesByIDM operation
            */
           public void receiveResultgetDeliveryDocumentTypesByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryDocumentTypesByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypesByIDM operation
           */
            public void receiveErrorgetDeliveryDocumentTypesByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEmployeeCount method
            * override this method for handling normal response from getEmployeeCount operation
            */
           public void receiveResultgetEmployeeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetEmployeeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEmployeeCount operation
           */
            public void receiveErrorgetEmployeeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryDocumentTypes method
            * override this method for handling normal response from getDeliveryDocumentTypes operation
            */
           public void receiveResultgetDeliveryDocumentTypes(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryDocumentTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryDocumentTypes operation
           */
            public void receiveErrorgetDeliveryDocumentTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProduct method
            * override this method for handling normal response from getCostInvoiceProduct operation
            */
           public void receiveResultgetCostInvoiceProduct(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCostInvoiceProductResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProduct operation
           */
            public void receiveErrorgetCostInvoiceProduct(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductCount method
            * override this method for handling normal response from getCostInvoiceProductCount operation
            */
           public void receiveResultgetCostInvoiceProductCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCostInvoiceProductCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductCount operation
           */
            public void receiveErrorgetCostInvoiceProductCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlan method
            * override this method for handling normal response from getPurchasePlan operation
            */
           public void receiveResultgetPurchasePlan(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlan operation
           */
            public void receiveErrorgetPurchasePlan(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyByIDM method
            * override this method for handling normal response from getCurrencyByIDM operation
            */
           public void receiveResultgetCurrencyByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCurrencyByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyByIDM operation
           */
            public void receiveErrorgetCurrencyByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getWarehouse method
            * override this method for handling normal response from getWarehouse operation
            */
           public void receiveResultgetWarehouse(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetWarehouseResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getWarehouse operation
           */
            public void receiveErrorgetWarehouse(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryByIDM method
            * override this method for handling normal response from getCountryByIDM operation
            */
           public void receiveResultgetCountryByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCountryByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryByIDM operation
           */
            public void receiveErrorgetCountryByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanCount method
            * override this method for handling normal response from getPurchasePlanCount operation
            */
           public void receiveResultgetPurchasePlanCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanCount operation
           */
            public void receiveErrorgetPurchasePlanCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonByID method
            * override this method for handling normal response from getAssetLiquidationReasonByID operation
            */
           public void receiveResultgetAssetLiquidationReasonByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetLiquidationReasonByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonByID operation
           */
            public void receiveErrorgetAssetLiquidationReasonByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetCardInfoCount method
            * override this method for handling normal response from getAssetCardInfoCount operation
            */
           public void receiveResultgetAssetCardInfoCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetCardInfoCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetCardInfoCount operation
           */
            public void receiveErrorgetAssetCardInfoCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetAccountingPeriodCount method
            * override this method for handling normal response from getAssetAccountingPeriodCount operation
            */
           public void receiveResultgetAssetAccountingPeriodCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetAccountingPeriodCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetAccountingPeriodCount operation
           */
            public void receiveErrorgetAssetAccountingPeriodCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountByIDM method
            * override this method for handling normal response from getCompanyBankAccountByIDM operation
            */
           public void receiveResultgetCompanyBankAccountByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCompanyBankAccountByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountByIDM operation
           */
            public void receiveErrorgetCompanyBankAccountByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourcesOfProjectFunding method
            * override this method for handling normal response from getSourcesOfProjectFunding operation
            */
           public void receiveResultgetSourcesOfProjectFunding(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSourcesOfProjectFundingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourcesOfProjectFunding operation
           */
            public void receiveErrorgetSourcesOfProjectFunding(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetLiquidationReasonByIDM method
            * override this method for handling normal response from getAssetLiquidationReasonByIDM operation
            */
           public void receiveResultgetAssetLiquidationReasonByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetLiquidationReasonByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetLiquidationReasonByIDM operation
           */
            public void receiveErrorgetAssetLiquidationReasonByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByIDM method
            * override this method for handling normal response from getUnitBudgetByIDM operation
            */
           public void receiveResultgetUnitBudgetByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByIDM operation
           */
            public void receiveErrorgetUnitBudgetByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getVatRate method
            * override this method for handling normal response from getVatRate operation
            */
           public void receiveResultgetVatRate(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetVatRateResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getVatRate operation
           */
            public void receiveErrorgetVatRate(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractByIDM method
            * override this method for handling normal response from getContractByIDM operation
            */
           public void receiveResultgetContractByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractByIDM operation
           */
            public void receiveErrorgetContractByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitOfMeasureByIDM method
            * override this method for handling normal response from getUnitOfMeasureByIDM operation
            */
           public void receiveResultgetUnitOfMeasureByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitOfMeasureByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitOfMeasureByIDM operation
           */
            public void receiveErrorgetUnitOfMeasureByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoKind method
            * override this method for handling normal response from getUnitBudgetKoKind operation
            */
           public void receiveResultgetUnitBudgetKoKind(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoKindResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoKind operation
           */
            public void receiveErrorgetUnitBudgetKoKind(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetKoByID method
            * override this method for handling normal response from getUnitBudgetKoByID operation
            */
           public void receiveResultgetUnitBudgetKoByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetKoByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetKoByID operation
           */
            public void receiveErrorgetUnitBudgetKoByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBugdet method
            * override this method for handling normal response from getProjectBugdet operation
            */
           public void receiveResultgetProjectBugdet(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBugdetResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBugdet operation
           */
            public void receiveErrorgetProjectBugdet(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeByID method
            * override this method for handling normal response from getAssetRecordSystemTypeByID operation
            */
           public void receiveResultgetAssetRecordSystemTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeByID operation
           */
            public void receiveErrorgetAssetRecordSystemTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPaymentTermsByIDM method
            * override this method for handling normal response from getPaymentTermsByIDM operation
            */
           public void receiveResultgetPaymentTermsByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPaymentTermsByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPaymentTermsByIDM operation
           */
            public void receiveErrorgetPaymentTermsByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserByID method
            * override this method for handling normal response from getErpUserByID operation
            */
           public void receiveResultgetErpUserByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetErpUserByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserByID operation
           */
            public void receiveErrorgetErpUserByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierOrderHeaderByIDM method
            * override this method for handling normal response from getSupplierOrderHeaderByIDM operation
            */
           public void receiveResultgetSupplierOrderHeaderByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierOrderHeaderByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierOrderHeaderByIDM operation
           */
            public void receiveErrorgetSupplierOrderHeaderByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypesForOperationTypeCount method
            * override this method for handling normal response from getAssetRecordSystemTypesForOperationTypeCount operation
            */
           public void receiveResultgetAssetRecordSystemTypesForOperationTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypesForOperationTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypesForOperationTypeCount operation
           */
            public void receiveErrorgetAssetRecordSystemTypesForOperationTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitByIDM method
            * override this method for handling normal response from getOrganizationalUnitByIDM operation
            */
           public void receiveResultgetOrganizationalUnitByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetOrganizationalUnitByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitByIDM operation
           */
            public void receiveErrorgetOrganizationalUnitByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProject method
            * override this method for handling normal response from getProject operation
            */
           public void receiveResultgetProject(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProject operation
           */
            public void receiveErrorgetProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharingCount method
            * override this method for handling normal response from getPatternsOfCostSharingCount operation
            */
           public void receiveResultgetPatternsOfCostSharingCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPatternsOfCostSharingCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharingCount operation
           */
            public void receiveErrorgetPatternsOfCostSharingCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByID method
            * override this method for handling normal response from getProjectByID operation
            */
           public void receiveResultgetProjectByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByID operation
           */
            public void receiveErrorgetProjectByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractCount method
            * override this method for handling normal response from getContractCount operation
            */
           public void receiveResultgetContractCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractCount operation
           */
            public void receiveErrorgetContractCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getData method
            * override this method for handling normal response from getData operation
            */
           public void receiveResultgetData(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDataResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getData operation
           */
            public void receiveErrorgetData(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResource method
            * override this method for handling normal response from getProjectBudgetItemResource operation
            */
           public void receiveResultgetProjectBudgetItemResource(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBudgetItemResourceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResource operation
           */
            public void receiveErrorgetProjectBudgetItemResource(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPatternsOfCostSharingByID method
            * override this method for handling normal response from getPatternsOfCostSharingByID operation
            */
           public void receiveResultgetPatternsOfCostSharingByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPatternsOfCostSharingByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPatternsOfCostSharingByID operation
           */
            public void receiveErrorgetPatternsOfCostSharingByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetByID method
            * override this method for handling normal response from getUnitBudgetByID operation
            */
           public void receiveResultgetUnitBudgetByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetByID operation
           */
            public void receiveErrorgetUnitBudgetByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchasePlanByIDM method
            * override this method for handling normal response from getPurchasePlanByIDM operation
            */
           public void receiveResultgetPurchasePlanByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchasePlanByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchasePlanByIDM operation
           */
            public void receiveErrorgetPurchasePlanByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPersonAgreementByID method
            * override this method for handling normal response from getPersonAgreementByID operation
            */
           public void receiveResultgetPersonAgreementByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPersonAgreementByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPersonAgreementByID operation
           */
            public void receiveErrorgetPersonAgreementByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getContractTypeByID method
            * override this method for handling normal response from getContractTypeByID operation
            */
           public void receiveResultgetContractTypeByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetContractTypeByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getContractTypeByID operation
           */
            public void receiveErrorgetContractTypeByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccount method
            * override this method for handling normal response from getCompanyBankAccount operation
            */
           public void receiveResultgetCompanyBankAccount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCompanyBankAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccount operation
           */
            public void receiveErrorgetCompanyBankAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCount method
            * override this method for handling normal response from getCount operation
            */
           public void receiveResultgetCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCount operation
           */
            public void receiveErrorgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCountryCount method
            * override this method for handling normal response from getCountryCount operation
            */
           public void receiveResultgetCountryCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCountryCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCountryCount operation
           */
            public void receiveErrorgetCountryCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProductionOrder method
            * override this method for handling normal response from getProductionOrder operation
            */
           public void receiveResultgetProductionOrder(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProductionOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProductionOrder operation
           */
            public void receiveErrorgetProductionOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCurrencyByID method
            * override this method for handling normal response from getCurrencyByID operation
            */
           public void receiveResultgetCurrencyByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCurrencyByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCurrencyByID operation
           */
            public void receiveErrorgetCurrencyByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplier method
            * override this method for handling normal response from getSupplier operation
            */
           public void receiveResultgetSupplier(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplier operation
           */
            public void receiveErrorgetSupplier(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getErpUserCount method
            * override this method for handling normal response from getErpUserCount operation
            */
           public void receiveResultgetErpUserCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetErpUserCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getErpUserCount operation
           */
            public void receiveErrorgetErpUserCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectBudgetItemResourceCount method
            * override this method for handling normal response from getProjectBudgetItemResourceCount operation
            */
           public void receiveResultgetProjectBudgetItemResourceCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectBudgetItemResourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectBudgetItemResourceCount operation
           */
            public void receiveErrorgetProjectBudgetItemResourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectByIDM method
            * override this method for handling normal response from getProjectByIDM operation
            */
           public void receiveResultgetProjectByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetProjectByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectByIDM operation
           */
            public void receiveErrorgetProjectByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCompanyBankAccountCount method
            * override this method for handling normal response from getCompanyBankAccountCount operation
            */
           public void receiveResultgetCompanyBankAccountCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCompanyBankAccountCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCompanyBankAccountCount operation
           */
            public void receiveErrorgetCompanyBankAccountCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceByIDM method
            * override this method for handling normal response from getFundingSourceByIDM operation
            */
           public void receiveResultgetFundingSourceByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFundingSourceByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceByIDM operation
           */
            public void receiveErrorgetFundingSourceByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSupplierBankAccount method
            * override this method for handling normal response from getSupplierBankAccount operation
            */
           public void receiveResultgetSupplierBankAccount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetSupplierBankAccountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSupplierBankAccount operation
           */
            public void receiveErrorgetSupplierBankAccount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccountingPeriodByIDM method
            * override this method for handling normal response from getAccountingPeriodByIDM operation
            */
           public void receiveResultgetAccountingPeriodByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAccountingPeriodByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccountingPeriodByIDM operation
           */
            public void receiveErrorgetAccountingPeriodByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAssetRecordSystemTypeByIDM method
            * override this method for handling normal response from getAssetRecordSystemTypeByIDM operation
            */
           public void receiveResultgetAssetRecordSystemTypeByIDM(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetAssetRecordSystemTypeByIDMResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAssetRecordSystemTypeByIDM operation
           */
            public void receiveErrorgetAssetRecordSystemTypeByIDM(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUnitBudgetCount method
            * override this method for handling normal response from getUnitBudgetCount operation
            */
           public void receiveResultgetUnitBudgetCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetUnitBudgetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUnitBudgetCount operation
           */
            public void receiveErrorgetUnitBudgetCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentTypeCount method
            * override this method for handling normal response from getPurchaseDocumentTypeCount operation
            */
           public void receiveResultgetPurchaseDocumentTypeCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetPurchaseDocumentTypeCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentTypeCount operation
           */
            public void receiveErrorgetPurchaseDocumentTypeCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDeliveryLocation method
            * override this method for handling normal response from getDeliveryLocation operation
            */
           public void receiveResultgetDeliveryLocation(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetDeliveryLocationResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryLocation operation
           */
            public void receiveErrorgetDeliveryLocation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFundingSourceCount method
            * override this method for handling normal response from getFundingSourceCount operation
            */
           public void receiveResultgetFundingSourceCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetFundingSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFundingSourceCount operation
           */
            public void receiveErrorgetFundingSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductByID method
            * override this method for handling normal response from getCostInvoiceProductByID operation
            */
           public void receiveResultgetCostInvoiceProductByID(
                    pl.compan.docusafe.ws.imgw.DictionaryServiceStub.GetCostInvoiceProductByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductByID operation
           */
            public void receiveErrorgetCostInvoiceProductByID(java.lang.Exception e) {
            }
                


    }
    