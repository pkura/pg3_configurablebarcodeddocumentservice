
/**
 * DictionaryServicesOrganizationalUnitCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServicesOrganizationalUnitCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesOrganizationalUnitCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesOrganizationalUnitCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesOrganizationalUnitCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getOrganisationalUnits method
            * override this method for handling normal response from getOrganisationalUnits operation
            */
           public void receiveResultgetOrganisationalUnits(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganisationalUnitsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganisationalUnits operation
           */
            public void receiveErrorgetOrganisationalUnits(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitsCount method
            * override this method for handling normal response from getOrganizationalUnitsCount operation
            */
           public void receiveResultgetOrganizationalUnitsCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganizationalUnitsCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitsCount operation
           */
            public void receiveErrorgetOrganizationalUnitsCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrganizationalUnitByIdn method
            * override this method for handling normal response from getOrganizationalUnitByIdn operation
            */
           public void receiveResultgetOrganizationalUnitByIdn(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesOrganizationalUnitStub.GetOrganizationalUnitByIdnResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrganizationalUnitByIdn operation
           */
            public void receiveErrorgetOrganizationalUnitByIdn(java.lang.Exception e) {
            }
                


    }
    