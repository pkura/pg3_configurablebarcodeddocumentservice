
/**
 * DictionaryServicesCostInvoiceProductCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServicesCostInvoiceProductCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesCostInvoiceProductCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesCostInvoiceProductCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesCostInvoiceProductCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductCount method
            * override this method for handling normal response from getCostInvoiceProductCount operation
            */
           public void receiveResultgetCostInvoiceProductCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesCostInvoiceProductStub.GetCostInvoiceProductCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductCount operation
           */
            public void receiveErrorgetCostInvoiceProductCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProductByIdn method
            * override this method for handling normal response from getCostInvoiceProductByIdn operation
            */
           public void receiveResultgetCostInvoiceProductByIdn(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesCostInvoiceProductStub.GetCostInvoiceProductByIdnResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProductByIdn operation
           */
            public void receiveErrorgetCostInvoiceProductByIdn(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCostInvoiceProducts method
            * override this method for handling normal response from getCostInvoiceProducts operation
            */
           public void receiveResultgetCostInvoiceProducts(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesCostInvoiceProductStub.GetCostInvoiceProductsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCostInvoiceProducts operation
           */
            public void receiveErrorgetCostInvoiceProducts(java.lang.Exception e) {
            }
                


    }
    