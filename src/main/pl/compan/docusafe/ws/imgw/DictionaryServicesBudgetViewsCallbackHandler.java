
/**
 * DictionaryServicesBudgetViewsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.imgw;

    /**
     *  DictionaryServicesBudgetViewsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesBudgetViewsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesBudgetViewsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesBudgetViewsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getBudgets method
            * override this method for handling normal response from getBudgets operation
            */
           public void receiveResultgetBudgets(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetBudgetsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgets operation
           */
            public void receiveErrorgetBudgets(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSources method
            * override this method for handling normal response from getSources operation
            */
           public void receiveResultgetSources(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetSourcesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSources operation
           */
            public void receiveErrorgetSources(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSourceCount method
            * override this method for handling normal response from getSourceCount operation
            */
           public void receiveResultgetSourceCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetSourceCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSourceCount operation
           */
            public void receiveErrorgetSourceCount(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getBudgetCount method
            * override this method for handling normal response from getBudgetCount operation
            */
           public void receiveResultgetBudgetCount(
                    pl.compan.docusafe.ws.imgw.DictionaryServicesBudgetViewsStub.GetBudgetCountResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getBudgetCount operation
           */
            public void receiveErrorgetBudgetCount(java.lang.Exception e) {
            }
                


    }
    