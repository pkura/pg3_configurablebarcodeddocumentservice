
/**
 * IcDataProcessorCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.4  Built on : Dec 19, 2010 (08:18:42 CET)
 */

    package pl.compan.docusafe.ws.ic;

    /**
     *  IcDataProcessorCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class IcDataProcessorCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public IcDataProcessorCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public IcDataProcessorCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for dajDokZkInfo method
            * override this method for handling normal response from dajDokZkInfo operation
            */
           public void receiveResultdajDokZkInfo(
                    pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajDokZkInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from dajDokZkInfo operation
           */
            public void receiveErrordajDokZkInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for importDokZk method
            * override this method for handling normal response from importDokZk operation
            */
           public void receiveResultimportDokZk(
                    pl.compan.docusafe.ws.ic.IcDataProcessorStub.ImportDokZkResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from importDokZk operation
           */
            public void receiveErrorimportDokZk(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for dajKhInfo method
            * override this method for handling normal response from dajKhInfo operation
            */
           public void receiveResultdajKhInfo(
                    pl.compan.docusafe.ws.ic.IcDataProcessorStub.DajKhInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from dajKhInfo operation
           */
            public void receiveErrordajKhInfo(java.lang.Exception e) {
            }
                


    }
    