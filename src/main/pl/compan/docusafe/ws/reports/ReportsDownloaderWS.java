package pl.compan.docusafe.ws.reports;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.dom4j.io.SAXReader;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.reports.Report;
import pl.compan.docusafe.reports.ReportEnvironment;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class ReportsDownloaderWS 
{

	private static final Logger log = LoggerFactory.getLogger(ReportsDownloaderWS .class);
	
	private static final String OK = "SUCCESS";
	private static final String NOT_FOUND = "REPORT NOT FOUND";
	private static final String ERROR = "ERROR";
	
	protected static ReportEnvironment reportEnvironment = new ReportEnvironment();
	
	public ReportDownloadResponse DownloadReport(ReportDownloadRequest request)
	{
		
		ReportDownloadResponse response = new ReportDownloadResponse();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try
		{
			DSApi.openAdmin();
			ps = DSApi.context().prepareStatement("select * from ds_new_report where remote_key=? and status=? order by process_date desc");
			ps.setString(1, request.getKey());
			ps.setInt(2, Report.DONE_REPORT_STATUS);
			
			rs = ps.executeQuery();
			if(rs.next())
			{
				File file = null;
        		Blob blob = rs.getBlob("criteria");
                if (blob != null)
                {
                    InputStream is = blob.getBinaryStream();
                    file = File.createTempFile("docusafe_report_", ".xml");
                    OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
                    byte[] buffer = new byte[1024];
                    int count;
                    while ((count = is.read(buffer)) > 0)
                    {
                        os.write(buffer, 0, count);
                    }
                    org.apache.commons.io.IOUtils.closeQuietly(is);
                    org.apache.commons.io.IOUtils.closeQuietly(os);
                }   			
    			SAXReader sax = new SAXReader();
    			Report r;
    			
    			r = reportEnvironment.getReportInstanceByCn(rs.getString("report_cn"));
    			r.setId(rs.getLong("id"));
    			r.init(sax.read(file),rs.getString("report_cn"));
    			//System.out.println(r.getId());
    			File[] files = reportEnvironment.getResultsDirForReport(r).listFiles();
    			//System.out.println(files.length);
    			if(files.length>0)
    			{
    				//byte[] bytes = FileUtils.readFileToByteArray(files[0]);
    				String content = Base64.encodeBase64String(FileUtils.readFileToByteArray(files[0]));
    				response.setReportBase64Content(content);
    				Calendar cal = Calendar.getInstance();
    				cal.setTime(rs.getDate("process_date"));
    				response.setReportDate(cal);
    				response.setReportTitle(rs.getString("title"));
    				response.setStatus(OK);
    			}
    			else
    			{
    				response.setStatus(ERROR);
    				response.setStatusDescription("Nie znaleziono plikow wynikowych raportu");
    			}

				
			}
			else
			{
				response.setStatus(NOT_FOUND);
			}
			rs.close();
			
			
			
			
			
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			response.setStatus(ERROR);
			response.setStatusDescription(e.getMessage());
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			DSApi._close();	
		}
		return response;
	}
	
}