package pl.compan.docusafe.ws.reports;


public class CentrumSummaryResponse 
{
	private Integer centrumId;
	private String centrumName;
	private String status;
	private String statusDescription;
	/**
	 * lista beanow dla centrum kosztowego. Moze byc ich wiele ze wzgledu na mozliwosc wsytepowania faktu w wielu walutach.
	 */
	CentrumEntry[] kwoty;
	
	public CentrumSummaryResponse()
	{
	}

	public Integer getCentrumId() {
		return centrumId;
	}
	public void setCentrumId(Integer centrumId) {
		this.centrumId = centrumId;
	}
	public String getCentrumName() {
		return centrumName;
	}
	public void setCentrumName(String centrumName) {
		this.centrumName = centrumName;
	}
	
	public CentrumEntry[] getKwoty() {
		return kwoty;
	}

	public void setKwoty(CentrumEntry[] kwoty) {
		this.kwoty = kwoty;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}

	/*public void addKwota(String waluta, String kwotaZaakc, String kwotaNiezaakc)
	{
		CentrumEntry e = new CentrumEntry();
		e.setWaluta(waluta);
		e.setSumaNiezaakceptowanych(kwotaNiezaakc);
		e.setSumaZaakceptowanych(kwotaZaakc);
		kwoty.add(e);
	}*/
}
