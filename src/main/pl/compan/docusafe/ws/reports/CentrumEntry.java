package pl.compan.docusafe.ws.reports;

public class CentrumEntry
{
	private String waluta;
	private String sumaZaakceptowanych;
	private String sumaNiezaakceptowanych;
	
	public String getWaluta() {
		return waluta;
	}
	public void setWaluta(String waluta) {
		this.waluta = waluta;
	}
	public String getSumaZaakceptowanych() {
		return sumaZaakceptowanych;
	}
	public void setSumaZaakceptowanych(String sumaZaakceptowanych) {
		this.sumaZaakceptowanych = sumaZaakceptowanych;
	}
	public String getSumaNiezaakceptowanych() {
		return sumaNiezaakceptowanych;
	}
	public void setSumaNiezaakceptowanych(String sumaNiezaakceptowanych) {
		this.sumaNiezaakceptowanych = sumaNiezaakceptowanych;
	}
	
	
}