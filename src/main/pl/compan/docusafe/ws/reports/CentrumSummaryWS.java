package pl.compan.docusafe.ws.reports;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class CentrumSummaryWS 
{
	private static final Logger log = LoggerFactory.getLogger(CentrumSummaryWS.class);
	
	private static final String OK = "SUCCESS";
	private static final String NOT_FOUND = "REPORT NOT FOUND";
	private static final String ERROR = "ERROR";
	
	private String sql;
	
	public CentrumSummaryResponse GetCentrumSummary(CentrumSummaryRequest request)
	{
		CentrumSummaryResponse response = new CentrumSummaryResponse();
	
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try
		{
			DSApi.openAdmin();
			NumberFormat nf = NumberFormat.getInstance();
			nf.setMaximumFractionDigits(2);
			response.setCentrumId(request.getCentrumId());
			response.setCentrumName(request.getCentrumName());
			if(request.getCentrumId()!=null)
			{
				sql = "SELECT centrum_id, centrum_name, waluta, kwota_zaakceptowane, kwota_niezaakceptowane FROM dsg_ic_centra_report WHERE centrum_id=?";
				ps = DSApi.context().prepareStatement(sql);
				ps.setInt(1, request.getCentrumId());
			}
			else if(request.getCentrumName()!=null)
			{
				sql = "SELECT centrum_id, centrum_name, waluta, kwota_zaakceptowane, kwota_niezaakceptowane FROM dsg_ic_centra_report WHERE centrum_name=?";
				ps = DSApi.context().prepareStatement(sql);
				ps.setString(1, request.getCentrumName());
			}
			else
			{
				throw new Exception("Brak identyfikatora centrum");
			}
			
			rs = ps.executeQuery();
			boolean added = false;
			List<CentrumEntry> lista = new ArrayList<CentrumEntry>();
			
			while(rs.next())
			{
				response.setCentrumName(rs.getString("centrum_name"));
				added = true;
				CentrumEntry e = new CentrumEntry();
				e.setWaluta(rs.getString("waluta"));
				e.setSumaNiezaakceptowanych(nf.format(rs.getDouble("kwota_niezaakceptowane")));
				e.setSumaZaakceptowanych(nf.format(rs.getDouble("kwota_zaakceptowane")));
				lista.add(e);
			}
			rs.close();
			response.setKwoty(lista.toArray(new CentrumEntry[lista.size()]));
			
			if(added)
			{
				response.setStatus(OK);
			}
			else
			{
				response.setStatus(NOT_FOUND);
			}
		}
		catch (Exception e) 
		{
			log.error(e.getMessage(),e);
			response.setStatus(ERROR);
			response.setStatusDescription(e.getMessage());
		}
		finally
		{
			DSApi.context().closeStatement(ps);
			DSApi._close();	
		}
		return response;
	}
}
