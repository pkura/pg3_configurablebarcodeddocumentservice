package pl.compan.docusafe.ws.reports;

public class ReportDownloadRequest 
{
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
