package pl.compan.docusafe.ws.reports;

public class CentrumSummaryRequest 
{
	/**
	 * id centrum kosztowego dla ktorego maja byc zwrocone wyniki
	 */
	Integer centrumId;
	
	/**
	 * nazwa centrum kosztowego dla ktorego maja byc zwrocone wyniki. Lepiej podawac id niz nazwe.
	 */
	String centrumName;
	
	public Integer getCentrumId() {
		return centrumId;
	}
	public void setCentrumId(Integer centrumId) {
		this.centrumId = centrumId;
	}
	public String getCentrumName() {
		return centrumName;
	}
	public void setCentrumName(String centrumName) {
		this.centrumName = centrumName;
	}
	
	
}
