package pl.compan.docusafe.ws.reports;

import java.util.Calendar;
import java.util.Date;

public class ReportDownloadResponse 
{

	private String status;
	private String statusDescription;
	private String reportTitle;
	private Calendar reportDate;
	private String reportBase64Content;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusDescription() {
		return statusDescription;
	}
	public void setStatusDescription(String statusDescription) {
		this.statusDescription = statusDescription;
	}
	public String getReportTitle() {
		return reportTitle;
	}
	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
	public Calendar getReportDate() {
		return reportDate;
	}
	public void setReportDate(Calendar reportDate) {
		this.reportDate = reportDate;
	}
	public String getReportBase64Content() {
		return reportBase64Content;
	}
	public void setReportBase64Content(String reportBase64Content) {
		this.reportBase64Content = reportBase64Content;
	}
	
	
}
