/**
 * 
 */
package pl.compan.docusafe.ws.im;

import java.util.Calendar;
import java.security.*;
import java.io.*;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.service.imports.dsi.DSIImportService;
import pl.compan.docusafe.service.imports.dsi.DSIImportService.ImportDocument;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import sun.plugin.javascript.navig.ImageArray;

/**
 * @author stachera_j
 * 
 *  DocuSafe Upload Service
 *
 */
public class DocuSafeUploadService 
{	
	private static final Logger log = LoggerFactory.getLogger(DocuSafeUploadService.class);
	
	// authorization code parameters
	private static final String salt = Docusafe.getAdditionProperty("importDocument.authCodeSalt");
	private static final String digestAlg = Docusafe.getAdditionProperty("importDocument.digestAlg");
	// file attachement
	private static final String filePath = Docusafe.getAdditionProperty("importDocument.filePath");
	
	/**
	 * Validate document upload arguments 
	 * @param request - document upload request
	 * @return status
	 * @throws EdmException 
	 */
	private UploadDocumentArgsValidationStatus validateArgs(UploadDocumentRequest request) throws EdmException {
		int authCodeLen = DocuSafeUploadServiceUtils.getDigestLenght(digestAlg);
		//
		if ((request.getAuthCode() == null) || ((authCodeLen > 0) && (request.getAuthCode().length != authCodeLen))) {
			String authCode = (request.getAuthCode() != null) ? 
					DocuSafeUploadServiceUtils.byteArrayToHexString(request.getAuthCode()): "[none]";
			log.debug("[WS/Upload document/ValidateArgs] Invalid authCode: " + authCode);
			return UploadDocumentArgsValidationStatus.InvalidAuthCode;
		}
		if (request.getImportId() <= 0) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid importID: " + request.getImportId());
			return UploadDocumentArgsValidationStatus.InvalidDocumentID;
		}
		if (request.getBoxNumber().length() == 0) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid boxNumber: " + request.getBoxNumber());
			return UploadDocumentArgsValidationStatus.InvalidBoxNumber;
		}
		if ((request.getImageArray() != null) && (request.getImageName().length() == 0)) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid image array name [imageName]");
			return UploadDocumentArgsValidationStatus.InvalidImageArrayName;
		}
		if ((request.getImageArray() == null) && (request.getImageFullName().length() == 0)) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid image file name [imageFullName]");
			return UploadDocumentArgsValidationStatus.InvalidImageFileName;
		}
		if (request.getImportKind() != 1) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid importKind: " + request.getImportKind());
			return UploadDocumentArgsValidationStatus.InvalidImportKind;
		}
		// check document kind
		if (DocumentKind.getAvailableDocumentKindsCn().contains(request.getDocumentKind()) == false) {
			log.debug("[WS/Upload document/ValidateArgs] Invalid documentKind: " + request.getDocumentKind());
			return UploadDocumentArgsValidationStatus.InvalidDocumentKind;
		}
		// check if image file exist
		if ((request.getImageArray() == null) && (request.getImageFullName().length() > 0)) {
			String realPath = filePath + "/" + request.getImageFullName();
			File f = new File(realPath);
			if (f.exists() == false) {
				log.debug("[WS/Upload document/ValidateArgs] Image file not exist for file path: " + 
						request.getImageFullName());
				return UploadDocumentArgsValidationStatus.ImageFileNotExist;
			}
		}
		// success, i guess
		return UploadDocumentArgsValidationStatus.Success;
	}
	
	/**
	 * Authorize 
	 * @param request
	 * @return success/failure
	 * @throws NoSuchAlgorithmException 
	 */
	private boolean authorize(UploadDocumentRequest request) throws NoSuchAlgorithmException {
		boolean authorized = true;
		// compute auth code
		byte[] baseAuthCode = DocuSafeUploadServiceUtils.generateDigest(request, salt, digestAlg);
		// request auth code
		byte[] requestAuthCode = request.getAuthCode();
		// compare auth code
		if (baseAuthCode.length != requestAuthCode.length) {
			log.debug("[WS/Upload document/authorize] Different authorization code lenghts where " + 
					"base: " + (8*baseAuthCode.length) + "b, " + 
					"request: " + (8*requestAuthCode.length) + "b");
			return false;
		}
		for (int i = 0; i < baseAuthCode.length; i++) {
			if (baseAuthCode[i] != requestAuthCode[i]) {
				authorized = false;
				break;
			}
		}
		//
		return authorized;
	}
	
	/**
	 * Upload document to DocuSafe
	 */ 
	public UploadDocumentResponse UploadDocument(UploadDocumentRequest request) 
	{
		if (log.isTraceEnabled())
		{
			log.trace("Zamienic obrazek na info o wielkosci obrazka");
			log.trace("[WS/Upload document] started with params: " + request.toString());
			
		}
		//
		UploadDocumentResponse response = null;
		//
		// VALIDATE PARAMS
		UploadDocumentArgsValidationStatus argsValidationStatusCode = UploadDocumentArgsValidationStatus.Success;
		try {
			argsValidationStatusCode = validateArgs(request);
		}
		catch (Exception ex) {
			response = UploadDocumentResponse.CreateErrorResponse(ex);
			log.error("[WS/Upload document] Could not validate ws arguments", ex);
			return response;
		}
		//
		if (argsValidationStatusCode != UploadDocumentArgsValidationStatus.Success) {
			response = UploadDocumentResponse.CreateInvalidArgsResponse(argsValidationStatusCode);
			log.debug("[WS/Upload document] Invalid args: " + argsValidationStatusCode);
			return response;
		}
		else {
			log.debug("[WS/Upload document] Arguments Ok...");
		}
		//
		// AUTHORIZE
		boolean authorized = true;
		try {
			authorized = authorize(request);
			if (authorized == false) {
				log.debug("[WS/Upload document] Authorization failed");
				response = UploadDocumentResponse.CreateStatusResponse(UploadDocumentStatus.INVALID_AUTH_CODE);
			}
			else {
				log.debug("[WS/Upload document] Authorization Ok...");
			}
		} 
		catch (NoSuchAlgorithmException e) {
			authorized = false;
			log.error("[WS/Upload document] Authorization method error", e);
			response = UploadDocumentResponse.CreateErrorResponse(e);
		}
		// authorization was invalid or there was an error 
		if (response != null) {
			return response;
		}
		// DO IMPORT
		DSIImportService importService = new DSIImportService();
		ImportDocument importDocument = importService.new ImportDocument();
		response = importDocument.importFromWebServices(request);
		//
		return response;
	}
	
	/**
	 * Test method
	 * @param msg - some message
	 */
	public String Test(byte[] authCode, String msg) {
		log.debug("Test WS started with params - authCode: " + 
				DocuSafeUploadServiceUtils.byteArrayToHexString(authCode) +
				", msg: " + msg);
		//
		boolean authorized = true;
		try {
			// compute auth code
			byte[] baseAuthCode = DocuSafeUploadServiceUtils.generateTestDigest(msg, salt, digestAlg);
			// request auth code
			byte[] requestAuthCode = authCode;
			// compare auth code
			if (baseAuthCode.length == requestAuthCode.length) {
				for (int i = 0; i < baseAuthCode.length; i++) {
					if (baseAuthCode[i] != requestAuthCode[i]) {
						authorized = false;
						break;
					}
				}
			}
			else {
				authorized = false;
			}
		} 
		catch (NoSuchAlgorithmException e) {
			log.error("Digest algorithm is not available: " + digestAlg, e);
			authorized = false;
		}
		//
		if (authorized) {
			String time = Calendar.getInstance().getTime().toString();
			return "[DocuSafeUploadService/Test] " + msg + ", " + time;
		}
		else {
			return "[DocuSafeUploadService/Test] Authorization failed !";
		}
	}
}