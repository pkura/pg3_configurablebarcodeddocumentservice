/**
 * 
 */
package pl.compan.docusafe.ws.im;

import org.apache.commons.logging.LogFactory;

/**
 * @author stachera_j
 * 
 * 	Document descriptor
 */
public class Descriptor {
	private int setId;
	private int setName;
	private String descriptorName;
	private String descriptorValue;
	
	public String getDescriptorValue() {
		return descriptorValue;
	}
	public void setDescriptorValue(String descriptorValue) {
		this.descriptorValue = descriptorValue;
	}
	
	public int getSetId() {
		return setId;
	}
	public void setSetId(int setId) {
		this.setId = setId;
	}
	
	public int getSetName() {
		return setName;
	}
	public void setSetName(int setName) {
		this.setName = setName;
	}
	
	public String getDescriptorName() {
		return descriptorName;
	}
	public void setDescriptorName(String descriptorName) {
		this.descriptorName = descriptorName;
	}
	
	@Override
	public String toString() {
		String str =
			"setId: " + setId + 
			", setName: " + setName + 
			", descName: " + descriptorName + 
			", descValue: " + descriptorValue;
		//
		return str;
	}
}
