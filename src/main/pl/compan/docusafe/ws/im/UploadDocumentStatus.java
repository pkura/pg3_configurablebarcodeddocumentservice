/**
 * 
 */
package pl.compan.docusafe.ws.im;

/**
 * @author stachera_j
 *	
 *	Upload document status
 */
public enum UploadDocumentStatus {
	Success,			// ok
	InvalidArgs,		// niepoprawne argumenty
	INVALID_AUTH_CODE,	// niepoprawny kod autoryzacji
	InternalError,		// blad wewnetrzny
	AttributeNotPresent,// brak atrybutu
	AttributeNotParsed,	// atrybut nie mogl zostac sparsowany
	BoxNotPresent		// brak pudla
}
