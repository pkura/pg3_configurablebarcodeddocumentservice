/**
 * 
 */
package pl.compan.docusafe.ws.im;

import java.util.Calendar;
import java.util.Date;

import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.service.imports.dsi.DSIDefinitions;
import pl.compan.docusafe.util.StringManager;


/**
 * @author stachera_j
 *
 *	Upload document response
 */
public class UploadDocumentResponse {
	final private static StringManager strMgr = GlobalPreferences.loadPropertiesFile("", null);
	final private static String methodStatusConfigPrefix = "im.ws.upload.methodstatus.";
	final private static String argsValidStatusConfigPrefix = "im.ws.upload.validargsstatus.";
	final private static String newLine = System.getProperty("line.separator"); 
	
	//
	private long dsDocumentID;
	/**
	 * document status and description
	 */
	private UploadDocumentStatus statusCode;
	private String statusDesc;
	/**
	 * document arguments validation status and description
	 */
	private UploadDocumentArgsValidationStatus argsValidationStatusCode;
	private String argsValidationStatusDesc;
	/**
	 * invalid argument name/value
	 * TODO: currently not used
	 */
	private String attributeName;
	private String attributeValue;
	
	/**
	 *  docusafe archive date
	 */
	private Calendar archiveDate;
	/**
	 * document URL
	 */
	private String documentURL;

	public UploadDocumentResponse()
	{
		super();
		this.dsDocumentID = -1;
		setStatusCode(UploadDocumentStatus.Success);
		setArgsValidationStatusCode(UploadDocumentArgsValidationStatus.Success);
		setAttributeName("");
		setAttributeValue("");
		Calendar cal = Calendar.getInstance();
		cal.setTime(DocuSafeUploadServiceUtils.GetEmptyDate());
		setArchiveDate(cal);
		setDocumentURL("");
	}
	
	/**
	 * Create error response
	 * @param ex
	 * @return
	 */
	public static UploadDocumentResponse CreateErrorResponse(Exception ex) {
		UploadDocumentResponse response = new UploadDocumentResponse();
		response.assignError(ex);
		return response;
	}
	
	/**
	 * Create invalid arguments response
	 * @param argsValidationStatusCode
	 * @return
	 */
	public static UploadDocumentResponse CreateInvalidArgsResponse(UploadDocumentArgsValidationStatus argsValidationStatusCode) {
		UploadDocumentResponse response = new UploadDocumentResponse();
		response.assignInvalidArgsResponse(argsValidationStatusCode);
		return response;
	} 
	
	/**
	 * Create status response
	 * @param ex
	 * @return
	 */
	public static UploadDocumentResponse CreateStatusResponse(UploadDocumentStatus statusCode) {
		UploadDocumentResponse response = new UploadDocumentResponse();
		response.setStatusCode(statusCode);
		return response;
	}
	
	/**
	 * Create DSI - DocuSafe Import response
	 * @param dsiDefConst
	 * @return
	 */
	public static UploadDocumentResponse CreateDSIResponse(String dsiDefConst) {
		UploadDocumentResponse response = new UploadDocumentResponse();
		response.assignDSIResponse(dsiDefConst);
		return response;
	}
	
	public void assignError(Exception ex) {
		setStatusCode(UploadDocumentStatus.InternalError);
		appendStatusDesc(DocuSafeUploadServiceUtils.getStackTrace(ex));
	}
	
	
	public void assignDSIResponse(String dsiDefConst) {
		// Brak zalacznika
		if (dsiDefConst.compareTo(DSIDefinitions.ERR_001_MISSING_ATTACHMENT) == 0) {
			this.assignInvalidArgsResponse(UploadDocumentArgsValidationStatus.ImageFileNotExist);
		}
		// Niewlasciwe pudlo
		else if (dsiDefConst.compareTo(DSIDefinitions.ERR_002_MISSING_BOX) == 0) {
			this.assignInvalidArgsResponse(UploadDocumentArgsValidationStatus.BoxNotExist);
		} 
		// Brak atrybutow
		else if (dsiDefConst.compareTo(DSIDefinitions.ERR_003_MISSING_ATTRIBUTES) == 0) {
			this.assignInvalidArgsResponse(UploadDocumentArgsValidationStatus.DescriptorsNotExist);
		}
		// Brak pola - nie ma pola wymaganego
		else if (dsiDefConst.compareTo(DSIDefinitions.ERR_010_FIELD_MISSING) == 0) {
			this.setStatusCode(UploadDocumentStatus.AttributeNotPresent);
		}
		// Zly format daty w polu
		else if (dsiDefConst.compareTo(DSIDefinitions.ERR_020_DATE_FORMAT_IS_BAD) == 0) {
			this.setStatusCode(UploadDocumentStatus.AttributeNotParsed);
		}
		// Zly format kwoty w polu
		else if (dsiDefConst.compareTo(DSIDefinitions.ERR_021_MONEY_FORMAT_IS_BAD) == 0) {
			this.setStatusCode(UploadDocumentStatus.AttributeNotParsed);
		} 		
		// default
		else {
			this.setStatusCode(UploadDocumentStatus.InternalError);
			this.appendStatusDesc(dsiDefConst);
		}
	}
	
	public void assignInvalidArgsResponse(UploadDocumentArgsValidationStatus argsValidationStatusCode) {
		setStatusCode(UploadDocumentStatus.InvalidArgs);
		setArgsValidationStatusCode(argsValidationStatusCode);
	}
	
	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
	
	public long getDsDocumentID() {
		return dsDocumentID;
	}

	public void setDsDocumentID(long dsDocumentID) {
		this.dsDocumentID = dsDocumentID;
	}

	
	public String getStatusDesc() {
		return statusDesc;
	}
	

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	
	public void appendStatusDesc(String statusDesc) {
		this.statusDesc += newLine + statusDesc;
	}
	
	public UploadDocumentStatus getStatusCode() {
		return statusCode;
	}
	
	public int getStatusCodeAsInt() {
		return statusCode.ordinal();
	}

	public void setStatusCode(UploadDocumentStatus statusCode) {
		this.statusCode = statusCode;
		this.statusDesc = strMgr.getString(methodStatusConfigPrefix + statusCode.toString());
	}
	
	public UploadDocumentArgsValidationStatus getArgsValidationStatusCode() {
		return argsValidationStatusCode;
	}
	
	public int getArgsValidationStatusCodeAsInt() {
		return argsValidationStatusCode.ordinal();
	}

	public void setArgsValidationStatusCode(
			UploadDocumentArgsValidationStatus argsValidationStatusCode) {
		this.argsValidationStatusCode = argsValidationStatusCode;
		this.argsValidationStatusDesc = strMgr.getString(
				argsValidStatusConfigPrefix + 
				argsValidationStatusCode.toString());
	}
	
	public String getArgsValidationStatusDesc() {
		return argsValidationStatusDesc;
	}

	public void setArgsValidationStatusDesc(String argsValidationStatusDesc) {
		this.argsValidationStatusDesc = argsValidationStatusDesc;
	}
	
	public void appendArgsValidationStatusDesc(String argsValidationStatusDesc) {
		this.argsValidationStatusDesc += newLine + argsValidationStatusDesc;
	}
		
	@Override
	public String toString() {
		
		String str = 
			"dsDocumentID: " + dsDocumentID +
			", statusCode: " + statusCode.ordinal() +
			", statusDesc: " + statusDesc +
			", argsValidationStatusCode: " + argsValidationStatusCode.ordinal() + 
			", argsValidationStatusDesc: " + argsValidationStatusDesc +
			", attributeName: " + attributeName +  
			", attributeValue: " + attributeValue +
			", archiveDate: " + getArchiveDate() +
			", documentLink: " + getDocumentURL();
			//
		return str;
	}

	public void setArchiveDate(Calendar archiveDate) {
		this.archiveDate = archiveDate;
	}

	public Calendar getArchiveDate() {
		return archiveDate;
	}

	public void setDocumentURL(String documentURL) {
		this.documentURL = documentURL;
	}

	public String getDocumentURL() {
		return documentURL;
	}
}
