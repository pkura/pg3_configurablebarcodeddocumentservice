package pl.compan.docusafe.ws.im;

/**
 * @author stachera_j
 *	
 *	Upload Document arguments validation status
 */
public enum UploadDocumentArgsValidationStatus {
	Success,				// operacja zakonczyla sie powodzeniem
	InvalidAuthCode,		// niepoprawny kod autoryzacji
	InvalidImportID,		// niepoprawny importID
	InvalidDocumentID,		// niepoprawny documentID
	InvalidBoxNumber,		// niepoprawny boxNumber
	InvalidImageArrayName,	// niepoprawna lub brak nazwy pliku
	InvalidImageFileName,	// niepoprawna lub brak nazwy pelnej pliku
	InvalidDocumentKind,	// niepoprawny document kind
	InvalidImportKind,		// niepoprawny import kind
	ImageFileNotExist,		// plik obrazu nie istnieje
	BoxNotExist,			// box o okreslonym numerze nie istnieje
	DescriptorsNotExist		// brak deskryptorow
}
