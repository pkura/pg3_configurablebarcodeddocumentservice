/**
 * 
 */
package pl.compan.docusafe.ws.im;


/**
 * @author stachera_j
 * 
 *         Upload document request
 */
public class UploadDocumentRequest {
	// sprawdzi� czy dokument o documentID (Project Designer Document ID)
	// istnieje w DS
	// je�li istnieje i updateDocument == true to update wpp. pomi�
	// jesli nie istnieje dodaj
	public static final Integer IMPORTKIND_OFFICEDOCUMENT = 1;
	public static final Integer IMPORTKIND_ARCHIVEDOCUMENT = 1;

	private byte[] authCode;
	private Long importId;
	private int pdDocumentID;
	private String boxNumber;
	private String imageArray;
	private String imageName;
	private String imageFullName;
	private boolean updateDocument;
	private Descriptor[] descriptors;
	private String documentKind;
	private Integer importKind;

	public Integer getImportKind() {
		return importKind;
	}

	public void setImportKind(Integer importKind) {
		this.importKind = importKind;
	}

	public String getDocumentKind() {
		return documentKind;
	}

	public void setDocumentKind(String documentKind) {
		this.documentKind = documentKind;
	}

	public Descriptor[] getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(Descriptor[] descriptors) {
		this.descriptors = descriptors;
	}

	public boolean getUpdateDocument() {
		return updateDocument;
	}

	public void setUpdateDocument(boolean updateDocument) {
		this.updateDocument = updateDocument;
	}

	public String getImageFullName() {
		return imageFullName;
	}

	public void setImageFullName(String imageFullName) {
		this.imageFullName = imageFullName;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageArray() {
		return imageArray;
	}

	public void setImageArray(String imageArray) {
		this.imageArray = imageArray;
	}

	public String getBoxNumber() {
		return boxNumber;
	}

	public void setBoxNumber(String boxNumber) {
		this.boxNumber = boxNumber;
	}

	public int getDocumentID() {
		return pdDocumentID;
	}

	public void setDocumentID(int documentID) {
		this.pdDocumentID = documentID;
	}

	public Long getImportId() {
		return importId;
	}

	public void setImportId(Long importId) {
		this.importId = importId;
	}

	public byte[] getAuthCode() {
		return authCode;
	}

	public void setAuthCode(byte[] authCode) {
		this.authCode = authCode;
	}
	
	@Override
	public String toString() {
		String newLine = System.getProperty("line.separator"); 
		//
		String authCodeStr = (authCode != null) ? DocuSafeUploadServiceUtils.byteArrayToHexString(authCode): "[none]"; 
		//
		String str = 
			"authCode: " + authCodeStr +
			", importId: " + importId +
			", documentID: " + pdDocumentID +
			", boxNumber: " + boxNumber +
			", imageArray: " + ((imageArray != null) ? "[exist]": "[none]") +
			", imageSize: " + ((imageArray != null) ? imageArray.length(): 0)+
			", imageName: " + imageName +
			", imageFullName: " + imageFullName +
			", updateDocument: " + updateDocument + 
			", documentKind: " + documentKind + 
			", importKind: " + importKind + newLine +
			">>> Descriptors: " + newLine;
		//
		if (descriptors != null) {
			for (Descriptor desc: descriptors) {
				str += "\t" + desc.toString() + newLine; 
			}
		}
		else {
			str += "[none]";
		}
		//
	
		return str;
	}

}