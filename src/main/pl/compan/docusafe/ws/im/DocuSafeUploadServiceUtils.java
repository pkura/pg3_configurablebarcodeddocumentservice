/**
 * 
 */
package pl.compan.docusafe.ws.im;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;


/**
 * @author stachera_j
 *
 */
public class DocuSafeUploadServiceUtils {
	// log
	private static final Logger log = LoggerFactory.getLogger(DocuSafeUploadServiceUtils.class);
	static final byte[] HEX_CHAR_TABLE = {
	    (byte)'0', (byte)'1', (byte)'2', (byte)'3',
	    (byte)'4', (byte)'5', (byte)'6', (byte)'7',
	    (byte)'8', (byte)'9', (byte)'a', (byte)'b',
	    (byte)'c', (byte)'d', (byte)'e', (byte)'f'
	}; 
	
	public static Date GetEmptyDate(){
		return new Date(0);
	}
	
	/**
	 * Generate digest for upload request
	 * @param request 
	 * @param salt - authorization salt
	 * @param digestAlg - digest algorithm
	 * @return digest
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] generateDigest(UploadDocumentRequest request, String salt, String digestAlg) 
		throws NoSuchAlgorithmException 
	{
		MessageDigest md = MessageDigest.getInstance(digestAlg);
		// create descriptorKey = [last descriptor] name + value
		Descriptor[] descriptor = request.getDescriptors();
		String descriptorKey = null;
		if (descriptor != null) 
		{
			descriptorKey = 
				descriptor[descriptor.length - 1].getDescriptorName();
			    //po ustaleniu z Jurkiem wywaliem
				//descriptor[descriptor.length - 1].getDescriptorValue();			
		} 
		else 
		{
			descriptorKey = "";
		}
		log.trace("descriptorKey={}",descriptorKey);
		// create final key = importID + salt + documentKind + descriptorKey
		String key = 
				request.getImportId().toString() + 
				salt +
				request.getDocumentKind().toString() + 
				descriptorKey;
		// compute digest
		byte[] digest = md.digest(key.getBytes());
		//
		return digest;
	}
	
	/**
	 * Generate digest for test method
	 * @param msg - message
	 * @param salt - salt
	 * @param digestAlg - digest algorithm
	 * @return digest
	 * @throws NoSuchAlgorithmException
	 */
	public static byte[] generateTestDigest(String msg, String salt, String digestAlg) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance(digestAlg);
		//
		String key = salt + msg;
		// compute digest
		byte[] digest = md.digest(key.getBytes());
		//
		return digest;		
	}
	
	public static int getDigestLenght(String digestAlg) {
		int digestLenght = 0;
		try {
			MessageDigest md = MessageDigest.getInstance(digestAlg);
			digestLenght = md.getDigestLength();
		}
		catch (Exception ex) {
			log.debug("[DocuSafeUploadServiceUtils/getDigestLenght] Could not get digest lenght", ex);	
			digestLenght = -1;
		}
		//
		return digestLenght;
	}
	
	/**
	 * Convert byte array to hex string
	 * @param authCode
	 * @return byte array as Hex string
	 */
	@Deprecated
	public static String byteArrayToHexString(byte[] barray) {
		StringBuilder sb = new StringBuilder();
		for (byte b: barray) {
			sb.append(Integer.toHexString(b));
		}	
		return sb.toString();
	}
	
	public static String toHexString(byte[] raw) throws UnsupportedEncodingException
	{
		byte[] hex = new byte[2 * raw.length];
		int index = 0;
		
		for (byte b : raw) {
		  int v = b & 0xFF;
		  hex[index++] = HEX_CHAR_TABLE[v >>> 4];
		  hex[index++] = HEX_CHAR_TABLE[v & 0xF];
		}
		return new String(hex, "ASCII");  
	}
	
	/**
	 * Get stack trace
	 * @param t
	 * @return stack trace string
	 */
	public static String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        String stackTraceStr = sw.toString();
        return stackTraceStr;
	}
}
