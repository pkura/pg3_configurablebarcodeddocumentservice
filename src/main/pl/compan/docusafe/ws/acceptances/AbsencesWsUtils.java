package pl.compan.docusafe.ws.acceptances;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AbsencesWsUtils 
{
	public static Logger LOG = new LoggerFactory().getLogger("absences_ws");
	public static final String FILEPATH = Docusafe.getAdditionProperty("importDocument.filePath");
}
