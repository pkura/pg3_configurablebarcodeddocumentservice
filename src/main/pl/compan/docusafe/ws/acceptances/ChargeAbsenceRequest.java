package pl.compan.docusafe.ws.acceptances;

import java.util.Calendar;

public class ChargeAbsenceRequest 
{
	private Long empCardId;
	private String editorName;
	private Calendar dateFrom;
	private Calendar dateTo;
	private Integer daysNum;
	private String info;
	private String absenceTpe;
	private Long documentId;
	
	public Long getEmpCardId() 
	{
		return empCardId;
	}
	
	public void setEmpCardId(Long empCardId) 
	{
		this.empCardId = empCardId;
	}
	
	public String getEditorName() 
	{
		return editorName;
	}
	
	public void setEditorName(String editorName) 
	{
		this.editorName = editorName;
	}
	
	public Calendar getDateFrom() 
	{
		return dateFrom;
	}
	
	public void setDateFrom(Calendar dateFrom) 
	{
		this.dateFrom = dateFrom;
	}
	
	public Calendar getDateTo() 
	{
		return dateTo;
	}
	
	public void setDateTo(Calendar dateTo) 
	{
		this.dateTo = dateTo;
	}
	
	public Integer getDaysNum() 
	{
		return daysNum;
	}
	
	public void setDaysNum(Integer daysNum) 
	{
		this.daysNum = daysNum;
	}
	
	public String getInfo() 
	{
		return info;
	}
	
	public void setInfo(String info) 
	{
		this.info = info;
	}
	
	public String getAbsenceTpe() 
	{
		return absenceTpe;
	}
	
	public void setAbsenceTpe(String absenceTpe) 
	{
		this.absenceTpe = absenceTpe;
	}

	/**
	 * @param documentId the documentId to set
	 */
	public void setDocumentId(Long documentId) 
	{
		this.documentId = documentId;
	}

	/**
	 * @return the documentId
	 */
	public Long getDocumentId() 
	{
		return documentId;
	}
}
