package pl.compan.docusafe.ws.acceptances;

import java.util.Calendar;

public class DocumentExAcceptances 
{
	private String userName;
	private String acceptanceCn;
	private Calendar acceptanceTime;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getAcceptanceCn() {
		return acceptanceCn;
	}
	public void setAcceptanceCn(String acceptanceCn) {
		this.acceptanceCn = acceptanceCn;
	}
	public Calendar getAcceptanceTime() {
		return acceptanceTime;
	}
	public void setAcceptanceTime(Calendar acceptanceTime) {
		this.acceptanceTime = acceptanceTime;
	}
	
	
}
