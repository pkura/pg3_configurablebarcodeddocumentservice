package pl.compan.docusafe.ws.acceptances;

public class ConditionRequest 
{
	private ConditionEntry[] conditions;

	public ConditionEntry[] getConditions() {
		return conditions;
	}

	public void setConditions(ConditionEntry[] conditions) {
		this.conditions = conditions;
	}
}
