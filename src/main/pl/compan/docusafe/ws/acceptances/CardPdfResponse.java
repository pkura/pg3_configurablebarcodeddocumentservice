package pl.compan.docusafe.ws.acceptances;

public class CardPdfResponse 
{

	private Integer status;
	private String error;
	
	private String cardArray;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getCardArray() {
		return cardArray;
	}
	public void setCardArray(String cardArray) {
		this.cardArray = cardArray;
	}
	
	
	
}
