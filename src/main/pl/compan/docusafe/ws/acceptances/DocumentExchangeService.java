package pl.compan.docusafe.ws.acceptances;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogicLoader;
import pl.compan.docusafe.service.imports.dsi.DSIAttribute;
import pl.compan.docusafe.service.imports.dsi.DSIBean;
import pl.compan.docusafe.service.imports.dsi.DSIImportService;
import pl.compan.docusafe.service.imports.dsi.DSIImportService.ImportDocument;
import pl.compan.docusafe.service.imports.dsi.DSIManager;
import pl.compan.docusafe.util.DateUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class DocumentExchangeService 
{
	private Logger log = AbsencesWsUtils.LOG;
	
	public DocumentExResponse upload(DocumentExRequest request)
	{
		LoggerFactory.getLogger("tomekl").debug("upload");
		DocumentExResponse response = new DocumentExResponse();
		try
		{
			DSApi.openAdmin();			
			
			DSIImportService importService = new DSIImportService();
			ImportDocument importDocument = importService.new ImportDocument();
			
			DSIBean dsiBean = new DSIBean();
			List<DSIAttribute> dsiAttributes = new ArrayList<DSIAttribute>();
			
			for(DocumentExAttribute attribute : request.getAtributes())
			{
				dsiAttributes.add(new DSIAttribute(attribute.getAttributeCn(), attribute.getAttributeValue()));				
			}
			
			LoggerFactory.getLogger("tomekl").debug("acceptances {}",request.getAcceptances()!=null?"nie null":"null");
			
			if(request.getAcceptances()!=null)
			{
				for(int i=0; i<request.getAcceptances().length;i++)
				{
					dsiAttributes.add(new DSIAttribute("ACCEPTANCE_CN_"+i,request.getAcceptances()[i].getAcceptanceCn()));
					dsiAttributes.add(new DSIAttribute("ACCEPTANCE_USER_"+i,request.getAcceptances()[i].getUserName()));
					dsiAttributes.add(new DSIAttribute("ACCEPTANCE_TIME_"+i,DateUtils.formatJsDateTime(request.getAcceptances()[i].getAcceptanceTime().getTime())));				
				}
			}
			
			dsiAttributes.add(new DSIAttribute("TARGET_USER",request.getTargetUser()));
			dsiAttributes.add(new DSIAttribute("NEW_EXTERNAL_ID", request.getExternalId().toString()));
			
			dsiBean.setAttributes(dsiAttributes);
			dsiBean.setBoxCode("noneed");
			dsiBean.setDocumentKind(DocumentLogicLoader.ABSENCE_REQUEST);
			
			File tempFile = new File(AbsencesWsUtils.FILEPATH+request.getImageName());

			if (request.getImageArray() != null) 
			{
	            try
	            {
	            	ByteArrayInputStream bains = new ByteArrayInputStream(Base64.decodeBase64(request.getImageArray()));
	            	FileOutputStream fos = new FileOutputStream(tempFile);
	            	IOUtils.copy(bains, fos);
	            	IOUtils.closeQuietly(bains);
	            	IOUtils.closeQuietly(fos);
	            }
	            catch (Exception e)
	            {
	            	throw new EdmException("Blad w czasie konwersji tablicy bajt�w obrazu z base64", e);
		        }
			}
			
			dsiBean.setImageArray(null);
			dsiBean.setFilePath(request.getImageName());
			dsiBean.setImportKind(4);
			dsiBean.setSubmitDate(new Date());
			dsiBean.setUserImportId(request.getExternalId().toString());
			dsiBean.setImageName(request.getImageName());
			dsiBean.setImportStatus(0);
			dsiBean.setImportId(request.getExternalId());
			
			
			try
			{
				DSIManager manager = new DSIManager();
				DSApi.context().begin();
				
				manager.saveDSIBean(dsiBean, DSApi.context().session().connection());
				
				//importDocument.importOneDSI(dsiBean);
				
				//manager.markImportResult(dsiBean, DSApi.context().session().connection(), false);
				
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				DSApi.context()._rollback();
				throw new EdmException(e.getMessage(),e);
			}
			
			importDocument.importOneDSI(dsiBean);
			
			try
			{
				DSIManager manager = new DSIManager();
				DSApi.context().session().clear();
				DSApi.context().begin();
				
				manager.markImportResult(dsiBean, DSApi.context().session().connection(), false);
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				DSApi.context()._rollback();
				throw new EdmException("Blad w czasie zapisania wyniku");
			}
			
			if(dsiBean.getImportStatus().equals(DSIManager.IMPORT_STATUS_ERROR))
			{
				throw new EdmException(dsiBean.getErrCode());
			}
			
			response.setDocumentId(dsiBean.getDocumentId());
			response.setError("OK");
		}
		catch (Exception e) 
		{
			//DSApi.context()._rollback();
			response.setDocumentId(-1L);
			response.setError(e.getMessage());
			log.debug(e);
			LoggerFactory.getLogger("tomekl").debug(e.getMessage(),e);
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
}
