package pl.compan.docusafe.ws.acceptances;

public class LoginRequest 
{
	String login;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}
