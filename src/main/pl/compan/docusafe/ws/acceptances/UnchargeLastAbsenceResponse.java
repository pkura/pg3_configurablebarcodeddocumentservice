package pl.compan.docusafe.ws.acceptances;

public class UnchargeLastAbsenceResponse 
{
	private String error;
	private Integer status;
	
	public String getError() 
	{
		return error;
	}
	
	public void setError(String error) 
	{
		this.error = error;
	}
	
	public Integer getStatus() 
	{
		return status;
	}
	
	public void setStatus(Integer status) 
	{
		this.status = status;
	}
}
