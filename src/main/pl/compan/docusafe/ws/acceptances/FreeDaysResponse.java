package pl.compan.docusafe.ws.acceptances;

import java.util.Calendar;

public class FreeDaysResponse 
{
	private Integer status;
	private String error;
	
	private Calendar[] freeDays;
	
	public Integer getStatus() 
	{
		return status;
	}
	
	public void setStatus(Integer status) 
	{
		this.status = status;
	}
	
	public String getError() 
	{
		return error;
	}
	
	public void setError(String error) 
	{
		this.error = error;
	}
	
	public Calendar[] getFreeDays() 
	{
		return freeDays;
	}
	
	public void setFreeDays(Calendar[] freeDays) 
	{
		this.freeDays = freeDays;
	}
}
