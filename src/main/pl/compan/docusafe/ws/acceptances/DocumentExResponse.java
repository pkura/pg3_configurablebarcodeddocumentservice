package pl.compan.docusafe.ws.acceptances;

public class DocumentExResponse 
{
	private Long documentId;
	private String error;
	
	public Long getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Long documentId) {
		this.documentId = documentId;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	
	
}
