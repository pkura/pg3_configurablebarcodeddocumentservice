package pl.compan.docusafe.ws.acceptances;

public class DocumentExAttribute 
{
	private String attributeCn;
	private String attributeValue;
	
	
	public String getAttributeCn() {
		return attributeCn;
	}
	public void setAttributeCn(String attributeName) {
		this.attributeCn = attributeName;
	}
	public String getAttributeValue() {
		return attributeValue;
	}
	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}
}
