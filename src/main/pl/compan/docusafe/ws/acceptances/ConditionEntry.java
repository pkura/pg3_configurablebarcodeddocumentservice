package pl.compan.docusafe.ws.acceptances;

public class ConditionEntry 
{
	private String cn;
	private String fieldValue;
	private String username;
	private String divisionGuid;
	private Double maxAmountForAcceptance;
	
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDivisionGuid() {
		return divisionGuid;
	}
	public void setDivisionGuid(String divisionGuid) {
		this.divisionGuid = divisionGuid;
	}
	public Double getMaxAmountForAcceptance() {
		return maxAmountForAcceptance;
	}
	public void setMaxAmountForAcceptance(Double maxAmountForAcceptance) {
		this.maxAmountForAcceptance = maxAmountForAcceptance;
	}
}
