package pl.compan.docusafe.ws.acceptances;

import java.util.Calendar;

public class AbsenceEntry 
{
	private Integer year;
	private Calendar startDate;
	private Calendar endDate;
	private Integer daysNum;
	private Calendar ctime;
	private String absenceType;
	private String editor;
	private Calendar mtime;
	private String info;
	
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Calendar getStartDate() {
		return startDate;
	}
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	public Calendar getEndDate() {
		return endDate;
	}
	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}
	public Integer getDaysNum() {
		return daysNum;
	}
	public void setDaysNum(Integer daysNum) {
		this.daysNum = daysNum;
	}
	public Calendar getCtime() {
		return ctime;
	}
	public void setCtime(Calendar ctime) {
		this.ctime = ctime;
	}
	public String getAbsenceType() {
		return absenceType;
	}
	public void setAbsenceType(String absenceType) {
		this.absenceType = absenceType;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public Calendar getMtime() {
		return mtime;
	}
	public void setMtime(Calendar mtime) {
		this.mtime = mtime;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
}
