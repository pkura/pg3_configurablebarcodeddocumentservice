package pl.compan.docusafe.ws.acceptances;

public class DocumentExRequest 
{
	private String imageArray;
	private String imageName;
	private DocumentExAttribute[] atributes;
	private DocumentExAcceptances[] acceptances;
	private Long externalId;
	private String targetUser;
	
	public String getImageArray() {
		return imageArray;
	}
	public void setImageArray(String imageArray) {
		this.imageArray = imageArray;
	}
	public DocumentExAttribute[] getAtributes() {
		return atributes;
	}
	public void setAtributes(DocumentExAttribute[] atributes) {
		this.atributes = atributes;
	}
	public DocumentExAcceptances[] getAcceptances() {
		return acceptances;
	}
	public void setAcceptances(DocumentExAcceptances[] acceptances) {
		this.acceptances = acceptances;
	}
	
	public Long getExternalId() {
		return externalId;
	}
	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}
	public String getTargetUser() {
		return targetUser;
	}
	public void setTargetUser(String targetUser) {
		this.targetUser = targetUser;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	
}
