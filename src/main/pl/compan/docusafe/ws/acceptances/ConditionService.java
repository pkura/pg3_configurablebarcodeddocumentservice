package pl.compan.docusafe.ws.acceptances;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.dockinds.acceptances.AcceptanceCondition;

public class ConditionService 
{
	public ConditionResponse exchange(ConditionRequest request)
	{
		ConditionResponse response = new ConditionResponse();
		response.setStatus(1);
		response.setError("OK");
		try
		{
			DSApi.openAdmin();
			try
			{
				DSApi.context().begin();
				
				//wyrzucamy stare akceptacje
				for(AcceptanceCondition ac : AcceptanceCondition.list())
        		{
        			if(ac.getCn().equals("szef_dzialu") || ac.getCn().equals("dzial_personalny"))
        			{
        				pl.compan.docusafe.core.Persister.delete(ac);
        			}
        		}
				
				//zapisujemy nowe akceptacje z requesta
				for(ConditionEntry entry : request.getConditions())
				{
					AcceptanceCondition condition = new AcceptanceCondition();
					condition.setCn(entry.getCn());
					condition.setDivisionGuid(entry.getDivisionGuid());
					condition.setMaxAmountForAcceptance(entry.getMaxAmountForAcceptance() < 0 ? null : entry.getMaxAmountForAcceptance());
					if(entry.getUsername().startsWith("ext:"))
					{
						condition.setUsername(entry.getUsername().replace("ext:", ""));
					}
					else
					{
						condition.setUsername("ext:"+entry.getUsername());
					}
					
					//condition.setFieldValue(entry.getFieldValue());
					//condition.setFieldValue(entry.getFieldValue().replace("ext:", ""));
					
					if(entry.getFieldValue() != null && entry.getFieldValue().startsWith("ext:"))
					{
						condition.setFieldValue(entry.getFieldValue().replace("ext:", ""));
					}
					else if(entry.getFieldValue() != null)
					{
						condition.setFieldValue("ext:"+entry.getFieldValue());
					}
					
					pl.compan.docusafe.core.Persister.create(condition);
				}
				
				
				DSApi.context().commit();
			}
			catch (Exception e) 
			{
				DSApi.context()._rollback();
				e.printStackTrace();
				throw new EdmException(e.getMessage(),e);
			}
			finally
			{
				DSApi._close();
			}
		}
		catch (Exception e) 
		{
			response.setStatus(-1);
			response.setError(e.getMessage());
			//e.printStackTrace();
		}
		return response;
	}
}
