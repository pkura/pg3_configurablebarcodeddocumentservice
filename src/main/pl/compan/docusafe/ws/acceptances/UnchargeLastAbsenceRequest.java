package pl.compan.docusafe.ws.acceptances;

public class UnchargeLastAbsenceRequest 
{
	private Long empCardId;
	private Long documentId;

	public Long getEmpCardId() 
	{
		return empCardId;
	}

	public void setEmpCardId(Long empCardId) 
	{
		this.empCardId = empCardId;
	}
	
	public Long getDocumentId() 
	{
		return documentId;
	}
	
	public void setDocumentId(Long documentId) 
	{
		this.documentId = documentId;
	}
}
