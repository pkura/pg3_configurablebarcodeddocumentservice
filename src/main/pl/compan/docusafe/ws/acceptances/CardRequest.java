package pl.compan.docusafe.ws.acceptances;

public class CardRequest 
{
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
