package pl.compan.docusafe.ws.acceptances;

public class ConditionResponse 
{
	private Integer status;
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
