package pl.compan.docusafe.ws.acceptances;

public class CardPdfRequest 
{
	private String userName;
	private Integer year;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	
}
