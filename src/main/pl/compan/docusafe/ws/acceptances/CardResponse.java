package pl.compan.docusafe.ws.acceptances;

import java.util.Calendar;

public class CardResponse 
{
	private Integer status;
	private String error;
	
	private Long id;
	private String KPX;
	private String company;
	private String position;
	private String department;
	private String division;
	private Calendar employmentStartDate;
	private Calendar employmentEndDate;
	private String info;
	private String user;
	private String divisionName;
	private Boolean T188KP;
	private Integer t188kpDaysNum;
	private Boolean TUJ;
	private Integer tujDaysNum;
	private Boolean TINW;
	private Integer invalidAbsenceDaysNum;
	private String externalUser;
	
	private AbsenceEntry[] absences;
	
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKPX() {
		return KPX;
	}
	public void setKPX(String kpx) {
		KPX = kpx;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
	public String getInfo() {
		return info;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Calendar getEmploymentStartDate() {
		return employmentStartDate;
	}
	public void setEmploymentStartDate(Calendar employmentStartDate) {
		this.employmentStartDate = employmentStartDate;
	}
	public Calendar getEmploymentEndDate() {
		return employmentEndDate;
	}
	public void setEmploymentEndDate(Calendar employmentEndDate) {
		this.employmentEndDate = employmentEndDate;
	}
	public Integer getT188kpDaysNum() {
		return t188kpDaysNum;
	}
	public void setT188kpDaysNum(Integer daysNum) {
		t188kpDaysNum = daysNum;
	}
	public Boolean getTUJ() {
		return TUJ;
	}
	public void setTUJ(Boolean tuj) {
		TUJ = tuj;
	}
	public Integer getTujDaysNum() {
		return tujDaysNum;
	}
	public void setTujDaysNum(Integer tujDaysNum) {
		this.tujDaysNum = tujDaysNum;
	}
	public Boolean getTINW() {
		return TINW;
	}
	public void setTINW(Boolean tinw) {
		TINW = tinw;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getDivisionName() {
		return divisionName;
	}
	public void setDivisionName(String divisionName) {
		this.divisionName = divisionName;
	}
	public Boolean getT188KP() {
		return T188KP;
	}
	public void setT188KP(Boolean t188kp) {
		T188KP = t188kp;
	}
	public Integer getInvalidAbsenceDaysNum() {
		return invalidAbsenceDaysNum;
	}
	public void setInvalidAbsenceDaysNum(Integer invalidAbsenceDaysNum) {
		this.invalidAbsenceDaysNum = invalidAbsenceDaysNum;
	}
	public String getExternalUser() {
		return externalUser;
	}
	public void setExternalUser(String externalUser) {
		this.externalUser = externalUser;
	}
	public AbsenceEntry[] getAbsences() {
		return absences;
	}
	public void setAbsences(AbsenceEntry[] absences) {
		this.absences = absences;
	}
	
}
