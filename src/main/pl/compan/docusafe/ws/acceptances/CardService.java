package pl.compan.docusafe.ws.acceptances;

import java.io.File;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.Absence;
import pl.compan.docusafe.core.base.absences.AbsenceFactory;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.util.FileUtils;

public class CardService 
{
	private static final Logger LOG = LoggerFactory.getLogger("kamilj");
	
	public LoginResponse getUserInfo(LoginRequest request)
	{
		LoginResponse response = new LoginResponse();
		response.setStatus(1);
		response.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			DSUser user = DSUser.findByUsername(request.getLogin());
			response.setFirstName(user.getFirstname());
			response.setLastName(user.getLastname());
			response.setLogin(user.getName());
		}
		catch (Exception e) 
		{
			response.setStatus(-1);
			response.setError(e.getMessage());
			LOG.debug(e.getMessage(),e);
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
	public CardResponse getCard(CardRequest request)
	{
		CardResponse response = new CardResponse();
		response.setStatus(1);
		response.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			
			EmployeeCard card = AbsenceFactory.getActiveEmployeeCardByUserName(request.getUserName());
			
			if(card == null)
				throw new EdmException("Brak uzytkownika");
			
			fillInResponse(card, response);
		}
		catch (Exception e) 
		{
			response.setStatus(-1);
			response.setError(e.getMessage());
			LOG.error(e.getMessage(), e);
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
	public CardPdfResponse getPdfCard(CardPdfRequest request)
	{
		CardPdfResponse response = new CardPdfResponse();
		response.setStatus(1);
		response.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			
			File pdfCard = AbsenceFactory.pdfCard(request.getUserName(), request.getYear());

			response.setCardArray(Base64.encodeBase64String(FileUtils.getBytesFromFile(pdfCard)));
		}
		catch (Exception e) 
		{
			response.setStatus(-1);
			response.setError(e.getMessage());
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
	public ChargeAbsenceResponse chargeAbsence(ChargeAbsenceRequest request)
	{
		ChargeAbsenceResponse response = new ChargeAbsenceResponse();
		response.setStatus(1);
		response.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			// pobranie karty pracownika
			EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());
			// ustawienie obiektu uzytkownika ktory zaakceptowal wniosek
			
			// naliczenie urlopu
			AbsenceFactory.chargeAbsence(card, request.getDateFrom().getTime(), request.getDateTo().getTime(), 
					request.getAbsenceTpe(), request.getInfo(), request.getDaysNum(), null, request.getEditorName(), 
					request.getDocumentId());
		}
		catch (Exception ex)
		{
			response.setStatus(-1);
			response.setError(ex.getMessage());
			LOG.error(ex.getMessage(), ex);
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
	public UnchargeLastAbsenceResponse unchargeLastAbsence(UnchargeLastAbsenceRequest request)
	{
		UnchargeLastAbsenceResponse response = new UnchargeLastAbsenceResponse();
		response.setStatus(1);
		response.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			
			DSApi.context().begin();
			// pobranie karty pracownika
			EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());
			AbsenceFactory.unchargeLastAbsences(card, request.getDocumentId());
			DSApi.context().commit();
		}
		catch (Exception ex)
		{
			response.setStatus(-1);
			response.setError(ex.getMessage());
		}
		finally
		{
			DSApi._close();
		}
		
		return response;
	}
	
	public FreeDaysResponse getFreeDays()
	{
		FreeDaysResponse daysResponse = new FreeDaysResponse();
		daysResponse.setStatus(1);
		daysResponse.setError("OK");
		
		try
		{
			DSApi.openAdmin();
			
			List<FreeDay> freeDays = AbsenceFactory.getFreeDays();
			Calendar[] calendars = new Calendar[freeDays.size()];
			
			int i = 0;
			for (FreeDay day : freeDays)
			{
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(day.getDate());
				calendars[i++] = calendar;
			}
			
			daysResponse.setFreeDays(calendars);
		}
		catch (Exception ex)
		{
			daysResponse.setStatus(-1);
			daysResponse.setError(ex.getMessage());
		}
		finally
		{
			DSApi._close();
		}
		
		return daysResponse;
	}
	
	private static void fillInResponse(EmployeeCard card, CardResponse response) throws Exception
	{
		response.setId(card.getId());
		response.setCompany(card.getCompany());
		response.setDepartment(card.getDepartment());
		response.setDivision(card.getDivision());
		response.setDivisionName(card.getDivisionName());
		response.setExternalUser(card.getExternalUser());
		response.setInfo(card.getInfo());
		response.setInvalidAbsenceDaysNum(card.getInvalidAbsenceDaysNum());
		response.setKPX(card.getKPX());
		response.setPosition(card.getPosition());
		response.setT188KP(card.getT188KP());
		response.setT188kpDaysNum(card.getT188kpDaysNum());
		response.setTINW(card.getTINW());
		response.setTUJ(card.getTUJ());
		response.setTujDaysNum(card.getTujDaysNum());
		
		if (card.getEmploymentEndDate() != null)
		{
			Calendar endDate = Calendar.getInstance();
			endDate.setTime(card.getEmploymentEndDate());
			response.setEmploymentEndDate(endDate);
		}
		
		if (card.getEmploymentStartDate() != null)
		{
			Calendar startDate = Calendar.getInstance();
			startDate.setTime(card.getEmploymentStartDate());
			response.setEmploymentStartDate(startDate);
		}
		
		if (card.getAbsences() != null)
		{
			AbsenceEntry[] absences = new AbsenceEntry[card.getAbsences().size()];
			
			int i = 0;
			for(Absence abs : card.getAbsences())
			{
				absences[i] = new AbsenceEntry();
				
				absences[i].setAbsenceType(abs.getAbsenceType().getCode());
				
				if (abs.getCtime() != null)
				{
					Calendar ctime = Calendar.getInstance();
					ctime.setTime(abs.getCtime());
					absences[i].setCtime(ctime);
				}
				
				absences[i].setDaysNum(abs.getDaysNum());
				
				/*
				if (abs.getEditor() != null)
					absences[i].setEditor(abs.getEditor().getName());*/
				
				if (abs.getEndDate() != null)
				{
					Calendar endTime = Calendar.getInstance();
					endTime.setTime(abs.getEndDate());
					absences[i].setEndDate(endTime);
				}
				
				if (abs.getMtime() != null)
				{
					Calendar mTime = Calendar.getInstance();
					mTime.setTime(abs.getMtime());
					absences[i].setMtime(mTime);
				}
				
				if (abs.getStartDate() != null)
				{
					Calendar startTime = Calendar.getInstance();
					startTime.setTime(abs.getStartDate());
					absences[i].setStartDate(startTime);
				}
				
				absences[i].setInfo(abs.getInfo());
				absences[i].setYear(abs.getYear());
				i++;
			}
			
			response.setAbsences(absences);
		}
	}
	
}
