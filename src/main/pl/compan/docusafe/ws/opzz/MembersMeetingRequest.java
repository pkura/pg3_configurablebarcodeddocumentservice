package pl.compan.docusafe.ws.opzz;

public class MembersMeetingRequest
{
	private Long id;

	/**
	 *	Zmienna charakteryzująca spotkanie do zwrócenia uczestników 
	 */
	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}
}
