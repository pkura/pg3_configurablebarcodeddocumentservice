package pl.compan.docusafe.ws.opzz;

import pl.compan.docusafe.ws.overtimes.BaseResponse;

public class MembersMeetingResponse extends BaseResponse
{
	private String[] members;

	public String[] getMembers()
	{
		return members;
	}

	public void setMembers(String[] members)
	{
		this.members = members;
	}
}
