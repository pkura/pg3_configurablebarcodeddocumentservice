package pl.compan.docusafe.ws.opzz;

/**
 * Request dodawania delegata
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class AddDelegateRequest
{
	//delegat
	private String delegate;
	
	private Long eventId;
	
	private Long organizationId;

	
	public String getDelegate()
	{
		return delegate;
	}

	public void setDelegate(String delegate)
	{
		this.delegate = delegate;
	}

	public Long getEventId()
	{
		return eventId;
	}

	public void setEventId(Long eventId)
	{
		this.eventId = eventId;
	}

	public Long getOrganizationId()
	{
		return organizationId;
	}

	public void setOrganizationId(Long organizationId)
	{
		this.organizationId = organizationId;
	}
	
	
}
