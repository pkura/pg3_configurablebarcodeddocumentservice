package pl.compan.docusafe.ws.opzz;

public class RemoveDelegateRequest
{
	private String delegate;
	private Long eventId;
	
	public String getDelegate()
	{
		return delegate;
	}
	public void setDelegate(String delegate)
	{
		this.delegate = delegate;
	}
	public Long getEventId()
	{
		return eventId;
	}
	public void setEventId(Long eventId)
	{
		this.eventId = eventId;
	}
}
