package pl.compan.docusafe.ws.opzz;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.parametrization.opzz.OpzzFactory;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 * Webserwis do polaczenia z Liferay'em. Pocz�tkowo mia� dostarcza� informacje o uczestnikach spotka�
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class OpzzService
{
	private final static Logger log = LoggerFactory.getLogger(OpzzService.class);
	
	/**
	 * Dodaje delegata do zdarzenia
	 * @param request
	 * @return
	 */
	public AddDelegateResponse addDelegate(AddDelegateRequest request)
	{
		AddDelegateResponse response = new AddDelegateResponse();
		response.checkStatusOk();
		log.error("Dodaje Delegata {}", request.getDelegate());
		try
		{
			DSApi.openAdmin();
			DSApi.context().begin();
			
			OpzzFactory.addDelegate(request.getDelegate(), request.getEventId(), request.getOrganizationId());
			
			DSApi.context().commit();
			
		}catch( Exception ex ){
			response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
            try
            {
            	DSApi.context().rollback();
            }catch(EdmException e){
            	log.error("blad przy rollbacku", e);
            }
		}finally{
			DSApi._close();
		}
		
		return response;
	}
	
	/**
	 * Usuwa delegata ze zdarzenia
	 * @param request
	 * @return
	 */
	public RemoveDelegateResponse removeDelegate(RemoveDelegateRequest request)
	{
		RemoveDelegateResponse response = new RemoveDelegateResponse();
		response.checkStatusOk();
		log.error("Usuwam delegata {}", request.getDelegate());
		try
		{
			DSApi.openAdmin();
			DSApi.context().begin();
			
			OpzzFactory.removeDelegate(request.getDelegate(), request.getEventId());
			
			DSApi.context().commit();
			
		}catch( Exception ex ){
			response.checkStatusError(ex.getMessage());
			response.setError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
            try
            {
            	DSApi.context().rollback();
            }catch(EdmException e){
            	log.error("blad przy rollbacku", e);
            }
		}finally{
			DSApi._close();
		}
		
		return response;
	}
	
	/**
	 * Dodaje kandydata do zdarzenia
	 * @param request
	 * @return
	 */
	public AddCandidateResponse addCandidate(AddCandidateRequest request)
	{
		AddCandidateResponse response = new AddCandidateResponse();
		response.checkStatusOk();
		log.error("Dodaje kandydata {}", request.getCandidate());
		try
		{
			DSApi.openAdmin();
			DSApi.context().begin();
			
			OpzzFactory.addCandidate(request.getCandidate(), request.getEventId());
			DSApi.context().commit();
			
		}catch( Exception ex ){
			response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
            try
            {
            	DSApi.context().rollback();
            }catch(EdmException e){
            	log.error("blad przy rollbacku", e);
            }
		}finally{
			DSApi._close();
		}
		
		return response;
	}
	
	/**
	 * Usuwa kandydata ze zdarzenia
	 * @param request
	 * @return
	 */
	public RemoveCandidateResponse removeCandidate(RemoveCandidateRequest request)
	{
		RemoveCandidateResponse response = new RemoveCandidateResponse();
		response.checkStatusOk();
		log.error("Usuwam Kandydata {}", request.getCandidate());
		try
		{
			DSApi.openAdmin();
			DSApi.context().begin();
			
			OpzzFactory.removeCandidate(request.getCandidate(), request.getEventId());
			
			DSApi.context().commit();
			
		}catch( Exception ex ){
			response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
            try
            {
            	DSApi.context().rollback();
            }catch(EdmException e){
            	log.error("blad przy rollbacku", e);
            }
		}finally{
			DSApi._close();
		}
		
		return response;
	}
}
