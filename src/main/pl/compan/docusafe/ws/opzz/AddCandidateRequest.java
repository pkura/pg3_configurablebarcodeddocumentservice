package pl.compan.docusafe.ws.opzz;

public class AddCandidateRequest
{
	// 
	private String candidate;

	private Long eventId;

	public String getCandidate()
	{
		return candidate;
	}

	public void setCandidate(String candidate)
	{
		this.candidate = candidate;
	}

	public Long getEventId()
	{
		return eventId;
	}

	public void setEventId(Long eventId)
	{
		this.eventId = eventId;
	}

}
