package pl.compan.docusafe.ws.authorization;

import java.io.Serializable;

import javax.security.auth.Subject;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

public class AbstractAuthenticator implements Serializable
{
	private final static Logger log = LoggerFactory.getLogger(AbstractAuthenticator.class);
	
	/**
     * W tej mapie przechowujemy subject wyszukany w PasswordCallbackach
     * Nie mozna korzystac z Contextu bo jest on zamykany
     */
    protected static ThreadLocal<Subject> authenticatedSubject = new ThreadLocal<Subject>();
    
    /**
     * Metoda zapisuje uzytkownika dla watku
     * @param user
     */
    public static void setThreadSubject(Subject subject)
    {
        authenticatedSubject.set(subject);
        if (subject==null) {
            return;
        }
        log.trace("setThreadSubject {}",subject.toString());
        
    }
    
    /**
     * Metoda czysci subject przypisanego dla watku
     */
    public static void clearThreadSubject()
    {
        log.trace("clearThreadSubject");
        authenticatedSubject.remove();
    }
    
    /**
     * Pobieramy subject dla watku
     * @return
     */
    public static Subject getThreadSubject()
    {
        log.trace("getThreadSubject");
        return authenticatedSubject.get();
    }
    
    /**
     * Otwiera DSContext z lokalnego getThreadSubject()
     * @throws Exception
     */
    public static void openContext() throws Exception, AccessDeniedException
    {
    	DSApi.open(getThreadSubject());
    }
    
    /**
     * Metoda sprawdza czy użytkownik ma uprawnienia do wywołania tej metody
     * poprzez należenie do odpowiedniego GUIDa
     * 
     * @param guid
     * @throws Exception,{@link AccessDeniedException}
     */
    public static void openContext(String guid) throws Exception, AccessDeniedException
    {
    	openContext();
    	if(!DSApi.context().getDSUser().inDivisionByGuid(guid))
    		throw new AccessDeniedException("Użytkownik nie ma uprawnień do wywołania tej metody");
    }
    
    /**
     * Zamyka i czysci authenticatedSubject
     */
    public static void _closeContext()
    {
    	DSApi._close();
    	clearThreadSubject();
    }
}
