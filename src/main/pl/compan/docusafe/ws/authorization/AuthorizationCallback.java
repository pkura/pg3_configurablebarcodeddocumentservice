package pl.compan.docusafe.ws.authorization;

import java.io.IOException;
import java.io.Serializable;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;

import org.apache.ws.security.WSPasswordCallback;

import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.core.users.UserNotFoundException;
import pl.compan.docusafe.core.users.auth.AuthUtil;
import pl.compan.docusafe.core.users.auth.FormCallbackHandler;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import com.opensymphony.webwork.ServletActionContext;
import pl.compan.docusafe.web.filter.CasUtils;

/**
 * Obiekt autoryzacji WS,Klasa weryfikujaca haslo 
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class AuthorizationCallback implements CallbackHandler, Serializable
{
	private final static Logger log = LoggerFactory.getLogger(AuthorizationCallback.class);
	
	/**
     * Obsluga hasla
     * @param callbacks
     * @throws java.io.IOException
     * @throws javax.security.auth.callback.UnsupportedCallbackException
     */
	@Override
	public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException
	{
		log.debug("handle {} ",callbacks.length);
        for (int i = 0; i < callbacks.length; i++)
        {
            log.trace("handler {}",i);
            if (callbacks[i] instanceof WSPasswordCallback)
            {
                log.trace("WSPasswordCallback");
                WSPasswordCallback pc = (WSPasswordCallback)callbacks[i];

                log.trace("identifier={}",pc.getIdentifier());                
                log.trace("passwordType={}",pc.getPasswordType());
                log.trace("usage={}",pc.getUsage());

                doAuthorization(pc);            
            }
            else
            {
                log.warn("handler {}",callbacks[i]);
                throw new UnsupportedCallbackException(callbacks[i], "Unrecognized Callback");
            }
        }
	}
	
	/**
	 * 
	 * @param pc
	 * @throws IOException
	 */
	protected void doAuthorization (WSPasswordCallback pc) throws IOException
    {
        try
        {
        	AbstractAuthenticator.clearThreadSubject();
        	boolean passwordDigest = pc.getPasswordType().contains("PasswordDigest");
            boolean passwordText = pc.getPasswordType().contains("PasswordText");
            boolean ok = false;
            if(!passwordDigest && !passwordText)
            {
            	throw new IOException("Wymagane jest PasswordDigest lub PasswordText");
            }
            log.debug("szukam danych o uzytkowniku {}",pc.getIdentifier());
            
            if (passwordDigest)
            {
                log.trace("haslo Digest");
                throw new IOException("Blad autoryzacji: passwordDigest nie jest obs�ugiwane");
            }
            else // passwordText
            {
        		LoginContext lc =getLoginContext(pc);
        		
        		lc.login();
        		AbstractAuthenticator.setThreadSubject(lc.getSubject());
        		log.trace("logowanie udane - {}", pc.getIdentifier());
            }
            
        }
        catch (IOException e)
        {
            log.debug("",e);
            throw e;
        }
        catch (Exception e)
        {
            log.error("",e);
            throw new IOException(e.toString());
        }
        finally
        {
        }
    }
	
	private LoginContext getLoginContext(WSPasswordCallback pc) throws Exception
	{
        if(pc.getIdentifier() == null || pc.getIdentifier().equals("ticket")) {
            String username = CasUtils.checkProxyTicket(pc.getPassword());
            return AuthUtil.getLoginContext(username, "", null);
        }
		return AuthUtil.getBasicLoginContext(pc.getIdentifier(), pc.getPassword(), null);
	}
}
