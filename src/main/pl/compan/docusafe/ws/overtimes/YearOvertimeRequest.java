package pl.compan.docusafe.ws.overtimes;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class YearOvertimeRequest
{
    private Long empCardId;
    private int year;

    public Long getEmpCardId()
    {
        return empCardId;
    }

    public void setEmpCardId(Long empCardId)
    {
        this.empCardId = empCardId;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }   
}
