package pl.compan.docusafe.ws.overtimes;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class ReportResponse extends BaseResponse
{
    private String cardReport;

    public String getCardReport()
    {
        return cardReport;
    }

    public void setCardReport(String cardReport)
    {
        this.cardReport = cardReport;
    }
}
