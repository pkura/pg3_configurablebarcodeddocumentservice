package pl.compan.docusafe.ws.overtimes;

import java.util.Calendar;

/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class DayIsFreeRequest {
	private Calendar date;

    public Calendar getDate()
    {
        return date;
    }

    public void setDate(Calendar date)
    {
        this.date = date;
    }
        
        
}
