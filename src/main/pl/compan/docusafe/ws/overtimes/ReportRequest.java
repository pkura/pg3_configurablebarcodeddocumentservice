package pl.compan.docusafe.ws.overtimes;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class ReportRequest
{
    public static final int _YEAR_PDF_CARD = 1;
    public static final int _YEAR_XLS_CARD = 2;
    public static final int _MONTH_PDF_CARD = 3;
    public static final int _MONTH_XLS_CARD = 4;
    
    public String username;
    public int type;
    public int year;
    public int month;
    

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public int getType()
    {
        return type;
    }

    public void setType(int type)
    {
        this.type = type;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }
}
