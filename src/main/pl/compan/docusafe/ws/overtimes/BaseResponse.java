package pl.compan.docusafe.ws.overtimes;
/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class BaseResponse {
	private Integer status;
	private String error;

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public Integer getStatus()
    {
        return status;
    }

    public void setStatus(Integer status)
    {
        this.status = status;
    }
        
        /**
         * Ustawia  status na -1 z przekazanym bledem;
         */
	public void checkStatusError(String error) {
	      this.status = -1;
          this.error = error;
	}
	
        /**
         * Ustawia poprawny status
         */
	public void checkStatusOk() {
            this.status = 1;
            this.error = "OK";
	}
}
