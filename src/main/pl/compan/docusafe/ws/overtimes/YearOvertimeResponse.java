package pl.compan.docusafe.ws.overtimes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class YearOvertimeResponse extends BaseResponse
{
    private EmployeeOvertimeEntity[] empOvertimes;

    public EmployeeOvertimeEntity[] getEmpOvertimes()
    {
        return empOvertimes;
    }

    public void setEmpOvertimes(EmployeeOvertimeEntity[] empOvertimes)
    {
        this.empOvertimes = empOvertimes;
    }
    
    public void setEmpOvertimes(List<EmployeeOvertimeEntity> empOvertimes)
    {
        if(!empOvertimes.isEmpty())
        {
            this.empOvertimes = new EmployeeOvertimeEntity[empOvertimes.size()];
            
            int i =0;
            
            for(EmployeeOvertimeEntity entity : empOvertimes)
            {
                this.empOvertimes[i] = entity;
                i++;
            }
        }
    }
}
