package pl.compan.docusafe.ws.overtimes;

import java.util.Calendar;

/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class ChargeOvertimeRequest {
	private String from;
	private String to;
	public String count;
	public Calendar date;
	public String description;
	public Long documentId;
	public Long empCardId;
	public Long id;
	public Boolean overtimeReceive;

    public String getCount()
    {
        return count;
    }

    public void setCount(String count)
    {
        this.count = count;
    }

    public Calendar getDate()
    {
        return date;
    }

    public void setDate(Calendar date)
    {
        this.date = date;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getEmpCardId()
    {
        return empCardId;
    }

    public void setEmpCardId(Long empCardId)
    {
        this.empCardId = empCardId;
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Boolean getOvertimeReceive()
    {
        return overtimeReceive;
    }

    public void setOvertimeReceive(Boolean overtimeReceive)
    {
        this.overtimeReceive = overtimeReceive;
    }

    public String getTo()
    {
        return to;
    }

    public void setTo(String to)
    {
        this.to = to;
    }
        
        
}
