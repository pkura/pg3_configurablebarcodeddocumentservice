package pl.compan.docusafe.ws.overtimes;

import java.util.Arrays;
import java.util.List;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeMonthlyEntity;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class MonthOvertimeResponse extends BaseResponse
{
    private OvertimeMonthlyWSEntity[] monthOvertimes;

    public OvertimeMonthlyWSEntity[] getMonthOvertimes()
    {
        return monthOvertimes;
    }

    public void setMonthOvertimes(OvertimeMonthlyWSEntity[] monthOvertimes)
    {
        this.monthOvertimes = monthOvertimes;
    }
}
