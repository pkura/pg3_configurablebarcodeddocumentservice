package pl.compan.docusafe.ws.overtimes;

/**
 *
 * @author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class MonthOvertimeRequest
{
    private Long empCardId;
    private int year;
    private int month;

    public Long getEmpCardId()
    {
        return empCardId;
    }

    public void setEmpCardId(Long empId)
    {
        this.empCardId = empId;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public int getYear()
    {
        return year;
    }

    public void setYear(int year)
    {
        this.year = year;
    }
    
    
}
