package pl.compan.docusafe.ws.overtimes;

import java.util.Calendar;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeReceiveMonthlyEntity;

/**
 * Klasa do zarz�dzania miesiecznymi nadgodzinami EmployeeOvertimeMonthlyEntity
 * u�ywa list co jest niezgodne z ws
 * @author �ukasz Wo�niak <l.g.wozniak@gmail.com>
 */
public class OvertimeMonthlyWSEntity
{
    private int month;
    private Calendar date;
    /** Wypracowane nadgodziny*/
    private String overtimes;
    private String description;
    private Long documentId;
    
    private String firstDateReceive;
    private String firstOvertimesReceive;
    public int hours;
    public int minutes;
    
    private EmployeeOvertimeReceiveMonthlyEntity[] empOvertimeReceive;

     public OvertimeMonthlyWSEntity()
    {
    }
    
    public OvertimeMonthlyWSEntity(Calendar date, String overtimes, String description, Long documentId, int month)
    {
        this.date = date;
        this.overtimes = overtimes;
        this.description = description;
        this.documentId = documentId;
        this.month = month;
        
    }
    
    public Calendar getDate()
    {
        return date;
    }

    public void setDate(Calendar date)
    {
        this.date = date;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public EmployeeOvertimeReceiveMonthlyEntity[] getEmpOvertimeReceive()
    {
        return empOvertimeReceive;
    }

    public void setEmpOvertimeReceive(EmployeeOvertimeReceiveMonthlyEntity[] empOvertimeReceive)
    {
        this.empOvertimeReceive = empOvertimeReceive;
    }

    public String getFirstDateReceive()
    {
        return firstDateReceive;
    }

    public void setFirstDateReceive(String firstDateReceive)
    {
        this.firstDateReceive = firstDateReceive;
    }

    public String getFirstOvertimesReceive()
    {
        return firstOvertimesReceive;
    }

    public void setFirstOvertimesReceive(String firstOvertimesReceive)
    {
        this.firstOvertimesReceive = firstOvertimesReceive;
    }

    public int getHours()
    {
        return hours;
    }

    public void setHours(int hours)
    {
        this.hours = hours;
    }

    public int getMinutes()
    {
        return minutes;
    }

    public void setMinutes(int minutes)
    {
        this.minutes = minutes;
    }

    public int getMonth()
    {
        return month;
    }

    public void setMonth(int month)
    {
        this.month = month;
    }

    public String getOvertimes()
    {
        return overtimes;
    }

    public void setOvertimes(String overtimes)
    {
        this.overtimes = overtimes;
    }
    
    
}
