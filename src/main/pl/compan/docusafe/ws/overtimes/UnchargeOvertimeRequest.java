package pl.compan.docusafe.ws.overtimes;

/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class UnchargeOvertimeRequest
{
    private Long empCardId;
    private Long documentId;

    public Long getDocumentId()
    {
        return documentId;
    }

    public void setDocumentId(Long documentId)
    {
        this.documentId = documentId;
    }

    public Long getEmpCardId()
    {
        return empCardId;
    }

    public void setEmpCardId(Long empCardId)
    {
        this.empCardId = empCardId;
    }
}
