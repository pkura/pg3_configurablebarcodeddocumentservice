package pl.compan.docusafe.ws.overtimes;
/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class DayIsFreeResponse  extends BaseResponse {
	private Boolean isFree;

    public Boolean getIsFree()
    {
        return isFree;
    }

    public void setIsFree(Boolean isFree)
    {
        this.isFree = isFree;
    }
        
        
}
