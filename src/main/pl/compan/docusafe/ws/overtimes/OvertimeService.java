package pl.compan.docusafe.ws.overtimes;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.Finder;
import pl.compan.docusafe.core.base.EmployeeCard;
import pl.compan.docusafe.core.base.absences.FreeDay;
import pl.compan.docusafe.core.users.DSUser;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeMonthlyEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.EmployeeOvertimeReceiveMonthlyEntity;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeFactory;
import pl.compan.docusafe.parametrization.aegon.overtime.core.OvertimeRaportFactory;
import pl.compan.docusafe.util.FileUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

/**
 *
 * author �ukasz Wo�niak <lukasz.wozniak@docusafe.pl;l.g.wozniak@gmail.com>
 */
public class OvertimeService
{
    private static final Logger log = LoggerFactory.getLogger("lukaszw");

    public UserResponse getUserInfo(UserRequest request)
    {
        UserResponse response = new UserResponse();
        response.checkStatusOk();
        
        try
        {
            DSApi.openAdmin();

            DSUser user = DSUser.findByUsername(request.getLogin());

            response.setFirstName(user.getFirstname());
            response.setLastName(user.getLastname());
            response.setLogin(user.getName());
            response.setId(user.getId());
        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }
        return response;
    }
    
    public ChargeOvertimeResponse chargeOvertimes(ChargeOvertimeRequest request)
    {
        ChargeOvertimeResponse response = new ChargeOvertimeResponse();
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();

            EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());

            OvertimeFactory.chargeOvertimes(card, request.getDocumentId(), request.getDate().getTime(), request.getFrom(), request.getTo(), request.getCount(), request.getDescription(), request.getOvertimeReceive());

        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }
        return response;
    }

    public UnchargeOvertimeResponse unchargeOvertimes(UnchargeOvertimeRequest request)
    {
        UnchargeOvertimeResponse response = new UnchargeOvertimeResponse();
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();
            DSApi.context().begin();

            EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());

            OvertimeFactory.unchargeOvertimes(card, request.getDocumentId());

            DSApi.context().commit();

        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }

        return response;
    }

    /** 
     * sprawdza czy dzie� jest wolny
     */
    public DayIsFreeResponse getIsFreeDay(DayIsFreeRequest request)
    {
        DayIsFreeResponse response = new DayIsFreeResponse();
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();

            List<FreeDay> freeDays = FreeDay.find(request.getDate().getTime());

            if( freeDays.isEmpty() )
            {
                response.setIsFree(Boolean.TRUE);
            }
            else
            {
                response.setIsFree(Boolean.FALSE);
            }

        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }
        return response;
    }

    /**
     * Roczna karta pracownika nadgodzin
     * @param request
     * @return 
     */
    public YearOvertimeResponse getEmployeeOvertimes(YearOvertimeRequest request)
    {
        YearOvertimeResponse response = new YearOvertimeResponse();
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();

            EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());

            List<EmployeeOvertimeEntity> overtimes = OvertimeFactory.getEmployeeOvertimes(card, request.getYear());
            //zamieniam na tablice bo ws nie pozwala na listy
            if(!overtimes.isEmpty())
                response.setEmpOvertimes(overtimes.toArray(new EmployeeOvertimeEntity[overtimes.size()]));
        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }

        return response;
    }

    public MonthOvertimeResponse getMonthEmployeeOvertimes(MonthOvertimeRequest request)
    {
        MonthOvertimeResponse response = new MonthOvertimeResponse();
        OvertimeMonthlyWSEntity[] overtimeMonthWS = null;
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();

            EmployeeCard card = Finder.find(EmployeeCard.class, request.getEmpCardId());

            List<EmployeeOvertimeMonthlyEntity> overtimes = OvertimeFactory.getEmployeeMonthlyOvertimes(card, request.getYear(), request.getMonth());

            if(!overtimes.isEmpty())
            {
                overtimeMonthWS = new OvertimeMonthlyWSEntity[overtimes.size()];
                int i = 0 ;
                
                for(EmployeeOvertimeMonthlyEntity entity : overtimes)
                {
                    Calendar caledar = Calendar.getInstance();
                    caledar.setTime(entity.getDate());
                    
                    OvertimeMonthlyWSEntity wsEntity = new OvertimeMonthlyWSEntity(caledar, entity.getOvertimes(), entity.getDescription() , entity.getDocumentId(), entity.getMonth());
                    wsEntity.setFirstDateReceive(entity.getFirstDateReceive());
                    wsEntity.setFirstOvertimesReceive(entity.getFirstOvertimesReceive());
                    wsEntity.setEmpOvertimeReceive(entity.getEmpOvertimeReceive().toArray(new EmployeeOvertimeReceiveMonthlyEntity[entity.getEmpOvertimeReceive().size()]));
//                    log.error("ilosc nadgodzin odebranych w "+ entity.getMonth() + " :"+ entity.getEmpOvertimeReceive().size());
                    wsEntity.setHours(entity.hours);
                    wsEntity.setMinutes(entity.minutes);
                    
                    overtimeMonthWS[i] = wsEntity;
                    
                    i++;
                }
            }
            else
                overtimeMonthWS = new OvertimeMonthlyWSEntity[0];
                
            response.setMonthOvertimes(overtimeMonthWS);
        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }

        return response;
    }

    public ReportResponse getReport(ReportRequest request)
    {
        ReportResponse response = new ReportResponse();
        response.checkStatusOk();

        try
        {
            DSApi.openAdmin();

            File card = null;
            int year = request.getYear();
            String username = request.getUsername();

            // tworz� pdf 1,2 roczna ; 3,4 miesieczne
            switch( request.getType() )
            {
                default:
                case 1:
                    card = OvertimeRaportFactory.pdfCard(username, year);
                    break;
                case 2:
                    card = OvertimeRaportFactory.xlsCard(username, year);
                    break;
                case 3:
                    card = OvertimeRaportFactory.pdfMonthlyCard(username, year, request.getMonth());
                    break;
                case 4:
                    card = OvertimeRaportFactory.xlsMonthlyCard(username, year, request.getMonth());
                    break;
            }

            response.setCardReport(FileUtils.encodeByBase64(card));

        }
        catch ( Exception ex )
        {
            response.checkStatusError(ex.getMessage());
            log.debug(ex.getMessage(), ex);
        }
        finally
        {
            DSApi._close();
        }

        return response;
    }
}
