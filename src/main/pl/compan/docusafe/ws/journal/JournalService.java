package pl.compan.docusafe.ws.journal;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.core.GlobalPreferences;
import pl.compan.docusafe.core.dockinds.DocumentKind;
import pl.compan.docusafe.core.dockinds.FieldsManager;
import pl.compan.docusafe.core.dockinds.logic.DocumentLogic;
import pl.compan.docusafe.core.office.AssignmentHistoryEntry;
import pl.compan.docusafe.core.office.Journal;
import pl.compan.docusafe.core.office.OutOfficeDocument;
import pl.compan.docusafe.core.office.workflow.WorkflowFactory;
import pl.compan.docusafe.core.security.AccessDeniedException;
import pl.compan.docusafe.core.users.DSDivision;
import pl.compan.docusafe.parametrization.nfos.NfosLogicable;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;
import pl.compan.docusafe.ws.authorization.AbstractAuthenticator;
import pl.compan.docusafe.ws.nfos.HelperWsNfos;
import pl.compan.docusafe.ws.nfos.RDEFault;

/**
 * Us�uga serwisowa udostepnia metody do rezerwacji i anulacji numeru kancelaryjnego
 * @author <a href="mailto:lukasz.wozniak@docusafe.pl"> �ukasz Wo�niak </a>
 *
 */
public class JournalService implements Serializable
{
	private final static Logger log = LoggerFactory.getLogger(JournalService.class);
	private final static int _minIloscZnakow = 5;
	private DocumentKind documentKind = new DocumentKind();
	
	/**
	 * Rezerwacja numeru KO
	 * @return
	 */
	public String rezerwacjaKO() throws RDEFault {
		Long nrKO = new Long(0);
		
		try {
			AbstractAuthenticator.openContext();
			DSApi.context().begin();
			
			documentKind = DocumentKind.findByCn("blank");
			nrKO = createBlankDocument(documentKind);
			DSApi.context().commit();
		} catch (AccessDeniedException e) {
			log.error("B��d dost�pu");
			throw new RDEFault("ERR-002", "Brak dost�pu");
		} catch (Exception e){
			log.error("", e);
			DSApi.context()._rollback();
			throw new RDEFault();
		} finally {
			DSApi._close();
			AbstractAuthenticator._closeContext();
		}

		return nrKO.toString();
	}
	
	private Long createBlankDocument(DocumentKind documentKind) throws EdmException {
		
		OutOfficeDocument document = new OutOfficeDocument();
		document.setPermissionsOn(false);
		
		setUpDocumentBeforeCreate(document, documentKind);
		
		/* Atrybuty office */
		document.setSummary(documentKind.getName());
        document.setCurrentAssignmentGuid(DSDivision.ROOT_GUID);
        document.setDivisionGuid(DSDivision.ROOT_GUID);
        document.setCurrentAssignmentAccepted(Boolean.FALSE);
        document.setCreatingUser(DSApi.context().getPrincipalName());
        
        document.setForceArchivePermissions(false);
        document.setAssignedDivision(DSDivision.ROOT_GUID);
        document.setClerk(DSApi.context().getPrincipalName());
        
        document.create();
        
      //przypisanie dokumentu do dziennika
        Journal journal = Journal.getMainOutgoing();
        Long journalId;
        journalId = journal.getId();
        Integer sequenceId = null;
        Calendar currentDay = Calendar.getInstance();
        currentDay.setTime(GlobalPreferences.getCurrentDay());
        sequenceId = Journal.TX_newEntry2(journalId, document.getId(), new Date(currentDay.getTime().getTime()));
        document.bindToJournal(journalId, sequenceId);
        
        setUpDocumentAfterCreate(document, documentKind);
        
        document.setTitle("Rezerwacja numeru KO");
		document.setDescription("Document blank");
        
        document.addAssignmentHistoryEntry(new AssignmentHistoryEntry(AssignmentHistoryEntry.IMPORT,DSApi.context().getPrincipalName(), "x","x","x","x", false, AssignmentHistoryEntry.PROCESS_TYPE_REALIZATION), true, null);
        
        WorkflowFactory.createNewProcess(document, true, "Document accepted");
		
        HashMap<String, Object> values = new HashMap<String, Object>();
        values.put("ANULOWANY", false);
        
        document.getDocumentKind().setOnly(document.getId(), values);
		document.getDocumentKind().logic().onStartProcess(document);
        
		return document.getJournal().getSequenceId().longValue();
	}

	private void setUpDocumentBeforeCreate(OutOfficeDocument document, DocumentKind documentKind) throws EdmException {
		documentKind.logic().correctImportValues(document, new HashMap<String, Object>(), new StringBuilder());
		document.setDocumentKind(documentKind);
	}

	private void setUpDocumentAfterCreate(OutOfficeDocument document, DocumentKind documentKind) throws EdmException {
		
		documentKind.logic().archiveActions(document, DocumentLogic.TYPE_OUT_OFFICE);
		documentKind.logic().documentPermissions(document);
	}

	/**
	 * Anulacja numeru kancelaryjnego
	 * @param numerKancelaryjny
	 * @param powod 
	 * @return
	 * @throws Exception, RDEFault
	 * 
	 *  B��dy jakie mog� wyst�pi�:
	 *  ERR-002 - Brak dost�pu
	 *  ERR-006 - B��dny numer kancelaryjny
	 *  ERR-009 - Pow�d anulacji powinien minimalnie 5 znak�w
	 */
	public String anulacjaKO(String numerKancelaryjny, String powod) throws RDEFault {
		Integer nrKO;
		try {
			AbstractAuthenticator.openContext();
			nrKO = new Integer(numerKancelaryjny);
			if (powod.length() < _minIloscZnakow)
				throw new RDEFault("ERR-009", "Pow�d anulacji powinien minimalnie 5 znak�w");
			
			OutOfficeDocument document = getOutDocumentByOfficeNumber(nrKO);
			if(document == null) throw new RDEFault("ERR-006", "B��dny numer kancelaryjny");
			
			HelperWsNfos.sprawdzDokument(document); // TODO
			
			anulujZarezerwowanyDokument(document, powod);
		} catch (NumberFormatException e) {
			log.debug("B��d podczas konwersji numeru kancelaryjnego");
			throw new RDEFault("ERR-006", "B��dny numer kancelaryjny");
		} catch (AccessDeniedException e) {
			throw new RDEFault("ERR-002", "Brak dost�pu");
		} catch (Exception e) {
			throw new RDEFault();
		} finally {
			AbstractAuthenticator._closeContext();
		}
		return "OK";
	}
	
	private void anulujZarezerwowanyDokument(OutOfficeDocument document, String powod){
		try {
			DSApi.context().begin();
			Map<String,Object> fieldValues = new HashMap<String, Object>();
			
			document.setTitle("numer KO anulowany");
			document.setDescription("Powodem anulacji jest: " + powod);
			fieldValues.put(NfosLogicable.ANULOWANY_CN, true);
			document.getDocumentKind().setOnly(document.getId(), fieldValues);
			
			DSApi.context().commit();
		} catch (EdmException e) {
			DSApi.context()._rollback();
			log.error(e.getMessage(), e);
		} finally {
			DSApi._close();
		}
	}
	
	/**
	 * 
	 * @param numerKo
	 * @return
	 */
	private OutOfficeDocument getOutDocumentByOfficeNumber(int numerKo){
        OutOfficeDocument document = null;        
        try {
			List<OutOfficeDocument> listaDocumentow = OutOfficeDocument.findByOfficeNumber(numerKo);
			for (OutOfficeDocument outOfficeDocument : listaDocumentow) {
				Journal j = outOfficeDocument.getJournal();
				if(j.getJournalType().equals(Journal.OUTGOING))
					document = outOfficeDocument;
			}
		} catch (EdmException e) {
			log.error(e.getMessage(), e);
		}
		
		return document;
	}
}
