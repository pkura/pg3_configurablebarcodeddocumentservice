
/**
 * SupplierServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package pl.compan.docusafe.ws.simple;

    /**
     *  SupplierServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class SupplierServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public SupplierServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public SupplierServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getSuppliersByNIP method
            * override this method for handling normal response from getSuppliersByNIP operation
            */
           public void receiveResultgetSuppliersByNIP(
                    pl.compan.docusafe.ws.simple.SupplierServiceStub.GetSuppliersByNIPResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSuppliersByNIP operation
           */
            public void receiveErrorgetSuppliersByNIP(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSuppliersByIdn method
            * override this method for handling normal response from getSuppliersByIdn operation
            */
           public void receiveResultgetSuppliersByIdn(
                    pl.compan.docusafe.ws.simple.SupplierServiceStub.GetSuppliersByIdnResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSuppliersByIdn operation
           */
            public void receiveErrorgetSuppliersByIdn(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for putSupplier method
            * override this method for handling normal response from putSupplier operation
            */
           public void receiveResultputSupplier(
                    pl.compan.docusafe.ws.simple.SupplierServiceStub.PutSupplierResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from putSupplier operation
           */
            public void receiveErrorputSupplier(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSuppliers method
            * override this method for handling normal response from getSuppliers operation
            */
           public void receiveResultgetSuppliers(
                    pl.compan.docusafe.ws.simple.SupplierServiceStub.GetSuppliersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSuppliers operation
           */
            public void receiveErrorgetSuppliers(java.lang.Exception e) {
            }
                


    }
    