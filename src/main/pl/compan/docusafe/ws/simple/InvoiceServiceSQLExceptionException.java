
/**
 * InvoiceServiceSQLExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.compan.docusafe.ws.simple;

public class InvoiceServiceSQLExceptionException extends java.lang.Exception{
    
    private pl.compan.docusafe.ws.simple.InvoiceServiceStub.InvoiceServiceSQLException faultMessage;

    
        public InvoiceServiceSQLExceptionException() {
            super("InvoiceServiceSQLExceptionException");
        }

        public InvoiceServiceSQLExceptionException(java.lang.String s) {
           super(s);
        }

        public InvoiceServiceSQLExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public InvoiceServiceSQLExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.compan.docusafe.ws.simple.InvoiceServiceStub.InvoiceServiceSQLException msg){
       faultMessage = msg;
    }
    
    public pl.compan.docusafe.ws.simple.InvoiceServiceStub.InvoiceServiceSQLException getFaultMessage(){
       return faultMessage;
    }
}
    