//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.01 at 01:12:59 PM CEST 
//


package pl.compan.docusafe.ws.simple.general.dokzak;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IDENTITY_NATURE_C_TYPE.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IDENTITY_NATURE_C_TYPE">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="N"/>
 *     &lt;enumeration value="T"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IDENTITY_NATURE_C_TYPE")
@XmlEnum
public enum IDENTITYNATURECTYPE {

    N,
    T;

    public String value() {
        return name();
    }

    public static IDENTITYNATURECTYPE fromValue(String v) {
        return valueOf(v);
    }

}
