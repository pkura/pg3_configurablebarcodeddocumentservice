
/**
 * DictionaryServicesDocumentTypesCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.6  Built on : Aug 30, 2011 (10:00:16 CEST)
 */

    package pl.compan.docusafe.ws.simple.ul;

    /**
     *  DictionaryServicesDocumentTypesCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DictionaryServicesDocumentTypesCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DictionaryServicesDocumentTypesCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DictionaryServicesDocumentTypesCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getDeliveryRequestType method
            * override this method for handling normal response from getDeliveryRequestType operation
            */
           public void receiveResultgetDeliveryRequestType(
                    pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetDeliveryRequestTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDeliveryRequestType operation
           */
            public void receiveErrorgetDeliveryRequestType(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPurchaseDocumentType method
            * override this method for handling normal response from getPurchaseDocumentType operation
            */
           public void receiveResultgetPurchaseDocumentType(
                    pl.compan.docusafe.ws.simple.ul.DictionaryServicesDocumentTypesStub.GetPurchaseDocumentTypeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPurchaseDocumentType operation
           */
            public void receiveErrorgetPurchaseDocumentType(java.lang.Exception e) {
            }
                


    }
    