
/**
 * ProductServiceSQLExceptionException.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

package pl.compan.docusafe.ws.simple;

public class ProductServiceSQLExceptionException extends java.lang.Exception{
    
    private pl.compan.docusafe.ws.simple.ProductServiceStub.ProductServiceSQLException faultMessage;

    
        public ProductServiceSQLExceptionException() {
            super("ProductServiceSQLExceptionException");
        }

        public ProductServiceSQLExceptionException(java.lang.String s) {
           super(s);
        }

        public ProductServiceSQLExceptionException(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ProductServiceSQLExceptionException(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(pl.compan.docusafe.ws.simple.ProductServiceStub.ProductServiceSQLException msg){
       faultMessage = msg;
    }
    
    public pl.compan.docusafe.ws.simple.ProductServiceStub.ProductServiceSQLException getFaultMessage(){
       return faultMessage;
    }
}
    