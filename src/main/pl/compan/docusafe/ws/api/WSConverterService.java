package pl.compan.docusafe.ws.api;

import org.springframework.http.HttpStatus;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.web.archive.repository.ViewAttachmentRevisionAction;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletResponse;

public class WSConverterService {

    /**
     * Metoda zwraca za��cznik jako pdf w base64
     * @param attachmentId
     * @return
     * @throws Exception
     */
    public String convertAttachmentAsPdf(final Long attachmentId) throws Exception {
        String result = DSApiUtils.inWSContextTransaction(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return ViewAttachmentRevisionAction.convertAttachmentToPdfBase64(attachmentId);
            }
        });
        return result;
    }
}
