package pl.compan.docusafe.ws.api;

import com.google.common.base.Optional;

import org.apache.oro.text.regex.Substitution;
import org.joda.time.DateTime;

import pl.compan.docusafe.api.user.SubstitutionDto;
import pl.compan.docusafe.api.user.SubstitutionService;
import pl.compan.docusafe.api.user.UserDto;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.rest.RestSubstitutionService;
import pl.compan.docusafe.util.DSApiUtils;

import javax.jws.WebParam;

import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestSubstitutionService
 */
public class WsSubstitutionService {

    private final SubstitutionService substitutionService;

    public WsSubstitutionService() {
        this.substitutionService = Docusafe.getBean(SubstitutionService.class);
    }

    public SubstitutionDto[] getCurrentSubstitutions(){
        List<SubstitutionDto> ret = DSApiUtils.inWSContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws Exception {
                return substitutionService.getAllCurrentSubstitutions();
            }
        });

        return ret.toArray(new SubstitutionDto[ret.size()]);
    }

    public SubstitutionDto[] getSubstitutionsAtTime(final Calendar time){
        List<SubstitutionDto> ret = DSApiUtils.inWSContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws Exception {
                return substitutionService.getSubstitutionsAtTime(new DateTime(time));
            }
        });

        return ret.toArray(new SubstitutionDto[ret.size()]);
    }

    public SubstitutionDto getById(final long id){
        return DSApiUtils.inWSContext(new Callable<SubstitutionDto>() {
            @Override
            public SubstitutionDto call() throws Exception {
                return substitutionService.getById(id).orNull();
            }
        });
    }

    public SubstitutionDto getUserSubstitution(@WebParam(name = "user") final UserDto user){
        return DSApiUtils.inWSContext(new Callable<SubstitutionDto>() {
            @Override
            public SubstitutionDto call() throws Exception {
                return substitutionService.getUserSubstitution(user).orNull();
            }
        });
    }

    public SubstitutionDto[] getSubstitutionsByUser(@WebParam(name = "user") final UserDto user){
        List<SubstitutionDto> ret = DSApiUtils.inWSContext(new Callable<List<SubstitutionDto>>() {
            @Override
            public List<SubstitutionDto> call() throws Exception {
                return substitutionService.getSubstitutedByUser(user);
            }
        });
        return ret.toArray(new SubstitutionDto[ret.size()]);
    }

    public long create(@WebParam(name = "substitution") final SubstitutionDto substitutionDto){
        return DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return substitutionService.create(substitutionDto);
            }
        });
    }

    public boolean cancel(@WebParam(name = "substitutionId") final long id){
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                Optional<SubstitutionDto> subs = substitutionService.getById(id);
                if(subs.isPresent()){
                    substitutionService.cancel(subs.get());
                } else {
                    throw new IllegalArgumentException("Substitution " + id + " not found");
                }
                return null;
            }
        });
        return true;
    }
}
