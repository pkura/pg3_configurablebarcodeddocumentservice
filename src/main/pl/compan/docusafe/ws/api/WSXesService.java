package pl.compan.docusafe.ws.api;

import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.xes.XesXmlFactory;
import pl.compan.docusafe.rest.RESTXesService;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RESTXesService
 */
public class WSXesService {

    private static final Logger log = LoggerFactory.getLogger(WSXesService.class);

    /**
     * Zwraca XesLog Data po dokumencie
     * @param documentId
     * @return
     * @throws Exception
     */

    public String getLogByDocumentId(final String documentId) throws Exception {

        String result = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Long docId = CmisPrepend.lastId(documentId);
                return new XesXmlFactory().getXesLogBase64Document(docId, true);
            }
        });

        return returnResult(result);
    }

    /**
     * Zwraca XesLog Data po użytkowniku
     * @param userId
     * @return
     * @throws Exception
     */

    public String getLogByUserId( final Long userId, final Date time) throws Exception {
        String result = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return new XesXmlFactory().getXesLogBase64User(userId, time, true);
            }
        });

        return returnResult(result);
    }

    /**
     * Zwraca XesLog Data po użytkowniku dla wyznaczonego zakresu
     * @param userId
     * @return
     * @throws Exception
     */

    public String getLogByUserIdRange( final Long userId, final Date starttime, final Date endtime) throws Exception {
        String result = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return new XesXmlFactory().getXesLogBase64User(userId, starttime, endtime, true);
            }
        });

        return returnResult(result);
    }


    protected String returnResult(String result) {
        if(StringUtils.isNotEmpty(result)){
            return result;
        } else {
            return "false";
        }
    }
}
