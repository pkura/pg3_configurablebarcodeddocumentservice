package pl.compan.docusafe.ws.api;

import com.beust.jcommander.internal.Lists;
import com.google.common.base.Optional;

import edu.emory.mathcs.backport.java.util.Arrays;
import org.apache.commons.lang.StringUtils;

import pl.compan.docusafe.api.user.*;
import pl.compan.docusafe.api.user.office.KeyElement;
import pl.compan.docusafe.boot.Docusafe;
import pl.compan.docusafe.core.DSApi;
import pl.compan.docusafe.core.EdmException;
import pl.compan.docusafe.rest.ResourceNotFoundException;
import pl.compan.docusafe.rest.RestUserService;
import pl.compan.docusafe.util.DSApiUtils;

import javax.xml.bind.annotation.XmlElement;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestUserService
 *
 */
public class WsUserService {

    final UserService userService;
    final DivisionService divisionService;
    final SubstitutionService substitutionService;

    public WsUserService(){
        userService = Docusafe.getBean(UserService.class);
        divisionService = Docusafe.getBean(DivisionService.class);
        substitutionService = Docusafe.getBean(SubstitutionService.class);
    }

    public UserDto[] getAllUsers(){
        List<UserDto> ret = DSApiUtils.inWSContext(new Callable<List<UserDto>>() {
            @Override
            public List<UserDto> call() throws Exception {
                return userService.getAllUsers();
            }
        });
        return ret.toArray(new UserDto[ret.size()]);
    }

    public UserDto getById(final String id){
        return DSApiUtils.inWSContext(new Callable<UserDto>() {
            @Override
            public UserDto call() throws Exception {
                Optional<UserDto> ret = userService.getByIdOrNameOrExternalName(id);

                if (ret.isPresent()) {
                    return ret.get();
                } else {
                    throw new ResourceNotFoundException("User '" + id + "' not found");
                }
            }
        });
    }

    public UserDto getByName(final String name){
        return DSApiUtils.inWSContext(new Callable<UserDto>() {
            @Override
            public UserDto call() throws Exception {
                return userService.getByIdOrNameOrExternalName(name).orNull();
            }
        });
    }

    public long create(final UserDto user){
       return DSApiUtils.inWSContextTransaction(new Callable<Long>() {
            @Override
            public Long call() throws Exception {
                return userService.create(user);
            }
        });
    }

    public Boolean update(final UserDto user){
        return DSApiUtils.inWSContextTransaction(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                userService.update(user);
                return true;
            }
        });
    }

    public Boolean delete(final String id){
        return DSApiUtils.inWSContextTransaction(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                userService.delete(userService.getByIdOrNameOrExternalName(id).get());
                return true;
            }
        });
    }

    public KeyElement[] getAvailableRoles(){
        List<KeyElement> elements = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return userService.getAvailableOfficeRoles();
            }
        });

        return elements.toArray(new KeyElement[elements.size()]);
    }

    public KeyElement[] getUserRoles(final String username){
        List<KeyElement> elements = DSApiUtils.inWSContext(new Callable<List<KeyElement>>() {
            @Override
            public List<KeyElement> call() throws Exception {
                return userService.getUserOfficeRoles(username);
            }
        });

        return elements.toArray(new KeyElement[elements.size()]);
    }

    public boolean updateUserRoles(final String username, final Long[] roleIds){
        DSApiUtils.inWSContextTransaction(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                userService.updateUserOfficeRoles(Arrays.asList(roleIds), username);
                return null;
            }
        });

        return true;
    }

    /**
     * Lista dzia��w do kt�rych nale�y u�ytkownik
     * @param userId id, name lub externalName/personNumber
     * @return lista dzia��w u�ytkownika
     * @throws Exception
     */
    public DivisionDto[] getDivisions(final String userId) throws Exception {
        List<DivisionDto> divs = DSApiUtils.inWSContext(new Callable<List<DivisionDto>>() {
            @Override
            public List<DivisionDto> call() throws EdmException {
                Optional<UserDto> user = userService.getByIdOrNameOrExternalName(userId);
                if (user.isPresent()) {
                    return divisionService.getUserDivisions(user.get());
                } else {
                    throw new ResourceNotFoundException("User '" + userId + "' not found");
                }
            }
        });

        return divs.toArray(new DivisionDto[divs.size()]);
    }
}
