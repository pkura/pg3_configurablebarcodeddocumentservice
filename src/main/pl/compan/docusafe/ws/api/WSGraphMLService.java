package pl.compan.docusafe.ws.api;

import org.apache.commons.lang.StringUtils;
import pl.compan.docusafe.core.cmis.model.CmisPrepend;
import pl.compan.docusafe.core.graphs.GraphGenerator;
import pl.compan.docusafe.core.xes.XesXmlFactory;
import pl.compan.docusafe.util.DSApiUtils;
import pl.compan.docusafe.util.Logger;
import pl.compan.docusafe.util.LoggerFactory;

import java.util.Date;
import java.util.concurrent.Callable;

/**
 * @see pl.compan.docusafe.rest.RestGraphMLService
 */
public class WSGraphMLService {

    private static final Logger log = LoggerFactory.getLogger(WSGraphMLService.class);

    /**
     * Zwraca GraphML Data po dokumencie
     * @param documentId
     * @return
     * @throws Exception
     */

    public String getLogByDocumentId(final String documentId) throws Exception {

        String result = DSApiUtils.inWSContext(new Callable<String>() {
            @Override
            public String call() throws Exception {
                Long docId = CmisPrepend.lastId(documentId);
                return new GraphGenerator().generateGraphFromDocIdBase64(docId,false, true);
            }
        });

        return returnResult(result);
    }

    protected String returnResult(String result) {
        if(StringUtils.isNotEmpty(result)){
            return result;
        } else {
            return "false";
        }
    }
}
